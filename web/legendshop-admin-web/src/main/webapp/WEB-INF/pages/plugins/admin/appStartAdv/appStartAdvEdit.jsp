<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>移动装修 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" href="<ls:templateResource item='/resources/plugins/zoom/css/zoom.css'/>" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
							<span class="title-span">
								移动装修  ＞
								<a href="<ls:url address="/admin/appStartAdv/query"/>">App启动广告</a>
								<c:if test="${not empty bean.id }"><span style="color:#0e90d2;">  ＞  广告编辑</span></c:if>
								<c:if test="${empty bean.id }"><span style="color:#0e90d2;">  ＞ 广告新增</span></c:if>
							</span>
						</th>
					</tr>
				</thead>
			</table>

			<form:form action="${contextPath}/admin/appStartAdv/${empty bean.id?'add':'update' }" method="POST" id="beanForm" enctype="multipart/form-data">
				<input type="hidden" id="id" name="id" value="${bean.id }" />
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%">
						<tbody>
							<tr>
								<td style="width:200px;">
									<div align="right">
										<span class="required">*</span>广告名称：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input type="text" id="name" name="name" style="width:300px;"
									value="${bean.name }" class="${inputclass}" /></td>
							</tr>
							<tr>
								<td align="right" valign="top">
									<div>跳转URL：</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input type="text" id="url" name="url" style="width:300px;"
									value="${bean.url}" placeholder="http://" class="${inputclass}" />
									<span class="tips">设置用户点击广告时跳转的URL</span></td>
							</tr>
							<tr>
								<td align="right" valign="top">
									<div>
										<span class="required">*</span>广告图片：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><c:if test="${not empty bean.imgUrl }">
										<img style="margin-bottom:10px;" src="<ls:photo item='${bean.imgUrl}'/>" width="90"
											height="160" data-action="zoom" />
										<br />
									</c:if> <input type="file" id="imgFile" name="imgFile"
									oldFile="${bean.imgUrl}" class="file" /> <span class="tips">广告图片建议像素(宽*高)
										720 * 1280 及以上</span></td>
							</tr>
							<c:if test="${empty bean}">
								<tr>
									<td valign="top">
										<div align="right" >
											<span class="required">*</span>状态：
										</div>
									</td>
									<td align="left" style="padding-left: 2px;"><select id="status" name="status"
										class="${selectclass }">
											<option value="0"
												<c:if test="${empty bean.status or bean.status eq 0}">selected="selected"</c:if>>下线</option>
											<option value="1"
												<c:if test="${bean.status eq 1}">selected="selected"</c:if>>上线</option>
									</select> <span class="tips">只有上线状态的广告才会在app启动时显示</span></td>
								</tr>
							</c:if>
							<tr>
								<td align="right" valign="top">
									<div>广告描述：</div>
								</td>
								<td align="left" style="padding-left: 2px;"><textarea id="description" name="description"
									style="width:300px;" class="textarea" rows="5" cols="60">${bean.description }</textarea>
								</td>
							</tr>
							<tr>
								<td></td>
								<td align="right" style="padding-left: 2px;">
									<div>
										<input class="${btnclass}" type="submit" value="保存" /> 
										 
										<input class="${btnclass}" name="returnBtn" type="button" value="返回" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<!-- 图片居中放大插件 -->
<script type="text/javascript" scr="<ls:templateResource item='/resources/common/js/transition.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/zoom/js/zoom.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>' ></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/appStartAdvEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var isEdit = "${bean}";
	var urls = {
		list : contextPath + "/admin/appStartAdv/query",
		checkNameIsExist : contextPath + "/admin/appStartAdv/checkNameIsExits"
	}
</script>
</html>