<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>二级域名 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
 	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
    
    <!-- 标题 -->
    <table class="${tableclass}" style="width: 100%">
	    <thead>
	    	<tr>
	    		<th><strong class="am-text-primary am-text-lg">商城管理</strong> /  二级域名</th>
	    	</tr>
	    </thead>
   	</table>
   	
   	<!-- tab -->
    <div style="margin-left: 0.5rem">
		<div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
          	 <li class="am-active"><i></i><a href="<ls:url address='/admin/secDomainName/query'/>">域名列表</a></li>
         	 <li><i></i><a href="<ls:url address='/admin/secDomainName/setting/'/>">设置</a></li>
         </ul>
        </div>
   </div>
   
   <!-- 条件搜索 -->
   <form:form action="${contextPath}/admin/secDomainName/query" id="form1" method="get">
    <table class="${tableclass}" style="width: 100%">
     <thead>
     	<tr>
     		<td>
			 <div align="left" style="padding: 3px">
			  <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
			            二级域名: <input type="text" maxlength="50" name="secDomainName" value="${shopDetail.secDomainName }" class="${inputclass}" placeholder="请输入二级域名"/>
			            &nbsp;&nbsp;
			            店铺名称: <input type="text" maxlength="50" name="siteName" value="${shopDetail.siteName }" class="${inputclass}" placeholder="请输入店铺名称"/>
			            <input type="button" onclick="search()" value="搜索" class="${btnclass}" />
			 </div>
 			</td>
 			</tr>
 		</thead>
    </table>
    </form:form>
    
    <!-- 数据表格 -->
    <div align="center">
       <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
       
       <display:table name="list" requestURI="/admin/secDomainName/query" id="item"
         export="false" class="${tableclass}" style="width:100%" sort="external">
         
	       <display:column title="顺序"  style="min-width:50px;"><%=offset++%></display:column>
		   <display:column title="二级域名">${item.secDomainName}</display:column>
		   <display:column title="注册时间" property="secDomainRegDate" format="{0,date,yyyy-MM-dd HH:mm}"></display:column>
		   <display:column title="店铺名字">${item.siteName}</display:column>
	   
		  <!-- 操作列按钮 -->   
	      <display:column title="操作" media="html"   style="width: 250px;">
             	  <div class="am-btn-toolbar">
		      		<div class="am-btn-group am-btn-group-xs">
					    <button id="check" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="toSite('${item.secDomainName}')"><span class="am-icon-search" ></span> 查看</button>
	      				<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.shopId}')" ><span class="am-icon-trash-o"></span> 删除</button> 
					</div>
				</div>
	      </display:column>
	 </display:table>
	 
 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
 </div>
 </div>
 </div>
 </body>
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/secDomainNameList.js'/>"></script>
 <script type="text/javascript">
 	var contextPath = '${contextPath}';
 </script>
 </html>
