<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<script src="<ls:templateResource item='/common/default/js/alternative.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	function pager(curPageNO){
	    document.getElementById("curPageNO").value=curPageNO;
	    document.getElementById("form1").submit();
	}
</script>

	<form:form  action="${contextPath}/admin/coupon/usecoupon_view/${couponId}" id="form1"
		method="post">
		<table class="${tableclass}" style="width: 100%">
			<thead>
			<tr>
				<th><strong class="am-text-primary am-text-lg">优惠券管理</strong> / 礼券使用情况</th>
			</tr>
		</thead>
			<tbody>
				<tr>
					<td>
						<div align="left" style="padding: 3px"> 
						   <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				            	&nbsp;用户名:
				             <input class="${inputclass}" type="text" name="userName" maxlength="50" value="${userName}" />
				             <input class="${btnclass}" type="submit" value="搜索"/>
				              <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/coupon/query"/>'" />
						</div>
				   </td>
				</tr>
			</tbody>
		</table>
	</form:form>
	<div align="center">
		<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/coupon/usecoupon_view/${couponId}" id="item"
			export="false" sort="external" class="${tableclass}"
			style="width:100%">
			<display:column title="用户名称" property="userName"></display:column>
			<display:column title="券号" property="couponSn" sortable="true" sortName="couponSn"></display:column>
			<display:column title="领取渠道 " property="getSources"></display:column>
			<display:column title="领取时间" sortable="true" sortName="getTime">
			   <c:choose>
			  		<c:when test="${empty item.getTime || item.getTime==''}">
			  		     未领取
			  		</c:when>
			  		<c:otherwise>
			  		  <fmt:formatDate value="${item.getTime}" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
			<display:column title="使用时间" sortable="true" sortName="useTime">
			   <c:choose>
			  		<c:when test="${empty item.useTime || item.useTime==''}">
			  		     未使用
			  		</c:when>
			  		<c:otherwise>
			  		   <fmt:formatDate value="${item.useTime}" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
			<display:column title="抵扣订单号" sortable="true" sortName="orderNumber">
			     <c:choose>
			  		<c:when test="${empty item.orderNumber}">
			  		     无
			  		</c:when>
			  		<c:otherwise>
			  		   ${item.orderNumber}
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
			<display:column title="订单总金额" property="orderPrice" sortable="true" sortName="orderPrice">
			    <c:choose>
			  		<c:when test="${empty item.orderPrice}">
			  		   无
			  		</c:when>
			  		<c:otherwise>
			  		   ${item.orderPrice}
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
			<display:column title="用户使用状态" sortable="true" sortName="useStatus">
			   <c:choose>
			  		<c:when test="${item.useStatus == 1}">
			  		     未使用
			  		</c:when>
			  	     <c:when test="${item.useStatus==2}">
			  		     已使用
			  		</c:when>
			  		<c:otherwise>
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
				<display:column title="是否绑定" sortable="true">
			   <c:choose>
			  		<c:when test="${empty item.userId}">
			  		   未绑定
			  		</c:when>
			  		<c:otherwise>
			  		    已绑定
			  		</c:otherwise>
			  	</c:choose>
			</display:column>
		</display:table>
		<c:if test="${not empty toolBar}">
			<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
		</c:if>
	</div>

