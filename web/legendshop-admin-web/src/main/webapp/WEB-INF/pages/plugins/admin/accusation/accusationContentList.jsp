<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
	 <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/accusation/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	   	
	   	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="店铺">
				<a href="<ls:url address="/admin/shopDetail/load/${item.shopId}"/>" target="_blank">${item.shopName}</a>
		</display:column>
	   	<display:column title="商品">
	   		 <a href="${PC_DOMAIN_NAME}/views/${item.prodId}"  target="_blank" title="${item.prodName}" style="margin: 10px;">
	       	<img width="65" height="65"  src="<ls:images item='${item.prodPic}' scale='3'/>" alt="${item.prodName}">
	       	</a>
	   	</display:column>
	   		<display:column title="举报人用户ID" property="userName"></display:column>
	   		<display:column title="举报人昵称" property="nickName"></display:column>
	   		<display:column title="举报主题" property="title"></display:column>
	   		<display:column title="举报时间" >
	   			<fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss" />
	   		</display:column>
	   		<display:column title="状态"  style="width:100px">
	   			<c:choose>
	   			<c:when test="${item.status ==1}">已处理</c:when>
	   			<c:otherwise>未处理</c:otherwise>
	   			</c:choose>
	   		</display:column>
	   		<display:column title="处理结果"  style="width:100px">
	   			<c:choose>
	   			<c:when test="${item.result ==1}">无效举报</c:when>
	   			<c:when test="${item.result ==2}">有效举报</c:when>
	   			<c:when test="${item.result ==3}">恶意举报</c:when>
	   			<c:otherwise></c:otherwise>
	   			</c:choose>
	   		</display:column>    		
	   <display:column title="操作" media="html"  style="width:100px">
	   
	      <%-- <a href="<ls:url address='/admin/accusation/load/${item.id}'/>" title="详情">
	     		 详情
	      </a>
	      <c:if test="${item.status ==0}">
		      <a href="<ls:url address='/admin/accusation/handle/${item.id}'/>" title="处理">
		      		处理
		      </a>
	      </c:if> --%>
	      <div class="table-btn-group">
		      <a href="<ls:url address='/admin/accusation/load/${item.id}'/>" title="详情">
		     		 详情
		      </a>
		      <c:if test="${item.status ==0}">
			      <span class="btn-line">|</span> 
		      </c:if>
		      <c:if test="${item.status ==0}">
			      <a href="<ls:url address='/admin/accusation/handle/${item.id}'/>" title="处理">
			      		处理
			      </a>
		      </c:if>
	      </div>
	     </display:column>
	   </display:table>
	      <!-- 分页条 -->
     	<div class="clearfix" style="margin-bottom: 100px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
