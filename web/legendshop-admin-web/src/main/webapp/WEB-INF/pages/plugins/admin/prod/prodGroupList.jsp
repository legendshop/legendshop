<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>商品分组 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
       <jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
		  	 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<table class="${tableclass} title-border" style="width: 100%">
			    <tr>
			        <th class="title-border">商品管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">商品分组</span></th>
			    </tr>
			</table>
			<div>
			    <div class="seller_list_title">
			        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
			        </ul>
			    </div>
			</div>
			<form:form action="${contextPath}/admin/prodGroup/query" id="form1" method="get">
               <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
               <div class="criteria-div">
               	<span class="item">
              	     	分组名称：
                   	<input type="text" id="name" name="name" class="${inputclass}" maxlength="50" value="${prodGroup.name}"/>
                   </span>
                   <span class="item">
                    <input type="button" onclick="search()" class="${btnclass}" value="搜索"/>
                    <input type="button" value="创建分组" class="${btnclass}" onclick='window.location="${contextPath}/admin/prodGroup/load"'/>
                </span>
               </div>
			</form:form>
		<div align="center" id="prodGroupContentList" name="prodGroupContentList" class="order-content-list"></div>
   </div>
   </div>
   </body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/prodGroupList.js'/>"></script>
<script language="javascript">
	var contextPath = "${contextPath}";
	var shopName = '${brand.shopName}';
</script>
</html>
