<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	int offset = ((Integer) request.getAttribute("offset")).intValue();
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<script language="JavaScript" type="text/javascript">
  function selAll(){
    chk = document.getElementById("checkbox");
	allSel = document.getElementsByName("strArray");
	
	if(!chk.checked)
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=false;
		}
	else
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=true;
		}
	}
	function  saveLeastOne()
{
	 //获取选择的记录集合
	selAry = document.getElementsByName("strArray");
	count = 0;
	selValue = "";
	//判断是否只有一条记录被选中
	for(i=0;i<selAry.length;i++)
	{
		if(selAry[i].checked)
		{
			count++;
			selValue = selAry[i].value;
		}
	}
	if(count==0)
	{
		layer.msg('请选择要增加的用户角色！', {icon: 0});
		return false;
	}
	else
	{
      return true;
	}
	  return false;
} 

</script>

</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">增加用户[${bean.name }]角色</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/user/saveRoleToUser"
				id="form1" name="form1">
				<input type="hidden" id="curPageNO" name="curPageNO"
					value="${curPageNO}">
				<input type="hidden" id="appNo" name="appNo" value="${param.appNo}">
				<input type="hidden" name="userId" value="${bean.id }">
				<div align="center" class="order-content-list" style="margin-top: 20px;">
					<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
					<display:table name="list" id="item" export="false"
						class="${tableclass}" style="width:100%">
						<display:column style="width:70px" title="
							<span>
					           <label class='checkbox-wrapper'>
									<span class='checkbox-item'>
										<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
										<span class='checkbox-inner' style='margin-left:10px;'></span>
									</span>
							   </label>	
							</span>">
							<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" value="${item.id}" name="strArray" class="checkbox-input selectOne" onclick="selectOne(this);"/>
									<span class="checkbox-inner" style="margin-left:10px;"></span>
								</span>
						   </label>	
						</display:column>
						<display:column title="名称 " property="name" sortable="true"></display:column>
						<display:column title="角色名称 " property="roleType" sortable="true"></display:column>
						<display:column title="状态">
							<ls:optionGroup type="label" required="true" cache="true"
								beanName="ENABLED" selectedValue="${item.enabled}"
								defaultDisp="" />
						</display:column>
						<display:column title="备注" property="note"></display:column>
					</display:table>
					<div style="margin-top: 30px;">
					<input class="${btnclass}" type="submit" value="保存"
						onclick="return saveLeastOne();" /> <input class="${btnclass}"
						type="button" value="返回" onclick="window.history.go(-1)" />
					</div>
				</div>
			</form:form>
			<div align="center">
				<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
			</div>
		</div>
	</div>
</body>
</html>
