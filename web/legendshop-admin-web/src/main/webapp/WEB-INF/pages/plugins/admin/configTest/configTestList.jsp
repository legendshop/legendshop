<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>配置测试 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/publishProd1${_style_}.css'/>"
	rel="stylesheet" />
</head>
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
				<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">系统配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">配置测试</span></th>
					</tr>
				</table>

			<div align="center" class="order-content-list" style="margin-top: 15px;">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/system/config/query"
					id="item" export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="顺序" class="orderwidth" style="width:50px"><%=offset++%>
					</display:column>
					<display:column style="width:100px;" title="功能说明"
						property="configItemExplain"></display:column>
					<display:column style="width:100px;" title="参数名称"
						property="configItemName"></display:column>
					<display:column
						style="max-width:200px;word-break:break-all;"
						title="参数"><div class="order_img_name">${item.configItemParas}</div></display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>

			<table class="${tableclass}" id="col1"
				style="width: 100%; margin-left: 30px;;border:none;margin-bottom: 60px;">
				<tr style="border-bottom: none;">
					<td colspan="2" style="border-bottom: none;text-align: left;padding-top:25px;padding-bottom: 0;"><span style="font-weight: bold;">短信测试</span><br>
						<form:form action="${contextPath}/admin/system/config/sendSMSCode"
								method="post" name="userRegForm" id="userRegForm">
	               					 请输入正确的手机号码：
	                		<input type="text" class="text" id="tel" maxlength="11"
									name="tel" placeholder="请输入11位手机号码">
							<button type="submit" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="" style="background: #0e90d2;color: white;">短信测试</button>
						</form:form>
						<span>${sendSMSResult}</span>
					</td>
				</tr>
				<%--<tr style="border-bottom: none;">--%>
					<%--<td style="border-bottom: none;text-align: left;height: 40px;padding-top:0;padding-bottom: 8px;"><form:form--%>
							<%--action="${contextPath}/admin/system/config/sendSMSCode"--%>
							<%--method="post" name="userRegForm" id="userRegForm">--%>
                <%--请输入正确的手机号码：--%>
                <%--<input type="text" class="text" id="tel" maxlength="11"--%>
								<%--name="tel" placeholder="请输入11位手机号码">--%>
							<%--<button type="submit"--%>
								<%--class="am-btn am-btn-default am-btn-xs am-text-secondary"--%>
								<%--onclick="">短信测试</button>--%>
						<%--</form:form></td>--%>
				<%--</tr> --%>
				 <%--<tr style="border-bottom: none;text-align: left;">--%>
					<%--<td style="border-bottom: none;text-align: left;height: 45px;"><span>${sendSMSResult}</span></td>--%>
				<%--</tr>  --%>
				 <tr>
					<td colspan="2" style="text-align:left">邮件测试</td>
				</tr> 

			   <tr>
					<td style="text-align:left">
					<form:form action="${contextPath}/admin/system/config/sendMail" method="post" name="userRegForm" id="userRegForm">
                     请输入有效的邮件地址：
                <input type="text" class="text" id="email" name="email"
								placeholder="邮件地址">
							<button type="submit"
								class="am-btn am-btn-default am-btn-xs am-text-secondary"
								onclick="" style="color:white;background-color:#0e90d2">邮件测试</button>
						</form:form></td>
				</tr> 
				<%-- <tr>
					<td><span>${sendMailResult}</span></td>
				</tr> --%>
				<tr style="border-bottom: none;"> 
					<td colspan="2" style="border-bottom: none;text-align: left;height: 45px;padding-top: 30px;"><span style="font-weight: bold;">图片测试</span><br>
					<input type="file" id="file" name="file" onchange="" multiple="multiple" style="display: inline-block;width: 292px;">
					<button type="submit" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="uploadFile();" style="background: #0e90d2;color: white;">图片测试</button>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/configTestList.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
</script>
</html>