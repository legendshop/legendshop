<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/system/district/savecity"
				method="post" id="form1">
				<input id="id" name="id" value="${city.id}" type="hidden">
				<input id="provinceid" name="provinceid" value="${province.id}"
					type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass}" id="col1"
						style="width: 100%">
						<thead>
							<tr class="sortable">
								<th colspan="2">
									<div align="center">[${province.province }] - 城市管理</div>
								</th>
							</tr>
						</thead>

						<tr>
							<td style="width: 30%">
								<div align="right">
									名称: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input class="${inputclass}" type="text" name="city"
								id="city" value="${city.city}" maxlength="20" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									邮编: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input class="${inputclass}" type="text" name="cityid"
								id="cityid" value="${city.cityid}" maxlength="10" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input class="${btnclass}" type="submit" value="保存" /> 
								    <input class="${btnclass}" type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/system/district/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({});
	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				city : "required",
				cityid : {
					number : true,
					required : true
				}
			},
			messages : {
				city : "请输入城市名称",
				cityid : {
					number : "请输入正确的邮编，必须是数字",
					required : "请输入邮编"
				}
			}
		});

	});
</script>
</html>