<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<a href="<ls:url address="/system/menu/query"/>">菜单管理</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">编辑菜单</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/system/menu/save" method="post"
				id="form1">
				<input id="menuId" name="menuId" value="${menu.menuId}"
					type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
						<thead>
							<tr class="sortable">
								<%-- <th colspan="2">
									<div align="center">
										<c:if test="${allParentMenu !=null }">
											<c:forEach items="${allParentMenu }" var="menu">
						    		${menu.value } &nbsp;＞ 
						    	</c:forEach>
										</c:if>
										创建
										<c:choose>
											<c:when test="${parentMenu !=null}">${parentMenu.grade +1}级</c:when>
											<c:otherwise>1级</c:otherwise>
										</c:choose>
										菜单
									</div>
								</th> --%>
							</tr>
						</thead>
						<tr>
							<td width="200px">
								<div align="right">
									<font color="ff0000">*</font>名称:
								</div>
							</td>
							<td align="left"><input type="text" name="name" id="name"
								value="${menu.name}" size="30" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">国际化标签:</div>
							</td>
							<td align="left"><input type="text" name="label" id="label"
								value="${menu.label}" size="30" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>标题:
								</div>
							</td>
							<td align="left">
								<input type="text" name="title" id="title" autocomplete="off" value="${menu.title}" size="30" />
							</td>
						</tr>
						<c:if test="${parentMenu != null}">
							<tr>
								<td>
									<div align="right">父节点:</div>
								</td>
								<td align="left"><select id="parentId" name="parentId">
										<ls:optionGroup type="select" required="true" cache="fasle"
											defaultDisp="-- 父节点 --"
											sql=" select m.menu_id, m.name from ls_menu m where m.grade = ?"
											param="${parentMenu.grade}"
											selectedValue="${parentMenu.menuId}" />
								</select></td>
							</tr>
						</c:if>
						<tr>
							<td>
								<div align="right">连接地址:</div>
							</td>
							<td align="left"><input type="text" name="action" id="action"
								value="${menu.action}" size="50" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">由那个插件提供的功能:</div>
							</td>
							<td align="left"><input type="text" name="providedPlugin"
								id="providedPlugin" value="${menu.providedPlugin}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>顺序:
								</div>
							</td>
							<td align="left"><input type="text" name="seq" id="seq"
								value="${menu.seq}" size="5" maxlength="5" /></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div align="left">
									<input class="${btnclass}" type="submit" value="保存" /> 
									<c:choose>
										<c:when test="${parentMenu != null }">
											<input class="${btnclass}" type="button" value="返回"
												onclick="window.location='${contextPath}/system/menu/queryChildMenu/${parentMenu.menuId}'" />
										</c:when>
										<c:otherwise>
											<input class="${btnclass}" type="button" value="返回"
												onclick="window.location='<ls:url address="/system/menu/query"/>'" />
										</c:otherwise>
									</c:choose>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/templets/amaze/js/appdecorate/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({});

	$(document)
			.ready(
					function() {
						jQuery("#form1")
								.validate(
										{
											rules : {
												name : {
													required : true,
													maxlength:15
												},
												title : {
													required:true,
													maxlength:15
												},
												seq : {
													required : true,
													integer : true,
													maxlength:5
												},
												action : {
													maxlength:50
												},
												providedPlugin : {
													maxlength:15
												},
												label : {
													maxlength:15
												}
												
											},
											messages : {
												name : {
													required : '<fmt:message key="simple.errors.required"/>'
												},
												title : {
													required : '<fmt:message key="simple.errors.required"/>'
												},
												seq : {
													required : '<fmt:message key="simple.errors.required"/>',
													number : '<fmt:message key="errors.number"><fmt:param value=""/></fmt:message>'
												},

											}
										});
						highlightTableRows("col1");
						//斑马条纹
						$("#col1 tr:nth-child(even)").addClass("even");
					});
</script>
</html>
