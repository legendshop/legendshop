<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
  <c:set var="_userDetail" value="${shopApi:getSecurityUserDetail(pageContext.request)}"></c:set>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>"/>
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>"/>
  <link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet"/>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <form:form method="post" action="${contextPath}/admin/news/save" id="form1" onsubmit="return checkFinish();">
    <table class="${tableclass}" style="width: 100%">
        <thead>
        <tr>
            <th class="no-bg title-th">
            	<span class="title-span">
					帮助中心  ＞
					<a href="<ls:url address="/admin/news/query"/>">帮助文章</a>
					<c:if test="${not empty news }">  ＞  <span style="color:#0e90d2;">文章编辑</span></c:if>
					<c:if test="${empty news }">  ＞  <span style="color:#0e90d2;">文章新增</span></c:if>
				</span>
            </th>
        </tr>
        </thead>
      </table>
        <table style="width:100%;" class="${tableclass} no-border content-table" id="col1">

            <tr>
                <td width="200px;" align="right"><font color="FF0000">*</font>文章栏目：</td>
                <td align="left" style="padding-left: 2px;">
                    <select id="newsCategoryId" name="newsCategoryId">
                        <ls:optionGroup type="select" required="false" cache="fasle" defaultDisp="-- 请选择 --"
                                        sql="select t.news_category_id, t.news_category_name from ls_news_cat t where t.status = 1" param="${_userDetail.shopId}" selectedValue="${news.newsCategoryId}"/>
                    </select>
                </td>
            </tr>
                <%--<tr>--%>
                <%--<td>文章标签</td>--%>
                <%--<td>--%>
                <%--<input type="text" id="newsTags" name="newsTags" value="${news.newsTags}" style="width: 460px;" class="text text-short" style="margin-left: 0px;">&nbsp;文章对应的标签，选填，可多选--%>
                <%--</td>--%>
                <%--</tr>--%>
            <tr>
                <td align="right">文章位置：</td>
                <td align="left" style="line-height:28px;padding-left: 2px;">
                    <input type="text" id="positionTags" name="positionTags" value="${news.positionTags}" style="width: 460px;" class="text text-short" style="margin-left: 0px;">&nbsp;文章对应的位置，选填，可多选
                </td>
            </tr>
            <tr>
                <td align="right">状态：</td>
                <td align="left" style="line-height:28px;padding-left: 2px;">
                    <select id="status" name="status">
                        <ls:optionGroup type="select" required="true" cache="true"
                                        beanName="ONOFF_STATUS" selectedValue="${news.status}"/>
                    </select>

                        <%--&nbsp;高亮--%>
                        <%--<select id="highLine" name="highLine">--%>
                        <%--<ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${news.highLine == null ? 0 : news.highLine}"/>--%>
                        <%--</select>--%>
                    &nbsp;&nbsp; <font>顺序&nbsp;&nbsp;</font>
                    <input type="text" name="seq" id="seq" value="${news.seq}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" style="width:50px;"/>
                    
                </td>
            </tr>
            <tr>
                <td align="right">
                   	 <font color="FF0000">*</font>文章标题：
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" name="newsTitle" id="newsTitle" size="40" maxlength="50" class=input value="${news.newsTitle}"/>
                    <input type="hidden" id="newsId" name="newsId" value="${news.newsId}"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <span title="如果不填写则以文章内容前面部分代替" style="cursor: pointer;">文章提要：</span>
                </td>
                <td align="left" style="padding-left: 2px;"><input type="text" name="newsBrief" id="newsBrief" size="100" maxlength="100" class=input value="${news.newsBrief}"/></td>
            </tr>
            <tr>
	            <td align="right" valign="top">
	                   	 文章内容：
                </td>
                <td colspan="2" style="padding-left: 2px;">
                    <textarea name="newsContent" id="newsContent" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;" maxlength="60000">${news.newsContent}</textarea>
                </td>
            </tr>

        </table>
        <div style="text-align: center;">
	        <input type="submit" value="保存" class="${btnclass}"/>
	        <input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/news/query/${position}'"/>
        </div>

        </form:form>
   </div>
   </div>
   </body>

<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>" type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
   var contextPath = "${contextPath}";
   var UUID = "${cookie.SESSION.value}";
   var tagData = [];
   <c:forEach items="${tags}" var="item">
   tagData.push({id: '${item.itemId}', text: '${item.itemName}'});
   </c:forEach>
   
 //文章位置
   var positiontagData = [];
   <c:forEach items="${positionItem}" var="item">
   		positiontagData.push({id: '${item.positionId}', text: '${item.positionName}'});
   </c:forEach>
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/news.js'/>"></script>
</html>
