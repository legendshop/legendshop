<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销活动管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">
						<!-- <a href="<ls:url address='/admin/index'/>" target="_parent">首页</a> &raquo; 商城管理  &raquo; 微信管理  &raquo; 
		    	<a href="<ls:url address='/admin/drawActivity/query'/>">抽奖活动</a> -->
						平台营销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">抽奖活动</span>
					</th>
				</tr>
		    </table>
			<form:form action="${contextPath}/admin/drawActivity/query"
				id="form1" method="get">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							活动名称：<input type="text" name="actName" maxlength="50" value="${drawActivity.actName}" class="${inputclass}" /> 
						</span>
						<span class="item">
							活动类型：
							<select id="actType" name="actType" class="criteria-select"">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="ACTIVE_TYPE"
									selectedValue="${drawActivity.actType}" />
							</select> 
						</span>
						<span class="item">
							开始时间：<input readonly="readonly" class="${inputclass}" name="startTime" id="startTime"  type="text"
								value='<fmt:formatDate value="${drawActivity.startTime}" pattern="yyyy-MM-dd"/>' />
						</span>
						<span class="item">
							结束时间：<input readonly="readonly" class="${inputclass}" name="endTime" id="endTime" type="text"
								value='<fmt:formatDate value="${drawActivity.endTime}" pattern="yyyy-MM-dd"/>' />
							<input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
							<input type="button" value="新增抽奖活动" onclick='window.location="<ls:url address='/admin/drawActivity/load'/>"' class="${btnclass}" />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/drawActivity/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">

					<display:column title="序号" style="width:4%" class="orderwidth"><%=offset++%></display:column>
					<display:column style="width:9%" title="活动名称"><div class="order_img_name">${item.actName}</div></display:column>
					<display:column title="活动类型">
						<c:choose>
							<c:when test="${item.actType == 'DA_ZHUAN_PAN'}">大转盘</c:when>
							<c:when test="${item.actType == 'ZA_JIN_DAN'}">砸金蛋</c:when>
						</c:choose>
					</display:column>
					<display:column title="状态" style="width:5%">
						<c:set value="${item.endTime}" var="endTime"></c:set>
						<c:choose>
							<c:when test="${nowDate gt endTime}">
							已过期
						</c:when>
							<c:when test="${nowDate lt endTime}">
								<c:if test="${item.actStatus eq 1}">
									<span>上线</span>
								</c:if>
								<c:if test="${item.actStatus eq 0}">下线</span>
								</c:if>
							</c:when>
						</c:choose>
					</display:column>
					<display:column title="开始时间" property="startTime"
						format="{0,date,yyyy-MM-dd HH:mm:ss}" style="width:11%"></display:column>
					<display:column title="结束时间" property="endTime"
						format="{0,date,yyyy-MM-dd HH:mm:ss}" style="width:11%"></display:column>
					<display:column title="个人每日抽奖次数" property="timesPerday"></display:column>
					<display:column title="个人抽奖总次数" property="timesPerson"></display:column>
					<display:column title="中奖可继续参与">
						<c:choose>
							<c:when test="${item.winJoinStatus == 0}">否</c:when>
							<c:when test="${item.winJoinStatus == 1}">是</c:when>
						</c:choose>
					</display:column>
					<%-- 	<display:column title="绑定手机可参与" >
     		       <c:choose>
						<c:when test="${item.bindJoinStatus == 0}">否</c:when>
						<c:when test="${item.bindJoinStatus == 1}">是</c:when>
				  </c:choose>
     		</display:column> --%>
					<!-- 操作 -->
					<display:column title="操作" media="html">
							<div class="table-btn-group" data-am-dropdown>
							<c:choose>
								<c:when test="${nowDate lt endTime}">
									<button class="tab-btn"
										name="statusChange" itemId="${item.id}"
										itemName="${item.actName}" status="${item.actStatus}">
										<c:if test="${item.actStatus eq 1}">下线
									</button>
									<span class="btn-line">|</span>
									</c:if>
									<c:if test="${item.actStatus eq 0}">上线</button>
									<span class="btn-line">|</span>
									</c:if>
								</c:when>
								<c:when test="${nowDate gt endTime}">
									<button class="tab-btn"
										name="statusChange" itemId="${item.id}"
										itemName="${item.actName}" status="${item.actStatus}"
										disabled="disabled">
										已过期
									</button>
									<span class="btn-line">|</span>
								</c:when>
							</c:choose>
							<%-- <c:if test="${nowDate lt endTime}">
	                 		<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusChange"  itemId="${item.id}" itemName="${item.actName}" status="${item.actStatus}" style="color:#f37b1d;"><span class="am-icon-arrow-down"></span>下线</button>
	                 </c:if>
							<c:if test="${item.actStatus eq 0}">
                              <button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusChange"  itemId="${item.id}" itemName="${item.actName}" status="${item.actStatus}" style="color:#f37b1d;"><span class="am-icon-arrow-up"></span>上线</button>
                     </c:if>
							 <c:if test="${nowDate gt endTime}">
                           	<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusChange"  itemId="${item.id}" itemName="${item.actName}" status="${item.actStatus}" style="color:#f37b1d;" disabled="disabled"><span class="am-icon-arrow-down"></span>已过期</button>
                     </c:if> --%>
							<button
								class="tab-btn am-dropdown-toggle"
								data-am-dropdown-toggle>
								<span class="am-icon-cog"></span> <span
									class="am-icon-caret-down"></span>
							</button>
							<ul class="am-dropdown-content">
								<li><a href="javascript:void(0);"
									onclick="window.location='${contextPath}/admin/drawActivity/load/${item.id}'">1.
										查看或编辑</a></li>
								<li><a href="javascript:void(0);"
									onclick="toDrawRecord('${item.id}','1');">2. 中奖纪录</a></li>
								<li><a href="javascript:void(0);"
									onclick="toDrawRecord('${item.id}','2');">3. 抽奖纪录</a></li>
								<li><a href="javascript:void(0);"
									onclick="checkLink('${item.id}','${item.actName}')">4. 复制链接</a></li>
								<c:if test="${item.actStatus eq 0}">
									<li><a href="javascript:void(0);"
										onclick="deleteById('${item.id}','${item.actStatus}');">5.
											删除</a></li>
								</c:if>
							</ul>
						</div>
						
					</display:column>


				</display:table>
				<div class="clearfix">
			   		<div class="fr">
			   			 <div cla ss="page">
			    			 <div class="p-wrap">
			        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						 	</div>
			  			 </div>
			  		</div>
		   		</div>
			</div>
		</div>
	</div>
	<script language="JavaScript" type="text/javascript">
		$(document)
				.ready(
						function() {
							$("button[name='statusChange']")
									.click(
											function(event) {
												$this = $(this);
												changeStatus(
														$this.attr("itemId"),
														$this.attr("itemName"),
														$this.attr("status"),
														"${contextPath}/admin/drawActivity/updateActStatus/",
														$this, "${contextPath}");
											});

						});

		/* 更改订单状态 */
		function changeStatus(itemId, itemName, status, url, target, path) {
			var desc;
			var toStatus;
			if (status == 1) {
				toStatus = 0;
				desc = '活动【' + itemName + '】 下线?';
			} else {
				toStatus = 1;
				desc = '活动【' + itemName + '】 上线?';
			}
			
			layer.confirm(desc, {icon:3}, function(){
				jQuery.ajax({
					url : url + itemId + "/" + toStatus,
					type : 'get',
					async : false, //默认为true 异步   
					dataType : 'json',
					error : function(data) {
						alert(data);
					},
					success : function(data) {
						if (data == "OK") {
							window.location.reload();
						} else if (data == "fail") {
							layer.msg("操作失败！", {icon:2});
						} else if (data == "overdue") {
							layer.msg("该活动已过期！", {icon:0});
						}
					}
				});
				return true;
			});
		}

		/* 删除 */
		function deleteById(id, actStatus) {
			layer.confirm('确认删除？', {icon:3}, function(){
				jQuery.ajax({
					url : "${contextPath}/admin/drawActivity/delete",
					data : {
						"id" : id,
						"actStatus" : actStatus
					},
					type : 'POST',
					async : true, //默认为true 异步   
					dataType : 'json',
					error : function(data) {
						layer.alert("网络异常,请稍后重试！",{icon:2});
					},
					success : function(data) {
						if (data == "OK") {
							window.location.reload();
						} else if (data == "rollout") {
							layer.alert('该活动已上线，禁止删除！', {icon:0});
						} else {
							layer.alert('删除失败！', {icon:2});
						}
					}
				});
				return true;
			});
		}

		/*跳转记录页面*/
		function toDrawRecord(actId, status, awardsType) {
			var temp = document.createElement("form");
			if (status == "1") {
				temp.action = "${contextPath}/admin/drawWinRecord/query";
			} else if (status == "2") {
				temp.action = "${contextPath}/admin/drawRecord/query";
			}
			temp.method = "get";
			temp.style.display = "none";
			var opt = document.createElement("input");
			opt.name = 'actId';
			opt.value = actId;
			temp.appendChild(opt);
			document.body.appendChild(temp);
			temp.submit();
			return temp;
		}

		/*二维码链接页面*/
		function checkLink(id, actName) {
			layer.open({
				title : "【" + actName + "】链接",
				id : 'editGift',
				type:2,
				content: contextPath + "/admin/drawActivity/qrCode/" + id,
				area: ['590px', '280px']
			});
		}
		function search() {
			$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			$("#form1")[0].submit();
		}
		
		 laydate.render({
	   		 elem: '#startTime',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
	</script>

</html>

