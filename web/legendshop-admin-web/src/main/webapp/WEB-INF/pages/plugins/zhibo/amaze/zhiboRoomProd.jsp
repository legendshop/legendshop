<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>直播管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<tr>
					<th><strong class="am-text-primary am-text-lg">直播管理</strong> /
						直播房间商品列表</th>
				</tr>
			</table>

			<div style="margin-left: 0.5rem">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty seckillActivity.status&& empty seckillActivity.overdue}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/zhibo/zhiboRoomRrod"/>">全部商品</a></li>
					</ul>
				</div>
			</div>


			<form:form action="${contextPath}/admin/zhibo/zhiboRoomRrod"
				id="form1" method="get">
				<table class="${tableclass}" style="width: 100%">
					<tbody>
						<tr>
							<td>
								<div align="left" style="padding: 3px">
									<input type="hidden" id="curPageNO" name="curPageNO"
										value="${curPageNO}" /> &nbsp;房间号 <input type="text"
										name="avRoomId" maxlength="50" class="${inputclass}"
										value="${avRoomId}" placeholder="请输入房间号搜索" /> &nbsp;商品名称 <input
										type="text" name="prodName" maxlength="50" value="${prodName}"
										class="${inputclass}" placeholder="请输入商品名称搜索" /> <input
										type="submit" value="搜索" class="${btnclass}" />

								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="admin/zhibo/zhiboRoomRrod"
					id="item" export="false" sort="external" class="${tableclass}">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="商品名称" property="name"></display:column>
					<display:column title="商品">
						<a href="<ls:url address='/views/${item.prodId}'/>"
							target="_blank" title="${item.name}" style="margin: 10px;"> <img
							width="65" height="65"
							src="<ls:images item='${item.pic}' scale='3'/>"
							alt="${item.name}">
						</a>
					</display:column>

					<display:column title="直播房间号" property="avRoomId"></display:column>
					<display:column title="商品价格" property="cash"></display:column>
					<display:column title="商品库存" property="stocks"></display:column>



				</display:table>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
			</div>
		</div>
	</div>
</body>
</html>
