<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>帮助栏目 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">帮助中心&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">帮助栏目</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/newsCategory/query"
				id="form1" method="get">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							栏目名称：
							<input type="text" name="newsCategoryName" maxlength="50" value="${bean.newsCategoryName}" class="${inputclass}" placeholder="请输入栏目名搜索" /> 
						</span>
						<span class="item">
						状态：
						   <select id="status" name="status" class="criteria-select">
								<ls:optionGroup type="select" required="true" cache="true"
									beanName="ONOFF_STATUS" selectedValue="${bean.status}" />
						   </select> 
							<input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
							<input type="button" value="创建栏目" class="${btnclass}" onclick='window.location="${contextPath}/admin/newsCategory/load"' />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/newsCategory/query"
					id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="栏目名称" property="newsCategoryName"></display:column>
					<display:column title="状态">
						<c:choose>
							<c:when test="${item.status == 0}">
								<font color="red">下线</font>
							</c:when>
							<c:otherwise>上线</c:otherwise>
						</c:choose>

					</display:column>
					<display:column title="建立时间" property="newsCategoryDate"
						format="{0,date,yyyy-MM-dd HH:mm}" sortable="true"></display:column>
					<display:column title="顺序" property="seq"></display:column>
					<display:column title="操作" media="html" style="width:150px">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/newsCategory/load/${item.newsCategoryId}'">
								编辑
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.newsCategoryId}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<!-- 分页条 -->
		     	<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 每个文章都是挂在栏目下面，是文章的分类<br>
						2. 栏目处于上线状态页面可见，处于下线状态页面不可见<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
<!--
	function deleteById(id) {
		layer.confirm('确定删除 ?', {icon:3}, function(){
			window.location = "${contextPath}/admin/newsCategory/delete/" + id;
		});
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	highlightTableRows("item");
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
//-->
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>

