<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = 1;
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>分类广告- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">商品管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">分类广告</span>
					<%-- <a href="${contextPath}/admin/adminDoc/view/3"><img src="${contextPath}/resources/templets/img/adminDocHelp.png" /></a> --%>
					</th>
				</tr>
			</table>
			<div align="center" style="margin-top:16px;"  class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="topCatList" requestURI="/admin/brand/query"
					id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth" style="width:20%;"><%=offset++%></display:column>
					<display:column title="分类" property="name" style="width:30%;"></display:column>
					<display:column title="操作" media="html" style="width:40%;">
							<div class="table-btn-group">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/category/recomm/update/${item.id}'">编辑分类广告</button>
								<c:if test="${ not empty item.recommCon}">
									<span class="btn-line">|</span>
								</c:if>
								<c:if test="${ not empty item.recommCon}">
									<button class="tab-btn" onclick="initSet('${item.id}');">还原导航设置</button>
								</c:if>
							</div>
					</display:column>
				</display:table>
			</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 一级分类才能编辑 分类广告<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	function initSet(id) {
		layer.confirm('是否确认还原导航设置?', {icon:3, title:'提示'}, function(index) {
			var url = "${contextPath}/admin/category/recomm/initSetting/" + id;
			jQuery.ajax({
				type : 'POST',
				"url" : url,
				datatype : "json",
				success : function(result) {
					var data = eval(result);
					if (data == "OK") {
						layer.alert("操作成功！", {icon:1});
						window.location.reload();
					} else {
						layer.alert("操作失败！", {icon:2});
						return;
					}
				}
			});
			layer.close(index);
		});
	}
</script>
</html>
