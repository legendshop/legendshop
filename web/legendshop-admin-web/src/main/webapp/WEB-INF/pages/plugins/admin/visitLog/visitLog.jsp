<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<table class="${tableclass}" style="width: 100%">
    <thead>
    	<tr><th> <strong class="am-text-primary am-text-lg">报表管理</strong> /  浏览编辑</th></tr>
    </thead>
</table>

        <form:form  action="${contextPath}/admin/visitLog/save" method="post" id="form1">
            <input id="visitId" name="visitId" value="${bean.visitId}"  type="hidden"/>
             <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                
      <tr>
        <td>
          <div align="center">Ip: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="ip" id="ip" value="${bean.ip}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">Country: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="country" id="country" value="${bean.country}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">Area: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="area" id="area" value="${bean.area}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">UserName: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="userName" id="userName" value="${bean.userName}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">ShopName: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="shopName" id="shopName" value="${bean.shopName}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">ProductName: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="productName" id="productName" value="${bean.productName}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">Page: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="page" id="page" value="${bean.page}" class="${inputclass}" />
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">Date: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="date" id="date" value="${bean.date}" class="${inputclass}" />
        </td>
      </tr>

                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="添加" class="${btnclass}" />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='${contextPath}/admin/visitLog/query'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script language="javascript">
    $.validator.setDefaults({
    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
      $("#col1 tr").each(function(i){
      if(i>0){
         if(i%2 == 0){
             $(this).addClass('even');
         }else{    
              $(this).addClass('odd'); 
         }   
    }
     });   
         $("#col1 th").addClass('sortable'); 
});
</script>

