<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
 <%
     Integer offset = (Integer) request.getAttribute("offset");
 %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>活动抽奖列表- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
		<table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">平台营销&nbsp;＞&nbsp;<a href="<ls:url address="/admin/drawActivity/query"/>">抽奖活动</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">活动抽奖记录</span>
			    	</th>
		    	</tr>
		</table>
    <form:form action="${contextPath}/admin/drawRecord/query" id="form1" method="post">
   	    <div class="criteria-div">
	       	     <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	       	     <input type="hidden" id="actId" name="actId" value="${drawRecord.actId}" />
	       	     <span class="item">
	       	                       微信昵称&nbsp;
	       	        <input  name="weixinNickname" id="weixinNickname"  type="text"  value="${drawRecord.weixinNickname}"/>
	       	     </span>
	       	     <span class="item">
	            	抽奖时间&nbsp;
      		        <input readonly="readonly"  name="startTime" id="startTime" class="Wdate" type="text"  value='<fmt:formatDate value="${drawRecord.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'/>
      		        &nbsp;-&nbsp;
      		        <input readonly="readonly"  name="endTime" id="endTime" class="Wdate" type="text" value='<fmt:formatDate value="${drawRecord.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
      		     </span>
      		     <span class="item">  
      		                      中奖状态&nbsp;
	            	<select name="winStatus" class="criteria-select">
	            	     <option value="">--请选择--</option>
	            	     <option value="0" <c:if test="${drawRecord.winStatus eq 0}"> selected="selected" </c:if>>未中奖</option>
	            	     <option value="1" <c:if test="${drawRecord.winStatus eq 1}"> selected="selected" </c:if>>中奖</option>
	            	</select>
	             </span>
	            <span class="item">
	            	<input type="submit" value="搜索" class="${btnclass}"/>
	            </span>
	      </div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/drawRecord/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
     	    <display:column title="序号"  class="orderwidth"><%=offset++%></display:column>
     		<display:column title="活动" property="actName"></display:column>
     		<%-- <display:column title="微信openId" property="weixinOpenid"></display:column> --%>
     		<display:column title="用户昵称" property="weixinNickname"></display:column>
     		<display:column title="抽奖时间" property="awardsTime" format="{0,date,yyyy-MM-dd HH:mm:ss}"></display:column>
     		<display:column title="是否中奖" >
     		               <c:if test="${not empty item.winId}">
     		                       <span>中奖</span>
     		               </c:if>
     		               <c:if test="${empty item.winId}">
     		                       <span>未中奖</span>
     		               </c:if>
     		</display:column>

	    <%--  <display:column title="操作" media="html">
	    	<div class="am-btn-toolbar">
			      <div class="am-btn-group am-btn-group-xs">
		     		<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}');" ><span class="am-icon-trash-o"></span> 删除</button>
		       	</div>
		      </div>
	     </display:column> --%>
	    </display:table>
        <div class="clearfix">
	   		<div class="fr">
	   			 <div cla ss="page">
	    			 <div class="p-wrap">
	        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				 	</div>
	  			 </div>
	  		</div>
   		</div>
    </div>
    </div>
    </div>
    </body>
 <script language="JavaScript" type="text/javascript">
	  function deleteById(id) {
		  layer.confirm(" 确定删除 ?", {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   window.location = "<ls:url address='/admin/drawRecord/delete/" + id + "'/>";
			   });
	    }
	
       function pager(curPageNO){
           document.getElementById("curPageNO").value=curPageNO;
           document.getElementById("form1").submit();
       }
       
       laydate.render({
	   		 elem: '#startTime',
	   		 type:'datetime',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     type:'datetime',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
</script>
</html>

