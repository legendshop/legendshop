<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>店铺管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">报表管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/advAnalysis"/>">广告点击量统计</a>
              		&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">浏览详情</span>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/advAnalysis/detail"
				id="form1" method="post">
					<tbody>
						<tr>
							<input type="hidden" id="curPageNO" name="curPageNO"
								value="${curPageNO}" />
							<input type="hidden" id="url" name="url" value="${url}" />
						</tr>
					</tbody>
			</form:form>
			<div align="center" class="order-content-list" style="margin-top: 15px;">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/advAnalysis/detail"
					id="item" export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="广告链接" property="url"></display:column>
					<display:column title="用户IP" property="ip"></display:column>
					<display:column title="来源页面" property="sourcePage"></display:column>
					<display:column title="访问时间">
						<fmt:formatDate value="${item.createTime}"
							pattern="yyyy-MM-dd HH:mm:ss " type="date" />
					</display:column>
					<display:column title="用户名" property="userName"></display:column>
				</display:table>
				<c:if test="${not empty toolBar}">
					<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
				</c:if>
				<div class="clearfix">
		       		<div class="fl">
			       		<input type="button" style="background: #0e90d2;color: white;" value="返回" onclick="window.location.href='${contextPath}/admin/advAnalysis'" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
</html>