<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>报表管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<input type="hidden" id="" />
			<form:form action="${contextPath}/admin/report/sales" id="form1" method="post">
				<table class="${tableclass}" style="width: 100%">
					<thead>
						<tr>
							<th><strong class="am-text-primary am-text-lg">报表管理</strong>
								/ 销售额总览</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<div align="left" style="padding: 3px">
									&nbsp; 店铺名： <input type="text" name="shopName" id="shopName"
										maxlength="50" value="${reportRequest.shopName}"
										class="${inputclass}" /> <select id="queryTerms"
										class="criteria-select" name="queryTerms"
										onchange="change(this.value)">
										<ls:optionGroup type="select" required="true" cache="true"
											beanName="SALE_QUERY_TERMS"
											selectedValue="${salesRequest.queryTerms}" />
									</select> <span id="range"> <input readonly="readonly"
										name="selectedDate" id="selectedDate" 
										type="text"
										value='<fmt:formatDate value="${salesRequest.selectedDate}" pattern="yyyy-MM-dd"/>' />
									</span> <input type="button" value="搜索" class="${btnclass}"
										id="submitOrder" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>


			<input type="hidden" id="salesResponseJSON" value='${reportJson}' />
			<div id="main" style="margin-left: 10px; width: 85%; height: 450px"></div>

		</div>
	</div>
</body>
<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/sales.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var selectedYear = '${salesRequest.selectedYear}';
	var selectedMonth = '${salesRequest.selectedMonth}';
	var selectedDay = '<fmt:formatDate value="${salesRequest.selectedDate}" pattern="yyyy-MM-dd"/>';
	var selectedWeek = '${reportRequest.selectedWeek}';
	
</script>
</html>
