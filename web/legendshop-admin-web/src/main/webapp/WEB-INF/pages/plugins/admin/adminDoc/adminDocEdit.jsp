<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet"/>

 <body>
    	<jsp:include page="/admin/top" />
    	<div class="am-cf admin-main">
	    	<jsp:include page="../frame/left.jsp"></jsp:include>
	    	<div class="admin-content" id="admin-content" style="overflow-x:auto;text-align: center;">
	    	<table class="${tableclass} title-border" style="width: 100%" >
		    	 <tr>
		    	 	<th class="title-border">系统管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/adminDoc/query"/>">帮助管理</a>
		    	 		&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">帮助文档编辑</span>
		    	 	</th>
		    	 </tr>
			</table>
				<form:form  method="post" action="${contextPath}/admin/adminDoc/save"  id="form1" onsubmit="return checkFinish();">
					       <table style="width:100%;margin-top: 15px;" class="${tableclass} no-border" id="col1">
					         
					         <tbody>
					         	<tr>
					            <td width="200px;">
						            <div align="right">
						                 	<font color="FF0000">*</font>新闻编号：
						            </div>
					            </td>
					            <td>
						            <div align="left">
						            	<input type="text" name="id" id="id" size="40" class=input  value="${adminDoc.id}" />
						            </div>
					            </td>
					          </tr>
								<tr>
						            <td>
							            <div align="right">
							             	   <font color="FF0000">*</font> 新闻标题：
							            </div> 
						            </td>
						            <td>
							            <div align="left">
							            	<input type="text" name="title" id="title" size="40" class=input  value="${adminDoc.title}" maxlength="50"/>
							            </div>	
						            </td>
					           </tr>
					           <tr>
						           <td>
							            <div align="right" style="margin-top: -207px;">
							             	   <font color="FF0000">*</font> 新闻内容：
							            </div> 
						            </td>
						            <td align="left">
											<textarea name="content"  id="content" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;">${adminDoc.content}</textarea>
						            </td>
					          </tr>
					
							</tbody>
				        </table>
				       <input type="submit" value="保存" class="${btnclass}" />
				        <input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/adminDoc/query'" />
				     	<input type="hidden" id="id" name="id" value="${adminDoc.id}"/>
				      </form:form>
			</div>	
		</div>		
</body>

  
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>" type="text/javascript"></script>
<script type="text/javascript">
 function checkFinish(){
	 var result = checkSensitiveData();	 
	 if(result != null && result != '' ){ 		
	   		layer.msg("您的输入含有敏感字： '"+result+"' 请更正后再提交",{icon:0}); 	
	   		return false;
	 }else{
   		return true;
 	}			
  }


jQuery.validator.addMethod("checkSensitiveData1", function(value, element) {
    return checkSensitiveData(value);}, ''); 

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				id:{
					required : true,
					digits:true,
					maxlength:11,
					isNotExist:true
				},
				title : {
					required : true,
					minlength : 3,
					maxlength:30
				},
				content: {
					required : true,
					minlength : 10
					//checkSensitiveData1:true		
				}
			},
			messages : {
				id:{
					required : "请输入帮助文档的数字编号",
					digits : "请输入合法的整数数字",
					maxlength : "长度不能大于11位",
					isNotExist:"此编号已存在，请重新输入"
				},
				title : {
					required : "请输入帮助文档标题",
					minlength : "请认真填写，帮助文档标题不能少于3个字符"
				},
				content:{
		               required: "请输入帮助文档内容",
		               minlength : "请认真填写，帮助文档内容不能少于10个字符"
		        }
			}
		});
		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
		KindEditor.options.filterMode=false;
			 KindEditor.create('textarea[name="content"]', {
				cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
				uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
				fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
				allowFileManager : true,
				afterBlur:function(){this.sync();},
				width : '100%',
				height:'400px',
				afterCreate : function() {
					var self = this;
					KindEditor.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					KindEditor.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			prettyPrint();
	});

	function checkSensitiveData() {
		var content =  $("#content").val();
		var url = "${contextPath}/admin/sensitiveWord/filter";
		var result;
		$.ajax({
			"url" : url,
			data : {
				"src" : content
			},
			type : 'post',
			dataType : 'json',
			async : false, //默认为true 异步   
			error : function(jqXHR, textStatus, errorThrown) {
			},
			success : function(retData) {
				result = retData;
			}
		});
		return result;
	}
	$("document").ready(function(){
		var adminDocId= $("#id").val();
		var idIsEdit=true;
		if(adminDocId !=null&adminDocId !=""){
			$("#id").attr("disabled","disabled");
			 idIsEdit=false;
		}
		
		jQuery.validator.addMethod("isNotExist",function(value, element){
			var isExist=false;
			if(idIsEdit){
						$.ajax({
									url:contextPath+"/admin/adminDoc/checkAdminDocId",
									data:{id:$("#id").val()},
									type:"post",
									async:false,
									error: function(jqXHR, textStatus, errorThrown) {
							 			 console.log(textStatus, errorThrown);
									},
									success:function(result){
										if("true"==result){
												isExist=true;
										}
							}
		
						});
			}
			return this.optional(element) || !isExist;  
		},"此编号已存在，请重新输入")
});
</script>