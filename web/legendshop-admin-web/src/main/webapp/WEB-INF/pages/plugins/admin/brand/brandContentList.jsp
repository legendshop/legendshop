<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
	    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	    <display:table name="list" requestURI="/admin/brand/query" id="item"
	                   export="false" class="${tableclass}" sort="external">
	    <display:column style="width: 80px; " title="
		    <span>
	           <label class='checkbox-wrapper ' style='float:left;margin-left:20px;'>
					<span class='checkbox-item'>
						<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
						<span class='checkbox-inner' style='margin-left:10px;'></span>
					</span>
			   </label>	
			</span>" >
	    	<c:choose>
	    		<c:when test="${item.status==-1||item.status==0||item.status==-2}">
				   <label class="checkbox-wrapper checkbox-wrapper-disabled" style="float:left;margin-left:20px;">
						<span class="checkbox-item">
							<input type="checkbox" name='brandId' value='${item.brandId}' status='${item.status}' class="checkbox-input" disabled='disabled'"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:when>
	    		<c:otherwise>
	    			<label class="checkbox-wrapper" style="float:left;margin-left:20px;">
						<span class="checkbox-item">
							<input type="checkbox" name='brandId' value='${item.brandId}' status='${item.status}' class="checkbox-input selectOne" onclick="selectOne(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:otherwise>
	    	</c:choose>
	    </display:column>
	    
	    <%-- <display:column title="顺序" class="orderwidth"><%=offset++%> 
	    </display:column>
	    --%>
	    <display:column title="店铺">
	    <a href="<ls:url address='/admin/shopDetail/load/${item.shopId}'/>">${item.shopName}
	        </display:column>
	        <display:column title="品牌名称">
	        	<a href="<ls:url address='/admin/brand/check/${item.brandId}'/>">${item.brandName}
	        </display:column>
	        <display:column title="品牌Logo">
	        <c:if test="${not empty item.brandPic}"><a href="<ls:url address='/admin/brand/check/${item.brandId}'/>"><img width="90" height="30" src="<ls:photo item='${item.brandPic}'/>"/></a></c:if>
	        </display:column>
	        <display:column title="申请时间" sortable="true" sortName="applyTime">
	            <fmt:formatDate value="${item.applyTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
	        </display:column>
	        <display:column title="状态">
	        <c:choose>
	        <c:when test="${item.status eq -2}">审核失败</c:when>
	        <c:when test="${item.status eq -1}">审核中</c:when>
	        <c:when test="${item.status eq 0}">下线中</c:when>
	        <c:when test="${item.status eq 1}">上线中</c:when>
	        </c:choose>
	        </display:column>
	        <display:column title="次序">${item.seq}</display:column>
	        <display:column title="推荐品牌">
	        <c:choose>
	        	<c:when test="${'1' eq item.commend}">是</c:when>
	        	<c:when test="${'0' eq item.commend}">否</c:when>
	            <c:otherwise></c:otherwise>
	        </c:choose>
	        </display:column>
	        <display:column title="操作" media="html" style="width:200px">
	            <div class="table-btn-group">
	                <c:choose>
	                	<c:when test="${item.status==-1}">
		      				<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/brand/check/${item.brandId}'">审核</button>
		      				<span class="btn-line">|</span>
		      			</c:when>
	                    <c:when test="${item.status == 1}">
	                        <button class="tab-btn" name="statusImg" itemId="${item.brandId}" itemName="${item.brandName}" status="1">下线</button>
	                        <span class="btn-line">|</span>
	                    </c:when>
	                    <c:when test="${item.status == 0}">
	                        <button class="tab-btn" name="statusImg" itemId="${item.brandId}" itemName="${item.brandName}" status="0">上线</button>
	                        <span class="btn-line">|</span>
	                    </c:when>
	                </c:choose>
	                <c:if test="${item.status!=-1}">
	                    <button class="tab-btn" onclick="window.location.href='${contextPath}/admin/brand/check/${item.brandId}'">查看</button>
	                    <span class="btn-line">|</span>
	                </c:if>
	                <c:if test="${item.status!=-2 }">
	                	<button class="tab-btn" onclick="window.location='${contextPath}/admin/brand/update/${item.brandId}'">编辑</button>
	                	<span class="btn-line">|</span>
	                </c:if>
	                <button class="tab-btn" onclick="deleteById('${item.brandId}');">删除</button>
	            </div>
	        </display:column>
	        </display:table>
	       	<div class="clearfix">
	       		<div class="fl">
		       		<c:if test="${empty brand.status}">
						<input type="button" value="批量下线" onclick="batchOff" id="batchOff" class="batch-btn">
					</c:if>
		<!--        <input type="button" value="批量上线" id="batch-on"> -->
		<!--        <input type="button" value="批量删除" id="batch-del"> -->
				</div>
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
	        
<script type="text/javascript">
$("button[name='statusImg']").click(function (event) {
	$this = $(this);
	initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), contextPath+"/admin/brand/updatestatus/", $this, "${contextPath}");
}
);
$("#batchOff").on("click",function(){
	var brandIdsStr=getCheckedIds();
	console.log("brandIdsStr => ", brandIdsStr);
	if(brandIdsStr==""){
		layer.msg("请选择要下线的品牌", {icon:7});
		return;
	}

	
	layer.confirm("是否批量下线选中品牌 ?", {icon:3, title:'提示'}, function (index) {
		
		jQuery.ajax({
			url: contextPath+"/admin/brand/batchOffOrOnLine/"+brandIdsStr,
			type: 'get',
			async: false, //默认为true 异步  
			data:{brandStatus:0},
			dataType: 'json',
			success: function (data) {
				if (data.successValue == "Y") {
					layer.msg(data.msg, {icon:1});
					setInterval(function(){
						location.href=contextPath+"/admin/brand/query";
					},2000);
				}else{
					layer.msg(data.msg, {icon:2});
				}
			}
		});
		
		layer.close(index);
	});
	
});
</script>
