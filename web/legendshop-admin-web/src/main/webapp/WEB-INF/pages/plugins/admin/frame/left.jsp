<%@ page language="java" pageEncoding="UTF-8" %>
<%
	//记录当前页
	  	Integer currentMenuId = (Integer)request.getSession().getAttribute("currentMenuId");
	  	if(currentMenuId == null){
	  		request.getSession().setAttribute("currentMenuId", 0);
	  	}
 %>
 <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
 		<%-- ${currentMenuId} --%>
 		<jsp:include page="/admin/menuContent/${currentMenuId}" />
 </div>