<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
<body>
    <form:form  action="${contextPath}/admin/mobileFloor/query" id="form1" method="get">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  首页楼层装修</th></tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							 楼层名称： <input type="text" name="name" maxlength="50" value="${mobileFloor.name}" class="${inputclass}" />
				            <input type="button" onclick="search()" value="搜索" class="${btnclass}" />
				            <input type="button" value="创建首页楼层" class="${btnclass}" onclick='window.location="<ls:url address='/admin/mobileFloor/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/mobileFloor/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
     		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="楼层标题" property="name"></display:column>
     		<display:column title="链接" property="url"></display:column>
     		<display:column title="次序" property="seq" style="width:60px"></display:column>
     		<display:column title="创建时间"><fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss" /></display:column>

	    <display:column title="操作" media="html"  style="width:130px">
		      <div class="am-btn-toolbar">
		  		<div class="am-btn-group am-btn-group-xs">
		            <c:choose>
				  		<c:when test="${item.status == 1}">
				  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusImg"  itemId="${item.id}"  itemName="${item.name}"  status="${item.status}" style="color:#f37b1d;"><span class="am-icon-arrow-down"></span>下线</button>
				  		</c:when>
				  		<c:otherwise>
				  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusImg"  itemId="${item.id}"  itemName="${item.name}"  status="${item.status}" style='color:#0e90d2;'><span class="am-icon-arrow-up"></span>上线</button>
				  		</c:otherwise>
				  	</c:choose>

                <div data-am-dropdown="" class="am-dropdown">
                <button data-am-dropdown-toggle="" class="am-btn am-btn-default am-btn-xs am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
                <ul class="am-dropdown-content">
                  <li><a href="<ls:url address='/admin/mobileFloor/decorate/${item.id}'/>">楼层编辑</a></li>
                
                  <li><a href="<ls:url address='/admin/mobileFloor/load/${item.id}'/>">修改</a></li>
                  
                  <li><a href='javascript:deleteById("${item.id}")'>删除</a></li>
                </ul>
              	</div>
	    
              </div>
            </div>
	    </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
    </div>
        
</body>

<script language="JavaScript" type="text/javascript">
  function deleteById(id) {
      if(confirm("确定删除 ?")){
            window.location = "<ls:url address='/admin/mobileFloor/delete/" + id + "'/>";
        }
    }

  function pager(curPageNO){
      document.getElementById("curPageNO").value=curPageNO;
      document.getElementById("form1").submit();
  }
  
  $(document).ready(function(){
      $("button[name='statusImg']").click(function(event){
      $this = $(this);
      initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/mobileFloor/updatestatus/", $this,"${contextPath}");
      });
  });
  
  function search(){
  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
  	$("#form1")[0].submit();
}
</script>
