<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
<!--
.group-item {
	display: block;
	float: left;
	font-size: 14px;
	width: 160px;
}

.w-150px {
	width: 150px;
}

.text-right {
	text-align: right;
}
-->
.am-table > thead > tr > td, .am-table > tbody > tr > td, .am-table > tfoot > tr > td {
    height: 50px !important;
}
.checkbox-wrapper {
    font-size: 12px;
    vertical-align: middle;
    display: inline-block;
    position: relative;
    white-space: nowrap;
    margin-right: 0px !important;
    cursor: pointer;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<input type="hidden" value="${saveStatus}" id="saveStatus" />
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/member/role/query"/>">角色管理</a>
               				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">角色[${bean.name }]对应的权限列表</span>
						
					</th>
				</tr>
			</table>
			<%@ include file="/WEB-INF/pages/common/messages.jsp"%>


			<form:form
				action="${contextPath}/admin/member/role/saveFunctionsToRole"
				id="form1" method="post" onsubmit="return checkForm();">
				<input type="hidden" name="roleId" value="${bean.id}">
				<table class="${tableclass}" style="width: 100%">
					<thead>
						<tr>
							<th class="text-right" width="13%" style="text-align: right;">菜单</th>
							<th>权限</th>
						</tr>
					</thead>
				</table>
				<table class="${tableclass} no-border" style="width: 100%;margin-top: 20px;">
					<tbody>
						<c:if test="${not empty  authorizeSysFunctions}">
							<tr>
								<!-- <td width="13%" align="right">系统权限 <input type="checkbox"
									onclick="selectAll(this, 'sysFunction')"></td> -->
								<td width="13%" align="right"> 
									<span>
							           <label class='checkbox-wrapper'>
							           		<span style="color: #5c5c5c;">系统权限</span>
											<span class='checkbox-item'>
												<input type='checkbox' class='checkbox-input selectAll' onclick="selectAll(this, 'sysFunction')"/>
												<span class='checkbox-inner'></span>
											</span>
									   </label>	
									</span>
								</td>
								<td align="left" id="sysFunction" style="padding-left: 10px;"><c:forEach
										items="${authorizeSysFunctions}" var="authSysFunction">
										<div class="group-item">
											<%-- <input type="checkbox" value="${authSysFunction.id}"
												id='${authSysFunction.id}' name="funcId"> --%>
											<label class="checkbox-wrapper">
												<span class="checkbox-item">
													<input type="checkbox" value="${authSysFunction.id}" id='${authSysFunction.id}' name="funcId" 
													class="checkbox-input selectChild" onclick="selectChild(this);"/>
													<span class="checkbox-inner"></span>
												</span>
											<span id="index-index" class="priv" style="color: #5c5c5c;">${authSysFunction.note}</span>
										   </label>		
										</div>
									</c:forEach></td>
							</tr>
						</c:if>

						<c:forEach items="${resultFuncList}" var="resultFunc"
							varStatus="status">
							<c:forEach items="${resultFunc.funcWrapperList}"
								var="funcWrapper">
								<tr>
									<%-- <td width="13%" align="right">${funcWrapper.menuName}<input
										type="checkbox"
										onclick="selectAll(this, 'funcDiv${status.index}')"></td> --%>
									<td width="13%" align="right">
										<span>
								           <label class='checkbox-wrapper'>
								           		<span style="color: #5c5c5c;">${funcWrapper.menuName}</span>
												<span class='checkbox-item'>
													<input type='checkbox' class='checkbox-input funcDiv${status.index}' parent-index="${status.index}" onclick="selectAll(this, 'funcDiv${status.index}')"/>
													<span class='checkbox-inner'></span>
												</span>
										   </label>	
										</span>
									</td>
									<td align="left" id="funcDiv${status.index}" style="padding-left: 10px;">
									<c:forEach items="${funcWrapper.funcList}" var="func">
											<div class="group-item">
												<%-- <input type="checkbox" value='${func.id}' id='${func.id}'
													name="funcId"> <span id="index-index" class="priv">${func.note}</span> --%>
												<label class="checkbox-wrapper">
													<span class="checkbox-item">
														<input type="checkbox" value='${func.id}' id='${func.id}' name="funcId" child-index="${status.index}"
														class="checkbox-input funcDiv${status.index}child" onclick="selectFunDiv(this,'funcDiv${status.index}')"/>
														<span class="checkbox-inner"></span>
													</span>
												<span id="index-index" class="priv">${func.note}</span>
											   </label>	
											</div>
									</c:forEach>
							</c:forEach>
							</td>
							</tr>
						</c:forEach>

						<tr>
							<!-- <th class="text-right">全选 <input
								onclick='selectAll(this, "")' type="checkbox"></th> -->
							<th class="text-right">全选 
								<span>
						           <label class='checkbox-wrapper'>
										<span class='checkbox-item'>
											<input type='checkbox'  class='checkbox-input' onclick='selectAll(this, "")'/>
											<span class='checkbox-inner'></span>
										</span>
								   </label>	
								</span>
							</th>
							<td colspan="2" style="padding-left: 20px;"><input class="${btnclass}" id="submit"
								type="submit" value="保存"></input> <input class="${btnclass} "
								onclick="window.location='${contextPath}/admin/member/role/query'"
								type="button" value="返回"></input></td>
						</tr>
					</tbody>
				</table>
			</form:form>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
	$(document).ready(function() {
		//将该角色已经拥有的资源选上
		<c:forEach items="${list}" var="selecFunc">
			$("#${selecFunc.id}").attr("checked",true);
			$("#${selecFunc.id}").parent().parent().addClass("checkbox-wrapper-checked");
			/* var statusIndex = $("#${selecFunc.id}").attr('child-index');
			var flag = true;
			$("input[child-index='"+statusIndex+"']").each(function(){
				if(!this.checked){
					flag = false;
					break;
				}
			})
			if(flag){
				console.log($("input[parent-index='"+statusIndex+"']"));
				$("input[parent-index='"+statusIndex+"']").parent().parent().addClass("checkbox-wrapper-checked");
			} */
		</c:forEach>
		
		var saveStatus = $("#saveStatus").val();
		if(saveStatus == 1) {
			layer.msg("保存成功!",{icon:0});
		}
	});
	
	/* function selectAll(checker, scope){
    if(scope){
         $('#' + scope + ' input').each(function() 
         {
         	$(this).prop("checked", checker.checked);
         });
    }else{
         $('input:checkbox').each(function() 
         {
             $(this).prop("checked", checker.checked)
         });
    }
} */
	function selectAll(checker, scope){
		if(checker.checked){
	 		$(checker).parent().parent().addClass("checkbox-wrapper-checked");
	 	}else{
	 		$(checker).parent().parent().removeClass("checkbox-wrapper-checked");
	 	}
	    if(scope){
	         $('#' + scope + ' input').each(function() 
	         {
	         	$(this).prop("checked", checker.checked);
	         	if(checker.checked){
	         		$(this).parent().parent().addClass("checkbox-wrapper-checked");
	         	}else{
	         		$(this).parent().parent().removeClass("checkbox-wrapper-checked");
	         	}
	         });
	    }else{
	         $('input:checkbox').each(function() 
	         {
	             $(this).prop("checked", checker.checked)
	             if(checker.checked){
	         		$(this).parent().parent().addClass("checkbox-wrapper-checked");
	         	}else{
	         		$(this).parent().parent().removeClass("checkbox-wrapper-checked");
	         	}
	         });
	    }
	}
	
	function checkForm(){
		var funLen = $('input[name=funcId]:checked').length
		if(funLen==0){
			layer.msg("至少选择一个权限！",{icon:0});
			return false;
		}
		return true;
	}
	
	function selectChild(obj){
	 		if(!obj.checked){
	 			$(".selectAll").checked = obj.checked;
	 			$(obj).prop("checked",false);
	 			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	 		}else{
	 			$(obj).prop("checked",true);
	 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	 		}
	 		 var flag = true;
	 		  var arr = $(".selectChild");
	 		  for(var i=0;i<arr.length;i++){
	 		     if(!arr[i].checked){
	 		    	 flag=false; 
	 		    	 break;
	 		    	 }
	 		  }
	 		 if(flag){
	 			 $(".selectAll").prop("checked",true);
	 			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 		 }else{
	 			 $(".selectAll").prop("checked",false);
	 			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 		 }
 	}
	
	function selectFunDiv(obj,index){
		if(!obj.checked){
 			$("."+index).checked = obj.checked;
 			$(obj).prop("checked",false);
 			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
 		}else{
 			$(obj).prop("checked",true);
 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
 		}
 		 var flag = true;
 		  var arr = $("."+index+"child");
 		  for(var i=0;i<arr.length;i++){
 		     if(!arr[i].checked){
 		    	 flag=false; 
 		    	 break;
 		    	 }
 		  }
 		 if(flag){
 			 $("."+index).prop("checked",true);
 			 $("."+index).parent().parent().addClass("checkbox-wrapper-checked");
 		 }else{
 			 $("."+index).prop("checked",false);
 			 $("."+index).parent().parent().removeClass("checkbox-wrapper-checked");
 		 }
	}
</script>
</html>