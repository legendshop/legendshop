<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/mergeGroup/fight.css'/>" rel="stylesheet" />
</head>
<body>
   
   <jsp:include page="/admin/top" />
   
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div style="border-bottom:1px solid #ddd;width: 100%;height:40px;line-height:40px;">
		      		<span class="title-span" style="margin-left:10px;">
						秒杀管理  ＞
						<a href="<ls:url address="/admin/mergeGroup/query"/>">秒杀活动</a>
						  ＞   <span style="color:#0e90d2;">秒杀运营</span>
					</span>
		      	</div>
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
					<div class="fight-operate"
						style="margin-left: 50px; float: left; width: 93%;">
						<!-- 运营统计数据 -->

						<table cellpadding="0" cellspacing="0">
							<tr class="tab-tit">
								<th width="60">编号</th>
								<th width="200">订单号</th>
								<th>买家</th>
								<th>数量</th>
								<th width="100">交易金额(元)</th>
								<th>秒杀时间</th>
								<th>秒杀状态</th>
							</tr>
							<c:if test="${empty list}">
								<tr class="first-leve">
									<td colspan='7' height="60" style="text-align: center; color: #999;">暂无数据</td>
								</tr>
							</c:if>

							<c:forEach items="${list}" var="item" varStatus="itemStatus">
								<tr>
									<td>${itemStatus.index+1}</td>
									<td>${item.subNumber}</td>
									<td>${item.userName}</td>
									<td>${item.productNums}</td>
									<td>${item.actualTotal}</td>
									<td>
										<fmt:formatDate value="${item.payDate}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
									</td>
									<td>
										<c:choose>
											<c:when test="${item.status eq 1}">
												<span class="sta-wate">支付中</span>
											</c:when>
											<c:when test="${item.status eq 2}">
												<span class="sta">待发货</span>
											</c:when>
											<c:when test="${item.status eq 3}">
												<span class="sta-fail">发货中</span>
											</c:when>
											<c:when test="${item.status eq 4}">
												<span class="sta-fail">交易成功</span>
											</c:when>
											<c:otherwise><span class="sta-fail">交易失败</span></c:otherwise>
										</c:choose>
									</td>

								</tr>
							</c:forEach>
						</table>
						<div align="center"><br/>
							<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default" />
						</div>
						<div class="activity-mes" style="margin-top: 35px;">
							<span class="act-tit"></span> <a
								href="${contextPath }/admin/seckillActivity/query" class="lim-btn on"
								style="background: #fff; color: #666; border: 1px solid #e4e4e4;">返回</a>
						</div>
					</div>			    
		     </div>
	 </div>
</body>
<script language="JavaScript" type="text/javascript">
	var curPageNO = "${curPageNO}";
	var contextPath = "${contextPath}";
	var seckillId = "${seckillId}"
	function pager(curPageNO) {
		location.href = contextPath + "/admin/seckillActivity/operatorSeckillActivity/"+seckillId+"?curPageNO="
				+ curPageNO;
	}
</script>
</html>
