<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">菜单管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/system/menu/queryChildMenu/${parentId}"
				id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<input type="hidden" id="parentId" name="parentId" value="${parentId}" /> 
					<span class="item">
						名称：<input class="${inputclass}" type="text" name="name" maxlength="50" value="${menu.name}" placeholder="请输入名称"/> 
						<input class="${btnclass}" onclick="search()" type="submit" value="搜索" />
					<c:if test="${allParentMenu !=null }">
						<a href="<ls:url address='/system/menu/query'/>">菜单列表</a>&nbsp;＞&nbsp; 
				    	<c:forEach items="${allParentMenu }" var="menu">
							<a href="<ls:url address='/system/menu/queryChildMenu/${menu.key}'/>">${menu.value }</a>&nbsp;＞
				    	</c:forEach>
					</c:if>
						<input class="${btnclass}" type="button" value="创建${menuLevel}级菜单"
							onclick='window.location="<ls:url address='/system/menu/create/${parentId}'/>"' />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/system/menu/query" id="item"
					export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="名称" property="name"></display:column>
					<display:column title="标题" property="title"></display:column>
					<display:column title="动作" property="action"></display:column>
					<display:column title="插件" property="providedPlugin"></display:column>
					<display:column title="顺序" property="seq" sortable="true"
						sortName="seq"></display:column>
					<display:column title="操作" media="html" style="width:220px">
						<div class="table-btn-group">
							<c:if test="${parentMenu == null || (parentMenu != null && parentMenu.grade < 2)}">
								<button class="tab-btn" onclick="window.location='${contextPath}/system/menu/queryChildMenu/${item.menuId }'">
									子菜单
								</button>
								<span class="btn-line">|</span>
							</c:if>
							<button class="tab-btn" onclick="window.location='${contextPath}/system/menu/load/${item.menuId}'">
								修改
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.menuId}')">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}

	function deleteById(id) {
		layer.confirm("确定删除？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/system/menu/delete/" + id + "'/>";
		   });
	}

	highlightTableRows("item");
</script>
</html>
