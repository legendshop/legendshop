<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>重建索引 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
td {
	text-align: left;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">系统配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">重建索引</span></th>
					</tr>
			</table>
		<form:form action="${contextPath}/admin/system/index/rebuildIndex"
			method="post" id="form1" onSubmit="return validateReindexForm(this);">
			<input id="id" name="id" value="${bean.id}" type="hidden">
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<table align="center" class="${tableclass} no-border content-table" id="col1">
					<tr>
						<td width="200px;">
							<div align="right">
								<font color="ff0000">*</font>依实体类型：
							</div>
						</td>
						<td align="left" style="padding-left: 2px;"><!-- <input type="radio" id="prodType" name="entityType"
							value="product" checked="checked" />商品 --> <!-- <input type="radio" id="prodType" name="entityType" value="1">用户
					    <input type="radio" id="prodType" name="entityType" value="2">文章 -->
						    <label class="radio-wrapper radio-wrapper-checked">
								<span class="radio-item">
									<input type="radio" class="radio-input" id="prodType" name="entityType" value="product" checked="checked">
									<span class="radio-inner"></span>
								</span>
								<span class="radio-txt">商品</span>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">
								<!-- <input type="radio" id="reindexByDate" value="1" name="type"
									checked="checked"> -->
								<label class="radio-wrapper radio-wrapper-checked" style="margin-right:0px;font-weight: 400">
									<span class="radio-item">
										<input type="radio" class="radio-input" id="reindexByDate" value="1" name="type" checked="checked">
										<span class="radio-inner"></span>
									</span>
								<font color="ff0000">*</font>依日期：
								</label>
							</div>
						</td>
						<td align="left" style="padding-left: 2px;">
						开始时间 <input readonly="readonly" class="${inputclass}" name="fromDate" id="fromDate"/>
						&nbsp; 结束时间
						<input readonly="readonly" class="${inputclass}"  name="toDate" id="toDate" />
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">
								<!-- <input type="radio" id="reindexByMessage" value="2" name="type"><font color="ff0000">*</font>依实体Id： -->
								<label class="radio-wrapper" style="margin-right:0px;font-weight: 400">
									<span class="radio-item">
										<input type="radio" class="radio-input" id="reindexByMessage" value="2" name="type">
										<span class="radio-inner"></span>
									</span>
								<font color="ff0000">*</font>依实体Id：
								</label>
							</div>
						</td>
						<td align="left" style="padding-left: 2px;">开&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;始 
							<input type="text" class="${inputclass}" name="firstPostId" onblur="chooseByPostId(this);" /> 
							&nbsp;&nbsp;结&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;束 
							<input type="text" class="${inputclass}" name="lastPostId" onBlur="chooseByPostId(this);" />
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">
								<!-- <input type="radio" id="reindexAll" value="3" name="type"><font color="ff0000">*</font>全部重建： -->
								<label class="radio-wrapper" style="margin-right:0px;font-weight: 400">
									<span class="radio-item">
										<input type="radio" class="radio-input" id="reindexAll" value="3" name="type">
										<span class="radio-inner"></span>
									</span>
								<font color="ff0000">*</font>全部重建：
								</label>
							</div>
						</td>
						<td align="left" style="padding-left: 2px;"><span style="color: #d30050">全部重建将会删除所有存在的纪录,重新建立索引文件</span>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">选项：</div>
						</td>
						<td align="left" style="padding-left: 2px;">
						<!-- <input type="radio" name="indexOperationType" id="operationTypeAppend" checked="checked" value="append" />  -->
						<label class="radio-wrapper radio-wrapper-checked" style="margin-right:0px;font-weight: 400">
							<span class="radio-item">
								<input type="radio" class="radio-input" name="indexOperationType" id="operationTypeAppend" checked="checked" value="append">
								<span class="radio-inner"></span>
							</span>
						加入文件到索引,将纪录加到现存的索引数据库而不重新建立.
						</label>
						<!-- <span>加入文件到索引,将纪录加到现存的索引数据库而不重新建立.</span> --><br>
						<label class="radio-wrapper" style="margin-right:0px;font-weight: 400">
							<span class="radio-item">
								<input type="radio" class="radio-input" name="indexOperationType" id="operationTypeRecreate" value="recreate">
								<span class="radio-inner"></span>
							</span>
						<span style="color: #d30050">从无到有重建索引,建立一个全新的索引数据库.这样将会删除所有存在的纪录,如果要选择这项,建议选择：全部重建按钮.</span>
						</label>
						<!-- <input type="radio" name="indexOperationType" id="operationTypeRecreate" value="recreate" />  -->
						<!-- <span style="color: #d30050">从无到有重建索引,建立一个全新的索引数据库.这样将会删除所有存在的纪录,如果要选择这项,建议选择：全部重建按钮.</span> --></td>
					</tr>
					<tr>
						<td></td>
						<td align="left" style="padding-left: 2px;">
							<div>
								<input type="submit" class="${btnclass}" value="保存" /> 
							</div>
						</td>
					</tr>
				</table>
			</div>
		</form:form>
	</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script
	src="<ls:templateResource item='/resources/common/js/alternative.js'/>"
	type="text/javascript"></script>

<script type="text/javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		highlightTableRows("col1");
		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
		
		 laydate.render({
			   elem: '#fromDate',
			   calendar: true,
			   theme: 'grid',
		 	   trigger: 'click'
		  });
      
     	 laydate.render({
			   elem: '#toDate',
			   calendar: true,
			   theme: 'grid',
		 	   trigger: 'click'
		  });
	});

	function chooseByPostId(field) {
		var value = field.value;
		if (value.length > 0) {
			if (isNaN(value * 1)) {
				alert("输入ID范围有误");
				field.value = "";
			}
			/*
			else {
				var f = field.form;

				f.fromDay.selectedIndex = 0;
				f.fromMonth.selectedIndex = 0;
				f.fromYear.selectedIndex = 0;
				f.fromHour.value = "";
				f.fromMinutes.value = "";

				f.toDay.selectedIndex = 0;
				f.toMonth.selectedIndex = 0;
				f.toYear.selectedIndex = 0;
				f.toHour.value = "";
				f.toMinutes.value = "";

				f.type[1].checked = true;
			}
			 */
		}
	}

	function writeOptions(fieldName, from, to) {
		document.write("<select name='" + fieldName
				+ "' onChange='chooseByDate(this.form);'>");
		document.write("<option value=''>--</option>");

		for (var i = from; i <= to; i++) {
			document.write("<option value='" + i + "'>" + i + "</option>");
		}

		document.write("</select>&nbsp;&nbsp;");
	}

	function chooseByDate(f) {
		f.firstPostId.value = "";
		f.lastPostId.value = "";
		f.type[0].checked = true;
	}

	function validateReindexForm(f) {
		if (f.type[0].checked) {
			return validateDateRange(f);
		} else if (f.type[1].checked) {
			return validatePostIdRange(f);
		}
	}

	function validatePostIdRange(f) {
		if (f.firstPostId.value.length == 0 || isNaN(f.firstPostId.value * 1)
				|| f.lastPostId.value.length == 0
				|| isNaN(f.lastPostId.value * 1)) {
			alert("输入ID范围有误");
			return false;
		}

		return true;
	}

	function validateDateRange(f) {
		/*
		var fromHour = f.fromHour.value * 1;
		var fromMinutes = f.fromMinutes.value * 1
		var toHour = f.toHour.value * 1;
		var toMinutes = f.toMinutes.value * 1

		if (f.fromDay.selectedIndex == 0 
			|| f.fromMonth.selectedIndex == 0
			|| f.fromYear.selectedIndex == 0
			|| f.fromHour.value.length == 0
			|| f.fromMinutes.value.length == 0
			|| isNaN(fromHour)
			|| isNaN(fromMinutes)
			|| fromHour < 0 || fromHour > 59
			|| fromMinutes < 0 || fromMinutes > 59
			|| f.toDay.selectedIndex == 0 
			|| f.toMonth.selectedIndex == 0
			|| f.toYear.selectedIndex == 0
			|| f.toHour.value.length == 0
			|| f.toMinutes.value.length == 0
			|| isNaN(toHour)
			|| isNaN(toMinutes)
			|| toHour < 0 || toHour > 59
			|| toMinutes < 0 || toMinutes > 59) {
			alert("输入时间范围有误");
			return false;
		}
		 */
		var fromDate = $("#fromDate").val();
		var toDate = $("#toDate").val();
		if (fromDate == null || fromDate == "") {
			alert("请选择开始时间");
			return false;
		} else if (toDate == null || toDate == "") {
			alert("请选择结束时间");
			return false;
		}
		return true;
	}

	function writeTimeGroup(fieldNamePrefix) {
		document
				.write("<input type='text' style='width: 35px;' maxlength='2' name='" + fieldNamePrefix + "Hour'> : ");
		document
				.write("<input type='text' style='width: 35px;' maxlength='2' name='" + fieldNamePrefix + "Minutes'>");
	}
	$("input:radio[name='type']").change(function(){
		 $("input:radio[name='type']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
    $("input:radio[name='indexOperationType']").change(function(){
		 $("input:radio[name='indexOperationType']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
</script>
</html>
