<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

	<%
		Integer offset = (Integer) request.getAttribute("offset");
	%>
	
	<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>支付管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style>
  .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<form:form  action="${contextPath}/admin/paytype/query" id="form1" method="post">
		<table class="${tableclass} title-border" style="width: 100%">
				<tr><th class="title-border">业务设置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">支付设置</span></th></tr>
		</table>
	</form:form>
	<div align="center" class="order-content-list" style="margin-top: 15px;">
		<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/paytype/query" id="item"
			export="false" class="${tableclass}" style="width:100%" sort="external">
			<display:column title="顺序" class="orderwidth" style="width:50px"><%=offset++%></display:column>
			<display:column title="支付方式名称" property="payTypeName"></display:column>
			<display:column title="支付方式描述"><div class="order_img_name">${item.memo}</div></display:column>
			<display:column title="启用" style="width:50px">
				<c:choose>
					<c:when test="${item.isEnable==0}">
                                 否
          </c:when>
					<c:otherwise>
                         是
          </c:otherwise>
				</c:choose>
			</display:column>
				<display:column title="操作" media="html" style="width:150px">
		          	<div class="table-btn-group">
		                    <button class="tab-btn" onclick="editGateway('${item.payTypeId}');">编辑</button>
		                	<%-- <button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}');" ><span class="am-icon-trash-o"></span> 删除</button> --%>
		           </div>
				</display:column>
		</display:table>
	</div>
	</div>
	</div>
	</body>
     <script language="JavaScript" type="text/javascript">
		var contextPath='${contextPath}';
		function editGateway(payTypeId){
				if(payTypeId==null){
					layer.alert("参数错误",{icon: 2});
				}
				var url = contextPath+"/admin/paytype/paySiteSetting/"+payTypeId;
				layer.open({
					title :"编辑支付方式",
					  type: 2, 
					  content: url, 
					  area: ['700px','auto'], 
					  success: function(layero, index) {
		 				  layer.iframeAuto(index);
		 				} 
					});  
			}
</script>
</html>
  