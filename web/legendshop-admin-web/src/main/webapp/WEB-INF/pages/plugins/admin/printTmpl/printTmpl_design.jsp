<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/dialog.css'/>" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<style>
.title {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #BEC6CE;
	height: 65px;
	padding: 0 3px;
	float: left;
}
.input {
	font-family: "微软雅黑";
	font-size: 12px;
	padding-left: 10px;
	padding-right: 10px;
	margin: 0 auto;
}
.Popup_shade {
    background: #000 none repeat scroll 0 0;
    display: block;
    height: 100%;
    left: 0;
    min-width: 1200px;
    opacity: 0.3;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 105;
}

.submitBtn{
	cursor: pointer;
}
</style>
	      	 <c:choose>
	      		<c:when test="${isUpdate}"><!-- 非系统默认，图片放在图片服务器上 -->
      			 		<c:set var="bgimage" value="${printTmpl.bgimage}"/> 
      			</c:when>
	      		<c:otherwise>
      				 	<c:set var="bgimage" value="${contextPath}${printTmpl.bgimage}"/> 
      			</c:otherwise>
	      	</c:choose> 

<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/flash/swfobject.js'/>"></script>
<script type="text/javascript">
	function isHaveFalsh(){
	
		var flash = null;
	  if(typeof window.ActiveXObject != "undefined"){
	    flash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
	  }else{
	    flash = navigator.plugins['Shockwave Flash'];
	  }
	  
	  return Boolean(flash);
	}
   
    $.validator.setDefaults({});
    var xmldata = "${printTmpl.prtTmplData}";
	var swfVersionStr = "10.0.0";
    var flashvars = {};
    flashvars.data = encodeURIComponent(xmldata);
	flashvars.bg = encodeURIComponent("${bgimage}");
	swfobject : undefined;
    var params = {};
    params.quality = "high";
    params.allowScriptAccess = "always";
    params.swLiveConnect = "true";
    params.wMode = "opaque";
    var attributes = {};
    attributes.id = "orderDesign";
    attributes.name = "orderDesign";
    swfobject.embedSWF(
        "<ls:templateResource item='/resources/common/js/flash/OrderDesign.swf'/>","swf",
        "100%", "100%", 
        swfVersionStr, {}, 
        flashvars, params, attributes);

  function getFlashObj(movieName) {
	if (navigator.appName.indexOf("Microsoft") != -1) {
		return window[movieName];
	} else {
		return document[movieName];
	}
  }

  function createItem(obj){
	if(obj.value != "--"){
		var spv = obj.value.split("|");
		getFlashObj("orderDesign").createItem(spv[0],spv[1]);
	}
  }

  function delItem(){
	 getFlashObj("orderDesign").delItem();
  }

  function uploadOk(data){
		if(data.result==1){
			$("#bgimage").val(data.path);
			getFlashObj("orderDesign").setBG(data.path);
		}else{
			alert(data.message);
		}
	}

  function setBG(){
	  uploadShow();
  }
  
  function uploadShow(){
     $(".Popup_shade").css('display','block'); 
	 $("#dlg_upload").css('display','block'); 
  }
  
  function uploadClose(){
      $(".Popup_shade").css('display','none'); 
	  $("#dlg_upload").css('display','none'); 
	  $("#imgViewUrl").val("http://");
	  $("#imagesfile").val("");
  }
  
  

  function lockbg(obj){
		getFlashObj("orderDesign").lockBg(obj.checked);
  }

  function setFontSize(obj){
	if(obj.value != "--"){
		getFlashObj("orderDesign").setFontSize(obj.value);
	}
  }

  function setFont(obj){
	if(obj.value != "--"){
		getFlashObj("orderDesign").setFontType(obj.value);
	}
  }

  function setFontSpace(obj){
	if(obj.value != "--"){
		getFlashObj("orderDesign").setFontSpace(obj.value);
	}
  }

  function setFontBold(obj){
	getFlashObj("orderDesign").setFontBold();
  }

  function setFontItalic(obj){
	getFlashObj("orderDesign").setFontItalic();
  }

  function setTextAlign(value){
	getFlashObj("orderDesign").setTextAlign(value);
  }

  function resizeDesign(type,obj){
	if(type == "width"){
		document.getElementById("flashwrap").style.width = obj.value ;
		$("#flashwrap").find(".design").css("width",obj.value);
	}else if(type == "height"){
		document.getElementById("flashwrap").style.height = obj.value;
		$("#flashwrap").find(".design").css("height",obj.value);
		$("#flashwrap").css("min-height",obj.value);
	}
  }

  function saveDesign(){
	    $("#xmldata").val(decodeURIComponent(getFlashObj("orderDesign").exportData()));
  }
  
 function uploadFile() {
	$.ajaxFileUpload({
				url : "<ls:url address='/admin/printTmpl/uploadFile'/>",
				secureuri : false,
				fileElementId : 'imagesfile',
				dataType : 'json',
				error : function(data, status, e) {
					alert(data);
				},
				success : function(data, status) {
					if (data == "fail") {
						alert(data);
					} else {
						var url = "<ls:photo item='"+data+"'/>" ;
						$("#bgimage").val(data);
						getFlashObj("orderDesign").setBG(url);
		                uploadClose();	
					}
		}
   });
}

  $(document).ready(function() {
  		if(!isHaveFalsh()){
	 		if(confirm("检测到您没有安装flash插件,导致不能正常设计打印模板, 是否前往下载安装?")){
	 			window.open("https://get2.adobe.com/cn/flashplayer","_blank");
	 		}
	 	}
		
		$("#imgFromLocal").click(function(){
		if(this.checked){
			$("#imgViewLocal").show();
			$("#imgViewNet").hide();
		}
	   });

	  $("#imgFromNet").click(function(){
		 if(this.checked){
			$("#imgViewLocal").hide();
			$("#imgViewNet").show();
		 }
	   });
		
	$("#okBtn").click(function(){
		 var imagesfile=$("#imagesfile").val();
		 if(imagesfile=="" || imagesfile==null){
		   alert("请上传图片");
		   return false;
		 }
		 uploadFile();
	});
	
		$("#closeBtn").click(function(){
				uploadClose();	
	});
	
	 jQuery("#theForm").validate({
            rules: {
               prtTmplTitle: {
                 required : true,
                 maxlength : 10
               },
               prtTmplWidth: {
                required : true,
                digits: true 
                
               },
               prtTmplHeight: {
                required : true,
                digits: true 
               }
           },
          messages: {
             prtTmplTitle: {
                required : "模板名称不能为空",
                maxlength : "模板名称最多10个字" 
            },
            prtTmplWidth: {
                required : "宽度不能为空",
                digits: "宽度必须为数字"
            },
            prtTmplHeight: {
                required : "高度不能为空",
                digits: "高度必须为数字"
            }
        },
        submitHandler : function(form) {
            saveDesign();
            $("#okBtn").attr("disabled",true);
			$(".editAdd_load").show();
            form.submit();
        }
     });
     
	
  });
  
  
  </script>
  
   <body>
	<div align="center">
		<table border="0" align="center" class="${tableclass}" id="col1">
			<thead>
				<tr class="sortable">
					<th colspan="2">
						<div align="center">设置运单模版</div></th>
				</tr>
		</table>
	</div>

	<div class="input">
	   <form:form  enctype="multipart/form-data" action="${contextPath}/admin/printTmpl/savePrintDesign" cssClass="validate" name="theForm" id="theForm">
		<div
			style="height:67px;width:1025px;border:1px solid #ff0000;margin: 0 auto;">
			<div class="title">
				单据名称<br /> 
				<input type="hidden" name="prtTmplId" value="${printTmpl.prtTmplId }" /> 
					<input type="text"
					name="prtTmplTitle" maxlength="10" value="${printTmpl.prtTmplTitle }" /><br />
				<input type="radio" name="prtDisabled" value="1" <c:if test="${printTmpl.prtDisabled == 1 }">checked</c:if> />启用&nbsp;
				<input type="radio" name="prtDisabled" value="0" <c:if test="${printTmpl.prtDisabled == 0 }">checked</c:if> />停用
			</div>
			<div class="title">
				单据尺寸<br />*宽<input type="text" style="width:45px;"
					name="prtTmplWidth" maxlength="3" value="${printTmpl.prtTmplWidth }"
					onchange="resizeDesign('width',this)" />*高 <input type="text"
					style="width:45px;" maxlength="3" name="prtTmplHeight"
					value="${printTmpl.prtTmplHeight }"
					onchange="resizeDesign('height',this)" />mm
			</div>
			<div class="title">
				单据背景<br /> 
				<input type="hidden"   name="bgimage" id="bgimage" /> 
				<input type="checkbox" id="lockbgel" checked onchange="lockbg(this)" />锁定
			</div>
			<div class="title">
				单据打印项<br /> <select onchange="createItem(this);">
					<option value="收货人-姓名|ship_name">收货人-姓名</option>
					<!-- <option value="收货人-地区|ship_area">收货人-地区</option> -->
					<option value="收货人-地址|ship_addr">收货人-地址</option>
					<option value="收货人-电话|ship_tel">收货人-电话</option>
					<option value="收货人-手机|ship_mobile">收货人-手机</option>
					<option value="收货人-邮编|ship_zip">收货人-邮编</option>
					<option value="发货人-姓名|dly_name">发货人-姓名</option>
					<!-- <option value="发货人-地区|dly_area">发货人-地区</option> -->
					<option value="发货人-地址|dly_address">发货人-地址</option>
					<option value="发货人-电话|dly_tel">发货人-电话</option>
					<option value="发货人-手机|dly_mobile">发货人-手机</option>
					<option value="发货人-邮编|dly_zip">发货人-邮编</option>
					<option value="当日日期-年|date_y">当日日期-年</option>
					<option value="当日日期-月|date_m">当日日期-月</option>
					<option value="当日日期-日|date_d">当日日期-日</option>
					<option value="订单-订单号|order_number">订单-订单号</option>
					<option value="订单商品名称|order_name">订单商品名称</option>
					<option value="订单总金额|order_price">订单总金额</option>
					<option value="订单运费金额|ship_price">订单运费金额</option>
					<option value="订单物品总重量|order_weight">订单物品总重量</option>
					<option value="订单物品总体积|order_volume">订单物品总体积</option>
					<option value="订单-物品数量|order_count">订单-物品数量</option>
					<option value="订单-备注|order_memo">订单-备注</option>
					<option value="订单-送货时间|ship_time">订单-送货时间</option>
					<option value="店铺名称|shop_name">店铺名称</option>
					<option value="tick">对号 - √</option>
				</select> <input type="button" onclick="delItem();" value="删除" />
			</div>
			<div class="title">
				<select onchange="setFontSize(this);">
					<option value="--">大小</option>
					<option value="10">10</option>
					<option value="12">12</option>
					<option value="14">14</option>
					<option value="18">18</option>

					<option value="20">20</option>
					<option value="24">24</option>
					<option value="27">27</option>
					<option value="30">30</option>
					<option value="36">36</option>
				</select> <select onchange="setFont(this);">
					<option value="--">字体</option>
					<option value="宋体">宋体</option>
					<option value="黑体">黑体</option>
					<option value="Arial">Arial</option>
					<option value="Verdana">Verdana</option>
					<option value="Serif">Serif</option>
					<option value="Cursive">Cursive</option>
					<option value="Fantasy">Fantasy</option>
					<option value="Sans-Serif">Sans-Serif</option>
				</select> <br /> <select name="jianju" id="jianju" style="height: 20px;"
					onchange="setFontSpace(this);">
					<option value="--" selected="selected">间距</option>
					<option value="-4">-4</option>
					<option value="-2">-2</option>
					<option value="0">0</option>
					<option value="2">2</option>

					<option value="4">4</option>
					<option value="6">6</option>
					<option value="8">8</option>
					<option value="10">10</option>
					<option value="12">12</option>
					<option value="14">14</option>

					<option value="16">16</option>
					<option value="18">18</option>
					<option value="20">20</option>
					<option value="22">22</option>
					<option value="24">24</option>
					<option value="26">26</option>

					<option value="28">28</option>
					<option value="30">30</option>
				</select> <input type="button" onclick="setFontBold();" value="&nbsp;B&nbsp;" />
				<input type="button" onclick="setFontItalic();"
					value="&nbsp;I&nbsp;" />
			</div>
			<div class="title">
				<input type="button" onclick="setTextAlign('left');" value="左对齐" />
				<input type="button" onclick="setTextAlign('center');" value="居中对齐" />
				<input type="button" onclick="setTextAlign('right');" value="右对齐" />
				<input type="button" value="设置背景" onclick="setBG()" />

			</div>
		</div>
		<input type="hidden" name="prtTmplData" id="xmldata" />
		<div id="flashwrap" style="min-height:590px;width:1025px;border:1px solid #000;margin: 0 auto;">
			 <div style="height: ${printTmpl.prtTmplHeight};width: ${printTmpl.prtTmplWidth}px;margin: 0 auto;" class="design">
			    <div id="swf"></div>
			 </div>
		</div>
		<div class="submitlist" align="center">
			<table>
				<tr>
					<td>
						<input name="submit" style="height: 50px;width: 100px;text-align: center;" type="submit" value=" 保存   " class="submitBtn" />
						<input name="back" style="height: 50px;width: 100px;text-align: center;" type="button" value=" 返回  " class="submitBtn"  onclick="window.location='${contextPath}/admin/printTmpl/query'"/>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
</div>


<div class="Popup_shade" style="display: none;"></div>
<div style="width: 500px; z-index: 3000; display: none;"  class="dialog"
	id="dlg_upload">
	<div class="dialog_box">
		<div class="head">
			<!-- <div class="title">上传模板背景图片</div> -->
			<span class="closeBtn"></span>
		</div>
		<div class="body dialogContent">
			<div id="upload">
				<div style="" class="headContent">
					<div class="tableform mainHead">
						<h4>选择上传图片的方式</h4>
						<div id="imgFrom">
							<input type="radio" name="from" id="imgFromLocal" checked="">
							<label for="imgFromLocal">上传图片</label>&nbsp;&nbsp;&nbsp;&nbsp;
							 <!-- <input type="radio" name="from" id="imgFromNet"> <label
								for="imgFromNet">网络图片地址</label>&nbsp;&nbsp;&nbsp;&nbsp; -->
						</div>

					</div>
				</div>
				<div>
					   	<div id="imgViewLocal" class="tableform" style="display: block;">
							<h4>从您的电脑中挑选一张图片：</h4>
							<input type="file" id="imagesfile"  name="imagesfile" >
						</div>
					<div style="display: none;" id="imgViewNet" class="tableform">

						<h4>输入一张网络图片的网址：</h4>
						<input type="text" style="width: 80%;" id="imgViewUrl"
							value="http://">
						<div id="imgViewUrlPreivew" class="note">
							复制网络上的一张图片路径到上面的输入框<br>
							.例如:"http://www.example.com/images/pic.jpg"
						</div>
					</div>



					<div style="" class="footContent">
						<div align="center" class="submitlist">
							<table>
								<tbody>
									<tr>
										<td>
												<input type="button" class="submitBtn" value=" 确    定   " id="okBtn" name="btn">
												<input type="button" class="submitBtn" value=" 关 闭   " id="closeBtn" name="closeBtn" style="margin-left: 6px;">
									   </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/>
 <style type="text/css">
    .editAdd_load{position: absolute;top:50%;left:50%;margin:-16px 0 0 -16px;z-index: 99;display: none;}
</style>
