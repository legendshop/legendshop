<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<body>
<%Integer offset = (Integer) request.getAttribute("offset");%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style>
  .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<table class="${tableclass} title-border" style="width: 100%">
	    <tr>
	        <th class="title-border">帮助中心&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">反馈意见</span></th>
	    </tr>
	</table>

	<div >
	    <div class="seller_list_title">
	        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
	            <li <c:if test="${empty userFeedback.status}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/userFeedBack/query"/>">所有资讯</a></li>
	            <li <c:if test="${userFeedback.status== 0}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/userFeedBack/query?status=0"/>">未读资讯</a></li>
	            <li <c:if test="${userFeedback.status==1}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/userFeedBack/query?status=1"/>">已读资讯</a></li>
	        </ul>
	    </div>
	</div>

	<form:form action="${contextPath}/admin/userFeedBack/query" id="form1" method="get">
	<input type="hidden" name="status" value="${userFeedback.status}">
          <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
          <div class="criteria-div">
	          <span class="item">
	              	反馈内容：<input type="text" name="content" placeholder="反馈内容" value="${userFeedback.content}"/>
	              <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
	          </span>
          </div>
	</form:form>

<div align="center" class="order-content-list">
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/userFeedBack/query"
                   id="item" export="false" class="${tableclass}" style="width:100%">
        <display:column title="顺序" class="orderwidth"><%=offset++%>
        </display:column>
        <display:column title="用户名">
            <c:choose>
                <c:when test="${empty item.name }">
                    匿名
                </c:when>
                <c:otherwise>${item.name}</c:otherwise>
            </c:choose>
        </display:column>
        <display:column title="反馈内容" style="max-width: 200px;">
        	<div class="order_img_name">
                <c:choose>
                    <c:when test="${fn:length(item.content) >= 45}">
                        ${fn:substring(item.content,0,45)}
                        <a href="javaScript:void(0)" onclick="showContent('${item.id}')">......</a>
                        <input type="hidden" value="${item.content}" id="${item.id}_content">
                    </c:when>
                    <c:otherwise>
                        <input type="text" style="border:0px" value="${item.content}">
                    </c:otherwise>
                </c:choose>
            </div>
        </display:column>
        <display:column title="反馈时间" property="recDate" format="{0,date,yyyy-MM-dd HH:mm:ss}" style="width:100px;"></display:column>
        <display:column title="ip地址">
            <font id="ipAddress">${item.ip}</font>
        </display:column>
        <display:column title="联系方式" property="contactInformation"></display:column>
        <display:column title="反馈来源" style="width:100px;">
            <c:choose>
                <c:when test="${item.feedBackSource == 1}">PC端</c:when>
                <c:when test="${item.feedBackSource == 2}">android端</c:when>
                <c:when test="${item.feedBackSource == 3}">wap端</c:when>
                <c:when test="${item.feedBackSource == 4}">IOS端</c:when>
                <c:otherwise>PC端</c:otherwise>
            </c:choose>
        </display:column>
        <display:column title="处理意见" style="max-width: 200px;">
        	<div class="order_img_name">${item.respContent}</div>
        </display:column>
        <display:column title="状态" sortable="true" sortName="s.enabled" style="width:100px;">
            <c:choose>
                <c:when test="${item.status == 0}"><font>未读</font></c:when>
                <c:otherwise><font>已读</font></c:otherwise>
            </c:choose>
        </display:column>
        <display:column title="操作" media="html" style="width:205px;">
         <div class="table-btn-group">
            <c:if test="${item.status==0}">
                <button id="check" class="tab-btn" onclick="feedbackReply(${item.id})">
                	回复
            	</button>
            </c:if>
         </div>

        </display:column>
    </display:table>
    <!-- 分页条 -->
     	<div class="clearfix">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
	    			 </div>
       			 </div>
       		</div>
       	</div>
</div>
</div>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/userFeedBackList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</body>
</html>
<div id="showContent" style="display:none;">

</div>
