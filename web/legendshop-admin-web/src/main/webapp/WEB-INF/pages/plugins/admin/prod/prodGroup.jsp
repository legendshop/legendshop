<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>分组编辑 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<body>
       <jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
		  	 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/prodGroup/save" method="post" id="form1" enctype="multipart/form-data">
        <table class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr>
		    	<th class="no-bg title-th">
			    	<span class="title-span">商品管理  ＞  <a href="<ls:url address="/admin/brand/query"/>">商品分组</a>  ＞
						<c:choose>
							<c:when test="${not empty bean.id }"><span style="color:#0e90d2;">分组编辑</span></c:when>
							<c:otherwise><span style="color:#0e90d2;">创建商品分组</span></c:otherwise>
						</c:choose>
					</span>
				</th>
	    	</tr>
	    </thead>
	    </table>
            <input id="id" name="id" value="${bean.id}" type="hidden">
            <div align="center">
            <table  align="center" class="${tableclass} no-border content-table" id="col1">
      <tr>
        <td width="200px">
          <div align="right" > <font color="ff0000">*</font>分组名：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <input type="text" style="width:150px;" name="name" id="name" value="${bean.name}" maxlength="30"/>
        </td>
      </tr>
      <tr>
	      <td>
	      <div align="right" >分组优先级：</div>
	      </td>
	      <td align="left" style="padding-left:2px;">
	      	<select id="sort" name="sort">
	       			<option value="buys">组内按热度分组</option>
	       			<option value="rec_date">组内按照最新分组</option>
      		</select>
	      </td>      
      </tr>
      <tr>
        <td valign="top" >
          <div align="right" ><font color="ff0000">*</font>分组描述：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <%-- <input type="text" name="brief" id="brief" value="${bean.brief}" size="50" maxlength="50"/> --%>
           <textarea name="description" id="description" cols="60" style="width: 300px;height: 100px;">${bean.description}</textarea>
        </td>
      </tr>
		<%-- <tr>
            <td colspan="2">
					<textarea name="content" id="content" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;">${bean.content}</textarea>
            </td>
        </tr> --%>
                <tr>
                	<td></td>
                    <td>
                        <div align="left">
					        <input class="${btnclass}" type="submit" value="保存"/>
                            
                            <%-- <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='${contextPath}/admin/brand/query?status=${bean.status}'" /> --%>
                                <input class="${btnclass}" type="button" value="返回"
                                onclick="javascript:history.go(-1)" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
     </div>
     </div>
     </body>
 		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
        <script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
        <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/prodGroup.js'/>"></script>
		<script language="javascript">
			var contextPath = "${contextPath}";
			var UUID = "${cookie.SESSION.value}";
			$.validator.setDefaults({});
		</script>
</html>
