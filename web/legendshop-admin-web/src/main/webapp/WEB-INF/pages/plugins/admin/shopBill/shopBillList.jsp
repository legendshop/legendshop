<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>商家结算 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">结算管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">结算账单</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty shopOrderBill.status}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/shopBill/query"/>">所有结算单</a></li>
						<li
							<c:if test="${shopOrderBill.status==1}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/shopBill/query?status=1"/>">已出账</a></li>
						<li
							<c:if test="${shopOrderBill.status==2}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/shopBill/query?status=2"/>">商家已确认</a></li>
						<li
							<c:if test="${shopOrderBill.status==3}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/shopBill/query?status=3"/>">平台已审核</a></li>
						<li
							<c:if test="${shopOrderBill.status==4}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/shopBill/query?status=4"/>">结算完成</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/shopBill/query" id="form1"
				method="get">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				<div class="criteria-div">
					<span class="item">
						 店铺：
						<input type="text" id="shopId" name="shopId" value="${shopOrderBill.shopId}"/>
						<input type="hidden" id="shopName" name="shopName" class="${inputclass}" maxlength="50" value="${shopOrderBill.shopName}" /> 
					</span>
					<span class="item">
						 档期：
						<input type="text" id="flag" name="flag" class="${inputclass}" maxlength="20" value="${shopOrderBill.flag}" placeholder="档期"/>
					</span>
					<span class="item">
						 结算单号：
						<input type="text" id="sn" name="sn" class="${inputclass}" maxlength="20" value="${shopOrderBill.sn}" placeholder="结算单号"/> 
						<input type="hidden"id="status" name="status" value="${shopOrderBill.status}" />
						<input type="button" onclick="search()" class="${btnclass}" value="搜索" />
					</span>
				</div>
			</form:form>
			<div align="center" id="shopBillContentList" class="order-content-list"></div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
						 1. 当前商家结算日为:
						 <c:choose>
						 	<c:when test="${shopBillPeriodType eq 'DAY'}">每天</c:when>
						 	<c:when test="${shopBillPeriodType eq 'WEEK'}">每周 (周一 )</c:when>
						 	<c:when test="${shopBillPeriodType eq 'MONTH'}">每月 (${shopBillDay}号)</c:when>
						 </c:choose><br>
						 2. 账单计算公式：<br>
							&nbsp;&nbsp;&nbsp;2.1、 订单金额(含运费) - 退单金额 - 平台佣金 - 分销佣金 + 红包总额 - 退单红包金额 + 预售定金 + 拍卖保证金<br>
							&nbsp;&nbsp;&nbsp;2.2、 结算价模式:（自营店铺，且结算模式为结算价模式 ） 不计算平台佣金，平台向店铺应付金额为：(按订单的商品数量-退单数量) * 商品结算价计算帐单
							<br>4.
						账单处理流程为：系统出账 > 商家确认 > 平台审核 > 财务支付(完成结算)
						4个环节，其中平台审核和财务支付需要平台介入，请予以关注<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	$(document).ready(function () {
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});
	
	function pager(curPageNO) {
		$("#curPageNO").val(curPageNO);
		sendData(curPageNO);
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	function sendData(curPageNO) {
	    var data = {
    		"shopId": $("#shopId").val(),
	        "curPageNO": curPageNO,
	        "status": $("#status").val(),
	        "flag": $("#flag").val(),
	        "sn": $("#sn").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/shopBill/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#shopBillContentList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
</script>
</html>
