<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.legendshop.base.util.ResourcePathUtil"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen"href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/floorTemplate.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<%
	String imagePrefix = ResourcePathUtil.getPhotoPathPrefix();
	request.setAttribute("imagePrefix",  imagePrefix);
%>
</head>
<style>
.radio-wrapper {
  font-size: 12px;
  vertical-align: middle;
  display: inline-block;
  position: relative;
  white-space: nowrap;
  margin-right: 8px;
  cursor: pointer;
}
.radio-item {
  display: inline-block;
  position: relative;
  line-height: 14px;
  vertical-align: middle;
  cursor: pointer;
}
.radio-input {
  position: absolute;
  z-index: 1;
  cursor: pointer;
  opacity: 0;
  filter: alpha(opacity=0);
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
}
.radio-wrapper:hover .radio-item .radio-inner {
  border-color: #0e90d2;
}
.radio-inner {
  position: relative;
  display: inline-block;
  width: 14px;
  height: 14px;
  border-radius: 14px;
  border: 1px solid #d9d9d9;
  background: #fff;
  transition: all 0.2s ease-in-out;
  box-sizing: border-box;
}
.radio-inner:after {
  content: " ";
  display: block;
  width: 6px;
  height: 6px;
  position: absolute;
  left: 3px;
  top: 3px;
  border-radius: 6px;
  opacity: 0;
  transition: all 0.2s ease-in-out;
  background: #fff;
}
.radio-wrapper-checked .radio-inner {
  border-color: #0e90d2;
}
.radio-wrapper-checked .radio-inner:after {
  background: #0e90d2;
  opacity: 1;
}
.radio-wrapper-disabled .radio-inner {
  background: #f3f3f3;
}
.radio-wrapper-disabled .radio-inner:after {
  background: #e4e4e4;
  opacity: 1;
}
.radio-wrapper-disabled ,
.radio-wrapper-disabled .radio-item ,
.radio-wrapper-disabled .radio-input {
  color: rgba(0, 0, 0, 0.25);
  cursor: not-allowed;
}
.radio-wrapper-disabled:hover .radio-item .radio-inner {
  border-color: #d9d9d9;
}
.radio-txt {
  padding: 0 8px;
  margin-left: -3px;
  font-weight: 400;
  vertical-align: top;
}
</style>
<body>

	<form:form  action="${contextPath}/admin/floorItem/saveAdvMessage" enctype="multipart/form-data" method="post" id="updateForm" >
		<div style="margin: 0 auto;text-align:center;" class="${tableclass}">
			<input id="id" name="id" value="${floorAdv.referId}" type="hidden" />
			<input id="floorId" name="floorId" value="${floorId}" type="hidden" />
			<input id="fiId" name="fiId" value="${floorAdv.fiId}" type="hidden" />
			<input id="type" name="type" value="${type}" type="hidden" /> 
			<input id="layout" name="layout" value="${layout}" type="hidden" />
				
			<table class="${tableclass}" style="width: 100%">
				<tbody>
					<tr>
						<td width="50"></td>
						<td><font color="ff0000">*</font>标题: <input type="text" name="title" id="title" value="${floorAdv.title}"  style="width: 500px;"/></td>
					</tr>
					<tr>
						<td width="50"></td>
						<td><font color="ff0000">*</font>链接: <input type="text" name="linkUrl" id="linkUrl" value="${floorAdv.linkUrl}" style="width: 500px;"/></td>
					</tr>

					<tr>
						<td width="50"></td>
						<td>
							 <font color="ff0000">*</font>广告类型: 
							 <label class="radio-wrapper radio-wrapper-checked">
								<span class="radio-item">
									<input type="radio" class="radio-input" value="1"  checked="checked">
									<span class="radio-inner"></span>
								<span class="radio-txt">图片</span>
								</span>
							</label>
						</td>
					</tr>

					<tr>
						<td width="50"></td>
						<td><font color="ff0000">*</font>上传图片:
						<span id="picUrl">
						  <input type="file"  name="file" id="file" oldFile="${floorAdv.picUrl}" />
						</span> 
						<!-- <input type="button" id="image1" value="选择图片" /></td> -->
					</tr>
					<tr>
						<td width="50"></td>
						  <td>
						  	<div class="multimage-gallery" style="height:90px;padding-left: 66px;">
								<ul>
									<li data-index="0" class="primary prodImg">
										<div class="preview">
											<c:if test="${not empty floorAdv.picUrl}">
												 <img width="90px" height="90px" src="<ls:photo item='${floorAdv.picUrl}'/>">
											</c:if>
										</div>
									</ul>
								</div>
						  </td>
					<tr>
					<tr>
						<td width="50"></td>
						<td style=" text-align:center; ">
						<input type="submit" value="修改广告位" id="updateAdvt" class="edit-blue-btn" />
						<input type="button" value="取消" onclick="cancel();" class="edit-gray-btn"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</form:form>
</body>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/advOverlay.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>"type="text/javascript"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var oldPicUrl="${floorAdv.picUrl}";
</script>
</html>