<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>客服聊天</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0"> 
	<link type="text/css" href="<ls:templateResource item='/resources/plugins/layer/theme/default/layer.css'/>" rel="stylesheet" />
	<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/im/im-common.css">
	<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/im/im-index.css">
</head>
<body class="gray-body">
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <jsp:include page="../frame/left.jsp"></jsp:include>
      <div class="admin-content" id="admin-content" style="overflow-x:auto;background: #f6f7f8;">
	    
	    <div class="index clear">
			<div class="index-right">
				<!-- 当前会话、历史会话 -->
				<div class="service-con">
					<!-- 左边栏 -->
					<div class="left-sidebar">
						<div class="shop-list">
							<p class="list-tit">
								<span class="tit-text">
									会话中
									<em class="num">（<span>0</span>）
									</em>
								</span>
							</p>
						</div>
					</div>
					<!-- /左边栏 -->
					<!-- 右半部分内容 -->
					<div class="service-right">
						<div class="rig-head clear">
							<div class="hea-shop">
								<span class="shop-name currentName"></span>
							</div>
							<!-- <a href="javascript:void(0)" class="finish-talk">结束会话</a> -->
						</div>
						<div class="chat-window">
							<!-- 对话窗口 -->
							<div class="chat-list">
								
								<!-- <div class="chat-more">
									<a href="#" class="more">点击加载更多</a>
									<span class="txt">没有更多了</span>
								</div> -->
								<!-- 对话内容 -->
								<!-- <div class="chat-time">
									<p class="time">11:33</p>
								</div> -->
								<!-- 对话内容 -->
							</div>
							<!-- /对话窗口 -->
							<!-- 回复区 -->
							<div class="replay-area">
								<!-- 工具栏 -->
								<div class="rep-toolbar">
									<div class="cho-too clear">
										<a href="javascript:void(0)" class="too-a rep-face">
											<i class="too-i"></i>
										</a>
										<a href="javascript:void(0)" class="too-a rep-img" style="position: relative;cursor: pointer;">
											<i class="too-i"></i>
											<input type="file" id="sendImgFile" name="sendImgFile" accept="image/*" style="cursor: pointer;position: absolute;opacity: -1;width: 22px;height: 22px;top: 0;z-index: 11;overflow: hidden;">
										</a>
									</div>
								</div>
								<!-- /工具栏 -->
								<div class="edit-rep clear">
									<ul class="emoji hide">
										<li class="webim-emoji-item"><img key="U1F401" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_1.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F402" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_2.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F403" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_3.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F404" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_4.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F405" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_5.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F406" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_6.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F407" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_7.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F408" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_8.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F409" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_9.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F410" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_10.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F411" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_11.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F412" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_12.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F413" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_13.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F414" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_14.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F415" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_15.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F416" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_16.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F417" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_17.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F418" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_18.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F419" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_19.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F420" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_20.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F421" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_21.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F422" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_22.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F423" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_23.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F424" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_24.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F425" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_25.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F426" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_26.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F427" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_27.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F428" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_28.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F429" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_29.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F430" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_30.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F431" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_31.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F432" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_32.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F433" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_33.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F434" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_34.png'/>"></li>
					                    <li class="webim-emoji-item"><img key="U1F435" src="<ls:templateResource item='/resources/templets/amaze/images/faces/ee_35.png'/>"></li>
					            	</ul>
									<div class="edit-box" id="context" contenteditable="true"></div>
								</div>
								<div class="edit-btn">
									<a href="javascript:void(0)" class="send-btn" id="send">发送</a>
								</div>
							</div>
							<!-- /回复区 -->
						</div>
						<!-- 右边栏 -->
						<jsp:include page="conversationRight.jsp"></jsp:include>
						<!-- /右边栏 -->
					</div>
					<!-- /右半部分内容 -->
				</div>
				<!-- 当前会话、历史会话 -->
			</div>
		</div>
	   </div>
	</div>
	
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<c:set var="imagesPrefix" value="${shopApi:getPhotoPrefix()}"></c:set>
<c:set var="imagesSuffix" value="${shopApi:getImagesSuffix(1)}"></c:set>
<script type="text/javascript" src="${contextPath}/resources/plugins/layer/layer.js"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/amaze/js/im/imAdmin.js"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var sign = "${sign}";
	var customId = "${customId}";//当前登录的客服ID
	var shopId = "${shopId}";//当前登录的商家ID
	var name = "${name}";//客服名称
	var imagesPrefix = '${imagesPrefix}';
    var imagesSuffix = '${imagesSuffix}';
    var siteName = "${siteName}";//商城名称
    var offset = 0; //分页偏移量
    var count = 50; //聊天记录展示记录条数
    var IM_BIND_ADDRESS = '${IM_BIND_ADDRESS}';
	var websocket = null;//定义的websocket对象
	//屏蔽textarea中回车默认换行
	var myTextArea = document.getElementById("context");  
	myTextArea.onkeydown = function(e){  
		var code;  
	    if (!e) var  e = window.event;  
	    if (e.keyCode) code = e.keyCode;  
	    else if (e.which) code = e.which;  
	    if(code==13 && window.event){  
	        e.returnValue = false;  
	    }else if(code==13){  
	        e.preventDefault();  
	    }   
	}  
</script>

<style>
	.un-read{
		position: absolute;
		left: -5px;
		cursor: pointer;
		margin-top: 10px;
		vertical-align: top;
		border-radius: 5px;
		color: #999;
	}
</style>

</html>