<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<style type="text/css">
.edit-gray-btn {
    color: #333;
    border: 1px solid #ddd;
    background-color: #f9f9f9;
    cursor: pointer;
    height: 28px;
    text-align: center;
    line-height: 24px;
    margin: 0 3px;
    font-size: 12px;
    padding: 0 12px;
    min-width: 50px;
	display: inline-block;
}

.criteria-btn {
  color: #ffffff;
  border: 1px solid #0e90d2;
  background-color: #0e90d2;
  border-color: #0e90d2;
   cursor: pointer;
   height: 28px;
   padding-left: 12px;
   padding-right: 12px;
   text-align: center;
   line-height: 24px;
   margin: 0 3px;
   font-size: 12px;
}
</style>
</head>
<body>
		<div id="menuContentList" style="margin:10px 0px 0 10px;height:300px;overflow-y:auto;" > 
	   		<ul id="catTrees" class="ztree" style="height:245px;"></ul>
     	</div>
		<div align="center">
			<input style="margin:10px;" type="button" class="criteria-btn" onclick="javascript:saveCategoryType();" value="保存" id="Button1" name="Button1">
			<input type="button" value="取消" class="edit-gray-btn" style="margin:10px;" onclick="closeDialog();">
		</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript">
	function saveCategoryType(){
			var zTree = $.fn.zTree.getZTreeObj("catTrees");
			var chkNodes = zTree.getCheckedNodes(true);
				var catIdStr = "";
				var catNameStr = "";
				for(var i=0;i<chkNodes.length;i++){
					if(i>0){
						catIdStr+=",";
						catNameStr+="、";
					}
					catIdStr+=chkNodes[i].id;
					catNameStr+=chkNodes[i].name;
				}
				var msgCon = "确定将【${typeName}】关联到以下分类吗?<br>"+catNameStr;
				if(chkNodes.length<1){
					msgCon = "确定清除所有的关联吗？";
				}
				layer.confirm(msgCon, {icon:3}, function(index){
	    	       	$.ajax({
	    				url:"${contextPath}/admin/category/saveCategoryType", 
	    				type:'post', 
	    				data: {"catIdStr":catIdStr,"typeId":'${typeId}'},
	    				async : true, //默认为true 异步   
	    				error: function(jqXHR, textStatus, errorThrown) {
	    			 		layer.alert("系统错误", {icon:2});
	    				},
	    				success:function(result){
	    				 	layer.close(index);
	    				 	layer.msg("保存成功",{icon:1, time:500},function(){
	    				 		parent.location.reload(); // 父页面刷新
	    				 	});
	    				}
	    			});
				});
	}
	
	function closeDialog(){
		var index = parent.layer.getFrameIndex("RoleMenu"); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭   
	}
	
	$(document).ready(function(){
			//zTree设置
			var setting = {
					check:{enable: true,chkboxType: { "Y": "s", "N": "ps" },chkStyle: "checkbox"}, 
					data:{
						simpleData:{ enable: true }
					}, 
					callback:{beforeCheck: beforeCheck,onCheck: onCheck}
			};
			$.fn.zTree.init($("#catTrees"), setting, ${catTrees});
	});
	
	function beforeCheck(){
	}
		    
	function onCheck(){
	}
</script>
</body>

</html>

