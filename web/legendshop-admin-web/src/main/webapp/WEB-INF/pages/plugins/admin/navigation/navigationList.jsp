<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

	<%
			Integer offset = (Integer)request.getAttribute("offset");
	%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <form:form  action="${contextPath}/admin/system/navigation/query" id="form1" method="post">
    	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr><th> <strong class="am-text-primary am-text-lg">系统管理</strong> /  导航管理</th></tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				        <input type="button" value="创建网站导航" class="${btnclass}" onclick='window.location="<ls:url address='/admin/system/navigation/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>	    
		<display:table name="list" requestURI="/admin/system/navigation/query"  id="parent" export="false" sort="external" class="${tableclass}" style="width:100%">
	    	 <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="名称" property="name"></display:column>
     		<display:column title="次序" property="seq" ></display:column>  
     		<display:column title="位置" >
     					<ls:optionGroup type="label" required="true" cache="true"  beanName="NAVI_POSITION" selectedValue="${parent.position}"/>
     		</display:column> 		
		<c:set var="subItemList" value="${parent.subItems}" scope="request"/>
      <display:column title="二级导航" >
      <c:if test="${fn:length(subItemList) > 0}">
	    <display:table name="subItemList"  id="child" cellpadding="0" cellspacing="0" class="am-table am-table-bordered" >
	      <display:column title="名称" property="name"></display:column>
	      <display:column title="次序" property="seq" style="width:40px"></display:column>
	      <display:column title="操作" media="html" autolink="true" style="width:130px">
		  	<div class="am-btn-toolbar">
			  <div class="am-btn-group am-btn-group-xs">
	            <c:choose>
			  		<c:when test="${child.status == 1}">
			  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="childStatusImg"  itemId="${child.itemId}"  itemName="${child.name}"  status="${child.status}" style="color:#f37b1d;"><span class="am-icon-arrow-down"></span>下线</button>
			  		</c:when>
			  		<c:otherwise>
			  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="childStatusImg"  itemId="${child.itemId}"  itemName="${child.name}"  status="${child.status}" style='color:#0e90d2;'><span class="am-icon-arrow-up"></span>上线</button>
			  		</c:otherwise>
			  	</c:choose>
	      
	      	  <div data-am-dropdown="" class="am-dropdown">
	               <button data-am-dropdown-toggle="" class="am-btn am-btn-default am-btn-xs am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
	               <ul class="am-dropdown-content">
	                 <li><a href="<ls:url address='/admin/system/navigationItem/load/${child.itemId}'/>" >修改</a></li>
	                 <li><a href='javascript:deleteBychildId("${child.itemId}")'>删除</a></li>
	               </ul>
	          </div>
	        </div>
	      </div>
      </display:column>
	    </display:table>
	    </c:if>
      </display:column>	
	    <display:column title="操作" media="html" style="width:130px">
		  	<div class="am-btn-toolbar">
			  <div class="am-btn-group am-btn-group-xs">
			  	<c:choose>
			  		<c:when test="${parent.status == 1}">
			  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="parentStatusImg" itemId="${parent.naviId}" itemName="${parent.name}" status="${parent.status}" style="color:#f37b1d;"><span class="am-icon-arrow-down"></span>下线</button>
			  		</c:when>
			  		<c:otherwise>
			  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="parentStatusImg" itemId="${parent.naviId}" itemName="${parent.name}" status="${parent.status}" style='color:#0e90d2;'><span class="am-icon-arrow-up"></span>上线</button>
			  		</c:otherwise>
			  	</c:choose>
			      
			    <div data-am-dropdown="" class="am-dropdown">
		               <button data-am-dropdown-toggle="" class="am-btn am-btn-default am-btn-xs am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
		               <ul class="am-dropdown-content">
		               	 <li><a href="<ls:url address='/admin/system/navigation/querysearth/${parent.naviId}'/>" >创建二级导航</a></li>
		                 <li><a href="<ls:url address='/admin/system/navigation/load/${parent.naviId}'/>" >修改</a></li>
		                 <li><a href='javascript:deleteById("${parent.naviId}")'>删除</a></li>
		               </ul>
	          	 </div>
	          </div>
	         </div>
	      </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
    </div>
    </div>
    </div>
    </body>
    <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/navigationList.js'/>"></script>
    <script type="text/javascript">
	    var contextPath = "${contextPath}";
	</script>
		</html>
