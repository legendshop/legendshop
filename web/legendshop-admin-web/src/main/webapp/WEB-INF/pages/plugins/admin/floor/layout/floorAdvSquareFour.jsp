<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
  <head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/indexlayout.css'/>" rel="stylesheet"/>
    <title>4x方形广告楼层编辑界面</title>
     
     <style type="text/css">
     	.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}
     </style>
  </head>
  <body >
  <form:form  action="${contextPath}/admin/system/floor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 98%;margin: 10px" >
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo; 
				    	<a href="<ls:url address='/admin/floor/query'/>">4x方形广告楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
	    </table>
	  </form:form>
	     <div id="doc">
	      <div id="bd">
	         <div class="wide_floor" style="">
			    <div class="wide_floor_3">
			      <div class="wide_floor_3_m"> 
			      <div id="floorImage" style="width: 1190px; height: 25px;line-height: 25px;" class="flooredit">
			          <a href="javascript:onclickAdv('${floor.id}')" style="color:white;">广告编辑(287*287)</a>
			       </div>
			       
			       <c:choose>
				         <c:when test="${empty advList}">
				             <div class="wide_floor_3_one">  
					        </div>
					         <div class="wide_floor_3_one">          
					        </div>
					         <div class="wide_floor_3_one">           
					        </div>
					        <div class="wide_floor_3_one"> 
					        </div>
				         </c:when>
				         <c:otherwise>
				          <c:forEach items="${advList}" var="adv" end="4"> 
				             <div class="wide_floor_3_one">  
					            <a href="${adv.linkUrl}" target="_blank"><img src="<ls:photo item='${adv.picUrl}'/>"></a>       
					        </div>
	      		 		  </c:forEach>
				         </c:otherwise>
				      </c:choose>
			      </div>
			    </div>
			  </div>
	          
	      </div>
	      	     <div   style="text-align: center;clear: both; padding-top : 30px;" >
	          	      <input type="button" onclick="window.location='${contextPath}/admin/floor/query' " class="criteria-btn" value="返回">
	     		 </div>
     </div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script> 
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorAdvSquareleFour.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
</script>
</html>
