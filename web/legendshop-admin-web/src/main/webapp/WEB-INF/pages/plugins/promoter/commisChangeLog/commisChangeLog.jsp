<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建佣金变更历史表</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</head>
    <body>
        <form:form action="${contextPath}/admin/commisChangeLog/save" method="post" id="form1">
            <input id="id" name="id" value="${commisChangeLog.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建佣金变更历史表
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">流水号: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="sn" id="sn" value="${commisChangeLog.sn}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">User_id: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userId" id="userId" value="${commisChangeLog.userId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userName" id="userName" value="${commisChangeLog.userName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">变更金额: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="amount" id="amount" value="${commisChangeLog.amount}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">添加时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="addTime" id="addTime" value="${commisChangeLog.addTime}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">描述: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="logDesc" id="logDesc" value="${commisChangeLog.logDesc}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/commisChangeLog/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
    </body>
</html>

