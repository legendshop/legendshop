<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<style type="text/css">
		body{font-size:13px;}
		a:hover{background-color:#d5e4fa;}
		.serachProd{border: 1px solid #d5e4fa;width: 100%;height: 190px;text-align: center;}
		.prod_list {width: 95%;float: left;overflow: hidden;margin-top: 10px;}
		.prod_list li {width: 80px;float: left;display: inline;position: relative;border: 1px solid #ddd;margin-top: 5px;margin-right: 10px;margin-left: 10px;}
		.prod {width: 70px;padding: 5px;overflow: hidden;display: block;}
		.prod_name {float: left;width: 70px;height: 15px;margin-top: 5px;color: #000;overflow: hidden;}
		.prod_page {width: 100%;float: left;margin-top: 10px;margin-bottom: 5px;text-align: center;}
	</style>
</head>
<body scroll="no">
	 <form:form action="${contextPath}/admin/auction/AuctionProdLayout" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f5f9ff;border-bottom:1px solid #d5e4fa;font-size: 14px;text-align: center;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;height:25px;border:1px solid #d5e4fa"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" value="搜索" style="height:25px;outline: none;"/>
			</div>
		</div>
	 </form:form>
	<%-- <table>
		<thead>
			<tr><td>图片</td><td>名称</td><td>操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.name}</td>
          	<td><button><a style="text-decoration: none;color:#333;" target="_blank" href="${contextPath}/views/${product.prodId}">查看</a></button>&nbsp;<button onclick="chooseProd('${product.prodId}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table> --%>
      <div class="serachProd" >
      	<b>商品列表</b>
	      <ul class="prod_list">
			<c:forEach items="${requestScope.list}" var="prod" varStatus="status">
			   <li> 
			        <a class="prod"  onclick="loadProdSku('${prod.prodId}')" href="javascript:void(0);" title="${prod.name}"> <span class="floor_sear_img"> 
			         <img width="70" height="70" src="<ls:images item='${prod.pic}' scale='3' />" alt="${prod.name}"> </span> <span class="prod_name">${prod.name}</span> </a> 
			  </li>
			</c:forEach>
			
			<c:if test="${empty requestScope.list}">
			    暂无商品信息
			</c:if>
		</ul>
      <div class="prod_page" align="right">
			<c:if test="${not empty toolBar}">
	            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
	    	</c:if>
		</div>
      </div>
      
      <div class="serachProdsku" style="margin-top: 10px;">
      	
      </div>
     <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
      <script type="text/javascript">
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      
	      function loadProdSku(prodId){
	      		if(prodId==null||prodId==""||prodId==undefined){
	      				alert("商品Id为空");
	      		}
				var url="${contextPath}/admin/auction/AuctionProdSku/"+prodId;
				$(".serachProdsku").load(url);
	      }
	      function addProd(skuid,name,prodid){
	      		//排除预售商品
	      		$.ajax({
					    url: "${contextPath}/admin/auction/excludePresell",    //请求的url地址
					    dataType: "json",   //返回格式为json
					    async: false, //请求是否异步，默认为异步，这也是ajax重要特性
					    data: {"prodId":prodid,"skuId":skuid},    //参数值
					    type: "post",   //请求方式
					    success: function(result) {
					        if(result=="OK"){
					        	parent.addProdTo(skuid,name,prodid);
	 							layer.closeAll();
					        }else{
					        	 layer.alert(result,{icon:2});
					        }
					    },
					    error: function() {
					    	 layer.alert("网络异常,请稍后重试");
					    }
					});
			}
      </script>
</body>
