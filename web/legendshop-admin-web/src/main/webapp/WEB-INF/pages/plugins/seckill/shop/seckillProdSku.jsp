<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
		.skulist{border:solid #dddddd; border-width:1px 0px 0px 1px;width: 100%;}
		.skulist tr td{border:solid #dddddd; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
	</style>
				<table class="skulist">
					<c:if test="${empty skuList}">
						<c:if test="${not empty prod}">
							<tr >
								<td colspan="5"><b>商品信息</b></td>
							</tr>
							<tr>
								<td>商品名称</td>
								<td>商品价格</td>
								<td>秒杀价格</td>
								<td>库存数量</td>
								<td>操作</td>
								
							</tr>
								<tr id="list_${prod.prodId}">
									<td>${prod.name}</td>
									<td>${prod.price}</td>
									<td>
										<input type="hidden" id="prodId" value="${prod.prodId}"/>
										<input type="text" id="seckillPrice"/>
									</td>
									<td><input type="text" value="${prod.stocks}" id="seckillStock"/></td>
									<td><input type="button" value="移除" onclick="removeSku(${prod.prodId})"/></td>
								</tr>
						</c:if>
					</c:if>
					<c:if test="${not empty skuList}">
							<tr >
								<td colspan="6"><b>商品信息</b></td>
							</tr>
						<tr>
							<td style="min-width: 100px;">sku名称</td>
							<td style="min-width: 80px;">属性</td>
							<td>sku价格</td>
							<td>sku秒杀价格</td>
							<td>sku库存数量</td>
							<td>操作</td>
						</tr>
						<c:forEach items="${skuList}" var="sku" varStatus="status">
							<tr id="list_${sku.skuId}">
								<td>${sku.name}</td>
								<td>${sku.property}</td>
								<td>${sku.price}</td>
								<td>
									<input type="hidden" id="prodId" value="${sku.prodId}"/>
									<input type="hidden" id="skuId" value="${sku.skuId}"/>
									<input type="text" id="seckillPrice" maxlength="8"/>
								</td>
								<td><input type="text" value="${sku.stocks}" id="seckillStock" maxlength="8"/></td>
								<td><input type="button" value="移除" onclick="removeSku(${sku.skuId})"/></td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
