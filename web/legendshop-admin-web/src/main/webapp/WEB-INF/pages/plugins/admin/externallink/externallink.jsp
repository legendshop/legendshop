<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>链接编辑 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	     <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<table class="${tableclass}" style="width: 100%">
    <thead>
    <tr>
        <th class="no-bg title-th">
			<span class="title-span">
				PC首页装修  ＞
				<a href="<ls:url address="/admin/externallink/query"/>">友情链接管理</a>
				  ＞  <c:if test="${not empty bean }"><span style="color:#0e90d2;">链接编辑</span></c:if>
				 <c:if test="${empty bean }"><span style="color:#0e90d2;">链接新增</span></c:if>
			</span>
        </th>
    </tr>
    </thead>
</table>
<form:form action="${contextPath}/admin/externallink/save" method="post" id="form1" enctype="multipart/form-data">
    <input id="id" name="id" value="${bean.id}" type="hidden">
    <div align="center">
        <table align="center" class="${tableclass} no-border content-table" id="col1">

            <tr>
                <td style="width:200px;">
                    <div align="right"><font color="ff0000">*</font>连接地址：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" name="url" id="url" value="${bean.url}" size="50" maxlength="300" class="${inputclass}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="right"><font color="ff0000">*</font>连接显示文字：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" name="wordlink" id="wordlink" value="${bean.wordlink}" size="50" maxlength="50" class="${inputclass}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="right"><font color="ff0000">*</font>描述：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" name="content" id="content" value="${bean.content}" maxlength="50" size="50" class="${inputclass}"/>
                </td>
            </tr>
           <%--  <tr>
                <td>
                    <div align="right">上传友情连接图片(大小81*31)</div>
                </td>
                <td>
                    <input type="file" name="file" id="file" size="50"/>
                    <input type="hidden" name="picture" id="picture" size="80" value="${bean.picture}" class="${inputclass}"/>
                </td>
            </tr> --%>
            <tr>
                <td>
                    <div align="right">次序（数字，越小越靠前）：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" name="bs" id="bs" value="${bean.bs}" maxlength="10" class="${inputclass}"/>
                </td>
            </tr>
            <tr>
            	<td></td>
                <td align="left" style="padding-left: 2px;">
                    <div>
                        <input type="submit" value="保存" class="${btnclass}"/>
                        <input type="button" value="返回" class="${btnclass}"
                               onclick="window.location='${contextPath}/admin/externallink/query'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
</div>
</div>
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/externallink.js'/>"></script>
<script type="text/javascript">
    $.validator.setDefaults({});
</script>
</html>

