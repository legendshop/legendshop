<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
.seckill-desc img {
	width: 100%
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/seckillActivity/save"
				method="post" id="form1">
				<table class="${tableclass}" style="width: 100%">
					<tr>
						<th class="no-bg title-th">
			            	<span class="title-span">
								秒杀管理  ＞  
								<a href="<ls:url address="/admin/seckillActivity/query"/>">秒杀活动管理</a>
								  ＞  <span style="color:#0e90d2;">查看秒杀活动</span>
							</span>
			            </th>
					</tr>
				</table>
				<div align="center">
					<table style="width: 100%" class="${tableclass} no-border " id="col1">
						<%-- <tr>
        	<td>
             <div align="right">操作:</div>
             </td>
            <td>
                <div align="left">
                  <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/seckillActivity/query'" />
                      <c:if test="${seckillActivity.status == -1 && seckillActivity.endTime>nowDate}">
					    <input type="button" class="${btnclass}" id="btn_audit" onclick="audit('${seckillActivity.id}')" value="审核"/>
					  </c:if>
                 </div>
             </td>
         </tr> --%>
						<tr>
							<td style="width:13%;min-width:185px;">
								<div align="right" style="float: right;">活动名称:</div>
							</td>
							<td align="left">${seckillActivity.seckillTitle}</td>
						</tr>
						<tr>
							<td>
								<div align="right">活动摘要:</div>
							</td>
							<td align="left">${seckillActivity.seckillBrief}</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">活动图片:</div>
							</td>
							<td align="left"><a
								href="<ls:photo item='${seckillActivity.seckillPic}'/>"
								target="_blank"><img
									src="<ls:photo item='${seckillActivity.seckillPic}'/>"
									style="max-height: 150px; max-width: 150px;"></a></td>
						</tr>
						<tr>
							<td>
								<div align="right">店铺名称:</div>
							</td>
							<td align="left">${seckillActivity.shopName}</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">活动商品:</div>
							</td>
							<td align="left">
								<table class="am-table am-table-striped content-table see-able" style="width: 100%">
									<tr>
										<th style="width: 35%">商品名称</th>
										<th>商品属性</th>
										<th>秒杀价格</th>
										<th>秒杀库存</th>
									</tr>
									<c:forEach items="${activityDtos}" var="prodDto">
										<tr>
											<td><a href="${PC_DOMAIN_NAME}/views/${prodDto.prodId}"
												target="_blank">${prodDto.prodName}</a></td>
											<td><c:choose>
													<c:when test="${not empty  prodDto.prop}">${prodDto.prop}</c:when>
													<c:otherwise>无</c:otherwise>
												</c:choose></td>
											<td>${prodDto.price}</td>
											<td>${prodDto.stock}</td>
										</tr>
									</c:forEach>

								</table>
							</td>
						</tr>
						<tr>
							<!--         <td> -->
							<!--           <div align="right">活动最低价:</div> -->
							<!--        </td> -->
							<!--         <td> -->
							<!--             ${seckillActivity.seckillLowPrice} -->
							<!--         </td> -->
						</tr>
						<tr>
							<td>
								<div align="right">活动状态:</div>
							</td>
							<td align="left"><c:choose>
									<c:when
										test="${seckillActivity.status eq -2 && seckillActivity.endTime > nowDate}">
										<span style="color: red;">未通过</span>
									</c:when>
									<c:when
										test="${seckillActivity.status eq -1 && seckillActivity.endTime > nowDate}">
										<span style="color: red;">审核中</span>
									</c:when>
								</c:choose> <c:choose>
									<c:when
										test="${seckillActivity.status eq 0 && seckillActivity.endTime > nowDate}">
										<span style="color: red;">未上线</span>
									</c:when>
									<c:when
										test="${seckillActivity.status eq 1 && seckillActivity.endTime > nowDate}">
										<c:if test="${seckillActivity.startTime <= nowDate}">
											<span style="color: red;">上线中</span>
										</c:if>
										<c:if test="${seckillActivity.startTime > nowDate}">
											<span style="color: red;">未开始</span>
										</c:if>
									</c:when>
									<c:when
										test="${seckillActivity.status eq 2 && seckillActivity.endTime > nowDate}">
										<span style="color: red;">已完成</span>
									</c:when>
									<c:when test="${seckillActivity.endTime < nowDate}">
										<span style="color: red;">已过期</span>
									</c:when>
								</c:choose></td>
						</tr>

						<tr>
							<td>
								<div align="right">开始时间:</div>
							</td>
							<td align="left"><fmt:formatDate value="${seckillActivity.startTime}"
									pattern="yyyy-MM-dd HH:mm:ss" var="startTime" /> ${ startTime}
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">结束时间:</div>
							</td>
							<td align="left"><fmt:formatDate value="${seckillActivity.endTime}"
									pattern="yyyy-MM-dd HH:mm:ss" var="endTime" /> ${ endTime}</td>
						</tr>
						<c:if test="${not empty seckillActivity.auditOpinion}">
							<tr>
								<td>
									<div align="right">审批意见:</div>
								</td>
								<td align="left">${seckillActivity.auditOpinion}</td>
							</tr>
							<tr>
								<td>
									<div align="right">审批时间:</div>
								</td>
								<td align="left"><fmt:formatDate value="${seckillActivity.auditTime}"
										pattern="yyyy-MM-dd HH:mm:ss" var="auditTime" /> ${auditTime}
								</td>
							</tr>
						</c:if>
						<tr>
							<td valign="top">
								<div align="right">秒杀描述:</div>
							</td>
							<td class="seckill-desc" align="left" valign="top" style="word-break: break-all;">
								<center style="text-align: left;">${seckillActivity.seckillDesc}</center>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="button" class="${btnclass}" value="返回"
										onclick="window.location='${contextPath}/admin/seckillActivity/query'" />
									<c:if
										test="${seckillActivity.status == -1 && seckillActivity.endTime>nowDate}">
										<input type="button" class="${btnclass}" id="btn_audit"
											onclick="audit('${seckillActivity.id}')" value="审核" />
									</c:if>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>

            <div id="auditFrame" style="display: none; ">
				<div  style="font-size: 0.8em;padding: 10px;">
					<p>
						审核结果<font color="red">*</font>： <select id="auditResult">
							<option value="">请选择</option>
							<option value="0">同意</option>
							<option value="-2">不同意</option>
						</select>
					</p>
					审核意见<font color="red">*</font>：
					<textarea id="auditOpinion"
						style="resize: none; width: 250px; height: 80px;" maxlength="50"
						placeholder="审核意见（50字符以内）"></textarea>
				</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	$("#col1 tr td div").first().css("width", "80px");
	jQuery(document).ready(function() {
		$('.seckill-desc img').attr("style", "width: 480px;height: 360px");
	});
	//弹出审核窗口
	function audit(id) {
		layer.open({
			id : 'audit',
			title : '审核意见',
			type: 1,
			content: $('#auditFrame').html(),
			btn: ['确定', '取消'],
			yes: function(index, layero){
				var result = $(layero).find("#auditResult").val();
				if (!auditmethod(id, $(layero).find("#auditOpinion").val(), result)) {
					return false;
				} 
			}
		});
	}

	//审核方法	
	function auditmethod(id, auditOpinion, status) {
		if (status == null || status == "" || status == undefined) {
			layer.msg("审核结果不能为空", {icon:0});
			return false;
		}
		if (auditOpinion == null || auditOpinion == "" || auditOpinion == undefined) {
			layer.msg("审核意见不能为空", {icon:0});
			return false;
		}
		$.ajax({
			url : contextPath + "/admin/seckillActivity/audit",
			data : {
				"id" : id,
				"auditOpinion" : auditOpinion,
				"status" : status
			},
			type : 'post',
			async : true, //默认为true 异步   
			dataType : 'json',
			error : function(data) {
				layer.alert("错误", {icon:2});
			},
			success : function(data) {
				if (data == "OK") {
					layer.msg("审核成功", {icon:1, time: 700}, function(){
						window.location = '${contextPath}/admin/seckillActivity/query'
					});
					return true;
				} else {
					layer.msg(data, {icon:0});
					return false;
				}
			}
		});
	}
</script>
</html>