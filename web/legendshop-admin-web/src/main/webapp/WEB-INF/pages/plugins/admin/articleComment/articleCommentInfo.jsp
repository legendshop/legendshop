<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>杂志管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
<table class="am-table am-table-striped am-table-hover table title-border" style="width: 100%">
	<thead>
		<tr>
			<th class="no-bg title-th">
            	<span class="title-span">
					帮助中心  ＞
					<a href="<ls:url address="/admin/article/comment/query"/>">杂志评论管理</a>
					  ＞  <span style="color:#0e90d2;">评论</span>
				</span>
            </th>
		</tr>
	</thead>
</table>
<center>
	<form:form method="post" action="${contextPath}/admin/article/save" id="form1" onsubmit="return checkFinish();">
		<table class="am-table am-table-striped content-table no-border content-table" style="width: 100%;font-size:12px;line-height: 28px;">
			<tbody>
				<tr>
					<td style="width:200px;">
						<div align="right">杂志名称：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<div name="artId" id="artId" />
						${articleComment.artName}
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">评论人：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<div name="userId" id="userId">${articleComment.nickName}</div>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">评论时间：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<div name="createtime" id="createtime" />
						<fmt:formatDate value="${articleComment.createtime}" pattern="yyyy-MM-dd HH:mm" />
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">评论内容：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<div name="content" id="content">${articleComment.content}</div>
					</td>
				</tr>
			</tbody>
		</table>
		<input type="button" class="criteria-btn" value="返回" style="color:#fff;float:left;margin-left: 200px;margin-top: -6px;"
                                onclick="window.location='${contextPath}/admin/article/comment/query'" />
		<input type="hidden" id="id" name="id" value="${articleComment.id}" />
	</form:form>
</center>
</div>
</div>
</body>
</html>

