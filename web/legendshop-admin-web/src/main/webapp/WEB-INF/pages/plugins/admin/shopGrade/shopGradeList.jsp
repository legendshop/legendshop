<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/system/shopGrade/query"
				id="form1" method="get">
				<table class="${tableclass}" style="width: 100%">
					<thead>
						<tr>
							<th><strong class="am-text-primary am-text-lg">商家管理</strong>
								/ 商家管理</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<div align="left" style="padding: 3px">
									<input type="hidden" id="curPageNO" name="curPageNO"
										value="${curPageNO}" /> &nbsp; 名称： <input
										class="${inputclass}" type="text" name="name" id="name"
										maxlength="50" value="${shopGrade.name}" /> <input
										class="${btnclass}" type="button" onclick="search()"
										value="搜索" /> <input class="${btnclass}" type="button"
										value="创建商家等级"
										onclick='window.location="<ls:url address='/admin/system/shopGrade/load'/>"' />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list"
					requestURI="/admin/system/shopGrade/query" id="item" export="false"
					sort="external" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="名称" property="name" style="width:80px"></display:column>
					<display:column title="允许的最大展柜" property="maxSortSize"
						style="width:170px"></display:column>
					<display:column title="允许的最大展柜分类" property="maxNsortSize"></display:column>
					<display:column title="允许的最大产品数" property="maxProd"></display:column>

					<display:column title="操作" media="html" style="width: 145px;">
						<div class="am-btn-toolbar">
							<div class="am-btn-group am-btn-group-xs">
								<button
									class="am-btn am-btn-default am-btn-xs am-text-secondary"
									onclick="window.location='${contextPath}/admin/system/shopGrade/load/${item.gradeId}'">
									<span class="am-icon-pencil-square-o"></span> 修改
								</button>
								<button
									class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only"
									onclick="deleteById('${item.gradeId}')">
									<span class="am-icon-trash-o"></span> 删除
								</button>
							</div>
						</div>
					</display:column>
				</display:table>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm("确定删除 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/admin/system/shopGrade/delete/" + id + "'/>";
		   });
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>
