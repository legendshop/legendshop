<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<ul class="floor_sear_pro">
    <c:forEach items="${requestScope.list}" var="product" varStatus="status">
		  <li> <a href="javascript:void(0);" class="floor_sear_a" onclick="goods_list_set('${product.pic}','${product.prodId}','${product.name}');"> 
		  		<span  class="floor_sear_img">
		 			<img id="${product.prodId}" title="${product.name}" src="<ls:photo item='${product.pic}'/>" style="width: 100%; height:100%;"/>
		  		</span>
		  		<span class="floor_sear_name">${product.name}</span>
		   		</a> 
		  </li>
	</c:forEach>
</ul>
<div class="floor_page" align="right">
	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
</div>