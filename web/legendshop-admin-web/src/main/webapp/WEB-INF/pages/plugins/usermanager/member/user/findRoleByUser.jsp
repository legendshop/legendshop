<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	int offset = 1;
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">
						<c:if test="${type eq 'admin'}">管理员管理</c:if><c:if test="${type eq 'user'}">用户管理</c:if>&nbsp;＞&nbsp;
						<a href="<ls:url address="/admin/system/userDetail/query"/>"><c:if test="${type eq 'admin'}">管理员角色管理</c:if><c:if test="${type eq 'user'}">用户角色管理</c:if></a>
              				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">用户[${bean.name }]角色列表</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/user/roles" id="form1"
				method="post">
				<input type="hidden" id="appNo" name="appNo" value="${param.appNo}">
				<input type="hidden" name="userId" value="${bean.id }">
				<div align="center" class="order-content-list" style="margin-top: 20px;">
					<%@ include file="/WEB-INF/pages/common/messages.jsp"%>

					<display:table name="list" id="item" export="false"
						class="${tableclass}" style="width:100%">
						<display:column style="width:70px" title="
							<span>
					           <label class='checkbox-wrapper'>
									<span class='checkbox-item'>
										<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
										<span class='checkbox-inner' style='margin-left:10px;'></span>
									</span>
							   </label>	
							</span>">
							<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" value="${item.id}" name="strArray" class="checkbox-input selectOne" onclick="selectOne(this);"/>
									<span class="checkbox-inner" style="margin-left:10px;"></span>
								</span>
						   </label>	
						</display:column> 
						<display:column title="名称 " property="name"></display:column>
						<display:column title="角色名称 " property="roleType"></display:column>
						<display:column title="状态">
							<ls:optionGroup type="label" required="true" cache="true"
								beanName="ENABLED" selectedValue="${item.enabled}"
								defaultDisp="" />
						</display:column>
						<display:column title="备注" property="note"></display:column>
					</display:table>
				</div>
				<div align="center" style="margin-top: 15px;">
					<input class="${btnclass}" type="button" value="增加角色"
						onclick="window.location='<ls:url address="/admin/member/user/otherRoles/${bean.id}?appNo=${appNo}"/>'" />
					<input class="${btnclass}" type="button" value="删除"
						onclick="deleteAction();" /> <%-- <input class="${btnclass}"
						type="button" value="返回" onclick="javascript:history.go(-1);" /> --%>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">

	//全选
	function selAll() {
		chk = document.getElementById("checkbox");
		allSel = document.getElementsByName("strArray");

		if (!chk.checked) {
			for (i = 0; i < allSel.length; i++) {
				allSel[i].checked = false;
			}
		} else {
			for (i = 0; i < allSel.length; i++) {
				allSel[i].checked = true;
			}
		}
	}

	function deleteAction() {
		//获取选择的记录集合
		selAry = document.getElementsByName("strArray");
		count = 0;
		selValue = "";
		for (i = 0; i < selAry.length; i++) {
			if (selAry[i].checked) {
				count++;
				selValue = selAry[i].value;
			}
		}

		if (count < 1) {
			layer.msg("删除时至少选中一条记录！",{icon:0});
			return false;
		}
		
		layer.confirm("确定要删除吗？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
				document.forms[0].action = "${contextPath}/admin/member/user/deleteUserRoleByUserId";
				document.forms[0].submit();
				return true;
		   });
	}
</script>
</html>
