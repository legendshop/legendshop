<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
 <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
 <script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
 <style type="text/css">
		.productDetail{border:solid #dddddd; border-width:1px 0px 0px 1px;width: 100%;}
		.productDetail tr td{border:solid #dddddd; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
</style>
            <table class="${tableclass}" style="width: 100%">
			     <thead>
			    	<tr>
			    	  <th><strong class="am-text-primary am-text-lg">团购活动</strong> / 编辑团购活动
					  </th>
			    	</tr>
			    </thead>
	    	</table>
        <form:form action="${contextPath}/admin/group/save" method="post" id="form1" enctype="multipart/form-data">
            <div align="center">
             <table  style="width: 100%" class="${tableclass}" id="col1">
      			  <input type="hidden" id="id" name="id" value="${group.id}"/>
      <tr>
        <td>
          <div align="right">活动名称:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="50" name="groupName" id="groupName" value="${group.groupName}"  size="50" class="${inputclass}" />
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">选择商品:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="button" value="选择商品" onclick="showProdlist()"/>
           <input type="hidden" name="productId" id="productId" value="${group.productId}"/>
           <div class="prodList">
           		<c:if test="${not empty productDetail}">
					<table class="productDetail">
						<tr >
							<td colspan="5"><b>商品信息</b></td>
						</tr>
						<tr>
							<td>商品名称</td>
							<td>商品价格</td>
							
						</tr>
						<tr id="list_${productDetail.prodId}">
							<td>${productDetail.name}</td>
							<td>
								${productDetail.price}
								<input type="hidden" id="prodId" value="${productDetail.prodId}"/>
							</td>
						</tr>
					</table>
				</c:if>
           </div>
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">团购商品分类:<font color="ff0000">*</font></div>
       	</td>
        <td>
           <input type="text" style="cursor: pointer;" id="categoryName" name="categoryName" value="${group.categoryName }" onclick="loadCategoryDialog();" class="${inputclass}"  placeholder="商品分类" readonly="readonly"/>
		   <input type="hidden" id="firstCatId" name="firstCatId"  value="${integralProd.firstCatId }"/>
		   <input type="hidden" id="secondCatId" name="secondCatId"  value="${integralProd.secondCatId}"/>
		   <input type="hidden" id="thirdCatId" name="thirdCatId"  value="${integralProd.thirdCatId }"/>
        </td>
		</tr>
       <tr>
        <td>
          <div align="right">活动图片(大小180*210):<font color="ff0000">*</font></div>
       </td>
        <td>
        	<input type="file" name="groupPicFile" accept="image/*" id="groupPicFile" style="display: inline;"/>
        </td>
      </tr>
       <c:if test="${not empty group.groupImage}">
	      <tr>
	        <td>
	          <div align="right">原有图片:<font color="ff0000">*</font></div>
	       </td>
	        <td>
	        	 <a href="<ls:photo item='${group.groupImage}'/>" target="_blank"><img  src="<ls:photo item='${group.groupImage}'/>" style="max-height: 150px;max-width: 150px;"></a>
	        </td>
	      </tr>
       </c:if>
       <tr>
        <td>
          <div align="right">每人限购:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxlength="5" onchange="checkInputNumForNormal(this,0);" onkeyup="checkInputNumForNormal(this,0);" name="buyQuantity" id="buyQuantity" value="${group.buyQuantity}" class="${inputclass}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">团购价格:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxlength="8"  onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" name="groupPrice" id="groupPrice" value="${group.groupPrice}"  class="${inputclass}" />
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">开始时间:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input readonly="readonly"  name="startTime" size="21" id="startTime" value='<fmt:formatDate value="${group.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' type="text"  />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">结束时间:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input readonly="readonly"  name="endTime" size="21" id="endTime" value='<fmt:formatDate value="${group.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'  type="text" />
        </td>
      </tr>
    	   <tr>
	        <td>
	          <div align="right">团购描述:</div>
	       </td>
	        <td>
	        	<textarea name="groupDesc" id="groupDesc" cols="100" rows="8" style="width:100%;height:400px;visibility:hidden;">${group.groupDesc}</textarea> 
	        </td>
    	  </tr>
      <tr>
             <td colspan="2">
                 <div align="center">
                     <input type="submit" class="${btnclass}" value="保存"/>
                     <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/group/query'" />
                 </div>
             </td>
         </tr>
     </table>
           </div>
        </form:form>
<script language="javascript">
KindEditor.options.filterMode=false;
KindEditor.create('textarea[name="groupDesc"]', {
	cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
	uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
	fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
	allowFileManager : true,
	afterBlur:function(){this.sync();},
	width : '100%',
	height:'400px',
	afterCreate : function() {
		var self = this;
		KindEditor.ctrl(document, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
		KindEditor.ctrl(self.edit.doc, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
	}
});

jQuery.validator.addMethod("isNumber", function(value, element) {       
         return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
    }, "必须整数或小数");  

jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
         value=parseFloat(value);      
         return this.optional(element) || value>=1;       
    }, "必须大于等于1元"); 
    
 $(document).ready(function() {
 		var pic="${group.groupImage}";
 		if(!isBlank(pic)){
 			$("#groupPicFile").addClass("ignore");
 		}
	    jQuery("#form1").validate({
	    ignore: ".ignore",
	            rules: {
			             groupName: {
		               		 required: true
		           		 },productId: {
		               		 required: true
		           		 },categoryName: {
		               		 required: true
		           		 },groupPicFile: {
		               		 required: true
		           		 },startTime: {
		               		 required: true
		           		 },endTime: {
		               		 required: true
		           		 },buyQuantity: {
		               		 required: true
		           		 },groupPrice: {
		               		 required: true,
		               		 isNumber:true,
		               		 isFloatGtZero:true
		           		 }
	       		 },
	        messages: {
	           			 groupName: {
		               		 required: "团购活动名称不能为空"
		           		 },productId: {
		               		 required: "商品不能为空"
		           		 },categoryName: {
		               		 required: "商品分类不能为空"
		           		 },groupPicFile: {
		               		 required: "团购活动图片不能为空"
		           		 },startTime: {
		               		 required: "开始时间不能为空"
		           		 },endTime: {
		               		 required: "结束时间不能为空"
		           		 },buyQuantity: {
		               		 required: "限购人数不能为空"
		           		 },groupPrice: {
		               		 required: "团购价格不能为空"
		           		 }
	        },
	          submitHandler: function (form) {
	         			var prodid=$("#productId").val();
	        	  		if(isBlank(prodid)){
	        	  			layer.msg("商品不能为为空",{icon:0});
	        	  			return false;
	        	  		}
						form.submit();
				}	        
	        
	    });
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
		
     	 laydate.render({
	   		 elem: '#startTime',
	   		 type:'datetime',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     type:'datetime',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
});
function showProdlist(){
	layer.open({
		  title :"选择商品",
		  type: 2, 
		  content: "${contextPath}/admin/group/groupProdLayout",
		  area: ['700px', '560px']
		}); 
	
}
//加载分类用于选择
	function loadCategoryDialog() {
		var page = "${contextPath}/admin/product/loadAllCategory/G";
		layer.open({
			  title :"选择商品",
			  type: 2, 
			  content: page,
			  area: ['280px', '350px']
			}); 
	}
	function loadCategoryCallBack(firstCatId,secondCatId,thirdCatId, catName) {
		
	
	    if(!isBlank(firstCatId)){
	     $("#firstCatId").val(firstCatId);
	    }
	    if(!isBlank(secondCatId)){
	     $("#secondCatId").val(secondCatId);
	    }
	    if(!isBlank(thirdCatId)){
	     $("#thirdCatId").val(thirdCatId);
	    }
		$("#categoryName").val(catName);
	}

 function removeProd(id){
		var length=$(".productDetail tr").length;
		if(length<=3){
			art.dialog.tips("至少保留一条商品数据");
		}else{
			$(".productDetail tr[id=list_"+id+"]").remove();
		}
	}

function addProdTo(prodId){
		if(prodId==null||prodId==""||prodId==undefined){
		    alert("商品Id为空");
		 }
		$("#productId").val(prodId);	 
	 	var url="${contextPath}/admin/group/groupProd/"+prodId;
		$(".prodList").load(url);
}


//检查金额
function checkMoney(val){
	if(isBlank(val)){
		layer.msg("价格不能为空",{icon:0});
    	return false;
    }
    if(parseFloat(val)<=0){
    	layer.msg("价格不能小于0",{icon:0});
    	return false;
    }
    var regex = /^[\d]*(.[\d]{1,2})?$/;
    if(!regex.test(val)){
    	layer.msg("请输入正确的金额格式",{icon:0});
        return false;
    }
    return true;
}

$("input[groupPicFile=file]").change(function(){
	checkImgType(this);
	checkImgSize(this,512);
});

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

function checkInputNumForNormal(obj, num){
		obj.value = obj.value.replace(/[^\d.]/g, "");// 清除“数字”和“.”以外的字符 
		obj.value = obj.value.replace(/^\./g, "");// 验证第一个字符不是.
		obj.value = obj.value.replace(/\.{2,}/g, ".");// 只保留第一个. 清除多余的. 
		obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");// 保证.只出现一次，而不能出现两次以上
		if (num == 4){
			obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d\d\d).*$/,'$1$2.$3');// 只能输入两个小数
		}
		else{
			obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');// 只能输入两个小数
		}
		if(obj.value ==  undefined || obj.value==null || obj.value==""){
			obj.value="1";
		}
		var tempStr = new String(obj.value);
		if(tempStr.indexOf('.') == -1){
		    var valss = parseInt(obj.value);
		    obj.value = Math.round(valss * Math.pow(10, num)) / Math.pow(10, num) ;
		}
	}
</script>