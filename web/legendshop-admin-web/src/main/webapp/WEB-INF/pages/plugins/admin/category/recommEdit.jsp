<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>分类广告- 编辑 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<style type="text/css">
    	.text-overflow{
        	word-break:keep-all;           /* 不换行 */
       	 	white-space:nowrap;          /* 不换行 */
        	overflow:hidden;               /* 内容超出宽度时隐藏超出部分的内容 */
        	text-overflow:ellipsis;         /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
   	 	}
   	 	.am-table > thead > tr > td, .am-table > tbody > tr > td, .am-table > tfoot > tr > td {
		    height: 35px !important;
		}
		.checkbox-items {
		    display: inline-block;
		    position: relative;
		    vertical-align: middle;
		    cursor: pointer;
		}
		.checkbox-wrappers {
		    font-size: 12px;
		    vertical-align: middle;
		    display: inline-block;
		    position: relative;
		    white-space: nowrap;
		    cursor: pointer;
		}
	</style>
</head>
<body>
 	<jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
			  <!-- sidebar start -->
			   <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
<form:form action="${contextPath}/admin/category/recomm/save" method="post" id="form1" enctype="multipart/form-data">
    <table class="${tableclass}" style="width: 100%">
        <thead>
        <tr>
            <th class="no-bg title-th">
                <span class="title-span">商品管理  ＞ <a href="<ls:url address="/admin/category/recomm/query"/>">分类广告</a> ＞  <span style="color:#0e90d2;">编辑</span></span>&nbsp;
            </th>
        </tr>
        </thead>
    </table>
    <input id="categoryId" name="categoryId" value="${category.id}" type="hidden">
    <div align="center">
        <table align="center" class="${tableclass} no-border" id="col1" style="margin-top: 25px;">
            <tr>
                <td style="width:8%;">
                    <div align="right">分类</div>
                </td>
                <td align="left">
                        ${category.name}
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <div>推荐品牌</div>
                </td>
                <td>
                    <div style="width:100%;max-height:300px;overflow:auto;max-width:680px; ">
                        <c:forEach items="${brandList}" var="brand" varStatus="s">
                            <label  class="text-overflow" style="margin-right: 10px;text-align: left;width:150px;<c:if test="${brand.recommedSelect}">color:#0e90d2;</c:if>">
								<label class="checkbox-wrappers <c:if test='${brand.recommedSelect}'>checkbox-wrapper-checked</c:if>">
									<span class="checkbox-items">
										<input type="checkbox" style="margin: 0px;" name="brandIds" <c:if test="${brand.recommedSelect}">checked='checked'</c:if>
										class="checkbox-input selectOne" onchange="brdChkChange(this);" value="${brand.brandId}"/>
										<span class="checkbox-inner"></span>
									</span>
							   </label>	
						   ${brand.brandName}
						   </label>
                        </c:forEach>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <div>广告</div>
                </td>
                <td align="left">
                    <c:if test="${empty advList}">
                        <div><span style="float:left;">广告图片：</span><input style="float:left;" type="file" name="advPicFile"/></div>
                        <br>
                        <div style="margin-top:20px;display:block;">广告链接：<input style="width:450px;border:1px solid #ccc;height: 24px;" type="text" name="advLink"/>(以http开头的链接)</div>
                    </c:if>
                    <c:forEach items="${advList}" var="adv" varStatus="s">
                        <input type="hidden" name="advPic" value="${adv.advPic}"/>
                        <img style="max-width:280px;" src="<ls:photo item='${adv.advPic}' />">
                        <div style="margin-top:20px;"><span style="float:left;">广告图片(285*392)：</span>
                            <input style="float:left;" type="file" name="advPicFile"/>
                        </div>
                        <br>
                        <div style="margin-top:20px;display:block;text-align: left;">广告链接：<input value="${adv.link}" style="width:450px;height: 24px;" type="text" name="advLink"/>(以http开头的链接)</div>
                    </c:forEach>

                </td>
            </tr>
            <tr>
            	<td></td>
                <td align="left">
                    <div>
                        <input class="${btnclass}" type="submit" value="提交"/>
                        <input class="${btnclass}" type="button" value="返回"
                               onclick="window.location='${contextPath}/admin/category/recomm/query'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
</div>
</div>
</body>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/recommEdit.js'/>"></script>
<script type="text/javascript">
    var errorMessage="${errorMessage}";
</script>
</html>