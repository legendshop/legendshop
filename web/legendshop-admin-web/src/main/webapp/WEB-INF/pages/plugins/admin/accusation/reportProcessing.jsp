<!Doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>举报管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
<jsp:include page="/admin/top" />
    <div class="am-cf admin-main">
        <jsp:include page="../frame/left.jsp"></jsp:include>
     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
     	<table border="0" align="center" class="${tableclass} title-border" id="col1" style="width: 100%;">
               <tr>
                   <th class="title-border">咨询管理&nbsp;＞&nbsp;
                        <a href="<ls:url address="/admin/accusation/query"/>" style="color: #0e90d2;">举报管理</a> 
                        &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">举报处理
                   </th>
               </tr>
         </table>
         <form:form  action="${contextPath}/admin/accusation/save" method="post" id="form1">
		<input id="id" name="id" value="${accusation.id}" type="hidden">
		<input name="userDelStatus" type="hidden" value="${accusation.userDelStatus}">
		<input name="prodId" type="hidden" value="${accusation.prodId}">
		<div align="center">
         <table border="0" align="center" class="${tableclass} no-border content-table" id="col1" style="width: 100%;">
	         <tr>
		        <td width="200px">
		          	<div align="right">产品名称：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	${accusation.prodName}
		        </td>
			</tr>
			<tr>
		        <td>
		          	<div align="right">处理结果：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
					<label class="radio-wrapper">
						<span class="radio-item">
							<input type="radio" class="radio-input" name="result" value="1">
							<span class="radio-inner"></span>
						</span>
						<span class="radio-txt">无效举报</span>
					</label>
					<label class="radio-wrapper radio-wrapper-checked">
						<span class="radio-item">
							<input type="radio" class="radio-input" name="result" value="2" checked="checked">
							<span class="radio-inner"></span>
						</span>
						<span class="radio-txt">有效举报</span>
					</label>
					<label class="radio-wrapper">
						<span class="radio-item">
							<input type="radio" class="radio-input" name="result" value="3">
							<span class="radio-inner"></span>
						</span>
						<span class="radio-txt">恶意举报</span>
					</label>
		        </td>
			</tr>
			<tr>
		        <td>
		          	<div align="right">商品处理：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           <label class="radio-wrapper radio-wrapper-checked">
						<span class="radio-item">
							<input type="radio" class="radio-input" name="illegalOff" value="0" checked="checked">
							<span class="radio-inner"></span>
						</span>
						<span class="radio-txt">不处理</span>
					</label>
					<label class="radio-wrapper">
						<span class="radio-item">
							<input type="radio" class="radio-input" name="illegalOff" value="1">
							<span class="radio-inner"></span>
						</span>
						<span class="radio-txt">违规下架</span>
					</label>
		        </td>
			</tr>
			<tr>
				<td>
					<div align="right">处理意见：</div>
				</td>
				<td align="left" style="padding-left: 2px;">
					<textarea name="handleInfo" id="handleInfo" maxlength="250" style="width:500px;height:100px;"></textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="left" style="padding-left: 0px;">
					<div>
						<input class="${btnclass}" type="submit" value="保存" /><input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/accusation/query"/>'" />
					</div>
				</td>
			</tr>
		</table>
         </div>
         </form:form>
         </div>
         </div>
         </body>
         <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
         
           <script type="text/javascript">
	         $(document).ready(function() {
	        	 jQuery("#form1").validate({
	             rules: {
	            	 handleInfo: "required"
	             },
	             messages: {
	            	 handleInfo: {
	                     required: "请填写处理意见"
	                 }
	             }
	         });
	        	//斑马条纹
	         	 $("#col1 tr:nth-child(even)").addClass("even");
	         });
	         $("input:radio[name='result']").change(function(){
	    		 $("input:radio[name='result']").each(function(){
	    			 if(this.checked){
	    				 $(this).parent().parent().addClass("radio-wrapper-checked");
	    			 }else{
	    				 $(this).parent().parent().removeClass("radio-wrapper-checked");
	    			 }
	    	 	});
	    	 });
	         $("input:radio[name='illegalOff']").change(function(){
	    		 $("input:radio[name='illegalOff']").each(function(){
	    			 if(this.checked){
	    				 $(this).parent().parent().addClass("radio-wrapper-checked");
	    			 }else{
	    				 $(this).parent().parent().removeClass("radio-wrapper-checked");
	    			 }
	    	 	});
	    	 });
         </script>
