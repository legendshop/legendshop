<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>预存款管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<style>
.am-table > thead > tr > td, .am-table > tbody > tr > td, .am-table > tfoot > tr > td {
    height: 55px !important;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
		<table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border"> 
			    	用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/preDeposit/pdRechargeQuery"/>">预存款管理</a>
			    	&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">充值信息编辑</span>
		    	</th>
	    	</tr>
	    </table>
	<form:form  action="${contextPath}/admin/preDeposit/pdRechargeUpdate" method="post" id="form1" onsubmit="return;" style="margin-top: 15px;">
			    <table style="100%" border="0" align="center" id="col1"  class="${tableclass} no-border">
			    	<input type="hidden" name="id" id="id" value="${recharge.id}"/>
					<tr>
						<td style="width:13%;min-width:185px;"><div align="right">充值流水号：</div></td>
						<td align="left"><input type="text" name="sn" id="sn" value="${recharge.sn}" size="50" readonly="readonly"/></td>
					</tr>
					<tr>
						<td ><div align="right">充值金额(元)：</div></td>
						<td align="left">${recharge.amount}元</td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${not empty recharge.nickName}">
								<td ><div align="right">用户昵称：</div></td>
								<td align="left">${recharge.nickName}</td>
							</c:when>
							<c:otherwise>
								<td ><div align="right">用户名：</div></td>
								<td align="left">${recharge.userName}</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<td ><div align="right">付款时间：<font color="ff0000">*</font></div></td>
						<td align="left"><input readonly="readonly"  name="paymentTime" id="paymentTime" type="text" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					</tr>
					<tr>
						<td ><div align="right">付款方式：<font color="ff0000">*</font></div></td>
						<td align="left">
							<select id="paymentName" name="paymentName" >
							   	<option selected="selected" value="">--请选择充值方式--</option>
								<option value="支付宝">支付宝</option>
							</select>
							<input type="hidden" value="ALP" name="paymentCode" id="paymentCode"/>
							<input type="hidden" value="${recharge.paymentState}" name="paymentState" id="paymentState"/>
						</td>
					</tr>
					<tr>
						<td ><div align="right">第三方交易平台号：</div></td>
						<td align="left"><input type="text" id="tradeSn" name="tradeSn" maxlength="50" size="50" placeholder="请输入第三方支付平台交易号，内容不超过50字符"/><br>支付宝等第三方支付平台交易号</td>
					</tr>
					<tr>
						<td ><div align="right">操作备注：<font color="ff0000">*</font></div></td>
						<td align="left"><textarea rows="5" cols="50" id="adminUserNote" name="adminUserNote" maxlength="120" placeholder="请输入管理员操作，内容不超过120字符"></textarea></td>
					</tr>
					<tr>
						<td ></td>
						<td>
	                    	 <input type="submit" class="${btnclass}" value="确认提交"/>注：<font color="red">提交前请务必确认是否已收到付款</font>
	           			</td>
					</tr>
				</table>
	</form:form>
</div>
</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				"paymentTime":{required:true},
				"paymentName":{required:true},
				"adminUserNote":{required:true,maxlength:120}
			},
			messages : {
				"paymentTime":{
					required:"请填写支付时间"
				},
				"paymentName":{
					required:"请填写支付方式"
				},
				"adminUserNote":{
					required:"请填写备注信息",
					maxlength:"超出最大字符"
				},
			}
		});
	});
	highlightTableRows("col1");
    //斑马条纹
   	$("#col1 tr:nth-child(even)").addClass("even");	
    
    laydate.render({
  		 elem: '#paymentTime',
  		 type: 'datetime',
  		 calendar: true,
  		 theme: 'grid',
 		 trigger: 'click'
  	  });
	
</script>
</html>