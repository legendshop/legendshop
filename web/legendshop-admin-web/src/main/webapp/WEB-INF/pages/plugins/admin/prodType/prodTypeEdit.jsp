<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>编辑类型- 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
    <style>
        table th {
            background-color: #f9f9f9 !important;
            border: none !important;
            border-bottom: 1px solid #efefef !important;
        }
    </style>
</head>
<body>
<jsp:include page="/admin/top"/>
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form action="${contextPath}/admin/prodType/save" method="post" id="form1">
        <input id="id" name="id" value="${prodType.id}" type="hidden">
        <div align="center">
            <table class="${tableclass} no-border" style="width: 100%">
                <thead>
                <tr class="sortable">
                    <th class="no-bg title-th">
                        <span class="title-span">属性管理  ＞  <a href="<ls:url address="/admin/prodType/query"/>" style="color: #0e90d2;">属性类型管理</a>  ＞  <span style="color:#0e90d2;">编辑类型</span></span>
                    </th>
                </tr>
                </thead>
            </table>
            <table border="0" align="center" class="${tableclass} no-border content-table" id="col0" style="width: 100%">
                <tr>
                    <td align="right" width="160">
                        <font color="ff0000">*</font>类型名称：
                    </td>
                    <td align="left" style="padding-left: 0;">
                        <input type="text" name="name" style="width:200px;" id="name" value="${prodType.name}" maxlength="50"/>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        次&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;序：
                    </td>
                    <td align="left" style="padding-left: 0;">
                        <input type="text" style="width:45px;" name="sequence" id="sequence" value="${prodType.sequence}"/>
                    </td>
                </tr>
<%--                <tr>--%>
<%--                    <td align="right">--%>
<%--                        是否允许用户编辑属性：--%>
<%--                    </td>--%>
<%--                    <td align="left" style="padding-left: 0;">--%>
<%--                        <select id="attrEditable" name="attrEditable">--%>
<%--                            <ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${prodType.attrEditable}"/>--%>
<%--                        </select>--%>
<%--                    </td>--%>
<%--                </tr>--%>
<%--                <tr>--%>
<%--                    <td align="right">--%>
<%--                        是否允许用户编辑参数：--%>
<%--                    </td>--%>
<%--                    <td align="left" style="padding-left: 0;">--%>
<%--                        <select id="paramEditable" name="paramEditable">--%>
<%--                            <ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${prodType.paramEditable}"/>--%>
<%--                        </select>--%>
<%--                    </td>--%>
<%--                </tr>--%>
                <tr>
                    <td></td>
                    <td align="left" style="padding-left: 0;">
                        <div>
                            <input class="${btnclass}" style="margin-left:0;" type="submit" value="保存"/>
                        </div>
                    </td>
                </tr>
            </table>
            </form:form>
            <div style="width:100%;padding:10px;">
                <div align="left" style="margin:5px;">
                    <span style="line-height: 24px;">选择关联规格属性:</span>
                    <a href="javascript:void(0);" style="padding-left: 5px;" onclick="selectProperty('${prodType.id}');">选择</a>
                </div>
                <table align="center" class="${tableclass}" id="col1" style="width: 100%;border-top:1px solid #efefef;margin-bottom:30px;">
                    <thead>
                    <c:if test="${not empty propertyList}">
                        <tr>
                            <th style="width:25%;">规格名称</th>
                            <th style="width: 15%;">别名</th>
                            <th style="width: 40%;">规格值</th>
                            <th style="width: 10%;">顺序</th>
                            <th style="width:10%;">操作</th>
                        </tr>
                    </c:if>
                    </thead>
                    <c:forEach items="${requestScope.propertyList}" var="property">
                        <tr>
                            <td>
                                    ${property.propName}
                            </td>
                            <td>
                                    ${property.memo}
                            </td>
                            <td>
                                <c:forEach items="${property.valueList}" var="propertyValue">
                                    ${propertyValue.name};
                                </c:forEach>
                            </td>
                            <td>
                                    ${property.seq}
                            </td>
                            <td>
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <a href="javascript:void(0);" seq="${property.seq}" onclick="editProdProp('${property.propId}','${prodType.id}',this)">修改</a>｜
                                        <a href="javascript:void(0);" onclick="deleteProdProp('${property.propId}','${prodType.id}',this)">删除</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

                <div align="left" style="margin:5px;">
                    <span style="line-height: 24px;">选择关联品牌:</span>
                    <a href="javascript:void(0);" style="padding-left: 5px;" onclick="selectBrand('${prodType.id}');">选择</a>
                </div>
                <table border="0" align="center" class="${tableclass}" id="col2" style="width: 100%;border-top:1px solid #efefef;margin-bottom:30px;">
                    <thead>
                    <c:if test="${not empty brandList}">
                        <tr>
                            <th style="width: 40%;">品牌</th>
                            <th style="width: 30%;">图片</th>
                            <th style="width: 20%;">操作</th>
                        </tr>
                    </c:if>
                    </thead>

                    <c:forEach items="${brandList}" var="brand" varStatus="status">
                        <tr>
                            <td>${brand.brandName}</td>
                            <td>
                                <a href="<ls:photo item='${brand.brandPic}'  />" title="${brand.brandName}" target="_blank">
                                    <img width="90" height="30" src="<ls:photo item='${brand.brandPic}'  />"/>
                                </a>
                            </td>
                            <td>
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <a href="javascript:void(0);" onclick="deleteBrand('${brand.brandId}','${prodType.id}',this)">删除</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

                <div align="left" style="margin:5px;">
                    <span style="line-height: 24px;">新增参数属性:</span>
                    <%-- <input type="button" class="criteria-btn" value="选择" onclick="selectParameterProperty('${prodType.id}');"/> --%>
                    <a href="javascript:void(0);" style="padding-left: 5px;" onclick="selectParameterProperty('${prodType.id}');">选择</a>
                </div>
                <table border="0" align="center" class="${tableclass}" id="col3" style="width: 100%;border-top:1px solid #efefef;margin-bottom:30px;">
                    <thead>
                    <c:if test="${not empty parameterPropertyList}">
                        <tr>
                            <th style="width: 260px;">属性名称</th>
                            <th style="width: 100px;">别名</th>
                            <th style="width: 470px;">属性值</th>
                            <th style="width: 30px;">顺序</th>
                            <th style="width: 100px;">分组</th>
                            <th>操作</th>
                        </tr>
                    </c:if>
                    </thead>

                    <c:forEach items="${requestScope.parameterPropertyList}" var="parameterProperty">
                        <tr>
                            <td>
                                    ${parameterProperty.propName}
                            </td>
                            <td>
                                    ${parameterProperty.memo}
                            </td>
                            <td>
                                <c:forEach items="${parameterProperty.valueList}" var="parameterPropertyValue">
                                    ${parameterPropertyValue.name};
                                </c:forEach>
                            </td>
                            <td>
                                    ${parameterProperty.seq}
                            </td>
                            <td>
                                    ${parameterProperty.groupName}
                            </td>
                            <td style="width:150px">
                                <div class="am-btn-toolbar">
                                    <div class="am-btn-group am-btn-group-xs">
                                        <a href="javascript:void(0);" onclick="editParameterProp('${parameterProperty.propId}','${prodType.id}',this)">修改</a>｜
                                        <a href="javascript:void(0);" onclick="deleteParameterProp('${parameterProperty.propId}','${prodType.id}',this)">删除</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

                <table border="0" align="center" class="${tableclass}" id="col4" style="width: 100%;border:none;">
                    <tr>
                        <td style="border-bottom:none;">
                            <div align="center">
                                <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/prodType/query"/>'"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/prodTypeEdit.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var brandIds = new Array();
</script>
</html>
