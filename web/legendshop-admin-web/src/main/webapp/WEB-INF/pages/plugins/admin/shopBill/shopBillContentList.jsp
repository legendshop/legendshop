<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/shopBill/query"
		id="item" export="false" class="${tableclass}"
		style="min-width:1200px;">
		<display:column title="档期" property="flag"></display:column>
		<display:column title="结算单号" property="sn"></display:column>
		<display:column title="起止时间" style="min-width: 100px;">
			<fmt:formatDate value="${item.startDate}" pattern="yyyy-MM-dd" />&nbsp;~&nbsp;
			<fmt:formatDate value="${item.endDate}" pattern="yyyy-MM-dd" />
		</display:column>
		<%-- <display:column title="结束日期" style="min-width: 90px;">
			<fmt:formatDate value="${item.endDate}" pattern="yyyy-MM-dd" />
		</display:column> --%>
		<display:column title="订单金额">
			<fmt:formatNumber pattern="#.##" value="${item.orderAmount}" />
		</display:column>
		<display:column title="运费" property="shippingTotals"></display:column>
		<display:column title="退单金额" property="orderReturnTotals"></display:column>
		<display:column title="平台佣金" property="commisTotals"></display:column>
		<display:column title="分销佣金" property="distCommisTotals"></display:column>
		<display:column title="红包总额" property="redpackTotals"></display:column>
		<display:column title="应结金额">
			<fmt:formatNumber pattern="#.##" value="${item.resultTotalAmount}" />
		</display:column>
		<display:column title="出账日期" style="min-width: 90px;">
			<fmt:formatDate value="${item.createDate}" pattern="yyyy-MM-dd" />
		</display:column>
		<display:column title="状态">
			<c:choose>
				<c:when test="${item.status==1}">已出账</c:when>
				<c:when test="${item.status==2}">商家已确认</c:when>
				<c:when test="${item.status==3}">平台已审核</c:when>
				<c:when test="${item.status==4}">结算完成</c:when>
			</c:choose>
		</display:column>
		<display:column title="店铺" property="shopName"></display:column>
		<display:column title="店铺ID" property="shopId"></display:column>
		<display:column title="操作" media="html">
			<div class="table-btn-group">
				<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/shopBill/load/${item.id}'">
					查看
				</button>
			</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 	</div>
   			 </div>
   		</div>
 	 </div>
</html>
