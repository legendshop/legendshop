<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
	<style type="text/css">
		*{margin:0;padding:0;}
		body{font-size:14px;color:#333;padding:10px;}
		.main{width:560px;margin:0 auto;}
		
		/** 搜索区域样式 */
		.main .search-warp{
			width:558px;
			padding:10px 0;
			border:1px solid #ddd;
			background-color:#f9f9f9; 
		}
		.main .search-warp .search-box{
			width:400px;
			margin:0 auto;
		}
		.main .search-warp .search-box input[type='text']{
			width:323px;
			height:30px;
			line-height:30px;
			padding-left:5px;
			background-color:#fff;
			border: 1px solid #ccc;
			color:#666;
		}
		.main .search-warp .search-box input[type='submit']{
			width:60px;
			height:30px;
			margin-left:10px;
			background-color:#0E90D2;
			border:none;
			border-radius:2px;
			color:#fff;
			cursor: pointer;
		}
		/** 搜索区域样式结束 */
		
		/** 商品列表样式 */
		.main .prod-list-warp{
			width:558px;
			border:1px solid #ddd;
			margin-top:10px;
			overflow: hidden;
		}
		.main .prod-list-warp h3{
			padding:10px 0;
			background-color:#F9F9F9;
			border-bottom:1px solid #ddd;
			text-align: center;
			color:#333;
			font-size:12px;
			font-weight:  bold;
		}
		.main .prod-list-warp .prod-list-box{
			width: 100%;
			overflow: hidden;
			padding:10px 0;
			list-style: none;
		}
		.main .prod-list-warp .prod-list-box li{
			float:left;
			width:100px;
			margin-left:10px;
			margin-bottom:10px;
		}
		.main .prod-list-warp .prod-list-box li .prod-box{
			display:inline-block;
			width:88px;
			padding:5px;
			border:1px solid #ddd;
			color:#666;
		}
		.main .prod-list-warp .prod-list-box li .prod-box-on{
			background-color: #7EC8ED;
			border:1px solid #0E90D2;
			color:#fff;
		}
		.main .prod-list-warp .prod-list-box li .prod-box:hover{
			background-color: #7EC8ED;
			border:1px solid #0E90D2;
			color:fff;
		}
		
		.main .prod-list-warp .prod-list-box .prod-box .prod-img{
			width:88px;
			height:88px;
		}
		.main .prod-list-warp .prod-list-box .prod-box .prod-name{
			display:inline-block;
			width:88px;
			margin-top: 5px;
			overflow: hidden;
			white-space:nowrap;
			text-overflow: ellipsis;
		}
		/** 商品列表样式结束 */
		
		/** 分页样式开始 */
		.main .prod-list-warp .prod-list-page{
			width:430px;
			margin:10px auto;
		}
		/* 分页样式结束 */
		
		/** SKU列表样式 */
		.main .prod-sku-list-frame{
			width:100%;
			height:168px;
			margin-top:10px;
		}
	</style>
</head>
<body>
	<div class="main">
		  <!-- 搜索区域 -->
		  <div class="search-warp">
			 <form:form action="${contextPath}/admin/presellProd/addProdSkuLayout" id="form1" method="GET">
			 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
			 		<div class="search-box">
						<input type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
						<input type="submit" value="搜索" />
					</div>
			 </form:form>
		  </div>
		  
		  <!-- 商品列表 -->
	      <div class="prod-list-warp" >
	      	  <h3>商品列表</h3>
		      <ul class="prod-list-box">
				<c:forEach items="${requestScope.list}" var="prod" varStatus="status">
				   <li> 
				        <a class="prod-box"  onclick="loadProdSku('${prod.prodId}',this);" href="javascript:void(0);" title="${prod.name}">
					        <img class="prod-img" src="<ls:images item='${prod.pic}' scale='3' />" alt="${prod.name}">
					        <span class="prod-name">${prod.name}</span>
				       	</a> 
				  </li>
				</c:forEach>
				
				<c:if test="${empty requestScope.list}">
				    暂无商品信息
				</c:if>
			</ul>
	      	<div class="prod-list-page">
				<c:if test="${not empty toolBar}">
		            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
		    	</c:if>
			</div>
	      </div>
	      
	      <!--  单品信息列表 -->
	      <div class="prod-sku-list">
	      	<iframe id="prodSkuListFrame" class="prod-sku-list-frame" frameborder="0" ></iframe>
	      </div>
      </div>
      <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
      <script type="text/javascript">
      	var contextPath = '${contextPath}';
      	
	     function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      
	      var beforeProd = null;
	      function loadProdSku(prodId,currProd){
	      		$(beforeProd).removeClass("prod-box-on");
	      		$(currProd).addClass("prod-box-on");
	      		beforeProd = currProd;
				var url = "${contextPath}/admin/presellProd/loadProdSku/"+prodId;
				document.getElementById("prodSkuListFrame").src = url;
	      }
	      
	      function addProd(prodId,skuId,skuName,skuPrice,cnProperties){
	      		//判断当前选择的sku是否已参加其他活动
	      		if(isAttendActivity(prodId,skuId)){
	      			layer.alert("对不起,您选择的sku已参加其他活动!", {icon:0});
	      			return false;
	      		}
	      		window.parent.addProdTo(prodId,skuId,skuName,skuPrice,cnProperties);
	 			var index = parent.layer.getFrameIndex('prodSkuLayout'); //先得到当前iframe层的索引
	 			parent.layer.close(index); //再执行关闭
		  }
			
			//是否已参加其他营销活动
			function isAttendActivity(prodId,skuId){
			    var b = false;
		    	$.ajax({
		    		url : contextPath + "/admin/presellProd/isAttendActivity/" + prodId + "/"+ skuId,
		    		async : false, 
		    		dataType : "JSON",
		    		success : function(result){
		    			b = result;
		    		}
		    	});
		    	return b;
			}
			
      </script>
</body>
