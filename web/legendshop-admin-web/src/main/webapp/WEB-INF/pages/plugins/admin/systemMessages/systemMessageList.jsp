<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">发送会员通知</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/system/Message/query" id="form1" name="form1">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
				<div class="criteria-div"">
					<span class="item">
						标题：<input type="text" name="title" maxlength="50" value="${title}" class="${inputclass}" placeholder="标题"/>
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
						<input type="button" value="发送会员通知" class="${btnclass}" onclick='window.location="<ls:url address='/admin/system/Message/load'/>"' />
					</span>
				</div>
			</form:form>

			<div align="center" class="order-content-list">
				<c:if test="${not empty requestScope.list }">
					<display:table name="list" requestURI="/admin/system/userDetail/query" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
						<display:column title="
							<span>
					           <label class='checkbox-wrapper'>
									<span class='checkbox-item'>
										<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
										<span class='checkbox-inner' style='margin-left:10px;'></span>
									</span>
							   </label>	
							</span>" style="width: 100px;">
							<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" name="messageId" value="${item.msgId}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
									<span class="checkbox-inner" style="margin-left:10px;"></span>
								</span>
						   </label>	
						</display:column>
						<display:column title="顺序" class="orderwidth" style="width: 145px;"><%=offset++%></display:column>
						<display:column title="标题"><div class="order_img_name">${item.title}</div></display:column>
						<display:column title="接收会员等级" property="userLevel"></display:column>

						<display:column title="发送时间">
							<fmt:formatDate value="${item.recDate}"
								pattern="yyyy-MM-dd HH:mm" />
						</display:column>

						<display:column title="操作" media="html" style="width: 145px;">
							<div class="table-btn-group">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/system/Message/load/${item.msgId}'">
									修改
								</button>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="deleteById('${item.msgId}');">
									删除
								</button>
							</div>
						</display:column>
					</display:table>
				</c:if>
				<c:if test="${empty requestScope.list }">
					<div>找不到符合条件的记录!</div>
				</c:if>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fl">
			       		<input type="button" value="批量刪除" id="batch-del" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/systemMessageList.js'/>"></script>

</html>
