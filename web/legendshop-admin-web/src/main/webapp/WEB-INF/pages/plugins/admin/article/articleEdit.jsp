<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>杂志管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
<table class="${tableclass}" style="width: 100%">
	<thead>
		<tr>
			<th class="no-bg title-th">
            	<span class="title-span">
					帮助中心  ＞
					<a href="<ls:url address="/admin/article/query"/>">杂志管理</a>
					<c:if test="${not empty article }">  ＞  <span style="color:#0e90d2;">杂志编辑</span></c:if>
					<c:if test="${empty article }">  ＞  <span style="color:#0e90d2;">杂志新增</span></c:if>
				</span>
            </th>
		</tr>
	</thead>
</table>
<center>
	<form:form id="form1" method="post" action="${contextPath}/admin/article/save" enctype="multipart/form-data">
		<table class="${tableclass} no-border content-table" style="width: 100%">
			<tbody>
				<tr>
					<td width="200px;">
						<div align="right">
							<font color="FF0000">*</font>杂志名称：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<input type="text" name="name" id="name" size="40" maxlength="50" class="input" style="width:290px;" value="${article.name}" />
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<font color="FF0000">*</font>作者：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<input type="text" name="author" id="author" size="40" maxlength="50" class="input" style="width:290px;" value="${article.author}" />
					</td>
				</tr>

				<tr>
					<td valign="top">
						<div align="right">
							<font color="FF0000">*</font>简介：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<textarea style="min-width:290px" name="intro" id="intro" maxlength="300" size="40">${article.intro}</textarea>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div align="right">
							<font color="FF0000">*</font>概要：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<textarea style="min-width:290px" name="summary" id="summary" maxlength="300" size="40">${article.summary}</textarea>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<font color="FF0000">*</font>类型：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<select name="type">
							<option value="0" <c:if test="${article.type eq 0 }">selected</c:if>>原创</option>
							<option value="1" <c:if test="${article.type eq 1 }">selected</c:if>>转发</option>
						</select>
					</td>
				</tr>

				<tr>
					<td valign="top">
						<div align="right">
							<font color="FF0000">*</font>图片：
						</div>
					</td>
					<td align="left" valign="top" style="padding-left: 2px;">
						<input type="hidden" name="pic" id="pic" value="${article.pic}" />
						<c:if test="${not empty article.pic}">
							<img style="max-width:280px;min-width:100px;margin-bottom:10px;" src="<ls:photo item='${article.pic}' />">
						</c:if>
						<div>
							<input style="float:left;" type="file" name="picFile" />
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right"><font color="FF0000">*</font>发布时间：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
		<%-- 				<input class="Wdate" type="text" readonly="readonly" placeholder="" style="width: 184px;" name="issueTime" id="issueTime"
							value="<fmt:formatDate value="${article.issueTime}" pattern="yyyy-MM-dd HH:mm" />"
							onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',})" /> --%>
							
							<input  type="text" readonly="readonly"   name="issueTime" id="issueTime" value="<fmt:formatDate value="${article.issueTime}" pattern="yyyy-MM-dd HH:mm" />" />
							
					</td>
				</tr>
				<c:if test="${not empty article.createTime }">
					<tr>
						<td>
							<div align="right">创建时间：</div>
						</td>
						<td align="left" style="padding-left: 2px;">
							<input  type="text" disabled="disabled" value="<fmt:formatDate value="${article.createTime}" pattern="yyyy-MM-dd HH:mm" />" />
						</td>
					</tr>
				</c:if>
				<tr>
					<td>
						<div align="right">是否前台显示：</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<select id="status" name="status" value="${article.status }">
							<option value="1" <c:if test="${article.status==1}">selected</c:if>>是</option>
							<option value="0" <c:if test="${article.status==0}">selected</c:if>>否</option>
						</select>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div align="right">内容：</div>
					</td>
					<td colspan="2" style="padding-left: 2px;">
						<textarea name="content" name="content" id="content" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;">
							${article.content}
						</textarea>
					</td>
				</tr>
			</tbody>
		</table>
		<div style="margin:30px auto;">
			<input type="submit" value="发表" class="${btnclass}" />
			<input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/article/query'" />
		</div>

		<input type="hidden" id="id"  name="id" value="${article.id}" />
	</form:form>
</center>
</div>
</div>
</body>

<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/articleEdit.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
	var UUID = "${cookie.SESSION.value}";
</script>
</html>