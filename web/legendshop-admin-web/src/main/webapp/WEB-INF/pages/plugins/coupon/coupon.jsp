<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>营销管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  <style type="text/css">
		.prodList{border:solid #dddddd; border-width:1px 0px 0px 1px;width: 100%;}
		.prodList tr td{border:solid #dddddd; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
	</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
	<div align="center">
	<form:form  action="${contextPath}/admin/coupon/saveCoupon" method="post" id="form1" enctype="multipart/form-data">
		<input id="couponId" name="couponId" value="${coupon.couponId}" type="hidden">
		<table border="0" align="center" class="${tableclass} title-border" style="width: 100%">
			<thead>
				<tr>
					<th class="no-bg title-th">
		            	<span class="title-span">
							店铺促销  ＞
							<a href="<ls:url address="/admin/coupon/query"/>">优惠券管理</a>
							<c:if test="${ not empty coupon.couponId}">  ＞  <span style="color:#0e90d2;">查看详情</span></c:if>
							<c:if test="${empty coupon.couponId}">  ＞  <span style="color:#0e90d2;">添加优惠券</span></c:if>
						</span>
		            </th>
				</tr>
			</thead>
		</table>
		<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
			<tr>
				<td style="width:200px;">
					<div align="right">
						<font color="ff0000">*</font>优惠券名称：
					</div>
				</td>
				<td align="left" style="padding-left: 2px;"><input type="text" maxlength="50" class="${inputclass}" style="width: 300px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> name="couponName" id="couponName" value="${coupon.couponName}" /></td>
			</tr>
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>状态：
					</div>
				</td>
				<td align="left" style="padding-left: 2px;">
				<select <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" id="status" name="status">
					<option <c:if test="${coupon.status eq 1}">selected="selected"</c:if> value="1">上线</option>
					<option <c:if test="${coupon.status eq 0}">selected="selected"</c:if> value="0">下线</option>					
				  </select>
				</td>
			</tr>
			
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>优惠券：
					</div></td>
				<td align="left" style="line-height: 28px;padding-left: 2px;">
						<span id="full">
						满&nbsp;<input class="${inputclass}" style="width: 110px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> type="text" name="fullPrice" id="fullPrice"
							value="<fmt:formatNumber type="number" value="${coupon.fullPrice==null?0:coupon.fullPrice}" pattern="0.00" maxFractionDigits="2"/>" />
						&nbsp;元&nbsp;&nbsp;&nbsp;</span>
					 
					减&nbsp;<input class="${inputclass}" style="width: 110px;" type="text" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> name="offPrice" id="offPrice" value="<fmt:formatNumber type="number" value="${coupon.offPrice}" pattern="0.00" maxFractionDigits="2"/>" />&nbsp;元</td>
			</tr>

			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>活动开始时间：
					</div></td>
				<td align="left" style="padding-left: 2px;"><input class="${inputclass}" style="width: 300px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> readonly="readonly" name="startDate" id="startDate" type="text"
					value='<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm:ss" />' /></td>
			</tr>
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>活动结束时间：
					</div></td>
				<td align="left" style="padding-left: 2px;"><input style="width: 300px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${inputclass}" readonly="readonly" name="endDate" id="endDate" type="text" 
					value='<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss" />' /></td>
			</tr>
			
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>优惠券类型：
					</div>
				</td>
				<td align="left" style="padding-left: 2px;">
					<select <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" id="couponType" name="couponType">
							<ls:optionGroup type="select" required="true" cache="true" beanName="COUPON_TYPE" selectedValue="${coupon.couponType}" />
					</select>					
					<c:if test="${not empty categoryName}">
						&nbsp;&nbsp;&nbsp;类目：<font color='red'>${categoryName }</font>
					</c:if>
				</td>				
			</tr>								
			
			<c:if test="${not empty coupon.couponId && not empty productList}">							
				<tr><td></td>				
					<td align="left">商品图片&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					商品名称		
				</tr>					
				<c:forEach items="${productList }" var="product">
					<tr>
						<td></td>	
						<td align="left"><img src="<ls:images item='${product.pic}' scale='3' />" >
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						${product.name}
						</td>						
					</tr>
				</c:forEach>												
			</c:if>
					
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>领取方式：
					</div>
				</td>
				<td align="left" style="padding-left: 2px;"><select <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" id="getType" name="getType">
						<ls:optionGroup type="select" required="true" cache="true" beanName="GET_TYPE" selectedValue="${coupon.getType}" />
				</select></td>
			</tr> 
			
			<c:if test="${coupon.getType eq 'free' }">
				<tr><td></td><td align="left"><span>领取优惠券的链接地址：
				<a href="${domainName}/p/coupon/apply/${coupon.couponId}" target="_blank">${domainName}/p/coupon/apply/${coupon.couponId}</a></span></td></tr>
			</c:if>
			
			<c:if test="${not empty coupon.couponId && coupon.getType eq 'points'}">
				<tr><td><div align='right'>兑换所需积分: <font color='ff0000'>*</font></div></td>
				<td align="left"><input class='${inputclass}' type='text' <c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if> name='needPoints' id='needPoints' value='${coupon.needPoints}' /></td>
				</tr>
			</c:if>

			<tr>
				<td>
					<div align="right"><font color="ff0000">*</font>领取上限：</div></td>
				<td id="limit" align="left" style="padding-left: 2px;"><input style="width: 300px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${inputclass}" type="text" name="getLimit" id="getLimit" value="${coupon.getLimit}" /></td>
			</tr>
	
			<tr>
				<td>
					<div align="right">
						<font color="ff0000">*</font>初始化优惠券数量：</font>
					</div></td>
				<td align="left" style="padding-left: 2px;"><input style="width: 300px;" <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> type="text" class="${inputclass}" name="couponNumber" id="couponNumber" value="${coupon.couponNumber}" /></td>
			</tr>						
			
			<tr>
				<td valign="top">
					<div align="right"><font color="ff0000">*</font>描述：</div></td>
				<td align="left" style="padding-left: 2px;"><textarea <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> rows="2" cols="5" style="width:60%;height: 100px;" name="description" id="description" maxlength="300">${coupon.description}</textarea></td>
			</tr>
			 <c:if test="${empty coupon.couponId}">
				<tr>
		    		<td>
		    			<div align="right">优惠券图片：</div>
		    		</td>
		    		<td align="left" style="padding-left: 2px;">
		    			<input type="file" class="file" name="file" id="file" size="30"/>
		    			<input type="hidden" class="file" name="couponPic" id="couponPic" size="30" value="${coupon.couponPic}"/>
		    		</td>
		    	</tr>	
	    	</c:if>	
			<c:if test="${not empty coupon.couponPic}">
			    <tr>
			   		<th>原有图片:</th>
			     	<td>
			      	<img src="<ls:photo item='${coupon.couponPic}'/>" height="200" width="200"/>
			      </td>
			    </tr>
    		</c:if>
		</table>
	  <!-- 用户优惠券 -->
	  <c:if test="${not empty coupon.couponId}">
	  <%
        Integer offset = (Integer) request.getAttribute("offset");
  	  %>
      <div align="center" style="padding:10px;">
	  <display:table name="list"  id="item" 
	       export="false" class="${tableclass} see-able" sort="external">
      <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
      <display:column title="礼券名称"><span class="stockAdmin">${item.couponName}</span></display:column>
      <display:column title="券号" ><span class="stockAdmin">${item.couponSn}</span></display:column>
      <display:column title="卡密"><span class="stockAdmin">${item.couponPwd}</span></display:column>
      <display:column title="用户名称"><span class="stockAdmin">${item.userName}</span></display:column>
      <display:column title="领取时间"><span class="stockAdmin"><fmt:formatDate value="${item.getTime}" pattern="yyyy-MM-dd HH:mm" /></span></display:column>
      <display:column title="使用时间"><span class="stockAdmin"><fmt:formatDate value="${item.useTime}" pattern="yyyy-MM-dd HH:mm" /></span></display:column>
      <display:column title="订单编号"><span class="stockAdmin">${item.orderNumber}</span></display:column>
	  <display:column title="状态">
	  	<span class="stockAdmin">
	  	<c:if test="${item.useStatus eq 1}">
      		未使用
      	</c:if>
      	<c:if test="${item.useStatus eq 2}">
      		已使用
      	</c:if>
      	</span>
	   </display:column>  
       </display:table>
        <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default" />
       </div>
       </c:if>
		<div align="center">
			<c:if test="${empty coupon.couponId}">
				<input class="${btnclass}" type="submit" value="保存" /> 
				 
			</c:if>
			<input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/coupon/query"/>'" />
		</div>
	</form:form>

       <!-- 用于提交分页 -->
	  <c:if test="${not empty coupon.couponId}">
		  <form:form action="${contextPath}/admin/coupon/watchCoupon/${coupon.couponId}" method="post" id="userCouponForm">
		   	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		  </form:form>
	  </c:if>
	  <div style="height:100px;"></div>
	</div>
	</div>
	</div>
	</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
	function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("userCouponForm").submit();
     }
     
	 laydate.render({
   		 elem: '#startDate',
   		 type:'datetime',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endDate',
   	     type:'datetime',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
</script>
</html>
