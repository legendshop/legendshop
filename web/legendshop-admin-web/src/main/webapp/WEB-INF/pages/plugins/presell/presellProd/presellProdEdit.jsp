<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>预售活动管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/amaze/css/presell/edit-presell-prod.css" />
   <style>
<!--
-->
</style>
</head>
 <body>
 	<jsp:include page="/admin/top" />
 	<div class="am-cf admin-main">
 	<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
	<!-- sidebar end -->
	<div class="admin-content" id="admin-content" style="overflow-x: auto;">
   <form:form action="${contextPath}/admin/presellProd/save" method="post" id="form1">
       	<input id="id" name="id" type="hidden" value="${presellProd.id }"/>  
        <table class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr>
	    		<th>
		    		<strong class="am-text-primary am-text-lg">预售管理</strong> /
		    		<a href="<ls:url address="/admin/presellProd/query"/>">预售活动管理</a> /
		    		编辑竞价活动
				</th>
	    	</tr>
	    </thead>
	    </table>
       <div align="center">
         <table  style="width: 100%" class="${tableclass}" id="col1">
      <tr>
        <td>
          <div align="right">方案名称：<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="50" name="schemeName" id="schemeName"  size="20" class="${inputclass}" value="${presellProd.schemeName }"/>
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">预售商品：<font color="ff0000">*</font></div>
       </td>
        <td>
        	 <!-- <input type="button" value="选择商品" style="width: 100px;" onclick="showProdlist();"/> -->
        	<table class="prod-table" <c:if test="${empty presellProd.prodId and empty presellProd.skuId}">style="display:none;"</c:if>>
        		<input type="hidden" value="${presellProd.prodId }" name="prodId" id="prodId"/>
        		<input type="hidden" value="${presellProd.skuId }" name="skuId" id="skuId"/>
        		<thead>
	        		<tr>
	        			<td width="60%">单品名称</td>
	        			<td width="20%">属性</td>
	        			<td width="20%">价格(元)</td>
	        		</tr>
        		</thead>
        		<tbody>
	        		<tr>
	        			<td style="padding-left:20px;width:380px;"><a id="skuName" target="_blank" href="${contextPath}/views/${presellProd.prodId}" >${presellProd.skuName}&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
	        			<td id="cnProperties" style="text-align: center;">${empty presellProd.cnProperties?'无':presellProd.cnProperties}</td>
	        			<td id="skuPrice" style="text-align: center;">${presellProd.skuPrice}</td>
	        		</tr>
        		</tbody>
        	</table>
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">预售价格：<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="10" name="prePrice" id="prePrice"  class="${inputclass}" value="${presellProd.prePrice}" onblur="calculatePayPct()"/>
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">预售时间：<font color="ff0000">*</font></div>
       </td>
        <td>
          <input readonly="readonly"  name="preSaleStart" size="21" id="preSaleStart"  type="text"   value="<fmt:formatDate value='${presellProd.preSaleStart}' pattern='yyyy-MM-dd HH:mm:ss'/>" placeholder="开始时间"/>
          -
          <input readonly="readonly"  name="preSaleEnd" size="21" id="preSaleEnd"  type="text"  value="<fmt:formatDate value='${presellProd.preSaleEnd}' pattern='yyyy-MM-dd HH:mm:ss'/>" onchange="generateFinalMTime()" placeholder="结束时间"/>
          </br><span class="input-tips">必填，活动开始时间不早于当前时间，结束时间与开始时间必须相隔24小时以上，前台生效后可以在此时间段付定金。</span>
        </td>
      <tr>
      	<td>
      		<div align="right">支付设置：<font color="ff0000">*</font></div>
      	</td>
      	<td>
      		<input type="radio" id="payPctType-0" name="payPctType" value="0" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">checked="checked"</c:if>/>全额支付
      		&nbsp;&nbsp;
      		<input type="radio"  id="payPctType-1" name="payPctType" value="1" <c:if test="${presellProd.payPctType eq 1}">checked="checked"</c:if>/>定金支付
      		
      		<div id="depositPay" class="deposit-form" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">style="display:none;"</c:if>>
      			<div class="row">
      				<input type="hidden" id="payPct" name="payPct" value="${presellProd.payPct }" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if>/>
      				<span class="label">定金金额：<font color="ff0000">*</font></span>
      				<span class="input"><input type="text" maxLength="10" name="preDepositPrice" id="preDepositPrice"  class="${inputclass}" value="${presellProd.preDepositPrice}" onblur="calculatePayPct();" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if>/>&nbsp;&nbsp;&nbsp;&nbsp;定金占百分比: <span id="payPctSpan" style="color:#FF4500;"><fmt:formatNumber value="${presellProd.payPct*100}" pattern="#0.0#"/></span>&nbsp;%</span>
      			</div>
      			<div class="row" style="margin-bottom:0px;">
      				<span class="label">尾款时间：<font color="ff0000">*</font></span>
      				<span class="input">
      					<input readonly="readonly"  name="finalMStart" size="21" id="finalMStart" class="Wdate" type="text"  value="<fmt:formatDate value='${presellProd.finalMStart}' pattern='yyyy-MM-dd HH:mm'/>" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if> placeholder="开始时间"/>
            			-
            			<input readonly="readonly"  name="finalMEnd" size="21" id="finalMEnd" class="Wdate" type="text"  value="<fmt:formatDate value='${presellProd.finalMEnd}' pattern='yyyy-MM-dd HH:mm'/>" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if> placeholder="结束时间"/>
      				</span>
      				</br><span class="input-tips">尾款支付开始时间为方案设置的预售结束时间， 3天后的第一个24点即结束时间。</span>
      			</div>
      		</div>
      	</td>
      </tr>
      <tr>
        <td>
          <div align="right">发货时间：<font color="ff0000">*</font></div>
       </td>
<%--         <td>
           <input readonly="readonly"  name="preDeliveryTime" id="preDeliveryTime" class="Wdate" type="text" onfocus="" value="<fmt:formatDate value='${presellProd.preDeliveryTime}' pattern='yyyy-MM-dd'/> " size="21" />
        	</br><span class="input-tips">必填，晚于预售结束时间后的第一个24点,如果是定金支付则晚于尾款支付结束时间的第一个24点。</span>
        </td> --%>
        <td>
           <input readonly="readonly"  name="preDeliveryTimeStr" id="preDeliveryTimeStr" class="Wdate" type="text" onfocus="" value="<fmt:formatDate value='${presellProd.preDeliveryTime}' pattern='yyyy-MM-dd'/> " size="21" />
        	</br><span class="input-tips">必填，晚于预售结束时间后的第一个24点,如果是定金支付则晚于尾款支付结束时间的第一个24点。</span>
        </td>
      </tr>
      <tr>
             <td colspan="2">
                 <div align="center">
                     <input type="submit" class="${btnclass}" value="保存"/>
                     <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/presellProd/query?status=${presellProd.status eq -2?'':presellProd.status}'" />
                 </div>
             </td>
         </tr>
     </table>
           </div>
        </form:form>
	</div>
</body>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	
	var oldPayPacType = '${presellProd.payPctType}';
	var oldDeliveryTime = '<fmt:formatDate value='${presellProd.preDeliveryTime}' pattern='yyyy-MM-dd HH:mm'/>';
	
	var schemeName = '${presellProd.schemeName}';
	
	 laydate.render({
		   elem: '#preSaleStart',
		   type:'datetime',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
		  });
		   
		 laydate.render({
		   elem: '#preSaleEnd',
		   type:'datetime',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	    });
</script>
<script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/js/presell/presellProdEdit.js"></script>
</html>