<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    
    <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
			<tr>
				<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               		&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
               	</th>
			</tr>
		</table>
		<div class="user_list" style="margin-bottom:15px;">
        <div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li  ><i></i><a href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
            <li ><i></i><a href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
            <li ><i></i><a href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
           <li ><i></i><a href="<ls:url address="/admin/userinfo/userVisitLog/${userName}?userId=${userId}"/>">会员浏览历史</a></li>
           <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdComm/${userId}?userName=${userName}"/>">会员商品评论</a></li>
           <ls:plugin pluginId="integral">
		        <li ><i></i><a href="<ls:url address="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}"/>">会员积分明细</a></li>
		     </ls:plugin>
           <li class=" am-active"><i></i><a href="">会员预存款明细</a></li>
		     <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdCons/${userName}?userId=${userId}"/>">会员商品咨询</a></li>
          </ul>
        </div>
      </div>
    
    <form:form  action="${contextPath}/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}" id="form1" method="post">
	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
    </form:form>
    <div align="center" class="order-content-list">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
 
	  <display:table name="list" requestURI="/admin/preDeposit/pdCashLogQuery" id="item" export="false" class="${tableclass}">
	  	 <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
	  	  <display:column title="充值流水号" property="sn"></display:column>
	  	  <display:column title="用户名" property="userName"></display:column>
	  	  <display:column title="用户昵称" property="nickName"></display:column>
	  	   <display:column title="创建时间" format="{0,date,yyyy-MM-dd}" property="addTime"></display:column>
	  	   <display:column title="金额变更(元)" property="amount"></display:column>
	  	   <display:column title="日志描述"><div class="order_img_name">${item.logDesc}</div></display:column>
	  </display:table>
 		<c:if test="${empty list}">
         	<div style="background:#f9f9f9;margin-top:20px;padding:20px;border:1px solid #efefef;height:60px;width:99%;">未找到预存款明细记录~</div>
         </c:if> 
        <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
    </div>
    </div>
    </body>
    <script language="JavaScript" type="text/javascript">
    
     function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("form1").submit();
        }
        
  function deleteById(id) {
      if(confirm("  确定删除 ?")){
            window.location = "${contextPath}/admin/productcomment/delete/"+id;
        }
    }
        highlightTableRows("item");
</script>
</html>

