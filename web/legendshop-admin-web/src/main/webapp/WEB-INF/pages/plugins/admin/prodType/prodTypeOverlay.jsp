<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
    <script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
    <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
    <title>类型管理弹框</title>
</head>
<body>

    <form:form  action="${contextPath}/admin/prodType/prodTypeOverlay/${sortId}" id="form1" method="post">
			<div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							 类型名称：<input type="text" id="name" name="name" value="${prodType.name}"/>
				            <input type="submit" value="搜索"/>
			</div>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/prodType/prodTypeOverlay/${sortId}" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
	    	<display:column title="关联" style="width: 60px;">
					<input type="radio" name="id"  value="${item.id}"/>			
			</display:column>
     		<display:column title="类型名称" property="name"></display:column>
     		<display:column title="记录时间" property="recDate" format="{0,date,yyyy-MM-dd HH:mm}" style="width: 200px;"></display:column>
     		<display:column title="次序" property="sequence" style="width: 50px;"></display:column>

	    </display:table>
       <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
    </div>
    
    <div align="center" style="margin-top: 15px;"><input type="button" value=" 保存并关闭" onclick="save_prodType();"/></div>
        <script language="JavaScript" type="text/javascript">
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }

			      //保存选定的类型
			        function save_prodType(){
			        	var prodTypeId;
			     		 jQuery("input[type=radio]").each(function(){
			     			 if(jQuery(this).attr("checked")=="checked"){
			     				prodTypeId = jQuery(this).attr("value");
			     			 }
			     		 });
			     		 $.ajax({
			     				url:"${contextPath}/admin/prodType/saveProdTypeOverlay", 
			     				data: {"prodTypeId":prodTypeId,"id":'${sortId}'},
			     				type:'post', 
			     				dataType : 'json', 
			     				async : true, //默认为true 异步   
			     				error: function(jqXHR, textStatus, errorThrown) {
			     			 		layer.alert("异常错误，请稍后重试",{icon:2});
			     				},
			     				success:function(result){
			     					parent.location.reload();
			     				}
			     				});  
			     	 }
		</script>
</body>
</html>

