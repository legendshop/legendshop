<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
    <form:form  action="${contextPath}/admin/weixinNewstemplate/query" id="form1" method="get">
        <table class="${tableclass}" style="width: 100%">
		      <thead>
		    	<tr>
			    	<th>
				    	<strong class="am-text-primary am-text-lg">微信管理</strong> /  微信素材管理	
			    	</th>
		    	</tr>
			    <tr>
			    	<td>
			    		<ul class=" am-tabs-nav am-nav am-nav-tabs">
				    			<li class="am-active"><a>图文消息</a></li>
				    			<li><a href="${contextPath}/admin/weixinNewstemplate/filePage">图片库</a></li>
				    		</ul>
			    	</td>
			    </tr>
		    </thead>
		    <tbody>
		    <tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				       	    <input class="${inputclass}" type="text" name="templatename" value="${weixinNewstemplate.templatename }" placeholder="标题">
				            <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
				            <input class="${btnclass}" type="button" value="新建图文消息" onclick='window.location="<ls:url address='/admin/weixinNewstemplate/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/weixinNewstemplate/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
     		<display:column title="图文消息标题" property="templatename"></display:column>
     		<display:column title="创建时间"><fmt:formatDate value="${item.createDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" /></display:column>

	    <display:column title="操作" media="html">
	    	<div class="am-btn-toolbar">
	      		<div class="am-btn-group am-btn-group-xs">
		      	<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weixinNewstemplate/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 编辑</button>
      			<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}');" ><span class="am-icon-trash-o"></span> 删除</button>
		      </div>
		     </div>
	      </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
    </div>
        <script language="JavaScript" type="text/javascript">
			<!--
			  function deleteById(id) {
			      if(confirm("  确定删除 ?")){
			            window.location = "<ls:url address='/admin/weixinNewstemplate/delete/" + id + "'/>";
			        }
			    }
			
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			        function search(){
					  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
					  	$("#form1")[0].submit();
					}
			//-->
		</script>

