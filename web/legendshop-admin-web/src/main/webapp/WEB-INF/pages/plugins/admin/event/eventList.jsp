<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
 <style type="text/css">
 .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
 </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%">
	    	<tr><th class="title-border">系统报表&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">后台系统日志</span></th></tr>
		</table>
    <form:form  action="${contextPath}/admin/event/query" id="form1" method="get">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		<div class="criteria-div">
			<span class="item">
				操作员名称：<input type="text" name="userName" maxlength="50" value="${event.userName}" class="${inputclass}" placeholder="操作员名称"/>
			</span>
			<span class="item">
	           	开始时间：<input readonly="readonly"  name="startTime" id="startTime" class="Wdate" type="text" value='<fmt:formatDate value="${event.startTime}" pattern="yyyy-MM-dd"/>' placeholder="开始时间"/>
			</span>
			<span class="item">
				结束时间：<input readonly="readonly" name="endTime" id="endTime" class="Wdate" type="text"  value='<fmt:formatDate value="${event.endTime}" pattern="yyyy-MM-dd"/>' placeholder="结束时间"/>
	            <input type="button" onclick="search()" value="搜索" class="${btnclass}" style="margin-left: 6px;"/>
	        </span>
		</div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
          <c:if test="${not empty requestScope.list }">
			  <display:table name="list" requestURI="/admin/event/query" id="item" export="false" class="${tableclass}" style="width:100%">
		      <display:column title="操作员名称" property="userName"></display:column>
		      <display:column title="操作描述" property="operation"></display:column>
		      <display:column title="响应时间(毫秒)" property="time"></display:column>
		      <display:column title="请求方法"><div class="order_img_name">${item.method}</div></display:column>
		      <display:column title="ip地址" property="ip"></display:column>
		      <display:column title="操作时间" property="createTime" sortable="true" sortName="createTime" format="{0,date,yyyy-MM-dd HH:mm:ss}" ></display:column>
		    <display:column title="操作" media="html" style="width: 60px">
		           <div class="table-btn-group">
		               <button class="tab-btn" onclick="window.location='<ls:url address='/admin/event/load/${item.eventId}'/>'">查看</button>
		           </div>
		      </display:column>
		    </display:table>
	    </c:if>
	    <c:if test="${empty requestScope.list }">
	    	<div>找不到符合条件的记录!</div>
	    </c:if>
        <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
    
    </div>
    </div>
    </body>
        <script language="JavaScript" type="text/javascript">
			highlightTableRows("item");
			function search(){
			  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			  	$("#form1")[0].submit();
			}
			
			 laydate.render({
		   		 elem: '#startTime',
		   		 calendar: true,
		   		 theme: 'grid',
		 		 trigger: 'click'
		   	  });
		   	   
		   	  laydate.render({
		   	     elem: '#endTime',
		   	     calendar: true,
		   	     theme: 'grid',
		 		 trigger: 'click'
		      });
		</script>
</html>
