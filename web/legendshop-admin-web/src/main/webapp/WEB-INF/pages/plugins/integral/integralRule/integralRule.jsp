<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<form:form action="" method="post" id="form1">
				<div align="center">
					<table class="${tableclass}" style="width: 100%">
						<thead>
							<tr>
								<th class="no-bg title-th">
									<span class="title-span">
										积分商城  ＞  
										<span style="color:#0e90d2;">积分规则设置</span>
									</span>
						        </th>
							</tr>
						</thead>
					<table border="0" align="center" class="${tableclass} content-table no-border" id="col1" style="width: 100%">
						<tr>
							<td style="width: 200px;">
								<strong><span style="font-size: 14px;">会员日常获取积分设定</span></strong>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td align="right" >
								是否开启：
							</td>
							<td align="left" style="padding-left: 2px;">
								<label class="radio-wrapper <c:if test='${releSetting.status eq 1}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="rid" id="rid" value="1" <c:if test="${releSetting.status eq 1}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">是</span>
								</label>
								<label class="radio-wrapper <c:if test='${releSetting.status eq 0 || empty releSetting.status}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="0" id="rid" name="rid" <c:if test="${releSetting.status eq 0 || empty releSetting.status}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">否</span>
								</label>
								<input type="hidden" name="status" id="status" value="${releSetting.status}" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">会员注册：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" class="${inputclass}" maxlength="5" name="reg" id="reg" value="${releSetting.reg}" />
								&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">会员登录：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" class="${inputclass}" maxlength="5" name="login" id="login" value="${releSetting.login}" />
								&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">验证邮箱：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" class="${inputclass}" maxlength="5" name="verifyEmail" id="verifyEmail" value="${releSetting.verifyEmail}" />
								&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">验证手机：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" class="${inputclass}" maxlength="5" name="verifyMobile" id="verifyMobile" value="${releSetting.verifyMobile}" />
								&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">订单商品评论：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" class="${inputclass}" maxlength="5" name="productReview" id="productReview" value="${releSetting.productReview}" />
								&nbsp;
							</td>
						</tr>
						<%-- <tr>
		        <td>
		          	<div align="right">推荐用户:</div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="recUser" id="recUser" value="${releSetting.recUser}"/>&nbsp;
		        </td>
			</tr> --%>
						<%--<tr>
		        <td>
		          	<div align="right">晒单:</div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="showOrder" id="showOrder" value="${releSetting.showOrder}"/>&nbsp;
		        </td>
			</tr>
			<tr>
            	<td colspan="2">
            		<strong><span style="font-size: 17px;">会员购物并付款时积分获取设定</span></strong>
            	</td>
            </tr>
			<tr>
		        <td>
		          	<div align="right">积分兑换规则:</div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="integralConvertRelu" id="integralConvertRelu" value="${releSetting.integralConvertRelu}"/>&nbsp;</br>
					<span style="color:#999;font-size:12px;">注:每消费100元，可以获得积分${releSetting.integralConvertRelu}.为空或0则不开启此规则!</span>
		        </td>
			</tr>
			<tr>
		        <td>
		          	<div align="right">单次最高可获得积分:</div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="capping" id="capping" value="${releSetting.capping}"/></br>
		           	<span style="color:#999;font-size:12px;">注:为空则不封顶!</span>
		        </td>
			</tr>--%>
						<%-- <tr>
		        <td>
		          	<div align="right">积分消费规则：</div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="integralConsumptionRelu" id="integralConsumptionRelu" value="${releSetting.integralConsumptionRelu}"/>&nbsp;<br>
					注:每消费10积分，可抵消金额(元)
		        </td>
			</tr> --%>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				reg : {
					digits : true,
					maxlength:5
				},
				login : {
					digits : true,
					maxlength:5
				},
				verifyEmail : {
					digits : true,
					maxlength:5
				},
				verifyMobile : {
					digits : true,
					maxlength:5
				},
				productReview : {
					digits : true,
					maxlength:5
				},
				recUser : {
					digits : true,
					maxlength:5
				},
				showOrder : {
					digits : true,
					maxlength:5
				},
				proportion : {
					digits : true,
					maxlength:5
				},
				mostpoints : {
					digits : true,
					maxlength:5
				},
				integralConvertRelu : {
					digits : true,
					maxlength:5
				},
				capping : {
					digits : true,
					min : 1,
					maxlength:5
				},
			},
			messages : {
				reg : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				login : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				verifyEmail : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				verifyMobile : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				productReview : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				recUser : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				showOrder : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				proportion : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				mostpoints : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				integralConvertRelu : {
					digits : "请输入数字",
					maxlength: "最大长度不能大于 5 个"
				},
				capping : {
					digits : "请输入数字",
					min : "必须要大于0",
					maxlength: "最大长度不能大于 5 个"
				},
			},
			submitHandler : function() {
				$.ajax({
					url : "${contextPath}/admin/integralRule/addRule",
					data : $("#form1").serialize(),
					type : 'post',
					dataType : 'json',
					async : true,
					error : function(jqXHR, textStatus, errorThrown) {
					},
					success : function(retData) {
						layer.msg("更新成功", {
							icon : 1,
							time : 1000
						});
						setTimeout(function() {
							location.reload();
						}, 1000);
					}
				});
			}
		});

		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
		
		  $("input:radio[name='rid']").change(function(){
			  var text = $(this).val();
				$("#status").val(text);
				 $("input:radio[name='rid']").each(function(){
					 if(this.checked){
						 $(this).parent().parent().addClass("radio-wrapper-checked");
					 }else{
						 $(this).parent().parent().removeClass("radio-wrapper-checked");
					 }
			 	});
			 });

	});
</script>
</html>
