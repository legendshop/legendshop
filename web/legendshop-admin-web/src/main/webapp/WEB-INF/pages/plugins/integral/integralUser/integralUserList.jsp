<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>用户管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">积分管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">积分管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li class="am-active"><i></i><a>积分明细</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userIntegral/decrease"/>">积分增减</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/userIntegral/query" id="form1" method="post">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				<div class="criteria-div">
					<span class="item">
						用户名称：<input type="text" name="userName" class="${inputclass}" maxlength="50" value="${integraLog.userName}" placeholder="用户名称" />
					</span>
					<span class="item">
					操作类型：<select name="logType" class="criteria-select">
							<option value="">请选择</option>
							<option value="0" <c:if test="${integraLog.logType eq 0}">selected="selected"</c:if>>系统</option>
							<option value="1" <c:if test="${integraLog.logType eq 1}">selected="selected"</c:if>>注册</option>
							<option value="2" <c:if test="${integraLog.logType eq 2}">selected="selected"</c:if>>登录</option>
							<option value="3" <c:if test="${integraLog.logType eq 3}">selected="selected"</c:if>>充值</option>
							<option value="4" <c:if test="${integraLog.logType eq 4}">selected="selected"</c:if>>订单兑换</option>
							</select>
						<input type="submit" onclick="search()" class="${btnclass}" value="搜索" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/userIntegral/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth" style="width: 80px"><%=offset++%></display:column>
					<display:column title="用户名称" property="userName"
						style="width: 250px"></display:column>
					<display:column title="积分" property="integralNum"
						style="width: 110px"></display:column>
					<display:column title="操作类型" style="width: 200px">
						<c:choose>
							<c:when test="${item.logType eq 0}">系统</c:when>
							<c:when test="${item.logType eq 1}">注册</c:when>
							<c:when test="${item.logType eq 2}">登录</c:when>
							<c:when test="${item.logType eq 3}">充值</c:when>
							<c:when test="${item.logType eq 4}">订单兑换</c:when>
						</c:choose>
					</display:column>
					<display:column title="操作描述" property="logDesc"></display:column>
					<display:column title="添加时间" property="addTime"
						format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"
						sortName="add_time" style="width: 250px"></display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 管理用户的积分<br>
						</td>
					<tr>
				</table>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	$(document).ready(
			function() {
				$("button[name='statusImg']").click(
						function(event) {
							$this = $(this);
							initStatus($this.attr("itemId"), $this
									.attr("itemName"), $this.attr("status"),
									"${contextPath}/admin/brand/updatestatus/",
									$this, "${contextPath}");
						});
				highlightTableRows("item");
			});
</script>
</html>
