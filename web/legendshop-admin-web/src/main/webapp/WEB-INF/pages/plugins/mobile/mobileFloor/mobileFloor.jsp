<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        
    <body>
    	<table class="${tableclass}" style="width: 100%">
         <thead>
             <thead>
				<tr><th><strong class="am-text-primary am-text-lg">商城管理</strong> /  楼层编辑</th></tr>
			</thead>
         </thead>
        </table>
    
        <form:form  action="${contextPath}/admin/mobileFloor/save" method="post" id="form1">
            <input id="id" name="id" value="${mobileFloor.id}" type="hidden">
            <div align="center">
       <table border="0" align="center" class="${tableclass}" id="col1">
                         
		<tr>
		        <td>
		          	<div align="right">楼层名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="name" id="name" value="${mobileFloor.name}" class="${inputclass}" maxlength="25"/>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">状态:</div>
		       	</td>
		        <td>
		           	<select id="status" name="status">
							<ls:optionGroup type="select" required="true" cache="true" beanName="ONOFF_STATUS" selectedValue="${mobileFloor.status}" />
					</select>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">顺序: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="seq" id="seq" value="${mobileFloor.seq}" class="${inputclass}" style="width: 30px;" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">链接:</div>
		       	</td>
		        <td>
		           	<input type="text" name="url" id="url" value="${mobileFloor.url}" class="${inputclass}" style="width: 350px;"/>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">样式:</div>
		       	</td>
		        <td>
		        	<select id="style" name="style">
							<ls:optionGroup type="select" required="true" cache="true" beanName="MOBILE_FLOOR_STYLE" selectedValue="${mobileFloor.style}" />
					</select>
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
	                            <input type="submit" value="保存" class="${btnclass}" />
	                            <input type="button" value="返回" class="${btnclass}"
	                                onclick="window.location='<ls:url address="/admin/mobileFloor/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
    </body>

<script language="javascript">
$(document).ready(function() {
    jQuery("#form1").validate({
        rules: {
            name:"required",
            seq: "required"
        },
        messages: {
        	name: {
                required: "请输入楼层名称"
            },
            seq: {
                required: "请输入顺序"
            }
        }
    });
 
	//斑马条纹
    $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
