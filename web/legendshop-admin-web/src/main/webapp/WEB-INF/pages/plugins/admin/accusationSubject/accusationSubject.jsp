<!Doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>创建举报主题 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
	
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table border="0" align="center" class="${tableclass} title-border" id="col1" style="width: 100%;">
              <tr>
                  <th class="title-border">咨询管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/accusationType/query"/>">举报类型管理</a>
          				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">编辑举报主题</span>
                  </th>
              </tr>
        </table>
        <form:form  action="${contextPath}/admin/accusationSubject/save" method="post" id="form1">
            <input id="asId" name="asId" value="${accusationSubject.asId}" type="hidden">
            <input type="hidden" name="typeId" id="typeId" value="${typeId}" />
            <div align="center" style="margin-top: 10px;">
            <table border="0" align="center" class="${tableclass} no-border" id="col1" style="width: 100%;">
				<tr>
			        <td width="200px">
			          	<div align="right"><font color="ff0000">*</font>名称：</div>
			       	</td>
			        <td align="left">
			           	<input maxlength="20" type="text" name="title" id="title" value="${accusationSubject.title}" />
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>状态：</div>
			       	</td>
			        <td align="left">
			           	<select id="status" name="status">
			        			<ls:optionGroup type="select" required="true" cache="true" beanName="ONOFF_STATUS" selectedValue="${accusationSubject.status}"/>          			
			        		</select>
			        </td>
				</tr>
                <tr>
                	<td></td>
                    <td align="left">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/accusationType/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
 <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
 
   <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            title: "required"
        },
        messages: {
        	title: {
                required: "请输入举报主题名称"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
