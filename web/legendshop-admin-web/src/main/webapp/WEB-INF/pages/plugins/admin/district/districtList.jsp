<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style type="text/css">
  input[type='text']{
  	text-align: center;
  }
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	      <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border">配送管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">地区管理</span></th>
	    	</tr>
	     </table>
     <div class="criteria-div">
        <span class="item"> 选择地区：
	        <select class="combox ${selectclass}"  id="provinceid" name="provinceid"  requiredTitle="true"  childNode="cityid" selectedValue="${entity.provinceid}"  retUrl="${contextPath}/common/loadProvinces"  onchange="changeCities(this.value)">
			</select>
			<select class="combox ${selectclass}"   id="cityid" name="cityid"  requiredTitle="true"  selectedValue="${entity.cityid}"   showNone="false"  parentValue="${entity.provinceid}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}"   onchange="changAreas(this.value)">
			</select>
			<select class="combox ${selectclass}"   id="areaid" name="areaid"   requiredTitle="true"  selectedValue="${entity.areaid}"    showNone="false"   parentValue="${entity.cityid}"  retUrl="${contextPath}/common/loadAreas/{value}" onchange="filterAreas(this.value);">
			</select>
			<input type="button" class="${btnclass}" value="添加"  onclick="javascript:addDisctrct()"/>  
		</span>
    </div>
      
	<div align="center"  id="districtContentList" name="districtContentList" class="order-content-list"></div>
	</div>
	</div>
	</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/districtList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
    var contextPath = "${contextPath}";
    var areaid = '${entity.areaid}';
	var cityid = '${entity.cityid}';
	var provinceid = '${entity.provinceid}';
</script>
</html>
