<%@ page language="java" pageEncoding="UTF-8"%>
<!-- 模板6 -->
 <template v-if="e.template == '6'"> 
                                    <!-- 模板6 -->
                                    <div class="cont_box">
                                   		 <div class="floor left">
                                        	 <h2>{{e.text}}</h2>
                                         </div>
                                         <div class="floor right">
                                        	 <!-- <h2>{{e.lookMore}}</h2> -->
                                         </div>
                                         <!-- <div class="clear"/> -->
                                         <swipe class="swiper-container banner_box">
                                              <swipe-item  v-for="item in e.banners"   class="swiper-slide">
                                                    <img alt="" v-bind:src="photoPath+item.img" />
                                              </swipe-item>
                                         </swipe>

                                        <div class="edit_btns" floor-index={{$index}}>
                                            <a href="javascript:;" class="edit">编辑</a>
                                            <a href="javascript:;" class="delete" v-on:click="removeModule($index)">删除</a>
                                        </div>
										<!-- <div class="clear"/> -->
                                        <div class="edit_area">
                                            <s></s>
                                            <a href="javascript:;" class="close">×</a>
                                            <div class="edit_form">
                                                <div class="form_item">
                                                    <label>楼层标题：</label>
                                                    <input type="text" v-model="e.text">
                                                </div>
												<!-- <div class="form_item">
                                                    <label>查看更多：</label>
                                                    <input type="text" v-model="e.lookMore" placeholder="请输入链接地址">
                                                </div> -->
                                                <div class="add_img_box" style="border-top:none" floor-index={{$index}}>
                                                    <a href="javascript:;" class="add_img" v-on:click="addImage($index, 'banners')" >[+]添加一个轮播图片</a>
                                                    <ul class="img_list">

                                                        <li v-bind:class="{'item_error': item.error}" v-for="item in e.banners">
                                                            <div class="img_item">
                                                                <span class="num">{{$index + 1}}</span>
                                                                <img alt="" v-bind:src="photoPath+item.img">
                                                                <a href="javascript:;" class="img_edit" v-on:click="chooseImage('floors['+$parent.$index+'].banners['+ $index +']','748*180')">重新上传</a>
                                                                <div class="img_link">
                                                                    <div>
                                                                        <span>链接类型：</span>
                                                                        <div class="app-select-bar">
                                                                            <strong v-if="!item.action || item.action == '' || !item.actionParam || !item.actionParam.searchParam" class="app-selected-text">请选择</strong>
                                                                            <strong v-if="item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.cates" class="app-selected-text">商品分类</strong>
                                                                            <strong v-if="item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.brands" class="app-selected-text">商品品牌</strong>
                                                                            <strong v-if="item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.goods" class="app-selected-text">单个商品</strong>
                                                                            <ul class="app-select-box">
                                                                                <li v-bind:class="{'selected': !item.action || item.action == '' || !item.actionParam || !item.actionParam.searchParam }">
                                                                                    <a href="javascript:;" onClick="chooseAction(0, 'floors['+{{$parent.$index}}+'].banners['+ {{$index}} + ']')">请选择</a>
                                                                                </li>
                                                                                <li v-bind:class="{'selected': item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.cates}">
                                                                                    <a href="javascript:;" onClick="chooseAction(1, 'floors['+{{$parent.$index}}+'].banners['+ {{$index}} + ']')">商品分类</a>
                                                                                </li>
                                                                                <li v-bind:class="{'selected': item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.brands}">
                                                                                    <a href="javascript:;" onClick="chooseAction(2, 'floors['+{{$parent.$index}}+'].banners['+ {{$index}} + ']')">商品品牌</a>
                                                                                </li>
                                                                                <li v-bind:class="{'selected':item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.goods}">
                                                                                    <a href="javascript:;" onClick="chooseAction(3, 'floors['+{{$parent.$index}}+'].banners['+ {{$index}} + ']')">单个商品</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <p>
                                                                        <span>链接内容：</span>
                                                                        <template v-if="item.action == 'GoodsDetail' &&  item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.goods">
                                                                            <span>
                                                                                <font color='blue' v-if="item.actionParam.searchParam.goods.goodsInfoname.length > 10" title="{{ item.actionParam.searchParam.goods.goodsInfoname }}">
                                                                                    {{ item.actionParam.searchParam.goods.goodsInfoname.substring(0,10) }}...
                                                                                </font>
                                                                                <font color='blue' v-else title="{{ item.actionParam.searchParam.goods.goodsInfoname }}">
                                                                                    {{ item.actionParam.searchParam.goods.goodsInfoname }}
                                                                                </font>
                                                                            </span>
                                                                        </template>
                                                                        <template v-if="item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.cates">
                                                                            <span><font color='blue'>{{item.actionParam.searchParam.cates.catName}}</font></span>
                                                                        </template>
                                                                        <template v-if="item.actionParam && item.actionParam.searchParam && item.actionParam.searchParam.brands">
                                                                            <span><font color='blue'>{{item.actionParam.searchParam.brands.brandName}}</font></span>
                                                                        </template>
                                                                    </p>
                                                                    <span style="position: absolute;top:5px;margin-left:175px; font-weight:500;width: 150px">建议尺寸（748*180px）</span>
                                                                </div>
                                                            </div>
                                                            <a class="delete_item" href="javascript:;" v-on:click="deleteImg($parent.$index, $index, 'banners')">×</a>
                                                        </li>
                                                        

                                                    </ul>
                                                </div>
                                                
                                                <a href="javascript:;" class="btn btn_green" onClick="handleSubmit(this, 'floors', {{$index}})">保存</a>
                                                
                                                
                                                <div class="img_sample">
                                                    <a href="javascript:;" class="show_sample">查看图片广告示例 <i class="icon-angle-right"></i></a>
                                                    <div style="display:none">
                                                        <img alt="" src="${contextPath}/resources/templets/amaze/images/appdecorate/img_type7_sample.jpg">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
    </template> 
