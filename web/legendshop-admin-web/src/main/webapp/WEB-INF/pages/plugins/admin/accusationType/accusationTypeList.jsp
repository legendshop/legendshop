<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>举报类型管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
th {
    text-align: center !important;
}
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
.am-table-child > thead:first-child > tr:first-child > th{
	background: #f9f9f9;
	border-left:none;
}
.am-table-child > thead:first-child > tr:first-child > th:first-child{
	border-left:1px solid #ddd;
}
.am-table-child > thead:first-child > tr:first-child > th:last-child{
	border-right:1px solid #ddd;
}
.am-table-bordered >tbody > tr> td {
   border-left:none !important;
}
.am-table-bordered >tbody > tr> td:first-child {
   border-left:1px solid #ddd !important;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border">咨询管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">举报类型管理</span></th>
	    	</tr>
		</table>
    <form:form  action="${contextPath}/admin/accusationType/query" id="form1" method="get">
   	    <div class="criteria-div">
	       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	       	    <span class="item">
	       	    	举报类型名称：
	            	<input class="${inputclass}" type="text" name="name" maxlength="50" value="${accusationType.name}" />
		            <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
		            <input class="${btnclass}" type="button" value="创建举报类型" onclick='window.location="<ls:url address='/admin/accusationType/load'/>"'/>
	            </span>
	      </div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/s/accusationType/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%;margin-bottom: 60px;">
	    	
	    	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="名称"><div class="order_img_name">${item.name}</div></display:column>
     		<display:column title="举报主题">
					<display:table name="${item.subjectList}" id="subject"
						class="am-table am-table-bordered am-table-child" cellpadding="0" style="min-width: 275px !important;"
						cellspacing="0">
						<display:column title="名称" style="width:100px;"><div class="order_img_name">${subject.title}</div></display:column>
						<display:column title="记录时间" style="width:160px">
				      		<fmt:formatDate value="${subject.recDate}" type="date" />
				      </display:column>
						<display:column title="操作" media="html" autolink="true"
							style="width:50px">
								<div class="table-btn-group">
								<c:choose>
									<c:when test="${subject.status == 1}">
										<button class="tab-btn" name="childStatusImg"  itemId="${subject.asId}"  itemName="${subject.title}"  status="${subject.status}">下线</button>
										<span class="btn-line">|</span>
									</c:when>
									<c:otherwise>
										<button class="tab-btn" name="childStatusImg"  itemId="${subject.asId}"  itemName="${subject.title}"  status="${subject.status}">上线</button>
										<span class="btn-line">|</span>
									</c:otherwise>
								</c:choose>
								 <div data-am-dropdown="" class="am-dropdown">
					                <button data-am-dropdown-toggle="" class="tab-btn am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
					                <ul class="am-dropdown-content">
					                  <li><a href="<ls:url address='/admin/accusationSubject/load/${item.typeId}/${subject.asId}'/>">编辑</a></li>
					                  <li><a href='javascript:deleteBychildId("${subject.asId}")'>删除</a></li>
					                </ul>
					              </div>
							  </div>
						</display:column>
					</display:table>
			</display:column>	
     		<display:column title="记录时间" >
     			<fmt:formatDate value="${item.recDate}" type="date" />
     		</display:column>
		    <display:column title="操作" media="html" >
				  <div class="table-btn-group">
					<c:choose>
						<c:when test="${item.status == 1}">
							<button class="tab-btn" onclick="changeStatus(this);" itemId="${item.typeId}" itemName="${item.name}" status="${item.status}"  >下线</button>
							<span class="btn-line">|</span>
						</c:when>
						<c:otherwise>
							<button class="tab-btn" onclick="changeStatus(this);" itemId="${item.typeId}" itemName="${item.name}" status="${item.status}"  >上线</button>
							<span class="btn-line">|</span>
						</c:otherwise>
					</c:choose>
					 <div data-am-dropdown="" class="am-dropdown">
		                <button data-am-dropdown-toggle="" class="tab-btn am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
		                <ul class="am-dropdown-content">
		                  <li><a href="<ls:url address='/admin/accusationSubject/load/${item.typeId}'/>">创建主题</a></li>
		                  <li><a href="<ls:url address='/admin/accusationType/load/${item.typeId}'/>">编辑</a></li>
		                  <li><a href='javascript:deleteById("${item.typeId}")'>删除</a></li>
		                </ul>
		              </div>
				  </div>
		      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
    </div>
    </div>
    </div>
    </body>
    </html>
      <script language="JavaScript" type="text/javascript">
	
function deleteById(id) {

      layer.confirm("确定删除 ?",function(){
    	  window.location = "<ls:url address='/admin/accusationType/delete/" + id + "'/>";
      });
    }
	
	function deleteBychildId(id) {

	      layer.confirm("确定删除该主题？如果这是最后一个关联主题，则举报类型也同步下线",function(){
	    	  window.location = "<ls:url address='/admin/accusationSubject/delete/" + id + "'/>";
	      });
	    }
	
   function pager(curPageNO){
       document.getElementById("curPageNO").value=curPageNO;
       document.getElementById("form1").submit();
   }
   
   jQuery(document).ready(function(){
  		
  		 $("button[name='childStatusImg']").click(function(event){

			 $this = $(this);

			 var status = $this.attr("status");

			 if(status == 1){
				 toStatus  = 0;
				 desc = '确定要下线该举报主题?如果这是最后一个关联主题，则举报类型也同步下线';
			 }else{
				 toStatus = 1;
				 desc = '确定要上线该举报主题?';
			 }

			 layer.confirm(desc, {icon:3, title:'提示'}, function(index){

			 	 var url = "${contextPath}/admin/accusationSubject/updatestatus/";

				 jQuery.ajax({
					 url: url + $this.attr("itemId") + "/" + toStatus,
					 type:'get',
					 async : false, //默认为true 异步
					 dataType : 'json',
					 success:function(data){
						 if(data == 1){

							 $this.html("下线");
						 }else if(data == 0){

							 $this.html("上线");
						 }
						 $this.attr("status",data);
						 window.location.reload();
					 }
				 });

				 <%--initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/accusationSubject/updatestatus/", $this,"${contextPath}");--%>
			 });

		 });
		 highlightTableRows("parent");
    });


    function search(){
	  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#form1")[0].submit();
	}
    
    
    /* 举报类型上下线 */
    function changeStatus(obj){
    	
    	$_this = $(obj);
    	var itemId = $_this.attr("itemId");
    	var status = $_this.attr("status");
    	var url = "${contextPath}/admin/accusationType/updatestatus/";
    	
    	var desc;
      	var toStatus;
	   	   if(status == 1){
	   	   		toStatus  = 0;
	   	   		desc = '确定要 下线该举报类型?';
	   	   }else{
	   	       toStatus = 1;
	   	       desc = '确定要 上线该举报类型?';
	   	   }
   	    layer.confirm(desc, {icon:3, title:'提示'}, function(index){
    		   jQuery.ajax({
  				url: url + itemId + "/" + toStatus, 
  				type:'get', 
  				async : false, //默认为true 异步   
  				dataType : 'json', 
  				success:function(data){
  					
  					if(data == 1){
  						layer.msg("上线成功", {icon:1});   
  						window.location.reload();
  					}else if(data == 0){
  						layer.msg("下线成功", {icon:1});
  						window.location.reload();
  					}else if(data == -2){
  						layer.alert("上线失败，至少要创建并且上线一个举报主题", {icon:2});  
						return false;
  					}
  				}   
  		 });  
   		 return true;
   		 layer.close(index);
   	     });
    }
	
</script>

