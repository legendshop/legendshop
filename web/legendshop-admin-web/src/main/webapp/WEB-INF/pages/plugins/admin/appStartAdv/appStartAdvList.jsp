<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>移动装修 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" href="<ls:templateResource item='/resources/plugins/zoom/css/zoom.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
    <!-- 标题 -->
    <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
	    		<th class="title-border">移动装修&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">APP启动广告</span></th>
	    	</tr>
   	</table>
   	
    <!-- 条件搜索 -->
    <form:form action="${contextPath}/admin/appStartAdv/query" id="form1" method="GET">
    	 <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		   <div class="criteria-div">
		   	  <span class="item">
			     	广告名称：<input type="text" class="${inputclass}" maxlength="50" name="name" value="${search.name }" placeholder="请输入广告名"/>
			  </span>
			  <span class="item">
			    	 状态：
			       	<select name="status" class="criteria-select">
		            	<option value="" <c:if test="${empty search.status}">selected="selected"</c:if>>全部</option>
		            	<option value="0" <c:if test="${search.status eq 0}">selected="selected"</c:if>>下线</option>
		            	<option value="1" <c:if test="${search.status eq 1}">selected="selected"</c:if>>上线</option>
		            </select>
			     <input type="button" onclick="search()" id="searchBtn" value="搜索" class="${btnclass}" />
			     <input type="button" id="addBtn" value="创建广告" class="${btnclass}" />
			  </span>
			</div>
    </form:form>
    
    <!-- 数据表格 -->
    <div align="center" class="order-content-list">
       <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
       
       <display:table name="list" requestURI="${contextPath}/admin/appStartAdv/query" id="item"
         export="false" class="${tableclass}" style="width:100%" sort="external">
         
	      <display:column title="顺序" style="width:50px;"><%=offset++%></display:column>
		  <display:column title="广告名称" style="width:150px;">${item.name}</display:column>
		  <display:column title="URL" style="width:200px;" ><div class="url">${item.url}</div></display:column>
		  <display:column title="广告图" style="min-width:90px;"><img src="<ls:photo item='${item.imgUrl}'/>" width="90" height="160" data-action="zoom" /></display:column>
		  <display:column title="描述" style="width:200px;">${item.description}</display:column>
		  <display:column title="状态" style="width:100px;">
		  	<c:choose>
	      		<c:when test="${item.status eq 0}"><span class="offline">下线</span></c:when>
	      		<c:when test="${item.status eq 1}"><span class="online">上线</span></c:when>
	      		<c:otherwise>未知</c:otherwise>
	      	</c:choose>
		  </display:column>
		  <display:column title="创建时间" property="createTime" format="{0,date,yyyy-MM-dd HH:mm:ss}" style="width:200px;"/>
	   
		  <!-- 操作列按钮 -->   
	      <display:column title="操作" media="html" style="width: 250px;">
      		<div class="table-btn-group">
				<c:choose>
			  		<c:when test="${item.status eq 1}">
			  			<button class="tab-btn offline" itemId="${item.id}" >下线</button>
			  			<span class="btn-line">|</span>
			  		</c:when>
			  		<c:otherwise>
			  			<button class="tab-btn online" itemId="${item.id}" >上线</button>
			  			<span class="btn-line">|</span>
			  		</c:otherwise>
			  	</c:choose>
		  		<button class="tab-btn edit" itemId="${item.id}">编辑</button> 
		  		<c:if test="${item.status eq 0}">
		  			<span class="btn-line">|</span> 
		  		</c:if>
		  		<c:if test="${item.status eq 0}">
		  			<button class="tab-btn delete" itemId="${item.id}">删除</button> 
		  		</c:if>
			</div>
	      </display:column>
	 </display:table>
	 <!-- 分页条 -->
     	<div class="clearfix">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
 </div>
 
	<!-- 提示区域 -->
	<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
	    <tr>
	        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
	            1. 友情链接管理，出现在网站首页底部<br>
	            2.只有上线的广告才能在APP启动的时候显示!<br>
	            3.已上线的广告会在APP启动时随机显示!<br>
	            <a href="${contextPath}/admin/system/systemParameter/query?groupId=SHOP" target="_blank">更新广告后，要使已加载广告的用户生效，需要更新APP启动广告版本。请点击设置!</a>
	        </td>
	    <tr>
	</table>
</div>
</div>
</body>
 <!-- 图片居中放大插件 -->
 <script type="text/javascript" scr="<ls:templateResource item='/resources/common/js/transition.js'/>"></script>
 <script type="text/javascript" src="<ls:templateResource item='/resources/plugins/zoom/js/zoom.js'/>"></script>
 <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/appStartAdvList.js'/>"></script>
 <script type="text/javascript">
 	var contextPath = '${contextPath}';
	
 </script>
 <script language="JavaScript" type="text/javascript">
	 function search() {
			$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			$("#form1")[0].submit();
		}
 </script>
 </html>
