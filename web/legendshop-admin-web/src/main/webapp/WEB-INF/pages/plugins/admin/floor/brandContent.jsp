<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<ul> 
       <c:forEach items="${requestScope.brandList}" var="brand"  varStatus="status">
	   		<li>
		        <a href="#" title="${brand.brandName}" target="_blank">
		        		<img src="<ls:images item='${brand.pic}' scale='2'/>" alt="${brand.brandName}">
		        </a>
	       </li>
       </c:forEach>
 </ul>
           
<%--     <div id="brandEdit" style="width: 212px;height: 23px;top: 0px;" class="flooredit"><a href ='javascript:onclickBrand(${flId})' style="color:white;">品牌编辑</a></div>
  	<ul>
  		<c:forEach items="${requestScope.brandList}" var="brand"  varStatus="status">
	   		<li>
		        <a href="#" title="${brand.brandName}" target="_blank">
		        		<img src="<ls:images item='${brand.pic}' scale='2'/>" alt="${brand.brandName}">
		        </a>
	       </li>
       </c:forEach>
	</ul> --%>