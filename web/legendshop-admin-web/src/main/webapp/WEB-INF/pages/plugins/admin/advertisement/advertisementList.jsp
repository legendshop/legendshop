<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>广告栏管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/advertisement/query"
				id="form1" method="post">
				<table class="${tableclass}" style="width: 100%">
					<thead>
						<tr>
							<th><strong class="am-text-primary am-text-lg">商城管理</strong>/ 广告管理</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<div align="left" style="padding: 3px">
									<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
									<input type="button" class="${btnclass}" value="创建广告" onclick='window.location="${contextPath}/admin/advertisement/load"' />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<c:if test="${not empty list}">
					<display:table name="list" requestURI="/admin/advertisement/query" id="item" export="false" class="${tableclass}" style="width:100%">
						<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
						<display:column title="类型" sortable="true">
							<ls:optionGroup type="label" required="true" cache="true" beanName="ADVERTISEMENT_TYPE" selectedValue="${item.type}" defaultDisp="" />
						</display:column>
						<display:column title="链接地址">
							<a href="${item.linkUrl}" target="_blank">${item.linkUrl }</a>
						</display:column>
						<display:column title="标题" property="title" sortable="true"></display:column>
						<display:column title="图片">
							<a href="<ls:photo item='${item.picUrl}'/>" target="_blank"><img
								src="${contextPath}/resources/common/images/filter_search.png"></a>
							</a>
						</display:column>
						<display:column title="操作" media="html" style="width: 250px">
							<div class="am-btn-toolbar">
								<div class="am-btn-group am-btn-group-xs">
									<c:choose>
										<c:when test="${item.enabled == 1}">
											<button
												class="am-btn am-btn-default am-btn-xs am-hide-sm-only"
												name="statusImg" itemId="${item.id}"
												itemName="${item.title}" status="${item.enabled}"
												style="color: #f37b1d;">
												<span class="am-icon-arrow-down"></span>下线
											</button>
										</c:when>
										<c:otherwise>
											<button
												class="am-btn am-btn-default am-btn-xs am-hide-sm-only"
												name="statusImg" itemId="${item.id}"
												itemName="${item.title}" status="${item.enabled}"
												style='color: #0e90d2;'>
												<span class="am-icon-arrow-up"></span>上线
											</button>
										</c:otherwise>
									</c:choose>
									<button
										class="am-btn am-btn-default am-btn-xs am-text-secondary"
										onclick="window.location='${contextPath}/admin/advertisement/load/${item.id}'">
										<span class="am-icon-pencil-square-o"></span> 编辑
									</button>
									<button
										class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only"
										onclick="deleteById('${item.id}');">
										<span class="am-icon-trash-o"></span> 删除
									</button>
								</div>
							</div>

						</display:column>
					</display:table>
				</c:if>
				<c:if test="${not empty toolBar}">
					<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
				</c:if>
			</div>
			<table class="${tableclass}">
				<tr>
					<td align="left">说明：<br> 1. 设置首页、商品、商品详细信息页面的广告位<br>
						2. 广告处于上线状态页面可见，处于下线状态页面不可见<br>
					</td>
				<tr>
			</table>

		</div>
	</div>
</body>
</html>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/advertisementList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
var contextPath="${contextPath}";
</script>


