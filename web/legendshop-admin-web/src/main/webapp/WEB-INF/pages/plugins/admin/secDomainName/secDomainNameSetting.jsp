<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>二级域名设置- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <table class="${tableclass}" style="width: 100%">
    	<thead>
	    	<tr>
	    		<th><strong class="am-text-primary am-text-lg">商场管理</strong> /  二级域名</th>
	    	</tr>
    	</thead>
   	</table>
   	
    <div style="margin-left: 0.5rem">
		<div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
          	 <li><i></i><a href="<ls:url address='/admin/secDomainName/query'/>">域名列表</a></li>
         	 <li class="am-active" ><i></i><a href="<ls:url address='/admin/secDomainName/setting/'/>">设置</a></li>
         </ul>
        </div>
   </div>
   
   
   <form:form action="${contextPath}/admin/secDomainName/settingUpdate" method="post" id="formId">
      <div align="center">
         <table border="0" align="center" class="${tableclass}" id="col1"  style="width: 100%">
         <thead>
            <tr>
            	<td width="250">
		          	<div align="right">是否启用二级域名 :</div>
		       	</td>
            	<td>
            		<input type="radio" name="isEnable"  value="true" <c:if test="${secDomainNameSetting.isEnable eq true}">checked="checked"</c:if>/>&nbsp;是
            		<input type="radio" name="isEnable" value="false" <c:if test="${secDomainNameSetting.isEnable eq false || empty secDomainNameSetting.isEnable}">checked="checked"</c:if>/>&nbsp;否
            		</br>
            		<span style="font-size:12px;color:#aaa;">启用二级域名需要您的服务器支持泛域名解析</span>
            	</td>
            </tr>
			<tr>
		        <td width="250">
		          	<div align="right">保留域名:</div>
		       	</td>
		        <td>
		           	<input type="text" id="retain" class="${inputclass}" name="retain"  value="${secDomainNameSetting.retain}"/></br>
		           	<span style="font-size:12px;color:#aaa;">保留的二级域名，多个保留域名之间请用","隔开</span>
		        </td>
			</tr>
			<tr>
		        <td>
		          	<div align="right">长度限制:</div>
		       	</td>
		        <td>
		           	<input type="text"  id="sizeRange" name="sizeRange" class="${inputclass}" value="${secDomainNameSetting.sizeRange}"/></br>
		        	<span style="font-size:12px;color:#aaa;">如"3-12"，代表注册的域名长度限制在3到12个字符之间</span>
		        </td>
			</tr>
			<tr>
		        <td>
		          	<div align="right">修改周期:</div>
		       	</td>
		        <td>
		           	<input type="text"  id="modifyCycle" name="modifyCycle" class="${inputclass}" value="${secDomainNameSetting.modifyCycle}"/></br>
		        	<span style="font-size:12px;color:#aaa;">如"7"，代表修改周期为7天,也就是说从注册时间起7天后才允许卖家修改!</span>
		        </td>
			</tr>
            <tr>
            	<td>
                </td>
                <td>
                	 <div align="left">
                        <input class="${btnclass}" type="submit" value="确认提交" />
                    </div>
                </td>
            </tr>
            </thead>
          </table>
       </div>
     </form:form>
     </div>
     </div>
     </body>
     <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
    <script type="text/javascript">
    	/* $("#formId").submit(function(){
    		 var isEnable = $("#formId :radio").val();
    		 var sizeRange = $("#sizeRange").val();
    		
    		if(isEnable == ""){
    			  alert("必须要选择'是'或'否'!");
    		 	  return false;
    		}
    		if(sizeRange == ""){
    			  alert("长度限制不能为空!");
    		 	  return false;
    		}
    		
    		return true;
    	}); */
    	
    $.validator.setDefaults({});

    $(document).ready(function() {
	    jQuery("#formId").validate({
	        rules: {
	        	isEnable: {
	                required: true,
	            },
	        	sizeRange: {
	                required: true,
	            }
	        },
	        messages: {
	        	isEnable: {
	                required: '<fmt:message key="secDomainNameSetting.isEnable.required"/>',
	            },
	        	sizeRange: {
	                required: '<fmt:message key="secDomainNameSetting.sizeRange.required"/>',
	            }
	        }
	    }); 
	});
    </script>
</html>
