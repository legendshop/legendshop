<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家拼团活动</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/discoverArticle/mergeGroupEdit.css'/>" rel="stylesheet"/>
    <%-- <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addShipping.css'/>" rel="stylesheet"/> --%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/discoverArticle/addProdFull.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/discoverArticle/addFull.css'/>" rel="stylesheet"/>
    <script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/plugins/default/layer.css" />
</head>
<body>
	<div class="w1190">
		<div class="add-fight">
			<form id="prodShipping">
					<div class="prodContext">
						<div class="add-tit" style="padding-bottom: 10px;border-bottom: 1px solid #f9f9f9;"><span>选择关联商品</span></div>
						
						<div class="form-box">
							<div class="ant-tabs ant-tabs-top ant-tabs-line">
								<div role="tablist" class="ant-tabs-bar" tabindex="0">
									<div class="ant-tabs-nav-container">
										<div class="ant-tabs-nav-wrap">
											<div class="ant-tabs-nav-scroll">
												<div class="ant-tabs-nav ant-tabs-nav-animated">
													<div class="ant-tabs-ink-bar ant-tabs-ink-bar-animated" style="display: block; transform: translate3d(0px, 0px, 0px); width: 96px;"></div>
													<div role="tab" aria-disabled="false" aria-selected="true" data-type="select" class="ant-tabs-tab-active ant-tabs-tab">选择商品</div>
													<div role="tab" aria-disabled="false" aria-selected="false" data-type="setting" class=" ant-tabs-tab">已选商品</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="ant-tabs-content ant-tabs-content-animated" style="margin-left: 0%;">
									<div role="tabpanel" aria-hidden="true" class="all-prod ant-tabs-tabpane ant-tabs-tabpane-active all">
									</div>
									
									<div role="tabpanel" aria-hidden="false" class="join-prod ant-tabs-tabpane ant-tabs-tabpane-active">
										
										<div class=" clearfix">
											<div class="ant-spin-nested-loading">
												<div class="ant-spin-container">
													<div class="ant-table ant-table-large ant-table-without-column-header ant-table-scroll-position-left">
														<div class="ant-table-content">
															<div class="ant-table-body">
																<table class="">
																	<colgroup>
																		<col>
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 150px; min-width: 150px;">
																		<col style="width: 100px; min-width: 100px;">
																	</colgroup>
																	<thead class="ant-table-thead">
																		<tr>
																			<th class="ant-table-selection-column">
																			</th>
																			<th class=""><span>商品</span></th>
																			<th class=""><span>价格(元)</span></th>
																			<th class=""><span>库存</span></th>
																			<th class=""><span>操作</span></th>
																		</tr>
																	</thead>
																	<tbody class="ant-table-tbody selected">
																	
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	            		</div>
	           	 </div>
			</form> 
		</div>
	</div>
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/amaze/js/discoverArticle/addProdShipping.js'/>"></script>
<script type="text/javascript">	
$(document).ready(function() {
	laydate.render({
		   elem: '#startDate',
		   calendar: true,
		   theme: 'grid',
		   type:'date',
		   min:'-1',
		   trigger: 'click'
	  });

	laydate.render({
		   elem: '#endDate',
		   calendar: true,
		   theme: 'grid',
		   type:'date',
		   min:'-1',
		   trigger: 'click'
	  });
});
var contextPath = "${contextPath}";
$(function() {
	$(".all-prod").load(contextPath + "/admin/discoverArticle/fullProd");
	//定义prodId数组
	prodIdList = new Array;
	//定义参加数组
	joinProdIdList = new Array;
});

</script>
</html>