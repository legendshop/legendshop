<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file='/WEB-INF/pages/common/layui.jsp'%>
<head>
<c:set var="_userDetail"
	value="${shopApi:getSecurityUserDetail(pageContext.request)}"></c:set>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}-后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<link
	href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form method="post"
				enctype="multipart/form-data"
				action="${contextPath}/admin/discoverArticle/save" id="form1"
				onsubmit="return checkFinish();">
				<table class="${tableclass}" style="width: 100%">
					<thead>
						<tr>
							<th class="no-bg title-th"><span class="title-span">
									新闻管理 ＞ <a
									href="<ls:url address="/admin/discoverArticle/query"/>">发现文章管理</a>
									<c:if test="${not empty discoverArticle}">  ＞  <span
											style="color: #0e90d2;">文章编辑</span>
									</c:if> <c:if test="${empty discoverArticle}">  ＞  <span
											style="color: #0e90d2;">文章新增</span>
									</c:if>
							</span></th>
						</tr>
					</thead>
				</table>
				<table style="width: 10%;"
					class="${tableclass} no-border content-table" id="col1">

					<tr class="left">
						<td align="right"><font color="FF0000">*</font>文章标题：</td>
						<td align="left" style="line-height: 28px; padding-left: 2px;">
							<input type="text" id="name" name="name"
							value="${discoverArticle.name}" style="width: 290px;"
							class="text text-short" style="margin-left: 0px;">
						</td>
					</tr>
					<tr class="left">
						<td align="right">文章简介：</td>
						<td align="left" style="line-height: 28px; padding-left: 2px;">
							<input type="text" id="intro" name="intro"
							value="${discoverArticle.intro}" style="width: 290px;"
							class="text text-short" style="margin-left: 0px;">
						</td>
					</tr>
					<tr class="left">
						<td align="right"><font color="FF0000">*</font>文章作者：</td>
						<td align="left" style="line-height: 28px; padding-left: 2px;">
							<input type="text" id="writerName" name="writerName"
							value="${discoverArticle.writerName}" style="width: 290px;"
							class="text text-short" style="margin-left: 0px;">
						</td>
					</tr>
					<tr class="left">
						<td align="right">状态：</td>
						<td align="left" style="line-height: 28px; padding-left: 2px;">
							<select id="status" name="status">
								<ls:optionGroup type="select" required="true" cache="true"
									beanName="ONOFF_STATUS"
									selectedValue="${discoverArticle.status}" />
						</select> <%--&nbsp;高亮--%> <%--<select id="highLine" name="highLine">--%> <%--<ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${news.highLine == null ? 0 : news.highLine}"/>--%>
							<%--</select>--%>
						</td>
					</tr>
					<tr class="left">
						<td align="right"><font color="FF0000">*</font>文章图片：</td>
						<td align="left" style="padding-left: 2px;">
						<c:if test="${!empty discoverArticle.image}">
							<div style="margin-bottom: 20px">
								<img src="<ls:photo item='${discoverArticle.image}'/>" height="100px" />
							</div>
						</c:if>
						<input type="file" name="picFile" id="picFile" size="40"  class=input value="${discoverArticle.image}" />
						<input type="hidden"  id="picFilePath" size="40"  class=input value="${discoverArticle.image}" />
						<p style="float: left;display: contents;">建议尺寸：240*150px</p>
						<input type="hidden" id="id" name="id" value="${discoverArticle.id}" />
						</td>
						
					</tr>
					<tr class="left">
						<td align="right">文章相关商品：</td>
						<td align="left" style="padding-left: 2px;"><input
							type="button" name="pro" id="pro" size="40" maxlength="50"
							class=${btnclass } value="选择商品" /></td>
					</tr>
					<tr  class="left" id="prodtable">
							<table  style="width: 50%; margin-left: 280px" class="layui-table" class="prodList" lay-size="sm" id="products">
								<!-- <colgroup>
									<col width="200">
									<col width="300">
									<col>
								</colgroup> -->
								<thead>
									<tr>
										<th style="windth:200px">商品名称</th>
										<th style="windth:200px">商品图片</th>
										<th style="windth:100px">商品价格</th>
										<th style="windth:50px">操作</th>
									</tr>
								</thead>
								<tbody id="prodList">
								<c:choose>
								<c:when test="${not empty products}">
									<c:forEach items="${products}" var="prod">
										<tr id="list_${prod.prodId}">
											<td>${prod.name}</td>
											<td><img src="<ls:photo item='${prod.pic}'/>" height="60px" /></td>
											<td>${prod.price}</td>
											<td>
											<input type="hidden" value="${prod.prodId}" name="prods"/>
											<input type="button" value="移除"
												class="criteria-btn removeProd" /></td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
										<tr>
											<td colspan="4">暂未选择商品</td>
										</tr>
								</c:otherwise>
								</c:choose>
								</tbody>
							</table>
					</tr>
					<div>
					
					</div>
					<tr>
						<td valign="top">
							<div class="neirong">内容：</div>
						</td>
						<td colspan="2" style="padding-left: 2px;">
							<div class="ke-container ke-container-default"
								style="width: 100%;"></div> <textarea name="content"
								id="content" cols="100" rows="8"
								style="width: 700px; height: 200px; visibility: hidden; display: none;">${discoverArticle.content}</textarea>
						</td>
					</tr>


				</table>
				<div style="text-align: center; margin:30px 0px 10px -280px">
					<input type="submit" value="保存" class="${btnclass}" /> <input
						type="button" value="返回" class="${btnclass}"
						onclick="window.location='${contextPath}/admin/discoverArticle/query/${position}'" />
				</div>

			</form:form>
		</div>
	</div>
</body>

<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script
	src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"
	type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var UUID = "${cookie.SESSION.value}";
	var tagData = [];
	<c:forEach items="${tags}" var="item">
	tagData.push({
		id : '${item.itemId}',
		text : '${item.itemName}'
	});
	</c:forEach>

	//文章位置
	var positiontagData = [];
	<c:forEach items="${positionItem}" var="item">
	positiontagData.push({
		id : '${item.positionId}',
		text : '${item.positionName}'
	});
	</c:forEach>

	$(function() {
	
		$("#pro").click(function() {
			layer.open({
				title : "文章相关商品",
				type : 2,
				id : "sasa",
				content : [ '/admin/discoverArticle/discoverArticleProd' ],
				area : [ '1000px', '800px' ]
			});
		})

		$("#prodList").on("click",".removeProd",function() {
			$(this).parents("tr").remove();
			if ($("#prodList").find("tr").size() == 0) {
				$("#prodList").html("<tr><td colspan='4'>暂未选择商品</td></tr>");
			}
		})
	})
	window.onload=function(){
		if($("#prodList").html()!=null&&$("#prodList").html()!=""){
		$(".ke-container.ke-container-default").css({"width":"50%","margin-left":"280px"});	
		$(".neirong").css({"margin-left":"250px"});	
		}
	}
	function findProduct(ids) {
		var url=contextPath+"/admin/discoverArticle/findByprodId?ids="+ids;
		$.post(url,function(data){
			$("#prodList").html(data);
			layer.closeAll();
			layui.use('element', function(){
				  var element = layui.element;
				  element.render("table");
			});
		})
	}
</script>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/templets/js/admin/discoverArticle.js'/>"></script>
</html>
