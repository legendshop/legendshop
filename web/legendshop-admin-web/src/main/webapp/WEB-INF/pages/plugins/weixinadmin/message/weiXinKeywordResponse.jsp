<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
         <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
      <table class="${tableclass}" style="width: 100%">
	    <thead>
	    	<tr><th>
				    	<strong class="am-text-primary am-text-lg">微信消息管理</strong> /  创建关键词管理
	    	</th></tr>
	    </thead>
	    </table>
        <form:form  action="${contextPath}/admin/weiXinKeywordResponse/save" method="post" id="form1">
            <input id="id" name="id" value="${weiXinKeywordResponse.id}" type="hidden">
            <input id="templatename" name="templatename" value="${weiXinKeywordResponse.templatename}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                                                                   创建关键词管理
                            </div>
                        </th>
                    </tr>
                </thead>
		<tr>
		<tr width="30%">
				<td>
					<div align="center">消息关键字:<font color="ff0000">*</font> </div>
				</td>
				<td>
				<input type="text" name="keyword" id="keyword" maxlength="30" value="${weiXinKeywordResponse.keyword}" size="30" />
			  </td>
	</tr>
		
		<td width="30%"><div align="center">匹配类型: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           <select id="pptype" name="pptype" class="span2">
					<option value="1">模糊匹配</option>
					<option value="2">完全匹配</option>
				  </select>
		        </td>
		</tr>
		
		  <tr id="xxtr" >
				        <td>
				          	<div align="center">消息类型: <font color="ff0000">*</font></div>
				       	</td>
				        <td>
				             <select   style="width: 150px;" id="msgType" name="msgtype">
					            <option value="">-----------------</option>
					          	<option value="text" <c:if test="${weiXinKeywordResponse.msgtype=='text'}"> selected="selected"</c:if> >文本消息</option>
					          	<option value="news" <c:if test="${weiXinKeywordResponse.msgtype=='news'}"> selected="selected"</c:if> >图文消息</option>
					       		<!-- <option value="voice">音频消息</option>
					       		<option value="video">视频消息</option> -->
					       		<%-- <option value="image" <c:if test="${weiXinKeywordResponse.msgtype=='image'}"> selected="selected"</c:if> >图片消息</option> --%>
			                </select>
				        </td>
			</tr>
	
		  <tr id="text_content" >
				        <td>
				          	<div align="center">消息内容:<font color="ff0000">*</font> </div>
				       	</td>
				        <td>
				           <textarea rows="2" cols="5" style="width:60%;height: 250px;" name="content" id="content" maxlength="250">${weiXinKeywordResponse.content}</textarea>
				        </td>
	       </tr>
				
           <tr id="text_template" style="display: none;">
				<td>
					<div align="center">选择模板:<font color="ff0000">*</font> </div>
				</td>
				<td>
				  <select id="templateId" name="templateid" style="width: 150px;">
				     <option value="">---------------------</option>
				  </select> 
			  </td>
			</tr>
		
              <tr>
                     <td colspan="2">
                        <div align="center">
                            <input class="${btnclass}" type="submit" value="保存" />
                            
							 <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/weiXinKeywordResponse/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        <table class="am-table am-table-striped am-table-hover table-main">
		<tbody>
			<tr>
				<td align="left">说明：<br> 
				     1.匹配类型:完全匹配是指回复内容与关键字相同才能触发,模糊匹配只要包含关键词字符就会触发,两种匹配均区分大小写.<br> 
				     2.如需在内容中添加超链接请添加此段代码,只要将网址和链接文字替换成您自己的即可。<br>
				     3.在微信公众平台设置关键词自动回复，可以通过添加规则（规则名最多为60字数），订阅用户发送的消息内如果有您设置的关键字
				</td>
			</tr>
			<tr>
			</tr>
		</tbody>
	</table>
        <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>

        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
       jQuery("#form1").validate({
            rules: {
            keyword:{
             required: true
            },
            msgtype: {
                required: true
            },
            templateid:{
               required: true
            },
            content:{
               required: true
            }
        },
        messages: {
            keyword:{
             required: "请输入关键字"
            },
            msgtype: {
                required: "请选择消息类型"
            },
            templateid:{
               required: "请选择消息消息模板"
            },
            content:{
                required: "请输入文本内容"
            }
        },
        submitHandler:function(form){
           var content=$.trim($("textarea[name='content']").val());
            var selectValue = $(this).children('option:selected').val();
           if( (content=="" || content==null) && selectValue=="text" ){
              alert("请输入文本内容");
              return false;
           }
            form.submit();
        }
      });
      
       var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name="content"]', {
					resizeType : 1,
					allowPreviewEmoticons : false,
					allowImageUpload : false,
					items : ['link'],
					afterBlur:function(){
						this.sync();
					},
				});
		  });
		  
		 var templateId = "${weiXinKeywordResponse.templateid}";
       var msgType = "${weiXinKeywordResponse.msgtype}";
       
       	if("text"==msgType){
	           	   $("#text_content").show();
	               $("#text_template").attr("style","display:none");
	 			   $("#templateId").attr("disabled","disabled");
	      }else{
	               $("#text_template").show("style");
	 			   $("#templateId").removeAttr("disabled");
	               $("#text_content").hide();
	     }
       
       if(templateId!=null && templateId!="" && templateId!=undefined && msgType!="text"){
 	   		var templateObj = $("#templateId");
	 		templateObj.html("");
	 		$.ajax({
	 			url:"${contextPath}/admin/weixinMenu/getTemplates",
	 			data:{"msgType":msgType},
	 			dataType:"JSON",
	 			type:"POST",
	 			success:function(result){
	 			var msg="";
 				msg += "<option value=''>--------------------</option>";
 				var data=eval(result);
	 				for(var i=0;i<data.length;i++){
	 				    if(templateId == data[i].key){
	 				 	    msg += "<option value='"+data[i].key+"' selected='selected' >"+data[i].value+"</option>";
	 				 	}else{
	 				 	   msg += "<option value='"+data[i].key+"'  >"+data[i].value+"</option>";
	 				 	}
	 				}
 				  templateObj.html(msg);
	 			}
	 		});
	    }
	    
		  
		 $("#msgType").change(function(){
 			 var selectValue = $(this).children('option:selected').val();
 			 	if("text"==selectValue){
	           	   $("#text_content").show();
	               $("#text_template").attr("style","display:none");
	 			   $("#templateId").attr("disabled","disabled");
	 			   $("#templatename").val("");
	           }else{
	               $("#text_template").removeAttr("style");
	 			   $("#templateId").removeAttr("disabled");
	               $("#text_content").hide();
	               $("#templatename").val($.trim($(this).children('option:selected').html()));
 			       getTemplates(selectValue);
	           }
 		});
 		
	
 		highlightTableRows("col1");
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
    });
    
    function getTemplates(msgType){
 		var templateObj = $("#templateId");
 		templateObj.html("");
 		$.ajax({
 			url:"${contextPath}/admin/weixinMenu/getTemplates",
 			data:{"msgType":msgType},
 			dataType:"JSON",
 			type:"POST",
 			success:function(result){
 				var msg="";
 				msg += "<option value=''>--------------------</option>";
 				var data=eval(result);
 				for(var i=0;i<data.length;i++){
 					msg += "<option value='"+data[i].key+"'>"+data[i].value+"</option>";
 				}
 				templateObj.html(msg);
 			}
 		});
 	}
 	
</script>

