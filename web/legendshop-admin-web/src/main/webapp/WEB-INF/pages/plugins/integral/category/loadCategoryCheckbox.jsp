<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript">
	function saveCategoryType(){
			var zTree = $.fn.zTree.getZTreeObj("catTrees");
			var chkNodes = zTree.getCheckedNodes(true);
				var catIdStr = "";
				var catNameStr = "";
				for(var i=0;i<chkNodes.length;i++){
					if(i>0){
						catIdStr+=",";
						catNameStr+="、";
					}
					catIdStr+=chkNodes[i].id;
					catNameStr+=chkNodes[i].name;
				}
				var msgCon = "确定将【${typeName}】关联到以下分类吗?<br>"+catNameStr;
				if(chkNodes.length<1){
					msgCon = "确定清除所有的关联吗？";
				}
				layer.confirm(msgCon, {
					 icon: 3
				     ,btn: ['确定','关闭'] //按钮
				   }, function(){
						$.ajax({
		    				url:"${contextPath}/admin/category/saveCategoryType", 
		    				type:'post', 
		    				data: {"catIdStr":catIdStr,"typeId":'${typeId}'},
		    				async : true, //默认为true 异步   
		    				error: function(jqXHR, textStatus, errorThrown) {
		    			 		alert("err");
		    				},
		    				success:function(result){
		    				 	layer.msg("保存成功",{icon:1});
		    				}
		    			});
				   });
	}
	
	function closeDialog(){
		layer.closeAll();
	}
	
	$(document).ready(function(){
			//zTree设置
			var setting = {
					check:{
							enable: true,
							chkboxType: {'Y':'','N':''},
							chkStyle: "checkbox"
							}, 
					data:{
						simpleData:{ enable: true }
					}, callback:{
						beforeCheck: beforeCheck,
						onCheck: onCheck
					} };
			$.fn.zTree.init($("#catTrees"), setting, ${catTrees});
	});
	
		function beforeCheck(){
		}
		    
		function onCheck(){
		}
</script>
</head>
<body>
     	<fieldset style="padding-bottom: 30px; height:245px;">
			<legend>${typeName}&nbsp;关联分类</legend>
			<div id="menuContentList" style="margin:10px 0px 10px 10px;height:245px;overflow-y:auto;" > 
		   		<ul id="catTrees" class="ztree" style="height:245px;"></ul>
	     	</div>
		</fieldset>
		<div align="center">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:saveCategoryType();" value="保存" id="Button1" name="Button1">
			<input type="button" value="取消" style="margin:10px;padding:8px" onclick="closeDialog();">
		</div>
</body>

</html>

