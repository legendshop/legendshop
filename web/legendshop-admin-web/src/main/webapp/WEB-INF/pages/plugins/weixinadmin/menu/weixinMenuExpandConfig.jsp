<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
<table class="${tableclass}" style="width: 100%">
    <thead>
    <tr>
        <th>
            <strong class="am-text-primary am-text-lg">微信扩展接口管理</strong> / 创建扩展接口
        </th>
    </tr>
    </thead>
</table>
<form:form action="${contextPath}/admin/weixin/expandConfig/save" method="post" id="form1">
    <input id="id" name="id" value="${config.id}" type="hidden">
    <div align="center">
        <table border="0" align="center" class="${tableclass}" id="col1">
            <thead>
            <tr class="sortable">
                <th colspan="2">
                    <div align="center">
                        创建扩展接口
                    </div>
                </th>
            </tr>
            </thead>
            <tr>
                <td width="30%">
                    <div align="center">名称: <font color="ff0000">*</font></div>
                </td>
                <td>
                    <input type="text" name="name" id="name" value="${config.name}" maxlength="40"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="center">消息关键字: <font color="ff0000">*</font></div>
                </td>
                <td>
                    <input type="text" name="keyword" id="keyword" value="${config.keyword}"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="center">服务service: <font color="ff0000">*</font></div>
                </td>
                <td>
                    <textarea rows="2" cols="3" style="width:60%;height:40px;" name="classService" id="classService" maxlength="250">${config.classService}</textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div align="center">
                        <input class="${btnclass}" type="submit" value="添加"/>
                        <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/weixin/expandConfig/query"/>'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
<script language="javascript">
    $.validator.setDefaults({});

    $(document).ready(function () {
        jQuery("#form1").validate({
            rules: {
                name: {
                    required: true
                },
                keyword: {
                    required: true
                },
                classService: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "请输入名称"
                },
                keyword: {
                    required: "请输入消息关键字"
                },
                classService: {
                    required: "请输入服务service"
                }
            },
            submitHandler: function (form) {  //通过之后回调
                //form.submit();提交表单，如果不写，即便通过表单也不会自动提交
                jQuery.ajax({
                    url: "${contextPath}/admin/weixin/expandConfig/save",
                    type: 'post',
                    data: {"id": $("#id").val(), "name": $("#name").val(), "keyword": $("#keyword").val(), "classService": $("#classService").val()},
                    async: false, //默认为true 异步  
                    dataType: 'json',
                    success: function (result) {
                        if (result == "OK") {
                            alert("操作成功！");
                            window.location = "${contextPath}/admin/weixin/expandConfig/query";
                            return true;
                        } else if (result == "fail") {
                            alert("创建失败");
                            return false;
                        } else {
                            alert(result);
                            return false;
                        }
                    }
                });
            }
        });

        highlightTableRows("col1");
        //斑马条纹
        $("#col1 tr:nth-child(even)").addClass("even");
    });
</script>

