<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>链接管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<table class="${tableclass} title-border" style="width: 100%">
        <tr>
            <th class="title-border">PC首页装修&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">友情链接管理</span></th>
        </tr>
    </table>
	<form:form action="${contextPath}/admin/externallink/query" id="form1" method="get">
    <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
    <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
         <div class="criteria-div">
         	<span class="item">
	             <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
	             <input type="button" class="${btnclass}" value="创建友情链接" onclick='window.location="${contextPath}/admin/externallink/load"'/>
            </span>
         </div>
</form:form>
<div align="center" class="order-content-list">

    <display:table name="list" requestURI="/admin/externallink/query" id="item"
                   export="false" class="${tableclass}" style="width:100%" sort="external">
        <display:column title="顺序" class="orderwidth"><%=offset++%>
        </display:column>
        <%-- <display:column title="图片">
            <c:if test="${item.picture != ''}">
                <img src="<ls:photo item='${item.picture}'/>" height="31px" width="81px"/>
            </c:if>
        </display:column> --%>
        <display:column title="链接地址"><a href="${item.url}" target="_blank">${item.url}</a></display:column>
        <display:column title="链接文字说明" property="wordlink"></display:column>
        <display:column title="描述" property="content"></display:column>
        <display:column title="次序" property="bs" sortable="true" sortName="bs"></display:column>
        <display:column title="操作" media="html" class="actionwidth" style="width: 150px;">
            <%-- <div class="am-btn-toolbar">
                <div class="am-btn-group am-btn-group-xs">
                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/externallink/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 编辑</button>

                    <button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}');"><span class="am-icon-trash-o"></span> 删除</button>
                </div>
            </div> --%>
             <div class="table-btn-group">
                 <button class="tab-btn" onclick="window.location='${contextPath}/admin/externallink/load/${item.id}'">编辑</button>
				 <span class="btn-line">|</span>
                 <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
             </div>
        </display:column>
    </display:table>
    <div class="clearfix">
		<div class="fr">
			 <div class="page">
				 <div class="p-wrap">
	    		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	 			</div>
			 </div>
		</div>
	</div>
</div>
	<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
	    <tr>
	        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
	            1. 友情链接管理，出现在网站首页底部<br>
	        </td>
	    <tr>
	</table>
</div>
</div>
</body>
<script type="text/javascript">

    function deleteById(id) {
    	layer.confirm('确定删除？', {icon:3, title:'提示'}, function(){
    		window.location = "${contextPath}/admin/externallink/delete/" + id;
    	})
    }

    function pager(curPageNO) {
        document.getElementById("curPageNO").value = curPageNO;
        document.getElementById("form1").submit();
    }

    highlightTableRows("item");
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
    //-->
</script>
</html>

