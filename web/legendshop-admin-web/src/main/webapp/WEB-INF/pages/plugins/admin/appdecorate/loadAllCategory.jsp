<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<html>
<head>
<%-- <%@ include file="/WEB-INF/pages/common/jquery.jsp"%> --%>
	<style type="text/css">
		.criteria-btn {
		    color: #ffffff;
		    border: 1px solid #0e90d2;
		    background-color: #0e90d2;
		    border-color: #0e90d2;
		    cursor: pointer;
		    height: 28px;
		    padding-left: 12px;
		    padding-right: 12px;
		    text-align: center;
		    line-height: 24px;
		    margin: 0 3px;
		    font-size: 12px;
		}
		.edit-gray-btn {
		    color: #333;
		    border: 1px solid #efefef;
		    background-color: #f9f9f9;
		    cursor: pointer;
		    height: 28px;
		    text-align: center;
		    line-height: 24px;
		    margin: 0 3px;
		    font-size: 12px;
		    padding: 0 12px;
		    min-width: 50px;
			display: inline-block;
		}
	</style>
</head> 
<body>
		<div  style="margin:10px 0px 10px 10px;height:300px;overflow-y:auto;" > 
	   		<ul id="catTrees" class="ztree"></ul>
     	</div>
		<div align="center">
			<input type="button" class="criteria-btn" onclick="javascript:selectCategory();" value="确定" id="Button1" name="Button1">
			<input type="button" class="edit-gray-btn" onclick="javascript:clearCategory();" value="取消">
		</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<%-- <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.js'/>"></script> --%>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/loadAllCategory.js'/>"></script>
<script language="JavaScript" type="text/javascript">
var contextPath="${contextPath}";
var sourceId="${sourceId}";
var _catTrees ='${catTrees}';
</script>
</body>

</html>

