<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>库存管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
	.stockAdmin{
		line-height:80px; 
		height:80px;
	}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <form:form  action="${contextPath}/admin/stock/stockcontrol" id="form1" method="post">
    <table class="${tableclass}" style="width: 100%">
    <thead>
    	<tr><th> <strong class="am-text-primary am-text-lg">库存管理</strong> /  库存管理</th></tr>
    </thead>
      <tbody><tr><td>
		 <div align="left" style="padding: 3px">
 		  <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
          	商品名称&nbsp; <input type="text" name="productName" maxlength="50" class="${inputclass}"  value="${searchProName}"/>
          <input type="submit" value="搜索" class="${btnclass}"/>
 		 </div>
 	  </td></tr></tbody>
    </table>
       
    </form:form>
      <div align="center">

    <display:table name="list"  id="item" 
         export="false" class="${tableclass}" sort="external">
      <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
      <display:column title="商品">
      	<c:if test="${item.skuPicture != null}">
      	<img src="<ls:photo item='${item.skuPicture}'/>" height="81px" width="81px"/>
      	</c:if>
      </display:column>
      <display:column title="商品名称" ><span class="stockAdmin">${item.productName}</span></display:column>
      <display:column title="属性"><span class="stockAdmin">${item.cnProperties}</span></display:column>
      <display:column title="商家"><a href="${item.shopId}" target="_blank"><span class="stockAdmin">商家详情</span></a></display:column>
      <display:column title="库存"><span class="stockAdmin">${item.stocks}</span></display:column>
      <display:column title="实际库存"><span class="stockAdmin">${item.actualStocks}</span></display:column>
	  <display:column title="状态">
	  	<span class="stockAdmin">
	  	<c:if test="${item.status eq 1}">
      		上线
      	</c:if>
      	<c:if test="${item.status eq 0}">
      		下线
      	</c:if>
      	</span>
	  </display:column>  
	     <display:column title="操作" media="html" class="actionwidth" style="width: 150px;">
	     
	  		 <div class="am-btn-toolbar" style="padding: 20% 0;"> 
               <div class="am-btn-group am-btn-group-xs">
                   <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="addStock(${item.skuId})"><span class="am-icon-pencil-square-o"></span> 补货</button>
              </div>
        	</div>
		  </display:column>
    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
  </div>
     <table class="${tableclass}">
     	<tr>
     		<td align="left">说明：<br>
			   1. 友情链接管理，将会出现在商城首页<br>
			   2. 可以为每个友情链接增加一个图片<br>
   			</td>
   	   <tr>
   </table> 
   </div>
   </div>
   </body>
 <script type="text/javascript">
    function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("form1").submit();
    }
    
    //补货弹出窗
    function addStock(skuId){
    	art.dialog.open("${contextPath}/admin/stock/edit?skuId="+skuId,{
        	width: 650,
        	height: 400,
	    	drag: true,
	    	resize: true,
	    	fixed: true,
        	title: '补货',
        	lock: true,
        	close:function(){
        		var win = art.dialog.open.origin;
				win.location.reload();
        	}
        	});
      }
        
    highlightTableRows("item");  
</script>
</html>

