<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table border="0" align="center" class="${tableclass} title-border" id="col1" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userGrade/query"/>" style="color: #0e90d2;">会员等级</a>
             			&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">编辑会员等级</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/system/userGrade/save"
				method="post" id="form1">
				<input id="gradeId" name="gradeId" value="${userGrade.gradeId}"
					type="hidden">
				<div align="center" style="margin-top: 10px;">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%">
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" name="name" id="name"value="${userGrade.name}" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>自动升级：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" name="score" id="score" value="${userGrade.score}" style="width: 50px" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>描述：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"> 
								<input type="text" name="memo" id="memo" value="${userGrade.memo}" style="width: 500px" />
							</td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" /> 
								    <input class="${btnclass}" type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/system/userGrade/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				name : {
					required : true,
					maxlength:20
				},
				score : {
					required : true,
					number : true,
					maxlength:10
				},
				memo : {
					maxlength:100
				}
			},
			messages : {
				name : {
					required:"请输入名字",
				},
				score : {
					required : "请输入自动升级",
					number : "请输入数字"
				}
			}
		});

		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>
