<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
   
   <jsp:include page="/admin/top" />
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
				<table class="${tableclass} no-border" style="width: 100%">
					<thead>
						<tr>
							<th class="title-border">
								预售管理&nbsp;＞&nbsp;
								<span style="color:#0e90d2; font-size:14px;">预售广告</span>
							</th>
						</tr>
					</thead>
				</table>
				<div style="margin-left: 0.5rem">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li>
								<i></i>
								<a href="<ls:url address="/admin/presellAdvertise/index"/>">轮播图列表</a>
							</li>
							<li class="am-active">
								<i></i>
								<a href="javascript:void(0)"><c:if test="${empty activeBanner}">新增轮播图</c:if><c:if test="${not empty activeBanner}">修改轮播图</c:if></a>
							</li>
						</ul>
					</div>
				</div>
				<form:form action="${contextPath}/admin/presellAdvertise/save" method="post" id="bannerForm" enctype="multipart/form-data">
					<input type="hidden" name="id" id="id" value="${activeBanner.id}">
					<input type="hidden" name="bannerType" id="bannerType" value="PRESELL">
					<table class="${tableclass} no-border content-table" id="col1" style="width: 100%">
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="#ff0000">*</font>图片链接地址：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" name="url" id="url" size="30" value="${activeBanner.url}" class="${inputclass}" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="#ff0000">*</font>顺序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" name="seq" id="seq" size="30" value="${activeBanner.seq}" class="${inputclass}" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="#ff0000">*</font>上传图片：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="file" name=file id=file size="30" />
								<input type="hidden" id="exist_file" name="imageFile" value="${activeBanner.imageFile}" />
							</td>
						</tr>
						<c:if test="${activeBanner.imageFile != null}">
							<tr>
								<td>
									<div align="right">图片：</div>
								</td>
								<td align="left" style="padding-left: 2px;">
									<a href="<ls:photo item='${activeBanner.imageFile}'/>" target="_blank">
										<img id="uploadFile" src="<ls:photo item='${activeBanner.imageFile}'/>" height="145" width="265" />
									</a>
								</td>
							</tr>
						</c:if>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input type="submit" value="保存" class="${btnclass}" />
									<input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/presellAdvertise/index"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</form:form>
   			</div>
	 </div>
	<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
	<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
	<script type="text/javascript">
		var contextPath='${contextPath}';
		$(document).ready(function() {
			$.validator.addMethod("pic", function(value) {
				if ($("#file").val() !== "") {
					var pattern = /^.*(jpg|gif|png|jpeg|bmp)$/;
					if (pattern.test(value)) {
						return true;
					}
				} else {
					return true;
				}
			});
			$.validator.addMethod("picRequired", function(value) {
				if ($("#exist_file").val() === "") {
					if ($("#file").val() === "") {
						return false;
					}
				}
				return true;
			});
			$("#bannerForm").validate({
				rules : {
					url : {
						required : true,
						maxlength : 250
					},
					file : {
						picRequired : true,
						pic : true
					},
					seq : {
						   required: true,
						   range:[0,99999],
						   digits:true
					}
				},
				messages : {
					url : {
						required:"请输入链接地址",
						maxlength:"链接地址长度不能超过250个字符"
					},
					file : {
						picRequired : "上传文件不能为空",
						pic : "仅支持jpg、gif、png、jpeg、bmp格式"
					},
					seq : {
						required : "请输入顺序",
						range : "只能为0-99999之间的整数",
						digits : "只能为0-99999之间的整数"
					}
				}
			});
			$("#file").change(function() {
				var file = $("#file")[0].files[0];
	
				var reader = new FileReader();
				reader.readAsDataURL(file);
	
				reader.onload = function(e) {
					$("#uploadFile").attr('src', e.target.result);
				};
			});
			$("#col1 tr").each(function(i) {
				if (i > 0) {
					if (i % 2 == 0) {
						$(this).addClass('even');
					} else {
						$(this).addClass('odd');
					}
				}
			});
			$("#col1 th").addClass('sortable');
			
			$("input[type=file]").change(function(){
		    	checkImgType(this);
		    	checkImgSize(this,5120);
		    });
		});
	</script>
</body>
</html>
