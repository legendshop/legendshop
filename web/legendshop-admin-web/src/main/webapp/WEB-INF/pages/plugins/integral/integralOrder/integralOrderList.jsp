<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>积分商城 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
<style>
/* .orderlist_one {
	float: left;
	padding-bottom: 0px;
	width: 100%;
	margin-top: 15px;
}

.orderlist_one_h4 {
	background: #eeeeee none repeat scroll 0 0;
	border: 1px solid #eeeeee;
	height: 50px;
	line-height: 25px;
	margin-top: -1px;
	margin-bottom: 0px;
	padding-left: 10px;
}

.orderlist_one_h4 span {
	padding-left: 20px;
	font-size: 12px;
}

.user_list {
	margin-left: 0.5rem;
}

.order_xx {
	display: block;
	position: relative;
}

.xx_sp {
	float: left;
	text-align: left;
	width: 50px;
}

.xx_date {
	float: left;
	text-align: left;
}

.xx {
	background: #fff none repeat scroll 0 0;
	border: 1px solid #ddd;
	float: left;
	left: 210px;
	padding: 5px;
	position: absolute;
	top: -80px;
	width: 250px;
}

.xx h6 {
	color: #3399ff;
	font-size: 12px;
	height: 20px;
	line-height: 20px;
	text-align: center;
}

.xx ul {
	padding-left: 0;
}

.xx ul li {
	list-style-type: none;
	display: inline;
}

.xx ul li div {
	clear: both;
}

.Popup_shade {
	background: #000 none repeat scroll 0 0;
	display: block;
	height: 100%;
	left: 0;
	min-width: 1200px;
	opacity: 0.3;
	position: fixed;
	top: 0;
	width: 100%;
	z-index: 1094;
}
/*取消订单*/
.white_content {
	background-clip: padding-box;
	border: 8px solid rgba(0, 0, 0, 0.3);
	left: 40%;
	margin-top: -60px;
	overflow: auto;
	position: absolute;
	text-align: center;
	top: 50%;
	width: 404px;
	z-index: 2147483643;
}

.white_close {
	color: #fff;
	display: block;
	float: right;
	height: 21px;
	position: absolute;
	right: 15px;
	top: 0px;
	width: 21px;
	font-size: 28px;
	z-index: 1000000;
}

.white_box {
	background: #fff none repeat scroll 0 0;
	margin-left: auto;
	margin-right: auto;
	overflow: hidden;
	padding-bottom: 30px;
	text-align: center;
}

.white_box h1 {
	background: #0e90d2;
	border-bottom: 1px solid #ccc;
	color: #fff;
	float: left;
	font-size: 16px;
	line-height: 40px;
	margin-bottom: 20px;
	text-align: left;
	text-indent: 1em;
	width: 100%;
}

.box_table {
	border-collapse: collapse;
	font-size: 12px;
	width: 100%;
	border: 0px;
	margin: 0px;
} */
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<div class="seller_right">
				<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">订单管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">积分商品订单</span></th>
					</tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li
								<c:if test="${empty paramDto.status}"> class="this am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/integralOrder/list"/>">所有积分订单</a></li>
							<li <c:if test="${paramDto.status==0}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/integralOrder/list?status=0"/>">已经提交</a></li>
							<li <c:if test="${paramDto.status==1}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/integralOrder/list?status=1"/>">已经发货</a></li>
							<li <c:if test="${paramDto.status==2}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/integralOrder/list?status=2"/>">已经完成</a></li>
							<li <c:if test="${paramDto.status==3}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/integralOrder/list?status=3"/>">已经取消</a></li>
						</ul>
					</div>
					<form:form id="ListForm" method="post" action="${contextPath}/admin/integralOrder/list">
						<input type="hidden" value="1" id="curPageNO" name="curPageNO">
						<input type="hidden" value="${paramDto.status}" id="status" name="status">
						<div class="criteria-div">
							<span class="item">
								<input class="${inputclass}" type="text" placeholder="订单编号" id="subNumber" value="${paramDto.subNumber}"
									class="user_title_txt" name="subNumber"> 
							</span>
							<span class="item">
								<input class="${inputclass}" type="text" placeholder="买家ID" id="userName" value="${paramDto.userName}"
									class="user_title_txt" name="userName"> 
							</span>
							<span class="item">
								<input type="text" value="<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />" readonly="readonly" placeholder="下单时间(起始)"
									class="user_title_txt" id="startDate" name="startDate">
							</span>
							<span class="item">
								<input type="text" readonly="readonly" value="<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />" id="endDate" name="endDate"
									placeholder="下单时间(结束)" class="user_title_txt"> 
								<input class="${btnclass}" style="margin-left: 6px;" type="submit" value="查询">
							</span>
						</div>
					</form:form>
					<div align="left" class="order-content-list">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" class="am-table am-table-striped" >
							<thead>
								<tr>
									<th width="28%" style="text-align:left;">
										<span style="display: inline-block;margin: 0 130px 0 10px;">图片</span>
										<span style="display: inline-block;">商品名称</span>
									</th>
									<th style="text-align: center;width: 14%;">数量</th>
									<th style="text-align: center;width: 15%;">兑换积分</th>
									<th style="text-align: center;width: 14%;">买家</th>
									<th style="text-align: center;width: 14%;">总积分</th>
									<th style="text-align: center;">操作</th>
								</tr>
							</thead>
						</table>
						<c:forEach items="${requestScope.list}" var="order"
							varStatus="orderstatues">
							<div class="orderlist_one" orderId="${order.orderId}">
								<h4 class="orderlist_one_h4" style="padding-left: 20px !important;">
									<span>编号：${orderstatues.count}</span> <span>订单号：${order.orderSn}</span>
									<span>下单时间：<fmt:formatDate value="${order.addTime}" /></span> <span>订单状态：<strong
										style="color: #F00"><c:choose>
												<c:when test="${order.orderStatus==0 }">
								        已提交
								 </c:when>
												<c:when test="${order.orderStatus==1 }">
								       已发货
								 </c:when>
												<c:when test="${order.orderStatus==2 }">
								       已完成
								 </c:when>
												<c:when test="${order.orderStatus==3 }">
								      已取消
								 </c:when>
											</c:choose> </strong>
									</span>
								</h4>
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									class="user_order_table ${tableclass }">
									<tbody>
												<!-- S 商品列表 --> 
											<c:forEach items="${order.orderItemDtos}"
													var="orderItem" varStatus="orderItemStatues">
												<tr>
													<td class="vertical-dividers" style="width: 23%;text-align:left;">
														<div class="clearfix">
															<div class="fl">
																<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}" class="order_img">
																	<img src="<ls:images item="${orderItem.prodPic}" scale="3"/>">
																</a>
															</div>
															<div>
																<span class="order_img_name">
																	<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}">${orderItem.prodName}</a>
																</span>
															</div>
														</div>
													</td>	
													<td align="center" class="vertical-dividers">
														<span>${orderItem.basketCount}</span>
													</td>
													<td align="center" class="vertical-dividers">
														<span>${orderItem.exchangeIntegral}</span>
													</td>
													<c:if test="${orderItemStatues.index eq 0 }">
														<c:choose>
															<c:when test="${fn:length(order.orderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
																<td align="center" class="vertical-dividers" rowspan="${fn:length(order.orderItemDtos) }">
															</c:when>
															<c:otherwise>
																<td align="center" class="vertical-dividers">
															</c:otherwise>
														</c:choose>
														<span obj_id="${order.userName}" mark="name_${order.userName}" obj_id="${order.userName}" mark="name_${order.userName}"> 
															<a href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'>${order.userName}</a>
														</span>
													</td>
													<c:choose>
														<c:when test="${fn:length(order.orderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
															<td align="center" class="vertical-dividers" rowspan="${fn:length(order.orderItemDtos) }">
														</c:when>
														<c:otherwise>
															<td align="center" class="vertical-dividers">
														</c:otherwise>
													</c:choose>
														<span>${order.integralTotal}</span>
													</td>
												<c:choose>
													<c:when test="${fn:length(order.orderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
														<td align="center" class="vertical-dividers" rowspan="${fn:length(order.orderItemDtos) }">
													</c:when>
													<c:otherwise>
														<td align="center" class="vertical-dividers">
													</c:otherwise>
												</c:choose>
												<div class="table-btn-group">
													<c:if test="${order.orderStatus==0 }">
															<a class="tab-btn" href="javascript:void(0);" onclick="orderCancel('${order.orderSn}');"/>取消订单</a> </br>
															<a class="tab-btn" href="javascript:void(0);" onclick="deliverGoods('${order.orderSn}');">确认发货</a> </br>
													</c:if>	
													<a class="tab-btn" href="<ls:url address="/admin/integralOrder/orderDetail/${order.orderSn}"/>"/>查看订单</a>
													<c:if test="${order.orderStatus==3 }">
														</br>
														<a class="tab-btn" href="javascript:void(0);" onclick="orderDel('${order.orderSn}');"/>删除订单</a>
													</c:if>
												</div>
												</td>
												</c:if>
											</tr>
										</c:forEach> <!-- S 商品列表 -->
									</tbody>
								</table>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
			<div class="clearfix" style="margin-bottom: 60px;">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	var contextPath = "${contextPath}";

	$(document).ready(function() {
		$("b[mark^='name_']").hover(function() {
			$(".xx").hide();
			$(this).parent().parent().find("div.xx").show();
		}, function() {
			$(".xx").hide();
		});
		
		laydate.render({
		   elem: '#startDate',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
		  });
	   
     	 laydate.render({
		   elem: '#endDate',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	    });
	});

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("ListForm").submit();
	}

	function deliverGoods(_orderId) {
		layer.open({
			title : '确认发货',
			id : 'deliverGoods',
			type: 2,
			content: contextPath + "/admin/integralOrder/deliverGoods/" + _orderId,
			area: ['396px', '250px']
		})
	}

	function orderCancel(sn) {
		layer.confirm("是否将订单：'" + sn + "'取消?", {icon:3}, function() {
			jQuery.ajax({
				url : contextPath + "/admin/integralOrder/orderCancel/" + sn,
				type : 'get',
				async : false, //默认为true 异步  
				dataType : 'json',
				error : function(data) {
				},
				success : function(data) {
					if (data == "OK") {
						window.location.reload(true);
					} else {
						layer.msg("操作失败！" + data, {icon:2});
					}
				}
			});
		});
	}

	function orderDel(sn) {
		layer.confirm("是否将订单：'" + sn + "删除 ,请慎重考虑,删除后不能恢复该订单'?", {icon:3},
				function() {
					jQuery.ajax({
						url : contextPath + "/admin/integralOrder/orderDel?sn=" + sn,
						type : 'get',
						async : false, //默认为true 异步  
						dataType : 'json',
						error : function(data) {
						},
						success : function(data) {
							if (data == "OK") {
								window.location.reload(true);
							} else {
								layer.msg("操作失败！", {icon:2});
							}
						}
					});
				});
	}
</script>
</html>

