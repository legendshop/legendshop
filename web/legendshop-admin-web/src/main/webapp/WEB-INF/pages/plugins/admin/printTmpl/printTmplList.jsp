<!Doctype html>
<html class="no-js fixed-la">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>运单模版列表- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">配送管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">打印模板管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/printTmpl/query" id="form1"
				method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						模版名称：<input type="text" class="${inputclass}" name="prtTmplTitle" maxlength="50" value="${printTmpl.prtTmplTitle}" placeholder="模板名称"/> 
						<input type="button" onclick="search()" class="${btnclass}" value="搜索" /> 
						<input type="button" class="${btnclass}" value="创建" onclick='window.location="<ls:url address='/admin/printTmpl/load'/>"' />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/printTmpl/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="模版名称" property="prtTmplTitle"></display:column>
					<display:column title="是否启用">
     		   ${item.prtDisabled==0?'否':'是'}
     		</display:column>

			<display:column title="是否系统默认">
     		   ${item.isSystem==0?'否':'是'}
     		</display:column>
			<display:column title="操作" media="html">
				<div class="table-btn-group">
					<button class="tab-btn" onclick="window.location='<ls:url address='/admin/printTmpl/loadDesign/${item.prtTmplId}'/>'">
						设计模版
					</button>
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="window.location='<ls:url address='/admin/printTmpl/load/${item.prtTmplId}'/>'">
						编辑
					</button>
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="javascript:deleteById('${item.prtTmplId}');">
						删除
					</button>
				</div>
			</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.js'/>" /></script>
<script type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
</script>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm("确认删除?", {icon:3}, function(){
			$.ajax({
				url : contextPath + '/admin/printTmpl/deleteById',
				data : {
					"id" : id
				},
				type : 'post',
				dataType : 'json',
				async : true, //默认为true 异步
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				},
				success : function(result) {
					if (result == "OK") {
						window.location.reload();
					} else {
						layer.alert(result,{icon: 2});
					}
				}
			});
		})
		
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>