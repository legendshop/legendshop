<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>微信管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">微信菜单管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">后台${menuLevel}级微信菜单定义</span></th>
				</tr>
			</table>
				<form:form action="${contextPath}/admin/weixinMenu/query"
					id="weixinForm" method="get">
						<div class="criteria-div">
							<%--<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>--%>
							<%--<input type="hidden" name="parentId" value="${parentId}"/>--%>
							<%--名称 <input class="${inputclass}" type="text" name="name" maxlength="50" value="${weixinMenu.name}"/>--%>
							<%--<input class="${btnclass}" type="button" onclick="search()" value="搜索"/>--%>
							<%-- <c:if test="${allParentMenu !=null}">
                                      <a href="<ls:url address='/admin/weixinMenu/query'/>">菜单列表</a>&raquo; 
                                      <c:forEach items="${allParentMenu }" var="menu">
                                          <a href="<ls:url address='/admin/weixinMenu/queryChildMenu/${menu.key}'/>"> ${menu.value }</a>&raquo; 
                                      </c:forEach>
                      </c:if> --%>
                      	<span class="item">
							<c:if test="${menuLevel<3&&list.size()<3}">
								<input class="${btnclass}" type="button"
								 	value="创建${menuLevel}级菜单"
									onclick='window.location="<ls:url address='/admin/weixinMenu/create?parentId=${parentId}'/>"' />
							</c:if>
							<c:if test="${empty parentId  ||parentId == 0}">
								<input class="${btnclass}" type="button" id="synchr"
									value="同步微信" onclick="synchronous();" />
							</c:if>
						</span>
						</div>
				</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/weixinMenu/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%>
					</display:column>
					<display:column title="名称" property="name"></display:column>
					<display:column title="顺序" property="seq" sortable="true"
						sortName="seq"></display:column>
					<display:column title="操作" media="html" style="width:220px">
						<%-- <div class="am-btn-toolbar">
							<div class="am-btn-group am-btn-group-xs">
								<c:if
									test="${parentMenu == null || (parentMenu != null && parentMenu.grade < 1)}">
									<button
										class="am-btn am-btn-default am-btn-xs am-text-secondary"
										onclick="window.location='${contextPath}/admin/weixinMenu/queryChildMenu/${item.id }'">
										<span class="am-icon-search"></span> 子菜单
									</button>
								</c:if>
								<button
									class="am-btn am-btn-default am-btn-xs am-text-secondary"
									onclick="window.location='${contextPath}/admin/weixinMenu/load/${item.id}'">
									<span class="am-icon-pencil-square-o"></span> 修改
								</button>
								<button
									class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only"
									onclick="deleteById('${item.id}')">
									<span class="am-icon-trash-o"></span> 删除
								</button>
							</div>
						</div> --%>
						<div class="table-btn-group">
							<c:if
								test="${parentMenu == null || (parentMenu != null && parentMenu.grade < 1)}">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/weixinMenu/queryChildMenu/${item.id }'">
									子菜单
								</button>
								<span class="btn-line">|</span>
							</c:if>
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/weixinMenu/load/${item.id}'">
								修改
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.id}')">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
			</div>

			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
							1.编辑中的商品不能直接在用户的手机上生效，您需要进行同步至微信，发布24小时内的所有用户都将更新到新的菜单
						</td>
					</tr>
			</table>
			<c:if test="${not empty parentId}">
				<div align="center" style="margin-top: 60px;">
					<input class="${btnclass}" type="button" value="返回"
						onclick="javascript:window.location.href='${contextPath}/admin/weixinMenu/query'" />
						
				</div>
			</c:if>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm('确定删除？', {icon:0}, function(){
			window.location = "<ls:url address='/admin/weixinMenu/delete/" + id + "'/>";
		});
	}

	function synchronous() {
		$("#synchr").attr("disabled", "disabled");
		layer.open({
			icon: 3,
			content : "是否同步至微信？",
			btn: ['确定', '关闭'], //可以无限个按钮
			yes: function(index, layero){
				jQuery.ajax({
					url : "${contextPath}/admin/weixinMenu/synchronous",
					type : 'post',
					async : false, //默认为true 异步  
					dataType : 'json',
					error : function(data) {
					},
					success : function(data) {
						var result = eval(data);
						if (result.success) {
							layer.msg("同步数据成功！", {icon:1});
							$("#synchr").removeAttr("disabled");
							return true;
						} else {
							var mess = data.msg;
							layer.msg(mess, {icon:2});
							$("#synchr").removeAttr("disabled");
							return false;
						}
					}
				});
				return true;
			},
			btn2: function(index, layero){
				$("#synchr").removeAttr("disabled");
			}
		});
	}

	highlightTableRows("item");

	function search() {
		$("#weixinForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#weixinForm")[0].submit();
	}
</script>
</html>

