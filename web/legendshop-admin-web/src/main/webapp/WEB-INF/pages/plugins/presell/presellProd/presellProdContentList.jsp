<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm"
	var="nowDate" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
	<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/presellProd/query"
		id="item" export="false" class="${tableclass}" style="width:100%">
		<display:column title="
			<span>
	           <label class='checkbox-wrapper'>
					<span class='checkbox-item'>
						<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
						<span class='checkbox-inner' style='margin-left:10px;'></span>
					</span>
			   </label>	
			</span>">
			<c:choose>
	    		<c:when test="${!(item.status eq 0 && item.preSaleStart lt nowDate)}">
				   <label class="checkbox-wrapper checkbox-wrapper-disabled">
						<span class="checkbox-item">
							<input type="checkbox" value="${item.id}" is-disabled="${item.status eq 0 && item.preSaleStart lt nowDate}" 
							can-offline="${item.status eq 0 && item.preSaleStart lt nowDate}"
							can-del="${item.status lt 0 or item.status eq 2 or (item.status eq 0 and item.preSaleStart gt nowDate)}"
							class="checkbox-input" disabled='disabled'/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:when>
	    		<c:otherwise>
	    			<label class="checkbox-wrapper">
						<span class="checkbox-item">
							<input type="checkbox" value="${item.id}" is-disabled="${item.status eq 0 && item.preSaleStart lt nowDate}" 
							can-offline="${item.status eq 0 && item.preSaleStart lt nowDate}"
							can-del="${item.status lt 0 or item.status eq 2 or (item.status eq 0 and item.preSaleStart gt nowDate)}"
							class="checkbox-input selectOne" onclick="selectOne(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:otherwise>
	    	</c:choose>
		</display:column>
		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="方案名称">
			<c:choose>
				<c:when test="${item.status eq 0 && item.preSaleEnd ge nowDate}">
					<a href="${PC_DOMAIN_NAME}/presell/views/${item.id}"
						target="_blank">${item.schemeName}</a>
				</c:when>
				<c:otherwise>
					${item.schemeName}
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="店铺" style="width: 150px;">
			<a href="${contextPath}/admin/shopDetail/load/${item.shopId}" target="_blank"
				style="word-wrap: break-word; word-break: break-all;">${item.shopName}</a>
		</display:column>
		<display:column title="促销时间" sortable="true">
			<fmt:formatDate value="${item.preSaleStart}"
				pattern="yyyy-MM-dd HH:mm" />&nbsp;-
<fmt:formatDate value="${item.preSaleEnd}" pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column title="状态" sortable="true"
			style="word-wrap:break-word;word-break:break-all;">
			<c:choose>
				<c:when test="${item.status eq 2 }">已完成</c:when>
				<c:when test="${item.status eq 3}">已终止</c:when>
				<c:when test="${(item.preSaleStart le nowDate && item.status ne 0) || item.status eq -3}">已过期</c:when>
				<c:when test="${item.status eq -2 and item.preSaleStart gt nowDate}">未提审</c:when>
				<c:when test="${item.status eq -1 and item.preSaleStart gt nowDate}">待审核</c:when>
				<c:when test="${item.status eq 1 and item.preSaleStart gt nowDate}">已拒绝</c:when>
				<c:when test="${item.status eq 0}">
					<c:choose>
						<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
							商品已删除
						</c:when>
						<c:when test="${item.prodStatus eq 0}">
							商品已下线
						</c:when>
						<c:when test="${item.prodStatus eq 1 and item.preSaleStart le nowDate and item.preSaleEnd gt nowDate}">
							<font color="#e5004f">进行中</font>
						</c:when>
						<c:when test="${item.prodStatus eq 2}">
							商品违规下线
						</c:when>
						<c:when test="${item.prodStatus eq 3}">
							商品待审核
						</c:when>
						<c:when test="${item.prodStatus eq 4}">
							商品审核失败
						</c:when>
						<c:when test="${item.preSaleStart gt nowDate}">
							未开始
						</c:when>
					</c:choose>
				</c:when>
			</c:choose>
		</display:column>
		<display:column title="创建时间">
			<fmt:formatDate value="${item.createTime }"
				pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column title="操作" media="html">
			<div class="table-btn-group">
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/presellProd/toPresellProdDetail/${item.id}'">
					查看
				</button>
				<c:if test="${item.status ne -1 && item.status ne -2}">
				<span class="btn-line">|</span>
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/presellProd/operatorPresellActivity/${item.id}'">
					运营
				</button>
				</c:if>
				<c:if test="${item.status eq -1 and item.preSaleStart gt nowDate}">
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="window.location='${contextPath}/admin/presellProd/toAudit/${item.id}'">
						审核
					</button>
				</c:if>
<%--				<c:if test="${item.preSaleStart gt nowDate}">--%>
<%--					<c:if test="${item.status le 0 or item.status eq 1}">--%>
<%--						<span class="btn-line">|</span>--%>
<%--						<button class="tab-btn" onclick="window.location='${contextPath}/admin/presellProd/load/${item.id}'">--%>
<%--							修改--%>
<%--						</button>--%>
<%--					</c:if>--%>
<%--				</c:if>--%>
				<c:if test="${item.status eq 0 && item.preSaleEnd gt nowDate}">
					<span class="btn-line">|</span>
					<button class="tab-btn" id="statusImg" onclick="stopPresellProd('${item.id}')">
						终止
					</button>
				</c:if>
				</button>
			</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fl">
    		<input type="button" value="批量下线" onclick="batchDeletePresellProds()" id="batch-off" class="batch-btn">
		</div>
    		<div class="fr">
    			 <div class="page">
     			 <div class="p-wrap">
         		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 </div>
   			 </div>
   		</div>
   	</div>
	<script type="text/javascript">
		$("#selectAll").on(
				"click",
				function() {
					if ($(this).is(":checked")) {
						$("input[type=checkbox][name=presellId][is-disabled=true]")
								.prop("checked", true);
					} else {
						$("input[type=checkbox][name=presellId][is-disabled=true]")
								.prop("checked", false);
					}
				});
	</script>