<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/amaze/css/amazeui.css" />
</head>
<body>
<div align="center">      
      <form:form   action="${contextPath}/admin/adminUser/updatePwd" id="updatePwd" method="post">
        <table align="center" class="${tableclass} no-border content-table" id="col1" style="width: 450px">
                <%-- <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center"> 修改管理员 ${adminUser.name } 权限</div>
                           
                        </th>
                    </tr>
                </thead> --%>
                 <input type="hidden" name="id" value="${adminUser.id }" >
		<tbody>
       	 <tr>
	       	<td width="100px">
	          <div align="right" > <font color="ff0000">*</font>原密码：</div>
	       </td>
	        <td align="left" style="padding-left:2px;">
	           <input type="password" name="oldPassword" style="width: 260px;height:26px;" id="oldPassword"  value="" autocomplete="off" maxlength="20" placeholder=" 请输入原密码">
	        </td>
         </tr>
         <tr>
           <td>
	          <div align="right" > <font color="ff0000">*</font>新密码：</div>
	       </td>
	        <td align="left" style="padding-left:2px;">
	           <input type="password" name="password" style="width: 260px;height:26px;" id="password"  value="" autocomplete="off" maxlength="20" placeholder=" 请输入新密码">
	        </td>
         </tr>
       	 <tr>
       	 	 <td>
	          <div align="right" > <font color="ff0000">*</font>确认密码：</div>
	       </td>
	        <td align="left" style="padding-left:2px;">
	           	<input type="password" name="password2"  id="password2" style="width: 260px;height:26px;" value="" autocomplete="off" maxlength="20" placeholder=" 请确认密码">
	        </td>
         </tr>         
          <tr>
          		<td></td>
               <td>
                   <div style="padding-left: 55px;">
                       <input type="button" class="criteria-btn" name="Submit" id="Submit" value="保存">
              			<input type="button" class="criteria-btn" value="关闭" onclick="javascript:closeDialog()" />
                   </div>
               </td>
           </tr>
        </tbody>
</table>
      </form:form>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/updateAdminUserPwd.js'/>"></script>
<script language="javascript">
var contextPath="${contextPath}";
$.validator.setDefaults({
	
});
</script>
</body>
</html>

