<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>推广员管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet"
	href="${contextPath}/resources/templets/amaze/css/amazeui.switch.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<style>
.distset_title_ul li {
	cursor: pointer;
}
.am-switch am-switch-wrapper{
    width: 50px !important;
    height: 30px !important;
}
.am-switch-container{
    width: 150px !important;
    height: 30px !important;
}
.am-switch-handle-on ,.am-switch-primary{
    width: 50px !important;
    height: 30px !important;
}
.am-switch-label{
	width: 0 !important;
    padding: 0 !important;
    height: 30px !important;
}
.am-switch-handle-off ,.am-switch-default{
	width: 50px !important;
    height: 30px !important;
}
.tips {
	font-size: 12px;
}
</style>

</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

			<table class="${tableclass} no-border" style="width: 100%">
				<thead>
					<tr>
						<th class="title-border">
							推广管理&nbsp;＞&nbsp;
							<span style="color:#0e90d2; font-size:14px;">推广设置</span>
						</th>
					</tr>
				</thead>
			</table>

			<div style="margin: 10px;">
				<ul class="distset_title_ul am-tabs-nav am-nav am-nav-tabs" style="width:100%;">
					<li
						<c:if test="${empty type || type=='rule' }"> class="am-active" </c:if>><i></i><a
						href="<ls:url address="/admin/distSet/load?type=rule"/>">分佣规则设置</a></li>
					<li <c:if test="${type=='promo' }"> class="am-active" </c:if>><i></i><a
						href="<ls:url address="/admin/distSet/load?type=promo"/>">推广员申请设置</a></li>
				</ul>
			</div>

			<form:form action="${contextPath}/admin/distSet/save" method="post"
				id="form1" onsubmit="return checkForm();">
				<input id="distSetId" name="distSetId" value="${distSet.distSetId}"
					type="hidden">
				<div align="right">
					<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
					<c:choose>
						<c:when test="${empty type || type=='rule' }">
							<table border="0" align="right" class="${tableclass} no-border">
								<input type="hidden" name="type" value="rule">
								<tr>
									<td valign="top" style="width: 13%;min-width:185px;">
										<div align="right">注册佣金：</div>
									</td>
									<td><span>￥</span><input
										onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');"
										maxLength="5" class="${inputclass}" type="text"
										name="regCommis" id="regCommis" value="${distSet.regCommis}" />
										<label class="error" style="display: none;"></label> <br>
									<span class="tips">A邀请一个新会员B注册后，A所能获得的佣金</span></td>
								</tr>

								<tr>
									<td valign="top">
										<div align="right">开启多级分佣：</div>
									</td>
									<td><input id="support-dist" type="checkbox" 
										data-am-switch data-size="xs"
										<c:if test="${distSet.supportDist==1}">checked="checked"</c:if>>
										<input type="hidden" name="supportDist" id="supportDist"
										value="${distSet.supportDist}" /> <span class="tips">
											使用多级分佣及高额佣金，有关停微信支付的风险，请妥善使用!</span></td>
								</tr>
								<tr>
									<td valign="top">
										<div align="right">会员直接上级分佣比例：</div>
									</td>
									<td><input class="${inputclass}" type="text"
										name="firstLevelRate" id="firstLevelRate"
										value="${distSet.firstLevelRate}" maxLength="5"
										onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');" /> <span
										style="margin-left: -20px;">%</span> <label class="error"
										style="display: none;"></label> <br>
									<span class="tips">A邀请了B，B后续消费A所能获得的佣金</span></td>
								</tr>
								<tr class="multi-dist"
									<c:if test="${distSet.supportDist==0}">style="display:none;"</c:if>>
									<td valign="top">
										<div align="right">会员上二级分佣比例：</div>
									</td>
									<td><input class="${inputclass}" type="text"
										name="secondLevelRate" id="secondLevelRate"
										value="${distSet.secondLevelRate}" maxLength="5"
										onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');" /> <span
										style="margin-left: -20px;">%</span> <label class="error"
										style="display: none;"></label> <br>
									<span class="tips">A邀请的会员B后续发展了其它会员C，C消费时，A所能获得的佣金</span></td>
								</tr>
								<tr class="multi-dist"
									<c:if test="${distSet.supportDist==0}">style="display:none;"</c:if>>
									<td valign="top">
										<div align="right">会员上三级分佣比例：</div>
									</td>
									<td><input class="${inputclass}" type="text"
										name="thirdLevelRate" id="thirdLevelRate"
										value="${distSet.thirdLevelRate}" maxLength="5"
										onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');" /> <span
										style="margin-left: -20px;">%</span> <label class="error"
										style="display: none;"></label> <br>
									<span class="tips">A邀请的会员B后续发展了会员C，C又发展了会员D，D消费时，A所能获得的佣金</span></td>
								</tr>
								<tr>
									<td colspan="2">
										<div align="center">
											<input type="submit" value="保存" class="${btnclass}" /> 
										</div>
									</td>
								</tr>
							</table>
						</c:when>
						<c:when test="${type=='promo' }">
							<table border="0" align="right" class="${tableclass} no-border">
								<input type="hidden" name="type" value="promo">
								<tr>
									<td style="width: 13%;min-width:185px;">
										<div align="right">注册会员即为推广员：</div>
									</td>
									<td><input id="reg-promoter" type="checkbox"
										data-am-switch data-size="xs"
										<c:if test="${distSet.regAsPromoter==1}">checked="checked"</c:if>>
										<input type="hidden" name="regAsPromoter" id="regAsPromoter"
										value="${distSet.regAsPromoter}" /></td>
								</tr>
								<tr class="promoter-need"
									<c:if test="${distSet.regAsPromoter==1}">style="display:none;"</c:if>>
									<td>
										<div align="right">申请需提交信息：</div>
									</td>
									<td>
										<label>
												<label class="checkbox-wrapper <c:if test='${distSet.chkRealName==1}'>checkbox-wrapper-checked</c:if>">
													<span class="checkbox-item">
														<input type="checkbox" class="checkbox-input" id="chkbox-real-name" 
														<c:if test="${distSet.chkRealName==1}">checked="checked"</c:if>>
														<span class="checkbox-inner"></span>
													</span>
													<span class="checkbox-txt">真实姓名</span>
												</label>
										</label>
										<label>
											<label class="checkbox-wrapper <c:if test='${distSet.chkMobile==1}'>checkbox-wrapper-checked</c:if>">
													<span class="checkbox-item">
														<input type="checkbox" class="checkbox-input" id="chkbox-mobile" 
														<c:if test="${distSet.chkMobile==1}">checked="checked"</c:if>>
														<span class="checkbox-inner"></span>
													</span>
													<span class="checkbox-txt">手机号</span>
												</label>
										</label>
<%--										<label>--%>
<%--											<label class="checkbox-wrapper <c:if test='${distSet.chkMail==1}'>checkbox-wrapper-checked</c:if>">--%>
<%--												<span class="checkbox-item">--%>
<%--													<input type="checkbox" class="checkbox-input" id="chkbox-mail" --%>
<%--													<c:if test="${distSet.chkMail==1}">checked="checked"</c:if>>--%>
<%--													<span class="checkbox-inner"></span>--%>
<%--												</span>--%>
<%--												<span class="checkbox-txt">邮箱</span>--%>
<%--											</label>--%>
<%--										</label>--%>
										<label>
											<label class="checkbox-wrapper <c:if test='${distSet.chkAddr==1}'>checkbox-wrapper-checked</c:if>">
												<span class="checkbox-item">
													<input type="checkbox" class="checkbox-input" id="chkbox-addr" 
													<c:if test="${distSet.chkAddr==1}">checked="checked"</c:if>>
													<span class="checkbox-inner"></span>
												</span>
												<span class="checkbox-txt">住址</span>
											</label>
										</label>

										<input type="hidden" name="chkRealName" id="chkRealName"
										value="${distSet.chkRealName}"> <input type="hidden"
										name="chkMobile" id="chkMobile" value="${distSet.chkMobile}">
										<input type="hidden" name="chkMail" id="chkMail"
										value="${distSet.chkMail}"> <input type="hidden"
										name="chkAddr" id="chkAddr" value="${distSet.chkAddr}">
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<div align="center">
											<input type="submit" value="保存" class="${btnclass}" />
										</div>
									</td>
								</tr>
							</table>
						</c:when>
					</c:choose>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script
	src="${contextPath}/resources/templets/amaze/js/amazeui.switch.min.js"></script>
<script>
	$(document).ready(
			function() {
				
				$('#support-dist').on('switchChange.bootstrapSwitch',
						function(event, state) {
							if (state) {
								$("#supportDist").val(1);
								$(".multi-dist").show();
							} else {
								$("#supportDist").val(0);
								$(".multi-dist").hide();
							}
						});

				$('#reg-promoter').on('switchChange.bootstrapSwitch',
						function(event, state) {
							if (state) {
								$("#regAsPromoter").val(1);
								$(".promoter-need").hide();
							} else {
								$("#regAsPromoter").val(0);
								$(".promoter-need").show();
							}
						});

				$("#chkbox-real-name").change(function() {
					if ($(this).is(':checked')) {
						$(this).parent().parent().addClass("checkbox-wrapper-checked");
						$("#chkRealName").val(1);
					} else {
						$(this).parent().parent().removeClass("checkbox-wrapper-checked");
						$("#chkRealName").val(0);
					}
				});

				$("#chkbox-mobile").change(function() {
					if ($(this).is(':checked')) {
						$(this).parent().parent().addClass("checkbox-wrapper-checked");
						$("#chkMobile").val(1);
					} else {
						$(this).parent().parent().removeClass("checkbox-wrapper-checked");
						$("#chkMobile").val(0);
					}
				});

				$("#chkbox-mail").change(function() {
					if ($(this).is(':checked')) {
						$(this).parent().parent().addClass("checkbox-wrapper-checked");
						$("#chkMail").val(1);
					} else {
						$(this).parent().parent().removeClass("checkbox-wrapper-checked");
						$("#chkMail").val(0);
					}
				});

				$("#chkbox-addr").change(function() {
					if ($(this).is(':checked')) {
						$(this).parent().parent().addClass("checkbox-wrapper-checked");
						$("#chkAddr").val(1);
					} else {
						$(this).parent().parent().removeClass("checkbox-wrapper-checked");
						$("#chkAddr").val(0);
					}
				});

				$("input").focus(function() {
					$(this).parent().find(".error").hide();
				});

			});

	function checkForm() {
		var errCount = 0;
		var regCommis = $("#regCommis").val();
		if (regCommis != undefined) {
			if (isNaN(regCommis)) {
				$("#regCommis").parent().find(".error").html("请输入正确的数字");
				$("#regCommis").parent().find(".error").show();
				errCount++;
			}
		}

		var firstLevelRate = $("#firstLevelRate").val();
		if (firstLevelRate != undefined) {
			if (firstLevelRate == "") {
				$("#firstLevelRate").parent().find(".error").html("请输入分佣比例");
				$("#firstLevelRate").parent().find(".error").show();
				errCount++;
			} else if (isNaN(firstLevelRate)) {
				$("#firstLevelRate").parent().find(".error").html("请输入正确的数字");
				$("#firstLevelRate").parent().find(".error").show();
				errCount++;
			} else if (firstLevelRate > 50) {
				$("#firstLevelRate").parent().find(".error").html("分佣不能大于50%");
				$("#firstLevelRate").parent().find(".error").show();
				errCount++;
			}
		}

		var secondLevelRate = $("#secondLevelRate").val();
		if (secondLevelRate != undefined) {
			if (secondLevelRate == "") {
				$("#secondLevelRate").parent().find(".error").html("请输入分佣比例");
				$("#secondLevelRate").parent().find(".error").show();
				errCount++;
			} else if (isNaN(secondLevelRate)) {
				$("#secondLevelRate").parent().find(".error").html("请输入正确的数字");
				$("#secondLevelRate").parent().find(".error").show();
				errCount++;
			} else if (secondLevelRate > 50) {
				$("#secondLevelRate").parent().find(".error").html("分佣不能大于50%");
				$("#secondLevelRate").parent().find(".error").show();
				errCount++;
			}
		}

		var thirdLevelRate = $("#thirdLevelRate").val();
		if (thirdLevelRate != undefined) {
			if (thirdLevelRate == "") {
				$("#thirdLevelRate").parent().find(".error").html("请输入分佣比例");
				$("#thirdLevelRate").parent().find(".error").show();
				errCount++;
			} else if (isNaN(thirdLevelRate)) {
				$("#thirdLevelRate").parent().find(".error").html("请输入正确的数字");
				$("#thirdLevelRate").parent().find(".error").show();
				errCount++;
			} else if (thirdLevelRate > 50) {
				$("#thirdLevelRate").parent().find(".error").html("分佣不能大于50%");
				$("#thirdLevelRate").parent().find(".error").show();
				errCount++;
			}
		}

		if ((parseFloat(firstLevelRate)+parseFloat(secondLevelRate)+parseFloat(thirdLevelRate))>50){
			layer.alert("分佣比例总和不能超过50%",{icon:0});
			errCount++;
		}
		if (errCount > 0) {
			return false;
		}

	}
</script>
</html>
