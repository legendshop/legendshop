<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="../back-common.jsp"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
 <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/amaze/css/payEdit.css'/>" />
 <%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>

	<form id="payForm" style="max-height: 685px;height:685px; overflow: auto;">
		
		<input type="hidden" name="isEnable" id="isEnable" value="${payType.isEnable}"/>   
		
		<div class="modal-body-main filll fillr">
			<div style="padding-top:7px;">
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label class="">支付类型</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<span>支付宝支付</span>
						</div>
					</div>
				</div>
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="seller_email" class="ant-form-item-required">支付宝企业账户</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="seller_email" name="seller_email"  value="${jsonObject['seller_email']}"
								class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="partner" class="ant-form-item-required">合作者身份(PID)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="partner"  name="partner"  value="${jsonObject['partner']}" class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="key" class="ant-form-item-required">安全校验码(Key)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="password" id="key" name="key"  value="${jsonObject['key']}" class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
	
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="appId" class="ant-form-item-required">开放平台应用ID(APPID)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="appId" name="appId" value="${jsonObject['appId']}"
								class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
	
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="rsa_private" class="ant-form-item-required">商户应用私钥(rsa_private)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="rsa_private"  name="rsa_private" value="${jsonObject['rsa_private']}"
								class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
	
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="rsa_publish" class="ant-form-item-required">支付宝公钥(rsa_publish)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="rsa_publish" name="rsa_publish" value="${jsonObject['rsa_publish']}"
								class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
	
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="platform" >使用终端</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control has-success">
							<div class="ant-checkbox-group">
								<c:choose>
								   <c:when test="${not empty jsonObject['use']['pc']}">
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox ant-checkbox-checked"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input"  checked="checked" name="use" value="pc" switch="off">
								      </span><span>PC端</span> </label>
								   </c:when>
								   <c:otherwise>
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input" name="use" value="pc" switch="on">
								       </span><span>PC端</span> </label>
								   </c:otherwise>
								</c:choose>
								
								<c:choose>
								   <c:when test="${not empty jsonObject['use']['app']}">
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox ant-checkbox-checked"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input" checked="checked" name="use" value="app" switch="off">
								      </span><span>移动端</span> </label>
								   </c:when>
								   <c:otherwise>
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input" name="use" value="app" switch="on">
								       </span><span>移动端</span> </label>
								   </c:otherwise>
								</c:choose>
								
							   <c:choose>
								   <c:when test="${not empty jsonObject['use']['mobile']}">
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox ant-checkbox-checked"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input"  checked="checked" name="use" value="mobile" switch="off">
								      </span><span>WAP端</span> </label>
								   </c:when>
								   <c:otherwise>
								      <label class="ant-checkbox-group-item ant-checkbox-wrapper"><span
									class="ant-checkbox"><span class="ant-checkbox-inner"></span>
										<input type="checkbox" class="ant-checkbox-input" name="use" value="mobile" switch="on">
								       </span><span>WAP端</span> </label>
								   </c:otherwise>
								</c:choose>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="accountState" class="">状态</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control has-success">
						<div class="ant-radio-group ant-radio-group-large">
							<label class="ant-radio-wrapper"><span
								class="ant-radio <c:if test="${payType.isEnable eq 0}"> ant-radio-checked</c:if>">
								
								<input type="radio"
									class="ant-radio-input" value="0"  <c:if test="${payType.isEnable eq 0}"> checked="checked"</c:if>  ><span
									class="ant-radio-inner"></span> </span><span>关闭</span> 
						   </label>
									
							<label
								class="ant-radio-wrapper"><span class="ant-radio <c:if test="${payType.isEnable eq 1}"> ant-radio-checked</c:if>">
								<input
									type="radio" class="ant-radio-input" value="1"  <c:if test="${payType.isEnable eq 1}"> checked="checked"</c:if> >
									<span
									class="ant-radio-inner"></span> </span><span>启用</span>
							</label>
									
									
						</div>
					</div>
				</div>
			</div>
			<div data-show="true"
				class="ant-alert ant-alert-info ant-alert-no-icon">
				<span class="ant-alert-message"><div>
						<p>重要提醒：</p>
						<p>1、设置账号前，请确保您已签约支付宝“即时到账收款”、“移动支付”和“手机网站支付”收款服务</p>
						<p>2、安全校验码默认为空；如果需要编辑，请重新输入</p>
						<p>3、如何设置支付宝签约信息？</p>
						<p>a.访问支付宝商户服务中心(b.alipay.com)，用您的签约支付宝账号登录；</p>
						<p>b.在“我的商户服务”中，点击“查询PID、Key”，将查询的相应信息填写到上面相应框中；</p>
						<p>c.在上述打开的支付宝页面的“合作伙伴密钥管理”下，点击"RSA加密"后的“<a href="https://docs.open.alipay.com/200/105310" target="_blank">配置密钥</a>”
							把下面的密钥复制到弹出框内（注意不要多余的空格）。</p>
						<p>MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBzj/lZl4yQU56fsdlECskCsz1
							scopuC7XBPrP2WrcfQRMd2DN5ziqjvbH+ActaZHx6tC1qxHih8c+4YA17cFZMHPt
							2S6gLzNRNpa+m/Xdax4u6H6/9hD5w4Ba1XsY0xLatlxhqqR4uR+VveY/GbFj0AUT
							0sRAjzox1BvIqr3+4QIDAQAB</p>
					</div> </span><span class="ant-alert-description"></span>
			</div>
			<input type="submit" id="editBtn" value = "提交更改" class="criteria-btn" />
		</div>
	</form>
<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/AliPayTypeEdit.js'/>"></script>
<script>
var contextPath = "${contextPath}";
</script>

</body>
</html>