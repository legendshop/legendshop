<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
 <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
 <script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
 <style type="text/css">
		.skulist{border:solid #dddddd; border-width:1px 0px 0px 1px;width: 100%;}
		.skulist tr td{border:solid #dddddd; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
</style>
            <table class="${tableclass}" style="width: 100%">
			     <thead>
			    	<tr>
			    	  <th><strong class="am-text-primary am-text-lg">秒杀活动</strong> / 编辑秒杀活动
					  </th>
			    	</tr>
			    </thead>
	    	</table>
        <form:form action="${contextPath}/admin/seckillActivity/save" method="post" id="form1" enctype="multipart/form-data">
            <div align="center">
             <table  style="width: 100%" class="${tableclass}" id="col1">
      			  <input type="hidden" id="skus" name="skus" />
      			  <input type="hidden" id="id" name="id" value="${seckillActivity.id}"/>
      <tr>
        <td>
          <div align="right">活动名称:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="50" name="seckillTitle" id="seckillTitle" value="${seckillActivity.seckillTitle}"  size="50" class="${inputclass}" />
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">选择商品:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="button" value="选择商品" onclick="showProdlist()"/>
           <input type="hidden" name="prodId" id="prodId" value="${prodId}"/>
           <div class="prodList">
           		<c:if test="${not empty activityDtos}">
           		<table class="skulist" class="${tableclass}" style="width: 100%">
           			<tr >
						<td colspan="6"><b>商品信息</b></td>
					</tr>
					<tr>
						<td>商品名称</td>
						<td>属性</td>
						<td>商品价格</td>
						<td>秒杀价格</td>
						<td>库存数量</td>
						<td>操作</td>
					</tr>
					<c:forEach items="${activityDtos}" var="prodDto">
					<tr id="list_${prodDto.skuId}">
	            		<td><a href="${PC_DOMAIN_NAME}/views/${prodDto.prodId}" target="_blank">${prodDto.prodName}</a></td>
	            		<td>
	            			<c:choose>
	            				<c:when test="${not empty  prodDto.prop}">${prodDto.prop}</c:when>
	            				<c:otherwise>无</c:otherwise>
	            			</c:choose>
	            		</td>
	            		<td>100</td>
	            		<td>
	            			<input type="text" id="seckillPrice" value="${prodDto.price}"/>
	            			<input type="hidden" id="skuId" value="${prodDto.skuId}"/>
	            		</td>
	            		<td><input type="text" id="seckillStock" value="${prodDto.stock}"/></td>
	            		<td><input type="button" value="移除" onclick="removeSku(${prodDto.skuId})"/></td>
            		</tr>
					</c:forEach>        		
           		</table>
           	</c:if>
           </div>
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">活动摘要:<font color="ff0000">*</font></div>
       </td>
        <td>
        	<textarea   name="seckillBrief" id="seckillBrief" style="width:100%; padding:10px" maxlength="99">${seckillActivity.seckillBrief}</textarea>
        	<input type="hidden" name="seckillLowPrice" id="seckillLowPrice" value="${seckillActivity.seckillLowPrice}"/>
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">活动图片(大小580*200):<font color="ff0000">*</font></div>
       </td>
        <td>
        	<input type="file" name="seckillPicFile" id="seckillPicFile" style="display: inline;"/>
        </td>
      </tr>
       <c:if test="${not empty seckillActivity.seckillPic}">
	      <tr>
	        <td>
	          <div align="right">原有图片:<font color="ff0000">*</font></div>
	       </td>
	        <td>
	        	 <a href="<ls:photo item='${seckillActivity.seckillPic}'/>" target="_blank"><img  src="<ls:photo item='${seckillActivity.seckillPic}'/>" style="max-height: 150px;max-width: 150px;"></a>
	        </td>
	      </tr>
       </c:if>
       <tr>
        <td>
          <div align="right">开始时间:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input readonly="readonly"  name="startTime" size="21" id="startTime" value='<fmt:formatDate value="${seckillActivity.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'  type="text"  />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">结束时间:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input readonly="readonly"  name="endTime" size="21" id="endTime" value='<fmt:formatDate value="${seckillActivity.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'  type="text" />
        </td>
      </tr>
    	   <tr>
	        <td>
	          <div align="right">秒杀描述:</div>
	       </td>
	        <td>
	        	<textarea name="seckillDesc" id="seckillDesc" cols="100" rows="8" style="width:100%;height:400px;visibility:hidden;">${seckillActivity.seckillDesc}</textarea> 
	        </td>
    	  </tr>
      <tr>
             <td colspan="2">
                 <div align="center">
                     <input type="submit" class="${btnclass}" value="保存"/>
                     <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/seckillActivity/query'" />
                 </div>
             </td>
         </tr>
     </table>
           </div>
        </form:form>
<script language="javascript">
KindEditor.options.filterMode=false;
KindEditor.create('textarea[name="seckillDesc"]', {
	cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
	uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
	fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
	allowFileManager : true,
	afterBlur:function(){this.sync();},
	width : '100%',
	height:'400px',
	afterCreate : function() {
		var self = this;
		KindEditor.ctrl(document, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
		KindEditor.ctrl(self.edit.doc, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
	}
});

var hour;
var date;
function pickedFunc(){
	var a=$dp.$D('startTime');
	hour=a.H;
	date=a.y+"-"+a.M+"-"+a.d+" ";
}

$("#endTime").click(function(){
	var time=$("#startTime").val();
		if(isBlank(time)){
			layer.msg("请先选择开始时间", {icon:0});
			return false;
		}
		var max=date+getTimePeriod(hour);
		WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:time,maxDate:max});
		
	})
	
function getTimePeriod(hour){
	if(hour>=9 && hour<12){
		return "11:59:59"
	}else if(hour>=12 && hour<15){
		return "14:59:59"
	}else if(hour>=15 && hour<18){
		return "17:59:59"
	}else if(hour>=18 && hour<21){
		return "20:59:59"
	}else if(hour>=21 && hour<24){
		return "23:59:59"
	}
}	
jQuery.validator.addMethod("isNumber", function(value, element) {       
         return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
    }, "必须整数或小数");  

jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
         value=parseFloat(value);      
         return this.optional(element) || value>=1;       
    }, "必须大于等于1元"); 
 $(document).ready(function() {
 	var pic="${seckillActivity.seckillPic}";
 		if(!isBlank(pic)){
 			$("#seckillPicFile").addClass("ignore");
 		}
	    jQuery("#form1").validate({
	    ignore: ".ignore",
	            rules: {
			             seckillTitle: {
		               		 required: true
		           		 },startTime: {
		               		 required: true
		           		 },seckillBrief: {
		               		 required: true
		           		 },seckillPicFile: {
		               		 required: true
		           		 }
		           		 
		           		 
	       		 },
	        messages: {
	           			 seckillTitle: {
		               		 required: "标题不能为空"
		           		 },startTime: {
		               		 required: "开始时间不能为空"
		           		 },seckillBrief: {
		               		 required: "摘要不能为空"
		           		 },seckillPicFile: {
		               		 required: "图片不能为空"
		           		 }
	        },
	          submitHandler: function (form) {
	         			var prodid=$("#prodId").val();
	        	  		if(isBlank(prodid)){
	        	  			layer.msg("商品不能为为空", {icon:0});
	        	  			return false;
	        	  		}
	         			var prodParamArray =getParam();
	         			if(isBlank(prodParamArray)){
	         				return false;
	         			}
						 var prodParameter = JSON.stringify(prodParamArray);
						 console.log(prodParameter);
						 if(!isBlank(prodParameter)){
							 $("#skus").val(prodParameter);
							 form.submit();
						 }
				}	        
	        
	    });
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});

function getParam(){
	var temp=$("#seckillPrice").val();
	var prodParamArray = [];
	var prod,prodid="";skuid="",price="",stock="";
	var id=$("#id").val();
    $(".skulist tr[id^='list_']").each(function(){
		prod = new Object();
    	if(isBlank(id)){
			prodid= $(this).find("#prodId").val();
			skuid= $(this).find("#skuId").val();
			var prodprice= $(this).find("#seckillPrice").val();
			var prodstock= $(this).find("#seckillStock").val();
			if(!checkMoney(prodprice)||!checkStock(prodstock)){
				prodParamArray=[];
				return false;
			}
			if(temp>prodprice){
				temp=prodprice
			}
			//skuid为空
			if(!isBlank(skuid)){
					prod.prodid = prodid;
					prod.skuid = skuid;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}else{
				if(!isBlank(prodid)){
					prod.prodid = prodid;
					prod.skuid=0;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
				}
			}
		}else{
			prodid= $("#prodId").val();
			skuid= $(this).find("#skuId").val();
			var prodprice= $(this).find("#seckillPrice").val();
			var prodstock= $(this).find("#seckillStock").val();
			if(!checkMoney(prodprice)||!checkStock(prodstock)){
				prodParamArray=[];
				return false;
			}
			if(!isBlank(skuid)){
					prod.prodid = prodid;
					prod.skuid = skuid;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}else{
					prod.prodid = prodid;
					prod.skuid=0;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}
		}
	 });
	 $("#seckillLowPrice").val(temp);
	 return prodParamArray;
}

function showProdlist(){
	layer.open({
		title: '选择商品',
		id: 'seckillProd',
		type: 2,
		content: "${contextPath}/admin/seckillActivity/seckillProdLayout",
		area: ['1000px', '400px']
		
	});
}
	
 function removeSku(id){
		var length=$(".skulist tr").length;
		if(length<=3){
			layer.msg("最少保留一条数据",{icon:0});
		}else{
			$(".skulist tr[id=list_"+id+"]").remove();
		}
	}
//检查库存	
function addProdTo(prodId){
		if(prodId==null||prodId==""||prodId==undefined){
		    layer.msg("商品Id为空",{icon:2});
		 }
		$("#prodId").val(prodId);	 
	 	var url="${contextPath}/admin/seckillActivity/seckillProdSku/"+prodId;
		$(".prodList").load(url);
}


function checkStock(val){
	if(isBlank(val)){
		layer.msg("库存不能为空",{icon:0});
    	return false;
    }
	var regex=/^[0-9]*[1-9][0-9]*$/;
	if(!regex.test(val)){
    	layer.msg("请输入正确的库存",{icon:0});
        return false;
    }
	return true;
}



//检查金额
function checkMoney(val){
	if(isBlank(val)){
		layer.msg("价格不能为空",{icon:0});
    	return false;
    }
    if(parseFloat(val)<=0){
    	layer.msg("价格不能小于0",{icon:0});
    	return false;
    }
    var regex = /^[\d]*(.[\d]{1,2})?$/;
    if(!regex.test(val)){
    	layer.msg("请输入正确的金额格式",{icon:0});
        return false;
    }
    return true;
}

$("input[seckillPicFile=file]").change(function(){
	checkImgType(this);
	checkImgSize(this,512);
});

laydate.render({
	   elem: '#startTime',
	   type:'datetime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
	  });
	   
	 laydate.render({
	   elem: '#endTime',
	   type:'datetime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
 });

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
</script>