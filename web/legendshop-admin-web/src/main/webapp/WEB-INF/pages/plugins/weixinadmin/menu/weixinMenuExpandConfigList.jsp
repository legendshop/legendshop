<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<form:form action="${contextPath}/admin/weixin/expandConfig/query" id="form1" method="get">
    <table class="${tableclass}" style="width: 100%">
        <thead>
        <tr>
            <th>
                <strong class="am-text-primary am-text-lg">微信管理</strong> / 微信扩展接口管理
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <div align="left" style="padding: 3px">
                    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                    &nbsp;名称
                    <input class="${inputclass}" type="text" name="name" maxlength="50" value="${config.name}"/>
                    <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
                    <input class="${btnclass}" type="button" value="创建微信扩展接口" onclick='window.location="<ls:url address='/admin/weixin/expandConfig/load'/>"'/>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</form:form>
<div align="center">
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/weixin/expandConfig/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
        <display:column title="顺序" class="orderwidth"><%=offset++%>
        </display:column>
        <display:column title="服务说明" property="name"></display:column>
        <display:column title="服务service" property="classService"></display:column>
        <display:column title="消息关键字" property="keyword"></display:column>
        <display:column title="" media="html" style="width:220px">
            <div class="am-btn-toolbar">
                <div class="am-btn-group am-btn-group-xs">
                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weixin/expandConfig/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 修改</button>
                    <button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}')"><span class="am-icon-trash-o"></span> 删除</button>
                </div>
            </div>
        </display:column>
    </display:table>
    <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default"/>
</div>
<table class="am-table am-table-striped am-table-hover table-main">
    <tbody>
    <tr>
        <td align="left">说明：<br> 1.服务service必须存在,不然微信是无法服务
        </td>
    </tr>
    <tr>
    </tr>
    </tbody>
</table>
<script language="JavaScript" type="text/javascript">
    function deleteById(id) {
    	layer.confirm("删除会影响微信服务确定删除 ?", {
   		 icon: 3
   	     ,btn: ['确定','关闭'] //按钮
   	   }, function(){
   		 window.location = "<ls:url address='/admin/weixin/expandConfig/delete/" + id + "'/>";
   	   });
    }
    highlightTableRows("item");

    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>

