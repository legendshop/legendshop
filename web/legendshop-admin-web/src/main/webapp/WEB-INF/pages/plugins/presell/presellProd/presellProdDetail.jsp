<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>预售活动管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
 .auditAuctionImage img{width:100%;}
 #col1{table-layout:fixed;}
#col1 tr td:first-child,#infoTable tr td:first-child{width:20%;}
	.prod-table{
			width:80%;
			margin:10px 0;
			border:solid #efefef;
			border-width:1px 0px 0px 1px;
		}
		.prod-table tr td{
			margin:0px;
			border:solid #efefef;
		}
		.prod-table thead tr td{
			text-align: center;
			background-color:#F9F9F9;
			padding:8px;
			font-size:14px;
			border-width:0px 0px 1px 0px;
		}
		.prod-table thead tr td:last-child{
			border-right:1px solid #efefef;
		}
		.prod-table tbody tr td{
			padding:8px 0px 8px 0px;
			font-size:14px;
			border-width:0px 1px 1px 0px;
		}
		.prod-table tbody tr td a{
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			width: 340px;
			display: inline-block;
		}
 </style> 
</head>
 <body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
        <table id="infoTable" class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr>
				<th class="title-border">
					预售管理&nbsp;＞&nbsp;
					<a href="<ls:url address="/admin/presellProd/query"/>">预售活动管理</a>&nbsp;＞&nbsp;
					<span style="color:#0e90d2; font-size:14px;">查看预售活动详情</span>
				</th>
	    	</tr>
	    </thead>
	    </table>
       <div align="center">
         <table  style="width: 100%" class="${tableclass} no-border content-table" id="col1">
         	
      <tr>
        <td style="width:13%;min-width:185px;">
          <div align="right">方案名称:</div>
       </td>
        <td>
           <a href="${contextPath}/presell/views/${presellProd.id}" target="_blank">${presellProd.schemeName}</a>
        </td>
      </tr>
      <tr>
      <tr>
        <td valign="top">
          <div align="right">预售商品:</div>
       </td>
        <td valign="top">
        	<table class="prod-table see-able content-table" style="margin-top:0;">
        		<thead>
	        		<tr>
						<td width="6%">单品图片</td>
						<td width="45%">单品名称</td>
						<td width="15%">单品属性</td>
						<td width="10%">价格(元)</td>
						<td width="15%">预售价格(元)</td>
	        		</tr>
        		</thead>
        		<tbody>
				<c:if test="${!empty prodLists}">
					<c:forEach items="${prodLists}" var="sku">
						<tr>
							<td id="skuPic" align="center"><img src="<ls:images item='${sku.prodPic}' scale='3'/>" alt="单品图片"/></td>
							<td style="text-align: center;"><a id="skuName" target="_blank" href="${PC_DOMAIN_NAME}/views/${sku.prodId}" >${sku.prodName}&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
							<td id="cnProperties" align="center">${empty sku.cnProperties?'无':sku.cnProperties}</td>
							<td id="skuPrice" align="center">${sku.skuPrice}</td>
							<td id="prsellPrice" align="center">${sku.prsellPrice}</td>
						</tr>
					</c:forEach>
				</c:if>
        		</tbody>
        	</table>
        </td>
      </tr>
<%--       <tr>--%>
<%--        <td>--%>
<%--          <div align="right">预售价格:</div>--%>
<%--       </td>--%>
<%--        <td>--%>
<%--          ${presellProd.prePrice} 元--%>
<%--        </td>--%>
<%--      </tr>--%>
       <tr>
        <td>
          <div align="right">预售时间:</div>
       </td>
        <td>
          <fmt:formatDate value="${presellProd.preSaleStart}" pattern="yyyy-MM-dd HH:mm"/>
           - 
          <fmt:formatDate value="${presellProd.preSaleEnd}" pattern="yyyy-MM-dd HH:mm"/>
        </td>
      </tr>
      <tr>
      	<td>
      		<div align="right">支付类型:</div>
      	</td>
      	<td>
      		${presellProd.payPctType eq 0?'全额支付':'定金支付' }
      	</td>
      </tr>
      <c:if test="${presellProd.payPctType eq 1}">
		<tr>
	        <td>
	          <div align="right">定金:</div>
	       </td>
	        <td>
	          ${presellProd.preDepositPrice} %
	        </td>
	      </tr>
		<tr>
<%--	        <td>--%>
<%--	          <div align="right">定金占百分比: </div>--%>
<%--	       </td>--%>
<%--	        <td>--%>
<%--				<span style="color:#FF4500;"><fmt:formatNumber value="${presellProd.payPct*100}" pattern="#0.0#"/></span> %--%>
<%--	        </td>--%>
	      </tr>
		<tr>
	        <td>
	          <div align="right">尾款时间:</div>
	       </td>
	        <td>
	          <fmt:formatDate value="${presellProd.finalMStart}" pattern="yyyy-MM-dd HH:mm"/>
	           - 
	          <fmt:formatDate value="${presellProd.finalMEnd}" pattern="yyyy-MM-dd HH:mm"/>
	        </td>
	      </tr>
      </c:if>
       <tr>
        <td>
          <div align="right">发货时间:</div>
       </td>
        <td>
          <fmt:formatDate value="${presellProd.preDeliveryTime}" pattern="yyyy-MM-dd HH:mm"/>
        </td>
      </tr>
      <tr>
             <td colspan="2">
                 <div align="center">
                 	<c:if test="${presellProd.status eq -1 and presellProd.preSaleStart gt nowDate}">
	                     <input type="button" class="${btnclass}" id="btn_audit" onclick="showAuditWin('${presellProd.id}')" value="审核"/>
	                </c:if>
	                <c:choose>
						<c:when test="${presellProd.status eq 2}">
	                		<input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/presellProd/query?isExpires=true'" />
	                	</c:when>
	                	<c:otherwise>
	                		<input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/presellProd/query?status=${presellProd.status eq -2?'':presellProd.status}'" />
	                	</c:otherwise>
	                </c:choose>
                 </div>
             </td>
         </tr>
     </table>
     
     <c:if test="${presellProd.status eq -1 and presellProd.preSaleStart gt nowDate}">
	     <div id="auditFrame" style="display:none;font-size:0.8em">
			<p>审核结果<font color="red">*</font>：
				<select id="isAgree" name="isAgree">
					<option value="">请选择</option>
					<option value="true">同意</option>
					<option value="false">不同意</option>
				</select>
			</p>
			审核意见<font color="red">*</font>：
			<textarea id="auditOpinion" style="resize:none;width:250px;height:80px;" maxlength="50" ></textarea>
			 <br>
			 <span style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;需要填写审核意见（50字符以内）</span>
		</div>
     </c:if>
     
     </div>
     </div>
     </div>
     </body>
<c:if test="${presellProd.status eq -1 and presellProd.preSaleStart gt nowDate}">

	<script language="javascript">
		var contextPath = "${contextPath}";
		
		//弹出审核窗口
		function showAuditWin(id){
			layer.open({
	   			title : "审核意见",
	   			type:1,
	   			content: $('#auditFrame'),
	   			area: ['440px', '510px'],
	   			btn: ['确定', '取消'],
	   			yes: function(index, layero){
	   				var isAgree = $("#isAgree").val();
			    	if(!audit(id, $("#auditOpinion").val(), isAgree)){
			    		return false;
			    	}
	   			},
	   			btn2:  function(index, layero){
	   				layer.close(index);
	   			}
			})
			
		}
		
		//审核预售活动
		function audit(id,auditOpinion,isAgree){
				    	if(isBlank(isAgree)){
				    		layer.msg("请输入审核结果!", {icon:0});
				    		return false;
				    	}
				    	if(isBlank(auditOpinion)){
				    		layer.msg("审核意见不能为空", {icon:0});
				    		return false;
				    	}
				    	$.ajax({
							url : contextPath+"/admin/presellProd/audit", 
							data : {"id" : id,"auditOpinion" : auditOpinion,"isAgree" : isAgree},
							type : "GET", 
							async : true, //默认为true 异步   
							dataType : "JSON",
							error : function(data){
								layer.alert("对不起,请求失败,请稍后重试!", {icon:2});
							},   
							success : function(result){
								if(result == "OK"){
									layer.msg("恭喜您,审核成功", {icon:1, time:700}, function(){
										window.location = contextPath + "/admin/presellProd/query";
									});
									return true;
								}else {
									layer.alert(data, {icon:0});
									return false;
								}
							}   
						});
		}
		
		function isBlank(value){
			return value == null || value == undefined || $.trim(value) == "";
		}
	
	</script>
</c:if>
</html>