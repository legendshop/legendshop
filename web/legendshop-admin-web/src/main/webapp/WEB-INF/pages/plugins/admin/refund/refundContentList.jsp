<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<c:choose>
	<c:when test="${not empty requestScope.list}">
		<div align="center">
			<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
			<display:table name="list" requestURI="/admin/returnMoney/query" id="item" export="false" class="${tableclass}" style="width:100%;min-width=1024px" sort="external">
				<display:column title="顺序" class="orderwidth" style="min-width:50px"><%=offset++%></display:column>
				<display:column title="退款编号" property="refundSn" style="min-width:100px"></display:column>
				<display:column title="退款金额" style="min-width:80px">
					<c:choose>
							<c:when test="${item.isRefundDeposit}">
								&#65509;${item.depositRefund + item.refundAmount}
							</c:when>
							<c:otherwise>
								&#65509;${item.refundAmount}
							</c:otherwise>
						</c:choose>
				</display:column>
				<display:column title="申请原因" property="buyerMessage" style="min-width:100px"></display:column>
				<display:column title="申请时间" property="applyTime" format="{0,date,yyyy-MM-dd HH:mm:ss}"></display:column>
				<display:column title="商家处理备注"  style="min-width: 200px; max-width: 300px; word-break:break-all">
					<c:if test="${empty item.sellerMessage }">暂无</c:if>
					<c:if test="${not empty item.sellerMessage }"><div class="order_img_name">${item.sellerMessage }</div></c:if>
				</display:column>
				<display:column title="商家处理时间">
					<c:if test="${empty item.sellerTime }">商家未处理</c:if>
					<c:if test="${not empty item.sellerTime }">
						<fmt:formatDate value="${item.sellerTime}"
							pattern="yyyy-MM-dd HH:mm" />
					</c:if>
				</display:column>
				<display:column title="涉及商品">
					<c:if test="${item.subItemId eq 0 }">
						<c:if test="${item.isPresell}">
							<a  href="${contextPath }/admin/order/presellOrderAdminDetail/${item.subNumber}" title="${item.productName}">${item.productName}</a>
						</c:if>
						<c:if test="${!item.isPresell}">
							<a  href="${contextPath }/admin/order/orderAdminDetail/${item.subNumber}" title="${item.productName }">${item.productName}</a>
						</c:if>
						
					</c:if>
					<c:if test="${item.subItemId ne 0 }">
						<a  href="${PC_DOMAIN_NAME}/views/${item.productId}" title="${item.productName }"><div class="order_img_name">${item.productName}</div></a>
					</c:if>
				</display:column>
				<display:column title="处理状态">
					<c:choose>
						<c:when test="${item.applyState  eq -1}">用户已撤销</c:when>
						<c:when test="${item.applyState  eq 2}">待处理</c:when>
						<c:when test="${item.applyState  eq 3}">处理完成</c:when>
						<c:otherwise>
							暂不需处理
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column title="操作" media="html">
				<div class="table-btn-group">
					<c:choose>
						<c:when test="${item.applyState eq 2}">
							<button id="check" class="tab-btn" onclick="window.location.href='${contextPath}/admin/returnMoney/refundDetail/${item.refundId}'">
								 处理
							</button>
						</c:when>
						<c:otherwise>
							<button id="check" class="tab-btn" onclick="window.location.href='${contextPath}/admin/returnMoney/refundDetail/${item.refundId}'">
								 查看详情
							</button>
						</c:otherwise>
					</c:choose>
				</div>
				</display:column>
			</display:table>
			 <!-- 分页条 -->
	     	<div class="clearfix">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
		</div>
	</c:when>
	<c:otherwise>
		<div style="width: 300px; margin: 50px auto; text-align: center;"> 对不起,没有找到符合条件的记录!</div>
	</c:otherwise>
</c:choose>

