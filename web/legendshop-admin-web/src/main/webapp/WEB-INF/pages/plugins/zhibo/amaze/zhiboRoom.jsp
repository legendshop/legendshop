<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>直播管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<tr>
					<th><strong class="am-text-primary am-text-lg">直播管理</strong> /
						直播房间</th>
				</tr>
			</table>

			<div style="margin-left: 0.5rem">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty seckillActivity.status&& empty seckillActivity.overdue}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/zhibo/zhiboRoomList"/>">全部房间</a></li>
					</ul>
				</div>
			</div>


			<form:form action="${contextPath}/admin/zhibo/zhiboRoomList"
				id="form1" method="get">
				<table class="${tableclass}" style="width: 100%">
					<tbody>
						<tr>
							<td>
								<div align="left" style="padding: 3px">
									<input type="hidden" id="curPageNO" name="curPageNO"
										value="${curPageNO}" /> &nbsp;房间号 <input type="text"
										name="avRoomId" maxlength="50" class="${inputclass}"
										value="${avRoomId}" placeholder="请输入房间号搜索" /> <input
										type="submit" value="搜索" class="${btnclass}" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div align="center">
				<display:table name="list" requestURI="admin/zhibo/zhiboRoomList"
					id="item" export="false" sort="external" class="${tableclass}">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="房间号" property="avRoomId"></display:column>
					<display:column title="主播UID" property="hostUid"></display:column>
					<display:column title="创建日期" property="createTime"
						format="{0,date,yyyy-MM-dd HH:MM:SS}">
					</display:column>
					<display:column title="更新时间" property="modifyTime"></display:column>
					<display:column title="标题" property="title"></display:column>
					<display:column title="房间类型" property="roomType"></display:column>
					<display:column title="操作" media="html">
						<div class="am-btn-toolbar">
							<div class="am-btn-group am-btn-group-xs">
								<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only"
									id="statusImg" style='color: #f37b1d;'
									onclick="javascript:outLine('${item.avRoomId }', '${item.hostUid }');">
									<span class="am-icon-arrow-down">房间下线</span>
								</button>
							</div>
						</div>
					</display:column>
				</display:table>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="simple" />
			</div>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
	function outLine(avRoomId, hostUid) {
		layer.confirm("确定要下线【" + avRoomId + "】么？", {icon:1}, function() {
			$.ajax({
				url : "${contextPath}/admin/zhibo/zhiboRoomOutline/" + avRoomId
						+ "/" + hostUid,
				type : 'get',
				async : false, //默认为true 异步   
				"dataType" : "json",
				error : function() {
					window.location.reload(true);
				},
				success : function(retData) {
					if (retData == "OK") {
						window.location.reload(true);
						return;
					} else {
						layer.msg("操作失败！", {icon:2});
						return;
					}
				}
			});
		});
	}
</script>
</html>
