<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>参数组管理- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">属性管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">参数组管理</span></th>
					</tr>
			</table>
			<form:form action="${contextPath}/admin/paramGroup/query" id="form1"
				method="get">
				<div class="criteria-div">
					<span class="item">
						组名称：<input class="${inputclass}" type="text" name="name" id="name" maxlength="50" value="${paramGroup.name}" /> 
						 <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						 <input class="${btnclass}" type="button" onclick="search()" value="搜索" /> 
						 <input class="${btnclass}" type="button" value="创建" onclick='window.location="<ls:url address='/admin/paramGroup/load'/>"'/>
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/paramGroup/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">

					<display:column title="组名称" property="name" style="width:40%;"></display:column>
					<display:column title="排序" property="seq" style="width:40%;"></display:column>

					<display:column title="操作" media="html" style="width:20%;">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/paramGroup/load/${item.id}'">修改分组</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.id}')">删除</button>
						</div>
					</display:column>
				</display:table>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
			</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1、先建参数组。<br> 2、在类型管理里选择了参数。<br>
						3、修改分到的参数组里。
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm('确定删除？', {icon:3}, function(){
			layer.msg('删除成功', {icon:1, time:700}, function(){
				window.location = "<ls:url address='/admin/paramGroup/delete/" + id + "'/>";
			});
		});
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>
