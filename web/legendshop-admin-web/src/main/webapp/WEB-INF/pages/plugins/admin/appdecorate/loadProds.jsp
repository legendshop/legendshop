 <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<style type="text/css">
		body{
			min-height:96%;
		}
		#form1{
			margin:15px;
			line-height: 28px;
		}	
		input[type="text"]{
			height:28px;
			border: 1px solid #efefef;
		}
		.see-able{
			width: 95%;
			min-width:680px;
		}
		.see-able th,td{
			height:30px !important;
			min-width:50px !important;
		}
		.edit-gray-btn {
		    color: #333;
		    border: 1px solid #efefef;
		    background-color: #f9f9f9;
		    cursor: pointer;
		    height: 28px;
		    text-align: center;
		    line-height: 24px;
		    margin: 0 3px;
		    font-size: 12px;
		    padding: 0 12px;
		    min-width: 50px;
			display: inline-block;
		}
		
	</style>
</head>
<body>
	 <div style="width: 742px;">
	    <form:form action="${contextPath}/admin/app/decorate/loadProds" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<input type="hidden" id="sourceId" name="sourceId" value="${sourceId}" />
	 	<input type="hidden" id="maxChoose" name="maxChoose" value="${maxChoose}" />
	 	<div style="height:50px;width:705px;background-color: #f9f9f9;border: 1px solid #efefef;font-size: 14px;margin: 10px 10px 10px 3px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:605px;border:1px solid #efefef;padding: 5px;"  type="text" name="name" id="name" maxlength="20" value="${name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" value="搜索" class="criteria-btn"/>
			</div>
		</div>
	 </form:form>
	<table class="see-able am-table am-table-striped am-table-hover table" style="margin: 0 auto;">
		<thead>
			<tr><th width="75">图片</th><th width="420">名称</th><th width="125">操作</th></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.name}</td>
          	<td>
          	<!-- <button style="border:0px;"> -->
          	   <a class="edit-gray-btn" target="_blank" href="${contextPath}/views/${product.prodId}">查看</a>
          	<!-- </button> -->
          	&nbsp;
          	<button class="criteria-btn" onclick="saveChoooseProduct('${product.prodId}','${product.pic}','${product.name}','${product.cash}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
     <!-- 分页条 -->
     <%-- <div align="center">
		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
	</div> --%>
     	<div class="fr" style="display: block;width:100%;">
  			 <div class="page">
	   			 <div class="p-wrap" style="font-size:14px;margin-bottom:5px;">
	       		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				 </div>
  			 </div>
		</div> 		
	 </div>
	 
      <script type="text/javascript">
          var sourceId="${sourceId}";
          var maxChoose="${maxChoose}";
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      function saveChoooseProduct(prodid,pic,name,cash){
	 			var ids=new Array();
	 			ids.push(prodid);
	 			var imgs=new Array();
	 			imgs.push(pic);
	 			var names=new Array();
	 			names.push(name);
	 			var prices=new Array();
	 			prices.push(cash);
	 			//保存商品信息
				window.parent.saveChoooseProduct(ids,imgs,names,prices,sourceId);
				var index = parent.layer.getFrameIndex('loadProds'); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭 
			}
      </script>
</body>
</html>