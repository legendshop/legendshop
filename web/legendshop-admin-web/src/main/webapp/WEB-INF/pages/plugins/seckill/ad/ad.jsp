<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
<body>
   
   <jsp:include page="/admin/top" />
   
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			    
				
<script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
<table class="${tableclass} title-border" style="width: 100%">
	<tr>
		<th class="title-border">秒杀管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">秒杀广告</span></th>
	</tr>
</table>
<div>
	<div class="seller_list_title">
		<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
			<li class="am-active">
				<i></i>
				<a href="javascript:void(0)">轮播图列表</a>
			</li>
			<li>
				<i></i>
				<a href="<ls:url address="/admin/seckillAdvertise/update"/>">新增轮播图</a>
			</li>
		</ul>
	</div>
</div>
<div align="center" class="order-content-list" style="margin-top: 15px;margin-bottom: 60px;">
	<c:choose>
		<c:when test="${not empty bannerList}">
			<display:table name="bannerList" requestURI="/admin/seckillAdvertise/index" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
				<display:column title="顺序" class="orderwidth">
		        	${item.seq}
		        </display:column>
				<display:column title="图片">
					<a href="<ls:photo item='${item.imageFile}'/>" target="_blank">
						<img id="uploadFile" src="<ls:photo item='${item.imageFile}'/>" height="145" width="265" />
					</a>
				</display:column>
				<display:column title="图片链接地址">
					<div class="order_img_name">${item.url}</div>
			</display:column>
				<display:column title="操作" media="html">
					<div class="table-btn-group">
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillAdvertise/update?id=${item.id}'">
							编辑
						</button>
						<span class="btn-line">|</span>
						<button id="check" class="tab-btn delete" onclick="deleteBanner(${item.id})">
							删除
						</button>
					</div>
				</display:column>
			</display:table>
		</c:when>
		<c:otherwise>
			<div style="width: 300px; margin: 50px auto; text-align: center;">对不起,没有找到符合条件的记录!</div>
		</c:otherwise>
	</c:choose>
</div>
<script language="JavaScript" type="text/javascript">
			var contextPath='${contextPath}';
			function deleteBanner(id){
				
				layer.confirm("确定要删除吗？", {icon:3}, function() {
					$.ajax({
						url : contextPath+"/admin/seckillAdvertise/delete/"+id,
						type : 'post',
						async : false, //默认为true 异步   
						dataType : "json",
						error : function(jqXHR, textStatus, errorThrown) {
						},
						success : function(data) {
							if('OK'==data){
								layer.msg('操作成功'); 
								setInterval(function(){
					  				location.reload();
								},500);
							}else{
								layer.alert(data, {icon:0});
								return;
							}
						}
					});
				});
			}
			
		</script>
</body>

				
		     </div>
	 </div>
</body>
<script type="text/javascript">
	var contextPath='${contextPath}';
</script>

</html>
