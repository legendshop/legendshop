<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>咨询详情 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	        <table class="${tableclass} no-border" style="width: 100%;">
	        	<thead>
			    	<tr> 
			    		<th class="no-bg title-th">
			            	<span class="title-span">
								咨询管理  ＞
								<a href="<ls:url address="/admin/productConsult/query"/>">咨询管理</a>
								  ＞  <span style="color:#0e90d2;">回复商品咨询</span>
							</span>
			            </th>
			    	</tr>
			    </thead>
	        </table>
        <form:form  action="${contextPath}/admin/productConsult/save" method="post" id="form1">
            <input id="consId" name="consId" value="${productConsult.consId}" type="hidden">
            <div align="center">
            <table  align="center" class="${tableclass} no-border content-table" id="col1">
			      <tr>
			        <td style="width:200px;">
			          <div align="right">商品：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           ${productConsult.prodName}
			        </td>
			      </tr>
			     <tr>
			        <td>
			          <div align="right">咨询人：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           ${productConsult.nickName}
			        </td>
			      </tr>
			      <tr>
			        <td>
			          <div align="right">咨询类型：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			        <ls:optionGroup type="label" required="false" cache="true"
				                beanName="CONSULT_TYPE" selectedValue="${productConsult.pointType}" defaultDisp=""/>
			        </td>
			      </tr>
			     <tr>
			        <td>
			          <div align="right">咨询内容：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           ${productConsult.content}
			        </td>
			      </tr>
			     <tr>
			        <td>
			          <div align="right">咨询时间：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           <fmt:formatDate value="${productConsult.recDate}" pattern="yyyy-MM-dd HH:mm"/>
			        </td>
			      </tr>
			         <tr>
			        <td>
			        <div align="right">评论IP：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           ${productConsult.postip}
			        </td>
			      </tr>
			      <c:if test="${productConsult.ansUserName != null }">
			       <tr>
			        <td>
			        <div align="right">回复：</div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           回复人：${productConsult.ansUserName}
			           回复时间： <fmt:formatDate value="${productConsult.answertime}" pattern="yyyy-MM-dd HH:mm"/>
			        </td>
			      </tr>
			      </c:if>
			     <tr>
			        <td valign="top">
			          <div align="right"><font color="ff0000">*</font>回复内容： </div>
			       </td>
			        <td align="left" style="padding-left: 2px;">
			           <c:choose>
				        	<c:when test="${productConsult.replySts==1}">
								${productConsult.answer}
							</c:when>
				            <c:otherwise>  
				            	<textarea rows="5" cols="50" id="answer" name="answer" maxlength="100">${productConsult.answer}</textarea>
				            </c:otherwise>
				        </c:choose>
			        </td>
			      </tr>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <c:choose>
                            	<c:when test="${not empty productConsult.answer and productConsult.replySts eq 1}">
                            	   <%-- <input class="${btnclass}" type="button" value="返回"
                                       onclick="window.location='${contextPath}/admin/productConsult/query?replyed=1'" /> --%>
                                   <input class="${btnclass}" type="button" value="返回" onclick="window.history.back(-1)" />
                            	</c:when>
                            	<c:otherwise>
                            			<input class="${btnclass}" type="submit" value="保存" />
                          				 
                            	        <%-- <input class="${btnclass}" type="button" value="返回"
                                       onclick="window.location='${contextPath}/admin/productConsult/query?replyed=0'" /> --%>
                                       <input class="${btnclass}" type="button" value="返回" onclick="window.history.back(-1)" />
                            	</c:otherwise>
                            </c:choose>
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script language="javascript">
    $.validator.setDefaults({
    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            answer: {
                required: true,
                minlength: 5,
                maxlength:300
            }
        },
        messages: {
            answer: {
                required: "请输入回复内容",
                minlength: "最少不能少于5个字",
                maxlength: "最大不能超过300字"
            }
        }
    });
 
			highlightTableRows("col1");
          //斑马条纹
     	   $("#col1 tr:nth-child(even)").addClass("even");	  
});
</script>
</html>
