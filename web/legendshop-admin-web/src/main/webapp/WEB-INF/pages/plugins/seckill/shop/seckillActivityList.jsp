<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">秒杀管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">秒杀活动管理</span></th>
				</tr>
			</table>

			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty seckillActivity.status&& empty seckillActivity.overdue}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/seckillActivity/query"/>">全部活动</a>
						</li>
						<li
							<c:if test="${seckillActivity.status eq -1}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/seckillActivity/query?status=-1"/>">待审核</a>
						</li>
						<li
							<c:if test="${seckillActivity.status eq 0}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/seckillActivity/query?status=0"/>">已失效</a>
						</li>
						<li
							<c:if test="${seckillActivity.status eq 1}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/seckillActivity/query?status=1"/>">进行中</a>
						</li>
						<li
							<c:if test="${seckillActivity.status eq 4}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/seckillActivity/query?status=4"/>">已完成</a>
						</li>
						<li id="passedTab"
							<c:if test="${not empty seckillActivity.overdue}"> class="am-active"</c:if>>
							<i></i><a
							href="<ls:url address="/admin/seckillActivity/query?overdue=${nowDate}"/>">已过期</a>
						</li>
					</ul>
				</div>
			</div>


			<form:form action="${contextPath}/admin/seckillActivity/query" id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						店铺名称：
						<input type="text" id="shopId" name="shopId" value="${seckillActivity.shopId}" />
						<input type="hidden" id="shopName" name="shopName" maxlength="50" class="${inputclass}" value="${seckillActivity.shopName}"/>
						<input type="hidden" id="overdate" name="overdate" value="${seckillActivity.overdate}" maxlength="50" class="${inputclass}"/>
					</span>
					<span class="item">
						活动名称：
						<input type="text" id="seckillTitle" name="seckillTitle" maxlength="50" value="${seckillActivity.seckillTitle}"
							class="${inputclass}" placeholder="请输入活动名称搜索" /> 
					</span>
					<span class="item">
						开始时间：
						<input readonly="readonly" class="${inputclass}" name="startTime" id="startTime" class="Wdate" type="text"
							value='<fmt:formatDate value="${seckillActivity.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
					</span>
					<span class="item">
						结束时间：
						<input readonly="readonly" class="${inputclass}" name="endTime" id="endTime" class="Wdate" type="text"
							value='<fmt:formatDate value="${seckillActivity.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
						<input type="hidden" id="status" name="status" value="${seckillActivity.status}" /> 
						<input type="hidden" id="overdue" name="overdue" value="<fmt:formatDate value="${seckillActivity.overdue}" pattern="yyyy-MM-dd"/>" />
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
						<ls:plugin pluginId="b2c">
							<input type="button" value="创建" class="${btnclass}"
								onclick='window.location="<ls:url address='/admin/seckillActivity/load'/>"' />
						</ls:plugin>
					</span>
				</div>
			</form:form>
			<div align="center" id="seckillActivityContentList" class="order-content-list"></div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		sendData(curPageNO);
	    }
	var shopName = $("#shopName").val();
	$(document).ready(function () {
		if(shopName == ''){
	        makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	function sendData(curPageNO) {
	    var data = {
    		"shopId": $("#shopId").val(),
	        "curPageNO": curPageNO,
	        "seckillTitle": $("#seckillTitle").val(),
	        "startTime": $("#startTime").val(),
	        "endTime": $("#endTime").val(),
	        "endTime": $("#endTime").val(),
	        "status": $("#status").val(),
	        "overdue": $("#overdue").val(),
	        "overdate": $("#overdate").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/seckillActivity/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#seckillActivityContentList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	
	function Delete(id, name) {
		layer.confirm('确定要删除【 " + name + " 】吗？', {icon:3}, function(){
			window.location.href = '${contextPath}'
				+ "/admin/seckillActivity/delete/" + id;
		});
	}
	/*function Off(id, name, status) {
		if (id == null || status == null) {
			return;
		}
		var desc;
		if (status == 0) {
			desc = "确定要下线【" + name + "】吗";
		} else {
			desc = "确定要上线【" + name + "】吗";
		}
		layer.confirm(desc, {icon:3}, function(){
			jQuery.ajax({
				url : "${contextPath}/admin/seckillActivity/updateStatus/"
						+ id + "/" + status,
				type : 'get',
				async : false, //默认为true 异步  
				dataType : 'json',
				error : function(data) {
				},
				success : function(data) {
					if (data == "OK") {
						if (status == 1) {
							$('#statusImg').html("下线");
						} else if (status == 0) {
							$('#statusImg').html("上线");
						}
						window.location.reload();
					} else {
						layer.msg("操作失败！", {icon:2});
					}
				}
			});
		});
	}*/

	function offlineSeckillActivity(id) {
		layer.confirm("是否确定进行终止操作?", {icon:3}, function() {
			$.ajax({
				url : contextPath + "/admin/seckillActivity/offlineSeckillActivity/" + id,
				type : 'post',
				async : false, //默认为true 异步
				dataType : "json",
				error : function(jqXHR, textStatus, errorThrown) {
				},
				success : function(retData) {
					if (retData == "OK") {
						window.location.reload(true);
						return;
					} else {
						layer.alert(retData, {icon:0});
						return;
					}
				}
			});
		});
	}
	
	laydate.render({
		   elem: '#startTime',
		   type:'datetime',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
		  });
		   
		 laydate.render({
		   elem: '#endTime',
		   type:'datetime',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	 });
</script>

</html>
