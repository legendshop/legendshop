<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
.group-desc img {
	width: 100%
}
.auditAuctionImage img{width:100%;}
#col1{table-layout:fixed;}
#col1 tr td:first-child,#infoTable tr td:first-child{width:20%;}
.prod-table{
	width:80%;
	margin:10px 0;
	border:solid #efefef;
	border-width:1px 0px 0px 1px;
}
.prod-table tr td{
	margin:0px;
	border:solid #efefef;
}
.prod-table thead tr td{
	text-align: center;
	background-color:#F9F9F9;
	padding:8px;
	font-size:14px;
	border-width:0px 0px 1px 0px;
}
.prod-table thead tr td:last-child{
	border-right:1px solid #efefef;
}
.prod-table tbody tr td{
	padding:8px 0px 8px 0px;
	font-size:14px;
	border-width:0px 1px 1px 0px;
}
.prod-table tbody tr td a{
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	width: 340px;
	display: inline-block;
}
	</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

			<table class="${tableclass} no-border" style="width: 100%">
				<tr>
					<th class="no-bg title-th">
						<span class="title-span">
							团购管理  ＞  
							<a href="<ls:url address="/admin/group/query"/>">团购活动管理</a>
							  ＞  <span style="color:#0e90d2;">查看团购活动</span>
						</span>
			        </th>
				</tr>
			</table>
			<div align="center">
				<table style="width: 100%" class="${tableclass} no-border" id="col1">
					<%--           <c:if test="${group.status == -1 && group.endTime>nowDate}"> --%>
					<!-- 		          <tr> -->
					<!-- 		        	<td> -->
					<!-- 		             <div align="right">操作:</div> -->
					<!-- 		             </td> -->
					<!-- 		            <td> -->
					<!-- 		                <div align="left"> -->
					<%-- 							    <input type="button" class="${btnclass}" id="btn_audit" onclick="audit('${group.id}')" value="审核"/> --%>
					<%-- 						      <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/group/query?status=${group.status}'" /> --%>
					<!-- 		                 </div> -->
					<!-- 		             </td> -->
					<!-- 		         </tr> -->
					<%--           </c:if> --%>
					<tr>
						<td style="width:13%;min-width:185px;">
							<div align="right">活动名称:</div>
						</td>
						<td align="left">${group.groupName}</td>
					</tr>
					<tr>
						<td valign="top">
							<div align="right">活动图片:</div>
						</td>
						<td align="left">
							<a href="<ls:photo item='${group.groupImage}'/>" target="_blank"><img src="<ls:photo item='${group.groupImage}'/>" style="max-height: 150px; max-width: 150px;"></a>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">店铺名称:</div>
						</td>
						<td align="left">${group.shopName}</td>
					</tr>
					<tr>
						<td valign="top">
							<div align="right">活动商品:</div>
						</td>
						<td align="left">
							<c:if test="${not empty prodLists}">
								<table class="prod-table see-able content-table" style="margin-top:0;">
									<tbody>
									<tr style="background: #f9f9f9;">
										<td width="70px">商品图片</td>
										<td width="170px">商品名称</td>
										<td width="100px">商品规格</td>
										<td width="100px">商品价格</td>
										<td width="100px">可销售库存</td>
										<td width="100px">团购价(元)</td>
									</tr>
									<c:choose>
										<c:when test="${not empty prodLists}">
											<c:forEach items="${prodLists}" var="SkuList">
												<c:forEach items="${SkuList.dtoList}" var="groupSku">
													<tr class="sku" >
														<td>
															<img src="<ls:images item='${groupSku.prodPic}' scale='3' />" >
														</td>
														<td>
															<span class="name-text">${groupSku.prodName}</span>
														</td>
														<td>
															<span class="name-text">${empty groupSku.cnProperties?'暂无':groupSku.cnProperties}</span>
														</td>
														<td>
															<span class="name-text">${groupSku.skuPrice}</span>
														</td>
														<td class="prodStocks_p1">${groupSku.skuStocks}</td>
														<td>
															<input type="text" disabled class="set-input groupPrice" id="groupPrice" name="groupPrice" style="text-align: center" value="${groupSku.groupPrice}">
														</td>
													</tr>
												</c:forEach>
											</c:forEach>

										</c:when>
										<c:otherwise>
											<tr class="first">
												<td colspan="7" >暂未选择商品</td>
											</tr>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</c:if>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">每人限购数:</div>
						</td>
						<td align="left">${group.buyQuantity}</td>
					</tr>
					<tr>
				        <td>
				          <div align="right">成团人数:</div>
				       </td>
				        <td align="left">
				            ${group.peopleCount}
				        </td>
				      </tr>
<%--					<tr>--%>
<%--						<td>--%>
<%--							<div align="right">团购价格:</div>--%>
<%--						</td>--%>
<%--						<td align="left">${group.groupPrice}</td>--%>
<%--					</tr>--%>
					<tr>
						<td>
							<div align="right">活动状态:</div>
						</td>
						<td align="left"><c:choose>
								<c:when test="${group.status eq -2 && group.endTime > nowDate}">
									<span style="color: red;">未通过</span>
								</c:when>
								<c:when test="${group.status eq -1 && group.endTime > nowDate}">
									<span style="color: red;">审核中</span>
								</c:when>
							</c:choose> <c:choose>
								<c:when test="${group.status eq 0 && group.endTime > nowDate}">
									<span style="color: red;">未上线</span>
								</c:when>
								<c:when test="${group.status eq 1 && group.endTime > nowDate}">
									<c:if test="${group.startTime <= nowDate}">
										<span style="color: red;">上线中</span>
									</c:if>
									<c:if test="${group.startTime > nowDate}">
										<span style="color: red;">未开始</span>
									</c:if>
								</c:when>
								<c:when test="${group.status eq 2 && group.endTime > nowDate}">
									<span style="color: red;">已完成</span>
								</c:when>
								<c:when test="${group.endTime < nowDate}">
									<span style="color: red;">已过期</span>
								</c:when>
							</c:choose></td>
					</tr>

					<tr>
						<td>
							<div align="right">开始时间:</div>
						</td>
						<td align="left"><fmt:formatDate value="${group.startTime}"
								pattern="yyyy-MM-dd HH:mm:ss" var="startTime" /> ${startTime}</td>
					</tr>
					<tr>
						<td>
							<div align="right">结束时间:</div>
						</td>
						<td align="left"><fmt:formatDate value="${group.endTime}"
								pattern="yyyy-MM-dd HH:mm:ss" var="endTime" /> ${endTime}</td>
					</tr>

					<c:if test="${not empty group.auditOpinion}">
						<tr>
							<td>
								<div align="right">审核意见:</div>
							</td>
							<td align="left">${group.auditOpinion}</td>
						</tr>
						<tr>
							<td>
								<div align="right">审核时间:</div>
							</td>
							<td align="left"><fmt:formatDate value="${group.auditTime}"
									pattern="yyyy-MM-dd HH:mm:ss" var="auditTime" /> ${auditTime}</td>
						</tr>
					</c:if>

					<tr>
						<td valign="top">
							<div align="right">团购描述:</div>
						</td>
						<td class="group-desc" align="left" valign="top">
							${group.groupDesc}
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div align="center">
								<input type="button" class="${btnclass}" value="返回"
									onclick="javascript :history.go(-1)" />
								<c:if test="${group.status == -1 && group.endTime>nowDate}">
									<input type="button" class="${btnclass}" id="btn_audit"
										onclick="audit('${group.id}')" value="审核" />
								</c:if>

								<c:if
									test="${seckillActivity.status == -1 && seckillActivity.endTime>nowDate}">
									<input type="button" class="${btnclass}" id="btn_audit"
										onclick="audit('${seckillActivity.id}')" value="审核" />
								</c:if>
							</div>
						</td>
					</tr>
				</table>
			</div>


			<div id="auditFrame" style="display: none;">
				<div   style="font-size: 0.8em; padding: 10px;">
					<p>
						审核结果<span style="color: red">*</span>： <select id="auditResult">
							<option value="">请选择</option>
							<option value="1">同意</option>
							<option value="-2">不同意</option>
						</select>
					</p>
					审核意见<span style="color: red">*</span>：
					<textarea id="auditOpinion" style="resize: none; width: 250px; height: 80px;" maxlength="50"></textarea><br>
					<span style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;需要填写审核意见（50字符以内）</span>

				</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	jQuery(document).ready(function() {
		$('.group-desc img').attr("style", "width: 480px;height: 360px");
	});
	// $("#col1 tr td div").first().css("width","80px");
	//弹出审核窗口
	function audit(id) {
		layer.open({
			id : 'audit', 
			title : '审核意见',
			content: $('#auditFrame').html(), 
			type:1,
			btn: ['确定', '取消'], 
			yes: function(index, layero){
				var result = $(layero).find('#auditResult').val();
				if (!auditmethod(id,  $(layero).find("#auditOpinion").val(), result)) {
					return false;
				}
			}
		});
	}

	//审核方法	
	function auditmethod(id, auditOpinion, status) {
		if (status == null || status == "" || status == undefined) {
			layer.msg("审核结果不能为空", {icon:0});
			return false;
		}
		if (auditOpinion == null || auditOpinion == "" || auditOpinion == " " || auditOpinion == undefined) {
			layer.msg("审核意见不能为空", {icon:0});
			return false;
		}
		$.ajax({
			url : contextPath + "/admin/group/audit",
			data : {
				"id" : id,
				"auditOpinion" : auditOpinion,
				"status" : status
			},
			type : 'post',
			async : true, //默认为true 异步   
			dataType : 'json',
			error : function(data) {
				layer.msg("程序异常", {icon:2});
			},
			success : function(data) {
				if (data == "OK") {
					layer.msg("审核成功", {icon:1, time:700}, function(){
						window.location = '${contextPath}/admin/group/query'
					});
					return true;
				}else if(data == "fail") {
					layer.msg("审核失败，请检查审核意见是否正常填写", {icon:0});
					return false;
				} else {
					layer.msg(data, {icon:0});
					return false;
				}
			}
		});
	}
</script>
</html>