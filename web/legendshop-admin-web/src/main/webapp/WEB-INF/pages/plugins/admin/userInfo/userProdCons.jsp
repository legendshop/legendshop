<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
               		</th>
				</tr>
			</table>
			<div class="user_list">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userVisitLog/${userName}?userId=${userId}"/>">会员浏览历史</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userProdComm/${userId}?userName=${userName}"/>">会员商品评论</a></li>
						<ls:plugin pluginId="integral">
							<li><i></i><a
								href="<ls:url address="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}"/>">会员积分明细</a></li>
						</ls:plugin>
						<ls:plugin pluginId="predeposit">
							<li><i></i><a
								href="<ls:url address="/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}"/>">会员预存款明细</a></li>
						</ls:plugin>
						<li class=" am-active"><i></i><a href="">会员商品咨询</a></li>
					</ul>
				</div>
			</div>
			<form:form
				action="${contextPath}/admin/userinfo/userProdCons/${userName}?userId=${userId}"
				id="form1" method="post">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							店铺名称&nbsp;<input class="${inputclass}" type="text" name="siteName" maxlength="50" value="${productConsult.siteName}" />
						</span>
						<span class="item">
							商品名称&nbsp;<input class="${inputclass}" type="text" name="prodName" maxlength="50" value="${productConsult.prodName}" /> 
						</span>
						<span class="item">
							类型&nbsp; <select id="pointType" name="pointType" class="criteria-select"><ls:optionGroup
									type="select" required="false" cache="true"
									beanName="CONSULT_TYPE"
									selectedValue="${productConsult.pointType}" /></select> 
						</span>
						<span class="item">
							回复状态&nbsp; <select
								name="replySts" class="criteria-select">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="YES_NO" selectedValue="${productConsult.replySts}" />
							</select>
						</span>
						<span class="item">
							开始时间&nbsp; <input readonly="readonly" name="startTime"
								id="startTime"  type="text"
								value='<fmt:formatDate value="${productConsult.startTime}" pattern="yyyy-MM-dd"/>' />
						</span>
						<span class="item">
							结束时间&nbsp;<input readonly="readonly" name="endTime"
								id="endTime" type="text" 
								value='<fmt:formatDate value="${productConsult.endTime}" pattern="yyyy-MM-dd"/>' />
						</span>
						<span class="item">
							<input type="submit" value="搜索" class="${btnclass}" />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/productConsult/query"
					id="item" export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="店铺名称">
					${item.siteName}
				</display:column>
					<display:column title="商品">
						<a href="${PC_DOMAIN_NAME}/views/${item.prodId}"
							target="_blank" title="${item.prodName}">${item.prodName}</a>
					</display:column>
					<display:column title="咨询内容" property="content"></display:column>
					<display:column title="咨询类型" style="width:80px">
						<ls:optionGroup type="label" required="false" cache="true"
							beanName="CONSULT_TYPE" selectedValue="${item.pointType}"
							defaultDisp="" />
					</display:column>
					<display:column title="时间">
						<fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<display:column title="回复状态">
						<c:choose>
							<c:when test="${item.replySts==1}">已回复</c:when>
							<c:otherwise>未回复</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html" style="width:145px;">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.replySts==1}">
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/productConsult/load/${item.id}'">
										查看
									</button>
									<span class="btn-line">|</span>
								</c:when>
								<c:otherwise>
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/productConsult/load/${item.id}'">
										回复
									</button>
									<span class="btn-line">|</span>
								</c:otherwise>
							</c:choose>
							<button class="tab-btn" onclick="deleteById('${item.id}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<c:if test="${empty list}">
					<div
						style="background: #f9f9f9; padding: 20px; border: 1px solid #efefef; height: 60px; width: 99%;">未找到相应记录~</div>
				</c:if>
				<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		if (confirm("  确定删除 ?")) {
			window.location = "${contextPath}/admin/productConsult/delete/"
					+ id;
		}
	}
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	highlightTableRows("item");
	
	 laydate.render({
   		 elem: '#startTime',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endTime',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
</script>
</html>
