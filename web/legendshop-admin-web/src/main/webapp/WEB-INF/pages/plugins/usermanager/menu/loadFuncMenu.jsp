<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript">
	//保存菜单和权限的对应关系
	function saveFunctionMenu(){
			var zTree = $.fn.zTree.getZTreeObj("menuTrees");
			var chkNodes = zTree.getCheckedNodes(true);
			if(chkNodes.length<1) {
				layer.msg("请选择菜单",{icon: 2});
				return;
			}
			var menuId = chkNodes[0].id;
			var menuName = chkNodes[0].name;
			
			parent.loadMenuDialogCallBack(menuId,menuName);
			
			 var index = parent.layer.getFrameIndex("loadmenu"); //先得到当前iframe层的索引
			 parent.layer.close(index); //再执行关闭   
	}
	
		function cancelFunctionMenu(){
			parent.calcelMenuDialogCallBack();
			var index = parent.layer.getFrameIndex("loadmenu"); //先得到当前iframe层的索引
			 parent.layer.close(index); //再执行关闭
	}

	$(document).ready(function(){
			//zTree设置
			var setting = {
					check:{
							enable: true,
							chkboxType: {'Y':'ps','N':'ps'},
							chkStyle: 'radio',
							radioType: 'all'
							}, 
					data:{
						simpleData:{ enable: true }
					}, callback:{
						beforeCheck: beforeCheck,
						onCheck: onCheck
					} };
			$.fn.zTree.init($("#menuTrees"), setting, ${menuList});
	});
	
		function beforeCheck(){
		}
		    
		function onCheck(){
		}
</script>
</head>
<body>
     	<fieldset style="padding-bottom: 30px; padding-top: 15px;">
			<legend>菜单</legend>
			<div id="menuContentList" style="margin:10px 0px 10px 10px" > 
		   		<ul id="menuTrees" class="ztree"></ul>
	     	</div>
		</fieldset>
		<div align="center">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:saveFunctionMenu();" value="保存" id="Button1" name="Button1">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:cancelFunctionMenu();" value="清除" id="Button1" name="Button1">
		</div>
</body>
</html>

