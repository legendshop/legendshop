<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
        <script language="javascript">
    $.validator.setDefaults({
    });

    $(document).ready(function() {
    jQuery("#updateFunction").validate({
        rules: {
            name: {
                required: true,
                maxlength:50
            },
            protectFunction: {
		        required: true,
		        maxlength:50
		    },
		    note:{
		    	 required: true,
		    	 maxlength:50
		    },
		    url : {
		    	maxlength:1024
		    }
        },
        messages: {

          	 name: {
                required: '必填'
            },
            protectFunction: {
                required: '必填'
            },
            note:{
		    	 required: "必填"
		    }
        }
    });
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
     	 
     	 //binding Submit
     	 $("#Submit").click(function(){
     	 if($("#updateFunction").valid()){
     	 var formData = $("#updateFunction").serialize();
     	   $.ajax({
            type: 'post', // 提交方式 get/post
            url:  "${contextPath}/admin/member/right/save", // 需要提交的 url
            data: formData,
            error: function(jqXHR, textStatus, errorThrown) {
		 		 layer.alert("保存失败，已经存在同名权限",{icon: 2});
			},
            success: function(data) { // data 保存提交后返回的数据，一般为 json 数据
                // 此处可对 data 作相关处理
                closeDialog();
            }
        });
        return false; // 阻止表单自动提交事件
     	 }
		});
});


 	//加载菜单用于选择
	function loadMenuDialog(){
		var menuId = $("#menuId").val();
		var page = "${contextPath}/admin/member/right/loadFunctionMenu?menuId="+menuId;
		layer.open({
			title :"关联权限菜单",
			  type: 2, 
			  id: "loadmenu",
			  content: [page], 
			  area: ['280px', '350px']
			});
	}
	
	function loadMenuDialogCallBack(id,menuName){
			$("#menuId").val(id);
			$("#menuName").html("<b>" + menuName + "</b>");
	}
	//清空选项
	function calcelMenuDialogCallBack(id,menuName){
			$("#menuId").val("");
			$("#menuName").html("尚未选择");
	}	
	
	function closeDialog(){
	    var index = parent.layer.getFrameIndex("func"); //先得到当前iframe层的索引
	    parent.layer.close(index); //再执行关闭   
	}
	
</script>
</head>


<body>
<div align="center">      
      <form:form  action="${contextPath}/admin/member/right/save" id="updateFunction" method="post">
      	<!-- 默认为后台应用 -->
		<input type="hidden" name="appNo" value="BACK_END"> 
		<!-- 默认为系统角色 -->
		<input type="hidden" name="category" value="2">
        <table align="center" class="${tableclass}" id="col1" style="width: 600px">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center"> 权限</div>
                        </th>
                    </tr>
                </thead>
		 <tr style="display: none">
            <td width="100px" height="29" align="center" class="forumRow"> 主键：</td>
            <td align="center" class="forumRow">
			<input type="text" name="id" value="${bean.id }" readonly="true">
			</td>
        </tr>
        <tr>
            <td  width="100px" height="27" align="center" class="forumRow">
     <div  align="right">名称 <font color="ff0000">*</font></div></td>
            <td align="center" class="forumRow"><input type="text" name="name" value="${bean.name}"></td>
          </tr>
          <tr>
            <td height="27" align="center" class="forumRow"><div align="right">权限名称 <font color="#ff0000">*</font></div></td>
            <td align="center" class="forumRow"><input type="text" name="protectFunction" value="${bean.protectFunction }"/></td>
          </tr>
            <%-- <tr>
		        <td align="center"  class="forumRow"  style="text-align: right;">所属应用</td>
		        <td>
		              <select id="appNo" name="appNo">
						  <ls:optionGroup type="select" required="true" cache="true"
			                beanName="APP_NO" selectedValue="${bean.appNo}"/>
			            </select>
		        </td>
		    </tr>
          <tr>
            <td height="27" align="center" class="forumRow"><div align="right">权限类型 <font color="#ff0000">*</font></div></td>
            <td align="center" class="forumRow">
			     <select id="category" name="category">
				 		 <ls:optionGroup type="select" required="true" cache="true" beanName="FUNCTION_CATEGORY" selectedValue="${bean.category}"/>
	            </select>
	            权限角色只有管理员可以操作
            </td>
          </tr> --%>
          <tr>
            <td height="27" align="center" class="forumRow"><div align="right">URL地址：</div></td>
            <td align="center" class="forumRow"><input type="text" name="url" value="${bean.url}" size="60">
            <br/>多个URL用英文逗号隔开</td>
          </tr>
          <tr>
            <td height="27" align="center" class="forumRow"><div align="right">关联菜单：</div></td>
            <td align="center" class="forumRow">
            	<input type="hidden" id="menuId"  name="menuId" value="${bean.menuId}"  />
            	<span id="menuName" name="menuName">
            		 <c:choose>
	                	<c:when test="${not empty menu}">${menu.name}</c:when>
	                	<c:otherwise>尚未选择</c:otherwise>
	                </c:choose>
            	</span>
            	<input type="button" value="选择" onclick="javascript:loadMenuDialog()" />
            </td>
          </tr>
          <tr>
            <td height="27" align="center" class="forumRow"><div align="right">显示Label：<font color="#ff0000">*</font></div></td>
            <td align="center" class="forumRow"><input type="text" name="note" value="${bean.note}"></td>
          </tr>
          <tr >
            <td height="42" colspan="2" class="forumRow">
                  <div align="center">
                   <input type="button" name="Submit" id="Submit" value="保存">
                   <input type="button" value="关闭"
                       onclick="javascript:closeDialog()" />
                   </div>
            </td>
          </tr>
</table>
      </form:form>
</div>
&nbsp; 
</body>
</html>

