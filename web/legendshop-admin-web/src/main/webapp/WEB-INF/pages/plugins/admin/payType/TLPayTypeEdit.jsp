<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="../back-common.jsp"%>

<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
 <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/amaze/css/payEdit.css'/>" />
 <%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>

	<form id="payForm" action="${contextPath}/admin/paytype/payEditTongLian" method="post" style="max-height: 650px;height:330px; overflow: auto;" enctype="multipart/form-data">
		
		<input type="hidden" name="isEnable" id="isEnable" value="${payType.isEnable}"/>   
		
		<div class="modal-body-main filll fillr" style="height:auto;"> 
			<div style="padding-top:20px;">
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label class="">支付类型</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<span>通联支付</span>
						</div>
					</div>
				</div>
				
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="key" class="ant-form-item-required">商户号(merchantId)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="text" id="merchantId" name="merchantId"  value="${jsonObject['merchantId']}" class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
				
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label for="key" class="ant-form-item-required">安全校验码(Key)</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<input type="password" id="key" name="key"  value="${jsonObject['key']}" class="ant-input ant-input-lg">
						</div>
					</div>
				</div>
				
				<div class="ant-row ant-form-item">
					<div class="ant-col-7 ant-form-item-label">
						<label class="">商户支付签名文件</label>
					</div>
					<div class="ant-col-13">
						<div class="ant-form-item-control ">
							<span class=""><div
									class="ant-upload ant-upload-select ant-upload-select-text">
									<span tabindex="0" class="ant-upload" role="button">
									<input  type="file" name="file" style="color: #cccccc;" accept=".cer"  >
									</span>
								</div>
								<div class="ant-upload-list ant-upload-list-text">
								</div>
							</span>
						</div>
					</div>
			   </div>
	
				
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="accountState" class="">状态</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control has-success">
						<div class="ant-radio-group ant-radio-group-large">
							<label class="ant-radio-wrapper"><span
								class="ant-radio <c:if test="${payType.isEnable eq 0}"> ant-radio-checked</c:if>">
								
								<input type="radio"
									class="ant-radio-input" value="0"  <c:if test="${payType.isEnable eq 0}"> checked="checked"</c:if>  ><span
									class="ant-radio-inner"></span> </span><span>关闭</span> 
						   </label>
									
							<label
								class="ant-radio-wrapper"><span class="ant-radio <c:if test="${payType.isEnable eq 1}"> ant-radio-checked</c:if>">
								<input
									type="radio" class="ant-radio-input" value="1"  <c:if test="${payType.isEnable eq 1}"> checked="checked"</c:if> >
									<span
									class="ant-radio-inner"></span> </span><span>启用</span>
							</label>
									
									
						</div>
					</div>
				</div>
			</div>
			<input type="submit" id="editBtn" value = "提交更改" class="criteria-btn"  />
		</div>
	</form>
	<script src="${contextPath}/resources/common/js/jquery.form.js"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/TLPayTypeEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
</script>

</body>
</html>