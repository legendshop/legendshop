<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>订单管理- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<div class="seller_right" style="margin-bottom:70px;">
				<table class="${tableclass} title-border" style="width: 100%">
						<tr><th class="title-border">订单管理 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">订单管理<span></th></tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li <c:if test="${empty paramDto.status}"> class="this am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing"/>">所有订单</a>
							</li>
							<li <c:if test="${paramDto.status==1}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing?status=1"/>">已经提交</a>
							</li>
							<li <c:if test="${paramDto.status==2}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing?status=2"/>">已经付款</a>
							</li>
							<li <c:if test="${paramDto.status==3}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing?status=3"/>">已经发货</a>
							</li>
							<li <c:if test="${paramDto.status==4}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing?status=4"/>">已经完成</a>
							</li>
							<li <c:if test="${paramDto.status==5}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/order/processing?status=5"/>">已经取消</a>
							</li>
						</ul>
					</div>
					<div>
						<form:form id="ListForm" method="get" action="${contextPath}/admin/order/processing">
							<input type="hidden" value="${paramDto.curPageNO==null?1:paramDto.curPageNO}" id="curPageNO" name="curPageNO">
							<input type="hidden" value="${paramDto.status}" id="status" name="status">
							<div class="criteria-div">
								<span class="item">
									<input type="text" id="shopId" name="shopId" value="${paramDto.shopId}"/> 
								</span>
								<span class="item">
									<input class="${inputclass}" type="hidden" id="shopName" value="${paramDto.shopName}"  name="shopName" /> 
									<input class="${inputclass}" type="text" placeholder="订单编号" id="subNumber" value="${paramDto.subNumber}" name="subNumber"> 
								</span>
								<span class="item">
									<input class="${inputclass}" type="text" placeholder="用户名称" id="userName" value="${paramDto.userName}" name="userName"> 
								</span>
								<span class="item">
									<input class="${inputclass}" type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />' readonly="readonly" placeholder="下单时间(起始)" id="startDate" name="startDate">
								</span>
								<span class="item">	
									<input class="${inputclass}" type="text" readonly="readonly" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" placeholder="下单时间(结束)" > 
								</span>
								<span class="item">	
									订单类型:&nbsp; 
									<select id="subType" name="subType"  class="criteria-select">
											<option value="">-- 请选择 --</option>
											<option value="NORMAL" <c:if test="${paramDto.subType eq 'NORMAL'}">selected="selected"</c:if>>普通订单</option>
											<ls:plugin pluginId="group">
												<option value="GROUP" <c:if test="${paramDto.subType eq 'GROUP'}">selected="selected"</c:if>>团购订单</option>
											</ls:plugin>
											<ls:plugin pluginId="auction">
												<option value="AUCTIONS" <c:if test="${paramDto.subType eq 'AUCTIONS'}">selected="selected"</c:if>>拍卖订单</option>
											</ls:plugin>
											<ls:plugin pluginId="seckill">
												<option value="SECKILL" <c:if test="${paramDto.subType eq 'SECKILL'}">selected="selected"</c:if>>秒杀订单</option>
											</ls:plugin>
											<ls:plugin pluginId="mergeGroup">
												<option value="MERGE_GROUP" <c:if test="${paramDto.subType eq 'MERGE_GROUP'}">selected = "selected"</c:if>>拼团订单</option>
											</ls:plugin>
											<ls:plugin pluginId="presell">
												<option value="PRE_SELL" <c:if test="${paramDto.subType eq 'PRE_SELL'}">selected = "selected"</c:if>>预售订单</option>
											</ls:plugin>
                                            <ls:plugin pluginId="presell">
                                                <option value="SHOP_STORE" <c:if test="${paramDto.subType eq 'SHOP_STORE'}">selected = "selected"</c:if>>门店订单</option>
                                            </ls:plugin>
									</select> 
									<input class="${btnclass}" type="button" onclick="search()" value="查询">
									<input class="${btnclass}" onclick="exportOrder();" type="button" value="导出搜索数据">
								</span>
							</div>
						</form:form>
					</div>
					<div align="left" id="orderContentList" name="orderContentList" class="order-content-list"></div>
				</div>
			</div>

			<div id="order_cancel" style="display: none;">
				<div class="white_content ui-draggable">
					<div class="white_box">
						<table width="390" cellspacing="0" cellpadding="0" border="0" class="${tableclass} no-border" style="float: left;min-width:390px;">
							<tbody>
								<tr>
									<td width="100" align="right">
										订单号： <input type="hidden" value="" id="sub_id">
									</td>
									<td align="left"></td>
								</tr>
								<tr>
									<td valign="top" align="right" rowspan="4">取消原因：</td>
									<td align="left" style="height:auto">
										<label class="radio-wrapper radio-wrapper-checked">
											<span class="radio-item">
												<input type="radio" class="radio-input" onclick="switch_reason(0);" value="买家要求取消,改买其他商品" id="radio1" name="state_info" checked="checked">
												<span class="radio-inner"></span>
											</span>
											<span class="radio-txt">买家要求取消</span>
										</label>
									</td>
								</tr>
								<tr>
									<td align="left">
										<label class="radio-wrapper">
											<span class="radio-item">
												<input type="radio" class="radio-input" onclick="switch_reason(0);" value="商品缺货,从其他店铺购买" id="radio2" name="state_info">
												<span class="radio-inner"></span>
											</span>
											<span class="radio-txt">商品缺货</span>
										</label>
									</td>
								</tr>
								<tr>
									<td align="left">
										<label class="radio-wrapper">
											<span class="radio-item">
												<input type="radio" class="radio-input" onclick="switch_reason(1);" value="其他原因" id="radio3" name="state_info">
												<span class="radio-inner"></span>
											</span>
											<span class="radio-txt">其他原因</span>
										</label>
									</td>
								</tr>
								<tr>
									<td align="left" style="display: none;" id="other_reason">
										<textarea style="height: 80px;" maxlength="50" placeholder="请在50字以内简短的描述您的理由" rows="5" cols="50" id="other_state_info" name="other_state_info"></textarea>
									</td>
								</tr>
								<tr>
									<td align="center" style="text-align: center;" colspan="2">
										<span class="inputbtn"> <input type="button" class="${btnclass}" onclick="orderCancel();" style="cursor: pointer;" value="保存"></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- 调整订单费用窗口   普通订单  -->
			<div id="order_fee" style="display: none;">
				<div class="white_content ui-draggable">
					<div class="white_box">
						<table width="390" cellspacing="0" cellpadding="0" border="0" class="${tableclass} no-border" style="float: left;min-width:390px;">
							<tbody>
								<tr>
									<td align="right">买家用户：</td>
									<td align="left" id="order_fee_username"></td>
								</tr>
								<tr>
									<td width="100" align="right">订单号：</td>
									<td align="left" id="order_fee_id"></td>
								</tr>
								<tr>
									<td align="right">商品总价：</td>
									<td align="left"><input type="text" maxlength="9" id="order_fee_amount" name="order_fee_amount" value="0"></td>
								</tr>

								<tr id="order_fee_ship_div">
									<td align="right">配送金额：</td>
									<td align="left">
										<input type="text" maxlength="5" id="order_fee_ship_price" name="order_fee_ship_price" value="0">
									</td>
								</tr>
								<tr>
									<td align="right">订单金额：</td>
									<td align="left">
										<span id="totalPriceText" style="color: #F00; font-weight: bold;"></span> 
										<input type="hidden" id="totalPrice" name="totalPrice">
									</td>
								</tr>
								<tr>
									<td align="center" style="text-align: center;" colspan="2">
										<span class="inputbtn"> 
											<input class="${btnclass}" type="button" onclick="changeOrderFee();" id="orderFee" style="cursor: pointer;" value="保存">
										</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<!-- 调整订单费用窗口     预售订单 -->
			<div id="presell_order_fee" style="display: none;">
				<div class="white_content ui-draggable">
					<div class="white_box">
							<table width="390" cellspacing="0" cellpadding="0" border="0" class="${tableclass} no-border" style="float:left;min-width:390px;" >
								<tbody>
									<tr>
										<td width="35%" align="right">买家用户：</td>
										<input type="hidden" id="orderFeeUserName" name="orderFeeUserName"/>
										<td align="left" id="orderFeeUserNameText"></td>
									</tr>
									<tr>
										 <td align="right">订单号：</td>
										 <input type="hidden" id="orderFeeId" name="orderFeeId"/>
										 <td align="left" id="orderFeeIdText" ></td>
									</tr>
									<tr id="preDepositPriceTr" style="display: none">
										<td align="right">定金金额：</td>
										<td align="left">
										   <input type="text" maxlength="9"  id="preDepositPrice" name="preDepositPrice" value="0">
										</td>
									</tr>
									<tr id="finalPriceTr">
										<td align="right">尾款金额：</td>
										<td align="left">
										   <input type="text" maxlength="9" id="finalPrice" name="finalPrice" value="0">
										</td>
									</tr>
									<!-- <tr>
										<td valign="top" align="right">配送金额：</td>
										<td align="left">
										<input type="text" maxlength="5" id="freightAmount" name="freightAmount" value="0">
										</td>
									</tr> -->
									<tr>
										<td align="right">订单金额：</td>
										<td align="left">
										  <span id="presellTotalPriceText" style="color:#F00; font-weight:bold;"></span>
										  <input type="hidden" id="presellTotalPrice" name="totalPrice">
										</td>
									</tr>
									<tr>
										<td align="center" style="text-align: center;" colspan="2">
									       <span class="inputbtn">
												<input class="${btnclass}" onclick="changePreOrderFee();" id="preOrderFee" style="cursor:pointer;" value="保存" >
										   </span>
										</td>
									</tr>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/processingOrder.js'/>"></script>
<script>
	var contextPath = "${contextPath}";
	var status = '${paramDto.status}';
	var shopName = '${paramDto.shopName}';
	var orderSn = '${paramDto.subNumber}';
	var userName = '${paramDto.userName}';
	var startDate = '${paramDto.startDate}';
	var endDate = '${paramDto.endDate}';
	var subtype = '${paramDto.subType}';
	//判断定金支付，还是全额支付
	var payPctTypeTemp = 0;
	  function switch_reason(type){
		 $("input:radio[name='state_info']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
		if(0==type){
			$("#orderCancel").find("#other_reason").hide();
		}else{
			$("#orderCancel").find("#other_reason").show();
		}
		} 
</script>
</body>
</html>
