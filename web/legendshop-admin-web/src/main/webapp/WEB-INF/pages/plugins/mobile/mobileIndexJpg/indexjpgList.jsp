<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
    <table class="${tableclass}" style="width: 100%">
    <thead>
    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  图片</th></tr>
    </thead>
     <tbody><tr><td>
 	<div align="left" style="padding: 3px">
		<form:form  id="form1" action="${contextPath}/admin/indexjpg/mobile/query">
		        <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
				商城
				<input type="text" name="userName" id="userName" maxlength="50" value="${indexJpg.userName}" class="${inputclass}" />
				<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
				<input type="button" class="${btnclass}" value="创建首页图片" onclick='window.location="${contextPath}/admin/indexjpg/mobile/load"'/>
		</form:form>
 	</div>
 </td></tr></tbody>
    </table>
	<div align="center">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
    <display:table name="list" requestURI="/admin/indexjpg/mobile/query" id="item"
         export="false" class="${tableclass}" style="width:100%" sort="external">
      <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
       <display:column title="商城" property="userName"></display:column>
       <display:column title="标题" property="title"></display:column>
       <display:column title="图片" style="width:35px"><a href="<ls:photo item='${item.img}'/>" target="_blank"><img src="<ls:photo item='${item.img}'/>" height="145" width="265"/></a></display:column>
     	 <display:column title="操作" media="html"  style="width:220px">
      	   <div class="am-btn-toolbar">
                 <div class="am-btn-group am-btn-group-xs">
		            <c:choose>
				  		<c:when test="${item.status == 1}">
				  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusImg"  itemId="${item.id}"  itemName="${item.title}"  status="${item.status}" style="color:#f37b1d;"><span class="am-icon-arrow-down"></span>下线</button>
				  		</c:when>
				  		<c:otherwise>
				  			<button class="am-btn am-btn-default am-btn-xs am-hide-sm-only" name="statusImg"  itemId="${item.id}"  itemName="${item.title}"  status="${item.status}" style='color:#0e90d2;'><span class="am-icon-arrow-up"></span>上线</button>
				  		</c:otherwise>
				  	</c:choose>
                 
                   <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/indexjpg/mobile/update/${item.id}'"><span class="am-icon-pencil-square-o"></span> 编辑</button>
				
                   	<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}');" ><span class="am-icon-trash-o"></span> 删除</button>
                 </div>
	        </div>
      	</display:column>
    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
        </div>
   
   <script language="JavaScript" type="text/javascript">
	function deleteById(id) {
	    if(confirm("确定删除 ?")){
	          window.location = "${contextPath}/admin/indexjpg/mobile/delete/"+id ;
	      }
	  }
  
	$(document).ready(function(){
        $("button[name='statusImg']").click(function(event){
          		$this = $(this);
          		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/indexjpg/updatestatus/", $this,"${contextPath}");
       	});
             	highlightTableRows("item");   
     });   
     function search(){
	  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#form1")[0].submit();
	}  
</script>


