<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<link rel="stylesheet" type="text/css"
	href="<ls:templateResource item='/resources/plugins/layuiv2.3.0/css/layui.css'/>" />
	<link rel="stylesheet" href="/resources/templets/amaze/css/amazeui.css">
<form:form action="${contextPath}/admin/product/groupProdUpdate"
	method="post" id="from1" target="_parent" >
	<input type="hidden" value="${prodIds}" name="prodIds" id="prodIds">
	<ul class="goods-tag-list" style="list-style: none; line-height: 30px">
		<li>
			<div class="layui-input-inlineblock" style="margin:10px 0px 0px 50px">
				<c:forEach var="prod" items="${groupProd}">
					<span style="display:block;margin:10px 0px">
						<input value="${prod.id}" type="checkbox" name="catId" class="catId"/>${prod.name}
					</span>
				</c:forEach>
			</div>
		</li>


	</ul>
	<div style="position: fixed; left:120px;bottom:20px" >
		<input onclick="groupProd();" type="button" value="保存"
			class="criteria-btn" />
	</div>
</form:form>

<script src="/resources/plugins/layuiv2.3.0/layui.js"></script>
<script
	src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript">
	var prodIds = '${selAry}';
	$(function() {
		
	});
	function closeDialog() {
		var index = parent.layer.getFrameIndex('groupProd');
		parent.layer.close(index);
	}

	function groupProd() {
		var boxs = $(".catId:checked");
		var cids = new Array();
		boxs.each(function(index, domEle) {
			var value = $(domEle).val();
			cids.push(value);
		});
		var catIds = cids.toString();
		if (catIds == "") {
			layer.alert("至少选中一个分组！", {
				icon : 2
			});
			return false;
		}
		var load = layer.load(1, {
			shade : false
		})
		$("input[type=button]").attr('class',
				"layui-btn layui-btn-radius layui-btn-disabled")
		$("input[type=button]").attr('disabled', true)
		$.ajax({
			url : '/admin/product/groupProdUpdate',
			data : {
				"prodIds" : $("#prodIds").val(),
				"groupid" : catIds,
			},
			type : 'post',
			dataType : 'json',
			async : true, //默认为true 异步   
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			},
			success : function(result) {
				if (result == 'true') {
					layer.close(load);
					parent.layer.msg('修改成功', {
						icon : 1,
						time : 1500
					//2秒关闭（如果不配置，默认是3秒）
					}, function() {
						parent.window.location.reload();
					});
					return;
				} else {
					layer.close(load);
					parent.layer.alert('数据错误，修改失败！', {
						icon : 2,
						time : 6000
					//2秒关闭（如果不配置，默认是3秒）
					}, function() {
						parent.window.location.reload();
					});
					return;
				}

			}
		});

	}
</script>


