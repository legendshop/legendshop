<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/> 
<%@ include file="../back-common.jsp"%>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="am-table am-table-striped" >
	<thead>
		<tr>
			<th width="10%" style="text-align:left;">
				<span style="display: inline-block;margin: 0 80px 0 10px;">图片</span>
				<span style="display: inline-block;">商品名称</span>
			</th>
			<th style="text-align: center;width: 10%;">规格</th>

			<th style="text-align: center;width: 10%;">商家ID</th>
			<th style="text-align: center;width: 10%;">商家名称</th>
			<th style="text-align: center;width: 10%;">支付时间</th>


			<th style="text-align: center;width: 10%;">单价</th>
			<th style="text-align: center;width: 10%;">数量</th>
			<th style="text-align: center;width: 10%;">买家</th>
			<th style="text-align: center;width: 10%;">金额</th>
			<th style="text-align: center;">操作</th>
		</tr>
	</thead>
</table>
<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
	<div class="orderlist_one" orderId="${order.subId}">
		<h4 class="orderlist_one_h4" style="padding-left: 20px !important;">
			<span>编号：${orderstatues.count}</span> 
			<span>店铺：<a href="${contextPath}/admin/shopDetail/load/${order.shopId}">${order.shopName}</a></span>
			<span>订单号：<a href="${contextPath}/admin/order/orderAdminDetail/${order.subNum}">${order.subNum}</a></span>
			<span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span>
			<span>订单状态： 
				<strong style="color: #ff6c00;font-weight: 400;"> 
					<c:choose>
						<c:when test="${order.status==1 }">待付款</c:when>
						<c:when test="${order.status==2&&order.subType=='SHOP_STORE'}">待提货</c:when>
						<c:when test="${order.status==2 }">
							<c:choose>
								<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
								<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">团购中</c:when>
								<c:otherwise>待发货</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${order.status==3}">待收货</c:when>
						<c:when test="${order.status==4 }">已完成</c:when>
						<c:when test="${order.status==5 }"> 交易关闭</c:when>
					</c:choose>
				</strong>
			</span> 
			<span class="blue">付款类型：${order.payManner==1?"货到付款":"在线支付"}</span> 
			<span class="blue">支付方式： 
				<c:choose>
						<c:when test="${!order.ispay}">未支付</c:when>
						<c:otherwise>${order.payTypeName}</c:otherwise>
				</c:choose>
			</span> 
			<span class="blue">订单类型： 
			<c:choose>
				<c:when test="${order.subType == 'NORMAL'}">普通订单</c:when>
				<c:when test="${order.subType == 'AUCTIONS'}">拍卖订单</c:when>
				<c:when test="${order.subType == 'GROUP'}">团购订单</c:when>
				<c:when test="${order.subType == 'SHOP_STORE'}">门店订单</c:when>
				<c:when test="${order.subType == 'SECKILL'}">秒杀订单</c:when>
				<c:when test="${order.subType == 'MERGE_GROUP'}">拼团订单</c:when>
				<c:when test="${order.subType == 'PRE_SELL'}">预售订单</c:when>
			</c:choose>
			</span>
			<c:if test="${order.status==5 }">
				<span class="blue">关闭时间： <fmt:formatDate value="${order.updateDate}" type="both" /> </span>
			</c:if>
			<c:if test="${not empty order.subSettlement}">
				<span class="blue">商城支付流水号： ${order.subSettlement} </span>
			</c:if>
			<c:if test="${not empty order.flowTradeNo}">
				<span class="blue">第三方支付流水号： ${order.flowTradeNo} </span>
			</c:if>
			<c:if test="${order.refundState eq 1 }">
				<span style="color: #ff6c00;">该订单正在退款/退货中</span>
			</c:if>
			<c:if test="${order.refundState eq 2 }">
				<span style="color: #ff6c00;">该订单退款/退货已完成</span>
			</c:if>
		</h4>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="user_order_table ${tableclass }" style="margin-bottom: 0">
			<tbody>
				<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
					<tr>
						<td class="vertical-dividers" style="width: 10%;text-align:left;">
							<div class="clearfix">
								<div class="fl">
									<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}" class="order_img">
										<img src="<ls:images item="${orderItem.pic}" scale="3"/>">
									</a>
								</div>
								<div>
									<span class="order_img_name">
										<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">
											<a href="${PC_DOMAIN_NAME}/snapshot/${orderItem.snapshotId}" target="_blank">[交易快照]</a>
										</c:if> 
										<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}">${orderItem.prodName}</a>
									</span>
									<c:if test="${not empty orderItem.promotionInfo}"><span class="order_img_span">促销：${orderItem.promotionInfo}</span></c:if>
								</div>
							</div>
						</td>
						<td align="center" class="vertical-dividers">
							<c:choose>
								<c:when test="${not empty orderItem.attribute}">
									<span>${orderItem.attribute}</span>
								</c:when>
								<c:otherwise>
									<span>&nbsp;</span>
								</c:otherwise>
							</c:choose>
						</td>

					<td align="center" class="vertical-dividers">
						<span>商家ID：${order.shopId}</span>
					</td>
					<td align="center" class="vertical-dividers">
						<span>商家名称：${order.shopName}</span>
					</td>
					<td align="center" class="vertical-dividers">
						<span>支付时间：
							<c:choose>
								<c:when test="${empty order.payDate}">未支付</c:when>
								<c:otherwise><fmt:formatDate value="${order.payDate}" type="both" /></c:otherwise>
							</c:choose>
						</span>
					</td>



						<td align="center" class="vertical-dividers">
							<span>价格：￥${orderItem.cash}</span>
						</td>
						<td align="center" class="vertical-dividers">
							<span>数量：${orderItem.basketCount}(件)</span>
						</td>
						<c:if test="${orderItemStatues.index eq 0 }">
							<c:choose>
								<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
									<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">
								</c:when>
								<c:otherwise>
									<td align="center" class="vertical-dividers">
								</c:otherwise>
							</c:choose>
								<span obj_id="${order.userName}" mark="name_${order.userName}" addrOrderId="${order.addrOrderId}"> 
									<a href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'>${order.userName}</a>
								</span>
							</td>
							<c:choose>
								<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
									<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">
								</c:when>
								<c:otherwise>
									<td align="center" class="vertical-dividers">
								</c:otherwise>
							</c:choose>
								<span class="order_sp">¥<fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></span></br>
								<c:if test="${not empty order.freightAmount}">
									<span class="order_sp">(含运费:¥<fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber>)</span>
									<span class="order_sp">
										<br> 
										<c:if test="${order.status==1}">
											<c:choose>
												<c:when test="${order.subType eq  'PRE_SELL' }">
									                <a class="order_money_modify presellOrderModify" href="javascript:void(0);" 
														subNum="${order.subNum}" userName="${order.userName}" preDepositPrice="${order.preDepositPrice}" 
														finalPrice="${order.finalPrice}" actualTotal="${order.actualTotal}" 
														payPctType="${order.payPctType}">
									               		调整费用
									                </a>
												</c:when>
												<c:otherwise>
													<a class="order_money_modify orderModify" freight="${order.freightAmount}" subtype="${order.subType}"
													actualTotal="${order.actualTotal}" name="${order.userName}" subNumber="${order.subNum}" href="javascript:void(0);"> 
														调整费用
													</a>
												</c:otherwise>
											</c:choose>
										</c:if>
									</span>
								</c:if>
							</td>
							<c:choose>
								<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
									<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">
								</c:when>
								<c:otherwise>
									<td align="center" class="vertical-dividers">
								</c:otherwise>
							</c:choose>
								<div class="table-btn-group">
									<c:choose>
										<c:when test="${order.subType eq 'PRE_SELL'}"> <!-- 预售订单 -->
											<a class="tab-btn" href="<ls:url address="/admin/order/presellOrderAdminDetail/${order.subNum}"/>">查看订单</a> 
										</c:when>
										<c:otherwise>
											<a class="tab-btn" href="<ls:url address="/admin/order/orderAdminDetail/${order.subNum}"/>">查看订单</a> 
										</c:otherwise>
									</c:choose>
								</div>
								<c:if test="${order.refundState != 1 }">
									<c:choose>
										<c:when test="${order.status==1 }">
											<c:choose>
												<c:when test="${order.subType eq 'PRE_SELL'}">
													<div class="table-btn-group">
														<a href="javascript:void(0);" onclick="orderCancelShow('${order.subNum}');">取消订单</a>
													</div>
												</c:when>
												<c:otherwise>
												<div class="table-btn-group">
													<a class="order_bottom_btn" href="<ls:url address="/admin/order/receiveMoneyPage/${order.subNum}"/>">收到货款</a>
												</div>
												<div class="table-btn-group">
													<a href="javascript:void(0);" onclick="orderCancelShow('${order.subNum}');">取消订单</a>
												</div>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${order.status==2&&order.subType!='SHOP_STORE' }">
											<c:choose>
												<c:when test="${order.subType ne 'MERGE_GROUP' && order.subType ne 'GROUP'}">
													<div class="table-btn-group">
														<a href="javascript:void(0);" onclick="deliverGoods('${order.subNum}');" class="order_outline_ok" href="javascript:void(0);">确认发货</a>
													</div>
												</c:when>
												<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 1}">
													<div class="table-btn-group">
														<a href="javascript:void(0);" onclick="deliverGoods('${order.subNum}');" class="order_outline_ok" href="javascript:void(0);">确认发货</a>
													</div>
												</c:when>
												<c:otherwise>
													<c:if test="${order.mergeGroupStatus eq 1}">
														<div class="table-btn-group">
															<a href="javascript:void(0);" onclick="deliverGoods('${order.subNum}');" class="order_outline_ok" href="javascript:void(0);">确认发货</a>
														</div>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:when>
										<%--<c:when test="${order.status==3 || order.status==4}">
											<div class="table-btn-group">
												<a href="<ls:url address="/admin/order/printDeliveryDocument/${order.subId}/${order.dvyTypeId}"/>" class="order_bottom_btn" target="_blank">打印快递单</a>
											</div>
										</c:when>--%>
									</c:choose>
								</div>
								</c:if> 
								<div class="table-btn-group">
									<a target="_blank" class="order_bottom_btn"	href="<ls:url address="/admin/order/orderPrint/${order.subNum}"/>">打印订单</a>
								</div>
							</c:if>
					</tr>
				</c:forEach>
				
				<c:if test="${order.subType eq 'PRE_SELL' }">
					<tr style="color:#999;">
			            <td align="center">阶段1：定金</td> 
	            		<td>
							<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
							¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
							</c:if>
							<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
							¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</c:if>
						</td>
			            <c:if test="${order.status eq 5 }"> 
			            	<td colspan="8"></td>
			            </c:if>
			            <c:if test="${order.status ne 5 }">
				            <c:if test="${order.isPayDeposit eq 0}">
			            		<c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) le 60}">
			            			<td colspan="1" align="center">用户可支付时间还剩<font color="red">${60 - dateUtil:getOffsetMinutes(order.subDate,nowTime)}</font>分钟</td>
			            			<td colspan="1" align="center">买家未付款</td>
			            			<td colspan="6"></td>
			            		</c:if>
			            		<c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) gt 60}">
			            			<td colspan="2" align="center">已过期</td>
			            			<td colspan="6"></td>
			            		</c:if>
				            </c:if>
				            <c:if test="${order.isPayDeposit eq 1}">
				             	<td colspan="2" align="center"><font color="green">买家已付款</font></td>
				             	<td colspan="6"></td>
				            </c:if>
			            </c:if>
	          		</tr>
			        <c:choose>
			        	<c:when test="${order.payPctType eq 1}">
				          	<tr style="color:#999;">
				             <td align="center">阶段2：尾款</td>
					            <td>¥${order.finalPrice+order.freightAmount }(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)</td>
					            <c:if test="${order.status eq 5 }">
					            	<td colspan="8" align="center"></td>
					            </c:if>
				            	<c:if test="${order.status ne 5 }">
				            	<c:if test="${order.isPayFinal eq 1}">
				              	<td colspan="1" align="center">
				              		尾款支付时间: </br>
				              		<fmt:formatDate value="${order.finalMStart }"  pattern="yyyy-MM-dd"/> - <fmt:formatDate value="${order.finalMEnd }"  pattern="yyyy-MM-dd"/>
				              	</td>
				            	</c:if>
				             	<c:if test="${order.isPayFinal eq 0}">
					             	<td colspan="2" align="center">
					              		<c:choose>
						               		<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
						               		<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
						               		<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
					               		</c:choose>
					             	</td>
					             	<td colspan="6"></td>
				             </c:if>
				             <c:if test="${order.isPayFinal eq 1}">
						 	 <td colspan="2" align="center"><font color="green">买家已付款</font></td>
						 	 <td colspan="2"></td>
				             </c:if>
				            </c:if>
				          	</tr>
			        	</c:when>
			    	</c:choose>
		    	</c:if>
			</tbody>
		</table>
	</div>
</c:forEach>

<form id="exportForm" method="get" action="${contextPath}/admin/order/export">
	<input id="ex_status" name="status" type="hidden" value="${paramDto.status}">
	<input name="shopName" type="hidden" value="${paramDto.shopName}">
	<input name="shopId" type="hidden" value="${paramDto.shopId}">
	<input name="subNumber" type="hidden" value="${paramDto.subNumber}">
	<input name="userName" type="hidden" value="${paramDto.userName}">
	<input id="ex_startDate" name="startDate" type="hidden" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />'>
	<input name="endDate" type="hidden" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />'>
	<input name="payStartDate" type="hidden" value='<fmt:formatDate value="${paramDto.payStartDate}" pattern="yyyy-MM-dd" />'>
	<input name="payEndDate" type="hidden" value='<fmt:formatDate value="${paramDto.payEndDate}" pattern="yyyy-MM-dd" />'>
	<input name="subType" type="hidden" value="${paramDto.subType}">
	<input name="isPayed" type="hidden" value="${paramDto.isPayed}">
	<input name="refundStatus" type="hidden" value="${paramDto.refundStatus}">
	<input name="isShopRemak" type="hidden" value="${paramDto.isShopRemak}">
	<input name="userMobile" type="hidden" value="${paramDto.userMobile}">
</form>

<div class="clearfix">
 	<div class="fr">
 		<div class="page">
   			 <div class="p-wrap">
       		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 </div>
 		</div>
 	</div>
</div>