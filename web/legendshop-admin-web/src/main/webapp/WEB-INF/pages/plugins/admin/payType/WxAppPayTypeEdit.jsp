<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="../back-common.jsp"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
 <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/amaze/css/payEdit.css'/>" />
 <%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>

 <form:form  action="${contextPath}/admin/paytype/payEditWxApppay" style="max-height: 600px;height:600px; overflow: auto;"  method="post" id="payForm"  enctype="multipart/form-data" >
	<input type="hidden" name="isEnable" id="isEnable" value="${payType.isEnable}" />
	<input type="hidden" name="isRefund" id="isRefund" value="${jsonObject['isRefund']}"  />
	<div class="modal-body-main filll fillr">
		<div style="padding-top:7px;">
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label class="">支付类型</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<span>微信APP支付</span>
					</div>
				</div>
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="MCH_ID" class="ant-form-item-required">请输入APP支付商户号</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="text" id="MCH_ID" name="MCH_ID" value="${jsonObject['MCH_ID']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="APPID" class="ant-form-item-required">APP支付APPID(应用ID)</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="text" id="APPID" name="APPID" value="${jsonObject['APPID']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
		
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="APPSCECRET" class="ant-form-item-required">APP支付APPSCECRET</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="password" id="APPSCECRET" name="APPSCECRET" value="${jsonObject['APPSCECRET']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
			
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="PARTNERKEY" class="ant-form-item-required">PARTNERKEY(API密钥)</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="password" id="PARTNERKEY" name="PARTNERKEY" value="${jsonObject['PARTNERKEY']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
			
			
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label class="">原路退款证书</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<span class=""><div
								class="ant-upload ant-upload-select ant-upload-select-text">
								<span tabindex="0" class="ant-upload" role="button">
								<input  type="file" name="file" style="color: #cccccc;" accept=".p12">
								</span>
							</div>
							<div class="ant-upload-list ant-upload-list-text">
							</div>
						</span>
						<!-- react-text: 53 -->
						退款证书请登录微信商户平台下载。
						<!-- /react-text -->
						<!-- <a href="https://qianmiwang.kf5.com/hc/kb/article/1061360/"
							target="_blank" style="color: #5491de;">如何下载证书</a> -->
					</div>
				</div>
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="originalRefundFlag" class="">原路退款</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control has-success">
						<div class="ant-radio-group ant-radio-group-large">
							<label class="ant-radio-wrapper"><span
								class="ant-radio <c:if test="${empty jsonObject['isRefund'] || jsonObject['isRefund'] eq 0}"> ant-radio-checked</c:if>">
								
								<input type="radio"
									class="ant-radio-input ant-radio-refund" value="0"  <c:if test="${empty jsonObject['isRefund'] || jsonObject['isRefund'] eq 0}"> checked="checked"</c:if>  ><span
									class="ant-radio-inner"></span> </span><span>关闭</span> 
						   </label>
									
							<label
								class="ant-radio-wrapper"><span class="ant-radio <c:if test="${jsonObject['isRefund'] eq 1}"> ant-radio-checked</c:if>">
								<input
									type="radio" class="ant-radio-input ant-radio-refund" value="1"  <c:if test="${jsonObject['isRefund'] eq 1}"> checked="checked"</c:if> >
									<span
									class="ant-radio-inner"></span> </span><span>启用</span>
							</label>
									
									
						</div>
						<br>
						<!-- react-text: 72 -->
						上传了原路退款证书之后方能开启原路退款。
						<!-- /react-text -->
					</div>
				</div>
			</div>
			
			
		</div>
		
		<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="accountState" class="">状态</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control has-success">
						<div class="ant-radio-group ant-radio-group-large">
							<label class="ant-radio-wrapper"><span
								class="ant-radio <c:if test="${empty payType.isEnable || payType.isEnable eq 0}"> ant-radio-checked</c:if>">
								
								<input type="radio"
									class="ant-radio-input ant-radio-isEnable" value="0"  <c:if test="${empty payType.isEnable || payType.isEnable eq 0}"> checked="checked"</c:if>  ><span
									class="ant-radio-inner"></span> </span><span>关闭</span> 
						   </label>
									
							<label
								class="ant-radio-wrapper"><span class="ant-radio <c:if test="${payType.isEnable eq 1}"> ant-radio-checked</c:if>">
								<input
									type="radio" class="ant-radio-input ant-radio-isEnable" value="1"  <c:if test="${payType.isEnable eq 1}"> checked="checked"</c:if> >
									<span
									class="ant-radio-inner"></span> </span><span>启用</span>
							</label>
									
									
						</div>
					</div>
				</div>
			</div>

		<div data-show="true"
			class="ant-alert ant-alert-info ant-alert-no-icon">
			<span class="ant-alert-message"><div>
					<p>注:</p>
					<p>1、设置账号前，请确保您已在微信平台完成相关设置。</p>
					<p>2、启用后微信端可使用微信支付进行付款。</p>
					<p>3、密钥默认为空；如果需要编辑，请重新输入密钥。</p>
					<p>4、APP支付的商户帐号与微信公众号支付的商户号是不同一个帐号来的。</p>
				</div> </span><span class="ant-alert-description"></span>
		</div>
		<input type="submit" id="editBtn" value = "提交更改" class="criteria-btn" />
	</div>
</form:form>

<script src="${contextPath}/resources/common/js/jquery.form.js"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/WxAPPPayTypeEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
</script>
</body>
</html>