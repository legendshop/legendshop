<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>店铺管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/css/viewBigImage.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%">
				<tr>
					<th class="title-border">商城管理 &nbsp;＞&nbsp;<a href="<ls:url address="/admin/shopDetail/query"/>">店铺管理</a>  
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺编辑</span>
					</th>
				</tr>
			</table>
			<div style="margin-left: 0.5rem">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/shopDetail/load/${shopCompanyDetail.shopId}"/>">店铺信息</a></li>
						<li class="am-active"><i></i><a href="javaScript:void(0)">公司信息</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/shopDetail/shopAuditDetail?shopId=${shopCompanyDetail.shopId}"/>">审核历史</a></li>
					</ul>
				</div>
			</div>
			<form:form style="padding:0 15px;"
				action="${contextPath}/admin/shopDetail/updateShopCompanyInfo"
				method="post" id="form1" enctype="multipart/form-data">
				<table style="width: 100%" class="${tableclass} no-border content-table" id="col1">
					<input type="hidden" name="shopId" id="shopId" value="${shopId}" />
					<input type="hidden" name="userName" id="userName"
						value="${userName}" />
					<tr>
						<td colspan="2">
							<div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">公司信息：</div>
						</td>
					</tr>
					<tr>
						<td style="width:13%;min-width:185px;">
							<div align="right">公司名字:</div>
						</td>
						<td><input type="text"
							value="${shopCompanyDetail.companyName}" name="companyName"
							id="companyName" style="width: 325px;" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">注册号(营业执照号)：</div>
						</td>
						<td><input type="text"
							value="${shopCompanyDetail.licenseNumber}" name="licenseNumber"
							id="licenseNumber" style="width: 325px;" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">法人身份证电子版：</div>
						</td>
						<td><input type="file"
							oldFile="${shopCompanyDetail.legalIdCard}" id="legalIdCardPic"
							name="legalIdCardPic"></td>
					</tr>
					<c:if test="${not empty shopCompanyDetail.legalIdCard}">
						<tr>
							<td>
								<div align="right">原有图片:</div>
							</td>
							<td style="max-width: 500px;">
								<div class="zoombox">
									<span class="photoBox">
										<div class="loadingBox">
											<span class="loading"></span>
										</div> <img
										src="<ls:photo item='${shopCompanyDetail.legalIdCard}'/>"
										class="zoom" onclick="zoom_image($(this).parent());"
										style="max-height: 60px; max-width: 198px;">
									</span>

									<div class="photoArea" style="display: none;">
										<p>
											<img src="about:blank" class="minifier"
												onclick="zoom_image($(this).parent().parent());" />
										</p>
										<p class="toolBar gc">
											<span><a class="green" href="javascript:void(0)"
												onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
												class="view"><a class="green"
												href="<ls:photo item='${shopCompanyDetail.legalIdCard}'/>"
												target="_blank">查看原图</a></span>
										</p>
									</div>
								</div>
							</td>
						</tr>
					</c:if>
					<tr>
						<td align="right"><span class="sred_span">营业执照所在地：</span></td>
						<td><select class="combox sele" id="licenseProvinceid"
							name="licenseProvinceid" requiredTitle="true"
							childNode="licenseCityid"
							selectedValue="${shopCompanyDetail.licenseProvinceid}"
							retUrl="${contextPath}/common/loadProvinces">
						</select> <select class="combox sele" id="licenseCityid"
							name="licenseCityid" requiredTitle="true"
							selectedValue="${shopCompanyDetail.licenseCityid}"
							showNone="false"
							parentValue="${shopCompanyDetail.licenseProvinceid}"
							childNode="licenseAreaid"
							retUrl="${contextPath}/common/loadCities/{value}">
						</select> <select class="combox sele" id="licenseAreaid"
							name="licenseAreaid" requiredTitle="true"
							selectedValue="${shopCompanyDetail.licenseAreaid}"
							showNone="false" parentValue="${shopCompanyDetail.licenseCityid}"
							retUrl="${contextPath}/common/loadAreas/{value}">
						</select> <em style="margin-left: 5px;"></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">营业执照详细地址：</div>
						</td>
						<td><input type="text"
							value="${shopCompanyDetail.licenseAddr}" id="licenseAddr"
							name="licenseAddr" style="width: 325px;" /></td>
					</tr>

					<tr>
						<td>
							<div align="right">成立日期：</div>
						</td>
						<td><input class="size200" readonly="readonly" 
							 pattern="yyyy-MM-dd" type="text" style="width: 155px;"
							id="licenseEstablishDate" name="licenseEstablishDate"
							value="<fmt:formatDate value="${shopCompanyDetail.licenseEstablishDate}" pattern="yyyy-MM-dd"/>"><em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">营业期限：</div>
						</td>
						<td><input class="size200" readonly="readonly" 
							 pattern="yyyy-MM-dd" type="text"
							style="width: 155px;" id="licenseStartDate"
							name="licenseStartDate"
							value="<fmt:formatDate value="${shopCompanyDetail.licenseStartDate}" pattern="yyyy-MM-dd"/>">至
							<input class="size200" readonly="readonly" 
							 pattern="yyyy-MM-dd" type="text"
							style="width: 155px;" id="licenseEndDate" name="licenseEndDate"
							value="<fmt:formatDate value="${shopCompanyDetail.licenseEndDate}" pattern="yyyy-MM-dd"/>"><em
							style="margin-left: 24px;"></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">注册资本：</div>
						</td>
						<td><input class="size200" type="text"
							name="licenseRegCapital" id="licenseRegCapital"
							value="${shopCompanyDetail.licenseRegCapital}" maxlength="5"
							style="width: 155px;">&nbsp;万元<em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">经营范围：</div>
						</td>
						<td><textarea style="width: 325px; height: 70px;"
								name="licenseBusinessScope" id="licenseBusinessScope">${shopCompanyDetail.licenseBusinessScope}</textarea><em
							style="margin-left: 27px;"></em></td>
					</tr>

					<tr>
						<td>
							<div align="right">营业执照副本电子版：</div>
						</td>
						<td><input type="file"
							oldFile="${shopCompanyDetail.licensePic}" id="licensePics"
							name="licensePics"></td>
					</tr>
					<c:if test="${not empty shopCompanyDetail.licensePic}">
						<tr>
							<td>
								<div align="right">原有图片：</div>
							</td>
							<td style="max-width: 500px;">
								<div class="zoombox">
									<span class="photoBox">
										<div class="loadingBox">
											<span class="loading"></span>
										</div> <img src="<ls:photo item='${shopCompanyDetail.licensePic}'/>"
										class="zoom" onclick="zoom_image($(this).parent());"
										style="max-height: 60px; max-width: 198px;">
									</span>

									<div class="photoArea" style="display: none;">
										<p>
											<img src="about:blank" class="minifier"
												onclick="zoom_image($(this).parent().parent());" />
										</p>
										<p class="toolBar gc">
											<span><a class="green" href="javascript:void(0)"
												onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
												class="view"><a class="green"
												href="<ls:photo item='${shopCompanyDetail.licensePic}'/>"
												target="_blank">查看原图</a></span>
										</p>
									</div>
								</div>
							</td>
						</tr>
					</c:if>
					<tr>
						<td>
							<div align="right">公司所在地：</div>
						</td>
						<td><select class="combox sele" id="companyProvinceid"
							name="companyProvinceid" requiredTitle="true"
							childNode="companyCityid"
							selectedValue="${shopCompanyDetail.companyProvinceid}"
							retUrl="${contextPath}/common/loadProvinces">
						</select> <select class="combox sele" id="companyCityid"
							name="companyCityid" requiredTitle="true"
							selectedValue="${shopCompanyDetail.companyCityid}"
							showNone="false"
							parentValue="${shopCompanyDetail.companyProvinceid}"
							childNode="companyAreaid"
							retUrl="${contextPath}/common/loadCities/{value}">
						</select> <select class="combox sele" id="companyAreaid"
							name="companyAreaid" requiredTitle="true"
							selectedValue="${shopCompanyDetail.companyAreaid}"
							showNone="false" parentValue="${shopCompanyDetail.companyCityid}"
							retUrl="${contextPath}/common/loadAreas/{value}">
						</select> <em style="margin-left: 5px;"></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">公司详细地址：</div>
						</td>
						<td><input class="size200" name="companyAddress"
							id="companyAddress" value="${shopCompanyDetail.companyAddress}"
							type="text" style="width: 325px;"><em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">公司电话：</div>
						</td>
						<td><input class="size200" name="companyTelephone"
							id="companyTelephone" style="width: 155px;"
							value="${shopCompanyDetail.companyTelephone}" type="text">（020-12345678）<em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">公司人数：</div>
						</td>
						<td><select id="companyNum" name="companyNum" class="sele"
							style="width: 155px;">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="COMPANY_NUM"
									selectedValue="${shopCompanyDetail.companyNum}" />
						</select><em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">公司行业：</div>
						</td>
						<td><select id="companyIndustry" name="companyIndustry"
							class="sele" style="width: 155px;">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="COMPANY_INDUSTRY"
									selectedValue="${shopCompanyDetail.companyIndustry}" />
						</select><em></em></td>
					</tr>
					<tr>
						<td>
							<div align="right">公司性质：</div>
						</td>
						<td><select id="companyProperty" name="companyProperty"
							class="sele" style="width: 155px;">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="COMPANY_PROPERTIES"
									selectedValue="${shopCompanyDetail.companyProperty}" />
						</select><em></em></td>
					</tr>
					<c:if test="${shopCompanyDetail.codeType eq 1}">
						<tr>
							<td colspan="2">
								<div align="right" style="text-align: left; font-weight: bold;">营业执照信息：</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">纳税人识别号:</div>
							</td>
							<td><input type="text"
								value="${shopCompanyDetail.taxpayerId}" name="taxpayerId"
								id="taxpayerId" style="width: 325px;" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">税务登记证号：</div>
							</td>
							<td><input type="text"
								value="${shopCompanyDetail.taxRegistrationNum }"
								name="taxRegistrationNum" id="taxRegistrationNum"
								style="width: 325px;" /></td>
						</tr>

						<tr>
							<td>
								<div align="right">税务登记证电子版:</div>
							</td>
							<td><input type="file"
								oldFile="${shopCompanyDetail.taxRegistrationNumE}"
								id="taxRegistrationNumEPic" name="taxRegistrationNumEPic">
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">原有图片:</div>
							</td>
							<td style="max-width: 500px;">
								<div class="zoombox">
									<span class="photoBox">
										<div class="loadingBox">
											<span class="loading"></span>
										</div> <img
										src="<ls:photo item='${shopCompanyDetail.taxRegistrationNumE}'/>"
										class="zoom" onclick="zoom_image($(this).parent());"
										style="max-height: 60px; max-width: 198px;">
									</span>

									<div class="photoArea" style="display: none;">
										<p>
											<img src="about:blank" class="minifier"
												onclick="zoom_image($(this).parent().parent());" />
										</p>
										<p class="toolBar gc">
											<span><a class="green" href="javascript:void(0)"
												onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
												class="view"><a class="green"
												href="<ls:photo item='${shopCompanyDetail.taxRegistrationNumE}'/>"
												target="_blank">查看原图</a></span>
										</p>
									</div>
								</div>
							</td>
						</tr>
					</c:if>
					<c:if test="${shopCompanyDetail.codeType eq 2}">
						<tr>
							<td colspan="2">
								<div align="right" style="text-align: left; font-weight: bold;">统一社会信用代码：</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="red">*</font>统一社会信用代码号:
								</div>
							</td>
							<td><input type="text"
								value="${shopCompanyDetail.socialCreditCode}"
								name="socialCreditCode" id="socialCreditCode" /></td>
						</tr>
					</c:if>

					<tr>
						<td colspan="2">
							<div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">开户银行许可证：</div>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">银行开户名：</div>
						</td>
						<td><input type="text"
							value=" ${shopCompanyDetail.bankAccountName}"
							id="bankAccountName" name="bankAccountName" style="width: 325px;" />
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">公司银行账号：</div>
						</td>
						<td><input type="text" value=" ${shopCompanyDetail.bankCard}"
							name="bankCard" id="bankCard" style="width: 325px;"
							maxlength="30" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">开户银行支行名称：</div>
						</td>
						<td><input type="text" value=" ${shopCompanyDetail.bankName}"
							name="bankName" id="bankName" style="width: 325px;" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">开户行支行联行号：</div>
						</td>
						<td><input type="text"
							value="${shopCompanyDetail.bankLineNum}" name="bankLineNum"
							id="bankLineNum" maxlength="50" style="width: 325px;" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">开户银行支行所在地：</div>
						</td>
						<td><select class="combox sele" id="bankProvinceid"
							name="bankProvinceid" requiredTitle="true" childNode="bankCityid"
							selectedValue="${shopCompanyDetail.bankProvinceid}"
							retUrl="${contextPath}/common/loadProvinces">
						</select> <select class="combox sele" id="bankCityid" name="bankCityid"
							requiredTitle="true"
							selectedValue="${shopCompanyDetail.bankCityid}" showNone="false"
							parentValue="${shopCompanyDetail.bankProvinceid}"
							childNode="bankAreaid"
							retUrl="${contextPath}/common/loadCities/{value}">
						</select> <select class="combox sele" id="bankAreaid" name="bankAreaid"
							requiredTitle="true"
							selectedValue="${shopCompanyDetail.bankAreaid}" showNone="false"
							parentValue="${shopCompanyDetail.bankCityid}"
							retUrl="${contextPath}/common/loadAreas/{value}">
						</select></td>
					</tr>
					<tr>
						<td>
							<div align="right">银行开户许可证电子版：</div>
						</td>
						<td><input type="file"
							oldFile="${shopCompanyDetail.bankPermitImg}" id="bankPermitPic"
							name="bankPermitPic"></td>
					</tr>
					<c:if test="${not empty shopCompanyDetail.bankPermitImg}">
						<tr>
							<td>
								<div align="right">原有图片:</div>
							</td>
							<td style="max-width: 500px;">
								<div class="zoombox">
									<span class="photoBox">
										<div class="loadingBox">
											<span class="loading"></span>
										</div> <img
										src="<ls:photo item='${shopCompanyDetail.bankPermitImg}'/>"
										class="zoom" onclick="zoom_image($(this).parent());"
										style="max-height: 60px; max-width: 198px;">
									</span>

									<div class="photoArea" style="display: none;">
										<p>
											<img src="about:blank" class="minifier"
												onclick="zoom_image($(this).parent().parent());" />
										</p>
										<p class="toolBar gc">
											<span><a class="green" href="javascript:void(0)"
												onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
												class="view"><a class="green"
												href="<ls:photo item='${shopCompanyDetail.bankPermitImg}'/>"
												target="_blank">查看原图</a></span>
										</p>
									</div>
								</div>
							</td>
						</tr>
					</c:if>
					<tr>
						<td></td>
						<td align="left" style="padding-left: 7px;">
							<div>
								<c:if test="${status eq 1}">
									<input type="submit" value="保存" class="${btnclass}" />
								</c:if>
								<input type="button" value="返回" class="${btnclass}"
									onclick="window.location='${contextPath}/admin/shopDetail/query'" />
							</div>
						</td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/browser.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/js/viewBigImage.js"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopCompanyDetail.js'/>"></script>
</html>
