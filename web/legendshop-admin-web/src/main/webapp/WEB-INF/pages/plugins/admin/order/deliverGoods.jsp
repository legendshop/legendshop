<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/processOrder.css?1'/>" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<title>保存物流信息</title>
</head>
<body>
		<div style="position: absolute;  width: 410px;">
			<div class="white_box" style="margin-left:15px;">
				<%--<h1 style="cursor: move;">确认发货</h1>
					--%><table width="100%" cellspacing="0" cellpadding="0" border="1"
						class="box_table no-border" style="float:left;margin-top:20px;">
						<tbody>
							<tr>
								<td valign="top" align="center" colspan="2" style="font-weight: bold;">请输入您的物流信息</td>
							</tr>
							<tr>
								<td width="100" valign="top" align="right">订单号： <input
									type="hidden" value="${sub.subNumber}" readonly="readonly" id="subNumber"  name="subNumber"></td>
								<td align="left">${sub.subNumber}</td>
							</tr>
							<tr>
								<td align="right">配送方式：</td>
								<td align="left">
								<select id="ecc_id" name="ecc_id" style="width:214px;">
								       <c:forEach items="${deliveryTypes}" var="dt">
								           <option value="${dt.dvyTypeId}" <c:if test="${dt.dvyTypeId==sub.dvyTypeId}" > selected="selected" </c:if> >${dt.name}</option>
								       </c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td align="right">物流单号：</td>
								<td align="left"><input type="text" value="${sub.dvyFlowId}" style="width:204px;" size="30" id="dvyFlowId" name="dvyFlowId" maxlength="50">
								</td>
							</tr>
						<!-- 	<tr>
								<td valign="top" align="right">操作说明：</td>
								<td align="left">
								 <textarea rows="5" cols="35" id="state_info" name="state_info"></textarea>
								</td>
							</tr> -->
							<tr>
								<td align="center" colspan="2">
								<span class="inputbtn" style="margin-left: 10px;">
									    <c:if test="${sub.status==2}">
									      <input type="button" onclick="fahuo();" class="white_btn" style="cursor:pointer;" value="发货" > 
									    </c:if>
									    <input type="button" onclick="deliverGoods();" class="white_btn"  style="cursor:pointer;width: 115px;background-color: gray;"  value="保存发货信息" > 
								</span>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/deliverGoods.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</body>
</html>