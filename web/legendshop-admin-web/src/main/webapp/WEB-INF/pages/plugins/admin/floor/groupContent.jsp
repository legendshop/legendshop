<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<c:if test="${not empty  group}">
<div class="sm_pic">
	<img src="<ls:images item='${group.pic}' scale='3'/>" width="85" height="85" />
</div>
<p class="it_info">
	<c:if test="${fn: length(group.brief)>34}">
		${fn: substring(group.brief, 0, 35)}...
	</c:if>
	<c:if test="${fn: length(group.brief)<=34}">
		${group.brief}
	</c:if> <br/> 
	<span class="graytxt" style="text-decoration: line-through;"> 
	 	 原价:<fmt:formatNumber type="currency" value="${group.price}" pattern="${CURRENCY_PATTERN}" />
	</span>
</p>
<div class="itprice" style="background-size: 242px 41px;">
	<a href="${contextPath}/group/view/${group.referId}" target="_blank">
			团购价:<span><fmt:formatNumber type="currency"  value="${group.cash}" pattern="${CURRENCY_PATTERN}" /></span>
	</a>
</div>
</c:if>