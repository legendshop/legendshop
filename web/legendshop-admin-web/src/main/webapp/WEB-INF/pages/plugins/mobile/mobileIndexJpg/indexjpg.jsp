<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>


<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<script type="text/javascript">
$.validator.setDefaults({
});
 $(document).ready(function() {
	$("#indexJpgForm").validate({
		rules: {
			alt:  "required",
			title: "required",
			stitle: "required",
			link: "required",
			titleLink: "required",
			file:  {
				required: "#imgName:blank"
			},
			seq: {
              number: true
            }
		},
		messages: {
			alt: "说明文字不能少于5个字符",
			title:"请输入说明文字",
			stitle:"请输入说明文字",
			link:"请输入说明文字",
			titleLink:"请输入说明文字",
			file:"上传文件不能为空",
			seq: {
                number: "请输入数字"
            }
		}
		});
    $("#col1 tr").each(function(i){
	      if(i>0){
	         if(i%2 == 0){
	             $(this).addClass('even');
	         }else{    
	              $(this).addClass('odd'); 
	         }   
	    }
	     });   
	         $("#col1 th").addClass('sortable');
		
	});

</script>

<form:form  action="${contextPath}/admin/indexjpg/mobile/save"  method="post" id="indexJpgForm" enctype="multipart/form-data" >
  <input id="id" name="id" value="${index.id}" type="hidden">
  <table class="${tableclass}" style="width: 100%">
    <thead>
    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  图片编辑</th></tr>
    </thead>
    </table>
  <table class="${tableclass}" id="col1" style="width:100%">
    
     <tr>
      <td width="30%"><div align="right">标题<font color="#ff0000">*</font></div></td>
      <td width="70%">
      
      <input type="text" name="title" id="title" size="30" value="${index.title}"  class="${inputclass}" />
      
      </td>
    </tr>   
    <tr>
      <td width="30%"><div align="right">图片链接地址<font color="#ff0000">*</font></div></td>
      <td width="70%">
      
      <input type="text" name="link" id="link" size="30" value="${index.link}" class="${inputclass}" />
      
      </td>
    </tr>  
      <tr>
      <td>
      	<div align="right">上传图片，不能大于2M，大小为530*290<font color="#ff0000">*</font></div></td>
      <td>
      	
      	<input type="file" name="file" id="file" size="30"/>
      	
      	<input type="hidden" name="imgName" id="imgName" size="30" value="${index.img}" class="${inputclass}" />
      </td>
    </tr>
	<tr>
      <td>
      	<div align="right">次序</div></td>
      <td>
      	 <input type="text" name="seq" id="seq" value="${index.seq}"  maxlength="5" size="5" class="${inputclass}" />
      </td>
    </tr>
     <tr>
      <td width="30%"><div align="right">描述，说明文字</div></td>
      <td width="70%">
      <input type="text" name="des" id="des" size="30" value="${index.des}" class="${inputclass}" />
      </td>
    </tr>
     <c:if test="${index.img!=null}">
    <tr>
    <td ><div align="right">原有图片</div></td>
      <td>
      	<a href="<ls:photo item='${index.img}'/>" target="_blank">
      	<img src="<ls:photo item='${index.img}'/>" height="145" width="265"/></a>
      </td>
    </tr>
    </c:if>
    <tr>
      <td colspan="2">
          <div align="center">
                <input type="submit" value="保存" class="${btnclass}" />
                <input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/indexjpg/mobile/query'" />
            </div>
      
      </td>
    </tr>
  </table>
</form:form>

