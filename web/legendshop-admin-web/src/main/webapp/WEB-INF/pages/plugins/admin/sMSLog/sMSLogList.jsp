<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">系统报表&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">短信发送历史</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/system/smsLog/query"
				id="form1" method="get">
				<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					<span class="item">
						用户名称：<input type="text" name="userName" maxlength="50" value="${sMSLog.userName}"	class="${inputclass}" placeholder="用户名称"/>
					</span>
					<span class="item">
						手机号码：<input type="text" name="userPhone" maxlength="50" value="${sMSLog.userPhone}"	class="${inputclass}" placeholder="手机号码"/>
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/system/smsLog/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="用户名称" property="userName"></display:column>
					<display:column title="手机号码" property="userPhone"></display:column>
					<display:column title="短信内容" style="width:280px;">
						<div class="order_img_name">${item.content}</div>	
					</display:column>
					<display:column title="短信类型" property="type">
					</display:column>
					<display:column title="发送时间" style="width:200px">
						<fmt:formatDate value="${item.recDate}" type="both"
							dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
					</display:column>
					<display:column title="响应码" property="responseCode"></display:column>
					<display:column title="发送状态">
						<c:choose>
							<c:when test="${item.responseCode eq 200}">
								<span>成功</span>
							</c:when>
							<c:otherwise>
								<span>失败</span>
							</c:otherwise>
						</c:choose>
					</display:column>

				</display:table>
				 <div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>

</html>