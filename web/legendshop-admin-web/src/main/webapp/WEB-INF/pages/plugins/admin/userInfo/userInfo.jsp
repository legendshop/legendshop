<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="../back-common.jsp" %>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style>
    .orderlist_one {
        float: left;
        padding-bottom: 0px;
        width: 100%;
        margin-top: 15px;
    }

    .orderlist_one_h4 {
        background: #eeeeee none repeat scroll 0 0;
        border: 1px solid #eeeeee;
        height: 50px;
        line-height: 25px;
        margin-top: -1px;
        margin-bottom: 0px;
        padding-left: 10px;
    }

    .orderlist_one_h4 span {
        padding-left: 20px;
        font-size: 12px;
    }

    .user_list {
        margin-left: 0.5rem;
    }

    .order_xx {
        display: block;
        position: relative;
    }
    label {
    display: inline-block;
    font-weight: 400 !important;
    font-size: 12px;
	}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
<div class="seller_right">
    <table class="${tableclass} title-border" style="width: 100%">
        <tr>
            <th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
                	&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
            </th>
        </tr>
    </table>
    <div class="user_list">
        <div class="seller_list_title">
            <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
                <li class=" am-active"><i></i><a href="javascript:void(0);">基本资料</a></li>
                <li><i></i><a href="<ls:url address="/admin/userinfo/userOrderInfo/${MyPersonInfo.userName}?userId=${MyPersonInfo.id}"/>">会员订单</a></li>
                <li><i></i><a href="<ls:url address="/admin/userinfo/expensesRecord/${MyPersonInfo.id}?userName=${MyPersonInfo.userName}" />">会员消费记录</a></li>
                <li><i></i><a href="<ls:url address="/admin/userinfo/userVisitLog/${MyPersonInfo.userName}?userId=${MyPersonInfo.id}"/>">会员浏览历史</a></li>
                <li><i></i><a href="<ls:url address="/admin/userinfo/userProdComm/${MyPersonInfo.id}?userName=${MyPersonInfo.userName}"/>">会员商品评论</a></li>
                <ls:plugin pluginId="integral">
                    <li><i></i><a href="<ls:url address="/admin/userIntegral/userIntegralDetail/${MyPersonInfo.id}?userName=${MyPersonInfo.userName}"/>">会员积分明细</a></li>
                </ls:plugin>
                <ls:plugin pluginId="predeposit">
                    <li><i></i><a href="<ls:url address="/admin/preDeposit/userPdCashLog/${MyPersonInfo.userName}?userId=${MyPersonInfo.id}"/>">会员预存款明细</a></li>
                </ls:plugin>
                <li><i></i><a href="<ls:url address="/admin/userinfo/userProdCons/${MyPersonInfo.userName}?userId=${MyPersonInfo.id}"/>">会员商品咨询</a></li>
            </ul>
        </div>
    </div>

    <div class="am-cf am-padding">
        <div class="am-fl am-cf" style="text-align: center;width:80%;"><strong class="am-text-primary am-text-lg" style="font-size: 14px;">个人资料</strong> /
            <small>Personal information</small>
        </div>
    </div>
    <div class="am-g">
        <div class="am-u-sm-12 am-u-md-4 am-u-md-push-8">
            <div class="am-panel am-panel-default">
                <div class="am-panel-bd">
                    <div class="am-g">
                        <div class="am-u-md-12">
                            <c:choose>
                                <c:when test="${not empty MyPersonInfo.portraitPic }">
                                    <img alt="用户头像" src="<ls:photo item='${MyPersonInfo.portraitPic}' />" id="infoimg" class="am-img-circle am-img-thumbnail">
                                </c:when>
                                <c:otherwise>
                                    <img alt="用户头像" src="${contextPath}/resources/templets/amaze/images/no-img_mid_.jpg" id="infoimg" class="am-img-circle am-img-thumbnail">
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
            <div class="am-panel am-panel-default">
                <div class="am-panel-bd">
                    <div class="user-info">
                        <p class="user-info-order">当前等级：<strong>${userGrade.name}</strong></p>
                        <p class="user-info-order">成长值：<strong>${empty userGrade.grouthValue?0:userGrade.grouthValue}</strong></p>
                        <ls:plugin pluginId="predeposit"><p class="user-info-order">可用预存款：￥<strong>${MyPersonInfo.availablePredeposit}</strong></p></ls:plugin>
                        <ls:plugin pluginId="integral"><p class="user-info-order">积分：<strong>${empty MyPersonInfo.score? 0:MyPersonInfo.score}</strong>分</p></ls:plugin>
                    </div>
                </div>
            </div>
        </div>

        <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4">
            <form:form cssClass="am-form am-form-horizontal">
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name" width="13%">用户名：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value="${MyPersonInfo.userName}" id="user-name">
                    </div>
                </div>
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name">昵称： </label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value="${MyPersonInfo.nickName}" id="user-name">
                    </div>
                </div>
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name">真实姓名 ：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value="${MyPersonInfo.realName}" id="user-name">
                    </div>
                </div>

                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name">性别： </label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <label>
                            <c:choose>
                                <c:when test="${MyPersonInfo.sex=='M'}">男</c:when>
                                <c:when test="${MyPersonInfo.sex=='F'}">女</c:when>
                                <c:otherwise>保密</c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>

                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-idCard">身份证号码：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="email" readonly="readonly" value="${MyPersonInfo.idCard}" id="user-idCard">
                    </div>
                </div>

                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-phone">手机号码： </label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="email" value="${MyPersonInfo.userMobile}" readonly="readonly" id="user-phone">
                    </div>
                </div>

                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-email">电子邮件 / Email：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="email" value="${MyPersonInfo.userMail}" readonly="readonly" id="user-email">
                    </div>
                </div>

                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-QQ">QQ：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="email" readonly="readonly" value="${MyPersonInfo.qq}" id="user-QQ">
                    </div>
                </div>


                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-QQ">生日：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value='<fmt:formatDate value="${MyPersonInfo.birthDate}" pattern="yyyy-MM-dd" />' id="user-QQ">
                    </div>
                </div>

				<div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name" width="13%">所在地：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value="${MyPersonInfo.province}${MyPersonInfo.city}${MyPersonInfo.area}" id="user-name">
                    </div>
                </div>
                
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name" width="13%">详细地址：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <input type="text" readonly="readonly" value="${MyPersonInfo.userAdds}" id="user-name">
                    </div>
                </div>
                
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-name" width="13%">婚姻状况：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <c:choose>
					     	<c:when test='${MyPersonInfo.marryStatus==1}'>
					     		 <input type="text" readonly="readonly" value="未婚" id="user-name">
					     	</c:when>
					     	<c:when test='${MyPersonInfo.marryStatus==2}'>
							      <input type="text" readonly="readonly" value="$已婚" id="user-name">
							</c:when>
							<c:when test='${MyPersonInfo.marryStatus==3}'>
					     		 <input type="text" readonly="readonly" value="保密" id="user-name">
					     	</c:when>
					     	<c:otherwise>
					     		 <input type="text" readonly="readonly" value="未设置" id="user-name">
					     	</c:otherwise>
		     			</c:choose>
                    </div>
                </div>
                
                <div class="am-form-group">
                    <label class="am-u-sm-3 am-form-label" for="user-intro">兴趣爱好：</label>
                    <div class="am-u-sm-9" style="padding-left: 2px;">
                        <textarea readonly="readonly" id="user-intro" rows="5" class="">${MyPersonInfo.interest}</textarea>
                    </div>
                </div>
                <div class="am-form-group" style="text-align: center;width:112%;">
                    <input type="button" value="返回" class="${btnclass}"
                           onclick="goBack()"/>
                </div>
            </form:form>
        </div>
    </div>
</div>
</div>
</div>
</body>
<script type="text/javascript">
    function goBack() {
        window.history.go(-1)
    }
</script>
</html>
        