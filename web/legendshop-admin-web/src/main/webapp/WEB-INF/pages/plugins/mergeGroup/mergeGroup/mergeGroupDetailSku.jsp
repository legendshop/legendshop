
<%@ page language="java" pageEncoding="UTF-8"%>
<%-- <link type="text/css" href="<ls:templateResource item='/resources/common/css/base.css'/>" rel="stylesheet"/> --%>

<div class="goods-tit-table">
    <table width="80%" cellpadding="0" cellspacing="0">
        <tr>
            <th width="105"><div style="margin-left:15px;" class="goods-img">图片</div></th>
            <th width="190">商品名称/规格</th>
            <th width="100">商品价格(元)</th>
            <th width="100">可销售库存</th>
            <th width="90">拼团价(元)</th>
            <th width="70">操作</th>
        </tr>
    </table>
</div>
<div class="goods-con-table">
    <table width="80%" cellpadding="0" cellspacing="0" class="prodTable">
    	<c:if test="${empty prodLists}">
			<tr class="first-leve"><td colspan='8' height="60" style="text-align: center;color: #999;">暂无数据</td></tr>
		</c:if>
		
		<c:forEach items="${prodLists}" var="prod">
	        <tr class="first-leve" data-id="${prod.prodId}">
	            <td width="110">
	                <div class="goods-img">
	                <span class="switch open">-</span>
	                <img src="<ls:photo item='${prod.prodPic}'/>" alt="">
	                </div>
	            </td>
	            <td width="200"><div class="goods-name">${prod.prodName}</div></td>
	           <!--  <td width="149">--</td> -->
	            <td width="110">${prod.cash}</td>
	            <td width="110">${prod.stocks}</td>
	            <td width="100"></td>
	            <td width="60"></td>
	        </tr>
	        
	        <c:forEach items="${prod.dtoList}" var="sku">
		        <tr class="second-leve" data-id="${prod.prodId}">
		            <td width="110">
		                <div class="goods-img">
		            		<input type="checkbox" class="second-check checkedSku" data-prodId="${prod.prodId}" style="opacity: 0;">
		                 <img src="<ls:photo item='${sku.skuPic}'/>" alt="">
		                </div>
		            </td>
		            <td width="200">
		            	<div class="goods-name">
		            		<c:if test="${empty sku.cnProperties}">默认规格</c:if>
		            		<c:if test="${not empty sku.cnProperties}">${sku.cnProperties }</c:if>
		            	</div>
		            </td>
		            <td width="110">${sku.skuPrice}</td>
		            <td width="110">${sku.skuStocks}</td>
		            <td width="100">
		            	<input type="text" class="set-input mergePrice" value="${sku.mergePrice}" disabled="disabled">
		            </td>
		            <td width="60"></td>
		        </tr>
		     </c:forEach>
		     
	     </c:forEach>
        
    </table>
</div>

<script type="text/javascript">
$(function(){
	//点击商品-
	$(".goods-con-table").on("click",".switch",function(){
		var str = $(this).text();
		if(str == "-"){
			var prodId = $(this).parents(".first-leve").attr("data-id");
			$(".second-leve[data-id='"+prodId+"']").css("display","none");
			$(this).text("+");
		}
		if(str == "+"){
			var prodId = $(this).parents(".first-leve").attr("data-id");
			$(".second-leve[data-id='"+prodId+"']").css("display","");
			$(this).text("-");
		}
		
	})
})
</script>