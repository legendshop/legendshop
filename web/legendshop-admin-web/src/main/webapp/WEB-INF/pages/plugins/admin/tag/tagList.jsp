<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <form:form  action="${contextPath}/admin/tag/query" id="form1" method="get">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  标签管理</th></tr>
		    </thead>
		     <tbody><tr><td>
 <div align="left" style="padding: 3px">
           <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
                             标签名称&nbsp;<input type="text" name="name" maxlength="50" value="${tag.name}" class="${inputclass}" />
            <input type="button" onclick="search()" value="搜索" class="${btnclass}" />
            <input type="button" value="创建标签" class="${btnclass}" onclick='window.location="<ls:url address='/admin/tag/load'/>"'/>
 </div>
 </td></tr></tbody>
	    </table>
        
    </form:form>
    <div align="center">
      <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	 <display:table name="list" requestURI="/admin/tag/query" id="item" export="false" class="${tableclass}"  sort="external">
      <display:column title="名称" style="width: 20%" property="name"></display:column>
      <display:column title="状态"  style="width: 10%"> 
              <font color="red">
				  <ls:optionGroup type="label" required="false" cache="true"
	                beanName="SHOP_STATUS" selectedValue="${item.status}" defaultDisp=""/></font>
      </display:column>
      
      <display:column title="楼层类型"  style="width:10%">
         <c:choose>
           <c:when test="${item.type=='A'}">
                                      广告
           </c:when>
            <c:when test="${item.type=='N'}">
                                      文章
           </c:when>
            <c:when test="${item.type=='P'}">
                                      商品
           </c:when>
         </c:choose>
      </display:column>
         <c:set var="tagItemList" value="${item.tagItemList}" scope="request"/>
        <display:column title="子标签" style="width:50%">
                <c:if test="${fn:length(tagItemList) > 0}">
		  <display:table name="tagItemList"  id="child" class="am-table am-table-bordered" cellpadding="0" cellspacing="0">
		      <display:column title="编号" property="itemId"></display:column>
		      <display:column title="名称" property="itemName" style="width:60px"></display:column>
		      <display:column title="操作" media="html" autolink="true" style="width:130px">
      	  	 <div class="am-btn-toolbar">
		  		<div class="am-btn-group am-btn-group-xs">
	      	   <div data-am-dropdown="" class="am-dropdown">
                <button data-am-dropdown-toggle="" class="am-btn am-btn-default am-btn-xs am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
                <ul class="am-dropdown-content">
                  <li><a href="<ls:url address='/admin/tag/loadTagItems/${child.itemId}'/>">编辑</a></li>
                  
                  <li><a href='javascript:deleteBychildId("${child.itemId}","${child.itemName}")'>删除</a></li>
                </ul>
              </div>
              </div>
           </div>
	      	</display:column>
		  </display:table>
		  </c:if>
		  
        </display:column>
      
      
		 <display:column title="操作" media="html" style="width:130px">
		     <div data-am-dropdown="" class="am-dropdown">
                <button data-am-dropdown-toggle="" class="am-btn am-btn-default am-btn-xs am-dropdown-toggle"><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
                <ul class="am-dropdown-content">
			  		<li><a href="<ls:url address='/admin/tag/AddTagItems/${item.tagId}'/>">创建子标签</a></li>
		  		
				  	  <li><a href="<ls:url address='/admin/tag/update/${item.tagId}'/>">编辑</a></li>
                  
                  	  <li><a href='javascript:deleteById("${item.tagId}","${item.name}")'>删除</a></li>
                </ul>
              </div>
         </display:column> 
      
	    </display:table>
       <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
        <div style="height:100px;"></div>
    </div>
        <script language="JavaScript" type="text/javascript">
			 
			  function deleteById(id,name) {
				  art.dialog.confirm("确定删除 ' "+name+" ' 标签 ?", function () {
					  window.location = "<ls:url address='/admin/tag/delete/" + id + "'/>";
				  });
			    }
			  
			  function deleteBychildId(id,name) {
				  art.dialog.confirm("确定删除 ' "+name+" '子标签 ?", function () {
					  window.location = "<ls:url address='/admin/tag/deleteTagItems/" + id + "'/>";
				  });
			    }
				highlightTableRows("item");
				
				function search(){
				  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
				  	$("#form1")[0].submit();
				}
		</script>



