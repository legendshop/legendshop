<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page import="java.util.Date"%>

<!-- 获取当前日期 -->
<c:set var="nowDate">  
    <fmt:formatDate value="<%=new Date()%>" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>  
</c:set> 

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style type="text/css">
   .editAdd_load{position: absolute;top:50%;left:50%;margin:-16px 0 0 -16px;z-index: 99;display: none;}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">平台营销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">平台红包</span></th>
				</tr>
			</table>
		<form:form  action="${contextPath}/admin/coupon/platform" id="form1" method="get">
			<div class="criteria-div">
				<span class="item">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					红包名称：<input type="text" name="couponName" maxlength="50" value="${coupon.couponName}" class="${inputclass}" />
					<input type="submit" onclick="search()" value="搜索" class="${btnclass}" />
					<input class="${btnclass}" type="button" value="创建" onclick="window.location='<ls:url address="/admin/coupon/loadRedPacket"/>'" /> 
				</span>
			</div>
		</form:form>
		<div align="center" class="order-content-list">
		      <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
			  <display:table name="list" requestURI="/admin/coupon/platform" id="item" export="false" class="${tableclass}" style="width:100%">
			  	<display:column title="
				  	<span>
			           <label class='checkbox-wrapper'>
							<span class='checkbox-item'>
								<input type='checkbox' name='platfromId' value='${item.couponId}' status='${item.status}' class='checkbox-input selectAll' onclick='selectAll(this);'/>
								<span class='checkbox-inner' style='margin-left:10px;'></span>
							</span>
					   </label>	
					  </span>">
				  	  <label class="checkbox-wrapper">
						<span class="checkbox-item">
							<input type="checkbox" name='platfromId' value='${item.couponId}' status='${item.status}' class="checkbox-input selectOne" onclick="selectOne(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				      </label>	
			  	</display:column>
		   		<display:column title="序号">${item_rowNum}</display:column>
		   		<display:column title="红包名称" property="couponName"></display:column>
		   		<display:column title="面额" property="offPrice"></display:column>
		   		<display:column title="消费限额" property="fullPrice"></display:column>
		   		<display:column title="开始时间" sortable="true" sortName="startDate">
		   		  <fmt:formatDate value="${item.startDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		   		</display:column>
		   		<display:column title="结束时间" sortable="true" sortName="endDate">
		   		  <fmt:formatDate value="${item.endDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		   		</display:column>
		   		<display:column title="领取方式" sortable="true" sortName="getType">
		   			<c:choose>
		            	<c:when test="${item.getType == 'free'}">免费领取</c:when>
		                <c:when test="${item.getType == 'points'}">积分兑换</c:when>
		                <c:when test="${item.getType == 'pwd'}">卡密兑换</c:when>
		                <c:when test="${item.getType == 'draw'}">大转盘红包</c:when>
		            </c:choose>
		   		</display:column>
		   		<display:column title="红包类型" sortable="true" sortName="couponType">
		   			<c:choose>
		            	<c:when test="${item.couponType == 'common'}">通用红包</c:when>
		                <c:when test="${item.couponType == 'shop'}">店铺红包</c:when>
		            </c:choose>
		        </display:column>
		   		<display:column title="状态">
		   			<c:choose>
			   			<c:when test="${nowDate<=item.endDate}">
				   			<c:choose>
				            	<c:when test="${1==item.status}"><font>有效</font></c:when>
				                <c:when test="${0==item.status}">失效</c:when>
				            </c:choose>
			   			</c:when>
		   			<c:otherwise><font>过期</font></c:otherwise>
		   			</c:choose>
		        </display:column>
		   		<display:column title="总数/已领取">
					<font>${item.couponNumber}/${item.bindCouponNumber}</font>
		        </display:column>
				<display:column title="操作" media="html" style="width:125px;">
					<div class="table-btn-group">
						<c:if test="${nowDate<=item.endDate}">
							<c:choose>
								<c:when test="${item.status == 1}">
									<button class="tab-btn" name="statusImg" itemId="${item.couponId}" itemName="${item.couponName}" status="${item.status}">
										下线
									</button>
									<span class="btn-line">|</span>
								</c:when>
								<c:when test="${item.status==0}">
									<button class="tab-btn" name="statusImg" itemId="${item.couponId}" itemName="${item.couponName}" status="${item.status}">
										上线
									</button>
									<span class="btn-line">|</span>
								</c:when>
							</c:choose>
						</c:if>
						<div class="am-dropdown" data-am-dropdown>
							<button class="tab-btn am-dropdown-toggle" data-am-dropdown-toggle>
								<span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span>
							</button>
							<ul class="am-dropdown-content">
								<li><a href="<ls:url address='/admin/coupon/watchRedPacket/${item.couponId}'/>">查看详情</a></li>
								<c:if test="${item.getType eq 'pwd' }">
									<c:if test="${nowDate<=item.endDate}">
										<c:if test="${1==item.status}">
											<li><a href="javascript:void(0);" onclick="sendCoupon(${item.couponId},'${item.couponName}')">按用户发放</a></li>
										</c:if>
									 </c:if>
								</c:if>
								<li><a href='javascript:confirmDelete("${item.couponId}","${fn:escapeXml(fn:replace(item.couponName,"' '","&acute;"))}")'>删除</a></li>
							</ul>
						</div>
					</div>
				</display:column>
			</display:table>
			<div class="clearfix">
		   		<div class="fl">
		    		<input type="button" value="批量删除" id="batch-del" class="batch-btn">
		    		<input type="button" value="批量下线" id="batch-off" class="batch-btn">
				</div>
		   		<div class="fr">
		   			 <div cla ss="page">
		    			 <div class="p-wrap">
		        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					 	</div>
		  			 </div>
		  		</div>
		   	</div>
		
		<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/>
		 <table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
		 	<tr>
			 <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
			 	1.按用户发放 :  只有在卡密兑换的方式和有效的状态下才能发放<br>
		     </td>
		   <tr>
		</table> 
	</div>
   </div>
   </div>
   </body>
<script language="JavaScript" type="text/javascript">

	jQuery(document).ready(function() {
		$("button[name='statusImg']").click(function(event){
      		$this = $(this);
      		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/coupon/updatestatus/", $this,"${contextPath}");
		});
		highlightTableRows("item");
		
		$("#checkedAll").on("click",function(){
			if($(this).is(":checked")){
				$("input[type=checkbox][name=platfromId]").prop("checked",true);
			}else{
				$("input[type=checkbox][name=platfromId]").prop("checked",false);
			}
		});
	});
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
	
	function getCheckedFloor(status){
		var selectorStr="";
		if(status==""){
			selectorStr="input[type=checkbox][name=platfromId]:checked";
		}else{
			selectorStr="input[type=checkbox][name=platfromId][status="+status+"]:checked";
		}
		var platfromIds=$(selectorStr);
		
		var platfromIdArr=new Array();
		platfromIds.each(function(index, domEle){
			var value=$(domEle).val();
			platfromIdArr.push(value);
		});
		return platfromIdArr.toString();
	}
	
	$("#batch-off").on("click",function(){
		var platfromIdStr=getCheckedFloor("");
		if(platfromIdStr==""){
			layer.alert("请选择要下线的记录", {icon:0});
			return false;
		}
		jQuery.ajax({
            url: "${contextPath}/admin/coupon/batchOffline/" + platfromIdStr,
            type: 'get',
            async: false, //默认为true 异步  
            dataType: 'json',
            success: function (data) {
            	if(data=="OK"){
            		layer.msg("批量下线平台红包成功。", {icon:1, time:1000}, function(){
	            		setInterval(function(){
	            			location.href="${contextPath}/admin/coupon/platform";
	            		},1000);
            		});
            	}else{
            		layer.alert("批量下线平台红包失败。", {icon:2});
            	}
            }
        });
	});
	
	$("#batch-del").on("click",function(){
		var platfromIdStr=getCheckedFloor(0);
		if(platfromIdStr==""){
			layer.alert("请选择要删除且已经失效或过期的记录", {icon:0});
			return false;
		}
		jQuery.ajax({
            url: "${contextPath}/admin/coupon/batchDelete/" + platfromIdStr,
            type: 'get',
            async: false, //默认为true 异步  
            dataType: 'json',
            success: function (data) {
            	if(data=="OK"){
            		layer.msg("批量删除平台红包成功。", {icon:1, time:1000}, function(){
	            		setInterval(function(){
	            			location.href="${contextPath}/admin/coupon/platform";
	            		},1000);
            		});
            	}else{
            		layer.alert(data, {icon:2});
            	}
            }
        });
	});

	function confirmDelete(id, name) {
		layer.confirm("确定要删除'" + name + "'吗？", {icon:3}, function() {
			var result;
			jQuery.ajax({
				url : "${contextPath}/admin/coupon/delete/" + id,
				type : 'post',
				async : false, //默认为true 异步   
				dataType : 'json',
				error : function(jqXHR, textStatus, errorThrown) {
					// alert(textStatus, errorThrown);
				},
				success : function(retData) {
					result = retData;
				}
			});

			if ('OK' == result) {
				layer.msg('删除成功', {icon:1, time:1000}, function(){
					window.location.reload(true);
				});
			} else {
				layer.msg(result, {icon:2});
			}
		});
	}
	
	function highlightTableRows(tableId) {
		var previousClass = null;
		var table = document.getElementById(tableId);
		if (table == null)
			return;
		var tbody;
		if (table.getElementsByTagName("tbody") != null) {
			tbody = table.getElementsByTagName("tbody")[0];
		}
		if (tbody == null) {
			var rows = table.getElementsByTagName("tr");
		} else {
			var rows = tbody.getElementsByTagName("tr");
		}
		for (i = 0; i < rows.length; i++) {
			rows[i].onmouseover = function() {
				previousClass = this.className;
				this.className = 'pointercolor'
			};
			rows[i].onmouseout = function() {
				this.className = previousClass
			};
		}
	}

	function issueCoupon(id) {
		layer.open({
			id : 'Retested',
			title : '提示',
			type: 2,
			content: "${contextPath}/admin/coupon/issueCoupon/" + id,
		});
	}
	
	function sendCoupon(data,name){
		var page = "${contextPath}/admin/coupon/uploadUI?data="+data;
		var setting = { title:"["+name+"] 按用户发放", id:"loadImportprod", width:500, height:300,close: function (obj){ }};
		layer.open({
			title: "["+name+"] 按用户发放",
			id: "loadImportprod",
			type: 2,
			content: "${contextPath}/admin/coupon/uploadUI?data="+data,
			area: ['500px', '250px']
		});
		
	}
</script>
</html>
