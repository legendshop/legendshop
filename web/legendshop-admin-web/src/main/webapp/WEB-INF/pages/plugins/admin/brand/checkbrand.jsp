<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="../back-common.jsp"%>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>品牌查看 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/red/css/viewBigImage.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
			 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<table class="${tableclass} no-border" style="width: 100%">
	    		<th class="no-bg title-th">
			    	<c:choose>
			    		<c:when test="${brand.status==-1}">
			    			<span class="title-span"><a href="<ls:url address="/admin/brand/query"/>">品牌管理</a>  ＞  <span style="color:#0e90d2;">审核品牌详情</span></span>
			    		</c:when>
			    		<c:otherwise>
			    			<span class="title-span"><a href="<ls:url address="/admin/brand/query"/>">品牌管理</a>  ＞  <span style="color:#0e90d2;">查看品牌详情</span></span>
			    		</c:otherwise>
			    	</c:choose>
	    		</th>
	   		 </table>
	   		  <table  align="center" class="${tableclass} no-border content-table" style="margin-left:30px;margin-top:15px;">
	   		  	<tr>
		            <td align="right" style="width: 200px;">品牌名称：</td>
		            <td align="left" style="padding-left: 2px;"><input  type="text" style="width: 150px;" value="${brand.brandName}" disabled="disabled"></td>
		        </tr>
		       <tr>
		            <td align="right">PC品牌LOGO：</td>
		            <td align="left" style="padding-left: 2px;">
		                <c:if test="${not empty brand.brandPic}">
							<div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${brand.brandPic}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>
								
								<div class="photoArea" style="display:none;">
									<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
										<p class="toolBar gc">
										<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${brand.brandPic}'/>" target="_blank">查看原图</a></span>
										</p>
								</div>
							</div>
		                </c:if>
		            </td>
		        </tr>
		       <tr>
		            <td align="right">移动端品牌LOGO：</td>
		            <td align="left" style="padding-left: 2px;">
		                <c:if test="${not empty brand.brandPicMobile}">
							<div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${brand.brandPicMobile}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>

								<div class="photoArea" style="display:none;">
									<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
										<p class="toolBar gc">
										<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${brand.brandPicMobile}'/>" target="_blank">查看原图</a></span>
										</p>
								</div>
							</div>
		                </c:if>
		            </td>
		        </tr>
		        <tr>
		            <td align="right">品牌大图：</td>
		            <td align="left" style="padding-left: 2px;">
		                <c:if test="${not empty brand.bigImage}">
							<div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${brand.bigImage}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>
								
								<div class="photoArea" style="display:none;">
									<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
										<p class="toolBar gc">
										<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${brand.bigImage}'/>" target="_blank">查看原图</a></span>
										</p>
								</div>
							</div>
		                </c:if>
		            </td>
		        </tr>
		        
		        <tr>
		            <td align="right">品牌授权书：</td>
		            <td align="left" style="padding-left: 2px;">
						<c:choose>
							<c:when test="${not empty brand.brandAuthorize}">
								<div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${brand.brandAuthorize}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>

									<div class="photoArea" style="display:none;">
										<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
										<p class="toolBar gc">
											<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${brand.brandAuthorize}'/>" target="_blank">查看原图</a></span>
										</p>
									</div>
								</div>
							</c:when>
							<c:otherwise>暂无</c:otherwise>
						</c:choose>
		            </td>
		        </tr>
		        <tr>
		            <td align="right" valign="top">品牌介绍：</td>
		            <td align="left" style="padding-left: 2px;"><textarea name="brief" cols="60" style=" padding:5px;margin-right:10px;width:300px;" rows="5" id="brief" class="base_text" disabled="disabled">${brand.brief}</textarea></td>
		        </tr>
		        <c:if test="${brand.status!=-1}">
			         <tr>
			            <td align="right">处理人：</td>
			            <td align="left" style="padding-left: 2px;">${brand.checkUserName}</td>
			        </tr>
			         <tr>
			            <td align="right" valign="top">处理意见：</td>
			            <td align="left" style="padding-left: 2px;"><textarea name="brief" cols="30" style=" padding:5px;margin-right:10px;width:300px;" rows="5" id="brief" class="base_text" disabled="disabled">${brand.checkOpinion}</textarea></td>
			        </tr>
			         <tr>
			            <td align="right">处理时间：</td>
			            <td align="left" style="padding-left: 2px;"><fmt:formatDate value="${brand.checkTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" /></td>
			        </tr>
			        <tr>
			            <td align="right">绑定的商品：</td>
			            <td align="left" style="padding-left: 2px;"><a href="javascript:void(0);" onclick="javascript:window.location.href='${contextPath}/admin/product/query?brandId=${brand.brandId}&brandName=${brand.brandName}'">查看</a></td>
			        </tr>
		        </c:if>
		        <tr>
		            <td>&nbsp;</td>
		            <td align="left" style="padding-left: 2px;">
		            	<c:if test="${brand.status==-1}">
		                	<input type="button" value="审核通过" onclick="check(${brand.brandId},1);" style="cursor:pointer;" class="${btnclass}">
		                	<input type="button" value="审核失败" onclick="check(${brand.brandId},-2);" style="cursor:pointer;" class="${btnclass}"/>
		                </c:if>
		                <input type="button" value="返回"  style="cursor:pointer;" class="${btnclass}" id="returnBtn"/>
		            </td>
		        </tr>
	   		  </table>
	   		  </div>
	   		  </div>
	   		  </body>
<script src="<ls:templateResource item='/resources/templets/red/js/viewBigImage.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/checkBrand.js'/>"></script>
<script language="javascript">
	var contextPath = "${contextPath}";
</script>
			</html>