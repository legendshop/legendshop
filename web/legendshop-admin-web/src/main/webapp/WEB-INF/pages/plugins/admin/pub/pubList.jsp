<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>公告管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

	<table class="${tableclass} title-border" style="width: 100%">
	    <tr>
	        <th class="title-border">公告管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">公告管理</span></th>
	    </tr>
	</table>

    <div class="seller_list_title">
        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li <c:if test="${empty bean.type}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/pub/query"/>">所有公告</a></li>
            <li <c:if test="${bean.type eq 0}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/pub/query?type=0"/>">买家公告</a></li>
            <li <c:if test="${bean.type eq 1}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/pub/query?type=1"/>">卖家公告</a></li>
        </ul>
    </div>
	<form:form action="${contextPath}/admin/pub/query" id="form1" method="get">
	    <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
	    <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
	     <table class="${tableclass}">
	     	<div class="criteria-div">
		     	<span class="item">
			        <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
			                     标题：<input type="text" name="title" maxlength="50" value="${bean.title}" class="${inputclass}" placeholder="请输入标题搜索"/>
	                <input type="hidden" id="type" name="type" value="${bean.type}"/>
	                <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
	                <input type="button" value="创建公告" class="${btnclass}" onclick='window.location="${contextPath}/admin/pub/load"'/>
		        </span>
	        </div>
	    </table>
	</form:form>

	<div align="center" class="order-content-list">
	    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	    <display:table name="list" requestURI="/admin/pub/query" id="item" export="false" class="${tableclass}">
	        <display:column title="顺序" style="min-width:60px"><%=offset++%>
	        </display:column>
	        <display:column title="标题" style="max-width:400px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">
	            <a href="${PC_DOMAIN_NAME}/pub/${item.id}" target="_blank">${item.title }</a>
	        </display:column>
	        <display:column title="创建时间" property="recDate" format="{0,date,yyyy-MM-dd}" sortable="true" sortName="recDate"></display:column>
	        <display:column title="有效日期" property="startDate" format="{0,date,yyyy-MM-dd}" sortable="true" sortName="startDate"></display:column>
	        <display:column title="结束日期" property="endDate" format="{0,date,yyyy-MM-dd}" sortable="true" sortName="endDate"></display:column>
	        <display:column title="操作" media="html" style="width:210px;">
               <div class="table-btn-group">
                   <c:choose>
                       <c:when test="${item.status == 1}">
                           <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.title}" status="${item.status}">下线</button>
                           <span class="btn-line">|</span>
                       </c:when>
                       <c:otherwise>
                           <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.title}" status="${item.status}">上线</button>
                           <span class="btn-line">|</span>
                       </c:otherwise>
                   </c:choose>

                   <button class="tab-btn" onclick="window.location='${contextPath}/admin/pub/load/${item.id}'">编辑</button>
				<span class="btn-line">|</span>
                   <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
               </div>
	        </display:column>
	    </display:table>
	    <div class="clearfix">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
      	</div>
	</div>
	<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
	    <tr>
	        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
	            1.买家公告：出现在会员中心左上角，用于对注册用户发布公告信息。<br>
	            2.卖家公告：出现在卖家中心的右下角，用于对卖家发布公告信息。<br>
	        </td>
	    <tr>
	</table>
</div>
</div>
</body>
<script language="JavaScript" type="text/javascript">

    function deleteById(id) {
    	layer.confirm('确定删除？', {icon:3}, function(){
    		window.location = "${contextPath}/admin/pub/delete/" + id;
    	});
    }
    $(document).ready(function() {
        $("button[name='statusImg']").click(function(event) {
        	$this = $(this);
            initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/pub/updatestatus/", $this, "${contextPath}");
        });
        //   highlightTableRows("item"); 
    });
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }

</script>
</html>
