<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>订单管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/templets/amaze/css/orderAdminDetail.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;border-bottom:1px solid #efefef !important;">
				<thead>
					<tr>
						<th style="border:none;text-align: left" class="no-bg">
							<span class="title-span">
								积分订单管理  ＞  
								<span style="color:#0e90d2;">查看订单详情</span>
							</span>
						</th>
					</tr>
				</thead>
			</table>
			<div class="order_pay">
				<div class="order_pay_note">
					<span style="margin-right:30px;">订单号：${order.orderSn}</span><span>状态：<b
						style="color: #fd6760;"> <c:choose>
								<c:when test="${order.orderStatus==0 }">
						        已提交
						 </c:when>
								<c:when test="${order.orderStatus==1 }">
						       已发货
						 </c:when>
								<c:when test="${order.orderStatus==2 }">
						       已完成
						 </c:when>
								<c:when test="${order.orderStatus==3 }">
						      已取消
						 </c:when>
							</c:choose>
					</b>
					</span>
				</div>

				<div class="ncm-order-step" id="order-step">
					<dl class="step-first current">
						<dt>生成订单</dt>
						<dd class="bg"></dd>
						<dd title="下单时间" class="date">
							<fmt:formatDate value="${order.addTime}" type="both" />
						</dd>
					</dl>
					<dl class="<c:if test="${not empty order.dvyTime}">current</c:if>">
						<dt>商家发货</dt>
						<dd class="bg"></dd>
						<c:if test="${not empty order.dvyTime}">
							<dd title="发货时间" class="date">
								<fmt:formatDate value="${order.dvyTime}" type="both" />
							</dd>
						</c:if>
					</dl>
					<dl
						class="<c:if test="${not empty order.finallyTime}">current</c:if>">
						<dt>确认收货</dt>
						<dd class="bg"></dd>
						<c:if test="${not empty order.finallyTime}">
							<dd title="确认收货时间" class="date">
								<fmt:formatDate value="${order.finallyTime}" type="both" />
							</dd>
						</c:if>
					</dl>
				</div>


				<div class="order_pay_tab">
					<span style="line-height: 30px;">商品清单</span>
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_pay_table">
						<tbody>
							<tr>
								<th width="100">商品图片</th>
								<th width="232">商品名称</th>
								<th width="100">单价</th>
								<th width="100">兑换积分</th>
								<th width="100">数量</th>
								<th width="100">总积分</th>
							</tr>
							<!-- S 商品列表 -->
							<c:forEach items="${order.orderItemDtos}" var="orderItem"
								varStatus="orderItemStatues">
								<tr>
									<td align="center"><a target="_blank"
										href="${PC_DOMAIN_NAME}/integral/view/${orderItem.prodId}">
											<img width="62" height="62"
											src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
									</a></td>
									<td align="left"><a target="_blank" class="blue"
										href="${PC_DOMAIN_NAME}/integral/view/${orderItem.prodId}">${orderItem.prodName}</a>
									</td>
									<td align="center"><b class="red">¥${orderItem.price}</b>
									</td>
									<td align="center">${orderItem.exchangeIntegral}</td>
									<td align="center">${orderItem.basketCount}</td>
									<td align="center">${orderItem.totalIntegral}</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>

				</div>
				<div class="order_follow">
					<ul class="order_follow_top" style="padding:0;margin-bottom:0;">
						<li style="cursor: pointer">物流信息</li>
					</ul>
					<div class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody id="order_kuaidi">
								<tr>
									<th align="left">时间</th>
									<th align="left">信息</th>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
				<div class="order_pay_info">
					<%-- <h3>订单信息</h3>
					<div class="order_pay_con">
						<div class="order_pcon_in">
							<h4>收货人信息</h4>
							<ul class="order_pcon_ul">
								<li>下单用户： <a style="cursor: pointer;" id="show_userinfo"
									target="_blank"
									href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'><span>${order.userName}</span></a>
									<div class="more">
										<span class="arrow"></span>
										<ul id="shipping_ul">
										</ul>
									</div>
								</li>
								<li>收货人：${order.usrAddrSubDto.receiver}</li>
								<li>收货地址：${order.usrAddrSubDto.subAdds}</li>
								<li>邮政编码：${order.usrAddrSubDto.subPost}</li>
								<li>电话号码：${order.usrAddrSubDto.telphone}</li>
								<li>手机号码：${order.usrAddrSubDto.mobile}</li>
							</ul>
						</div>
						<div class="order_pcon_in">
							<h4>支付方式及配送方式</h4>
							<ul class="order_pcon_ul">
								<li>物流公司： <a href="${order.delivery.delUrl}"
									target="_blank">${order.delivery.delName}</a></li>
								<li>发货时间： <fmt:formatDate value="${order.dvyTime}"
										pattern="yyyy-MM-dd HH:mm" /></li>
								<li>物流单号 ：${order.dvyFlowId}</li>
							</ul>
						</div>
					</div> --%>
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_info_table">
						<thead>
							<tr>
								<th style="text-align: left;" colspan="3"><span style="margin-left:10px;">订单信息</span></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>收货人信息</li>
											<li>下单用户： <a style="cursor: pointer;" id="show_userinfo"
												target="_blank"
												href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'><span>${order.userName}</span></a>
												<div class="more">
													<span class="arrow"></span>
													<ul id="shipping_ul">
													</ul>
												</div>
											</li>
											<li>收货人：${order.usrAddrSubDto.receiver}</li>
											<li>收货地址：${order.usrAddrSubDto.subAdds}</li>
											<li>邮政编码：${order.usrAddrSubDto.subPost}</li>
											<li>电话号码：${order.usrAddrSubDto.telphone}</li>
											<li>手机号码：${order.usrAddrSubDto.mobile}</li>
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>支付方式及配送方式</li>
											<li>物流公司： <a href="${order.delivery.delUrl}"
												target="_blank">${order.delivery.delName}</a></li>
											<li>发货时间： <fmt:formatDate value="${order.dvyTime}"
													pattern="yyyy-MM-dd HH:mm" /></li>
											<li>物流单号 ：${order.dvyFlowId}</li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
					</table>

					<div class="order_pay_money">
						<ul>
							<li>商品总积分：¥${order.integralTotal}</li>
							</span>
							</li>
						</ul>
					</div>
				</div>

				<div style="margin-top: 10px; overflow: hidden; padding: 0 0 10px; width: 100%; text-align: center; padding: 20px 0 10px;">
					<input type="button" onclick="window.history.go(-1)" class="${btnclass}" value="返回">&nbsp;&nbsp;
				</div>

			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		findKuaidiInfo();
	});

	function findKuaidiInfo() {
		var dvyFlowId = "${order.delivery.dvyFlowId}";
		if (dvyFlowId != null || dvyFlowId != undefined || dvyFlowId != "") {
			$.ajax({
				url : "${contextPath}/admin/order/findKuaidiInfo",
				data : {
					"dvyId" : '${order.delivery.dvyTypeId}',
					"dvyFlowId" : '${order.delivery.dvyFlowId}',
					"reciverMobile" : '${order.usrAddrSubDto.mobile}'
				},
				type : 'POST',
				async : false, //默认为true 异步   
				dataType : 'json',
				error : function(jqXHR, textStatus, errorThrown) {
					return;
				},
				success : function(result) {
					if (result == "fail" || result == "") {
						$("#order_kuaidi tr:eq(0)").after("<tr><td>暂无快递信息</td></tr>");
					} else {
						var list = eval(result);
						var html = "";
						for ( var obj in list) { //第二层循环取list中的对象 
							html += "<tr><td>" + list[obj].ftime + "</td><td>" + list[obj].context + "</td> </tr>";
						}
						$("#order_kuaidi tr:eq(0)").after(html);
					}
				}
			});
		}
	}
</script>
</html>
