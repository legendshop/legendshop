<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ls:auth ifAnyGranted="F_VIEW_ALL_DATA">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">首页/今天统计</strong> (注：每半小时更新一次)</div>
    </div>

    <ul class="am-avg-sm-1 am-avg-md-3 am-margin am-padding am-text-center admin-content-list ">
      <li><a href="${contextPath}/admin/system/userDetail/query" class="am-text-success"><span class="am-icon-btn am-icon-users"></span><br/>当天新增会员数 <br/>${USER_COUNTS}</a></li>
      <!--  
       <li><a href="${contextPath}/admin/loginHistory/query" class="am-text-secondary"><span class="am-icon-btn am-icon-user-md"></span><br/>在线用户<br/>${onLineUser}</a></li>
       -->
      <li><a href="${contextPath}/admin/product/query" class="am-text-warning"><span class="am-icon-btn am-icon-shopping-cart"></span><br/>当天新增商品数<br/>${PROD_COUNTS}</a></li>
      <li><a href="${contextPath}/admin/shopDetail/query" class="am-text-danger"><span class="am-icon-btn am-icon-weixin"></span><br/>当天新增店铺数<br/>${SHOP_COUNTS}</a></li>
    </ul>
    
    <ul class="am-avg-sm-1 am-avg-md-3 am-margin am-padding am-text-center admin-content-list ">
      <li><a href="${contextPath}/admin/order/processing" class="am-text-success"><span></span><br/>当天新增订单数<br/>${SUB_COUNTS}</a></li>
       <li><a href="${contextPath}/admin/productConsult/query" class="am-text-secondary"><span ></span><br/>当天新增咨询数<br/>${CONSULT_COUNTS}</a></li>
      <li><a href="${contextPath}/admin/accusation/query" class="am-text-warning"><span></span><br/>当天新增举报数<br/>${ACCUSATION_COUNTS}</a></li>
      <!-- 
      <li><a href="${contextPath}/admin/brand/query" class="am-text-danger"><span></span><br/>总品牌数<br/>${BRAND_COUNTS}</a></li>
       -->
    </ul>
</ls:auth>
    <div class="am-g">
    <ls:auth ifAnyGranted="M_ACCUSATION">
      <div class="am-u-md-6">
        <div class="am-panel am-panel-default" style="border-bottom:none;">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-2'}">未处理的商品举报<span class="am-icon-chevron-down am-fr" ></span></div>
          <div id="collapse-panel-2" class="am-in"></div>
        </div>
      </div>
	</ls:auth>
	<ls:auth ifAnyGranted="M_PRODUCTCONSULT">
      <div class="am-u-md-6">
        <div class="am-panel am-panel-default">
          <div class="am-panel-hd am-cf" data-am-collapse="{target: '#collapse-panel-3'}">最近未回复咨询<span class="am-icon-chevron-down am-fr" ></span></div>
          <div class="am-panel-bd am-collapse am-in am-cf" id="collapse-panel-3" style="padding-top: 0px;"></div>
        </div>
      </div>
      </ls:auth>
    </div>
    <footer>
      <hr>
      <p class="am-padding-left">©
          <c:choose>
              <c:when test="${not empty config.shopName}">${config.shopName}</c:when>
              <c:otherwise>LegendShop By 广州朗尊软件科技有限公司</c:otherwise>
          </c:choose>
      </p>
    </footer>
<script type="text/javascript">
	$(document).ready(function(e) {
	     <ls:auth ifAnyGranted="M_ACCUSATION">
					$.post("${contextPath}/admin/accusation/queryUnhandler",function(data) {
							$("#collapse-panel-2").html(data);
			});
		</ls:auth>
		<ls:auth ifAnyGranted="M_PRODUCTCONSULT">
 					$.post("${contextPath}/admin/productConsult/queryUnReply",function(data) {
 							$("#collapse-panel-3").html(data);
				});
		 </ls:auth>
	}); 
</script>
