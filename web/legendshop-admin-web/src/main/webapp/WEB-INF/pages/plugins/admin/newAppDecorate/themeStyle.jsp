<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>移动装修- 基础设置</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/appSetting.css"/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<div class="seller_right" style="margin-bottom:70px;">
				<table class="${tableclass} title-border" style="width: 100%">
						<tr><th class="title-border">移动装修 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基础设置<span></th></tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li class="this am-active" >
								<i></i><a href="<ls:url address="/admin/appSetting/themeStyle"/>">主题风格</a>
							</li>
							<li>
								<i></i><a href="<ls:url address="/admin/appSetting/categorySetting"/>">分类展示</a>
							</li>
						</ul>
					</div>
					<!-- 主题选择 -->
					<div align="center" class="order-content-list">
						
						<div class="store-style">
                            <div class="style-item">
                                <div class="item-row">
                                    <span class="style-tit">选择配色方案：</span>
                                    <span class="color-item" theme="green" color="#4eae31" style="margin-left: 14px;" data-img="/resources/templets/amaze/images/appdecorate/style01.png"><span class="col" style="background: #4eae31;"></span><span class="col" style="background: #584e61;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="crimson" color="#de0233" data-img="/resources/templets/amaze/images/appdecorate/style02.png"><span class="col" style="background: #de0233;"></span><span class="col" style="background: #ff9b00;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="red" color="#e5004f" data-img="/resources/templets/amaze/images/appdecorate/style03.png"><span class="col" style="background: #e5004f;"></span><span class="col" style="background: #f9cfde;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="pale-red" color="#fd8cb0" data-img="/resources/templets/amaze/images/appdecorate/style04.png"><span class="col" style="background: #fd8cb0;"></span><span class="col" style="background: #ffe1eb;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="orange" color="#ff6700" data-img="/resources/templets/amaze/images/appdecorate/style05.png"><span class="col" style="background: #ff6700;"></span><span class="col" style="background: #ffd9bf;"></span><span class="col"></span></span>
                                </div>
                                <div class="item-row" style="padding-left: 113px;">
                                    <span class="color-item" theme="brown" color="#bc9f5f" data-img="/resources/templets/amaze/images/appdecorate/style06.png"><span class="col" style="background: #bc9f5f;"></span><span class="col" style="background: #f2ede1;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="blue" color="#2e82e3" data-img="/resources/templets/amaze/images/appdecorate/style07.png"><span class="col" style="background: #2e82e3;"></span><span class="col" style="background: #dae8f8;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="light-green" color="#5fc221" data-img="/resources/templets/amaze/images/appdecorate/style08.png"><span class="col" style="background: #5fc221;"></span><span class="col" style="background: #e1f3e3;"></span><span class="col"></span></span>
                                    <span class="color-item" theme="black" color="#333333" data-img="/resources/templets/amaze/images/appdecorate/style09.png"><span class="col" style="background: #333333;"></span><span class="col" style="background: #ffffff;border: 1px solid #e5e5e5;border-left-color: #333;border-right: 0;"></span><span class="col"></span></span>
                                </div>
                            </div>
                            <div class="style-item">
                                <span class="style-tit">选择配色方案：</span>
                                <div class="style-img"><img src="${contextPath}/resources/templets/amaze/images/appdecorate/style01.png" alt=""></div>
                            </div>
                        </div>
						<div class="save-set"><a href="javascript:save()" class="blue_but">保存</a></div>
					</div>
					
				</div>	
			</div>
		</div>
	</div>
<script>
	var contextPath = '${contextPath}';
	var theme = '${appSetting.theme}';
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/themeStyle.js'/>"></script>
</body>
</html>
