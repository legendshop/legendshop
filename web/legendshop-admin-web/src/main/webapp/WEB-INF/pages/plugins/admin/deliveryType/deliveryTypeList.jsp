<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>配送方式编辑 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">配送管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">配送方式管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/deliveryType/query"
				id="form1" method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						用户名：<input type="text" name="userName" maxlength="50" value="${deliveryType.userName}" class="${inputclass}" placeholder="用户名"/>
					</span>
					<span class="item">
						配送方式：<input type="text" name="name" maxlength="50" value="${deliveryType.name}" class="${inputclass}" placeholder="配送方式"/> 
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
						<input type="button" value="创建配送方式" class="${btnclass}" onclick='window.location="<ls:url address='/admin/deliveryType/load'/>"' />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/deliveryType/query"
					id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth">${item_rowNum}</display:column>
					<display:column title="用户名" property="userName"></display:column>
					<display:column title="配送方式" property="name"></display:column>
					<display:column title="是否系统模版">
						<c:choose>
							<c:when test="${item.isSystem==1}">系统模版</c:when>
							<c:when test="${item.isSystem==0}">普通模版</c:when>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html" style="width: 150px;">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='<ls:url address='/admin/deliveryType/load/${item.dvyTypeId}'/>'">
								编辑
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.dvyTypeId}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		
		layer.confirm("确定删除 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/admin/deliveryType/delete/" + id + "'/>";
		   });
	}
	highlightTableRows("item");

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>


