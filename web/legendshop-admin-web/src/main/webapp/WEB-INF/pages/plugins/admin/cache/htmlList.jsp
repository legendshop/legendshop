<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
    <%
        Integer offset = 1;
    %>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	 <table class="${tableclass}" style="width: 100%">
	    <thead>
	    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  清空静态页面</th></tr>
	    </thead>
	    	     <tbody><tr><td>
 <div align="left" style="padding: 3px">
 	 <input type="button" value="一键清除缓存"  onclick="javascript:clearHtmlCache('${requestPath}')" class="${btnclass}"/>
<c:if test="${!rootPath}"><a href="javascript:enterUpFolder()"><b>上一层</b></a></c:if> &nbsp;  &nbsp;当前路径： ${requestPath} 
 </td></tr></tbody>
	    </table>
    
    <div align="center">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
        <form:form  id="queryhtml" action="${contextPath}/admin/cache/queryhtml">
        <input type="hidden"  name="requestFolder"  id="requestFolder" value="${requestFolder }" />
         <input type="hidden"  name="fileName"  id="fileName"/>
         <input type="hidden"  name="direct"  id="direct"/>
    <display:table name="fileList" requestURI="/admin/cache/queryhtml" id="item"
         export="false" class="${tableclass}" style="width:100%">
       <display:column title="顺序"   class="orderwidth">&nbsp;<%=offset++%>&nbsp;&nbsp;&nbsp;</display:column>
      <display:column title="文件名称" property="fileName"></display:column>
      <display:column title="是否是文件" property="isFile"></display:column>
      <display:column title="操作" >
      		<c:choose>
      			<c:when test="${item.isFile }">
      				
      				<a href="javascript:clearHtmlCache('${requestPath}/${item.fileName }')">删除</a>
      			</c:when>
      			<c:otherwise>
      				<a href="javascript:enterFolder('${item.fileName }')">进入</a>
      			</c:otherwise>
      		</c:choose>
      </display:column>
    </display:table>
    </form:form>
    </div>
   </div>
  </div>
 </body> 
    <script type="text/javascript">
      function clearHtmlCache(path) {
	    jQuery.post("${contextPath}/admin/file/deleteHtmlFile", {"filePath" : path}, function(retData){
	    	 if(retData == 0 ){
	    	 alert("删除Html成功！") ;
	          window.location.reload() ;
	       }else{
	          alert("删除Html失败！" + path) ;
	       }
	    });
	}
	
	 /**
	 * 进入下一层目录
	 */
	 function enterFolder(fileName) {
	 		var folderPath = document.getElementById("requestFolder").value;
	 		document.getElementById("requestFolder").value = folderPath + "/" + fileName;
	 		
	        document.getElementById("queryhtml").submit();
	 }
	 
	 /**
	 * 进入上一层目录
	 */
	 	 function enterUpFolder() {
	 		document.getElementById("direct").value = "up";
	        document.getElementById("queryhtml").submit();
	 }
    </script>
</html>

