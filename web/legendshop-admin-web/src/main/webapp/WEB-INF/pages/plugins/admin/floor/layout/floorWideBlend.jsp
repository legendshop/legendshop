<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<html>
<head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp" %>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/indexlayout.css'/>" rel="stylesheet"/>
    <title>楼层编辑界面</title>
    <style type="text/css">
        .flooredit {
            text-align: center;
            background-color: rgba(0, 0, 0, 0.5);
            position: absolute;
            display: none;
        }
    </style>
</head>
<body>
<form:form action="${contextPath}/admin/system/floor/query" id="form1" method="post">
<table class="${tableclass}" style="width: 98%;margin: 10px">
    <thead>
    <tr>
        <th height="40">
            <a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo;
            <a href="<ls:url address='/admin/floor/query'/>">混合模式楼层管理</a>
        </th>
    </tr>
    </thead>
</table>
</form:form>
<div id="doc">
    <div id="bd">
        <!--产品块-->
        <div class="yt-wrap bf_floor_new clearfix">

            <div class="pro-left">
                <div class="pro-left-tit">
                    <h4>${floor.name}</h4>
                </div>
                <div class="pro-left-con">

                    <div class="left-ads-con">
                        <div id="floorImage" class="flooredit" style="width: 210px; height: 25px;line-height: 25px;z-index:1000;"><a href="javascript:onclickLeftAdv(${floor.flId})" style="color:white;">广告编辑(210*280)</a></div>
                        <div class="pro-left-adv">
                            <c:if test="${not empty leftAdv}">
                                <a href="javascript:void(0);"><img src="<ls:photo item='${leftAdv.picUrl}'/>" width="210px" height="280px"></a>
                            </c:if>
                        </div>
                    </div>


                    <div class="recommend-classes" style="width: 212px;height: 214px;height:auto;min-height:213px;" id="sorts">
                        <div id="sortsEdit" style="width: 210px; height: 25px;line-height: 25px; top: 412px; display: none;" class="flooredit"><a href="javascript:onclickTitle()" style="color:white;">分类编辑</a></div>
                        <div class="pro-left-rec" style="width: 209px;">
                            <ul>
                                <c:forEach items="${requestScope.floorSortItemList}" var="floorItem">
                                    <li><a target="_blank" title="${floorItem.categoryName}" href="javascript:void(0);" style="text-align:center;">${floorItem.categoryName}</a></li>
                                    <c:forEach items="${floorItem.floorSubItemSet}" var="floorSubItem">
                                        <li><a target="_blank" title="${floorSubItem.nsortName}" href="javascript:void(0);" style="text-align:center;">${floorSubItem.nsortName}</a></li>
                                    </c:forEach>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>


            <div class="pro-right">
                <div class="pro-right-tit">
                    <ul>
                        <c:forEach items="${requestScope.subFloors}" var="subFloor" varStatus="index" end="4">
                            <c:choose>
                                <c:when test="${index.index eq 0}">
                                    <li class="arrow" value="${subFloor.sfId}">
                                        <i></i>
                                        <h3>${subFloor.name}</h3>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li value="${subFloor.sfId}"><i></i>
                                        <h3>${subFloor.name}</h3></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ul>
                </div>

                <div class="tabs-panel middle-goods-list" style="position:relative;height: 495px;border-top: 1px solid #e4e4e4;" id="prods">
                </div>

            </div>

        </div>
        <!--产品块 广告+分类+商品列表-->
    </div>
    <div style="text-align: center;clear: both; padding-top : 30px;">
        <input type="button" onclick="window.location='${contextPath}/admin/floor/query' " class="criteria-btn" value="返回">
    </div>
</div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorWideBlend.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var _flId = "${floor.flId}";
</script>
</html>
