<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
  <head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/red/css/legend.css'/>" rel="stylesheet"/>
	
	<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
    <title>楼层编辑界面</title>
     
     <style type="text/css">
     	.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}
     </style>
  </head>
  <body>
  <form:form  action="${contextPath}/admin/system/floor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 98%;margin: 10px" >
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo; 
				    	<a href="<ls:url address='/admin/floor/query'/>">楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
	    </table>
    </form:form>
    
<div class="home-standard-layout wrapper style-default w" style="height: 510px;">
  <div class="left-sidebar" style="height: 500px;margin-left: 2px;">
    <div class="title">
      	 <div class="txt-type">
                <span>F</span>
                <h2>${floor.name}</h2>
         </div>
    </div>
    <div class="left-ads" style="height: 240px;position:relative;">
    	<div id="floorImage" class="flooredit" style="width: 212px; height: 25px;z-index:1000;"><a  href="javascript:onclickLeftAdv(${floor.flId})"  style="color:white;">广告编辑(212*280)</a></div>
      	<a href="javascript:void(0);">
      		<img alt="楼层左上方广告" src="<ls:photo item='${leftAdv.picUrl}'/>" style="width: 212px;height: 240px;">
      	</a>
    </div>
    <div class="recommend-classes" style="width: 212px;" id="sorts">
    	<div id="sortsEdit" style="width: 212px; height: 25px; top: 324px;" class="flooredit"><a href="javascript:onclickTitle()" style="color:white;">分类编辑</a></div>
      	<ul style="width: 212px;">
      		 <c:forEach items="${requestScope.floorSortItemList}" var="floorItem">
      		 		 <li><a target="_blank" title="${floorItem.sortName}" href="#" style="text-align:center;">${floorItem.sortName}</a></li> 
      		 		<c:forEach items="${floorItem.floorSubItemSet}" var="floorSubItem"> 
      		 			<li><a target="_blank" title="${floorSubItem.nsortName}" href="#" style="text-align:center;">${floorSubItem.nsortName}</a></li>
      		 		</c:forEach>
      		 </c:forEach>
		</ul>
    </div>
  </div>
  
  <c:choose>
  	<c:when test="${floor.leftSwitch eq 0}"><c:set var="subFloorLi" value="4"></c:set></c:when>
  	<c:otherwise><c:set var="subFloorLi" value="3"></c:set></c:otherwise>
  </c:choose>
  
  <c:choose>
  	<c:when test="${floor.leftSwitch eq 0}"><div class="middle-layout" style="width: 1000px;"></c:when>
  	<c:otherwise><div class="middle-layout" style="width: 801px;"></c:otherwise>
  </c:choose>
<%--  <div class="middle-layout" style="width: 777px;">--%>

  <c:choose>
  	<c:when test="${floor.leftSwitch eq 0}"><ul class="tabs-nav" style="width: 1000px;"></c:when>
  	<c:otherwise><ul class="tabs-nav" style="width: 801px;"></c:otherwise>
  </c:choose>
<%--	    <ul class="tabs-nav" style="width: 777px;">--%>

	    	<c:forEach items="${floor.subFloors}" var="subFloor"  varStatus="index" end="${subFloorLi}">
	    		<c:choose>
	    			<c:when test="${index.index eq 0}">
	    				<c:choose>
	    					<c:when test="${floor.leftSwitch eq 0}"><li style="width: 20%" class="l1 tabs-selected" value="${subFloor.sfId}"></c:when>
	    					<c:otherwise><li class="l1 tabs-selected" value="${subFloor.sfId}"></c:otherwise>
	    				</c:choose>
	    			</c:when>
	    			<c:otherwise>
	    				<c:choose>
	    					<c:when test="${floor.leftSwitch eq 0}"><li style="width: 20%" class="l1" value="${subFloor.sfId}"></c:when>
	    					<c:otherwise><li class="l1" value="${subFloor.sfId}"></c:otherwise>
	    				</c:choose>
	    			</c:otherwise>
	    		</c:choose>
	         		<i class="arrow" style="top: 30px;"></i>
	         		<h3>${subFloor.name}</h3>
	         	</li>
	        </c:forEach>
	    </ul>
	    
	    <c:choose>
	    	<c:when test="${floor.leftSwitch eq 0}">
	    		<div class="tabs-panel middle-goods-list" style="width: 1000px;height: 460px;position:relative;" id="prods"></div>
	    	</c:when>
	    	<c:otherwise>
	    		<div class="tabs-panel middle-goods-list" style="width: 801px;height: 460px;position:relative;" id="prods"></div>
	    	</c:otherwise>
	    </c:choose>
  </div>
  
   <c:choose>
  	<c:when test="${floor.leftSwitch eq 0}"><div class="right-sidebar tabs-hide" style="height: 502px;float:left"></c:when>
  	<c:otherwise><div class="right-sidebar" style="height: 502px;float:left"></c:otherwise>
  </c:choose>
<%--  <div class="right-sidebar" style="height: 502px;float:left">--%>
    <div class="title"></div>
    <div class="recommend-brand" id="brands" style="position:relative;"></div>
    <div class="right-side-focus" style="height: 204px;position:relative;" id="adv">
      <div id="advEdit" class="flooredit" style="width: 212px; height: 25px;z-index:1000;"><a href="javascript:void(0);" onclick="onclickAdv('${floor.flId}',this);"  style="color:white;">广告编辑(212*241)</a></div>
      <ul>
      	<c:forEach items="${rightAdvList}" var="adv" varStatus="status">
      		<li style="height: 205px;" floorItemId="${adv.fiId}">
	         	 <a><img alt="楼层右下方广告" src="<ls:photo item='${adv.picUrl}'/>" style="height: 205px;"></a>
         	</li>
      	</c:forEach>
     </ul>

   </div>
  </div>
  <div class="clear"></div>
</div>
  </body>
  
  <div style="margin: 0px 0px 10px 75px; width: 1216px;position:relative;margin:auto;max-height:350px;" id="bottom">
  	 <div id="bottomAdvEdit" class="flooredit" style="width: 212px; z-index:1000;max-height:350px;"><a href="javascript:void(0);" onclick="onclickBottomAdv(${floor.flId});"  style="color:white;">广告编辑(宽度1216,高度不限)</a></div>
  		<img alt="楼层最下方广告" src="<ls:photo item='${bottomAdv.picUrl}'/>" style=" width: 1216px;max-height:350px;">
  </div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var _flId = "${floor.flId}";
	var _contentType = "${floor.contentType}";
	var _contentType2 = "${floor.contentType2}";
</script>
</html>
