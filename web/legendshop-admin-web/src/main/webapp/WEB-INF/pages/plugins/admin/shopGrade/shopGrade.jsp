<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/system/shopGrade/save"
				method="post" id="form1">
				<input id="gradeId" name="gradeId" value="${shopGrade.gradeId}"
					type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass}" id="col1"
						style="width: 100%">
						<thead>
							<tr class="sortable">
								<th colspan="2">
									<strong class="am-text-primary am-text-lg">商家管理</strong> / <a href="<ls:url address="/admin/system/shopGrade/query"/>" style="color: #0e90d2">商家管理</a>
                				/ 编辑商家等级
									</th>
							</tr>
						</thead>

						<tr>
							<td width="200px">
								<div align="center">
									名称: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="name" id="name"
								value="${shopGrade.name}" style="width: 200px"/></td>
						</tr>
						<tr>
							<td>
								<div align="center">允许的最大展柜:</div>
							</td>
							<td><input type="text" name="maxSortSize" id="maxSortSize"
								value="${shopGrade.maxSortSize}" style="width: 100px" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">允许的最大展柜分类:</div>
							</td>
							<td><input type="text" name="maxNsortSize" id="maxNsortSize"
								value="${shopGrade.maxNsortSize}" style="width: 100px" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">允许的最大产品数:</div>
							</td>
							<td><input type="text" name="maxProd" id="maxProd"
								value="${shopGrade.maxProd}" style="width: 100px" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input class="${btnclass}" type="submit" value="保存" /> 
								    <input class="${btnclass}" type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/system/shopGrade/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				name : {
					required:true,
					maxlength:30
				},
				maxSortSize : {
					number : true,
					maxlength:10
				},
				maxNsortSize : {
					number : true,
					maxlength:10
				},
				maxProd : {
					number : true,
					maxlength:10
				}
			},
			messages : {
				name : {
					required:"请输入名字"
				},
				maxSortSize : {
					number : "请输入数字"
				},
				maxNsortSize : {
					number : "请输入数字"
				},
				maxProd : {
					number : "请输入数字"
				}
			}
		});

		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>