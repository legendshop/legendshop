<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>图片编辑 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  <style>
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<form:form action="${contextPath}/admin/indexjpg/save" method="post" id="indexJpgForm" enctype="multipart/form-data">
    <input id="id" name="id" value="${index.id}" type="hidden">
    <table class="${tableclass}" style="width: 100%">
        <thead>
        <tr>
            <th class="no-bg title-th">
            	<span class="title-span">
					PC首页装修  ＞
					<a href="<ls:url address="/admin/indexjpg/query"/>">轮播图管理</a>
					<c:if test="${not empty index }">  ＞  <span style="color:#0e90d2;">图片编辑</span></c:if>
					<c:if test="${empty index }">  ＞  <span style="color:#0e90d2;">图片新增</span></c:if>
				</span>
            </th>
        </tr>
        </thead>
    </table>
    <table class="${tableclass} no-border content-table" id="col1" style="width:100%">

        <tr>
            <td width="200px" align="right"><font color="#ff0000">*</font>标题：</td>
            <td align="left" style="padding-left: 2px;">
                <input type="text" name="title" id="title" maxlength="200" size="30" value="${index.title}" class="${inputclass}"/>
            </td>
        </tr>
        <tr>
            <td align="right"><font color="#ff0000">*</font>图片链接地址：</td>
            <td align="left" style="padding-left: 2px;">
                <input type="text" name="link" id="link" size="30" value="${index.link}" class="${inputclass}" maxlength="200"/>
            </td>
        </tr>
        <tr>
            <td style="padding-top:0;" align="right">图片：</td>
            <td align="left" style="padding-top:0;padding-left: 2px;">

                <input style="margin-top:20px;" type="file" name="file" id="file" size="30"/>
               	<span class="tips-span">图片不能大于2M，主要展示宽度建意为1280*470</span><font color="#ff0000">*</font>
                <input type="hidden" name="imgName" id="imgName" size="30" value="${index.img}" class="${inputclass}"/>
            </td>
        </tr>
        <tr>
            <td align="right">次序：</td>
            <td align="left" style="padding-left: 2px;">
			     <input type="text" name="seq" id="seq" value="${index.seq}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
            </td>
        </tr>
        <tr>
            <td align="right">描述，说明文字：</td>
            <td align="left" style="padding-left: 2px;">
                <input type="text" name="des" id="des" size="30" value="${index.des}" class="${inputclass}"/>
            </td>
        </tr>
        <c:if test="${index.img!=null}">
            <tr>
                <td align="right">原有图片：</td>
                <td align="left" style="padding-left: 2px;">
                    <a href="<ls:photo item='${index.img}'/>" target="_blank">
                        <img src="<ls:photo item='${index.img}'/>" height="145" width="265"/></a>
                </td>
            </tr>
        </c:if>
        <tr>
        	<td></td>
            <td align="left" style="padding-left: 2px;">
                <div>
                    <input type="submit" value="保存" class="${btnclass}"/>
                    <input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/indexjpg/query'"/>
                </div>
            </td>
        </tr>
    </table>
</form:form>
</div>
</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>' ></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/indexjpg.js'/>"></script>
<script type="text/javascript">
    $.validator.setDefaults({});
</script>
</html>

