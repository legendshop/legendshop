<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/pagination.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
				<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
               			</th>
					</tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li><i></i><a
								href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
							<li><i></i><a
								href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
							<li class=" am-active"><i></i><a
								href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
							<li><i></i><a
								href="<ls:url address="/admin/userinfo/userVisitLog/${userName}?userId=${userId}"/>">会员浏览历史</a></li>
							<li><i></i><a
								href="<ls:url address="/admin/userinfo/userProdComm/${userId}?userName=${userName}"/>">会员商品评论</a></li>
							<ls:plugin pluginId="integral">
								<li><i></i><a
									href="<ls:url address="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}"/>">会员积分明细</a></li>
							</ls:plugin>
							<ls:plugin pluginId="predeposit">
								<li><i></i><a
									href="<ls:url address="/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}"/>">会员预存款明细</a></li>
							</ls:plugin>
							<li><i></i><a
								href="<ls:url address="/admin/userinfo/userProdCons/${userName}?userId=${userId}"/>">会员商品咨询</a></li>
						</ul>
					</div>

				<div class="order-content-list" style="margin-top: 20px;">
					<display:table name="list"
						requestURI="/admin/userinfo/expensesRecord/${userId}" id="item"
						export="false" class="${tableclass}" style="width:100%"
						sort="external">
						<display:column title="记录时间">
							<fmt:formatDate value="${item.recordDate}" type="both" />
						</display:column>
						<display:column title="记录金额" property="recordMoney"></display:column>
						<display:column title="备注" property="recordRemark"></display:column>
					</display:table>

					<c:if test="${empty list}">
						<div
							style="background: #f9f9f9; margin-top: 20px; padding: 20px; border: 1px solid #efefef; height: 60px; width: 99%;">该会员还没有消费记录~</div>
					</c:if>
					<div class="clearfix" style="margin-bottom: 60px;">
			       		<div class="fr">
			       			 <div class="page">
				       			 <div class="p-wrap">
				           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				    			 </div>
			       			 </div>
			       		</div>
			       	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function pager(curPageNO) {
		window.location = "${contextPath}/admin/userinfo/expensesRecord/${userId}?userName=${userName}&curPageNO="
				+ curPageNO;
	}
</script>
</html>

