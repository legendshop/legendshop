<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>杂志管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
 <style>
  .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">杂志管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">杂志管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/article/query" id="form1"
				method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						杂志名称： <input type="text" id="name" name="name" value="${article.name}" style="padding: 3px 5px;" placeholder="杂志名称"/> 
					</span>
					<span class="item">
						作者：<input type="text" id="author" name="author" value="${article.author}" style="padding: 3px 5px;" placeholder="作者"/>
					</span>
					<!-- 
			 发表时间<input type="text"
			id="createTime" name="createTime" value="${article.createTime}" /> -->
					<span class="item">
						是否前台显示：
					 <select id="status" name="status" class="criteria-select">
						<option value="" <c:if test="${empty article.status}">selected</c:if>>请选择</option>
						<option value="1" <c:if test="${article.status==1}">selected</c:if>>是</option>
						<option value="0" <c:if test="${article.status==0}">selected</c:if>>否</option>
					</select> 
						<input type="submit" onclick="search()" value="搜索" class="${btnclass}" /> 
						<input type="button" value="创建杂志" class="${btnclass}" onclick='window.location="${contextPath}/admin/article/load"' />
					</span>
				</div>
			</form:form>


			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/article/query"
					id="item" export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="杂志名称" style="width:300px"><div class="order_img_name">${item.name}</div></display:column>
					<display:column title="作者" style="width:200px"><div class="order_img_name">${item.author}</div></display:column>
					<%-- <display:column title="简介" style="width:280px">${item.intro}</display:column>
					<display:column title="概要" style="width:280px">${item.summary}</display:column> --%>
					<display:column title="类型" style="width: 50px">
						<c:choose>
							<c:when test="${item.type==1}">转发</c:when>
							<c:otherwise>原创</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="是否前台显示" style="width:110px" >
						<c:choose>
							<c:when test="${item.status==1}">是</c:when>
							<c:otherwise>否</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="点赞数量" style="width:100px">${item.thumbNum}</display:column>
					<display:column title="评论数量" style="width:100px">${item.commentsNum}</display:column>
					<display:column title="发表时间">
						<fmt:formatDate value="${item.issueTime}"
							pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<display:column title="创建时间">
						<fmt:formatDate value="${item.createTime}"
							pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<display:column title="操作" media="html" style="width:205px;">
						<div class="table-btn-group">
							<div class="am-btn-group am-btn-group-xs">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/article/load/${item.id}'">
									编辑
								</button>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="deleteById('${item.id}');">
									删除
								</button>
							</div>
						</div>

					</display:column>
				</display:table>
				<div align="center">
					<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
					<ls:page pageSize="${pageSize }" total="${total}"
						curPageNO="${curPageNO }" type="default" />
				</div>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm('确定删除 ?', {icon:3}, function(){
			window.location = "<ls:url address='/admin/article/delete/" + id + "'/>";
		});
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>