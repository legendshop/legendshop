<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%
   Integer offset = (Integer)request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
	<c:choose>
		<c:when test="${not empty requestScope.list}">
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/returnGoods/query" id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="退货编号" property="refundSn"></display:column>
					<display:column title="退款金额">
						<c:choose>
							<c:when test="${item.isRefundDeposit}">
								&#65509;${item.depositRefund + item.refundAmount}
							</c:when>
							<c:otherwise>
								&#65509;${item.refundAmount}
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="退货数量" property="goodsNum"></display:column>
					<display:column title="申请原因" property="buyerMessage"></display:column>
					<display:column title="申请时间" property="applyTime" format="{0,date,yyyy-MM-dd HH:mm}" style="min-width: 90px;"></display:column>
					<display:column title="商家处理备注" style="max-width: 300px; word-break:break-all">
						<c:if test="${empty item.sellerMessage }">暂无</c:if>
						<c:if test="${not empty item.sellerMessage }">
							<div class="order_img_name">${item.sellerMessage }</div>
						</c:if>
					</display:column>
					<display:column title="商家处理时间" style="min-width: 90px;">
						<c:if test="${empty item.sellerTime }">商家未处理</c:if>
						<c:if test="${not empty item.sellerTime }"><fmt:formatDate value="${item.sellerTime}" pattern="yyyy-MM-dd HH:mm" /></c:if>
					</display:column>
					<display:column title="商家收货时间" style="min-width: 90px;">
						<c:if test="${empty item.receiveTime }">暂无</c:if>
						<c:if test="${not empty item.receiveTime }"><fmt:formatDate value="${item.receiveTime}" pattern="yyyy-MM-dd HH:mm" /></c:if>
					</display:column>
					<display:column title="涉及商品">
						<c:if test="${item.subItemId eq 0 }">
							${item.productName }
						</c:if>
						<c:if test="${item.subItemId ne 0 }">
							<a class="over-text" href="${PC_DOMAIN_NAME}/views/${item.productId}" title="${item.productName }">${item.productName }</a>
						</c:if>
					</display:column>
					<display:column title="处理状态">
						<c:choose>
							<c:when test="${item.applyState  eq 2}">待处理</c:when>
							<c:when test="${item.applyState  eq 3}">处理完成</c:when>
							<c:otherwise>
								暂不需处理
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.applyState eq 2}">
									<button id="check" class="tab-btn"
										onclick="window.location.href='${contextPath}/admin/returnGoods/returnDetail/${item.refundId}'">
										处理
									</button>
								</c:when>
								<c:otherwise>
									<button id="check" class="tab-btn"
										onclick="window.location.href='${contextPath}/admin/returnGoods/returnDetail/${item.refundId}'">
										查看详情
									</button>
								</c:otherwise>
							</c:choose>
						</div>
					</display:column>
				</display:table>
				<!-- 分页条 -->
		     	<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<form:form  id="exportForm" action="${contextPath}/admin/returnGoods/export" method="get">
					<input type="hidden" name="shopId" value="${paramData.shopId }" />
					<input type="hidden" name="shopName" value="${paramData.shopId }" />
					<input type="hidden" name="status" value="${status}" />
					<input type="hidden" name="status" value="${paramData.applyState}" />
					<input type="hidden" name="numType" value="${paramData.numType }" />
					<input type="hidden" name="number" value="${paramData.number }" />
					<input type="hidden" name="startDate" value="<fmt:formatDate value="${paramData.startDate}" pattern="yyyy-MM-dd" />">
					<input type="hidden" name="endDate" value="<fmt:formatDate value="${paramData.endDate}" pattern="yyyy-MM-dd" />">
					<%-- <input type="hidden" name="refundStartDate" value="<fmt:formatDate value="${paramData.refundStartDate}" pattern="yyyy-MM-dd" />">
					<input type="hidden" name="refundEndDate"  value="<fmt:formatDate value="${paramData.refundEndDate}" pattern="yyyy-MM-dd" />" /> --%>
				</form:form>
			</div>
		</c:when>
		<c:otherwise>
			<div style="width: 300px;margin: 50px auto;text-align: center;">
				    对不起,没有找到符合条件的记录!
			</div>
		</c:otherwise>
	</c:choose>
