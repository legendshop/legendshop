<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>评论列表 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">咨询管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">评论管理</span></th>
				</tr>
			</table>		
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li <c:if test="${empty status}">class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/productcomment/query"/>">所有评论</a></li>
						<li <c:if test="${status == 0}" >class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/productcomment/query?status=0"/>">待审核</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/productcomment/query?status=${status }" id="form1" method="post">
				<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
				<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
						<span class="item"> 
							店铺：
							<input type="text" id="shopId" name="shopId" value="${bean.shopId}""/>
							<input type="hidden" class="${inputclass}" id="siteName" name="siteName" maxlength="50" value="${bean.siteName}" /> 
							<input type="hidden" id="status" name="status" value="${bean.status}"/>
						</span>
						<span class="item">
							商品：
							<input class="${inputclass}" type="text" name="prodName" id="prodName" maxlength="50" value="${bean.prodName}" style="padding: 3px 5px;" placeholder="商品"/>
						</span>
						<span class="item">
							用户名：
							<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${bean.userName}" style="padding: 3px 5px;" placeholder="用户名"/>
							<input class="${btnclass}" type="button" onclick="search()"value="搜索" />
						</span>
					</div>
			</form:form>
			<div align="center" id="prodCommentContentList" class="order-content-list" style="width: 100%;"></div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 用户在前台录入商品评论<br> 2.
						可以删除不合理的评论<br>
					</td>
				<tr>
			</table>

		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var need = "${commentNeedReview}";

	highlightTableRows("item");

	function deleteById(id) {
		layer.confirm("确定要删除吗？", {icon:3}, function() {
			window.location = "${contextPath}/admin/productcomment/delete/" + id;
		});
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		sendData(curPageNO);
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	
	$(document).ready(function () {
		var shopName = $("#siteName").val();
		if(shopName == ''){
	        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});

	function sendData(curPageNO) {
	    var data = {
	        "shopId": $("#shopId").val(),
	        "status": $("#status").val(),
	        "curPageNO": curPageNO,
	        "prodName": $("#prodName").val(),
	        "userName": $("#userName").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/productcomment/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#prodCommentContentList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
</script>
</html>
