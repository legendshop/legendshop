<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<style>
 .dialog_content dl { font-size: 0; *word-spacing:-1px/*IE6、7*/; line-height: 20px; display: block; clear: both; overflow:hidden;}
 .dialog_content dl dt { font-size: 12px; line-height: 32px; vertical-align: top; letter-spacing: normal; word-spacing: normal; text-align: right; display: inline-block; *display: inline/*IE6,7*/; width: 29%; padding: 10px 1% 10px 0; margin: 0; zoom: 1;}
 .dialog_content dl dt i.required { font: 12px/16px Tahoma; color: #F30; vertical-align: middle; margin-right: 4px; }
</style>

<c:choose>
	<c:when test="${empty list}">
	    暂无符合条件的数据记录
	</c:when>
	<c:when test="${not empty list}">
	    <ul class="goods-list">
	       <c:forEach items="${list}" var="prods" varStatus="status">
	          <li>
				 <div class="goods-thumb">
					<a target="_blank" href="<ls:url address='/views/${prods.prodId}'/>">
					   <img src="<ls:images item='${prods.pic}' scale='3'/>">
					</a>
				 </div>
				 <dl class="goods-info">
					<dt>
						<a target="_blank" href="<ls:url address='/views/${prods.prodId}'/>">
						  ${prods.prodName}
						</a>
					</dt>
					<dd>价格：¥<fmt:formatNumber value="${prods.cash}" pattern="#0.00#"/></dd>
				 </dl> 
				   <a style="margin-left: 25px; border: 1px solid black;" class="ncbtn-mini" href="javascript:void(0);" onclick="selectProd('${prods.prodId}','${prods.skuId}','${prods.cash}');" id="btn_add_xianshi_goods">选择商品</a>
				</li>
		   </c:forEach>
		</ul>
		
		
       <div class="fanye">
					<c:if test="${not empty toolBar}">
					<div>
							<p align="right">
								<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
							</p>
					</div>
		          </c:if> 
		</div>   
		
		<div class="clear"></div>
		
	</c:when>
</c:choose>

<script type="text/javascript">
	   var marketId=${marketId};
	   var contextPath = '${contextPath}';
	   var type="${type}";
	   function prodPager(_curPage){
	        var url =contextPath +"/admin/marketing/marketingProds/"+marketId+"?curPage="+_curPage;
	        if(type!=null){
	        	url+="&type="+type;
	        }
            url += '&' + $.param({name: $('#search_goods_name').val()});
            $('#div_goods_search_result').load(url);
	   }
	   
	  function selectProd(_prodId,_skuId,_cash){
	      if(type=='2'){
	      
	          discountPrice(_prodId,_skuId,_cash);
     
	      }else{
	      
		       $.ajax({
		        //提交数据的类型 POST GET
		        type:"POST",
		        //提交的网址
		        url:contextPath+"/admin/marketing/addMarketingRuleProds/"+marketId,
		        data:{"prodId":_prodId,"skuId":_skuId,"cash":_cash},
		        //提交的数据
		        async : false,  
		        //返回数据的格式
		        datatype: "json",
		        //成功返回之后调用的函数            
		        success:function(data){
		        	 var result=eval(data);
		        	 if(result=="OK"){
		        		layer.msg("添加成功",{icon:1,time:800},function(){
		        			$("#prodContent").load(contextPath+"/admin/marketing/marketingRuleProds/"+marketId+"/"+shopId+"?curPage=1");
		        			 return ;
		        		});
		        	 }else{
		        		 layer.msg(result,{icon:2});
		        		 return false;
		        	 }
		        }       
	         });
	      }
	  }
	  
				function discountPrice(_prodId,_skuId,_cash){
				    var html="<div style='padding:10px'>商品价格:<span id='dialog_edit_goods_price'>"+_cash+"</span> <br /> 促销价：<input type='text' id='discountPrice'  value='' /></div>";
				    layer.open({
						  title :"促销价格",
						  type: 1, 
						  content: html, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
						  area: ['400px', '300px'],
						  btn: ['确定','关闭'],
						  yes:function(){
							  var discountPrice=$.trim($("#discountPrice").val());
							    if(discountPrice=="" || discountPrice==undefined){
							       layer.msg("请输入正确的促销价格",{icon:0});
							       return false;
							    }
							    var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
							    if(!exp.test(discountPrice)){
							       layer.msg("请输入正确的金额格式",{icon:0});
							       return false;
							    }
							    var xianshi_price = Number(discountPrice);
                              var goods_price = Number(_cash);
							    if(xianshi_price>goods_price){
							       layer.msg("促销价格不能大于销售价格",{icon:0});
							       return false;
							    }
					    	    $.ajax({
							        //提交数据的类型 POST GET
							        type:"POST",
							        //提交的网址
							        url:contextPath+"/admin/marketing/addZkMarketingRuleProds/"+marketId,
							        data:{"prodId":_prodId,"skuId":_skuId,"cash":goods_price,"discountPrice":xianshi_price},
							        //提交的数据
							        async : false,  
							        //返回数据的格式
							        datatype: "json",
							        //成功返回之后调用的函数            
							        success:function(data){
							        	 var result=eval(data);
							        	 if(result=="OK"){
							        		 layer.msg("添加成功",{icon:1,time:800},function(){
							        		 	$("#prodContent").load(contextPath+"/admin/marketing/marketingZkRuleProds/"+marketId+"/"+shopId+"?curPage=1");
		        		                     	return ;
							        		 });
							        	 }else{
							        		 layer.msg(result,{icon:0});
							        		 return false;
							        	 }
							        }       
						     });
						  }
				    )};
		     };
					 
</script>