<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               			&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
               		</th>
				</tr>
			</table>
			<div class="user_list">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userVisitLog/${userName}?userId=${userId}"/>">会员浏览历史</a></li>
						<li class=" am-active"><i></i><a href="">会员商品评论</a></li>
						<ls:plugin pluginId="integral">
							<li><i></i><a
								href="<ls:url address="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}"/>">会员积分明细</a></li>
						</ls:plugin>
						<ls:plugin pluginId="predeposit">
							<li><i></i><a
								href="<ls:url address="/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}"/>">会员预存款明细</a></li>
						</ls:plugin>
						<li><i></i><a
							href="<ls:url address="/admin/userinfo/userProdCons/${userName}?userId=${userId}"/>">会员商品咨询</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/userinfo/userProdComm/${userId}" id="form1" method="post">
				<input class="${inputclass}" type="hidden" name="userName" id="userName" maxlength="50" value="${userName}" />
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						商品&nbsp; <input class="${inputclass}" type="text" name="prodName" id="prodName" maxlength="50" value="${bean.prodName}" /> 
					</span>
					<span class="item">
						店铺名&nbsp;<input type="text" class="${inputclass}" name="siteName" maxlength="50" value="${bean.siteName}" /> 
					</span>
					<span class="item">
						<input class="${btnclass}" type="submit" onclick="search()" value="搜索" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/productcomment/query"
					id="item" export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="顺序" class="orderwidth"
						style="min-width:50px;"><%=offset++%></display:column>
					<display:column title="店铺名" property="siteName"
						style="min-width:100px;"></display:column>
					<display:column title="用户名" property="userName" style="width:5%"></display:column>
					<display:column title="商品" sortName="name" style="width:15%">
						<a target="_blank" href="${PC_DOMAIN_NAME}/views/${item.prodId}">${item.prodName}</a>
					</display:column>
					<display:column title="评论内容" style="width:200px;"
						property="content"></display:column>
					<display:column title="评分" property="score" style="min-width:60px"
						sortable="true" sortName="score"></display:column>
					<display:column title="评论时间" style="min-width:140px"
						sortable="true" sortName="addtime">
						<fmt:formatDate value="${item.addtime}" pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<c:choose>
						<c:when test="${commentNeedReview == true }">
							<display:column title="状态" sortable="ture" sortName="status"
								class="review" style="min-width:60px">
								<c:choose>
									<c:when test="${item.status == 1}">
										<span>通过</span>
									</c:when>
									<c:when test="${item.status == 0}">
										<span>待审核</span>
									</c:when>
									<c:when test="${item.status == -1 }">
										<span>不通过</span>
									</c:when>
								</c:choose>
							</display:column>
						</c:when>
					</c:choose>
					<display:column title="追加评论" style="width:200px;">
						<c:choose>
							<c:when test="${not empty item.addContent}">${item.addContent}</c:when>
							<c:otherwise>------</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="追加时间">
						<c:choose>
							<c:when test="${not empty item.addAddTime}">
								<fmt:formatDate value="${item.addAddTime}"
									pattern="yyyy-MM-dd HH:mm" />
							</c:when>
							<c:otherwise>------</c:otherwise>
						</c:choose>
					</display:column>
					<c:choose>
						<c:when test="${commentNeedReview == true }">
							<display:column title="状态" class="review" style="min-width:60px">
								<c:choose>
									<c:when test="${item.addStatus == 1}">
										<span>通过</span>
									</c:when>
									<c:when test="${item.addStatus == 0}">
										<span>待审核</span>
									</c:when>
									<c:when test="${item.addStatus == -1 }">
										<span>不通过</span>
									</c:when>
									<c:otherwise>------</c:otherwise>
								</c:choose>
							</display:column>
						</c:when>
					</c:choose>
					<display:column title="操作" media="html" style="min-width:140px">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.status == 0}">
									<button onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'" class="tab-btn">
										审核
									</button>
									<span class="btn-line">|</span>
								</c:when>
								<c:when test="${item.status eq 1 and item.isAddComm and item.addStatus eq 0}">
									<button onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'" class="tab-btn">
										审核
									</button>
									<span class="btn-line">|</span>
								</c:when>
								<c:otherwise>
									<button class="tab-btn" onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'">
										查看
									</button>
									<span class="btn-line">|</span>
								</c:otherwise>
							</c:choose>
							<button class="tab-btn" onclick="deleteById('${item.id}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm("确定要删除吗？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   jQuery.ajax({
					url : "${contextPath}/admin/productcomment/delete/" + id,
					type : 'post',
					async : true, // 默认为true 异步
					error : function(jqXHR, textStatus, errorThrown) {
						// alert(textStatus, errorThrown);
					},
					success : function(result) {
						window.location.reload();
					}
				});
		   });

	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	highlightTableRows("item");
</script>
</html>
