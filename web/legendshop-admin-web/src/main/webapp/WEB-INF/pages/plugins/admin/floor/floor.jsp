<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>首页管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<style type="text/css">
.android_type ul {
	clear: both;
	width: 100%;
	overflow: hidden;
	margin: 0px;
	padding: 0px;
}

.android_type ul li {
	float: left;
	margin: 0px 5px 10px 0px;
	border: 1px solid #DDD;
	position: relative;
	padding: 2px;
	list-style: none;
}

.android_type ul li a {
	padding: 2px;
	display: block;
}

.setcont .set1 .android_type .this {
	border: 1px solid #1EC7ED;
}

.setcont .set1 .android_type ul li:hover {
	border: 1px solid #1EC7ED;
}

.android_type .this i {
	width: 16px;
	height: 16px;
	position: absolute;
	bottom: 0px;
	right: 0px;
	background: transparent url(${contextPath}/resources/templets/amaze/images/selected.png) no-repeat scroll 50% 50%;
}

.android_type ul li span {
	width: 100%;
	display: inline-block;
	text-align: center;
}

.editul ul li {
	line-height: 30px;
}

img, fieldset, abbr, acronym {
	border: 0 none;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
							<span class="title-span">
								首页管理   ＞  
								<a href="<ls:url address="/admin/floor/query"/>">首页楼层装修</a>
								<c:if test="${not empty floor }"><span style="color:#0e90d2;">  ＞  楼层编辑</span></c:if>
								<c:if test="${empty floor }"><span style="color:#0e90d2;">  ＞  楼层新增</span></c:if>
							</span>
						</th>
					</tr>
				</thead>
			</table>

			<form:form action="${contextPath}/admin/floor/save" method="post"
				id="form1">
				<input id="flId" name="flId" value="${floor.flId}" type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%">
						<tr>
							<td width="200px;">
								<div align="right">
									<font color="ff0000">*</font>楼层标题：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="name" id="name" maxlength="49"
								value="${floor.name}" class="${inputclass}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>排序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input style="width:50px;" type="text" name="seq" id="seq" value="${floor.seq}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
							</td>
						</tr>

						<tr>
							<td align="right" style="vertical-align: top;">
								版式：
							</td>
							<input name="layout" type="hidden" id="layout"
								value="wide_adv_rectangle_four" />
							<td align="left" style="padding-left: 2px;">
								<div class="android_type">
									<ul class=" pd2" id="choose_template">

										<li class="this" mark="wide_adv_rectangle_four"><a
											href="javascript:void(0)"> <img
												src="<ls:templateResource item='/resources/templets/amaze/images/01.png'/>">
										</a> <i></i><span>(4X矩形广告)</span></li>
										<li mark="wide_adv_brand"><a href="javascript:void(0)">
												<img
												src="<ls:templateResource item='/resources/templets/amaze/images/02.png'/>">
										</a><i></i> <span>(广告+品牌)</span></li>
										<li mark="wide_goods"><a href="javascript:void(0)"> <img
												src="<ls:templateResource item='/resources/templets/amaze/images/03.png'/>">
										</a> <i></i><span>(5x商品)</span></li>
										<li mark="wide_adv_five"><a href="javascript:void(0)"><img
												src="<ls:templateResource item='/resources/templets/amaze/images/04.png'/>">
										</a><i></i><span>(5x广告)</span></li>
										<li mark="wide_adv_square_four"><a
											href="javascript:void(0)"><img
												src="<ls:templateResource item='/resources/templets/amaze/images/05.png'/>">
										</a><i></i><span>(4x方形广告)</span></li>
										<li mark="wide_adv_eight"><a href="javascript:void(0)"><img
												src="<ls:templateResource item='/resources/templets/amaze/images/06.png'/>">
										</a><i></i><span>(8x广告)</span></li>
										<li mark="wide_adv_row"><a href="javascript:void(0)"><img
												src="<ls:templateResource item='/resources/templets/amaze/images/07.png'/>">
										</a><i></i><span>(横屏广告)</span></li>
										<li mark="wide_blend"><a href="javascript:void(0)"><img
												src="<ls:templateResource item='/resources/templets/amaze/images/09.png'/>">
										</a><i></i><span>(混合版式)</span></li>
									</ul>
								</div>

							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="submit" value="保存" class="${btnclass}" /> 
									<input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/floor/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({
	});

	$(document).ready(function() {
						jQuery("#form1").validate({
							rules : {
								name : "required",
								seq : {
									required : true,
									number : true
								}
							},
							messages : {
								name : "请输入标题",
								seq : {
									required : "请输入数字",
									number : "请输入数字"
								}
							}
						});

						var layout = "${empty floor.layout?'wide_adv_rectangle_four':floor.layout}";
						<c:choose>
						<c:when test="${not empty floor.flId}">
						// $("#choose_template").children().removeClass("this");
						$("li[mark='" + layout + "']").addClass("this");
						$("input[name='layout']").val(layout);
						$("li[mark='" + layout + "']").siblings().remove();
						</c:when>
						<c:otherwise>
						$("#choose_template li").click(function() {
							$(this).parent().find("li").removeClass("this");
							$(this).addClass("this");
							var mark = $(this).attr("mark");
							$("input[name='layout']").val(mark);
						});
						</c:otherwise>
						</c:choose>
						//斑马条纹
						$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>

