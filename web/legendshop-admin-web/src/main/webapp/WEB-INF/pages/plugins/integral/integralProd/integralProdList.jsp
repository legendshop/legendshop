<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
</head>
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">积分商城&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">积分商品列表 </span></th>
					</tr>
			</table>
			<form:form id="form1" action="${contextPath}/admin/integralProd/query" method="post">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					<div class="criteria-div">
						<span class="item">
							商品分类：<input type="text" style="cursor: pointer;" name="prodCatName" id="prodCatName" value="${prod.prodCatName}" onclick="loadCategoryDialog();" class="${inputclass}" placeholder="商品分类" readonly="readonly" />
							<input type="hidden" id="firstCid" name="firstCid" value="${prod.firstCid }" /> 
							<input type="hidden" id="twoCid" name="twoCid" value="${prod.twoCid }" /> 
							<input type="hidden" id="thirdCid" name="thirdCid" value="${prod.thirdCid }" />
						</span>
						<span class="item">
							商品名称：<input type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="搜索相关商品..." class="${inputclass}" />
						</span>
						<span class="item">
							精品：<select id="isCommend" name="isCommend" class="${selectclass}">
								<option value="">请选择</option>
								<option value="0"
									<c:if test="${prod.isCommend eq 0 }">selected="selected"</c:if>>
									否</option>
								<option value="1"
									<c:if test="${prod.isCommend eq 1 }">selected="selected"</c:if>>是
								</option>
								</select> 
						<input type="submit" onclick="search()" class="${btnclass}" value="搜索" /> 
						<input class="criteria-btn" type="button" value="创建积分商品"
							onclick="window.location='<ls:url address="/admin/integralProd/load"/>'">
						</span>
					</div>
			</form:form>
			<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
			<div align="center" class="order-content-list">
			<display:table name="list" requestURI="/admin/integralProd/query"
				id="item" export="false" class="${tableclass}" style="width:100%;"
				sort="external">
				<display:column style="min-width:35px;vertical-align: middle;text-align: center;" title="
					<span>
			           <label class='checkbox-wrapper'>
							<span class='checkbox-item'>
								<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
								<span class='checkbox-inner' style='margin-left:10px;'></span>
							</span>
					   </label>	
					</span>">
					<label class="checkbox-wrapper">
						<span class="checkbox-item">
							<input type="checkbox" value="${item.id}" arg="${item.name}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
				</display:column>
				
				<display:column title="商品" style="width:100px">
					<a href="<ls:url address='${PC_DOMAIN_NAME}/integral/view/${item.id}'/>"
						target="_blank" title="${item.name}" style="margin: 10px;"> <img
						width="65" height="65"
						src="<ls:images item='${item.prodImage}' scale='3'/>"
						alt="${item.name}">
					</a>
				</display:column>
				<display:column title="名称"><div class="order_img_name">${item.name}</div></display:column>
				<display:column title="商品分类"  style="min-width:100px" property="categoryName"></display:column>
				<display:column title="价格" style="min-width:50px" property="price"></display:column>
				<display:column title="订购数" style="min-width:70px" property="saleNum" sortable="true"
					sortName="saleNum"></display:column>
				<display:column title="库存" property="prodStock" sortable="true"
					sortName="prodStock"></display:column>
				<display:column title="商品状态" style="min-width:100px">
					<c:choose>
						<c:when test="${item.status==0}">已下线</c:when>
						<c:when test="${item.status==1}">在出售</c:when>
					</c:choose>
				</display:column>
				<display:column title="操作" media="html" style="width:205px;">
					<div class="table-btn-group">
						<c:choose>
							<c:when test="${item.status == 1}">
								<button class="tab-btn" name="statusImg" item_id="${item.id}" itemName="${fn:escapeXml(item.name)}" status="${item.status}">
									下线
								</button>
								<span class="btn-line">|</span>
							</c:when>
							<c:otherwise>
								<button class="tab-btn" name="statusImg" item_id="${item.id}" itemName="${fn:escapeXml(item.name)}" status="${item.status}">
									上线
								</button>
								<span class="btn-line">|</span>
							</c:otherwise>
						</c:choose>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/integralProd/load/${item.id}'">
							编辑
						</button>
						<span class="btn-line">|</span>
						<button class="tab-btn" prodName="${fn:escapeXml(item.name)}" onclick="confirmDelete('${item.id}',this);">
							删除
						</button>
					</div>
				</display:column>
			</display:table>
			<div class="clearfix">
		   		<div class="fl">
		    		<input type="button" value="批量删除" id="batch-del" class="batch-btn">
		    		<input type="button" value="批量下线" id="batch-offline" class="batch-btn">
				</div>
		   		<div class="fr">
		   			 <div cla ss="page">
		    			 <div class="p-wrap">
		        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					 	</div>
		  			 </div>
		  		</div>
		   	</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 建立商品的4个步骤：1.商品详细信息 2.商品相关说明图片
						3.动态属性 4.动态参数 <br> 2. 商品有一个对应的品牌<br> 3.
						商品状态，在开始时间和结束时间之内有效，失效后包括下线状态将不会在前台显示，推荐状态为是则在首页精品推荐中出现<br> 4.
						价格、运费、库存量为正数，不填写则不在前台显示，如果填写了库存量为0则无法订购<br> 5.
						库存量在用户下单时会减少，实际库存量在订单成交时减少<br>
					</td>
				<tr>
			</table>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	function getCheckedProd() {
		var integralProdList = $(".selectOne:checked");
		var prodArr = new Array();
		integralProdList.each(function(index, domEle) {
			var prodId = $(this).val();
			prodArr.push(prodId);
		});
		return prodArr.toString();
	}

	$("#batch-del").on("click", function() {
		var prodIdStr = getCheckedProd();

		jQuery.ajax({
			url : "${contextPath}/admin/integralProd/batchDelete/" + prodIdStr,
			type : 'DELETE',
			async : false, //默认为true 异步  
			dataType : 'json',
			error : function(data) {
			},
			success : function(data) {
				if (data == "OK") {
					layer.msg("批量删除积分商品成功！", {icon:1, time:700}, function(){
						setInterval(function() {
							window.location.reload(true);
						}, 1000);
					});
				} else {
					layer.msg("操作失败！", {icon:2});
				}
			}
		});
	});

	$("#batch-offline").on("click", function() {
				var prodIdStr = getCheckedProd();

				jQuery.ajax({
					url : "${contextPath}/admin/integralProd/batchOffline/" + prodIdStr,
					type : 'PUT',
					async : false, //默认为true 异步  
					dataType : 'json',
					error : function(data) {
					},
					success : function(data) {
						if (data == "OK") {
							layer.msg("批量下线积分商品成功！", {icon:1, time:700}, function(){
								setInterval(function() {
									window.location.reload(true);
								}, 1000);
							});
						} else {
							layer.msg("操作失败！", {icon:2});
						}
					}
				});
			});
	//将商品放入回收站
	function confirmDelete(prodId, obj) {
		var prodName = $(obj).attr("prodName");
		layer.confirm("是否将商品：'" + prodName + "'删除,会影响积分订单及积分商品查看?", {icon:3}, function() {
					jQuery.ajax({
						url : "${contextPath}/admin/integralProd/delete/" + prodId,
						type : 'get',
						async : false, //默认为true 异步  
						dataType : 'json',
						error : function(data) {
						},
						success : function(data) {
							if (data == "OK") {
								layer.msg("已将积分商品删除！", {icon:1, time: 700}, function(){
									setInterval(function() {
										window.location.reload(true);
									}, 1000);
								});
							} else {
								layer.msg("操作失败！", {icon:2});
							}
						}
					});
				});
	}

	function pager(curPageNO) {
		$("#curPageNO").val(curPageNO);
		$("#form1").submit();
	}

	jQuery(document).ready(function() {
						//三级联动
						$("select.combox").initSelect();

						jQuery("button[name='statusImg']").click(function(event) {
											$this = $(this);
											var item_id = $(this).attr("item_id");
											var status = $(this).attr("status");
											var desc;
											var toStatus;
											if (status == 1) {
												toStatus = 0;
												desc = "将'"+ $(this).attr("itemName")+ "'放入仓库?";
											} else {
												toStatus = 1;
												desc = $(this).attr("itemName")+ ' 上线?';
											}
											
											layer.confirm(desc, {icon:3}, function(){
												jQuery.ajax({
													url : "${contextPath}/admin/integralProd/updatestatus/"+item_id+"/"+toStatus,
													type : 'get',
													async : false, //默认为true 异步  
													dataType : 'json',
													error : function(
															data) {
													},
													success : function(data) {
														if (data == "OK") {
															window.location.reload(true);
														} else {
															layer.alert(data, {icon:0});
														}
													}
												});
												return true;
											});
						});
					});

	//加载分类用于选择
	function loadCategoryDialog() {
		layer.open({
			  title :"选择分类",
			  id: "prodCat",
			  type: 2, 
			  content: "${contextPath}/admin/product/loadAllCategory/I", //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['350px', '400px']
			}); 
	}

	function clearCategoryCallBack() {
		$("#firstCid").val("");
		$("#twoCid").val("");
		$("#thirdCid").val("");
		$("#prodCatName").val("");
	}

	function loadCategoryCallBack(firstCid, twoCid, thirdCid, catName) {
		if (firstCid != "" && firstCid != undefined) {
			$("#firstCid").val(firstCid);
		}
		if (twoCid != "" && twoCid != undefined) {
			$("#twoCid").val(twoCid);
		}
		if (thirdCid != "" && thirdCid != undefined) {
			$("#thirdCid").val(thirdCid);
		}
		$("#prodCatName").val(catName);
	}
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	
</script>
</html>
