 <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<style type="text/css">
		body{
			min-height:96%;
		}
		#form1{
			margin:15px;
			line-height: 28px;
		}	
		input[type="text"]{
			height:28px;
			border: 1px solid #efefef;
		}
		.see-able{
			width: 93%;
			min-width:600px;
			margin-left:25px;
		}
		.see-able th,td{
			height:30px !important;
		}
	</style>
</head>
<body>
	 <form:form action="${contextPath}/admin/coupon/loadSelectShop" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f9f9f9;border: 1px solid #efefef;font-size: 14px;margin: 10px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;border:1px solid #efefef;padding: 5px;height: 28px;"  type="text" name="shopName" id="name" maxlength="20" value="${shopName}" size="20" placeholder="请输入店铺名称" />
			<input type="submit" value="搜索" class="criteria-btn" />
			</div>
		</div>
	 </form:form>
	<table class="see-able am-table am-table-striped am-table-hover table" alig="center">
		<thead>
			<tr><th width="600">名称</td><th width="110">操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="2" style="text-align: center;">没有找到符合条件的店铺</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="shop">
          <tr>      
          	<td>${shop.siteName}</td>
          	<td>
          	<button class="criteria-btn" onclick="addShop('${shop.shopId}','${shop.siteName}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
      <div class="fr" style="display: block;width:100%;padding-right:10px;">
	 	<div class="page">
 			 <div class="p-wrap">
     		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 </div>
	 	</div>
	   </div>
      <script type="text/javascript">
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      function addShop(shopId,siteName){
	    	  window.parent.addShopTo(shopId,siteName);
	    	  var index = parent.layer.getFrameIndex('selectShop'); //先得到当前iframe层的索引
	    	  parent.layer.close(index); //再执行关闭 
		  }
      </script>
</body>
</html>