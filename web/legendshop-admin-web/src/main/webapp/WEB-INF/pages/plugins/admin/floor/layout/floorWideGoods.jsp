<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
  <head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/indexlayout.css'/>" rel="stylesheet"/>
    <title>楼层编辑界面</title>
     
     <style type="text/css">
     	.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}
     </style>
  </head>
  <body >
  <form:form  action="${contextPath}/admin/system/floor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 98%;margin: 10px" >
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo; 
				    	<a href="<ls:url address='/admin/floor/query'/>">5X商品楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
	    </table>
	  </form:form>
	     <div id="doc">
	      <div id="bd">
	           
	           <div class="wide_floor" mark="wide_goods" style="">
				    <div class="wide_floor_c">
				      <div id="prodEdit" style="width: 1190px; height: 25px;line-height: 25px;top:100px;" class="flooredit"><a href="javascript:onclickProducts('${floor.id}')" style="color:white;">商品编辑</a></div>
				      <div class="wide_floor_c_top">${floor.name}</div>
				      <div class="wide_floor_c_b">
				        <div class="wide_floor_c_right">
				          <ul>
				          <c:choose>
				         <c:when test="${empty items}">
				                <li> 
							    </li>
							    <li> 
							    </li>
							    <li> 
							    </li>
							    <li> 
							    </li>
							    <li> 
							    </li>
				         </c:when>
				         <c:otherwise>
				          <c:forEach items="${items}" var="item" end="5"> 
				             <li> 
				                <span>
				                 <a href="javascript:void(0);"> 
				                  <img src="<ls:images item='${item.productPic}' scale='0' />">
												</a>
								</span> <b style="height: 49px;overflow: hidden"><a href="goods_url">${item.productName}</a>
											</b> <strong>￥${item.productCash}</strong>
							</li>
	      		 		  </c:forEach>
				         </c:otherwise>
				      </c:choose>
				           </ul>
				        </div>
				      </div>
				    </div>
				  </div>
	         <div   style="text-align: center;clear: both; padding-top : 30px;" >
	          	      <input type="button" onclick="window.location='${contextPath}/admin/floor/query' " class="criteria-btn" value="返回">
	     		 </div> 
	      </div>
     </div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorWideGoods.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var _flId = "${floor.flId}";
</script>
</html>
