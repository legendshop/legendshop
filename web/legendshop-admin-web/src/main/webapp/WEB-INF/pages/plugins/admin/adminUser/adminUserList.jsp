<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	int offset = ((Integer) request.getAttribute("offset")).intValue();
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">管理员管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"> 管理员管理</span></th>
					</tr>
				</table>
			<form:form action="${contextPath}/admin/adminUser/query" id="form1"
				method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						管理员名：<input class="${inputclass}" type="text" name="name" maxlength="50" value="${bean.name }" placeholder="管理员名"/> 
					</span>
					<span class="item">
						姓名：<input class="${inputclass}" type="text" name="realName" maxlength="50" value="${bean.realName }" placeholder="姓名"/> 
					</span>
					<span class="item">
						状态 ：<select id="enabled" name="enabled" class="criteria-select">
							<ls:optionGroup type="select" required="false" cache="true"
								beanName="ENABLED" selectedValue="${bean.enabled}" />
						   </select> 
						<input type="button" onclick="search()" class="${btnclass}" value="搜索" /> 
						<input type="button" class="${btnclass}" value="创建管理员" onclick='window.location="<ls:url address='/admin/adminUser/load'/>"' />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/adminUser/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="管理员名" property="name"></display:column>
					<display:column title="姓名" property="realName"></display:column>
					<display:column title="状态">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="ENABLED" selectedValue="${item.enabled}" defaultDisp="" />
					</display:column>
					<display:column title="到期时间">
						<fmt:formatDate value="${item.activeTime }" type="both"
							dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
					</display:column>
					<display:column title="备注" property="note"></display:column>
					<display:column title="用户角色">
						<a
							href="<ls:url address='/admin/member/user/roles/${item.id}?appNo=BACK_END'/>">用户角色</a>
					</display:column>
					<display:column title="用户权限">
						<a
							href="<ls:url address='/admin/member/user/functions/${item.id}?appNo=BACK_END'/>">用户权限</a>
					</display:column>
					<display:column title="修改密码">
						<a href="<ls:url address='/admin/member/user/update/${item.id}?appNo=BACK_END'/>">修改密码</a>
					</display:column>

					<display:column title="操作" media="html" style="width:235px">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/adminUser/load/${item.id}'">
								 修改
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.id}')">
								 删除
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>

	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm("确定删除 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/admin/adminUser/delete/" + id + "'/>";
		   });
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>
