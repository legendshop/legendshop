<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	<display:table name="list" requestURI="/admin/prodGroup/query" id="item" export="false" class="${tableclass}" sort="external">

	    <display:column title="分组名称"><c:if test="${item.type eq 0}"><i style="color:red;">*</i></c:if> ${item.name}</display:column>
	    <display:column title="分组类型">
	    	<c:choose>
	    		<c:when test="${item.type eq 0}">系统定义</c:when>
	    		<c:when test="${item.type eq 1}">自定义</c:when>
	    	</c:choose>
	    </display:column>
	    <display:column title="分组描述">
	    	<c:choose>
	    		<c:when test="${empty item.description}">暂无</c:when>
	    		<c:otherwise> ${item.description}</c:otherwise>
	    	</c:choose>
	    </display:column>
	    <display:column title="修改时间" sortable="true" sortName="applyTime">
	        <fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
	    </display:column>
	    <display:column title="操作" media="html" style="width:200px">
	        <div class="table-btn-group">
	        
	        	<c:if test="${item.type eq 1}">
	            	<button class="tab-btn" onclick="window.location='${contextPath}/admin/prodGroup/update/${item.id}'">编辑</button>
	           		<span class="btn-line">|</span>
	            	<button class="tab-btn" onclick="window.location='${contextPath}/admin/prodGroup/queryProdList?groupId=${item.id}'">查看商品</button>
	           		<span class="btn-line">|</span>
	           		<button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
	           	</c:if>
	        </div>
	    </display:column>
    </display:table>
   	<div class="clearfix">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 </div>
   			 </div>
   		</div>
   	</div>
