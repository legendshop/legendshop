<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/mergeGroup/mergeGroupDetail.css'/>" rel="stylesheet"/>
</head>
<body>
   
   <jsp:include page="/admin/top" />
   
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div style="border-bottom:1px solid #ddd;width: 100%;height:40px;line-height:40px;">
		      		<span class="title-span" style="margin-left:10px;">
						拼团管理  ＞  
						<a href="<ls:url address="/admin/mergeGroup/query"/>">拼团活动管理</a>
						  ＞   <span style="color:#0e90d2;">拼团详情</span>
					</span>
		      	</div>
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
				<div class="add-fight" style="width: 90%; margin: 25px 75px;; float: left;">
					<!-- <div class="w1190"> -->
					<form id="mergeForm">
						<!-- 拼团列表 -->
						<div class="add-item" style="margin-top: 10px">
							<div class="add-tit">
								<span>选择活动商品</span>
							</div>
				
							<div class="goods-table" style="margin-top: 20px;">
								<%@ include file="mergeGroupDetailSku.jsp"%>
							</div>
						</div>
						<!-- /拼团列表 -->
						<!-- 活动信息 -->
						<div class="add-item">
							<div class="add-tit">
								<span>活动信息</span>
							</div>
							<div class="activity-mes">
								<span class="act-tit"><em>*</em>拼团活动名称：</span> <input type="text"
									disabled="disabled" value="${mergeGroup.mergeName }"
									class="act-name" placeholder="请填写活动名称"> <span
									class="act-des">（2-20）字</span>
							</div>
							<div class="activity-mes">
								<span class="act-tit"><em>*</em>活动时间：</span> <span class="act-time">
									<input type="text"
									value='<fmt:formatDate value="${mergeGroup.startTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>'
									disabled="disabled"> &nbsp;-&nbsp; <input type="text"
									value='<fmt:formatDate value="${mergeGroup.endTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>'
									disabled="disabled">
								</span>
							</div>
						</div>
						<!-- /活动信息 -->
						<!-- 活动规则 -->
						<div class="add-item">
							<div class="add-tit">
								<span>活动规则</span>
							</div>
							<div class="activity-mes">
								<span class="act-tit"><em>*</em>成团人数：</span> <input type="text"
									class="act-name" placeholder="请填写成团人数"
									value="${mergeGroup.peopleNumber }" disabled="disabled"> <span
									class="act-des">（满足设定人数即可成团）</span>
							</div>
							<%-- <div class="activity-mes">
								<span class="act-tit">限购设置：</span> <span class="act-limit"> <a
									href="javascripe:void(0)"
									class="lim-btn <c:if test='${!mergeGroup.isLimit }'>on</c:if>">不限</a><a
									href="javascripe:void(0)"
									class="lim-btn <c:if test='${mergeGroup.isLimit }'>on</c:if>">限制</a>
								</span> <span class="act-limit number"> </span>
							</div> --%>
							<div class="activity-mes">
								<span class="act-tit">团长是否免单：</span> <span class="act-limit">
									<a href="javascripe:void(0)"
									class="lim-btn <c:if test='${!mergeGroup.isHeadFree }'>on</c:if>">否</a><a
									href="javascripe:void(0)"
									class="lim-btn <c:if test='${mergeGroup.isHeadFree }'>on</c:if>">是</a>
								</span>
							</div>
							<div class="activity-mes" style="margin-top: 15px;">
								<span class="act-tit">活动图片：</span>
								<div class="con-input">
									<img class="add-img" src="<ls:photo item='${mergeGroup.pic }'/>">
								</div>
							</div>
							<div class="activity-mes" style="margin-top: 35px;">
								<span class="act-tit"></span> <a
									href="${contextPath }/admin/mergeGroup/query" class="lim-btn on"
									style="border: 1px solid #0e90d2;background-color: #0e90d2;border-color: #0e90d2;color: #fff;">返回</a>
							</div>
						</div>
						<!-- /活动规则 -->
					</form>
				</div>			    
		     </div>
	 </div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var photoPath = "<ls:photo item=''/>";
</script>
</html>
