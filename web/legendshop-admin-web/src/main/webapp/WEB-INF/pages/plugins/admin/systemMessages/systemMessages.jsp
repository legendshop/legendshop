<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.checkbox-item {
    display: inline-block;
    position: relative;
    line-height: 19px !important;
    vertical-align: middle;
    cursor: pointer;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/Message/query"/>"> 发送会员通知</a>
	               	&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"> 发送会员通知</span>
		    	</th>
	    	</tr>
	    </table>
	<form:form action="${contextPath}/admin/system/Message/save" method="post" id="form1" enctype="multipart/form-data">
	<input id="msgId" name="msgId" value="${siteInformation.msgId}" type="hidden">
		<div align="center" style="margin-top: 10px;">
			<table class="${tableclass} no-border content-table" id="col1" style="width: 100%">
				<tr>
					<td style="width:200px;">
						<div align="right">
							<font color="ff0000">*</font> 标题:
						</div>
					</td>
					<td align="left">
						<input type="text" name="title" id="title" value="${siteInformation.title}" maxlength="40" style="width: 400px" class="${inputclass}" />
					</td>
				</tr>

				<tr>
					<td>
						<div align="right" style="margin-top:-35px;">
							<font color="ff0000">*</font> 参加活动的会员等级:
						</div>
					</td>
					<td align="left" style="height:80px !inportant;">
						<span>
							<input style="margin-bottom: 10px;" type="button" value="全选" onclick="selAll();" class="${btnclass}" />
							<span id="userGrade"><em></em><br></span>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right" style="margin-top: -155px;">
							<font color="ff0000">*</font> 内容:
						</div>
					</td>
					<td	>
						<div style="margin-left:4px;">
							<textarea name="text" id="text" cols="100" rows="8" style="width:100%;height:300px;visibility:hidden;">${siteInformation.text}</textarea>
                   		</div>
                   </td>
				</tr>
				<tr>
					<td colspan="2">
						<div align="center">
							<input type="submit" value="发送" class="${btnclass}" /> 
							<input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/system/Message/query"/>'" />
						</div></td>
				</tr>
			</table>
		</div>
	</form:form>
	</div>
	</div>
	</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/systemMessage.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
	$.validator.setDefaults({});
	$.extend($.validator.defaults,{ignore:""});
	var userGradeArray = jQuery.parseJSON('${userGrade}');
	var userGradeDiv = jQuery("#userGrade");
</script>
</html>

