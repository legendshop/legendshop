<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    <form:form  action="${contextPath}/admin/weiXinKeywordResponse/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<strong class="am-text-primary am-text-lg">微信消息管理</strong> /  微信关键词回复管理
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	     <div align="right" style="padding: 3px">
					            	<input class="${btnclass}" type="button" value="创建关键词" onclick='window.location="<ls:url address='/admin/weiXinKeywordResponse/load'/>"'/>
					  </div>
		    </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		 <display:table name="list" requestURI="/admin/weiXinKeywordResponse/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
		 	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="微信关键字 " property="keyword"> </display:column>
     		<display:column title="匹配类型  ">
     		      <c:choose>
				     <c:when test="${item.pptype==1}">模糊匹配</c:when>
				     <c:when test="${item.pptype==2}">完全匹配</c:when>
				 </c:choose>
     		</display:column>
     		<display:column title="消息类型  ">
     		      <c:choose>
				     <c:when test="${item.msgtype=='text'}">文本消息</c:when>
				     <c:when test="${item.msgtype=='news'}">图文消息</c:when>
				       <c:when test="${item.msgtype=='image'}">图片消息</c:when>
				         <c:when test="${item.msgtype=='image'}">图片消息</c:when>
				           <c:when test="${item.msgtype=='voice'}">音频消息</c:when>
				             <c:when test="${item.msgtype=='video'}">视频消息</c:when>
				 </c:choose>
     		</display:column>
     		<display:column title="消息模版" property="templatename" >
     		</display:column>
		    <display:column title="" media="html"  style="width:220px">
		    	<div class="am-btn-toolbar">
				  <div class="am-btn-group am-btn-group-xs">
						<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weiXinKeywordResponse/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 修改</button>
						<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}')" ><span class="am-icon-trash-o"></span> 删除</button>
				  </div>
				</div>
		      </display:column>
	    </display:table>
       <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
	    </div>
	<table class="am-table am-table-striped am-table-hover table-main">
		<tbody>
			<tr>
				<td align="left">说明：<br> 1.针对于用户行为，您可以设定文字、语音、图文、图片 作为自动回复，
				      微信用户发送任意消息，服务会根据微信用户发送的消息类型来自动回复你编辑好的内容；
				            如：用户发送 “你好！”; 服务端回复：“亲爱用户您好：回复0查看目录 A推荐商品 B查看个人资料”
					</td>
			</tr>
			<tr>
			</tr>
		</tbody>
	</table>
<script language="JavaScript" type="text/javascript">
			  function deleteById(id) {
				  layer.confirm(" 删除会影响微信服务确定删除 ?", {
						 icon: 3
					     ,btn: ['确定','关闭'] //按钮
					   }, function(){
						   window.location = "<ls:url address='/admin/weiXinKeywordResponse/delete/" + id + "'/>";
					   });
			    }
         highlightTableRows("item");
		</script>

