<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
<style type="text/css">
	.informButton {margin: 0 auto;}
	.mybutton{
	color: #ffffff;
    border: 1px solid #0e90d2;
    background-color: #0e90d2;
    border-color: #0e90d2;
    height: 26px;
    margin: 3px;
    padding-left: 12px;
    padding-right: 12px;
    text-align: center;
}
</style>
	<input type="hidden" id="userId" name="userId" value="${userId}"/>

	<table class="selling-table" style="width:400px;margin-left:10px;">
		<tr class="detail">
			<td>充值：</td>
			<td>
			<input type="text" name="amount"  id ="amount">
			</td>
		</tr>
		<tr class="detail">
			<td colspan="2">
			<div>
				<input class="mybutton" type="button" value="返回" onclick="goback();"/>
				<input type="button" class="mybutton" value="充值" onclick="recharge();"/>
			</div>
			</td>
		</tr>
	</table>
<script type="text/javascript">
	function recharge(){
		var userId = $("#userId").val();
		var amount = $("#amount").val();
		if(isBlank(amount)){
			layer.msg("请正确输入！",{icon:0});
			return;
		}
		var data = {"userId":userId,"amount":amount};
		jQuery.ajax({
			url:"${contextPath}/admin/coin/recharge" , 
			data: data,
			type:'post', 
			async : false, //默认为true 异步     
		    dataType : 'json',
			success:function(msg){
				if("OK" == msg){
					layer.msg("充值成功！",function(){
						parent.location.reload();
					});
				}else if("fail" == msg){
					layer.msg("充值失败,请稍后重试",{icon:2});
				}else if("notAccount" == msg){
					layer.msg("充值不能为空！",{icon:0});
				}else{
					layer.msg("网络错误，请稍后重试！",{icon:2});
				}
			}
		});
	}
	
	function goback(){
		layer.closeAll();
		window.location.href="${contextPath}/admin/system/userDetail/query";
	}
	
	function isBlank(value){
		if(value == null || value == undefined || value == "" || value == 0){
			return true;
		}
		return false;
	}
    
</script>



