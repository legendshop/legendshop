<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
  <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
        <form:form action="${contextPath}/admin/auction/save" method="post" id="form1">
        	<input type="hidden" value="" name="skuId" id="skuId"/>
        <table class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr><th>
	    	<strong class="am-text-primary am-text-lg">拍卖活动</strong> / 添加拍卖活动
			</th>
	    	</tr>
	    </thead>
	    </table>
       <div align="center">
         <table  style="width: 100%" class="${tableclass}" id="col1">
      <tr>
        <td>
          <div align="right">活动名称:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="50" name="auctionsTitle" id="auctionsTitle"  size="50" class="${inputclass}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">拍卖商品:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="button" value="选择商品" onclick="showProdlist()"/>
           	<input type="hidden" value="" name="prodId" id="prodId"/>
        </td>
      </tr>
      <tr class="prodInfo" style="display: none;">
      	 <td>
          <div align="right">商品信息:</div>
       </td>
        <td>
        	<a id="prodName" target="_blank"></a>
           	<input type="button" id="remove" value="移除" onclick="removeProd()"/>
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">开始时间:<font color="ff0000">*</font></div>
       </td>
        <td>
          <input readonly="readonly"  name="startTime" size="21" id="startTime" type="text"  />
        </td>
      </tr>
       <tr>
        <td>
          <div align="right">结束时间:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input readonly="readonly"  name="endTime" size="21" id="endTime" class="Wdate" type="text"  />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">起拍价:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text"  maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="floorPrice" id="floorPrice"  class="${inputclass}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">一口价:<font color="ff0000">*</font></div>
       </td>
        <td>
           <div class="onePrice" id="onePrice"><input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="fixedPrice" id="fixedPrice"  class="${inputclass}" /></div>
        	<input  type="checkbox" class="isCeiling"/> 无封顶
        	<input name="isCeiling" id="isCeiling" type="hidden" value="1"/>
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">最小加价幅度:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="minMarkupRange" id="minMarkupRange"  class="${inputclass}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">最高加价幅度:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="maxMarkupRange" id="maxMarkupRange"  class="${inputclass}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">是否支付保证金:<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="radio" name="isSecurity"  onclick="show(this)"  value="1" /> 是   
           <input type="radio" name="isSecurity"  onclick="show(this)" value="0" checked="checked"/> 否 
        </td>
      </tr>
      	
	       <tr style="display: none;" class="securityPrice">
	        <td>
	          <div align="right">保证金:<font color="ff0000">*</font></div>
	       </td>
	        <td>
	          <input type="text" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" maxLength="10" name="securityPrice" id="securityPrice"  class="${inputclass} ignore" />
	        </td>
	      </tr>
	      <tr style="display: none;" class="delayTime">
	        <td>
	          <div align="right">延长周期:<font color="ff0000">*</font></div>
	       </td>
	        <td>
	          <input type="text" maxLength="3" name="delayTime" id="delayTime"  class="${inputclass} ignore" />
	        </td>
	      </tr>
	      <tr>
	        <td>
	          <div align="right">是否隐藏起拍价:<font color="ff0000">*</font></div>
	       </td>
	        <td>
	        	<div>
		           <input type="radio" value="1" name="hideFloorPrice" checked="checked"/> 是   
		           <input type="radio" value="0" name="hideFloorPrice" /> 否 
	           </div>
	        </td>
    	  </tr>
    	   <tr>
	        <td>
	          <div align="right">拍卖描述:</div>
	       </td>
	        <td>
	        	<textarea name="auctionsDesc" id="auctionsDesc" cols="100" rows="8" style="width:100%;height:400px;visibility:hidden;"></textarea> 
	        </td>
    	  </tr>
      <tr>
             <td colspan="2">
                 <div align="center">
                     <input type="submit" class="${btnclass}" value="保存"/>
                     <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/auction/query?status=${bean.status}'" />
                 </div>
             </td>
         </tr>
     </table>
           </div>
        </form:form>
<script language="javascript">
KindEditor.options.filterMode=false;
KindEditor.create('textarea[name="auctionsDesc"]', {
	cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
	uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
	fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
	allowFileManager : true,
	afterBlur:function(){this.sync();},
	width : '100%',
	height:'400px',
	afterCreate : function() {
		var self = this;
		KindEditor.ctrl(document, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
		KindEditor.ctrl(self.edit.doc, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
	}
});
var contextPath='${contextPath}';
	function showProdlist(){
		layer.open({
			  title :"选择商品",
			  type: 2, 
			  content: '${contextPath}/admin/auction/AuctionProdLayout', 
			  area: ['1000px', '560px']
			}); 
		}
		
jQuery.validator.addMethod("isNumber", function(value, element) {       
         return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
    }, "必须整数或小数");  

jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
         value=parseFloat(value);      
         return this.optional(element) || value>=1;       
    }, "必须大于等于1元"); 
    
jQuery.validator.addMethod("checkMaxVal", function(value, element) {
		var min =parseFloat($("#minMarkupRange").val());
		var newValue=parseFloat(value);
      return this.optional(element) || newValue>min;  
     }, "必须大于最小金额");
jQuery.validator.addMethod("checkMoney", function(value, element) {
		var min =parseFloat($("#floorPrice").val());
		var newValue=parseFloat(value);
      return this.optional(element) || newValue>min;  
     }, "一口价必须大于起拍价");
     
    $(document).ready(function() {
	    jQuery("#form1").validate({
	    ignore: ".ignore",
	            rules: {
		            auctionsTitle: {
		                required: true
		            }, startTime: {
		                required: true
		            },endTime: {
		                required: true
		            },floorPrice: {
		                required: true,
		                isNumber:true,
		                isFloatGtZero:true
		            },fixedPrice: {
		                required: true,
		                isNumber:true,
		                isFloatGtZero:true,
		                checkMoney:true
		            },minMarkupRange: {
		                required: true,
		                isNumber:true,
		                isFloatGtZero:true
		            },maxMarkupRange: {
		                required: true,
		                isNumber:true,
		                isFloatGtZero:true,
		                checkMaxVal:true
		            },securityPrice:{
		                required: true,
		                isFloatGtZero:true
		            },delayTime:{
		                required: true,
		                digits:true
		            },prodId:{
		            	required: true
		            }
	            
	        },
	        messages: {
	            auctionsTitle: {
	                required: "请输入活动名称"
	            },startTime: {
	                required: "请输入开始时间"
	            },endTime: {
	                required: "请输入结束时间"
	            },floorPrice: {
	                required: "请输入起拍价",
	            },fixedPrice: {
	                required: "请输入一口价"
	            },minMarkupRange: {
	                required: "请输入最小加价幅度"
	            },maxMarkupRange: {
	                required: "请输入最大加价幅度"
	            },securityPrice:{
	                required: "请输入保证金"
	            },delayTime:{
	                required: "请输入延长时间(分钟)",
	                digits:"必须为整数"
	            },prodId:{
	            	required: "商品不能为空"
	            }
	        }
	    });
});

	function show(obj){
		var val= $(obj).val();
		if(val==1){
			$("#securityPrice").removeClass("ignore");
			$("#delayTime").removeClass("ignore");
			$(".securityPrice").css("display","");
	    	$(".delayTime").css("display","");
		}else{
			$("#securityPrice").addClass("ignore");
			$("#delayTime").addClass("ignore");
			$("#securityPrice").val("");
			$("#delayTime").val("");
			$(".securityPrice").css("display","none");
	    	$(".delayTime").css("display","none");
		}
	}
	
	$(".isCeiling").click(function(){
		  if($(this).is(':checked')){//无封顶
		  	$("#onePrice").hide();
		  	$("#fixedPrice").addClass("ignore");
		  	$("#isCeiling").val(0);
		  }else{
		  	$("#onePrice").show();
		  	$("#fixedPrice").val("");
		  	$("#fixedPrice").removeClass("ignore");
		  	$("#isCeiling").val(1);
		  }
	});
	 $(".isSecurity").click(function(){
    	 if($(this).is(':checked')){//有保证金
    		$(".securityPrice").show(300);
    		$(".delayTime").show(300);
    		$("#securityPrice").removeClass("ignore");
    		$("#delayTime").removeClass("ignore");
		  }else{
    		$(".securityPrice").hide(300);
    		$(".delayTime").hide(300);
    		$("#securityPrice").addClass("ignore");
    		$("#delayTime").addClass("ignore");
    		$("#securityPrice").val("");
    		$("#delayTime").val("");
		  }
    });
    
	function addProdTo(skuid,name,prodid){
			$(".prodInfo").css("display","");
			$("#prodName").text(name);
			$("#prodName").attr("href","${contextPath}/views/"+prodid);
			$("#prodId").val(prodid);
			$("#skuId").val(skuid);
			$("#prodId").next().css("display","none");
			$("#remove").show();
			
	}
	
	function removeProd(){
		$("#prodId").val("");
		$("#skuId").val("");
		$(".prodInfo").css("display","none");
	}
	
	
	 laydate.render({
   		 elem: '#startTime',
   		 type:'datetime',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endTime',
   	     type:'datetime',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
</script>
