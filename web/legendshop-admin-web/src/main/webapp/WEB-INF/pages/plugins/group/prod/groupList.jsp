<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">团购管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">团购活动管理</span></th>
				</tr>
			</table>

			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty group.searchType}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query"/>">全部活动</a></li>
						<li <c:if test="${group.searchType eq 'WAIT_AUDIT'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=WAIT_AUDIT"/>">待审核</a></li>
						<li <c:if test="${group.searchType eq 'NOT_PASS'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=NOT_PASS"/>">未通过</a></li>
						<li <c:if test="${group.searchType eq 'NOT_STARTED'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=NOT_STARTED"/>">未开始</a></li>
						<li <c:if test="${group.searchType eq 'ONLINE'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=ONLINE"/>">进行中</a></li>
						<li <c:if test="${group.searchType eq 'FINISHED'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=FINISHED"/>">已结束</a></li>
						<li <c:if test="${group.searchType eq 'EXPIRED'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/group/query?searchType=EXPIRED"/>">已失效</a></li>
						
					</ul>
				</div>
			</div>


			<form:form action="${contextPath}/admin/group/query" id="form1" method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<input type="hidden" value="${group.searchType}" name="searchType" id="searchType" /> 
					<span class="item">
						店铺：
						<input type="text" id="shopId" name="shopId" value="${group.shopId}" />
					</span>
					<span class="item">
						<input type="hidden" id="shopName" name="shopName" maxlength="50" class="${inputclass}" value="${group.shopName}" />
						活动名称：
						<input type="text" id="groupName" name="groupName" maxlength="50" value="${group.groupName}" class="${inputclass}" placeholder="请输入活动名称搜索" /> 
					</span>
					<span class="item">
						开始时间：
						<input readonly="readonly" class="${inputclass}" name="startTime" id="startTime"  type="text"
							value='<fmt:formatDate value="${group.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="开始时间"/>
					</span>
					<span class="item">
						结束时间：
						<input readonly="readonly" class="${inputclass}" name="endTime" id="endTime" type="text"
							value='<fmt:formatDate value="${group.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="结束时间"/>
						
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
						<ls:plugin pluginId="b2c">
							<input type="button" value="创建" class="${btnclass}"
								onclick='window.location="<ls:url address='/admin/group/load'/>"' />
						</ls:plugin>
					</span>
				</div>
			</form:form>
			<div align="center" id="groupContentList" class="order-content-list"></div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = '${contextPath}';
	
	$(document).ready(function () {
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});

	function sendData(curPageNO) {
	    var data = {
	   		"shopId": $("#shopId").val(),
	        "curPageNO": curPageNO,
	        "status": $("#status").val(),
	        "groupName": $("#groupName").val(),
	        "startTime": $("#startTime").val(),
	        "endTime": $("#endTime").val(),
	        "searchType": $("#searchType").val(),
	        "flag": "platform",
	    };
	    $.ajax({
	        url: "${contextPath}/admin/group/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#groupContentList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	
	$("#checkboxAll").on("click",function() {
		if ($(this).is(":checked")) {
			$('input[type=checkbox][name=groupId][can-offline=true]')
					.prop("checked", true);
		} else {
			$('input[type=checkbox][name=groupId][can-offline=true]')
					.removeAttr("checked");
		}
	});
	
	function pager(curPageNO) {
	    $("#curPageNO").val(curPageNO);
	    sendData(curPageNO);
	}
	//取消批量下线
/* 	$("#batch-off").on("click",function() {
		var groups = $("input[type=checkbox][name=groupId][can-offline=true]:checked");
		var groupArr = new Array();
		groups.each(function() {
			var groupId = $(this).val();
			groupArr.push(groupId);
		});
		var groupIdStr = groupArr.toString();
		if (groupIdStr == "") {
			layer.alert("请选择要下线的团购活动", {icon:0});
			return false;
		}

		$.ajax({
			url : contextPath + "/admin/group/batchOffline/"
					+ groupIdStr,
			type : 'put',
			async : false, //默认为true 异步   
			"dataType" : "json",
			error : function(jqXHR, textStatus, errorThrown) {
			},
			success : function(retData) {
				if (retData == "OK") {
					layer.msg("批量下线成功", {icon:1, time: 700}, function() {
						window.location.reload(true);
					});
					return;
				} else {
					layer.alert(retData, {icon:0});
					return;
				}
			}
		});
	}); */
	function deleteGroup(id, name) {
		layer.confirm('确定要删除【 " + name + " 】吗？', {icon:3}, function(){
			window.location.href = contextPath + "/admin/group/delete/" + id;
		});
	}
	
	function offlineGroup(id) {
		layer.confirm("是否确定进行终止操作?如果下线未成团的订单货款将全部原路返回", {icon:3}, function() {
			$.ajax({
				url : contextPath + "/admin/group/offlineGroup/" + id,
				type : 'post',
				async : false, //默认为true 异步   
				dataType : "json",
				error : function(jqXHR, textStatus, errorThrown) {
				},
				success : function(retData) {
					if (retData == "OK") {
						window.location.reload(true);
						return;
					} else {
						layer.alert(retData, {icon:0});
						return;
					}
				}
			});
		});
	}
	
	 laydate.render({
   		 elem: '#startTime',
   		 type:'datetime',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endTime',
   	     type:'datetime',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
</script>
</html>
