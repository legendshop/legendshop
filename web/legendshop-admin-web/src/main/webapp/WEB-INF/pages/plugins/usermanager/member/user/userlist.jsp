<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	int offset = ((Integer) request.getAttribute("offset")).intValue();
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">用户权限管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/user/query" id="form1"
				method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="1">
					<span class="item">
					用户名：<input class="${inputclass}" type="text" name="name" maxlength="50" value="${bean.name }" />
					</span>
					<span class="item">
					状态：<select id="enabled" name="enabled" class="criteria-select">
						<ls:optionGroup type="select" required="false" cache="true" beanName="ENABLED" selectedValue="${bean.enabled}" />
					</select> 
					</span>
					<span class="item">
					手机号码：<input class="${inputclass}" type="text" name="userMobile" maxlength="50" value="${bean.userMobile }" />
					<input class="${btnclass}" type="submit" value="搜索" />
					<!-- <input type="button" value="创建用户" onclick='window.location="<ls:url address='/admin/member/user/load'/>"'/> -->
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list"
					requestURI="${contextPath}/admin/member/user/query" id="item"
					export="false" class="${tableclass}" style="width:100%">
					<display:column title="
						<span>
				           <label class='checkbox-wrapper'>
								<span class='checkbox-item'>
									<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
									<span class='checkbox-inner' style='margin-left:10px;'></span>
								</span>
						   </label>	
						</span>" style="width: 80px;">
						<label class="checkbox-wrapper <c:if test='${item.enabled != 1}'>checkbox-wrapper-checked</c:if>">
							<span class="checkbox-item">
								<input type="checkbox" value="${item.id}" can-offline="${item.enabled == 1}" class="checkbox-input selectOne"
								 <c:if test="${item.enabled != 1}">disabled=true</c:if> onclick="selectOne(this);"/>
								<span class="checkbox-inner" style="margin-left:10px;"></span>
							</span>
					   </label>	
					</display:column>
					<%-- <display:column title="顺序" class="orderwidth"><%=offset++%></display:column> --%>
					<display:column title="用户名 " property="name" sortable="true" style="width:20%;"></display:column>
					<%--   <display:column title="用户手机号 " property="name" sortable="true"></display:column> --%>
					<display:column title="状态">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="ENABLED" selectedValue="${item.enabled}" defaultDisp="" />
					</display:column>
					<display:column title="手机号码" property="userMobile"></display:column>
					<display:column title="用户角色">
						<a href="<ls:url address='/admin/member/user/roles/${item.id}?appNo=FRONT_END'/>">用户角色</a>
					</display:column>
					<display:column title="用户权限">
						<a href="<ls:url address='/admin/member/user/functions/${item.id}?appNo=FRONT_END'/>">用户权限</a>
					</display:column>
					<display:column title="修改密码">
						<a href="<ls:url address='/admin/member/user/update/${item.id}?appNo=FRONT_END'/>">修改密码</a>
					</display:column>
					<display:column title="状态" style="width:150px;">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.enabled == 1}">
									<button class="tab-btn" onclick="changeUserStatus('${item.id}', 0);">下线</button>
								</c:when>
								<c:otherwise>
									<button class="tab-btn" onclick="changeUserStatus('${item.id}', 1);">上线</button>
								</c:otherwise>
							</c:choose>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fl">
			       		<input type="button" value="批量下线" id="batch-offline" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<c:if test="${not empty toolBar}">
					<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
				</c:if>
			</div>
			<div align="center"></div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	$("#checkedAll").on(
			"click",
			function() {
				if ($(this).is(":checked")) {
					$("input[type=checkbox][name=userId][can-offline=true]")
							.prop("checked", true);
				} else {
					$("input[type=checkbox][name=userId][can-offline=true]")
							.removeAttr("checked");
				}
			});

	$("#batch-offline").on("click",function() {
		var userIds = $(".selectOne:checked");
		var userIdList = new Array();
		userIds.each(function() {
			var userId = $(this).val();
			userIdList.push(userId);
		});
		var userIdStr = userIdList.toString();
		if (userIdStr == "") {
			layer.alert("请选择要下线的用户!",{icon: 0});
			return false;
		}

		$.ajax({
				url : "${contextPath}/admin/system/userDetail/batchOfflineUser/"
						+ userIdStr,
				type : "PUT",
				dataType : "JSON",
				async : true,
				success : function(result, status, xhr) {
					if (result == "OK") {
						layer.alert('批量下线用户成功。',{icon: 1}, function(index){
							window.location.reload(true);
					  });     
					} else {
						layer.alert(result,{icon:2});
				}
			}
		});
	});

	function changeUserStatus(userId, status) {
		if (status == 0) {
			layer.confirm("您确定要下线该用户?<p style='margin-top:5px;font-size:13px;color:#333;'>PS:如果该用户有商城,那么用户的商城也会自动下线!</p>", {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   sendReq(userId, status, "恭喜您,下线成功!");
			   });
		} else {
			layer.confirm("您确定要上线该用户?", {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   sendReq(userId,status,"恭喜您,上线成功!<p style='margin-top:5px;font-size:13px;color:#333;'>如果该用户有商城,需要去店铺管理手动上线商城,否则商城不会上线!</p>");
			   });
		}
	}

	function sendReq(userId, status, msg) {
		$.ajax({
			url : "${contextPath}/admin/system/userDetail/changeStatus/"
					+ userId + "/" + status,
			type : "POST",
			dataType : "JSON",
			async : true,
			success : function(result, status, xhr) {
				if (result == "OK") {
					layer.alert(msg,{icon: 1}, function(index){
						window.location.reload(true);
				  });    
				} else {
					layer.alert(result,{icon: 2});
				}
			}
		});
	}
</script>
</html>	
