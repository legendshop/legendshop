<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery1.9.1.jsp"%>
<html>
  <head>
  <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
<style type="text/css">
.a-upload {
	position: absolute;
    position: relative;
    display: inline-block;
    background: #FFF0F5;
    border: 1px solid #99D3F5;
    border-radius: 4px;
    padding: 4px 12px;
    overflow: hidden;
    color: #1E88C7;
    text-decoration: none;
    text-indent: 0;
    line-height: 20px;
}
.mybtn{
	background-color: #e5004f;
    border: 0 none;
    border-radius: 2px;
    color: #fff;
    cursor: pointer;
    display: block;

    margin-top: 10px;
    padding: 7px 0;
    text-align: center;
    width: 96px;
}
.mybutton{
	color: #ffffff;
    border: 1px solid #0e90d2;
    background-color: #0e90d2;
    border-color: #0e90d2;
    height: 26px;
    margin: 3px;
    padding-left: 12px;
    padding-right: 12px;
    text-align: center;
}
</style>
</head>
<body>
<form:form id="uploadForm" action="${contextPath}" enctype="multipart/form-data"  name="uploadForm">
<div align="center">
	<span >请选择excel文件</span><br/><br/>
	<input type="file" name="file" id="file" style="background: #ffffff;" class="a-upload"><br/><br/><br/>
	<input type="button" class="mybutton" id="but" value="返回" onclick="goback();"/> 
	<input type="button"  class="mybutton" id="but" value="确定" onclick="confirmUpload();"/> 
</div>
<div id="failImport"></div>
</form:form>
  
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script>
	function confirmUpload(){
		var path=$("#file").val();
		var filename=path.split(".")[1];
		if(path.length>0){
			if(filename=="xls"||filename=="xlsx"){
				var url="${contextPath}/admin/coin/uploadExecl";
				ajaxSubmitForm(url);
			}else{
				layer.msg("请选择excel文件",{icon:0});
			}
		}else{
			layer.msg("请选择文件",{icon:0});
		}
	}
  			
	function ajaxSubmitForm(url){
		$("#uploadForm").attr("action",url);
		$("#uploadForm").ajaxForm().ajaxSubmit({
			   dataType:"json",
			   success:function(result) {
			   		var isSuccess = result.isSuccess;
			        if(isSuccess == "1"){              	 	
		            	 layer.msg("导入成功！",{icon:1});
		            	 var failImport_tab = $("#failImport");
		            	 failImport_tab.html("<span><p>"+result.message+"</p></span>");
		            	 failImport_tab.append("<span><p>充值成功的手机号码个数："+result.successNumber+"&nbsp;总金额："+result.sum+"</p></span>");
		            }else if(isSuccess == "0"){
		            	 layer.msg("部分导入失败！",{icon:0});
		            	 var failImport_tab = $("#failImport");
		            	 failImport_tab.html("<span><p>"+result.message+"</p></span>");
		            	 failImport_tab.append("<span><p>充值成功的手机号码个数："+result.successNumber+"&nbsp;总金额："+result.sum+"</p></span>");
		            	 failImport_tab 
		            	 .append("<span><p>充值失败的手机号码个数："+result.failNumber+"</p></span>");
		            }else{
		            	 layer.msg("导入失败！",{icon:2});
		            	 var failImport_tab = $("#failImport");
		            	 failImport_tab.html("<span><p>"+result.message+"</p></span>");
		            }
			   }
	     });
	}
	
	//返回
	function goback(){
		layer.closeAll();
		window.location.href="${contextPath}/admin/coin/rechargeAdmin";
	}
	
  </script>
  </body>
  
</html>
