<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>热门商品编辑 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
   	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" /> 
</head>
   <body>
		<jsp:include page="/admin/top" />
			<div class="am-cf admin-main">
				 <!-- sidebar start -->
			    <jsp:include page="../frame/left.jsp"></jsp:include>
		 		  <!-- sidebar end -->
		   	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/hotsearch/save" method="post" id="form1">
            <input id="id" name="id" value="${bean.id}" type="hidden">
            <div align="center">
       <table class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr>
	    		<th class="no-bg title-th">
	    			<span class="title-span">PC首页装修   ＞  <a href="<ls:url address="/admin/hotsearch/query"/>">热门商品管理</a>  ＞
                		<span style="color:#0e90d2;">创建热门商品</span>
                	</span>
	    		</th>
	    	</tr>
	    </thead>
	    </table>
            <table  align="center" class="${tableclass} no-border content-table" id="col1" style="width: 100%">
      <tr>
        <td width="200px;">
          <div align="right"><font color="ff0000">*</font>商品标题：</div>
       </td>
        <td align="left" style="padding-left: 2px;">
           <input type="text" style=" width: 300px;" name="title" id="title" value="${bean.title}" size="50"/>
        </td>
      </tr>
     <tr>
        <td>
          <div align="right"><font color="ff0000">*</font>查询内容(用于全文检索)：</div>
       </td>
        <td align="left" style="padding-left: 2px;">
           <input type="text" style=" width: 300px;" name="msg" id="msg" value="${bean.msg}" size="50"/>
        </td>
      </tr>
       <%--<tr>
        <td>
          <div align="right">商品类型</div>
       </td>
        <td>
	            <lb:sortOption id="sort"   type="P"  selectedValue="${bean.sort}"/>
        </td>
      </tr>
      --%><tr>
        <td>
          <div align="right"><font color="ff0000">*</font>状态：</div>
       </td>
        <td align="left" style="padding-left: 2px;">
				<select id="status" name="status">
				  <ls:optionGroup type="select" required="true" cache="true"
	                beanName="ONOFF_STATUS" selectedValue="${bean.status}"/>
	            </select>
        </td>
      </tr>
      <tr>
        <td>
          <div align="right">次序：</div>
       </td>
        <td align="left" style="padding-left: 2px;">
           <input type="text" name="seq" id="seq" value="${bean.seq}" maxlength="5" size="5"/>
        </td>
      </tr>
       <tr>
       				<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            
                            <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='<ls:url address='/admin/hotsearch/query'/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
        <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
		<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
        <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
        <script language="javascript">
        jQuery.validator.setDefaults({});

        jQuery(document).ready(function() {
        jQuery("#form1").validate({
            rules: {
        	    title: {
                    required: true,
                    maxlength: 50
                },
                seq: {
                  number: true,
                  isNumber: true
                },
                msg: {
                	required: true,
                    maxlength: 255
                }
            },
            messages: {
            	title: {
                    required: "请输入标题",
                    maxlength: "最大长度为50"
                },
                seq: {
                    number: "请输入数字",
                    isNumber: "请输入5位之内的正整数!"
                },
                msg: {
                    required: "请输入链接地址",
                    maxlength: "最大长度为255"
                },
            }
        });
     			highlightTableRows("col1");
              //斑马条纹
         	   $("#col1 tr:nth-child(even)").addClass("even");	

    });
        
        jQuery.validator.addMethod("isNumber", function(value, element) {
            return this.optional(element) || /^[1-9]\d{0,4}$/.test(value);
        }, "必须整数");
    </script>
</html>
