<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
  Integer offset = (Integer) request.getAttribute("offset");
			String groupId = (String) request.getAttribute("groupId");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.order_img_name {
	text-overflow: -o-ellipsis-lastline;
	overflow: hidden;
	text-overflow: ellipsis;
	display: -webkit-box;
	-webkit-line-clamp: 2;
	line-clamp: 2;
	-webkit-box-orient: vertical;
	max-height: 36px;
	line-height: 18px;
	word-break: break-all;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">系统配置&nbsp;＞&nbsp;配置管理&nbsp;＞&nbsp;<span
						style="color: #0e90d2; font-size: 14px;"> <c:choose>
								<c:when test="${groupId eq 'SYS'}">系统配置</c:when>
								<c:when test="${groupId eq 'SHOP'}">商城配置</c:when>
								<c:when test="${groupId eq 'MAIL'}">邮件配置</c:when>
								<c:when test="${groupId eq 'LOGIN'}">登录配置</c:when>
								<c:when test="${groupId eq 'LOG'}">日志配置</c:when>
								<c:when test="${groupId eq 'SMS'}">短信配置</c:when>
							</c:choose>
					</span>
					</th>
				</tr>
			</table>

			<c:if test="${groupId eq 'ln' }">
				<!-- tab -->
				<div>
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li><i></i><a
								href="<ls:url address='/admin/system/systemParameter/query/ln_model'/>">登录方式配置</a></li>
							<li class="am-active"><i></i><a
								href="<ls:url address='/admin/system/systemParameter/query/ln'/>">登录参数配置</a></li>
						</ul>
					</div>
				</div>
			</c:if>

			<!-- 搜索 -->
			<form:form action="${contextPath}/admin/system/systemParameter/query"
				id="form1" method="get">
				<input type="hidden" id="curPageNO" name="curPageNO"
					value="${curPageNO}" />
				<div class="criteria-div">
					<span class="item"> <select id="groupId" name="groupId">
							<option value="">所有配置</option>
							<option value="SYS"
                <c:if test="${groupId eq 'SYS'}">selected="selected"</c:if>>系统配置</option>
							<option value="SHOP"
								<c:if test="${groupId eq 'SHOP'}">selected="selected"</c:if>>商城配置</option>
							<option value="LOGIN"
								<c:if test="${groupId eq 'LOGIN'}">selected="selected"</c:if>>登录配置</option>
							<option value="SMS"
								<c:if test="${groupId eq 'SMS'}">selected="selected"</c:if>>短信配置</option>
							<option value="MAIL"
								<c:if test="${groupId eq 'MAIL'}">selected="selected"</c:if>>邮件配置</option>
							<option value="LOG"
								<c:if test="${groupId eq 'LOG'}">selected="selected"</c:if>>日志配置</option>
					</select> <input type="text" name="name" id="name" maxlength="50" size="50" value="${name}" class="${inputclass}" placeholder="参数名称" /> <input
						type="button" onclick="search()" value="搜索" class="${btnclass}" />
					</span>
				</div>
			</form:form>

			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list"
					requestURI="/admin/system/systemParameter/query" id="item"
					export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="所属配置">
						<c:choose>
						  <c:when test="${item.groupId eq 'SYS'}">系统配置</c:when>
							<c:when test="${item.groupId eq 'SHOP'}">商城配置</c:when>
							<c:when test="${item.groupId eq 'LOGIN'}">登录配置</c:when>
							<c:when test="${item.groupId eq 'SMS'}">短信配置</c:when>
							<c:when test="${item.groupId eq 'MAIL'}">邮件配置</c:when>
							<c:when test="${item.groupId eq 'LOG'}">日志配置</c:when>
						</c:choose>

					</display:column>
					<display:column title="功能说明" property="memo" style="min-width:180px"></display:column>
					<%-- <display:column title="参数名称" property="name"></display:column> --%>
					<display:column title="参数值">
						<c:choose>
							<c:when test="${item.type == 'Selection'}">
								<ls:optionGroup type="label" required="true" cache="true" beanName="${item.optional}" selectedValue="${item.value}" />
							</c:when>
							<c:when test="${item.type == 'Password'}">
								<c:if test="${not empty item.value}">[protected]</c:if>
							</c:when>
							<c:otherwise>
								<div class="order_img_name">${item.value}</div>
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html" style="width: 90px;">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/system/systemParameter/load/${item.name}'">编辑</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
					<div class="fr">
						<div class="page">
							<div class="p-wrap">
								<ls:page pageSize="${pageSize }" total="${total}"
									curPageNO="${curPageNO }" type="simple" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	highlightTableRows("item");
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>

