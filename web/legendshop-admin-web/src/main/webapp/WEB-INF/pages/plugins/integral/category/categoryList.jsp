<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link type="text/css" rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>">
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
<style type="text/css">
.cats_set {
	margin-left: 10px;
	padding: 0;
	width: 20%;
	float: left;
	font-size: 12px !important;
}

.cats_set #catContent {
	margin-top: 20px;
}

.am-table{
	margin-bottom:20px;
}

.detail_set {
	padding: 0;
	width: 72%;
	float: left;
	height: 90%;
	font-size: 12px !important;
}

.detail_set_title {
	margin: auto;
	text-align: center;
	font-weight: bold;
	font-size: 14px;
}

.cat_set_title {
	margin: auto;
	text-align: left;
}

.detail_div {
	float: left;
	width: 100%;
	margin: auto;
}

.detail_div table {
	width: 100%;
	margin: auto;
	margin-top: 20px;
	border-color: #ccc;
	BORDER-COLLAPSE: collapse;
}

.detail_div table tr {
	height: 35px;
}

.detail_div table tr td {
	text-align: left;
	vertical-align: middle;
	padding: 5px;
}

.detail_div table tr td:first-child {
	text-align: right;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

			<form:form action="${contextPath}/admin/category/query" id="form1"
				method="post">
				<table class="am-table am-table-hover table no-border" style="width: 100%">
					<thead>
						<tr>
							<th class="no-bg title-th">
								<span class="title-span">
									积分商城  ＞  
									 <span style="color:#0e90d2;">积分商品分类</span>
								</span>
					        </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="border-bottom: 1px solid #ddd !important;">
								<div align="left" style="padding: 3px">
									<input type="hidden" id="curPageNO" name="curPageNO"
										value="${curPageNO}" /> <input type="button" value="创建类目"
										class="${btnclass}" onclick='editCategory();' /> <input
										type="button" value="更新类目缓存" class="${btnclass}"
										onclick="cleanCategoryCache();" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>

			<div class="cats_set">
				<div class="cat_set_title">
					[选择分类]&nbsp; <a style="font-weight: bold;font-size: 12px;"
						href="javascript:expandAll();void(0);">展开+</a>&nbsp; <a
						style="font-weight: bold;font-size: 12px;" href="javascript:zhedie();void(0);">折叠-</a>
				</div>
				<div id="catContent">
					<ul id="catTree" class="ztree"></ul>
				</div>
			</div>

			<div class="detail_set" id="catDetail" style="display: none;">
				<div class="detail_set_title">类目详情</div>
				<div align="center" class="detail_div">
					<form:form action="${contextPath}/admin/integral/category/save" method="post" id="form1" onsubmit="return checkCatEdit()" enctype="multipart/form-data">
						<input type="hidden" name="id" id="catid">
						<table cellspacing="0" cellpadding="0" class="${tableclass } no-border"
							style="position: relative;">
							<tr>
								<td width="30%"><font color="ff0000">*</font>名称:</td> 
								<td width="70%"><input class="${inputclass}" name="name" id="catname" maxlength="20"></td>
							</tr>
							<tr>
								<td>类目图片:</td>
								<td><span id="imgspan" style="float: left;"></span> 
								<input class="catPicFile" style="float: left; margin-top: 5px;" type="file" name="catPicFile"></td>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>父类:</td>
								<td><span id="catparent"></span>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>排序:</td>
								<td><input class="${inputclass}" name="seq" id="catseq"
									onkeyup="this.value=this.value.replace(/\D/g,'')"
									onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="5"></td>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>状态:</td>
								<td><select name="status" id="catsts"
									class="${selectclass}">
										<option value="1">上线</option>
										<option value="0">下线</option>
								</select></td>
							</tr>
							<tr>
								<td style="vertical-align: top;">SEO关键字:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="keyword" id="catkey" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td style="vertical-align: top;">SEO描述:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="catDesc" id="catdesc" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td style="vertical-align: top;">SEO标题:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="title" id="cattitle" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: center;">
									<div style="margin-top: 10px;">
										<input class="${btnclass}" type="submit" value="保存修改"> 
										<input
											class="${btnclass}" type="button" value="删除类目"
											onclick="deleteCategory();">
									</div>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
			</div>

			<div class="detail_set" id="newDetail">
				<div class="detail_set_title">创建类目</div>
				<div align="center" class="detail_div">
					<form:form action="${contextPath}/admin/integral/category/save"
						method="post" id="form2" onsubmit="return checkNewCat()"
						enctype="multipart/form-data">
						<table cellspacing="0" cellpadding="0" class="${tableclass } no-border"
							style="position: relative;">
							<tr>
								<td width="30%"><font color="ff0000">*</font>名称:</td>
								<td width="70%"><input class="${inputclass}" name="name" maxlength="20"></td>
							</tr>
							<tr>
								<td>类目图片(大小55*55):</td>
								<td><input style="margin-top: 5px;" type="file" name="catPicFile"></td>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>父类:</td>
								<td>
									<input type="hidden" id="parentId" name="parentId" /> 
									<span id="parentName"></span> 
									<input type="button" value="选择" onclick="javascript:loadCategoryDialog()" />
								</td>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>排序:</td>
								<td><input class="${inputclass}" name="seq"
									onkeyup="this.value=this.value.replace(/\D/g,'')"
									onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="5"></td>
							</tr>
							<tr>
								<td><font color="ff0000">*</font>状态:</td>
								<td><select name="status" class="${selectclass}">
										<option value="1">上线</option>
										<option value="0">下线</option>
								</select></td>
							</tr>
							<%-- <tr><td>Header菜单展示</td><td>
	          		<select name="headerMenu" class="${selectclass}">
	          			<option value="false">否</option>
	          			<option value="true">是</option>
	          		</select>
	          	</td></tr>
	          	<tr><td>导航菜单中显示</td><td>
	          		<select name="navigationMenu" class="${selectclass}">
	          			<option value="false">否</option>
	          			<option value="true">是</option>
	          		</select>
	          	</td></tr> --%>
							<%-- 	<tr><td>类型选择</td><td>
	          		<div class="J_spu-property" style="margin: 0px;">
						<ul class="J_ul-single ul-select" style="padding-left: 2px; margin-top: 3px; margin-bottom: 3px;">
							<li>
								<div class="kui-combobox" role="combobox">
									<div class="kui-dropdown-trigger">
										<input readonly="true" style="width:160px; " class="kui-combobox-caption" >
												<div class="kui-icon-dropdown">	</div>
												<div id="kui-list" class="kui-list kui-listbox"  style="display: none; <c:if test='${fn:length(typeList) > 30}'>overflow-y:auto; overflow-x:hidden;  width:168px; height: 400px</c:if>" >
													<div role="region" class="kui-header">
													    <input type="text" class="kui-dropdown-search" autocomplete="off"   onkeypress="if(event.keyCode==13) {searchType(this);return false;}"/>
													 </div>
													<div class="kui-body" style="width: 150px;">
															<div  class="kui-list kui-group">
															       <div data-value=""  class="kui-option" title=""></div>
																   <c:forEach items="${typeList }" var="type" >
																        <div  class="kui-option" title="${type.name }"  data-value="${type.id }">${type.name }</div>
																   </c:forEach>
														   </div>
													</div>
												</div>
										
									</div>
								</div> 
									<select data-transtype="combox" class="keyPropClass" id="newTypeId" name="typeId" style="display: none; visibility: hidden;">
										<option selected="selected" value=""></option>
									</select>
							</li>
						</ul> </span> 
				</div>
	          	</td></tr> --%>
							<tr>
								<td style="vertical-align: top;">SEO关键字:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="keyword" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td style="vertical-align: top;">SEO描述:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="catDesc" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td style="vertical-align: top;">SEO标题:</td>
								<td><textarea class="${inputclass}" rows="3" cols="50"
										name="title" maxlength="50"></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td align="left" style="padding-left: 6px;">
									<div>
										<input class="${btnclass}" type="submit" value="创建类目">
									</div>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	        $(document).ready(function(){
	        	var setting = { check:{}, data:{}, callback:{},view:{} };
	    		setting.check.enable = true;
	    		setting.check.chkboxType = {'Y':'ps','N':'ps'};
	    		setting.check.chkStyle = 'radio';
	    		setting.check.radioType = 'all';
	    		setting.check.enable = false;
	    		setting.view.dblClickExpand = false;
	    		setting.callback.onClick = clickNode;
	    		setting.data.simpleData =  { enable: true };
	    		
	    		$("input[type=file]").change(function(){
	        		checkImgType(this);
	        		checkImgSize(this,512);
	        	});
	        	
	    		$.fn.zTree.init($("#catTree"), setting, ${catTrees});
	    		  
	    		//expandAll();
	        	$(".J_spu-property").divselect();
	        });
	        
			function deleteCategory() {
				var catname = $("#catname").val();
				var catid= $("#catid").val();
				layer.confirm("确定删除类目："+catname+" 吗？", {icon:3}, function(){
					var zTree = $.fn.zTree.getZTreeObj("catTree");
					//var node = zTree.getNodeById(catid);
					var nodes = zTree.getSelectedNodes();
					if(nodes[0].isParent){
						layer.msg("请先删除子分类", {icon:0});
					}else{
						 window.location = "<ls:url address='/admin/integral/category/delete/" + catid + "'/>";
					}
				});
			}
			
	        function pager(curPageNO){
	            document.getElementById("curPageNO").value=curPageNO;
	            document.getElementById("form1").submit();
	        }
	        
	        function zhedie(){
	    		var treeObj = $.fn.zTree.getZTreeObj("catTree");
	    		treeObj.expandAll(false);
	    	}
	    	
	    	function expandAll() {
	    		var treeObj = $.fn.zTree.getZTreeObj("catTree");
	    		treeObj.expandAll(true);
	    	}
	    	
	    	function clickNode(event, treeId, treeNode) {
	        	var catId = treeNode.id;
	        	if(catId==0){
    		  		$("#catDetail").hide();
    		  		$("#newDetail").show();
	        	}else{
		        	$.ajax({
		    			url:"${contextPath}/admin/category/load/"+catId, 
		    			type:'get', 
		    			dataType : 'json', 
		    			async : true, //默认为true 异步   
		    			error: function(jqXHR, textStatus, errorThrown) {
		    				layer.alert("err", {icon:2});
		    			},
		    			success:function(retData){
		    				$("#catid").val(retData.id);
		    		  		$("#catname").val(retData.name);
		    		  		//$("#catpic").val(retData.pic);
		    		  		if(retData.pic=="" || retData.pic=="null" ||retData.pic==null){
		    		  			$("#imgspan").html("");
		    		  		}else{
			    		  		$("#imgspan").html("<img src='"+"<ls:photo item='"+retData.pic+"'/>"+"' width='30' height='30' style='margin-right:10px;'>");
		    		  		}
		    		  		$("#catparent").html(retData.parentName);
		    		  		$("#catseq").val(retData.seq);
		    		  		$("#catsts").val(retData.status);
		    		  		$("#cat").val(retData.name);
		    		  		$("#catheader").val(retData.headerMenu+"");
		    		  		$("#catnavi").val(retData.navigationMenu+"");
		    		  		$("#catkey").val(retData.keyword);
		    		  		$("#catdesc").val(retData.catDesc);
		    		  		$("#cattitle").val(retData.title);
		    		  		if(retData.typeId!=null){
			    		  		$(".kui-combobox-caption").val(retData.typeName);
			    		  		$("#typeId").html("<option selected='selected' value='"+retData.typeId+"'></option>");
		    		  		}else{
		    		  			$(".kui-combobox-caption").val("");
			    		  		$("#typeId").html("<option selected='selected' value=''></option>");
		    		  		}
		    		  		$("#catDetail").show();
		    		  		$("#newDetail").hide();
		    			}
		    		});
	        	}
	    	}
	    	
	    	function editCategory(){
	    		$(".kui-combobox-caption").val("");
		  		$("#newTypeId").html("<option selected='selected' value=''></option>");
		  		$("#catDetail").hide();
		  		$("#newDetail").show();
	    	}
	    	
	    		
	    	function cleanCategoryCache(){
	    		layer.confirm("确定清除前台缓存分类树？", {icon:3}, function(){
	    		       $.ajax({
	    				url:"${contextPath}/admin/integral/cleanCategoryCache", 
	    				type:'post', 
	    				async : true, //默认为true 异步   
	    				 error: function(jqXHR, textStatus, errorThrown) {
	    				 },
	    				 success:function(result){
	    					 layer.msg("清除成功", {icon:1});
	    				  }
	    			    });
	    		});
	    	}
	    	
	    	//加载分类用于选择
	    	function loadCategoryDialog(){
	    		layer.open({
	    			title:"选择父类", 
	    			id:"RoleMenu", 
	    			type: 2,
	    			content: "${contextPath}/admin/integral/category/loadCategory",
	    			area: ['280px', '410px']
	    		});
	    	}
	    	
	    	function loadCategoryCallBack(id,catName){
				$("#parentId").val(id);
				$("#parentName").html("<b>" + catName + "</b>");
			}
	    	
	    	function checkNewCat(){
	    		$("#newDetail input[name=name]").val($.trim($("#newDetail input[name=name]").val()));
	    		var flag = false;
	    		if($.trim($("#newDetail input[name=name]").val())==""){
	    			layer.msg("请输入类目名称", {icon:0});
	    		}else if($("#newDetail input[name=name]").val().length>10){
	    			layer.msg("类目名称不能超过10个字", {icon:0});
	    		}else if($("#parentId").val()==""){
	    			layer.msg("请选择父类", {icon:0});
	    		}else if($.trim($("#newDetail input[name=seq]").val())==""){
	    			layer.msg("请输入排序", {icon:0});
	    		}else{
	    			flag = true; 
	    		}
	    		return flag;
	    	}
	    	
	    	function checkCatEdit(){
	    		$("#catname").val($.trim($("#catname").val()));
	    		var flag = false;
	    		if($.trim($("#catname").val())==""){
	    			layer.msg("请输入类目名称", {icon:0});
	    		}else if($("#catname").val().length>10){
	    			layer.msg("类目名称不能超过10个字", {icon:0});
	    		}else if($.trim($("#catseq").val())==""){
	    			layer.msg("请输入排序", {icon:0});
	    		}else{
	    			flag = true; 
	    		}
	    		return flag;
	    	}
	    	
		</script>
</html>
