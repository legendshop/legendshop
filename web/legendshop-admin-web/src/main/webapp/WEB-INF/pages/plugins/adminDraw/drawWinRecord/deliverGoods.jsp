<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/processOrder.css?1'/>" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<title>发货</title>
</head>
<body>
		<div style="position: absolute;  width: 410px;">
			<div class="white_box" style="margin-left:15px;">
				<%--<h1 style="cursor: move;">确认发货</h1>
					--%><table width="100%" cellspacing="0" cellpadding="0" border="1"
						class="box_table no-border" style="float:left;margin-top:18px;">
						<tbody>
							<tr>
								<td valign="top" align="center" colspan="2" style="font-weight: bold;">请输入您发货的物流信息</td>
								 <input type="hidden" size="30" id="id" name="id" value="${id}">
							</tr>
							<tr>
								<td align="right">物流公司：</td>
								<td align="left">
									<select id="courierId" name="courierId" style="width:216px;">
									       <c:forEach items="${deliveryTypes}" var="dt">
	                                           <option value="${dt.dvyTypeId}" company="${dt.name}" >${dt.name}</option>
									       </c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td align="right">物流单号：</td>
								<td align="left">
								    <input type="text" value="" size="30" id="courierNumber" name="courierNumber" maxlength="35">
								</td>
							</tr>
			
							<tr>
								<td align="center" colspan="2">
								<span class="inputbtn" style="margin-left: 10px;">
									      <input type="button" onclick="fahuo();" class="white_btn" style="cursor:pointer;" value="发货" > 
								</span>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	<script type="text/javascript">	
	//发货
	function fahuo(){
	     var dvyFlowId=$.trim($("#courierNumber").val());
	     if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
	        layer.msg("请输入物流单号", {icon:0});
	        return ;
	     }
	       
	     var courierId=$("#courierId").find("option:selected").val();
	     var courierCompany=$("#courierId").find("option:selected").attr("company");
	     if(courierId==undefined || courierId==null || courierId==""){
	    	 layer.msg("请选择物流公司", {icon:0});
	        return ;
	     }
	     
	    $.post("${contextPath}/admin/drawWinRecord/saveDeliverGoods" , 
			 {"id": $("#id").val(), "courierNumber": $("#courierNumber").val(),"courierId":$("#courierId").val(),"courierCompany":courierCompany},
		        function(retData) {
			       if(retData == 'OK' ){
			    	   parent.location.reload();
			    	   var index = parent.layer.getFrameIndex('deliverGoods'); //先得到当前iframe层的索引
			    	   parent.layer.close(index); //再执行关闭
			       }else if(retData == 'sended'){
			           layer.msg('该奖品已发货，请刷新检查！ ', {icon:0}) ;
			       }else if(retData == 'fail'){
			    	   layer.msg('发货失败 ', {icon:2}) ;
			       }else{
			    	   layer.msg(retData, {icon:0});
			       }
		 },'json');
	}
	
	</script>
</body>
</html>