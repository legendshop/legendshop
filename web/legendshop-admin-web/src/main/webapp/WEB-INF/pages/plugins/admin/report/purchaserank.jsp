<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>报表管理 - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon-precomposed"  href="${contextPath}/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
</head>

<body>
<jsp:include page="/admin/top"/>
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content"
         style="overflow-x: auto;">
          <table class="${tableclass} title-border" style="width: 100%">
              <tr>
                  <th class="title-border">报表管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">买家购买排名</span></th>
              </tr>
          </table>
        <form:form action="${contextPath}/admin/report/purchaserank"
                   id="form1" method="post">
            <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
            <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
            <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                <div class="criteria-div">
                	<span class="item">
                    	 买家账号：<input type="text" name="userName" id="userName" maxlength="50" value="${purchaserankDto.userName}" class="${inputclass}" placeholder="买家账号"/> 
                    </span>
                    <span class="item">
                    	手机号码：<input type="text" name="userMobile" id="userMobile" maxlength="50" value="${purchaserankDto.userMobile}" class="${inputclass}" placeholder="手机号码"/> 
                    </span>
                    <span class="item">
                    	开始时间：<input readonly="readonly" name="startDate" id="startDate" class="Wdate" type="text" value='<fmt:formatDate value="${purchaserankDto.startDate}" pattern="yyyy-MM-dd"/>' placeholder="开始时间"/>
                    </span>
                    <span class="item">
                   		结束时间：<input readonly="readonly" name="endDate" id="endDate" class="Wdate" type="text" value='<fmt:formatDate value="${purchaserankDto.endDate}" pattern="yyyy-MM-dd"/>' placeholder="结束时间"/>
                   	</span>
                    <span class="item">
                                                                          按店铺名查询：
                      <select id="status" name="status" onchange='btnChange(this[selectedIndex].value);' class="criteria-select"> 
                    		<ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${purchaserankDto.status}"/>
                	 </select>
                	</span>
                    <span class="item">
                    	店铺名：<input type="text" name="shopName" disabled="true" id="shopName" maxlength="50" value="${purchaserankDto.shopName}" size="20px" class="${inputclass}" placeholder="店铺名"/>
					<input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
					</span>
                </div>
        </form:form>


        <div align="center" class="order-content-list">
            <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
            <display:table name="list" requestURI="/admin/report/purchaserank" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
                <display:column title="顺序" class="orderwidth" style="width:110px"><%=offset++%>
                </display:column>
                <c:if test="${purchaserankDto.status == 1}">
                    <display:column title="店铺名" property="shopName" style="width:300px"></display:column>
                </c:if>
                <display:column title="买家账号" property="userName" style="width:150px"></display:column>
                <display:column title="手机号码" property="userMobile" style="width:150px"></display:column>
                <display:column title="购买数量" style="width:200px" property="userOeders" sortName="userOeders" sortable="true"></display:column>
                <display:column title="消费金额" style="width:100px" sortName="totalConsumption" sortable="true">
                    <fmt:formatNumber type="currency" value="${item.totalConsumption}" pattern="${CURRENCY_PATTERN}"/>
                </display:column>
                <display:column title="退款金额" style="width:100px" sortName="totalRefundAmount" sortable="true">
                    <fmt:formatNumber type="currency" value="${item.totalRefundAmount}" pattern="${CURRENCY_PATTERN}"/>
                </display:column>
                <display:column title="成交金额" style="width:100px" sortName="totalBalanceVolume" sortable="true">
                    <fmt:formatNumber type="currency" value="${item.totalBalanceVolume}" pattern="${CURRENCY_PATTERN}"/>
                </display:column>
            </display:table>
            <div class="clearfix" style="margin-bottom: 60px;">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
        </div>
    </div>
</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/purchaserank.js'/>"></script>
<script type="text/javascript">
	var purchaserankDtoStatus =  ${purchaserankDto.status};
	var index = document.getElementById("status").selectedIndex;
	var value = document.getElementById("status").options[index].value;
	if(value == 0){
		document.getElementById("shopName").disabled="true";
	}else{
		document.getElementById("shopName").disabled="";
	}
	function btnChange(values) {
		if(values == 0){
			document.getElementById("shopName").disabled="true";
		}else{
			document.getElementById("shopName").disabled="";
		}
	}
	
</script>

</html>