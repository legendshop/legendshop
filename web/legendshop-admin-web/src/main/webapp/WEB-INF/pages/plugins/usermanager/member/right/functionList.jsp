<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link type="text/css" rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>">
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">权限管理</span></th>
				</tr>
			</table>
			<form:form id="form1" action="${contextPath}/admin/member/right/query" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="<%=request.getAttribute("curPageNO")%>"> 
					<span class="item">
					名称：<input class="${inputclass}" type="text" name="name" id="name" maxlength="50" value="${bean.name }" placeholder="请输入名称"/> 
					</span>
					<span class="item">
				           应用：<select class="${selectclass}" id="appNo" name="appNo">
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="APP_NO" selectedValue="${bean.appNo}" />
							</select>
					</span>
					<span class="item">
					权限类型：<select class="${selectclass}" id="category" name="category">
									<ls:optionGroup type="select" required="false" cache="true"
										beanName="FUNCTION_CATEGORY" selectedValue="${bean.category}" />
								</select> 
					<input type="hidden" id="menuId" name="menuId" value="${bean.menuId }" />
					<!-- 
			&nbsp; 权限菜单	 
		<input type="text" id="menuName"  name="menuName"  value="${bean.menuName }"  onclick="javascript:loadMenuDialog(); return false;"/>
		 -->
					<input class="${btnclass}" type="button" id="search" value="搜索" />
					<input class="${btnclass}" type="button" value="创建权限" onclick='javascript:createFuncDialog();' />
					</span>
				</div>
			</form:form>
			<table style="width: 100%; border: 0px;">
				<tr>
					<td width="180px;" valign="top">
						<fieldset
							style="padding: 10px; border: 0px solid #000; margin-top: 0px;">
							<legend style="font-size: 12px;">
								[选择菜单]&nbsp; <a style="font-weight: bold;"
									href="javascript:expandAll();void(0);">展开+</a>&nbsp; <a
									style="font-weight: bold;" href="javascript:zhedie();void(0);">折叠-</a>
							</legend>
							<div id="menuContent" style="margin: 0;">
								<ul id="menuTree" class="ztree"></ul>
							</div>
						</fieldset>
					</td>
					<td valign="top">
						<div id="contentframe" align="center" style="width: 100%;" class="order-content-list"></div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript'
	src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script language="JavaScript" type="text/javascript">

$(document).ready(function(){
		var setting = { check:{}, data:{}, callback:{},view:{} };
		setting.check.enable = true;
		setting.check.chkboxType = {'Y':'ps','N':'ps'};
		setting.check.chkStyle = 'radio';
		setting.check.radioType = 'all';
		setting.check.enable = false;
		setting.view.dblClickExpand = false;
		setting.callback.onClick = clickNode;
		setting.data.simpleData =  { enable: true };
		
		$.fn.zTree.init($("#menuTree"), setting, ${menuList});
		
		//刷新内容
		refreshContent();
		
		$("#search").click(function(){
			$.ajax({
			"url":"${contextPath}/admin/member/right/querycontent", 
			data:  {"name":$("#name").val(), "category":$("#category").val(), "curPageNO": 1 , "appNo": $("#appNo").val() },
			type:'post', 
			dataType : 'html', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
			},
			success:function(retData){
		  		$("#contentframe").html(retData);
			}
			});
		});
	});
	
	function refreshContent(){
		$("#contentframe").load("${contextPath}/admin/member/right/querycontent?curPageNO=${curPageNO}&name=${function.name}&appNo=${function.appNo}&category=${function.category}");
	}
	
	function zhedie(){
		var treeObj = $.fn.zTree.getZTreeObj("menuTree");
		treeObj.expandAll(false);
	}
	
	function expandAll() {
		var treeObj = $.fn.zTree.getZTreeObj("menuTree");
		treeObj.expandAll(true);
	}
	
	function clickNode(event, treeId, treeNode) {
    	var menuId = treeNode.id;
    	$("#contentframe").load("${contextPath}/admin/member/right/querycontent?menuId=" + menuId);
	};
         	
    //加载菜单用于选择
	function loadMenuDialog(){
		var menuId = $("#menuId").val();
		var page = "${contextPath}/admin/member/right/loadFunctionMenu?menuId="+menuId;
    	/* var setting = { title:"关联权限菜单", id:"RoleMenu",
    				 width:280, height:350,close: function (obj){
    				 		if(obj) {
    				 			$("#menuId").val(obj.menuId);
    				 		}
		                  }}; */
		
		layer.open({
			title :"关联权限菜单",
			  type: 2, 
			  content: [page,'no'], 
			  area: ['280px', '350px']
			});
  }
  
   	//加载菜单用于选择
	function loadMenuDialog(menuId){
		var page = "${contextPath}/admin/member/right/loadFunctionMenu?menuId="+menuId;
    	layer.open({
			title :"关联权限菜单",
			  type: 2, 
			  content: [page,'no'], 
			  area: ['280px', '350px']
			});
	}
	
	 //建立权限菜单
	function createFuncDialog(){
		var page = "${contextPath}/admin/member/right/load";
		layer.open({
			title :"新建权限",
			  type: 2, 
			  id: "func",
			  content: [page,'no'], 
			  area: ['650px', '400px']
			}); 
	}
	
		 //更新权限菜单
	function updateFuncDialog(funcId){
		var page = "${contextPath}/admin/member/right/update/" + funcId;
    	layer.open({
			title :"修改权限",
			  type: 2, 
			  id: "func",
			  content: [page,'no'], 
			  area: ['650px', '400px']
			}); 
	}
	
	function loadMenuDialogByMenuId(menuId){
		var menuId = $("#menuId").val();
		return loadMenuDialog(menuId);
	}
  
	function loadMenuDialogCallBack(menuId,menuName){
			$("#menuId").val(menuId);
			$("#menuName").val(menuName);
	}
	
		//清空选项
	function calcelMenuDialogCallBack(id,menuName){
			$("#menuId").val("");
			$("#menuName").val("");
	}	
	
	 function deleteById(id) {
		 var index = layer.confirm("确定删除该权限?",{
			 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function () {
	      		 $.ajax({
			          	url:"${contextPath}/admin/member/right/delete/"+id,			       
			          	dataType:'html',
			          	type:'post',
			          	async:true,
			          	error: function(jqXHR, textStatus, errorThrown) {
						},
						success:function(retData){
					  		refreshContent();
					  		layer.close(index);
						}
			          });
		 });
			        
			        
		}
	
   function pager(curPageNO){
			document.getElementById("curPageNO").value=curPageNO;
			document.getElementById("form1").submit();
		}  
</script>

</html>

