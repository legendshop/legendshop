<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<div id="prodEdit" class="flooredit" style="width: 980px; height: 25px;line-height: 25px; z-index: 1000; display: none;">
  		 <a href="javascript:onclickProducts(${sfId})" style="color:white;">商品编辑</a>
 </div>
  <div class="pro-right-con">
<ul>
     <c:forEach items="${requestScope.subItemLimit}" var="subFloorItem"  varStatus="status">
             <li>
                    <dl>
                     <dt>
                        <a href="javascript:void(0);" title="${subFloorItem.productName}" >
                          <img alt="${subFloorItem.productName}"  src="<ls:images item='${subFloorItem.productPic}' scale='1' />">
                        </a>
                     </dt>
                     <dd class="pro-right-dd">
                        <a href="javascript:void(0);">${subFloorItem.productName}</a>
                     </dd>
                     <dd>
                        <em>¥${subFloorItem.productCash}</em>
                         <c:if test="${ not empty subFloorItem.productPrice}">
              	         	<span>¥${subFloorItem.productPrice}</span>
                         </c:if>
                     </dd>
                   </dl>
                </li>
     </c:forEach>
</ul>
</div>