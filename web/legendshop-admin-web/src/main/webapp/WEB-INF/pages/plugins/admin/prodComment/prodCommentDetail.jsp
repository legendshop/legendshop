<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>评论列表 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/css/viewBigImage.css" />
<style>
  	#col1 td{
  		height:40px;
  	}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;">
			    <thead>
			    	<tr>
			    		<th class="no-bg title-th">
			            	<span class="title-span">
								咨询管理  ＞
								<a href="<ls:url address="/admin/productcomment/query"/>">评论管理</a>
								  ＞  <span style="color:#0e90d2;">评论详情</span>
							</span>
			            </th>
			    	</tr>
			    </thead>
			</table>
			<table style="width: 100%;margin-left:30px;" class="${tableclass} no-border content-table" id="col1">
				<tbody>
					<tr>
						<td width="200px" style="text-align: center;margin-right: 50px;">
							 <div align="right" style="text-align: left; font-weight: bold;">购买信息：</div>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<div align="right">来源订单：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${bean.subNumber}</td>
					</tr>
					<tr>
						<td>
							<div align="right" >所属商家：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${bean.siteName}</td>
					</tr>
					<tr>
						<td>
							<div align="right" >商品名称：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${bean.prodName}</td>
					</tr>
					<c:if test="${not empty bean.attribute}">
						<tr>
							<td>
								<div align="right">购买属性：</div>
							</td>
							<td align="left" style="padding-left: 2px;">${bean.attribute}</td>
						</tr>
					</c:if>
					<tr>
						<td>
							<div align="right">购买用户：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${bean.userName}</td>
					</tr>
					<tr>
						<td>
							<div align="right">购买日期：</div>
						</td>
						<td align="left" style="padding-left: 2px;">
							<fmt:formatDate value="${bean.buyTime}" pattern="yyyy-MM-dd HH:mm" />
						</td>
					</tr>
				
					<!-- 初次评论信息 -->
					<tr>
						<td colspan="2" style="word-break:break-all" width="200">
							<div align="right" style="text-align: left; font-weight: bold;">初次评论：</div>
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">评论分数：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${bean.score}</td>
					</tr>
					<tr>
						<td>
							<div align="right">评论内容：</div>
						</td>
						<td style="word-wrap:break-word;word-break:break-all;" align="left" style="padding-left: 2px;">${bean.content}</td>
					</tr>
					
					<c:if test="${not empty bean.photoPaths }">
						<tr>
							<td valign="top">
								<div align="right">评论图片：</div>
							</td>
							<td align="left" style="padding-left: 2px;" valign="top">
								<c:forEach items="${bean.photoPaths }" var="photo">
									<%-- <img src="<ls:photo item='${photo}'/>" data-action="zoom"> --%>
									<div class="zoombox">
													<span class="photoBox">
														<div class="loadingBox">
															<span class="loading"></span>
														</div>
														<img src="<ls:photo item='${photo}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 60px;max-width: 198px;">
													</span>
													
													<div class="photoArea" style="display:none;">
														<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
															<p class="toolBar gc">
															<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${photo}'/>" target="_blank">查看原图</a></span>
															</p>
													</div>
										</div>
								</c:forEach>
							</td>
						</tr>
					</c:if>
					
					<tr>
						<td>
							<div align="right">评论时间：</div>
						</td>
						<td align="left" style="padding-left: 2px;">
							<fmt:formatDate value="${bean.addtime}" pattern="yyyy-MM-dd HH:mm" />
						</td>
					</tr>
					<tr>
						<td>
							<div align="right">评论状态：</div>
						</td>
						<td align="left" style="padding-left: 2px;">
							<c:choose>
								<c:when test="${bean.status eq 0}">待审核</c:when>
								<c:when test="${bean.status eq 1}">审核通过</c:when>
								<c:when test="${bean.status eq -1}">审核不通过</c:when>
								<c:otherwise>未知</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<c:if test="${bean.status eq 1}">
						<tr>
							<td>
								<div align="right">商家回复状态：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<c:choose>
									<c:when test="${bean.isReply}">已回复</c:when>
									<c:otherwise>未回复</c:otherwise>
								</c:choose>
							</td>
							<c:if test="${bean.isReply}">
								<tr>
									<td>
										<div align="right">商家回复内容：</div>
									</td>
									<td align="left" style="padding-left: 2px;">${bean.shopReplyContent}</td>
								</tr>
								<tr>
									<td>
										<div align="right">回复时间：</div>
									</td>
									<td align="left" style="padding-left: 2px;">
										<fmt:formatDate value="${bean.shopReplyTime}" pattern="yyyy-MM-dd HH:mm" />
									</td>
								</tr>
							</c:if>
						</tr>
					</c:if>
					
					<c:if test="${bean.status eq 1 and bean.isAddComm}">
						<!-- 追加评论信息 -->
						<tr>
							<td colspan="2">
								<div align="right" style="text-align: left; font-weight: bold;">追加评论：</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">评论内容：</div>
							</td>
							<td align="left" style="padding-left: 2px;" style="word-wrap:break-word;word-break:break-all; width: 1450;">${bean.addContent}</td>
						</tr>
						<c:if test="${not empty bean.addPhotoPaths }">
							<tr>
								<td>
									<div align="right">评论图片：</div>
								</td>
								<td align="left" style="padding-left: 2px;">
									<c:forEach items="${bean.addPhotoPaths }" var="photo">
										<%-- <img src="<ls:photo item='${photo}'/>" data-action="zoom" style="width: 100px;"> --%>
										<div class="zoombox">
													<span class="photoBox">
														<div class="loadingBox">
															<span class="loading"></span>
														</div>
														<img src="<ls:photo item='${photo}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 60px;max-width: 198px;">
													</span>
													
													<div class="photoArea" style="display:none;">
														<p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
															<p class="toolBar gc">
															<span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${photo}'/>" target="_blank">查看原图</a></span>
															</p>
													</div>
										</div>
									</c:forEach>
								</td>
							</tr>
						</c:if>
						<tr>
							<td>
								<div align="right">评论时间：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<fmt:formatDate value="${bean.addAddTime}" pattern="yyyy-MM-dd HH:mm" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">评论状态：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<c:choose>
									<c:when test="${bean.addStatus eq 0}">待审核</c:when>
									<c:when test="${bean.addStatus eq 1}">审核通过</c:when>
									<c:when test="${bean.addStatus eq -1}">审核不通过</c:when>
									<c:otherwise>未知</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<c:if test="${bean.addStatus eq 1}">
							<tr>
								<td>
									<div align="right">商家回复状态：</div>
								</td>
								<td align="left" style="padding-left: 2px;">
									<c:choose>
										<c:when test="${bean.addIsReply}">已回复</c:when>
										<c:otherwise>未回复</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<c:if test="${bean.addIsReply}">
								<tr>
									<td>
										<div align="right">商家回复内容：</div>
									</td>
									<td align="left" style="padding-left: 2px;">${bean.addShopReplyContent}</td>
								</tr>
								<tr>
									<td>
										<div align="right">回复时间：</div>
									</td>
									<td align="left" style="padding-left: 2px;">
										<fmt:formatDate value="${bean.addShopReplyTime}" pattern="yyyy-MM-dd HH:mm" />
									</td>
								</tr>
							</c:if>
						</c:if>
					</c:if>
					
					<tr>
						<td></td>
						<td align="left" style="padding-left: 2px;">
							<div>
								<c:choose>
									<c:when test="${bean.status eq 0}">
										<input id="audit-btn" type="button" value="审核初次评论" class="${btnclass}" commentId="${bean.id}" auditType="first"/>
									</c:when>
									<c:when test="${bean.status eq 1 and bean.isAddComm and bean.addStatus eq 0}">
										<input id="audit-btn" type="button" value="审核追加评论" class="${btnclass}" commentId="${bean.id}" auditType="append"/>
									</c:when>
								</c:choose>
								<input type="button" value="返回" class="${btnclass}" onclick="window.location.href='${contextPath}/admin/productcomment/query'" />
							</div>
						</td>
					</tr>
				</tbody>
			</table>
<div id="auditWin" style="display: none;">
<div style="padding: 10px;">
		<form:form id="auditForm">
			<table  >
				<thead>
				    <tr>
				      <td><div align="right"><font color="red">*</font>审核状态:</div></td>
				      <td>
				       	<label class="radio-wrapper" style="padding-left: 20px;">
							<span class="radio-item">
								<input type="radio" class="radio-input" name="audit" value="1" onclick="selectChild()">
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">通过</span>
						</label>
						<label class="radio-wrapper">
							<span class="radio-item">
								<input type="radio" class="radio-input" name="audit" value="-1" onclick="selectChild()">
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">不通过</span>
						</label>
				      </td>
				    </tr>
			    </thead>
			</table>
		</form:form>
	</div>
</div>
</div>
</div>
</body>
 <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
 <script type="text/javascript" src="${contextPath}/resources/templets/js/viewBigImage.js"></script>
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/prodCommentDetail.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	 function selectChild(){
		 $("input:radio[name='audit']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 };
</script>
</html>
