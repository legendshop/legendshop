<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>首页管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/headmenu/submit"
				method="post" id="form1" onsubmit="return imageCheck();"
				enctype="multipart/form-data">
				<input type="hidden" name="type" value="${type}" />
				<input type="hidden" name="id" value="${headmenu.id}" />
				<div align="center">
					<table class="${tableclass}" style="width: 100%">
						<thead>
							<tr>
								<th class="no-bg title-th">
									<span class="title-span">
										<c:if test="${type=='pc' }">PC首页装修</c:if>
	       								<c:if test="${type=='mobile'}">移动装修</c:if>  ＞
										<a href="<ls:url address="/admin/headmenu/query/pc"/>">头部菜单管理</a>
										<c:if test="${not empty headmenu }"><span style="color:#0e90d2;">  ＞  编辑</span></c:if>
										<c:if test="${empty headmenu }"><span style="color:#0e90d2;">  ＞  新增</span></c:if>
									</span>
								</th>
							</tr>
						</thead>
					</table>
					<table style="width: 100%" class="${tableclass} no-border content-table" id="col1">
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>菜单名称 ：
								</div>

							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" maxLength="100" name="name" style="width:300px;"
								id="title" value="${headmenu.name}" size="50"
								class="${inputclass}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>链接：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="url" value="${headmenu.url}" style="width:300px;"
								maxLength="100" size="50" style="width:350px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font
								color="ff0000">注意：请以 “http://” 开头，填#代表 正在开发中</font></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>是否上线：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<select id="status" name="status">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ONOFF_STATUS" selectedValue="${headmenu.status}" />
								</select>
							</td>
						</tr>
						<%--   <tr>
                    <td>
                      <div align="right">AndroidId</div>
                   </td>
                    <td>
                        <input type="text" name="androidId" value="${headmenu.androidId}" maxLength="100" size="50"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div align="right">iOSId</div>
                   </td>
                    <td>
                        <input type="text" name="iOSId" value="${headmenu.iOSId}" maxLength="100" size="50"/>
                    </td>
                  </tr> --%>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>顺序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input style="width:150px;" type="text" name="seq" maxlength="5" value="${headmenu.seq}">
							</td>
						</tr>
						<c:if test="${type eq 'mobile'}">
							<tr>
								<c:choose>
									<c:when test="${not empty headmenu}">
										<td align="right" valign="top">
											<font color="ff0000">*</font>图片：
										</td>
										<td align="left" valign="top" style="padding-left: 2px;">
											<div id="choose">
												<img src="<ls:photo item='${headmenu.pic}'/>" height="92" width=92 "/> 
												<br><input type="button" value="更改图片" class="criteria-btn" onclick="change()" id="btn">
												<!-- <a href="javascript:void(0);" onclick="change()" id="btn">更改图片</a> -->
											</div>
											<div style="display: none;" id="upload">
												<input type="file" name="imageFile" id="prodImg" onchange="changeImage()" /> 
												<input type="button" value="取消" style="margin-top:10px;margin-left:0;" class="criteria-btn" onclick="hides()" />
												<!-- <a href="javascript:void(0);" style="padding-top:10px;margin-left:0;" onclick="hides()">取消</a> -->
											</div>
											<div style="display: none;" id="previewImage">
												<img id="ImgPr" width="92" height="92" /> 
												<br><input type="button" value="更改图片" style="margin-top:10px;margin-left:0;" class="criteria-btn" onclick="change()" id="btn">
											</div>
										</td>
									</c:when>
									<c:otherwise>
										<td align="right" valign="top">
												图片<font color="ff0000">*</font>
										</td>
										<td align="left" valign="top" style="padding-left: 2px;"><input type="file" style="margin-bottom:8px;" name="imageFile" id="prodImg" />
											<div>
												<img style="display:none;" id="ImgPr" width="92" height="92" />
											</div>
										</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:if>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<input type="submit" class="${btnclass}" value="保存" /> 
									<input type="button" class="${btnclass}" value="返回"
										onclick="window.location='${contextPath}/admin/headmenu/query/${type}'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/headMenu.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</html>
