<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
    <title>属性名列表</title>
    <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<style type="text/css">
		body{
			min-height:96%;
		}
		#form1{
			margin:15px;
			line-height: 28px;
		}	
		input[type="text"]{
			height:28px;
			border: 1px solid #ddd;
		}
		.see-able{
			width: 95%;
			min-width:680px;
		}
		.see-able th,td{
			height:30px !important;
		}
	</style>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
<body>
    <form:form  action="${contextPath}/admin/prodType/parameterProperty/${proTypeId}" id="form1" method="post">
       
        <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
            参数属性名称&nbsp;<input type="text" name="propName" maxlength="50" value="${propName}" />
            别名&nbsp;<input type="text" name="memo" maxlength="50" value="${memo}" />
            <input type="submit" class="criteria-btn" value="搜索"/>
    </form:form>
        <c:choose>
    	<c:when test="${fn:length(list) > 0 }">
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/prodType/parameterProperty/${proTypeId}" id="item" export="false" class="see-able am-table am-table-striped am-table-hover table" >
			<display:column title="关联" style="width:30px;">
					<%-- <input type="checkbox" name="id"  value="${item.id}"/>	 --%>	
					<label class="checkbox-wrapper" style="float:left;margin-left:27px;">
						<span class="checkbox-item">
							<input type="checkbox" name="id"  value="${item.id}" class="checkbox-input" onclick="selectChild(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				    </label>		
			</display:column>
			
     		<display:column title="参数属性名称" property="propName" style="min-width:120px;"></display:column>
     		
     		<display:column title="别名" property="memo" style="min-width:120px;"></display:column>
     		
     		<display:column title="类型" style="min-width:40px;">
     			<ls:optionGroup type="label" required="true" cache="true" beanName="PROPERTY_TYPE" selectedValue="${item.type}"/> 
     		</display:column>


	    </display:table>
	    <div class="fr" style="display: block;width:100%;">
   			 <div class="page">
       			 <div class="p-wrap">
           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
    			 </div>
  			 </div>
		</div>
    </div>
    
    	<div align="center" style="margin-top: 70px;padding-bottom:25px;"><input type="button" value="保存并关闭" class="criteria-btn" onclick="save_form();"/></div>
    	</c:when>
    	<c:otherwise>
    			<div  style="margin: 15px;">找不到记录</div>
    	</c:otherwise>
    	</c:choose>
    	 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/parameterProperty.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var proTypeId = '${proTypeId}';
			function selectChild(obj){
		 		if(!obj.checked){
		 			$(obj).prop("checked",false);
		 			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
		 		}else{
		 			$(obj).prop("checked",true);
		 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
		 		}
			}
		</script>
</body>
</html>

