<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<div id="proppertyContent">
		<form:form  action="${contextPath}/admin/productProperty/loadPropPage/${productProperty.isRuleAttributes}" id="form1" method="post" style="margin-bottom: 0px;">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		        <div class="criteria-div">
		        	<span class="item">
		         		   名称：<input class="${inputclass}" type="text" id="propName" name="propName" maxlength="50" value="${productProperty.propName}" />
		            </span>
		            <span class="item">
		           		别名：<input class="${inputclass}" type="text" id="memo" name="memo" maxlength="50" value="${productProperty.memo}" />  
		            	<input class="${btnclass}" type="button" value="搜索" onclick="search()" />
		            </span>
		        </div>
		</form:form>

		<div align="center" style="min-width:800px;padding:0 20px;" class="order-content-list">
				<display:table name="list" requestURI="/admin/productProperty/loadPropPage" id="item" export="false" class="${tableclass}" style="min-width:800px;">
		     		<display:column title="名称" property="propName" style="max-width: 350px"></display:column>
		     		<display:column title="别名" property="memo" style="max-width: 350px;"></display:column>
			      <display:column title="操作" media="html" style="max-width: 100px;text-align:center;">
				 	<div class="table-btn-group">
						<button class="tab-btn" onclick="chooseProp('${item.propId}');">选择</button>
				 	</div>
			      </display:column>
			    </display:table>
		      <div class="clearfix">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
		</div>
</div>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("form1").submit();
	}

	function chooseProp(propId){
		self.parent.window.location="${contextPath}/admin/productProperty/similarProp/"+propId;
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1").submit();
	}
</script>