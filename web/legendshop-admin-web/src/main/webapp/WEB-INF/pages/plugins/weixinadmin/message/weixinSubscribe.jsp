<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
         <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
      <table class="${tableclass}" style="width: 100%">
	    <thead>
	    	<tr><th>
				    	<strong class="am-text-primary am-text-lg">微信扩展接口管理</strong> /  创建扩展接口
	    	</th></tr>
	    </thead>
	    </table>
        <form:form  action="${contextPath}/admin/weixinSubscribe/save" method="post" id="form1">
            <input id="id" name="id" value="${weixinSubscribe.id}" type="hidden">
            <input id="templatename" name="templatename" value="${weixinSubscribe.templatename}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                                                                   创建关注欢迎语
                            </div>
                        </th>
                    </tr>
                </thead>
		<tr>
		 <td width="30%">
		          	<div align="center">消息类型: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		        <select   style="width: 150px;" id="msgType" name="msgType">
		            <option value="">-----------------</option>
		          	<option value="text" <c:if test="${weixinSubscribe.msgType=='text'}"> selected="selected"</c:if> >文本消息</option>
		          	<option value="news" <c:if test="${weixinSubscribe.msgType=='news'}"> selected="selected"</c:if> >图文消息</option>
		       		<!-- <option value="voice">音频消息</option>
		       		<option value="video">视频消息</option> -->
		       		<option value="image" <c:if test="${weixinSubscribe.msgType=='image'}"> selected="selected"</c:if> >图片消息</option>
                </select>
		        </td>
		</tr>
	
		<tr id="text_template">
		        <td>
		          	<div align="center">消息模板: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
			       <select id="templateId" name="templateId" style="width: 150px;">
				     <option value="">---------------------</option>
				  </select> 
		        </td>
		</tr>
		
		
		<tr id="text_content" style="display: none;">
				        <td>
				          	<div align="center">消息内容:<font color="ff0000">*</font> </div>
				       	</td>
				        <td>
				          <textarea rows="2" cols="5" style="width:60%;height: 100px;" name="content" id="content" maxlength="250">${weixinSubscribe.content}</textarea>
				        </td>
		   </tr>
				
		
		
		
         <tr>
                    <td colspan="2">
                        <div align="center">
                            <input class="${btnclass}" type="submit" value="保存" />
                            
							 <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/weixinSubscribe/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    	      
       jQuery("#form1").validate({
            rules: {
            msgType: {
                required: true
            },
            templateId:{
               required: true
            },
            content:{
               required: true
            }
        },
        messages: {
            msgType: {
                required: "请选择消息类型"
            },
            templateId:{
               required: "请选择消息消息模板"
            },content:{
               required: "请输入文本内容"
            }
        }
      });
      
       var templateId = "${weixinSubscribe.templateId}";
       var msgType = "${weixinSubscribe.msgType}";
       
       	if("text"==msgType){
	           	   $("#text_content").show();
	               $("#text_template").attr("style","display:none");
	 			   $("#templateId").attr("disabled","disabled");
	      }else{
	               $("#text_template").show("style");
	 			   $("#templateId").removeAttr("disabled");
	               $("#text_content").hide();
	     }
       
       
       if(templateId!=null && templateId!="" && templateId!=undefined && msgType!="text"){
 	   		var templateObj = $("#templateId");
	 		templateObj.html("");
	 		$.ajax({
	 			url:"${contextPath}/admin/weixinMenu/getTemplates",
	 			data:{"msgType":msgType},
	 			dataType:"JSON",
	 			type:"POST",
	 			success:function(result){
	 			var msg="";
 				msg += "<option value=''>--------------------</option>";
 				var data=eval(result);
	 				for(var i=0;i<data.length;i++){
	 				    if(templateId == data[i].key){
	 				 	    msg += "<option value='"+data[i].key+"' selected='selected' >"+data[i].value+"</option>";
	 				 	}else{
	 				 	   msg += "<option value='"+data[i].key+"'  >"+data[i].value+"</option>";
	 				 	}
	 				}
 				  templateObj.html(msg);
	 			}
	 		});
	    }
	    
       
	
	  $("#msgType").change(function(){
 			   var selectValue = $(this).children('option:selected').val();
 			 	if("text"==selectValue){
	           	   $("#text_content").show();
	               $("#text_template").attr("style","display:none");
	 			   $("#templateId").attr("disabled","disabled");
	 			   $("#templatename").val("");
	           }else{
	               $("#text_template").removeAttr("style");
	 			   $("#templateId").removeAttr("disabled");
	               $("#text_content").hide();
	               $("#templatename").val($.trim($(this).children('option:selected').html()));
 			       getTemplates(selectValue);
	           }
 			 
 		});
	
 		highlightTableRows("col1");
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
    });
    
    function getTemplates(msgType){
 		var templateObj = $("#templateId");
 		templateObj.html("");
 		$.ajax({
 			url:"${contextPath}/admin/weixinMenu/getTemplates",
 			data:{"msgType":msgType},
 			dataType:"JSON",
 			type:"POST",
 			success:function(result){
 				var msg="";
 				msg += "<option value=''>--------------------</option>";
 				var data=eval(result);
 				for(var i=0;i<data.length;i++){
 					msg += "<option value='"+data[i].key+"'>"+data[i].value+"</option>";
 				}
 				templateObj.html(msg);
 			}
 		});
 	}
 	
</script>

