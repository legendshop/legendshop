<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</head>
    <body>
        <form:form  action="${contextPath}/admin/userCoupons/save" method="post" id="form1">
            <input id="cpnsUsrId" name="cpnsUsrId" value="${userCoupons.cpnsUsrId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">礼券ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="cpnsId" id="cpnsId" value="${userCoupons.cpnsId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">礼券名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="cpnsName" id="cpnsName" value="${userCoupons.cpnsName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">劵号: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="cpnsSn" id="cpnsSn" value="${userCoupons.cpnsSn}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">用户ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="usrId" id="usrId" value="${userCoupons.usrId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">用户名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userName" id="userName" value="${userCoupons.userName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">领取时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="getTime" id="getTime" value="${userCoupons.getTime}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">使用时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="useTime" id="useTime" value="${userCoupons.useTime}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">订单总金额: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="orderPrice" id="orderPrice" value="${userCoupons.orderPrice}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">订单编号: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="orderNumber" id="orderNumber" value="${userCoupons.orderNumber}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">领取来源: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="getSources" id="getSources" value="${userCoupons.getSources}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/userCoupons/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
    </body>
</html>

