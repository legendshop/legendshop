<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>规格属性 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/common/css/specProperty.css'/>">
<style type="text/css">
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
.order_img_name:hover { width:auto; }
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">

			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">属性管理&nbsp;＞&nbsp;
						<c:choose>
							<c:when test="${isRuleAttributes ==1}"><span style="color:#0e90d2; font-size:14px;">规格属性</span></c:when>
							<c:when test="${isRuleAttributes ==2 }"><span style="color:#0e90d2; font-size:14px;">参数属性</span></c:when>
						</c:choose>
					</th>
				</tr>
			</table>
			<div class="demo">
				<div id="proppertyContent">
					<div class="criteria-div">
               			<span class="item">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							名称：<input class="${inputclass}" type="text" id="propName" name="propName" maxlength="50" value="${productProperty.propName}" />
						</span>
               			<span class="item">
							别名：<input class="${inputclass}" type="text" id="memo" name="memo" maxlength="50" value="${productProperty.memo}" />
							<input class="${btnclass}" type="button" value="搜索" onclick="javascript:searchContent();" />
							<%-- <input class="${btnclass}" type="button" value="搜索" onclick="search()"/> --%>
							<input class="${btnclass}" type="button" value="创建属性" onclick='window.location="<ls:url address='/admin/productProperty/new/${productProperty.isRuleAttributes}'/>"' />
							<input class="${btnclass}" type="button" value="类似属性导入" onclick="importSimilarProp();" />
						</span>
					</div>	
					<div align="center" class="order-content-list">
						<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
						<display:table name="list" requestURI="/admin/productProperty/query" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="名称" property="propName" style="width:20%"></display:column>
							<display:column title="别名" property="memo" style="width:20%"></display:column>
							<c:if test="${item.isRuleAttributes!=2}">
							<display:column title="类型" style="width:20%">
								<ls:optionGroup type="label" required="true" cache="true" beanName="PROPERTY_TYPE" selectedValue="${item.type}" />
							</display:column>
							</c:if>
							<display:column title="值" style="width:20%">
								<div class="order_img_name"><c:forEach var="value" items="${item.valueList}">${value.name}</c:forEach></div>
							</display:column>

							<display:column title="操作" media="html" style="width:20%">
								<div class="table-btn-group">
										<button class="tab-btn" onclick="window.location='${contextPath}/admin/productProperty/load/${item.propId}'">修改</button>
										<span class="btn-line">|</span>
										<button class="tab-btn" onclick="deleteById('${item.propId}','${item.propName}','${item.isRuleAttributes}');">删除</button>
								</div>
							</display:column>
						</display:table>
						<div class="clearfix" style="margin-bottom: 60px;">
				       		<div class="fr">
				       			 <div class="page">
					       			 <div class="p-wrap">
					           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					    			 </div>
				       			 </div>
				       		</div>
			       		</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/specProPerty.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var isRuleAttributes = '${productProperty.isRuleAttributes}';
</script>
</html>
