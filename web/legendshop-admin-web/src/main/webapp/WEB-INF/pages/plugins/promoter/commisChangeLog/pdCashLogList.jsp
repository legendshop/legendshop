<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>推广管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">推广管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">历史转账</span></th>
				</tr>
			</table>
			<form:form id="ListForm" method="post" action="${contextPath}/admin/commisChangeLog/pdCashLogQuery">
				<input type="hidden" id="curPageNO" name="curPageNO" value="1" />
				<div class="criteria-div">
					<span class="item">
		           		用户名：
		           		<input type="text" class="${inputclass}" name="userName" maxlength="50" value="${pdCashLog.userName}" placeholder="请输入用户名"/>
		           	</span>
		           	<span class="item">
		           	 	转账时间：
		           		<input type="text"  value="<fmt:formatDate value="${pdCashLog.startTime}" pattern="yyyy-MM-dd" />" readonly="readonly" placeholder="转账时间" class="user_title_txt Wdate ${inputclass}" id="startTime" name="startTime">&nbsp;-
		               <input type="text" readonly="readonly" value="<fmt:formatDate value="${pdCashLog.endTime}" pattern="yyyy-MM-dd" />" id="endTime" name="endTime" placeholder="转账时间" class="user_title_txt Wdate ${inputclass}">
						<input type="submit" class="${btnclass}" value="查询" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>

				<display:table name="list" id="item" export="false"
					class="${tableclass}">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="用户名" property="userName"></display:column>
					<display:column title="转账金额">
						<fmt:formatNumber value="${item.amount}" type="currency" pattern="0.00"></fmt:formatNumber>
					</display:column>
					<display:column title="转账时间" format="{0,date,yyyy-MM-dd HH:mm:ss}" property="addTime"></display:column>
				</display:table>
				<div class="clearfix">
 		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("ListForm").submit();
	}
	
	  laydate.render({
		 elem: '#startTime',
		 calendar: true,
		 theme: 'grid',
		 trigger: 'click'
	  });
	   
	  laydate.render({
	     elem: '#endTime',
	     calendar: true,
	     theme: 'grid',
		 trigger: 'click'
      });
</script>
</html>
