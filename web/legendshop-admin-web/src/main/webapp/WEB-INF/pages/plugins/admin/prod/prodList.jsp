<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>商品列表 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
 </head>
 <style>
#prodContentList td{  
	white-space: normal;
    word-break: break-all;
}
.order_img_name {
	text-overflow: -o-ellipsis-lastline;
	overflow: hidden;
	text-overflow: ellipsis;
	display: -webkit-box;
	-webkit-line-clamp: 2;
	line-clamp: 2;
	-webkit-box-orient: vertical;
	max-height: 40px;
	line-height: 20px;
	word-break: break-all;
}
	label{
		margin-bottom: 0 !important;
	}
</style>
 <body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<table class="${tableclass} title-border" style="width: 100%;">
	    <tr>
	        <th class="title-border">商品管理 &nbsp;＞&nbsp;<span style="color:#0e90d2;">商品列表<span>
	        <%-- <a href="${contextPath}/admin/adminDoc/view/1"><img src="${contextPath}/resources/templets/img/adminDocHelp.png"/></a> --%>
	        </th>
	    </tr>
	</table>
<div>
    <ul class="am-tabs-nav am-nav am-nav-tabs">
        <li id="allProdLi" <c:if test="${empty type}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=4">所有商品</a></li>
        <li id="sellingLi" <c:if test="${type==1}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=1">出售中的商品</a></li>
        <li id="offLineLi" <c:if test="${type==0}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=0">仓库中的商品</a></li>
        <li id="illegalLi" <c:if test="${type==2}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=2">违规下架</a></li>
        <li id="auditLi" <c:if test="${type==3}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=3">待审核</a></li>
        <li id="auditLi" <c:if test="${type==-1}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=-1">被删除商品</a></li>
        <li id="shopLelete" <c:if test="${type==-2}">class="am-active"</c:if>><a href="${contextPath}/admin/product/query?status=-2">商家永久删除的商品</a></li>
    </ul>
</div>
<form:form id="form1" action="${contextPath}/admin/product/query" method="POST">
    <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
    <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
    <div class="criteria-div">
            <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
            <input type="hidden" id="status" name="status" value="${status}"/>
            <span class="item" style="margin-left:40px">
                   	 店铺：
                    <input type="text" id="shopId" name="shopId" value="${prod.shopId}"/>
                    <input type="hidden" id="siteName" name="siteName" value="${prod.siteName}" class="${inputclass}"/>
             </span>
             <span class="item">
                    	商品分类：
                    <input type="text" style="cursor: pointer;" id="prodCatName" name="categoryName" value="${prod.categoryName }" onclick="loadCategoryDialog();" class="${inputclass}" placeholder="商品分类" readonly="readonly"/>
                    <input type="hidden" id="prodCatId" name="categoryId" value="${prod.categoryId }"/>
             </span>
               	<span class="item">
                   	 商品品牌：
                    <input type="text" id="brandId" name="brandId" value="${prod.brandId}"/>
                    <input type="hidden" id="brandName" name="brandName" value="${prod.brandName}"/>
              </span>
              <span class="item">
                   	 商品名称：
                    <input type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="搜索商品名称" class="${inputclass}"/>
              </span>
              <span class="item">      
                    	商品编号：
                    	
                    <input type="text" name="prodId" id="prodId" maxlength="20" value="${prod.prodId}" size="20" placeholder="商品编号" class="${inputclass}" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
                    <input type="button" onclick="search()" class="${btnclass}" value="搜索"/>
             </span>
             <span class="item">  
		            <input type="button" class="${btnclass}" value="导出搜索数据" onclick="exprotData();"/>
		            <input type="button" value="分组管理" onclick="return groupProd();" class="${btnclass}">
             </span>
     </div>
</form:form>
<div align="center" id="prodContentList" name="prodContentList" class="order-content-list"></div>
<table class="${tableclass}" style="margin-top: 10px;">
    <tr>
        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
            1. 建立商品的4个步骤：1.商品详细信息 2.商品相关说明图片 3.动态属性 4.动态参数 <br>
            2. 商品有一个对应的品牌<br>
            3. 商品状态，在开始时间和结束时间之内有效，失效后包括下线状态将不会在前台显示，推荐状态为是则在首页精品推荐中出现<br>
            4. 价格、运费、库存量为正数，不填写则不在前台显示，如果填写了库存量为0则无法订购<br>
            5. 库存量在用户下单时会减少，实际库存量在订单成交时减少<br>
        </td>
    <tr>
</table>
</div>
</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/plugins/select2-3.5.2/select2.js"></script>
<script language="JavaScript" type="text/javascript">
	var brandName = '${prod.brandName}';
	var siteName = '${prod.siteName}';

	function batchOffline(){
		var prodList=$(".selectOne:checked");
		var prodIdArr=new Array();
		prodList.each(function(index, domEle){
			var value=$(domEle).val();
			prodIdArr.push(value);
		});
		var prodIdStr=prodIdArr.toString();
		if(prodIdStr==""){
			layer.alert("批量下线时至少选中一条记录！", {icon:2});
			return;
		}
        layer.confirm('确定要下架这些商品吗？', {icon: 3, title:'提示'},function () {
            jQuery.ajax({
                url: "${contextPath}/admin/product/batchOffline/" + prodIdStr,
                type: 'get',
                async: false, //默认为true 异步
                dataType: 'json',
                success: function (data) {
                	if(data=="OK"){
                		layer.msg("成功。", {icon:1});
                    	setInterval(function(){
                    		location.href="${contextPath}/admin/product/query";
                    	},1000);
                	}else{
                		layer.alert("删除出错。", {icon:2});
                	}
                }
            });
        }, function(index){
            layer.close(index);
        });

	}
	//分组管理
	function groupProd(){
		var prodList=$(".selectOne:checked");
		var prodIdArr=new Array();
		prodList.each(function(index, domEle){
			var value=$(domEle).val();
			prodIdArr.push(value);
		});
		var prodIdStr=prodIdArr.toString();
		if(prodIdStr==""){
			layer.alert("分组管理时至少选中一条记录！", {icon:2});
			return false;
		}
		layer.open({
			  title :"分组管理",
			  id: "groupProd",
			  type: 2, 
			  resize: false,
			  content: [contextPath+"/admin/product/groupProd?selAry="+prodIdStr,'yes'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['300px', '430px']
		}); 
	}
	
	
	
    function deleteAction() {
        //获取选择的记录集合
        selAry = $(".selectOne");
        if (!checkSelect(selAry)) {
            layer.alert('删除时至少选中一条记录！', {icon:2});
            return false;
        }
        layer.confirm("确定永久删除选中的商品吗？", {icon:3, title:'提示'}, function (index) {
            for (i = 0; i < selAry.length; i++) {
                if (selAry[i].checked) {
                    var prodId = selAry[i].value;
                    realDelOne(prodId);
                }
            }
            window.location = '${contextPath}/admin/product/query?status=${status}&siteName='+siteName;
            layer.close(index);
        });
    }
    
    function updateWeight(prodId,searchWeight) {
        //修改权重页面
            layer.prompt({
                formType: 0,
                value: searchWeight,
                title: '修改商品权重-会影响商品搜索',
                area: ['800px', '350px'] //自定义文本域宽高
            }, function(value, index, elem){
                var patrn = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)/;  //验证是否为数字
                if(!patrn.test(value)){
                    layer.alert("输入权重只能为两位小数",{icon: 0});
                    return;
                }
                if(searchWeight==value){
                   layer.alert("请填写修改的权重");
                   return;
                }
                //调整库存
                $.ajax({
                    type: "post",
                    data:{"prodId":prodId,"searchWeight":value},
                    url: contextPath+"/admin/product/updateSearchWeight",
                    dataType: "json",
                    async : true, //默认为true 异步
                    success: function(msg){
                        layer.msg('更新成功！',{icon:1,time:700},function(){
                            parent.location.reload();
                            layer.close(index);
                        });

                    }
                });
                layer.close(index);
            });
        }

    function realDelOne(prodId) {
        jQuery.ajax({
            url: "${contextPath}/admin/product/delete/" + prodId,
            type: 'get',
            async: false, //默认为true 异步  
            dataType: 'json',
            success: function (data) {
            }
        });
    }

    //将商品放入回收站
    function confirmDelete(prodId, prodName, obj) {
        layer.confirm("是否将商品：'" + prodName + "' 放入商品回收站?", {icon:3, title:'提示'}, function () {
            jQuery.ajax({
                url: "${contextPath}/admin/product/toDustbin/" + prodId,
                type: 'get',
                async: false, //默认为true 异步  
                dataType: 'json',
                success: function (data) {
                    if (data == "OK") {
                        layer.msg("已将商品放入回收站！", {icon:1});
                        $(obj).parents("tr").remove();
                    } else {
                    	layer.msg("操作失败！", {icon:2});
                    }
                }
            });
        });
    }

    function pager(curPageNO) {
        $("#curPageNO").val(curPageNO);
        sendData(curPageNO);
    }


    jQuery(document).ready(function () {
        //三级联动
        $("select.combox").initSelect();
        if(brandName == ''){
	        makeSelect2(contextPath + "/admin/brand/getBrandSelect2","brandId","品牌","value","key",'请选择品牌', 'brandName');
        }else{
        	makeSelect2(contextPath + "/admin/brand/getBrandSelect2","brandId","品牌","value","key",brandName, 'brandName');
    		$("#select2-chosen-1").html(brandName);
        }
        
        if(siteName == ''){
	        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
        }else{
        	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",siteName, 'siteName');
    		$("#select2-chosen-2").html(siteName);
        }  
        
        jQuery("button[name='statusImg']").click(function (event) {
            $this = $(this);
            var item_id = $(this).attr("item_id");
            var status = $(this).attr("status");
            var desc;
            var toStatus;
            if (status == 1) {
                toStatus = 0;
                desc = "将'" + $(this).attr("itemName") + "'放入仓库?";
            } else {
                toStatus = 1;
                desc = $(this).attr("itemName") + ' 上线?';
            }
			
            layer.confirm(desc, {icon:3,title:desc}, function(){
            	 jQuery.ajax({
                     url: "${contextPath}/admin/product/updatestatus/" + item_id + "/" + toStatus,
                     type: 'get',
                     async: false, //默认为true 异步  
                     dataType: 'json',
                     success: function (data) {
                         //if (data == 1) {
                         //	$this.html("<span class='am-icon-arrow-down'></span>下线");
                         //	$this.attr("style", "color:#f37b1d;");
                         //} else if (data == 0) {
                         //	$this.html("<span class='am-icon-arrow-up'></span>上线");
                         //	$this.attr("style", "color:#0e90d2;");
                         //}
                         //$this.attr("status", data);
                         window.location.reload(true);
                     }
                 });
            	 return true;
            });
        });
        
        sendData(1);
    });

    function sendData(curPageNO) {
        var data = {
            "shopId": $("#shopId").val(),
            "curPageNO": curPageNO,
            "status": $("#status").val(),
            "commend": $("#commend").val(),
            "brandId": $("#brandId").val(),
            "hot": $("#hot").val(),
            "name": $("#name").val(),
            "prodId": $("#prodId").val(),
            "categoryId": $("#prodCatId").val(),
            "categoryName": $("#prodCatName").val(),
            "brandId": $("#brandId").val(),
            "siteName":siteName
        };
        $.ajax({
            url: "${contextPath}/admin/product/queryContent",
            data: data,
            type: 'post',
            async: true, //默认为true 异步   
            success: function (result) {
                $("#prodContentList").html(result);
            }
        });
    }

    //加载分类用于选择
    function loadCategoryDialog() {
    	layer.open({
    		title: "选择分类",
    		id: "prodCat",
    		type: 2, 
    		content: ["${contextPath}/admin/product/loadAllCategory/P",'no'],
    		area: ['280px', '400px']
    	});
    }

    function clearCategoryCallBack() {
        $("#prodCatId").val("");
        $("#prodCatName").val("");
    }

    function loadCategoryCallBack(id, catName) {
        $("#prodCatId").val(id);
        $("#prodCatName").val(catName);
    }

    function searchBrands(obj) {
        var value = {
            "brandName": $(obj).val()
        };
        $.ajax({
            url: "${contextPath}/admin/product/getBrandsByName",
            type: 'post',
            data: value,
            async: true, //默认为true 异步   
            success: function (result) {
                $(obj).parent().next().html(result);
            }
        });
    }

    /**
     *违规下架
     */
    function illegalOff(ths) {
        var name = $(ths).attr("itemName");
        var id = $(ths).attr("item_id");
        var siteName = $(ths).attr("siteName");
        
        layer.open({
        	  type: 1,
        	  title: '违规下架',
        	  skin: 'layui-layer-rim', //加上边框
        	  area: ['570px', '350px'], //宽高
        	  btn: ['确认提交'],
        	  content: "<table class='${tableclass} no-border content-table' style='min-width:550px;width:550px;'><tr><td width='120' align='right'>商城：</td><td align='left' width='360'>" + siteName + "</td></tr><tr><td align='right'>违规下架商品：</td><td align='left'>" + name + "</td></tr>"
              + "<tr><td align='right' valign='top'>违规下架意见：</td><td align='left'><textarea id='auditOpinion' style='width:350px;' cols='70' rows='5' maxlength='250'></textarea></td></tr></table>",
              yes: function(index, layero){
            	  var auditOpinion = $("#auditOpinion").val().trim();
            	  if (auditOpinion == "") {
                      layer.msg("意见不能为空", {icon:2});
                      $("#auditOpinion").focus();
                      return false;
                  }
            	  layer.load(2);
            	  $.ajax({
                      url: "${contextPath}/admin/product/illegalOff",
                      type: 'post',
                      data: {"prodId": id, "auditOpinion": auditOpinion},
                      dataType: "json",
                      async: false, //默认为true 异步   
                      success: function (data) {
                          if (data == "OK") {
                              window.location.reload(true);
                          } else {
                              layer.alert("下架失败", {icon:2});
                          }
                      }
                  });
              }
        	});
    }

    /** 审核商品  */
    function auditProd(prodId, ths) {
        var itemName = $(ths).attr("itemName");
        layer.open({
        	type: 1,
        	title: "审核商品",
       	  	skin: 'layui-layer-rim', //加上边框
       	 	btn: ['确认提交'],
       	  	area: ['650px', '400px'], //宽高
       	 	content: "<table class='${tableclass} no-border' style='min-width:600px;'><tr><td align='right'>商品：</td><td width='80%' align='left'>" + itemName + "</td></tr><tr><td align='right'>审核：</td><td width='80%' align='left'>"
            + "<input id='agreenAudit' type='radio' name='audit' value='1' checked='checked'/><label for='agreenAudit'>通过<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            + "<input id='disAudit' type='radio' name='audit' value='0'/><label for='disAudit'>不通过</label></td></tr>"
            + "<tr><td align='right' valign='top'>处理意见：</td><td width='80%' align='left'><textarea id='auditOpin' cols='70' rows='5' maxlength='250'></textarea></td></tr></table>",
            yes: function(index, layero){
            	var audit = $("input[name=audit]:checked").val();
                var auditOpin = $("#auditOpin").val().trim();
                if (auditOpin == "") {
                    layer.msg("处理意见不能为空", {icon:2});
                    $("#auditOpin").focus();
                    return false;
                }
                $.ajax({
                    url: "${contextPath}/admin/product/audit",
                    type: 'post',
                    data: {"prodId": prodId, "audit": audit, "auditOpinion": auditOpin},
                    dataType: "json",
                    async: false, //默认为true 异步   
                    success: function (data) {
                        if (data == "OK") {
                            window.location.reload(true);
                        } else {
                            layer.alert("审核失败", {icon:2});
                        }
                    }
                });
            }
        });
    }

    function clearSearData() {
        $("#siteName").val("");
        $("#commend").val("");
        $("#brandId").val("");
        $(".kui-combobox-caption").val("");
        $("#hot").val("");
        $("#name").val("");
        $("#prodId").val("");
        $("#prodCatId").val("");
        $("#prodCatName").val("");
    }

    function realDel(prodId, name) {
        layer.confirm("删除后不可恢复, 确定要删除'" + name + "' 吗?", {icon:3, title:'提示'}, function () {
            jQuery.ajax({
                url: "${contextPath}/admin/product/delete/" + prodId,
                type: 'get',
                async: false, //默认为true 异步  
                dataType: 'json',
                success: function (data) {
                    if (data == "OK") {
                        window.location = "${contextPath}/admin/product/query?status=${status}&siteName=${prod.siteName}";
                    } else {
                        layer.msg(data, {icon:2});
                    }
                }
            });
        });
    }
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        sendData(1);
    }
    
    
    /**
	 * 初始化select2
	 * @param id
	 * @param clearFlag
	 */
function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
                     	 currPage: page, //要查询的页码
	                };
	            },
    			type : "GET",
    			dataType : "JSON",
                results: function (data, page, query) {
                	if(page * 5 < data.total){//判断是否还有数据加载
                		data.more = true;
                	}
                    return data;
                },
                cache: true,
                quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
    
  function exprotData() {
    var status = $("#status").val();
    var shopId = $("#shopId").val();
    var categoryId = $("#prodCatId").val();
    var brandId = $("#brandId").val();
    var name = $("#name").val();
    var prodId = "${prodId}";
    var data = [];
    data.push(status);
    data.push(shopId);
    data.push(categoryId);
    data.push(brandId);
    data.push(name);
    data.push(prodId);

    var jsondata = encodeURI(JSON.stringify(data));
    window.location.href = "${contextPath}/admin/product/export?data=" + jsondata;
}
  
</script>
</html>
