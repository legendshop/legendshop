<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">用户信息管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li <c:if test="${empty haveShop}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/system/userDetail/query"/>">所有用户</a></li>
						<li <c:if test="${haveShop eq '0'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/system/userDetail/query?haveShop=0"/>">普通用户</a></li>
						<li <c:if test="${haveShop eq '1'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/system/userDetail/query?haveShop=1"/>">商家</a></li>
					</ul>
				</div>
			</div>

			<form:form action="${contextPath}/admin/system/userDetail/query" id="form1" name="form1" method="get">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
				<div class="criteria-div"">
					<span class="item">
						<input class="${inputclass}" type="text" name="userName" maxlength="50" value="${userDetail.userName}" placeholder="请输入用户名搜索" /> 
					</span>
					<span class="item">
						<input class="${inputclass}" type="text" name="userMail" maxlength="50" value="${userDetail.userMail}" placeholder="请输入邮件搜索" /> 
					</span>
					<span class="item">
						<input class="${inputclass}" type="text" name="realName" maxlength="50" value="${userDetail.realName}" placeholder="请输入姓名搜索" />
					</span>
					<span class="item">
						<input class="${inputclass}" type="text" name="nickName" maxlength="50" value="${userDetail.nickName}" placeholder="请输入昵称搜索" /> 
					</span>
					<span class="item">
						<input class="${inputclass}" type="text" name="userMobile" maxlength="50" value="${userDetail.userMobile}" placeholder="请输入手机号码搜索" /> 
					</span>
						<input type="hidden" id="haveShop" name="haveShop" />
					<span class="item">
						&nbsp;状态
						<select id="enabled" name="enabled" class="criteria-select">
							<option value="">请选择</option>
							<option value="1">有效</option>
							<option value="0">无效</option>
						</select> 
						<input class="${btnclass}" type="button" onclick="search()" value="搜索" /> 
						<input class="${btnclass}" type="button" value="导出搜索数据" onclick="exportUser();" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list"
					requestURI="/admin/system/userDetail/query" id="item"
					export="false" class="${tableclass}" sort="external">
					<display:column style="width:60px;vertical-align: middle;text-align: center;" title="
						<span>
				           <label class='checkbox-wrapper'>
								<span class='checkbox-item'>
									<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
									<span class='checkbox-inner' style='margin-left:10px;'></span>
								</span>
						   </label>	
						</span>">
						<label class="checkbox-wrapper">
							<span class="checkbox-item">
								<input type="checkbox" value="${item.userId}" arg="${item.userName}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
								<span class="checkbox-inner" style="margin-left:10px;"></span>
							</span>
					   </label>	
					</display:column>
					<%-- <display:column title="顺序" class="orderwidth" style="width:100px;"><%=offset++%></display:column> --%>
					<display:column title="用户名" style="width:280px;" sortable="true"
						sortName="u.user_name">
						<a
							href="<ls:url address='/admin/userinfo/userDetail/${item.userId}'/>">${item.userName }</a>
					</display:column>
					<display:column title="昵称" property="nickName" sortable="true"
						sortName="u.nick_name"
						style="max-width:150px; word-wrap: break-word;width:180px;"></display:column>
					<c:if test="${haveShop eq '1'}">
						<display:column title="店铺名" style="width:130px;">
							<a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.siteName}</a>
						</display:column>
					</c:if>
					<display:column title="邮件" property="userMail" style="width:150px;"></display:column>
					<display:column title="手机号码" property="userMobile"
						style="width:120px;min-width:80px;"></display:column>
					<display:column title="用户类型" style="min-width:100px;width:200px;">
						<c:choose>
							<c:when test="${not empty item.shopId && (item.shopStatus == -3 || item.shopStatus == 0 || item.shopStatus == 1)}">商家</c:when>
							<c:otherwise>普通用户</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="预存款" style="min-width:100px;width:150px;">
						<fmt:formatNumber type="CURRENCY"
							value="${item.availablePredeposit}" pattern="#0.00#"
							maxFractionDigits="2" />
					</display:column>
					
					
					<!-- 暂时没有金币充值业务，不显示金币情况 -->
					<%-- <display:column title="金币" property="userCoin"></display:column> --%>
					
					<display:column title="状态" sortable="true" sortName="s.enabled">
						<c:choose>
							<c:when test="${item.enabled == 1}">有效</c:when>
							<c:otherwise>
								<font>无效</font>
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html"
						style="width:200px; min-width:190px;">
						<%--<ls:auth ifAnyGranted="F_VIEW_USER">
		         <a href='${contextPath}/admin/system/userDetail/load/${item.userName}' title="查看用户${item.userName}信息"><img alt="查看用户${item.userName}信息" src="${contextPath}/resources/common/images/ind_left_login.gif"></a>
		      </ls:auth>
		     --%>
					<div class="table-btn-group">
						<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/userinfo/userDetail/${item.userId}'">
							查看
						</button>
						<!--用户上下线 start  -->
						<c:choose>
							<c:when test="${item.enabled == 1}">
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="changeUserStatus('${item.userId}', 0);">
									下线
								</button>
								<span class="btn-line">|</span>
							</c:when>
							<c:otherwise>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="changeUserStatus('${item.userId}', 1);">
									上线
								</button>
								<span class="btn-line">|</span>
							</c:otherwise>
						</c:choose>
						<!--用户上下线 end  -->
						<button class="tab-btn" onclick="javascript:confirmDelete('${item.userId}','${item.userName}', false)">
							 删除
						</button>
						<span class="btn-line">|</span>
						<div class="am-dropdown" data-am-dropdown>
							<button class="am-dropdown-toggle tab-btn" data-am-dropdown-toggle>
								<span class="am-icon-cog"></span> <span
									class="am-icon-caret-down"></span>
							</button>
							<ul class="am-dropdown-content">
								<li><a
									href="${contextPath}/admin/member/user/roles/${item.userId}?appNo=FRONT_END">1.
										查看用户角色</a></li>
								<li><a
									href="${contextPath}/admin/member/user/functions/${item.userId}?appNo=FRONT_END">2.
										查看用户权限</a></li>
								<li><a
									href="<ls:url address='/admin/member/user/update/${item.id}?appNo=FRONT_END'/>">3.
										修改密码</a></li>
								<li><a
									href="<ls:url address='/admin/sendmessage/load/${item.userId}?type=1'/>">4.
										发送站内信</a></li>
								<%-- <li><a
									href="<ls:url address='/admin/sendmessage/load/${item.userId}?type=2'/>">5.
										发送邮件</a></li> --%>
								<%--   <li><a href="<ls:url address='/admin/sendmessage/load/${item.userId}?type=3'/>">6. 发送短信</a></li> --%>
								<%-- 	                  <li onclick="coinPage('${item.userId}');"><a href="javascript:void(0);">6. 金币充值</a></li> --%>
							</ul>
						</div>
					</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fl">
			       		<input type="button" value="批量刪除" onclick="return deleteAction();" id="batch-off" class="batch-btn">
			       		<input type="button" value="批量下线" id="batch-offline" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
	<form id="exportForm" method="get" action="${contextPath}/admin/system/userDetail/export">
	<input name="haveShop" id="haveShop"  type="hidden" value="${haveShop}">
	<input name="userName" id="userName"  type="hidden" value="${userDetail.userName}">
	<input name="userMail" id="userMail"  type="hidden" value="${userDetail.userMail}">
	<input name="realName" id="realName"  type="hidden" value="${userDetail.realName}">
	<input name="nickName" id="nickName"  type="hidden" value="${userDetail.nickName}">
	<input name="userMobile" id="userMobile"  type="hidden" value="${userDetail.userMobile}">
	<input name="enabled" id="enabled"  type="hidden" value="${userDetail.enabled}">
</form>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/userDetailList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
	var haveShop = '${haveShop}';
	var enabled = '${userDetail.enabled}'
</script>
</html>

