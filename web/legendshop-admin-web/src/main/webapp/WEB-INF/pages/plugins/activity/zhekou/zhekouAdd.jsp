<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>营销管理 - 折扣添加</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed"href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
 	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
 <table class="${tableclass}" style="width: 100%">
			    	<tr><th> <strong class="am-text-primary am-text-lg">营销管理/</strong><a href="<ls:url address="/admin/shopDetail/query"/>">折扣促销</a> /  添加活动</th></tr>
			    </table>
		<form:form  action="${contextPath}/admin/marketing/saveZkMarketing"  id="add_form"  method="post">
			<table style="width: 100%" class="${tableclass}" id="col1">
				<tr>
					 <td>
	         			 <div align="right"> <font color="ff0000">*</font>活动名称： </div>
	      			 </td>
	      			 <td>
	      			 	<input type="text" class="${inputclass}" maxlength="25" name="marketName" id="marketName" size="30">&nbsp;&nbsp;活动名称最多为25个字符
	      			 </td>
      			 </tr>
      			 <tr>
      			 	<td>
      			 		 <div align="right"> <font color="ff0000">*</font>开始时间： </div>
      			 	</td>
      			 	<td>
      			 		<input type="text" class="text w130 Wdate "  onfocus="var endDate=$dp.$('endTime'); WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(){endTime.focus();},maxDate:'#F{$dp.$D(\'endTime\')}'})"   name="startTime" id="startTime" readonly="readonly" >
      			 	</td>
      			 </tr>
				 <tr>
      			 	<td>
      			 		 <div align="right"> <font color="ff0000">*</font>结束时间： </div>
      			 	</td>
      			 	<td>
      			 		 <input type="text" class="text w130 Wdate " onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startTime\')}'})"   name="endTime" id="endTime" readonly="readonly">
      			 	</td>
      			 </tr>
      			 
      			 <tr>
      			 	<td><div align="right"> <font color="ff0000">*</font>选择商品： </div></td>
      			 	<td>
      			 		<label for="goods_status_1">
					       <input
								type="radio" checked="checked" id="goods_status_1" value="1"
								name="isAllProds">全部商品</label>
						   <label for="goods_status_0"  style="margin-left: 10px;"> 
					         <input type="radio" id="goods_status_0" value="0" name="isAllProds">部分参与商品
					     </label>
					     <div id="zk_rule">
					     <input type="text" class="text w50" id="manze_discount" name="discount"><em class="add-on">折</em>
					     </div>
      			 	</td>
      			 	
      			 </tr>
      			 
      			  <tr>
      			 	<td><div align="right">备注： </div></td>
      			 	<td>
      			 	<textarea class="textarea w400" maxlength="100" id="remark" rows="5" cols="100" name="remark" placeholder="活动备注最多为100个字符"></textarea>
      			 	</td>
      			 </tr>
      			 <tr>
      				<td colspan="2"> 
      					<div style="margin-left: 190px;">
      						<input type="submit" value="保存"  class="${btnclass}">
      						 <input type="button" class="${btnclass}" value="返回" onclick="window.location='<ls:url address='/admin/marketing/zhekou/query' />'">
      					</div>
      				</td>	
      			 </tr>
      			 <tr>
      			 	<td colspan=2">
      			 		<br>
						1. 选择商品分为全场商品和部分商品[部分商品需要在列表进行管理添加商品信息]
						<br>
						1. 选择全场商品,需要设定全部商品的折扣价格
      			 	</td>
      			 </tr>
			</table>
		</form:form>
	<div class="clear"></div>
	</div>
	</div>
	</body>
  <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#startTime").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };
    
    
    jQuery.validator.methods.isAllProds = function(value, element) {
         var type= $("input[name='isAllProds']:checked").val();
         if(type==1){
	        if(isNaN(value)) {
              return false;
            } 
            var re =/^[1-9]([.]{1}[1-9])?$/;  
			if(!re.test( value)) {
	            return false;
			}
         }
         return true;
    };
    
    
    //页面输入内容验证
    $("#add_form").validate({
        ignore: "",
        onfocusout: false,
        rules : {
            marketName : {
                required : true
            },
            startTime : {
                required : true,
            },
            endTime : {
                required : true,
                greaterThanStartDate : true
            },
            type:{
               required : true
            },
            discount:{
               isAllProds : true
            }
        },
        messages : {
            marketName : {
                required : '活动名称不能为空'
            },
            startTime : {
                required : '开始时间不能为空'
            },
            endTime : {
                required : '结束时间不能为空',
                greaterThanStartDate : '结束时间必须大于开始时间'
            },
            type:{
               required : '请选择促销类型',
            },
            discount: {
                isAllProds: '请输入正确的折扣格式,规则金额只能是1-10之间的1位整数或小数1-9.9之间'
            }
        },
        submitHandler:function(form){
            $("#add_form").ajaxForm().ajaxSubmit({
			   success:function(data) {
				  var result=eval(data);
			      if(result=='OK'){
			       layer.msg('添加成功',{icon:1,time:1000},function(){
			    	   window.location.href=contextPath+"/admin/marketing/zhekou/query";
				        return;
			       });
			     
			      }else{
			        layer.msg(result,{icon:2});
			         return ;
			      }
			   }
	        });
        }
    
    });
     
     $("input[name='isAllProds']").change(function(){
         var type=$(this).val();
         if(type=='1'){ //满折促销 
            $("#zk_rule").show();
         }else {
            $("#zk_rule").hide();
         }
     });
     
    
</script>  
</html>