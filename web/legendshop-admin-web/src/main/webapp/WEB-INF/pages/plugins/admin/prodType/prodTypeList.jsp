<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>属性类型管理- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">属性管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">属性类型管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/prodType/query" id="form1"
				method="get">
					<div class="criteria-div">
						<span class="item">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
							类型名称：<input class="${inputclass}" type="text" id="name" name="name" value="${prodType.name}" />
						</span>
						<span class="item">
							分类：<input type="text" style="cursor: pointer;" id="prodCatName" name="categoryName" value="${categoryName }" onclick="loadCategoryDialogForSrh();" class="${inputclass}" placeholder="" readonly="readonly" /> 
							<input type="hidden" id="prodCatId" name="categoryId" value="${categoryId}" /> 
							<input class="${btnclass}" type="button" onclick="search()" value="搜索" /> 
							<input class="${btnclass}" type="button" value="创建类型" onclick='window.location="<ls:url address='/admin/prodType/load'/>"' />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/prodType/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">

					<display:column title="序号" class="orderwidth" style="width: 60px"><%=offset++%></display:column>
					<display:column title="类型名称" property="name" style="width: 300px"></display:column>
<%--					<display:column title="允许编辑属性">--%>
<%--						<ls:optionGroup type="label" required="true" cache="true"--%>
<%--							beanName="YES_NO" selectedValue="${item.attrEditable}"--%>
<%--							defaultDisp="" />--%>
<%--					</display:column>--%>
<%--					<display:column title="允许编辑参数">--%>
<%--						<ls:optionGroup type="label" required="true" cache="true"--%>
<%--							beanName="YES_NO" selectedValue="${item.paramEditable}"--%>
<%--							defaultDisp="" />--%>
<%--					</display:column>--%>
					<display:column title="记录时间" property="recDate"
						format="{0,date,yyyy-MM-dd HH:mm}" style="width: 200px;"></display:column>

					<display:column title="操作" media="html" style="width: 285px;">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="loadCategoryDialog('${item.id}');">关联分类</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/prodType/load/${item.id}'">修改</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.id}')">删除</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/prodTypeList.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
</script>
</html>
