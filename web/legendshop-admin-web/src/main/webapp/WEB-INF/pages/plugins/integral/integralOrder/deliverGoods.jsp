<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/processOrder.css?1'/>" />
<style type="text/css">
	td{
		height:30px;
	}
</style>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<title>保存物流信息</title>
</head>
<body>
		<div style="position: absolute;  width: 395px;">
			<div class="white_box" style="margin-left:15px;">
			    <table width="358" class="no-border" cellspacing="0" cellpadding="0" border="1"
						class="box_table" style="float:left;margin-top:20px;">
						<tbody>
							<tr>
								<td valign="top" align="center" colspan="2" style="font-weight: bold;">请输入您的物流信息</td>
							</tr>
							<tr>
								<td width="100"  align="right">订单号： 
								<input type="hidden" value="${integralOrder.orderSn}" readonly="readonly" id="orderSn"  name="orderSn"></td>
								<td align="left">${integralOrder.orderSn}</td>
							</tr>
							<tr>
								<td align="right">物流公司：</td>
								<td align="left">
								<select id="ecc_id" name="ecc_id" style="width:214px;">
								       <c:forEach items="${deliveryTypes}" var="dt">
								           <option value="${dt.dvyTypeId}" <c:if test="${dt.dvyTypeId==integralOrder.dvyTypeId}" > selected="selected" </c:if> >${dt.name}</option>
								       </c:forEach>
								</select>
								</td>
							</tr>
							<tr>
								<td align="right">物流单号：</td>
								<td align="left"><input type="text" style="width:204px;" value="${integralOrder.dvyFlowId}" size="30" id="dvyFlowId" name="dvyFlowId" maxlength="20">
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2">
								<span class="inputbtn" style="margin-left: 10px;">
									 <input type="button" onclick="fahuo();" class="white_btn" style="cursor:pointer;" value="发货" > 
								</span>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	<script type="text/javascript">	
	function fahuo(){
	     var deliv=$("#ecc_id").find("option:selected").val();
	     
	     if(deliv==undefined || deliv==null || deliv==""){
	        //alert("请去设置配送方式");
	        layer.msg("请去设置配送方式", {icon:2});
	        return ;
	     }
	     
	     var dvyFlowId=$.trim($("#dvyFlowId").val());
	     if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
	       // alert("请输入物流单号");
	        layer.msg("请输入物流单号", {icon:2});
	        return ;
	     }
	    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	 	var reg = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
	 	if(pattern.test(dvyFlowId)||reg.test(dvyFlowId)){
	 		layer.msg("您的物流单号不合法！", {icon:2});
	 		return ;
	 	}
	     var state_info="";
	     var orderSn=$.trim($("#orderSn").val());
	     if(orderSn==undefined || orderSn==null || orderSn==""){
	        return;
	     }
	     
	    $.post("${contextPath}/admin/integralOrder/fahuo/"+ orderSn, {"deliv": deliv,"dvyFlowId":dvyFlowId},
		        function(retData) {
			       if(retData == 'OK' ){
			    	   parent.location.reload();
			    	   var index = parent.layer.getFrameIndex('deliverGoods'); //先得到当前iframe层的索引
			    	   parent.layer.close(index); //再执行关闭  
			       }else if(retData == 'fail'){
			    	   layer.msg('发货失败，订单号 ' + orderSn, {icon:2});
			       }else{
			    	   layer.msg(retData, {icon:2});
			       }
		 },'json');
	}
	</script>
</body>
</html>