<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<table class="${tableclass}" style="width: 100%">
   	<thead>
			<tr>
				<th><strong class="am-text-primary am-text-lg">用户管理</strong> / 金币管理</th>
			</tr>
			</thead>
	</table>
	
	 <div style="margin-left: 0.5rem">
		<div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
          	<li class="am-active"><i></i><a>充值管理</a></li>
         	<li ><i></i><a href="<ls:url address='/admin/coin/consumeDetails'/>">消费明细</a></li>
          </ul>
        </div>
	 </div>
   
<form:form  action="${contextPath}/admin/coin/rechargeAdmin" id="rechargeForm">
<table class="${tableclass}" style="width: 100%">
<tbody><tr><td>
 <div align="left" style="padding: 3px">
	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${userName}" placeholder="用户名"/>
	<input class="${inputclass}" type="text" name="mobile" id="mobile" maxlength="50" value="${mobile}" placeholder="手机号码"/>
    <input class="${btnclass}" type="submit" value="搜索"/>
    <input class="${btnclass}" value="下载批量充值模板" type="button" id="downLoadTemplate" onclick="download();"/>
    <input class="${btnclass}" value="上传批量充值" type="button" id="upLoadTemplate" onclick="uploadPage();"/>
 </div>
 </td></tr></tbody>
</table>       
</form:form>
<div align="center">
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
<display:table name="list" requestURI="/admin/coin/rechargeAdmin" id="item" export="false" 
	 class="${tableclass}">
 	 <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
 	 <display:column title="用户名" property="userName"></display:column>
 	 <display:column title="手机号码" property="mobile"></display:column>
 	 <display:column title="昵称" property="nickName"></display:column>
 	 <display:column title="金额变更" property="amount"></display:column>
 	 <display:column title="充值时间">
 	 	<fmt:formatDate value="${item.addTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm" />
 	 </display:column>
</display:table>
	  
<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
</div>
<script language="JavaScript" type="text/javascript">
function pager(curPageNO){
      document.getElementById("curPageNO").value=curPageNO;
      document.getElementById("rechargeForm").submit();
}

//下载模板
function download(){
	window.location.href="${contextPath}/prodExecl/recharge_kungyuncoin.xls";
}

function uploadPage(){
	layer.open({
		  title :"批量充值",
		  type: 2, 
		  content: "${contextPath}/admin/coin/uploadPage", 
		  area: ['500px', '550px']
		}); 
	
}
</script>
