<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>微信素材管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/weixin/css/newstemplate.css'/>" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
 </head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	 	<div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <form:form  action="${contextPath}/admin/weixinNewstemplate/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<strong class="am-text-primary am-text-lg">微信管理</strong> /  微信素材管理	
			    	</th>
		    	</tr>
			    <tr>
			    	<td>
			    		<ul class=" am-tabs-nav am-nav am-nav-tabs">
				    			<li class="am-active"><a>图文消息</a></li>
				    			<li><a href="${contextPath}/admin/weixinNewstemplate/filePage">图片库</a></li>
				    		</ul>
			    	</td>
			    </tr>
		    </thead>
	    </table>
    </form:form>
     <div class="head_msg_info">
    	<a href="${contextPath}/admin/weixinNewstemplate/query"><span class="am-icon-chevron-circle-left am-icon-sm" style="margin-right:5px;margin-left:5px;"></span>图文消息</a> / 新建图文消息
    </div>
    <div class="news_create_content">
    	<!-- 左侧预览 -->
		<div class="news_preview_area">
			<div class="news_preview_content">
				<div class="cover_msg_area">
					<input name="id" type="hidden" value="${coverItem.id}">
					<input name="title" type="hidden" value="${coverItem.title}">
					<input name="author" type="hidden" value="${coverItem.author}">
					<input name="img" type="hidden" value="${coverItem.imagepath}">
					<input name="content" type="hidden" value="${fn:escapeXml(coverItem.content)}">
					<input name="desc" type="hidden" value="${coverItem.desction}">
					<input name="url" type="hidden" value="${coverItem.url}">
					<input name="imgForContent" type="hidden" value="${coverItem.imagepathContent}">
					<input name="newsTemplateid" type="hidden" value="${coverItem.newsTemplateid}">
					<input name="thumbMediaId" type="hidden" value="${coverItem.thumbMediaId}">
					<input name="createDate" type="hidden" value="${coverItem.createDate}">
					<input name="createUserId" type="hidden" value="${coverItem.createUserId}">
					<h4 class="cover_msg_title">
						<a class="v_title">${coverItem.title}</a>
					</h4>
					<div class="news_cover_img">
						<c:choose>
							<c:when test="${empty coverItem.imagepath}">
								<i>封面图片</i>
								<img alt="" src="" style="display:none;">
							</c:when>
							<c:otherwise>
								<i style="display:none;">封面图片</i>
								<img alt="" src="<ls:photo item='${coverItem.imagepath}'/>">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="news_edit_mask">
			            <a onclick="editCoverItem(this);" id="coverEditA"><span class="am-icon-pencil am-icon-sm"></span></a>
			        </div>
				</div>
				
				<c:forEach items="${newsItems}" begin="1" var="newsItem" varStatus="status">
				
					<div class="news_item">
						<input name="id" type="hidden" value="${newsItem.id}">
						<input name="title" type="hidden" value="${newsItem.title}">
						<input name="author" type="hidden" value="${newsItem.author}">
						<input name="img" type="hidden" value="${newsItem.imagepath}">
						<input name="content" type="hidden" value="${fn:escapeXml(newsItem.content)}">
						<input name="desc" type="hidden" value="${newsItem.desction}">
						<input name="url" type="hidden" value="${newsItem.url}">
						<input name="imgForContent" type="hidden" value="${newsItem.imagepathContent}">
						<input name="newsTemplateid" type="hidden" value="${newsItem.newsTemplateid}">
						<input name="thumbMediaId" type="hidden" value="${newsItem.thumbMediaId}">
						<input name="createDate" type="hidden" value="${newsItem.createDate}">
						<input name="createUserId" type="hidden" value="${newsItem.createUserId}">
						<c:choose>
							<c:when test="${empty newsItem.imagepath}">
								<i>缩略图</i>
								<img style="display:none;" alt="" src="">
							</c:when>
							<c:otherwise>
								<i style="display:none;">缩略图</i>
								<img alt="" src="<ls:photo item='${newsItem.imagepath}'/>">
							</c:otherwise>
						</c:choose>
						<h4>
							<a class="v_title">${newsItem.title}</a>
						</h4>
						<div class="news_edit_mask">
				            <a onclick="editItem(this);" class="item_edit_a"><span class="am-icon-pencil am-icon-sm"></span></a> <a onclick="delItem(this);"><span class="am-icon-trash-o am-icon-sm"></span></a>
				        </div>
					</div>
				
				</c:forEach>
			</div>
			<a class="news_addItem_a" onclick="addItem();">
				<i>+</i>
			</a>
		</div>
		
		<!-- 右侧编辑-->
		<div class="news_edit_area" >
			<a name="news_edit_area"></a>
			<div class="news_item_editor">
				<div class="item_editor">
					<div class="inner">
						<div class="news_field_item">
				            <label for="" class="frm_label">标题</label>
				            <span class="frm_input_box with_counter counter_in append count">
				                <input id="titleEdit" type="text" class="frm_input js_title js_counter" maxlength="64" style="padding-right: 14px;">
				                <em class="frm_input_append frm_counter">0/64</em>
				            </span>
				
				            <div class="frm_msg fail js_title_error" style="display: none;">标题不能为空且长度不能超过64字</div>
				        </div>
				        <div class="news_field_item">
				            <label for="" class="frm_label">作者<span>（选填）</span></label>
				            <span class="frm_input_box with_counter counter_in append count">
				                <input id="authorEdit" type="text" class="frm_input js_title js_counter" maxlength="8">
				                <em class="frm_input_append frm_counter">0/8</em>
				            </span>
				        </div>
				         <div class="news_field_item">
				            <label for="" class="frm_label">封面<span>（大图片建议尺寸：900像素 * 500像素）</span></label>
				            <div class="upload_wrap">
				                <div class="upload_box">
				                    <div class="upload_area webuploader-container">
				                        <a id="js_appmsg_upload_cover" href="javascript:void(0);" onclick="uploadOneFile();" class="btn btn_upload webuploader-pick">
				                         	本地上传 </a>
					                   <form:form  style='display: none;' id='formFile' name='formFile' method="post" action="${contextPath}/admin/weixinNewstemplate/uploadOneFile" target='frameFile' enctype="multipart/form-data">
											<input id="js_upload_btn" name="file" type="file" accept="image/*" >
										</form:form>
										<iframe id='frameFile' name='frameFile' style='display: none;'></iframe>
				                    </div>
				                </div>
				                &nbsp;&nbsp;<a id="js_imagedialog" href="javascript:void(0);" onclick="imageDialog();" class="btn btn_upload">从图片库选择</a>
				
				                <p class="js_cover upload_preview" style="display: none;">
				                	<img alt="" src="">
				                    <a class="js_removeCover" href="javascript:void(0);" onclick="delImg(this);">删除</a>
				                </p>
				            </div>
							<p class="frm_tips">
				                <label for="" class="frm_checkbox_label js_show_cover_pic selected">
				                    <i class="icon_checkbox"></i>
				                    <input id="imgForContentEdit" type="checkbox" class="frm_checkbox">
				                   	 封面图片显示在正文中                </label>
				            </p>
				            <div class="frm_msg fail js_img_error" style="display: none;">必须插入一张图片</div>
				        </div>
				        
				        <div class="news_field_item">
				            <label for="" class="frm_label">摘要
				                <span>（选填，该摘要只在发送图文消息为单条时显示）</span>
				            </label>
				            <span class="frm_textarea_box with_counter counter_out counter_in append count">
				                <textarea id="descEdit" class="frm_textarea js_desc js_counter" maxlength="120"></textarea>
				                <em class="frm_input_append frm_counter">0/120</em>
				            </span>
				        </div>
				        
				        <div class="news_field_item">
				            <label for="" class="frm_label">正文
				                <span></span>
				            </label>
				            <span class="content_span frm_textarea_box with_counter counter_out counter_in append count">
				                <textarea id="contentEdit" name="content" class="frm_textarea js_desc js_counter" maxlength="20000"></textarea>
				            </span>
				
				            <div class="frm_msg fail js_content_error" style="display:none;">正文不能为空且长度不能超过2000字</div>
				        </div>
				        
				        <div class="news_field_item">
				            <label for="" class="frm_label">原文链接
				                <span>（选填）</span>
				            </label>
				           <span class="frm_input_box with_counter counter_in append count">
				                <input id="urlEdit" type="text" class="frm_input js_title js_counter" maxlength="200">
				            </span>
				        </div>
					</div>
					<i class="arrow arrow_out" style="margin-top: 0px;"></i>
					<i class="arrow arrow_in" style="margin-top: 0px;"></i>
				</div>
				<!-- <p class="frm_tips">编辑好的图文消息，需要点击“保存并群发”才能发送到关注者手机上。</p> -->
			</div>
		</div>
		
		<!-- 底部按钮 -->
		<div class="tool_area">
			<div class="tool_bar">
				<div class="tool_span">
					<input onclick="saveNewsItems();" class="${btnclass} tool_btn" type="button" value="保存">
					<%--<input class="${btnclass} tool_btn oth_btn" type="button" value="预览">
					 <input class="${btnclass} tool_btn oth_btn" type="button" value="保存并群发"> --%>
				</div>
			</div>
		</div>
	</div>
	
	<div id="addItemDiv" style="display:none;">
		<div class="news_item">
			<input name="id" type="hidden" value="">
			<input name="title" type="hidden" value="">
			<input name="author" type="hidden" value="">
			<input name="img" type="hidden" value="">
			<input name="content" type="hidden" value="">
			<input name="desc" type="hidden" value="">
			<input name="url" type="hidden" value="">
			<input name="imgForContent" type="hidden" value="1">
			<input name="newsTemplateid" type="hidden" value="">
			<input name="thumbMediaId" type="hidden" value="">
			<input name="createDate" type="hidden" value="">
			<input name="createUserId" type="hidden" value="">
			<i>缩略图</i>
			<img style="display:none;" alt="" src="">
			<h4>
				<a class="v_title">标题</a>
			</h4>
			<div class="news_edit_mask">
	            <a class="edit_tool_a" onclick="editItem(this);"><span class="am-icon-pencil am-icon-sm"></span></a> <a onclick="delItem(this);"><span class="am-icon-trash-o am-icon-sm"></span></a>
	        </div>
		</div>
	</div>
	</div>
	</div>
	</body>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
		var photoPath = "<ls:photo item='' />";
	</script>		
	<script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/weixin/js/newstemplate.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
</html>