<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%> 
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>商品分类- 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
	<link type="text/css" rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>">
	<style type="text/css">
.cats_set {
	margin-left: 10px;
	padding: 0;
	width: 20%;
	float: left;
	font-size: 12px !important;
}

.cats_set #catContent {
	margin-top: 20px;
}

.detail_set {
	padding: 0;
	width: 72%;
	float: left;
	height: 90%;
	font-size: 12px !important;
}

.detail_set_title {
	margin-left: 312px;
	font-weight: bold;
	font-size: 14px;
}

.cat_set_title {
	margin: auto;
	text-align: left;
}

.detail_div {
	float: left;
	width: 80%;
	margin: auto;
}

.detail_div table {
	width: 50%;
	margin: auto;
	margin-top: 20px;
	border-color: #ccc;
	BORDER-COLLAPSE: collapse;
}

.detail_div table tr {
	height: 35px;
}

.detail_div table tr td {
	text-align: left;
	vertical-align: middle;
	padding: 5px;
	width: 100px;
}

.detail_div table tr td:first-child {
	text-align: right;
}
.am-table > tbody > tr > td{
	padding: 8px 8px !important;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	   <!-- sidebar start -->
		   <jsp:include page="../frame/left.jsp"></jsp:include>
	   <!-- sidebar end -->
	     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<form:form action="${contextPath}/admin/category/query" id="form1"
		method="post">
		<table class="${tableclass} title-border" style="width: 100%">
			<thead>
				<tr>
					<th class="title-border">
						<span class="title-span">商品管理  ＞  <span style="color:#0e90d2;">商品分类</span></span>&nbsp;
						<%-- <a href="${contextPath}/admin/adminDoc/view/2"><img src="${contextPath}/resources/templets/img/adminDocHelp.png" /></a> --%>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<div align="left" style="padding: 3px">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							<input type="button" value="创建类目" class="${btnclass}" onclick='editCategory();' /> 
							<input type="button" value="更新类目缓存" class="${btnclass}" onclick="cleanCategoryCache();" />
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</form:form>

<div class="cats_set" style="margin-top:20px;">
	<div class="cat_set_title">[选择分类]&nbsp; 
		<a style="font-weight: bold;" href="javascript:expandAll();void(0);">展开+</a>&nbsp; 
		<a style="font-weight: bold;" href="javascript:zhedie();void(0);">折叠-</a>
	</div>
	<div id="catContent">
		<ul id="catTree" class="ztree"></ul> 
	</div>
</div>

<div class="detail_set" id="catDetail" style="display: none;margin:20px 0;">
	<div class="detail_set_title">类目详情</div>
	<div align="center" class="detail_div">
		<form:form action="${contextPath}/admin/category/save" method="post"
			id="form1" onsubmit="return checkCatEdit()"
			enctype="multipart/form-data">
			<input type="hidden" name="id" id="catid">
			<table cellspacing="0" cellpadding="0" class="${tableclass } no-border content-table"
				style="position: relative;">
				<tr>
					<td width="30%"><span style="color: red;">* </span>名称</td>
					<td width="70%"><input class="${inputclass}" name="name" id="catname" maxlength="20"></td>
				</tr>
				<tr>
					<td><%--<span style="color: red;">* </span>--%>类目小图标</td>
					<td>
						<span id="iconspan" style="float: left; margin-right: 5px;"></span>
						<input style="float: left; margin-top: 5px;" type="file" id="smallCatFile1" name="catIconFile">
						<input class="criteria-btn" type="button" value="删除图片" onclick="deleteImg();">
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top;">类目图片</td>
					<td>
						<span id="imgspan" style="float: left;"></span> 
						<input style="float: left; margin-top: 5px;" type="file" id="catFile1" name="catPicFile">
					</td>
				</tr>
				<tr>
					<td><span style="color: red;">* </span>父类</td>
					<td><span id="catparent"></span>
				</tr>
				<tr>
					<td><span style="color: red;">* </span>排序</td>
					<td><input class="${inputclass}" name="seq" id="catseq"
						onkeyup="this.value=this.value.replace(/\D/g,'')"
						onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="4"></td>
				</tr>
				<tr>
					<td>状态</td>
					<td><select name="status" id="catsts" class="${selectclass}" style="width:150px;">
							<option value="1">上线</option>
							<option value="0">下线</option>
					</select></td>
				</tr>
				<%-- <tr>
					<td>Header菜单展示</td>
					<td><select name="headerMenu" id="catheader"
						class="${selectclass}">
							<option value="true">是</option>
							<option value="false">否</option>
					</select></td>
				</tr> --%>
				<tr>
					<td>导航菜单中显示</td>
					<td><select name="navigationMenu" id="catnavi"
						class="${selectclass}" style="width:150px;">
							<option value="true">是</option>
							<option value="false">否</option>
					</select></td>
				</tr>
				<tr>
					<td>类型选择</td>
					<td>
						<div class="J_spu-property" style="margin: 0px;">
							<ul class="J_ul-single ul-select"
								style="padding-left: 2px; margin-top: 3px; margin-bottom: 3px;">
								<li>
									<div class="kui-combobox" role="combobox">
										<div class="kui-dropdown-trigger">
											<input readonly="true" style="width: 145px;"
												class="kui-combobox-caption">
											<div class="kui-icon-dropdown"></div>
											<div id="kui-list" class="kui-list kui-listbox" size=5
												style="display: none; overflow-y:auto; overflow-x:hidden; width:168px; height: 400px">
												<div role="region" class="kui-header">
													<input type="text" class="kui-dropdown-search"
														autocomplete="off"
														onkeypress="if(event.keyCode==13) {searchType(this);return false;}" />
												</div>
												<div class="kui-body" style="width: 150px;">
													<div class="kui-list kui-group">
														<div data-value="" class="kui-option" title=""></div>
														<c:forEach items="${typeList }" var="type">
															<div class="kui-option" title="${type.name }"
																data-value="${type.id }">${type.name }</div>
														</c:forEach>
													</div>
												</div>
											</div>

										</div>
									</div> <select data-transtype="combox" class="keyPropClass"
									id="typeId" name="typeId"
									style="display: none; visibility: hidden;">
										<option selected="selected" value=""></option>
								</select>
								</li>
							</ul>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td width="30%">退换货有效时间</td>
					<td width="70%"><input class="${inputclass}"
						name="returnValidPeriod" id="returnValidPeriod"
						value="${category.returnValidPeriod }" onkeyup="this.value=this.value.replace(/\D/g,'')"
						onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="5"><strong>&nbsp;(天)</strong></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO关键字</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="keyword" id="catkey" maxlength="250"></textarea></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO描述</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="catDesc" id="catdesc" maxlength="250"></textarea></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO标题</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="title" id="cattitle" maxlength="250"></textarea></td>
				</tr>
			</table>
			<div style="margin-top: 10px;">
				<input class="${btnclass}" type="submit" value="保存修改"> 
				<input class="${btnclass}" type="button" value="删除类目" onclick="deleteCategory();"> 
				<input class="${btnclass}" type="button" value="查看类目下的商品链接" onclick="lookLink();">
			</div>
		</form:form>
	</div>
</div>

<div class="detail_set" id="newDetail" style="margin-top:20px;">
	<div class="detail_set_title">创建类目</div>
	<div align="center" class="detail_div">
		<form:form action="${contextPath}/admin/category/save" method="post" id="form2" onsubmit="return checkNewCat()" enctype="multipart/form-data">
			<table cellspacing="0" cellpadding="0" class="${tableclass } no-border content-table"
				style="position: relative;">
				<tr>
					<td width="30%"><span style="color: red;">* </span>名称：</td>
					<td width="70%"><input class="${inputclass}" name="name" maxlength="20"></td>
				</tr>
				<tr>
					<td><%--<span style="color: red;">* </span>--%>类目小图标(大小20*20)：</td>
					<td><input style="margin-top: 5px;" type="file" id="smallCatFile2"
						name="catIconFile"></td>
					<td align="left"><span style="color: red;">图片将出现在PC端1类目左边上</span></td>
				</tr>
				<tr>
					<td><%--<span style="color: red;">* </span>--%>类目图片(大小55*55)：</td>
					<td><input style="margin-top: 5px;" type="file" id="catFile2"
						name="catPicFile"></td>
					<td><span style="color: red;">图片将出现在手机端三级类目上</span></td>	
				</tr>
				<tr>
					<td><span style="color: red;">* </span>父类：</td>
					<td><input type="hidden" id="parentId" name="parentId" /> <span
						id="parentName"></span> <input type="button" value="选择"
						onclick="javascript:loadCategoryDialog()" /></td>
				</tr>
				<tr>
					<td><span style="color: red;">* </span>排序：</td>
					<td><input class="${inputclass}" name="seq"
						onkeyup="this.value=this.value.replace(/\D/g,'')"
						onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="4"></td>
				</tr>
				<tr>
					<td>状态：</td>
					<td><select name="status" class="${selectclass}" style="width:150px;">
							<option value="1">上线</option>
							<option value="0">下线</option>
					</select></td>
				</tr>
				<%-- <tr>
					<td>Header菜单展示</td>
					<td><select id="headerMenu1" name="headerMenu"
						class="${selectclass}" style="float: left;">
							<option value="false">否</option>
							<option value="true">是</option>
					</select> <span id="headerMenu_span"
						style="float: left; margin-left: 5px; margin-top: 5px;">
							说明：如果选择是，将出现在网页上方的大菜单中 </span></td>
				</tr> --%>
				<tr>
					<td>导航菜单中显示：</td>
					<td><select name="navigationMenu" class="${selectclass}"
						style="float: left;width:150px;">
							<option value="false">否</option>
							<option value="true">是</option>
					</select> <span id="navigationMenu_span"
						style="float: left; margin-left: 5px; margin-top: 5px;">
							说明：如果选择是，则出现在左上方的导航菜单中 </span></td>
				</tr>
				<tr>
					<td>类型选择：</td>
					<td>
						<div class="J_spu-property" style="margin: 0px;">
							<ul class="J_ul-single ul-select"
								style="padding-left: 0; margin-top: 3px; margin-bottom: 3px; float: left;">
								<li>
									<div class="kui-combobox" role="combobox">
										<div class="kui-dropdown-trigger">
											<input readonly="true" style="width: 145px;"
												class="kui-combobox-caption">
											<div class="kui-icon-dropdown"></div>
											<div id="kui-list" class="kui-list kui-listbox"
												style="display: none; overflow-y:auto; overflow-x:hidden;  width:168px; height: 400px">
												<div role="region" class="kui-header">
													<input type="text" class="kui-dropdown-search"
														autocomplete="off"
														onkeypress="if(event.keyCode==13) {searchType(this);return false;}" />
												</div>
												<div class="kui-body" style="width: 150px;">
													<div class="kui-list kui-group">
														<div data-value="" class="kui-option" title=""></div>
														<c:forEach items="${typeList }" var="type">
															<div class="kui-option" title="${type.name }"
																data-value="${type.id }">${type.name }</div>
														</c:forEach>
													</div>
												</div>
											</div>

										</div>
										
									</div> <select data-transtype="combox" class="keyPropClass"
									id="newTypeId" name="typeId"
									style="display: none; visibility: hidden;">
										<option selected="selected" value=""></option>
								</select>
								</li>
							</ul>
							<span id="J_spu-property_span"
								style="float: left; margin-left: 5px; margin-top: 5px;">
								说明：如果没有类型，请先<a target="_blank"
								href="${contextPath}/admin/prodType/query">新建 </a>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td width="30%">退换货有效时间：</td>
					<td width="70%"><input class="${inputclass}"
						name="returnValidPeriod" id="returnValidPeriod" onkeyup="this.value=this.value.replace(/\D/g,'')"
						onafterpaste="this.value=this.value.replace(/\D/g,'')" maxlength="5"><strong>&nbsp;(天)</strong>
						&nbsp;<span class="msg-span">类别退换货有效时间优先于系统默认的退换货有效时间</span>
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO关键字：</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="keyword" maxlength="250"></textarea></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO描述：</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="catDesc" maxlength="250"></textarea></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">SEO标题：</td>
					<td><textarea class="${inputclass}" rows="3" cols="50"
							name="title" maxlength="250"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input class="${btnclass}" type="submit" value="创建类目"> 
						<span class="msg-span">&nbsp;&nbsp;&nbsp;&nbsp;*注：创建类目后需更新类目缓存</span>
					</td>
				</tr>
			</table>
		</form:form>
	</div>
</div>
</div>
</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
			jQuery.validator.setDefaults({});
			var catTreesValue = ${catTrees};
			
	        $(document).ready(function(){
	        	
				//表单数据校验
				jQuery("#form1").validate({
            rules: {
                newsCategoryName: {
                    required: true,
                    minlength: 2
                },
                seq: {
                    isNumber: true,
                },
                file:{
                	required: "#imgName:blank",
                	isImage:true
                },
            },
            messages: {
                newsCategoryName: {
                    required: "请输入栏目名称",
                    minlength: "栏目名称不能少于2个字"
                },
                seq: {
                    isNumber: "请输入5位之内的正整数!",
                }
               /* smallCatFile2:{
                	required:"请上传类目小图标图片",
                	isImage:"请选择正确的图片上传格式"
                },
                catFile2:{
                	required:"请上传类目图片",
                	isImage:"请选择正确的图片上传格式"
                },*/
            }
        });
        
        $("input[name=catIconFile]").change(function(){
			checkImgType(this);
			checkImgSize(this,1024);
		});
				
        $("input[name=catPicFile]").change(function(){
			checkImgType(this);
			checkImgSize(this,1024);
		});
				
				
        	var setting = { check:{}, data:{}, callback:{},view:{} };
    		setting.check.enable = true;
    		setting.check.chkboxType = {'Y':'ps','N':'ps'};
    		setting.check.chkStyle = 'radio';
    		setting.check.radioType = 'all';
    		setting.check.enable = false;
    		setting.view.dblClickExpand = false;
    		setting.callback.onClick = clickNode;
    		setting.data.simpleData =  { enable: true };
    		
    		$.fn.zTree.init($("#catTree"), setting, catTreesValue);
    		  
    		//expandAll();
        	 $(".J_spu-property").divselect();
        	 
        	var treeObj = $.fn.zTree.getZTreeObj("catTree");
			var node = treeObj.getNodeByTId('${category.id}');
			
			/* $("#headerMenu1").change(function(){
			    var headerMenu = $("#headerMenu1").val();
			    if(headerMenu=='true'){
			    	$("#headerMenu_span").css("display","block");
			    }else{
			    	$("#headerMenu_span").css("display","none");
			    }
			}); */
	    		
	        });
	        
			function deleteCategory() {
				var catname = $("#catname").val();
				var catid= $("#catid").val();
				layer.confirm("确定删除类目："+catname+" 吗？", {icon:3, title:'提示'}, function(){
					var zTree = $.fn.zTree.getZTreeObj("catTree");
					//var node = zTree.getNodeById(catid);
					var nodes = zTree.getSelectedNodes();
					if(nodes[0].isParent){
						layer.msg("请先删除子分类", {icon: 2});
					}else{
						// window.location = "<ls:url address='/admin/category/delete/" + catid + "'/>";
						 var url="${contextPath}/admin/category/delete/"+catid;
						 $.post(url,function(result){
						 	if(result=="ok"){
						 		layer.msg('删除成功', {icon: 1,time: 1500}, function(){
						 			window.location.reload(); 
						 		}); 
						 	}else{
						 		layer.msg("请先删除该分类下的商品", {icon: 2});
						 	}
						 },"json");
					}
				});
			}
			
	        function pager(curPageNO){
	            document.getElementById("curPageNO").value=curPageNO;
	            document.getElementById("form1").submit();
	        }
	        
	        function zhedie(){
	    		var treeObj = $.fn.zTree.getZTreeObj("catTree");
	    		treeObj.expandAll(false);
	    	}
	    	
	    	function expandAll() {
	    		var treeObj = $.fn.zTree.getZTreeObj("catTree");
	    		treeObj.expandAll(true);
	    	}
	    	
	    	function clickNode(event, treeId, treeNode) {
	        	var catId = treeNode.id;
	        	if(catId==0){
    		  		$("#catDetail").hide();
    		  		$("#newDetail").show();
	        	}else{
		        	$.ajax({
		    			url:"${contextPath}/admin/category/load/"+catId, 
		    			type:'get', 
		    			dataType : 'json', 
		    			async : true, //默认为true 异步   
		    			error: function(jqXHR, textStatus, errorThrown) {
		    				layer.alert('err', {icon: 2});
		    			},
		    			success:function(retData){
		    				$("#catid").val(retData.id);
		    		  		$("#catname").val(retData.name);
		    		  		//$("#catpic").val(retData.pic);
		    		  		if(retData.pic=="" || retData.pic=="null" ||retData.pic==null){
		    		  			$("#imgspan").html("");
		    		  		}else{
			    		  		$("#imgspan").html("<img src='"+"<ls:photo item='"+retData.pic+"'/>"+"' width='100' height='100' style='margin-right:10px;'>");
		    		  		}
		    		  		if(retData.icon=="" || retData.icon=="null" ||retData.icon==null){
		    		  			$("#iconspan").html("");
		    		  		}else{
			    		  		$("#iconspan").html("<img src='"+"<ls:photo item='"+retData.icon+"'/>"+"' width='30' height='30' >");
		    		  		}
		    		  		$("#catparent").html(retData.parentName);
		    		  		$("#catseq").val(retData.seq);
		    		  		$("#catsts").val(retData.status);
		    		  		$("#cat").val(retData.name);
		    		  		$("#catheader").val(retData.headerMenu+"");
		    		  		$("#catnavi").val(retData.navigationMenu+"");
		    		  		$("#catkey").val(retData.keyword);
		    		  		$("#catdesc").val(retData.catDesc);
		    		  		$("#returnValidPeriod").val(retData.returnValidPeriod);
		    		  		$("#cattitle").val(retData.title);
		    		  		if(retData.typeId!=null){
			    		  		$(".kui-combobox-caption").val(retData.typeName);
			    		  		$("#typeId").html("<option selected='selected' value='"+retData.typeId+"'></option>");
		    		  		}else{
		    		  			$(".kui-combobox-caption").val("");
			    		  		$("#typeId").html("<option selected='selected' value=''></option>");
		    		  		}
		    		  		$("#catDetail").show();
		    		  		$("#newDetail").hide();
		    			}
		    		});
	        	}
	    	}
	    	
	    	function editCategory(){
	    		$(".kui-combobox-caption").val("");
		  		$("#newTypeId").html("<option selected='selected' value=''></option>");
		  		$("#catDetail").hide();
		  		$("#newDetail").show();
	    	}
	    	
	    	function cleanCategoryCache(){
	    	   layer.confirm("确定清除前台缓存分类树？", {icon:3, title:'提示'}, function(index){
	    		       $.ajax({
	    					url:"${contextPath}/admin/category/cleanCategoryCache", 
	    					type:'post', 
	    					async : true, //默认为true 异步   
	    				 	error: function(jqXHR, textStatus, errorThrown) {
	    					 },
	    				 	success:function(result){
	    				   		layer.msg("清除成功", {icon:1,time:1500});
	    				 	}
	    				});
	    		       layer.close(index);
	    		});
	    	}
	    	
	    	//加载分类用于选择
	    	function loadCategoryDialog(){
	    		layer.open({
	    			title :"选择父类",
	    			id:"RoleMenu",
	    			type: 2, 
	    			content: "${contextPath}/admin/category/loadCategory",
	    			area: ['280px', '430px']
	    		});
	    	}
	    	
	    	function loadCategoryCallBack(id,catName){
				$("#parentId").val(id);
				$("#parentName").html("<b>" + catName + "</b>");
			}
	    	
	    	function checkNewCat(){
	    		$("#newDetail input[name=name]").val($.trim($("#newDetail input[name=name]").val()));
	    		var obj_file_1 = document.getElementById("smallCatFile2"); 
	    		var obj_file_2 = document.getElementById("catFile2");
	    		var returnValidPeriod = document.getElementById("returnValidPeriod");
	    		var flag = false;
	    		if($.trim($("#newDetail input[name=name]").val())==""){
	    			layer.msg("请输入类目名称", {icon:2});
	    		}else if($("#newDetail input[name=name]").val().length>20){
	    			layer.msg("类目名称不能超过10个字", {icon:2});
	    		}else if($("#parentId").val()==""){
	    			layer.msg("请选择父类", {icon:2});
	    		}else if($.trim($("#newDetail input[name=seq]").val())==""){
	    			layer.msg("请输入排序", {icon:2});
	    		}/*else if(obj_file_1.value==""){
	    			layer.msg("类目小图标不能为空", {icon:2});
	    		}else if(obj_file_2.value==""){
	    			layer.msg("类目图片不能为空", {icon:2});
	    		}*/else if(obj_file_1.files[0] && obj_file_1.files[0].size>1*1024*1024){
	    			layer.msg("类目小图标不能大于1M", {icon:2});
	    		}else if(obj_file_2.files[0] && obj_file_2.files[0].size>1*1024*1024){
	    			layer.msg("类目图片不能大于1M", {icon:2});
	    		}else{
	    			flag = false;
	    			var parentId = $("#parentId").val();
	    			$.ajax({
	    				url:contextPath+"/admin/category/ifCreate",
	    				data:{"parentId":parentId},
	    				type:'post',
	    				dataType:'json',
	    				async:false, //默认为true 异步   
	    				success:function(result){
	    					if(result=="OK"){
	    						layer.alert("创建成功", {icon:1});
	    						flag = true;
	    					}else{
	    						layer.msg(result, {icon:2});
	    					}
	    				}
	    			});
	    		}
	    		return flag;
	    	}
	    	
	    	function checkCatEdit(){
	    		$("#catname").val($.trim($("#catname").val()));
	    		var obj_file_1 = document.getElementById("smallCatFile1"); 
	    		var obj_file_2 = document.getElementById("catFile1");
	    		var returnValidPeriod = document.getElementById("returnValidPeriod");
	    		
	    		var flag = false;
	    		if($.trim($("#catname").val())==""){
	    			layer.msg("请输入类目名称", {icon:2});
	    		}else if($("#catname").val().length>10){
	    			layer.msg("类目名称不能超过10个字", {icon:2});
	    		}else if($.trim($("#catseq").val())==""){
					layer.msg("请输入排序", {icon:2});
	    		}else if(obj_file_1.files[0].size>1*1024*1024){
	    			layer.msg("类目小图标不能大于1M", {icon:2});
	    		}else if(obj_file_2.files[0].size>1*1024*1024){
	    			layer.msg("类目图片不能大于1M", {icon:2});
	    		}else{
	    			$("input[name=catIconFile]").change(function(){
 						checkImgType(this);
						checkImgSize(this,512);
						
 					});
	    			flag = true; 
	    		}
	    		return flag;
	    	}
	    	
	    	function searchType(obj){
	    		var value = {"typeName": $(obj).val() };
	    	       $.ajax({
	    				url:"${contextPath}/admin/category/getProdType", 
	    				type:'post', 
	    				data: value,
	    				async : true, //默认为true 异步   
	    				error: function(jqXHR, textStatus, errorThrown) {
	    			 		// layer.alert(textStatus, errorThrown);
	    				},
	    				success:function(result){
	    				 	$(obj).parent().next().html(result);
	    				}
	    				});
	    	}
	    	
	    	//查看类目下的商品集合链接
	    	function lookLink(themeId){
	    	    var catid=$("#catid").val();
		 		layer.open({
		 			anim: 2,
		 			  shadeClose: true, //开启遮罩关闭
		 			  skin: 'demo-class',
		 			  content: "pc端链接：${pcDomainName}/list?categoryId="+catid+"<br><br>手机端链接：${mobileDomainName}/m_search/list?categoryId="+catid
		 		});
		 	}

			function deleteImg(){
				var catId= $("#catid").val();
				layer.confirm("确定删除类目小图标？",function(){
					$.ajax({
						url:"${contextPath}/admin/category/deleteImg/"+catId,
						type:'post',
						dataType:'json',
						async : true, //默认为true 异步
						error: function(jqXHR, textStatus, errorThrown) {
							layer.alert("系统异常，请重试！",{icon:0});
						},
						success:function(result){
							if(result=="OK"){
								layer.alert("删除图片成功",{icon:1},function () {
									window.location.reload();
								});
							}else{
								layer.alert(result,{icon:2});
							}
						}
					});
				});
			}


</script>
</html>
