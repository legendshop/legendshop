<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<c:if test="${not empty products}">
			<c:forEach items="${products}" var="prod">
				<tr id="list_${prod.prodId}">
					<td>${prod.name}</td>
					<td><img src="<ls:photo item='${prod.pic}'/>" height="60px"  /></td>
					<td>${prod.price}</td>
					<td>
					<input type="hidden" value="${prod.prodId}" name="prods"/>
					<input type="button" value="移除"
						class="criteria-btn removeProd" /></td>
				</tr>
			</c:forEach>
</c:if>
