<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>订单管理- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
 <jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	     <table class="${tableclass} no-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border">订单管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">收到货款</span>
				</th>
	    	</tr>
	    </table>
 	<form:form action="${contextPath}/admin/order/receiveMoney" method="POST" id="form1">
       	<input id="subId" name="subId" type="hidden" value="${order.subId }"/>  
       <div align="center">
         <table  style="width: 100%" class="${tableclass} no-border content-table" id="col1">
		      <tr>
		        <td style="width:200px">
		          <div align="right" ><font color="ff0000">*</font>订单编号：</div>
		       </td>
		        <td align="left" style="padding-left: 2px;">${order.subNumber }</td>
		      </tr>
		       <tr>
		        <td>
		          <div align="right"><font color="ff0000">*</font>订单总额：</div>
		       </td>
		        <td align="left" style="padding-left: 2px;">￥ ${order.actualTotal }</td>
		      <tr>
		      <tr>
		        <td>
		          <div align="right"><font color="ff0000">*</font>付款方式：</div>
		       </td>
		        <td align="left" style="padding-left: 2px;">
		          	<select id="payId" name="payId">
		          		<ls:optionGroup type="select" required="true" cache="false" sql="SELECT pay_id,pay_type_name FROM ls_pay_type WHERE is_enable=1"/>  
		          	</select>
		        </td>
		      </tr>
		       <tr>
		        <td>
		          <div align="right"><font color="ff0000">*</font>付款时间：</div>
		       </td>
		        <td align="left" style="padding-left: 2px;">
		          <input name="payDate" size="21" id="payDate"  type="text"  placeholder="付款时间"/>
		        </td>
		      <tr>
		        <td valign="top">
		          <div align="right"><font color="ff0000">*</font>第三方支付平台交易号：</div>
		       </td>
		        <td align="left" style="padding-left: 2px;">
		           <input name="flowTradeNo" id="flowTradeNo" type="text" size="21" class="${inputclass}" />
		        	</br><span style="color: #999;">支付宝等第三方支付平台交易号</span>
		        </td>
		      </tr>
		      	<tr>
		      		<td></td>
		             <td align="left" style="padding-left: 2px;">
		                 <div>
		                 	 <a href="${contextPath}/admin/order/orderAdminDetail/${order.subNumber }" target="_blank">
		                 	 	<input type="button" class="${btnclass}" value="订单详情" onclick="window.location.href='${contextPath}/admin/order/orderAdminDetail/${order.subNumber }'"/>
		                 	 </a>
		                     <input type="submit" class="${btnclass}" value="保存"/>
		                     <input type="button" class="${btnclass}" value="返回" onclick="window.location='${contextPath}/admin/order/processing'" />
		                 </div>
		             </td>
		         </tr>
		     </table>
           </div>
        </form:form>
 <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<script type="text/javascript">
	var contextPath = '${contextPath}';
	$(function(){
		jQuery("#form1").validate({
	        rules: {
		            payDate: {
		                required: true,
		            },
		            flowTradeNo: {
		            	required:true
		            },
	        },
	        messages: {
	            payDate: {
	                required: "请选择付款时间",
	            },
	            flowTradeNo: {
	                required: "请输入支付宝等第三方支付平台交易号"
	            },
	        }
    	});
		
		laydate.render({
	   	     elem: '#payDate',
	   	     calendar: true,
	   	     theme: 'grid',
	 		 trigger: 'click'
	      });
	})
</script>
</body>
</html>