<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>图片管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.order_img_name{
	text-overflow: -o-ellipsis-lastline;
	overflow: hidden;
	text-overflow: ellipsis;
	display: -webkit-box;
	-webkit-line-clamp: 2;
	line-clamp: 2;
	-webkit-box-orient: vertical;
	max-height: 36px;
	line-height: 18px;
	word-break: break-all;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	<table class="${tableclass} title-border" style="width: 100%">
	    <tr>
	        <th class="title-border">PC首页装修&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">轮播图管理</span></th>
	    </tr>
    </table>
     <form:form id="form1" action="${contextPath}/admin/indexjpg/query">
         <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
         <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
         <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
         <div class="criteria-div" style="padding:20px 0 0 0 !important;">
			 <span class="item" Style="padding-left:10px;">
         		标题：<input type="text" name="title" id="title" maxlength="50" value="${indexJpg.title}" class="${inputclass}"/>
		         <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
		         <input type="button" class="${btnclass}" value="创建首页轮播图" onclick='window.location="${contextPath}/admin/indexjpg/load"'/>
		     </span>
     </form:form>
<div align="center" class="order-content-list">
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/indexjpg/query" id="item"
                   export="false" class="${tableclass}" style="width:100%;" sort="external">
        <display:column title="顺序" class="orderwidth" style="width:100px;"><%=offset++%></display:column>
        <display:column title="标题" style="width:300px">
        	<div class="order_img_name">${item.title}</div>
        </display:column>
        <display:column title="图片" style="width:265px"><a href="<ls:photo item='${item.img}'/>" target="_blank"><img src="<ls:photo item='${item.img}'/>" height="145" width="265"/></a></display:column>
        <display:column title="次序" property="seq" sortable="true" sortName="seq" style="width:100px"></display:column>
        <display:column title="操作" media="html" style="width:250px">
           <div class="table-btn-group">
                 <c:choose>
                     <c:when test="${item.status == 1}">
                         <a class="tab-btn" href="javascript:void(0);" name="statusImg" itemId="${item.id}" itemName="${item.title}" status="${item.status}">下线</a>
                         <span class="btn-line">|</span>
                     </c:when>
                     <c:otherwise>
                         <a class="tab-btn" href="javascript:void(0);" name="statusImg" itemId="${item.id}" itemName="${item.title}" status="${item.status}">上线</a>
                         <span class="btn-line">|</span>
                     </c:otherwise>
                 </c:choose>

                 <a class="tab-btn" href="javascript:void(0);" onclick="window.location.href='${contextPath}/admin/indexjpg/update/${item.id}'">编辑</a>
				<span class="btn-line">|</span>
                 <a class="tab-btn"  href="javascript:void(0);" onclick="deleteById('${item.id}');">删除</a>
           </div>
        </display:column>
    </display:table>
    <div class="clearfix">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
      	</div>
     </div>
    <table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
        <tr>
            <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
                1、 轮播图显示在商城首页，菜单下面。<br>
                2、 轮播图有效显示数最大为7个，多于的不显示。<br>
                3、 轮播图支持的格式为图片格式，包括GIF动态图片。
            </td>
        <tr>
    </table>
</div>
</div>
</body>

<script  type="text/javascript">
    function deleteById(id) {
    	layer.confirm('确定删除？', {icon:3, title:'提示'}, function(){
    		window.location.href = "${contextPath}/admin/indexjpg/delete/" + id;
    	});
    }
    $(document).ready(function () {
        $("a[name='statusImg']").click(function (event) {
            $this = $(this);
            initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/indexjpg/updatestatus/", $this, "${contextPath}");
        });
        highlightTableRows("item");
    }); 
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>

