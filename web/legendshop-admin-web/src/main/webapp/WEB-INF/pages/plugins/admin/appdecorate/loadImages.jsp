 <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<html>
<head>
	<style type="text/css">
		*{
		    font-size: 14px;
		}
		.fields-box {
				    overflow: hidden;
				    padding-left: 1px;
				}
		.multimage {
		    margin-right: 10px;
		}
		.fieldset {
		    margin-bottom: 8px;
		    overflow: hidden;
		}
		
		.multimage .multimage-wrapper {
		    border: 1px solid #ececec;
		}
		
		.multimage .multimage-tabs {
		    background-color: #f8f8f8;
		    padding: 5px 20px 0;
		    border-bottom: 1px solid #ececec;
		}
		
		
		.multimage .multimage-tabs .tab {
		    border: 1px solid #ececec;
		    cursor: pointer;
		    display: inline-block;
		    *display:inline;
		    zoom:1;
		    margin-right: 10px;
		    padding: 8px 20px 5px;
		    border-bottom:0;
		    margin-bottom:-1px;
		}
		
		.multimage .multimage-tabs .actived {
		    background-color: #fff;
		    border-bottom: 1px solid #fff;
		    border-top: 2px solid #999;
		    margin-top: 1px;
		}
		.multimage .multimage-panels {
		    background-color: #fff;
		}
		
		.multimage .multimage-panels .local-panel {
		    padding-bottom: 30px;
		    padding-top: 30px;
		}
		
		.multimage .multimage-panels .local-panel .upload-field {
		    margin-left: 80px;
		    margin-bottom: 50px;
		}
		
		.multimage .multimage-panels .local-panel .upload-field label {
		    display: inline-block;
		    *display:inline;
		    zoom:1;
		    float: none;
		    padding: 0;
		    width: 100px;
		    font-size: 12px;
		}
		.multimage .multimage-panels .local-panel .upload-field a {
		    font-size: 12px;
		}

	</style>
</head>
<body>
        <div class="fieldset multimage">
		<div class="fields-box">
			<div class="multimage-wrapper">
				<div class="multimage-tabs">
					<div class="tab actived">本地上传</div>
					<div type="remote-image" class="tab">图片空间</div>
				</div>
				<div class="multimage-panels">
					<div class="panel local-panel" style="display: block;">
						<div class="upload-field">
							<label>选择本地图片：</label> <input type="file" id="file" name="files" multiple="multiple" onchange="javascript:uploadFile()" >
						</div>
						<div class="multimage-tips">
							<div class="tip-title">提示：</div>
							<ol>
							    <li style="color: red;">鼠标点击就可以选择图片信息</li>
								<li style="color: red;">本地上传图片大小不能超过<strong class="bright">500K</strong>。</li>
							</ol>
						</div>
					</div>
					<div class="panel remote-image hide" style="display: none;"><iframe style="height:330px;width:810px;border:0px;" src="/legendshop/imageAdmin/remoteImages"></iframe></div>
				</div>
			</div>
		</div>
	  </div>
	  <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/loadImages.js'/>"></script>
		<script language="JavaScript" type="text/javascript">
			var contextPath="${contextPath}";
			var appendCsrfInput=false;
	        var showsize ="${showsize}"; 
	        var sourceId ="${sourceId}";
	        var photoPath = "<ls:photo item=''/>"; 
		</script>
</body>
</html>