<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/seckillActivity/query"
		id="item" export="false" sort="external" class="${tableclass}"
		style="width:100%">
		<display:column title="顺序" class="orderwidth"><%=offset++%>
		</display:column>
		<display:column title="活动名称" property="seckillTitle"
			style="width:20%"></display:column>
		<display:column title="店铺名称">
			<a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.shopName}</a>
		</display:column>
		<display:column title="开始时间" property="startTime"
			format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"></display:column>
		<display:column title="结束时间" property="endTime"
			format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"></display:column>
		<display:column title="活动状态">
			<c:choose>
				<c:when test="${item.status eq 2 || item.status eq 3 || item.status eq 4}">
					已完成
				</c:when>
				<c:when test="${item.status eq 1 && item.endTime lt nowDate}">
					   已过期
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
						   商品已删除
						</c:when>
						<c:when test="${item.prodStatus eq 0}">
						   商品已下线
						</c:when>
						<c:when test="${item.prodStatus eq 2}">
						  商品违规下线
						</c:when>
						<c:when test="${item.prodStatus eq 3}">
						  商品待审核
						</c:when>
						<c:when test="${item.prodStatus eq 4}">
						  商品审核失败
						</c:when>
						<c:when test="${item.status==-2}">
						   审核未通过
						</c:when>
						<c:when test="${item.status==-1}">
						   审核中
						</c:when>
						<c:when test="${item.status==0}">
						   已失效
						</c:when>
						<c:when test="${item.status==1}">
							<c:if test="${item.startTime gt nowDate }">未开始</c:if>
							<c:if test="${item.startTime lt nowDate }">进行中</c:if>
						</c:when>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</display:column>

		<display:column title="操作" media="html">
				<div class="table-btn-group">

					<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/seckView/${item.id}'">
						查看
					</button>

					<c:if test="${item.status ne -1 || item.status ne -2}">
						<span class="btn-line">|</span>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/operatorSeckillActivity/${item.id}'">
							运营
						</button>
					</c:if>
					<c:choose>
						<%-- 上线状态  --%>
						<c:when test="${item.status eq 1}">
							<%--未开始 终止  --%>
							<c:if test="${item.startTime gt nowDate }">
								<span class="btn-line">|</span>
								<button class="tab-btn" id="statusImg" onclick="offlineSeckillActivity('${item.id}')">
									终止
								</button>
							</c:if>
							<c:if test="${nowDate > item.startTime && nowDate < item.endTime}">
								<span class="btn-line">|</span>
								<button class="tab-btn" id="statusImg" onclick="offlineSeckillActivity('${item.id}')">
									终止
								</button>
							</c:if>
						</c:when>
						<c:when test="${item.status eq 0 || item.status eq 2}">
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick=" Delete('${item.id}','${item.seckillTitle}')">
								删除
							</button>
						</c:when>
					</c:choose>

					<%--<ls:plugin pluginId="b2c">
						<c:choose>
							&lt;%&ndash; 审核通过 下线状态  &ndash;%&gt;
							<c:when test="${item.status lt 1}">
								&lt;%&ndash; 未过期 ：可以修改  &ndash;%&gt;
								<c:if test="${item.endTime gt nowDate }">
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/update/${item.id}'">
										编辑
									</button>
									<span class="btn-line">|</span>
								</c:if>
							</c:when>
							&lt;%&ndash; 上线状态:活动日期之前可修改，已到活动日期不可修改  &ndash;%&gt;
							<c:when test="${item.status eq 1}">
								<fmt:formatDate value="${item.startTime}"
									pattern="yyyy-MM-dd" var="item_time" />
								<c:if test="${item_time gt nowDay}">
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/update/${item.id}'">
										编辑
									</button>
									<span class="btn-line">|</span>
								</c:if>
							</c:when>
						</c:choose>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/seckView/${item.id}'">
							查看
						</button>
						<!-- 过期：可删除 -->
						<c:if test="${item.endTime lt nowDate }">
							<button class="tab-btn" onclick=" Delete('${item.id}','${item.seckillTitle}')">
								删除
							</button>
							<span class="btn-line">|</span>
						</c:if>
					</ls:plugin>
					<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/seckView/${item.id}'">
						查看
					</button>
					<c:if test="${item.status ne -1 && item.status ne -2}">
					<button class="tab-btn" onclick="window.location='${contextPath}/admin/seckillActivity/operatorSeckillActivity/${item.id}'">
						| 运营
					</button>
					</c:if>--%>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
