<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/report/salerank"
		id="item" export="false" sort="external" class="${tableclass}"
		style="width:100%">
		<display:column title="顺序" class="orderwidth" style="width:60px"><%=offset++%></display:column>
		<display:column title="店铺" property="shopName"></display:column>
		<display:column title="销售数量" style="width:200px"
			property="salesOrder" sortName="salesOrder" sortable="true"></display:column>
		<display:column title="销售金额" style="width:200px"
			sortName="salesAmount" sortable="true">
			<fmt:formatNumber type="currency" value="${item.salesAmount}"
				pattern="${CURRENCY_PATTERN}" />
		</display:column>
	</display:table>
	<div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 				 </div>
   			 </div>
   		</div>
   	</div>
</html>