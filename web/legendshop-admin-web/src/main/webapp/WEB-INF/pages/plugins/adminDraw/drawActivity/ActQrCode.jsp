<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/qrcode/jquery.qrcode.min.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/zclip/jquery.zclip.min.js'/>"></script>
<style type="text/css">
body{
  background-color: #fff;
}
.criteria-btn {
    color: #ffffff;
    border: 1px solid #cccccc;
    background-color: #9EA4A7;
    cursor: pointer;
    height: 28px;
    margin: 3px;
    padding-left: 12px;
    padding-right: 12px;
    text-align: center;
}

.criteria-field {
    padding: 0.1em;
    font-size: 1.2rem;
    line-height: 1.2;
    color: #555555;
    vertical-align: middle;
    border: 1px solid #cccccc;
}
</style>

    
	
	<script type="text/javascript">
		$(document).ready(function(){
			//  jQuery('#qrcode').qrcode({width: 200,height: 200,text: "${mobileDomainName}${contextPath}/admin/drawActivity/load/${itemId}"});  
			   jQuery('#qrcode').qrcode({width: 200,height: 200,text: "${mobileDomainName}/p/weixin/draw/drawPage/${itemId}"}); 
			if(!isIE()){
				$('#copy_input').zclip({ 
				        path: '<ls:templateResource item='/resources/plugins/zclip/ZeroClipboard.swf'/>', 
				        copy: function(){//复制内容 
				            return $("#actLink").val(); 
				        }, 
				        afterCopy: function(){//复制成功 
				            $("#success").html('复制成功'); 
				        } 
		       }); 
			}else{
				$('#copy_input').on("click",function(){
					window.clipboardData.setData("Text",$("#actLink").val());
					$("#success").html('复制成功'); 
				});
			}
		});
		function isIE() { //ie?
		   if (!!window.ActiveXObject || "ActiveXObject" in window)
		          return true;
		   else
		          return false;
		}
	</script>
	<body>
		<div class="info" style="margin-top: 15px;">
			<div style="float: left;text-align: center;">
			      <div id="qrcode" style="display: inline-block;"></div><br/>
			    	<span>微信扫一扫</span>
			  </div>
		    <div style="float: left;margin-left: 30px;">
		    	<span><b>链接地址：</b></span><br/>
			    <p>
				    <input type="text" name="actLink" id="actLink" value="${mobileDomainName}/p/weixin/draw/drawPage/${itemId}" class="criteria-field"/>
				   	<input type="button" value="复制" id="copy_input" class="criteria-btn"/>
				   <!-- 	<a href="#" id="copy_input" class="copy" >复制</a>  -->
			   	</p>
			   	<span style="color: #ccc;font-size: 12px;">可将链接复制到您的公众号菜单中</span>
<!-- 			   	margin-left:64px; -->
			   	<span id="success" style="color: #0FADEF;font-size: 14px;"></span>
			   	
		    </div>
	   	</div>
	</body>