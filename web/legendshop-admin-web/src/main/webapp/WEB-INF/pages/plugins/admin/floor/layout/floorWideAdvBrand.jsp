<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
  <head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/indexlayout.css'/>" rel="stylesheet"/>
    <title>楼层编辑界面</title>

     <style type="text/css">
     	.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}
     </style>
  </head>
  <body >
  	<form:form  action="${contextPath}/admin/system/floor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 98%;margin: 10px" >
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo;
				    	<a href="<ls:url address='/admin/floor/query'/>">广告+品牌楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
	    </table>
	  </form:form>
	     <div id="doc">
	      <div id="bd">

	           <div class="wide_brand" mark="wide_adv_brand" style="">
			    <div class="wide_brand_c">
<%--					<strong><a href="${contextPath}/allbrands">全部品牌 &gt;</a></strong>--%>
			      <div class="wide_brand_c_t"><b style="width:175px;margin-left:36px;overflow: hidden;">${floor.name}</b></div>
			      <div class="wide_brand_c_m">
			        <div class="wide_brand_c_m_left">
			           <div id="advEdit" style="width: 310px; height: 25px; line-height: 25px;" class="flooredit"><a href="javascript:onclickAdv('${floor.flId}','${adv.fiId}');" style="color:white;">广告编辑(310*421)</a></div>
			           <c:if test="${not empty adv}" >
							   <img src="<ls:photo item='${adv.picUrl}'/>" width="310" height="421">
				        </c:if>
			         </div>
			        <div class="wide_brand_c_m_right">
			            <div id="brandEdit" style="width: 100%; height: 25px; line-height: 25px; " class="flooredit"><a href="javascript:onclickBrand('${floor.id}');" style="color:white;">品牌编辑</a></div>
			            <div class="recommend-brand" >

			            </div>
			        </div>
			      </div>
			    </div>
			  </div>
	          <div   style="text-align: center;clear: both; padding-top : 30px;" >
	          	      <input type="button" onclick="window.location='${contextPath}/admin/floor/query' " class="criteria-btn" value="返回">
	     		 </div>
	      </div>
     </div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorWideAdvBrand.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var _flId = "${floor.flId}";
</script>
</html>
