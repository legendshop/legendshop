<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css" />

<title>编辑团购</title>
</head>
<body>

<form:form  action=" " method="post" id="theForm">
<div class="box_floor">
  <div class="box_floor_six">
    <div class="box_floor_class">
      <b>楼层标题：</b>
      <input name="gf_name" type="text" id="gf_name" value="今日团购"  readonly="readonly"/>
    </div>
    <span class="floor_choose">已选推荐商品：</span>
    <div class="box_floor_prodel"> <em class="floor_warning">注释：选择1件团购商品，双击删除已有商品信息</em>
      <div class="floor_box_pls" id="floor_goods_info">

        <ul ondblclick="jQuery(this).remove();" class="floor_pro">
          <li class="floor_pro_img">
          	<img  id="${group.referId}" title="${group.sortName}" src="<ls:images item='${group.pic}' scale='2' />" />
		  </li>
        </ul>
		
        </div>
    </div>
    <span class="floor_choose">选择要展示的商品：</span>
    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
    <div class="floor_choose_box"> 
    <span class="floor_choose_sp">
    <b>选择分类：</b>
      <lb:allSortOption id="sort" type="G" />
      <b>团购商品名称：</b>
      <input name="groupName" type="text" id="groupName" />
      <input type="button"  value="搜索" onclick="groupFloorLoad(${curPageNO});" style="cursor:pointer;" />
    </span> 
      <em class="floor_warning">注释：点击商品加入楼层，选择1件团购商品</em>
     <div id="floor_goods_list">
     	
     </div>
    </div>
  </div>
  <!--图片开始-->
  <div class="submit" style="text-align:center;">
    &nbsp;<input type="button" value="保存" onclick="save_form( );" />
  </div>
</div>
</form:form>

</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/groupFloorOverlay.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var image = "<ls:images item='' scale='2' />";
</script>
</html>