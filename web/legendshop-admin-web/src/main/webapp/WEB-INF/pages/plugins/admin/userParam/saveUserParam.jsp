<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
   
   <div class="area_box"  style="width:400px; margin: 10px; ">
       <div class="area_box_title" style="width:380px;">
        <span class="area_box_title_left">保存参数模板</span>
        <span class="area_box_title_right"><a href="javascript:void(0);" onclick="javascript:jQuery('.area_box').remove();">×</a></span>
    </div>
      <div class="area_bg_white" style="width:360px; margin: 20px;">
	   <div class="headbox" style="width:360px; height:50px;font-size: 12pt">保存自定义参数模板</div>
	   <div class="tpl-form" style="width:360px; height:50px">
	   		<label for="">模板名字：</label>
	   		<input type="text" class="btn-default" maxlength="10" id="userParam">
	   		<span class="darkish">最多10个汉字</span>
	   </div>
	   <div class="optbar" style="text-align:center;">
	   		<a href="javascript:saveUserParam();" class="btn"><input id="btn_keyword" class="bti" type="button" name="" value="保存"></a>
	   		<a href="javascript:void(0);" onclick="javascript:jQuery('.area_box').remove();">取消</a>
	   	</div>
	   	</div>
  </div>

