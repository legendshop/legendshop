<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">店铺促销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">满减满折</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
                        <li <c:if test="${empty marketing.searchType || marketing.searchType eq 'ALL'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=ALL"/>">全部活动</a></li>
                        <li <c:if test="${marketing.searchType eq 'NO_PUBLISH'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=NO_PUBLISH"/>">未发布</a></li>
                        <li <c:if test="${marketing.searchType eq 'NOT_STARTED'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=NOT_STARTED"/>">未开始</a></li>
                        <li <c:if test="${marketing.searchType eq 'ONLINE'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=ONLINE"/>">进行中</a></li>
                        <li <c:if test="${marketing.searchType eq 'PAUSE'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=PAUSE"/>">已暂停</a></li>
                        <li <c:if test="${marketing.searchType eq 'FINISHED'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=FINISHED"/>">已结束</a></li>
                        <li <c:if test="${marketing.searchType eq 'EXPIRED'}">class="am-active"</c:if>><i></i><a
                                href="<ls:url address="/admin/marketing/mansong/query?searchType=EXPIRED"/>">已失效</a></li>
					</ul>
				</div>
			</div>

			<form:form action="${contextPath}/admin/marketing/mansong/query" id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
					<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<input type="hidden" id="searchType" name="searchType" value="${marketing.searchType}" />
					<span class="item">
						<input type="text" id="shopId" name="shopId" value="${marketing.shopId}"/>
					</span>
						<input type="hidden" id="siteName" name="siteName" value="${marketing.siteName}" class="${inputclass}" /> 
					<span class="item">
						<input type="text" id="marketName"  name="marketName" maxlength="50" value="${marketing.marketName}" class="${inputclass}" placeholder="请输入活动名称搜索" /> 
					<input type="hidden" id="type" name="type" value="${marketing.type}" />
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
						<ls:plugin pluginId="b2c">
							<input type="button" value="添加活动"
								onclick="window.location='${contextPath}/admin/marketing/addMsMarketing'" class="${btnclass}" />
						</ls:plugin>
					</span>
				</div>
			</form:form>
			<div align="center" id="mansongContentList"name="mansongContentList" class="order-content-list"></div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1.用于满减促销活动信息展示<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var state = "${marketing.state}";
	var returnShopName = "${marketing.siteName}";
     function getCheckedBox(){
    	var checkedMansong=$("input[type=checkbox][name=mansongIds][state=0]:checked");
    	var checkedMansongs=new Array();
    	checkedMansong.each(function(index, domEle){
    		var mansongId=$(domEle).val();
    		checkedMansongs.push(mansongId);
    	});
    	return checkedMansongs.toString();
     }
     
     function batchDel(){
    	var mansongIds=getCheckedBox();
    	if(mansongIds==""){
    		layer.msg("请选择要删除的营销活动。", {icon: 2});
    		return;
    	}
    	layer.confirm("确定要删除吗？", {
    		icon: 3,btn: ['确定','取消'] //按钮
    	}, function(){
    		/* var reloadUrl=$(".am-nav-tabs li[class=am-active] a").attr("href"); */
    		var shopId = $("#shopId").val();
    		var siteName = $("#siteName").val();
    		var reloadUrl= "${contextPath}/admin/marketing/mansong/query?shopId="+shopId+"&state="+state+"&siteName="+siteName;
        	jQuery.ajax({
                url: "${contextPath}/admin/marketing/batchDetele/"+mansongIds,
                type: 'get',
                async: false, //默认为true 异步  
                dataType: 'json',
                success: function (data) {
                    if (data== "OK") {
                    	layer.msg("批量删除成功。", {icon:1, time:700}, function(){
    	                	setInterval(function(){
    	                		location.href=reloadUrl;
    	                		
    	                		//回显店铺名
    	                    	if(returnShopName == ''){
    	                            makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
    	                        }else{
    	                        	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",returnShopName, 'siteName');
    	                    		$("#select2-chosen-1").html(returnShopName);
    	                        } 
    	                	},1000);
                    	});
                    }else{
                    	layer.msg("批量删除失败。", {icon: 2});
                    }
                }
            });
    	});
    	
    };
     
	  function deleteById(id,shopId) {
		  layer.confirm('确定删除？', {icon:3}, function(){
		  		window.location = "${contextPath}/admin/marketing/deleteShopMarketing/1/"+id+"/"+shopId+"?state="+state;
		  });
	   }
	  
	  function search() {
			$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			sendData(1);
		}
	  
       $(document).ready(function(){
        	$("button[name='statusImg']").click(function(event){
        		$this = $(this);
        		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/pub/updatestatus/", $this,"${contextPath}");
     			 }
     		 );
        	var shopName = $("#siteName").val();
        	if(shopName == ''){
                makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
            }else{
            	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'siteName');
        		$("#select2-chosen-1").html(shopName);
            } 
        	sendData(1);
        });
       
       function sendData(curPageNO) {
    	    var data = {
    	        "shopId": $("#shopId").val(),
    	        "curPageNO": curPageNO,
				"searchType": $("#searchType").val(),
    	        "marketName": $("#marketName").val(),
    	    };
    	    $.ajax({
    	        url: "${contextPath}/admin/marketing/mansong/queryContent",
    	        data: data,
    	        type: 'post',
    	        async: true, //默认为true 异步   
    	        success: function (result) {
    	            $("#mansongContentList").html(result);
    	        }
    	    });
    	}
       
       function pager(curPageNO){
           document.getElementById("curPageNO").value=curPageNO;
           sendData(curPageNO);
       }
          
       function onlineShopMarketing(_id){
	    layer.confirm("确定要发布该活动麽？", {icon: 3}, function () {
	       $.ajax({
				url: "${contextPath}/admin/marketing/onlineShopMarketing/1/"+_id, 
				type:'post', 
				async : false, //默认为true 异步   
				"dataType":"json",
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(retData){
				   if(retData=="OK"){
				     window.location.reload(true);
				     return;
				   }else{
				      layer.alert(retData, {icon:2});
				      return;
				   }
				}
			});
		});
	}
		
	function offlineShopMarketing(_id){
     	layer.confirm("确定要下线该活动麽？", {icon:3}, function () {
			window.location.href="${contextPath}/admin/marketing/offlineShopMarketing/1/"+_id+"?state="+state;
		});
	}	
		
	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	</script>
</html>
