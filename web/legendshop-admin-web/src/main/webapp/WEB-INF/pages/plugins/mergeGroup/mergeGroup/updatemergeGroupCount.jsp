<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
	.mergeGroupCount {
		float:left;
	    font-size: 14px;
	    width: 130px;
	    height: 25px;
	    line-height: 25px;
	    margin-top:2px;
	    color: #555555;
	    vertical-align: middle;
	    background-color: #ffffff;
	    background-image: none;
	    border: 1px solid #cccccc;
	}
	
	.criteria-btn {
	    color: #ffffff;
	    border: 1px solid #0e90d2;
	    background-color: #0e90d2;
	    border-color: #0e90d2;
	    cursor: pointer;
	    height: 26px;
	    margin: 3px;
	    padding-left: 12px;
	    padding-right: 12px;
	    text-align: center;
	    display: block;
	    text-decoration: none;
	}
	table {
		width:100%;
		margin: 30px 0;
	}
	.empty-tips {
		color: green;
	    text-align: center;
	    margin-top: 50px;
	    font-size: 22px;
	}
</style>
</head>
<body>
	<div>
		<table>
			<tr>
				<td align="right" width="37%">成团订单数: </td>
				<td align="left" valign="middle" width="63%">
					<input class="mergeGroupCount" type="text" name="count" id="count" value="${count}" maxlength="5"/>
					<input type="hidden" id="id" name="id" value="${id}">
					<a href="javascript:void(0)" class="criteria-btn" style="float:left;width: 30px;font-size: 12px;line-height: 26px;" id="up-grade">保存</a>
				</td>
			<tr>
		</table>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	var  contextPath = "${contextPath}";
	var id = $("#id").val();
	$(function(){
		$("#up-grade").on("click", function(){
			var count = $("#count").val();
			var re =  /^[1-9]+[0-9]*]*$/;
			if(isBlank(count)) {
				art.dialog.tips("成团订单数不能为空");
				return false;
			}else if(!re.test(count)){
				art.dialog.tips("成团订单数为正整数");
				return false;
			}
			jQuery.ajax({
				url :  contextPath + "/admin/mergeGroup/updateCount",
				data : {
					"count" : count,
					"id" : id,
				},
				type : 'post',
				async : false, //默认为true 异步   
				dataType : 'json',
				error : function(jqXHR, textStatus, errorThrown) {
					layer.msg("网络异常，请联系管理员");
				},
				success : function(data) {
					if(data.status == "OK") {
						layer.msg(data.msg);
						setTimeout(function(){
							parent.location.reload();
						}, 1500);
					}
				}
			});
			
		});
	});
	
	/**判断是否为空**/
	function isBlank(_value) {
		if (_value == null || _value == "" || _value == undefined) {
			return true;
		}
		return false;
	}
</script>
</html>
