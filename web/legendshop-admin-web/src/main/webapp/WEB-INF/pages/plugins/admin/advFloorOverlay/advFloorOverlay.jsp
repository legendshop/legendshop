<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css" />

<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<title>编辑团购</title>
</head>
<body>
<form:form  action="" method="post" enctype="multipart/form-data" id="theForm">
<div class="box_floor">
  <div class="floor_advertisment"> <em class="floor_warning">注释：可以上传广告图片也可以使用商城广告</em>
    <ul class="floor_adv">
      <li>
       <b>
       选择类型：</b> 
      <span class="floor_adv_sp">
      <i><input name="type" type="radio" id="type" value="user" checked="checked" />图片上传</i>
      <i><input name="type" type="radio" id="type" value="adv" />商城广告</i>
      </span> 
      </li>
      <li id="user_adv_img"> 
      <b>广告图片上传：</b> 
      <span class="floor_adv_sp">
        <input name="file_val"  type="text" class="floor_file_txt" id="file_val" />
        <input  type="file"  size="33" class="flooor_file" id="img" name="img"/>
        </span>
        <i class="floor_adv_i">建议上传156*156像素图片</i> 
      </li>
      <li id="user_adv_url"> <b>广告链接：</b>
       <span class="floor_adv_sp">
        <input name="adv_url" type="text"class="floor_file_txt2" id="adv_url" />
        </span>
      </li>
       <li id="shop_adv" style="display:none;"> <b>广告选择：</b>
       <span class="floor_adv_sp">
       	<select name="adv_id" id="adv_id">
        </select>
        </span>
        <i class="floor_adv_i">仅支持156*156图片广告位</i> 
      </li>
    </ul>
  </div>
    <div class="submit" style="text-align: center;">
    <input name="" type="submit" value="保存"  />
    </div>
  </div>
  </form:form>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script>
jQuery(document).ready(function(){
  jQuery(":radio").click(function(){
     if(jQuery(this).val()=="user"){
	   jQuery("li[id^=user_adv_]").show();
	   jQuery("#shop_adv").hide();
	 }else{
	   jQuery("li[id^=user_adv_]").hide();
	   jQuery("#shop_adv").show();	 
	 }
  });
  jQuery("#img").change(function(){
     jQuery("#file_val").val(jQuery(this).val());
  });
});

  	function save_form(){
  	}
</script>
</body>
</html>