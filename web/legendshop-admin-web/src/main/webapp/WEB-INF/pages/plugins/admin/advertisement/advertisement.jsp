<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>公告编辑 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

			<form:form action="${contextPath}/admin/advertisement/save" method="post" id="form1" enctype="multipart/form-data">
				<input id="id" name="id" value="${bean.id}" type="hidden">
				<div align="center">
					<table class="${tableclass}" style="width: 100%">
						<thead>
							<tr>
								<th><strong class="am-text-primary am-text-lg">商城管理</strong>
									/ 广告编辑</th>
							</tr>
						</thead>
					</table>
					<table align="center" class="${tableclass}" id="col1">

						<tr>
							<td width="28%">
								<div align="right">
									广告类型: <font color="ff0000">*</font>
								</div>
							</td>
							<td><select id="type" name="type">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ADVERTISEMENT_TYPE" selectedValue="${bean.type}" />
							</select></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									链接地址<font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="linkUrl" id="linkUrl"
								value="${bean.linkUrl}" class="${inputclass}" size="50" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">标题</div>
							</td>
							<td><input type="text" name="title" id="title"
								value="${bean.title}" class="${inputclass}" size="50"
								maxlength="255" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">需求描述</div>
							</td>
							<td><input type="text" name="sourceInput" id="sourceInput"
								value="${bean.sourceInput}" class="${inputclass}" size="50"
								maxlength="255" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									状态<font color="ff0000">*</font>
								</div>
							</td>
							<td><select id="enabled" name="enabled">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ONOFF_STATUS" selectedValue="${bean.enabled}" />
							</select></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									上传广告图片(对联广告只能上传一张，大小为110*300) <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="file" name="file" id="file" size="50" /> <input
								type="hidden" name="picUrlName" id="picUrlName" size="80"
								class="${inputclass}" value="${bean.picUrl}" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="submit" value="保存" class="${btnclass}" />
									<input type="button" value="返回" class="${btnclass}"
										onclick="window.location='${contextPath}/admin/advertisement/query'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>

<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/advertisement.js'/>"></script>
<script type="text/javascript">
	$.validator.setDefaults({});
</script>

