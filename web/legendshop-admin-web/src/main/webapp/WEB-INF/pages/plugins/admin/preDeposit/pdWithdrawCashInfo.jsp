<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
label {
    display: inline-block;
    margin-bottom: 5px;
}
dt + dd {
    margin-top: .5em;
}
dd {
    margin-left: 0;
}
.dialog_title_icon {
    font-size: 16px!important;
    font-weight: normal!important;
    line-height: 24px!important;
    color: #333!important;
}
.dialog_form dl.row{
	font-size:0;
    color: #333;
    background-color: #FFF;
    margin-top: -1px;
    position: relative;
    z-index: 1;
}

.dialog_form dt.tit{
	text-align: right;
    width: 35%;
    padding-right: 2%;
    font-size: 12px;
    line-height: 24px;
    vertical-align: top;
    letter-spacing: normal;
    display: inline-block;
}
.dialog_form dd.opt{
    text-align: left;
    width: 50%;
    font-size: 12px;
    line-height: 13px;
    vertical-align: top;
    letter-spacing: normal;
    display: inline-block;
}
.dialog_content .bot{
	text-align: center;
    padding: 12px 0 10px 0 !important;
}
.dialog_content .changeState{
	    background-color: #1BBC9D;
    	border-color: #16A086;
    	color: #fff;
}
.dialog_content .changeState_big{
	text-decoration:none;
	font: bold 14px/20px "microsoft yahei", arial;
    color: #333;
    background-color: #ECF0F1;
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    padding: 7px 19px;
    border: solid 1px #BEC3C7;
    border-radius: 3px;
    cursor: pointer;
}
.dialog_form {
    width: 96%;
    margin: 0 auto;
    padding: 0;
    overflow: hidden;
}
.criteria-btn {
    color: #ffffff;
    border: 1px solid #0e90d2;
    background-color: #0e90d2;
    border-color: #0e90d2;
    cursor: pointer;
    height: 28px;
    padding-left: 12px;
    padding-right: 12px;
    text-align: center;
    line-height: 24px;
    margin: 0 3px;
    font-size: 12px;
    text-decoration: none;
} 
</style>
<div class="dialog_body" style="position: relative;width: 480px;height:auto;">
	
	<div class="dialog_content" style="margin: 0px;padding: 0px;width: 480px;height:auto;">
		<div class="dialog_form">
			<dl class="row">
				<dt class="tit">
					<label>提现流水号：</label>
				</dt>
				<dd class="opt">${withdrawCash.pdcSn}</dd>
			</dl>
			<dl class="row">
				<c:choose>
					<c:when test="${not empty withdrawCash.nickName}">
						<dt class="tit">
							<label>用户昵称：</label>
						</dt>
						<dd class="opt">${withdrawCash.nickName}</dd>
					</c:when>
					<c:otherwise>
						<dt class="tit">
							<label>用户名：</label>
						</dt>
						<dd class="opt">${withdrawCash.userName}</dd>
					</c:otherwise>
				</c:choose>
				
			</dl>
			<dl class="row">
				<dt class="tit">
					<label>提现金额(元)：</label>
				</dt>
				<dd class="opt">${withdrawCash.amount}元</dd>
			</dl>
			<dl class="row">
				<dt class="tit">
					<label>申请时间：</label>
				</dt>
				<dd class="opt"><fmt:formatDate value="${withdrawCash.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
			</dl>
			<dl class="row">
				<dt class="tit">
					<label>收款银行：</label>
				</dt>
				<dd class="opt">${withdrawCash.bankName}</dd>
			</dl>
			<dl class="row">
				<dt class="tit">
					<label>收款账号：</label>
				</dt>
				<dd class="opt">${withdrawCash.bankNo}</dd>
			</dl>
			<dl class="row">
				<dt class="tit">
					<label>开户人姓名：</label>
				</dt>
				<dd class="opt">${withdrawCash.bankUser}</dd>
			</dl>
			<c:if test="${withdrawCash.paymentState eq 1}">
				
				<dl class="row">
					<dt class="tit">
						<label>付款时间：</label>
					</dt>
					<dd class="opt"><fmt:formatDate value="${withdrawCash.paymentTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>操作管理员：</label>
					</dt>
					<dd class="opt">${withdrawCash.updateAdminName}</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>管理员备注：</label>
					</dt>
					<dd class="opt">${withdrawCash.adminNote}</dd>
				</dl>
			</c:if>
			<c:if test="${withdrawCash.paymentState ne 1}">
				<div class="bot">
					<button onclick="changePay('${withdrawCash.id}')" class="criteria-btn">更改支付状态</button>
				</div>
			</c:if>
		</div>
		<div style="clear:both; display:block;"></div>
	</div>
</div>
<script type="text/javascript">
function changePay(id){
	parent.location.href="${contextPath}/admin/preDeposit/pdWithdrawCashEdit/"+id;
}
</script>