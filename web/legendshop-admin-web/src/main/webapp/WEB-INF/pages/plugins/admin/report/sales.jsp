<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>报表管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<input type="hidden" id="" />
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">报表管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">销售额总览</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/report/sales" id="form1" method="post">
				<div class="criteria-div">
					<span class="item">
						店铺：
		        		<input type="text" id="shopId" name="shopId" value="${reportRequest.shopId}" />
	        		</span>
						<input type="hidden" id="shopName" name="shopName" id="shopName" maxlength="50" value="${reportRequest.shopName}" class="${inputclass}" /> 
					<span class="item">
						<select id="queryTerms" class="criteria-select" name="queryTerms" onchange="change(this.value)">
							<ls:optionGroup type="select" required="true" cache="true" beanName="SALE_QUERY_TERMS"
								selectedValue="${salesRequest.queryTerms}" />
						</select> 
					</span>
					<span class="item" id="range"> 
						<input readonly="readonly"name="selectedDate" id="selectedDate" type="text"
						value='<fmt:formatDate value="${salesRequest.selectedDate}" pattern="yyyy-MM-dd"/>' />
					</span>
					<span class="item" style="margin-left: -6px;"> 
						<input type="button" value="搜索" class="${btnclass}" id="submitOrder" />
					</span>
				</div>
			</form:form>
			<input type="hidden" id="salesResponseJSON" value='${reportJson}' />
			<div id="main" style="margin-left: 10px; width: 85%; height: 450px"></div>

		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/sales.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var selectedYear = '${salesRequest.selectedYear}';
	var selectedMonth = '${salesRequest.selectedMonth}';
	var selectedDay = '<fmt:formatDate value="${salesRequest.selectedDate}" pattern="yyyy-MM-dd"/>';
	var selectedWeek = '${reportRequest.selectedWeek}';
	
</script>
</html>
