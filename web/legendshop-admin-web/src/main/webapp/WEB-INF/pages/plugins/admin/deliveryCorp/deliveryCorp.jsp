<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>物流公司编辑 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
 </head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">配送管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/deliveryCorp/query"/>">物流公司管理</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">物流公司管理编辑</span>
			    	</th>
		    	</tr>
		</table>
	<form:form  action="${contextPath}/admin/deliveryCorp/save" method="post" id="form1">
		<input id="dvyId" name="dvyId" value="${deliveryCorp.dvyId}" type="hidden">
		<div align="center" style="margin-top: 10px;">
			<table border="0" align="center" class="${tableclass} no-border " id="col1">
				<tr>
					<td width="200px">
						<div align="right"><font color="ff0000">*</font>物流公司名称：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<input type="text" name="name" id="name" value="${deliveryCorp.name}"  size="50" class="${inputclass}" maxlength="20"/>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<font color="ff0000">*</font>公司网址：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<input type="text" name="companyHomeUrl" id="companyHomeUrl" value="${deliveryCorp.companyHomeUrl}"   size="50" class="${inputclass}" maxlength="50"/>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<font color="ff0000">*</font>公司编号：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
						<input type="text" name="companyCode" id="companyCode" value="${deliveryCorp.companyCode}" size="50" class="${inputclass}" />
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<font color="ff0000">*</font>物流接口：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;">
					  <input type="text"  name="queryUrl" id="queryUrl" value="${deliveryCorp.queryUrl}"   size="50"  class="${inputclass}" maxlength="150"/>
					</td>
				</tr>
				
				<tr>
					<td></td>
					<td align="left" style="padding-left: 2px;">
						<div>
							 <input type="submit" value="保存" class="${btnclass}" />
							 <input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/deliveryCorp/query"/>'" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
	</div>
	</div>
	</body>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
	 <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
        rules: {
        	name: {
                required: true,
            },
        	companyHomeUrl: {
                required: true,
            }
            ,
        	queryUrl: {
                required: true,
            }
        },
        messages: {
        	name: {
                required: "请输入物流公司名称!",
            },
        	companyHomeUrl: {
                required: "请输入物流公司网址!",
            },
        	queryUrl: {
                required: "请输入物流端口!",
            }
        }
    });
 
			highlightTableRows("col1");
          //斑马条纹
     	   $("#col1 tr:nth-child(even)").addClass("even");	  
});
</script>
</html>
