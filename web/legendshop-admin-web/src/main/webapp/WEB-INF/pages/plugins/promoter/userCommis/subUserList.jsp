<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<c:if test="${empty requestScope.list }">
	<div style="height:50px;line-height:50px;">没有找到相应记录</div>
</c:if>
 <display:table name="list" id="item"
         export="false" class="${tableclass}" style="width:100%">
      <display:column title="用户ID" property="userName" style="width:100px" ></display:column>
      <display:column title="用户昵称" property="nickName" style="width:100px"></display:column>
      <display:column title="注册时间" property="userRegTime" style="width:100px"></display:column>
      <display:column title="累积获得佣金" property="totalDistCommis" style="width:100px"></display:column>
</display:table>
<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
