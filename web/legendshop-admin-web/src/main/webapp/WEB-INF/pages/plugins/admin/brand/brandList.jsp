<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>品牌编辑 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
       <jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
		  	 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<table class="${tableclass} title-border" style="width: 100%">
			    <tr>
			        <th class="title-border">商品管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">品牌管理</span>
			        <%-- <a href="${contextPath}/admin/adminDoc/view/4"><img src="${contextPath}/resources/templets/img/adminDocHelp.png"/></a> --%>
			        </th>
			    </tr>
			</table>
			<div>
			    <div class="seller_list_title">
			        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
			            <li <c:if test="${empty brand.status}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/brand/query"/>">所有品牌</a></li>
			            <li <c:if test="${brand.status==-1}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/brand/query?status=-1"/>">待审核</a></li>
			            <li <c:if test="${brand.status==-2}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/brand/query?status=-2"/>">审核失败</a></li>
			        </ul>
			    </div>
			</div>
			<form:form action="${contextPath}/admin/brand/query" id="form1" method="get">
               <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
               <div class="criteria-div">
               	<span class="item">
                 	     	店铺：
                  	<input type="text" id="shopId" name="shopId" value="${prod.shopId}"/>
                   	<input type="hidden" id="shopName" name="shopName" value="${brand.shopName}" class="${inputclass}"/>
                    <input type="hidden" id="status" name="status" value="${brand.status}"/>
                   </span>
               	<span class="item">
              	     	品牌名称：
                   	<input type="text" id="brandName" name="brandName" class="${inputclass}" maxlength="50" value="${brand.brandName}"/>
                   </span>
                   <span class="item">
                   	推荐品牌：
                   	<select id="commend" name="commend" class="criteria-select">
				        <option value="">全部</option>
				        <option value="0" <c:if test="${brand.commend==0}">selected='selected'</c:if>>否</option>
				        <option value="1"<c:if test="${brand.commend==1}">selected='selected'</c:if>>是</option>
				    </select>
                    <input type="button" onclick="search()" class="${btnclass}" value="搜索"/>
                    <input type="button" value="创建品牌" class="${btnclass}" onclick='window.location="${contextPath}/admin/brand/load"'/>
                    <input type="button" value="导出" class="${btnclass}" id="exportBrand"/>
                </span>
               </div>
			</form:form>
		<div align="center" id="brandContentList" name="brandContentList" class="order-content-list"></div>
		<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
		    <tr>
		        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
		            1. 品牌带有一个品牌图片<br>
		            2. 品牌可以挂在商品三级类型上，在用户选择了三级类型之后出现对应的品牌<br>
		        </td>
		    <tr>
		</table>
   </div>
   </div>
   </body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/brandList.js'/>"></script>
<script language="javascript">
	var contextPath = "${contextPath}";
	var shopName = '${brand.shopName}';
</script>
</html>
