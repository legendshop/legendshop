<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/app/shopIndexDecorate/query" id="item" export="false" class="${tableclass}" style="width:100%;" sort="external">
		
		<display:column title="店铺名称" style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
			${item.siteName}
		</display:column>
		
		<display:column title="状态" sortable="ture" sortName="status" class="review" style="min-width:60px">
			<c:choose>
				<c:when test="${item.status eq -2}">
					<span class="page-state draft">平台下线</span>
				</c:when>
				<c:when test="${item.status eq -1}">
					<span class="page-state draft">下线</span>
				</c:when>
				<c:when test="${item.status eq 0}">
					<span class="page-state draft">已修改未上线</span>
				</c:when>
				<c:when test="${item.status eq 1}">
					<span class="page-state">上线</span>
				</c:when>
			</c:choose>
		</display:column>
	
		<display:column title="创建/修改时间" style="min-width: 90px;">
			<c:choose>
				<c:when test="${not empty item.recDate}">
					<fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss" />
				</c:when>
				<c:otherwise>暂无</c:otherwise>
			</c:choose>
		</display:column>

		<display:column title="操作" media="html" style="min-width:140px">
				<div class="table-btn-group">
					
					<c:choose>
                        <c:when test="${item.status == 1}">
                            <button class="tab-btn" onclick="update('${item.id}','-2')">下线</button>
                        </c:when>
                        <c:otherwise>
                            <button class="tab-btn" onclick="update('${item.id}','1')">上线</button>
                        </c:otherwise>
                    </c:choose>
					
					<c:if test="${item.status ne 1}">
						<span class="btn-line">|</span>
                     	<button onclick="del('${item.id}')" class="tab-btn">
							删除
						</button>
                    </c:if>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
