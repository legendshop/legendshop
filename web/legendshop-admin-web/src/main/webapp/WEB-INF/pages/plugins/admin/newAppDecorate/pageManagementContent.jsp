<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/app/pageManagement/query" id="item" export="false" class="${tableclass}" style="width:100%;" sort="external">
		
		
        	<span class="in-use-bg"></span>
         	<span class="in-use">使用中</span>
       
		<display:column title="页面名称" style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
			<c:if test="${item.isUse eq 1}"><span class="page-state">[使用中]</span></c:if>&nbsp;
			<span class="name-txt">${item.name}<i class="edit-name"></i></span>
            <input type="text" class="name-inp" maxlength="20" value="${item.name}" >
            <a href="javascript:void(0);" data-id="${item.id}" class="confirm">确定</a>
		</display:column>
		
		<display:column title="状态" sortable="ture" sortName="status" class="review" style="min-width:60px">
			<c:choose>
				<c:when test="${item.status eq 1}">
					<span class="page-state">已发布</span>
				</c:when>
				<c:when test="${item.status eq -1}">
					<span class="page-state draft">草稿</span>
				</c:when>
				<c:when test="${item.status eq 0}">
					<span class="page-state draft">已修改未发布</span>
				</c:when>
			</c:choose>
		</display:column>
	
		<display:column title="修改时间" style="min-width: 90px;">
			<c:choose>
				<c:when test="${not empty item.recDate}">
					<fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss" />
				</c:when>
				<c:otherwise>暂无</c:otherwise>
			</c:choose>
		</display:column>

		<display:column title="操作" media="html" style="min-width:140px">
				<div class="table-btn-group">
					<button onclick="edit('${item.id}')" class="tab-btn">
						编辑
					</button>
					<span class="btn-line">|</span>
					<button onclick="clone('${item.id}')" class="tab-btn">
						复制
					</button>
					<c:if test="${item.isUse eq 0}">
						<span class="btn-line">|</span>
                     	<button onclick="del('${item.id}')" class="tab-btn">
							删除
						</button>
                    </c:if>
					<c:if test="${item.status eq -1 || item.status eq 0}">
						<span class="btn-line">|</span>
                     	<button onclick="release('${item.id}')" class="tab-btn">
							发布
						</button>
                    </c:if>
                    <c:if test="${item.status eq 1}">
                    	<c:if test="${item.category eq 'INDEX' && item.isUse eq 0}">
                    		<span class="btn-line">|</span>
	                     	<button onclick="use('${item.id}')" class="tab-btn">
								设为首页
							</button>
                    	</c:if>
                    </c:if>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
   	<c:if test=""></c:if>
