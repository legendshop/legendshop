<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm"
	var="nowDate" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>预售活动管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<style>
input[type='text'] {
	width: 190px;
}
.criteria-div {
    padding: 20px 0 0 0 !important;
}

</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">预售管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">预售活动管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty presellProd.status and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query"/>">全部活动</a></li>
						<li
							<c:if test="${presellProd.status eq -1 and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=-1"/>">待审核</a>
						</li>
						<li
							<c:if test="${presellProd.status eq 1 and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=1"/>">已拒绝</a>
						</li>
						<li
							<c:if test="${presellProd.status eq 0 and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=0"/>">上线中</a>
						</li>
						<li
							<c:if test="${presellProd.status eq 3 and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=3"/>">已终止</a>
						</li>
						<li
							<c:if test="${presellProd.status eq 2 and !isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=2"/>">已完成</a>
						</li>
						<li id="passedTab"
							<c:if test="${isExpires}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/presellProd/query?status=-3"/>">已过期</a>
						</li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/presellProd/query" id="form1"
				method="GET">
				<div style="padding-left: 10px;">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						店铺：
						<input type="text" id="shopId" name="shopId" value="${presellProd.shopId}"/>
						<input type="hidden" id="shopName" name="shopName" maxlength="50" value="${presellProd.shopName}" class="${inputclass}"/> 
					</span>
					<span class="item">
						方案名称：
						<input type="text" id="schemeName" name="schemeName" maxlength="50"value="${presellProd.schemeName}" class="${inputclass}" placeholder="请输入方案名称" /> 
					</span>
					<span class="item">
						<%-- 商品编码：<input type="text" name="prodCode" maxlength="50" value="${presellProd.prodCode}" class="${inputclass}" placeholder="请输入商品编码" /> --%>
						促销时间：<input type="text" id="preSaleStart" name="preSaleStart" class="Wdate" maxlength="50" placeholder="请输入活动开始时间" readonly="readonly"
						value="<fmt:formatDate value='${presellProd.preSaleStart}' pattern='yyyy-MM-dd'/>" />
						- <input type="text" id="preSaleEnd" name="preSaleEnd" class="Wdate" maxlength="50" placeholder="请输入活动结束时间" readonly="readonly"
						value="<fmt:formatDate value='${presellProd.preSaleEnd}' pattern='yyyy-MM-dd'/>" />
						<input type="hidden" id="status" name="status" value="${presellProd.status}" /> 
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" style="margin-left: 6px;"/> <%-- <input type="button" value="添加预售活动" class="${btnclass}" onclick='window.location="${contextPath}/admin/presellProd/load"' /> --%>
					</span>
				</div>
				</div>
			</form:form>
			<div align="center" id="presellProdList" class="order-content-list"></div>
			<table class="${tableclass}" style="width: 100%; border: 0px; margin-top: 10px;">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 预售活动列表<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	$(document).ready(function () {
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	function sendData(curPageNO) {
	    var data = {
	   		"shopId": $("#shopId").val(),
	        "curPageNO": curPageNO,
	        "status": $("#status").val(),
	        "schemeName": $("#schemeName").val(),
	        "preSaleStart": $("#preSaleStart").val(),
	        "preSaleEnd": $("#preSaleEnd").val(),
	        "isExpires" : "${isExpires}",
	    };
	    $.ajax({
	        url: contextPath+"/admin/presellProd/queryContent",
	        data: data,
	        type: 'get',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#presellProdList").html(result);
	        }
	    });
	}
	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	function pager(curPageNO) {
	    $("#curPageNO").val(curPageNO);
	    sendData(curPageNO);
	}

	function batchDeletePresellProds() {
		var presellIdStr = getCheckedPresellIds("can-offline=true");
		if (presellIdStr == "") {
			layer.alert("请选择要下线的预售活动", {icon:0});
			return false;
		}
		$.ajax({
			url : contextPath + "/admin/presellProd/batchStop/" + presellIdStr,
			type : "put",
			dataType : "JSON",
			error : function(result) {
				layer.alert("对不起,请求失败,请稍后重试!", {icon:2});
			},
			success : function(result) {
				if (result == "OK") {
					layer.msg("批量下线成功!", {icon:1, time:700}, function() {
						window.location.reload();
					});
				} else {
					layer.alert(result, {icon:2});
				}
			}
		});
	};

	function getCheckedPresellIds(condition) {
		var checkedPresell = $(".selectOne:checked");
		var presellIds = new Array();
		checkedPresell.each(function(index, domEle) {
			var checkedId = $(this).val();
			presellIds.push(checkedId);
		});
		return presellIds.toString();
	}
	
	//删除预售活动
	function deletePresellProd(id) {
		layer.confirm("您确定要删除该预售活动?", {icon:3}, function() {
			$.ajax({
				url : contextPath + "/admin/presellProd/delete/" + id,
				type : "GET",
				dataType : "JSON",
				error : function(result) {
					layer.alert("对不起,请求失败,请稍后重试!", {icon:2});
				},
				success : function(result) {
					if (result == "OK") {
						layer.msg("删除成功!", {icon:1, time:700}, function() {
							window.location.reload();
						});
					} else {
						layer.alert(result, {icon:2});
					}
				}
			});
		});
	}

	//终止预售活动
	function stopPresellProd(id) {
		layer.confirm("您确定要终止该预售活动?", {icon:3}, function() {
			$.ajax({
				url : contextPath + "/admin/presellProd/stop/" + id,
				type : "GET",
				dataType : "JSON",
				error : function(result) {
					layer.alert("对不起,请求失败,请稍后重试!", {icon:2});
				},
				success : function(result) {
					if (result == "OK") {
						layer.msg("终止成功!", {icon:1}, function() {
							window.location.reload();
						});
					} else {
						layer.alert(result, {icon:2});
					}
				}
			});
		});
	}
	
	 laydate.render({
		   elem: '#preSaleStart',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
		  });
		   
		 laydate.render({
		   elem: '#preSaleEnd',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	    });
</script>
</html>