<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>店铺管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/css/viewBigImage.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">商城管理 &nbsp;＞&nbsp;<a href="<ls:url address="/admin/shopDetail/query"/>">店铺管理</a>  
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺编辑</span>
					</th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/shopDetail/load/${shopDetail.shopId}"/>">店铺信息</a></li>
						<c:if test="${shopDetail.type eq 1 && shopDetail.opStatus ne 1}">
							<li><i></i><a
								href="<ls:url address="/admin/shopDetail/queryCompanyInfo?shopId=${shopDetail.shopId}&userName=${shopDetail.userName}&status=${shopDetail.status}"/>">公司信息</a></li>
						</c:if>
						<li class="am-active"><i></i><a href="javaScript:void(0)">审核历史</a></li>
					</ul>
				</div>
			</div>
			<form:form
				action="${contextPath}/admin/shopDetail/shopAuditDetail?shopId=${shopDetail.shopId }"
				id="form1" method="post">
				<input type="hidden" id="curPageNO" name="curPageNO"
					value="${curPageNO}" />
			</form:form>
			<div align="center" class="order-content-list" style="margin-top: 20px;">
				<display:table name="list"
					requestURI="${contextPath}/admin/shopDetail/shopAuditDetail?shopId=${shopDetail.shopId }"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="审核时间">
						<fmt:formatDate value="${item.modifyDate}" type="both"
							dateStyle="long" pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<display:column title="审核结果">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="SHOP_STATUS" selectedValue="${item.status}" />
					</display:column>
					<display:column title="审核意见" property="auditOpinion"></display:column>
				</display:table>
				<c:if test="${not empty toolBar}">
					<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
				</c:if>
				<div align="center" style="margin: 30px auto;">
					<input type="button" value="返回" class="${btnclass}"
						onclick="window.location='${contextPath}/admin/shopDetail/query'" />
				</div>
			</div>

		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/browser.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/js/viewBigImage.js"></script>
<script type="text/javascript" src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
</html>