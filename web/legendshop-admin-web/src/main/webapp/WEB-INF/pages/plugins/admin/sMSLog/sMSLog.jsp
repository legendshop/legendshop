<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建短信记录表</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/system/smsLog/save" method="post" id="form1">
            <input id="id" name="id" value="${sMSLog.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建短信记录表
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">用户名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userName" id="userName" value="${sMSLog.userName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">手机号码: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userPhone" id="userPhone" value="${sMSLog.userPhone}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">短信内容:</div>
		       	</td>
		        <td>
		           	<input type="text" name="content" id="content" value="${sMSLog.content}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">短信类型: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="type" id="type" value="${sMSLog.type}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">发送时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="recDate" id="recDate" value="${sMSLog.recDate}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/system/smsLog/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    	jQuery("#form1").validate({
			rules: {
			userName: "required",
			type:{
				required:true,
				number:true
			},
			userPhone: {	
             number:true
            }
		},
    messages: {
    		userName:"请输入名字",
    		type:{
    			required:"请输入短信类型",
    			number:"请输入数字"
    		},
			userPhone: {
                number:"请输入数字"
            }		
		}
    });
    
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

