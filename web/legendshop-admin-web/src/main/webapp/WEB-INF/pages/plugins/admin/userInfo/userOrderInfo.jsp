<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
 <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/pagination.css" />
<style>
/* .orderlist_one {
    float: left;
    padding-bottom: 0px;
    width: 100%;
}
.orderlist_one_h4 {
    background:#eeeeee none repeat scroll 0 0;
    border: 1px solid #eeeeee;
    height: 50px;
    line-height: 25px;
    margin-top: -1px;
    margin-bottom:0px;
    padding-left:10px;
}
.orderlist_one_h4 span {
    padding-left: 20px;
    font-size: 12px;
}
.user_list{
	margin-left:0.5rem;
}

.order_xx {
    display: block;
    position: relative;
}

.xx_sp {
    float: left;
    text-align: left;
    width: 50px;
}

.xx_date {
    float: left;
    text-align: left;
}
.xx {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #efefef;
    float: left;
    left: 37px;
    padding: 5px;
    position: absolute;
    top: 15px;
    width: 250px;
}
.xx h6 {
    color: #3399ff;
    font-size: 12px;
    height: 20px;
    line-height: 20px;
    text-align: center;
}
.xx ul{
	padding-left:0;
}
.xx ul li {
    list-style-type: none;
    display: inline;
}
.xx ul li div{
	clear: both;
} */

.orderlist_one_h4 {
    margin: 0;
    font-weight: 400;
    background: #f9f9f9;
    border: 1px solid #efefef;
    padding: 5px 0 5px 17px !important;
    position: relative;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
<div class="seller_right">
		<table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
	              		&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
	              	</th>
		    	</tr>
  		 </table>
      <div class="user_list">
        <div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li ><i></i><a href="<ls:url address="/admin/userinfo/userDetail/${paramDto.userId}"/>">基本资料</a></li>
            <li class=" am-active" ><i></i><a href="javascript:void(0);">会员订单</a></li>
            <li ><i></i><a href="<ls:url address="/admin/userinfo/expensesRecord/${paramDto.userId}?userName=${paramDto.userName}" />">会员消费记录</a></li>
           <li ><i></i><a href="<ls:url address="/admin/userinfo/userVisitLog/${paramDto.userName}?userId=${paramDto.userId}"/>">会员浏览历史</a></li>
           <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdComm/${paramDto.userId}?userName=${paramDto.userName}"/>">会员商品评论</a></li>
            <ls:plugin pluginId="integral">
		        <li ><i></i><a href="<ls:url address="/admin/userIntegral/userIntegralDetail/${paramDto.userId}?userName=${paramDto.userName}"/>">会员积分明细</a></li>
		     </ls:plugin>
		     <ls:plugin pluginId="predeposit">
		        <li ><i></i><a href="<ls:url address="/admin/preDeposit/userPdCashLog/${paramDto.userName}?userId=${paramDto.userId}"/>">会员预存款明细</a></li>
		     </ls:plugin>
		     <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdCons/${paramDto.userName}?userId=${paramDto.userId}"/>">会员商品咨询</a></li>
          </ul>
        </div>
        <form:form  id="ListForm" method="post" action="${contextPath}/admin/userinfo/userOrderInfo/${paramDto.userName}?userId=${paramDto.userId}">
            <input type="hidden" value="1" id="curPageNO" name="curPageNO">
          <div class="criteria-div">
	          <span class="item">
	            <input class="${inputclass}" type="text" placeholder="订单编号" id="subNumber" value="${paramDto.subNumber}" class="user_title_txt" name="subNumber">
	          </span>
	          <span class="item">
	            <input type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />'   readonly="readonly"  placeholder="下单时间(起始)"  class="user_title_txt" id="startDate" name="startDate" >
	          </span>
	          <span class="item">
	            <input type="text" readonly="readonly" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate"  placeholder="下单时间(结束)"  class="user_title_txt" >
	          </span>
	          <span class="item">
	            <input class="${btnclass}" type="submit" value="查询" >
	          </span>
          </div>
        </form:form>  
        
        <div class="order-content-list">
<%--        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="am-table am-table-striped" >--%>
<%--			<thead>--%>
<%--				<tr>--%>
<%--					<th width="28%" style="text-align:left;">--%>
<%--						<span style="display: inline-block;margin: 0 130px 0 10px;">图片</span>--%>
<%--						<span style="display: inline-block;">商品名称</span>--%>
<%--					</th>--%>
<%--					<th style="text-align: center;width: 12%;">规格</th>--%>
<%--					<th style="text-align: center;width: 12%;">数量</th>--%>
<%--					<th style="text-align: center;width: 12%;">金额</th>--%>
<%--					<th style="text-align: center;width: 12%;">状态</th>--%>
<%--				</tr>--%>
<%--			</thead>--%>
<%--		</table>--%>
<%--           <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">--%>
<%--               <div class="orderlist_one" orderId="${order.subId}">--%>
<%--                   <h4 class="orderlist_one_h4">--%>
<%--                       <span>店铺名称：${order.shopName}</span>--%>
<%--                       <span>订单号：${order.subNum}</span>--%>
<%--                       <span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span>--%>
<%--					   <span class="blue">付款类型：${order.payManner==1?"货到付款":"在线支付"}</span>--%>
<%--                       <span class="blue">支付方式：--%>
<%--                       	  <c:choose>--%>
<%--						    <c:when test="${order.ispay}">未支付</c:when><c:otherwise>${order.payTypeName}</c:otherwise>--%>
<%--						  </c:choose>--%>
<%--					   </span>--%>
<%--             		</h4>--%>
<%--             --%>
<%--            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="user_order_table ${tableclass }">--%>
<%--              <tbody>--%>
<%--	              <c:forEach items="${order.subOrderItemDtos 	}" var="orderItem" varStatus="orderItemStatues">--%>
<%--	             	 <tr>--%>
<%--	             	 	<td class="vertical-dividers" style="width: 28%;text-align:left;">--%>
<%--								<div class="clearfix">--%>
<%--									<div class="fl">--%>
<%--										<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}" class="order_img">--%>
<%--											<img src="<ls:images item="${orderItem.pic}" scale="3"/>">--%>
<%--										</a>--%>
<%--									</div>--%>
<%--									<div>--%>
<%--										<span class="order_img_name">--%>
<%--											<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">--%>
<%--												<a href="${PC_DOMAIN_NAME}/snapshot/${orderItem.snapshotId}" target="_blank">[交易快照]</a>--%>
<%--											</c:if> --%>
<%--											<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}">${orderItem.prodName}</a>--%>
<%--										</span>--%>
<%--									</div>--%>
<%--								</div>--%>
<%--						</td>--%>
<%--						<td align="center" class="vertical-dividers">--%>
<%--							<c:choose>--%>
<%--								<c:when test="${not empty orderItem.attribute}">--%>
<%--									<span>${orderItem.attribute}</span>--%>
<%--								</c:when>--%>
<%--								<c:otherwise>--%>
<%--									<span>&nbsp;</span>--%>
<%--								</c:otherwise>--%>
<%--							</c:choose>--%>
<%--						</td>	--%>
<%--						<td align="center" class="vertical-dividers">--%>
<%--							<span>${orderItem.basketCount}(件)</span>--%>
<%--						</td>--%>
<%--						<c:choose>--%>
<%--							<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->--%>
<%--								<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">--%>
<%--							</c:when>--%>
<%--							<c:otherwise>--%>
<%--								<td align="center" class="vertical-dividers">--%>
<%--							</c:otherwise>--%>
<%--						</c:choose>--%>
<%--							<span class="order_sp">¥<fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></span></br>--%>
<%--							<c:if test="${not empty order.freightAmount}">--%>
<%--								<span class="order_sp">(含运费:¥<fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber>)</span>--%>
<%--							</c:if>--%>
<%--						</td>--%>
<%--						<c:choose>--%>
<%--							<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->--%>
<%--								<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">--%>
<%--							</c:when>--%>
<%--							<c:otherwise>--%>
<%--								<td align="center" class="vertical-dividers">--%>
<%--							</c:otherwise>--%>
<%--						</c:choose>--%>
<%--							<span class="order_sp">--%>
<%--								<c:choose>--%>
<%--									 <c:when test="${order.status==1 }">--%>
<%--									        待付款--%>
<%--									 </c:when>--%>
<%--									 <c:when test="${order.status==2 }">--%>
<%--									       待发货--%>
<%--									 </c:when>--%>
<%--									 <c:when test="${order.status==3 }">--%>
<%--									       待收货--%>
<%--									 </c:when>--%>
<%--									 <c:when test="${order.status==4 }">--%>
<%--									      已完成--%>
<%--									 </c:when>--%>
<%--									 <c:when test="${order.status==5 }">--%>
<%--									      交易关闭--%>
<%--									 </c:when>--%>
<%--								  </c:choose>--%>
<%--							</span>--%>
<%--							--%>
<%--						</td>--%>
<%--	             	 </tr>--%>
<%--	              </c:forEach>--%>
<%--            	</tbody>--%>
<%--            </table>--%>
<%--          </div>--%>
<%--         </c:forEach>--%>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="am-table am-table-striped" >
				<thead>
				<tr>
					<th width="10%" style="text-align:left;">
						<span style="display: inline-block;margin: 0 80px 0 10px;">图片</span>
						<span style="display: inline-block;">商品名称</span>
					</th>
					<th style="text-align: center;width: 25%;">规格</th>
					<th style="text-align: center;width: 10%;">单价</th>
					<th style="text-align: center;width: 21%;">数量</th>
					<th style="text-align: center;width: 12%;">金额</th>
					<th style="text-align: center;">状态</th>
				</tr>
				</thead>
			</table>
			<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
				<div class="orderlist_one" orderId="${order.subId}">
					<h4 class="orderlist_one_h4" style="padding-left: 20px !important;">
						<span>编号：${orderstatues.count}</span>
						<span>店铺：<a href="${contextPath}/admin/shopDetail/load/${order.shopId}">${order.shopName}</a></span>
						<span>订单号：<a href="${contextPath}/admin/order/orderAdminDetail/${order.subNum}">${order.subNum}</a></span>
						<span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span>

						<span class="blue">付款类型：${order.payManner==1?"货到付款":"在线支付"}</span>

						<span class="blue">订单类型：
							<c:choose>
								<c:when test="${order.subType == 'NORMAL'}">普通订单</c:when>
								<c:when test="${order.subType == 'AUCTIONS'}">拍卖订单</c:when>
								<c:when test="${order.subType == 'GROUP'}">团购订单</c:when>
								<c:when test="${order.subType == 'SHOP_STORE'}">门店订单</c:when>
								<c:when test="${order.subType == 'SECKILL'}">秒杀订单</c:when>
								<c:when test="${order.subType == 'MERGE_GROUP'}">拼团订单</c:when>
								<c:when test="${order.subType == 'PRE_SELL'}">预售订单</c:when>
							</c:choose>
							</span>
						<c:if test="${order.status==5 }">
							<span class="blue">关闭时间： <fmt:formatDate value="${order.updateDate}" type="both" /> </span>
						</c:if>
						<c:if test="${not empty order.subSettlement}">
							<span class="blue">商城支付流水号： ${order.subSettlement} </span>
						</c:if>
						<c:if test="${not empty order.flowTradeNo}">
							<span class="blue">第三方支付流水号： ${order.flowTradeNo} </span>
						</c:if>
						<c:if test="${order.refundState eq 1 }">
							<span style="color: #ff6c00;">该订单正在退款/退货中</span>
						</c:if>
						<c:if test="${order.refundState eq 2 }">
							<span style="color: #ff6c00;">该订单退款/退货已完成</span>
						</c:if>
					</h4>

					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="user_order_table ${tableclass }" style="margin-bottom: 0">
						<tbody>
						<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
							<tr>
								<td class="vertical-dividers" style="width: 10%;text-align:left;">
									<div class="clearfix">
										<div class="fl">
											<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}" class="order_img">
												<img src="<ls:images item="${orderItem.pic}" scale="3"/>">
											</a>
										</div>
										<div>
											<span class="order_img_name">
												<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">
													<a href="${PC_DOMAIN_NAME}/snapshot/${orderItem.snapshotId}" target="_blank">[交易快照]</a>
												</c:if>
												<a target="_blank" href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}">${orderItem.prodName}</a>
											</span>
											<c:if test="${not empty orderItem.promotionInfo}"><span class="order_img_span">促销：${orderItem.promotionInfo}</span></c:if>
										</div>
									</div>
								</td>
								<td align="center" class="vertical-dividers">
									<c:choose>
										<c:when test="${not empty orderItem.attribute}">
											<span>${orderItem.attribute}</span>
										</c:when>
										<c:otherwise>
											<span>&nbsp;</span>
										</c:otherwise>
									</c:choose>
								</td>

								<td align="center" class="vertical-dividers">
									<span>价格：￥${orderItem.cash}</span>
								</td>
								<td align="center" class="vertical-dividers">
									<span>数量：${orderItem.basketCount}(件)</span>
								</td>
								<c:if test="${orderItemStatues.index eq 0 }">

									<c:choose>
										<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
											<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">
										</c:when>
										<c:otherwise>
											<td align="center" class="vertical-dividers">
										</c:otherwise>
									</c:choose>
									<span class="order_sp">¥<fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></span></br>
									<c:if test="${not empty order.freightAmount}">
										<span class="order_sp">(含运费:¥<fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber>)</span>
									</c:if>
									</td>
									<c:choose>
										<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
											<td align="center" class="vertical-dividers" rowspan="${fn:length(order.subOrderItemDtos) }">
										</c:when>
										<c:otherwise>
											<td align="center" class="vertical-dividers">
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${order.status==1 }">待付款</c:when>
										<c:when test="${order.status==2&&order.subType=='SHOP_STORE'}">待提货</c:when>
										<c:when test="${order.status==2 }">
											<c:choose>
												<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
												<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">团购中</c:when>
												<c:otherwise>待发货</c:otherwise>
											</c:choose>
										</c:when>
										<c:when test="${order.status==3}">待收货</c:when>
										<c:when test="${order.status==4 }">已完成</c:when>
										<c:when test="${order.status==5 }"> 交易关闭</c:when>
									</c:choose>
									</td>
								</c:if>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</c:forEach>


         <c:if test="${empty list}">
         	<div style="background:#f9f9f9;margin-top:20px;padding:20px;border:1px solid #efefef;height:60px;width:99%;">未找到相应记录~</div>
         </c:if>  
         </div>                                                                                       
        <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
        
   </div>
</div>

</div>
</div>
</body>
<script type="text/javascript">
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("ListForm").submit();
}



laydate.render({
		 elem: '#startDate',
		 calendar: true,
		 theme: 'grid',
	 	 trigger: 'click'
	  });
	   
	  laydate.render({
	     elem: '#endDate',
	     calendar: true,
	     theme: 'grid',
	 	 trigger: 'click'
 });
</script>
</html>

