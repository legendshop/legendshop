<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>头部菜单设置 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">

	<table class="${tableclass} title-border" style="width: 100%">
	    <tr>
	        <th class="title-border">
	       		<c:if test="${type=='pc' }">PC首页装修</c:if>
	       		<c:if test="${type=='mobile'}">移动装修</c:if>
	        	&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"> 头部菜单设置</span>
	        </th>
	    </tr>
	</table>

    <div class="seller_list_title">
        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <c:choose>
                <c:when test="${type=='pc'}">
                    <li <c:if test="${headmenu.type eq 'pc'}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/headmenu/query/pc"/>">电脑端</a></li>
                </c:when>
                <c:otherwise>
                    <li <c:if test="${headmenu.type eq 'mobile'}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/headmenu/query/mobile"/>">手机端</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
<form:form action="${contextPath}/admin/headmenu/query/${headmenu.type}" id="form1" method="get">
    <table class="${tableclass}">
    	<div class="criteria-div">
		     <span class="item">
                <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
               	 菜单名：<input type="text" name="name" maxlength="50" value="${headmenu.name}" class="${inputclass}" placeholder="请输入菜单名搜索"/>
                <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
                <input type="button" value="创建菜单" class="${btnclass}" onclick='window.location="${contextPath}/admin/headmenu/load?type=${headmenu.type}"'/>
        	</span>
        </div>
    </table>
</form:form>

<div align="center" class="order-content-list">
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/headmenu/query/${headmenu.type}" id="item" export="false" class="${tableclass}">
        <%-- <display:column title="顺序" ><%=offset++%>
        </display:column> --%>
        <display:column title="菜单名称" property="name" sortable="true"></display:column>
        <c:if test="${headmenu.type eq 'mobile'}">
            <display:column title="图片（建议尺寸相同）">
                <img src="<ls:photo item='${item.pic}'/>" height="80px" width="80px"/>
            </display:column>
        </c:if>
        <display:column title="链接">
            <a href="${PC_DOMAIN_NAME}${item.url}" target="_blank">${item.url}</a>
        </display:column>
        <display:column title="排序" property="seq" sortable="true"></display:column>
        <display:column title="修改时间" property="updateTime" format="{0,date,yyyy-MM-dd hh:mm:ss}" sortable="true" style="width:300px;"></display:column>
        <display:column title="操作" media="html">
             <div class="table-btn-group">
             	<c:choose>
                     <c:when test="${item.status == 1}">
                         <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.name}" status="${item.status}">下线</button>
                         <span class="btn-line">|</span>
                     </c:when>
                     <c:otherwise>
                         <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.name}" status="${item.status}">上线</button>
                         <span class="btn-line">|</span>
                     </c:otherwise>
                 </c:choose>
                 <button class="tab-btn" onclick="window.location='${contextPath}/admin/headmenu/load?id=${item.id}&type=${item.type}'">编辑</button>
                 <span class="btn-line">|</span>
                 <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
             </div>
        </display:column>
    </display:table>
    <div class="clearfix">
  		<div class="fr">
  			 <div class="page">
	   			 <div class="p-wrap">
	       		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				 </div>
  			 </div>
  		</div>
     </div>
   </div>
    <table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
        <tr>
            <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
                1、 头部菜单展示在商城首页，查找框下面。<br>
                2、 菜单数量一般不能超过8个，不然会换行变形。
            </td>
        <tr>
    </table>
</div>
</div>
</body>
<script language="JavaScript" type="text/javascript">
    function deleteById(id) {
        layer.confirm("确定删除 ?", {icon:0}, function () {
            window.location = "${contextPath}/admin/headmenu/delete?id=" + id;
        });
    }
    $(document).ready(function () {
        $("button[name='statusImg']").click(function (event) {
        	$this = $(this);
            initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/headmenu/updatestatus/", $this, "${contextPath}");
        });

        //   highlightTableRows("item"); 
    });

    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>
