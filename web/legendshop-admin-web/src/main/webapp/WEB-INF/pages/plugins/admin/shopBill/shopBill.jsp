<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>结算查看 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">结算管理&nbsp;＞&nbsp;
						<a href="<ls:url address="/admin/shopBill/query"/>">结算账单 </a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">结算明细</span>
					</th>
				</tr>
			</table>
			<table align="center" class="${tableclass} no-border content-table">
				<tr>
					<td colspan="2">
						<div align="right" style="text-align: left; font-weight: bold;margin-left:10px;">
							店铺 - ${shopOrderBill.shopName}(店铺ID:${shopOrderBill.shopId}) 结算单
						</div>
					</td>
				</tr>
				<tr>
					<td width="50px;">
						<div align="right" style="text-align: left;margin-left: 15%;">结算档期:</div>
					</td>
					<td align="left">${shopOrderBill.flag}</td>
				</tr>
				<tr>
					<td>
						<div align="right">结算单号:</div>
					</td>
					<td align="left">${shopOrderBill.sn}</td>
				</tr>
				<tr>
					<td>
						<div align="right">起止时间:</div>
					</td>
					<td align="left"><fmt:formatDate value="${shopOrderBill.startDate}"
							pattern="yyyy-MM-dd" />&nbsp;至&nbsp;<fmt:formatDate
							value="${shopOrderBill.endDate}" pattern="yyyy-MM-dd" /></td>
				</tr>
				<tr>
					<td>
						<div align="right">出账日期:</div>
					</td>
					<td align="left"><fmt:formatDate value="${shopOrderBill.createDate}"
							pattern="yyyy-MM-dd" /></td>
				</tr>
				<tr>
					<td>
						<div align="right">本期平台佣金比例:</div>
					</td>
					<td align="left">${shopOrderBill.commisRate}%</td>
				</tr>
				<tr>
					<td>
						<div align="right">平台应付:</div>
					</td>
					<td align="left">
						<fmt:formatNumber  pattern="#.##" value="${shopOrderBill.resultTotalAmount}" /> =
						<fmt:formatNumber  pattern="#.##" value="${shopOrderBill.orderAmount}" /> <a href="javascript:void(0);" id="orderAmount" class="tips">(订单金额)</a> -
						${shopOrderBill.orderReturnTotals} <a href="javascript:void(0);" id="refundAmount" class="tips">(退单金额)</a> -
						${shopOrderBill.commisTotals}<a href="javascript:void(0);" id="commisTotals" class="tips">(平台佣金)</a> -
						${shopOrderBill.distCommisTotals}<a href="javascript:void(0);" id="distCommisTotals" class="tips">(分销佣金)</a> +
						${shopOrderBill.redpackTotals}<a href="javascript:void(0);" id="redpackTotals" class="tips">(红包总额)</a>
						- ${shopOrderBill.orderReturnRedpackTotals}<a href="javascript:void(0);" id="returnRedpackTotals" class="tips">(退单红包总额)</a>
						<%-- <c:if test=" ${shopOrderBill.preDepositPriceTotal ne 0}">--%>
						+ ${shopOrderBill.preDepositPriceTotal}<a href="javascript:void(0);" id="preDepositPriceTotal" class="tips">(预售定金)</a>
						<%-- </c:if>--%>
						<%-- <c:if test=" ${shopOrderBill.auctionDepositTotal ne 0}">--%>
						+ ${shopOrderBill.auctionDepositTotal}<a href="javascript:void(0);" id="auctionDepositTotal" class="tips">(拍卖保证金)</a>
						<%-- </c:if>--%>
					</td>
				</tr>
				<c:if test="${shopOrderBill.status==4}">
					<tr>
						<td align="right">付款日期:</td>
						<td align="left"><fmt:formatDate value="${shopOrderBill.payDate}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td align="right">付款备注:</td>
						<td align="left">${shopOrderBill.payContent}</td>
					</tr>
				</c:if>
				<tr>
					<td>
						<div align="right">结算状态:</div>
					</td>
					<td align="left"><c:choose>
							<c:when test="${shopOrderBill.status==1}">已出账</c:when>
							<c:when test="${shopOrderBill.status==2}">商家已确认</c:when>
							<c:when test="${shopOrderBill.status==3}">平台已审核</c:when>
							<c:when test="${shopOrderBill.status==4}">结算完成</c:when>
						</c:choose></td>
				</tr>
				<c:if test="${shopOrderBill.status==2}">
					<tr>
						<td align="right">平台操作:</td>
						<td align="left"><input type="button"
							onclick="adminConfirm(${shopOrderBill.id});" class="${btnclass}"
							value="审核" /></td>
					</tr>
				</c:if>
				<c:if test="${shopOrderBill.status==3}">
					<tr>
						<td align="right">平台操作:</td>
						<td align="left"><input type="button"
							onclick="finishShopBill(${shopOrderBill.id},${shopOrderBill.sn});"
							class="${btnclass}" value="付款完成" /></td>
					</tr>
				</c:if>
			</table>

			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li <c:if test="${empty type||type==1}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopBill/load/${shopOrderBill.id}?type=1"/>">订单列表</a></li>
						<li <c:if test="${type==2}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopBill/load/${shopOrderBill.id}?type=2"/>">退单列表</a></li>
						<li <c:if test="${type==3}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopBill/load/${shopOrderBill.id}?type=3"/>">分销订单</a></li>
						<li <c:if test="${type==4}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopBill/load/${shopOrderBill.id}?type=4"/>">预售定金</a></li>
						<li <c:if test="${type==5}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopBill/load/${shopOrderBill.id}?type=5"/>">拍卖保证金</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/shopBill/load/${shopOrderBill.id}" id="form1" method="post">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
				<input type="hidden" id="type" name="type" value="${type}" />

				<c:if test="${type ne 5}">
				<div class="criteria-div">
					<span class="item">
						订单编号&nbsp;<input type="text" name="subNumber" class="${inputclass}" maxlength="22" value="${subNumber}" /> 
					</span>
					<span class="item">
						<input type="submit" class="${btnclass}" value="搜索" />
						<c:if test="${type eq 1 || type eq 2}">
						<input type="button" class="${btnclass}" onclick="exportOrder();" value="数据导出" />
						</c:if>
					</span>
			    </div>
				</c:if>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<c:choose>
					<c:when test="${empty type||type==1}">
						<display:table name="list" requestURI="/admin/shopBill/load/${shopOrderBill.id}" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="订单编号" property="subNumber"></display:column>
							<display:column title="下单时间">
								<fmt:formatDate value="${item.payDate}" pattern="yyyy-MM-dd" />
							</display:column>
							<display:column title="成交时间">
								<fmt:formatDate value="${item.finallyDate}" pattern="yyyy-MM-dd" />
							</display:column>
							<display:column title="订单金额">
								<fmt:formatNumber value="${item.actualTotal}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="运费">
								<fmt:formatNumber value="${item.freightAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="红包金额">
								<fmt:formatNumber value="${item.redpackOffPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="操作" media="html">
								<div class="table-btn-group">
									<button class="tab-btn" onclick="window.location.href = '${contextPath}/admin/order/orderAdminDetail/${item.subNumber}'">
										查看
									</button>
								</div>
							</display:column>
						</display:table>
					</c:when>
					<c:when test="${type==2}">
						<display:table name="list" requestURI="/admin/shopBill/load/${shopOrderBill.id}" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="订单编号" property="subNumber"></display:column>
							<display:column title="类型">
								<c:if test="${item.applyType eq 1}">退款</c:if>
								<c:if test="${item.applyType eq 2}">退款退货</c:if>
							</display:column>
							<display:column title="退款金额">
								<fmt:formatNumber value="${item.refundAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="退款红包金额">
								<fmt:formatNumber value="${item.returnRedpackOffPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="退单时间(申请)">
								<fmt:formatDate value="${item.applyTime}" pattern="yyyy-MM-dd" />
							</display:column>
							<display:column title="完成时间">
								<fmt:formatDate value="${item.adminTime}" pattern="yyyy-MM-dd" />
							</display:column>
							<display:column title="操作" media="html">
								<div class="table-btn-group">
									<c:if test="${item.applyType eq 1}">
										<button class="tab-btn" onclick="window.open('${contextPath}/admin/returnMoney/refundDetail/${item.refundId}')">
											查看
										</button>
									</c:if>
									<c:if test="${item.applyType eq 2}">
										<button class="tab-btn" onclick="window.open('${contextPath}/admin/returnGoods/returnDetail/${item.refundId}')">
											查看
										</button>
									</c:if>
								</div>
							</display:column>
						</display:table>
					</c:when>
					<c:when test="${type==3}">
						<display:table name="list" requestURI="/admin/shopBill/load/${shopOrderBill.id}" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="订单编号" property="subNumber"></display:column>
							<display:column title="订单项编号" property="subItemNumber"></display:column>
							<display:column title="直接上级分销佣金">
								<fmt:formatNumber value="${item.distUserCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="上二级分销佣金">
								<fmt:formatNumber value="${item.distSecondCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="上三级分销佣金">
								<fmt:formatNumber value="${item.distThirdCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="总分销佣金">
								<fmt:formatNumber value="${item.distCommisCash}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
						</display:table>
					</c:when>
					<c:when test="${type==4}">
						<display:table name="list" requestURI="/admin/shopBill/load/${shopOrderBill.id}" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="订单编号" property="subNumber"></display:column>
							<display:column title="支付方式">
								<c:choose>
									<c:when test="${item.payPctType eq 0}">全额</c:when>
									<c:otherwise>定金</c:otherwise>
								</c:choose>
							</display:column>
							<display:column title="定金金额">
								<fmt:formatNumber value="${item.preDepositPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="定金支付时间">
								<fmt:formatDate value="${item.depositPayTime}" pattern="yyyy-MM-dd"/>
							</display:column>
							<display:column title="尾款支付">
								<c:choose>
									<c:when test="${item.isPayFinal eq 0}">未支付</c:when>
									<c:otherwise>已支付</c:otherwise>
								</c:choose>
							</display:column>
							<display:column title="尾款支付结束时间">
								<fmt:formatDate value="${item.finalMEnd}" pattern="yyyy-MM-dd"/>
							</display:column>
							<display:column title="操作" media="html">
								<div class="table-btn-group">
									<button class="tab-btn" onclick="window.location.href = '${contextPath}/admin/order/orderAdminDetail/${item.subNumber}'">
										查看
									</button>
								</div>
							</display:column>
						</display:table>
					</c:when>
					<c:otherwise>
						<display:table name="list" requestURI="/admin/shopBill/load/${shopOrderBill.id}" id="item" export="false" class="${tableclass}" style="width:100%">
							<display:column title="活动名称" property="auctionsTitle"></display:column>
							<display:column title="保证金额">
								<fmt:formatNumber value="${item.payMoney}" type="currency" pattern="￥##.##"  minFractionDigits="2" />
							</display:column>
							<display:column title="支付名称" property="payName" ></display:column>
							<display:column title="支付时间">
								<fmt:formatDate value="${item.payTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
							</display:column>
						</display:table>
					</c:otherwise>
				</c:choose>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 账单计算公式：订单金额(含运费) - 退单金额 -
						平台佣金 - 分销佣金 + 红包总额 - 退单红包金额 + 预售定金 + 拍卖保证金<br> 2. 账单处理流程为：系统出账 > 商家确认 > 平台审核 >
						财务支付(完成结算) 4个环节，其中平台审核和财务支付需要平台介入，请予以关注<br>
					</td>
				<tr>
			</table> 
			<form:form id="exportForm" method="post" action="${contextPath}/admin/shopBill/export">
				<input name="data" type="hidden" id="exportParam"/>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopBill.js'/>"></script>
<script type="text/javascript">
	var contextPath='${contextPath}';
	var endDate = '<fmt:formatDate value="${shopOrderBill.createDate}" pattern="yyyy-MM-dd"/>';
	var type = '${type}';
 	var tableclass = "${tableclass}";
 	var shopOrderBillId="${shopOrderBill.id}";
	   var subNumber="${subNumber}";
</script>
</html>
