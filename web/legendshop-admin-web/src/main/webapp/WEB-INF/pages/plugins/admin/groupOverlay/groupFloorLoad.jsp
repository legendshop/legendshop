<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<ul class="floor_sear_pro">
    <c:forEach items="${requestScope.list}" var="product" varStatus="status">
  <li> <a href="javascript:void(0);" class="floor_sear_a" onclick="goods_list_set('${product.pic}','${product.prodId}','${product.name}');"> 
  		<span  class="floor_sear_img"> 
 			<img  id="${product.prodId}" src="<ls:images item='${product.pic}' scale='3' />"  title="${product.name}"/>
  		</span>
   		</a> 
  </li>
	</c:forEach>
</ul>
<div class="floor_page" align="right">
	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default" actionUrl="javascript:selectGroupPager"/>
</div>