	<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>打印模板创建 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">配送管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/printTmpl/query"/>">打印模板管理</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"> 添加打印运费模版</span>
			    	</th>
		    	</tr>
		</table>
        <form:form  enctype="multipart/form-data" action="${contextPath}/admin/printTmpl/save" method="post" id="form1" onsubmit="return checkImage()">
            <div align="center" style="margin-top: 10px;">
            <table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
				<tr>
			        <td width="200px">
			          	<div align="right"><font color="ff0000">*</font>单据名称：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<input type="text" class="${inputclass}" name="prtTmplTitle" id="prtTmplTitle" />
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>单据尺寸宽：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<input type="text" class="${inputclass}" maxlength="3" name="prtTmplWidth" id="prtTmplWidth" />
						<span style="margin-left: 10px;font-size: 12px;color: #e5004f;">建议：980px</span>
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>单据尺寸高：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<input type="text" class="${inputclass}" maxlength="3" name="prtTmplHeight" id="prtTmplHeight" />
						<span style="margin-left: 10px;font-size: 12px;color: #e5004f;">建议：980px</span>
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>单据背景：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			         <input type="file"  name="bgimageFile" id="bgimageFile" size="45" /> 
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>是否系统默认：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           <!-- 	<input type="radio" class="am-radio-inline" checked="" value="1" name="isSystem" id="is_system_1">
			            <label class="cb-enable selected" for="is_system_1">系统</label>
			            <input type="radio" class="am-radio-inline"  value="0" name="isSystem" id=is_system_0>
			            <label class="cb-disable " for="is_system_0">普通</label> -->
			            <label class="radio-wrapper radio-wrapper-checked">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="1" name="isSystem" id="is_system_1" checked="checked">
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">系统</span>
						</label>
						<label class="radio-wrapper">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="0" name="isSystem" id=is_system_0>
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">普通</span>
						</label>
			        </td>
				</tr>
				<tr>
			        <td>
			          	<div align="right"><font color="ff0000">*</font>是否启用：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<!-- <input type="radio" class="am-radio-inline" checked="" value="1" name="prtDisabled" id="waybill_usable_1">
			            <label class="cb-enable selected" for="waybill_usable_1">是</label>
			            <input type="radio" class="am-radio-inline"  value="0" name="prtDisabled" id="waybill_usable_0">
			            <label class="cb-disable " for="waybill_usable_0">否</label> -->
			            <label class="radio-wrapper radio-wrapper-checked">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="1" name="prtDisabled" id="waybill_usable_1" checked="checked">
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">是</span>
						</label>
						<label class="radio-wrapper">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="0" name="prtDisabled" id="waybill_usable_0">
								<span class="radio-inner"></span>
							</span>
							<span class="radio-txt">否</span>
						</label>
			        </td>
				</tr>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 0px;">
                        <div>
                            <input type="submit"  class="${btnclass}" value="保存" />
                            <input type="button"  class="${btnclass}" value="返回"
                                onclick="window.location='<ls:url address="/admin/printTmpl/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({
	});
	
    $(document).ready(function() {
    	jQuery.validator.addMethod("checkExtName", function(value,element) {
			var fileNane = $("#bgimageFile").val().split('.');
			var extName = fileNane[fileNane.length - 1];
			var type = "jpg,gif,png,jpeg";
			return type.indexOf(extName)>-1?true:false;
        }, $.validator.format("您上传的文件格式有误，请重新上传！ "));
       jQuery("#form1").validate({
            rules: {
               prtTmplTitle: {
                 required : true,
                 maxlength : 10
               },
               prtTmplWidth: {
                required : true,
                digits: true 
                
               },
               prtTmplHeight: {
                required : true,
                digits: true 
               },
               bgimageFile: {
                     required : true,
                     checkExtName: true
               }
           },
          messages: {
             prtTmplTitle: {
                required : "模板名称不能为空",
                maxlength : "模板名称最多10个字" 
            },
            prtTmplWidth: {
                required : "宽度不能为空",
                digits: "宽度必须为数字"
            },
            prtTmplHeight: {
                required : "高度不能为空",
                digits: "高度必须为数字"
            },
            bgimageFile: {
                  required : "图片不能为空"
            }
        }
     });
		//斑马条纹
     $("#col1 tr:nth-child(even)").addClass("even");
  });
    
    function checkImage(){
		if(!checkImgSize("#bgimageFile",1024)){
			return false;
		}
		return true;
	}
    $("input:radio[name='isSystem']").change(function(){
		 $("input:radio[name='isSystem']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
    $("input:radio[name='prtDisabled']").change(function(){
		 $("input:radio[name='prtDisabled']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
</script>
</html>