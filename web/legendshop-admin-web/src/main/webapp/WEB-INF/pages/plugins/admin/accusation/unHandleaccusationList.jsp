<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
            <table class="am-table am-table-bd am-table-bdrs am-table-striped am-table-hover" style="min-width:unset;border:none;">
              <tbody>
              <tr>
                <th class="am-text-center">商品</th>
                <th>举报主题</th>
                <th>举报时间</th>
                <th>操作</th>
              </tr>
              <c:choose>
              		<c:when test="${not empty accusationList}">
              		              <c:forEach items="${accusationList}"  var="accusation">
					              <tr>
					                <td class="am-text-center">
					                	<a href="${PC_DOMAIN_NAME}/views/${accusation.prodId}" target="_blank"><img src="<ls:images item="${accusation.prodPic}" scale="3" />" alt="${accusation.prodName}"></a>
					               </td>
					                <td>${accusation.title}</td>
					                <td><fmt:formatDate value="${accusation.recDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					                 <td><a href="${contextPath}/admin/accusation/handle/${accusation.id}" target="_blank">处理</a></td>
					              </tr>
					            </c:forEach>
              		</c:when>
              		<c:otherwise>
              					<tr>
					                <td class="am-text-center" colspan="4"  style="padding-bottom: 0px;">
					               <span class="am-icon-exclamation-triangle"></span> 暂无投诉需要处理</td>

					              </tr>
              		</c:otherwise>
              </c:choose>
              </tbody>
            </table>
			<ul class="am-pagination am-fr ">
					<a href="${contextPath}/admin/accusation/query" class="am-comment-author" target="_blank" style="font-weight: normal;">举报列表</a>
			</ul>