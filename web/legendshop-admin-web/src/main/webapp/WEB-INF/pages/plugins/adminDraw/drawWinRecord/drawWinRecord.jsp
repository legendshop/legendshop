<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建抽奖中奖表</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</head>
    <body>
        <form:form action="${contextPath}/admin/drawWinRecord/save" method="post" id="form1">
            <input id="id" name="id" value="${drawWinRecord.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建抽奖中奖表
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">用户id: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userId" id="userId" value="${drawWinRecord.userId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">手机号码: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="mobile" id="mobile" value="${drawWinRecord.mobile}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">姓名: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="cusName" id="cusName" value="${drawWinRecord.cusName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">微信openid: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="weixinOpenid" id="weixinOpenid" value="${drawWinRecord.weixinOpenid}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">微信昵称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="weixinNickname" id="weixinNickname" value="${drawWinRecord.weixinNickname}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">邮寄地址: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="sendAddress" id="sendAddress" value="${drawWinRecord.sendAddress}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">奖项ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="awardsId" id="awardsId" value="${drawWinRecord.awardsId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">奖品ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="giftId" id="giftId" value="${drawWinRecord.giftId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">活动ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="actId" id="actId" value="${drawWinRecord.actId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">发货状态: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="sendStatus" id="sendStatus" value="${drawWinRecord.sendStatus}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">快递单号: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="courierNumber" id="courierNumber" value="${drawWinRecord.courierNumber}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">快递公司: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="courierCompany" id="courierCompany" value="${drawWinRecord.courierCompany}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">抽奖时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="awardsTime" id="awardsTime" value="${drawWinRecord.awardsTime}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/drawWinRecord/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
    </body>
</html>

