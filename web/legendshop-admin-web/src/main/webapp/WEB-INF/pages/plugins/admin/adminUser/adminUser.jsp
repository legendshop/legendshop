<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>员工管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<link type="text/css" rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>">
<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/idcardCheck.js'/>"></script>
<style type="text/css">
.group-item {
	display: block;
	float: left;
	font-size: 14px;
	width: 200px;
}

.w-150px {
	width: 150px;
}

.text-right {
	text-align: right;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
		<table class="${tableclass} title-border" style="width: 100%">
			<tr>
				<th class="title-border">管理员管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/adminUser/query"/>">管理员管理</a>
		            &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">创建管理员</span>
			    </th>
			</tr>
		</table>
			<form:form action="${contextPath}/admin/adminUser/save" method="post"
				id="form1">
				<input id="id" name="id" value="${adminUser.id}" type="hidden">
				<div align="center" style="margin-top: 15px;">
					<table border="0" align="center" class="${tableclass} no-border" id="col1">
						<tr>
							<td width="200px">
								<div align="right">
									<font color="ff0000">*</font>登录名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text" name="name"
								id="name" value="" maxlength="50" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>姓名：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text" name="realName"
								id="realName" autocomplete="off" maxlength="50" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>密码：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input class="${inputclass}" type="password" name="password" id="password" autocomplete="off" maxlength="20" />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>确认密码：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="password"
								name="password2" id="password2" autocomplete="off"
								maxlength="20" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>状态：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select id="enabled" name="enabled"
								class="${selectclass}">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ENABLED" selectedValue="" />
							</select></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>选择角色：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;" id="roles">
								<c:forEach items="${requestScope.roleList}" var="role">
										<div class="group-item">
											<%-- <input type="checkbox" value="${role.id}" id='${role.id}'
												name="roleId"> <span id="index-index" class="priv">${role.name}</span> --%>
											<label class="checkbox-wrapper">
												<span class="checkbox-item">
													<input type="checkbox" class="checkbox-input selectOne" value="${role.id}" id='${role.id}' name="roleId" onclick="selectOne(this);">
													<span class="checkbox-inner"></span>
												</span>
												<span class="checkbox-txt">${role.name}</span>
											</label>
										</div>
								</c:forEach>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>部门：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<input class="${inputclass}" id="deptName" name="deptName"
										type="text" readonly value=""
										onclick="showMenu(); return false;" /> <input id="deptId"
										name="deptId" type="hidden" />
								</div>
								<div id="menuContent" class="menuContent"
									style="display: none; position: absolute; height: 400px; overflow: auto">
									<ul id="deptMenu" class="ztree"
										style="margin-top: 0; background: #fff; border: 2px solid #0E90D2;">
									</ul>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">到期时间：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input readonly="readonly" name="activeTime" id="activeTime" class="${inputclass}" type="text"/></td>
						</tr>
						<tr>
							<td>
								<div align="right">手机号码：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text" name="mobile"
								id="mobile" value="" maxlength="15" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">身份证号：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text"
								name="idCardNum" id="idCardNum" value="" maxlength="18" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">住址：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text" name="addr"
								id="addr" value="" maxlength="150" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">入职时间：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input readonly="readonly" name="hireDate" id="hireDate"
								class="${inputclass}" type="text" 
								value='<fmt:formatDate value="${bean.startTime}" pattern="yyyy-MM-dd"/>' />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">注释：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}" type="text" name="note"
								id="note" value="" style="width: 300px" maxlength="50"/></td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input class="${btnclass}" type="submit" value="保存" /> 
									<input class="${btnclass}" type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/adminUser/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/adminUser.js'/>"></script>
<script language="javascript"> 
	var contextPath="${contextPath}";
	$.validator.setDefaults({});

	jQuery.validator.addMethod("stringCheck", function(value, element) {
		return value.isAlpha();
	}, '<fmt:message key="user.reg.username"/>');

	jQuery.validator.addMethod("checkName1", function(value, element) {
		return checkAdminName(value);
	},'<fmt:message key="errors.user.exists"><fmt:param value=""/></fmt:message>');
</script>
</html>
