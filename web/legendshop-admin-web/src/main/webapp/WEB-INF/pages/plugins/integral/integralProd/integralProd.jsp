<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>




<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/integralProd/save"
				method="post" id="form1" enctype="multipart/form-data">
				<input id="id" name="id" value="${integralProd.id}" type="hidden">
				<div align="center">
					<table class="${tableclass}">
						<thead>
							<tr>
								<th class="no-bg title-th">
									<span class="title-span">
										积分商城  ＞  
										<a href="<ls:url address="/admin/integralProd/query"/>">积分商品列表</a>
										  ＞   <c:if test="${empty integralProd }"><span style="color:#0e90d2;">创建积分商品</span></c:if>
										 <c:if test="${not empty integralProd }"><span style="color:#0e90d2;">修改积分商品</span></c:if>
									</span>
						        </th>
							</tr>
						</thead>
					<table border="0" align="center" class="${tableclass} content-table no-border" id="col1" style="margin-bottom: 50px;">
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>积分商品名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input maxlength="120" style="width: 157px" type="text"
								onkeyup="this.value = this.value.substring(0, 120)" name="name"
								id="name" value="${integralProd.name}"></input></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>商品价格：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" maxlength="8"
								name="price" id="price" value="${integralProd.price}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>兑换积分：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" maxlength="6"
								name="exchangeIntegral" id="exchangeIntegral"
								value="${integralProd.exchangeIntegral}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>商品库存：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text"  maxlength="6"
								name="prodStock" id="prodStock"
								value="${integralProd.prodStock}" /></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>积分商品分类：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" style="cursor: pointer;"
								id="categoryName" name="categoryName"
								value="${integralProd.categoryName }"
								onclick="loadCategoryDialog();" 
								placeholder="商品分类" readonly="readonly" /> <input type="hidden"
								id="firstCid" name="firstCid" value="${integralProd.firstCid }" />
								<input type="hidden" id="twoCid" name="twoCid"
								value="${integralProd.twoCid}" /> <input type="hidden"
								id="thirdCid" name="thirdCid" value="${integralProd.thirdCid }" />
							</td>
						</tr>



						<tr>
							<td>
								<div align="right">商品编号：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" maxlength="10"
								name="prodNumber" id="prodNumber"
								value="${integralProd.prodNumber}" /></td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">
									<font color="ff0000">*</font>商品图片(大小430*430)：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><c:if test="${not empty integralProd.prodImage}">
									<img width="300px"  style="margin-bottom:10px;" 
										src="<ls:photo item='${integralProd.prodImage}'/>" />
									<br />
								</c:if> <input type="file" name="imageFile" id="imageFile" size="50" />
								<input type="hidden" name="prodImage" id="prodImage" 
								value="${integralProd.prodImage}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>状态：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select id="status" name="status">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ENABLED" selectedValue="${integralProd.status}" />
							</select></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>是否推荐商品：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<label class="radio-wrapper <c:if test='${integralProd.isCommend==0}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="isCommend" value="0" <c:if test="${integralProd.isCommend==0}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">否</span>
								</label>
								<label class="radio-wrapper <c:if test='${integralProd.isCommend==1}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="isCommend" value="1" <c:if test="${integralProd.isCommend==1}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">是</span>
								</label>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>是否限制购买数量：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<label class="radio-wrapper <c:if test='${integralProd.isLimit==0}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="isLimit" value="0" <c:if test="${integralProd.isLimit==0}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">否</span>
								</label>
								<label class="radio-wrapper <c:if test='${integralProd.isLimit==1}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="isLimit" value="1" <c:if test="${integralProd.isLimit==1}">checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">是</span>
								</label>
							</td>
						</tr>
						<tr id="limitNumShow" style="display: none;">
							<td>
								<div align="right">
									限制数量：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="limitNum"
								id="limitNum" value="${integralProd.limitNum}" /></td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">商品描述：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea name="prodDesc" id="prodDesc" cols="100"
									rows="8"
									style="width: 700px; height: 200px; visibility: hidden;">${integralProd.prodDesc}</textarea>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">SEO关键字：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea cols="77" rows="3" maxlength="100"
									onkeyup="this.value = this.value.substring(0, 100)"
									name="seoKeyword" id="seoKeyword">${integralProd.seoKeyword}
		          </textarea></td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">SEO 描述：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<textarea cols="77" rows="3" maxlength="120"
									onkeyup="this.value = this.value.substring(0, 120)"
									name="seoDesc" id="seoDesc">${integralProd.seoDesc}
		          				</textarea>
		          			</td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="submit" value="保存" class="${btnclass}" />
								    <input type="button" value="返回" class="${btnclass}"
										onclick="window.location='<ls:url address="/admin/integralProd/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
<script language="javascript">
		    $.validator.setDefaults({
		    });

      jQuery.validator.addMethod("IsMoney", function(value, element) {
            return this.optional(element) || checkMoney(value);
       }, "请正确输入您金额格式：0~99999.99");
             
             
       jQuery.validator.addMethod("ExchangeIntegral", function(value, element) {
    	   var returnVal  = false;
    	   var integral = $("#exchangeIntegral").val();
    	   if(/^[1-9]*[1-9][1-9]*$/.test(integral)){
    		   returnVal = true;
		    }
    	   return returnVal;
       }, "请输入正整数");
       /**
		 * 验证金额（可验证 大于等于零，小于等于99999999.99 的数字）
		 * @param obj
		 * @returns {Boolean}
		 */
		function checkMoney(obj){
		    if(/^(0(?:[.](?:[1-9]\d?|0[1-9]))|[1-9]\d*(?:[.]\d{1,2}|$))$/.test(obj)){
		        return true;
		    }
		}
	 /**
		 * 验证正整数（可验证 大于等于零，小于等于99999999.99 的数字）
		 * @param obj
		 * @returns {Boolean}
		 */
/* 		function checkIntegral(obj){
		    if(/^[1-9]*[1-9][1-9]*$/.test(obj)){
		        return true;
		    }
		} */
		
    $(document).ready(function() {
    KindEditor.options.filterMode=false;
     	  KindEditor.create('textarea[name="prodDesc"]', {
				cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
				uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
				fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
				allowFileManager : true,
				afterBlur:function(){this.sync();},
				width : '80%',
				height:'400px',
				afterCreate : function() {
					var self = this;
					KindEditor.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					KindEditor.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			
		    jQuery("#form1").validate({
		            rules: {
		              name: {
		                required: true,
		                maxlength: 120
		              },
		              price: {
		                required: true,
		                IsMoney: 120
		              },
		              exchangeIntegral:{
		                 required: true,
		                 digits:true,
		                 ExchangeIntegral:false
		              },
		              prodStock:{
		                 required: true,
		                 digits:true
		              },
		              categoryName:{
		                required: true
		              },
		              <c:if test='${empty integralProd.id}'>
		               imageFile:{
		                required: true
		              },
		               </c:if>
		             
		              limitNum:{
		                 required: true,
		                 digits:true,
		                 maxlength:5
		              },
		              prodDesc:{
		                 required: true
		              },
		              sort:{
		                  required: true,
		                 digits:true
		              },
		              isLimit : {
		            	  required: true
		              },
		              isCommend : {
		            	  required: true
		              }
		        },
		        messages: {
		            name: {
		                required: "请输入积分商品名称",
		                maxlength: "不能超过120字数"
		            },
		            price:  {
		                required: "请输入商品价格",
		                IsMoney: "请正确输入您金额格式：0~99999.99"
		            },
		             exchangeIntegral:{
		                 required: "请输入兑换积分",
		                 digits:"请输入正确的格式",
		                 ExchangeIntegral:"请输入正整数"
		             },
		             prodStock:{
		                 required: "请输入兑换商品库存",
		                 digits:"请输入数字"
		             },
		              categoryName:{
		                required: "请选择积分商品分类"
		              },
		               <c:if test='${empty integralProd.id}'>
		              imageFile:{
		                required: "请上传积分图片"
		              },
		               </c:if>
		               
		              limitNum:{
		                 required: "请输入限制数量",
		                 digits:"请输入正确的格式"
		              },
		              prodDesc:{
		               required: "请输入商品描述"
		             },
		              sort:{
		                 required: "请输入排序",
		                 digits:"请输入正确的格式"
		              },
		              isLimit : {
		            	  required : "请选择是否限制购买数量"
		              },
		              isCommend : {
		            	  required : "请选择是否推荐商品"
		              }
		        }
		    });
		 	$("input[name=imageFile]").change(function(){
		 		checkImgType(this);
				checkImgSize(this,5120);
		 	});
            
		 	$("input[name=isLimit]").change(function(){
		 	    if($(this).val()==1){
		 	       $("#limitNumShow").show();
		 	        $("#limitNum").show();
		 	    }else{
		 	      $("#limitNumShow").hide();
		 	       $("#limitNum").hide();
		 	    }
		 	});
		 	
		 	
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});


	//加载分类用于选择
	function loadCategoryDialog() {
		layer.open({
			  title :"选择分类",
			  id: "prodCat",
			  type: 2, 
			  content: "${contextPath}/admin/product/loadAllCategory/I", //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['300px', '400px']
			}); 
	}
	
	function clearCategoryCallBack() {
		$("#firstCid").val("");
		$("#twoCid").val("");
		$("#thirdCid").val("");
		$("#categoryName").val("");
	}

	function loadCategoryCallBack(firstCid,twoCid,thirdCid, catName) {
	    if(firstCid!="" && firstCid!=undefined){
	     $("#firstCid").val(firstCid);
	    }
	    if(twoCid!="" && twoCid!=undefined){
	     $("#twoCid").val(twoCid);
	    }
	    if(thirdCid!="" && thirdCid!=undefined){
	     $("#thirdCid").val(thirdCid);
	    }
		$("#categoryName").val(catName);
	}
	
     <c:if test="${integralProd.isLimit==1}">
    	  $("#limitNumShow").show();
		  $("#limitNum").show();
     </c:if>

     $("input:radio[name='isCommend']").change(function(){
		 $("input:radio[name='isCommend']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
     $("input:radio[name='isLimit']").change(function(){
		 $("input:radio[name='isLimit']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
</script>


</html>

