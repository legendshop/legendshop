<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%
		Integer offset = (Integer) request.getAttribute("offset");
	%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>咨询列表 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
  <style>
  .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
	    	<th class="title-border">咨询管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">咨询管理 </span></th>
	    	</tr>
		</table>
	
	<form:form  action="${contextPath}/admin/productConsult/query" id="form1" method="post">
	  <input type="hidden" name="_order_sort_name" value="${_order_sort_name}"/>
      <input type="hidden" name="_order_indicator" value="${_order_indicator}"/>
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					<span class="item">
					   店铺：
					   <input type="text" id="shopId" name="shopId" maxlength="50" value="${productConsult.shopId}" />
					   <input class="${inputclass}" type="hidden" id="siteName" name="siteName" maxlength="50" value="${productConsult.siteName}" />
					</span>
               		<span class="item">
					   商品名称：<input class="${inputclass}" type="text" id="prodName" name="prodName" maxlength="50" value="${productConsult.prodName}" /> 
					</span>
               		<span class="item">
					      咨询用户：<input class="${inputclass}" type="text" id="nickName" name="nickName" maxlength="50" value="${productConsult.nickName}" /> 
					</span>
               		<span class="item">
				 		咨询类型：
					  <select id="pointType" name="pointType"  class="criteria-select">
					  <ls:optionGroup type="select" required="false" cache="true" beanName="CONSULT_TYPE" selectedValue="${productConsult.pointType}"/>
             		  </select>
             	    </span>
               		<span class="item">
						回复状态：
	            	    <select id="replySts"  name="replySts"  class="criteria-select">
		                    <ls:optionGroup type="select" required="false" cache="true" beanName="YES_NO" selectedValue="${productConsult.replySts}"/>
		                </select>
		            </span>
               		<span class="item">
						  开始时间：
						 <input readonly="readonly"  name="startTime" id="startTime" class="Wdate" type="text"  value='<fmt:formatDate value="${productConsult.startTime}" pattern="yyyy-MM-dd"/>' placeholder="开始时间"/>
					</span>
               		<span class="item">
						结束时间 ：
						 <input readonly="readonly" name="endTime" id="endTime" type="text"  value='<fmt:formatDate value="${productConsult.endTime}" pattern="yyyy-MM-dd"/>'  placeholder="结束时间"/>
					<input type="button" onclick="search()" value="搜索"  class="${btnclass}"/>
					</span>
				</div>
	</form:form>
	<div align="center" id="productConsultList" name="productConsultList" class="order-content-list"></div>
	<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
		<tr>
			<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 用户在前台商品页面中输入咨询内容<br></td>
		<tr>
	</table>
	</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/productConsultList.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
</script>
</html>