<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://www.legendesign.net/date-util" prefix="dateUtil"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm" var="nowDate" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>订单管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/templets/amaze/css/presell/presellOrderDetail.css" />
<style>
	.order_pay_table th{
		text-align:center;
	}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;border-bottom:1px solid #efefef !important;">
				<thead>
					<tr>
						<th style="border:none;text-align: left" class="no-bg">
							<span class="title-span">
								订单管理  ＞  
								<span style="color:#0e90d2;">查看订单详情</span>
							</span>
						</th>
					</tr>
				</thead>
			</table>
			<div class="order_pay">
				
				<div class="order_pay_note">
					<span style="margin-right:30px;">订单号：${order.subNo}</span>
					<span>状态：
					<b style="color: #fd6760;">
					<c:choose>
						<c:when test="${order.refundState ne 1}">
							<c:choose>
								<c:when test="${order.status==1 }">
								        订单已经提交，等待买家付款
								 </c:when>
										<c:when test="${order.status==2 }">
								       待发货
								 </c:when>
										<c:when test="${order.status==3 }">
								       待收货
								 </c:when>
										<c:when test="${order.status==4 }">
								      已完成
								 </c:when>
										<c:when test="${order.status==5 }">
								      交易关闭
								 </c:when>
							</c:choose>
						</c:when>
						<c:otherwise>
							退款退货中
						</c:otherwise>
					</c:choose>
					</b>
					</span>
				</div>

				<div class="ncm-order-step" id="order-step">
					<dl class="step-first current">
						<dt>生成订单</dt>
						<dd class="bg"></dd>
						<dd title="下单时间" class="date">
							<fmt:formatDate value="${order.subCreateTime}" type="both" />
						</dd>
					</dl>
					<dl class="<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}">current</c:if>">
						<dt>完成付款</dt>
						<dd class="bg"></dd>
						<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}">
							<dd title="付款时间" class="date">
								<fmt:formatDate value="${order.payDate}" type="both" />
							</dd>
						</c:if>
					</dl>
					<dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
						<dt>商家发货</dt>
						<dd class="bg"></dd>
						<c:if test="${not empty order.dvyDate}">
							<dd title="发货时间" class="date">
								<fmt:formatDate value="${order.dvyDate}" type="both" />
							</dd>
						</c:if>
					</dl>
					<dl
						class="<c:if test="${not empty order.finallyDate}">current</c:if>">
						<dt>确认收货</dt>
						<dd class="bg"></dd>
						<c:if test="${not empty order.finallyDate}">
							<dd title="确认收货时间" class="date">
								<fmt:formatDate value="${order.finallyDate}" type="both" />
							</dd>
						</c:if>
					</dl>
					<dl
						class="<c:if test="${not empty order.finallyDate}">current</c:if>">
						<dt>完成</dt>
						<dd class="bg"></dd>
						<c:if test="${not empty order.finallyDate}">
							<dd title="完成时间" class="date">
								<fmt:formatDate value="${order.finallyDate}" type="both" />
							</dd>
						</c:if>
					</dl>
				</div>


				<div class="order_pay_tab">
					<span style="line-height: 30px;">商品清单</span>
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_pay_table">
						<tbody>
							<tr>
								<th width="100">商品图片</th>
								<th width="232">商品名称</th>
								<th width="100">单价</th>
								<th width="100">数量</th>
								<th width="100">规格</th>
								<th width="100">总价</th>
								<!-- 					<th width="100">分销用户ID</th>
					<th width="100">分销佣金</th> -->
							</tr>
							<!-- S 商品列表 -->
							<c:forEach items="${order.orderItems}" var="orderItem"
								varStatus="orderItemStatues">
								<tr>
									<td align="center"><a target="_blank"
										href="<ls:url address='/views/${orderItem.prodId}'/>"> <img
											width="62" height="62"
											src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
									</a></td>
									<td align="left"><a target="_blank" class="blue"
										href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
									</td>
									<td align="center"><b class="red">
											¥ <fmt:formatNumber value="${orderItem.prodCash}" type="currency" pattern="0.00"></fmt:formatNumber></b></td>
									<td align="center"><fmt:formatNumber value="${orderItem.basketCount}" type="currency" pattern="0"></fmt:formatNumber></td>
									<td align="center">${empty orderItem.attribute?'无':orderItem.attribute}</td>
									<td align="center">¥ <fmt:formatNumber value="${orderItem.productTotalAmout+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></td>
								</tr>
							</c:forEach>
							<tr style="color: #999;">
								<td colspan="2" align="center">阶段1：定金</td>
								<td colspan="1" align="center">
									<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
									¥ <span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									(含运费：¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									</c:if>
									<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
									¥ <span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									</c:if>
								</td>
								<c:if test="${order.status eq 5 }">
									<td colspan="2"></td>
								</c:if>
								<c:if test="${order.status ne 5 }">
									<c:if test="${order.isPayDeposit eq 0}">
										<c:if
											test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
											<td colspan="2" align="center">用户可支付时间还剩<font
												color="red">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟
											</td>
											<td colspan="2" align="center">买家未付款</td>
										</c:if>
										<c:if
											test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
											<td colspan="4" align="center">已过期</td>
										</c:if>
									</c:if>
									<c:if test="${order.isPayDeposit eq 1}">
										<td colspan="4" align="center"><font color="green">买家已付款</font></td>
									</c:if>
								</c:if>
							</tr>
							<tr style="color: #999;">
								<td colspan="2" align="center">阶段2：尾款</td>
								<td colspan="1" align="center">
									<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
									¥ <span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									</c:if>
									<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
									¥ <span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									(含运费：¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									</c:if>
								</td>
								<c:if test="${order.status eq 5 }">
									<td colspan="4" align="center"></td>
								</c:if>
								<c:if test="${order.status ne 5 }">
									<c:if test="${order.isPayFinal eq 0}">
										<td colspan="2" align="center">尾款支付时间: <fmt:formatDate
												value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate
												value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
										</td>
									</c:if>
									<c:if test="${order.isPayFinal eq 0}">
										<td colspan="2" align="center"><c:choose>
												<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
												<c:when
													test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
												<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
											</c:choose></td>
									</c:if>
									<c:if test="${order.isPayFinal eq 1}">
										<td colspan="4" align="center"><font color="green">买家已付款</font></td>
									</c:if>
								</c:if>
							</tr>
						</tbody>
					</table>

				</div>
				<div class="order_follow">

					<ul class="order_follow_top" style="padding:0;margin-bottom:0;">
						<li style="cursor: pointer" class="this">付款信息</li>
						<li style="cursor: pointer">物流信息</li>
						<li style="cursor: pointer">订单日志</li>
					</ul>

					<div class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody>
								<tr>
									<td colspan="2">付款方式： <c:choose>
											<c:when test="${order.status eq 1}">
							       未支付
							 </c:when>
											<c:when test="${order.payManner eq 1}">
							       货到付款
							 </c:when>
											<c:otherwise>
							     在线支付
							 </c:otherwise>
										</c:choose>
									</td>
								</tr>
								<%-- 					<tr>
						<td>运费金额：¥${order.freightAmount}</td>
					</tr> --%>
								<tr>
									<td width="30%">定金金额：¥
										<fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber>
										<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
											(含运费：¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										</c:if>
									</td>
									<td width="70%">尾款金额：¥
										<!-- 全额支付 -->
										<c:if test="${order.payPctType eq 0}"><fmt:formatNumber value="0" type="currency" pattern="0.00"></fmt:formatNumber></span>
										</c:if>
										<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
											<fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>(含运费：
											¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										</c:if>
									</td>
								</tr>
								<tr>
									<td>优惠金额：¥ 0.00</td>
									<td>满减金额：¥ 0.00</td>
								</tr>
								<tr>
									<td>应支付金额：¥ <fmt:formatNumber value="${order.payableAmount+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- 物流信息 -->
					<div style="display: none" class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody id="order_kuaidi">
								<tr>
									<th align="left">时间</th>
									<th align="left">信息</th>
								</tr>
							</tbody>

						</table>
					</div>

					<!-- 订单日志 -->
					<div style="display: none" class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody id="order_ship">
								<tr>
									<th align="left">操作者</th>
									<th align="left">日志信息</th>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
				<div class="order_pay_info">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_info_table">
						<thead>
							<tr>
								<th style="text-align: left;" colspan="3"><span style="margin-left:10px;">订单信息</span></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>收货人信息</li>
											<li>下单用户： <a style="cursor: pointer;" id="show_userinfo"
												target="_blank"
												href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'><span>${order.userName}</span></a>
												<div class="more">
													<span class="arrow"></span>
													<ul id="shipping_ul">
													</ul>
												</div>
											</li>
											<li>收货人：${order.userAddressSub.receiver}</li>
											<li>收货地址：${order.userAddressSub.detailAddress}</li>
											<li>邮政编码：${order.userAddressSub.subPost}</li>
											<li>电话号码：${order.userAddressSub.telphone}</li>
											<li>手机号码：${order.userAddressSub.mobile}</li>
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>支付方式及配送方式</li>
											<c:choose>
												<c:when test="${order.status==1}">
													<li>支付方式：未支付</li>
													<li>支付时间：尚未支付，无支付时间</li>
												</c:when>
												<c:otherwise>
													<li>支付方式：${order.payTypeName}</li>
													<li>支付时间：<fmt:formatDate value="${order.payDate}"
															type="both" /></li>
												</c:otherwise>
											</c:choose>
											<li>配送运费：¥${order.freightAmount}</li>
											<li>配送时间：工作日9点-18点可配送</li>
											<li>配送方式： <c:choose>
													<c:when test="${order.dvyType eq 'mail'}">
										       平邮
										 </c:when>
													<c:when test="${order.dvyType eq 'express'}">
										       快递
										 </c:when>
													<c:when test="${order.dvyType eq 'ems'}">
										       EMS
										 </c:when>
												</c:choose>
											</li>
											<c:if test="${order.status==3 || order.status==4}">
												<li>物流公司： <a href="${order.delivery.delUrl}"
													target="_blank">${order.delivery.delName}</a></li>
												<li>发货时间： <fmt:formatDate value="${order.dvyDate}"
														pattern="yyyy-MM-dd HH:mm" /></li>
												<li>物流单号 <c:if test="${order.status==3}">[<a
															href="javascript:void(0);"
															onclick="deliverGoods('${order.subNo}');">修改</a>]</c:if>：${order.delivery.dvyFlowId}
												</li>
											</c:if>
											<!-- <li>快递公司：快递</li> -->
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>发票信息</li>

											<c:choose>
												<c:when test="${empty order.invoiceSub}">
													<li>发票信息：不开发票</li>
												</c:when>
												<c:otherwise>
													<li>发票类型：${order.invoiceSub.type==1?"普通发票":"增值税发票"}</li>
													<li>抬头类型：
														<c:choose>
															<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
															<c:otherwise>[公司抬头]</c:otherwise>
														</c:choose>
													</li>
													<li>发票抬头：${order.invoiceSub.company}</li>
													<c:if test="${order.invoiceSub.title ne 1}">
														<li>纳税人号：${order.invoiceSub.invoiceHumNumber}</li>
													</c:if>
													<c:if test="${order.invoiceSub.type eq 2}">
														<li>注册地址：${order.invoiceSub.registerAddr}</li>
														<li>注册电话：${order.invoiceSub.registerPhone}</li>
														<li>开户银行：${order.invoiceSub.depositBank}</li>
														<li>银行账号：${order.invoiceSub.bankAccountNum}</li>
													</c:if>
												</c:otherwise>
											</c:choose>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
						
					</table>

					<div class="order-pay-money">
						<ul class="money-ul">
							<li class="li"><span class="label">商品定金：</span>
								<span class="content">
									¥ <fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber>
									<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
										(含运费：¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									</c:if>
								</span>
							</li>
							<li class="li">
								<span class="label">商品尾款：</span>
								<span class="content">
									<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
										¥<fmt:formatNumber value="0" type="currency" pattern="0.00"></fmt:formatNumber>
									</c:if>
									<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
										¥ <fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>
										(含运费：¥ <fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									</c:if>
								</span>
							</li>
							<li class="li"><span class="label">优惠金额：</span> <span
								class="content">¥ 0.00</span></li>
							<li class="li"><span class="label">满减金额：</span> <span
								class="content">¥ 0.00</span></li>
							<li class="total-li"><span class="total-label">订单金额：</span>
								<span class="total-content">¥<fmt:formatNumber value="${order.actualTotalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</li>
						</ul>
					</div>
				</div>

				<div
					style="margin-top: 10px; overflow: hidden; padding: 0 0 10px; width: 100%; text-align: center; padding: 20px 0 10px;">
					<input type="button" onclick="window.history.go(-1)"
						class="${btnclass}" value="返回">&nbsp;&nbsp; <input
						type="button"
						onclick="window.open('<ls:url address="/admin/order/orderPrint/${order.subNo}"/>');"
						class="${btnclass}" value="订单打印">
				</div>

			</div>

		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/presellOrderAdminDetail.js'/>"></script>
<script>
var contextPath = "${contextPath}";
var dvyFlowId="${order.delivery.dvyFlowId}";
var dvyTypeId = '${order.delivery.dvyTypeId}';
var reciverMobile="${order.userAddressSub.mobile}";
var subId = '${order.subId}';
</script>
</html>