<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

      <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="cityList" requestURI="/admin/system/district/query" id="city" export="false" sort="external" class="${tableclass}" style="width:100%"> 
      <display:column style="width:55px;vertical-align: middle;text-align: center;" title="顺序<input type='checkbox'  id='checkbox' name='checkbox' onClick='javascript:selAll()' />">	
      <input type="checkbox" name="strArray" value="${city.id}"/></display:column>
     	<display:column title="名称"><input type="text" id="city" name="city" value="${city.city}" readonly/></display:column>
     	<display:column title="邮编"><input type="text" id="cityid" name="cityid" value="${city.cityid}" readonly/></display:column>
	    <display:column title="操作" media="html"  style="width: 180px;">
		<div class="am-btn-toolbar">
	      <div class="am-btn-group am-btn-group-xs">
		      <a href= "${contextPath}/admin/system/district/loadcity/${city.id}" title="修改">
		        	 <button class="am-btn am-btn-default am-btn-xs am-text-secondary"><span class="am-icon-pencil-square-o"></span> 修改</button>
			      			
		      </a>
		    <a href='javascript:confirmDeleteCity("${city.id}","${city.city}")' title="删除">
		      		<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" ><span class="am-icon-trash-o"></span> 删除</button>
		      </a>
	    </div>
	   </div>
	      </display:column>
	    </display:table>	    
	     <input type="button" class="${btnclass}" value="刪除" style="float: left" onclick="return deleteAction('deletecity');"/>
        <script language="JavaScript" type="text/javascript">
			 
		</script>
