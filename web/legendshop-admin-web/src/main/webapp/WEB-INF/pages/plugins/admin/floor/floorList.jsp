<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>首页楼层装修 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.am-table-child > thead:first-child > tr:first-child > th{
	background: #f9f9f9;
	border-left:none;
}
.am-table-child > thead:first-child > tr:first-child > th:first-child{
	border-left:1px solid #efefef;
}
.am-table-child > thead:first-child > tr:first-child > th:last-child{
	border-right:1px solid #efefef;
}
.am-table-bordered >tbody > tr> td {
   border-left:none !important;
}
.am-table-bordered >tbody > tr> td:first-child {
   border-left:1px solid #efefef !important;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">PC首页装修&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">首页楼层装修</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/floor/query" id="form1"
				method="get">
					<div class="criteria-div">
						<span class="item">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							 楼层标题 ：<input type="text" name="name" maxlength="50" value="${floor.name}" class="${inputclass}" /> 
							 <input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
							 <input type="button" value="创建" class="${btnclass}" onclick='window.location="<ls:url address='/admin/floor/load'/>"' />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/floor/query" id="item"
					export="false" sort="external" class="${tableclass}"
					style="width:100%;">
					<%--     	<display:column  class="orderwidth" title="<input type='checkbox'  id='checkAllbox' name='checkbox'/>"> --%>
					<%--     		<input type="checkbox" name="floorId" value="${item.flId}" status="${item.status}"  > --%>
					<%--     	</display:column> --%>
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="楼层标题" property="name"></display:column>
					<c:set var="subFloorList" value="${item.subFloors}" scope="request" />
					<display:column title="子楼层">
						<c:if test="${fn:length(subFloorList) > 0}">
							<display:table name="subFloorList" id="child"
								class="am-table am-table-bordered am-table-child" cellpadding="0" style="min-width: 275px !important;"
								cellspacing="0">
								<display:column title="名称" style="width:100px;">${child.name}</display:column>
								<display:column title="次序" style="width:50px;">${child.seq}</display:column>
								<display:column title="操作" media="html" autolink="true"
									style="width:50px">
										<div class="table-btn-group">
											<c:choose>
												<c:when test="${child.status == 1}">
													<button class="tab-btn"
														name="childStatusImg" itemId="${child.sfId}"
														itemName="${child.name}" status="${child.status}">
														下线
													</button>
													<span class="btn-line">|</span>
												</c:when>
												<c:otherwise>
													<button
														class="tab-btn"
														name="childStatusImg" itemId="${child.sfId}"
														itemName="${child.name}" status="${child.status}">
														上线
													</button>
													<span class="btn-line">|</span>
												</c:otherwise>
											</c:choose>

											<div data-am-dropdown="" class="am-dropdown">
												<button data-am-dropdown-toggle=""
													class="am-dropdown-toggle tab-btn">
													<span class="am-icon-cog"></span> <span
														class="am-icon-caret-down"></span>
												</button>
												<ul class="am-dropdown-content">
													<li><a
														href="<ls:url address='/admin/subFloor/load/${child.sfId}'/>">编辑</a></li>

													<li><a
														href='javascript:deleteBychildId("${child.sfId}")'>删除</a></li>
												</ul>
											</div>
										</div>
								</display:column>
							</display:table>
						</c:if>
					</display:column>

					<display:column title="楼层版式">
						<c:choose>
							<c:when test="${item.layout == 'wide_adv_rectangle_four'}">
                    (4X矩形广告)
                </c:when>
							<c:when test="${item.layout == 'wide_adv_brand'}">
                    (广告+品牌)
                </c:when>
							<c:when test="${item.layout == 'wide_goods'}">
                    (5x商品)
                </c:when>
							<c:when test="${item.layout == 'wide_adv_five'}">
                    (5x广告)
                </c:when>
							<c:when test="${item.layout == 'wide_adv_square_four'}">
                    (4x方形广告)
                </c:when>
							<c:when test="${item.layout == 'wide_adv_eight'}">
                    (8x广告)
                </c:when>
							<c:when test="${item.layout == 'wide_adv_row'}">
                    横排广告
                </c:when>
							<c:when test="${item.layout == 'wide_blend'}">
                    混合模式
                </c:when>
							<c:otherwise>
                    混合模式
                </c:otherwise>
						</c:choose>
					</display:column>

					<%--  <display:column title="右上内容类型" style="width: 100px;">
                 <ls:optionGroup type="label" required="true" cache="true"  beanName="CONTENT_TYPE" selectedValue="${item.contentType}"/>
         </display:column>
         <display:column title="右下内容类型" style="width: 100px;">
                 <ls:optionGroup type="label" required="true" cache="true"  beanName="CONTENT_TYPE2" selectedValue="${item.contentType2}"/>
         </display:column> --%>
					<display:column title="次序" property="seq"></display:column>
					<display:column title="操作" media="html" style="width:130px">
							<div class="table-btn-group">
								<c:choose>
									<c:when test="${item.status == 1}">
										<button
											class="tab-btn"
											name="statusImg" itemId="${item.flId}"
											itemName="${item.name}" status="${item.status}">
											下线
										</button>
										<span class="btn-line">|</span>
									</c:when>
									<c:otherwise>
										<button
											class="tab-btn"
											name="statusImg" itemId="${item.flId}"
											itemName="${item.name}" status="${item.status}">
											上线
										</button>
										<span class="btn-line">|</span>
									</c:otherwise>
								</c:choose>

								<div data-am-dropdown="" class="am-dropdown">
									<button data-am-dropdown-toggle=""
										class="am-dropdown-toggle tab-btn">
										<span class="am-icon-cog"></span> <span
											class="am-icon-caret-down"></span>
									</button>
									<ul class="am-dropdown-content">

										<c:if
											test="${empty item.layout || item.layout == 'wide_blend'}">
											<li><a
												href="<ls:url address='/admin/subFloor/loadSubFloor/${item.flId}'/>">创建子楼层</a></li>
										</c:if>

										<li><a
											href="<ls:url address='/admin/floor/loadFloorEdit/${item.flId}'/>">楼层编辑</a></li>

										<li><a
											href="<ls:url address='/admin/floor/load/${item.flId}'/>">修改</a></li>

										<li><a href='javascript:deleteById("${item.flId}")'>删除</a></li>
									</ul>
								</div>

							</div>
					</display:column>
				</display:table>
				<!-- <input class="criteria-btn" type="button" value="永久刪除"
					style="float: left" onclick="return deleteAction();"> -->
				<!--     <input class="criteria-btn" type="button" value="批量下线" style="float: left" onclick="return batchOffline();"> -->
				 <div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		      	</div>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorList.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</html>
