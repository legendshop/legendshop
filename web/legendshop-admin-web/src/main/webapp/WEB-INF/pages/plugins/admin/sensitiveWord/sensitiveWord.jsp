<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">业务设置&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/sensitiveWord/query"/>">敏感字管理</a>
               			&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"> 敏感字编辑</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/system/sensitiveWord/save"
				method="post" id="form1">
				<input type="hidden" id="curPageNO" name="curPageNO"
					value="${curPageNO}" />
				<input id="sensId" name="sensId" value="${sensitiveWord.sensId}"
					type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%;margin-top: 10px;">

						<%--<tr>
		        <td width="200px">
		          	<div align="center">是否全局敏感字: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		        		<select id="isGlobal" name="isGlobal" onchange="change(this.value)">
		        			<ls:optionGroup type="select" required="false" cache="true" beanName="YES_NO" selectedValue="${sensitiveWord.isGlobal}"/>          			
		        		</select>	  
		        </td>
		</tr>--%>
						<%--<tr id="one">
		<td>
		     <div align="center">分类:</div>
		</td>
			<td>
			<select class="combox"  id="sortId" name="sortId"  requiredTitle="-- 一级分类 -- "  childNode="nsortId" selectedValue="${sensitiveWord.sortId}"  retUrl="${contextPath}/sort/loadDefaultSorts">	
			</select>
			<select class="combox"   id="nsortId" name="nsortId"  requiredTitle="-- 二级分类 --"  selectedValue="${sensitiveWord.nsortId}" showNone="false"  parentValue="${sensitiveWord.sortId}" childNode="subNsortId" retUrl="${contextPath}/sort/loadNSorts/{value}">
			</select>
			<select class="combox"   id="subNsortId" name="subNsortId"  requiredTitle="-- 三级分类 --"  selectedValue="${sensitiveWord.subNsortId}"  showNone="false"   parentValue="${sensitiveWord.nsortId}" retUrl="${contextPath}/sort/loadSubNSorts/{value}">
			</select>
			</td>
		</tr>--%>
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>关键字：
								</div>
							</td>
							<td  align="left" style="padding-left: 2px;"><input type="text" name="words" id="words" 
								value="${sensitiveWord.words}" class="${inputclass}" maxlength="20" /></td>
						</tr>

						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input type="submit" value="保存" class="${btnclass}" /> 
								    <input type="button" value="返回" class="${btnclass}"
										onclick="window.location='<ls:url address="/admin/system/sensitiveWord/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/sensitiveWord.js'/>"></script>
<script type="text/javascript">
 	var contextPath = '${contextPath}';
	$.validator.setDefaults({});
</script>
</html>
