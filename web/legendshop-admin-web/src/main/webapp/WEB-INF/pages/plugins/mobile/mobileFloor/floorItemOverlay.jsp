<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
</head>
<body>

	<form:form  action="${contextPath}/admin/mobileFloor/floorItemOverlay/${fId}" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<a href="<ls:url address='/admin/index'/>" target="_parent">首页</a> &raquo; 商城管理  &raquo; 
				    	<a href="<ls:url address='/admin/mobileFloorItem/query'/>">楼层内容</a>
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		   			 <input type="hidden"  id="table_length" value="${table_length}">
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							标题：<input type="text" id="title" name="title"/>	
				            <input type="submit" value="搜索"/>
				            <input type="button" value="创建楼层内容" onclick="addFloorItem('${fId}')"/>
				      </div>
				      <div style="color: red">(顺序排最前的是:大图;后面两张也是根据顺序的大小来分上下;)</div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/mobileFloorItem/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
	    	<display:column title="顺序" class="orderwidth" property="seq"></display:column>
     		<display:column title="标题" property="title"></display:column>
     		<display:column title="图片">
     			<img title="${item.linkPath}"  src="<ls:images item="${item.pic}"/>" style="width: 230px; height: 120px;"/>
     		</display:column>
     		<display:column title="状态">
     			<c:choose>
     				<c:when test="${item.status == 1}">上线</c:when>
     				<c:otherwise>下线</c:otherwise>
     			</c:choose>
     		</display:column>

	    <display:column title="操作" media="html">
		      <a href="javascript:updateFloorItem('${fId}','${item.id}')"  title="修改">
		     		 <img alt="修改" src="<ls:templateResource item='/resources/common/images/grid_edit.png'/>">
		      </a>
		      <a href='javascript:deleteById("${item.id}")' title="删除">
		      		<img alt="删除" src="<ls:templateResource item='/resources/common/images/grid_delete.png'/>">
		      </a>
	      </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
    </div>
</body>
</html>
<script>
function updateFloorItem(parentId,id){
	var path = "${contextPath}/admin/mobileFloor/updateFloorItem/"+parentId+"/"+ id; 
	layer.open({
		  title :"编辑楼层内容",
		  type: 2, 
		  content: path,
		  area: ['650px', '400px']
		}); 
}

function addFloorItem(parentId){
	var path = "${contextPath}/admin/mobileFloor/loadFloorItem/"+parentId;
	openOverlay(path);
}

function openOverlay(path){
	var length = $("#table_length").val();
	if(length!="" && length!=null&& length!=undefined){
   		if(length>=3){
			 layer.msg("广告不能超过3个",{icon:0});
			 return;
		}
    }
	layer.open({
	  title :"编辑楼层内容",
	  type: 2, 
	  content: path,
	  area: ['650px', '400px']
	}); 
}

function deleteById(id){
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				type : "POST",
				url : '${contextPath}/admin/mobileFloor/deleteFloorItem/'+id,
				dataType : 'json',
				async : false,//同步
				success : function(data) {
					if (data == "fail") {
						var msg = "删除失败";
						layer.msg(msg,{icon:2});
						return;
					}else{
						layer.msg("删除成功！",{icon:1},function(){
							location.reload();
						});
						return;
					}
				}
			});
	   });
}
</script>