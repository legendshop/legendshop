<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>部门管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
	<style type="text/css">
	div.content_wrap {width: 100%;height:90%;}
	div.content_wrap div.left{float: left; padding:10px 0px 50px 20px}
	div.content_wrap div.right{float: right;}
	div.deptInfoBackground{ background:#f5f5f5; width:70%;height:100%;text-align:center;}
	div.zTreeBackground { min-width:15%;height:100%;text-align:left;background:#f5f5f5 }
	div.deptEditBackground{ background-color:white;border:1px solid;width:33%;height:300px;display:none;position: absolute;z-index:11;top:33%;left:33%;}
	
	.ztree * { /*font-size: 12pt;*/ background：#f5f5f5;}
	.ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
	.ztree li span.button.switch.level0 {visibility:hidden; width:1px;}
	.ztree li ul.level0 {padding:0; background:none;}
	label{
	font-weight: 400 !important;
	font-size: 12px;
	line-height: 26px;
	}
	</style>
 </head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	 	<div class="admin-content" id="admin-content" style="overflow-x:auto;">
    <form:form  action="${contextPath}/admin/department/query" id="form1" method="post">
    <table  class="${tableclass} title-border" style="width: 100%;margin-bottom: 20px;">
	    	<tr><th class="title-border">权限管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">部门管理</span></th></tr>
	</table>
    </form:form>

 <div class="content_wrap" align="center" style="width:95%; height:100%;">
	    
	    <div align="center" style="margin-left:30px; height:100%; float: left; " >
	    	<div id="treeDiv" class="zTreeBackground left" style="border: 1px solid #ddd; background：#f5f5f5;width:220px;overflow-x:auto">
		    	<ul id="deptTree" class="ztree"></ul>
		    </div>
	    </div>
	    
	    <div align="center" style="float:left;">
	        <div id="infoDiv" style="width:400px;margin-left:20px;background:#f8f8f8; padding:20px 20px; border:solid 1px #e8e8e8;text-align: left;">
	        	<div class="line"><label>部门名称：</label><span class="info" id="deptName">${dept.name}</span></div>
	        	<div class="line"><label>部门联系人：</label><span class="info" id="deptContact">${dept.contact}</span></div>
	        	<div class="line"><label>部门电话：</label><span class="info" id="deptMobile">${dept.mobile}</span></div>
	        	${dept.recDate}
	        	<div class="line"><label>记录时间：</label><span class="info" id="recDate"><fmt:formatDate value="${dept.recDate}" pattern="yyyy-MM-dd HH:mm" /> </span></div> 
		     </div>
		</div>

    </div>
    </div>
    </div>
    </body>
  <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.exedit-3.5.js'/>"></script>

	<script type="text/javascript">

		var freeze = false;
		var setting = {
			view: {
				addHoverDom: addHoverDom,
				removeHoverDom: removeHoverDom,
				selectedMulti: false
			},
			edit: {
				enable: true,
				editNameSelectAll: true,
				showRemoveBtn: showRemoveBtn,
				showRenameBtn: showRenameBtn,
				removeTitle: "删除部门",
				renameTitle: "编辑信息"
			},
			drag: {
				isCopy:false,
				isMove:true
			},
			data: {
				simpleData: {
					enable: true,
					pIdKey: "parentId"
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				beforeRemove: beforeRemove,
				beforeEditName: beforeRename,
				onClick: onClick
			}
		};
		// 初始化按钮
		function showRemoveBtn(treeId, treeNode) {
			return treeNode.level > 0;
		}
		function showRenameBtn(treeId, treeNode) {
			return treeNode.level >= 0;
		}
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='增加子部门' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var btn = $("#addBtn_"+treeNode.tId);
			
			if (btn) btn.bind("click", function(){
			    freeze = true;
				addDept(treeNode);
			});
		}
		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
		}
		// 响应事件
		function beforeDrag(treeId, treeNodes) {
			freeze = true;
			
			return treeNodes[0].level > 0;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
			freeze = true;
			
			if( moveType != "inner" ) return false;
			
			var operation = "确定要将 " + treeNodes[0].name + " 移动到 " + targetNode.name + " 吗";
			
			layer.confirm(operation, {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   editPid(treeNodes[0],targetNode);
			   });
		    return false;
		}
		function beforeRemove(treeId, treeNode) {
			freeze = true;
			
			if(treeNode.isParent){
				layer.alert("该部门有子部门，无法直接删除",{icon: 0});
				return false;
			}
			
			var operation = "确认删除部门 ： " + treeNode.name + " 吗？";
			
			layer.confirm(operation, {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   delDept(treeNode);
			   });
			
			return false;
		}
		function beforeRename(treeId, treeNode) {
			freeze = true;
			editDept(treeNode);
			return false;
		}
		
		function onClick( event, treeId, treeNode){
			setTimeout(function(){
				if(!freeze){
					showDept( treeNode );
				}
				freeze = false;
			},150);
		}
		function showDept(deptInfo){
        	var lastInfo = 	deptInfo.lastModify == undefined ?  deptInfo : deptInfo.lastModify ;
		    $("#deptName").html(lastInfo.name);
			$("#deptContact").html(lastInfo.contact);
			$("#deptMobile").html(lastInfo.mobile);
			var recDate = new Date(lastInfo.recDate);
			$("#recDate").html(recDate.getFullYear() + "-" + (recDate.getMonth()+1) + "-" +recDate.getDate() + " " + recDate.getHours() + ":" + recDate.getMinutes());
    	}
		
		// 处理函数
    	function editPid(thisDept,parentDept) {
		    $.ajax({
				url: "${contextPath}/admin/department/updatePid",  
				data: {"id":thisDept.id,"pid":parentDept.id},
				type:'post',
				async : true,
				dataType : 'json', 
				success:function(data){
					if(data == "OK"){
						var zTree = $.fn.zTree.getZTreeObj("deptTree");
											
						thisDept.pId = parentDept.id;
						if( thisDept.lastModify != undefined )
							thisDept.lastModify.pId = parentDept.id;
							
						zTree.moveNode( parentDept, thisDept, "inner");
						layer.msg('操作成功！', {icon: 1,time:700},function(){
	                		window.location.reload();
	                	});
					}else{
						layer.alert(data,{icon: 2});
					}
				}
			});
		}
    	function delDept(thisDept) {
		    $.ajax({
				url: "${contextPath}/admin/department/deleteDept", 
				data: {"id":thisDept.id},
				type:'post', 
				async : true,
				dataType : 'json', 
				success:function(data){
					if(data == "OK"){
						var zTree = $.fn.zTree.getZTreeObj("deptTree");
						zTree.removeNode( thisDept, false );
						layer.msg('操作成功！', {icon: 1,time:700},function(){
	                		window.location.reload();
	                	});
					}else{
						layer.alert(data,{icon: 2});
					}
				}
			});
		}
		
		// 增加子部门与编辑信息的工作外包给对话框
	    function addDept( parentDept ){
	    	showDlg( parentDept, false );
	    }
    
	    function editDept( thisDept ){
	    	showDlg( thisDept, true );
	    }
	    
	    /** 回调函数 **/
	    function dlgCallback( data, isEdit ){
	    
	    	var success = false;
	        var zTree = $.fn.zTree.getZTreeObj("deptTree");
    		if( isEdit ){
    			var newInfo = eval("(" + eval("(" + data + ")")+ ")");
              	var oldInfo = zTree.getNodeByParam("id", newInfo.id); 	
              	if( oldInfo != undefined ){
              		success = true;
              		oldInfo.lastModify = newInfo;
              		oldInfo.name = oldInfo.lastModify.name;
              		zTree.updateNode(oldInfo);
              	}
	    	}else{
    			var newDept = eval("(" + eval("(" + data + ")")+ ")");
    			var parent = zTree.getNodeByParam("id", newDept.parentId);
    			if( typeof(newDept) == "object" ){
    				success = true;
    				zTree.addNodes(parent, newDept);
    			}
	    	}
    		if( !success ){
    			layer.alert(data,{icon: 2});
			}
	    }
	    
	    /**
	      编辑部门
	    */
		 function showDlg( deptInfo, toEdit ){
	   
	    	var page = "${contextPath}/admin/department";
	    	
	    	var title="";
	    	var conf = { width:400, height:300, lock:true,
	    				 background:'#000', opacity:0.5 };
	    		
	    	if( toEdit ){
	    		page += "/editDept/" + deptInfo.id;
	    		title = "编辑部门";
	    	}else{
	    		page += "/addDept/" + deptInfo.id;
	    		title= "新增部门";
	    	}
	    	layer.open({
				title :title,
				  type: 2, 
				  id: 'dept',
				  content: [page,'no'], 
				  area: ['300px', '360px']
				}); 
	    }
	    
	function selAll(){
		var title = document.getElementsByName("checkbox")[0];
		var allSel = document.getElementsByName("strArray");
		for(i=0;i<allSel.length;i++){
			   allSel[i].checked = title.checked;
		}
		selectCheckboxItem();
	}
	
	function getcheckIds(){

    	var selAry = document.getElementsByName("strArray");
    	var checkIds = new Array(); 
    	for(i=0;i<selAry.length;i++){
    		if(selAry[i].checked){
				 checkIds.push( "'" + selAry[i].value +"'" );
		    }
    	}
    	// console.debug("选择的权限资源Ids:"+ checkIds);
   		return checkIds;
	}

	function saveSel(id){
	
		var roleIdList = document.getElementsByName("roleIdList")[0];
		var selRole = getcheckIds();
		roleIdList.value = '[' + selRole + ']';
		
		layer.confirm("要保存当前数据吗？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
				$("#roleForm").ajaxForm().ajaxSubmit(function(body){
					$("#roleTable").html(body);
	       		});
			   
		   });
	    return false;
	}
	
			// 初始化树
		$(document).ready(function(){
			$.fn.zTree.init($("#deptTree"), setting, ${deptTree});
			$.fn.zTree.getZTreeObj("deptTree").expandAll(true);
		});
		
</script>
</html>