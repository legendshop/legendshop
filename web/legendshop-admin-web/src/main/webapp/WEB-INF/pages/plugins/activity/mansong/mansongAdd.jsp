<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>营销管理 - 折扣添加</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport"content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed"href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
 	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
 <table class="${tableclass}" style="width: 100%">
			    	<tr><th> <strong class="am-text-primary am-text-lg">营销管理/</strong><a href="<ls:url address="/admin/shopDetail/query"/>">满送促销</a> /  添加活动</th></tr>
			    </table>
		<form:form  action="${contextPath}/admin/marketing/saveMjMarketing" id="add_form" method="post">
			<table style="width: 100%" class="${tableclass}" id="col1">
				<tr>
				 <td>
         			 <div align="right"> <font color="ff0000">*</font>活动名称： </div>
      			 </td>
      			 <td>
      			 	<input type="text" class="${inputclass}" maxlength="25" name="marketName" id="marketName" size="30">&nbsp;&nbsp;活动名称最多为25个字符
      			 </td>
      			 </tr>
      			 <tr>
      			 	<td>
      			 		 <div align="right"> <font color="ff0000">*</font>开始时间： </div>
      			 	</td>
      			 	<td>
      			 		<input type="text" class="text w130 Wdate"  name="startTime" id="startTime" readonly="readonly" >
      			 	</td>
      			 </tr>
      			 <tr>
      			 	<td>
      			 		 <div align="right"> <font color="ff0000">*</font>结束时间： </div>
      			 	</td>
      			 	<td>
      			 		<input type="text" class="text w130 Wdate"  name="endTime" id="endTime" readonly="readonly">
      			 	</td>
      			 </tr>
      			 <tr>
      			 	<td><div align="right"> <font color="ff0000">*</font>选择商品： </div></td>
      			 	<td>
      			 		 <label for="goods_status_1">
					       <input type="radio" checked="checked" id="goods_status_1" value="1"
								name="isAllProds">全部商品</label>
						   <label for="goods_status_0"  style="margin-left: 10px;"> 
					         <input type="radio" id="goods_status_0" value="0" name="isAllProds">部分参与商品
					     </label>
      			 	</td>
      			 </tr>
      			 
      			 <tr>
      			 	<td><div align="right"> <font color="ff0000">*</font>促销类型： </div></td>
      			 	<td>
      			 		 <label for="goods_type_0"> 
					        <input type="radio" checked="checked"  id="goods_type_0" value="0" name="type" >满减促销
					       </label>
						    <label for="goods_type_1"  style="margin-left: 10px;"> 
					           <input type="radio" id="goods_type_1" value="1" name="type">满折促销
					        </label>
      			 	</td>
      			 </tr>
      			 <tr>
      			 	<td><div align="right"> <font color="ff0000">*</font>活动规则： </div></td>
      			 	<td>
      			 		<input type="hidden" name="rule_count" id="mansong_rule_count">
      			 		<ul class="ncsc-mansong-rule-list" id="manjian_rule_list"></ul>
                   		<ul class="ncsc-mansong-rule-list" id="manze_rule_list"></ul>
                   		 <a class="ncbtn ncbtn-aqua" style="display: none;" id="btn_add_rule" href="javascript:void(0);"><i class="icon-plus-sign"></i>添加规则</a>
				           <div  id="add_manjian_rule">
				            <div class="ncsc-mansong-rule">
				              <span>满&nbsp;<input type="text" class="text w50" id="manjian_price"><em class="add-on">元</em>，</span>
				              <span>立减现金&nbsp;<input type="text" class="text w50" id="manjian_discount"><em class="add-on">元</em>，</span>
				                <div class="gift" id="mansong_goods_item"></div>
				                </div>
				                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
				               <div class="mt10">
				                  <a class="ncbtn ncbtn-aqua"  id="save_mj_rule" href="javascript:void(0);"><i class="icon-ok-circle"></i>确定规则设置</a>
				                  <a class="ncbtn ncbtn-bittersweet" id="btn_cancel_mj_rule" href="javascript:void(0);"><i class="icon-ban-circle"></i>取消</a></div>
				             </div>
				            <div style="display:none;" id="add_manze_rule">
				                <div class="ncsc-mansong-rule">
				                  <span>满&nbsp;<input type="text" class="text w50" id="manze_price"><em class="add-on">元</em>，</span>
				                  <span>打&nbsp;<input type="text" class="text w50" id="manze_discount"><em class="add-on">折</em>，</span>
				                  <div class="gift" id="mansong_goods_item"></div>
				                </div>
				                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
				               <div class="mt10" style="padding: 10px;">
				                  <a class="ncbtn ncbtn-aqua" id="save_mz_rule" href="javascript:void(0);"><i class="icon-ok-circle"></i>确定规则设置</a>
				                  <a class="ncbtn ncbtn-bittersweet" id="btn_cancel_mz_rule" href="javascript:void(0);"><i class="icon-ban-circle"></i>取消</a></div>
				             </div>
      			 	</td>
      			 </tr>
      			 <tr>
      			 	<td><div align="right">备注： </div></td>
      			 	<td>
      			 	<textarea class="textarea w400" maxlength="100" id="remark" rows="5" cols="100" name="remark" placeholder="活动备注最多为100个字符"></textarea>
      			 	</td>
      			 </tr>
      			 <tr>
      				<td colspan="2"> 
      					<div style="margin-left: 190px;">
      						<input type="submit" value="保存"  class="${btnclass}">
      						 <input type="button" class="${btnclass}" value="返回" onclick="window.location='<ls:url address='/admin/marketing/mansong/query?state=${state}' />'">
      					</div>
      				</td>	
      			 </tr>
      			 <tr>
      			 	<td colspan=2">
      			 		1. 满减活动可选店铺所有商品或只选择部分商品参加，活动时间不能和已有活动重叠
					<br>
					2. 每个满减活动最多可以设置3个价格级别，点击新增级别按钮可以增加新的级别，价格级别应该由低到高
					<br>
					3. 选择商品分为全场商品和部分商品[部分商品需要在列表进行管理添加商品信息]
					<br>
					4. 活动类型分为满减促销规则[满X送X元],满折促销[满X元打折X折扣],至少需要选择一种
      			 	</td>
      			 </tr>
			</table>
		</form:form>
	<div class="clear"></div>
	</div>
	</div>
	</body>
 <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    
    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#startTime").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };
    
    
    //页面输入内容验证
    $("#add_form").validate({
        ignore: "",
        onfocusout: false,
        rules : {
            marketName : {
                required : true
            },
            startTime : {
                required : true,
            },
            endTime : {
                required : true,
                greaterThanStartDate : true
            },
            status:{
               required : true
            },
            type:{
               required : true
            },
            rule_count: {
                required: true,
                min: 1
            }
        },
        messages : {
            marketName : {
                required : "活动名称不能为空"
            },
            startTime : {
                required : "开始时间不能为空"
            },
            endTime : {
                required : "结束时间不能为空",
                greaterThanStartDate : "结束时间必须大于开始时间"
            },
            status:{
               required : "请选择商品参与方式",
            },
            type:{
               required : "请选择促销类型",
            },
            rule_count: {
                required: "请至少添加一条规则并确定",
                min: "请至少添加一条规则并确定"
            }
        },
        submitHandler:function(form){
            $("#add_form").ajaxForm().ajaxSubmit({
			   success:function(data) {
			      var result=eval(data);
			      if(result=='OK'){
			        layer.msg('添加成功',{icon: 1,time:500},function(){
				        window.location.href=contextPath+"/admin/marketing/mansong/query";
			        });
			        return;
			      }else{
			         layer.alert(result,{icon:2});
			         return ;
			      }
			   }
	        });
        }
    
    });
    
     // 限时添加规则窗口
    $('#btn_add_rule').on('click', function() {
        var type= $("input[name='type']:checked").val();
        if(type=='0'){  //满减促销
              $('#add_manjian_rule').show();
              $('#add_manze_rule').hide();
              $("#manze_rule_list").hide();
              $("#manjian_rule_list").show();
               $('#manjian_price').val('');
              $('#manjian_discount').val('');
         }else if(type=='1'){ //满折促销 
              $('#add_manze_rule').show();
              $('#add_manjian_rule').hide();
              $("#manjian_rule_list").hide();
              $("#manze_rule_list").show();
              $('#manze_price').val('');
              $('#manze_discount').val('');
         }
        $('#btn_add_rule').hide();
    });
    
      $('#btn_add_rule').hide(); 
      $('#add_manjian_rule').show();
      $('#add_manze_rule').hide();
      $("#goods_type_0").attr("checked","checked");
     
     
     $("input[name='type']").change(function(){
         var type=$(this).val();
         var rule_count ;
         if(type=='0'){  //满减促销
               rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
              if( rule_count >= 3) {
                 $('#add_manjian_rule').hide();
              }else{  
                 $('#add_manjian_rule').show();
              }
              $('#add_manze_rule').hide();
              $('#manjian_price').val('');
              $('#manjian_discount').val('');
               $("#manze_rule_list").hide();
              $("#manjian_rule_list").show();
         }else if(type=='1'){ //满折促销 
            var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
             if(rule_count >= 3) {
                  $('#add_manze_rule').hide();
              }else{  
                 $('#add_manze_rule').show();
              }
              $('#add_manjian_rule').hide();
              $('#manze_price').val('');
              $('#manze_discount').val('');
              $("#manjian_rule_list").hide();
              $("#manze_rule_list").show();
         }
         $('#btn_add_rule').hide();
         $('#mansong_rule_count').val('');
     });
        
        
    
     // 规则保存
    $('#save_mj_rule').on('click', function() {
        var mansong = {};
        mansong.price = Number($('#manjian_price').val());
        if(isNaN(mansong.price) || mansong.price <= 0) {
            layer.alert("规则金额不能为空且必须为数字",{icon:0});
            return false;
        } 
        
        mansong.discount = Number($('#manjian_discount').val());
        if(isNaN(mansong.discount) || mansong.price <= 0) {
           layer.alert("规则金额不能为空且必须为数字",{icon:0});
            return false;
        } 
        if(mansong.discount > mansong.price){
          layer.alert("满减金额必须小于规则金额",{icon:0});
           return false;
        }
        
        var mansong_rule_item=ruleHtml(mansong.price,mansong.discount);
        
        $('#manjian_rule_list').append(mansong_rule_item);
        
        close_mj_rule();
    });
    
    
     // 规则保存
    $('#save_mz_rule').on('click', function() {
        var mansong = {};
        mansong.price = Number($('#manze_price').val());
        if(isNaN(mansong.price) || mansong.price <= 0) {
           layer.alert("规则金额不能为空且必须为金额",{icon:0});
            return false;
        } 
        if($('#manze_discount').val()==""){
            layer.alert("规则金额不能为空且必须为数字",{icon:0});
            return false;
        }
        mansong.discount = Number($('#manze_discount').val());
        if(isNaN(mansong.discount)) {
            layer.alert("规则金额不能为空且必须为数字",{icon:0});
            return false;
        } 
        
        var re =/^[1-9]([.]{1}[1-9])?$/;  
		if(!re.test( mansong.discount )) {
		 	layer.alert("规则金额或规则折扣只能是0-10之间的1位整数或小数1-9.9之间",{icon:0});
            return false;
		}
		
        var mansong_rule_item=ruleHtml2(mansong.price,mansong.discount);
        $('#manze_rule_list').append(mansong_rule_item);
        close_mz_rule();
    });
    
    
    
     // 关闭规则添加窗口
    function close_mz_rule() {
        var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
        if( rule_count >= 3) {
            $('#btn_add_rule').hide();
        } else {
            $('#btn_add_rule').show();
        }
        $('#add_manze_rule').hide();
        $('#mansong_rule_count').val(rule_count);
    }
    
     // 关闭规则添加窗口
    function close_mj_rule() {
        var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
        if( rule_count >= 3) {
            $('#btn_add_rule').hide();
        } else {
            $('#btn_add_rule').show();
        }
        $('#add_manjian_rule').hide();
        $('#mansong_rule_count').val(rule_count);
    }
    
       // 删除已添加的规则
    $('#manjian_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
        $(this).parents('[nctype="mansong_rule_item"]').remove();
        close_mj_rule();
    });
    
          // 删除已添加的规则
    $('#manze_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
        $(this).parents('[nctype="mansong_rule_item"]').remove();
        close_mz_rule();
    });

    // 取消添加规则
    $('#btn_cancel_mj_rule').on('click', function() {
        close_mj_rule();
    });
    
    // 取消添加规则
    $('#btn_cancel_mz_rule').on('click', function() {
        close_mz_rule();
    });
    
    function ruleHtml(price,discount){
       var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
       var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>元， </span><span>立减现金<strong>"+discount+"</strong>元， </span>";
       html+="<input type='hidden' value='"+price+","+discount+"' name='manjian_rule["+rule_count+"]'>";
       html+="<a class='ncbtn-mini ncbtn-grapefruit' href='javascript:void(0);' nctype='btn_del_mansong_rule'><i class='icon-trash'></i>删除</a></li>";
       return html;
    }
    
     function ruleHtml2(price,discount){
       var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
       var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>元， </span><span>打<strong>"+discount+"</strong>折， </span>";
       html+="<input type='hidden' value='"+price+","+discount+"' name='manze_rule["+rule_count+"]'>";
       html+="<a class='ncbtn-mini ncbtn-grapefruit' href='javascript:void(0);' nctype='btn_del_mansong_rule'><i class='icon-trash'></i>删除</a></li>";
       return html;
    }
    
     laydate.render({
   		 elem: '#startTime',
   		 calendar: true,
   		 theme: 'grid',
 		 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endTime',
   	     calendar: true,
   	     theme: 'grid',
 		 trigger: 'click'
      });
     
</script>  
</html>