<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<html>
<head>
    <title>参数编辑</title>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>"/>

    <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
    <style type="text/css">
        body {
            min-height: 96%;
        }

        #form1 {
            margin: 15px;
            line-height: 28px;
        }

        .kui-combobox {
            width: 153px;
        }

        input[type="text"] {
            width: 158px;
            height: 28px;
            border: 1px solid #e4e4e4;
        }

        .see-able {
            width: 95%;
            min-width: 570px;
        }

        .see-able td:first-child {
            text-align: right;
        }

        .see-able td:last-child {
            text-align: left;
            border-right: 1px solid #efefef;
        }

        .see-able th, td {
            height: 30px !important;
        }
    </style>
</head>
<body>
<div style="text-align: center;padding:25px 0;">
    <input id="typeParamId" type="hidden" value="${prodTypeParam.id}">
    <table border="0" align="center" class="see-able am-table am-table-striped am-table-hover table" id="col3" style="position: relative;">
        <thead>
        <tr class="sortable">
            <th colspan="5">
                <div align="left"
                ">
                编辑参数属性
</div>
</th>
</tr>
</thead>
<tr>
    <td>参数属性名称：</td>
    <td>${prodTypeParam.propName }</td>
</tr>
<tr>
    <td align="right">顺序：</td>
    <td align="left"><input id="paramSeq" type="text" value="${prodTypeParam.seq }" style="width:150px;" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"></td>
</tr>
<tr>
    <td align="right">分组：</td>
    <td align="left">
        <div class="J_spu-property" style="margin: 0px;">
            <ul class="J_ul-single ul-select" style="padding-left: 0; margin-top: 3px; margin-bottom: 3px;">
                <li>
                    <div class="kui-combobox" role="combobox">
                        <div class="kui-dropdown-trigger" style>
                            <input readonly="true" style="width:145px;padding:0 0 0 5px; " class="kui-combobox-caption" value="${paramGroup.name }">
                            <div class="kui-icon-dropdown"></div>
                            <div id="kui-list" class="kui-list kui-listbox" style="display: none; <c:if test='${fn:length(typeList) > 30}'>overflow-y:auto; overflow-x:hidden;  width:168px; height: 400px</c:if>">
                                <div role="region" class="kui-header">
                                    <input type="text" class="kui-dropdown-search" autocomplete="off" onkeypress="if(event.keyCode==13) {searchParamGroup(this);return false;}"/>
                                </div>
                                <div class="kui-body" style="width: 150px;">
                                    <div class="kui-list kui-group">
                                        <div data-value="" class="kui-option" title=""></div>
                                        <c:forEach items="${paramGroupList }" var="group">
                                            <div class="kui-option" title="${group.name }" data-value="${group.id }">${group.name }</div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <select data-transtype="combox" class="keyPropClass" id="groupId" name="groupId" style="display: none; visibility: hidden;">
                        <option selected="selected" value="${prodTypeParam.groupId }"></option>
                    </select>
                </li>
            </ul>
            </span>
        </div>
    </td>
</tr>
</table>
<input type="button" class="criteria-btn" style="margin-top:30px;" value="保存" onclick="saveProdTypeParam();"/>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/paramPropEdit.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
</script>
</body>
</html>

