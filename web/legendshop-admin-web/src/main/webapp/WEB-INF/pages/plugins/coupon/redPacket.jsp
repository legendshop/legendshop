<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<style type="text/css">
.shopList {
	border: solid #dddddd;
	border-width: 1px 0px 0px 1px;
	width: 100%;
}

.shopList th{
	height:36px;
}

.shopList tr td {
	border-bottom: 1px solid #dddddd;
	border-width: 0px 1px 1px 0px;
	padding: 10px 0px;
	text-align: center;
}
.shopList tr td:last-child {
	border-right: 1px solid #dddddd;
}
label{
	font-weight: normal !important;
	margin-bottom: 2px !important;
	line-height: 25px;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<div align="center">
				<form:form action="${contextPath}/admin/coupon/saveCoupon"
					method="post" id="form1" enctype="multipart/form-data">
					<input id="couponId" name="couponId" value="${coupon.couponId}"
						type="hidden">
					<table align="center" class="${tableclass}">
						<thead>
							<tr>
								<th class="no-bg title-th">
					            	<span class="title-span">
										平台营销  ＞  
										<a href="<ls:url address="/admin/coupon/platform"/>">平台红包</a>
										<c:if test="${ not empty coupon.couponId}">  ＞  <span style="color:#0e90d2;">查看详情</span></c:if>
										<c:if test="${empty coupon.couponId}">  ＞  <span style="color:#0e90d2;">添加红包</span></c:if>
									</span>
					            </th>
							</tr>
						</thead>
					</table>
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
						<tr>
							<td style="width:200px;;">
								<div align="right">
									<font color="ff0000">*</font> 红包名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" class="${inputclass}"
								style="width: 240px;"
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								name="couponName" id="couponName" value="${coupon.couponName}" maxlength="30"/>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 状态：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								class="${selectclass}" id="status" name="status">
									<option
										<c:if test="${coupon.status eq 1}">selected="selected"</c:if>
										value="1">上线</option>
									<option
										<c:if test="${coupon.status eq 0}">selected="selected"</c:if>
										value="0">下线</option>
							</select></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 红包：
								</div>
							</td>
							<td align="left" style="line-height: 30px;padding-left: 2px;"><span id="full"> 满&nbsp;<input style="width: 225px;" 
									class="${inputclass}"
									<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
									type="text" name="fullPrice" id="fullPrice"
									value="<fmt:formatNumber type="number" value="${coupon.fullPrice==null?0:coupon.fullPrice}" pattern="0.00" maxFractionDigits="2"/>" oninput="if(value.length>8)value=value.slice(0,8)" />
									&nbsp;元&nbsp;&nbsp;&nbsp;
							</span> 减&nbsp;<input class="${inputclass}" type="text" style="width: 225px;" 
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								name="offPrice" id="offPrice"
								value="<fmt:formatNumber type="number" value="${coupon.offPrice}" pattern="0.00" maxFractionDigits="2"/>" oninput="if(value.length>8)value=value.slice(0,8)"/>&nbsp;元</td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 活动开始时间：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input class="${inputclass}"
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								readonly="readonly" name="startDate" id="startDate"
								type="text"
								value='<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm:ss" />' />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 活动结束时间：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								class="${inputclass}" readonly="readonly" name="endDate"
								id="endDate" type="text"
								value='<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss" />' />
							</td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 红包类型：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<label>
										<label class="radio-wrapper ">
											<span class="radio-item <c:if test='${ not empty coupon.couponId}'></c:if>
											<c:if test="${not empty coupon.couponType && coupon.couponType == 'common'}">radio-wrapper-checked</c:if>">
												<input type="radio" class="radio-input" name="couponType" value="common" 
												<c:if test="${ not empty coupon.couponId}">disabled='disabled'</c:if>
												<c:if test="${coupon.couponType == 'common'}">checked='checked'</c:if> onclick="selectChild(this);">
												<span class="radio-inner"></span>
											</span>
											<span class="radio-txt">全平台通用</span>
										</label>
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<label>
									<label class="radio-wrapper ">
										<span class="radio-item <c:if test='${ not empty coupon.couponId}'></c:if>
										<c:if test="${not empty coupon.couponType && coupon.couponType == 'shop'}">radio-wrapper-checked</c:if>">
											<input type="radio" class="radio-input" name="couponType" value="shop" 
											<c:if test="${ not empty coupon.couponId}">disabled='disabled'</c:if>
											<c:if test="${coupon.couponType == 'shop'}">checked='checked'</c:if> onclick="selectChild(this);">
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">选择店铺</span>
									</label>	
									</label>
								</div>
							</td>
						</tr>

						<tr id="shopListTr"
							<c:if test="${empty coupon.couponId or coupon.couponType=='common'}" >style="display:none;"</c:if>>
							<td></td>
							<td><input type='hidden' name='shopIdList' id='shopIdList' />
								<div>
									<table class='shopList see-able'>
										<thead>
											<tr>
												<th>店铺名称</th>
												<th>操作</th>
											</tr>
										</thead>
										<c:if test="${not empty shopNames}">
											<c:forEach items="${shopNames }" var="shopName">
												<tr>
													<td>${shopName}</td>
													<td></td>
												</tr>
											</c:forEach>
										</c:if>
									</table>
									<div>
										<c:if test="${empty coupon.couponId}">
											<a href="javascript:void(0);"
												onclick='loadSelectShopDialog()'>+添加店铺</a>
										</c:if>
									</div>
								</div></td>
						</tr>

						<c:if test="${not empty coupon.couponId && not empty productList}">
							<tr>
								<td></td>
								<td>商品图片&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									商品名称
							</tr>
							<c:forEach items="${productList }" var="product">
								<tr>
									<td></td>
									<td><img
										src="<ls:images item='${product.pic}' scale='3' />">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										${product.name}</td>
								</tr>
							</c:forEach>
						</c:if>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 领取方式：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<select
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" id="getType" name="getType">
									<option value="">请选择</option>
										<option value="free" <c:if test="${coupon.getType eq 'free'}">selected = "selected"</c:if>>免费领取</option>
										<option value="points" <c:if test="${coupon.getType eq 'points'}">selected = "selected"</c:if>>积分兑换</option>
										<option value="pwd" <c:if test="${coupon.getType eq 'pwd'}">selected = "selected"</c:if>>卡密兑换</option>
<%--										<option value="draw" <c:if test="${coupon.getType eq 'draw'}">selected = "selected"</c:if>>大转盘红包</option>--%>
								</select>
							</td>
						</tr>

						<c:if test="${coupon.getType eq 'free' }">
							<tr>
								<td></td>
								<td align="left" style="padding-left: 2px;"><span>领取红包的链接地址：
								<a href="${domainName}/p/coupon/apply/${coupon.couponId}" target="_blank">
								${domainName}/p/coupon/apply/${coupon.couponId}
								</a>
								</span>
								</td>
							</tr>
						</c:if>

						<tr id="type" class="even"></tr>

						<c:if test="${not empty coupon.couponId && coupon.getType eq 'points'}">
							<tr>
								<td>
									<div align="right">
										<font color="ff0000">*</font> 兑换所需积分：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input class='${inputclass}' type='text'
									<c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if>
									name='needPoints' id='needPoints' value='${coupon.needPoints}' maxlength="9"/>
								</td>
							</tr>
						</c:if>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 领取上限：
								</div>
							</td>
							<td id="limit" align="left" style="padding-left: 2px;"><input
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								class="${inputclass}" type="text" name="getLimit" id="getLimit"
								value="${coupon.getLimit}" maxlength="4"/></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font> 初始化红包数量：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input
								<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
								type="text" class="${inputclass}" name="couponNumber"
								id="couponNumber" value="${coupon.couponNumber}" maxlength="8"/></td>
						</tr>

						<tr>
							<td valign="top">
								<div align="right">
									<font color="ff0000">*</font> 描述：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea
									<c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if>
									rows="2" cols="5" style="width: 60%; height: 100px;background: #eee;"
									name="description" id="description" maxlength="250">${coupon.description}</textarea>
							</td>
						</tr>
						<c:if test="${empty coupon.couponId}">
							<tr>
								<td>
									<div align="right">红包图片：</div>
								</td>
								<td align="left" style="padding-left: 2px;">
									<input type="file" class="file" name="file" id="file" size="30" /> 
									<input type="hidden" class="file" name="couponPic" id="couponPic" size="30" value="${coupon.couponPic}" />
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty coupon.couponPic}">
							<tr>
								<td align="right" valign="top">原有图片：</td>
								<td align="left" style="padding-left: 2px;"><img src="<ls:photo item='${coupon.couponPic}'/>"
									height="200" width="200" /></td>
							</tr>
						</c:if>
					</table>
					<!-- 用户优惠券 -->
					<c:if test="${not empty coupon.couponId}">
						<%
							Integer offset = (Integer) request.getAttribute("offset");
						%>
						<div align="right" style="padding:10px;">
							<display:table name="list" id="item" export="false"
								class="${tableclass} see-able" sort="external">
								<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
								<display:column title="礼券名称">
									<span class="stockAdmin">${item.couponName}</span>
								</display:column>
								<display:column title="券号">
									<span class="stockAdmin">${item.couponSn}</span>
								</display:column>
								<display:column title="卡密">
									<span class="stockAdmin">${item.couponPwd}</span>
								</display:column>
								<display:column title="用户名称">
									<span class="stockAdmin">${item.userName}</span>
								</display:column>
								<display:column title="领取时间">
									<span class="stockAdmin"> <fmt:formatDate
											value="${item.getTime}" pattern="yyyy-MM-dd HH:mm:ss" />
									</span>
								</display:column>
								<display:column title="使用时间">
									<span class="stockAdmin"> <fmt:formatDate
											value="${item.useTime}" pattern="yyyy-MM-dd HH:mm:ss" />
									</span>
								</display:column>
								<display:column title="订单编号">
									<span class="stockAdmin">${item.orderNumber}</span>
								</display:column>
								<display:column title="状态">
									<span class="stockAdmin"> <c:if
											test="${item.useStatus eq 1}">
					      		可使用
					      	</c:if> <c:if test="${item.useStatus eq 2}">
					      		已使用
					      	</c:if>
									</span>
								</display:column>
							</display:table>
							<div class="clear"></div>
							<div style="margin-top: 10px;" class="page clearfix">
								<div class="p-wrap">
									<span class="p-num"> <ls:page pageSize="${pageSize }"
											total="${total}" curPageNO="${curPageNO }" />
									</span>
								</div>
							</div>
						</div>
					</c:if>
					<div align="center">
						<c:if test="${empty coupon.couponId}">
							<input class="${btnclass}" type="submit" value="保存" />
							
						</c:if>
						<input class="${btnclass}" type="button" value="返回"
							onclick="window.location='<ls:url address="/admin/coupon/platform"/>'" />
					</div>
				</form:form>

				<!-- 用于提交分页 -->
				<c:if test="${not empty coupon.couponId}">
					<form:form
						action="${contextPath}/admin/coupon/watchRedPacket/${coupon.couponId}"
						method="post" id="userCouponForm">
						<input type="hidden" id="curPageNO" name="curPageNO"
							value="${curPageNO}" />
					</form:form>
				</c:if>
				<div style="height: 100px;"></div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
<script type="text/javascript">
	function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("userCouponForm").submit();
     }
	function selectChild(obj){
 		$("input:radio[name='couponType']").each(function(){
  			 if(this.checked){
  				 $(this).parent().parent().addClass("radio-wrapper-checked");
  			 }else{
  				 $(this).parent().parent().removeClass("radio-wrapper-checked");
  			 }
  	 	});
    		var val = $(obj).val();
    		if(val=="common"){
    			$("#shopListTr").hide();
    		}else{
    			$("#shopListTr").show();
    		}
 	}
	
     $(document).ready(function() {
     	//红包类型 点击事件
     	/* $("input[name=couponType]").click(function(){
     		$("input:radio[name='couponType']").each(function(){
   			 if(this.checked){
   				 $(this).parent().parent().addClass("radio-wrapper-checked");
   			 }else{
   				 $(this).parent().parent().removeClass("radio-wrapper-checked");
   			 }
   	 	});
     		var val = $(this).val();
     		if(val=="common"){
     			$("#shopListTr").hide();
     		}else{
     			$("#shopListTr").show();
     		}
     	}); */
     
     	
       /*  $("#couponType").change(function(){
	        $("#cate").empty();
	        $("#cate1").empty();
	       	var val = $("#couponType option:selected").val();
	       	if(val=="category"){	        		
	       		$("#cate").append("<td><div align='center' style='padding-top:7px'>选择类目: <font color='ff0000'>*</font></div></td>"+
	       		"<td><a style='float:left; padding-top:7px;' href='javascript:void(0)' onclick='loadCategoryDialog()'; >选择</a>&nbsp;&nbsp;&nbsp;"+	        	
	       		"<div style='float:left;height:37px;'><fieldset disabled><input type='text' id='catego' name='catego'/></fieldset></div></td>"+
	       		"<input type='hidden' id='categoryId' name='categoryId'/>");
	       	}   
	       		        	
	       	if(val=="product"){	        		
	       		$("#cate").append("<td><div align='center'>选择商品: <font color='ff0000'>*</font></div></td>"+
	       		"<td><a href='javascript:void(0)' onclick='loadSelectProdDialog()'; >选择</a></td>");	
	       		
	       		$("#cate1").append("<td></td>"+
	       		"<td><input type='hidden' name='prodidList' id='prodidList'/><div><table class='prodList'><tr>"+
	       		"<td>商品名称</td><td>商品价格</td><td>操作</td></tr></table></div></td>"); 		        	
	       	}        		        		       	        	
	    }); */
	    
       	$("#getType").change(function(){
       		$("#type").empty();
       		var val = $(this).val();
	       	if(val=="points"){
	       		$("#type").append("<td>"+
					"<div align='right'><font color='ff0000'>*</font>兑换所需积分：</div></td>"+
				"<td align='left' style='padding-left: 2px;'><input class='${inputclass}' type='text' <c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if> name='needPoints' id='needPoints' value='${coupon.needPoints}' /></td>");
	       	}	         	
       	});
	    
	    $("input[type=file]").change(function(){
	    	checkImgType(this);
	    	checkImgSize(this,1024);
	    });
       	 
       	 var messageType='${coupon.messageType}';
       	 if(messageType!=""&&messageType!=undefined&&messageType!=null){
       		 var arr=messageType.split(',');
       		 if(arr.length>0){
       			 for(var i=0;i<arr.length;i++)
           		 {
       				 var a=arr[i];
           		     $("input[name='messageType']").each(function(){
               			 var v=$(this).attr("value");
               			 if(a==v){
               				  $(this).attr("checked","checked");
               			 }
               		 });
           		 } 
       		 }
       		
       		
       	 }
       	
      	     jQuery.validator.addMethod("isnumber", function(value, element) {	  
  	            return this.optional(element) || /^\d+(\.\d{1,2})?$/.test(value);	  
  	         }, "只能包含数字、小数点"); 
      	    
      	     jQuery.validator.addMethod("isDigits", function(value, element) {	  
   	         return this.optional(element) || /^[1-9]\d*$/.test(value);	  
   	      }, "只能输入0-9数字");  
   	      
   	        jQuery.validator.addMethod("isLtFullPrice", function(value, element) {	  
  	            return this.optional(element) || Number(value)<Number($("#fullPrice").val());	  
  	         },"减免金额必须小于消费金额"); 
  	         
//   	          jQuery.validator.addMethod("isGtOffPrice", function(value, element) {	  
//   	            return this.optional(element) || Number(value)>Number($("#offPrice").val());	  
//   	         },"消费金额必须大于减免金额"); 
      	     
      	  
	      jQuery("#form1").validate({
	    	rules : {
	    		//couponType : "required",
	    		couponName : "required",
	    		fullPrice : {
					required:true,
					isnumber:true,
// 					isGtOffPrice:true
					max:99999.99
				},
				offPrice : {
					required:true,
					isnumber:true,
					isLtFullPrice:true,
					min:1,
					max:99999.99
				},
				status : "required",
	    		startDate : "required",
	    		endDate : "required",
	    		catego : "required",
	    		getType:{required:true},
	    		getLimit: {
	    			required : true,
	    			isDigits:true,
	    			max:999
	    		},
	    		needPoints: {
	    			required : true,
	    			isDigits:true,
	    			max:99999999
	    			
	    		},
	    		couponNumber:{
					required:true,
					isDigits:true,
					min:1,
					maxlength:6
					
				},
				description:"required",	    		
		
			},
			messages : {		 		
				//couponType : "请选择红包类型",
				couponName : "请输入红包名称",
				fullPrice : {
						required:"请输入红包值",
						isnumber:"请输入正确格式!",
						isGtOffPrice:"消费金额必须大于减免金额",
						max:"红包最大为99999.99"
				},
			    offPrice: {
				 required:"请输入红包值",
				 isnumber:"请输入正确格式!",
				isLtFullPrice:"减免金额必须小于消费金额",
				min:"减免金额最小为1元",
				max:"红包最大为99999.99"
		        },
		        status : "请选择状态",
				startDate : "请选择日期",
				endDate : "请选择日期",
		        catego : "请选择类目",
		        getType:{required:"请选择领取方式"},
		        getLimit:{
					 required:"请输入领取上限值",
					 isDigits:"请输入正确格式!",
					 max:"领取上限为999个"
		        },
		        needPoints: {
					 required:"请输入兑换所需积分!",
					 isDigits:"请输入正确格式!",
					 max:"积分最大为99999999"
		        },
				couponNumber:{
					required:"请输入初始化红包数量",
					isDigits:"请输入正确格式!",
					min:"最少为1个",
					maxlength:"最大为6位数字"
					
				},									
				description:"请输入描述",				    				    
			},
			 submitHandler : function(form) {
			 //把table的prodId循环拼接成字符串

				 if (parseInt($("#getLimit").val())>parseInt($("#couponNumber").val())){
					 layer.alert("领取上限不能大于红包初始数量", {icon:0});
					 return;
				 }

			 	var couponType=$("input[name=couponType]:checked").val();
			 	if(couponType=="shop"){
			 		var tableLength =  $("table[class='shopList see-able'] tr").length;
			 		if(tableLength<=1){
			 			layer.alert("请添加店铺数据", {icon:0});
			 			return;
			 		}
			 		
			 		var shopIdList="";
				 	$("table[class='shopList see-able'] tr").each(function (index,domEle){
						var shop_id=$(this).find(".shopId").val();
						if(!isEmpty(shop_id)){
							shopIdList += shop_id+",";
						}
					});
				
			 		$("#shopIdList").val(shopIdList);
			 	}else{
			 		$("#shopIdList").val("");
			 	}

				 if($("#startDate").val() >= $("#endDate").val()){
					 layer.alert("活动的结束时间不能小于开始时间！", {icon:0});
					 return;
				 }
			 	
			 	if($("#getType").val() == "pwd"){
				 		var couponNum = $("#couponNumber").val();
				 		if(couponNum <1 || couponNum >1000){
				 			layer.alert("如果红包领取方式为卡密兑换，则发放总数应为1~1000之间的整数！", {icon:0});
				 			return;
				 		}
				 }
			 	layer.confirm('确认无误，创建红包？',{
			  		 icon: 3
			  	     ,btn: ['确定','取消'] //按钮
			  	   }, function () {
			  		 form.submit();
			  		 layer.load(2);
				});

			 }
	    });  
	    
	     //斑马条纹
    	    $("#col1 tr:nth-child(even)").addClass("even");
	     
	     /* $("#couponType").change(function(){
	    	  var str=$(this).val();
	    	  if(str!=2){
	    		  $("#full").show();
	    	  }else{
	    		  $("#full").hide();
	    	  }
	     }); */
	     
	     var frequency='${coupon.frequency}';
	     if(frequency!=undefined&&frequency!=null&&frequency!=""){
	    	  $("#limit").append("<span id='fre'>&nbsp;&nbsp;&nbsp;领取频率:隔&nbsp;<input type='text' style='width:50px;' name='frequency' readonly='readonly' id='frequency' value='"+frequency+"' />&nbsp;小时领取一次</span>");
	     }
      });
        
        
        
	        /**
	 * 判断是否是空
	 * @param value
	 */
	function isEmpty(value){
		if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
			return true;
		}
		else{
			value = (value+"").replace(/\s/g,'');
			if(value == ""){
				return true;
			}
			return false;
		}
	}
        
      	//加载分类用于选择
		function loadCategoryDialog() {
			layer.open({
				title : "选择分类",
				id : "prodCat",
				type: 2,
				content: "${contextPath}/admin/product/loadAllCategory2/P",
				area: ['280px', '350px']
			})
		}  
		
		function addShopTo(shopId,siteName){
			if(shopId==null||shopId==""||shopId==undefined){
			    layer.alert("店铺Id为空", {icon:2});
			    return;
			 }
			 
			var config=true;
			$("table[class='shopList see-able'] tr").each(function (index,domEle){
				var shop_id=$(this).find(".shopId").val();		
				if(shop_id==shopId){
					layer.msg("该店铺已添加", {icon:0});
					config=false;
				}
			});
			 if(!config){
			 	return;
			 }
			 
			var tableLength =  $("table[class='prodList']").length;
			//拼接数据
			var str ='<tr>'+
				'<td><input type="hidden" class="shopId" value="'+shopId+'"/>'+siteName+'</td>'+
				'<td><a href="javascript:void(0)" onclick="deleteShop(this);" class="bot-gra">'+
				'删除</a></td>'+
			 '</tr>';
			//追加到Table
			 $("table[class='shopList see-able']").append(str);
			 layer.msg("添加成功", {icon:1});
		}
		
		//加载店铺用于选择(红包)
		function loadSelectShopDialog(){
			layer.open({
				title: '选择店铺',
				id: 'selectShop',
				type: 2,
				content: "${contextPath}/admin/coupon/loadSelectShop",
				area: ['700px', '450px']
			});
			
		}
		
		function deleteShop(obj){
			$(obj).parent().parent().remove();
		}
		
		function loadCategoryCallBack(catId,catName){
			$("#categoryId").val(catId);
			$("#catego").val(catName);								
		}
		
		function clearCategoryCallBack(){
			$("#categoryId").val(null);
			$("#catego").val(null);	
		}
        
      function change(val){
    	  var limit=$.trim($(val).val());
    	  var re = /^[0-9\s]*$/;
    	  if(limit==null||limit==undefined||limit==""){
    		  return;
    	  }else if(!re.test(limit)){
    		  $("#fre").remove();
    		  return;
    	  }
    	  else if(limit>1){
    		  $("#fre").remove();
  			  $("#limit").append("<span id='fre'>&nbsp;&nbsp;&nbsp;领取频率:隔&nbsp;<input type='text' style='width:50px;' name='frequency' id='frequency' value='${coupon.frequency}' />&nbsp;小时领取一次</span>");
    	  }else{
    		  $("#fre").remove();
    	  }
      }
      
      laydate.render({
	   		 elem: '#startDate',
	   		 type:'datetime',
	   		 calendar: true,
	   		 min:'-1',
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endDate',
	   	     type:'datetime',
	   	     min:'-1',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
</script>
</html>