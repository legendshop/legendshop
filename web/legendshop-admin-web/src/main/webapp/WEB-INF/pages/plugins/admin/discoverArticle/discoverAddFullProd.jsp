<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="ant-row" style="margin-right: 4px;">
	<div class="ant-col-12"></div>
	<div class="ant-col-12">
		<form class="ant-form">
			<div class="sold-ser clearfix sold-ser-no-bg">
				<div class="fr">
					<div class="item">
						<span class="ant-input-preSuffix-wrapper"> <input
							type="text" id="prodName" value="${prodName}" class="item-inp"
							style="width: 300px;" placeholder="请输入商品名称"> <input
							type="button" id="seach" onclick="search()" value="搜索"
							class="ant-btn btn-r">
						</span>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class=" clearfix">
	<div class="ant-spin-nested-loading">
		<div class="ant-spin-container">
			<div
				class="ant-table ant-table-large ant-table-without-column-header ant-table-scroll-position-left">
				<div class="ant-table-content">
					<div class="ant-table-body">
						<table class="prodTable">
							<colgroup>
								<col>
								<col style="width: 200px; min-width: 200px;">
								<col style="width: 200px; min-width: 200px;">
								<col style="width: 150px; min-width: 150px;">
								<col style="width: 100px; min-width: 100px;">
							</colgroup>
							<thead class="ant-table-thead">
								<tr>
									<th class="ant-table-selection-column allSel"><span>
											<label class="ant-checkbox-wrapper "> <span
												class="ant-checkbox" optional="true"> <span
													class="ant-checkbox-inner"></span> <input type="checkbox"
													class="ant-checkbox-input" value="on">
											</span>
										</label>
									</span></th>
									<th class=""><span>商品</span></th>
									<th class=""><span>价格(元)</span></th>
									<th class=""><span>库存</span></th>
									<th class=""><span>操作</span></th>
								</tr>
							</thead>
							<tbody class="ant-table-tbody">
								<c:if test="${empty requestScope.list}">
									<tr>
										<td colspan="5"><div
												style="text-align: center; font-size: 14px;">您还没有可用的商品</div></td>
									</tr>
								</c:if>
								<c:if test="${not empty requestScope.list}">
									<c:forEach var="item" items="${requestScope.list}">
										<tr class="ant-table-row  ant-table-row-level-0"
											data-id="${item.prodId }">
											<td class="ant-table-selection-column"><span> <label
													class="ant-checkbox-wrapper"> <span
														class="ant-checkbox" data-id="${item.prodId }"
														<c:if test="${empty sa.activeId}">optional="true"</c:if>>
															<span class="ant-checkbox-inner"></span> <input
															type="checkbox" class="ant-checkbox-input" value="on"
															<c:if test="${not empty sa.activeId}">disabled="disabled"</c:if>>
													</span>
												</label>
											</span></td>
											<td class=""><span
												class="ant-table-row-indent indent-level-0"
												style="padding-left: 0px;"></span> <span class="good-info">
													<img width="50" height="50"
													src="<ls:photo item='${item.pic}'/>"
													class="ant-table-row-img" alt="${item.name }"> <span
													class="name"><span>${item.name }</span></span>
											</span></td>
											<td class=""><span class="t-grayer">${item.price }</span></td>
											<td class=""><span class="t-grayer">${item.stocks }</span></td>
											<td class="caozuo"><c:if test="${empty sa.activeId }">
													<button type="button" class="ant-btn ant-btn-primary btn-r"
														data-id="${item.prodId }" name="join">
														<span>添 加</span>
													</button>
												</c:if> <c:if test="${not empty sa.activeId }">
													<span>已经选择</span>
												</c:if></td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
<div style="margin-top: 10px;" class="page clearfix">
	<button type="button"style="position:fixed;bottom:65px;left:10px"
		class="ant-btn ant-btn-default batch-add-prod batchJoin btn-r">
		<span>批量添加</span>
	</button>
	<button type="button" id="save"
		style="position:fixed;bottom:15px;display:inline-block;left: 420px; color: #ffffff; border: 1px solid #0e90d2; background-color: #0e90d2; border-color: #0e90d2; cursor: pointer; height: 28px; padding-left: 12px; padding-right: 12px; text-align: center; line-height: 24px;font-size: 12px;"
		class="save">
		<span>确定</span>
	</button>
	<button type="button" id="close"
		style="position:fixed;bottom:15px;display:inline-block;left: 510px; color: black; border: 1px solid #0e90d2; background-color: #f7f7f7; border-color: #f7f7f7; cursor: pointer; height: 28px; padding-left: 12px; padding-right: 12px; text-align: center; line-height: 24px;font-size: 12px;"
		class="close">
		<span>取消</span>
	</button>
	<div class="p-wrap" style="position:fixed;bottom:60px;display:inline-block;right:0px">
		<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
	</div>
</div>
</div>


