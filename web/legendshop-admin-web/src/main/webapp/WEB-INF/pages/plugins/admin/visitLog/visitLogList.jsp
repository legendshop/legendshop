<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>报表管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<style type="text/css">
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
						<tr>
							<th class="title-border">报表管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">IP 浏览管理</span></th>
						</tr>
				</table>
			<form:form action="${contextPath}/admin/visitLog/query" id="form1"
				method="post">
				<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
				<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							店铺： 
							<input type="text" id="shopId" name="shopId" value="${bean.shopId}"/>
							<input type="hidden" id="shopName" name="shopName" maxlength="50" value="${bean.shopName}" class="${inputclass}" /> 
						</span>
						<span class="item">
							开始时间：
							<input readonly="readonly" name="startTime" id="startTime" class="Wdate" type="text" 
								value='<fmt:formatDate value="${bean.startTime}" pattern="yyyy-MM-dd HH:MM:ss"/>' placeholder="开始时间"/>
						</span>
						<span class="item">
								结束时间：
							 <input readonly="readonly" name="endTime" id="endTime" class="Wdate" type="text" 
								value='<fmt:formatDate value="${bean.endTime}" pattern="yyyy-MM-dd HH:MM:ss"/>' placeholder="结束时间"/>
						</span>
						<span class="item">
							用户ID：
							<input type="text" id="userName" name="userName" maxlength="50" value="${bean.userName}" class="${inputclass}" placeholder="用户ID"/> 
						</span>
						<span class="item">
							页面：
							 <select id="page" id="page" name="page" class='criteria-select'>
								<ls:optionGroup type="select" required="false" cache="true"
									beanName="VISIT_LOG_TYPE" selectedValue="${bean.page}" />
							</select> 
						</span>
						<span class="item">
							商品：
					    	<input type="text" id="productName" name="productName" maxlength="50" value="${bean.productName}" class="${inputclass}" placeholder="商品"/> 
							<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
						</span>
					</div>
			</form:form>
			<div align="center" id="visitLogContentList" class="order-content-list"></div>
		</div>
	</div>
</body>
<%--     <table style="width: 100%; border: 0px"><tr><td align="left">说明：<br>--%>
<%--   1. 页面访问历史，记录用户于何时何地访问您的商城<br>--%>
<%--   2. 如果是系统用户访问，可以在论坛中找到该用户的联系方式<br>--%>
<%--   </td><tr></table>    --%>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	
	function deleteById(id) {
		layer.confirm("确定删除 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "${contextPath}/admin/visitLog/delete/" + id;
		   });
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		sendData(curPageNO);
	}

	highlightTableRows("item");

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	$(document).ready(function () {
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});
	function sendData(curPageNO) {
	    var data = {
    		"shopId": $("#shopId").val(),
	         "curPageNO": curPageNO,
	         "startTime": $("#startTime").val(),
	         "endTime": $("#endTime").val(),
	         "userName": $("#userName").val(),
	         "endTime": $("#endTime").val(),
	         "page": $("#page").val(),
	         "productName": $("#productName").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/visitLog/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#visitLogContentList").html(result);
	        }
	    });
	}
	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	  laydate.render({
			 elem: '#startTime',
			 type: 'datetime',
			 calendar: true,
			 theme: 'grid',
		 	 trigger: 'click'
		  });
		   
		  laydate.render({
		     elem: '#endTime',
		     type: 'datetime',
		     calendar: true,
		     theme: 'grid',
		 	 trigger: 'click'
	    });
</script>
</html>

