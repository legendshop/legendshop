<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/mergeGroup/fight.css'/>" rel="stylesheet" />
</head>
<body>
   
   <jsp:include page="/admin/top" />
   
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div style="border-bottom:1px solid #ddd;width: 100%;height:40px;line-height:40px;">
		      		<span class="title-span" style="margin-left:10px;">
						预售管理  ＞
						<a href="<ls:url address="/admin/mergeGroup/query"/>">预售活动管理</a>
						  ＞   <span style="color:#0e90d2;">预售运营</span>
					</span>
		      	</div>
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
					<div class="fight-operate"
						style="margin-left: 50px; float: left; width: 93%;">
						<!-- 运营统计数据 -->

						<table cellpadding="0" cellspacing="0">
							<tr class="tab-tit">
								<th width="60">编号</th>
								<th width="200">订单号</th>
								<th>买家</th>
								<th>数量</th>
								<th width="100">交易金额(元)</th>
								<th>定金</th>
								<th>尾款</th>
							</tr>
							<c:if test="${empty list}">
								<tr class="first-leve">
									<td colspan='7' height="60" style="text-align: center; color: #999;">暂无数据</td>
								</tr>
							</c:if>

							<c:forEach items="${list}" var="item" varStatus="itemStatus">
								<tr>
									<td>${itemStatus.index+1}</td>
									<td>${item.subNumber}</td>
									<td>${item.userName}</td>
									<td>${item.productNums}</td>
									<td>${item.actualTotal}</td>
									<td>${item.isPayDeposit eq 0?'未支付':'已支付'}</td>
									<c:choose>
										<c:when test="${presellProd.payPctType eq 0}">
											<td>无需支付尾款</td>
										</c:when>
										<c:otherwise>
											<td>${item.isPayFinal eq 0?'未支付':'已支付'}</td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</table>
						<div align="center"><br/>
							<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default" />
						</div>
						<div class="activity-mes" style="margin-top: 35px;">
							<span class="act-tit"></span> <a
								href="${contextPath }/admin/presellProd/query" class="lim-btn on"
								style="background: #fff; color: #666; border: 1px solid #e4e4e4;">返回</a>
						</div>
					</div>			    
		     </div>
	 </div>
</body>
<script language="JavaScript" type="text/javascript">
	var curPageNO = "${curPageNO}";
	var contextPath = "${contextPath}";
	var presellId = "${presellId}"
	function pager(curPageNO) {
		location.href = contextPath + "/admin/presellProd/operatorPresellActivity/"+presellId+"?curPageNO="
				+ curPageNO;
	}
</script>
</html>
