<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>专题详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/common/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/common/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">   
	<div id="doc">
       <div class="seller-cru" style="width:1190px;margin: 20px auto 0px;">
      </div> 
        <div class="shop-topic-det01" style="background-color: ${theme.backgroundColor};background-image:url('<ls:photo item='${theme.backgroundPcImg}' />');">
            <div class="topic-det-ban" style="background-color: ${theme.bannerImgColor};">
            	<c:choose>
            		<c:when test="${not empty theme.bannerPcImg}">
             			 <img src="<ls:photo item='${theme.bannerPcImg}' />" style="display:block;width: 100%">
            		</c:when>
            	<c:otherwise>
            		<div class="banner_details">
							<c:if test="${theme.isTitleShow==1}">
								<h3 style='color: ${theme.titleColor}'  align="center">${theme.title}</h3>
							</c:if>
							<c:if test="${theme.isIntroShow==1}">
								<p style="color:${theme.introColor}">${theme.intro}</p>
							</c:if>
				        </div>
            	</c:otherwise>
            	</c:choose>
            </div>
            <div class="topic-det-bg" style="background-color: ${theme.backgroundColor};">
            <div class="topic-det-con">
              <c:forEach items="${requestScope.themeModuleMap}" var="themeModule">
              <div class="topic-det-con01" style="background-color: ${theme.backgroundColor};">
                	<div>
                	<c:choose>
                		<c:when test="${themeModule.key.isTitleShow==1}">
                			<p class="topic-det-tit" style="color: ${themeModule.key.titleColor};background-color:${themeModule.key.titleBgColor};<c:if test="${not empty themeModule.key.titleBgPcimg}">background:url('<ls:photo item='${themeModule.key.titleBgPcimg}' />') no-repeat center</c:if>">${themeModule.key.title}</p>
                		</c:when>
                		<c:otherwise>
                		<p class="topic-det-tit" style="background-color:${themeModule.key.titleBgColor};<c:if test="${not empty themeModule.key.titleBgPcimg}">background:url('<ls:photo item='${themeModule.key.titleBgPcimg}' />') no-repeat center</c:if>"></p>
                		</c:otherwise>
                	</c:choose>
                	</div>
		                <ul style="overflow: hidden;">
		                	<c:forEach items="${themeModule.value}" var="modulePrd">
				                  <li>
				                    <a href="${PC_DOMAIN_NAME}/views/${modulePrd.prodId}"  target="_blank">
				                      <dl>
				                        <dt style="padding: 10px 10px 0 10px;">
				                          <img width="100%" src="<ls:images item='${modulePrd.img }' scale='4' />" alt="${modulePrd.prodName}">
				                        </dt>
				                        <dd style="padding: 5px 9px;height: 35px;">${modulePrd.prodName}</dd>
				                      </dl>
				                      <div><span>¥${modulePrd.cash}</span><em>立即购买</em></div>
				                    </a>
				                  </li>
		                 </c:forEach>
		                  </ul>
              </div>
              </c:forEach>
              <c:if test="${not empty theme.customContent }">
				<div class="ThemeModuleProd_custom">
					<div class="custom_con"><c:out value="${theme.customContent}" escapeXml="${theme.customContent}"></c:out> </div>
				</div>
			</c:if>
			<div width: 1190px;>
				<c:forEach items="${themeRelated}" var="themerelated">
					<a target="_blank" onclick="toDetail('${themerelated.relatedThemeId}')"><img style="display: block;width: 100%;" src='<ls:photo item='${themerelated.img}'/>'/></a>
				</c:forEach>
			</div>
				<div class="topic-det-end" style="margin-bottom: 55px;"><a href="${contextPath}/admin/theme/allThemes">查看更多专题 >></a></div>
            </div>
          </div>
        </div>
</div>
<script type="text/javascript">
	function toDetail(themeId){
		window.open("${contextPath}/theme/themeDetail/"+themeId,"_blank");
	}
</script>
</body></html>