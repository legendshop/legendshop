<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>参数组管理- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/paramGroup/save" method="post" id="form1">
            <input id="id" name="id" value="${paramGroup.id}" type="hidden">
            <div align="center">
            <table class="${tableclass} no-border" style="width:100%;">
                <thead>
                    <tr>
                        <th class="no-bg title-th">
                             <span class="title-span">属性管理  ＞  <a href="<ls:url address="/admin/paramGroup/query"/>" style="color: #0e90d2;">参数组管理</a>  ＞  <span style="color:#0e90d2;">编辑参数组</span></span>
                        </th>
                    </tr>
                </thead>
         	<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">       
				<tr>
				        <td width="200px;">
				          	<div align="right"><font color="ff0000">*</font>名称：</div>
				       	</td>
				        <td align="left" style="padding-left: 2px;">
				           	<input type="text" maxlength="20" style="width: 300px;" name="name" id="name" value="${paramGroup.name}" />
				        </td>
				</tr>
				<tr>
				        <td>
				          	<div align="right"><font color="ff0000">*</font>排序：</div>
				       	</td>
				        <td align="left" style="padding-left: 2px;">
				           	<input type="text" style="width: 300px;" name="seq" id="seq" value="${paramGroup.seq}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
				        </td>
				</tr>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            
                            <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/paramGroup/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({
	});

    $(document).ready(function() {
    	jQuery("#form1").validate({
            rules: {
            	name: {required: true},
           	 	seq: "required"
        	},
        	messages: {
           		name: {required: "请填写名称"},
            	seq: {required: "请填写排序"}
        	}
   	 	});
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>
