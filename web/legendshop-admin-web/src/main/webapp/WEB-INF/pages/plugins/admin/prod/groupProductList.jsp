<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>商品分组 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
       <jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
		  	 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<table class="${tableclass} title-border" style="width: 100%">
			    <tr>
			        <th class="title-border">商品管理&nbsp;＞&nbsp;商品分组 ＞ <span style="color:#0e90d2; font-size:14px;">分组商品列表</span></th>
			    </tr>
			</table>
			<div>
			    <div class="seller_list_title">
			        <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
			        </ul>
			    </div>
			</div>
			<form:form action="${contextPath}/admin/prodGroup/queryProdList" id="form1" method="get">
               <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
               <input type="hidden" id="groupId" name="groupId" value="${prod.groupId}"/>
               <div class="criteria-div">

				<span class="item">
                   	 店铺：
                    <input type="text" id="shopId" name="shopId" value="${prod.shopId}"/>
                    <input type="hidden" id="siteName" name="siteName" value="${prod.siteName}" class="${inputclass}"/>
				 </span>
			     <span class="item">
					 商品名称：
					<input type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="搜索商品名称" class="${inputclass}"/>
		  		 </span>
				 <span class="item">
                    <input type="button" onclick="search()" class="${btnclass}" value="搜索"/>
                 </span>
               </div>
			</form:form>
		<div align="center" id="groupProductContentList" name="groupProductContentList" class="order-content-list"></div>
   </div>
   </div>
   </body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="javascript">
	var contextPath = "${contextPath}";
	var siteName = "${prod.siteName}";

	$(document).ready(function () {

		if(siteName == ''){
			makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
		}else{
			makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",siteName, 'siteName');
			$("#select2-chosen-2").html(siteName);
		}

		sendData(1);

	});

	function sendData(curPageNO) {
		var data = {
			"curPageNO": curPageNO,
			"shopId": $("#shopId").val(),
			"groupId": $("#groupId").val(),
			"name": $("#name").val(),
			"siteName": $("#siteName").val(),
		};
		$.ajax({
			url: contextPath+"/admin/prodGroup/queryProdContentList",
			data: data,
			type: 'post',
			async: true, //默认为true 异步
			success: function (result) {
				$("#groupProductContentList").html(result);
			}
		});
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}

	function pager(curPageNO) {
		$("#curPageNO").val(curPageNO);
		sendData(curPageNO);
	}

	/**
	 * 初始化select2
	 * @param id
	 * @param clearFlag
	 */
	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
			placeholder: holder,
			allowClear: true,
			width:'200px',
			ajax: {
				url : url,
				data: function (term,page) {
					return {
						q: term,
						pageSize: 10,    //一次性加载的数据条数
						currPage: page, //要查询的页码
					};
				},
				type : "GET",
				dataType : "JSON",
				results: function (data, page, query) {
					if(page * 5 < data.total){//判断是否还有数据加载
						data.more = true;
					}
					return data;
				},
				cache: true,
				quietMillis: 200//延时
			},
			formatInputTooShort: "请输入" + text,
			formatNoMatches: "没有匹配的" + text,
			formatSearching: "查询中...",
			formatResult: function(row,container) {//选中后select2显示的 内容
				return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
			},formatSelection: function(row) { //选择的时候，需要保存选中的id
				$("#" + textValue).val(row.text);
				return row.text;//选择时需要显示的列表内容
			},
		});
	}

	// 移除商品
	function deleteById(prodId,groupId) {
		layer.confirm("确定移除该商品 ?", {icon:3, title:'提示'}, function (index) {
			$.post(contextPath+"/admin/prodGroup/removeProd?prodId=" + prodId +"&groupId="+groupId, function (data) {
				if (data == 'OK') {
					layer.alert("删除成功", {icon:1}, function(){
						window.location.reload();
					});
				} else{
					layer.alert(data, {icon:2});
				}
			},"json");
			layer.close(index);
		});
	}

</script>
</html>
