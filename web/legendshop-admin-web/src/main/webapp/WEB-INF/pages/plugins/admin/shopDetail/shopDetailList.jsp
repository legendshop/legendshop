<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>店铺管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">商家管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty shopDetail.status}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopDetail/query"/>">所有店铺</a></li>
						<li
							<c:if test="${shopDetail.status eq -1}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopDetail/query?status=-1"/>">审核中</a></li>
						<li
							<c:if test="${shopDetail.status eq 1}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopDetail/query?status=1"/>">上线中</a></li>
						<li
							<c:if test="${shopDetail.status eq 0}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopDetail/query?status=0"/>">下线中</a></li>
						<li
							<c:if test="${shopDetail.status eq -3}"> class="am-active"</c:if>><i></i><a href="<ls:url address="/admin/shopDetail/query?status=-3"/>">平台关闭</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/shopDetail/query" id="form1" method="get">
				<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
				<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
						<div class="criteria-div">
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
							<span class="item">
								<input type="text" name="siteName" value="${shopDetail.siteName}" placeholder="店铺名字" class="${inputclass}" /> 
							</span>
							<span class="item">
								<input type="text" name="nickName" maxlength="50" value="${shopDetail.nickName}" class="${inputclass}" placeholder="请输入搜索用户昵称" /><br /> 
							</span>
							<span class="item">
								<input type="text" name="contactName" maxlength="50" value="${shopDetail.contactName}" class="${inputclass}" placeholder="联系人姓名" /> 
							</span>
							<span class="item">
								<input type="text" name="contactMobile" value="${shopDetail.contactMobile}" class="${inputclass}" placeholder="联系人手机号码" /> &nbsp;店铺类型&nbsp; 
							</span>
							<span class="item">
								<select class="criteria-select" id="shopType" name="shopType">
									<option value="">--请选择--</option>
									<option value="0" <c:if test="${shopDetail.shopType eq 0}">selected="selected"</c:if>>专营店</option>
									<option value="1" <c:if test="${shopDetail.shopType eq 1}">selected="selected"</c:if>>旗舰店</option>
									<option value="2" <c:if test="${shopDetail.shopType eq 2}">selected="selected"</c:if>>自营店</option>
								</select> 
							</span>
							<span class="item">
								类型：
								<select id="type" name="type" class="criteria-select">
									<ls:optionGroup type="select" required="false" cache="true" beanName="SHOP_TYPE" selectedValue="${shopDetail.type}" />
								</select> 
								<input type="hidden" id="status" name="status" value="${shopDetail.status}" /> 
								<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
							</span>
						</div>

			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/shopDetail/query" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
					<%--       <display:column title='<input type="checkbox" id="checkedAll">'  style="min-width:50px;"> --%>
					<%--       		<input type="checkbox" name="sid" value="${item.id}"> --%>
					<%--       </display:column> --%>
					<display:column title="顺序" style="min-width:50px;"><%=offset++%></display:column>
					<display:column title="店铺名字">
						<a href="<ls:url address="/admin/shopDetail/load/${item.shopId}"/>" target="_blank">${item.siteName}</a>
					</display:column>
					<display:column title="用户名" property="nickName"></display:column>
					<display:column title="店铺图片" style="min-width: 100px;">
						<c:choose>
							<c:when test="${not empty item.shopPic}">
								<img src="<ls:photo item='${item.shopPic}' />" height="30px" />
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="昵称" style="min-width: 50px; max-width: 200px;  word-wrap:break-word"> ${item.userName} </display:column>
					<display:column title="联系人" property="contactName"></display:column>
					<display:column title="联系人手机" property="contactMobile"></display:column>
					<display:column title="商品数量" property="productNum" sortable="true" sortName="product_num" style="width:80px"></display:column>
					<display:column title="下线商品" property="offProductNum" sortable="true" sortName="off_product_num" style="width:80px"></display:column>
					<display:column title="分佣比例" property="commissionRate" style="width:80px"></display:column>
					<display:column title="状态" style="min-width:60px">
						<font> <ls:optionGroup type="label" required="true" cache="true" beanName="SHOP_STATUS" selectedValue="${item.status}" /></font>
					</display:column>
					<display:column title="操作" media="html" style="width: 50px;">
						<div class="table-btn-group" data-am-dropdown>
							<button class="tab-btn am-dropdown-toggle" data-am-dropdown-toggle>
								<span class="am-icon-cog"></span> 
								<span class="am-icon-caret-down"></span>
							</button>
							<ul class="am-dropdown-content">
								<li><a href="${contextPath}/admin/shopDetail/load/${item.shopId}">1.查看店铺详情</a></li>
<%--								<li><a href="javascript:deleteById('${item.id}');">2. 删除</a></li>--%>
								<c:if test="${item.status eq 1}">
									<%-- <li><a href="${contextPath}/admin/product/query?shopId=${item.shopId}&siteName=${item.siteName}""  target="_blank">3. 查看店铺商品</a></li> --%>
									<%-- <li><a href="javascript:void(0)" onclick="showProduct(this);" siteName="${item.siteName}" shopId="${item.shopId}">3. 查看店铺商品</a></li>
									<li><a href="${contextPath}/admin/order/processing?shopId=${item.shopId}" target="_blank">4. 查看店铺订单</a></li> --%>
									<li>
										<a href="javascript:void(0)" onclick="editShopCommission(this,${item.commissionRate});" shopId="${item.shopId}" commision="${item.commissionRate}">2. 设置分佣比例</a>
									</li>
									<c:if test="${not empty item.commissionRate}">
									<li>
										<a href="javascript:deleteShopCommission('${item.id}');">3. 清除店铺分佣比例</a>
									</li>
									</c:if>
									<c:choose>
										<c:when test="${not empty item.commissionRate}">
											<ls:auth ifAnyGranted="F_VIEW_ALL_DATA">
												<li><a href="javaScript:changeType('${item.shopId}',${item.shopType})">4.更改店铺类型</a></li>
											</ls:auth>
										</c:when>
										<c:otherwise>
											<ls:auth ifAnyGranted="F_VIEW_ALL_DATA">
												<li><a href="javaScript:changeType('${item.shopId}',${item.shopType})">3.更改店铺类型</a></li>
											</ls:auth>
										</c:otherwise>
									</c:choose>
								</c:if>
							</ul>
						</div>
					</display:column>
				</display:table>
				<!--     <div style="float:left;margin-left:10px;" > -->
				<!-- 	    <input type="button" value="批量删除" id="batch-del"> -->
				<!-- 	</div><br/> -->
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1. 商城为上线状态才可以正常访问<br> 2.
							您可以选择不同的页面风格，或者每天随即挑取其中一个风格<br>
						</td>
					<tr>
				</table>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopDetailList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</html>

