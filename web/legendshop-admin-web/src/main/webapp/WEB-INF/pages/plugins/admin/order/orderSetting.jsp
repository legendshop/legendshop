<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>订单设置 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table border="0" align="center" class="${tableclass} title-border" id="col1"  style="width: 100%">
            <tr>
                <th class="title-border">
                	业务设置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">订单设置</span>
                </th>
            </tr>
        </table>
        <form:form  action="${contextPath}/admin/order/settingManage" method="post" id="form1">
            <input id="gradeId" name="gradeId" value="${userGrade.gradeId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass} no-border content-table" id="col1"  style="width: 100%;margin-top: 10px;">
				<tr>
				        <td style="width:200px;min-width:185px;">
				          	<div align="right"><font color="ff0000">*</font>自动确认收货时长：</div>
				       	</td>
				        <td align="left" style="padding-left: 2px;">
				           	<input type="text" class="${inputclass}" name="auto_order_confirm" id="auto_order_confirm" value="${orderSetting.auto_order_confirm}"/>
				            <span><strong class="w" style="color: #999; font-weight: 400"> (天) 商家发货后，达到该时间系统自动确认收货</strong></span>
				        </td>
				        
				</tr>
		<%-- <tr>
		        <td>
		          	<div align="center">自动收货提醒时长: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" class="${inputclass}" name="auto_order_notice" id="auto_order_notice"  value="${orderSetting.auto_order_notice}" />
		           	<span><strong class="w"> (天) 商家发货后达到该时长，给买家发送即将自动确认收货的短信、邮件提醒</strong></span>
		        </td>
		</tr> --%>
		<tr>
		        <td style="width:200px;min-width:185px;">
		          	<div align="right"><font color="ff0000">*</font>自动取消未支付订单时长：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" class="${inputclass}" name="auto_order_cancel"  id="auto_order_cancel"    value="${orderSetting.auto_order_cancel}" />
		           		          <span><strong class="w" style="color: #999; font-weight: 400"> (分钟) 用户下单后达到该时长，并且没有完成支付,会自动取消该订单。</strong></span>
		        </td>
	
		</tr>
		<tr>
		        <td style="width:200px;min-width:185px;">
		          	<div align="right"><font color="ff0000">*</font>订单评论有效时间：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" class="${inputclass}" name="auto_order_comment"  id="auto_order_comment"    value="${orderSetting.auto_order_comment}" />
		           		          <span><strong class="w" style="color: #999; font-weight: 400"> (天) 用户不对订单进行评论，则会自动好评！</strong></span>
		        </td>
	
		</tr>
		<tr>
		        <td style="width:200px;min-width:185px;">
		          	<div align="right"><font color="ff0000">*</font>退换货有效时间：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" class="${inputclass}" name="auto_product_return"  id="auto_product_return" value="${orderSetting.auto_product_return}" />
		           		          <span><strong class="w" style="color: #999; font-weight: 400"> (天) 用户须在该时间段进行退换货处理才有效！</strong></span>
		        </td>
		</tr>
		<tr>
		        <td width="200px">
		          	<div align="right"><font color="ff0000">*</font>自动取消秒杀未转订单时间：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" class="${inputclass}" name="auto_seckill_cancel"  id="auto_seckill_cancel"    value="${orderSetting.auto_seckill_cancel}" />
		           		          <span><strong class="w" style="color: #999; font-weight: 400"> (分钟) 用户秒杀资格不转订单，则会自动取消！</strong></span>
		        </td>
	
		</tr>
				<%--<tr>--%>
					<%--<td>--%>
						<%--<div align="center">是否开启积分支付: <font color="ff0000">*</font></div>--%>
					<%--</td>--%>
					<%--<td>--%>
						<%--<input type="text" class="${inputclass}"  name="order_integral_enable" id="order_integral_enable" value="${orderSetting.order_integral_enable}"/>--%>
						<%--<span><strong class="w"> 开启后订单可以使用积分支付！</strong></span>--%>
					<%--</td>--%>
				<%--</tr>--%>
				<%--<tr>--%>
					<%--<td>--%>
						<%--<div align="center">积分支付订单的最大比例: <font color="ff0000">*</font></div>--%>
					<%--</td>--%>
					<%--<td>--%>
						<%--<input type="text" class="${inputclass}" name="order_integral_percent"  id="order_integral_percent"    value="${orderSetting.order_integral_percent}" />--%>
						<%--<span><strong class="w"> 积分可以占订单支付的最大比例(0-100)！</strong></span>--%>
					<%--</td>--%>
				<%--</tr>--%>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" ></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" ></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

   $(function() {
    	 $("#form1").validate({
 			rules: {
 			   auto_order_confirm: {
 			     required:true,
 			     digits:true
 			   },
 			auto_order_notice: {
                 required:true,
 			    digits:true
            },
 			auto_order_cancel: {
                 required:true,
 			     digits:true
            },
 			auto_order_comment: {
                 required:true,
 			     digits:true
            },
 			auto_product_return: {
                 required:true,
 			     digits:true
            },
			order_integral_enable: {
				required:true
			},
			order_integral_percent:{
				required:true,
				digits:true
			},
			auto_seckill_cancel: {
				required:true,
				digits:true
			}
            
 		},
        messages: {
 			auto_order_confirm:{
 			  required:"请输入确认收货时长",
 			  digits:"请输入数字"
 			},
 			auto_order_notice:{
 			  required:"请输入收货提醒时长",
 			  digits:"请输入数字"
 			},
 			auto_order_cancel:{
 			  required:"请输入取消未支付订单时长",
 			  digits:"请输入数字"
 			},
 			auto_order_comment: {
                 required:"请输入订单评论时长",
 			  	 digits:"请输入数字"
            },
 			auto_product_return: {
                 required:"请输入退换货时长",
 			     digits:"请输入数字"
            },
            auto_seckill_cancel: {
            	required:"请输入取消秒杀资格时长",
			     digits:"请输入数字"
            }

 		},
 		submitHandler: function(form){
        	$.formUtils.ajaxSubmit(form, function(result){
        		   if(result === "OK"){
       		   			layer.msg("恭喜您,保存成功!",{icon: 1});
	        	   }else{
	        		   layer.msg(result,{icon: 2});
	        	   }
        	});
	     }
     });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</html>
