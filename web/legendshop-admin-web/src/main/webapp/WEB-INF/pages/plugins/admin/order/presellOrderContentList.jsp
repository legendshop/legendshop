<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="am-table am-table-striped" >
	<thead>
		<tr>
			<th width="28%" style="text-align:left;">
				<input type="checkbox">
				<span style="display: inline-block;margin: 0 130px 0 10px;">图片</span>
				<span style="display: inline-block;">商品名称</span>
			</th>
			<th style="text-align: center;width: 12%;">规格</th>
			<th style="text-align: center;width: 12%;">单价</th>
			<th style="text-align: center;width: 12%;">数量</th>
			<th style="text-align: center;width: 12%;">买家</th>
			<th style="text-align: center;width: 12%;">金额</th>
			<th style="text-align: center;">操作</th>
		</tr>
	</thead>
</table> 
 <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
     <div class="orderlist_one" orderId="${order.subId}">
          <h4 class="orderlist_one_h4">
          <input type="checkbox">
          <span>编号：${orderstatues.count}</span>
          <span>店铺：<a href="${contextPath}/admin/shopDetail/load/${order.shopId}">${order.shopName}</a></span>
          <span>订单号：<a href="${contextPath}/admin/order/orderAdminDetail/${order.subNo}">${order.subNo}</a></span>
          <span>下单时间：<fmt:formatDate value="${order.subCreateTime}" type="both" /></span>
          <span>订单状态：
          	<strong style="color: #ff6c00;font-weight: 400;">
          		<c:choose>
					 <c:when test="${order.status eq 1 }">
					        待付款
					 </c:when>
					 <c:when test="${order.status eq 2 }">
					       待发货
					 </c:when>
					 <c:when test="${order.status eq 3 }">
					       待收货
					 </c:when>
					 <c:when test="${order.status eq 4 }">
					      已完成
					 </c:when>
					 <c:when test="${order.status eq 5 }">
					      交易关闭
					 </c:when>
			  </c:choose>
		 </strong>
		</span> 
	    <span class="blue">付款类型：${order.payManner eq 1?"货到付款":"在线支付"}</span>
        <span class="blue">支付方式：
            <c:choose>
		      <c:when test="${order.status eq 1}">未支付</c:when>
		      <c:otherwise>${order.payTypeName}</c:otherwise>
	    </c:choose>
	  </span>
      </h4>
       <table width="100%" cellspacing="0" cellpadding="0" border="0" class="user_order_table ${tableclass }">
         <tbody>
                <!-- S 商品列表 -->
    		 <c:forEach items="${order.orderItems}" var="orderItem" varStatus="orderItemStatues">
				<tr>              		
                       <div style="width: 50px;float: left;margin-top: 27px;">
                         <a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
                             <img src="<ls:images item="${orderItem.prodPic}" scale="3"/>">
                          </a>
                      </div>
                       <div style="width: 45%;float: left;margin-left:20px;">
                         <span class="order_img_name" ><a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
                         <c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >
						<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
					 </c:if>
                         </span>
			                             <span>${orderItem.attribute}</span> 
			                            <span>数量：${orderItem.basketCount}</span> 
			                            <span>促销：${orderItem.promotionInfo}</span> 
                        </div>
                        
                        <td class="vertical-dividers" style="width: 28%;text-align:left;">
							<div class="clearfix">
								<div class="fl">
									<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>" class="order_img"> 
										<img src="<ls:images item="${orderItem.pic}" scale="3"/>">
									</a>
								</div>
								<div>
									<span class="order_img_name">
										<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">
											<a href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
										</c:if> 
										<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
									</span>
									<span class="order_img_span">促销：${orderItem.promotionInfo}</span>
								</div>
							</div>
						</td>
                        
                        
                        <div style="width: 200px;float:left;">
                       <c:choose>
                       	<c:when test="${not empty  orderItem.attribute}">
                       		<span>${orderItem.attribute}</span>
                       	</c:when>
                       	<c:otherwise>
                       		<span>&nbsp;</span>
                       	</c:otherwise>
                       </c:choose>
                        </div>
                        	  
                        
                        <div style="width: 100px;float:left;">
                        	  <span>数量：${orderItem.basketCount}</span>
                        </div>
                        <div style="clear: both;"></div>
                          </div>
    
     <!-- S 商品列表 -->
           
           <td width="20%" align="center" class="vertical-dividers"  style="vertical-align: middle;">
           <span class="blue2 order_xx">
             <b obj_id="${order.userName}" mark="name_${order.userName}" addrOrderId="${order.addrOrderId}">
                <a href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'>${order.userName}</a>
             </b>
             </span>
            </td>
              <td width="21%" align="center"  style="vertical-align: middle;">
              ¥<fmt:formatNumber pattern="0.00" value="${order.actualTotalPrice}" />
              <br>
            <c:if test="${not empty order.freightAmount}">
               <span class="order_sp"><strong>(含运费:¥<fmt:formatNumber pattern="0.00" value="${order.freightAmount}" />)</strong></span><span class="order_sp">
               <br>
               <c:if test="${order.status eq 1 }">
                <a class="order_money_modify" href="javascript:void(0);" onclick="changeAmountShow('${order.subNo}','${order.userName}','${order.depositPrice}','${order.finalPrice}','${order.freightAmount}','${order.actualTotalPrice}','${order.payPctType}');"> 
                <input class="${btnclass}" type="button" value="调整费用">
                </a>
               </c:if>
               </span>
            </c:if>
           </td>
            <td align="center" class="vertical-dividers"  style="vertical-align: middle;">
                    <c:choose>
		 <c:when test="${order.status eq 1 }">
                               <a  href="javascript:void(0);" onclick="orderCancelShow('${order.subNo}');"> <input class="${btnclass}" type="button" value="取消订单"></a>
                               <br>
		 </c:when>
		 <c:when test="${order.status eq 2 }">
		 	<c:if test="${order.payPctType eq 1 && order.isPayFinal eq 1}">
		       <a href="javascript:void(0);" onclick="deliverGoods('${order.subNo}');" class="order_outline_ok"  href="javascript:void(0);"><input class="${btnclass}" type="button" value="确认发货"></a>
		 	
		 	</c:if>
		 	<c:if test="${order.payPctType eq 0}">
		       <a href="javascript:void(0);" onclick="deliverGoods('${order.subNo}');" class="order_outline_ok"  href="javascript:void(0);"><input class="${btnclass}" type="button" value="确认发货"></a>
		 	
		 	</c:if>
		 		<br>
		 </c:when>
	  </c:choose>
		 <a  class="order_bottom_btn" href="<ls:url address="/admin/order/presellOrderAdminDetail/${order.subNo}"/>"><input class="${btnclass}" type="button" value="查看订单"></a>
            			<br>
            			<a target="_blank" class="order_bottom_btn" href="<ls:url address="/admin/order/orderPrint/${order.subNo}"/>"><input class="${btnclass}" type="button" value="打印订单"></a> 
            			<br>
            			<%--<c:if test="${order.status eq 3 or order.status eq 4}">
                <a href="<ls:url address="/admin/order/printDeliveryDocument/${order.subId}/${order.dvyTypeId}"/>" class="order_bottom_btn" target="_blank"><input class="${btnclass}" type="button" value="打印快递单"></a> 
              </c:if>--%>
           </td>
         </tr>
       </c:forEach>
         
         
         
         
         <tr style="color:#999;">
            <td align="center">阶段1：定金</td> 
            <td>¥<fmt:formatNumber value="${order.depositPrice }" type="currency" pattern="0.00"></fmt:formatNumber></td>
            <c:if test="${order.status eq 5 }"> 
            	<td colspan="2"></td>
            </c:if>
            <c:if test="${order.status ne 5 }">
            	<c:if test="${order.isPayDeposit eq 0}">
            		<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
            			<td colspan="1" align="center">用户可支付时间还剩<font color="red">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟</td>
            			<td colspan="1" align="center">买家未付款</td>
            		</c:if>
            		<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
            			<td colspan="2" align="center">已过期</td>
            		</c:if>
             </c:if>
            	<c:if test="${order.isPayDeposit eq 1}">
             	<td colspan="2" align="center"><font color="green">买家已付款</font></td>
             </c:if>
            </c:if>
          </tr>
         <c:choose>
         <c:when test="${order.payPctType eq 1}">
          <tr style="color:#999;">
            <td align="center">阶段2：尾款</td>
            <td>¥${order.finalPrice+order.freightAmount}(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)</td>
            <c:if test="${order.status eq 5 }">
            	<td colspan="2" align="center"></td>
            </c:if>
            <c:if test="${order.status ne 5 }">
            	<c:if test="${order.isPayFinal eq 1}">
              <td colspan="1" align="center">
              	尾款支付时间: </br>
              	<fmt:formatDate value="${order.finalMStart }"  pattern="yyyy-MM-dd"/> - <fmt:formatDate value="${order.finalMEnd }"  pattern="yyyy-MM-dd"/>
              </td>
            	</c:if>
             <c:if test="${order.isPayFinal eq 0}">
             	<td colspan="2" align="center">
              		<c:choose>
               		<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
               		<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
               		<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
               	</c:choose>
             	</td>
             </c:if>
             <c:if test="${order.isPayFinal eq 1}">
		 <td colspan="2" align="center"><font color="green">买家已付款</font></td>
             </c:if>
            </c:if>
          </tr>
         </c:when>
         </c:choose>
       </tbody>
      </table>
     </div>
</c:forEach>
<div align="center">
	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
</div>    
