<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<body>
	<% int offset = 1; %>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>权限管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/member/right/query"/>">权限管理</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">拥有[${bean.name }]权限的角色列表</span></th>
				</tr>
			</table>
			<div align="center" class="order-content-list" style="margin-top: 20px;">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" id="item" export="false"
					class="${tableclass}" style="width:100%">
					<display:column style="width:70px" title="顺序"><%=offset++%></display:column>
					<display:column title="名称 " property="name"></display:column>
					<display:column title="角色名称 " property="roleType"></display:column>
					<display:column title="状态">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="ENABLED" selectedValue="${item.enabled}" defaultDisp="" />
					</display:column>
					<display:column title="备注" property="note"></display:column>
				</display:table>
			</div>
			<div align="center" style="margin-top: 60px;">
				<input class="${btnclass}" type="button" value="返回"
					onclick="window.location='${contextPath}/admin/member/right/query'" />
			</div>
		</div>
	</div>
</body>
</html>

