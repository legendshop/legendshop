<%@ page language="java" pageEncoding="UTF-8"%>
<!Doctype html>
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>版权管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">版权管理</strong> ＞ 
						<a href="${contextPath}/license/upgrade">版权验证</a>
						</th>
					</tr>
			</table>

			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li class="am-active"><i></i><a href="<ls:url address="/license/upgrade"/>">账号激活</a></li>
						<li><i></i><a href="<ls:url address="/license/upgradeByKey"/>">激活码激活</a></li>
					</ul>
				</div>
			</div>


			<form:form action="${contextPath}/license/postUpgrade" method="post" id="form1" enctype="multipart/form-data">
				<input id="id" name="id" value="${index.id}" type="hidden">
				<table class="${tableclass} no-border content-table">
					<tr>
						<td width="200px" style="text-align: center;">
							 <div align="right" style="font-weight: bold;padding-right: 53px;">版权信息</div>
						</td>
						<td></td>
					</tr>
					<tr>
						<td><div align="right">证书类型&nbsp;:</div></td>
						<td align="left">
							<c:choose>
								<c:when test="${licenseType eq 'UN_AUTH'}">未注册版</c:when>
								<c:otherwise>
									${licenseType}
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<c:if test="${not empty domainName }">
						<tr>
							<td><div align="right">域名信息&nbsp;:</div></td>
							<td align="left">${domainName }</td>
						</tr>
					</c:if>
					<tr>
						<td><div align="right">商城版本&nbsp;:</div></td>
						<td align="left">${version }</td>
					</tr>
					<tr>
						<td><div align="right">安装时间&nbsp;:</div></td>
						<td align="left"><fmt:formatDate value="${installDate}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
				</table>
				<table class="${tableclass} no-border content-table" id="col1" style="width: 100%">
					<tr>
						<td width="200px" style="text-align: center;">
							 <div align="right" style="font-weight: bold;">输入并提交新的版权信息</div>
						</td>
						<td></td>
					</tr>
					<tr>
						<td valign="top"><div align="right">
								<font color="#ff0000">*</font>商家编码:
							</div>
						</td>
						<td align="left">
							<input type="text" name="companyCode" id="companyCode" size="60" value="${bean.companyCode}" class="${inputclass}" />
							&nbsp;&nbsp;<br /> 
							<span style="color: #999;">(商家编码请与朗尊软件联系)</span>
						</td>
					</tr>
					<tr>
						<td valign="top"><div align="right"><font color="#ff0000">*</font>公司名:</div></td>
						<td align="left"><input type="text" name="company" id="company" size="60"
							value="${bean.company}" class="${inputclass}" /> &nbsp;&nbsp; <br />
							<span style="color: #999;">(请提供本公司的全称)</span>
						</td>
					</tr>
					<tr>
						<td valign="top"><div align="right"><font color="#ff0000">*</font>域名:</div></td>
						<td align="left"><input type="text" name="link" id="link" size="60" value="${bean.domainName}" class="${inputclass}" /> &nbsp;&nbsp;
							<br /> 
							<span style="color: #999;">(请提供本公司的域名,如果有多个域名请以英文逗号","隔开,域名不要带www,例如：legendshop.cn)</span>
						</td>
					</tr>
					<tr>
						<td valign="top"><div align="right">IP地址:</div></td>
						<td align="left">
						    <input type="text" name="des" id="des" size="60" value="${index.ip}" class="${inputclass}" /> &nbsp;&nbsp; <br />
							<span style="color: #999;">(请提供IP地址，如果有多个IP请以英文逗号","隔开)</span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td align="left">
							<div>
								<input type="submit" value="保存" class="${btnclass}" /> 
							</div>
						</td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
</body>
<script type="text/javascript">
	var upgradeResult = '${upgradeResult}';
	$.validator.setDefaults({});
	$(document).ready(function() {
		$("#form1").validate({
			rules : {
				companyCode : "required",
				company : "required",
				link : "required"
			},
			messages : {
				companyCode : "请输入商家编码",
				company : "请输入公司名字",
				link : "请输入域名"
			}
		});

		if(upgradeResult != ''){
			if(upgradeResult == 'true'){
				layer.alert("系统升级成功");
			}else{
				layer.alert("系统升级失败，请联系厂家获取配置参数");
			}
			
		}

	});
</script>
</html>