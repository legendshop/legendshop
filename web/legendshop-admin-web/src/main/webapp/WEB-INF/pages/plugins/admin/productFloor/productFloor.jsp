<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/system/productFloor/save" method="post" id="form1">
            <input id="id" name="id" value="${productFloor.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1" style="width:100%">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建首页楼层
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td  style="width: 12%">
		          	<div>名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="name" id="name" value="${productFloor.name}" style="width: 400px"/>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>状态: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		         <select id="status" name="status">
							<ls:optionGroup type="select" required="true" cache="true" beanName="ONOFF_STATUS" selectedValue="${productFloor.status}" />
				</select>
				&nbsp;顺序:<font color="ff0000">*</font>
				<input type="text" name="seq" id="seq" value="${productFloor.seq}" style="width: 30px"/>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>楼层样式: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="cssStyle" id="cssStyle" value="${productFloor.cssStyle}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>商品分类: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="sort" id="sort" value="${productFloor.sort}" />
		        </td>
		</tr>
		<tr>
				<td>
					<div>级别：<font color="ff0000">*</font></div>
				</td>
				<td>
					<input type="text" name="level" id="level" value="${productFloor.level}" />
				</td>
		</tr>
		<tr>
		        <td>
		          	<div>左边的广告位: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="leftAdv" id="leftAdv" value="${productFloor.leftAdv}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>右边的广告位: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="rightAdv" id="rightAdv" value="${productFloor.rightAdv}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>下面的广告位: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="bootomAdv" id="bootomAdv" value="${productFloor.bootomAdv}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>产品列表: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="prods" id="prods" value="${productFloor.prods}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>团购产品列表: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="gprods" id="gprods" value="${productFloor.gprods}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>品牌列表: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="brands" id="brands" value="${productFloor.brands}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div>文章列表: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="news" id="news" value="${productFloor.news}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/system/productFloor/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

