<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>公告编辑 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
  <style>
  	.layui-input{
  		width:107px;
  	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	 	<div class="admin-content" id="admin-content" style="overflow-x:auto;">

<form:form action="${contextPath}/admin/pub/save" method="post" id="form1" onsubmit="return checkFinish();">
    <input id="id" name="id" value="${bean.id}" type="hidden">
    <div align="center">
        <table class="${tableclass}" style="width: 100%">
            <thead>
            <tr>
                <th class="no-bg title-th">
                	<span class="title-span">公告管理   ＞  <a href="<ls:url address="/admin/pub/query"/>">公告管理</a>  ＞
                		<c:if test="${not empty bean.id }"><span style="color:#0e90d2;">公告编辑</span></c:if>
                		<c:if test="${empty bean.id }"><span style="color:#0e90d2;">公告新增</span></c:if>
                	</span>
                </th>
            </tr>
            </thead>
        </table>
        <table style="width: 100%" class="${tableclass} no-border content-table" id="col1">
            <tr>
                <td style="width:200px;">
                    <div align="right"><font color="ff0000">*</font>标题： </div>

                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" maxLength="100" name="title" id="title" value="${bean.title}" size="50" class="${inputclass}"/>
                    <select id="type" name="type" class="${selectclass }">
                        <ls:optionGroup type="select" required="true" cache="true"
                                        beanName="PUB_TYPE" selectedValue="${bean.type}"/>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="right">选择有效时间：</div>
                </td>
                <td align="left" style="line-height: 28px;padding-left: 2px;">
                   <%--  &nbsp;开始时间
                    <input readonly="readonly" name="startDate" id="startDate" class="Wdate" type="text" onClick="WdatePicker()" value='<fmt:formatDate value="${bean.startDate}" pattern="yyyy-MM-dd"/>'/>
                    &nbsp;结束时间
                    <input readonly="readonly" name="endDate" id="endDate" class="Wdate" type="text" onClick="WdatePicker()" value='<fmt:formatDate value="${bean.endDate}" pattern="yyyy-MM-dd"/>'/> --%>
                    	开始时间：<input type="text" readonly="readonly"  class="layui-input" name="startDate" id="startDate" placeholder="开始时间" value='<fmt:formatDate value="${bean.startDate}" pattern="yyyy-MM-dd"/>'/>&nbsp;&nbsp;&nbsp;&nbsp;
                       	结束时间：<input type="text"  readonly="readonly"  class="layui-input" name="endDate" id="endDate" placeholder="结束时间" value='<fmt:formatDate value="${bean.endDate}" pattern="yyyy-MM-dd"/>' />
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
					<font color="ff0000">*</font>内容：
                </td>
                <td align="left" style="padding-left: 2px;">
                    <textarea name="msg" id="msg" cols="100" rows="8" style="width:100%;height:400px;visibility:hidden;">${bean.msg}</textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div align="center">
                        <input type="submit" class="${btnclass}" value="保存"/>
                        <input type="button" class="${btnclass}" value="返回"
                               onclick="window.location='${contextPath}/admin/pub/query?type=${bean.type}'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
</div>
</div>
</body>

<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/pubAdmin.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var UUID = "${cookie.SESSION.value}";
    $.validator.setDefaults({});
</script>
</html>
