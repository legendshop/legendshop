<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>移动装修- 基础设置</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/appSetting.css"/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<div class="seller_right" style="margin-bottom:70px;">
				<table class="${tableclass} title-border" style="width: 100%">
						<tr><th class="title-border">移动装修 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基础设置<span></th></tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li>
								<i></i><a href="<ls:url address="/admin/appSetting/themeStyle"/>">主题风格</a>
							</li>
							<li class="this am-active">
								<i></i><a href="<ls:url address="/admin/appSetting/categorySetting"/>">分类展示</a>
							</li>
						</ul>
					</div>
					<!-- 分类设置 -->
					<div align="left" class="order-content-list">
			          <div class="box-item-content">
                        <div class="item-view">
                            <img src="${contextPath}/resources/templets/amaze/images/appdecorate/second-category.png" alt=""><!-- 二级分类图，默认模式 -->
                        </div>
                        <div class="item-edit category-edit">
                            <div class="category-edit-list">
                                <div class="list-line">
                                    <p class="line-p">模板选择：</p>
                                    <div class="line-div" id="classify">
                                        <a href="javascript:void(0)" class="cate-item" data-img="/resources/templets/amaze/images/appdecorate/first-category.png"
                                        data-noImg="/resources/templets/amaze/images/appdecorate/first-category.png" data-type="first">一级分类</a>
                                        <a href="javascript:void(0)" class="cate-item" data-img="/resources/templets/amaze/images/appdecorate/second-category.png"
                                        data-noImg="/resources/templets/amaze/images/appdecorate/no-second-category.png" data-type="second">二级分类</a>
                                        <a href="javascript:void(0)" class="cate-item" data-img="/resources/templets/amaze/images/appdecorate/third-category.png"
                                        data-noImg="/resources/templets/amaze/images/appdecorate/no-third-category.png" data-type="three">三级分类</a>
                                    </div>
                                </div>
                                <div class="list-line">
                                    <p class="line-p">样式选择：</p>
                                    <div class="line-div">
                                        <a href="#" class="cate-item cur">经典样式</a>
                                    </div>
                                </div>
                                <c:if test="${categorySetting.category ne 'first'}">
                                 <div class="list-line" id="selectCategory">
                             </c:if>
                                <c:if test="${categorySetting.category eq 'first'}">
                                 <div class="list-line" id="selectCategory" style="display: none;">
                             </c:if>
                                     <p class="line-p">分类图：</p>
                                     <div class="line-div">
                                      <c:if test="${empty categorySetting.schema || categorySetting.schema eq 'noImages'}">
                                          <span class="cate-img"><input type="radio" name="schema" checked="checked" value="noImages">无图模式</span>
                                          <span class="cate-img"><input type="radio" name="schema" value="hasImages">有图模式</span>
                                      </c:if>
                                      <c:if test="${categorySetting.schema eq 'hasImages'}">
                                          <span class="cate-img"><input type="radio" name="schema" value="noImages">无图模式</span>
                                          <span class="cate-img"><input type="radio" name="schema" checked="checked" value="hasImages">有图模式</span>
                                      </c:if>
                                     </div>
                                 </div>
                            </div>
                        </div>
                        <div class="save-set"><a href="javascript:save()" class="blue_but">保存</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/categorySetting.js'/>"></script>
<script>
	var contextPath = "${contextPath}";
	var category = '${categorySetting.category}';
    var schema = '${categorySetting.schema}';
</script>
</body>
</html>
