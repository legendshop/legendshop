<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
	        <tr>
	            <th class="title-border">新闻管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">发现文章管理</span></th>
	        </tr>
	    </table>
	<form:form action="${contextPath}/admin/discoverArticle/query" id="form1" method="get">
		 <div class="criteria-div">
		    <span class="item">            
		             <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		                      标题：
		             <input type="text" name="name" maxlength="50" size="30" value="${discoverArticle.name}" class="${inputclass}" placeholder="请输入标题搜索"/>
                    <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
                    <input type="button" value="创建文章" class="${btnclass}" onclick='window.location="${contextPath}/admin/discoverArticle/load"'/>
		    </span>
		 </div>
	</form:form>
	<div align="center" class="order-content-list">
	    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	    <display:table name="list" requestURI="/admin/discoverArticle/query/${news.position}" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
	        <display:column title="顺序" style="width:70px"><%=offset++%>
	        </display:column>
	        <display:column title="标题" style="max-width:150px; word-wrap:break-word">
		        <div class="order_img_name">
		        	${item.name}
		        </div>
	        </display:column>
	        
	        <display:column title="简介" style="width:150px">
	       	 ${fn:substring(item.intro, 0,8)}
	        </display:column>
	        <display:column title="点赞数" style="width:150px">
	            ${item.thumbNum}
	        </display:column>
	        <display:column title="浏览数" style="width:150px">
	            ${item.pageView}
	        </display:column>
	        <display:column title="文章审核状态" style="width:150px">
	            ${item.status==1?'上线':'下线'}
	        </display:column>
	        <display:column title="录入时间" style="width:250px;" property="createTime" format="{0,date,yyyy-MM-dd HH:mm}" sortable="true" sortName="n.news_date"></display:column>
	        <display:column title="操作" media="html" style="width:260px;">
                <div class="table-btn-group">
                    <c:choose>
                        <c:when test="${item.status == 1}">
                            <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.name}" status="0">下线</button>
                            <span class="btn-line">|</span>
                        </c:when>
                        <c:otherwise>
                            <button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.name}" status="1">上线</button>
                            <span class="btn-line">|</span>
                        </c:otherwise>
                    </c:choose>
                    <button class="tab-btn" onclick="window.location='${contextPath}/admin/discoverArticle/load/${item.id}'">编辑</button>
					<span class="btn-line">|</span>
                    <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
                </div>
	        </display:column>
	    </display:table>
	    <!-- 分页条 -->
	     	<div class="clearfix">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
	</div>
</div>
</body>
<script language="JavaScript" type="text/javascript">

    function deleteById(id) {
    	layer.confirm('确定删除 ?', {icon:3}, function(){
            window.location = "${contextPath}/admin/discoverArticle/delete/" + id;
    	});
    }
    $(document).ready(function () {
        $("button[name='statusImg']").click(function (event) {
                    $this = $(this);
                    $.post( "${contextPath}/admin/discoverArticle/updatestatus/",
                    		{"itemId":$this.attr("itemId"),
                    		 "status":$this.attr("status")
                    		},function(data){
                    			if(data){
                    				layer.msg("修改成功",{icon:1},function(){
                    					setTimeout(function(){
                    						window.location.reload();
                    					},500)
                    				});
                    				
                    			}else{
                    				layer.alert("修改失败",{icon:2},function(){
                    					setTimeout(function(){
                    						window.location.reload();
                    					},500)
                    				});
                    			}
                    			
                    		},"json")
                  //	initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/discoverArticle/updatestatus/", $this, "${contextPath}");
       				
        }
        );
        highlightTableRows("item");
    });
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>


