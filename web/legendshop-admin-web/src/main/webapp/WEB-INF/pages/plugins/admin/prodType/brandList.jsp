<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
	 <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <title>品牌列表</title>
    <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<style type="text/css">
		body{
			min-height:96%;
		}
		#form1{
			margin:15px;
			line-height: 28px;
		}	
		input[type="text"]{
			height:28px;
			border: 1px solid #ddd;
		}
		.see-able{
			width: 95%;
			min-width:680px;
		}
		.see-able th,td{
			height:30px !important;
		}
	</style>
</head>
<body>
    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    <form:form  action="${contextPath}/admin/prodType/brandList/${proTypeId}" id="form1" method="post" onsubmit="clearCurPageNo()">
    			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
            		品牌名称&nbsp;<input type="text" name="brandName" maxlength="50" value="${brandName}" />
            		<input type="submit" class="criteria-btn" value="搜索"/>
    </form:form>

	<c:choose>
		<c:when test="${fn:length(list) > 0 }">
			<div align="center">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/brand/query" id="item" export="false" class="see-able am-table am-table-striped am-table-hover table" >
				
					<display:column title="关联" style="width: 60px;">
						<label class="checkbox-wrapper" style="float:left;margin-left:27px;">
							<span class="checkbox-item">
								<input type="checkbox" name="id"  value="${item.brandId}" class="checkbox-input" onclick="selectChild(this);"/>
								<span class="checkbox-inner" style="margin-left:10px;"></span>
							</span>
					    </label>	
					</display:column>

					<display:column title="品牌" property="brandName" style="width: 250px;"></display:column>
					<display:column title="图片" style="width:140px">
						<c:if test="${not empty item.brandPic}">
							<img width="90" height="30" src="<ls:photo item='${item.brandPic}'  />" />
						</c:if>
					</display:column>
					<display:column title="精品" style="width:35px">
						<c:choose>
							<c:when test="${'1' eq item.commend}">
								<img src="<ls:templateResource item='/resources/common/images/right.png'/> ">
							</c:when>
							<c:otherwise>
								<img src="<ls:templateResource item='/resources/common/images/wrong.png'/> ">
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="商城" property="userName"></display:column>
				</display:table>
				<div class="fr" style="display: block;width:100%;">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
       			</div>
			</div>

			<div align="center" style="margin-top: 70px;padding-bottom:25px;">
				<input type="button" class="criteria-btn" value="保存并关闭" onclick="save_brand();" />
			</div>
		</c:when>
		<c:otherwise>
			<div style="margin: 15px;">找不到记录</div>
		</c:otherwise>
	</c:choose>

<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/brandAdminList.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var proTypeId = "${proTypeId}";
	<%--var brandName = "${brandName}";--%>
	function selectChild(obj){
 		if(!obj.checked){
 			$(obj).prop("checked",false);
 			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
 		}else{
 			$(obj).prop("checked",true);
 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
 		}
	}
	function clearCurPageNo(){
		const brandName = $('input[name="brandName"]').val();
		if (brandName != null){
			$('input[name="curPageNO"]').val(1);
		}
		return true;
	}
</script>   
</body>
</html>

