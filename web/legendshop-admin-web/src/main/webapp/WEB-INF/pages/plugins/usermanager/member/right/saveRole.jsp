﻿<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<style type="text/css">
.center_letter {
	text-align: center;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/member/role/query"/>">角色管理</a>
              				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"><c:choose>
							<c:when test="${not empty bean}">修改角色</c:when>
							<c:otherwise>创建角色</c:otherwise>
						</c:choose>
						</span>
						</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/role/save" id="form1"
				method="post">
				<input type="hidden" name="id" value="${bean.id }">
				<!-- 默认为后台应用 -->
				<input type="hidden" name="appNo" value="BACK_END"> 
				<!-- 默认为系统角色 -->
				<input type="hidden" name="category" value="2">
				<div align="center" style="margin-top: 10px;">
					<table align="center" class="${tableclass} no-border content-table" id="col1">
						<tr>
							<td width="200px">
								<div align="right">
								<font color="ff0000">*</font>名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="name" id="name"
								value="${bean.name}" maxlength="32" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
								<font color="ff0000">*</font>角色名称 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="roleType" id="roleType"
								value="${bean.roleType}" maxlength="32"/></td>
						</tr>
						<tr>
							<td>
							<div align="right">状态：</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<label class="radio-wrapper <c:if test='${empty bean.enabled ||bean.enabled eq 1}'> radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="enabled" value="1" <c:if test='${empty bean.enabled || bean.enabled eq 1}'> checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">有效</span>
								</label>
								<label class="radio-wrapper <c:if test='${bean.enabled eq 0}'> radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" name="enabled" value="0" <c:if test='${bean.enabled eq 0}'> checked="checked"</c:if>>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">无效</span>
								</label>
							</td>
						</tr>
						<%-- <tr>
							<td class="center_letter">所属应用</td>
							<td><select id="appNo" name="appNo">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="APP_NO" selectedValue="${bean.appNo}" />
							</select></td>
						</tr>
						<tr>
							<td class="center_letter">角色类型</td>
							<td><select id="category" name="category">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ROLE_CATEGORY" selectedValue="${bean.category}" />
							</select> (系统角色只有管理员可以操作)</td>
						</tr> --%>
						<tr>
							<td>
								<div align="right">备注：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input name="note" type="text" value="${bean.note}" maxlength="49">
							</td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" /> 
									<input class="${btnclass}" type="button" value="返回"
									onclick="window.location='${contextPath}/admin/member/role/query'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script language="javascript">
	$.validator.setDefaults({});



	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				name : {
					maxlength:15,
					required : true
				},
				roleType : {
					maxlength:15,
					required : true
				},
				note : {
					maxlength:30,
				}
			},
			messages : {
				name : {
					required : '<fmt:message key="name.required"/>'
				},
				roleType : {
					required : '<fmt:message key="roletype.required"/>'
				}
			}
		});
		highlightTableRows("col1");
		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
		/* layer.alert("成功",{icon:1},function(){
			window.location.reload(true);
			}); */
	});
	$("input:radio[name='enabled']").change(function(){
		 $("input:radio[name='enabled']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
		});
	});

</script>
</html>
