<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
	        <tr>
	            <th class="title-border">帮助中心&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">帮助文章</span></th>
	        </tr>
	    </table>
	<form:form action="${contextPath}/admin/news/query" id="form1" method="get">
		 <div class="criteria-div">
		 	<span class="item">
		                    栏目：
                    <select id="newsCategoryId" name="newsCategoryId" class="criteria-select">
                        <ls:optionGroup type="select" required="false" cache="fasle"
                                        sql="select t.news_category_id, t.news_category_name from ls_news_cat t where t.status = 1" selectedValue="${news.newsCategoryId}"/>
                    </select>
		    </span>
		    <span class="item">
		                    状态：
                    <select id="status" name="status" class="criteria-select">
                        <ls:optionGroup type="select" required="false" cache="true"
                                        beanName="ONOFF_STATUS" selectedValue="${news.status}"/>
                    </select>
		    </span>
		    <span class="item">
		                    位置：
                  <select id="position" name="position" class="criteria-select">
	                 <option value=""> -- 请选择 -- </option>
	                 <c:forEach items="${positionItem}" var="item">
	                 		<option value="${item.positionId}" <c:if test="${news.position eq item.positionId}">selected</c:if>>${item.positionName}</option>
	                 </c:forEach>
	              </select>
		    </span>
		    <span class="item">            
		             <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		                      标题：
		             <input type="text" name="newsTitle" maxlength="50" size="30" value="${news.newsTitle}" class="${inputclass}" placeholder="请输入标题搜索"/>
                    <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
                    <input type="button" value="创建文章" class="${btnclass}" onclick='window.location="${contextPath}/admin/news/load"'/>
		    </span>
		 </div>
	</form:form>
	<div align="center" class="order-content-list">
	    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	    <display:table name="list" requestURI="/admin/news/query/${news.position}" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
	        <display:column title="顺序" style="width:70px"><%=offset++%>
	        </display:column>
	        <display:column title="标题" style="max-width:300px; word-wrap:break-word">
		        <div class="order_img_name">
		        	<a href="${PC_DOMAIN_NAME}/news/${item.newsId}" target="_blank">${item.newsTitle}</a>
		        </div>
	        </display:column>
	        <display:column title="栏目" style="width:150px">
	            ${item.newsCategoryName}
	        </display:column>
	        <display:column title="录入时间" style="width:250px;" property="newsDate" format="{0,date,yyyy-MM-dd HH:mm}" sortable="true" sortName="n.news_date"></display:column>
	        <display:column title="排序" property="seq" style="width:70px"></display:column>
	        <display:column title="操作" media="html" style="width:260px;">
                <div class="table-btn-group">
                    <c:choose>
                        <c:when test="${item.status == 1}">
                            <button class="tab-btn" name="statusImg" itemId="${item.newsId}" itemName="${item.newsTitle}" status="${item.status}">下线</button>
                            <span class="btn-line">|</span>
                        </c:when>
                        <c:otherwise>
                            <button class="tab-btn" name="statusImg" itemId="${item.newsId}" itemName="${item.newsTitle}" status="${item.status}">上线</button>
                            <span class="btn-line">|</span>
                        </c:otherwise>
                    </c:choose>
                    <button class="tab-btn" onclick="window.location='${contextPath}/admin/news/load/${item.newsId}'">编辑</button>
					<span class="btn-line">|</span>
                    <button class="tab-btn" onclick="deleteById('${item.newsId}');">删除</button>
                </div>
	        </display:column>
	    </display:table>
	    <!-- 分页条 -->
	     	<div class="clearfix">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
		<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
		    <tr>
		        <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
		            1. 每个文章都是挂在栏目下面<br>
		            2. 文章处于上线状态页面可见，处于下线状态页面不可见<br>
		            3. 分类，指商品类型，用于标识该文章是描述那一个商品类型<br>
		            <%--4. 是否重点，如果是重点则在页面中以醒目颜色标注<br>--%>
		        </td>
		    <tr>
		</table>
	</div>
</div>
</body>
<script language="JavaScript" type="text/javascript">

    function deleteById(id) {
    	layer.confirm('确定删除 ?', {icon:3}, function(){
            window.location = "${contextPath}/admin/news/delete/" + id;
    	});
    }
    $(document).ready(function () {
        $("button[name='statusImg']").click(function (event) {
                    $this = $(this);
                    initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/news/updatestatus/", $this, "${contextPath}");
        }
        );
        highlightTableRows("item");
    });
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>


