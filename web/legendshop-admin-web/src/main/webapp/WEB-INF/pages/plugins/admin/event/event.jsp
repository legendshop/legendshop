<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
 		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
  		<!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%">
		   		<tr>
			   		<th class="title-border">系统报表&nbsp;＞&nbsp;<a href="<ls:url address="/admin/event/query"/>">后台系统日志</a>
			   			&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">日志详情</span>
			   		</th>
		   		</tr>
		    </thead>
		 </table>
			<table style="width: 100%;margin-left: 20px;" class="${tableclass} no-border content-table">
				<tbody>
					<tr>
						<td width="200px" style="text-align: center;margin-right: 50px;">
							 <div align="right" style="text-align: left; font-weight: bold;">日志详情：</div>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>
							<div align="right">操作员名称：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.userName}</td>
					</tr>
					<tr>
						<td >
							<div align="right">操作描述：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.operation}</td>
					</tr>
					<tr>
						<td >
							<div align="right">响应时间(毫秒)：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.time}</td>
					</tr>
					<tr>
						<td >
							<div align="right">请求方法：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.method}</td>
					</tr>
					<tr>
						<td >
							<div align="right">请求参数：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.params}</td>
					</tr>
					<tr>
						<td >
							<div align="right">ip地址：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.ip}</td>
					</tr>
					<tr>
						<td >
							<div align="right">操作时间：</div>
						</td>
						<td align="left" style="padding-left: 2px;">${event.createTime}</td>
					</tr>
					<tr>
                	<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <input class="${btnclass}" style="margin-top: -15px;" type="button" value="返回"  onclick="window.location='<ls:url address="/admin/event/query"/>'" />
                        </div>
                    </td>
                	</tr>
				</tbody>
			</table>
		</div>
        </div>
        
	</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" ></script>
<script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
      $("#col1 tr").each(function(i){
      if(i>0){
         if(i%2 == 0){
             $(this).addClass('even');
         }else{    
              $(this).addClass('odd'); 
         }   
    }
     });   
         $("#col1 th").addClass('sortable'); 
});
</script>

</html>
