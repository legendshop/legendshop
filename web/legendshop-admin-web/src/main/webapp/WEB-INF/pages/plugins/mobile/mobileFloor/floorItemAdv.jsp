<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.legendshop.base.util.ResourcePathUtil"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen"href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>"type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/floorTemplate.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>

<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
String imagePrefix = ResourcePathUtil.getPhotoPathPrefix();
	request.setAttribute("imagePrefix",  imagePrefix);
%>

<script type="text/javascript">
	$(document).ready(function() {
			jQuery("#updateForm").validate({
						rules : {
									title : "required",
									linkPath : "required",
									seq:"required"
								},
						messages : {
									title : "请输入标题名称！",
									linkPath : "请输入路径！",
									seq:"请输入次序"
								},
						submitHandler : function(form) {
									var picLen = $(".preview img").length;
									if($("#file").val()=="" && picLen>0){
										$("#file").remove();
									}else if($("#file").val()=="" && picLen==0){
										art.dialog.tips("图片不能为空");
										return;
									}
									$("#updateForm").ajaxSubmit({
							            dataType:"json",
							            success: function(data) {
							            	if (data && data.success == false) {
												var msg = "保存失败";
												layer.msg(msg,{icon:2});
												return;
											}
											if (data && data.success == true) {
												parent.location.reload();
												return;
											}
											if (data && data.success == "error") {
												var msg = "广告不能超过3个";
												layer.msg(msg,{icon:0,time:500});
												return;
											}
							            }
							        });

								}
							});
		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");

	});

	
	//取消按钮
	function cancel(){
		parent.location.reload();
	}
</script>


</head>
<body>

	<form:form  action="${contextPath}/admin/mobileFloor/saveFloorItem" method="post" id="updateForm"   enctype="multipart/form-data" >
		<div style="margin: 0 auto;text-align:center;" class="${tableclass}">
			<input id="parentId" name="parentId" value="${parentId}" type="hidden" />
			<input id="id" name="id" value="${floorItem.id}" type="hidden" />
			
			<table class="${tableclass}" style="width: 100%">
				<tbody>
					<tr>
						<td style="height: 20px;"><font color="ff0000">*</font>标题: <input type="text" name="title" id="title" value="${floorItem.title}" /></td>
					</tr>
					<tr>
						<td><font color="ff0000">*</font>链接: <input type="text" name="linkPath" id="linkPath" value="${floorItem.linkPath}" style="width: 300px;"/></td>
					</tr>

					<tr>
						<td>  <font color="ff0000">*</font>次序: <input type="text" name="seq" id="seq" value="${floorItem.seq}" style="width: 30px;"/></td>
					</tr>
					<tr>
						<td>
							上传图片: 
								<input type="file" name="file" id="file" value="选择图片" />
						</td>
					</tr>
					<tr>
						<td style=" text-align:center; ">
						<input type="submit"value="保存" />
						<input type="button"value="取消" onclick="cancel();" />
						</td>
					</tr>
					<tr>
					  <td>
					     <div class="multimage-gallery" style="height:90px;">
							<ul>
								<li data-index="0" class="primary prodImg">
								<div class="preview">
									<c:if test="${not empty floorItem.pic}">
										 <img width="90px" height="90px" src="<ls:photo item='${floorItem.pic}'/>">
									</c:if>
								</div>
							</ul>
						 </div>
					  </td>
				<tr>
				</tbody>
			</table>
		</div>
	</form:form>
</body>
</html>