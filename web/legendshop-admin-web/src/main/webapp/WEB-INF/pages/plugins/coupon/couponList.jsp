<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page import="java.util.Date"%>

<!-- 获取当前日期 -->
<c:set var="nowDate">
	<fmt:formatDate value="<%=new Date()%>" pattern="yyyy-MM-dd HH:mm:ss"
		type="date" />
</c:set>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<style type="text/css">
.editAdd_load {
	position: absolute;
	top: 50%;
	left: 50%;
	margin: -16px 0 0 -16px;
	z-index: 99;
	display: none;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">店铺促销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺优惠劵</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/coupon/query" id="form1"
				method="post">
				<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
				<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO"value="${curPageNO}" /> 
						<span class="item">
							<input type="text" id="shopId" name="shopId" value="${coupon.shopId}" />
						</span>
                   			<input type="hidden" id="shopName" name="shopName" value="${coupon.shopName}" class="${inputclass}"/>
                   		<span class="item">
							<input type="text" id="couponName" name="couponName" maxlength="50" placeholder="请输入优惠券名称" value="${coupon.couponName}" class="${inputclass}" /> 
							<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
						</span>
					</div>
			</form:form>
			<div align="center" id="cuponList" class="order-content-list"></div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		layer.confirm('确定删除 ?', {icon : 3}, function() {
			window.location = "<ls:url address='/admin/coupon/delete/" + id + "'/>";
		});
	}

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		sendData(curPageNO);
	}
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	
	 jQuery(document).ready(function() {
		/* $("a[name='statusImg']").click(function(event) {
			$this = $(this);
			initStatus($this.attr("itemId"),$this.attr("itemName"),$this.attr("status"),"${contextPath}/admin/coupon/updatestatus/",$this, "${contextPath}");
		}); */
		highlightTableRows("item");
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	}); 
	 
	function statusImg(obj){
		$this = $(obj);
		initStatus($this.attr("itemId"),$this.attr("itemName"),$this.attr("status"),"${contextPath}/admin/coupon/updatestatus/",$this, "${contextPath}");
	};
	
	function sendData(curPageNO) {
	    var data = {
	        "curPageNO": curPageNO,
	        "shopId": $("#shopId").val(),
	        "couponName": $("#couponName").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/coupon/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#cuponList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	
	function highlightTableRows(tableId) {
		var previousClass = null;
		var table = document.getElementById(tableId);
		if (table == null)
			return;
		var tbody;
		if (table.getElementsByTagName("tbody") != null) {
			tbody = table.getElementsByTagName("tbody")[0];
		}
		if (tbody == null) {
			var rows = table.getElementsByTagName("tr");
		} else {
			var rows = tbody.getElementsByTagName("tr");
		}
		for (i = 0; i < rows.length; i++) {
			rows[i].onmouseover = function() {
				previousClass = this.className;
				this.className = 'pointercolor'
			};
			rows[i].onmouseout = function() {
				this.className = previousClass
			};
		}
	}

	function issueCoupon(id) {
		layer.open({
			title : '提示',
			id : 'Retested',
			content : "${contextPath}/admin/coupon/issueCoupon/" + id,
			type : 2,
		});
	}

	function confirmDelete(couponId, name) {
		layer.confirm("确定要删除'" + name + "'吗？", {
			icon : 3,
			title : '提示'
		}, function() {
			var result;
			jQuery.ajax({
				url : "${contextPath}/admin/coupon/delete/" + couponId,
				type : 'post',
				async : false, //默认为true 异步
				dataType : 'json',
				error : function(jqXHR, textStatus, errorThrown) {
					layer.alert("网络异常,请稍后重试！",{icon:2});
				},
				success : function(retData) {
					result = retData;
				}
			});

			if ('OK' == result) {
				layer.msg('删除成功', {
					icon : 1,
					time : 700
				}, function() {
					window.location.reload(true);
				});
			} else {
				layer.msg(result, {icon:2, time:1500});
			}
		}, function() {
			//cancel
		});
	}
</script>
<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load" />
</html>
