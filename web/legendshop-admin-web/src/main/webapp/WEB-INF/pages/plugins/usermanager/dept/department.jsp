<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
	
<script type="text/javascript">
	var deptInfo = null;
	var parentInfo = ${parentDept};
	
	$(document).ready(function(){
		var isEdit = ( '${department}' != '' );
		
		if( isEdit ) deptInfo =	eval( '(' +'${department}'+ ')' );

		initFormToEdit( isEdit );

		jQuery("#editForm").validate({
            	rules         : {deptName : "required"},
				messages      : {deptName : "必填"},	
			 	submitHandler : function(form) {
			 	    $("#editForm").ajaxForm().ajaxSubmit(function(data){
						parent.dlgCallback( data, isEdit );
						var index = parent.layer.getFrameIndex("dept"); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭  
	       			});
			  	}
         });
	});
	
	function initFormToEdit( isEdit ){
		if( isEdit ){
			
			$("#deptId").val(deptInfo.id);
			$("#deptName").val(deptInfo.name);
			$("#deptContact").val(deptInfo.contact);
			$("#deptMobile").val(deptInfo.mobile);
			var recDate = new Date(deptInfo.recDate);
			$("#deptRecDate").val(recDate.getFullYear() + "-" + (recDate.getMonth()+1) + "-" +recDate.getDate() + " " + recDate.getHours() + ":" + recDate.getMinutes());
			
			$("#editBtn").val("提交修改");
			$("#editForm").attr("action","${contextPath}/admin/department/updateDept");
		}else{

			$("#parentId").val(parentInfo.id);
			$("#parentName").val(parentInfo.name);
			
			$("#editBtn").val("确认保存");
			$("#editForm").attr("action","${contextPath}/admin/department/saveDept");
		}
    }
    
</script>
</head>
<body>
	
 	<form:form  action="" method="post" id="editForm">
		<div style="margin: 0 auto; text-align:center; align:center;" class="${tableclass}">
			<input name="deptId"   id="deptId"  type="hidden"/>
			<input name="parentId" id="parentId" type="hidden"/>
			<table class="${tableclass}" style="width: 100%">
				<tbody>
					<tr>
						<td>部门名称：<font color="ff0000">*</font></td>
						<td><input type="text" name="deptName" id="deptName"/></td>
					</tr>
					<tr>
						<td>部门联系人：</td>
						<td><input type="text" name="deptContact" id="deptContact"/></td>
					</tr>
					<tr>
						<td>部门电话：</td>
						<td><input type="text" name="deptMobile" id="deptMobile" /></td>
					</tr>
					<c:choose>
		            <c:when test="${ not empty department }">
                    	<tr>
                    		<td>创建时间：</td>
							<td><input type="text"  name="deptRecDate" id="deptRecDate" readonly="readonly"/></td>
						</tr>
		            </c:when>
                    <c:otherwise>
                         <tr>
                         	<td>所属部门：</td>
							<td><input type="text" name="parentName" id="parentName" readonly="readonly"/></td>
						</tr>
                    </c:otherwise>
                    </c:choose>
                    
				</tbody>
			</table>
						<input type="submit" id="editBtn" class="${btnClass }" />
		</div>
	</form:form>
</body>

</html>



