/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.integral.page.IntegralAdminPage;
import com.legendshop.model.dto.integral.IntegralRuleDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 积分规则管理控制器
 */
@Controller
@RequestMapping("/admin/integralRule")
public class AdminIntegralReluController extends BaseController {

	@Autowired
	private ConstTableService constTableService;

	/**
	 * 积分规则设置
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/setting")
	public String orderSetting(HttpServletRequest request, HttpServletResponse response) {
		ConstTable constTable = constTableService.getConstTablesBykey("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING");
		IntegralRuleDto releSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			releSetting = JSONUtil.getObject(setting, IntegralRuleDto.class);
		}
		request.setAttribute("releSetting", releSetting);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_RELU_ADD);
	}

	/**
	 * 修改积分规则
	 * 
	 * @param request
	 * @param response
	 * @param integralRuleDto
	 * @return
	 */
	@SystemControllerLog(description="修改积分规则")
	@RequestMapping("/addRule")
	@ResponseBody
	public String addRule(HttpServletRequest request, HttpServletResponse response, IntegralRuleDto integralRuleDto) {
		if (AppUtils.isBlank(integralRuleDto)) {
			throw new NullPointerException("请正确的设置参数！");
		}
		String setting = JSONUtil.getJson(integralRuleDto);
		constTableService.updateConstTableByType("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING", setting);
		return "ok";
	}

}