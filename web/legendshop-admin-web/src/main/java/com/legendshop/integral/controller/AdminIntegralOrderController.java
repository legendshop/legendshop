/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.util.DateUtils;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonRedirectPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.integral.page.IntegralAdminBackPage;
import com.legendshop.integral.page.IntegralAdminPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IntegralOrderStatusEnum;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.util.AppUtils;

/**
 * 积分订单控制器.
 */
@Controller
@RequestMapping("/admin/integralOrder")
public class AdminIntegralOrderController extends BaseController {
	
	private final Logger log = LoggerFactory.getLogger(AdminIntegralOrderController.class);
	
	@Autowired
	private IntegralOrderService integralOrderService;
	
	@Autowired
	private DeliveryTypeService deliveryTypeService;
	
	
	
	/**
	 *我的积分订单列表.
	 *
	 * @param request the request
	 * @param response the response
	 * @param paramDto the param dto
	 * @return the string
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		paramDto.setPageSize(10);
		//处理结束日期
		if(AppUtils.isNotBlank(paramDto.getEndDate())){
			paramDto.setEndDate(DateUtils.getEndMonment(paramDto.getEndDate()));
		}
		PageSupport<IntegralOrderDto> pageSupport=integralOrderService.getIntegralOrderDtos(paramDto);
		pageSupport.getResultList();
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_ORDER_LIST_PAGE);
	}
	
	
	
	/**
	 * 取消积分订单.
	 *
	 * @param request the request
	 * @param response the response
	 * @param sn the sn
	 * @return the string
	 */
	@SystemControllerLog(description="取消积分订单")
	@RequestMapping("/orderCancel/{sn}")
	public @ResponseBody String orderCancel(HttpServletRequest request, HttpServletResponse response,@PathVariable String sn) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String updateUserId = user.getUserId();
		return integralOrderService.orderCancel(sn,null,updateUserId,"后台取消订单,该订单流水号是:"+sn);
	}
	

    /**
     * 删除积分订单.
     *
     * @param request the request
     * @param response the response
     * @param sn the sn
     * @return the string
     */
	@SystemControllerLog(description="删除积分订单")
	@RequestMapping("/orderDel")
	public @ResponseBody String orderDel(HttpServletRequest request, HttpServletResponse response, String sn) {
		return integralOrderService.orderDel(sn);
	}
	
	
	
	/**
	 * 积分订单发货.
	 *
	 * @param request the request
	 * @param response the response
	 * @param orderSn the order sn
	 * @return the string
	 */
	@RequestMapping("/deliverGoods/{orderSn}")
	public String deliverGoods(HttpServletRequest request, HttpServletResponse response,@PathVariable String orderSn ){
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(CommonRedirectPage.LOGIN);
		}
		
		IntegralOrder integralOrder= integralOrderService.getIntegralOrderByOrderSn(orderSn);
		if(AppUtils.isBlank(integralOrder) ){
			return "";
		}
		if(IntegralOrderStatusEnum.SUBMIT.value().intValue()==integralOrder.getOrderStatus().intValue()){
			List<DeliveryType> deliveryTypes=deliveryTypeService.getDeliveryTypeByShopId();
			request.setAttribute("deliveryTypes",deliveryTypes);
			request.setAttribute("integralOrder", integralOrder);
			return PathResolver.getPath(IntegralAdminBackPage.DELIVER_GOODS);
		}
		return "";
	}
	
	/**
	 * 积分订单发货.
	 *
	 * @param request the request
	 * @param response the response
	 * @param orderSn the order sn
	 * @param deliv the deliv
	 * @param dvyFlowId the dvy flow id
	 * @return the string
	 */
	@SystemControllerLog(description="积分订单发货")
	@RequestMapping(value = "/fahuo/{orderSn}",method=RequestMethod.POST)
	public @ResponseBody  String fahuo(HttpServletRequest request, HttpServletResponse response,@PathVariable String orderSn, Long deliv,String dvyFlowId) {
		if (orderSn == null || deliv == null ||dvyFlowId==null ) {
			log.error("visit /fahuo controller but missing parameter");
			return Constants.FAIL;
		}
		 integralOrderService.fahuo(orderSn,deliv,dvyFlowId);
		return Constants.SUCCESS;
	}
	
	/**
	 * 积分订单详情.
	 *
	 * @param request the request
	 * @param response the response
	 * @param orderSn the order sn
	 * @return the string
	 */
	@RequestMapping("/orderDetail/{orderSn}")
	public String orderDetail(HttpServletRequest request, HttpServletResponse response,@PathVariable String orderSn) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(CommonRedirectPage.LOGIN);
		}
		IntegralOrderDto order=integralOrderService.findIntegralOrderDetail(orderSn);
		request.setAttribute("order",order);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_ORDER_DETAIL);
	}
	
}
