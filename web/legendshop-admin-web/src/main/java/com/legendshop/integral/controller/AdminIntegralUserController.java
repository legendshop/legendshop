/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.integral.page.IntegralAdminPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.IntegralLogTypeEnum;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.IntegraLogService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 积分明细和积分充值.
 *
 */
@Controller
@RequestMapping("/admin/userIntegral")
public class AdminIntegralUserController extends BaseController {

	@Autowired
	private IntegraLogService integraLogService;

	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查询积分管理列表.
	 *
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, IntegraLog integraLog) {

		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "add_time" });

		PageSupport<IntegraLog> ps = integraLogService.getIntegraLogListPage(curPageNO, integraLog, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("integraLog", integraLog);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_USER_LIST);
	}

	/**
	 * 用户积分充值.
	 *
	 */
	@RequestMapping("/decrease")
	public String decrease(HttpServletRequest request, HttpServletResponse response, IntegraLog integraLog) {
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_USER_DECREASE);
	}

	/**
	 * 保存积分充值明细.
	 *
	 */
	@SystemControllerLog(description="积分增减")
	@RequestMapping("/decreaseSave")
	public @ResponseBody String decreaseSave(HttpServletRequest request, HttpServletResponse response, IntegraLog integraLog) {
		if (AppUtils.isBlank(integraLog)) {
			return "参数异常";
		}

		if (AppUtils.isBlank(integraLog.getNickName())) {
			return "请输入用户名称";
		}

		UserDetail userdetail = userDetailService.getUserDetailForScoreUser(integraLog.getNickName());
		if (AppUtils.isBlank(userdetail)) {
			return "找不到该用户";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		// 更新用户积分
		integraLog.setAddTime(new Date());
		integraLog.setLogType(IntegralLogTypeEnum.LOG_DEFAULT.value());
		integraLog.setUpdateUserId(userId);
		String result = userDetailService.updateUserScore(userdetail, integraLog);
		if (!Constants.SUCCESS.equals(result)) {
			return "扣除的积分值大于该用户当前积分!";
		}
		return result;
	}

	/**
	 * 查找用户昵称对应的积分数量.
	 *
	 * @param nickName
	 *            用户昵称
	 * @return the map< string, object>
	 */
	@RequestMapping("/findUser")
	public @ResponseBody Map<String, Object> findUser(String nickName) {
		UserDetail userdetail = userDetailService.getUserDetailForScoreUser(nickName);
		/*
		 * UserDetail userdetail=userDetailService.getUserDetailByAll(nickName);
		 */
		Map<String, Object> map = new HashMap<String, Object>();
		if (AppUtils.isBlank(userdetail)) {
			map.put("msg", "对不起，会员信息错误");
			map.put("status", false);
		} else {
			map.put("msg", "符合条件会员信息" + nickName + ",当前积分值为:" + (userdetail.getScore() == null ? 0 : userdetail.getScore()));
			map.put("userId", userdetail.getUserId());
			map.put("status", true);
		}
		return map;
	}

	/**
	 * 会员积分明细.
	 *
	 */
	@RequestMapping("/userIntegralDetail/{userId}")
	public String userIntegralDetail(HttpServletRequest request, HttpServletResponse response, String curPageNO, IntegraLog integraLog,
			@PathVariable String userId) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "add_time" });

		PageSupport<IntegraLog> ps = integraLogService.getIntegraLogListPage(curPageNO, integraLog, userId, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("integraLog", integraLog);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", integraLog.getUserName());
		return PathResolver.getPath(IntegralAdminPage.USER_INTEGRAL_DETAIL);
	}
}