/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum IntegralAdminBackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 保存物流信息 */
	DELIVER_GOODS("/integralOrder/deliverGoods");

	/** The value. */
	private final String value;

	/**
	 * 是芦花一个新的后台页面
	 *
	 * @param value
	 *            the value
	 */
	private IntegralAdminBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("integral", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
