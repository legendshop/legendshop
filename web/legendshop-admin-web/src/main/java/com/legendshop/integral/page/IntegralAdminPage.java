/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 积分管理页面定义
 */
public enum IntegralAdminPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 积分商城 商品类目 */
	INTEGRAL_CATEGORY_LIST("/category/categoryList"),

	/** 积分商品管理 */
	INTEGRAL_PROD_LIST("/integralProd/integralProdList"),

	/** 积分商品管理 */
	INTEGRAL_PROD_LOAD("/integralProd/integralProd"),

	/** 我的积分订单 */
	INTEGRAL_ORDER_LIST_PAGE("/integralOrder/integralOrderList"),

	/** 用户管理 积分管理 */
	INTEGRAL_USER_LIST("/integralUser/integralUserList"),

	/** 积分明细 积分增减 */
	INTEGRAL_USER_DECREASE("/integralUser/integralDecrease"),

	/** 积分规则设置 */
	INTEGRAL_RELU_ADD("/integralRule/integralRule"),

	/** 查看订单详情 */
	INTEGRAL_ORDER_DETAIL("/integralOrder/integralOrderDetail"),

	/** 会员积分明细 */
	USER_INTEGRAL_DETAIL("/integralUser/userIntegralDetail");

	/** The value. */
	private final String value;

	/**
	 * 构造器.
	 *
	 * @param value
	 */
	private IntegralAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("integral", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
