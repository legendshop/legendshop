/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.integral.page.IntegralAdminPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.IntegralProdService;
import com.legendshop.util.AppUtils;

/**
 * 积分商品管理控制器
 *
 */
@Controller
@RequestMapping("/admin/integralProd")
public class AdminIntegralProdController extends BaseController {

	@Autowired
	private IntegralProdService integralProdService;
	
	/**
	 * 批量删除积分商品
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="批量删除积分商品")
	@RequestMapping(value = "/batchDelete/{ids}", method = RequestMethod.DELETE)
	public @ResponseBody String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		try{
			integralProdService.batchDelete(ids);
		}catch(Exception e){
			e.printStackTrace();
			return "批量删除积分商品失败。";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 批量下线积分商品
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="批量下线积分商品")
	@RequestMapping(value = "/batchOffline/{ids}", method = RequestMethod.PUT)
	public @ResponseBody String batchOffline(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		try{
			integralProdService.batchOffline(ids);
		}catch(Exception e){
			e.printStackTrace();
			return "批量删除积分商品失败。";
		}
		return Constants.SUCCESS;
	}
	/**
	 * 查询积分商品.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @param integralProd
	 *            the integral prod
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, IntegralProd integralProd) {

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "viewNum", "saleNum", "prodStock" });

		PageSupport<IntegralProd> ps = integralProdService.getIntegralProdPage(curPageNO, integralProd, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", integralProd);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_PROD_LIST);
	}

	/**
	 * 跳转积分商品管理详情页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		IntegralProd integralProd = integralProdService.getIntegralProd(id);
		request.setAttribute("integralProd", integralProd);
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_PROD_LOAD);
	}

	/**
	 * 加载积分管理页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(IntegralAdminPage.INTEGRAL_PROD_LOAD);
	}

	/**
	 * 保存积分商品
	 * @param request
	 * @param response
	 * @param integralProd
	 * @return
	 */
	@SystemControllerLog(description="保存积分商品")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, IntegralProd integralProd) {
		if (Constants.NO.equals(integralProd.getIsLimit())) {
			integralProd.setLimitNum(null);
		}
		SecurityUserDetail user = UserManager.getUser(request);
		integralProd.setUserName(user.getUsername());
		integralProdService.saveIntegralProd(integralProd,user.getUsername(), user.getUserId(), user.getShopId());
		String result = PathResolver.getRedirectPath("/admin/integralProd/query");
		return result;
	}

	/**
	 * 更新积分商品上下线状态
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新积分商品上下线状态")
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		IntegralProd integralProd = integralProdService.getIntegralProd(id);
		if (AppUtils.isBlank(integralProd)) {
			return "找不到操作对象";
		}
		if (integralProd.getStatus().equals(status)) {
			return "请勿重复操作！";
		}
		// admin
		if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)) {
			integralProd.setStatus(status);
			integralProdService.updateIntegralProdStatus(integralProd);
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除积分商品
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除积分商品")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		IntegralProd integralProd = integralProdService.getIntegralProd(id);
		if (AppUtils.isBlank(integralProd)) {
			return "找不到操作对象";
		}
		integralProdService.deleteIntegralProd(integralProd);
		return Constants.SUCCESS;
	}

}
