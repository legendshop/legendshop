/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum 用户中心页面.
 */
public enum ActivityAdminFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/**错误页面 */
	FAIL("/activityFail"),

	/** 商店活动商品 */
	MARKETING_PRODS("/marketingRuleProds"),

	/** 商品搜索结果页面 */
	GOODS_SEARCH_RESULT("/goodsSearchResult");

	/** The value. */
	private final String value;

	private ActivityAdminFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("activity", path);
	}

	public String getNativeValue() {
		return value;
	}

}
