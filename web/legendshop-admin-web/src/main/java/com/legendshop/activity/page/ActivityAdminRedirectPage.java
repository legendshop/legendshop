/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum 重定向页面.
 */
public enum ActivityAdminRedirectPage implements PageDefinition {

	/** 查找满送活动列表 */
	MARKETING_MANSONG_LIST_PAG("/admin/marketing/mansong/query"),

	/** 查找折扣活动列表 */
	MARKETING_ZHEKOU_LIST_PAG("/admin/marketing/zhekou/query"),

	;
	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的tiles页面.
	 * 
	 * @param value
	 * @param template
	 */
	private ActivityAdminRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * 实例化一个新的tiles页面.
	 * 
	 * @param value
	 *            the value
	 */
	private ActivityAdminRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
