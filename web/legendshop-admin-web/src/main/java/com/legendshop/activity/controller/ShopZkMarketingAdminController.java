/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.activity.page.ActivityAdminFrontPage;
import com.legendshop.activity.page.ActivityAdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;

/**
 * 限时折扣促销
 * 
 */
@Controller
@RequestMapping("/admin/marketing")
public class ShopZkMarketingAdminController extends BaseController {

	@Autowired
	private MarketingService marketingService;

	@Autowired
	private MarketingProdsService marketingProdsService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 格式化时间
	 * 
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 折扣活动
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/zhekou/query")
	public String shop_matketing_zhekou(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityAdminPage.MARKETING_ZHEKOU_LIST_PAG);
	}
	
	/**
	 * 折扣活动列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/zhekou/queryContent")
	public String shop_matketing_zhekou＿content(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "start_time", "end_time" });
		PageSupport<Marketing> ps = marketingService.getShopMatketingZhekou(curPageNO, marketing, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("marketing", marketing);
		String path = PathResolver.getPath(ActivityAdminPage.MARKETING_ZHEKOU_CONTENT_LIST_PAG);
		return path;
	}

	/**
	 * id获取折扣活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/zhekouQueryById/{id}/{shopId}", method = RequestMethod.GET)
	public String zhekouQueryById(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long shopId) throws Exception {
		Marketing marketing = marketingService.getMarketingDetail(shopId, id);
		if (marketing == null) {
			throw new Exception("object is null");
		}
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityAdminPage.MARKETING_ZHEKOU_PAG);
	}

	/**
	 * 参加折扣活动商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param shopId
	 * @param curPage
	 * @return
	 */
	@RequestMapping(value = "/marketingZkRuleProds/{id}/{shopId}", method = RequestMethod.GET)
	public String marketingZkRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long shopId,
			String curPage) {
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingRuleProds(curPage, shopId, id);
		PageSupportHelper.savePage(request, pageSupport);
		String path = PathResolver.getPath(ActivityAdminFrontPage.MARKETING_PRODS);
		return path;
	}

	/**
	 * 添加折扣活动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addZkMarketing", method = RequestMethod.GET)
	public String addShopMarketing(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ActivityAdminPage.MARKETING_ADDZK_PAG);
	}

	/**
	 * 添加促销活动
	 * 
	 * @param request
	 * @param response
	 * @param marketing
	 * @return
	 */
	@RequestMapping(value = "/saveZkMarketing", method = RequestMethod.POST)
	public @ResponseBody String addShopZkMarketing(HttpServletRequest request, HttpServletResponse response, Marketing marketing) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		boolean config = validateMarketing(marketing);
		if (!config) {
			return "填写的数据格式有问题！";
		}
		marketing.setShopId(shopId);
		marketing.setUserId(userId);
		marketing.setType(MarketingTypeEnum.XIANSHI_ZE.value());
		String result = marketingService.saveMarketing(marketing);
		return result;
	}

	/**
	 * 验证活动
	 * 
	 * @param marketing
	 * @return
	 */
	private boolean validateMarketing(Marketing marketing) {
		if (AppUtils.isBlank(marketing.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getIsAllProds())) {
			return false;
		}
		if (marketing.getIsAllProds().intValue() == 1) {
			if (AppUtils.isBlank(marketing.getDiscount())) {
				return false;
			} else if (marketing.getDiscount() < 1 || marketing.getDiscount() > 9.9) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 添加限时折扣促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param curPage
	 * @return
	 */
	@RequestMapping("/addZkMarketingRuleProds/{id}")
	public @ResponseBody String addZkMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, Long prodId,
			Long skuId, Double cash, Double discountPrice) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return "找不到该活动！";
		} else if (marketing.getState().intValue() == 1) {
			return "该活动还在上线，请下线再管理！";
		}
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			return "该活动已过期";
		}
		String result = marketingProdsService.addZkMarketingRuleProds(id, shopId, prodId, skuId, cash, discountPrice);
		if (Constants.SUCCESS.equals(result)) {
			if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
				marketingProdsService.clearGlobalMarketCache(shopId);
			} else {
				marketingProdsService.clearProdMarketCache(shopId, prodId);
			}
		}
		return result;
	}
}
