/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.activity.page.ActivityAdminFrontPage;
import com.legendshop.activity.page.ActivityAdminPage;
import com.legendshop.activity.page.ActivityAdminRedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.model.entity.MarketingProds;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;

/**
 * 营销
 */
@Controller
@RequestMapping("/admin/marketing")
public class ShopMjMarketingAdminController extends BaseController {
	
	private final Logger logger = LoggerFactory.getLogger(ShopMjMarketingAdminController.class);
	
	@Autowired
	private MarketingService marketingService;

	@Autowired
	private MarketingProdsService marketingProdsService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 批量删除促销活动
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @param id
	 * @param shopId
	 * @return
	 */
	@SystemControllerLog(description="批量删除促销活动")
	@RequestMapping(value = "/batchDetele/{marketIds}")
	public @ResponseBody String batchDetele(HttpServletRequest request, HttpServletResponse response,@PathVariable String marketIds ) {
		if(AppUtils.isBlank(marketIds)){
			logger.error("marketIds is null");
			return "参数错误。";
		}
		try{
			int num=marketingService.batchDetele(marketIds);
			logger.debug("delete market with id {}, result {}", marketIds, num);
			return Constants.SUCCESS;
		}catch(Exception e){
			logger.error("",e);
			return "删除失败。";
		}
	}

	/**
	 * 格式化时间
	 * 
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 满送促销
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/mansong/query")
	public String shop_marketing_mansong(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityAdminPage.MARKETING_MANSONG_LIST_PAG);
	}
	
	/**
	 * 满送促销列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/mansong/queryContent")
	public String shop_marketing_mansong_Content(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "start_time", "end_time" });
		PageSupport<Marketing> ps = marketingService.getShopMarketingMansong(curPageNO, marketing, result, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("marketing", marketing);
		String path = PathResolver.getPath(ActivityAdminPage.MARKETING_MANSONG_CONTENT_LIST_PAG);
		return path;
	}

	/**
	 * id搜索满送活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mansongQueryById/{id}/{shopId}", method = RequestMethod.GET)
	public String mansongQueryById(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long shopId)
			throws Exception {
		Marketing marketing = marketingService.getMarketingDetail(shopId, id);
		if (marketing == null) {
			throw new Exception("object is null");
		}
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityAdminPage.MARKETING_MANSONG_PAG);
	}

	/**
	 * 参与促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param shopId
	 * @param curPage
	 * @return
	 */
	@RequestMapping(value = "/marketingRuleProds/{id}/{shopId}", method = RequestMethod.GET)
	public String marketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long shopId,
			String curPage) {
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingRuleProds(curPage, shopId, id);
		PageSupportHelper.savePage(request, pageSupport);
		String path = PathResolver.getPath(ActivityAdminFrontPage.MARKETING_PRODS);
		return path;
	}

	/**
	 * 添加促销活动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/addMsMarketing", method = RequestMethod.GET)
	public String addShopMarketing(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ActivityAdminPage.MARKETING_ADDMS_PAG);
	}

	/**
	 * 保存促销活动
	 * 
	 * @param request
	 * @param response
	 * @param marketing
	 * @return
	 */
	@RequestMapping(value = "/saveMjMarketing", method = RequestMethod.POST)
	public @ResponseBody String saveMjMarketing(HttpServletRequest request, HttpServletResponse response, Marketing marketing) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		boolean config = validateMarketing(marketing);
		if (!config) {
			return "填写的数据格式有问题！";
		}
		marketing.setShopId(shopId);
		marketing.setUserId(userId);
		String result = marketingService.saveMarketing(marketing);
		return result;

	}

	/**
	 * 验证促销
	 * 
	 * @param marketing
	 * @return
	 */
	private boolean validateMarketing(Marketing marketing) {
		if (AppUtils.isBlank(marketing.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getIsAllProds())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getType())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getManjian_rule()) && AppUtils.isBlank(marketing.getManze_rule())) {
			return false;
		}
		return true;
	}

	/**
	 * 删除促销活动
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @param id
	 * @param shopId
	 * @return
	 */
	@SystemControllerLog(description="删除促销活动")
	@RequestMapping(value = "/deleteShopMarketing/{type}/{id}/{shopId}")
	public String deleteShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable int type, @PathVariable Long id,
			@PathVariable Long shopId,@RequestParam Integer state) {
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityAdminFrontPage.FAIL);
		}

		if (marketing.getState().intValue() == 1) {
			request.setAttribute("messgae", "该活动在进行,不能删除");
			return PathResolver.getPath(ActivityAdminFrontPage.FAIL);
		}
		marketingService.deleteMarketing(marketing);
		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		String path = null;
		if (type == 1 || type == 0) {
			path =  PathResolver.getPath(ActivityAdminRedirectPage.MARKETING_MANSONG_LIST_PAG) ;
			if(AppUtils.isNotBlank(state)){
				path = path + "?state="+state;
			}
			
		} else if (type == 2) {
			path = PathResolver.getPath(ActivityAdminRedirectPage.MARKETING_ZHEKOU_LIST_PAG) ;
			if(AppUtils.isNotBlank(state)){
				path = path + "?state="+state;
			}
		}
		return path;
	}

	/**
	 * 参与营销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/marketingProds/{id}")
	public String marketingProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String name, String curPage, String type) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingProds(curPage, shopId, name);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("type", type);
		request.setAttribute("marketId", id);
		return PathResolver.getPath(ActivityAdminFrontPage.GOODS_SEARCH_RESULT);
	}

	/**
	 * 添加促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param curPage
	 * @re turn
	 */
	@RequestMapping(value = "/addMarketingRuleProds/{id}", method = RequestMethod.POST)
	public @ResponseBody String addMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, Long prodId, Long skuId,
			Double cash) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return "找不到该活动！";
		} /*
			 * else if(marketing.getState().intValue()==1){ return
			 * "该活动还在上线，请下线再管理！"; }
			 */
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			request.setAttribute("messgae", "该活动已过期！");
			return PathResolver.getPath(ActivityAdminFrontPage.FAIL);
		}
		String result = marketingProdsService.addMarketingRuleProds(id, marketing.getType(), shopId, prodId, skuId, cash);
		if (Constants.SUCCESS.equals(result) && marketing.getState().intValue() == 1) {
			marketingProdsService.clearProdMarketCache(shopId, prodId);
		}
		return result;
	}

	/**
	 * 删除添加的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param marketingId
	 * @return
	 */

	@RequestMapping("/deleteMarketingRuleProds/{marketingId}/{id}")
	public @ResponseBody String deleteMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
			@PathVariable Long marketingId) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		MarketingProds prods = marketingProdsService.getMarketingProds(id);
		if (prods == null) {
			return "找不到该信息";
		} else if (!prods.getShopId().equals(shopId)) {
			return "该信息没有权限删除！";
		}
		marketingProdsService.deleteMarketingProds(marketingId, shopId,prods);
		/*Long count = marketingService.isExistProds(marketingId, shopId); // 是否添加了商品信息
		if (count == 0) { // 该活动移除所有的商品信息后,会自动下线
			Marketing marketing = marketingService.getMarketing(marketingId);
			if (marketing.getState().intValue() == 1) {
				marketing.setState(0);
				marketingService.updateMarketing(marketing);
			}
		}*/
		marketingProdsService.clearProdMarketCache(shopId, prods.getProdId());
		return Constants.SUCCESS;
	}

	/**
	 * 上线促销活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="上线促销活动")
	@RequestMapping("/onlineShopMarketing/{type}/{id}")
	public @ResponseBody String onlineShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable int type, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return "找不到该活动！";
		}
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			return "该活动已过期！";
		}
		if (marketing.getIsAllProds().intValue() == 0) { // 部分商品
			Long count = marketingService.isExistProds(marketing.getId(), marketing.getShopId()); // 是否添加了商品信息
			if (count == 0) {
				return "请设置活动商品！";
			}
		}
		if (marketing.getState().intValue() == 0) {
			marketing.setState(1);
			marketingService.updateMarketing(marketing);
		}

		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		return Constants.SUCCESS;
	}

	/**
	 * 下线促销活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="下线促销活动")
	@RequestMapping("/offlineShopMarketing/{type}/{id}")
	public String offlineShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable int type, @RequestParam Integer state) {
		// Long shopId=UserManager.getShopId(request.getSession());
		Marketing marketing = marketingService.getMarketing(id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
		}
		if (marketing.getState().intValue() == 1) {
			marketing.setState(0);
			marketingService.updateMarketing(marketing);
		}
		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(marketing.getShopId());
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		String path = null;
		if (type == 1 || type == 0) {
			path =  PathResolver.getPath(ActivityAdminRedirectPage.MARKETING_MANSONG_LIST_PAG) ;
			if(AppUtils.isNotBlank(state)){
				path = path + "?state="+state;
			}
			
		} else if (type == 2) {
			path = PathResolver.getPath(ActivityAdminRedirectPage.MARKETING_ZHEKOU_LIST_PAG) ;
			if(AppUtils.isNotBlank(state)){
				path = path + "?state="+state;
			}
		}
		return path;
	}
}
