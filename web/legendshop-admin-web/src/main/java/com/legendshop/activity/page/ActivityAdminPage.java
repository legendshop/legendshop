/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum 活动管理页面.
 */
public enum ActivityAdminPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 促销活动满减促销列表页 */
	MARKETING_MANSONG_LIST_PAG("/mansong/mansongList"),
	
	/** 促销活动满减促销列 */
	MARKETING_MANSONG_CONTENT_LIST_PAG("/mansong/mansongContentList"),

	/** 促销活动满减促销详情页 */
	MARKETING_MANSONG_PAG("/mansong/mansong"),

	/** 促销活动折扣促销列表页 */
	MARKETING_ZHEKOU_LIST_PAG("/zhekou/zhekouList"),
	
	/** 促销活动折扣促销列表 */
	MARKETING_ZHEKOU_CONTENT_LIST_PAG("/zhekou/zhekouContentList"), 

	/** 促销活动折扣促销列表页 */
	MARKETING_ZHEKOU_PAG("/zhekou/zhekou"),

	/** 添加满送活动 */
	MARKETING_ADDMS_PAG("/mansong/mansongAdd"),

	/** 添加折扣活动 */
	MARKETING_ADDZK_PAG("/zhekou/zhekouAdd"), ;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的tiles页面.
	 * 
	 * @param value
	 * @param template
	 */
	private ActivityAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("activity", path);
	}

	public String getNativeValue() {
		return value;
	}

}
