/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * CouponAdminPage
 */
public enum CouponAdminPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 优惠券编辑 */
	COUPONS_EDIT_PAGE("/coupon"),
	
	/** 红包编辑 */
	RED_PACKET("/redPacket"),
	
	/** 礼券 */
	COUPONS_LIST("/couponList"),
	
	/** 礼券列表 */
	COUPONS_CONTENT_LIST("/couponContentList"),
	
	/** 礼券使用情况 */
	USE_CPNS_VIEW("/useCouponView"),
	
	/** 平台红包 */
	PLATFORM_LIST("/platformList"),
	
	/** 赠送礼券列表 */
	USERCOUPONS_LIST_PAGE("/userCouponList"), 
	
	/** 下载excel文件 */
	UPLOADUI("/uploadImport");

	/** The value. */
	private final String value;

	private CouponAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("coupon", path);
	}

	public String getNativeValue() {
		return value;
	}

}
