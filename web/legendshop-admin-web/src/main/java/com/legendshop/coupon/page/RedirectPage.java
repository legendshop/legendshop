/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 管理员的页面定义.
 */
public enum RedirectPage implements PageDefinition {
	
	/** 平台红包 */
	ADMIN_COUPON_PLATFORM("/admin/coupon/platform"),
	
	/** 优惠劵管理 */
	COUPONS_LIST_QUERY("/admin/coupon/query");

	private final String value;

	private RedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * 初始化一个新页面.
	 * 
	 * @param value
	 */
	private RedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
