/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.POIReadeExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.coupon.page.CouponAdminPage;
import com.legendshop.coupon.page.CouponBackPage;
import com.legendshop.coupon.page.RedirectPage;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.CouponShopService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 礼券管理控制器
 * 
 */
@Controller
@RequestMapping("/admin/coupon")
public class AdminCouponController extends BaseController{

	@Autowired
	private CouponService couponService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserCouponService userCouponService;

	@Autowired
	private CouponProdService couponProdService;

	@Autowired
	private CouponShopService couponShopService;

	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	/**
	 * 批量下线礼券
	 */
	@SystemControllerLog(description="批量下线礼券")
	@RequestMapping(value = "/batchOffline/{platfromIds}")
	public @ResponseBody String batchOffline(HttpServletRequest request, HttpServletResponse response, @PathVariable String platfromIds) {
		if(AppUtils.isBlank(platfromIds)){
			return "参数错误";
		}
		try{
			couponService.batchOffline(platfromIds);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return "批量下线出错。";
		}
	}

	/**
	 * 批量删除礼券
	 */
	@SystemControllerLog(description="批量删除礼券")
	@RequestMapping(value = "/batchDelete/{platfromIds}")
	public @ResponseBody String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String platfromIds) {
		if(AppUtils.isBlank(platfromIds)){
			return "参数错误";
		}
		try{
			 couponService.batchDelete(platfromIds);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	/**
	 * 进入查看优惠券详情页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/watchCoupon/{id}")
	public String watchCoupon(String curPageNO, HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);
		// 查看优惠券下的所有详细优惠券
		if (AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)) {
			curPageNO = "1";
		}
		PageSupport<UserCoupon> ps = userCouponService.getUserCouponList(curPageNO, id);
		PageSupportHelper.savePage(request, ps);

		// 如果是商品券,取出该商品券中所有的商品名称
		if (coupon.getCouponType().equals("product")) {
			List<CouponProd> couponProdList = couponProdService.getCouponProdByCouponId(id);
			List<Product> productList = new ArrayList<Product>();
			for (int i = 0; i < couponProdList.size(); i++) {
				CouponProd couponProd = couponProdList.get(i);
				Product product = productService.getProductById(couponProd.getProdId());
				productList.add(product);
			}
			request.setAttribute("productList", productList);
		}

		request.setAttribute("coupon", coupon);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("domainName", propertiesUtil.getPcDomainName());
		return PathResolver.getPath(CouponAdminPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 拿到prodId对应的商品
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/getProd/{prodId}")
	public String groupProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		ProductDetail productDetail = productService.getProdDetail(prodId);
		request.setAttribute("productDetail", productDetail);
		String result = PathResolver.getPath(CouponBackPage.GET_PROD);
		return result;
	}

	/**
	 * 挑选商品弹框
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadSelectProd")
	public String loadSelectProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		PageSupport<Product> ps = productService.getLoadSelectProd(curPageNO, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(CouponBackPage.LOAD_SELECT_PROD);
		return result;
	}

	/**
	 * 挑选店铺弹框:获取除了 参与了没有过期红包 的所有店铺
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopName
	 * @return
	 */
	@RequestMapping("/loadSelectShop")
	public String loadSelectShop(HttpServletRequest request, HttpServletResponse response, String curPageNO, String shopName) {
		PageSupport<ShopDetail> ps = shopDetailService.getloadSelectShop(curPageNO, shopName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopName", shopName);
		return PathResolver.getPath(CouponBackPage.LOAD_SELECT_SHOP);
	}

	/**
	 * 进入查看红包详情页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/watchRedPacket/{id}")
	public String watchRedPacket(String curPageNO, HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);
		// 如果有是品类券，即类目id不为空
		if (AppUtils.isNotBlank(coupon.getCategoryId())) {
			Category category = categoryService.getCategory(coupon.getCategoryId());
			request.setAttribute("categoryName", category.getName());
		}
		// 查看红包下的所有详细红包
		if (AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)) {
			curPageNO = "1";
		}

		PageSupport<UserCoupon> ps = userCouponService.getWatchRedPacket(id, curPageNO);
		PageSupportHelper.savePage(request, ps);

		// 如果是店铺红包,取出该券中所有的店铺名称
		if (CouponTypeEnum.SHOP.value().equals(coupon.getCouponType())) {
			List<String> shopNames = couponShopService.getShopNamesByCouponId(id);
			request.setAttribute("shopNames", shopNames);
		}

		request.setAttribute("coupon", coupon);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("domainName", propertiesUtil.getPcDomainName());
		return PathResolver.getPath(CouponAdminPage.RED_PACKET);
	}

	/**
	 * 添加礼券
	 */
	@SystemControllerLog(description="添加礼券")
	@RequestMapping(value = "/saveCoupon")
	public String saveCoupon(HttpServletRequest request, HttpServletResponse response, Coupon coupon) {
		SecurityUserDetail user = UserManager.getUser(request);
		coupon.setCouponProvider(CouponProviderEnum.PLATFORM.value());
		coupon.setIsDsignatedUser(0);
		MultipartFile rightFile = coupon.getFile();// 取得上传的文件	
		String fileName = null;
		if (!AppUtils.isNotBlank(coupon.getCouponId())) {
			if ((rightFile != null) && (rightFile.getSize() > 0)) {
	    		fileName = attachmentManager.upload(rightFile);
	    		coupon.setCouponPic(fileName);
			}
		}
		couponService.saveCouponByCoupon(coupon, user.getUsername(), user.getUserId(), user.getShopId(),fileName,rightFile);

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.ADMIN_COUPON_PLATFORM);
	}

	/**
	 * 进入添加红包页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/loadRedPacket")
	public String loadRedPacket(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(CouponAdminPage.RED_PACKET);
	}

	/**
	 * 后台优惠券管理
	 */
	
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Coupon coupon) {
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(CouponAdminPage.COUPONS_LIST);
	}
	
	/**
	 * 后台优惠券管理列表
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Coupon coupon) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "start_date", "end_date" });
		PageSupport<Coupon> ps = couponService.queryCoupon(curPageNO, coupon, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(CouponAdminPage.COUPONS_CONTENT_LIST);
	}

	@RequestMapping("/usecoupon_view/{couponId}")
	public String usecoupon_view(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId, String curPageNO, String userName) {

		if (AppUtils.isBlank(couponId)) {
			return "";
		}

		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();

		List<Object> objects = new ArrayList<Object>();

		sql.append(
				"SELECT DISTINCT  uc.user_name as userName,uc.coupon_sn as couponSn,uc.get_time as getTime ,uc.get_sources as getSources,uc.use_time as useTime ,uc.order_number as orderNumber  ,uc.order_price as orderPrice,uc.use_status as useStatus,uc.user_id as userId");
		sql.append(" FROM ls_user_coupon  uc  WHERE uc.coupon_id= ? ");

		sqlCount.append("SELECT DISTINCT COUNT(*)  FROM ls_user_coupon  uc WHERE uc.coupon_id= ?");
		objects.add(couponId);

		if (AppUtils.isNotBlank(userName)) {
			sql.append(" and  uc.user_name like ?");
			sqlCount.append(" and  uc.user_name like ?");
			objects.add(userName + "%");
		}

		String sqlString = sortSQL(sql, request);

		PageSupport<UserCoupon> ps = couponService.queryUsecouponView(curPageNO, sqlString, sqlCount, objects);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("couponId", couponId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(CouponAdminPage.USE_CPNS_VIEW);

	}

	/**
	 * 平台红包列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/platform")
	public String platform(HttpServletRequest request, HttpServletResponse response, String curPageNO, Coupon coupon) {
		PageSupport<Coupon> ps = couponService.getPlatform(curPageNO, coupon);
		PageSupportHelper.savePage(request, ps);
		String result=PathResolver.getPath(CouponAdminPage.PLATFORM_LIST);
		return result;
	}

	private String sortSQL(StringBuilder sql, HttpServletRequest request) {
		String sortName = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_SORT)));
		if (AppUtils.isNotBlank(sortName)) {
			sql.append(" order by");
			String sortOrder = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_ORDER)));
			if ("1".equals(sortOrder)) {
				sql.append(sortName).append(" desc").append(",");
			} else {
				sql.append(sortName).append(" asc").append(",");
			}
			if (sql.toString().endsWith(",")) {
				String stream = sideTrim(sql.toString(), ",");
				sql = null;
				sql = new StringBuilder();
				sql.append(stream);
			}
			return sql.toString();
		} else {
			sql.append(" order by uc.get_time desc,use_time desc");
			return sql.toString();
		}
	}

	/**
	 * 去掉指定字符串的开头和结尾的指定字符
	 *
	 * @param stream
	 *            要处理的字符串
	 * @param trimstr
	 *            要去掉的字符串
	 * @return 处理后的字符串
	 */

	public static String sideTrim(String stream, String trimstr) {
		// null或者空字符串的时候不处理
		if (stream == null || stream.length() == 0 || trimstr == null || trimstr.length() == 0) {
			return stream;
		}
		stream = stream.trim();
		// 结束位置
		int epos = 0;

		// 正规表达式
		String regpattern = "[" + trimstr + "]*+";
		Pattern pattern = Pattern.compile(regpattern, Pattern.CASE_INSENSITIVE);

		// 去掉结尾的指定字符
		StringBuffer buffer = new StringBuffer(stream).reverse();
		Matcher matcher = pattern.matcher(buffer);
		if (matcher.lookingAt()) {
			epos = matcher.end();
			stream = new StringBuffer(buffer.substring(epos)).reverse().toString();
		}

		// 返回处理后的字符串
		return stream;
	}

	/**
	 * 删除礼券
	 */
	@SystemControllerLog(description="删除礼券")
	@RequestMapping(value = "/delete/{id}")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		String result = couponService.deleteCoupon(id, null,"admin");
		return result;
	}

	/**
	 * 查看优惠卷详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(CouponAdminPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 加载优惠卷编辑页面
	 */
	
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(CouponAdminPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 更新
	 */
	
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// coupon.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(RedirectPage.COUPONS_LIST_QUERY);
	}

	/**
	 * 更新礼券状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新礼券状态")
	@RequestMapping(value = "/updatestatus/{id}/{status}")
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		if (AppUtils.isBlank(id)) {
			return -1;
		}
		Coupon coupon = couponService.getCoupon(id);
		if (coupon == null) {
			return -1;
		}
		coupon.setStatus(status);
		couponService.updateCoupon(coupon);
		return coupon.getStatus();
	}

	/**
	 * 发放优惠卷
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "issueCoupon/{id}")
	public String issueCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		request.setAttribute("couponId", id);
		return PathResolver.getPath(CouponBackPage.ISSUE_COUPON);
	}

	/**
	 * 保存
	 */
	
	public String save(HttpServletRequest request, HttpServletResponse response, Coupon entity) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 下载模板
	 * 
	 * @param request
	 * @param response
	 * @param data
	 * @return
	 */
	@RequestMapping("/uploadUI")
	public String uploadUI(HttpServletRequest request, HttpServletResponse response, Long data) {
		request.setAttribute("data", data);
		return PathResolver.getPath(CouponAdminPage.UPLOADUI);
	}

	/**
	 * 发送优惠卷
	 * 
	 * @param request
	 * @param response
	 * @param csvFile
	 * @param couponId
	 * @return
	 */
	@RequestMapping(value = "/sendCoupon", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> sendCoupon(HttpServletRequest request, HttpServletResponse response, MultipartFile csvFile, Long couponId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("b", "N");
		try {
			if (AppUtils.isNotBlank(csvFile) || AppUtils.isNotBlank(couponId)) {
				POIReadeExcelUtil readeExcelUtil = new POIReadeExcelUtil();
				List<String> result = readeExcelUtil.batch(csvFile.getInputStream()).getResult();
				if (AppUtils.isBlank(result)) {
					map.put("msg", "您要发放的用户为空");
					return map;
				}
				List<UserCoupon> lists = this.userCouponService.queryUnbindCoupon(couponId);
				if (result.size() > lists.size()) {// 判断红包的数量是否足够
					map.put("count", lists.size());
					map.put("msg", "红包数量不足");
					return map;
				}
				Coupon coupon = this.couponService.getCoupon(couponId);
				Integer successCount = this.couponService.updateCoupon(coupon,result,couponId,result,lists);
				
				map.put("b", "Y");
				map.put("successCount", successCount);
				return map;
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "请按照模板填写");
			return map;
		}
		return map;
	}
	
}
