/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum BackPage.
 */
public enum CouponBackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 优惠卷编辑 */
	USERCOUPONS_LIST_CENTTENT("/userCouponContent"), 
	
	/** 发放优惠卷 */
	ISSUE_COUPON("/issueCoupon"),
	
	/** 挑选商品弹框 */
	LOAD_SELECT_PROD("/loadSelectProd"),
	
	/** 挑选店铺弹框 */
	LOAD_SELECT_SHOP("/loadSelectShop"),
	
	/** 商品信息 */
	GET_PROD("/getProd")
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value the value
	 */
	private CouponBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("coupon", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
