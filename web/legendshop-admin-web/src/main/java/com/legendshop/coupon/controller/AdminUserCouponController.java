/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.coupon.page.CouponAdminPage;
import com.legendshop.coupon.page.CouponBackPage;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SendCouponTypeEnum;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.vo.UserCouponArgs;
import com.legendshop.model.vo.UserCouponVo;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * The Class UserCouponAdminController.
 */
@Controller
@RequestMapping("/admin/userCoupon")
public class AdminUserCouponController extends BaseController{

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminUserCouponController.class);

	@Autowired
	private UserCouponService userCouponService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 初始化binder.
	 *
	 * @param binder
	 * @throws Exception
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * Query.
	 *
	 * @param request
	 * @param response
	 * @param args
	 * @return the string
	 */
	@RequestMapping(value = "/sendquery", method = RequestMethod.POST)
	public String query(HttpServletRequest request, HttpServletResponse response, UserCouponArgs args) {
		PageSupport<UserCouponVo> ps = buildSelectSqlQuery(args);
		request.setAttribute("arg", args);
		request.setAttribute("couponId", args.getCouponId());
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(CouponBackPage.USERCOUPONS_LIST_CENTTENT);
	}

	/**
	 * Builds the select sql query.
	 *
	 * @param args
	 *            the args
	 * @return the page support< user coupon vo>
	 */
	private PageSupport<UserCouponVo> buildSelectSqlQuery(UserCouponArgs args) {
		if (AppUtils.isBlank(args.getCouponId())) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();

		sql.append(
				"SELECT  u.user_id AS userId, u.user_name AS userName, u.nick_name AS nickName, u.user_mail AS userMail, u.user_mobile AS userMobile,u.user_regtime AS userRegtime,");
		sql.append("(SELECT COUNT(*) AS lognumber FROM ls_login_hist WHERE ls_login_hist.user_name= u.user_name ) AS logNumber ,");
		sql.append("(SELECT SUM(actual_total) FROM ls_sub WHERE ls_sub.user_id = u.user_id) AS totalAmount,");
		sql.append("(SELECT COUNT(*) FROM ls_sub WHERE ls_sub.user_id = u.user_id) AS orderNumber ");
		sql.append(" FROM ls_usr_detail u,ls_user ur WHERE ");
		sql.append(" ur.id=u.user_id and ur.enabled =1 ");

		sqlCount.append("SELECT count(*) FROM ls_usr_detail u,ls_user ur  WHERE ur.id=u.user_id and ur.enabled =1");

		List<Object> objects = new ArrayList<Object>();

		int filMethod = args.getFilMethod();
		if (1 == filMethod) { // 活跃度筛选
			if (AppUtils.isNotBlank(args.getLogNumberFilter()) && AppUtils.isNotBlank(args.getLogNumber())) {
				Integer vInteger = Integer.parseInt(args.getLogNumberFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT COUNT(*)  FROM ls_login_hist WHERE ls_login_hist.user_name= u.user_name ) ");
					sql.append(str).append(" ? ");

					sqlCount.append(" and (SELECT COUNT(*)  FROM ls_login_hist WHERE ls_login_hist.user_name= u.user_name ) ");
					sqlCount.append(str).append(" ? ");

					objects.add(args.getLogNumber());
				}
			}
			if (AppUtils.isNotBlank(args.getTotalAmountFilter()) && AppUtils.isNotBlank(args.getTotalAmount())) {
				Integer vInteger = Integer.parseInt(args.getTotalAmountFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT SUM(actual_total) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sql.append(str).append(" ? ");

					sqlCount.append(" and (SELECT SUM(actual_total) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sqlCount.append(str).append(" ? ");

					objects.add(args.getTotalAmount());
				}
			}
			if (AppUtils.isNotBlank(args.getOrderNumberFilter()) && AppUtils.isNotBlank(args.getOrderNumber())) {
				Integer vInteger = Integer.parseInt(args.getOrderNumberFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT COUNT(*) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sql.append(str).append(" ? ");

					sqlCount.append(" and (SELECT COUNT(*) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sqlCount.append(str).append(" ? ");
					objects.add(args.getOrderNumber());
				}
			}

		} else if (2 == filMethod) {
			String startDate = args.getRegtime_start();
			String endDate = args.getRegtime_end();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate = null;
			Date toDate = null;
			if (AppUtils.isNotBlank(startDate)) {
				try {
					sql.append(" and u.user_regtime >= ? ");
					sqlCount.append(" and u.user_regtime >= ?  ");
					fromDate = dateFormat.parse(startDate);
					objects.add(fromDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if (AppUtils.isNotBlank(endDate)) {
				try {
					sql.append(" and u.user_regtime <= ? ");
					sqlCount.append(" and u.user_regtime <= ?  ");
					toDate = dateFormat.parse(endDate);
					objects.add(toDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} else if (3 == filMethod) {
			// 普通方式查询
			String attribute = args.getAttribute();
			String name = args.getName();
			if (AppUtils.isNotBlank(attribute) && AppUtils.isNotBlank(name)) {
				if ("0".equals(attribute)) {
					sql.append(" and  u.user_name like ? ");
					sqlCount.append(" and  u.user_name like ? ");
				} else if ("1".equals(attribute)) {
					sql.append(" and  u.nick_name like ? ");
					sqlCount.append(" and  u.nick_name like ? ");
				} else if ("2".equals(attribute)) {
					sql.append(" and  u.user_mail like ? ");
					sqlCount.append(" and  u.user_mail like ? ");
				} else if ("3".equals(attribute)) {
					sql.append(" and  u.user_mobile like ? ");
					sqlCount.append(" and  u.user_mobile like ? ");
				}
				objects.add(name + "%");
			}
		}
		Long couponId = args.getCouponId();
		sql.append(" AND u.user_id NOT IN (SELECT bzc.user_id FROM ls_user_coupon bzc  WHERE  bzc.coupon_id= ?) ");
		sqlCount.append(" AND u.user_id NOT IN (SELECT bzc.user_id FROM ls_user_coupon bzc  WHERE  bzc.coupon_id= ?) ");
		objects.add(couponId);
		sql.append(" order by totalAmount desc,orderNumber desc,u.user_regtime desc ");
		PageSupport<UserCouponVo> ps = userCouponService.getUserCoupon(args.getCurPageNO(), sql, sqlCount, objects);

		return ps;
	}

	/**
	 * Builds the sql query.
	 *
	 * @param args
	 *            the args
	 * @return the list< string>
	 */
	private List<String> buildSqlQuery(UserCouponArgs args) {
		if (AppUtils.isBlank(args.getCouponId())) {
			return null;
		}
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT  u.user_id AS userId ");
		sql.append(" FROM ls_usr_detail u ,ls_user ur WHERE ");
		sql.append(" ur.id=u.user_id and ur.enabled =1 ");

		List<Object> objects = new ArrayList<Object>();

		int filMethod = args.getFilMethod();
		if (1 == filMethod) { // 活跃度筛选
			if (AppUtils.isNotBlank(args.getLogNumberFilter()) && AppUtils.isNotBlank(args.getLogNumber())) {
				Integer vInteger = Integer.parseInt(args.getLogNumberFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT COUNT(*)  FROM ls_login_hist WHERE ls_login_hist.user_name= u.user_name ) ");
					sql.append(str).append(" ? ");
					objects.add(args.getLogNumber());
				}
			}
			if (AppUtils.isNotBlank(args.getTotalAmountFilter()) && AppUtils.isNotBlank(args.getTotalAmount())) {
				Integer vInteger = Integer.parseInt(args.getTotalAmountFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT SUM(actual_total) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sql.append(str).append(" ? ");
					objects.add(args.getTotalAmount());
				}
			}
			if (AppUtils.isNotBlank(args.getOrderNumberFilter()) && AppUtils.isNotBlank(args.getOrderNumber())) {
				Integer vInteger = Integer.parseInt(args.getOrderNumberFilter());
				String str = "";
				switch (vInteger) {
				case 1:
					str = ">";
					break;
				case 2:
					str = "<";
					break;
				case 3:
					str = "=";
					break;
				}
				if (AppUtils.isNotBlank(str)) {
					sql.append(" and (SELECT COUNT(*) FROM ls_sub WHERE ls_sub.user_id = u.user_id) ");
					sql.append(str).append(" ? ");
					objects.add(args.getOrderNumber());
				}
			}

		} else if (2 == filMethod) {
			String startDate = args.getRegtime_start();
			String endDate = args.getRegtime_end();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate = null;
			Date toDate = null;
			if (AppUtils.isNotBlank(startDate)) {
				try {
					sql.append(" and u.user_regtime >= ? ");
					fromDate = dateFormat.parse(startDate);
					objects.add(fromDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if (AppUtils.isNotBlank(endDate)) {
				try {
					sql.append(" and u.user_regtime <= ? ");
					toDate = dateFormat.parse(endDate);
					objects.add(toDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} else if (3 == filMethod) {
			// 普通方式查询
			String attribute = args.getAttribute();
			String name = args.getName();
			if (AppUtils.isNotBlank(attribute) && AppUtils.isNotBlank(name)) {
				if ("0".equals(attribute)) {
					sql.append(" and  u.user_name like ? ");
				} else if ("1".equals(attribute)) {
					sql.append(" and  u.nick_name like ? ");
				} else if ("2".equals(attribute)) {
					sql.append(" and  u.user_mail like ? ");
				} else if ("3".equals(attribute)) {
					sql.append(" and  u.user_mobile like ? ");
				}
				objects.add(name + "%");
			}
		}
		Long couponId = args.getCouponId();
		sql.append(" AND u.user_id NOT IN (SELECT bzc.user_id FROM ls_user_coupon bzc  WHERE  bzc.coupon_id= ?) ");
		objects.add(couponId);

		List<String> cnpsVos = userCouponService.getSendUsers(args.getCurPageNO(), sql, objects);
		return cnpsVos;
	}

	/**
	 * Send user cnps.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param userids
	 *            the userids
	 * @param couponId
	 *            the coupon id
	 * @return the string
	 */
	@RequestMapping(value = "/sendUserCnps", method = RequestMethod.POST)
	public @ResponseBody String sendUserCnps(HttpServletRequest request, HttpServletResponse response, String userids, Long couponId) {

		String reslut = userCouponService.sendUserCnps(userids, couponId);
		return reslut;

	}

	/**
	 * Batch send user cnps.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param args
	 *            the args
	 * @return the string
	 */
	@RequestMapping(value = "/batchSendUserCnps", method = RequestMethod.POST)
	public @ResponseBody String batchSendUserCnps(HttpServletRequest request, HttpServletResponse response, UserCouponArgs args) {
		Long couponId = args.getCouponId();
		if (couponId == null) {
			return "couponId不能为空";
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null) {
			return "找不到[" + couponId + "]礼券";
		} else if (!coupon.getSendType().equals(SendCouponTypeEnum.ONLINE.value())) { // 不是该类型
			return "[" + couponId + "]礼券类型不正确!";
		} else if (coupon.getStatus() == Constants.OFFLINE.intValue()) {
			return "[" + couponId + "]礼券已冻结!";
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date) {
			return "[" + couponId + "]礼券已过期!";
		} else if (coupon.getCouponNumber() == 0) {
			return "[" + couponId + "]礼券初始化数量为0 !";
		}

		/*
		 * SimpleSqlQuery sqlQuery = buildSqlQuery(args); if
		 * (AppUtils.isBlank(sqlQuery)) { return Constants.FAIL; }
		 * 
		 * String sql=sqlQuery.getQueryString(); Object[]
		 * param=sqlQuery.getParam();
		 */

		List<String> cnpsVos = buildSqlQuery(args);

		if (AppUtils.isBlank(cnpsVos)) {
			return "没有符合条件的用户信息";
		} else if (cnpsVos.size() > coupon.getCouponNumber()) {
			return "赠送用户数量超出礼券初始化数量!";
		}

		/**
		 * 这里需要做批量处理
		 */
		batckSendCnps(couponId, coupon, cnpsVos);
		return Constants.SUCCESS;
	}

	/**
	 * Batck send cnps.
	 *
	 * @param couponId
	 *            the coupon id
	 * @param coupon
	 *            the coupon
	 * @param cnpsVos
	 *            the cnps vos
	 */
	private void batckSendCnps(Long couponId, Coupon coupon, List<String> cnpsVos) {
		int total = cnpsVos.size();

		int init = 100;// 每隔100条循环一次

		int runnum = total % init == 0 ? (total / init) : (total / init) + 1;

		int cycelTotal = total;
		LOGGER.info("线下礼券生成-循环保存的次数：" + runnum);

		String prefix = coupon.getCouponNumPrefix();
		long cnps_number = 0;
		for (int r = 0; r < runnum; r++) {
			List<String> removeUserids = new ArrayList<String>();
			if (cycelTotal < init) { // 9
				init = cycelTotal;
			}
			List<UserCoupon> userCoupon = new ArrayList<UserCoupon>();
			for (int j = 0; j < init; j++) {
				if (cnpsVos.get(j) == null) {
					continue;
				}
				String uid = cnpsVos.get(j);
				removeUserids.add(uid);
				User user = userDetailService.getUser(uid);
				if (user == null) {
					continue;
				}
				UserCoupon cou = new UserCoupon();
				cou.setUserId(uid);
				cou.setUserName(user.getUserName());
				cou.setCouponId(couponId);
				cou.setCouponName(coupon.getCouponName());
				cou.setUseStatus(1);
				// 生产劵号
				cou.setCouponSn(CommonServiceUtil.getRandomCouponNumberGenerator());
				cou.setGetTime(new Date());
				cou.setGetSources("平台赠送");
				userCoupon.add(cou);
				cnps_number++;
			}
			System.out.println("批量保存赠送礼券数据：" + JSONUtil.getJson(removeUserids));
			// 接下来写保存数据库方法
			if (AppUtils.isNotBlank(userCoupon)) {
				boolean config = userCouponService.saveUserCoupon(userCoupon, userCoupon.size(), couponId);
				cnpsVos.removeAll(removeUserids);// 移出已经保存过的数据
			}
			userCoupon = null;
			cycelTotal -= init;
		}
		LOGGER.info("线下礼券生成count：" + cnps_number);
	}

	/**
	 * Send for off line.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param couponId
	 *            the coupon id
	 * @param count
	 *            the count
	 * @return the string
	 */
	@RequestMapping(value = "/sendForOffLine/{couponId}", method = RequestMethod.POST)
	public @ResponseBody String sendForOffLine(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId, Integer count) {
		boolean success = true;
		String msg = "";
		if (couponId == null && success) {
			msg = "couponId不能为空";
			success = false;
		} else if (AppUtils.isBlank(count) || count <= 0 && success) {
			msg = "生成数要大于0";
			success = false;
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null && success) {
			msg = "找不到[" + couponId + "]礼券";
			success = false;
		} else if (!coupon.getSendType().equals(SendCouponTypeEnum.OFFLINE.value()) && success) { // 不是该类型
			msg = "[" + couponId + "]礼券类型不正确!";
			success = false;
		} else if (coupon.getStatus() == Constants.OFFLINE.intValue() && success) {
			msg = "[" + couponId + "]礼券已冻结!";
			success = false;
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date && success) {
			msg = "[" + couponId + "]礼券已过期!";
			success = false;
		} else if (coupon.getCouponNumber() == 0 && success) {
			msg = "[" + couponId + "]礼券初始化数量为0 !";
			success = false;
		} else if (coupon.getCouponNumber() < count && success) {
			msg = "发放数量不能大于初始化数量 !";
			success = false;
		}

		if (success) {
			int init = 100;// 每隔100条循环一次

			int runnum = count % init == 0 ? (count / init) : (count / init) + 1;

			int cycelTotal = count;
			LOGGER.info("线下礼券生成-循环保存的次数：" + runnum);
			String prefix = coupon.getCouponNumPrefix();
			long cnps_number = 0;
			for (int r = 0; r < runnum; r++) {
				if (cycelTotal < init) { // 9
					init = cycelTotal;
				}
				List<UserCoupon> userCoupon = new ArrayList<UserCoupon>();
				for (int j = 0; j < init; j++) { // 每批多少个
					UserCoupon cou = new UserCoupon();
					cou.setCouponId(couponId);
					cou.setCouponName(coupon.getCouponName());
					cou.setUseStatus(1);
					// 生产劵号
					cou.setCouponSn(CommonServiceUtil.getRandomCouponNumberGenerator());
					// cou.setGetTime(new Date());
					cou.setGetSources("线下发放");
					userCoupon.add(cou);
					cnps_number++;
				}
				if (AppUtils.isNotBlank(userCoupon)) {
					boolean config = userCouponService.saveOffUserCoupon(userCoupon, userCoupon.size(), couponId);
					userCoupon = null;
				}
				cycelTotal -= init;
			}
			LOGGER.info("线下礼券生成count：" + cnps_number);
			success = true;
		}

		String res = "{\"success\":" + success + ",\"msg\":\"" + msg + "\"}";
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.base.AdminController#save(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, UserCoupon userCoupon) {
		userCouponService.saveUserCoupon(userCoupon);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/userCoupon/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.USERCOUPONS_LIST_QUERY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.base.AdminController#delete(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserCoupon userCoupon = userCouponService.getUserCoupon(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// userCoupon.getUserName());
		// if(result!=null){
		// return result;
		// }
		userCouponService.deleteUserCoupon(userCoupon);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/userCoupon/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.USERCOUPONS_LIST_QUERY);
	}

	/**
	 * Load.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserCoupon userCoupon = userCouponService.getUserCoupon(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// userCoupon.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("userCoupon", userCoupon);
		return "/userCoupon/userCoupon";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.USERCOUPONS_EDIT_PAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.base.AdminController#load(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/userCoupon/userCoupon";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.USERCOUPONS_EDIT_PAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.base.AdminController#update(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserCoupon userCoupon = userCouponService.getUserCoupon(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// userCoupon.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("userCoupon", userCoupon);
		return "forward:/admin/userCoupon/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.USERCOUPONS_EDIT_PAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.base.AdminController#query(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, java.lang.Object)
	 */
	
	@RequestMapping(value = "/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, UserCoupon entity) {
		if (AppUtils.isNotBlank(entity.getCouponId())) {
			request.setAttribute("couponId", entity.getCouponId());
			return PathResolver.getPath(CouponAdminPage.USERCOUPONS_LIST_PAGE);
		}
		return "";
	}

	/**
	 * Export pord excel.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param couponId
	 *            the coupon id
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/exportExcel/{couponId}", method = RequestMethod.POST)
	public @ResponseBody String exportPordExcel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) throws Exception {
		boolean success = true;
		String msg = "";

		if (AppUtils.isBlank(couponId) && success) {
			msg = "couponId不能为空";
			success = false;
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null && success) {
			msg = "找不到[" + couponId + "]礼券";
			success = false;
		} else if (!coupon.getSendType().equals(SendCouponTypeEnum.OFFLINE.value()) && success) { // 不是该类型
			msg = "[" + couponId + "]礼券类型不正确!";
			success = false;
		} else if (coupon.getStatus() == Constants.OFFLINE.intValue() && success) {
			msg = "[" + couponId + "]礼券已冻结!";
			success = false;
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date && success) {
			msg = "[" + couponId + "]礼券已过期!";
			success = false;
		}
		// 查询要导出的数据
		List<UserCoupon> userCoupon = userCouponService.getOffExportExcelCoupon(couponId);
		if (AppUtils.isBlank(userCoupon) && success) {
			msg = "[" + couponId + "]没有相关的礼券信息,请重新生成发放!";
			success = false;
		}
		if (success) {
			String[] headers = { "礼券ID", "礼券名称", "礼券序号" };
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmm");
			String fileName = "offline_coupon_" + dateFormat.format(new Date()) + ".xlsx";
			String filePath = systemParameterUtil.getDownloadFilePath();
			OutputStream out = new FileOutputStream(getExcelFile(filePath, fileName));

			boolean config = userCouponService.exportExcel(couponId + "线下发放礼券表", headers, userCoupon, out, "yyyy-MM-dd");
			if (config) {
				msg = fileName;
				success = true;
			} else {
				msg = "下载失败,请联系管理员";
				success = false;
			}
		}
		String res = "{\"success\":" + success + ",\"msg\":\"" + msg + "\"}";
		return res;
	}

	// 创建文件
	/**
	 * Gets the excel file.
	 *
	 * @param filePath
	 *            the file path
	 * @param fileName
	 *            the file name
	 * @return the excel file
	 */
	private File getExcelFile(String filePath, String fileName) {
		File file = new File(filePath);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
		File excelFile = new File(filePath, fileName);
		return excelFile;
	}

}
