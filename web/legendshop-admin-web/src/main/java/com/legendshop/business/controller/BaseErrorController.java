package com.legendshop.business.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.util.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 错误处理页面
 *
 */
@Controller
@Slf4j
public class BaseErrorController implements ErrorController {

    @Override
    public String getErrorPath() {
        log.info("出错啦！进入error页面");
        return "/common/error";
    }

    @RequestMapping("/error")
    public String error(HttpServletRequest request) {
		//获取statusCode:401,404,500
		Integer statusCode =  getStatus(request);
		String request_uri = (String) request.getAttribute("javax.servlet.error.request_uri");

		log.info("Error happened when access url {}, the statusCode is {}", request_uri, statusCode);

		String errorMessage = (String) request.getAttribute("javax.servlet.error.message");
		Throwable exception = (Throwable) request.getAttribute("javax.servlet.error.exception");

		if (AppUtils.isNotBlank(errorMessage)) {
			log.error("errorMessage is {}, exception is {}", errorMessage, exception);
			request.setAttribute("errorMessage", errorMessage);
		}
		request.setAttribute("status", statusCode);

        if(statusCode != null &&  statusCode.equals(404)){
			return PathResolver.getPath(AdminPage.ADMIN_404);
		}else{
			return getErrorPath();
		}
    }

	/**
	 * 获取错误的返回码
	 */
	private Integer getStatus(HttpServletRequest request) {
		Integer status = (Integer) request.getAttribute("javax.servlet.error.status_code");
		if (status != null) {
			return status;
		}

		return 500;
	}

}
