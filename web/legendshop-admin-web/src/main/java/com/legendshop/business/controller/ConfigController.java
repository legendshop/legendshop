/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.page.FrontPage;
import com.legendshop.business.util.ClientPropertiesConfigReader;
import com.legendshop.core.constant.PathResolver;

/**
 * 配置客户端详情，需要登录管理员权限.
 */
@Controller
public class ConfigController {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(ConfigController.class);

	/**
	 * 获取每个客户的配置项目
	 */
	@RequestMapping(value = "/admin/clientConfig", method =RequestMethod.GET)
	@ResponseBody
	public Map<String, Properties> getConfig(HttpServletRequest request, HttpServletResponse response) {
		log.info("getConfig calling");
		return ClientPropertiesConfigReader.getInstance().getPropertiesMap();
	}
	
	@RequestMapping(value = "/system/cookies/login_user", method =RequestMethod.GET)
	public String getLoginSession(HttpServletRequest request, HttpServletResponse response) {
		log.info("getLoginSession calling");
		return PathResolver.getPath(FrontPage.LOGIN_USER);
	}

}