/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;
import com.legendshop.core.helper.ThreadLocalContext;

/**
 * The Enum FrontPage.
 */
public enum FrontPage implements PageDefinition {
	
	//登录的用户session的情况
	LOGIN_USER("/login_user"),
	
	/** 错误页面 **/	
	ERROR("/error"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new front page.
	 * 
	 * @param value
	 *            the value
	 * @param template
	 *            the template
	 */
	private FrontPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, java.util.List)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath(ThreadLocalContext.getFrontType(), path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
