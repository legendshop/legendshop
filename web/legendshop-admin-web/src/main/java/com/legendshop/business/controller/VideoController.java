/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathEnum;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.CommonPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AttachmentTreeService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.util.AppUtils;
/**
 * 视频空间
 *
 */
@Controller
@RequestMapping("/videoAdmin")
public class VideoController extends BaseController{
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(VideoController.class);
	
	@Autowired
    private AttachmentService attachmentService;
	
	@Autowired
    private AttachmentTreeService attachmentTreeService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	/**
	 * 图片空间 页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /videoAdmin query controller but user does not logined");
			return null;
		}
		
		PhotoPathDto photoPathDto = PhotoPathResolver.getInstance().calculateContextPath();
		if(photoPathDto.getType().equals(PhotoPathEnum.OSS)) {
			request.setAttribute("imageSuffix", "?x-oss-process=image/resize,m_fixed,h_100,w_100");//OSS的后缀,对应原有的scale为2，弹层中的图片大小固定为100 * 100
		}
		
		request.setAttribute("imagePrefix", photoPathDto.getFilePath());
		
        return PathResolver.getPath(CommonPage.VIDEO_LIST_QUERY);
    }
	
	/**
	 * 图片控件 页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/prop")
    public String prop(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /prop query controller but user does not logined");
			return null;
		}
	
		PhotoPathDto photoPathDto = PhotoPathResolver.getInstance().calculateContextPath();
		if(photoPathDto.getType().equals(PhotoPathEnum.OSS)) {
			request.setAttribute("imageSuffix", "?x-oss-process=image/resize,m_fixed,h_100,w_100");//OSS的后缀,对应原有的scale为2，弹层中的图片大小固定为100 * 100
		}
		
		request.setAttribute("imagePrefix", photoPathDto.getFilePath());
        return PathResolver.getPath(CommonPage.VIDEO_PROP_QUERY);
    }
	
	/**
	 * 图片控件 页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/remoteImages")
    public String remoteImages(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /remoteImages query controller but user does not logined");
			return null;
		}
		
		PhotoPathDto photoPathDto = PhotoPathResolver.getInstance().calculateContextPath();
		if(photoPathDto.getType().equals(PhotoPathEnum.OSS)) {
			request.setAttribute("imageSuffix", "?x-oss-process=image/resize,m_fixed,h_100,w_100");//OSS的后缀,对应原有的scale为2，弹层中的图片大小固定为100 * 100
		}
		
		request.setAttribute("imagePrefix", photoPathDto.getFilePath());
		
        return PathResolver.getPath(CommonPage.VIDEO_REMOTE_QUERY);
    }
	
	/**
	 * 获取父目录 获取子目录
	 * @param request
	 * @param response
	 * @param parentId
	 * @return
	 */
	@RequestMapping("/getChildTree")
	public @ResponseBody List<AttachmentTree>  getChildTree(HttpServletRequest request, HttpServletResponse response,Long parentId) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /getChildTree query controller but user does not logined");
			return null;
		}
		
		String userName = user.getUsername();
		
		List<AttachmentTree> attmntTrees = null;
		if(parentId==null){
			attmntTrees = new ArrayList<AttachmentTree>();
			AttachmentTree attachmentTree = new AttachmentTree();
			attachmentTree.setId(0l);
			attachmentTree.setName("根目录");
			attachmentTree.setParentId(0l);
			attachmentTree.setUserName(userName);
			attachmentTree.setIsParent(true);
			attachmentTree.setOpen(true);
			attmntTrees.add(attachmentTree);
		}else{
			attmntTrees = attachmentTreeService.getAttachmentTreeByPid(parentId,userName);
		}
		return attmntTrees;
	}
	
	/**
	 * 图片空间 获取目录下的图片
	 * @param request
	 * @param response
	 * @param treeId
	 * @param curPageNO
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/getChildImg")
	public @ResponseBody PageSupport<Attachment> getChildImg(HttpServletRequest request, HttpServletResponse response,Long treeId, String curPageNO,String shopName,String searchName) throws IOException {
		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null || user.getShopId() == null){
			log.error("visit /getChildImg query controller but user shopId is null");
			return null;
		}
		PageSupport<Attachment> ps = attachmentService.getAttachment(shopName, 1, 1, 21, curPageNO,searchName, ImageTypeEnum.VIDEO.value());
		return ps;
	}
	
	/**
	 * 图片控件 获取目录下的图片
	 * @param request
	 * @param response
	 * @param treeId
	 * @param curPageNO
	 * @param orderBy
	 * @param searchName
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/getPropChildImg")
	public @ResponseBody PageSupport<Attachment> getPropChildImg(HttpServletRequest request, HttpServletResponse response,Long treeId, String curPageNO,String orderBy,String searchName) throws IOException {

		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /getPropChildImg query controller but user is null");
			return null;
		}

		PageSupport<Attachment> ps = attachmentService.getAttachmentPage(curPageNO,searchName,orderBy,user.getShopId(),treeId);
		return ps;
	}
	
	/**
	 * 删除图片
	 * @param request
	 * @param response
	 * @param filePath
	 * @return
	 */
	@RequestMapping("/delImg")
	public @ResponseBody String delImg(HttpServletRequest request, HttpServletResponse response,String filePath)  {
		
		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null || user.getShopId() == null){
			log.error("visit /delImg query controller but user shopId is null");
			return null;
		}
		//只有 该图片的拥有者才可删除
		Attachment attachment = attachmentService.getAttachmentByFilePath(filePath,user.getShopId());
		if(AppUtils.isNotBlank(attachment)){
			//删除附件表记录->删除文件
			attachmentService.deleteAttachment(attachment);
			attachmentManager.deleteTruely(filePath);
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 图片批量上传
	 * @param request
	 * @param response
	 * @param treeId
	 * @param file
	 * @return
	 */
	@RequestMapping("/uploadImg")
	public @ResponseBody String uploadImg(HttpServletRequest request, HttpServletResponse response,Integer treeId,@RequestParam("files") MultipartFile[] files)  {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			return null;
		}
		if(AppUtils.isNotBlank(files)){
			for(int i=0;i<files.length;i++){
				MultipartFile file = files[i];
				String filePath = attachmentManager.upload(file);
				//TODO  新增批量删除?
				attachmentManager.saveAttachment(user.getUsername(),user.getUserId(), user.getShopId(), filePath, file, AttachmentTypeEnum.PRODUCT,treeId, ImageTypeEnum.VIDEO);
			}
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 修改图片名称
	 * @param request
	 * @param response
	 * @param filePath
	 * @param fileName
	 * @return
	 */
	@RequestMapping("/renameImg")
	public @ResponseBody String renameImg(HttpServletRequest request, HttpServletResponse response,String filePath,String fileName)  {
		
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			
			return null;
		}
		attachmentService.updateAttachmentFileNameByFilePath(filePath, fileName);
		return Constants.SUCCESS;
	}
	
	/**
	 * 修改目录名称
	 * @param request
	 * @param response
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping("/renameTree")
	public @ResponseBody String renameTree(HttpServletRequest request, HttpServletResponse response,Integer id,String name)  {

		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /renameTree query controller but user does not logined");
			return null;
		}
		attachmentTreeService.updateAttmntTreeNameById(id,name);
		return Constants.SUCCESS;
	}
	
	/**
	 * 添加目录
	 * @param request
	 * @param response
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping("/addTree")
	public @ResponseBody Long addTree(HttpServletRequest request, HttpServletResponse response,Long id,String name)  {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			log.error("visit /addTree query controller but user does not logined");
			return null;
		}
		AttachmentTree attachmentTree = new AttachmentTree();
		attachmentTree.setName(name);
		attachmentTree.setParentId(id);
		attachmentTree.setUserName(user.getUsername());
		attachmentTree.setIsParent(true);
		attachmentTree.setOpen(true);
		attachmentTree.setShopId(user.getShopId());
		return attachmentTreeService.saveAttachmentTree(attachmentTree);
	}
	
	/**
	 * 删除目录 和 目录下的所有子目录 和图片
	 * @param request
	 * @param response
	 * @param treeId 目录id
	 * @return
	 */
	@RequestMapping(value = "/delTreeAndPic", method = RequestMethod.POST)
	public @ResponseBody String delTreeAndPic(HttpServletRequest request, HttpServletResponse response,Long treeId)  {
		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null || user.getShopId() == null){
			log.error("visit /delTreeAndPic query controller but user does not logined");
			return null;
		}
		//根节点 不能删除
		if(treeId==null || treeId==0){
			return Constants.FAIL;
		}
		//查询该节点是否属于该用户,然后进行递归查出该目录 的所有子目录 和 文件,最后删除
		AttachmentTree attachmentTree = attachmentTreeService.getAttachmentTree(treeId,user.getShopId());
		if(AppUtils.isNotBlank(attachmentTree)){
			//递归查出该节点 的所有子目录
			List<AttachmentTree> attmntTreeList = attachmentTreeService.getAllChildByTreeId(treeId);
			//找出所有子目录下的附件记录
			List<Attachment> attmntList  = attachmentService.getAttachmentByTrees(attmntTreeList);
			if(AppUtils.isNotBlank(attmntList)){
				//删除所有附件记录
				attachmentService.deleteAttachment(attmntList);
				for (Attachment attachment : attmntList) {
					//删除文件
					attachmentManager.deleteTruely(attachment.getFilePath());
				}
			}
			//删除所有目录
			attachmentTreeService.deleteAttachmentTree(attmntTreeList);
		}
		return Constants.SUCCESS;
	}
}
