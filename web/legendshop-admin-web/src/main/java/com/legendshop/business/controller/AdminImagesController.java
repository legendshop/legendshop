/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.CommonPage;
import com.legendshop.core.constant.PathResolver;
/**
 * 图片空间
 *
 */
@Controller
@RequestMapping("/admin/photo")
public class AdminImagesController extends BaseController{
	
	/**
	 * 图片空间 页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response) {
        return PathResolver.getPath(CommonPage.IMAGE_LIST_QUERY);
    }

	
	
}
