/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.util.EnvironmentConfig;
import com.legendshop.util.FileConfig;

/**
 * 
 * 配置文件工具类, 读取每个配置文件的配置项.
 */
public class ClientPropertiesConfigReader {

	/** The logger. */
	private final static Logger logger = LoggerFactory.getLogger(ClientPropertiesConfigReader.class);

	/** 单个实例. */
	static ClientPropertiesConfigReader instance;// 创建对象ec

	/**
	 * 构造函数。
	 */
	private ClientPropertiesConfigReader() {
		super();
	}

	/**
	 * 取得EnvironmentConfig的一个实例.
	 * 
	 * @return ec
	 */
	public static ClientPropertiesConfigReader getInstance() {
		if (instance == null) {
			synchronized (ClientPropertiesConfigReader.class) {
				if (instance == null) {
					instance = new ClientPropertiesConfigReader();// 创建EnvironmentConfig对象
				}
			}

		}
		return instance;// 返回EnvironmentConfig对象
	}

	/**
	 * 读取配置文件.
	 * 
	 * @param fileName
	 *            the file name
	 * @return 该配置文件的方式
	 */

	public synchronized Map<String, Properties> getPropertiesMap() {// 传递配置文件路径
		Map<String, Properties> map = new HashMap<String, Properties>();
		map.put(FileConfig.AppConfFile, getPropertiesByFile(FileConfig.AppConfFile));
		map.put(FileConfig.ConfigFile, getPropertiesByFile(FileConfig.ConfigFile));
		map.put(FileConfig.GlobalFile, getPropertiesByFile(FileConfig.GlobalFile));
		map.put(FileConfig.JdbcFile, getPropertiesByFile(FileConfig.JdbcFile));
		map.put(FileConfig.WeiXinConfigFile, getPropertiesByFile(FileConfig.WeiXinConfigFile));
		return map;// 返回Properties对象
	}

	private Properties getPropertiesByFile(String fileName) {
		Properties p = null;
		String filePath = fileName;
		/**
		 * 如果为空就尝试输入进文件
		 */
		try {
			InputStream is = null;// 定义输入流is
			try {
				is = new FileInputStream(filePath);// 创建输入流
			} catch (Exception e) {
				if (fileName.startsWith("/"))
					// 用getResourceAsStream()方法用于定位并打开外部文件。
					is = EnvironmentConfig.class.getResourceAsStream(filePath);
				else
					is = EnvironmentConfig.class.getResourceAsStream("/" + filePath);
			}
			p = new Properties();
			p.load(is);// 加载输入流
			is.close();// 关闭输入流

		} catch (Exception e) {
			logger.warn("getProperties: ", e);
		}
		return p;// 返回Properties对象
	}

}
