/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.base.util.ResourcePathUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.JSONUtil;

/**
 * 文件上传控制器
 *
 * @author bevan
 *
 */
@Controller
@RequestMapping("/editor/uploadJson")
public class FileUploadController extends BaseController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(FileUploadController.class);

	@Autowired
	AttachmentManager attachmentManager;

	@Autowired
	PropertiesUtil propertiesUtil;

	/**
	 * 采用spring session + redis的方式来处理, 如果采用普通session方式, 该类就没有用了.
	 */
	@Autowired(required = false)
	RedisOperationsSessionRepository sessionRespositry;

	/**
	 * 上传文件, session处理方式有2种方式, 1. 采用spring session + redis的方式来处理 2. 采用普通的http session
	 *
	 * Remark: 采用了spring boot之后ServletFileUpload失效, 需要采用spring boot的方式接受文件:MultipartFile, 单个文件 多个文件和视频都是用imgFile作为file字段传递过来
	 *
	 *
	 */
	@RequestMapping("/upload")
	public @ResponseBody Map<String, Object> upload(HttpServletRequest request, HttpServletResponse response, @RequestParam("imgFile") MultipartFile file) throws FileUploadException, IOException {

		if(file == null || file.getSize() <=0) {
			log.warn("您尚未选择文件");
			return getError("您尚未登录。");
		}

		SecurityUserDetail user = null;
		if(sessionRespositry != null) {
			String requestURI = request.getRequestURI();
			String sessionId = requestURI.substring(requestURI.length() - 48, requestURI.length());//获取sessionId
			String baseSessionId = base64Decode(sessionId);
			Session session = sessionRespositry.findById(baseSessionId);
			if (session == null) {
				log.warn("您尚未登录,不能上传文件");
				return getError("您尚未登录。");
			}
			user = UserManager.getUser(session);
		}else {
			user = UserManager.getUser(request);
		}

		if (user == null) {
			log.warn("您尚未登录,不能上传文件");
			return getError("您尚未登录。");
		}
		return doUploadFile(request, response, user, file);
	}



	/**
	 * 图片上传
	 *
	 * @param request
	 * @param response
	 * @param userName
	 * @return
	 * @throws FileUploadException
	 */
	private Map<String, Object> doUploadFile(HttpServletRequest request, HttpServletResponse response, SecurityUserDetail user, MultipartFile file) throws FileUploadException {
		try {
			String filePath = attachmentManager.upload(file);
			log.info("filePath:{}",filePath);
			if (filePath == null) {
				return getError("上传文件失败。");
			}
			if (Constants.PHOTO_FORMAT_ERR.equals(filePath)) {
				return getError("文件格式错误。");
			}
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filePath, file, AttachmentTypeEnum.PRODUCT);
			Map<String, Object> msg = new HashMap<String, Object>();
			msg.put("error", 0);
			PhotoPathDto photoPathDto = PhotoPathResolver.getInstance().calculateFilePath(filePath);
			msg.put("url", photoPathDto.getFilePath());
			return msg;
		} catch (Exception e) {
			e.printStackTrace();
			return getError("上传文件失败。");
		}
	}


	/**
	 * 获取返回的错误码
	 * @param message
	 * @return
	 */
	private Map<String, Object> getError(String message) {
		Map<String, Object> msg = new HashMap<String, Object>();
		msg.put("error", 1);
		msg.put("message", message);
		return msg;
	}


	/**
	 * 获取cookies中的sessionId
	 */
	private String base64Decode(String base64Value) {
		try {
			byte[] decodedCookieBytes = Base64.getDecoder().decode(base64Value);
			return new String(decodedCookieBytes);
		} catch (Exception e) {
			return null;
		}
	}
}
