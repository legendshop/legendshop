/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 重定向页面定义
 * 
 */
public enum RedirectPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 预存款充值列表页 */
	PDRECHARGE_LIST_QUERY("/admin/preDeposit/pdRechargeQuery"),
	
	/** 预存款提现列表页 */
	PDWITHDRAWCASH_LIST_QUERY("/admin/preDeposit/pdWithdrawCashQuery")

	;
	

	/** The value. */
	private final String value;

	private RedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private RedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
