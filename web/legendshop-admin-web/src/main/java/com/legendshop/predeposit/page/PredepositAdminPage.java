/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * Tiles页面定义 要和主工程.
 */
public enum PredepositAdminPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 预存款充值列表页 */
	PDRECHARGE_LIST_PAG("/preDeposit/pdRechargeList"),

	/** 预存款提现列表页 */
	PDWITHDRAWCASH_LIST_PAG("/preDeposit/pdWithdrawCashList"),

	/** 预存款提现列表页 */
	PDCASHLOG_LIST_PAG("/preDeposit/pdCashLogList"),

	/** 充值编辑页面 */
	PDRECHARGE_EDIT("/preDeposit/pdRecharge"),

	/** 提现编辑页面 */
	PDWITHDRAWCASH_EDIT("/preDeposit/pdWithdrawCash"),

	/** 会员预存款明细 */
	USER_PDCASHLOG("/preDeposit/userPdCashLog"),

	;

	/** The value. */
	private final String value;

	private PredepositAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	public String getNativeValue() {
		return value;
	}

}
