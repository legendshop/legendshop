/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义
 */
public enum BackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 收款人. */
	PAY_PAGE("/payment/payto"),
	
	/** 查看充值编号详细信息 */
	PDRECHARGE_INFO_PAGE("/preDeposit/pdRechargeInfo"),
	
	/** 查看提现编号详细信息 */
	WITHDRAWCASH_INFO_PAGE("/preDeposit/pdWithdrawCashInfo")
	
	;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面
	 * @param value the value
	 */
	private BackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
