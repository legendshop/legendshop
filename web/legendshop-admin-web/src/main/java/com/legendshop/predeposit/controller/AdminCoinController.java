/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.POIReadeExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CoinImportResult;
import com.legendshop.model.dto.CoinRechargeLog;
import com.legendshop.model.dto.coin.CoinConsumeDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.predeposit.page.CoinAdminFrontPage;
import com.legendshop.predeposit.page.CoinAdminPage;
import com.legendshop.spi.service.CoinLogService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 金币controller.
 */
@Controller
@RequestMapping(value = "/admin/coin")
public class AdminCoinController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(AdminCoinController.class);

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private CoinLogService coinLogService;

	/**
	 * 金币充值弹出窗.
	 *
	 * @param userId
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/rechargePage")
	public String rechargePage(String userId, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("userId", userId);
		String path = PathResolver.getPath(CoinAdminFrontPage.RECHARGE_PAGE);
		return path;
	}

	/**
	 * 金币充值.
	 *
	 * @param userId
	 * @param amount
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/recharge")
	@ResponseBody
	public String recharge(String userId, Double amount, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("<------start------UserDetailService#recharge()--------");
		logger.debug("params of UserDetailService#recharge() is userId = {},account is = {}", userId, amount);
		if (AppUtils.isBlank(userId)) {
			return "fail";
		}
		if (AppUtils.isBlank(amount)) {
			return "notAccount";
		}
		try {
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			userDetailService.rechargeUserCoin(userDetail, amount);
			return Constants.SUCCESS;
		} catch (Exception e) {
			logger.error("error: {}", e);
			return "fail";
		} finally {
			logger.debug("---------UserDetailService#recharge()-------end------>");
		}
	}

	/**
	 * 充值管理.
	 *
	 * @param curPageNO
	 * @param userName
	 * @param mobile
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/rechargeAdmin")
	public String rechargeAdmin(String curPageNO, String userName, String mobile, HttpServletRequest request, HttpServletResponse response) {
		PageSupport<CoinConsumeDto> ps = coinLogService.getRechargeDetailsList(curPageNO, userName, mobile);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userName", userName);
		request.setAttribute("mobile", mobile);
		String path = PathResolver.getPath(CoinAdminPage.COIN_RECHARGE_ADMIN);
		return path;
	}

	/**
	 * 批量充值上传页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/uploadPage")
	public String uploadPage(HttpServletRequest request, HttpServletResponse response) {
		String path = PathResolver.getPath(CoinAdminFrontPage.UPLOAD_PAGE);
		return path;
	}

	/**
	 * 批量充值上传.
	 *
	 * @param file
	 * @param request
	 * @param response
	 * @return the coin recharge log
	 */
	@RequestMapping(value = "/uploadExecl")
	@ResponseBody
	public CoinRechargeLog uploadExecl(MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
		try {
			POIReadeExcelUtil readeExcelUtil = new POIReadeExcelUtil();
			CoinImportResult result = readeExcelUtil.batchRecharge(file.getInputStream());
			CoinRechargeLog rechargeLog = userDetailService.batchUpdateCoin(result);
			return rechargeLog;
		} catch (Exception e) {
			logger.error("金币 批量导入失败 exception:{}", e);
			CoinRechargeLog rechargeLog = new CoinRechargeLog();
			rechargeLog.setIsSuccess("-1");
			rechargeLog.setMessage("导入失败！检查模板格式是否正确?");
			return rechargeLog;
		}
	}

	/**
	 * 金币消费明细.
	 *
	 * @param curPageNO
	 * @param userName
	 * @param mobile
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/consumeDetails")
	public String consumeDetails(String curPageNO, String userName, String mobile, HttpServletRequest request, HttpServletResponse response) {
		PageSupport<CoinConsumeDto> ps = coinLogService.getConsumeDetailsListPage(curPageNO, mobile, userName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userName", userName);
		request.setAttribute("mobile", mobile);
		String path = PathResolver.getPath(CoinAdminPage.COIN_CONSUME_DETAILS);
		return path;
	}


}
