/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.predeposit.page.BackPage;
import com.legendshop.predeposit.page.PredepositAdminPage;
import com.legendshop.predeposit.page.RedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.spi.service.PdRechargeService;
import com.legendshop.spi.service.PdWithdrawCashService;
import com.legendshop.spi.service.PreDepositService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 预存款控制器
 */
@Controller
@RequestMapping("/admin/preDeposit")
public class AdminPreDepositController extends BaseController {

	@Autowired
	private PreDepositService preDepositService;

	@Autowired
	private PdRechargeService pdRechargeService;

	@Autowired
	private PdWithdrawCashService pdWithdrawCashService;

	@Autowired
	private PdCashLogService pdCashLogService;

	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 根据id批量删除预存款充值信息
	 * 
	 * @param request
	 * @param response
	 * @param pdRecharge
	 * @return
	 */
	@SystemControllerLog(description="批量删除预存款充值信息")
	@RequestMapping(value = "/batchDelete/{ids}", method = RequestMethod.DELETE)
	public @ResponseBody String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String  ids) {
		try{
			pdRechargeService.batchDelete(ids);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return "批量删除预存款记录失败。";
		}
	}

	/**
	 * 格式化时间
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 预存款充值列表查询
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pdRecharge
	 * @return
	 */
	@RequestMapping("/pdRechargeQuery")
	public String pdRechargeQuery(HttpServletRequest request, HttpServletResponse response, String curPageNO, PdRecharge pdRecharge) {

		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "amount", "payment_name", "add_time", "payment_time" });

		PageSupport<PdRecharge> ps = pdRechargeService.getPdRechargeListPage(curPageNO, pdRecharge, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", pdRecharge);
		return PathResolver.getPath(PredepositAdminPage.PDRECHARGE_LIST_PAG);
	}

	/**
	 * 根据ID显示充值信息
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/pdRechargeQueryById")
	public String pdRechargeQueryById(HttpServletRequest request, HttpServletResponse response, Long id) {
		PdRecharge recharge = pdRechargeService.getPdRecharge(id);
		if (recharge == null) {
			throw new BusinessException("object is null");
		}

		request.setAttribute("recharge", recharge);
		String result = PathResolver.getPath(BackPage.PDRECHARGE_INFO_PAGE);
		return result;
	}

	/**
	 * 根据ID编辑预存款充值信息
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/pdRechargeEdit/{id}", method = RequestMethod.GET)
	public String pdRechargeEdit(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PdRecharge recharge = pdRechargeService.getPdRecharge(id);
		if (recharge == null) {
			throw new BusinessException("object is null");
		}
		String nickName = userDetailService.getNickNameByUserName(recharge.getUserName());
		if (AppUtils.isNotBlank(nickName)) {
			recharge.setNickName(nickName);
		}
		request.setAttribute("recharge", recharge);
		String result = PathResolver.getPath(PredepositAdminPage.PDRECHARGE_EDIT);
		return result;
	}

	/**
	 * 更新预存款充值状态
	 * 
	 * @param request
	 * @param response
	 * @param pdRecharge
	 * @return
	 */
	@SystemControllerLog(description="更新预存款充值状态")
	@RequestMapping(value = "/pdRechargeUpdate")
	public String pdRechargeUpdate(HttpServletRequest request, HttpServletResponse response, PdRecharge pdRecharge) {
		if (AppUtils.isBlank(pdRecharge.getPaymentTime())) {
			throw new BusinessException("PaymentTime is null");
		}
		if (AppUtils.isBlank(pdRecharge.getAdminUserNote())) {
			throw new BusinessException("AdminUserNote is null");
		}
		if (AppUtils.isBlank(pdRecharge.getPaymentName())) {
			throw new BusinessException("PaymentName is null");
		}
		PdRecharge orginPdRecharge = pdRechargeService.getPdRecharge(pdRecharge.getId());
		if (AppUtils.isBlank(orginPdRecharge)) {
			throw new BusinessException("PdRecharge is null");
		}
		if (!orginPdRecharge.getPaymentState().equals(pdRecharge.getPaymentState())) {
			throw new BusinessException("Operation Error");
		}
		if (!orginPdRecharge.getSn().equals(pdRecharge.getSn())) {
			throw new BusinessException("Operation Error");
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		orginPdRecharge.setPaymentName(pdRecharge.getPaymentName());
		orginPdRecharge.setPaymentState(Constants.ONLINE);
		orginPdRecharge.setPaymentCode(pdRecharge.getPaymentCode());
		orginPdRecharge.setPaymentTime(pdRecharge.getPaymentTime());
		orginPdRecharge.setTradeSn(pdRecharge.getTradeSn());
		orginPdRecharge.setAdminUserName(user.getUsername());
		orginPdRecharge.setAdminUserNote(pdRecharge.getAdminUserNote());
		String result = preDepositService.adminRechargeForPaySucceed(orginPdRecharge);
		if (PreDepositErrorEnum.NOT_USER.value().equals(result)) {
			throw new BusinessException("user is null");
		}
		if (PreDepositErrorEnum.NON_COMPLIANCE.value().equals(result)) {
			throw new BusinessException("Does not meet the operating conditions");
		}
		if (PreDepositErrorEnum.OK.value().equals(result)) {
			return PathResolver.getPath(RedirectPage.PDRECHARGE_LIST_QUERY);
		}
		return null;
	}

	/**
	 * 根据id删除预存款充值信息
	 * 
	 * @param request
	 * @param response
	 * @param pdRecharge
	 * @return
	 */
	@SystemControllerLog(description="删除预存款充值信息")
	@RequestMapping(value = "/pdRechargeDelete/{id}", method = RequestMethod.GET)
	public String pdRechargeDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PdRecharge recharge = pdRechargeService.getPdRecharge(id);
		if (AppUtils.isBlank(recharge)) {
			throw new BusinessException("object is null");
		}
		if (!id.equals(recharge.getId())) {
			throw new BusinessException("Operation Error");
		}
		pdRechargeService.deleteRechargeDetail(recharge.getUserId(), recharge.getSn());
		return PathResolver.getPath(RedirectPage.PDRECHARGE_LIST_QUERY);
	}

	/**
	 * 预存款提现列表查询
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pdRecharge
	 * @return
	 */
	@RequestMapping("/pdWithdrawCashQuery")
	public String pdWithdrawCashQuery(HttpServletRequest request, HttpServletResponse response, String curPageNO, PdWithdrawCash pdWithdrawCash) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "amount", "add_time" });

		PageSupport<PdWithdrawCash> ps = pdWithdrawCashService.getPdWithdrawCashListPage(curPageNO, pdWithdrawCash, result, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", pdWithdrawCash);
		return PathResolver.getPath(PredepositAdminPage.PDWITHDRAWCASH_LIST_PAG);
	}

	/**
	 * 根据ID显示提现信息
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/pdWithdrawCashQueryById")
	public String pdWithdrawCashQueryById(HttpServletRequest request, HttpServletResponse response, Long id) {
		PdWithdrawCash withdrawCash = pdWithdrawCashService.getPdWithdrawCash(id);
		if (withdrawCash == null) {
			throw new BusinessException("object is null");
		}
		request.setAttribute("withdrawCash", withdrawCash);
		String result = PathResolver.getPath(BackPage.WITHDRAWCASH_INFO_PAGE);
		return result;
	}

	/**
	 * 根据ID编辑欲预存款提现信息
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/pdWithdrawCashEdit/{id}", method = RequestMethod.GET)
	public String pdWithdrawCashEdit(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PdWithdrawCash pdWithdrawCash = pdWithdrawCashService.getPdWithdrawCash(id);
		if (pdWithdrawCash == null) {
			throw new BusinessException("object is null");
		}
		String nickName = userDetailService.getNickNameByUserName(pdWithdrawCash.getUserName());
		if (AppUtils.isNotBlank(nickName)) {
			pdWithdrawCash.setNickName(nickName);
		}
		request.setAttribute("pdWithdrawCash", pdWithdrawCash);
		String result = PathResolver.getPath(PredepositAdminPage.PDWITHDRAWCASH_EDIT);
		return result;
	}

	/**
	 * 更新预存款提现状态
	 * 
	 * @param request
	 * @param response
	 * @param pdRecharge
	 * @return
	 */
	@SystemControllerLog(description="更新预存款提现状态")
	@RequestMapping(value = "/pdWithdrawCashUpdate")
	public String pdWithdrawCashUpdate(HttpServletRequest request, HttpServletResponse response, PdWithdrawCash pdWithdrawCash) {
		if (AppUtils.isBlank(pdWithdrawCash.getAdminNote())) {
			throw new BusinessException("AdminNote is null");
		}
		if (AppUtils.isBlank(pdWithdrawCash.getPaymentTime())) {
			throw new BusinessException("PaymentTime is null");
		}
		PdWithdrawCash orginPdWithdrawCash = pdWithdrawCashService.getPdWithdrawCash(pdWithdrawCash.getId());
		if (AppUtils.isBlank(orginPdWithdrawCash)) {
			throw new BusinessException("PdRecharge is null");
		}
		if (!orginPdWithdrawCash.getPaymentState().equals(pdWithdrawCash.getPaymentState())) {
			throw new BusinessException("Operation Error");
		}
		if (!orginPdWithdrawCash.getPdcSn().equals(pdWithdrawCash.getPdcSn())) {
			throw new BusinessException("Operation Error");
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		orginPdWithdrawCash.setPaymentState(Constants.ONLINE);
		orginPdWithdrawCash.setPaymentTime(pdWithdrawCash.getPaymentTime());
		orginPdWithdrawCash.setUpdateAdminName(user.getUsername());
		orginPdWithdrawCash.setAdminNote(pdWithdrawCash.getAdminNote());
		String result = preDepositService.withdrawalApplyFinally(orginPdWithdrawCash);
		if (PreDepositErrorEnum.NOT_USER.value().equals(result)) {
			throw new BusinessException("user is null");
		}
		if (PreDepositErrorEnum.NON_COMPLIANCE.value().equals(result)) {
			throw new BusinessException("Does not meet the operating conditions");
		}
		if (PreDepositErrorEnum.OK.value().equals(result)) {
			return PathResolver.getPath(RedirectPage.PDWITHDRAWCASH_LIST_QUERY);
		}
		return null;
	}

	/**
	 * 根据id删除预存款提现信息
	 * 
	 * @param request
	 * @param response
	 * @param pdRecharge
	 * @return
	 */
	@SystemControllerLog(description="删除预存款提现信息")
	@RequestMapping(value = "/pdWithdrawCashDelete/{id}", method = RequestMethod.GET)
	public String pdWithdrawCashDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		PdWithdrawCash pdWithdrawCash = pdWithdrawCashService.getPdWithdrawCash(id);
		if (AppUtils.isBlank(pdWithdrawCash)) {
			throw new BusinessException("object is null");
		}
		
		if (!id.equals(pdWithdrawCash.getId())) {
			throw new BusinessException("Operation Error");
		}
		
		String result = preDepositService.withdrawalApplyCancel(pdWithdrawCash.getPdcSn(), pdWithdrawCash.getAmount(), pdWithdrawCash.getUserId());
		if (PreDepositErrorEnum.NOT_USER.value().equals(result)) {
			throw new BusinessException("user is null");
		}
		
		if (PreDepositErrorEnum.NON_COMPLIANCE.value().equals(result)) {
			throw new BusinessException("Does not meet the operating conditions");
		}
		
		if (PreDepositErrorEnum.OK.value().equals(result)) {
			return PathResolver.getPath(RedirectPage.PDWITHDRAWCASH_LIST_QUERY);
		}
		return null;
	}

	/**
	 * 预存款日志列表查询
	 * 
	 */
	@RequestMapping("/pdCashLogQuery")
	public String pdCashLogQuery(HttpServletRequest request, HttpServletResponse response, String curPageNO, PdCashLog pdCashLog) {

		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "amount" });

		PageSupport<PdCashLog> ps = pdCashLogService.getPdCashLogListPage(curPageNO, pdCashLog, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", pdCashLog);
		return PathResolver.getPath(PredepositAdminPage.PDCASHLOG_LIST_PAG);
	}

	/**
	 * 查询会员预存款日志
	 * 
	 */
	@RequestMapping("/userPdCashLog/{userName}")
	public String userPdCashLog(HttpServletRequest request, HttpServletResponse response, String curPageNO, String userId, @PathVariable String userName) {
		PageSupport<PdCashLog> ps = pdCashLogService.getPdCashLogListPage(curPageNO, userId, userName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(PredepositAdminPage.USER_PDCASHLOG);
	}

}