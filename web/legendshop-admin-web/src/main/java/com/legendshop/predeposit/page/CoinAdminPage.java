/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 金币后台页面
 */
public enum CoinAdminPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 金币消费详情 */
	COIN_CONSUME_DETAILS("consumeDetails"),

	/** 金币充值管理 */
	COIN_RECHARGE_ADMIN("rechargeAdmin");

	/** The value. */
	private final String value;

	private CoinAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("predeposit", path);
	}

	public String getNativeValue() {
		return value;
	}

}
