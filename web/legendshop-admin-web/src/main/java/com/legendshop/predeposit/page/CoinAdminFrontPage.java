/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 预存款管理员端页面
 */
public enum CoinAdminFrontPage implements PageDefinition {

	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 后台金币充值页面 */
	RECHARGE_PAGE("/rechargePage"),
	
	/** 批量充值上传页面 */
	UPLOAD_PAGE("/uploadPage");
	

	/** The value. */ 
	private final String value;

	private CoinAdminFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("predeposit", path);
	}

	public String getNativeValue() {
		return value;
	}

}
