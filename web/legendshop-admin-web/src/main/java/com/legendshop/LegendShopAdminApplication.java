/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * 系统启动类.
 */
@EnableCaching
@EnableWebSecurity
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		RedisAutoConfiguration.class, AopAutoConfiguration.class,
		AopAutoConfiguration.class }, scanBasePackages = { "com.legendshop"})
@ImportResource({ "classpath:spring/applicationContext.xml" })
public class LegendShopAdminApplication {

	/** 日志. */
	private final static Logger log = LoggerFactory.getLogger(LegendShopAdminApplication.class);
	
	/**
	  * 主方法
	 */
	public static void main(String[] args) {
		SpringApplication.run(LegendShopAdminApplication.class, args);

		System.out.println("LegendShopAdminApplication 启动成功！");
		// 将hook线程添加到运行时环境中去
		Runtime.getRuntime().addShutdownHook(new CleanWorkThread());
	}

	/**
	 * hook线程,关闭时调用
	 */
	static class CleanWorkThread extends Thread {
		@Override
		public void run() {
			log.info(" LegendShop Frontend System shutdowned.");
		}
	}


}
