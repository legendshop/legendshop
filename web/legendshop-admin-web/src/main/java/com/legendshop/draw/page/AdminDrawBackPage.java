/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 抽奖返回页面定义
 * @author quanzc
 *
 */
public enum AdminDrawBackPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 微信二维码页面 */
	DRAW_AWARDS_QRCODE("/drawActivity/ActQrCode"),
	
	/**中奖记录 填写发货物流页面**/
	DRAW_WIN_RECORD_DELIVERGOODS("/drawWinRecord/deliverGoods");
	
	/** The value. */
	private final String value;

	private AdminDrawBackPage(String value) {
		this.value = value;

	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("adminDraw", path);
	}

	public String getNativeValue() {
		return value;
	}
	
	
}
