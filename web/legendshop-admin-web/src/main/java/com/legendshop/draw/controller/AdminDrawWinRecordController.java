/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.draw.constants.Constants;
import com.legendshop.draw.page.AdminDrawBackPage;
import com.legendshop.draw.page.AdminDrawPage;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.model.entity.draw.DrawWinRecord;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.spi.service.DrawWinRecordService;
import com.legendshop.util.AppUtils;

/**
 * 中奖纪录
 */
@Controller
@RequestMapping("/admin/drawWinRecord")
public class AdminDrawWinRecordController extends BaseController {

	@Autowired
	private DrawWinRecordService drawWinRecordService;

	@Autowired
	private DeliveryTypeService deliveryTypeService;

	/**
	 * (non-Javadoc) 查询中奖纪录
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, DrawWinRecord drawWinRecord,
			String curPageNO) {
		PageSupport<DrawWinRecord> ps = drawWinRecordService.queryDrawWinRecordPage(curPageNO, drawWinRecord);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("drawWinRecord", drawWinRecord);
		return PathResolver.getPath(AdminDrawPage.DRAW_WIN_RECORD_PAGE);
	}

	/**
	 * @Description: 删除
	 * @date 2016-4-19
	 */
	@RequestMapping(value = "/delete")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, Long id) {
		if (AppUtils.isBlank(id)) {
			return Constants.FAIL;
		}
		DrawWinRecord drawWinRecord = drawWinRecordService.getDrawWinRecord(id);
		drawWinRecordService.deleteDrawWinRecord(drawWinRecord);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return Constants.SUCCESS;
	}

	/**
	 * @Description: 跳转发货
	 * @date 2016-4-19
	 */
	@RequestMapping(value = "/toDeliverGoodsPage/{id}")
	public String toDeliverGoodsPage(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId();
		request.setAttribute("id", id);
		request.setAttribute("deliveryTypes", deliveryTypes);
		return PathResolver.getPath(AdminDrawBackPage.DRAW_WIN_RECORD_DELIVERGOODS);
	}

	/**
	 * @Description: 发货
	 * @date 2016-4-19
	 */
	@RequestMapping(value = "/saveDeliverGoods")
	public @ResponseBody String saveDeliverGoods(HttpServletRequest request, HttpServletResponse response,
			DrawWinRecord drawWinRecord) {
		try {
			if (AppUtils.isBlank(drawWinRecord) || AppUtils.isBlank(drawWinRecord.getId())) {
				return Constants.FAIL;
			}
			DrawWinRecord rel = drawWinRecordService.getDrawWinRecord(drawWinRecord.getId());
			if (AppUtils.isBlank(rel)) {
				return Constants.FAIL;
			}
			if (rel.getSendStatus() == 1) {
				return Constants.SENDED;
			}
			rel.setSendStatus(1);// 发货
			rel.setCourierId(drawWinRecord.getCourierId());
			rel.setCourierCompany(drawWinRecord.getCourierCompany());
			rel.setCourierNumber(drawWinRecord.getCourierNumber());
			drawWinRecordService.updateDrawWinRecord(rel);
		} catch (Exception e) {
			e.printStackTrace();
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}

}
