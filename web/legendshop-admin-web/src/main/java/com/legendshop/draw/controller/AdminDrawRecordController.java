/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.draw.page.AdminDrawPage;
import com.legendshop.model.entity.draw.DrawRecord;
import com.legendshop.spi.service.DrawRecordService;

/**
 * 
 * 活动抽奖记录
 */
@Controller
@RequestMapping("/admin/drawRecord")
public class AdminDrawRecordController extends BaseController {
	
	@Autowired
	private DrawRecordService drawRecordService;

	/**
	 * 查询抽奖记录列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param drawRecord
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			DrawRecord drawRecord) {
		PageSupport<DrawRecord> ps = drawRecordService.queryDrawRecordPage(curPageNO, drawRecord);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("drawRecord", drawRecord);
		return PathResolver.getPath(AdminDrawPage.DRAW_RECORD_PAGE);
	}

	/**
	 * 删除抽奖记录
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DrawRecord drawRecord = drawRecordService.getDrawRecord(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// drawRecord.getUserName());
		// if(result!=null){
		// return result;
		// }
		drawRecordService.deleteDrawRecord(drawRecord);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/drawRecord/query.htm";
	}

}
