/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.constants;

import com.legendshop.model.constant.AttributeKeys;

/**
 * LegendShop 常量.
 */
public class Constants implements AttributeKeys {

	/** 操作失败 */
	public static final String FAIL = "fail";

	/** 操作成功 */
	public static final String SUCCESS = "OK";

	/** 已经存在 */
	public static final String EXIST = "EXIST";

	/** 不存在 */
	public static final String NOTEXIST = "NOTEXIST";

	/** 已上线 */
	public static final String ROLLOUT = "rollout";

	/** 已发货 */
	public static final String SENDED = "sended";

	/** 已被使用 */
	public static final String USED = "used";

	public final static String OPENID = "openid";

	public static final String WX_REDIRECT_URL = "WX_REDIRECT_URL";

	/** mobile **/
	/** 下线 */
	public final static String NOT_START = "not_start";

	/** 过期 */
	public final static String OVERDUE = "overdue";

	/** 不中奖 */
	public final static String NOT_WINNING = "not_winning";

	/** 已中奖 */
	public final static String HAS_WIN = "has_win";

	/** 中奖 */
	public final static String WIN = "win";

	/** 抽奖总次数用完 */
	public final static String NUM_END = "num_end";

	/** 当天抽奖次数用完 */
	public final static String NUM_END_DAY = "num_end_day";

	/** 密匙不匹配 */
	public final static String NOT_CORRECT = "not_correct";

	/** 登录过期 */
	public final static String LONGIN_OVERDUE = "login_overdue";

}
