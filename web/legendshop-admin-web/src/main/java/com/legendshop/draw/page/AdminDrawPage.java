/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

public enum AdminDrawPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 抽奖活动**/
	DRAWACT_LIST_PAGE("/drawActivity/drawActivityList"),
	
	/** 抽奖活动管理 */
	DRAWACT_EDIT_PAGE("/drawActivity/drawActivity"),
	
	/**中奖纪录列表页面**/
	DRAW_WIN_RECORD_PAGE("/drawWinRecord/drawWinRecordList"),
	
	/**抽奖记录列表页面**/
	DRAW_RECORD_PAGE("/drawRecord/drawRecordList"), 
	
	DRAWACT_SELECT_COUPON("/drawActivity/loadDrawCoupon");
	
	/** The value. */
	private final String value;

	private AdminDrawPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("adminDraw", path);
	}

	public String getNativeValue() {
		return value;
	}
	
	
}
