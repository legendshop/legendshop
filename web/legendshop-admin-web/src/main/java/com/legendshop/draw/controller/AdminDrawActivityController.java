/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.draw.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.draw.constants.Constants;
import com.legendshop.draw.page.AdminDrawBackPage;
import com.legendshop.draw.page.AdminDrawPage;
import com.legendshop.model.dto.DrawAwardsDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.DrawActivityService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 抽奖活动
 *
 */
@Controller
@RequestMapping("/admin/drawActivity")
public class AdminDrawActivityController extends BaseController {

	@Autowired
	private DrawActivityService drawActivityService;
	
	@Autowired
	private CouponService couponService;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	/**
	 * @Description: 查询抽奖活动
	 * @date 2016-4-14
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			DrawActivity drawActivity) {
		PageSupport<DrawActivity> ps = drawActivityService.getDrawActivityPage(curPageNO, drawActivity);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("drawActivity", drawActivity);
		request.setAttribute("nowDate", new Date());
		return PathResolver.getPath(AdminDrawPage.DRAWACT_LIST_PAGE);
	}

	/**
	 * @Description: 保存
	 * @date 2016-4-14
	 */
	@SystemControllerLog(description="新增大转盘活动")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response,
			DrawActivity drawActivity) {
		try {
			if (AppUtils.isBlank(drawActivity) || AppUtils.isBlank(drawActivity.getAwardInfo())) {
				return Constants.FAIL;
			}
			Long id = drawActivityService.saveDrawActivity(drawActivity);
			if (AppUtils.isNotBlank(id)) {
				saveMessage(request,
						ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
			}

		} catch (Exception e) {
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}

	/**
	 * @Description: 链接二维码
	 * @date 2016-4-20
	 */
	@RequestMapping(value = "/qrCode/{id}", method = RequestMethod.GET)
	public String qrCode(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		// 手机端 域名 查看common.properties配置
		request.setAttribute("mobileDomainName", propertiesUtil.getMobileDomainName());
		request.setAttribute("itemId", id);
		return PathResolver.getPath(AdminDrawBackPage.DRAW_AWARDS_QRCODE);
	}

	/**
	 * @Description: 更改上下线状态
	 * @date 2016-4-15
	 */
	@SystemControllerLog(description="更改大转盘上下线状态")
	@RequestMapping(value = "/updateActStatus/{actId}/{actStatus}", method = RequestMethod.GET)
	public @ResponseBody String updateActStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long actId, @PathVariable Integer actStatus) {
		try {
			DrawActivity drawActivity = drawActivityService.getDrawActivityById(actId);
			if (drawActivity == null) {
				return Constants.FAIL;
			}

			if (actStatus == 1 && new Date().after(drawActivity.getEndTime())) {
				return Constants.OVERDUE; // 该活动已超过有限期
			}

			if (actStatus != drawActivity.getActStatus()) {
				drawActivity.setActStatus(actStatus);
				drawActivityService.updateDrawActivity(drawActivity);
			}
		} catch (Exception e) {
			return Constants.FAIL;
		}

		return Constants.SUCCESS;
	}

	/**
	 * @Description: 去往编辑页面
	 * @date 2016-4-15
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		DrawActivity drawActivity = drawActivityService.getDrawActivityById(id);
		List<DrawAwardsDto> daDtos = JSONUtil.getArray(drawActivity.getAwardInfo(), DrawAwardsDto.class);
		if (daDtos != null && daDtos.size() > 0) {
			for (DrawAwardsDto drawAwardsDto : daDtos) {
				if (drawAwardsDto.getAwardsType() == 4) {
					Coupon coupon = couponService.getCoupon(drawAwardsDto.getCoupon());
					drawAwardsDto.setDrawCoupon(coupon);
				}
			}
			drawActivity.setDrawAwardsDtoList(daDtos);
		}
		request.setAttribute("drawActivity", drawActivity);
		return PathResolver.getPath(AdminDrawPage.DRAWACT_EDIT_PAGE);
	}

	/**
	 * @Description: 删除
	 * @date 2016-4-15
	 */
	@SystemControllerLog(description="删除大转盘活动")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, Long id,
			Integer actStatus) {
		try {
			if (actStatus == 1) {
				return Constants.ROLLOUT;// 已上线
			}
			DrawActivity drawActivity = drawActivityService.getDrawActivityById(id);
			if (AppUtils.isBlank(drawActivity)) {
				return Constants.FAIL;
			}
			drawActivityService.deleteDrawActivity(drawActivity);
			saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		} catch (Exception e) {
			e.printStackTrace();
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}

	/**
	 * @Description: 跳转抽奖活动管理页面
	 * @date 2016-4-15
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminDrawPage.DRAWACT_EDIT_PAGE);
	}
	
	/**
	 * 大转盘红包的弹层
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopName
	 * @return
	 */
	@RequestMapping("/loadDrawCoupon/{level}")
	public String loadDrawCoupon(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String level, String curPageNO, String couponName, Date startDate, Date endDate) {
		PageSupport<Coupon> ps = couponService.loadSelectCouponByDraw(curPageNO, couponName, startDate, endDate);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("couponName", couponName);
		request.setAttribute("level", level);
		return PathResolver.getPath(AdminDrawPage.DRAWACT_SELECT_COUPON);
	}

}
