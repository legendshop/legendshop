/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.legendshop.core.handler.LegendHandler;

/**
 * 过滤器,代替LegendFilter,必须要优先执行,所以Order要设置小一点
 *
 */
@Order(-999)
@Component
@WebFilter(urlPatterns = "/*",filterName = "legendSystemFilter")
public class LegendSystemFilter implements Filter{

	@Autowired
	private LegendHandler legendHandler;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse resp = (HttpServletResponse)response;


		String url = req.getRequestURI();
		//不包括有后缀的URL,只是支持Controller的动作（不带后缀）
		boolean isSupportUrl = supportURL(url);



		legendHandler.doHandle(isSupportUrl, req, resp, chain);
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 不包括有后缀的URL,只是支持Controller的动作（不带后缀）
	 * 
	 * @param url
	 * @return
	 */
	private boolean supportURL(String url) {
		if (url == null) {
			return false;
		}
		return url.indexOf(".") < 0;
	}


}
