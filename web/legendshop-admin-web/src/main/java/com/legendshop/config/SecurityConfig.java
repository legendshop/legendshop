package com.legendshop.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.session.CompositeSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.springframework.security.web.session.ConcurrentSessionFilter;

import com.legendshop.core.handler.ShopCartHandler;
import com.legendshop.security.OriginUrlRedirectStrategy;
import com.legendshop.security.admin.AdminAuthenticationFilter;
import com.legendshop.security.admin.AdminDaoAuthenticationProvider;
import com.legendshop.security.handler.AuthenticationSuccessBusinessHandler;
import com.legendshop.security.handler.impl.AuthenticationSuccessBusinessHandlerImpl;
import com.legendshop.security.handler.impl.AuthenticationSuccessHandlerImpl;
import com.legendshop.security.handler.impl.MyLoginUrlAuthenticationEntryPoint;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.spi.service.security.AdminAuthService;

/**
 * 
 * Spring Security配置 暂时没有用上 @Newway
 *
 */
//@Configuration
public class SecurityConfig {

	/**
	 *  过滤器
	 * @param authenticationManager
	 * @param successHandler
	 * @param failureHandler
	 * @return
	 */
	@Bean
	public AdminAuthenticationFilter adminAuthenticationFilter(AuthenticationManager authenticationManager, SessionAuthenticationStrategy sessionAuthenticationStrategy, SimpleUrlAuthenticationSuccessHandler successHandler, SimpleUrlAuthenticationFailureHandler failureHandler) {
		AdminAuthenticationFilter filter = new AdminAuthenticationFilter();
		filter.setFilterProcessesUrl("/admin/j_spring_security_check");
		filter.setAuthenticationSuccessHandler(successHandler);
		filter.setAuthenticationFailureHandler(failureHandler);
		filter.setAuthenticationManager(authenticationManager);
		filter.setSessionAuthenticationStrategy(sessionAuthenticationStrategy);
		return filter;

	}
	
	
	@Bean
	public RedirectStrategy originUrlRedirectStrategy() {
		return new OriginUrlRedirectStrategy();
	}
	
	/**
	 * 管理员登录URL入口 
	 * @return
	 */
	@Bean
	public MyLoginUrlAuthenticationEntryPoint adminProcessingFilterEntryPoint() {
		return new MyLoginUrlAuthenticationEntryPoint("/adminlogin");
	}
	
	/**
	 *  管理员登录失败返回的地址定义
	 */
	@Bean
	public SimpleUrlAuthenticationFailureHandler adminSimpleUrlAuthenticationFailureHandler() {
		return new SimpleUrlAuthenticationFailureHandler("/adminlogin?error=1");
	}
	
	/**
	 * 5. 管理员登录成功返回的地址定义 
	 */
	@Bean
	public SimpleUrlAuthenticationSuccessHandler adminLoginLogAuthenticationSuccessHandler(RedirectStrategy redirectStrategy) {
		SimpleUrlAuthenticationSuccessHandler impl = new AuthenticationSuccessHandlerImpl();
		impl.setDefaultTargetUrl("/admin/index");
		impl.setRedirectStrategy(redirectStrategy);
		return impl;
	}
	
	/**
	 *  登录成功之后的动作
	 * @param basketService
	 * @param productService
	 * @param userGradeService
	 * @param shopCartHandler
	 * @return
	 */
	@Bean
	public AuthenticationSuccessBusinessHandler authenticationSuccessBusinessHandler(BasketService basketService, ProductService productService, UserGradeService userGradeService, ShopCartHandler shopCartHandler) {
		AuthenticationSuccessBusinessHandlerImpl handler = new AuthenticationSuccessBusinessHandlerImpl();
		handler.setBasketService(basketService);
		handler.setSupportSSO(false);
		handler.setProductService(productService);
		handler.setUserGradeService(userGradeService);
		handler.setShopCartHandler(shopCartHandler);
		return handler;
	}
	
	/**
	 * session并发控制器
	 */
	@Bean
	public ConcurrentSessionFilter adminConcurrencyFilter(SessionRegistry sessionRegistry) {
		ConcurrentSessionFilter filter = new ConcurrentSessionFilter(sessionRegistry, "/adminlogin?error=2");
		return filter;
	}
	
	/**
	 * 登出filter
	 * @return
	 */
	@Bean
    public LogoutFilter adminLogoutFilter() {
		LogoutFilter filter = new LogoutFilter("/adminlogin", new SecurityContextLogoutHandler());
		filter.setFilterProcessesUrl("/admin/logout");
		return filter;
	}
	
	/**
	 * 注意加入组合的session策略, 引入RegisterSessionAuthenticationStrategy才可以踢人走
	 * @param sessionRegistry
	 * @return
	 */
	@Bean
	public SessionAuthenticationStrategy sessionAuthenticationStrategy(SessionRegistry sessionRegistry) {
		
		List<SessionAuthenticationStrategy> delegateStrategies = new ArrayList<>();
		
		ConcurrentSessionControlAuthenticationStrategy strategy1 = new ConcurrentSessionControlAuthenticationStrategy(sessionRegistry);
		strategy1.setMaximumSessions(10);//最多可以有10个同名sesson存在
		delegateStrategies.add(strategy1);
		
		SessionFixationProtectionStrategy strategy2 = new SessionFixationProtectionStrategy();
		delegateStrategies.add(strategy2);
		
		RegisterSessionAuthenticationStrategy strategy3 = new RegisterSessionAuthenticationStrategy(sessionRegistry);
		delegateStrategies.add(strategy3);
		
		CompositeSessionAuthenticationStrategy strategy = new CompositeSessionAuthenticationStrategy(delegateStrategies);
		
		return strategy;	
	}
	
	/**
	 * 缓存
	 * @return
	 */
	@Bean
	public UserCache adminUserCache() {
		return new NullUserCache();
	}
	
	/**
	 *  管理员登陆
	 * @param adminAuthService
	 * @param userCache
	 * @return
	 */
	@Bean
	public AuthenticationProvider AdminDaoAuthenticationProvider(AdminAuthService adminAuthService, UserCache userCache) {
		AdminDaoAuthenticationProvider provider = new AdminDaoAuthenticationProvider();
		provider.setAdminAuthService(adminAuthService);
		provider.setUserCache(userCache);
		return provider;
	}
	
}
