/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.legendshop.base.MyStringToDateConverter;
import com.legendshop.core.helper.MappingExceptionResolver;
import com.legendshop.core.utils.MyMultipartResolver;
import com.legendshop.web.common.filter.ImageCacheFilter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web mvc的基本配置
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	
   /**
        * 拦截器
    */
   @Override
   public void addInterceptors(InterceptorRegistry registry) {
   }
   
	/**
	 * 增加日期转化器
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new MyStringToDateConverter());
	}
	
	/**
	 * 文件上传处理
	 * @return
	 */
	@Bean
	public MultipartResolver parseMultipartResolver() {
		return new MyMultipartResolver();
	}

	/**
	 * 图片过滤器
	 * 
	 */
	@Bean
	public FilterRegistrationBean<ImageCacheFilter> indexImageCacheFilterRegistration() {
		FilterRegistrationBean<ImageCacheFilter> registration = new FilterRegistrationBean<ImageCacheFilter>(
				new ImageCacheFilter());
		registration.addInitParameter("Cache-Control", "max-age=315360000");
		registration.addInitParameter("Access-Control-Max-Age", "315360000");
		registration.addInitParameter("Expires", "315360000");
		registration.addUrlPatterns("/photoserver/*");
		return registration;
	}

	/**
	 *  异常处理
	 */
	@Bean(name = "exceptionResolver")
	public HandlerExceptionResolver parseMappingExceptionResolver() {
		MappingExceptionResolver resolver = new MappingExceptionResolver();
		resolver.setDefaultErrorView("/common/errorFrame");
		resolver.setDefaultAdminErrorView("/common/errorAdminFrame");
		resolver.setWarnLogCategory("WARN");
		resolver.setDefaultStatusCode(500);

		return resolver;
	}

	/**
	 * fastjson支持
	 */
	@Bean
	public HttpMessageConverters fastjsonHttpMessageConverter() {
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
		
		FastJsonConfig fastJsonConfig = new FastJsonConfig();

		//返回值加入括号
		fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteMapNullValue, SerializerFeature.QuoteFieldNames);

		fastConverter.setFastJsonConfig(fastJsonConfig);
		
		HttpMessageConverter<?> converter = fastConverter;
		return new HttpMessageConverters(converter);
	}

	/**
	 * 数据库监控请求
	 * @return
	 */
	@Bean
	public ServletRegistrationBean servletTLReportServlet() {
		return new ServletRegistrationBean(new StatViewServlet(), "/system/druid/*");
	}

}
