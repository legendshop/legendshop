package com.legendshop.admin.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.util.RedisImCacheUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.HistoryMessageTypeEnum;
import com.legendshop.model.constant.ImagePurposeEnum;
import com.legendshop.model.dto.im.ChatBody;
import com.legendshop.model.dto.im.UploadResultDto;
import com.legendshop.model.entity.im.Dialogue;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;
import com.legendshop.websocket.service.DialogueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/admin/customer")
public class ImAdminController {
		
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private DialogueService dialogueService;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private RedisImCacheUtil redisImCacheUtil;
	
	@RequestMapping(value = "/im", method = RequestMethod.GET)
	public String conversation(HttpServletRequest request, HttpServletResponse response) {
				
		TreeMap<String, String> params = new TreeMap<String, String>();
		params.put("customId", "0");
		params.put("name", "平台客服");
		params.put("shopId", "0");
		params.put("validateTime",null);
		String sign = MD5Util.createSign(params, Constants._MD5);
		
		request.setAttribute("customId",0);
		request.setAttribute("name","平台客服");
		request.setAttribute("shopId",0);
		request.setAttribute("sign",sign);
		
		request.setAttribute("siteName","平台客服");
		request.setAttribute("IM_BIND_ADDRESS",propertiesUtil.getImBindAddress());
		return PathResolver.getPath(AdminPage.IM);
	}
	
	/**
	 * 获取指定用户的聊天记录
	 * @param request
	 * @param response
	 * @param fromUserId  // 发送用户id;
	 * @param userId // 接收用户id;
	 * @param beginTime // 消息开始时间;
	 * @param endTime // 消息结束时间
	 * @param offset // 分页偏移量
	 * @param count // 数量
	 * @return 
	 */
	@RequestMapping(value = "/user/message", method = RequestMethod.POST)
	public @ResponseBody
	List<ChatBody> message(HttpServletRequest request, HttpServletResponse response,
						   String fromUserId,
						   String userId, // 接收用户id;
						   Double beginTime, //消息的开始时间
						   Double endTime,
						   Integer offset, Integer count) {

		//用户获取客服的消息
		return dialogueService.getImMessage(0L, fromUserId, userId, offset, HistoryMessageTypeEnum.CUSTOMER.value());
	}
	
	/**
	 * 上传聊天图片
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody Map<String,String> upload(HttpServletRequest request, HttpServletResponse response,MultipartFile sendImgFile) throws IOException {
		HashMap<String, String> map = new HashMap<String, String>();
		if (AppUtils.isBlank(sendImgFile)) {
			map.put("status", Constants.FAIL);
			map.put("message", "请选择上传图片");
			return map;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();


		// 处理图片上传
		UploadResultDto uploadResultDto= attachmentManager.uploadImage(ImagePurposeEnum.ADMIN_FILE,userId, userName,0l,sendImgFile);
		if (AppUtils.isBlank(uploadResultDto)){
			map.put("status", Constants.FAIL);
			map.put("message", "上传失败，请稍后重试");
			return map;
		}
		if (!uploadResultDto.isSuccess()){
			map.put("status", Constants.FAIL);
			map.put("message", uploadResultDto.getMessage());
			return map;
		}
		map.put("status", Constants.SUCCESS);
		map.put("path", uploadResultDto.getFirstPath());
		return map;
	}
	
	/**
	 * 本店订单
	 */
	@RequestMapping(value="/order", method = RequestMethod.POST)
	public String order(HttpServletRequest request, HttpServletResponse response,String userId) {
		/*CustomServer customServer = (CustomServer) request.getSession().getAttribute(SERVER_ACCOUNT);
		if (AppUtils.isBlank(customServer)) {
			return "login";
		}
		if(AppUtils.isBlank(userId)){
			request.setAttribute("imOrderList", null);
			return PathResolver.getPath(request, response, ShopFrontPage.CONVERSATION_ORDER);
		}
		List<ImOrderDto> imOrderList = orderService.getImOrderDto(userId,customServer.getShopId());
		request.setAttribute("imOrderList", imOrderList);*/
		return null;
	}
	
	
	
	/**
	 * 咨询商品内容
	 */
	@RequestMapping(value = "/getConsultProd", method = RequestMethod.POST)
	@ResponseBody
	public String getConsultProd(HttpServletRequest request, HttpServletResponse response,String userId) {
		Dialogue dialogue=dialogueService.getDialogue(userId,0l,0l);
		return AppUtils.isBlank(dialogue)? null : dialogue.getConsultContent();
	}
	
	
}
