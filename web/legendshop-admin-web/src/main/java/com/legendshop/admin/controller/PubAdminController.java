/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.ConflictException;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.util.DateUtils;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Pub;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.PubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.TimerUtil;

/**
 * 商城公告控制器.
 */
@Controller
@RequestMapping("/admin/pub")
public class PubAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(PubAdminController.class);

	@Autowired
	private PubService pubService;

	/**
	 * 查询商城公告.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pub
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Pub pub) {
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request,
				new String[] { "recDate", "endDate", "startDate" });

		PageSupport<Pub> ps = pubService.getPubListPage(curPageNO, pub, result);
		PageSupportHelper.savePage(request, ps);

		request.setAttribute("bean", pub);
		String path = PathResolver.getPath(AdminPage.PUB_LIST_PAGE);
		return path;
	}

	/**
	 * 检查Pub是否有效
	 * 
	 * @param list
	 * @return
	 */
	private List<Pub> checkValidPub(List<Pub> list) {
		if (AppUtils.isBlank(list)) {
			return null;
		}
		Date today = TimerUtil.getNowDateShort();
		for (Pub pub : list) {
			if ((pub.getStartDate() != null && pub.getStartDate().after(today))
					|| (pub.getEndDate() != null && pub.getEndDate().before(today))) {
				pub.setValid(false);
			}
		}
		return list;
	}

	/**
	 * 保存商城公告.
	 * 
	 * @param request
	 * @param response
	 * @param pub
	 * @return the string
	 */
	@SystemControllerLog(description="保存商城公告")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Pub pub) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		pub.setUserId(securityUserDetail.getUserId());
		pub.setUserName(securityUserDetail.getUsername());
		if(pub.getStartDate() != null) {
			pub.setStartDate(DateUtils.getIntegralEndTime(pub.getStartDate()));
		}
		if(pub.getEndDate() != null) {
			pub.setEndDate(DateUtils.getIntegralEndTime(pub.getEndDate()));
		}
		if (AppUtils.isNotBlank(pub.getStartDate()) && AppUtils.isNotBlank(pub.getEndDate())) {
			if (pub.getStartDate().after(pub.getEndDate())) {
				throw new ConflictException("Date range error, Start date can not late then end date");
			}
		}
		pubService.save(pub);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.PUB_LIST_QUERY) + "?type=" + pub.getType();
	}

	/**
	 * 删除商城公告.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="删除商城公告")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Pub pub = pubService.getPubById(id);
		if (pub == null) {
			throw new NotFoundException("Pub does not exists");
		}
		log.info("{} delete Pub Title {}", pub.getUserName(), pub.getTitle());
		pubService.deletePub(pub);
		saveMessage(request, ResourceBundleHelper.getDeleteString());
		return PathResolver.getPath(RedirectPage.PUB_LIST_QUERY);
	}

	/**
	 * 加载商城公告详情.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Pub pub = pubService.getPubById(id);
		request.setAttribute("bean", pub);
		return PathResolver.getPath(AdminPage.PUB_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PUB_EDIT_PAGE);
	}

	/**
	 * 更改公告状态
	 * 
	 * @param request
	 * @param response
	 * @param pubId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更改公告状态")
	@RequestMapping(value = "/updatestatus/{pubId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long pubId, @PathVariable Integer status) {
		Pub pub = pubService.getPubById(pubId);
		if (pub == null) {
			return -1;
		}
		if (!status.equals(pub.getStatus())) {
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)
					|| Constants.STOPUSE.equals(status)) {
				pub.setStatus(status);
				pub.setRecDate(new Date());
				pubService.update(pub);
			}
		}
		return pub.getStatus();
	}

}
