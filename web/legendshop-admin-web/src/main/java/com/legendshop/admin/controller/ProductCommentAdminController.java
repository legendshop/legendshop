/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProdAddCommService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.spi.service.ProductCommentStatService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;

/**
 * 用户评论控制器.
 */
@Controller
@RequestMapping("/admin/productcomment")
public class ProductCommentAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductCommentAdminController.class);

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ProdAddCommService prodAddCommService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 查找用户评论
	 * @param curPageNO
	 * @param productComment
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductComment productComment) {
		request.setAttribute("bean", productComment);
		if (AppUtils.isNotBlank(productComment.getStatus())) {
			request.setAttribute("status", productComment.getStatus());
		}
		return PathResolver.getPath(AdminPage.PROD_COMM_LIST_PAGE);
	}
	
	/**
	 * 查找用户评论
	 * @param curPageNO
	 * @param productComment
	 * @return the string
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductComment productComment) {
		
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		} else {
			pageSize = systemParameterUtil.getExportSize();
		}
		
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request,
				new String[] { "score", "addtime", "status" });
		boolean commentNeedReview = systemParameterUtil.isCommentNeedReview();
		request.setAttribute("commentNeedReview", commentNeedReview);
		String status = null;
//		if (commentNeedReview) {
			status = request.getParameter("status");
			if (AppUtils.isNotBlank(status)) {
				request.setAttribute("status", status);
			}
//		}
		
		PageSupport<ProductCommentDto> ps = productCommentService.getProductCommentList(curPageNO, productComment,
				pageSize, status, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", productComment);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.PROD_COMM_CONTENT_LIST_PAGE);
	}

	/**
	 * 保存评论
	 * @param request
	 * @param response
	 * @param productComment
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ProductComment productComment) {
		// only for update
		ProductComment origin = productCommentService.getProductCommentById(productComment.getId());
		if (origin == null) {
			throw new NotFoundException("ProductComment Not Found");
		}

		origin.setContent(XssFilterUtil.cleanXSS(productComment.getContent()));//去掉关键字,防止攻击
		productCommentService.update(origin);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.PROD_COMM_LIST_QUERY);
	}

	/**
	 * 根据id删除评论
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除评论")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		ProductComment productComment = productCommentService.getProductCommentById(id);
		if (productComment != null) {
			log.info("{}, delete ProductComment Addtime {}, delete person",
					new Object[] { productComment.getUserName(), productComment.getAddtime(), userName });
			productCommentService.delete(id);
			// 重新计算商品的 评论数和 分数
			productCommentStatService.reCalculateProdComments(productComment.getProdId());
		}
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.PROD_COMM_LIST_QUERY);
	}

	/**
	 * 加载编辑评论
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductComment productComment = productCommentService.getProductCommentById(id);
		if (productComment == null) {
			throw new NotFoundException("ProductComment not found with Id " + id);
		}
		Product product = productService.getProductById(productComment.getProdId());
		productComment.setProdName(product.getName());
		request.setAttribute("bean", productComment);
		return PathResolver.getPath(AdminPage.PROD_COMM_EDIT_PAGE);
	}

	/**
	 * 查看评论详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/detail/{id}")
	public String getCommentDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ProductCommentDto productComment = productCommentService.getProductCommentDetail(id,userId);

		request.setAttribute("bean", productComment);
		return PathResolver.getPath(AdminPage.PROD_COMM_DETAIL_PAGE);
	}

	/**
	 * 修改更新评论
	 * @param request
	 * @param response
	 * @param productComment
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response,
			@PathVariable ProductComment productComment) {
		productCommentService.update(productComment);
		return PathResolver.getPath(FowardPage.PROD_COMM_LIST_QUERY);
	}

	/**
	 * 审核初次评论
	 * 
	 * @param id
	 *            评论ID
	 * @param audit
	 *            1通过 -1不通过
	 * @return
	 */
	@SystemControllerLog(description="审核初次评论")
	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	@ResponseBody
	public String audit(HttpServletRequest request, HttpServletResponse response, @RequestParam Long id,
			@RequestParam Integer audit) {
		try {
			if (audit != 1 && audit != -1) {
				return "对不起, 请求参数错误!";
			}
			ProductComment productComment = productCommentService.getProductCommentById(id);

			if (null == productComment) {
				return "对不起, 您要审核的评论不存在或已被删除!";
			}

			if (!productComment.getStatus().equals(ProdCommStatusEnum.WAIT_AUDIT.value())) {
				return "对不起, 请不要非法操作!";
			}

			productCommentService.auditFirstComment(productComment, audit);

			return Constants.SUCCESS;
		} catch (Exception e) {
			log.error("审核用户初次评论失败!", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 审核追加评论
	 * 
	 * @param id
	 *            评论ID
	 * @param audit
	 *            1通过 -1不通过
	 * @return
	 */
	@SystemControllerLog(description="审核追加评论")
	@RequestMapping(value = "/auditAdd", method = RequestMethod.POST)
	@ResponseBody
	public String auditAppend(HttpServletRequest request, HttpServletResponse response, @RequestParam Long id,
			@RequestParam Integer audit) {
		try {
			if (audit != 1 && audit != -1) {
				return "对不起, 请求参数错误!";
			}
			ProductComment productComment = productCommentService.getProductCommentById(id);

			if (null == productComment) {
				return "对不起, 您要审核的评论不存在或已被删除!";
			}

			if (!productComment.getStatus().equals(ProdCommStatusEnum.SUCCESS.value())
					&& !productComment.getIsAddComm()) {
				return "对不起, 请不要非法操作!";
			}

			ProdAddComm prodAddComm = prodAddCommService.getProdAddCommByCommId(productComment.getId());

			if (null == prodAddComm) {
				return "对不起, 您要审核的追加评论不存在或已被删除!";
			}

			if (!prodAddComm.getStatus().equals(ProdCommStatusEnum.WAIT_AUDIT.value())) {
				return "对不起, 请不要非法操作!";
			}

			prodAddComm.setStatus(audit);
			prodAddCommService.updateProdAddComm(prodAddComm);

			return Constants.SUCCESS;
		} catch (Exception e) {
			log.error("审核用户追加评论失败!", e);
			return Constants.FAIL;
		}
	}
}
