/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewsitem;
import com.legendshop.spi.service.WeixinNewsitemService;

/**
 * The Class WeixinNewsitemController
 *
 */
@Controller
@RequestMapping("/admin/weixinNewsitem")
public class WeixinNewsitemController extends BaseController{
    @Autowired
    private WeixinNewsitemService weixinNewsitemService;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinNewsitem weixinNewsitem) {
        //cq.setPageSize(propertiesUtil.getPageSize());
        //DataFunctionUtil.hasAllDataFunction(cq, request, StringUtils.trim(cash.getUserName()));
        /*
           //TODO add your condition
        */
        PageSupport<WeixinNewsitem> ps = weixinNewsitemService.getWeixinNewsitemPage(curPageNO);
        PageSupportHelper.savePage(request,ps);
        request.setAttribute("weixinNewsitem", weixinNewsitem);
        
        return "/weixinNewsitem/weixinNewsitemList";  
        //TODO, replace by next line, need to predefined BackPage parameter
       // return PathResolver.getPath(request, response, BackPage.WEIXINNEWSITEM_LIST_PAGE);
    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, WeixinNewsitem weixinNewsitem) {
        weixinNewsitemService.saveWeixinNewsitem(weixinNewsitem);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return "forward:/admin/weixinNewsitem/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
		//return PathResolver.getPath(request, response, FowardPage.WEIXINNEWSITEM_LIST_QUERY);
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinNewsitem weixinNewsitem = weixinNewsitemService.getWeixinNewsitem(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinNewsitem.getUserName());
		//if(result!=null){
			//return result;
		//}
		weixinNewsitemService.deleteWeixinNewsitem(weixinNewsitem);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/weixinNewsitem/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
        //return PathResolver.getPath(request, response, FowardPage.WEIXINNEWSITEM_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinNewsitem weixinNewsitem = weixinNewsitemService.getWeixinNewsitem(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinNewsitem.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("weixinNewsitem", weixinNewsitem);
        return "/weixinNewsitem/weixinNewsitem";
         //TODO, replace by next line, need to predefined FowardPage parameter
         //return PathResolver.getPath(request, response, BackPage.WEIXINNEWSITEM_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/weixinNewsitem/weixinNewsitem";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.WEIXINNEWSITEM_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        WeixinNewsitem weixinNewsitem = weixinNewsitemService.getWeixinNewsitem(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinNewsitem.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("weixinNewsitem", weixinNewsitem);
		return "forward:/admin/weixinNewsitem/query.htm";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.WEIXINNEWSITEM_EDIT_PAGE);
    }

}
