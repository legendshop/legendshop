/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Tag;
import com.legendshop.model.entity.TagItems;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.TagService;
import com.legendshop.util.AppUtils;

/**
 *  标签管理控制器
 * 
 */
@Controller
@RequestMapping("/admin/tag")
public class TagController extends BaseController{
	
	@Autowired
	private TagService tagService;

	/**
	 * 查询标签列表
	 */
	
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Tag tag) {
		PageSupport<Tag> ps = tagService.getTagPage(curPageNO, tag);

		List<Tag> list = (List<Tag>) ps.getResultList();
		if (AppUtils.isNotBlank(list)) {
			List<TagItems> TagItemsList = tagService.queryTagItemsList(list);
			for (Tag tag2 : list) {
				for (TagItems tagItem : TagItemsList) {
					tag2.addTagItems(tagItem);
				}
			}
		}
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("tag", tag);
		return PathResolver.getPath(AdminPage.TAG_LIST_PAGE);
	}

	/**
	 * 保存标签
	 */
	
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Tag tag) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		tag.setUserId(userId);
		tag.setUserName(userName);
		tagService.saveTag(tag);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.TAG_QUERY);
	}

	/**
	 * 删除标签
	 */
	
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Tag tag = tagService.getTag(id);
		TagItems TagItems = tagService.getItemsByTagId(tag.getId());
		tagService.deleteTag(tag,TagItems);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.TAG_QUERY);

	}

	/**
	 * 加载标签编辑页面
	 */
	
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		Tag tag = new Tag();
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		tag.setUserName(user.getUsername());
		request.setAttribute("tag", tag);
		return PathResolver.getPath(AdminPage.TAG_EDIT_PAGE);
	}

	/**
	 *  删除标签
	 */
	
	@RequestMapping(value = "/update/{id}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Tag tag = tagService.getTag(id);
		request.setAttribute("tag", tag);
		return PathResolver.getPath(AdminPage.TAG_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody Long updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long id, @PathVariable Long status) {
		Tag tag = tagService.getTag(id);
		if (tag == null) {
			return (long) -1;
		}
		if (!status.equals(tag.getStatus())) {
			SecurityUserDetail user = UserManager.getUser(request);
			String loginName = UserManager.getShopUserName(user);
			if (!loginName.equals(tag.getUserName())) {
				return (long) -1;
			}
			tag.setStatus((long) status);
			tagService.updateTag(tag);
		}
		return tag.getStatus();
	}

	/**
	 * 添加标签子标签
	 * @param request
	 * @param response
	 * @param tagId
	 * @return
	 */
	@RequestMapping(value = "/AddTagItems/{tagId}")
	public String AddTagItems(HttpServletRequest request, HttpServletResponse response, @PathVariable Long tagId) {
		request.setAttribute("tagId", tagId);
		return PathResolver.getPath(AdminPage.TAGITEMS);
	}

	/**
	 * 查看标签子标签
	 * @param request
	 * @param response
	 * @param itemId
	 * @return
	 */
	@RequestMapping(value = "/loadTagItems/{itemId}")
	public String loadTagItems(HttpServletRequest request, HttpServletResponse response, @PathVariable Long itemId) {
		TagItems tagItems = tagService.getTagItems(itemId);
		request.setAttribute("tagItems", tagItems);
		request.setAttribute("tagId", tagItems.getTagId());
		return PathResolver.getPath(AdminPage.TAGITEMS);
	}

	/**
	 * 保存标签子标签
	 * @param request
	 * @param response
	 * @param tagItems
	 * @return
	 */
	@RequestMapping(value = "/saveTagItems")
	public String saveTagItems(HttpServletRequest request, HttpServletResponse response, TagItems tagItems) {
		tagService.saveTagItems(tagItems);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.TAG_QUERY);
	}

	/**
	 * 删除标签子标签
	 * @param request
	 * @param response
	 * @param itemId
	 * @return
	 */
	@RequestMapping(value = "/deleteTagItems/{itemId}")
	public String deleteTagItems(HttpServletRequest request, HttpServletResponse response, @PathVariable Long itemId) {
		tagService.deleteTagItems(itemId);
		return PathResolver.getPath(FowardPage.TAG_QUERY);
	}

}
