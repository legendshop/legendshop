/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Admin;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticle;
import com.legendshop.spi.service.GrassArticleService;

/**
 * 种草社区文章表控制器
 */
@Controller
@RequestMapping("/admin/grassArticle")
public class GrassArticleController extends BaseController{

	/** 日志. */
	private final Logger log = LoggerFactory.getLogger(GrassArticle.class);
	
    @Autowired
    private GrassArticleService grassArticleService;

	/**
	 * 种草社区文章表列表查询
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,String name) {
        PageSupport<GrassArticle> ps = grassArticleService.queryGrassArticlePage(curPageNO,name, 10);//TODO 10为每页的记录数, 可以页面传递过来或者从PropertiesUtil中获取
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("name", name);
        return PathResolver.getPath(AdminPage.GRASS_ARTICLE_LIST_PAGE);
    }

	/**
	 * 保存种草社区文章表
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, GrassArticle grassArticle) {
    
      if(grassArticle.getId() != null){//update
		GrassArticle entity = grassArticleService.getGrassArticle(grassArticle.getId());
		if (entity != null) {
				grassArticleService.updateGrassArticle(entity);
			}
		}else{//save
			grassArticleService.saveGrassArticle(grassArticle);
		}
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
        return "forward:/admin/grassArticle/query";
    }

	/**
	 * 删除种草社区文章表
	 */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
    	GrassArticle grassArticle = grassArticleService.getGrassArticle(id);
		if(grassArticle != null){
			log.info("{} delete grassArticle by Id {}", grassArticle.getId());
			grassArticleService.deleteGrassArticle(grassArticle);
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
        return "redirect:/admin/grassArticle/query";
    }

	/**
	 * 根据Id加载种草社区文章表
	 */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        GrassArticle grassArticle = grassArticleService.getGrassArticle(id);
        request.setAttribute("grassArticle", grassArticle);
        return PathResolver.getPath(AdminPage.GRASS_ARTICLE_PAGE);
    }
    
   /**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.GRASS_ARTICLE_PAGE);
	}


}
