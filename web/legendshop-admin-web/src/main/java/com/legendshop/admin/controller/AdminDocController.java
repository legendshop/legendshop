/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminDoc;
import com.legendshop.spi.service.AdminDocService;
import com.legendshop.util.AppUtils;

/**
 * 帮助管理控制器
 */
@Controller
@RequestMapping("/admin/adminDoc")
public class AdminDocController extends BaseController{
	@Autowired
	private AdminDocService adminDocService;

	/**
	 * 列表查询
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, AdminDoc adminDoc) {
		PageSupport<AdminDoc> ps = adminDocService.queryAdminDocList(curPageNO, adminDoc);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("adminDoc", adminDoc);
		return PathResolver.getPath(AdminPage.ADMIN_DOC_LIST_PAGE);
	}

	/**
	 * 保存帮助文档
	 */
	@SystemControllerLog(description="保存帮助文档")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, AdminDoc adminDoc) {
		adminDocService.saveAdminDoc(adminDoc);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getRedirectPath(RedirectPage.ADMIN_DOC_LIST.getNativeValue());
	}

	/**
	 * 删除帮助文档
	 */
	@SystemControllerLog(description=" 删除帮助文档")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AdminDoc adminDoc = adminDocService.getAdminDoc(id);
		adminDocService.deleteAdminDoc(adminDoc);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getRedirectPath(RedirectPage.ADMIN_DOC_LIST.getNativeValue());
	}

	/**
	 * 根据Id加载
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AdminDoc adminDoc = adminDocService.getAdminDoc(id);
		request.setAttribute("adminDoc", adminDoc);
		return PathResolver.getPath(AdminPage.ADMIN_DOC_EDIT_PAGE);
	}

	/**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(AdminPage.ADMIN_DOC_EDIT_PAGE);
	}

	/**
	 * 加载显示页面
	 */
	@RequestMapping(value = "/view/{id}")
	public String view(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AdminDoc adminDoc = adminDocService.getAdminDoc(id);
		request.setAttribute("adminDoc", adminDoc);
		 return PathResolver.getPath(AdminPage.ADMIN_DOC_PAGE);
	}

	/**
	 * 更新编辑页面
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AdminDoc adminDoc = adminDocService.getAdminDoc(id);
		request.setAttribute("adminDoc", adminDoc);
		return "forward:/admin/adminDoc/query.htm";
	}

	/**
	 * 根据Id加载
	 */
	@RequestMapping(value = "/checkAdminDocId")
	@ResponseBody
	public Boolean checkAdminDocId(HttpServletRequest request, HttpServletResponse response, Long id) {
		AdminDoc adminDoc = adminDocService.getAdminDoc(id);
		if (AppUtils.isBlank(adminDoc)) {
			return false;
		}
		return true;
	}
}
