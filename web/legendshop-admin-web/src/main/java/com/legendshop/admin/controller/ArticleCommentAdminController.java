/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ArticleComment;
import com.legendshop.spi.service.ArticleCommentService;

/**
 * 文章评论管理控制器
 */
@Controller
@RequestMapping("/admin/article/comment")
public class ArticleCommentAdminController extends BaseController{
	@Autowired
	private ArticleCommentService articleCommentService;

	/**
	 * 列表查询
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ArticleComment articleComment) {
		PageSupport<ArticleComment> ps = articleCommentService.getArticleCommentPage(curPageNO, articleComment);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("articleComment", articleComment);

		return PathResolver.getPath(AdminPage.ARTICLE_COMMENT_LIST_PAGE);
	}

	/**
	 * 更改杂志评论的审核状态
	 * 
	 * @param request
	 * @param response
	 * @param articleCommentId
	 *            评论id
	 * @param status
	 *            状态
	 * @return
	 */
	@SystemControllerLog(description="更改杂志评论的审核状态")
	@ResponseBody
	@RequestMapping(value = "/updateStatus")
	public String UpdateStatus(HttpServletRequest request, HttpServletResponse response, Long articleCommentId, int status) {
		ArticleComment articleComment = articleCommentService.getArticleComment(articleCommentId);
		int result = articleCommentService.updateStatus(articleComment, status);
		if (result > 0) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}

	}

	/**
	 * 保存
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ArticleComment articleComment) {
		articleCommentService.saveArticleComment(articleComment);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/articleComment/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.ARTICLECOMMENT_LIST_QUERY);
	}

	/**
	 * 删除杂志评论
	 */
	@SystemControllerLog(description="删除杂志评论")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ArticleComment articleComment = articleCommentService.getArticleComment(id);
		articleCommentService.deleteArticleComment(articleComment);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.ARTICLE_COMMENT_LIST);
	}

	/**
	 * 根据Id加载
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ArticleComment articleComment = articleCommentService.getArticleCommentLinkArt(id);
		request.setAttribute("articleComment", articleComment);
		return PathResolver.getPath(AdminPage.ARTICLE_COMMENT_INFO_PAGE);
	}

	/**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/articleComment/articleComment";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ARTICLECOMMENT_EDIT_PAGE);
	}

	/**
	 * 更新编辑页面
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ArticleComment articleComment = articleCommentService.getArticleComment(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// articleComment.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("articleComment", articleComment);
		return "forward:/admin/articleComment/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ARTICLECOMMENT_EDIT_PAGE);
	}

}
