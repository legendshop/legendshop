/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SysParamGroupEnum;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.spi.service.SystemParameterService;

/**
 * 系统参数控制器.
 */
@Controller
@RequestMapping("/admin/system/systemParameter")
public class SystemParameterController extends BaseController {

	/** The system parameter service. */
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Autowired
	private SystemParameterProvider systemParameterProvider;
		
	/**
	 * 查询全部的系统参数列表.
	 * 
	 */
	@RequestMapping("/query")
	public String queryAll(HttpServletRequest request, HttpServletResponse response, String curPageNO,String name,String groupId) {
		PageSupport<SystemParameter> ps = systemParameterService.getSystemParameterListPage(curPageNO, name, groupId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("groupId", groupId);
		request.setAttribute("name", name);
		return PathResolver.getPath(AdminPage.PARAM_LIST_PAGE);
	}

	
	
	/**
	 * 保存系统参数.
	 * 
	 * @param request
	 * @param response
	 * @param systemParameter
	 * @return the string
	 */
	@SystemControllerLog(description="保存系统参数")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, SystemParameter systemParameter) {
		SystemParameter parameter = systemParameterService.getSystemParameter(systemParameter.getName());
		/*String groupId = SysParamGroupEnum.SYS.name();*/
		String groupId = systemParameter.getGroupId();
		if (parameter != null && "Y".equals(parameter.getChangeOnline())) {//是否可以界面上编辑
			groupId = parameter.getGroupId();
			parameter.setValue(systemParameter.getValue());
			
			//1. 一定要先更新数据库
			systemParameterService.update(parameter);
			
			//2. 更改系统配置事件,同时更新子节点, 重新初始化数据
			systemParameterProvider.setParameter(parameter);
			saveMessage(request, ResourceBundleHelper.getSucessfulString());
		} else {
			saveError(request, "该参数不支持界面上直接编辑");
		}
		return RedirectPage.PARAM_LIST_QUERY.getValue()+"?groupId="+groupId;
	}

	/**
	 * 删除系统参数.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="删除系统参数")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		// systemParameterService.delete(id);
		saveMessage(request, ResourceBundleHelper.getErrorString());
		return PathResolver.getPath(RedirectPage.PARAM_LIST_QUERY);
	}

	/**
	 * 查看系统参数详情.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		SystemParameter systemParameter = systemParameterService.getSystemParameter(id);
		if("BILL_PERIOD_TYPE".equals(id)){
			request.setAttribute("rs", false);
		}
		request.setAttribute("bean", systemParameter);
		request.setAttribute("id", id);
		return PathResolver.getPath(AdminPage.PARAM_EDIT_PAGE);
	}

	/**
	 * 更新系统参数.
	 * 
	 * @param request
	 * @param response
	 * @param systemParameter
	 * @return the string
	 */
	@SystemControllerLog(description="更新系统参数")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable SystemParameter systemParameter) {
		systemParameterService.update(systemParameter);
		return PathResolver.getPath(RedirectPage.PARAM_LIST_QUERY);
	}

}
