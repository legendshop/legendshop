/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.order.SubOrderItemDto;
import com.legendshop.model.dto.order.UsrAddrSubDto;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.MyPersonInfo;
import com.legendshop.model.entity.OrderSub;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.spi.service.ExpensesRecordService;
import com.legendshop.spi.service.LocationService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserCenterPersonInfoService;
import com.legendshop.spi.service.VisitLogService;
import com.legendshop.util.AppUtils;

/**
 * 会员信息控制器
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/userinfo")
public class UserInfoAdminController {

	@Autowired
	private UserCenterPersonInfoService userCenterPersonInfoService;

	@Autowired
	private SubService subService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private ExpensesRecordService expensesRecordService;

	@Autowired
	private VisitLogService visitLogService;

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private ProductConsultService productConsultService;
	
	@Autowired
    private LocationService locationService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 会员基本资料
	 */
	@RequestMapping("/userDetail/{userId}")
	public String userDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId) {
		MyPersonInfo myPersonInfo = userCenterPersonInfoService.getUserCenterPersonInfo(userId);
		/*
		 * //处理手机号码 if(AppUtils.isNotBlank(myPersonInfo.getUserMobile())){
		 * String mobile = mobileSubstitution(myPersonInfo.getUserMobile());
		 * myPersonInfo.setUserMobile(mobile); }
		 * 
		 * //处理邮箱号码 if(AppUtils.isNotBlank(myPersonInfo.getUserMail())){ String
		 * mail = mailSubstitution(myPersonInfo.getUserMail());
		 * myPersonInfo.setUserMail(mail); }
		 */
		//获取地址
        if (AppUtils.isNotBlank(myPersonInfo.getProvinceid())) {
            String province = locationService.getProvinceById(Integer.parseInt(myPersonInfo.getProvinceid())).getProvince();
            myPersonInfo.setProvince(province);
        }
        if (AppUtils.isNotBlank(myPersonInfo.getCityid())) {
            String city = locationService.getCityById(Integer.parseInt(myPersonInfo.getCityid())).getCity();
            myPersonInfo.setCity(city);
        }
        if (AppUtils.isNotBlank(myPersonInfo.getAreaid())) {
            String area = locationService.getAreaById(Integer.parseInt(myPersonInfo.getAreaid())).getArea();
            myPersonInfo.setArea(area);
        }
        System.out.println("生日格式："+myPersonInfo.getBirthDate());
		UserGrade userGrade = userCenterPersonInfoService.getUserGrade(userId);
		request.setAttribute("userGrade", userGrade);
		request.setAttribute("MyPersonInfo", myPersonInfo);
		request.setAttribute("userId", userId);
		return PathResolver.getPath(AdminPage.USER_INFO);
	}

	/**
	 * 会员消费记录
	 */
	@RequestMapping("/expensesRecord/{userId}")
	public String expensesRecord(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable String userId, String userName) {
		PageSupport<ExpensesRecord> ps = expensesRecordService.getExpensesRecordPage(curPageNO, userId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(AdminPage.EXPENSES_RECORD);
	}

	/**
	 * 会员浏览历史
	 * 
	 * @throws ParseException
	 */
	@RequestMapping("/userVisitLog/{userName}")
	public String userVisitLog(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			VisitLog visitLog, String userId, @PathVariable String userName) throws ParseException {
		PageSupport<VisitLog> ps = visitLogService.getVisitLogListPage(curPageNO, userName, visitLog);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		request.setAttribute("bean", visitLog);
		return PathResolver.getPath(AdminPage.USER_VISIT_LOG);
	}

	/**
	 * 会员商品评论
	 */
	@RequestMapping("/userProdComm/{userId}")
	public String userProdComm(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductComment productComment, @PathVariable String userId, String userName) {

		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request,
				new String[] { "score", "addtime", "status" });

		PageSupport<ProductCommentDto> ps = productCommentService.getProductCommentListPage(curPageNO, productComment,
				userId, userName, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", productComment);

		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(AdminPage.USER_PROD_COMM);
	}

	/**
	 * 会员商品咨询
	 */
	@RequestMapping("/userProdCons/{userName}")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductConsult productConsult, String userId, @PathVariable String userName) {
		PageSupport<ProductConsult> ps = productConsultService.getProductConsultPage(userName, productConsult,
				curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productConsult", productConsult);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(AdminPage.USER_PRODCONS);
	}

	/**
	 * 会员订单信息
	 */
	@RequestMapping("/userOrderInfo/{userName}")
	public String userOrderInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable String userName, OrderSearchParamDto paramDto) {
		paramDto.setUserName(userName);
		buildAdminOrderSql(request, paramDto);
		return PathResolver.getPath(AdminPage.USER_ORDER_INFO);
	}

	/**
	 * 创建订单管理sql
	 * @param request
	 * @param paramDto
	 */
	private void buildAdminOrderSql(HttpServletRequest request, OrderSearchParamDto paramDto) {

		int pageSize = 10;// 默认一页显示条数
		PageSupport ps = subService.getOrdersPage(paramDto, pageSize);
		List<OrderSub> orders = (List<OrderSub>) ps.getResultList();

		List<MySubDto> myorderDtos = new ArrayList<MySubDto>();
		if (AppUtils.isNotBlank(orders)) {
			for (int i = 0; i < orders.size(); i++) {
				OrderSub sub = orders.get(i);
				MySubDto myorder = new MySubDto();
				myorder.setActualTotal(sub.getActualTotal());
				myorder.setFreightAmount(sub.getFreightAmount());
				myorder.setShopId(sub.getShopId());
				myorder.setShopName(sub.getShopName());
				myorder.setStatus(sub.getStatus());
				myorder.setSubDate(sub.getSubDate());
				myorder.setSubId(sub.getSubId());
				myorder.setSubNum(sub.getSubNumber());
				myorder.setPayManner(sub.getPayManner());
				myorder.setIspay(sub.isPayed());
				myorder.setUserName(sub.getUserName());
				myorder.setUserId(sub.getUserId());
				myorder.setPayTypeId(sub.getPayTypeId());
				myorder.setPayTypeName(sub.getPayTypeName());
				myorder.setSubType(sub.getSubType());
				myorder.setUpdateDate(sub.getUpdateDate());
//				UsrAddrSubDto usrAddrSubDto = new UsrAddrSubDto(sub.getReceiver(), sub.getMobile(), sub.getTelphone(),
//						sub.getSubPost(), sub.getProvince(), sub.getCity(), sub.getArea(), sub.getSubAdds());
				// myorder.setUsrAddrSubDto(usrAddrSubDto);
				List<SubItem> subItems = subItemService.getSubItem(sub.getSubNumber());
				List<SubOrderItemDto> itemDtos = new ArrayList<SubOrderItemDto>();
				for (SubItem subItem : subItems) {
					SubOrderItemDto subOrderItemDto = new SubOrderItemDto();
					subOrderItemDto.setAttribute(subItem.getAttribute());
					subOrderItemDto.setBasketCount(subItem.getBasketCount());
					subOrderItemDto.setPic(subItem.getPic());
					subOrderItemDto.setProdId(subItem.getProdId());
					subOrderItemDto.setProdName(subItem.getProdName());
					subOrderItemDto.setProductTotalAmout(subItem.getProductTotalAmout());
					subOrderItemDto.setSkuId(subItem.getSkuId());
					subOrderItemDto.setSubItemId(subItem.getSubItemId());
					subOrderItemDto.setSnapshotId(subItem.getSnapshotId());
					subOrderItemDto.setCash(subItem.getCash());
					itemDtos.add(subOrderItemDto);
				}
				myorder.setSubOrderItemDtos(itemDtos);
				myorderDtos.add(myorder);
			}
		}
		// 从发货单中取产品
		ps.setResultList(myorderDtos);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramDto", paramDto);
	}

	/**
	 * 页面加密手机号码，保留前3个和后3个
	 * 
	 * @param mobile
	 * @return
	 */
	private String mobileSubstitution(String mobile) {
		if (mobile == null) {
			return null;
		}
		if (mobile.length() < 11) {
			return mobile;
		}
		return mobile.replaceAll(mobile.substring(3, mobile.length() - 3), "******");
	}

	/**
	 * 页面加密邮箱
	 * @param mail
	 * @return
	 */
	private String mailSubstitution(String mail) {
		int pos = mail.indexOf("@");
		String preFix = mail.substring(0, pos);
		String postFix = mail.substring(pos, mail.length());
		int mailPrefixLength = preFix.length();
		StringBuilder sb = new StringBuilder();
		if (mailPrefixLength < 3) {
			for (int i = 0; i < mailPrefixLength; i++) {
				sb.append("*");
			}
		} else {
			sb.append(preFix.subSequence(0, 1));
			for (int i = 1; i < mailPrefixLength - 1; i++) {
				sb.append("*");
			}
			sb.append(preFix.subSequence(mailPrefixLength - 1, mailPrefixLength));
		}
		return sb.toString() + postFix;
	}

}
