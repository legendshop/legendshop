/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.util.AppUtils;

/**
 * 配送方式管理
 * 
 */
@Controller
@RequestMapping("/admin/deliveryType")
public class DeliveryTypeController extends BaseController{

	@Autowired
	private DeliveryTypeService deliveryTypeService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查询配送方式
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, DeliveryType deliveryType) {
		PageSupport<DeliveryType> ps = deliveryTypeService.getDeliveryTypePage(curPageNO, deliveryType);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("deliveryType", deliveryType);
		return PathResolver.getPath(AdminPage.DELIVERYTYPE_LIST_PAGE);
	}

	/**
	 * 保存配送方式
	 */
	@SystemControllerLog(description="保存配送方式")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, DeliveryType deliveryType) {
		DeliveryType dt = null;
		if (AppUtils.isNotBlank(deliveryType.getDvyTypeId())) {
			dt = deliveryTypeService.getDeliveryType(deliveryType.getDvyTypeId());
			dt.setName(deliveryType.getName());
			dt.setDvyId(deliveryType.getDvyId());
			dt.setNotes(deliveryType.getNotes());
			dt.setPrinttempId(deliveryType.getPrinttempId());
			dt.setIsSystem(deliveryType.getIsSystem());
		} else {
			dt = deliveryType;
			dt.setCreateTime(new Date());
			dt.setShopId(systemParameterUtil.getDefaultShopId());
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		dt.setPrinttempId(-1L);
		dt.setUserId(user.getUserId());
		dt.setUserName(user.getUsername());
		dt.setModifyTime(new Date());

		deliveryTypeService.saveDeliveryType(dt);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.DELIVERYTYPE_LIST_QUERY);
	}

	/**
	 * 删除配送方式
	 */
	@SystemControllerLog(description="删除配送方式")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryType deliveryType = deliveryTypeService.getDeliveryType(id);
		deliveryTypeService.deleteDeliveryType(deliveryType);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.DELIVERYTYPE_LIST_QUERY);
	}

	/**
	 * 根据id加载配送方式编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryType deliveryType = deliveryTypeService.getDeliveryType(id);
		request.setAttribute("deliveryType", deliveryType);
		return PathResolver.getPath(AdminPage.DELIVERYTYPE_EDIT_PAGE);
	}

	/**
	 * 加载配送方式编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.DELIVERYTYPE_EDIT_PAGE);
	}

	/**
	 * 更新配送方式
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryType deliveryType = deliveryTypeService.getDeliveryType(id);
		request.setAttribute("deliveryType", deliveryType);
		return PathResolver.getPath(RedirectPage.DELIVERYTYPE_LIST_QUERY);
	}

}
