/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdvAnalysis;
import com.legendshop.model.entity.User;
import com.legendshop.spi.service.AdvAnalysisService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 广告分析控制器
 * 
 * @author quanzc
 *
 */
@Controller
public class AdvAdminAnalysisController extends BaseController {

	@Autowired
	private AdvAnalysisService advAnalysisService;
	
	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 广告点击统计
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("admin/advAnalysis")
	public String advAnalysis(HttpServletRequest request, HttpServletResponse response, String curPageNO,String url, String userMobile) {
		if (curPageNO == null) {
			curPageNO = "1";
		}
		String userId = null;//查找用户
		if(AppUtils.isNotBlank(userMobile)) {
			User ud = userDetailService.getUserByMobile(userMobile);
			if(ud != null) {
				userId = ud.getId();
			}else {
				userId = "-1";//如果手机号码不对则找不到记录
			}
		}
		PageSupport<AdvAnalysis> ps = advAnalysisService.groupQuery(userId, curPageNO, url); //组合查询
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("url", url);
		request.setAttribute("userMobile", userMobile);
		return PathResolver.getPath(AdminPage.ADV_ANALYSIS_PAGE);
		

	}

	/**
	 * 广告统计详情
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("admin/advAnalysis/detail")
	public String detail(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		String url = request.getParameter("url");
		PageSupport<AdvAnalysis> ps = advAnalysisService.query(curPageNO, url);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("url", url);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(AdminPage.ADV_ANALYSISDETAIL_PAGE);
	}

}
