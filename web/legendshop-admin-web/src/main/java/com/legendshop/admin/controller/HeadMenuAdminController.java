/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Headmenu;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.HeadmenuService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 头部菜单管理
 * 
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/headmenu")
public class HeadMenuAdminController {

	@Autowired
	private HeadmenuService headmenuService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查找头部菜单
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param headmenu
	 * @param type
	 * @return
	 */
	@RequestMapping("/query/{type}")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Headmenu headmenu, @PathVariable String type) {
		PageSupport<Headmenu> ps = this.headmenuService.getHeadmenu(curPageNO, headmenu, type);
		PageSupportHelper.savePage(request, ps);
		headmenu.setType(type);
		request.setAttribute("headmenu", headmenu);
		request.setAttribute("type", type);
		return PathResolver.getPath(AdminPage.HEADMENU_LIST);
	}

	/**
	 * 跳转到编辑页
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param type
	 * @return
	 */
	@RequestMapping("/load")
	public String load(HttpServletRequest request, HttpServletResponse response, Long id, String type) {
		Headmenu headmenu = null;
		if (AppUtils.isNotBlank(id)) {
			headmenu = this.headmenuService.getHeadmenu(id);
		}
		request.setAttribute("headmenu", headmenu);
		request.setAttribute("type", type);
		return PathResolver.getPath(AdminPage.HEADMENU_EDIT);
	}

	/**
	 * 保存、更新头部菜单
	 * 
	 * @param request
	 * @param response
	 * @param headmenu
	 * @return
	 */
	@SystemControllerLog(description="保存更新头部菜单")
	@RequestMapping("/submit")
	public String saveOrUpdate(HttpServletRequest request, HttpServletResponse response, Headmenu headmenu) {
		if (AppUtils.isNotBlank(headmenu)) {
			SecurityUserDetail user = UserManager.getUser(request);
			String filePath = imageEdit(user, headmenu);
			if (AppUtils.isNotBlank(headmenu.getId())) {
				Headmenu oldHeadmenu = this.headmenuService.getHeadmenu(headmenu.getId());
				oldHeadmenu.setName(headmenu.getName());
				oldHeadmenu.setSeq(headmenu.getSeq());
				oldHeadmenu.setUpdateTime(new Date());
				oldHeadmenu.setUrl(headmenu.getUrl());
				oldHeadmenu.setStatus(headmenu.getStatus());
				oldHeadmenu.setAndroidId(headmenu.getAndroidId());
				oldHeadmenu.setiOSId(headmenu.getiOSId());
				if (AppUtils.isNotBlank(filePath)) {
					oldHeadmenu.setPic(filePath);
				}
				this.headmenuService.saveHeadmenu(oldHeadmenu);
			} else {
				if (AppUtils.isNotBlank(filePath)) {
					headmenu.setPic(filePath);
				}
				headmenu.setUpdateTime(new Date());
				this.headmenuService.saveHeadmenu(headmenu);
			}
			return PathResolver.getPath(RedirectPage.HEADMENU_LIST_QUERY) + "/" + headmenu.getType();
		}
		return PathResolver.getPath(AdminPage.ADMIN_404);
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除头部菜单")
	@RequestMapping("/delete")
	public String delete(HttpServletRequest request, HttpServletResponse response, Long id) {
		if (AppUtils.isNotBlank(id)) {
			Headmenu headmenu = this.headmenuService.getHeadmenu(id);
			this.headmenuService.deleteHeadmenu(headmenu);
			return PathResolver.getPath(RedirectPage.HEADMENU_LIST_QUERY) + "/" + headmenu.getType();
		}
		return PathResolver.getPath(AdminPage.ADMIN_404);
	}
	
	/**
	 * 图片编辑
	 * 
	 * @param headmenu
	 * @return
	 */
	private String imageEdit(SecurityUserDetail user, Headmenu headmenu) {
		String filePath = null;
		if (headmenu.getImageFile() != null && headmenu.getImageFile().getSize() > 0) {
			filePath = attachmentManager.upload(headmenu.getImageFile());
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filePath, headmenu.getImageFile(), AttachmentTypeEnum.OTHER);
			if (AppUtils.isNotBlank(headmenu.getId())) {
				attachmentManager.deleteTruely(headmenu.getPic());
			}
		}
		return filePath;
	}
	
	
	/**
	 * 更改菜单状态
	 * @param request
	 * @param response
	 * @param headmenuId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更改头部菜单状态")
	@RequestMapping(value = "/updatestatus/{headmenuId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long headmenuId, @PathVariable Integer status) {
		Headmenu headmenu = headmenuService.getHeadmenu(headmenuId);
		if (headmenu == null) {
			return -1;
		}
		if (!status.equals(headmenu.getStatus())) {
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)
					|| Constants.STOPUSE.equals(status)) {
				headmenu.setStatus(status);
				headmenu.setUpdateTime(new Date());
				headmenuService.updateHeadmenu(headmenu);
			}
		}
		return headmenu.getStatus();
	}
}
