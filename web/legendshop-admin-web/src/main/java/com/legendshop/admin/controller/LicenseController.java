/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.central.model.LicenseRequest;
import com.legendshop.central.model.LicenseUpdateByKeyEvent;
import com.legendshop.central.model.LicenseUpdateEvent;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.handler.LicenseManger;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * License控制器.
 */
@Controller
@RequestMapping("/license")
public class LicenseController extends BaseController {
	
	@Autowired
	private LicenseManger licenseManger;

	/**
	 * 根据账号激活系统， 查询版权信息
	 */
	@RequestMapping("/upgrade")
	public String upgrade(HttpServletRequest request, HttpServletResponse response) {
		String licenseType = licenseManger.getLicenseType();
		String domainName = licenseManger.getDomainName();
		String version = licenseManger.getVersion();
		Date installDate = licenseManger.getInstallDate();
		request.setAttribute("licenseType", licenseType);
		request.setAttribute("domainName", domainName);
		request.setAttribute("version", version);
		request.setAttribute("installDate", installDate);
		return PathResolver.getPath(AdminPage.ADMIN_UPGRADE_PAGE);
	}


	/**
	 * 根基激活码激活系统，查询版权信息
	 */
	@RequestMapping("/upgradeByKey")
	public String upgradeByKey(HttpServletRequest request, HttpServletResponse response) {
		String licenseType = licenseManger.getLicenseType();
		String domainName = licenseManger.getDomainName();
		String version = licenseManger.getVersion();
		Date installDate = licenseManger.getInstallDate();
		request.setAttribute("licenseType", licenseType);
		request.setAttribute("domainName", domainName);
		request.setAttribute("version", version);
		request.setAttribute("installDate", installDate);
		return PathResolver.getPath(AdminPage.ADMIN_UPGRADE_BYKEY_PAGE);
	}

	/**
	 * 更新版权信息
	 */
	@SystemControllerLog(description="更新版权信息")
	@RequestMapping("/postUpgrade")
	public String postUpgrade(HttpServletRequest request, HttpServletResponse response) {
		LicenseRequest licenseRequest = new LicenseRequest();
		licenseRequest.setCompanyCode(request.getParameter("companyCode"));
		licenseRequest.setCompanyName(request.getParameter("company"));
		licenseRequest.setDes(request.getParameter("des"));
		licenseRequest.setLinks(request.getParameter("link"));
		EventHome.publishEvent(new LicenseUpdateEvent(licenseRequest));
		return PathResolver.getPath(FowardPage.ADMIN_UPGRADE_PAGE);
	}


	/**
	 * 更新版权信息
	 */
	@SystemControllerLog(description="激活码更新版权信息")
	@RequestMapping("/postUpgradeByKey")
	public String postUpgradeByKey(HttpServletRequest request, HttpServletResponse response) {
		String certificate = request.getParameter("certificate");
		if(AppUtils.isNotBlank(certificate)){
			EventHome.publishEvent(new LicenseUpdateByKeyEvent(certificate));
		}

		return PathResolver.getPath(FowardPage.ADMIN_UPGRADE_BYKEY_PAGE);
	}


}
