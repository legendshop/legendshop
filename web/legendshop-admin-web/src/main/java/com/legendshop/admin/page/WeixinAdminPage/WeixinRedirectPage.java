/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.WeixinAdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 微信的重定向页面.
 */
public enum WeixinRedirectPage implements PageDefinition {

	/** 查找子菜单 */
	CHILD_MENU_LIST("/admin/weixinMenu/queryChildMenu/"),

	/** 一级菜单列表 */
	MENU_LIST_QUERY("/admin/weixinMenu/query"),

	/** 微信扩展接口管理 */
	WEIXINMENUEXPANDCONFIG_LIST_QUERY("/admin/weixin/expandConfig/query"),

	/** 微信消息自动回复管理 */
	WEIXINAUTORESPONSE_LIST_QUERY("/admin/weixinAutoresponse/query"),

	/** 微信关注欢迎语 */
	WEIXINSUBSCRIBE_LIST_QUERY("/admin/weixinSubscribe/query"),

	/** 查找微信用户 */
	WEIXINGZUSER_LIST_PAGE("/admin/weixinUser/query"),

	/** 微信关键词回复管理 */
	WEIXINKEYWORDRESPONSE_LIST_QUERY("/admin/weiXinKeywordResponse/query")

	;

	/** The value. */
	private final String value;

	private WeixinRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private WeixinRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
