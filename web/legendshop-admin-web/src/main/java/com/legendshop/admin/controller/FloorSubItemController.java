/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.spi.service.FloorSubItemService;

/**
 * 楼层类目
 *
 */
@Controller
@RequestMapping("/admin/floorSubItem")
public class FloorSubItemController extends BaseController{
    
	@Autowired
    private FloorSubItemService floorSubItemService;
	
	/**
	 * 查找楼层类目
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, FloorSubItem floorSubItem) {
        
        PageSupport<FloorSubItem> ps = floorSubItemService.getFloorSubItemPage(curPageNO);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("floorSubItem", floorSubItem);
        
        return "/floorSubItem/floorSubItemList";
        //TODO, replace by next line, need to predefined BackPage parameter
       // return PathResolver.getPath(request, response, BackPage.FLOORSUBITEM_LIST_PAGE);
    }
    
    /**
     * 保存楼层类目
     */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, FloorSubItem floorSubItem) {
        floorSubItemService.saveFloorSubItem(floorSubItem);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return "forward:/admin/system/floorSubItem/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
		//return PathResolver.getPath(request, response, FowardPage.FLOORSUBITEM_LIST_QUERY);
    }
    
    /**
     * 删除楼层类目
     */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        FloorSubItem floorSubItem = floorSubItemService.getFloorSubItem(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), floorSubItem.getUserName());
		//if(result!=null){
			//return result;
		//}
		floorSubItemService.deleteFloorSubItem(floorSubItem);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/system/floorSubItem/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
        //return PathResolver.getPath(request, response, FowardPage.FLOORSUBITEM_LIST_QUERY);
    }
    
    /**
     * 跳转编辑页面
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        FloorSubItem floorSubItem = floorSubItemService.getFloorSubItem(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), floorSubItem.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("floorSubItem", floorSubItem);
        return "/floorSubItem/floorSubItem";
         //TODO, replace by next line, need to predefined FowardPage parameter
         //return PathResolver.getPath(request, response, BackPage.FLOORSUBITEM_EDIT_PAGE);
    }
    
    /**
     * 跳转新建页面
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/floorSubItem/floorSubItem";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.FLOORSUBITEM_EDIT_PAGE);
	}
	
	/**
	 * 更新楼层类目
	 */
    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        FloorSubItem floorSubItem = floorSubItemService.getFloorSubItem(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), floorSubItem.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("floorSubItem", floorSubItem);
		return "forward:/admin/system/floorSubItem/query.htm";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.FLOORSUBITEM_EDIT_PAGE);
    }

}
