/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.collection.CollUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.BusinessMessageDTO;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.im.CustomServer;
import com.legendshop.permission.service.UserManagerService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.websocket.service.CustomServerService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopCompanyDetailService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 商城详细信息控制器.
 */
@Controller
@RequestMapping("/admin/shopDetail")
public class ShopDetailAdminController extends BaseController {

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ShopCompanyDetailService shopCompanyDetailService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserManagerService userManagerService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private CustomServerService customServerService;

	@Resource(name = "productIndexDelProcessor")
	private EventProcessor<ProductIdDto> productIndexDelProcessor;

	@Resource(name = "productIndexSaveProcessor")
	private EventProcessor<ProductIdDto> productIndexSaveProcessor;

	/**
	 * 查询商城详细信息列表.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopDetail
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopDetail shopDetail) throws Exception {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"product_num", "off_product_num"});

		PageSupport<ShopDetail> ps = shopDetailService.getShopDetailPage(curPageNO, shopDetail, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopDetail", shopDetail);

		adminMenuUtil.parseMenu(request, 6); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面

		return PathResolver.getPath(AdminPage.SHOPDETAIL_LIST_PAGE);
	}

	/**
	 * 保存商城详细信息.
	 *
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return the string
	 */
	@SystemControllerLog(description = "保存商城详细信息")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		if ((shopDetail != null) && (!AppUtils.isBlank(shopDetail.getShopId()))) {
			return null;
		} else {
			// 检查用户权限，只有超级用户可以使用
			String userName = shopDetail.getUserName();

			String shopPic = null;
			if (AppUtils.isBlank(userName)) {
				throw new NotFoundException("user name can not be empty");
			}
			UserDetail userDetail = shopDetailService.getShopDetailByName(userName);
			if (AppUtils.isBlank(userDetail)) {
				throw new NotFoundException("userDetail can not beempty");
			}
			if (shopDetail.getFile().getSize() > 0) {
				shopPic = attachmentManager.upload(shopDetail.getFile());
			}
			try {
				// SafeHtml safeHtml = new SafeHtml();
				shopDetail.setUserId(userDetail.getUserId());
				Date date = new Date();
				shopDetail.setModifyDate(date);
				shopDetail.setRecDate(date);
				shopDetail.setVisitTimes(0l);
				shopDetail.setOffProductNum(0l);
				shopDetail.setCommNum(0l);
				shopDetail.setProductNum(0l);
				shopDetail.setType(ShopTypeEnum.BUSINESS.value());
				shopDetail.setStatus(ShopStatusEnum.NORMAL.value());
				shopDetail.setCapital(0d);
				shopDetail.setCredit(0);
				shopDetailService.save(userName, shopPic, shopDetail);
				saveMessage(request, ResourceBundleHelper.getSucessfulString());
			} catch (Exception e) {
				throw new BusinessException(e, "Error happened when save shop", ErrorCodes.SAVE_ERROR);
			}

			String path = PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);
			return path;
		}
	}

	/**
	 * 删除商城详细信息.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description = "删除商城详细信息")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ShopDetail shopDetail = shopDetailService.getShopDetailById(id);
		if (shopDetail != null) {
			String result = shopDetailService.delete(shopDetail);
			if (AppUtils.isBlank(result)) {
				saveMessage(request, ResourceBundleHelper.getDeleteString());
			} else {
				/*saveError(request, result);样式太丑*/
				saveMessage(request, result);
			}
		}

		return PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);
	}

	/**
	 * 查看商城详细信息详情.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ShopDetail shopDetail = shopDetailService.getShopDetailById(id);
		request.setAttribute("userDetail", userDetailService.getUserDetail(shopDetail.getUserName()));
		request.setAttribute("shopDetail", shopDetail);
		request.setAttribute("id", request.getParameter("userId"));
		return PathResolver.getPath(AdminPage.SHOPDETAIL_EDIT_PAGE);
	}


	/**
	 * 跳转新建页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.SHOPDETAIL_EDIT_PAGE);
	}

	/**
	 * 更新商城详细信息.
	 *
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return the string
	 */
	@SystemControllerLog(description = "更新商城详细信息")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request, HttpServletResponse response, Long shopId, Integer requireAudit) {
		ShopDetail shop = shopDetailService.getShopDetailById(shopId);
		if (shop == null) {
			return Constants.FAIL;
		}
		shop.setProdRequireAudit(requireAudit);
		shopDetailService.update(shop);
		return Constants.SUCCESS;
	}

	/**
	 * 更新商城信息
	 *
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@SystemControllerLog(description = "更新店铺信息")
	@RequestMapping(value = "/updateShopInfo", method = RequestMethod.POST)
	public String updateShopInfo(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		ShopDetail shop = shopDetailService.getShopDetailById(shopDetail.getShopId());
		Integer shopStatus = shop.getStatus();
		// 因updateShopInfo方法用户跟后台公用，以flag区分逻辑
		String flag = "admin";
		//更新店铺信息
		Integer result = shopDetailService.updateShopInfo(shop, shopDetail, flag);
		if (result < 1) {
			throw new BusinessException("update error");
		}
		// 如果不是(not)更新店铺状态为正常, 直接返回
		if (shopDetail.getStatus().equals(ShopStatusEnum.OFFLINE.value()) && !shopStatus.equals(ShopStatusEnum.OFFLINE.value())) {
			//获得处于上线状态或审核中的商品
			List<Long> prodIdList = productService.getAllprodIdStr(shopDetail.getShopId(), ProductStatusEnum.PROD_ONLINE.value());
			prodIdList.addAll(productService.getAllprodIdStr(shopDetail.getShopId(), ProductStatusEnum.PROD_AUDIT.value()));
			if (CollUtil.isNotEmpty(prodIdList)) {
				for (Long prodId : prodIdList) {
					//将上线状态或审核中的商品放入仓库中
					productService.updateStatus(ProductStatusEnum.PROD_OFFLINE.value(),prodId,null);
				}
				//刷新商品索引
				productIndexDelProcessor.process(new ProductIdDto(prodIdList));
			}
		} else if (shopDetail.getStatus().equals(ShopStatusEnum.CLOSED.value()) && !shopStatus.equals(ShopStatusEnum.OFFLINE.value())) {
			//获得处于上线状态或审核中的商品
			List<Long> prodIdList = productService.getAllprodIdStr(shopDetail.getShopId(), ProductStatusEnum.PROD_ONLINE.value());
			prodIdList.addAll(productService.getAllprodIdStr(shopDetail.getShopId(), ProductStatusEnum.PROD_AUDIT.value()));
			if (CollUtil.isNotEmpty(prodIdList)) {
				for (Long prodId : prodIdList) {
					//将上线状态或审核中的商品放入仓库中
					productService.updateStatus(ProductStatusEnum.PROD_OFFLINE.value(),prodId,null);
				}
				//刷新商品索引
				productIndexDelProcessor.process(new ProductIdDto(prodIdList));
			}
		} else if (shopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			/*//上线线所有商品
			List<Long> prodIdList = productService.getAllprodIdStr(shopDetail.getShopId(), ProductStatusEnum.PROD_ILLEGAL_OFF.value());
			if (CollUtil.isNotEmpty(prodIdList)) {
				productService.batchOnlineLine(prodIdList);
				//保存索引
				productIndexSaveProcessor.process(new ProductIdDto(prodIdList));
			}*/
			// 如果是更新店铺为正常,需要检查是否存在客服
			Integer exist = this.customServerService.getCountByShopId(shopDetail.getShopId());
			if (exist > 0) {
				return PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);
			} else {
				ShopDetail detail = this.shopDetailService.getShopDetailById(shopDetail.getId());
				// 如果不存在 则创建一个默认的客服
				CustomServer customer = new CustomServer();
				customer.setName(detail.getUserName());
				customer.setShopId(detail.getShopId());
				customer.setCreateTime(new Date());

				// 加载当前用户的信息
				User user = this.userManagerService.getUser(detail.getUserId());
				customer.setAccount(user.getUserName());
				customer.setPassword(user.getPassword());
				customer.setNewPassword(user.getPassword());
				customer.setFrozen(false);
				customer.setRemark("这是系统创建的默认客服!");
				this.customServerService.saveCustomServer(customer);
			}
		}
		return PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);
	}

	/**
	 * @Description: 去往公司信息
	 * @date 2016-8-19
	 */
	@RequestMapping(value = "/queryCompanyInfo")
	public String queryCompanyInfo(HttpServletRequest request, HttpServletResponse response, Long shopId,
								   String userName, Integer status) {
		ShopCompanyDetail companyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopId);
		if (AppUtils.isBlank(companyDetail)) {
			throw new BusinessException("Can not found companyDetail record by shopid = " + shopId);
		}
		request.setAttribute("shopCompanyDetail", companyDetail);
		request.setAttribute("userName", userName);
		request.setAttribute("shopId", shopId);
		request.setAttribute("status", status);
		return PathResolver.getPath(AdminPage.SHOP_COMPANY_PAGE);

	}

	/**
	 * 修改公司信息
	 *
	 * @param request
	 * @param response
	 * @param newCompanyDetail
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/updateShopCompanyInfo", method = RequestMethod.POST)
	public String updateShopCompanyInfo(HttpServletRequest request, HttpServletResponse response, ShopCompanyDetail newCompanyDetail) {

		if (AppUtils.isBlank(newCompanyDetail)) {
			throw new BusinessException("companyDetail is null");
		}
		if (AppUtils.isBlank(newCompanyDetail.getShopId())) {
			throw new BusinessException("shopId is null");
		}

		ShopCompanyDetail detail = shopCompanyDetailService.getShopCompanyDetailByShopId(newCompanyDetail.getShopId());
		boolean result = shopCompanyDetailService.updataShopCompanyInfo(detail, newCompanyDetail);
		if (!result) {
			// throw new
			// BusinessException("updateShopCompanyInfo修改公司信息----"+result);
			return PathResolver.getRedirectPath("/admin/shopDetail/queryCompanyInfo?shopId="
					+ detail.getShopId() + "&userName=" + newCompanyDetail.getUserName());
		}
		return PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);
	}

	/**
	 * 检查如果该模版没有特定配置则用模版的第一个作为默认值, 模板化简来做
	 *
	 * @param entityList
	 * @param keyValue
	 * @return
	 */
	@Deprecated
	private String checkTemplet(List<StatusKeyValueEntity> entityList, String keyValue) {
		boolean matched = false;
		if (entityList != null) {
			for (StatusKeyValueEntity entity : entityList) {
				if (entity.getKey().equals(keyValue)) {
					matched = true;
					break;
				}
			}
		} else {
			return null;
		}

		if (matched) {
			return keyValue;
		} else {
			return entityList.get(0).getKey();
		}

	}

	/**
	 * 审核商城状态
	 *
	 * @param userId
	 * @param shopId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description = "审核商城状态")
	@RequestMapping("audit")
	public @ResponseBody
	String auditShop(HttpServletRequest request, String userId, Long shopId, Integer status,
					 String auditOpinion) {
		if (userId == null || shopId == null) {
			return Constants.FAIL;
		}

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (shopDetail != null && !shopDetail.getShopId().equals(systemParameterUtil.getDefaultShopId())) {
			shopDetail.setAuditOpinion(auditOpinion);
			boolean result = shopDetailService.updateShop(userId, shopDetail, status);
			if (result) {
				return Constants.SUCCESS;
			} else {
				return Constants.FAIL;
			}
		}
		return Constants.FAIL;

	}

	/**
	 * 修改商城状态
	 *
	 * @param shopId
	 * @param shopType
	 * @return
	 */
	@SystemControllerLog(description = "修改商城状态")
	@RequestMapping(value = "changeShopType", method = RequestMethod.POST)
	public @ResponseBody
	String changeShopType(HttpServletRequest request, @RequestParam Long shopId,
						  @RequestParam Long shopType) {
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (AppUtils.isBlank(shopDetail)) {
			return "该店铺不存在";
		}
		if (shopDetail.getOpStatus() != ShopOPstatusEnum.AUDITED.value()) {
			return "该店铺状态不正确";
		}
		if (shopDetail.getShopType() == shopType.intValue()) {
			return "状态未做修改";
		}

		SecurityUserDetail user = UserManager.getUser(request);

		if (!UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value())) {
			return "权限不足";
		}
		int result = shopDetailService.updateShopType(shopId, shopType);
		if (result <= 0) {
			return "更新失败";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 商城审核历史
	 *
	 * @param request
	 * @param response
	 * @param shopId
	 * @return
	 */
	@RequestMapping("shopAuditDetail")
	public String shopAuditDetail(HttpServletRequest request, HttpServletResponse response, Long shopId,
								  String curPageNO) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		PageSupport<ShopAudit> ps = shopDetailService.getShopAuditPage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		request.setAttribute("shopDetail", shopDetail);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(AdminPage.SHOPAUDIT_LIST_PAGE);
	}

	/**
	 * 修改商城佣金比例
	 *
	 * @param userId
	 * @param shopId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description = "修改商城佣金比例")
	@RequestMapping(value = "/changeShopCommision", method = RequestMethod.POST)
	public @ResponseBody
	String changeShopCommision(HttpServletRequest request, @RequestParam Long shopId, @RequestParam Integer commision) {

		if (AppUtils.isBlank(commision) || commision < 0 || commision > 100) {
			return "请输入正整数，且范围为0-100";
		}

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (AppUtils.isBlank(shopDetail)) {
			return "该店铺不存在";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		if (!UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value())) {
			return "权限不足";
		}

		int result = shopDetailService.updateShopCommision(shopId, commision);

		if (result <= 0) {
			return "更新失败";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除店铺设置的佣金比例
	 *
	 * @param request
	 * @param shopId
	 * @return
	 */
	@SystemControllerLog(description = "删除店铺设置的佣金比例")
	@RequestMapping(value = "/deleteShopCommision/{shopId}")
	public String deleteShopCommision(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId) {

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

		shopDetail.setCommissionRate(null);
		shopDetailService.update(shopDetail);

		return PathResolver.getPath(RedirectPage.SHOP_DETAIL_LIST_QUERY);

	}

//	/**
//	 * 获取营业执照信息
//	 */
//	@RequestMapping(value = "/business/message")
//	public BusinessMessageDTO getBusinessMessage(Long shopId) {
//		return shopDetailService.getBusinessMessage(shopId);
//
//	}
	/**
	 * 获取营业执照信息
	 */
	@ApiOperation(value = "根据店铺id获取营业执照信息", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(value = "/business/message")
	@ApiImplicitParam(name = "shopId", paramType = "query", value = "店铺id", required = true, dataType = "Long")
	public BusinessMessageDTO getBusinessMessage(Long shopId) {
		return shopDetailService.getBusinessMessage(shopId);
	}


}
