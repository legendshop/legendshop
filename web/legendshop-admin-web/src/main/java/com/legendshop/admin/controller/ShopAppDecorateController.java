/*
 * LegendShop 多用户商城系统
 *  版权所有,并保留所有权利。
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AppDecoratePageStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ShopAppDecorate;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.spi.service.ShopAppDecorateService;
import com.legendshop.util.AppUtils;

/**
 * 后台-移动端店铺首页装修管理
 */
@Controller
@RequestMapping("/admin/app/shopIndexDecorate")
public class ShopAppDecorateController{

	@Autowired
	private ShopAppDecorateService shopAppDecorateService;
	
	/**
	 * 店铺首页装修
	 * @param curPageNO 当前页码
	 * @param appPageManage 搜索参数
	 * @param flag 获取列表/内容的标记
	 */
	@RequestMapping(value="/query")
	public String query(HttpServletRequest request, HttpServletResponse response,String curPageNO,ShopAppDecorate shopAppDecorate,String flag){
		
		Integer pageSize = 10;
		PageSupport<ShopAppDecorate> ps = shopAppDecorateService.queryShopAppDecorate(curPageNO,pageSize,shopAppDecorate);
		PageSupportHelper.savePage(request, ps);
		
		request.setAttribute("shopAppDecorate", shopAppDecorate);
		
		if(AppUtils.isNotBlank(flag)){
			return PathResolver.getPath(AdminPage.SHOP_INDEX_APPDECORATE_CONTENT);
		}
		return PathResolver.getPath(AdminPage.SHOP_INDEX_APPDECORATE);
	}
	
	
	
	
	/**
	 * 删除
	 */
	@RequestMapping(value="/delete/{Id}")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response,@PathVariable Long Id){
		
		ShopAppDecorate shopAppDecorate = shopAppDecorateService.getShopAppDecorate(Id);
		
		if(AppUtils.isBlank(shopAppDecorate)){
			return "该数据不存在或已被删除，请刷新后重试！";
		}
		shopAppDecorateService.deleteShopAppDecorate(shopAppDecorate);
		return Constants.SUCCESS;
	}
	
	
	
	/**
	 * 上下线
	 */
	@RequestMapping(value="/updateStatus")
	@ResponseBody
	public String updateStatus(HttpServletRequest request, HttpServletResponse response,Long id,Integer status){
		
		ShopAppDecorate shopAppDecorate = shopAppDecorateService.getShopAppDecorate(id);
		if(AppUtils.isBlank(shopAppDecorate)){
			return "该数据不存在或已被删除，请刷新后重试！";
		}
		
		shopAppDecorate.setStatus(status);
		shopAppDecorateService.updateShopAppDecorate(shopAppDecorate);
		return Constants.SUCCESS;
	}
	
	
	
	
}
