/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Advertisement;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AdvertisementService;
import com.legendshop.uploader.AttachmentManager;

/**
 * 后台广告控制器.
 * TODO 此广告理应用于PC和wap的首页装修之用,用于记录客户的广告. 暂时不再开放,有需求再深化改造.
 */
@Controller
@RequestMapping("/admin/advertisement")
public class AdvertisementAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AdvertisementAdminController.class);

	/** 后台广告服务. */
	@Autowired
	private AdvertisementService advertisementService;

	/** 附件上传文件. */
	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查询广告列表.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param advertisement
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Advertisement advertisement) {
		PageSupport<Advertisement> ps = advertisementService.queryAdvertisement(curPageNO, advertisement);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", advertisement);
		return  PathResolver.getPath(AdminPage.ADV_LIST_PAGE);
	}

	/**
	 * 保存广告.
	 * 
	 * @param request
	 * @param response
	 * @param advertisement
	 * @return the string
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Advertisement advertisement) {
		SecurityUserDetail user = UserManager.getUser(request);
		String picUrl = null;
		if (advertisement.getFile().getSize() > 0) {
			picUrl = attachmentManager.upload(advertisement.getFile());
		}
		advertisementService.saveOrUpdate(user.getUsername(), user.getUserId(), user.getShopId(),advertisement,picUrl);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.ADV_LIST_QUERY);
	}

	/**
	 * 删除广告.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Advertisement advertisement = advertisementService.getAdvertisement(id);
		log.info("elete Advertisement Url {}", advertisement.getLinkUrl());
		advertisementService.delete(id,advertisement);
		saveMessage(request, ResourceBundleHelper.getDeleteString());
		return PathResolver.getPath(RedirectPage.ADV_LIST_QUERY);
	}

	/**
	 * 根据id加载广告编辑页面.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Advertisement advertisement = advertisementService.getAdvertisement(id);
		request.setAttribute("bean", advertisement);
		return PathResolver.getPath(AdminPage.ADV_EDIT_PAGE);
	}

	/**
	 * 加载广告编辑页面.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.ADV_EDIT_PAGE);
	}

	/**
	 * 更新广告.
	 * 
	 * @param request
	 * @param response
	 * @param advertisement
	 * @return the string
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Advertisement advertisement) {
		advertisementService.update(advertisement);
		return PathResolver.getPath(FowardPage.ADV_LIST_QUERY);
	}

	/**
	 * 更新广告状态.
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @param status
	 * @return the integer
	 */
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		Advertisement advertisement = advertisementService.getAdvertisement(id);
		if (advertisement == null) {
			return -1;
		}
		if (!status.equals(advertisement.getEnabled())) {
			// only for admin need to check TODO
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				advertisement.setEnabled(status);
				advertisementService.update(advertisement);
			}
		}
		return advertisement.getEnabled();
	}
}
