/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProductPurchaseRateDto;
import com.legendshop.model.dto.ReportDto;
import com.legendshop.model.report.AverageMemberOrders;
import com.legendshop.model.report.AverageOrderAmouont;
import com.legendshop.model.report.OrdersConversionRate;
import com.legendshop.model.report.PurchaseRankDto;
import com.legendshop.model.report.RegisteredPurchaseRate;
import com.legendshop.model.report.ReportRequest;
import com.legendshop.model.report.SaleAnalysis;
import com.legendshop.model.report.SaleAnalysisDto;
import com.legendshop.model.report.SaleAnalysisResponse;
import com.legendshop.model.report.SalerankDto;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ReportService;

/**
 * 报表控制器
 */
@Controller
@RequestMapping("/admin/report")
public class ReportController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ReportController.class);

	@Autowired
	private ReportService reportService;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 访问购买率
	 *
	 */
	@RequestMapping("/purchaserate")
	public String purchaseRate(HttpServletRequest request, HttpServletResponse response, String curPageNO,Long shopId, String prodName,Date startDate,Date endDate ) throws Exception {
		request.setAttribute("shopId", shopId);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(AdminPage.REPORT_PURCHASERATE_PAGE);
	}
	
	/**
	 * 访问购买率列表
	 *
	 */
	@RequestMapping("/purchaserateContent")
	public String purchaseRateContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,Long shopId, String prodName,Date startDate,Date endDate ) throws Exception {
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "cash", "views", "payedBuys" });
		PageSupport<ProductPurchaseRateDto> ps = productService.getPurchaseRate(curPageNO,shopId, prodName, result,startDate,endDate);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopId", shopId);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(AdminPage.REPORT_PURCHASERATE_CONTENT_PAGE);
	}

	/**
	 * 销售分析
	 *
	 */
	@RequestMapping("/saleanalysis")
	public String saleAnalysis(HttpServletRequest request, HttpServletResponse response, SaleAnalysisDto saleAnalysisDto) {
		// saleAnalysisDto 用于传递查询条件
		SaleAnalysisResponse saleAnalysisResponse = reportService.getSaleAnalysis(saleAnalysisDto);
		SaleAnalysis saleAnalysis = parseSaleAnalysis(saleAnalysisResponse);
		request.setAttribute("saleAnalysis", saleAnalysis);
		request.setAttribute("saleAnalysisDto", saleAnalysisDto);
		String rst = PathResolver.getPath(AdminPage.REPORT_SALEANALYSIS_PAGE);
		return rst;
	}

	/**
	 * 销售额总览
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/sales")
	public String sales(HttpServletRequest request, HttpServletResponse response, ReportRequest reportRequest) {
		if (AppUtils.isBlank(reportRequest.getSelectedDate()) && AppUtils.isBlank(reportRequest.getSelectedYear())) {
			reportRequest.setQueryTerms(Constants.CALCULATION_YEAR);
			reportRequest.setSelectedYear(Integer.valueOf(DateUtil.getNowYear()));
		}
		List<ReportDto> result = reportService.getOrderQuantity(reportRequest);
		String reportJson = JSONUtil.getJson(result);
		request.setAttribute("reportJson", reportJson);
		request.setAttribute("salesRequest", reportRequest);
		return PathResolver.getPath(AdminPage.REPORT_SALES_PAGE);
	}
	
	/**
	 * 销售排名
	 *
	 */
	@RequestMapping("/salerank")
	public String saleRank(HttpServletRequest request, HttpServletResponse response, SalerankDto salerankDto, String curPageNO) {
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("salerankDto", salerankDto);
		return PathResolver.getPath(AdminPage.REPORT_SALERANK_PAGE);
	}
	
	/**
	 * 销售排名列表
	 *
	 */
	@RequestMapping("/salerankContent")
	public String saleRankContent(HttpServletRequest request, HttpServletResponse response, SalerankDto salerankDto, String curPageNO) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "salesOrder", "salesAmount" });
		PageSupport<SalerankDto> ps = reportService.querySaleRank(salerankDto, curPageNO, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("salerankDto", salerankDto);
		return PathResolver.getPath(AdminPage.REPORT_SALERANK_CONTENT_PAGE);
	}

	/**
	 * 购买量排名
	 *
	 */
	@RequestMapping("/purchaserank")
	public String purchaseRank(HttpServletRequest request, HttpServletResponse response, String curPageNO, PurchaseRankDto purchaserankDto) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "userOeders", "totalConsumption", "totalRefundAmount", "totalBalanceVolume" });
		PageSupport<PurchaseRankDto> ps = reportService.queryPurchaseRankDto(curPageNO, purchaserankDto, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("purchaserankDto", purchaserankDto);
		return PathResolver.getPath(AdminPage.REPORT_PURCHASERANK_PAGE);
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	/**
	 * 构造页面演示对象
	 *
	 */
	private SaleAnalysis parseSaleAnalysis(SaleAnalysisResponse response) {
		SaleAnalysis saleAnalysis = new SaleAnalysis();
		if (response.getTotalAmount() == null) {
			response.setTotalAmount(0d);
		}
		// 平均会员订单量
		double averageOrders = response.getTotalOrder() == 0 ? 0 : Arith.div(response.getTotalOrder(), response.getTotalMember(), 3);
		AverageMemberOrders averageMemberOrders = new AverageMemberOrders(response.getTotalMember(), response.getTotalOrder(), averageOrders);

		// 客户订单转化率
		Double averageRate = response.getTotalViews() == 0l ? 0 : Arith.div(response.getTotalOrder(), response.getTotalViews(), 3);
		AverageOrderAmouont averageOrderAmouont = new AverageOrderAmouont(response.getTotalViews(), response.getTotalOrder(), averageRate);

		// 平均订单金额
		Double averageAmout = response.getTotalOrder() == 0l ? 0d : Arith.div(response.getTotalAmount(), response.getTotalOrder(), 3);
		OrdersConversionRate ordersConversionRate = new OrdersConversionRate(response.getTotalAmount(), response.getTotalOrder(), averageAmout);

		// 注册会员购买率
		Double averagePurchaseRate = response.getTotalMember() == 0 ? 0 : Arith.div(response.getHasOrder(), response.getTotalMember(), 3);
		RegisteredPurchaseRate registeredPurchaseRate = new RegisteredPurchaseRate(response.getTotalMember(), response.getHasOrder(), averagePurchaseRate);

		saleAnalysis.setAverageMemberOrders(averageMemberOrders);
		saleAnalysis.setAverageOrderAmouont(averageOrderAmouont);
		saleAnalysis.setOrdersConversionRate(ordersConversionRate);
		saleAnalysis.setRegisteredPurchaseRate(registeredPurchaseRate);
		return saleAnalysis;
	}

	/**
	 * 选项，获取周期
	 * 
	 * @param request
	 * @param response
	 * @param year
	 * @param month
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getWeek")
	public @ResponseBody String getWeek(HttpServletRequest request, HttpServletResponse response, int year, int month) throws Exception {
		return reportService.getWeek(year, month);
	}

	public Logger getLog() {
		return log;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
}
