/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.ThemeRelatedDto;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Theme;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.model.entity.ThemeRelated;
import com.legendshop.model.vo.ThemeRelatedVo;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ThemeModuleProdService;
import com.legendshop.spi.service.ThemeModuleService;
import com.legendshop.spi.service.ThemeRelatedService;
import com.legendshop.spi.service.ThemeService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 后台专题控制器
 * 
 */
@Controller
@RequestMapping("/admin/theme")
public class ThemeAdminController extends BaseController{
	
	private final Logger logger = LoggerFactory.getLogger(ThemeAdminController.class);

	@Autowired
	private ThemeService themeService;

	@Autowired
	private ThemeModuleService themeModuleService;

	@Autowired
	private ThemeModuleProdService themeModuleProdService;

	@Autowired
	private ThemeRelatedService themeRelatedService;

	@Autowired
	private ProductService productService;

	@Autowired
	AttachmentManager attachmentManager;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;


	/**
	 * 初始化
	 * 
	 * @param binder
	 * @throws Exception
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 后台专题列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Theme entity) {

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "startTime", "endTime", "updateTime" });

		PageSupport<Theme> ps = themeService.getThemePage(curPageNO, entity, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("theme", entity);
		Integer status = entity.getStatus() == null ? 1 : entity.getStatus();
		request.setAttribute("operation", status);

		request.setAttribute("pcDomainName", propertiesUtil.getPcDomainName());
		request.setAttribute("vueDomainName", propertiesUtil.getVueDomainName());
		return PathResolver.getPath(AdminPage.THEME_LIST);
	}

	/**
	 * 异步加载专题
	 * 
	 * @param request
	 * @param response
	 * @param status
	 * @param curPageNO
	 * @param title
	 * @return
	 */
	@RequestMapping(value = "/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, Integer status, String curPageNO, String title) {

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "startTime", "endTime", "updateTime" });
		if (AppUtils.isBlank(status)) {
			return Constants.FAIL;
		}
		PageSupport<Theme> ps = themeService.getThemeQueryContent(curPageNO, status, result, title);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("title", title);
		request.setAttribute("operation", status);
		return PathResolver.getPath(BackPage.THEME_CONTENT);
	}

	/**
	 * 保存专题
	 * 
	 * @param request
	 * @param response
	 * @param theme
	 * @return
	 */
	@SystemControllerLog(description="保存专题")
	@RequestMapping(value = "/saveTheme")
	public @ResponseBody Map<String, Object> saveTheme(HttpServletRequest request, HttpServletResponse response, Theme theme) {
		SecurityUserDetail user = UserManager.getUser(request);
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			theme.setStatus(0);
			if (theme.getIsTitleShow() == null) {
				theme.setIsTitleShow(0l);
			}
			if (theme.getIsIntroShow() == null) {
				theme.setIsIntroShow(0l);
			}
			if (theme.getIsIntroMShow() == null) {
				theme.setIsIntroMShow(0l);
			}
			// 专题pc活动图
			if (theme.getThemePcImgFile() != null) {
				String themePcImg = attachmentManager.upload(theme.getThemePcImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), themePcImg, theme.getThemePcImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getThemePcImg())) {
					attachmentManager.deleteTruely(theme.getThemePcImg());
					attachmentManager.delAttachmentByFilePath(theme.getThemePcImg());
				}
				theme.setThemePcImg(themePcImg);
				retMap.put("themePcImg", themePcImg);
			}
			// 专题手机活动图
			if (theme.getThemeMobileImgFile() != null) {
				String themeMobileImg = attachmentManager.upload(theme.getThemeMobileImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), themeMobileImg, theme.getThemeMobileImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getThemeMobileImg())) {
					attachmentManager.deleteTruely(theme.getThemeMobileImg());
					attachmentManager.delAttachmentByFilePath(theme.getThemeMobileImg());
				}
				theme.setThemeMobileImg(themeMobileImg);
				retMap.put("themeMobileImg", themeMobileImg);
			}
			// pc背景图
			if (theme.getBackgroundPcImgFile() != null) {
				String backgroundPcImg = attachmentManager.upload(theme.getBackgroundPcImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), backgroundPcImg, theme.getBackgroundPcImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getBackgroundPcImg())) {
					attachmentManager.deleteTruely(theme.getBackgroundPcImg());
					attachmentManager.delAttachmentByFilePath(theme.getBackgroundPcImg());
				}
				theme.setBackgroundPcImg(backgroundPcImg);
				retMap.put("backgroundPcImg", backgroundPcImg);
			}
			// 手机背景图
			if (theme.getBackgroundMobileImgFile() != null) {
				String backgroundMobileImg = attachmentManager.upload(theme.getBackgroundMobileImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), backgroundMobileImg, theme.getBackgroundMobileImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getBackgroundMobileImg())) {
					attachmentManager.deleteTruely(theme.getBackgroundMobileImg());
					attachmentManager.delAttachmentByFilePath(theme.getBackgroundMobileImg());
				}
				theme.setBackgroundMobileImg(backgroundMobileImg);
				retMap.put("backgroundMobileImg", backgroundMobileImg);
			}
			// pc横幅图
			if (theme.getBannerPcImgFile() != null) {
				String bannerPcImg = attachmentManager.upload(theme.getBannerPcImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), bannerPcImg, theme.getBannerPcImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getBannerPcImg())) {
					attachmentManager.deleteTruely(theme.getBannerPcImg());
					attachmentManager.delAttachmentByFilePath(theme.getBannerPcImg());
				}
				theme.setBannerPcImg(bannerPcImg);
				retMap.put("bannerPcImg", bannerPcImg);
			}
			// 手机横幅图
			if (theme.getBannerMobileImgFile() != null) {
				String bannerMobileImg = attachmentManager.upload(theme.getBannerMobileImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), bannerMobileImg, theme.getBannerMobileImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(theme.getBannerMobileImg())) {
					attachmentManager.deleteTruely(theme.getBannerMobileImg());
					attachmentManager.delAttachmentByFilePath(theme.getBannerMobileImg());
				}
				theme.setBannerMobileImg(bannerMobileImg);
				retMap.put("bannerMobileImg", bannerMobileImg);
			}
			Long themeId = themeService.saveTheme(theme);

			/*
			 * 清理缓存
			 */
			if (themeId > 0) {
				themeService.clearThemeDetailCache(themeId);
			}

			retMap.put("retCode", "OK");
			retMap.put("themeId", themeId);
		} catch (Exception e) {
			retMap.put("retCode", "FAIL");
			logger.error("save theme exception:{}", e);
		}
		return retMap;
	}

	/**
	 * 保存商品板块
	 * 
	 * @param request
	 * @param response
	 * @param themeModule
	 * @return
	 */
	@RequestMapping(value = "/saveThemeModule")
	public @ResponseBody Map<String, Object> saveThemeModule(HttpServletRequest request, HttpServletResponse response, ThemeModule themeModule) {
		SecurityUserDetail user = UserManager.getUser(request);
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			if (themeModule.getIsTitleShow() == null) {
				themeModule.setIsTitleShow(0l);
			}
			// pc 版头背景图
			if (themeModule.getTitleBgPcimgFile() != null) {
				String titleBgPcimg = attachmentManager.upload(themeModule.getTitleBgPcimgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), titleBgPcimg, themeModule.getTitleBgPcimgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeModule.getTitleBgPcimg())) {
					attachmentManager.deleteTruely(themeModule.getTitleBgPcimg());
					attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgPcimg());
				}
				themeModule.setTitleBgPcimg(titleBgPcimg);
				retMap.put("titleBgPcimg", titleBgPcimg);
			}
			// 手机 版头背景图
			if (themeModule.getTitleBgMobileimgFile() != null) {
				String titleBgMobileimg = attachmentManager.upload(themeModule.getTitleBgMobileimgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), titleBgMobileimg, themeModule.getTitleBgMobileimgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeModule.getTitleBgMobileimg())) {
					attachmentManager.deleteTruely(themeModule.getTitleBgMobileimg());
					attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgMobileimg());
				}
				themeModule.setTitleBgMobileimg(titleBgMobileimg);
				retMap.put("titleBgMobileimg", titleBgMobileimg);
			}
			Long themeModuleId = themeModuleService.saveThemeModule(themeModule);
			/*
			 * 清理缓存
			 */
			if (themeModuleId > 0) {
				if (AppUtils.isNotBlank(themeModule.getThemeId())) {
					themeService.clearThemeDetailCache(themeModule.getThemeId());
				}
			}
			retMap.put("themeModuleId", themeModuleId);
			retMap.put("retCode", "OK");
		} catch (Exception e) {
			retMap.put("retCode", "FAIL");
			logger.error("saveThemeModule exception:{}", e);
		}
		return retMap;
	}

	/**
	 * 删除板块
	 * 
	 * @param request
	 * @param response
	 * @param themeModuleId
	 *            板块id
	 * @return
	 */
	@RequestMapping(value = "/delThemeModule")
	public @ResponseBody Map<String, Object> delThemeModule(HttpServletRequest request, HttpServletResponse response, Long themeModuleId) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			ThemeModule themeModule = themeModuleService.getThemeModule(themeModuleId);
			List<ThemeModuleProd> themeModuleProdList = themeModuleProdService.getThemeModuleProdByModuleid(themeModule.getThemeModuleId());
			themeModuleService.deleteThemeModule(themeModuleProdList,themeModule);
			/*
			 * 清理缓存
			 */
			if (themeModuleId > 0) {
				if (AppUtils.isNotBlank(themeModule.getThemeId())) {
					themeService.clearThemeDetailCache(themeModule.getThemeId());
				}
			}
			retMap.put("retCode", "OK");
		} catch (Exception e) {
			retMap.put("retCode", "FAIL");
			logger.error("delThemeModule exception:{}", e);
		}
		return retMap;
	}

	/**
	 * 保存板块商品
	 * 
	 * @param request
	 * @param response
	 * @param themeModuleProd
	 *            板块商品
	 * @return
	 */
	@RequestMapping(value = "/saveThemeModulePrd")
	public @ResponseBody Map<String, Object> saveThemeModulePrd(HttpServletRequest request, HttpServletResponse response, ThemeModuleProd themeModuleProd) {
		SecurityUserDetail user = UserManager.getUser(request);
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			if (themeModuleProd.getIsSoldOut() == null) {
				themeModuleProd.setIsSoldOut(0l);
			}
			if (themeModuleProd.getProdId() == null) {
				retMap.put("retCode", "FAIL");
				return retMap;
			}
			ProductDetail prodInfo = productService.getProdDetail(themeModuleProd.getProdId());
			if (themeModuleProd.getImgFile() != null) {
				String img = attachmentManager.upload(themeModuleProd.getImgFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), img, themeModuleProd.getImgFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片 如果是商品表的原有图片则不删
				if (AppUtils.isNotBlank(themeModuleProd.getImg()) && !themeModuleProd.getImg().equals(prodInfo.getPic())) {
					attachmentManager.deleteTruely(themeModuleProd.getImg());
					attachmentManager.delAttachmentByFilePath(themeModuleProd.getImg());
				}
				themeModuleProd.setImg(img);
			}
			if (themeModuleProd.getAngleIconFile() != null) {
				String angleIcon = attachmentManager.upload(themeModuleProd.getAngleIconFile());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), angleIcon, themeModuleProd.getAngleIconFile(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeModuleProd.getAngleIcon())) {
					attachmentManager.deleteTruely(themeModuleProd.getAngleIcon());
					attachmentManager.delAttachmentByFilePath(themeModuleProd.getAngleIcon());
				}
				themeModuleProd.setAngleIcon(angleIcon);
			}
			Long modulePrdId = themeModuleProdService.saveThemeModuleProd(themeModuleProd);
			themeModuleProd.setModulePrdId(modulePrdId);
			themeModuleProd.setImgFile(null);
			themeModuleProd.setAngleIconFile(null);
			retMap.put("cash", prodInfo.getCash());
			retMap.put("themeModuleProd", themeModuleProd);
			retMap.put("retCode", "OK");
		} catch (Exception e) {
			retMap.put("retCode", "FAIL");
			logger.error("saveThemeModulePrd exception:{}", e);
		}
		return retMap;
	}

	/**
	 * 根据商品id获取商品信息
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 *            商品id
	 * @return
	 */
	@RequestMapping(value = "/getProdInfo")
	public @ResponseBody Map<String, Object> getProdInfo(HttpServletRequest request, HttpServletResponse response, Long prodId) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		ProductDetail prodInfo = productService.getProdDetail(prodId);
		if (prodInfo == null) {
			retMap.put("retCode", "NONE");
		} else if (!ProductStatusEnum.PROD_ONLINE.value().equals(prodInfo.getStatus())) {
			retMap.put("retCode", "OFF");
		} else {
			retMap.put("retCode", "OK");
			retMap.put("prodInfo", prodInfo);
		}
		return retMap;
	}

	/**
	 * 根据板块商品id 获取板块商品信息
	 * 
	 * @param request
	 * @param response
	 * @param modulePrdId
	 *            板块商品id
	 * @return
	 */
	@RequestMapping(value = "/getModuleProdInfo")
	public @ResponseBody Map<String, Object> getModuleProdInfo(HttpServletRequest request, HttpServletResponse response, Long modulePrdId) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		ThemeModuleProd themeModuleProd = themeModuleProdService.getThemeModuleProd(modulePrdId);
		if (themeModuleProd == null) {
			retMap.put("retCode", "FAIL");
		} else {
			retMap.put("retCode", "OK");
			retMap.put("themeModuleProd", themeModuleProd);
		}
		return retMap;
	}

	/**
	 * 删除板块商品
	 * 
	 * @param request
	 * @param response
	 * @param modulePrdId
	 *            板块商品id
	 * @return
	 */
	@RequestMapping(value = "/delModuleProd")
	public @ResponseBody Map<String, Object> delModuleProd(HttpServletRequest request, HttpServletResponse response, Long modulePrdId) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (modulePrdId == null) {
			retMap.put("retCode", "FAIL");
		} else {
			try {
				ThemeModuleProd themeModuleProd = themeModuleProdService.getThemeModuleProd(modulePrdId);
				ProductDetail prodInfo = productService.getProdDetail(themeModuleProd.getProdId());
				themeModuleProdService.deleteThemeModuleProd(prodInfo,themeModuleProd);
				retMap.put("retCode", "OK");
			} catch (Exception e) {
				retMap.put("retCode", "FAIL");
			}
		}
		return retMap;
	}

	/**
	 * 保存相关专题
	 * 
	 * @param request
	 * @param response
	 * @param themeRelatedVo
	 * @return
	 */
	@SystemControllerLog(description="保存相关专题")
	@RequestMapping(value = "/saveThemeRelated")
	public @ResponseBody Map<String, Object> saveThemeRelated(HttpServletRequest request, HttpServletResponse response, ThemeRelatedVo themeRelatedVo) {
		SecurityUserDetail user = UserManager.getUser(request);

		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			// 相关专题1的图片上传
			if (themeRelatedVo.getImgFile1() != null) {
				String img1 = attachmentManager.upload(themeRelatedVo.getImgFile1());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), img1, themeRelatedVo.getImgFile1(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeRelatedVo.getImg1())) {
					attachmentManager.deleteTruely(themeRelatedVo.getImg1());
					attachmentManager.delAttachmentByFilePath(themeRelatedVo.getImg1());
				}
				themeRelatedVo.setImg1(img1);
				retMap.put("img1", img1);
			}
			// 相关专题2的图片上传
			if (themeRelatedVo.getImgFile2() != null) {
				String img2 = attachmentManager.upload(themeRelatedVo.getImgFile2());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), img2, themeRelatedVo.getImgFile2(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeRelatedVo.getImg2())) {
					attachmentManager.deleteTruely(themeRelatedVo.getImg2());
					attachmentManager.delAttachmentByFilePath(themeRelatedVo.getImg2());
				}
				themeRelatedVo.setImg2(img2);
				retMap.put("img2", img2);
			}
			// 相关专题3的图片上传
			if (themeRelatedVo.getImgFile3() != null) {
				String img3 = attachmentManager.upload(themeRelatedVo.getImgFile3());
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), img3, themeRelatedVo.getImgFile3(), AttachmentTypeEnum.THEME);
				// 删除原有的图片
				if (AppUtils.isNotBlank(themeRelatedVo.getImg3())) {
					attachmentManager.deleteTruely(themeRelatedVo.getImg3());
					attachmentManager.delAttachmentByFilePath(themeRelatedVo.getImg3());
				}
				themeRelatedVo.setImg3(img3);
				retMap.put("img3", img3);
			}

			// 相关专题1的保存
			ThemeRelated themeRelated1 = null;
			if (themeRelatedVo.getRelatedId1() != null) {
				themeRelated1 = themeRelatedService.getThemeRelated(themeRelatedVo.getRelatedId1());

//				themeRelated1.setmLink(themeRelatedVo.getLinkM1());
				if (AppUtils.isNotBlank(themeRelatedVo.getImg1())) {
					themeRelated1.setImg(themeRelatedVo.getImg1());
				}
			} else {
				themeRelated1 = new ThemeRelated();
//				themeRelated1.setImg(themeRelatedVo.getImg1());
//				themeRelated1.setLink(themeRelatedVo.getLink1());
//				themeRelated1.setmLink(themeRelatedVo.getLinkM1());
				themeRelated1.setThemeId(themeRelatedVo.getThemeId());
			}
			themeRelated1.setRelatedThemeId(themeRelatedVo.getRelatedThemeId1());
			themeRelated1.setmRelatedThemeId(themeRelatedVo.getmRelatedThemeId1());
			Long relatedId1 = themeRelatedService.saveThemeRelated(themeRelated1);
			themeRelated1.setRelatedId(relatedId1);

			// 相关专题2的保存
			ThemeRelated themeRelated2 = null;
			if (themeRelatedVo.getRelatedId2() != null) {
				themeRelated2 = themeRelatedService.getThemeRelated(themeRelatedVo.getRelatedId2());
//				themeRelated2.setLink(themeRelatedVo.getLink2());
//				themeRelated2.setmLink(themeRelatedVo.getLinkM2());
				if (AppUtils.isNotBlank(themeRelatedVo.getImg2())) {
					themeRelated2.setImg(themeRelatedVo.getImg2());
				}
			} else {
				themeRelated2 = new ThemeRelated();
				themeRelated2.setImg(themeRelatedVo.getImg2());
//				themeRelated2.setLink(themeRelatedVo.getLink2());
//				themeRelated2.setmLink(themeRelatedVo.getLinkM2());
				themeRelated2.setThemeId(themeRelatedVo.getThemeId());
			}
			themeRelated2.setRelatedThemeId(themeRelatedVo.getRelatedThemeId2());
			themeRelated2.setmRelatedThemeId(themeRelatedVo.getmRelatedThemeId2());
			Long relatedId2 = themeRelatedService.saveThemeRelated(themeRelated2);
			themeRelated2.setRelatedId(relatedId2);

			// 相关专题3的保存
			ThemeRelated themeRelated3 = null;
			if (themeRelatedVo.getRelatedId3() != null) {
				themeRelated3 = themeRelatedService.getThemeRelated(themeRelatedVo.getRelatedId3());
//				themeRelated3.setLink(themeRelatedVo.getLink3());
//				themeRelated3.setmLink(themeRelatedVo.getLinkM3());
				if (AppUtils.isNotBlank(themeRelatedVo.getImg3())) {
					themeRelated3.setImg(themeRelatedVo.getImg3());
				}
			} else {
				themeRelated3 = new ThemeRelated();
				themeRelated3.setImg(themeRelatedVo.getImg3());
//				themeRelated3.setLink(themeRelatedVo.getLink3());
//				themeRelated3.setmLink(themeRelatedVo.getLinkM3());
				themeRelated3.setThemeId(themeRelatedVo.getThemeId());
			}
			themeRelated3.setRelatedThemeId(themeRelatedVo.getRelatedThemeId3());
			themeRelated3.setmRelatedThemeId(themeRelatedVo.getmRelatedThemeId3());
			Long relatedId3 = themeRelatedService.saveThemeRelated(themeRelated3);
			themeRelated3.setRelatedId(relatedId3);
			/*
			 * 清理缓存
			 */
			if (relatedId1 > 0 || relatedId2 > 0 || relatedId3 > 0) {
				if (AppUtils.isNotBlank(themeRelatedVo.getThemeId())) {
					themeService.clearThemeDetailCache(themeRelatedVo.getThemeId());
				}
			}

			retMap.put("relatedId1", relatedId1);
			retMap.put("relatedId2", relatedId2);
			retMap.put("relatedId3", relatedId3);
			retMap.put("retCode", "OK");
		} catch (Exception e) {
			retMap.put("retCode", "FAIL");
			logger.error("saveThemeRelated exception:{}", e);
		}
		return retMap;
	}

	/**
	 * 删除专题图片
	 * 
	 * @param request
	 * @param response
	 * @param themeId
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/delThemeImg")
	public @ResponseBody Map<String, Object> delThemeImg(HttpServletRequest request, HttpServletResponse response, Long themeId, String type) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (themeId == null) {
			retMap.put("retCode", "FAIL");
		} else {
			try {
				Theme theme = themeService.getTheme(themeId);
				themeService.updateTheme(type,theme);
				retMap.put("retCode", "OK");
			} catch (Exception e) {
				retMap.put("retCode", "FAIL");
			}
		}
		return retMap;
	}

	/**
	 * 删除板块图片
	 * 
	 * @param request
	 * @param response
	 * @param themeModuleId
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/delModuleImg")
	public @ResponseBody Map<String, Object> delModuleImg(HttpServletRequest request, HttpServletResponse response, Long themeModuleId, String type) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (themeModuleId == null) {
			retMap.put("retCode", "FAIL");
		} else {
			try {
				ThemeModule themeModule = themeModuleService.getThemeModule(themeModuleId);
				themeModuleService.updateThemeModule(type,themeModule);
				retMap.put("retCode", "OK");
			} catch (Exception e) {
				retMap.put("retCode", "FAIL");
			}
		}
		return retMap;
	}

	/**
	 * 删除板块商品角标图片
	 * 
	 * @param request
	 * @param response
	 * @param modulePrdId
	 * @return
	 */
	@RequestMapping(value = "/delAngleIcon")
	public @ResponseBody Map<String, Object> delAngleIcon(HttpServletRequest request, HttpServletResponse response, Long modulePrdId) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (modulePrdId == null) {
			retMap.put("retCode", "FAIL");
		} else {
			try {
				ThemeModuleProd themeModuleProd = themeModuleProdService.getThemeModuleProd(modulePrdId);
				/*if (AppUtils.isNotBlank(themeModuleProd.getAngleIcon())) {
					attachmentManager.deleteTruely(themeModuleProd.getAngleIcon());
					attachmentManager.delAttachmentByFilePath(themeModuleProd.getAngleIcon());
					themeModuleProd.setAngleIcon(null);
				}*/
				themeModuleProdService.updateThemeModuleProdt(themeModuleProd);
				retMap.put("retCode", "OK");
			} catch (Exception e) {
				retMap.put("retCode", "FAIL");
			}
		}
		return retMap;
	}

	/**
	 * 删除专题
	 */
	@SystemControllerLog(description="删除专题")
	@RequestMapping(value = "/delete/{id}")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		String result = themeService.deleteTheme(id);
		if (Constants.SUCCESS.equals(result)) {
			themeService.clearThemeDetailCache(id);
		}
		return result;
	}

	/**
	 * 更新状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新专题状态")
	@RequestMapping(value = "/updataStatus/{id}/{status}")
	public @ResponseBody String updataStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		String result = themeService.updataStatus(id, status);
		// if (Constants.SUCCESS.equals(result)) {
		// themeService.clearThemeDetailCache(id);
		// }
		return result;
	}

	/**
	 * 查看专题编辑详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Theme theme = themeService.getTheme(id);
		List<ThemeModule> themeModuleList = themeModuleService.getThemeModuleByTheme(id);
		List<ThemeRelated> themeRelatedList = themeRelatedService.getThemeRelatedByTheme(id);
		List<ThemeRelatedDto> themeRelatedDtos = themeRelatedService.getThemeRelatedDtoByTheme(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()), theme.getUserName());
		// if(result!=null){
		// return result;
		// }
		if (AppUtils.isNotBlank(themeModuleList)) {
			for (ThemeModule themeModule : themeModuleList) {
				List<ThemeModuleProd> moduleProdList = themeModuleProdService.getThemeModuleProdByModuleid(themeModule.getThemeModuleId());
				if (AppUtils.isNotBlank(moduleProdList)) {
					for (ThemeModuleProd themeModuleProd : moduleProdList) {
						ProductDetail prod = productService.getProdDetail(themeModuleProd.getProdId());
						if (prod != null) {
							themeModuleProd.setCash(prod.getCash());
						}
					}
				}
				themeModule.setModuleProdList(moduleProdList);
			}
		}
		request.setAttribute("theme", theme);
		request.setAttribute("themeRelatedDtoList",themeRelatedDtos);
		request.setAttribute("themeModuleList", themeModuleList);
		request.setAttribute("themeRelatedList", themeRelatedList);
		return PathResolver.getPath(AdminPage.THEME_EDIT_PAGE);
	}

	/**
	 * 跳转专题编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.THEME_EDIT_PAGE);
	}

	/**
	 * 更新专题
	 */
	@RequestMapping(value = "/loadSelectTheme")
	public String addRelatedTheme(HttpServletRequest request, HttpServletResponse response) {
		String curPageNO = request.getParameter("curPageNO");
		String title = request.getParameter("title");
		String themeid = request.getParameter("themeId");
		
		Long themeId = null;
		if (AppUtils.isNotBlank(themeid)) {
			themeId = Long.parseLong(request.getParameter("themeId"));
		}
		
		Integer index = null;
		if (AppUtils.isNotBlank(request.getParameter("index"))){
			index = Integer.parseInt(request.getParameter("index"));
		}

		PageSupport<Theme> ps = themeService.getSelectTheme(curPageNO,title,themeId);
		PageSupportHelper.savePage(request, ps);

		request.setAttribute("curPageNO",curPageNO);
		request.setAttribute("index",index);
		request.setAttribute("title",title);
		request.setAttribute("type",request.getParameter("type"));
		request.setAttribute("themeId",themeId);

//		PageSupport<ShopDetail> ps = shopDetailService.getloadSelectShop(curPageNO, shopName);
//		request.setAttribute("shopName", shopName);
		return PathResolver.getPath(AdminPage.LOAD_SELECT_THEME);
	}

	@SystemControllerLog(description="更新专题")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Theme theme = themeService.getTheme(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()), theme.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("theme", theme);
		return PathResolver.getPath(FowardPage.THEME_LIST_QUERY);
	}

	/**
	 * 保存专题
	 */
	public String save(HttpServletRequest request, HttpServletResponse response, Theme entity) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 用于后台预览专题
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/preview/{themeId}")
	public String preview(HttpServletRequest request, HttpServletResponse response, @PathVariable Long themeId) {
		Theme theme = themeService.getTheme(themeId);
		Map<ThemeModule, List<ThemeModuleProd>> themeModuleMap = themeModuleService.getThemeModuleAndProdByThemeId(themeId);
		List<ThemeRelated> themeRelated = themeRelatedService.getThemeRelatedByTheme(themeId);
		request.setAttribute("theme", theme);
		request.setAttribute("themeModuleMap", themeModuleMap);
		request.setAttribute("themeRelated", themeRelated);
		request.setAttribute("disTime", theme.getEndTime().getTime() - new Date().getTime());
		PageDefinition page = null;
		if (theme.getTemplateType() == null) {
			page = BackPage.THEME_DETAIL;
		} else {
			switch (theme.getTemplateType()) {
			case 1:
				page = BackPage.THEME_DETAIL;
				break;
			case 2:
				page = BackPage.THEME_DETAIL2;
				break;
			default:
				page = BackPage.THEME_DETAIL;
			}
		}
		return PathResolver.getPath(page);
	}

	/**
	 * 商品楼层加载
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "/productThemeLoad")
	public String productThemeLoad(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			Product product) {
		
		PageSupport<Product> ps = productService.getProductListPage(curPageNO, product);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(AdminPage.PRODUCT_THEME_LOAD);
	}

	/**
	 * 用于后台查看更多专题
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value="/allThemes",method= RequestMethod.GET)
	public String allThemes(HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		PageSupport<Theme> ps=themeService.allThemes(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("pcDomainName", propertiesUtil.getPcDomainName());
		request.setAttribute("vueDomainName", propertiesUtil.getVueDomainName());
		return PathResolver.getPath(AdminPage.THEME_LIST);
	}
}
