/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DeliveryCorpService;
import com.legendshop.util.AppUtils;

/**
 * 物流公司管理
 * 
 */
@Controller
@RequestMapping("/admin/deliveryCorp")
public class DeliveryCorpController extends BaseController{
	
	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 查询物流公司
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, DeliveryCorp deliveryCorp) {
		PageSupport<DeliveryCorp> ps = deliveryCorpService.getDeliveryCorpPage(curPageNO,deliveryCorp);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("deliveryCorp", deliveryCorp);
		return PathResolver.getPath(AdminPage.DELIVERYCORP_LIST_PAGE);
	}
	
	/**
	 * 保存物流公司
	 */
	@SystemControllerLog(description="保存物流公司")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, DeliveryCorp deliveryCorp) {
		DeliveryCorp dc = null;
		if (AppUtils.isNotBlank(deliveryCorp.getDvyId())) {
			dc = deliveryCorpService.getDeliveryCorp(deliveryCorp.getDvyId());
			dc.setName(deliveryCorp.getName());
			dc.setCompanyHomeUrl(deliveryCorp.getCompanyHomeUrl());
			dc.setQueryUrl(deliveryCorp.getQueryUrl());
			dc.setCompanyCode(deliveryCorp.getCompanyCode());
		} else {
			dc = deliveryCorp;
			deliveryCorp.setCreateTime(new Date());
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		dc.setUserId(user.getUserId());
		dc.setUserName(user.getUsername());
		dc.setShopId(systemParameterUtil.getDefaultShopId());
		dc.setModifyTime(new Date());

		deliveryCorpService.saveDeliveryCorp(dc);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.DELIVERYCORP_LIST_QUERY);
	}
	
	/**
	 * 删除物流公司
	 */
	@SystemControllerLog(description="删除物流公司")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorp(id);
		deliveryCorpService.deleteDeliveryCorp(deliveryCorp);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.DELIVERYCORP_LIST_QUERY);
	}
	
	/**
	 * 根据id加载物流公司编辑页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorp(id);
		request.setAttribute("deliveryCorp", deliveryCorp);
		return PathResolver.getPath(AdminPage.DELIVERYCORP_EDIT_PAGE);
	}
	
	/**
	 * 加载物流公司编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.DELIVERYCORP_EDIT_PAGE);
	}
	
	/**
	 * 更新物流公司
	 */
	@SystemControllerLog(description="更新物流公司")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorp(id);
		request.setAttribute("deliveryCorp", deliveryCorp);
		return PathResolver.getPath(RedirectPage.DELIVERYCORP_LIST_QUERY);
	}

}
