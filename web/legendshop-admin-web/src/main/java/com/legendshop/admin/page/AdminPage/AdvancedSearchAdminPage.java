/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.AdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * Tiles页面定义.
 */
public enum AdvancedSearchAdminPage implements PageDefinition {
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 重建索引 **/
	LUCENE_EDIT_PAGE("/lucene/reindexer"),

	;

	/** The value. */
	private final String value;

	private AdvancedSearchAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	public String getNativeValue() {
		return value;
	}

}
