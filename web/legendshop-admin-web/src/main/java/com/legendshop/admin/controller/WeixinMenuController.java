/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinRedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.util.WxConfig;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.entity.weixin.WeixinMenu;
import com.legendshop.spi.service.WeiXintemplateService;
import com.legendshop.spi.service.WeixinMenuService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信菜单管理
 *
 */
@Controller
@RequestMapping("/admin/weixinMenu")
public class WeixinMenuController extends BaseController{
    @Autowired
    private WeixinMenuService weixinMenuService;
    
    @Autowired
    private WeiXintemplateService weiXintemplateService;
    
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinMenu weixinMenu) {
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"seq"});
        PageSupport<WeixinMenu> ps = weixinMenuService.getWeixinMenu(curPageNO,weixinMenu,result);
        PageSupportHelper.savePage(request, ps);
        Integer menuLevel=1;
		if(AppUtils.isNotBlank(ps.getResultList())){
			menuLevel= ps.getResultList().get(0).getGrade();
		}
		List<KeyValueEntity> allParentMenu = weixinMenuService.getParentMenu(weixinMenu.getParentId());
		if(AppUtils.isNotBlank(allParentMenu)){
			allParentMenu.remove(allParentMenu.size()-1);
		}
		
	    request.setAttribute("allParentMenu",allParentMenu);
        request.setAttribute("weixinMenu", weixinMenu);
        request.setAttribute("menuLevel",menuLevel);
        request.setAttribute("parentId", weixinMenu.getParentId());
        return PathResolver.getPath(WeixinAdminPage.WEIXIN_MENU_LIST);
    }
    
	
    /**
     * 保存微信菜单
     * @param request
     * @param response
     * @param weixinMenu
     * @return
     */
    @SystemControllerLog(description="保存微信菜单")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, WeixinMenu weixinMenu) {
    	weixinMenuService.saveWeixinMenu(weixinMenu);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		//发布更新菜单指令
        return fowardToMenuList(request,response, weixinMenu);
    }
    
    /**
     * 根据目前的menu导航到不同level的菜单管理页面
     * @param request
     * @param response
     * @param menu
     * @return
     */
   private String fowardToMenuList(HttpServletRequest request, HttpServletResponse response,WeixinMenu menu){
       if(	1 == menu.getGrade()){//一级菜单
       	return PathResolver.getPath(WeixinRedirectPage.MENU_LIST_QUERY);
       }else{//二级和三级菜单
    	 WeixinMenu parentMenu= weixinMenuService.getMenu(menu.getParentId());
       	 Long parentId = parentMenu.getId();
       	 return PathResolver.getPath(WeixinRedirectPage.CHILD_MENU_LIST) + parentId;
       }
   }
   
    
    @RequestMapping(value = "/create")
	public String create(HttpServletRequest request, HttpServletResponse response,Long parentId) {
		request.setAttribute("parentMenu", weixinMenuService.getMenu(parentId));
        if(parentId != null){
            List<KeyValueEntity> allParentMenu = weixinMenuService.getParentMenu(parentId);
            request.setAttribute("allParentMenu",allParentMenu);
        }
		return PathResolver.getPath(WeixinAdminPage.MENU_EDIT_PAGE);
	}
    
    
    /**
     * 删除微信菜单
     * @param request
     * @param response
     * @param menuId
     * @return
     */
    @SystemControllerLog(description="删除微信菜单")
    @RequestMapping(value = "/delete/{menuId}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long menuId) {
    	WeixinMenu menu = weixinMenuService.getMenu(menuId);
    	if(!weixinMenuService.hasSubWeixinMenu(menuId)){
    		weixinMenuService.deleteWeixinMenu(menu);
    		 saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
    	}else{
    		saveError(request, ResourceBundleHelper.getString("","请先删除子菜单，再删除父菜单"));
    	}
		//发布更新菜单指令
        return fowardToMenuList(request,response, menu);
    }
    
    @RequestMapping(value = "/isExitMenuKey")
    @ResponseBody
    public boolean isExitMenuKey(HttpServletRequest request, HttpServletResponse response, String oldmenukey,String menukey) {
		if(isTheSameValue(oldmenukey,menukey)){
			return true;
		}
    	boolean account = weixinMenuService.isExitMenuKey(menukey);
		if (account) {
			return false;
		} else {
			return true;
		}
     }
    
	/**
	 * 判断2个值是否一致
	 * @param value
	 * @param value2
	 * @return
	 */
	private boolean isTheSameValue(String oldmenukey, String menukey){
		if(AppUtils.isBlank(oldmenukey) && AppUtils.isBlank(menukey)){ //2个都为空，则认为相同
			return true;
		}
		if(AppUtils.isBlank(oldmenukey) || AppUtils.isBlank(menukey)){//任意一个为空就不相等
			return false;
		}
		return oldmenukey.equals(menukey);
	}
	
    
    

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable  Long id) {
 	   WeixinMenu menu = weixinMenuService.getWeixinMenu(id);
        request.setAttribute("menu", menu);
        if(menu != null && menu.getParentId() != null){
     	   WeixinMenu parentMenu= weixinMenuService.getMenu(menu.getParentId());
        	request.setAttribute("parentMenu", parentMenu);
        }
         return PathResolver.getPath(WeixinAdminPage.MENU_EDIT_PAGE);
    }
    
    /**
     * 微信菜单管理列表
     * @param request
     * @param response
     * @param curPageNO
     * @param parentId
     * @return
     */
	@RequestMapping("/queryChildMenu/{parentId}")
    public String queryChildMenu(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long parentId) {
		WeixinMenu parentMenu= weixinMenuService.getMenu(parentId);
    	request.setAttribute("parentMenu", parentMenu);
		int pageSize = 30;
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"seq"});
        PageSupport<WeixinMenu> ps = weixinMenuService.getWeixinMenu(curPageNO,parentId,pageSize,result);
        PageSupportHelper.savePage(request,ps);
        request.setAttribute("menuLevel", parentMenu.getGrade() + 1);//得到下一级菜单
        List<KeyValueEntity> allParentMenu = weixinMenuService.getParentMenu(parentId);
    	if(AppUtils.isNotBlank(allParentMenu)){
			allParentMenu.remove(allParentMenu.size()-1);
		}
        request.setAttribute("allParentMenu",allParentMenu);
    	request.setAttribute("parentId", parentId);
        return PathResolver.getPath(WeixinAdminPage.WEIXIN_MENU_LIST);
    }
	
	
	/**
	 * 同步微信菜单
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/synchronous")
	@ResponseBody
    public AjaxJson synchronous(HttpServletRequest request, HttpServletResponse response
    		) {
		String addr=WxConfig.SYNCHRONOUS;
		Map<String, String> paramMap=new HashMap<String, String>();
		String token=RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",  MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(addr,paramMap);
		if(AppUtils.isNotBlank(result) && !"null".equals(result)){
			AjaxJson ajaxJson=JSONUtil.getObject(result, AjaxJson.class);
			return ajaxJson;
		}
		return null;
    }
	
	
	
	/**
	 * 
	 */
	@RequestMapping(value="/getTemplates",method=RequestMethod.POST)
	@ResponseBody
	public String getTemplates(String msgType){
		List<KeyValueEntity> entities= weiXintemplateService.getTemplates(msgType);
		if(AppUtils.isBlank(entities)){
			return null;
		}
		return JSONUtil.getJson(entities);
	}
	
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/weixinMenu/weixinMenu";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.WEIXINMENU_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        WeixinMenu weixinMenu = weixinMenuService.getWeixinMenu(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinMenu.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("weixinMenu", weixinMenu);
		return "forward:/admin/weixinMenu/query.htm";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.WEIXINMENU_EDIT_PAGE);
    }
    
    

}
