/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.SystemConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.AdminDashboard;
import com.legendshop.model.entity.Menu;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.menu.MenuManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AdminDashboardService;
import com.legendshop.web.util.CookieUtil;

/**
 * 后台首页功能.
 */
@Controller
public class IndexAdminController extends BaseController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(IndexAdminController.class);

	/** The index service. */
	@Autowired
	private AdminDashboardService adminDashboardService;

	@Autowired
	private MenuManager menuManager;

	@Autowired
	private AdminMenuUtil adminMenuUtil;


	@Autowired
	private SystemConfigService systemConfigService;

	/**
	  * 首页
	 */
	@RequestMapping(value = {"/"})
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(RedirectPage.ADMIN_INDEX_QUERY);
	}


	/**
	 * 记住当前选择的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = {"/admin/index"})
	public String home(HttpServletRequest request, HttpServletResponse response) {
		adminMenuUtil.parseMenu(request, 0); //固定死选择第一个菜单
		CookieUtil.deleteCookie(request, response, "_ls_menu_");//重新进入， 清空cookies中的当前选中菜单，默认选中第一个。see legendshop.js

		/** 在线用户  **/
		//int onLineUser = adminSessionRegistry.getAllPrincipals().size();
//		long onLineUser = 0;
//		if(mySessionListener != null){
//			 onLineUser = mySessionListener.getPrincipalOnlineSize();
//		}
//		request.setAttribute("onLineUser",onLineUser);

		List<AdminDashboard> dashboardList = adminDashboardService.queryAdminDashboard();
		for (AdminDashboard adminDashboard : dashboardList) {
			request.setAttribute(adminDashboard.getDashboardName(),adminDashboard.getValue());
		}
		SystemConfig config = systemConfigService.getSystemConfig();
		request.setAttribute("config", config);
		return PathResolver.getPath(AdminPage.ADMIN_INDEX_PAGE);
	}

	/**
	 * 菜单
	 * @param request
	 * @param response
	 * @param order
	 * @return
	 */
	@RequestMapping("/admin/menu/{order}")
	public String menu(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer order) {
			Menu currentMenu = adminMenuUtil.parseMenu(request, order); //固定死选择某个个菜单

			 //记录三级菜单的action
			 String menuAction = null;
				 if(currentMenu!=null && currentMenu.getSubList() != null && currentMenu.getSubList().get(0).getSubList() != null){
					 Menu thirdMenu =currentMenu.getSubList().get(0).getSubList().get(0);
					 menuAction = thirdMenu.getAction();
				 }

				 if(menuAction == null){//返回首页
					 return PathResolver.getPath(RedirectPage.ADMIN_INDEX_QUERY);
				 }

			 return PathResolver.getRedirectPath(menuAction);

	}

	/**
	 * 显示左边的菜单
	 * @param request
	 * @param response
	 * @param order
	 * @return
	 */
	@RequestMapping("/admin/menuContent/{order}")
	public String menuContent(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer order) {
		adminMenuUtil.parseMenu(request, order); //固定死选择某个个菜单
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		HttpSession session = request.getSession();
		session.setAttribute("systemConfig",systemConfig);
		return PathResolver.getPath(BackPage.MENU);
	}

	/**
	 * 显示顶部的菜单
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/admin/top")
	public String top(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		SecurityUserDetail user = UserManager.getUser(request);
		//加载菜单
		try {
			 menuManager.calUserMenus(user, session);
		} catch (Exception e) {
			session.removeAttribute(Constants.MENU_LIST);
			log.error("Calculate admin menu error ", e );
		}

		SystemConfig systemConfig = systemConfigService.getSystemConfig();

		request.setAttribute("logo", systemConfig.getLogo());
		request.setAttribute("shopName", systemConfig.getShopName());

		return PathResolver.getPath(BackPage.ADMIN_TOP);
	}

	/**
	 * 后台登陆页面.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping("/adminlogin")
	public String login(HttpServletRequest request, HttpServletResponse response) {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("adminLogo", systemConfig.getAdminLogoCode());
		request.setAttribute("shopName", systemConfig.getShopName());
		return PathResolver.getPath(BackPage.ADMIN_LOGIN);
	}

	/**
	 * 错误页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/admin/404")
	public String error404(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.ADMIN_404);
	}


}
