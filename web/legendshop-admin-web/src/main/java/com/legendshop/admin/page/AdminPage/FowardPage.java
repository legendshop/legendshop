/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.AdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 跳转页面.
 */
public enum FowardPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 广告管理 */
	ADV_LIST_QUERY("/admin/advertisement/query"),

	/** 链接管理 */
	LINK_LIST_QUERY("/admin/externallink/query"),

	/** 热门商品管理 */
	HOT_LIST_QUERY("/admin/hotsearch/query"),

	/** 图片管理 */
	IMG_LIST_QUERY("/admin/imgFile/query"),

	/** 支付管理 */
	PAY_TYPE_LIST_QUERY("/admin/paytype/query"),

	/**  评论管理 */
	PROD_COMM_LIST_QUERY("/admin/productcomment/query"),

	/** 浏览管理 */
	VLOG_LIST_QUERY("/admin/visitLog/query"),

	/** 供应商管理 */
	PARTNER_LIST_QUERY("/admin/partner/query"),

	/** 标签管理 */
	TAG_QUERY("/admin/tag/query"),

	/** 系统日志 */
	EVENT_QUERY("/admin/event/query"),

	/** 商品参数 */
	PRODUCTPROPERTY_LIST_QUERY("/admin/productProperty/query"),

	/** 首页楼层 */
	FLOOR_LIST_QUERY("/admin/floor/query"),

	/** 商品评论 */
	PRODUCTREPLY_LIST_QUERY("/admin/productReply/query"),

	/** 专题 */
	THEME_LIST_QUERY("/admin/theme/query"),

	/** 头条管理 */
	HEADLINE_LIST("/admin/headlineAdmin/query"),

	/** 创建头条 */
	HEADLINE_SAVE("/admin/headlineAdmin/save"),
	
	/**  版权验证 */
	ADMIN_UPGRADE_PAGE("/license/upgrade"),

	/** 激活码验证 */
	ADMIN_UPGRADE_BYKEY_PAGE("/license/upgradeByKey"),

	/** 新建头条 */
	HEADLINE_UPDATE("/admin/headlineAdmin/load/");




	/** The value. */
	private final String value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private FowardPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
