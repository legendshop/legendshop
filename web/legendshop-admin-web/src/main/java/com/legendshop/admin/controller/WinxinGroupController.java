/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.WeixinAdminPage.WeixinBackPage;
import com.legendshop.base.util.WxConfig;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.entity.weixin.WeixinGroup;
import com.legendshop.spi.service.WeixinGroupService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信用户分组控制层
 * 
 * @author liyuan
 *
 */
@Controller
@RequestMapping("/admin/weixinGroup")
public class WinxinGroupController extends BaseController {
	
	@Autowired
	private WeixinGroupService weixinGroupService;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@RequestMapping(value = "/query/{groupId}")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long groupId) {
		// 所有分组信息
		List<WeixinGroup> weixinGroupList = weixinGroupService.getWeixinGroup();
		request.setAttribute("weixinGroupList", weixinGroupList);
		request.setAttribute("groupId", groupId);
		String path = PathResolver.getPath(WeixinBackPage.WEIXIN_USERGROUP_LOADSELECT_PAGE);
		return path;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody String save(HttpServletRequest request, HttpServletResponse response, String newGroupName) {
		List<WeixinGroup> weinxinGroup = weixinGroupService.getWeixinGroup(newGroupName);
		if (AppUtils.isNotBlank(weinxinGroup) && weinxinGroup.size() > 0) {
			return Constants.FAIL;
		}
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("groupName", newGroupName);
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result = HttpUtil.httpPost(WxConfig.createGroups, paramMap);
		if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
			AjaxJson json = JSONUtil.getObject(result, AjaxJson.class);
			if (AppUtils.isNotBlank(json) && json.isSuccess()) {
				WeixinGroup group = JSONUtil.getObject(json.getSuccessValue(), WeixinGroup.class);
				if (group != null) {
					WeixinGroup w = new WeixinGroup();
					w.setId(group.getId());
					w.setName(newGroupName);
					w.setAddtime(new Date());
					weixinGroupService.saveWeixinGroup(w);
					return Constants.SUCCESS;
				}
			} else {
				return json.getMsg();
			}
		}
		return Constants.FAIL;
	}

}
