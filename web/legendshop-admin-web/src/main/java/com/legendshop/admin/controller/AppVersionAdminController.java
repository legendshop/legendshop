/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppVersion;
import com.legendshop.spi.service.AppVersionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * app版本管理
 */
@Controller
@RequestMapping("/admin/appVersion")
public class AppVersionAdminController extends BaseController {

	private static Logger LOGGER = LoggerFactory.getLogger(AppVersionAdminController.class);

	@Autowired
	private AppVersionService appsService;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * 查询APK
	 * 
	 * @return
	 */
	@RequestMapping(value = "/query/{platform}", method = RequestMethod.GET)
	public String query(HttpServletRequest request, HttpServletResponse response, @PathVariable String platform) {
		AppVersion appVersion = appsService.getAppVersion();

		if (null == appVersion) {
			throw new BusinessException("对不起, 系统数据出错, 请联系相关技术人员!");
		}

		request.setAttribute("bean", appVersion);
		if ("android".equals(platform)) {
			request.setAttribute("appFilePath", propertiesUtil.getAppFileSubPath());
			return PathResolver.getPath(AdminPage.ANDROID_VERSION_EDIT);
		}
		return PathResolver.getPath(AdminPage.IOS_VERSION_EDIT);
	}

	/**
	 * 版本下载页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/uploadPage/{type}", method = RequestMethod.GET)
	public String uploadPage(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) {
		AppVersion appVersion = appsService.getAppVersion();

		if (null == appVersion) {
			throw new BusinessException("对不起, 系统数据出错, 请联系相关技术人员!");
		}

		if ("user".equals(type)) {
			request.setAttribute("fileName", appVersion.getAndroidUserApk());
			request.setAttribute("version", appVersion.getAndroidUserVersion());
		} else {
			request.setAttribute("fileName", appVersion.getAndroidShopApk());
			request.setAttribute("version", appVersion.getAndroidShopVersion());
		}

		request.setAttribute("type", type);
		return PathResolver.getPath(AdminPage.APP_UPLOAD_PAGE);
	}

	/**
	 * 上传APK到服务器
	 * 
	 * @param apkFile
	 *            apk文件
	 * @param version
	 *            apk版本号
	 * @param type
	 *            类型: user: 用户版, shop: 商家版
	 * @return
	 */
	@SystemControllerLog(description="上传APK到服务器")
	@RequestMapping(value = "/uploadApk", method = RequestMethod.POST)
	@ResponseBody
	public String enable(HttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile apkFile, @RequestParam String version,
			@RequestParam String type) {
		try {
			return appsService.uploadApk(apkFile, version, type);
		} catch (Exception e) {
			LOGGER.debug("上传APK失败!", e);
			return Constants.FAIL;
		}

	}

	/**
	 * 删除APk
	 * 
	 * @param location
	 * @return
	 */
	@SystemControllerLog(description="删除APk")
	@RequestMapping(value = "/deleteApk", method = RequestMethod.POST)
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, String type) {
		try {
			return appsService.deleteApk(type);
		} catch (Exception e) {
			LOGGER.debug("删除APK失败!", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 更新IOSAppstore
	 * 
	 * @param request
	 * @param response
	 * @param adv
	 * @param l_pic
	 * @return
	 */
	@SystemControllerLog(description="更新IOSAppstore")
	@RequestMapping(value = "/updateIOSAppstore", method = RequestMethod.POST)
	@ResponseBody
	public String updateIOSAppstore(HttpServletRequest request, HttpServletResponse response, String url, @RequestParam String type) {
		try {
			return appsService.updateIOSAppstore(url, type);
		} catch (Exception e) {
			LOGGER.debug("更新IOS的appStore地址失败!", e);
			return Constants.FAIL;
		}
	}

}
