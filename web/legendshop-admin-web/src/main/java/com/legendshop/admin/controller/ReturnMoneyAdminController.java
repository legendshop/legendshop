/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.spi.service.SubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubRefundReturnDetail;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubRefundReturnDetailService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;

/**
 * 退款控制器
 *
 */
@Controller
@RequestMapping("/admin/returnMoney")
public class ReturnMoneyAdminController extends BaseController {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(ReturnMoneyAdminController.class);
	
	/** 待处理 */
	private static final String PENDING = "pending";
	
	/** 所有 */
	private static final String ALL = "all";

	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	
	@Autowired
	private SubRefundReturnDetailService subRefundReturnDetailService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	@Autowired
	private SubService subService;

	/**
	 * 查询退款页面
	 * 
	 * @param curPageNO
	 * @param paramData
	 *            查询参数
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("/query/{status}")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable String status, RefundSearchParamDto paramData) throws ParseException {
		request.setAttribute("status", status);
		request.setAttribute("paramData", paramData);
		return PathResolver.getPath(AdminPage.REFUND_LIST);
	}
	
	/**
	 * 查询退款列表
	 * 
	 * @param curPageNO
	 * @param paramData
	 *            查询参数
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String status, RefundSearchParamDto paramData) throws ParseException {
		if (PENDING.equalsIgnoreCase(status)) {
			request.setAttribute("status", PENDING);
		} else {
			request.setAttribute("status", ALL);
		}
		PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturn(curPageNO, status, paramData);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData", paramData);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.REFUND_CONTENT_LIST);
	}

	/**
	 * 查询退款详情
	 * 
	 * @param refundId 退款单Id
	 * @return
	 */
	@RequestMapping("/refundDetail/{refundId}")
	public String refundDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		if (RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() == subRefundReturn.getApplyState()) {
			request.setAttribute("status", PENDING);
		} else {
			request.setAttribute("status", ALL);
		}
		List<SubRefundReturnDetail> subRefundReturnDetailList = subRefundReturnDetailService.getSubRefundReturnDetailByRefundId(subRefundReturn.getRefundId());
		Sub sub = subService.getSubById(subRefundReturn.getSubId());
		request.setAttribute("returnFreightAmount", 0);
		StringBuffer sb = new StringBuffer();
		sb.append("定");
		sb.append(subRefundReturn.getDepositRefund());
		sb.append(" + 尾");
		sb.append(subRefundReturn.getRefundAmount());
		if(subRefundReturn.getDepositRefund() !=  null && 0 < subRefundReturn.getDepositRefund().doubleValue() && 0 == subRefundReturn.getRefundAmount().doubleValue() )
		{
			sb.append(" (运费:0)" );
			request.setAttribute("returnFreightAmount", 0);
		}
		else
		{
			if (subRefundReturn.getRefundAmount() !=null && subRefundReturn.getRefundAmount().compareTo(BigDecimal.ZERO) == 1){//预售商品，定金尾款情况
				sb.append("  (运费" + sub.getFreightAmount() +")");
				request.setAttribute("returnFreightAmount", sub.getFreightAmount());
			}
		}
		request.setAttribute("info", sb.toString());
		request.setAttribute("subRefundReturnDetailList", subRefundReturnDetailList);
		request.setAttribute("subRefundReturn", subRefundReturn);
		return PathResolver.getPath(AdminPage.REFUND_DETAIL);
	}

	/**
	 * 审核退款
	 * 
	 * @param refundId
	 *            退款退货记录ID
	 * @param adminMessage
	 *            管理员审核备注
	 * @return
	 */
	@RequestMapping("/audit")
	@SystemControllerLog(description="审核退款")
	public String audit(HttpServletRequest request, HttpServletResponse response, Long refundId, String adminMessage) {
		if (null == refundId || AppUtils.isBlank(adminMessage)) {
			throw new NullPointerException("refundId or adminMessage isNull");
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		if (RefundReturnTypeEnum.REFUND.value() != subRefundReturn.getApplyType()
				|| RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() != subRefundReturn.getApplyState()) {
			throw new BusinessException("Illegal Operate");
		}
		if (subRefundReturn.getIsHandleSuccess() == 0) {
			throw new BusinessException("请先处理退款,才能处理审核");
		}
		subRefundReturnService.adminAuditRefund(subRefundReturn, adminMessage);
		

		return PathResolver.getRedirectPath("/admin/returnMoney/query/pending");
	}
	
}
