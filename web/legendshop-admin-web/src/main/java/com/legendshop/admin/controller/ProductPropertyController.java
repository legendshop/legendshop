/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.Product;
import com.legendshop.spi.service.ProductService;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.Sku;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductPropertyService;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;

/**
 * 商品规格属性控制器
 *
 */
@Controller
@RequestMapping("/admin/productProperty")
public class ProductPropertyController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductPropertyController.class);

	@Autowired
	private ProductPropertyService productPropertyService;

	@Autowired
	private ProductPropertyValueService productPropertyValueService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductService productService;

	/**
	 * 后台规格属性 查询，不包括用户自定义的属性和参数
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productProperty
	 * @param isRuleAttributes
	 * @return
	 */
	@RequestMapping(value = "/query/{isRuleAttributes}")
	public String queryProductProperty(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductProperty productProperty,
			@PathVariable Integer isRuleAttributes) {
		PageSupport<ProductProperty> ps = productPropertyService.getProductPropertyPage(curPageNO, isRuleAttributes);
		//循环获取规格值
		List<ProductProperty> propertyList = ps.getResultList();
		if (AppUtils.isNotBlank(propertyList)) {
			List<ProductPropertyValue> valueList = productPropertyValueService.getAllProductPropertyValue(propertyList);
			for (ProductProperty property : propertyList) {
				for (ProductPropertyValue propertyValue : valueList) {
					property.addProductPropertyValueList(propertyValue);
				}
			}
		}
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productProperty", productProperty);
		return PathResolver.getPath(AdminPage.SPECPROPERTY_LIST_PAGE);
	}

	/**
	 * 规格属性 内容
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productProperty
	 * @param isRuleAttributes
	 * @return
	 */
	@RequestMapping(value = "/queryContent/{isRuleAttributes}", method = RequestMethod.POST)
	public String queryPropertyContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductProperty productProperty, @PathVariable Integer isRuleAttributes) {
		PageSupport<ProductProperty> ps = productPropertyService.getProductPropertyPage(curPageNO, isRuleAttributes, productProperty);
		//循环获取规格值
		List<ProductProperty> propertyList = ps.getResultList();
		if (AppUtils.isNotBlank(propertyList)) {
			List<ProductPropertyValue> valueList = productPropertyValueService.getAllProductPropertyValue(propertyList);
			for (ProductProperty property : propertyList) {
				for (ProductPropertyValue propertyValue : valueList) {
					property.addProductPropertyValueList(propertyValue);
				}
			}
		}
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productProperty", productProperty);
		return PathResolver.getPath(BackPage.PROPERTYCONTENT_PAGE);
	}

	/**
	 * 后台保存属性
	 *
	 * @param request
	 * @param response
	 * @param productProperty
	 * @return
	 */
	@SystemControllerLog(description="后台保存属性")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ProductProperty productProperty) {
		SecurityUserDetail user = UserManager.getUser(request);
		productProperty.setUserName(user.getUsername());
		productProperty.setStatus((short) 1);
		productProperty.setIsCustom(0); // 默认是系统自定义的 1:用户自定义，0：系统自带
		prepareProductProperty(productProperty);
		if (log.isDebugEnabled()) {
			log.debug("productProperty = " + ToStringBuilder.reflectionToString(productProperty));
		}
		productPropertyService.saveProductProperty(productProperty,user.getUserId(), user.getShopId());

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PRODUCTPROPERTY_LIST_QUERY) + "/"
				+ productProperty.getIsRuleAttributes();
	}

	/**
	 * 删除商品规格属性
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除商品规格属性")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductProperty productProperty = productPropertyService.getProductProperty(id);
		productPropertyService.deleteProductProperty(productProperty);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.PRODUCTPROPERTY_LIST_QUERY) + "/"
				+ productProperty.getIsRuleAttributes();
	}

	/**
	 * 加载规格属性编辑页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductProperty productProperty = productPropertyService.getProductProperty(id);

		List<ProductPropertyValue> valueList = productPropertyValueService.queryProductPropertyValue(id);

		request.setAttribute("productProperty", productProperty);
		request.setAttribute("valueList", valueList);
		request.setAttribute("isRuleAttributes", productProperty.getIsRuleAttributes());
		return PathResolver.getPath(AdminPage.PRODUCTPROPERTY_EDIT_PAGE);
	}

	/**
	 * 创建新规格属性
	 * @param request
	 * @param response
	 * @param isRuleAttributes
	 * @return
	 */
	@RequestMapping(value = "/new/{isRuleAttributes}")
	public String newRuleAttribute(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Integer isRuleAttributes) {
		request.setAttribute("isRuleAttributes", isRuleAttributes);
		return PathResolver.getPath(AdminPage.PRODUCTPROPERTY_EDIT_PAGE);
	}

	/**
	 * 更新商品规格属性
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductProperty productProperty = productPropertyService.getProductProperty(id);
		productProperty.setStatus((short) 1);
		request.setAttribute("productProperty", productProperty);
		return PathResolver.getPath(BackPage.PRODUCTPROPERTY_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * @param request
	 * @param response
	 * @param propId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新商品规格状态")
	@RequestMapping(value = "/updatestatus/{propId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Short updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long propId, @PathVariable Short status) {
		ProductProperty productProperty = productPropertyService.getProductProperty(propId);
		if (productProperty == null) {
			return -1;
		}
		if (!status.equals(productProperty.getStatus())) {
			productProperty.setStatus(status);
			productPropertyService.updateProductProperty(productProperty);
		}
		return productProperty.getStatus();
	}

	/**
	 * 加载属性编辑页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productProperty
	 * @param isRuleAttributes
	 * @return
	 */
	@RequestMapping(value = "/loadPropPage/{isRuleAttributes}")
	public String loadPropPage(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductProperty productProperty, @PathVariable Integer isRuleAttributes) {
		PageSupport<ProductProperty> ps = productPropertyService.getProductPropertyByPage(curPageNO, isRuleAttributes,
				productProperty);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productProperty", productProperty);
		return PathResolver.getPath(BackPage.LOAD_PROP_LIST_PAGE);
	}

	/**
	 * 创建相似属性
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/similarProp/{id}")
	public String similarProp(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		request.setAttribute("isSimilar", 1);
		return this.load(request, response, id);
	}

	/**
	 * 预备商品属性
	 * @param productProperty
	 */
	private void prepareProductProperty(ProductProperty productProperty) {
		if (productProperty.getIsRuleAttributes() == null) {
			productProperty.setIsRuleAttributes(1);
		}
	}

	/**
	 * 判断是否删除规格
	 * @param propId
	 * @return
	 */
	@RequestMapping(value="/ifDeleteProductProperty")
	@ResponseBody
	public Integer ifDeleteProductProperty(Long propId,Integer isRuleAttributes){
		Integer count=0;
		Integer prodId=0;
		if (isRuleAttributes==1){
			List<Sku> list =  skuService.findSkuByPropId(propId);
			count = list.size();
			prodId =  productPropertyService.findProdId(propId);
			count += prodId;
		}else if (isRuleAttributes==2){
			List<Product> list =  productService.findProdByPropId(propId);
			count = list.size();
		}
		return count;
	}
	
}
