/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.CategoryTree;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.recomm.RecommAdvDto;
import com.legendshop.model.dto.recomm.RecommBrandDto;
import com.legendshop.model.dto.recomm.RecommConDto;
import com.legendshop.model.dto.recomm.RecommDto;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProdTypeService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品分类控制器
 *
 */
@Controller
@RequestMapping("/admin/category")
public class CategoryAdminController extends BaseController{
	
    @Autowired
    private CategoryService categoryService;
    
    @Autowired
    private BrandService brandService;
    
    @Autowired
	 private AttachmentManager attachmentManager;
    
    @Autowired
	private ProdTypeService prodTypeService;
    
    @Autowired
    private ProductService prodService;
    
    @Autowired 
    private CategoryManagerUtil categoryManagerUtil;
    
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
    
    /**
     * 查询商品分类
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Category category) {
    	
        List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.PRODUCT.value());
        List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
        catTrees.add(new CategoryTree(0L,null,"商品分类",true));
        for (Category cat : catList) {
        	if(category.getId() != null && category.getId().equals(cat.getId())){
        		catTrees.add(new CategoryTree(cat.getId(),cat.getParentId(),cat.getName(),true));
        	}else{
        		catTrees.add(new CategoryTree(cat.getId(),cat.getParentId(),cat.getName(),false));
        	}
		}
        request.setAttribute("catTrees", JSONUtil.getJson(openParentCategory(catTrees, String.valueOf(category.getId()))));
        
        PageSupport<Category> ps = categoryService.getCategoryPage(curPageNO);
        PageSupportHelper.savePage(request, ps);
       
        request.setAttribute("category", category);
        
        List<ProdType> typeList =prodTypeService.getProdTypeListWithId(null);
		request.setAttribute("typeList", typeList);
		request.setAttribute("pcDomainName", propertiesUtil.getPcDomainName());
		request.setAttribute("mobileDomainName", propertiesUtil.getVueDomainName());
        return PathResolver.getPath(AdminPage.CATEGORY_LIST_PAGE);
    }
    
    /**
     * 打开特定节点
     */
    private List<CategoryTree> openParentCategory(List<CategoryTree> catTrees, String parentCategoryId){
    	for (CategoryTree categoryTree : catTrees) {
			if(parentCategoryId.equals(categoryTree.getId())){
				categoryTree.setOpen(true);
				String pId = categoryTree.getpId();
				if(!"0".equals(pId)){
					openParentCategory(catTrees, pId);
				}
			}
		}
    	return catTrees;
    }
    
    /**
     * 分类导航管理
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/recomm/query")
    public String recomm(HttpServletRequest request, HttpServletResponse response) {
    	List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.PRODUCT.value());
    	List<Category> topCatList = new ArrayList<Category>();
    	for (Category cat : catList) {
        	if(cat.getGrade().intValue()==1){
        		topCatList.add(cat);
        	}
		}
    	request.setAttribute("topCatList", topCatList);
    	return PathResolver.getPath(AdminPage.CATEGORY_RECOMM_LIST);
    }
    
    /**
     * 还原分类导航设置
     * @param request
     * @param response
     * @param categoryId
     * @return
     */
    @SystemControllerLog(description="还原分类导航设置")
    @RequestMapping("/recomm/initSetting/{categoryId}")
    @ResponseBody
    public String initSetting(HttpServletRequest request, HttpServletResponse response,@PathVariable Long categoryId) {
    	Category category = categoryService.getCategory(categoryId);
    	if(AppUtils.isBlank(category)){
    		return Constants.FAIL;
    	}
    	category.setRecommCon(null);
    	categoryService.updateCategory(category);
        return Constants.SUCCESS;
    }
    
    /**
     * 编辑分类导航
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/recomm/update/{categoryId}")
    public String recomm(HttpServletRequest request, HttpServletResponse response,@PathVariable Long categoryId) {    	
    	Category category = categoryService.getCategory(categoryId);    	
    	List<Brand> brandList = brandService.getAvailableBrands();

		return showCategory(request, response, category, brandList);
    }

	private String showCategory(HttpServletRequest request, HttpServletResponse response, Category category, List<Brand> brandList) {
		String recommCon = category.getRecommCon();
		if(AppUtils.isNotBlank(recommCon)){
            
            //解析json字符串
            RecommConDto recommConDto = JSONUtil.getObject(recommCon, RecommConDto.class);
            
            //获得 推荐的品牌
            if(AppUtils.isNotBlank(recommConDto) && AppUtils.isNotBlank(recommConDto.getBrandList())){
                List<RecommBrandDto> brandDtoList = recommConDto.getBrandList();
                //将推荐的品牌 设为选中
                for (RecommBrandDto recommBrandDto : brandDtoList) {
                    Long brandId = recommBrandDto.getBrandId();
                    for (Brand brand : brandList) {
                        if(brand.getBrandId().equals(brandId)){
                            brand.setRecommedSelect(true);
                            break;
                        }
                    }
                }
            }
            
            //获得推荐的广告
            if(AppUtils.isNotBlank(recommConDto) && AppUtils.isNotBlank(recommConDto.getAdvList())){
                request.setAttribute("advList", recommConDto.getAdvList());
            }
        }

		request.setAttribute("category", category);
		request.setAttribute("brandList", brandList);

		return PathResolver.getPath(AdminPage.CATEGORY_RECOMM_EDIT);
	}

	/**
     * 保存分类导航
     * @param request
     * @param response
     * @return
     */
	@SystemControllerLog(description="保存分类导航")
    @RequestMapping(value = "/recomm/save")
    public String saveRecomm(HttpServletRequest request, HttpServletResponse response,RecommDto recommDto) {//TODO 多事务
		
		SecurityUserDetail user = UserManager.getUser(request);
    	Category category = categoryService.getCategory(recommDto.getCategoryId());
    	String ids[] = recommDto.getBrandIds();		
    	List<Long> brandIds = new ArrayList<Long>();
    	if(ids!=null && ids.length>0){
			if(ids.length>9)
			{
				List<Brand> brandList = brandService.getAvailableBrands();
				request.setAttribute("errorMessage", "分类导航最多显示9个品牌，请设置9个以内的品牌！");
				return showCategory(request, response, category, brandList);
			}
			for (String id : ids) {
    			brandIds.add(Long.valueOf(id));
			}
    	}
    	List<Brand> brandList = brandService.getBrandByIds(brandIds); 
    	List<RecommBrandDto> recommBrandDtos = new ArrayList<RecommBrandDto>();
    	for (Brand brand : brandList) {
    		RecommBrandDto recommBrandDto = new RecommBrandDto();
    		recommBrandDto.setBrandId(brand.getBrandId());
    		recommBrandDto.setBrandName(brand.getBrandName());
    		recommBrandDto.setBrandPic(brand.getBrandPic());
    		recommBrandDtos.add(recommBrandDto);
		}
    	
    	List<RecommAdvDto> recommAdvDtos = new ArrayList<RecommAdvDto>();
    	RecommAdvDto recommAdvDto = new RecommAdvDto();
    	recommAdvDto.setAdvPic(recommDto.getAdvPic());
    	recommAdvDto.setLink(recommDto.getAdvLink());
    	
    	if(recommDto.getAdvPicFile()!=null && recommDto.getAdvPicFile().getSize() > 0){
    		String advPic = attachmentManager.upload(recommDto.getAdvPicFile());
    		recommAdvDto.setAdvPic(advPic);
    		attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), advPic, recommDto.getAdvPicFile(), AttachmentTypeEnum.CATEGORY_RECOMM_ADV);
    		if( AppUtils.isNotBlank(recommDto.getAdvPic())){
				//删除原图片
				attachmentManager.delAttachmentByFilePath(recommDto.getAdvPic());
				attachmentManager.deleteTruely(recommDto.getAdvPic());
			}
    	}
    	recommAdvDtos.add(recommAdvDto);
    	
    	RecommConDto recommConDto = new RecommConDto();
    	recommConDto.setAdvList(recommAdvDtos);
    	recommConDto.setBrandList(recommBrandDtos);
    	
    	category.setRecommCon(JSONUtil.getJson(recommConDto));
    	categoryService.updateCategory(category);
    	
    	saveMessage(request, ResourceBundleHelper.getSucessfulString());
    	return PathResolver.getPath(RedirectPage.CATEGORY_RECOMM_LIST);
    }
    
    /**
     * 保存
     */
    @SystemControllerLog(description="保存修改类目")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Category category) {
    	
    	Long parentCategoryId = null;
    	Category orginCategory = null;
    	String catPic = null;
    	String catIcon = null;
    	SecurityUserDetail user = UserManager.getUser(request);
    	if(category.getId()!=null){
    		orginCategory = categoryService.getCategory(category.getId());
    		if (orginCategory == null) {
				throw new NotFoundException("Origin Category is empty");
			}
    		orginCategory.setCatDesc(category.getCatDesc());
    		//orginCategory.setHeaderMenu(category.getHeaderMenu());
    		orginCategory.setKeyword(category.getKeyword());
    		//orginCategory.setLevel(category.getLevel());
    		orginCategory.setName(category.getName());
    		orginCategory.setNavigationMenu(category.getNavigationMenu());
    		//orginCategory.setParentId(category.getParentId());
    		orginCategory.setRecDate(new Date());
    		orginCategory.setSeq(category.getSeq());
    		orginCategory.setStatus(category.getStatus());
    		orginCategory.setTitle(category.getTitle());
    		orginCategory.setType(ProductTypeEnum.PRODUCT.value());
    		orginCategory.setTypeId(category.getTypeId());
    		orginCategory.setReturnValidPeriod(category.getReturnValidPeriod());
    		parentCategoryId = orginCategory.getParentId();
    		String originCatPic = orginCategory.getPic();
    		if (category.getCatPicFile() != null && category.getCatPicFile().getSize() > 0) {
    			catPic = attachmentManager.upload(category.getCatPicFile());
				//保存附件表
    			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catPic, category.getCatPicFile(), AttachmentTypeEnum.CATEGORY);
    			orginCategory.setPic(catPic);
    			
    			//删除原图片
    			if(AppUtils.isNotBlank(originCatPic)){
    				attachmentManager.delAttachmentByFilePath(originCatPic);
    				attachmentManager.deleteTruely(originCatPic);
    			}
			}
    		
    		String originIcon = orginCategory.getIcon();
    		if (category.getCatIconFile() != null && category.getCatIconFile().getSize() > 0) {
    			catIcon = attachmentManager.upload(category.getCatIconFile());
				//保存附件表
    			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catIcon, category.getCatIconFile(), AttachmentTypeEnum.CATEGORY);
    			orginCategory.setIcon(catIcon);
    			//删除原图片
    			if(AppUtils.isNotBlank(originIcon)){
    				attachmentManager.delAttachmentByFilePath(originIcon);
    				attachmentManager.deleteTruely(originIcon);
    			}
			}
    		
    		categoryService.updateCategory(orginCategory);
    	}else{
    		if (category.getCatPicFile() != null && category.getCatPicFile().getSize() > 0) {
    			catPic = attachmentManager.upload(category.getCatPicFile());
				//保存附件表
    			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catPic, category.getCatPicFile(), AttachmentTypeEnum.CATEGORY);
    			category.setPic(catPic);
			}
    		if (category.getCatIconFile() != null && category.getCatIconFile().getSize() > 0) {
    			catIcon = attachmentManager.upload(category.getCatIconFile());
				//保存附件表
    			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catIcon, category.getCatIconFile(), AttachmentTypeEnum.CATEGORY);
    			category.setIcon(catIcon);
			}
    		category.setType(ProductTypeEnum.PRODUCT.value());
    		category.setRecDate(new Date());
    		category.setReturnValidPeriod(category.getReturnValidPeriod());
    		category.setHeaderMenu(false); //头部菜单已隐藏，默认为0
    		if(category.getParentId()==null || category.getParentId()==0){
    			category.setParentId(0l);
    			category.setGrade(1);
    		}else{
    			Category catParent = categoryService.getCategory(category.getParentId());
    			category.setGrade(catParent.getGrade()+1);
    		}
    		parentCategoryId = category.getParentId();
    		categoryService.saveCategory(category);
    	}
    	//跟新类目缓存
    	//categoryService.cleanCategoryCache();
		//清理手机端的 分类树缓存
		//cleanMobileCategoryCache();
		//清理APP端的分类树缓存
		//cleanAppCategoryCache();
    	return  PathResolver.getPath(RedirectPage.CATEGORY_LIST_QUERY)+ "?id=" + parentCategoryId;
    }

    /**
     * id 为分类Id
     */
    @SystemControllerLog(description="删除类目")
    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Category category = categoryService.getCategory(id);
        List<Product> lists=this.prodService.getProdByCateId(id);
        if(AppUtils.isBlank(lists)){
        	categoryService.deleteCategory(category);
        	return "ok";
        }
        return "error";
    }
    
    /**
     * 加载商品类目
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Category load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Category category = categoryService.getCategory(id);
        if(category.getParentId()!=null && category.getParentId()!=0){
        	Category parentCat = categoryService.getCategory(category.getParentId());
        	category.setParentName(parentCat.getName());
        }else{
        	category.setParentName("顶级类目");
        }
        
        if(category.getTypeId()!=null){
        	ProdType prodType = prodTypeService.getProdType(category.getTypeId().longValue());
        	if(prodType!=null){
        		category.setTypeName(prodType.getName());
        	}
        	
        }
        return category;
    }
    
    
	@RequestMapping(value = "/new")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.CATEGORY_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        Category category = categoryService.getCategory(id);
		request.setAttribute("category", category);
		return "forward:/admin/category/query.htm";
    }
    
    @RequestMapping(value = "/loadCategory")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response) {
    	List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.PRODUCT.value());
        List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
        catTrees.add(new CategoryTree(0L,null,"顶级类目",true));
        for (Category cat : catList) {
        	catTrees.add(new CategoryTree(cat.getId(),cat.getParentId(),cat.getName()));
		}
        request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
    	return PathResolver.getPath(BackPage.CATEGORY_LOADCATEGORY_PAGE);
    }
    
    @RequestMapping(value = "/loadCategoryCheckbox/{typeId}", method = RequestMethod.GET)
	public String loadCategoryCheckbox(HttpServletRequest request, HttpServletResponse response,@PathVariable Long typeId) {
    	ProdType prodType = prodTypeService.getProdType(typeId);
    	List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.PRODUCT.value());
        List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
        for (Category cat : catList) {
        	CategoryTree categoryTree = new CategoryTree(cat.getId(),cat.getParentId(),cat.getName());
        	if(cat.getTypeId()!=null && cat.getTypeId()==typeId.intValue()){
        		categoryTree.setChecked(true);
        	}
        	catTrees.add(categoryTree);
		}
        request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
        request.setAttribute("typeName", prodType.getName());
        request.setAttribute("typeId", typeId);
    	return PathResolver.getPath(BackPage.CATEGORY_LOADCATEGORY_CHECKBOX_PAGE);
    }
    
    
	/**
	 * 查询产品类型
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getProdType")
	public String getProdType(HttpServletRequest request, HttpServletResponse response, String typeName){
        PageSupport<ProdType> ps = prodTypeService.getProdTypePage(typeName);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(BackPage.CATEGORY_PROD_TYPE_PAGE);
	}
	
	/**
	 * 保存类目类型
	 * @param request
	 * @param response
	 * @param catIdStr
	 * @param typeId
	 * @return
	 */
	@RequestMapping(value = "/saveCategoryType")
	@ResponseBody
	public String saveCategoryType(HttpServletRequest request, HttpServletResponse response, String catIdStr, Integer typeId){
		List<Category> oldCats = categoryService.getCategorysByTypeId(typeId);
		for (Category category : oldCats) {
			TreeNode node  = categoryManagerUtil.getTreeNodeById(category.getId());
			if(node!=null){
				node.getObj().setTypeId(null);
			}
			category.setTypeId(null);
		}
		categoryService.updateCategoryList(oldCats);
		
		if(catIdStr!=""){
			String[] catIds = catIdStr.split(",");
			List<Long> catIdList = new ArrayList<Long>();
			for (String catId : catIds) {
				catIdList.add(Long.valueOf(catId));
			}
			List<Category> categorys = categoryService.getCategoryByIds(catIdList);
			for (Category category : categorys) {
				TreeNode node  = categoryManagerUtil.getTreeNodeByIdForAll(category.getId());
				if(node!=null){
					node.getObj().setTypeId(typeId);
				}
				category.setTypeId(typeId);
			}
			categoryService.updateCategoryList(categorys);
		}
        return Constants.SUCCESS;
	}
	
	/**
	 * 清除分类树的缓存
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/cleanCategoryCache", method = RequestMethod.POST)
	@ResponseBody
	public String cleanCategoryCache(HttpServletRequest request,HttpServletResponse response) {
		categoryService.cleanCategoryCache();
		return Constants.SUCCESS;
	}

	/**
	 * 判断是否删除商品类型
	 * @param prodTypeId
	 * @return
	 */
	@RequestMapping(value="/ifDeleteProdType")
	@ResponseBody
	public boolean ifDeleteProdType(Integer prodTypeId){
		List<Category> list = categoryService.findProdTypeId(prodTypeId);
		if(AppUtils.isNotBlank(list)){
			return true;
		}
		return false;
	}
	
	/**
	 * 判断创建的类目等级是否大于3级
	 * @param parentId
	 * @return
	 */
	@RequestMapping(value="/ifCreate")
	@ResponseBody
	public String ifCreate(Long parentId){
		Category category = categoryService.getCategory(parentId);
		if(AppUtils.isNotBlank(category)){
			if(category.getGrade()>=3){
				return "新创建的类目等级不能大于3级";
			}else{
				return Constants.SUCCESS;
			}
		}
		return Constants.SUCCESS;
	}

	@RequestMapping(value = "/deleteImg/{catId}")
	@ResponseBody
	public String deleteImg(HttpServletRequest request, HttpServletResponse response, @PathVariable
			Long catId) {
		Category category = categoryService.getCategory(catId);
		if(AppUtils.isBlank(category)){
			return "该类目不存在！";
		}
		category.setIcon(null);
//        category.setPic(null);
		categoryService.updateCategory(category);
		return "OK";
	}


}
