/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Navigation;
import com.legendshop.model.entity.NavigationItem;
import com.legendshop.spi.service.NavigationItemService;
import com.legendshop.spi.service.NavigationService;
import com.legendshop.util.AppUtils;


/**
 * 导航管理
 *
 */
@Controller
@RequestMapping("/admin/system/navigation")
public class NavigationController extends BaseController {
    
	@Autowired
    private NavigationService navigationService;
    
	@Autowired
    private NavigationItemService navigationItemService;

	/**
	 * 查询导航管理
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Navigation navigation) {
       
        PageSupport<Navigation> ps = navigationService.getNavigationPage(curPageNO);
		@SuppressWarnings("unchecked")
		List<Navigation> list = (List<Navigation>) ps.getResultList();// 1级导航
		if(AppUtils.isNotBlank(list)){
			List<NavigationItem> navigationItemList = navigationItemService.getAllNavigationItem(); // 2级导航
			//将2级导航加入1级导航中
			for (Navigation navi : list) {
				for (NavigationItem navigationItem : navigationItemList) {
					navi.addSubItems(navigationItem);
				}
			}
		}
		
		PageSupportHelper.savePage(request, ps);
        request.setAttribute("navigation", navigation);
        
        //String path = PathResolver.getPath(request, response, BackPage.NAVIGATION_LIST_PAGE);
        String path = PathResolver.getPath(AdminPage.NAVIGATION_LIST_PAGE);
        return path;
    }
    
    /**
     * 保存
     */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Navigation navigation) {
        navigationService.saveNavigation(navigation);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.NAVIGATION_LIST_QUERY);
    }
    
    /**
     * 删除
     */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Navigation navigation = navigationService.getNavigation(id);
		navigationService.deleteNavigation(navigation);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(RedirectPage.NAVIGATION_LIST_QUERY);
    }
    
    /**
     * 根据id加载导航管理编辑页面
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Navigation navigation = navigationService.getNavigation(id);
        request.setAttribute("navigation", navigation);
         //return PathResolver.getPath(request, response, BackPage.NAVIGATION_EDIT_PAGE);
        return PathResolver.getPath(AdminPage.NAVIGATION_EDIT_PAGE);
    }

    /**
     * 加载导航管理编辑页面
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(AdminPage.NAVIGATION_EDIT_PAGE);
	}
	
	/**
	 * 更新
	 */
    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        Navigation navigation = navigationService.getNavigation(id);
		request.setAttribute("navigation", navigation);
		return PathResolver.getPath(AdminPage.NAVIGATION_EDIT_PAGE);
    }
    
    /**
     * 更新状态
     * @param request
     * @param response
     * @param naviId
     * @param status
     * @return
     */
    @RequestMapping(value = "/updatestatus/{naviId}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long naviId,
			@PathVariable Integer status) {
    	Navigation navigation = navigationService.getNavigation(naviId);
		if (navigation == null) {
			return -1;
		}
		if (!status.equals(navigation.getStatus())) {
//			if (!FoundationUtil.haveViewAllDataFunction(request)) {
//				String loginName = UserManager.getUserName(request.getSession());
//				// user
//				if (!loginName.equals(navigation.getName())) {
//					return -1;
//				}
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)) {
//					navigation.setStatus(status);
//					navigationService.updateNavigation(navigation);
//				}
//			} else {
//				// admin
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
//					navigation.setStatus(status);
//					navigationService.updateNavigation(navigation);
//				}
//			}
			
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				navigation.setStatus(status);
				navigationService.updateNavigation(navigation);
			}
		}
		return navigation.getStatus();
	}
    
    /**
     * 添加二级导航
     */
    @RequestMapping("/querysearth/{id}")
    public String querysearth(HttpServletRequest request, HttpServletResponse response, @PathVariable
    	    Long id) {
    	Navigation navigation = navigationService.getNavigation(id);
    	 
       request.setAttribute("navigation", navigation);
       NavigationItem item=new NavigationItem();
       item.setNaviId(navigation.getNaviId());
       request.setAttribute("navigationItem", item);

       //return PathResolver.getPath(request, response, BackPage.NAVIGATIONITEM_EDIT_PAGE);
       return PathResolver.getPath(AdminPage.NAVIGATIONITEM_EDIT_PAGE);
    }
    
    
}
