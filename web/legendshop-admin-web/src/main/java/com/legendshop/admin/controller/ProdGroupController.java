/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.spi.service.ProdGroupRelevanceService;
import com.legendshop.spi.service.ProdGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
/**
 * 商品分组控制器
 */

@Controller
@RequestMapping("/admin/prodGroup")
public class ProdGroupController extends BaseController {

	@Autowired
	private ProdGroupService prodGroupService;

	@Autowired
	private ProdGroupRelevanceService prodGroupRelevanceService;

	@Autowired
	ProductService productService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 列表查询
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProdGroup prodGroup) {
		request.setAttribute("prodGroup", prodGroup);
		return PathResolver.getPath(AdminPage.PROD_GROUP_LIST_PAGE);
	}

	/**
	 * 查询分组
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProdGroup prodGroup) {
		PageSupport<ProdGroup> ps = prodGroupService.queryProdGroupListPage(curPageNO, prodGroup, 20);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodGroup", prodGroup);
		String path = PathResolver.getPath(AdminPage.PROD_CONTENT_GROUP_LIST_PAGE);
		return path;
	}

	/**
	 * 保存
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, ProdGroup prodGroup) {

      if(AppUtils.isNotBlank(prodGroup.getId())){
		ProdGroup entity = prodGroupService.getProdGroup(prodGroup.getId());
		if (AppUtils.isNotBlank(entity)) {
			entity.setSort(prodGroup.getSort());
			entity.setName(prodGroup.getName());
			entity.setDescription(prodGroup.getDescription());
			entity.setRecDate(new Date());
			prodGroupService.updateProdGroup(entity);
		}
		}else{//save
			prodGroup.setType(1);
			prodGroup.setRecDate(new Date());
			prodGroupService.saveProdGroup(prodGroup);
		}

        return "redirect:/admin/prodGroup/query";
    }

	/**
	 * 删除
	 */
	@RequestMapping(value = "/delete/{id}")
	@ResponseBody
	public boolean delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		boolean rs = false;
		ProdGroup prodGroup = prodGroupService.getProdGroup(id);
		if (prodGroup != null) {
			rs = prodGroupService.deleteProdGroup(prodGroup) > 0;
			prodGroupRelevanceService.deleteProdGroupRelevanceByprod(prodGroup.getId());
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
		return rs;
	}

	/**
	 * 根据Id加载
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProdGroup prodGroup = prodGroupService.getProdGroup(id);
		request.setAttribute("prodGroup", prodGroup);
		return PathResolver.getPath(AdminPage.PROD_GROUP_EDIT_PAGE);
	}

	/**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PROD_GROUP_EDIT_PAGE);
	}

	/**
	 * 更新分组
	 */
	@RequestMapping(value = "/update/{id}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProdGroup prodGroup = prodGroupService.getProdGroup(id);
		request.setAttribute("bean", prodGroup);
		return PathResolver.getPath(AdminPage.PROD_GROUP_EDIT_PAGE);
	}

	/**
	 * 查看分组内商品列表
	 */
	@RequestMapping("/queryProdList")
	public String queryProdList(HttpServletRequest request, HttpServletResponse response, String curPageNO,Product product) {
		request.setAttribute("prod", product);
		return PathResolver.getPath(AdminPage.GROUP_PRODUCT_LIST_PAGE);
	}

	/**
	 * 查看分组内商品内容
	 */
	@RequestMapping(value = "/queryProdContentList")
	public String queryProdContentList(HttpServletRequest request, HttpServletResponse response,String curPageNO,Product product) {

		Integer pageSize = null;

		// 非导出情况
		if (!CommonServiceWebUtil.isDataForExport(request)) {
			pageSize = systemParameterUtil.getPageSize();
		}

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "siteName", "views", "buys", "stocks" });

		PageSupport<Product> ps = productService.getProductListPage(product, curPageNO, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(AdminPage.GROUP_PRODUCT_CONTENT_LIST_PAGE);
	}

	/**
	 * 移除分组内的商品
	 */
	@RequestMapping(value = "/removeProd")
	@ResponseBody
	public String removeProd(HttpServletRequest request, HttpServletResponse response,Long prodId,Long groupId) {

		ProdGroup prodGroup = prodGroupService.getProdGroup(groupId);

		if (AppUtils.isBlank(prodGroup)){
			return "分组不存在或已被删除";
		}

		prodGroupRelevanceService.removeProd(prodId,groupId);
		return Constants.SUCCESS;
	}

	/**
	 * 检查分组名是否存在
	 */
	@RequestMapping(value = "/checkBrandByName")
	public @ResponseBody boolean checkBrandByName(String name, Long id) {
		if (AppUtils.isNotBlank(name)) {
			return prodGroupService.checkNameIsExist(name, id);
		}
		return false;
	}

}
