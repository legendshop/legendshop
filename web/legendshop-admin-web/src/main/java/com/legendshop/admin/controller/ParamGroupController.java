/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ParamGroup;
import com.legendshop.spi.service.ParamGroupService;

/**
 * 参数组列表
 *
 */
@Controller
@RequestMapping("/admin/paramGroup")
public class ParamGroupController extends BaseController{

	@Autowired
	private ParamGroupService paramGroupService;

	/**
	 * 查找参数组列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ParamGroup paramGroup) {
		PageSupport<ParamGroup> ps = paramGroupService.getParamGroupPage(curPageNO, paramGroup);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramGroup", paramGroup);
		return PathResolver.getPath(AdminPage.PARAMGROUP_LIST_PAGE);
	}
	
	/**
	 * 保存参数组列表
	 */
	@SystemControllerLog(description="保存参数组列表")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ParamGroup paramGroup) {
		paramGroupService.saveParamGroup(paramGroup);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PARAMGROUP_LIST_QUERY);
	}

	/**
	 * 删除参数组列表
	 */
	@SystemControllerLog(description="删除参数组列表")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ParamGroup paramGroup = paramGroupService.getParamGroup(id);
		paramGroupService.deleteParamGroup(paramGroup);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.PARAMGROUP_LIST_QUERY);
	}
	
	/**
	 * 跳转编辑页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ParamGroup paramGroup = paramGroupService.getParamGroup(id);
		request.setAttribute("paramGroup", paramGroup);
		 return PathResolver.getPath(AdminPage.PARAMGROUP_EDIT_PAGE);
	}
	
	/**
	 * 跳转新建页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PARAMGROUP_EDIT_PAGE);
	}

	/**
	 * 更新参数组列表
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ParamGroup paramGroup = paramGroupService.getParamGroup(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// paramGroup.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("paramGroup", paramGroup);
		return "forward:/admin/paramGroup/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.PARAMGROUP_EDIT_PAGE);
	}

}
