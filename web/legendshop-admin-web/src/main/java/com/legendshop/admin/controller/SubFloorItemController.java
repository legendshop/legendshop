/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.model.floor.ProductFloor;
import com.legendshop.spi.service.FloorItemService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SubFloorItemService;
import com.legendshop.spi.service.SubFloorService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 楼层目录控制器
 *
 */
@Controller
@RequestMapping("/admin/subFloorItem")
public class SubFloorItemController extends BaseController {
	
	@Autowired
	private SubFloorItemService subFloorItemService;

	@Autowired
	private SubFloorService subFloorService;

	@Autowired
	private ProductService productService;

	@Autowired
	private FloorItemService floorItemService;

	/**
	 * 查看子楼层商品详情
	 * @param request
	 * @param response
	 * @param sfId
	 * @return
	 */
	@RequestMapping(value = "/sendFloorProduct/{sfId}")
	public String loadSendFloorProduct(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long sfId) {
		// 产品列表
		List<SubFloorItem> subItemLimit = subFloorItemService.getItemLimit(sfId);
		/*
		 * SubFloor subFloor = subFloorService.getSubFloor(sfId);
		 * if(AppUtils.isNotBlank(subFloor)){ Integer leftSwitch =
		 * floorService.getLeftSwitch(subFloor.getFloorId());
		 * request.setAttribute("leftSwitch",leftSwitch); }
		 */
		request.setAttribute("subItemLimit", subItemLimit);
		request.setAttribute("sfId", sfId);
		return PathResolver.getPath(BackPage.PRODUCT_FLOOR_LIMIT);
	}

	/**
	 * 覆盖子楼层商品
	 * @param request
	 * @param response
	 * @param sfId
	 * @param limit
	 * @param layout
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/productFloorOverlay/{sfId}")
	public String loadProductFloorOverlay(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long sfId, String limit, String layout, Long flId) {
		if (FloorLayoutEnum.WIDE_BLEND.value().equals(layout)) {
			SubFloor subFloor = subFloorService.getSubFloor(sfId);
			request.setAttribute("subFloorName", subFloor.getName());
			List<SubFloorItem> subFloorItemList = subFloorItemService.getAllSubFloorItem(sfId);
			request.setAttribute("subFloorItemList", subFloorItemList);
		} else {
			List<SubFloorItem> subFloorItemList = floorItemService.findSubFloorItem(flId,
					FloorLayoutEnum.WIDE_GOODS.value(), 5);
			request.setAttribute("subFloorItemList", subFloorItemList);
		}
		request.setAttribute("sfId", sfId);
		request.setAttribute("limit", limit);
		request.setAttribute("layout", layout);
		request.setAttribute("flId", flId);
		return PathResolver.getPath(BackPage.PRODUCT_FLOOR_OVERLAY);
	}

	/**
	 * 商品楼层加载
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "/productFloorLoad")
	public String productFloorLoad(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		PageSupport<Product> ps = productService.getProductListPage(curPageNO, product);
		List<Product> prodList = (List<Product>) ps.getResultList();

		String newName = null;
		for (Product prod : prodList) {
			newName = prod.getName().replaceAll("'", "");// 去掉双引号和单引号
			newName = newName.replace("\"", "");
			prod.setName(newName);
		}
		ps.setResultList(prodList);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.PRODUCT_FLOOR_LOAD);
	}

	/**
	 * 保存子楼层商品[混合版式的商品]
	 * 
	 * @param request
	 * @param response
	 * @param productFloors
	 * @param sfId
	 * @return
	 */
	@RequestMapping(value = "/saveProductFloor")
	public @ResponseBody String saveProductFloor(HttpServletRequest request, HttpServletResponse response,
			String productFloors, Long sfId, Long fId, String layout) {
		if (AppUtils.isBlank(productFloors)) {
			return "请选择商品信息";
		}
		if (AppUtils.isBlank(fId) || AppUtils.isBlank(layout)) {
			return "参数有误！";
		}
		List<ProductFloor> productFloorList = JSONUtil.getArray(productFloors, ProductFloor.class);
		if (AppUtils.isBlank(productFloorList)) {
			return "请选择商品信息";
		}
		if (FloorLayoutEnum.WIDE_BLEND.value().equals(layout)) {
			subFloorItemService.saveSubFloorItem(productFloorList, sfId, fId); // 混合模式
																				// 需要保存
																				// ls_sub_floor、ls_sub_floor_item.
		} else {
			subFloorItemService.saveWideGoodsFloor(productFloorList, fId, layout);
		}
		return Constants.SUCCESS;
	}

}
