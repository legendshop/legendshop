/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.model.entity.DiscoverProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DiscoverArticleService;
import com.legendshop.spi.service.DiscoverProdService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.uploader.AttachmentManager;

import io.protostuff.Response;

/**
 * 发现文章表控制器
 */
@Controller
@RequestMapping("/admin/discoverArticle")
public class DiscoverArticleController extends BaseController{

	/** 日志. */
	private final Logger log = LoggerFactory.getLogger(DiscoverArticle.class);
	
    @Autowired
    private DiscoverArticleService discoverArticleService;
    
    @Autowired
    AttachmentManager attachmentManager;
    @Autowired
    private ProductService productService;
    
    @Autowired
    private DiscoverProdService discoverProdService;
	/**
	 * 发现文章表列表查询
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,DiscoverArticle discoverArticle) {
    	PageSupport<DiscoverArticle> ps = discoverArticleService.queryDiscoverArticlePage(curPageNO, discoverArticle, 20);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("discoverArticle", discoverArticle);
		return PathResolver.getPath(AdminPage.DISCOVER_ARTICLE_LIST_PAGE);
    }

	/**
	 * 保存发现文章表
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, DiscoverArticle discoverArticle,Long[] prods) {
    	// 上传图片
    	String pic = null;
    	if (discoverArticle.getPicFile() != null && discoverArticle.getPicFile().getSize() > 0) {
    		pic = attachmentManager.upload(discoverArticle.getPicFile());
    	}
    	if(discoverArticle.getId() != null){//update
		DiscoverArticle entity = discoverArticleService.getDiscoverArticle(discoverArticle.getId());
		if (entity != null) {
				discoverProdService.deleteByDisId(entity.getId());
				entity.setWriterName(discoverArticle.getWriterName());
				entity.setName(discoverArticle.getName());
				entity.setIntro(discoverArticle.getIntro());
				entity.setContent(discoverArticle.getContent());
				entity.setStatus(discoverArticle.getStatus());
				entity.setPublishTime(new Date());
				if (AppUtils.isNotBlank(pic)){
					entity.setImage(pic);
				}

				discoverArticleService.updateDiscoverArticle(entity);
			}
		} else {// save
			discoverArticle.setImage(pic);
			discoverArticle.setCreateTime(new Date());
			discoverArticle.setPublishTime(new Date());
			discoverArticle.setPageView(0l);
			discoverArticle.setThumbNum(0l);
			discoverArticle.setCommentsNum(0l);
			discoverArticleService.saveDiscoverArticle(discoverArticle);
		}
    	
    	if (prods!=null) {
    		for (Long pid : prods) {
    				DiscoverProd discoverProd=new DiscoverProd();
        			discoverProd.setDisId(discoverArticle.getId());
        			discoverProd.setProdId(pid);
        			discoverProdService.saveDiscoverProd(discoverProd);
				}
    		}
        return "redirect:/admin/discoverArticle/query";
    }

	/**
	 * 删除发现文章表
	 */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
    	
    	DiscoverArticle discoverArticle = discoverArticleService.getDiscoverArticle(id);
		if(discoverArticle != null){
			log.info("{} delete discoverArticle by Id {}", discoverArticle.getId());
			discoverArticleService.deleteDiscoverArticle(discoverArticle);
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
        return "redirect:/admin/discoverArticle/query";
    }
    
    /**
	 *修改状态发现文章表
	 */
    @ResponseBody
    @RequestMapping(value = "/updatestatus/")
    public boolean updatestatus(HttpServletRequest request, HttpServletResponse response,Long itemId,Long status) {
    	DiscoverArticle discoverArticle = discoverArticleService.getDiscoverArticle(itemId);
		try {
			if(discoverArticle != null){
				discoverArticle.setStatus(status);
				discoverArticleService.updateDiscoverArticle(discoverArticle);
			}
		} catch (Exception e) {
			return false;
		}
        return true;
    }

	/**
	 * 根据Id加载发现文章表
	 */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        DiscoverArticle discoverArticle = discoverArticleService.getDiscoverArticle(id);
        List<DiscoverProd> list=discoverProdService.getByDisId(discoverArticle.getId());
		List<Product>products=new ArrayList<>();
		if (list!=null) {
			for (DiscoverProd discoverProd : list) {
				Product product=productService.getProductById(discoverProd.getProdId());
				products.add(product);
			}
		}
		request.setAttribute("products", products);
        request.setAttribute("discoverArticle", discoverArticle);
        return PathResolver.getPath(AdminPage.DISCOVER_ARTICLE_PAGE);
    }
    
   /**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(AdminPage.DISCOVER_ARTICLE_PAGE);
	}
	
	 /**
	 * 加载商品页面
	 */
	@RequestMapping(value = "/discoverArticleProd")
	public String discoverArticleProd(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(AdminPage.DISCOVER_ARTICLE_PROD_PAGE);
	}
	/*页面的商品*/
	@RequestMapping(value = "/fullProd")
	public String fullProd(HttpServletRequest request, HttpServletResponse response,Long activeId,String curPageNo,String prodName) {
		//查看商品
		PageSupport<Product> ps = productService.getProdductByName(curPageNo, prodName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(AdminPage.DISCOVER_ADDFULLPROD_PAGE);
	}
	
	/*页面的商品*/
	@RequestMapping(value = "/findByprodId")
	public String findByprodId(HttpServletRequest request, HttpServletResponse response,String ids) {
		List<Product>products=new ArrayList<>();
		String []pids=ids.split(",");
		for (String pid : pids) {
			Product product=productService.getProductById(Long.parseLong(pid));
			products.add(product);
		}
		request.setAttribute("products", products);
		return PathResolver.getPath(AdminPage.PRODTABLE_PAGE);
	}
}
