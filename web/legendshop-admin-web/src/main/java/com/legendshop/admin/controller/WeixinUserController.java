/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinRedirectPage;
import com.legendshop.base.util.WxConfig;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.dto.weixin.WeiXinGroups;
import com.legendshop.model.entity.weixin.WeixinGroup;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.spi.service.WeixinGroupService;
import com.legendshop.spi.service.WeixinGzuserInfoService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信用户管理信息控制层
 * 
 * @author liyuan
 *
 */
@Controller
@RequestMapping("/admin/weixinUser")
public class WeixinUserController extends BaseController {

	@Autowired
	private WeixinGzuserInfoService weixinGzuserInfoService;

	@Autowired
	private WeixinGroupService weixinGroupService;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	// private WeixinGroup
	public String save(HttpServletRequest request, HttpServletResponse response, WeixinGzuserInfo entity) {
		return null;
	}

	// 根据ID删除用户信息
	@RequestMapping(value = "delete/{id}/{groupId}", method = RequestMethod.GET)
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long groupId) {// TODO 多事务
		// id为0删除分组，判断分组里面是否有用户
		if (id == null || id == 0) {
			WeixinGroup weinxinGroup = weixinGroupService.getWeixinGroup(groupId);
			/*
			 * 先去删除微信分组信息 2、再同步本地数据分组信息
			 */
			if (AppUtils.isNotBlank(weinxinGroup)) {

				String url = WxConfig.deleteGroup;
				Map<String, String> paramMap = new HashMap<String, String>();
				paramMap.put("groupId", String.valueOf(weinxinGroup.getId()));
				String token = RandomStringUtils.randomNumeric(4);
				paramMap.put("token", token);
				paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));

				AjaxJson msg = null;
				String result = HttpUtil.httpPost(url, paramMap);
				if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
					msg = JSONUtil.getObject(result, AjaxJson.class);
				}
				if (msg != null && msg.isSuccess()) {
					// 查询该分组下的所有用户
					List<WeixinGzuserInfo> weixinUserList = weixinGzuserInfoService.getUserByGroupId(groupId);
					if (weixinUserList.size() > 0) {
						for (int i = 0; i < weixinUserList.size(); i++) {
							WeixinGzuserInfo weixinUser = weixinUserList.get(i);
							System.out.println(weixinUser.getNickname());
							weixinUser.setGroupid("0");
							weixinGzuserInfoService.updateWeixinGzuserInfo((weixinUser));
						}
						System.out.println(msg);
					}
					weixinGroupService.deleteWeixinGroup(weinxinGroup);
				} else {
					System.out.println("chucuo");
				}
			}
		} else {
			WeixinGzuserInfo weixinUser = weixinGzuserInfoService.getWeixinGzuserInfo(id);
			weixinGzuserInfoService.deleteWeixinGzuserInfo(weixinUser);
		}
		String path = PathResolver.getPath(WeixinRedirectPage.WEIXINGZUSER_LIST_PAGE);
		return path;
	}

	// 根据ID查询对应的微信用户信息
	@RequestMapping(value = "/load/{userid}", method = RequestMethod.GET)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long userid) {
		WeixinGzuserInfo weixinuser = weixinGzuserInfoService.getWeixinGzuserInfo(userid);
		request.setAttribute("weixinUserInfo", weixinuser);
		String path = PathResolver.getPath(WeixinAdminPage.WEIXINGZUSER_DETAIL_PAGE);
		return path;
	}

	// 根据相应ID用户的信息设置相应的分组信息
	@RequestMapping(value = "/update/{id}/{GroupId}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long GroupId) {
		WeixinGzuserInfo weixinUser = weixinGzuserInfoService.getWeixinGzuserInfo(id);
		weixinUser.setGroupid(GroupId.toString());
		// TODO 微信分组
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("openId", weixinUser.getOpenid());
		paramMap.put("groupId", String.valueOf(GroupId));
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result = HttpUtil.httpPost(WxConfig.updateUserGroup, paramMap);
		if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
			AjaxJson ajaxJson = JSONUtil.getObject(result, AjaxJson.class);
			if (ajaxJson.isSuccess()) {
				weixinGzuserInfoService.updateWeixinGzuserInfo(weixinUser);
				String path = PathResolver.getPath(WeixinRedirectPage.WEIXINGZUSER_LIST_PAGE);
				return path;
			} else {
				return ajaxJson.getMsg();
			}
		}
		return "更新失败";
	}

	// 分页查询用户的所有信息
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinGzuserInfo weixinGzuserInfo) {
		String key = weixinGzuserInfo.getNickname();
		PageSupport<WeixinGzuserInfo> ps = weixinGzuserInfoService.query(curPageNO, key);
		PageSupportHelper.savePage(request, ps);
		// 查询分组信息
		List<WeixinGroup> weixinGroup = weixinGroupService.getWeixinGroup();
		request.setAttribute("weixinGroupList", weixinGroup);
		request.setAttribute("key", key);
		return PathResolver.getPath(WeixinAdminPage.WEIXINGZUSER_LIST_PAGE);
	}

	// 根据相应ID用户的信息设置相应的分组信息
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request, HttpServletResponse response, Long id, String bzname) {
		WeixinGzuserInfo weixinUser = weixinGzuserInfoService.getWeixinGzuserInfo(id);
		if (AppUtils.isBlank(weixinUser)) {
			return Constants.FAIL;
		}
		weixinUser.setBzname(bzname);
		// TODO 更新用户的备注
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("openId", weixinUser.getOpenid());
		paramMap.put("remark", bzname);
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result = HttpUtil.httpPost(WxConfig.updateRemark, paramMap);
		if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
			AjaxJson ajaxJson = JSONUtil.getObject(result, AjaxJson.class);
			if (ajaxJson.isSuccess()) {
				weixinGzuserInfoService.updateWeixinGzuserInfo(weixinUser);
				return Constants.SUCCESS;
			} else {
				return ajaxJson.getMsg();
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 微信同步用户
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/synchronousUser", method = RequestMethod.GET)
	public @ResponseBody String synchronousUser(HttpServletRequest request, HttpServletResponse response, Long id) {
		// 先查询数据库里面的所有用户
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("nextOpenId", "");
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));

		String result = HttpUtil.httpPost(WxConfig.SYNCHRONOUS_USERINFO, paramMap);
		if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
			AjaxJson ajaxJson = JSONUtil.getObject(result, AjaxJson.class);
			if (ajaxJson.isSuccess()) {
				return ajaxJson.getMsg();
			} else {
				return ajaxJson.getMsg();
			}
		}
		return "微信同步操作正在异步执行中........,请稍等片刻!";
	}

	/**
	 * 微信同步分组
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/synchronousGroup", method = RequestMethod.GET)
	public @ResponseBody String synchronousGroup(HttpServletRequest request, HttpServletResponse response, Long id) {
		List<WeixinGroup> weinxinGroupList = weixinGroupService.getWeixinGroup();
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("nextOpenId", "");
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret", MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result = HttpUtil.httpPost(WxConfig.SYNCHRONOUSGROUP, paramMap);
		if (AppUtils.isNotBlank(result) && !"null".equals(result)) {
			AjaxJson json = JSONUtil.getObject(result, AjaxJson.class);
			List<WeiXinGroups> groups = JSONUtil.getArray(json.getSuccessValue(), WeiXinGroups.class);
			if (AppUtils.isNotBlank(groups)) {
				weixinGroupService.synchronousGroup(groups, weinxinGroupList, null);
			}
			return "下拉微信分组成功";
		}
		return "服务器异常，请稍后重试";
	}

}
