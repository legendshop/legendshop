/*
 * LegendShop 多用户商城系统
 *  版权所有,并保留所有权利。
 */
package com.legendshop.admin.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.APPTemplateCategoryEnum;
import com.legendshop.model.constant.AppDecoratePageStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.spi.service.AppPageManageService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 后台-移动端装修页面管理控制器
 * @author legendshop
 */
@Controller
@RequestMapping("/admin/app/pageManagement")
public class AppPageManageController{

	@Autowired
	private AppPageManageService appPageManageService;
	
	/**
	 * 页面管理
	 * @param curPageNO 当前页码
	 * @param appPageManage 搜索参数
	 * @param flag 获取列表/内容的标记
	 */
	@RequestMapping(value="/query")
	public String bottomSetting(HttpServletRequest request, HttpServletResponse response,String curPageNO,AppPageManage appPageManage,String flag){
		
		if(AppUtils.isBlank(appPageManage.getCategory())){
			appPageManage.setCategory(APPTemplateCategoryEnum.INDEX.value());
		}
		
		PageSupport<AppPageManage> ps = appPageManageService.queryAppPageManage(curPageNO,10,appPageManage);
		PageSupportHelper.savePage(request, ps);
		
		request.setAttribute("appPageManage", appPageManage);
		if(AppUtils.isNotBlank(flag)){
			return PathResolver.getPath(AdminPage.PAGE_MANAGEMENT_CONTENT);
		}
		return PathResolver.getPath(AdminPage.PAGE_MANAGEMENT);
	}
	
	
	
	/**
	 * 新建页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/createPage")
	public String createPage(HttpServletRequest request, HttpServletResponse response,String category){
		
		AppPageManage pageManage = new AppPageManage();
		pageManage.setData("{\"backgroundType\":\"\",\"background\":\"\",\"components\":[]}");
		request.setAttribute("category", category);
		request.setAttribute("pageManage", pageManage);
		return PathResolver.getPath(AdminPage.CREATE_PAGE);
	}
	
	
	/**
	 * 跳转页面编辑
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/editPage/{pageId}")
	public String editPage(HttpServletRequest request, HttpServletResponse response,@PathVariable Long pageId){
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);
		if (AppUtils.isBlank(pageManage)) {
			throw new BusinessException("获取页面装修数据失败");
		}
		
		request.setAttribute("pageManage", pageManage);
		request.setAttribute("category", pageManage.getCategory());
		return  PathResolver.getPath(AdminPage.CREATE_PAGE);
	}
	
	
	
	/**
	 * 设为首页
	 */
	@RequestMapping(value="/use/{pageId}")
	@ResponseBody
	public String use(HttpServletRequest request, HttpServletResponse response,@PathVariable Long pageId){
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);

		if(AppUtils.isBlank(pageManage)){
			return Constants.FAIL;
		}
		// 将使用中的页面更新为未使用
		appPageManageService.updatePageToUnUse(0);
		
		// 将当前操作的页面设为首页
		pageManage.setIsUse(1);
		pageManage.setRecDate(new Date());
		appPageManageService.updateAppPageManage(pageManage);
		
		return  Constants.SUCCESS;
	}

	
	
	/**
	 * 复制页面
	 */
	@RequestMapping(value="/clone/{pageId}")
	@ResponseBody
	public String clone(HttpServletRequest request, HttpServletResponse response,@PathVariable Long pageId){
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);

		if(AppUtils.isNotBlank(pageManage)){
			pageManage.setIsUse(0);
			pageManage.setStatus(AppDecoratePageStatusEnum.DRAFT.value());
			pageManage.setId(null);
			pageManage.setRecDate(new Date());
			appPageManageService.saveAppPageManage(pageManage);
		}
		return Constants.SUCCESS;
	}
	
	
	
	/**
	 * 删除页面
	 */
	@RequestMapping(value="/delete/{pageId}")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response,@PathVariable Long pageId){
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);
		if(AppUtils.isNotBlank(pageManage)){
			appPageManageService.deleteAppPageManage(pageManage);
		}
		return Constants.SUCCESS;
	}
	
	
	
	/**
	 * 发布页面
	 */
	@RequestMapping(value="/release/{pageId}")
	@ResponseBody
	public String release(HttpServletRequest request, HttpServletResponse response,@PathVariable Long pageId){
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);
		if(AppUtils.isBlank(pageManage)){
			return "该页面不存在";
		}

		// 发布装修数据
		pageManage.setReleaseData(pageManage.getData());
		pageManage.setStatus(AppDecoratePageStatusEnum.RELEASED.value());
		pageManage.setRecDate(new Date());
		
		appPageManageService.updateAppPageManage(pageManage);
		return Constants.SUCCESS;
	}
	
	
	
	/**
	 * 修改页面名称
	 */
	@RequestMapping(value="/changeName")
	@ResponseBody
	public String changeName(HttpServletRequest request, HttpServletResponse response,Long pageId,String name){
		
		// 判断编辑的name不能为空
		if (AppUtils.isBlank(name.trim())||(name.trim())==""){
			return "页面名称不能为空!";
		}
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);
		if(AppUtils.isNotBlank(pageManage)){
			pageManage.setName(name);
			appPageManageService.updateAppPageManage(pageManage);
		}
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 保存页面装修
	 * @param category 页面类型
	 * @param decotateData 装修数据
	 * @param pageId 页面id(修改)
	 */
	@RequestMapping(value="/savePage",method=RequestMethod.POST)
	@ResponseBody
	public String savePage(HttpServletRequest request, HttpServletResponse response,String category,String decotateData,Long pageId){
		
		if(AppUtils.isBlank(decotateData)){
			return "请先装修任意楼层";
		}
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);

		// 编辑
		if(AppUtils.isNotBlank(pageManage)){
			pageManage.setData(decotateData);
			pageManage.setRecDate(new Date());
			pageManage.setStatus(AppDecoratePageStatusEnum.MODIFIED.value());
			appPageManageService.updateAppPageManage(pageManage);
		// 新建
		}else{
			pageManage = new AppPageManage();
			pageManage.setName("自建页面");
			pageManage.setStatus(AppDecoratePageStatusEnum.DRAFT.value());
			pageManage.setCategory(category);
			pageManage.setData(decotateData);
			pageManage.setRecDate(new Date());
			pageManage.setIsUse(0);
			appPageManageService.saveAppPageManage(pageManage);
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 发布页面装修
	 * @param category 页面类型
	 * @param decotateData 装修数据
	 * @param pageId 页面id
	 */
	@RequestMapping(value="/releasePage",method=RequestMethod.POST)
	@ResponseBody
	public String releasePage(HttpServletRequest request, HttpServletResponse response,String category,String decotateData,Long pageId){
		
		if(AppUtils.isBlank(decotateData)){
			return "请先装修任意楼层";
		}
		
		AppPageManage pageManage = appPageManageService.getAppPageManage(pageId);

		// 编辑
		if(AppUtils.isNotBlank(pageManage)){
			pageManage.setData(decotateData);
			// 更新已发布装修数据
			pageManage.setReleaseData(decotateData);
			pageManage.setRecDate(new Date());
			pageManage.setStatus(AppDecoratePageStatusEnum.RELEASED.value());
			appPageManageService.updateAppPageManage(pageManage);
		}else{ // 新建
			pageManage = new AppPageManage();
			pageManage.setName("自建页面");
			pageManage.setStatus(AppDecoratePageStatusEnum.RELEASED.value());
			pageManage.setCategory(category);
			pageManage.setData(decotateData);
			//发布装修数据
			pageManage.setReleaseData(decotateData);
			pageManage.setRecDate(new Date());
			pageManage.setIsUse(0);
			appPageManageService.saveAppPageManage(pageManage);
		}
		return Constants.SUCCESS;
	}
	
}
