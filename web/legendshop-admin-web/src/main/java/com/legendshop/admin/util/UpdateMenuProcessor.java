/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.security.menu.MenuManager;
/**
 * 更新菜单
 *
 */
@Component
public class UpdateMenuProcessor implements EventProcessor<Object>{

	private final Logger logger = LoggerFactory.getLogger(UpdateMenuProcessor.class);
	
	@Autowired
	private MenuManager menuManager;
	/**
	 * 处理逻辑
	 */
	@Override
	public void process(Object task) {
		logger.debug("Starting update menu list ");
		menuManager.init();
		logger.debug("Finish update menuManager menu list ");
	}
}