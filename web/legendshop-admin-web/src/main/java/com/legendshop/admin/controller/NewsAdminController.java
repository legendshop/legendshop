/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.framework.cache.TableCache;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.TagTypeEnum;
import com.legendshop.model.dto.PositonDto;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.NewsPosition;
import com.legendshop.model.entity.TagItemNews;
import com.legendshop.model.entity.TagItems;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.NewsPositionService;
import com.legendshop.spi.service.NewsService;
import com.legendshop.spi.service.TagItemNewsService;
import com.legendshop.spi.service.TagService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.CodeFilter;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SafeHtml;

/**
 * 文章控制器.
 */
@Controller
@RequestMapping("/admin/news")
public class NewsAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(NewsAdminController.class);

	/** The news service. */
	@Autowired
	private NewsService newsService;

	@Autowired
	private TagService tagService;

	@Autowired
	private TagItemNewsService tagItemNewsService;

	@Autowired
	private NewsPositionService newsPositionService;

	@Autowired
	private TableCache codeTablesCache;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查找文章
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param news
	 * @param position
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, News news) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "n.news_date", "n.high_line" });

		PageSupport<News> ps = newsService.getNewsListPage(curPageNO, news, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		
		Map<String, String> map = codeTablesCache.getCodeTable("NEWS_POSITION"); 
		List<PositonDto> positionItem = new ArrayList<PositonDto>();

		for (String id : map.keySet()) {
			PositonDto dto = new PositonDto();
			dto.setPositionId(Integer.valueOf(id));
			dto.setPositionName(map.get(id));
			positionItem.add(dto);
		}
//		positionItem.remove(2); // 移走"底部文章"
		request.setAttribute("positionItem", positionItem);
		request.setAttribute("news", news);
		return PathResolver.getPath(AdminPage.NEWS_LIST_PAGE);
	}

	/**
	 * 查找文章
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param news
	 * @param position
	 * @return
	 */
	@RequestMapping(value = "/querynews/{catid}", method = RequestMethod.GET)
	@ResponseBody
	public String querynews(HttpServletRequest request, HttpServletResponse response, @PathVariable Long catid) {
		List<News> newsList = newsService.getNewsByCatId(catid);
		List<KeyValueEntity> entities = new ArrayList<KeyValueEntity>();
		if (AppUtils.isNotBlank(newsList)) {
			for (int i = 0; i < newsList.size(); i++) {
				News news = newsList.get(i);
				if (news.getStatus() != null && news.getStatus() == 1) {
					KeyValueEntity entity = new KeyValueEntity();
					entity.setKey(String.valueOf(newsList.get(i).getId()));
					entity.setValue(newsList.get(i).getNewsTitle());
					entities.add(entity);
				}
			}
		}
		return JSONUtil.getJson(entities);
	}

	/**
	 * 保存文章
	 * 
	 * @param request
	 * @param response
	 * @param news
	 * @return the string
	 */
	@SystemControllerLog(description="保存文章")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, News news) {
		if (news.getNewsId() != null) {
			return update(request, response, news);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		news.setUserId(userId);
		news.setUserName(userName);
		newsService.save(news);
	/*	saveMessage(request, ResourceBundleHelper.getSucessfulString());*/
		String result = PathResolver.getRedirectPath(RedirectPage.NEWS_LIST_QUERY.getNativeValue());
		return result;
	}

	/**
	 * 设置文章简介.
	 * 
	 * @param news
	 */
	private void setNewsBrief(News news) {
		String newsContent = news.getNewsContent();
		if (newsContent != null && newsContent.length() > 0) {
			Integer len = newsContent.length();
			int maxLength = 100;
			boolean max = len > maxLength;
			if (max) {
				news.setNewsBrief(CodeFilter.unHtml(news.getNewsContent().substring(0, maxLength)) + "...");
			} else {
				news.setNewsBrief(CodeFilter.unHtml(newsContent));
			}

		}

	}

	/**
	 * 删除文章
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@SystemControllerLog(description="删除文章")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		News news = newsService.getNewsById(id);
		if (news != null) {
			log.info("{},delete News Title{}", news.getUserName(), news.getNewsTitle());
			newsService.delete(id);
		}
		saveMessage(request, ResourceBundleHelper.getDeleteString());
		return PathResolver.getRedirectPath(RedirectPage.NEWS_LIST_QUERY.getNativeValue());
	}

	/**
	 * Load.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		News news = newsService.getNewsById(id);

		// 取得关于文章的所有Tag
		List<TagItems> tagItems = tagService.queryTagItemsListByType(TagTypeEnum.HELP_CENTER.value());
		if (!AppUtils.isBlank(tagItems)) {
			request.setAttribute("tags", tagItems);
		}
		// 取得所有位置的标题
		Map<String, String> map = codeTablesCache.getCodeTable("NEWS_POSITION"); // ls_cst_table
																					// 里的定义
		List<PositonDto> positionItem = new ArrayList<PositonDto>();
		for (String positionId : map.keySet()) {
			PositonDto dto = new PositonDto();
			dto.setPositionId(Integer.valueOf(positionId));
			dto.setPositionName(map.get(positionId));
			positionItem.add(dto);
		}
		request.setAttribute("positionItem", positionItem);

		// 取回文章ID对应的Tag

		List<TagItemNews> itemNews = tagItemNewsService.getTagItemNewsByNewsId(id);
		String tagNews = "";
		for (int i = 0; i < itemNews.size(); i++) {
			if (i > 0) {
				tagNews += ",";
			}
			tagNews += itemNews.get(i).getTagItemId();
		}
		news.setNewsTags(tagNews);

		// 取回文章ID对应的Position
		List<NewsPosition> newsPositions = newsPositionService.getNewsPositionByNewsId(id);
		String newPositon = "";
		for (int i = 0; i < newsPositions.size(); i++) {
			if (i > 0) {
				newPositon += ",";
			}
			newPositon += newsPositions.get(i).getPosition();
		}
		news.setPositionTags(newPositon);

		request.setAttribute("news", news);
		return PathResolver.getPath(AdminPage.NEWS_EDIT_PAGE);
	}

	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		// 取得关于文章的所有Tag
		List<TagItems> tagItems = tagService.queryTagItemsListByType(TagTypeEnum.HELP_CENTER.value());
		if (!AppUtils.isBlank(tagItems)) {
			request.setAttribute("tags", tagItems);
		}
		// 取得所有位置的标题
		Map<String, String> map = codeTablesCache.getCodeTable("NEWS_POSITION"); // ls_cst_table
																					// 里的定义

		List<PositonDto> positionItem = new ArrayList<PositonDto>();

		for (String id : map.keySet()) {
			PositonDto dto = new PositonDto();
			dto.setPositionId(Integer.valueOf(id));
			dto.setPositionName(map.get(id));
			positionItem.add(dto);
		}
		//positionItem.remove(2); // 移走"底部文章"
		request.setAttribute("positionItem", positionItem);
		return PathResolver.getPath(AdminPage.NEWS_EDIT_PAGE);
	}

	/**
	 * 更新文章
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param news
	 *            the news
	 * @return the string
	 */
	@SystemControllerLog(description="更新文章")
	public String update(HttpServletRequest request, HttpServletResponse response, News news) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		News origin = newsService.getNewsById(news.getNewsId());
		log.info("{} update News Title{}", origin.getUserName(), origin.getNewsTitle());

		origin.setHighLine(news.getHighLine());
		origin.setNewsCategoryId(news.getNewsCategoryId());
		origin.setNewsDate(new Date());
		origin.setStatus(news.getStatus());
		origin.setNewsContent(news.getNewsContent());
		origin.setNewsBrief(XssFilterUtil.cleanXSS(news.getNewsBrief()));
		origin.setNewsTitle(XssFilterUtil.cleanXSS(news.getNewsTitle()));
		origin.setSeq(news.getSeq());
		origin.setNewsTags(news.getNewsTags());
		origin.setPositionTags(news.getPositionTags());
		news.setUserId(userId);
		news.setUserName(userName);

		newsService.update(origin);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getRedirectPath(RedirectPage.NEWS_LIST_QUERY.getNativeValue());
	}

	/**
	 * 更新文章状态
	 * 
	 * @param request
	 * @param response
	 * @param newsId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新文章状态")
	@RequestMapping(value = "/updatestatus/{newsId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long newsId,
			@PathVariable Integer status) {
		
		News news = newsService.getNewsById(newsId);
		if (news == null) {
			return -1;
		}
		if (!status.equals(news.getStatus())) {
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				news.setStatus(status);
				news.setNewsDate(new Date());
				newsService.updateStatus(news);
			}
		}
		return news.getStatus();
	}

}
