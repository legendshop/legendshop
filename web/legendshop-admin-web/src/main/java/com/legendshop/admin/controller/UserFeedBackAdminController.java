/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.sms.event.processor.SendSiteMessageProcessor;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.UserFeelbackStatusEnum;
import com.legendshop.model.entity.UserFeedBack;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.UserFeedBackService;

/**
 * 反馈意见.
 */
@Controller
@RequestMapping("/admin/userFeedBack")
public class UserFeedBackAdminController extends BaseController{

	private final Logger log = LoggerFactory.getLogger(UserFeedBackAdminController.class);

	@Autowired
	private UserFeedBackService userFeedBackService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;
    @Autowired
    private SendSiteMessageProcessor sendSiteMessageProcessor;


	/**
	 * 查询反馈意见列表
	 */
	
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			UserFeedBack userFeedback) {
		PageSupport<UserFeedBack> ps = userFeedBackService.getUserFeedBackPage(curPageNO, userFeedback);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userFeedback", userFeedback);
		adminMenuUtil.parseMenu(request, 1); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.FEEDBACK_LIST_PAGE);
	}

	/**
	 * 保存反馈意见
	 */
	
	public String save(HttpServletRequest request, HttpServletResponse response, UserFeedBack entity) {
		return null;
	}

	/**
	 * 删除反馈意见
	 */
	
	public String delete(HttpServletRequest request, HttpServletResponse response, Long id) {
		return null;
	}

	/**
	 * 加载
	 */
	
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return null;
	}

	/**
	 * 更新反馈意见
	 * @param request
	 * @param response
	 * @param id
	 * @param reply
	 * @return
	 */
	@SystemControllerLog(description="更新反馈意见")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request, HttpServletResponse response, Long id, String reply) {
		UserFeedBack userFeedBack = userFeedBackService.getUserFeedBack(id);
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		userFeedBack.setRespContent(reply);
		userFeedBack.setRespMgntId(user.getUserId());
		userFeedBack.setStatus(UserFeelbackStatusEnum.READED.value());
		userFeedBack.setRespDate(new Date());
		userFeedBackService.updateUserFeedBack(userFeedBack);
		//给用户发送信息
        if(AppUtils.isNotBlank(userFeedBack.getName())){
            sendSiteMessageProcessor.process(new SendSiteMsgEvent(userFeedBack.getName(),"反馈信息更新啦！","您的反馈信息【"+userFeedBack.getContent()+"】平台已处理，处理意见为【"+userFeedBack.getRespContent()+"】感谢您的意见！").getSource());
        }
        return Constants.SUCCESS;
	}

	/**
	 * 更新
	 */
	public String update(HttpServletRequest request, HttpServletResponse response, Long id) {
		return null;
	}

}
