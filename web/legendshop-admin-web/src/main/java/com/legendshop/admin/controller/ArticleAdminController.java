/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Article;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ArticleService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 杂志管理控制器
 */
@Controller
@RequestMapping("/admin/article")
public class ArticleAdminController extends BaseController{
	@Autowired
	private ArticleService articleService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 列表查询
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Article article) {
		PageSupport<Article> ps = articleService.getArticlePage(curPageNO, article);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("article", article);
		return PathResolver.getPath(AdminPage.ARTICLE_LIST_PAGE);
	}

	/**
	 * 保存杂志
	 */
	@SystemControllerLog(description="保存杂志")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Article article) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(article.getId())) {
			Date date = new Date();
			article.setCreateTime(date);
			article.setCommentsNum(0L);
			article.setForwardNum(0L);
			article.setThumbNum(0L);
		} else {
			Article temp = articleService.getArticle(article.getId());
			article.setCreateTime(temp.getCreateTime());
			article.setCommentsNum(temp.getCommentsNum());
			article.setForwardNum(temp.getForwardNum());
			article.setThumbNum(temp.getThumbNum());
		}
		// 上传图片
		String pic = null;
		if (article.getPicFile() != null && article.getPicFile().getSize() > 0) {
			pic = attachmentManager.upload(article.getPicFile());
			}
		articleService.saveArticle(user.getUsername(), user.getUserId(), user.getShopId(),article,pic);
		return PathResolver.getPath(RedirectPage.ARTICLE_lIST);
	}

	/**
	 * 删除杂志
	 */
	@SystemControllerLog(description="删除杂志")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Article article = articleService.getArticle(id);
		articleService.deleteArticle(article);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.ARTICLE_lIST);
	}

	/**
	 * 根据Id加载
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Article article = articleService.getArticle(id);
		request.setAttribute("article", article);
		return PathResolver.getPath(AdminPage.ARTICLE_EDIT_PAGE);
	}

	/**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		// return "/article/article";
		return PathResolver.getPath(AdminPage.ARTICLE_EDIT_PAGE);
	}

	/**
	 * 更新编辑页面
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Article article = articleService.getArticle(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// article.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("article", article);
		return "forward:/admin/article/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ARTICLE_EDIT_PAGE);
	}

}
