/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.SubFloorService;

/**
 * 楼层管理控制器
 *
 */
@Controller
@RequestMapping("/admin/subFloor")
public class SubFloorController extends BaseController {
	
	@Autowired
	private SubFloorService subFloorService;

	/**
	 * 查询楼层列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param subFloor
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, SubFloor subFloor) {
		PageSupport<SubFloor> ps = subFloorService.getSubFloorPage(curPageNO, subFloor);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("subFloor", subFloor);
		return PathResolver.getPath(BackPage.SUBFLOOR_LIST_PAGE);
	}
	
	/**
	 * 保存楼层
	 * @param request
	 * @param response
	 * @param subFloor
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, SubFloor subFloor) {
		// check subFloor
		if (subFloor == null || subFloor.getFloorId() == null) {
			throw new NotFoundException("subFloor or floorId is NULL");
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		subFloor.setRecDate(new Date());
		subFloorService.saveSubFloor(subFloor);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.FLOOR_LIST_QUERY);
	}

	/**
	 * 删除楼层
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SubFloor subFloor = subFloorService.getSubFloor(id);
		subFloorService.deleteSubFloor(subFloor);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.FLOOR_LIST_QUERY);
	}

	/**
	 * 查看子楼层详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SubFloor subFloor = subFloorService.getSubFloor(id);
		if (subFloor != null) {
			request.setAttribute("flId", subFloor.getFloorId());
			request.setAttribute("subFloor", subFloor);
		}
		return PathResolver.getPath(AdminPage.SUBFLOOR_EDIT_PAGE);
	}
	
	/**
	 * 查看楼层详情
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/loadSubFloor/{flId}")
	public String loadSubFloor(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId) {
		request.setAttribute("flId", flId);
		return PathResolver.getPath(AdminPage.SUBFLOOR_EDIT_PAGE);
	}
	
	/**
	 * 更新楼层
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SubFloor subFloor = subFloorService.getSubFloor(id);
		request.setAttribute("subFloor", subFloor);
		return PathResolver.getPath(AdminPage.SUBFLOOR_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * @param request
	 * @param response
	 * @param sfId
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{sfId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long sfId, @PathVariable Integer status) {
		
		SubFloor subFloor = subFloorService.getSubFloor(sfId);
		if (subFloor == null) {
			return -1;
		}
		
		
		if (!status.equals(subFloor.getStatus())) {
			subFloor.setStatus(status);
			subFloor.setRecDate(new Date());
			subFloorService.updateSubFloor(subFloor);
		}
		return subFloor.getStatus();
	}

}
