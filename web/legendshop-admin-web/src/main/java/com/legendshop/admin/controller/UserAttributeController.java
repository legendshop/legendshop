/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ProdUserAttribute;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductService;

/**
 * 用户自定义属性控制器
 *
 */
@Controller
@RequestMapping("/admin/userAttribute")
public class UserAttributeController extends BaseController {

	@Autowired
	private ProductService productService;

	/**
	 * 去往选择用户属性弹框
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param prodUserParam
	 * @return
	 */
	@RequestMapping("/overlay")
	public String attributeOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProdUserAttribute prodUserAttribute) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ProdUserAttribute> ps = productService.getProdUserAttributePage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.USERATTRIBUTE_OVERLAY);
	}

	/**
	 * 删除用户自定义属性
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	public @ResponseBody String deleteAttribute(HttpServletRequest request, HttpServletResponse response, Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		productService.deleteProdUserAttribute(id, shopId);
		return Constants.SUCCESS;
	}

	/**
	 * 使用用户自定义属性
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param id
	 * @return
	 */
	@RequestMapping("/use/{id}")
	public @ResponseBody String useAttribute(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable Long id) {
		ProdUserAttribute prodUserAttr = productService.getProdUserAttribute(id);
		return prodUserAttr.getContent();
	}

	/**
	 * 去往保存 用户自定义属性的div
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/toSave")
	public String toSaveAttribute(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.SAVE_ATTRIBUTE);
	}

	/**
	 * 保存用户自定义参数
	 * 
	 * @param request
	 * @param response
	 * @param content
	 * @param name
	 * @return
	 */
	@RequestMapping("/save")
	public @ResponseBody String userAttribute(HttpServletRequest request, HttpServletResponse response, String content, String name) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		ProdUserAttribute prodUserAttribute = new ProdUserAttribute();
		prodUserAttribute.setContent(content);
		prodUserAttribute.setName(name);
		prodUserAttribute.setUserId(user.getUserId());
		prodUserAttribute.setShopId(user.getShopId());
		prodUserAttribute.setRecDate(new Date());
		productService.saveProdUserAttribute(prodUserAttribute);
		return Constants.SUCCESS;
	}
}
