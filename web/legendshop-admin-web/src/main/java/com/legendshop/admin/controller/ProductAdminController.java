/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.*;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.advanced.search.processor.ProductIndexDelProcessor;
import com.legendshop.advanced.search.processor.ProductIndexUpdateProcessor;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.model.entity.ProdGroupRelevance;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProdGroupRelevanceService;
import com.legendshop.spi.service.ProdGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 产品控制器.
 */
@Controller
@RequestMapping("/admin/product")
public class ProductAdminController extends BaseController {

	@Autowired
	private ProductService productService;

	@Autowired
	private ProdGroupService prodGroupService;

	@Autowired
	private ProdGroupRelevanceService prodGroupRelevanceService;

	@Autowired
	private BrandService brandService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductIndexDelProcessor productIndexDelProcessor;

	@Autowired
	private ProductIndexUpdateProcessor productIndexUpdateProcessor;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 删除商品.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param prodIdStr
	 *            the id
	 * @return the string
	 */
	@SystemControllerLog(description="批量删除商品")
	@RequestMapping(value = "/batchOffline/{prodIdStr}")
	public @ResponseBody Object batchOffline(HttpServletRequest request, HttpServletResponse response, @PathVariable String prodIdStr) {
		if (AppUtils.isNotBlank(prodIdStr)) {
			try {
				SecurityUserDetail user = UserManager.getUser(request);
				String userName = user.getUsername();
				List<Long> prodIdList = Stream.of(prodIdStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
				List<Product> productList = productService.queryProductByProdIds(prodIdList);
				productService.batchOffline(prodIdList);
				// 更新索引
				productIndexUpdateProcessor.process(new ProductIdDto(prodIdList));
				for (Product prod : productList) {
					sendSiteMessageProcessor.process(new SendSiteMsgEvent(userName, prod.getUserName(), "违规商品下架", "商品【" + prod.getName() + "】违规下架").getSource());
				}
				return Constants.SUCCESS;
			} catch (Exception e) {
				e.printStackTrace();
				return Constants.FAIL;
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 查询商品列表
	 *
	 * @param request
	 * @param response
	 * @param status
	 * @param product
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response,Integer status, Product product) {
		adminMenuUtil.parseMenu(request, 2); // 固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		request.setAttribute("status", status);

		if (!AppUtils.isBlank(status)) {
			if (status == 4) {
			} else {
				request.setAttribute("type", status);
			}
		}
		request.setAttribute("prod", product);
		return PathResolver.getPath(AdminPage.PRODUCT_LIST_PAGE);
	}

	/**
	 * 查询商品内容
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		Integer pageSize = null;

		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "siteName", "views", "buys", "stocks" });
		if (!AppUtils.isBlank(product.getStatus())) {
			if (product.getStatus() == 4) {
			} else {
				request.setAttribute("type", product.getStatus());
			}
		}

		PageSupport<Product> ps = productService.getProductListPage(product, curPageNO, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(AdminPage.PRODUCT_CONTENT_LIST_PAGE);
	}

	/**
	 * 加载类型编辑页面
	 *
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/loadAllCategory/{type}")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) {
		List<Category> catList = categoryService.getCategoryList(type);
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		for (Category cat : catList) {
			CategoryTree tree = new CategoryTree(cat.getId(), cat.getParentId(), cat.getName());
			catTrees.add(tree);
		}
		String json = JSONUtil.getJson(catTrees);
		request.setAttribute("catTrees", json);
		request.setAttribute("type", type);
		return PathResolver.getPath(BackPage.PRODLIST_LOADCATEGORY_PAGE);
	}

	/**
	 * 加载类型编辑页面
	 *
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/loadAllCategory2/{type}")
	public String loadCategory2(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) {
		List<Category> catList = categoryService.getCategoryList(type);
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		for (Category cat : catList) {
			CategoryTree tree = new CategoryTree(cat.getId(), cat.getParentId(), cat.getName());
			catTrees.add(tree);
		}
		request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
		request.setAttribute("type", type);
		request.setAttribute("all", "all");
		return PathResolver.getPath(BackPage.PRODLIST_LOADCATEGORY_PAGE);
	}

	@RequestMapping(value = "/getBrandsByName")
	public String getBrandsByName(HttpServletRequest request, HttpServletResponse response, String brandName) {
		PageSupport<Brand> ps = brandService.getBrandsByNamePage(brandName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.PROD_BRAND_EDIT_PAGE);
	}

	/**
	 * 删除商品.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@SystemControllerLog(description="删除商品")
	@RequestMapping(value = "/delete/{prodId}")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		Long shopId = user.getShopId();

		Product prod = productService.getProductById(prodId);
		boolean isAdmin = UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value());
		String result = productService.delete(shopId, prodId, isAdmin);
		if (Constants.SUCCESS.equals(result)) {
			// 删除商品后 ，删除索引数据
			productIndexDelProcessor.process(new ProductIdDto(prodId));
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(userName, prod.getUserName(), "违规删除", "商品【" + prod.getName() + "】违规删除").getSource());
		}
		return result;
	}

	/**
	 * 通过控制器到页面.
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @return the string
	 */
	@RequestMapping(value = "/append/{prodId}")
	public String append(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		request.setAttribute("prodId", prodId);
		return PathResolver.getPath(BackPage.APPEND_PRODUCT);
	}

	/**
	 * 创建步奏.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/createsetp")
	public String createsetp(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.CREATEP_RODUCT_STEP);
	}

	/**
	 * 更新状态
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param status
	 * @return the integer
	 */
	@SystemControllerLog(description="更新商品状态")
	@RequestMapping(value = "/updatestatus/{prodId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, @PathVariable Integer status) {
		Product product = productService.getProductById(prodId);
		if (product == null || status == null) {
			return -1;
		}
		if (!status.equals(product.getStatus())) {
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				product.setStatus(status);
				product.setModifyDate(new Date());
				productService.updateProd(product);
			}
		}
		return product.getStatus();
	}

	@SystemControllerLog(description="修改商品权重")
	@RequestMapping(value = "/updateSearchWeight")
	public @ResponseBody String updateSearchWeight(@RequestParam Long prodId,@RequestParam Double searchWeight) {
		Boolean reuslt = productService.updateSearchWeight(prodId,searchWeight);
		if(!reuslt){
			return Constants.FAIL;
		}
		productIndexUpdateProcessor.process((new ProductIdDto(prodId)));
		return Constants.SUCCESS;
	}

	/**
	 * 通过控制器到页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/loadnew")
	public String loadnew(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.PROD_EDIT_NEW_PAGE);
	}

	/**
	 * 通过控制器到页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/productProperty")
	public String productProperty(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.PROD_EDIT_PROPERTY_PAGE);
	}

	/**
	 * 通过控制器到页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/productDetails")
	public String productDetails(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.PROD_EDIT_DETAILS_PAGE);
	}

	/**
	 * 将商品放入回收站
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@SystemControllerLog(description="将商品放入回收站")
	@RequestMapping(value = "/toDustbin/{prodId}")
	public @ResponseBody String toDustbin(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		if (AppUtils.isNotBlank(prodId)) {
			Product product = productService.getProductById(prodId);
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			if (product.getShopId().equals(shopId)) {
				productService.updateStatus(Constants.STOPUSE, prodId, null);
				return Constants.SUCCESS;
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 将违规商品 下架
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param auditOpinion
	 *            违规意见
	 * @return
	 */
	@SystemControllerLog(description="将违规商品下架")
	@RequestMapping(value = "/illegalOff")
	public @ResponseBody String illegalOff(HttpServletRequest request, HttpServletResponse response, Long prodId, String auditOpinion) {
		if (AppUtils.isNotBlank(prodId)) {
			productService.updateStatus(ProductStatusEnum.PROD_ILLEGAL_OFF.value(), prodId, auditOpinion);
			Product prod = productService.getProductById(prodId);
			productIndexUpdateProcessor.process(new ProductIdDto(prodId));

			SecurityUserDetail user = UserManager.getUser(request);
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(user.getUsername(), prod.getUserName(), "违规商品下架", "商品【" + prod.getName() + "】违规下架，违规意见：" + auditOpinion).getSource());
            return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 审核商品
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param audit
	 *            审核状态 1通过 0不通过
	 * @param auditOpinion
	 *            审核意见
	 * @return
	 */
	@RequestMapping(value = "/audit")
	public @ResponseBody String audit(HttpServletRequest request, HttpServletResponse response, Long prodId, Long audit, String auditOpinion) {
		if (AppUtils.isNotBlank(prodId) && AppUtils.isNotBlank(audit)) {
			if (audit == 1) {
				Product product = productService.getProductById(prodId);
				Integer status = ProductStatusEnum.PROD_ONLINE.value();
				if (product != null && product.getPreStatus() != null) {
					status = product.getPreStatus();
				}
				productService.updateStatus(status, prodId, auditOpinion);
				if (ProductStatusEnum.PROD_ONLINE.value().equals(status)) {
					productIndexUpdateProcessor.process(new ProductIdDto(prodId));
				}
			} else {
				productService.updateStatus(ProductStatusEnum.PROD_AUDIT_FAIL.value(), prodId, auditOpinion);
			}

			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 导出商品信息
	 *
	 * @param request
	 * @param response
	 * @param data
	 * @return
	 */
	@SystemControllerLog(description="导出商品信息")
	@RequestMapping(value = "/export")
	@ResponseBody
	public String export(HttpServletRequest request, HttpServletResponse response, String data) {
		List<String> str = JSONUtil.getArray(data, String.class);
		Product prod = new Product();
		if (AppUtils.isNotBlank(str.get(0))) {
			prod.setStatus(Integer.parseInt(str.get(0)));
		}
		if(AppUtils.isNotBlank(str.get(1))){
			prod.setShopId(Long.parseLong(str.get(1)));
		}
		if (AppUtils.isNotBlank(str.get(2))) {
			prod.setCategoryId(Long.parseLong(str.get(2)));
		}
		if (AppUtils.isNotBlank(str.get(3))) {
			prod.setBrandId(Long.parseLong(str.get(3)));
		}
		prod.setName(str.get(4));
		if (AppUtils.isNotBlank(str.get(5))) {
			prod.setProdId(Long.parseLong(str.get(5)));
		}
		List<ProductExportDto> dataset = this.productService.findExportProd(prod);
		ExportExcelUtil<ProductExportDto> ex = new ExportExcelUtil<ProductExportDto>();
		String[] headers = { "商品名称", "副标题", "属性", "原价", "现价", "当前库存", "实际库存", "库存警告", "商品条形码", "商家编码" };
		try {
			response.setHeader("Content-Disposition", "attachment;filename=prodList.xls");
			ex.exportProdExcelUtil(headers, dataset, response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}


	/**
	 * 获取简单的商品信息数据
	 */
	@RequestMapping(value = "/getShopSelect2")
	public @ResponseBody Object getShopUserByShopName(HttpServletRequest request, HttpServletResponse response,String q,Integer currPage, Integer pageSize) {
		List<Select2Dto> list=productService.getProductSelect2(q,currPage,pageSize);
		int total = productService.getProductSelect2Count(q);
		JSONObject result = new JSONObject();
    	result.put("results", list);
    	result.put("total", total);
		return result;
	}

	/**
	 * 加载批量修改商品分组页面
	 * @param request
	 * @param response
	 * @param selAry
	 * @param map
	 * @return
	 */
	@RequestMapping("/groupProd")
	public String groupProd(HttpServletRequest request, HttpServletResponse response,@RequestParam String selAry) {
		request.setAttribute("groupProd", prodGroupService.getProdGroupByType(1));
		request.setAttribute("prodIds", selAry);
		String result = PathResolver.getPath(BackPage.GROUP_PRODUCT);
		return result;
	}

	/**
	 * 分组管理
	 * @param request
	 * @param response
	 * @param selAry
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/groupProdUpdate")
	public String batchEditUpdate(HttpServletRequest request, HttpServletResponse response,@RequestParam()String prodIds,@RequestParam(value = "groupid")String groupid) {
		try {
			String[]pids=prodIds.split(",");
			String[]gids=groupid.split(",");
			for (String gid : gids) {
				ProdGroup pro=prodGroupService.getProdGroup(Long.parseLong(gid));
				for(String psid:pids){
					Product product=productService.getProductById(Long.parseLong(psid));
					int num=prodGroupRelevanceService.getProductByGroupId(product.getProdId(),Long.parseLong(gid));
					if (num<=0) {
						ProdGroupRelevance prodGroupRelevance=new ProdGroupRelevance(pro.getId(),product.getId());
						prodGroupRelevanceService.saveProdGroupRelevance(prodGroupRelevance);
					}
				}
			}

		} catch (Exception e) {
			return "false";
		}
		return "true";
	}

//	/**
//	 * 获取用户服务说明
//	 */
//	@RequestMapping("/serviceShow")
//	public String getServerShow(Long prodId) {
//		String result = productService.getServerShowById(prodId);
//		return result;
//	}
	/**
	 * 获取用户服务说明
	 */
	@ApiOperation(value = "根据商品id获取服务说明", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/serviceShow")
	@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品id", required = true, dataType = "Long")
	public String getServerShow(Long prodId) {
		String result = productService.getServerShowById(prodId);
		return result;
	}


}
