/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.dto.AdminUserPassword;
import com.legendshop.model.entity.AdminUser;
import com.legendshop.model.entity.Role;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AdminUserService;
import com.legendshop.spi.service.DepartmentService;
import com.legendshop.util.AppUtils;

/**
 * 管理员控制器
 *
 */
@Controller
@RequestMapping("/admin/adminUser")
public class AdminUserController extends BaseController{
	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private DepartmentService departmentService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 检查管理员是否存在
	 * 
	 * @param userName
	 * @return
	 */
	@RequestMapping("/isAdminUserExist")
	public @ResponseBody Boolean isUserExist(String userName) {
		if (AppUtils.isBlank(userName)) {
			return true;// 用户名不允许有空值
		}
		return adminUserService.isAdminUserExist(userName.trim());
	}

	/**
	 * 查询列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, AdminUser adminUser) {
		PageSupport<AdminUser> ps = adminUserService.getAdminUser(curPageNO, adminUser);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", adminUser);

		return PathResolver.getPath(AdminPage.ADMINUSER_LIST_PAGE);
	}

	/**
	 * 保存管理员
	 */
	@SystemControllerLog(description="保存管理员")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) {
		// 加密密码
		adminUser.setPassword(passwordEncoder.encode(adminUser.getPassword()));
		Date date = new Date();
		adminUser.setRecDate(date);
		adminUser.setModifyDate(date);
		adminUserService.saveAdminUser(adminUser);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.ADMINUSER_LIST_QUERY);
	}

	/**
	 * 删除管理员
	 */
	@SystemControllerLog(description="删除管理员")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		AdminUser adminUser = adminUserService.getAdminUser(id);
		adminUserService.deleteAdminUser(adminUser);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.ADMINUSER_LIST_QUERY);
	}

	/**
	 * 根据Id加载
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		AdminUser adminUser = adminUserService.getAdminUser(id);
		request.setAttribute("adminUser", adminUser);
		if (adminUser.getDeptId() != null) {
			String deptName = departmentService.getDepartmentName(adminUser.getDeptId());
			request.setAttribute("deptName", deptName);
		}

		 return PathResolver.getPath(AdminPage.ADMINUSER_UPDATE_PAGE);
	}

	/**
	 * 加载管理员编辑
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		List<Role> roleList;
		// 判断是否有VIEW_ALL_DATA权限，如果有，则加载所有的权限
		if (UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value())) {
			roleList = adminUserService.loadRole(ApplicationEnum.BACK_END.value());
		} else {
			// 如果没有权限则只可以给自己范围内的权限
			roleList = adminUserService.loadRole(user.getUserId(), ApplicationEnum.BACK_END.value());
		}

		request.setAttribute("roleList", roleList);
		 return PathResolver.getPath(AdminPage.ADMINUSER_EDIT_PAGE);
	}

	/**
	 * 更新管理员信息
	 */
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		AdminUser adminUser = adminUserService.getAdminUser(id);
		request.setAttribute("adminUser", adminUser);
		return PathResolver.getPath(RedirectPage.ADMINUSER_LIST_QUERY);
	}
	
	/**
	 * 更新管理员信息
	 */
	@SystemControllerLog(description="更新管理员信息")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateAdminUser(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) {
		AdminUser orgin = adminUserService.getAdminUser(adminUser.getId());
		if (orgin != null) {
			orgin.setEnabled(adminUser.getEnabled());
			orgin.setNote(adminUser.getNote());
			orgin.setDeptId(adminUser.getDeptId());
			orgin.setRealName(adminUser.getRealName());
			orgin.setHireDate(adminUser.getHireDate());
			orgin.setIdCardNum(adminUser.getIdCardNum());
			orgin.setAddr(adminUser.getAddr());
			orgin.setMobile(adminUser.getMobile());
			orgin.setActiveTime(adminUser.getActiveTime());
			adminUserService.updateAdminUser(orgin);
			saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		} else {
			saveError(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.error"));
		}
		return PathResolver.getPath(RedirectPage.ADMINUSER_LIST_QUERY);
	}

	/**
	 * 加载更新管理员密码页面
	 * 
	 * @param request
	 * @param response
	 * @param adminUser
	 * @return
	 */
	@RequestMapping(value = "/updatePwd", method = RequestMethod.GET)
	public String loadUpdatePassword(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		AdminUser adminUser = adminUserService.getAdminUser(user.getUserId());
		request.setAttribute("adminUser", adminUser);
		return PathResolver.getPath(BackPage.UPDATE_ADMIN_PWD);
	}

	/**
	 * 更新管理员密码
	 * 
	 * @param request
	 * @param response
	 * @param adminUser
	 * @return
	 */
	@SystemControllerLog(description="更新管理员密码")
	@RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
	@ResponseBody
	public String updatePassword(HttpServletRequest request, HttpServletResponse response, AdminUserPassword userPassword) {
		AdminUser orgin = adminUserService.getAdminUser(userPassword.getId());
		if (orgin != null) {
			// check password
			if (passwordEncoder.matches(userPassword.getOldPassword(), orgin.getPassword())) {//旧密码相等
				if (!userPassword.getOldPassword().equals(userPassword.getPassword())) {// 密码不相等才修改
					orgin.setPassword(passwordEncoder.encode(userPassword.getPassword()));
					adminUserService.updateAdminUser(orgin);
					return "OK"; // 修改成功
				} else {
					return "NOCHANGE";// 没有改密码
				}

			} else {
				return "DISMATCH";// 原来密码不匹配
			}
		} else {
			return "NOTFOUND"; // 找不到记录
		}
	}

}
