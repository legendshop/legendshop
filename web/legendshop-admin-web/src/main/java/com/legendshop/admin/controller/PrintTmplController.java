/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.PrintTmpl;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.spi.service.PrintTmplService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 运单列表控制器
 * 
 */
@Controller
@RequestMapping("/admin/printTmpl")
public class PrintTmplController extends BaseController {
	
	private final Logger logger = LoggerFactory.getLogger(PrintTmplController.class);

	@Autowired
	private PrintTmplService printTmplService;

	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private DeliveryTypeService deliveryTypeService;

	/**
	 * 查找运单列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, PrintTmpl printTmpl) {
		PageSupport<PrintTmpl> ps = printTmplService.getPrintTmplPage(curPageNO, printTmpl);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("printTmpl", printTmpl);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(AdminPage.PRINTTMPL_LIST_PAGE);
	}

	/**
	 * 保存运单列表
	 */
	@SystemControllerLog(description="保存运单列表")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, PrintTmpl printTmpl) {
		String pic = null; //
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return null;
		}
		PrintTmpl origin = null;
		if (AppUtils.isNotBlank(printTmpl.getPrtTmplId())) { // update
			origin = printTmplService.getPrintTmpl(printTmpl.getPrtTmplId());
			if (origin == null) {
				throw new NotFoundException("Origin Brand is empty");
			}
			origin.setPrtTmplTitle(printTmpl.getPrtTmplTitle());
			origin.setPrtDisabled(printTmpl.getPrtDisabled());
			origin.setPrtTmplHeight(printTmpl.getPrtTmplHeight());
			origin.setPrtTmplWidth(printTmpl.getPrtTmplWidth());
			origin.setIsSystem(printTmpl.getIsSystem());
			if (AppUtils.isNotBlank(printTmpl.getIsSystem())) {
				origin.setIsSystem(printTmpl.getIsSystem());
			}
			if (printTmpl.getBgimageFile() != null && printTmpl.getBgimageFile().getSize() > 0) {
				pic = attachmentManager.upload(printTmpl.getBgimageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, printTmpl.getBgimageFile(), AttachmentTypeEnum.OTHER);
				// 删除附件记录
				attachmentManager.delAttachmentByFilePath(origin.getBgimage());
				// 删除图片文件
				attachmentManager.deleteTruely(origin.getBgimage());

				origin.setBgimage(pic);
			}
			printTmplService.updatePrintTmpl(origin);
		} else {
			if (printTmpl.getBgimageFile() != null && printTmpl.getBgimageFile().getSize() > 0) {
				pic = attachmentManager.upload(printTmpl.getBgimageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, printTmpl.getBgimageFile(), AttachmentTypeEnum.OTHER);
			}
			printTmpl.setBgimage(pic);
			printTmplService.savePrintTmpl(printTmpl);
		}
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PRINTTMPL_LIST_QUERY);
	}

	/**
	 * 保存打印模板
	 * 
	 * @param request
	 * @param response
	 * @param printTmpl
	 * @return
	 */
	@SystemControllerLog(description="保存打印模板")
	@RequestMapping(value = "/savePrintDesign")
	public String savePrintDesign(HttpServletRequest request, HttpServletResponse response, PrintTmpl printTmpl) {
		if (AppUtils.isBlank(printTmpl.getPrtTmplId())) {
			logger.error("Missing parameter PrtTmplId");
			return "";
		}
		PrintTmpl origin = printTmplService.getPrintTmpl(printTmpl.getPrtTmplId());
		if (origin == null) {
			throw new NotFoundException("Origin Brand is empty");
		}
		origin.setPrtTmplTitle(printTmpl.getPrtTmplTitle());
		origin.setPrtDisabled(printTmpl.getPrtDisabled());
		origin.setPrtTmplHeight(printTmpl.getPrtTmplHeight());
		origin.setPrtTmplWidth(printTmpl.getPrtTmplWidth());
		origin.setPrtTmplData(printTmpl.getPrtTmplData());
		printTmplService.updatePrintTmpl(origin,printTmpl);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PRINTTMPL_LIST_QUERY);
	}


	@SystemControllerLog(description="删除运单列表")
	@RequestMapping(value = "/deleteById")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @RequestParam Long id) {
		Integer result = deliveryTypeService.getDeliveryTypeByprintTempId(id);
		if(result>0){
			return "此模版正在被使用，请先在运费模版中删除使用！";
		}
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		if(AppUtils.isBlank(printTmpl)){
			return "数据有误，请重新选择！";
		}
		printTmplService.deletePrintTmpl(printTmpl);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return Constants.SUCCESS;
	}
	

	/**
	 * 跳转编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		request.setAttribute("printTmpl", printTmpl);
		return PathResolver.getPath(AdminPage.PRINTTMPL_EDIT_PAGE);
	}

	/**
	 * 跳转打印模板编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/loadDesign/{id}")
	public String loadDesign(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		if (printTmpl == null) {
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}
		if (!printTmpl.getBgimage().startsWith("/resources/")) {
			printTmpl.setBgimage(PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage()).getFilePath());
			request.setAttribute("isUpdate", true);
		}
		request.setAttribute("printTmpl", printTmpl);
		return PathResolver.getPath(BackPage.PRINTTMPL_DESIGN);
	}

	/**
	 * 下载文件
	 * 
	 * @param request
	 * @param response
	 * @param imagesfile
	 * @return
	 */
	@RequestMapping(value = "/uploadFile")
	public @ResponseBody String uploadFile(MultipartHttpServletRequest request, HttpServletResponse response,
			MultipartFile imagesfile) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return Constants.FAIL;
		}
		if (imagesfile != null && imagesfile.getSize() > 0) {
			String pic = attachmentManager.upload(imagesfile);
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, imagesfile, AttachmentTypeEnum.OTHER);
			return pic;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 跳转添加打印运费模版页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PRINTTMPL_ADD_PAGE);
	}

	/**
	 * 更新运单列表
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);

		request.setAttribute("printTmpl", printTmpl);
		return "forward:/admin/printTmpl/query.htm";
	}

}
