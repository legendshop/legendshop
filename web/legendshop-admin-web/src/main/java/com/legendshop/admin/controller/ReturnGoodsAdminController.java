/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubRefundReturnDetail;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubRefundReturnDetailService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;

/**
 * 商品退货
 *
 */
@Controller
@RequestMapping("/admin/returnGoods")
public class ReturnGoodsAdminController extends BaseController {
	
	/** 待处理 */
	private static final String PENDING = "pending";
	
	/** 所有 */
	private static final String ALL = "all";

	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	@Autowired
	private SubRefundReturnDetailService subRefundReturnDetailService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;
	
	@Autowired
	private SubHistoryService subHistoryService;

	/**
	 * 查询退货列表
	 * 
	 * @param curPageNO
	 * @param paramData
	 *            查询参数
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("/query/{status}")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable String status, RefundSearchParamDto paramData) throws ParseException {
		request.setAttribute("status", status);
		request.setAttribute("paramData", paramData);
		return PathResolver.getPath(AdminPage.RETURN_LIST);
	}
	
	/**
	 * 查询退货列表内容
	 * 
	 * @param curPageNO
	 * @param paramData
	 *            查询参数
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String status, RefundSearchParamDto paramData) throws ParseException {
		if (PENDING.equalsIgnoreCase(status)) {
			request.setAttribute("status", PENDING);
		} else {
			request.setAttribute("status", ALL);
		}
		PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnPage(curPageNO, status, paramData);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData", paramData);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.RETURN_CONTENT_LIST);
	}

	/**
	 * 查询退货详情
	 *
	 */
	@RequestMapping("/returnDetail/{refundId}")
	public String refundDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		if (RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() == subRefundReturn.getApplyState()) {
			request.setAttribute("status", PENDING);
		} else {
			request.setAttribute("status", ALL);
		}
	
		List<SubRefundReturnDetail> subRefundReturnDetailList = subRefundReturnDetailService.getSubRefundReturnDetailByRefundId(subRefundReturn.getRefundId()); 
		request.setAttribute("subRefundReturnDetailList", subRefundReturnDetailList);
		request.setAttribute("subRefundReturn", subRefundReturn);
		return PathResolver.getPath(AdminPage.RETURN_DETAIL);
	}

	
	/**
	 * 审核退货
	 * 
	 * @param curPageNO
	 * @param paramData
	 *            查询参数
	 * @return
	 */
	@SystemControllerLog(description="审核退货")
	@RequestMapping("/audit")
	public String audit(HttpServletRequest request, HttpServletResponse response, Long refundId, String adminMessage) {
		if (null == refundId || AppUtils.isBlank(adminMessage)) {
			throw new NullPointerException("refundId or adminMessage isNull");
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		if (RefundReturnTypeEnum.REFUND_RETURN.value() != subRefundReturn.getApplyType()
				|| RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() != subRefundReturn.getApplyState()) {
			throw new BusinessException("Illegal Operate");
		}
		subRefundReturnService.adminAuditReturn(subRefundReturn, adminMessage);
		// 后台审核退货 退款给用户 , 订单项退款状态更新为已完成 ,检查当前订单项是否是订单中最后一个在退款中的订单项
		// 是则也把订单的状态以及订单的退款状态更新为已完成
		return PathResolver.getRedirectPath("/admin/returnGoods/query/pending");
	}
	

}
