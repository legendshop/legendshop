/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductPropertyTypeEnum;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.ParamGroup;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdTypeBrand;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.model.entity.ProdTypeProperty;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ParamGroupService;
import com.legendshop.spi.service.ProdTypeParamService;
import com.legendshop.spi.service.ProdTypePropertyService;
import com.legendshop.spi.service.ProdTypeService;
import com.legendshop.spi.service.ProductPropertyService;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品类型控制器
 *
 */
@Controller
@RequestMapping("/admin/prodType")
public class ProdTypeController extends BaseController {

	@Autowired
	private ProdTypeService prodTypeService;

	@Autowired
	private ProductPropertyService productPropertyService;

	@Autowired
	private ProductPropertyValueService productPropertyValueService;

	@Autowired
	private BrandService brandService;

	@Autowired
	private ProdTypeParamService prodTypeParamService;

	@Autowired
	private ParamGroupService paramGroupService;

	@Autowired
	private ProdTypePropertyService prodTypePropertyService;
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private CategoryService categoryService;

	/**
	 * 查找商品类型列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param prodType
	 * @param categoryId
	 * @param categoryName
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProdType prodType,
			Long categoryId, String categoryName) {
		PageSupport<ProdType> ps = prodTypeService.queryProdTypePage(curPageNO, prodType, categoryId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodType", prodType);
		request.setAttribute("categoryId", categoryId);
		request.setAttribute("categoryName", categoryName);
		return PathResolver.getPath(AdminPage.PRODTYPE_LIST_PAGE);
	}

	/**
	 * 保存商品类型
	 * 
	 * @param request
	 * @param response
	 * @param prodType
	 * @return
	 */
	@SystemControllerLog(description="保存商品类型")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ProdType prodType) {
		prodType.setRecDate(new Date());
		prodType.setStatus(1);
		prodTypeService.saveProdType(prodType);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PRODTYPE_LIST_QUERY);
	}

	/**
	 * 删除商品类型列表
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除商品类型列表")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProdType prodType = prodTypeService.getProdType(id);
		prodTypeService.deleteProdType(prodType);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));

		return PathResolver.getPath(RedirectPage.PRODTYPE_LIST_QUERY);
	}

	/**
	 * 加载商品类型
	 * 
	 * @param request
	 * @param response
	 * @param proTypeId
	 * @return
	 */
	@RequestMapping(value = "/load/{proTypeId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long proTypeId) {
		ProdType prodType = prodTypeService.getProdType(proTypeId);
		request.setAttribute("prodType", prodType);

		// 相关联的规格
		List<ProductProperty> propertyList = productPropertyService.getProductPropertyList(proTypeId);
		if (AppUtils.isNotBlank(propertyList)) {
			List<ProductPropertyValue> valueList = productPropertyValueService.getAllProductPropertyValue(propertyList);
			for (ProductProperty property : propertyList) {
				for (ProductPropertyValue propertyValue : valueList) {
					property.addProductPropertyValueList(propertyValue);
				}
			}
		}
		request.setAttribute("propertyList", propertyList);

		// 相关联的品牌
		List<Brand> brandList = brandService.getBrandList(proTypeId);
		request.setAttribute("brandList", brandList);

		// 相关联的参数属性
		List<ProductProperty> parameterPropertyList = productPropertyService.getParameterPropertyList(proTypeId);
		if (AppUtils.isNotBlank(parameterPropertyList)) {
			List<ProductPropertyValue> parameterValueList = productPropertyValueService.getAllProductPropertyValue(parameterPropertyList);
			for (ProductProperty parameterProperty : parameterPropertyList) {
				for (ProductPropertyValue parameterPropertyValue : parameterValueList) {
					parameterProperty.addProductPropertyValueList(parameterPropertyValue);
				}
			}
		}
		request.setAttribute("parameterPropertyList", parameterPropertyList);

		return PathResolver.getPath(AdminPage.PRODTYPE_EDIT_PAGE);
	}

	/**
	 * 跳转类型管理创建页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PRODTYPE_PAGE);
	}

	/**
	 * 更新商品类型
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProdType prodType = prodTypeService.getProdType(id);
		request.setAttribute("prodType", prodType);

		return PathResolver.getPath(BackPage.PRODTYPE_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新商品类型状态")
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long id, @PathVariable Integer status) {
		ProdType prodType = prodTypeService.getProdType(id);
		if (prodType == null) {
			return -1;
		}
		if (!status.equals(prodType.getStatus())) {
			prodType.setStatus(status);
			prodTypeService.updateProdType(prodType);
		}
		return prodType.getStatus();
	}

	/**
	 * 选择规制的弹框页面
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productProperty
	 * @return
	 */
	@RequestMapping("/propertyList/{proTypeId}")
	public String propertyList(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String propName, String memo, @PathVariable Long proTypeId) {
		PageSupport ps = retrieveProdTypeProperty(curPageNO, propName, memo, ProductPropertyTypeEnum.SALE_ATTR.value(),
				proTypeId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("propName", propName);
		request.setAttribute("memo", memo);
		request.setAttribute("proTypeId", proTypeId);
		return PathResolver.getPath(BackPage.PROPERTY_LIST_PAGE);
	}

	/**
	 * 找出没有选中的品牌
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param brandName
	 * @param proTypeId
	 * @return
	 */
	@RequestMapping("/brandList/{proTypeId}")
	public String brandList(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String brandName, @PathVariable Long proTypeId) {

		PageSupport<Brand> ps = brandService.getBrand(curPageNO, brandName, proTypeId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("brandName", brandName);
		request.setAttribute("proTypeId", proTypeId);

		return PathResolver.getPath(BackPage.BRAND_LIST);
	}

	/**
	 * 保存商品列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param propertyIds
	 * @param typeId
	 * @return
	 */
	@SystemControllerLog(description="保存商品列表")
	@RequestMapping("/savePropertyList")
	public @ResponseBody String savePropertyList(HttpServletRequest request, HttpServletResponse response,
			String curPageNO, String propertyIds, Long typeId) {
		List<ProdTypeProperty> propertyList = JSONUtil.getArray(propertyIds, ProdTypeProperty.class);
		prodTypeService.saveTypeProp(propertyList, typeId);
		return Constants.SUCCESS;
	}

	/**
	 * 保存品牌列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param brandIds
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="保存品牌列表")
	@RequestMapping("/saveBrandList")
	public @ResponseBody String saveBrandList(HttpServletRequest request, HttpServletResponse response,
			String curPageNO, String brandIds, Long id) {
		List<ProdTypeBrand> brandList = JSONUtil.getArray(brandIds, ProdTypeBrand.class);
		prodTypeService.saveTypeBrand(brandList, id);
		return Constants.SUCCESS;
	}

	/**
	 * 删除商品品牌关联信息
	 * 
	 * @param request
	 * @param response
	 * @param propId
	 * @param prodTypeId
	 * @return
	 */
	@SystemControllerLog(description="删除商品品牌关联信息")
	@RequestMapping(value = "/deleteProdProp")
	public @ResponseBody String deleteProdProp(HttpServletRequest request, HttpServletResponse response, Long propId, Long prodTypeId) {
		if (AppUtils.isNotBlank(propId) && AppUtils.isNotBlank(prodTypeId)) {
			List<Sku> list =  skuService.findSkuByPropIdAndProdTypeId(propId,prodTypeId);
			if(AppUtils.isBlank(list)){
				prodTypeService.deleteProdProp(propId, prodTypeId);
				return Constants.SUCCESS;
			}else {
				return Constants.FAIL;
			}
		} else {
			return Constants.FAIL;
		}
	}
	

	/**
	 * 删除商品品牌
	 * 
	 * @param request
	 * @param response
	 * @param brandId
	 * @param prodTypeId
	 * @return
	 */
	@SystemControllerLog(description="删除商品品牌")
	@RequestMapping(value = "/deleteBrand")
	public @ResponseBody String deleteBrand(HttpServletRequest request, HttpServletResponse response, Long brandId,
			Long prodTypeId) {
		if (AppUtils.isNotBlank(brandId) && AppUtils.isNotBlank(prodTypeId)) {
			prodTypeService.deleteBrand(brandId, prodTypeId);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 参数属性选框
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productProperty
	 * @param proTypeId
	 * @return
	 */
	@RequestMapping("/parameterProperty/{proTypeId}")
	public String parameterProperty(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String propName, String memo, @PathVariable Long proTypeId) {
		PageSupport ps = retrieveProdTypeParam(curPageNO, propName, memo, ProductPropertyTypeEnum.PARAM_ATTR.value(),
				proTypeId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("propName", propName);
		request.setAttribute("memo", memo);
		request.setAttribute("proTypeId", proTypeId);
		return PathResolver.getPath(BackPage.PARAMETER_PROPERTY_PAGE);
	}

	/**
	 * 添加参数属性
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param propertyIds
	 * @param id
	 * @return
	 */
	@RequestMapping("/saveParameterProperty")
	public @ResponseBody String saveParameterProperty(HttpServletRequest request, HttpServletResponse response,
			String curPageNO, String propertyIds, Long id) {
		List<ProdTypeParam> propertyList = JSONUtil.getArray(propertyIds, ProdTypeParam.class);
		prodTypeService.saveParameterProperty(propertyList, id);
		return Constants.SUCCESS;
	}

	/**
	 * 删除参数属性
	 * 
	 * @param request
	 * @param response
	 * @param propId
	 * @param prodTypeId
	 * @return
	 */
	@RequestMapping(value = "/deleteParameterProp")
	public @ResponseBody String deleteParameterProp(HttpServletRequest request, HttpServletResponse response,
			Long propId, Long prodTypeId) {
		if (AppUtils.isNotBlank(propId) && AppUtils.isNotBlank(prodTypeId)) {
			prodTypeService.deleteParameterProp(propId, prodTypeId);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}

	}

	/**
	 * 编辑参数 顺序和 分组 页面
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param propName
	 * @param propId
	 * @param prodTypeId
	 * @return
	 */
	@RequestMapping("/editParameterProp/{propId}/{prodTypeId}")
	public String editParameterProp(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable Long propId, @PathVariable Long prodTypeId) {
		ProdTypeParam prodTypeParam = prodTypeParamService.getProdTypeParam(propId, prodTypeId);
		ParamGroup paramGroup = paramGroupService.getParamGroup(prodTypeParam.getGroupId());
		List<ParamGroup> paramGroupList = paramGroupService.getParamGroupList(prodTypeParam.getGroupId());
		request.setAttribute("prodTypeParam", prodTypeParam);
		request.setAttribute("paramGroup", paramGroup);
		request.setAttribute("paramGroupList", paramGroupList);
		return PathResolver.getPath(BackPage.PARAM_PROP_EDIT_PAGE);
	}

	/**
	 * 查询参数 组别
	 * 
	 * @param request
	 * @param response
	 * @param groupName
	 * @return
	 */
	@RequestMapping(value = "/getParamGroup")
	public String getParamGroup(HttpServletRequest request, HttpServletResponse response, String groupName) {
		PageSupport<ParamGroup> ps = paramGroupService.getParamGroupPage(groupName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.TYPE_PARAM_GROUP_EDIT_PAGE);
	}

	/**
	 * 保存参数属性
	 * 
	 * @param request
	 * @param response
	 * @param typeParamId
	 * @param paramSeq
	 * @param groupId
	 * @return
	 */
	@RequestMapping(value = "/saveProdTypeParam")
	@ResponseBody
	public String saveProdTypeParam(HttpServletRequest request, HttpServletResponse response, Long typeParamId,
			Integer paramSeq, Long groupId) {
		ProdTypeParam prodTypeParam = prodTypeParamService.getProdTypeParam(typeParamId);
		prodTypeParam.setGroupId(groupId);
		prodTypeParam.setSeq(paramSeq);
		prodTypeParamService.saveProdTypeParam(prodTypeParam);
		return Constants.SUCCESS;
	}

	/**
	 * 保存商品品牌关联信息
	 * 
	 * @param request
	 * @param response
	 * @param propId
	 * @param typeId
	 * @param propSeq
	 * @return
	 */
	@SystemControllerLog(description="保存商品品牌关联信息")
	@RequestMapping(value = "/saveProdTypeProp")
	@ResponseBody
	public String saveProdTypeProp(HttpServletRequest request, HttpServletResponse response, Long propId, Long typeId,
			Integer propSeq) {
		ProdTypeProperty prodTypeProperty = prodTypePropertyService.getProdTypeProperty(propId, typeId);
		prodTypeProperty.setSeq(propSeq);
		prodTypePropertyService.updateProdTypeProperty(prodTypeProperty);
		return Constants.SUCCESS;
	}

	/**
	 * 类型选择框
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/loadProdType")
	public @ResponseBody List<KeyValueEntity> loadProdTypeEntity(HttpServletRequest request,
			HttpServletResponse response) {
		return prodTypeService.loadProdType();
	}

	/**
	 * 覆盖商品类型
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param prodType
	 * @param sortId
	 * @return
	 */
	@RequestMapping("/prodTypeOverlay/{sortId}")
	public String prodTypeOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProdType prodType, @PathVariable Long sortId) {
		PageSupport<ProdType> ps = prodTypeService.getProdTypePage(curPageNO, prodType.getName());
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodType", prodType);
		request.setAttribute("sortId", sortId);
		return PathResolver.getPath(BackPage.PRODTYPE_LIST_OVERLAY);
	}
	
	
	/**
	 * 判断是否可以删除规格
	 * @param propId
	 * @return
	 */
	@RequestMapping(value="/ifDeleteProductProperty")
	@ResponseBody
	public Boolean ifDeleteProductProperty(Long prodTypeId, Long propId){	
		Boolean isHas = true;
		List<Long> skuId = prodTypeService.ifDeleteProductProperty(prodTypeId, propId);
		if(AppUtils.isNotBlank(skuId)) {
			isHas = false;
		}
		return isHas;
	}
	

	/**
	 * 更新 sort TypeId
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param prodTypeId
	 * @param id
	 * @return
	 */
	// @RequestMapping("/saveProdTypeOverlay")
	// public @ResponseBody String saveprodTypeOverlay(HttpServletRequest
	// request, HttpServletResponse response, String curPageNO, Long
	// prodTypeId,Long id) {
	// sortService.updateProdTypeId(prodTypeId,id);
	// return Constants.SUCCESS;
	// }

	/**
	 * 找出没有选中的产品销售属性
	 * 
	 * @param curPageNO
	 * @param propName
	 * @param isRuleAttributes
	 * @param proTypeId
	 * @return
	 */
	private PageSupport retrieveProdTypeProperty(String curPageNO, String propName, String memo,
			Integer isRuleAttributes, Long proTypeId) {
		PageSupport<ProductProperty> ps = productPropertyService.getProductPropertyPage(curPageNO, isRuleAttributes,
				propName, memo, proTypeId);
		return ps;
	}

	/**
	 * 找出没有选中的产品参数
	 * 
	 * @param curPageNO
	 * @param propName
	 * @param isRuleAttributes
	 * @param proTypeId
	 * @return
	 */
	private PageSupport retrieveProdTypeParam(String curPageNO, String propName, String memo, Integer isRuleAttributes,
			Long proTypeId) {
		PageSupport<ProductProperty> ps = productPropertyService.getProductPropertyParamPage(curPageNO,
				isRuleAttributes, propName, memo, proTypeId);
		return ps;
	}


}
