/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.LoginHistorySum;
import com.legendshop.model.entity.LoginHistory;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.LoginHistoryService;

/**
 * 登录历史
 */
@Controller
@RequestMapping("/admin/loginHistory")
public class LoginHistoryController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(LoginHistoryController.class);

	/** The login history service. */
	@Autowired
	private LoginHistoryService loginHistoryService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 查询登录历史.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param login
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, LoginHistory login) {
		PageSupport<LoginHistory> ps = loginHistoryService.getLoginHistoryPage(curPageNO, login);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("login", login);
		adminMenuUtil.parseMenu(request, 4); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.LOGIN_HIST_LIST_PAGE);
	}

	/**
	 * 加载用户登录历史.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.LOGIN_HIST_LIST_PAGE);
	}

	/**
	 * 登录历史表.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param login
	 * @return the string
	 */
	@RequestMapping("/sum")
	public String loginHistorySum(HttpServletRequest request, HttpServletResponse response, String curPageNO, LoginHistory login) {
		PageSupport<LoginHistorySum> ps = loginHistoryService.getLoginHistoryBySQL(curPageNO, login);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("login", login);
		return PathResolver.getPath(AdminPage.LOGIN_HIST_SUM_PAGE);

	}

}
