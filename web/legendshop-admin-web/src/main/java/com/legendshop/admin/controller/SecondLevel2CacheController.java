/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.CacheManageAdminPage.CacheManageAdminPage;
import com.legendshop.admin.page.CacheManageAdminPage.CacheManageFowardPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.framework.cache.CacheKeyValueEntity;
import com.legendshop.framework.cache.EntityCache;
import com.legendshop.util.AppUtils;

/**
 * 系统缓存控制器.
 */
@Controller
@RequestMapping("/admin/system/cache2")
public class SecondLevel2CacheController extends BaseController {

	/** 系统缓存管理. */
	@Autowired(required = false)
	private CacheManager cacheManager;

	/**
	 * 查询系统缓存列表.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response) {
		List<CacheKeyValueEntity> cacheList = parseMemcache(cacheManager);
		request.setAttribute("cacheList", cacheList);
		String path = PathResolver.getPath(CacheManageAdminPage.CACHE_LIST_PAGE);
		return path;
	}

	/**
	 * 删除系统缓存.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/clear")
	public String clear(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		return PathResolver.getPath(CacheManageFowardPage.PARAM_LIST_QUERY);
	}

	/**
	 * 清空缓存列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/removeall")
	public @ResponseBody String clearSecondLevelCache() {
		try {
			Collection<String> cacheNames = cacheManager.getCacheNames();
			for (String cacheName : cacheNames) {
				Cache cache = cacheManager.getCache(cacheName);
				if (cache != null) {
					System.out.println("clear cache " + cache.getName());
					cache.clear();
				}
			}
			return null;
		} catch (Exception e) {
			return "fail";
		}

	}

	/**
	 * 处理系统缓存.
	 * 
	 * @param cacheManager
	 *            the cache manager
	 * @return the list
	 */
	private List<CacheKeyValueEntity> parseMemcache(CacheManager cacheManager) {
		List<CacheKeyValueEntity> result = new ArrayList<CacheKeyValueEntity>();
		Collection<String> cacheNames = cacheManager.getCacheNames();
		if (AppUtils.isNotBlank(cacheNames)) {
			for (String cacheName : cacheNames) {

				EntityCache cache = (EntityCache) cacheManager.getCache(cacheName);
				if (cache != null) {
					Collection<CacheKeyValueEntity> entities = cache.getEntity(cacheName);
					if (entities != null) {
						result.addAll(entities);
					}
				}
			}
		}
		return result;
	}
	// /** TODO 需要审查
	// * Parses the ehcache.
	// *
	// * @param cacheManager
	// * the cache manager
	// * @return the list
	// */
	// private List<CacheKeyValueEntity> parseEhcache(CacheManager cacheManager)
	// {
	// List<CacheKeyValueEntity> result = new ArrayList<CacheKeyValueEntity>();
	// Collection<String> cacheNames = cacheManager.getCacheNames();
	// if (AppUtils.isNotBlank(cacheNames)) {
	// for (String cacheName : cacheNames) {
	// CacheKeyValueEntity entity = new CacheKeyValueEntity();
	// entity.setKey(cacheName);
	// Ehcache cache = ((LegendCache)
	// cacheManager.getCache(cacheName)).getNativeCache();
	// List<Serializable> keys = cache.getKeys();
	// if (AppUtils.isNotBlank(keys)) {
	// StringBuilder sb = new StringBuilder(keys.get(0).toString());
	// for (int i = 1; i < keys.size(); i++) {
	// sb.append(",").append(keys.get(i));
	// }
	// entity.setValue(sb.toString());
	// }
	//
	// result.add(entity);
	// }
	// }
	// return result;
	// }
	//

}
