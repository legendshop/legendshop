/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.FloorStatusEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.entity.Advertisement;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Product;
import com.legendshop.model.floor.NewsFloor;
import com.legendshop.model.floor.ProductFloor;
import com.legendshop.model.floor.SortFloor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AdvertisementService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.FloorItemService;
import com.legendshop.spi.service.FloorService;
import com.legendshop.spi.service.FloorSubItemService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 楼层分类
 * 
 */
@Controller
@RequestMapping("/admin/floorItem")
public class FloorItemController extends BaseController {
	@Autowired
	private FloorItemService floorItemService;

	@Autowired
	private FloorSubItemService floorSubItemService;

	@Autowired
	private FloorService floorService;

	@Autowired
	private AdvertisementService advertisementService;

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 加载楼层分类
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("/loadCategory/{categoryId}")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable Long categoryId) {

		Category category = categoryService.getCategory(categoryId);
		if (AppUtils.isNotBlank(category)) {
			List<Category> categories = categoryService.getAllChildrenByFunction(categoryId, null, ProductTypeEnum.PRODUCT.value(), 1);
			request.setAttribute("categories", categories);
			request.setAttribute("category", category);
		}
		String path = PathResolver.getPath(BackPage.LOAD_FLOOR_NSORT);
		return path;
	}

	@RequestMapping(value = "/sortFloorOverlay/{flId}")
	public String loadSortFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, String layout) {
		Floor floor = floorService.getFloor(flId);
		List<FloorItem> floorItems = floorItemService.getAllSortFloorItem(flId);
		for (FloorItem fI : floorItems) {
			List<FloorSubItem> floorSubItemList = floorSubItemService.getAllFloorSubItem(fI.getFiId());
			for (FloorSubItem fSI : floorSubItemList) {
				fI.addFloorSubItem(fSI);
			}
		}
		for (FloorItem floorItem : floorItems) {
			floor.addFloorItems(floorItem);
		}
		request.setAttribute("layout", layout);
		request.setAttribute("floor", floor);
		return PathResolver.getPath(BackPage.SORT_FLOOR_OVERLAY);
	}

	/**
	 * 保存楼层分类信息
	 * 
	 * @param request
	 * @param response
	 * @param sortFloors
	 * @param flId
	 * @param layout
	 * @return
	 */
	@RequestMapping(value = "/saveSortFloor")
	public @ResponseBody String saveSortFloor(HttpServletRequest request, HttpServletResponse response, String sortFloors, Long flId, String layout) {
		if (AppUtils.isBlank(sortFloors)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		boolean isUserSelf = floorService.checkPrivilege(flId);
		if (!isUserSelf) {
			return FloorStatusEnum.PERMISSION.value();
		}
		List<SortFloor> sortFloorList = JSONUtil.getArray(sortFloors, SortFloor.class);
		floorItemService.saveFloorItem(sortFloorList, flId, layout);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 品牌楼层覆盖
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @param layout
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/brandFloorOverlay/{flId}")
	public String brandFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, String layout, int limit) {
		List<FloorItem> floorItemList = floorItemService.getBrandList(flId, layout, limit);
		request.setAttribute("floorItemList", floorItemList);
		request.setAttribute("flId", flId);
		request.setAttribute("layout", layout);
		request.setAttribute("limit", limit);
		return PathResolver.getPath(BackPage.BRAND_FLOOR_OVERLAY);
	}

	/**
	 * 品牌楼层加载
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param brandName
	 * @return
	 */
	@RequestMapping(value = "/brandFloorLoad")
	public String brandFloorLoad(HttpServletRequest request, HttpServletResponse response, String curPageNO, String brandName) {
		PageSupport<Brand> ps = floorItemService.getBrandListPage(curPageNO, brandName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.BRAND_FLOOR_LOAD);
	}

	/**
	 * 保存楼层品牌
	 * 
	 * @param request
	 * @param response
	 * @param BrandFloors
	 * @param flId
	 * @param layout
	 * @return
	 */
	@RequestMapping(value = "/saveBrandFloor")
	public @ResponseBody String saveBrandFloor(HttpServletRequest request, HttpServletResponse response, String BrandFloors, Long flId, String layout) {
		if (AppUtils.isBlank(BrandFloors)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		boolean isUserSelf = floorService.checkPrivilege(flId);
		if (!isUserSelf) {
			return FloorStatusEnum.PERMISSION.value();
		}

		List<ProductFloor> BrandFloorList = JSONUtil.getArray(BrandFloors, ProductFloor.class);
		floorItemService.saveBrands(BrandFloorList, flId, layout);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 加载品牌楼层编辑页
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/rankFloorOverlay/{flId}")
	public String loadRankFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId) {
		List<FloorItem> rankList = floorItemService.getRankList(flId);
		request.setAttribute("rankList", rankList);
		request.setAttribute("flId", flId);
		return PathResolver.getPath(BackPage.RANK_FLOOR_OVERLAY);
	}

	/**
	 * 保存等级楼层
	 * 
	 * @param request
	 * @param response
	 * @param rankFloors
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/saveRankFloor")
	public @ResponseBody String saveRankFloor(HttpServletRequest request, HttpServletResponse response, String rankFloors, Long flId) {
		if (AppUtils.isBlank(rankFloors)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		boolean isUserSelf = floorService.checkPrivilege(flId);
		if (!isUserSelf) {
			return FloorStatusEnum.PERMISSION.value();
		}
		List<ProductFloor> rankFloorList = JSONUtil.getArray(rankFloors, ProductFloor.class);
		floorItemService.saveRanks(rankFloorList, flId);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 团购楼层覆盖
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/groupFloorOverlay/{flId}")
	public String groupFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId) {
		FloorItem group = floorItemService.getFloorGroup(flId);
		request.setAttribute("group", group);
		request.setAttribute("flId", flId);
		return PathResolver.getPath(BackPage.GROUP_FLOOR_OVERLAY);
	}

	/**
	 * 团购楼层加载
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param groupName
	 * @return
	 */
	@RequestMapping(value = "/groupFloorLoad")
	public String groupFloorLoad(HttpServletRequest request, HttpServletResponse response, String curPageNO, String groupName) {
		PageSupport<Product> ps = floorItemService.getGroupListPage(curPageNO, groupName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.GROUP_FLOOR_LOAD);
	}

	/**
	 * 保存团购楼层
	 * 
	 * @param request
	 * @param response
	 * @param groupFloors
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/saveGroupFloor")
	public @ResponseBody String saveGroupFloor(HttpServletRequest request, HttpServletResponse response, String groupFloors, Long flId) {
		if (AppUtils.isBlank(groupFloors)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		// check Privilege
		boolean isUserSelf = floorService.checkPrivilege(flId);
		if (!isUserSelf) {
			return FloorStatusEnum.PERMISSION.value();
		}
		List<ProductFloor> groupFloorList = JSONUtil.getArray(groupFloors, ProductFloor.class);
		floorItemService.saveGroups(groupFloorList, flId);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 楼层编辑中的 底部广告位
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/bottomAdvOverlay/{flId}")
	public String bottomAdvOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, String layout) {
		FloorItem floorAdv = floorItemService.getAdvFloorItem(flId, FloorTemplateEnum.BOTTOM_ADV.value(), layout);
		request.setAttribute("floorAdv", floorAdv);
		request.setAttribute("floorId", flId);
		request.setAttribute("type", FloorTemplateEnum.BOTTOM_ADV.value());
		return PathResolver.getPath(BackPage.ADV_FLOOR_OVERLAY);
	}

	/**
	 * 楼层编辑中的 左上方广告位
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/leftAdvOverlay/{flId}")
	public String leftAdvOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, String layout) {
		FloorItem floorAdv = floorItemService.getAdvFloorItem(flId, FloorTemplateEnum.LEFT_ADV.value(), layout);
		request.setAttribute("floorAdv", floorAdv);
		request.setAttribute("floorId", flId);
		request.setAttribute("type", FloorTemplateEnum.LEFT_ADV.value());
		request.setAttribute("layout", layout);
		return PathResolver.getPath(BackPage.ADV_FLOOR_OVERLAY);
	}

	/**
	 * 楼层编辑中的 广告列表中 新增广告
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/advFloorOverlay/{flId}")
	public String RightAdvOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, String layout) {
		request.setAttribute("floorId", flId);
		request.setAttribute("type", FloorTemplateEnum.RIGHT_ADV.value());
		request.setAttribute("layout", layout);
		return PathResolver.getPath(BackPage.ADV_FLOOR_OVERLAY);
	}

	/**
	 * 楼层编辑中的 广告列表中 修改广告
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @param fiId
	 * @return
	 */
	@RequestMapping(value = "/advFloorOverlay/{flId}/{fiId}")
	public String advFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, @PathVariable Long fiId, String layout) {
		FloorItem floorAdv = floorItemService.getAdvFloorItem(flId, fiId, FloorTemplateEnum.RIGHT_ADV.value());
		request.setAttribute("floorAdv", floorAdv);
		request.setAttribute("floorId", flId);
		request.setAttribute("type", FloorTemplateEnum.RIGHT_ADV.value());
		request.setAttribute("layout", layout);
		return PathResolver.getPath(BackPage.ADV_FLOOR_OVERLAY);
	}

	/**
	 * 楼层编辑中的 右下方广告列表
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @param curPageNO
	 * @param title
	 * @return
	 */
	@RequestMapping(value = "/advListOverlay/{flId}")
	public String advFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId, Integer limit, String curPageNO,
			String title, String layout) {
		PageSupport<FloorItem> ps = floorItemService.queryAdvFloorItemPage(curPageNO, flId, layout, title);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("floorId", flId);
		request.setAttribute("title", title);
		request.setAttribute("limit", limit);
		request.setAttribute("layout", layout);
		return PathResolver.getPath(BackPage.ADV_LIST_OVERLAY);
	}

	/**
	 * 文章楼层覆盖
	 * 
	 * @param request
	 * @param response
	 * @param flId
	 * @return
	 */
	@RequestMapping(value = "/newsFloorOverlay/{flId}")
	public String newsFloorOverlay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long flId) {
		List<FloorItem> floorItemList = floorItemService.getNewsList(flId);
		request.setAttribute("floorItemList", floorItemList);
		request.setAttribute("flId", flId);
		return PathResolver.getPath(BackPage.NEWS_FLOOR_OVERLAY);
	}

	/**
	 * 装载首页楼层文章
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param newsTitle
	 * @return
	 */
	@RequestMapping(value = "/newsFloorLoad")
	public String newsFloorLoad(HttpServletRequest request, HttpServletResponse response, String curPageNO, Long newsCategoryId, String newsTitle) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		PageSupport<News> ps = floorItemService.getNewsListPage(curPageNO, newsCategoryId, newsTitle, userName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.NEWS_FLOOR_LOAD);
	}

	/**
	 * 保存文章楼层
	 * 
	 * @param request
	 * @param response
	 * @param productFloors
	 * @param sfId
	 * @return
	 */
	@RequestMapping(value = "/saveNewsFloor")
	public @ResponseBody String saveNewsFloor(HttpServletRequest request, HttpServletResponse response, String productFloors, Long floorId) {
		if (AppUtils.isBlank(productFloors)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		// check Privilege
		boolean isUserSelf = floorService.checkPrivilege(floorId);
		if (!isUserSelf) {
			return FloorStatusEnum.PERMISSION.value();
		}
		List<NewsFloor> newsFloorList = JSONUtil.getArray(productFloors, NewsFloor.class);
		floorItemService.saveNews(newsFloorList, floorId);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 保存文章楼层
	 * 
	 * @param request
	 * @param response
	 * @param fiId
	 * @return
	 */
	@RequestMapping(value = "/delete/{fiId}")
	public @ResponseBody String saveNewsFloor(HttpServletRequest request, HttpServletResponse response, @PathVariable Long fiId) {
		if (AppUtils.isBlank(fiId)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		floorItemService.deleteFloorItem(fiId);
		return FloorStatusEnum.SUCCESS.value();
	}

	/**
	 * 删除广告条目
	 * 
	 * @param request
	 * @param response
	 * @param fiId
	 * @return
	 */
	@RequestMapping(value = "/deleteAdvtItem/{fiId}/{advId}")
	public @ResponseBody String deleteAdvtItem(HttpServletRequest request, HttpServletResponse response, @PathVariable Long fiId, @PathVariable Long advId) {
		if (AppUtils.isBlank(fiId)) {
			return FloorStatusEnum.NO_NULLABLE.value();
		}
		return floorItemService.deleteAdvtItem(fiId, advId);
	}

	/**
	 * 保存广告信息
	 * 
	 * @param request
	 * @param response
	 * @param advertisement
	 * @return
	 */
	@RequestMapping(value = "/saveAdvMessage")
	public @ResponseBody Object saveAdvMessage(HttpServletRequest request, HttpServletResponse response, Advertisement advertisement) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		// 判断是否update or save;
		if (AppUtils.isNotBlank(advertisement.getId())) { // update
			Advertisement orin=advertisementService.getAdvertisement(advertisement.getId());
			if(orin==null){
				return "没有找到操作对象"; 
			}
			String originPic = orin.getPicUrl();
			String picUrl=orin.getPicUrl();
			if(AppUtils.isNotBlank(advertisement.getFile()) && advertisement.getFile().getSize()>0){
				 //说明重新修改图片文件
				picUrl=attachmentManager.upload(advertisement.getFile());
				if(AppUtils.isBlank(picUrl)){
					return "上传文件失败";
				}
			}
			return advertisementService.updateAdvt(advertisement,orin,originPic,picUrl,user.getUsername(), user.getUserId(), user.getShopId());
		} else {
			// save Advt flooritem<one --- one> advt
			//upload file 
			String pic  = attachmentManager.upload(advertisement.getFile());
			if(AppUtils.isBlank(pic)){
				return "上传文件失败";
			}
			return advertisementService.saveAdvt(advertisement,pic,user.getUsername(), user.getUserId(), user.getShopId());
		}

	}

}
