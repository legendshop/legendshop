/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.model.entity.AccusationType;
import com.legendshop.spi.service.AccusationSubjectService;
import com.legendshop.spi.service.AccusationTypeService;
import com.legendshop.util.AppUtils;

/**
 * 举报类别控制器
 *
 */
@Controller
@RequestMapping("/admin/accusationType")
public class AccusationTypeController extends BaseController {
    @Autowired
    private AccusationTypeService accusationTypeService;
    
    @Autowired
    private AccusationSubjectService accusationSubjectService;
    
    /**
     * 查询
     * @param request
     * @param response
     * @param curPageNO
     * @param accusationType
     * @return
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, AccusationType accusationType) {
        PageSupport<AccusationType> ps = accusationTypeService.getAccusationType(curPageNO, accusationType);

        PageSupportHelper.savePage(request, ps);
        request.setAttribute("accusationType", accusationType);
        return PathResolver.getPath(AdminPage.ACCUSATIONTYPE_LIST_PAGE);
    }
    
    /**
     * 保存举报类型
     * @param request
     * @param response
     * @param accusationType
     * @return
     */
    @SystemControllerLog(description="保存举报类型")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, AccusationType accusationType) {
    	accusationType.setRecDate(new Date());
    	accusationType.setStatus(0L);//保存默认为下线，需要手动上线
        accusationTypeService.saveAccusationType(accusationType);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.ACCUSATIONTYPE_LIST_QUERY);
    }
    
    /**
     * 删除举报类型
     * @param request
     * @param response
     * @param id
     * @return
     */
    @SystemControllerLog(description="删除举报类型")
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        AccusationType accusationType = accusationTypeService.getAccusationType(id);
		accusationTypeService.deleteAccusationType(accusationType);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(RedirectPage.ACCUSATIONTYPE_LIST_QUERY);
    }
    
    /**
     * 根据Id加载
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        AccusationType accusationType = accusationTypeService.getAccusationType(id);
        request.setAttribute("accusationType", accusationType);
        return PathResolver.getPath(AdminPage.ACCUSATIONTYPE_EDIT_PAGE);
    }
    
    /**
     * 举报类型管理编辑
     * @param request
     * @param response
     * @return
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.ACCUSATIONTYPE_EDIT_PAGE);
	}
	
	/**
	 * 更新举报类型状态（上下线）
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新举报类型状态")
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Long updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
			@PathVariable Long status) {
		AccusationType accusationType = accusationTypeService.getAccusationType(id);
		if (accusationType == null) {
			return (long) -1;
		}
		
		if (status == 1) {//上线时判断，该类型是否有关联至少一个主题
			List<AccusationSubject> subjectList = accusationTypeService.getSubjectByTypeId(id);
			if (AppUtils.isBlank(subjectList)) {
				return (long) -2;
			}
		}
		
		if (!status.equals(accusationType.getStatus())) {
			accusationType.setStatus(status);
			accusationTypeService.saveAccusationType(accusationType);
		}
		return accusationType.getStatus();
	}
}
