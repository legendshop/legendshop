/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.spi.service.VisitLogService;

/**
 * 访问历史控制器.
 */
@Controller
@RequestMapping("/admin/visitLog")
public class VisitLogAdminController extends BaseController {

	public static String LIST_PAGE = "/visitLog/visitLogList";

	@Autowired
	private VisitLogService visitLogService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查询访问历史页面
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param visitLog
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, VisitLog visitLog) {
		request.setAttribute("bean", visitLog);
		return PathResolver.getPath(AdminPage.VLOG_LIST_PAGE);
	}
	
	/**
	 * 查询访问历史列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param visitLog
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, VisitLog visitLog) {
		
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "userName", "ip", "country", "area", "shopName" });
		
		PageSupport<VisitLog> ps = visitLogService.getVisitLogListPage(curPageNO, visitLog, result, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", visitLog);
		return PathResolver.getPath(AdminPage.VLOG_CONTENT_LIST_PAGE);
	}

	/**
	 * 保存访问历史
	 * 
	 * @param request
	 * @param response
	 * @param visitLog
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, VisitLog visitLog) {
		visitLogService.save(visitLog);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.VLOG_LIST_QUERY);
	}

	/**
	 * 删除访问历史
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		visitLogService.delete(id);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.VLOG_LIST_QUERY);
	}

}
