/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassLabel;
import com.legendshop.spi.service.GrassLabelService;

/**
 * 种草社区标签表控制器
 */
@Controller
@RequestMapping("/admin/grassLabel")
public class GrassLabelController extends BaseController{

	/** 日志. */
	private final Logger log = LoggerFactory.getLogger(GrassLabel.class);
	
    @Autowired
    private GrassLabelService grassLabelService;

	/**
	 * 种草社区标签表列表查询
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,String name) {
    	PageSupport<GrassLabel> ps = grassLabelService.queryGrassLabelPage(curPageNO, name, 20);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("name", name);
		return PathResolver.getPath(AdminPage.GRASS_LABEL_LIST_PAGE);
    }

	/**
	 * 保存种草社区标签表
	 */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, GrassLabel grassLabel) {
    
      if(grassLabel.getId() != null){//update
		GrassLabel entity = grassLabelService.getGrassLabel(grassLabel.getId());
		if (entity != null) {
			entity.setName(grassLabel.getName());
			entity.setIsrecommend(grassLabel.getIsrecommend());
			grassLabelService.updateGrassLabel(entity);
			}
		}else{//save
			grassLabel.setRefcount(0l);
			grassLabel.setCreateTime(new Date());
			grassLabelService.saveGrassLabel(grassLabel);
		}
    
        return "redirect:/admin/grassLabel/query";
    }

	/**
	 * 删除种草社区标签表
	 */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
    	
    	GrassLabel grassLabel = grassLabelService.getGrassLabel(id);
		if(grassLabel != null){
			grassLabelService.deleteGrassLabel(grassLabel);
		}
        return "redirect:/admin/grassLabel/query";
    }

	/**
	 * 根据Id加载种草社区标签表
	 */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        GrassLabel grassLabel = grassLabelService.getGrassLabel(id);
        request.setAttribute("grassLabel", grassLabel);
        return PathResolver.getPath(AdminPage.GRASS_LABEL_PAGE);
    }
    
   /**
	 * 加载编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.GRASS_LABEL_PAGE);
	}


}
