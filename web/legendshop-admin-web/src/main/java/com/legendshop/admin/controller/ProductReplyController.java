/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.spi.service.ProductReplyService;

/**
 * 商品回复管理控制器
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/productReply")
public class ProductReplyController extends BaseController {

	@Autowired
	private ProductReplyService productReplyService;
	
	/**
	 * 查看商品回复
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productReply
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductReply productReply) {
		PageSupport<ProductReply> ps = productReplyService.getProductReplyPage(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productReply", productReply);
		return PathResolver.getPath(BackPage.PRODUCTREPLY_LIST_PAGE);
	}
	
	/**
	 * 保存商品回复
	 * @param request
	 * @param response
	 * @param productReply
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ProductReply productReply) {
		productReplyService.saveProductReply(productReply);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.PRODUCTREPLY_LIST_QUERY);
	}
	
	/**
	 * 删除商品回复
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductReply productReply = productReplyService.getProductReply(id);
		productReplyService.deleteProductReply(productReply);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.PRODUCTREPLY_LIST_QUERY);
	}
	
	/**
	 * 查看商品回复详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductReply productReply = productReplyService.getProductReply(id);
		request.setAttribute("productReply", productReply);
		return PathResolver.getPath(BackPage.USERGRADE_EDIT_PAGE);
	}
	
	/**
	 * 跳转新建页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.PRODUCTREPLY_EDIT_PAGE);
	}
	
	/**
	 * 更新商品回复
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductReply productReply = productReplyService.getProductReply(id);
		productReplyService.updateProductReply(productReply);
		request.setAttribute("productReply", productReply);
		return PathResolver.getPath(BackPage.PRODUCTREPLY_EDIT_PAGE);
	}
}
