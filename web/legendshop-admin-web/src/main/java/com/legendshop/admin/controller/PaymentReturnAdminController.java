package com.legendshop.admin.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PrePayTypeEnum;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.SubRefundReturnDetail;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

@Controller
@RequestMapping("/admin/payment/return")
public class PaymentReturnAdminController {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(PaymentReturnAdminController.class);
	
	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	@Autowired
	private SubSettlementService subSettlementService;
	
	@Autowired
	private PaymentRefundService paymentRefundService;
	
	@Autowired
	private RefundHelper refundHelper;
	
	@Autowired
	private SubItemService subItemService;
	
	@Autowired
	private PromotionCommisService promotionCommisService;
	
	@Autowired
	private SubRefundReturnDetailService subRefundReturnDetailService;
	
	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private SubService subService;
	
	/**
	 * 线下处理退款
	 * @param request
	 * @param response
	 * @param returnPayId
	 * @return
	 */
	@RequestMapping("/offlinePay/{returnPayId}")
	public  JSONObject offlinePay(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long returnPayId) {
		JSONObject resultMap = new JSONObject();
		resultMap.put("status", false);
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		RefundLog.info(" ########### 后台线下退款操作操作  ################## ");
		RefundLog.info(" ########### returnPayId={},adminUserName={}  ################## ",returnPayId, userName);
		
		SubRefundReturn refundReturn=subRefundReturnService.getSubRefundReturn(returnPayId);
		
		//检查退款单
		String checkResult = refundHelper.checkSubRefundReturn(refundReturn);
		if (!Constants.SUCCESS.equals(checkResult)) {
			RefundLog.info(" ########### SubRefundReturn returnPayId={} 检查不通过 {}  ################## ", returnPayId,checkResult);
			resultMap.put("result", checkResult);
			return resultMap;
		}
		
		String subSettlementSn = refundReturn.getSubSettlementSn();  
		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(subSettlementSn); 
		if (subSettlement == null) {
			RefundLog.info("结算单为空，请联系管理员， subSettlementSn = {}   returnPayId={}  ", subSettlementSn,returnPayId);
			resultMap.put("result", "结算单为空，请联系管理员");
			return resultMap; 
		}
		
		String outRefundNo=CommonServiceUtil.getRandomSn();
		Integer prePayType= subSettlement.getPrePayType();
		RefundLog.info(" ############### 退款操作流水号outRefundNo={},returnPayId={}  ##############   ", outRefundNo,returnPayId);

        /*
		 * 退款情况：
		 *  情况一：全额的平台支付；
		 *  情况二：混合支付[预付款+支付宝]合并支付,形成两个支付订单，其中用户退款其中一个订单
		 *    如： 100预付款+100元支付宝; 订单A=50元 订单B=150元； 其中线下处理 
		 *  情况三: 单纯的第三方平台支付
		 *    如: 100元支付宝支付;
		 */		
		return null;
	}

	@ResponseBody
	@RequestMapping("/refund/{returnPayId}")
	public  JSONObject refund(HttpServletRequest request, HttpServletResponse response, @PathVariable Long returnPayId, String adminMessage) {
		JSONObject resultMap = new JSONObject();
		BigDecimal alpRefundAmount = new BigDecimal(0); // 第三方退款金额
		BigDecimal platformAmount = new BigDecimal(0); // 平台退款金额
		String fundType = null;  //退款处理方式
		String outRefundNo = null;  //第三方退款单号
		
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		RefundLog.info(" ########### 后台退款操作  ################## ");
		RefundLog.info(" ########### returnPayId={},adminUserName={}  ################## ",returnPayId, userName);
		
		SubRefundReturn refundReturn = subRefundReturnService.getSubRefundReturn(returnPayId);
		//检查退款单
		String checkResult = refundHelper.checkSubRefundReturn(refundReturn);
		if (!Constants.SUCCESS.equals(checkResult)) {
			RefundLog.info(" ########### SubRefundReturn returnPayId={} 检查不通过 {}  ################## ",returnPayId,checkResult);
			resultMap.put("result", checkResult);
        	return resultMap; 
		}

		if(refundReturn.getIsPresell() && refundReturn.getIsRefundDeposit()) {//预售订单，需要退订金
			RefundLog.info(" ###########  订单执行订金退款操作， returnPayId = {}  ################## ", returnPayId);
			//先退订金，再退尾款
			JSONObject processResult = subSettlementProcess(refundReturn.getDepositSubSettlementSn(),refundReturn,refundReturn.getDepositRefund().doubleValue(), "订金退款");
			
			alpRefundAmount  =  processResult.getBigDecimal("alpRefundAmount");
    		platformAmount  =  processResult.getBigDecimal("platformAmount");
    		fundType =  processResult.getString("fundType");
    		outRefundNo = processResult.getString("outRefundNo");
    		
    		RefundLog.info(" ###########  第三方退款金额：alpRefundAmount  = {}， 平台退款金额 ：platformAmount = {} ，  退款处理方式：  handleType = {} ################## ", 
    				alpRefundAmount, platformAmount, fundType);
			
    		if(!processResult.getBoolean("status")) {
    			RefundLog.info(" ########### 订单执行订金退款操作  失败  ---> fail ################## ");
				//退款失败，更新退款单
	    		subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount, platformAmount, 
	    				RefundReturnStatusEnum.DEPOSIT_REFUND_FALSE.value(), RefundReturnStatusEnum.HANDLE_FAIL.value(), null, fundType, null, outRefundNo);
	    		resultMap.put("status", false);
				resultMap.put("result", "预售订单，订金退款失败");
				return resultMap;
			}
			
    		RefundLog.info(" ########### 订单执行订金退款操作  成功  ---> success ################## ");
    		
			//判断是否 仅仅是 订金退款
			if(AppUtils.isBlank(refundReturn.getRefundAmount()) || BigDecimal.ZERO.compareTo(refundReturn.getRefundAmount()) >= 0) {
				
				RefundLog.info(" ########### 判断，该订单为仅执行退订金操作，处理退款成功后续操作   ################## ");
        		//订金退款成功，更新退款单
        		subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount, platformAmount, 
        				RefundReturnStatusEnum.DEPOSIT_REFUND_FINISH.value(), RefundReturnStatusEnum.HANDLE_SUCCESS.value(), null, fundType, null, outRefundNo);
        		
        		//退款成功处理
        		refundAndReturnSuccessHandler(returnPayId, adminMessage, refundReturn);
        		
        		resultMap.put("status", true);
        		resultMap.put("result", "预售订单，退款成功！");

				// 预售订单商家主动退款也要更新订单取消原因
				Sub sub = subService.getSubById(refundReturn.getSubId());
				sub.setCancelReason(refundReturn.getBuyerMessage());
				sub.setUpdateDate(new Date());
				subService.updateSub(sub);

    			return resultMap;

			}
			
			RefundLog.info(" ########### 订单执行 尾款退款操作   ################## ");
			//退尾款
			JSONObject finalResult = subSettlementProcess(refundReturn.getSubSettlementSn(),refundReturn,refundReturn.getRefundAmount().doubleValue(), "尾款退款");

			BigDecimal finalAlpRefundAmount  =  finalResult.getBigDecimal("alpRefundAmount");
			BigDecimal finalPlatformAmount  =  finalResult.getBigDecimal("platformAmount");
    		String finalFundType =  finalResult.getString("fundType");
    		String finalOutRefundNo = finalResult.getString("outRefundNo");
			
    		RefundLog.info(" ########### 第三方退款金额：finalAlpRefundAmount  = {}， 平台退款金额 ：finalPlatformAmount = {}，  退款处理方式：  finalFundType = {}  ################## ", 
    				finalAlpRefundAmount, finalPlatformAmount, finalFundType);
    		
			if(!(Boolean)processResult.getBoolean("status")) {
				RefundLog.info(" ########### 订单执行 尾款退款操作  失败    ------> fail   ################## ");
				//更新退款单
	    		subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount.add(finalAlpRefundAmount), platformAmount.add(finalPlatformAmount), 
	    				RefundReturnStatusEnum.DEPOSIT_REFUND_FINISH.value(), RefundReturnStatusEnum.HANDLE_FAIL.value(), finalFundType, fundType, finalOutRefundNo, outRefundNo);
	    		
	    		resultMap.put("status", false);
				resultMap.put("result", "预售订单，尾款退款失败");
	    		return resultMap;
			}
			
			//更新退款单
			subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount.add(finalAlpRefundAmount), platformAmount.add(finalPlatformAmount), 
    				RefundReturnStatusEnum.DEPOSIT_REFUND_FINISH.value(), RefundReturnStatusEnum.HANDLE_SUCCESS.value(), finalFundType, fundType, finalOutRefundNo, outRefundNo);
			
			//退款成功处理
    		refundAndReturnSuccessHandler(returnPayId, adminMessage, refundReturn);
    		resultMap.put("status", true);
    		resultMap.put("result", "预售订单，退款成功！");

			// 预售订单商家主动退款也要更新订单取消原因
			Sub sub = subService.getSubById(refundReturn.getSubId());
			sub.setCancelReason(refundReturn.getBuyerMessage());
			sub.setUpdateDate(new Date());
			subService.updateSub(sub);
    		
    		RefundLog.info(" ########### 订单执行 尾款退款操作  成功    ------> success   ################## ");
    		return resultMap;
			
		}else { //其他订单：不用退订金的预售订单 （全额支付的、用户勾选不退订金的预售订单）、普通订单等等
			
			RefundLog.info(" ########### 订单执行 退款操作  ################## ");
			
			//全额支付或者不需要退尾款的预售订单
			JSONObject processResult = subSettlementProcess(refundReturn.getSubSettlementSn(), refundReturn, refundReturn.getRefundAmount().doubleValue(), "订单退款");
			
			alpRefundAmount  =  processResult.getBigDecimal("alpRefundAmount");
    		platformAmount  =  processResult.getBigDecimal("platformAmount");
    		fundType =  processResult.getString("fundType");
    		outRefundNo = processResult.getString("outRefundNo");
			
    		RefundLog.info(" ########### 第三方退款金额：finalAlpRefundAmount  = {}， 平台退款金额 ： finalPlatformAmount = {} ，  退款处理方式： finalFundType= {}  ################## ", 
    				alpRefundAmount, platformAmount, fundType);
    		
			if(!processResult.getBoolean("status")) {
				RefundLog.info(" ########### 订单执行 退款操作  失败 ----> fail ################## ");
				//退款失败，更新退款单
	    		subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount, platformAmount, 
	    				RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value(), RefundReturnStatusEnum.HANDLE_FAIL.value(), fundType, null, outRefundNo, null);
	    		
	    		resultMap.put("status", false);
//				resultMap.put("result", "预售订单，订金退款失败");
				resultMap.put("result", "订单退款失败");
				return resultMap;
			}
			
			//更新退款单
			subRefundReturnService.updateNewSubPlatRefund(refundReturn.getRefundId(), alpRefundAmount, platformAmount, 
    				RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value(), RefundReturnStatusEnum.HANDLE_SUCCESS.value(), fundType, null, outRefundNo, null);
			
			//退款成功处理
    		refundAndReturnSuccessHandler(returnPayId, adminMessage, refundReturn);
    		RefundLog.info(" ####### 订单执行 退款操作  成功 success  #######");
    		resultMap.put("status", true);
			resultMap.put("result", "订单退款成功");
			Sub sub=new Sub();
			sub.setSubId(refundReturn.getSubId());
			saveOrderHistory(sub);
			return resultMap; 
		} 
	}
	
	/**
	 * SubSettlement 退款处理
	 * @return
	 */
	public JSONObject subSettlementProcess(String subSettlementSn, SubRefundReturn refundReturn, double refundAmount, String refundType) {
		
		JSONObject resultMap = new JSONObject();
		
		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(subSettlementSn); 
		if (subSettlement == null) {
			RefundLog.info("结算单为空，请联系管理员， subSettlementSn = {}", subSettlementSn);
			resultMap.put("status", false);
			return resultMap;
		}
		
		String outRefundNo = CommonServiceUtil.getRandomSn();
		
		Integer prePayType = subSettlement.getPrePayType();
		
		RefundLog.info(" ############### 退款操作流水号outRefundNo={} ##############   ", outRefundNo);
		
        /*
		 * 退款情况：
		 *  情况一：全额的平台支付；
		 *  情况二：混合支付[预付款+支付宝]合并支付,形成两个支付订单，其中用户退款其中一个订单
		 *    如： 100预付款+100元支付宝; 订单A=50元 订单B=150元； 其中线下处理 
		 *  情况三: 单纯的第三方平台支付
		 *    如: 100元支付宝支付;
		 */
        if(subSettlement.isFullPay()){ //情况一：全额的平台支付；

        	RefundLog.info(" ############### 该笔退款的支付单据是全款平台支付方式,直接返回用户的平台账户 subSettlementSn={} ##############   ", subSettlementSn);

        	if(PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)){ //预付款支付
        		RefundLog.info(" ############### 全款平台支付方式  = 预付款支付  ##############   ");
        		String message = "订单退款,订单号【"+refundReturn.getSubNumber()+"】 退款申请单号:"+refundReturn.getRefundSn();
        		boolean result = refundHelper.plateformPrePayReturn2(refundReturn.getUserId(), refundReturn.getUserName(), refundAmount, subSettlement.getSubSettlementSn(), message);

        		//保存退款详细记录
        		SubRefundReturnDetail subRefundReturnDetail =  new SubRefundReturnDetail();
        		subRefundReturnDetail.setPayType("预存款退款");
        		subRefundReturnDetail.setRefundId(refundReturn.getRefundId());
        		subRefundReturnDetail.setRefundType(refundType);
        		subRefundReturnDetail.setRefundStatus(result);
        		subRefundReturnDetail.setRefundAmount(new BigDecimal(refundAmount));
        		subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

        		resultMap.put("status", result);
        		resultMap.put("fundType", "预存款退款");  //平台退款
        		resultMap.put("alpRefundAmount", 0);  //第三方退款金额
        		resultMap.put("platformAmount", refundAmount); //平台退款金额
        		resultMap.put("outRefundNo", null); //退款操作流水号
        		return resultMap;
        	}
        }

		RefundLog.info(" ############### 第三方支付平台退款模式 或者是 混合退款模式  ##############   ");

		Double allowALPAmount =  refundHelper.allowReturnAmount(subSettlement);   //查看订单中目前允许微信退款的金额

        /*
         * 混合支付的退款原则是,先退第三方,再退平台钱包余额
         */
		// 说起允许第三方支付的金额为0  ,退平台钱包余额
		if (AppUtils.isBlank(allowALPAmount) || allowALPAmount <= 0) {
			RefundLog.info(" ############### 允许第三方支付的金额为0,直接退款平台帐号  ##############   ");
			if(PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)){ //预付款支付
				RefundLog.info(" ############### 预付款支付退款  ##############   ");
				String message = "订单退款,订单号【"+refundReturn.getSubNumber()+"】 退款申请单号:"+refundReturn.getRefundSn();
				boolean result = refundHelper.plateformPrePayReturn2(refundReturn.getUserId(), refundReturn.getUserName(), refundAmount, subSettlement.getSubSettlementSn(), message);

				//保存退款详细记录
        		SubRefundReturnDetail subRefundReturnDetail =  new SubRefundReturnDetail();
        		subRefundReturnDetail.setPayType("预存款退款");
        		subRefundReturnDetail.setRefundType(refundType);
        		subRefundReturnDetail.setRefundId(refundReturn.getRefundId());
        		subRefundReturnDetail.setRefundStatus(result);
        		subRefundReturnDetail.setRefundAmount(new BigDecimal(refundAmount));
        		subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

				resultMap.put("status", result);
        		resultMap.put("fundType", "预存款退款");  //是否平台退款
        		resultMap.put("alpRefundAmount", 0);  //第三方退款金额
        		resultMap.put("platformAmount", refundAmount); //平台退款金额
        		resultMap.put("outRefundNo", null); //退款操作流水号
        		return resultMap;
        	}
		}

		Double alpRefundAmount = 0d;   //记录第三方支付退款金额
		Double platformAmount = 0d;    //记录平台退款金额

		// 如果支付宝允许退款的够， 直接支付宝允许退款
		if(allowALPAmount> refundAmount) {
			alpRefundAmount= refundAmount;
			RefundLog.info( "第三方应退金额: {} ,refundSn={} ",alpRefundAmount, outRefundNo);
		}else {//实际要退款的大于支付宝允许退款的，其他用康云币或者预存款
			alpRefundAmount = allowALPAmount;
			platformAmount = refundAmount - allowALPAmount;
			RefundLog.info(" 实际要退款金额大于第三方允许退款的, 第三方应退金额: {}, 平台应退金额: {}, refundSn = {} ",alpRefundAmount, platformAmount, outRefundNo);
		}

		SysSubReturnForm returnForm=new SysSubReturnForm();
		returnForm.setOutRequestNo(outRefundNo);
		returnForm.setFlowTradeNo(subSettlement.getFlowTradeNo());
		returnForm.setSubSettlementSn(subSettlement.getSubSettlementSn());
		returnForm.setOrderDateTime(subSettlement.getCreateTime());
		returnForm.setReturnAmount(alpRefundAmount);
		returnForm.setRefundTypeId(subSettlement.getPayTypeId());
		returnForm.setPayAmount(subSettlement.getCashAmount());

		/**
		 * 第三方支付退款处理--->
		 */
		RefundLog.info( "开始第三方支付退款处理  payTypeId={}",subSettlement.getPayTypeId());
		Map<String, Object> refundMap = paymentRefundService.refund(returnForm);

		if(AppUtils.isNotBlank(refundMap) && !Boolean.valueOf(refundMap.get("result").toString())){
			RefundLog.info( "第三方支付退款处理 失败 结果: {} ",refundMap.get("message"));

			//保存退款详细记录
    		SubRefundReturnDetail subRefundReturnDetail =  new SubRefundReturnDetail();
    		subRefundReturnDetail.setPayType(subSettlement.getPayTypeName());
    		subRefundReturnDetail.setRefundType(refundType);
    		subRefundReturnDetail.setRefundId(refundReturn.getRefundId());
    		subRefundReturnDetail.setRefundStatus(Boolean.valueOf(refundMap.get("result").toString()));
    		subRefundReturnDetail.setRefundAmount(new BigDecimal(refundAmount));
    		subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

			resultMap.put("status", false);
    		resultMap.put("fundType", subSettlement.getPayTypeName());  //是否平台退款
    		resultMap.put("alpRefundAmount", alpRefundAmount);  //第三方退款金额
    		resultMap.put("platformAmount", platformAmount); //平台退款金额
    		resultMap.put("outRefundNo", outRefundNo); //退款操作流水号
			return resultMap;
		}else {
			//保存退款详细记录
    		SubRefundReturnDetail subRefundReturnDetail =  new SubRefundReturnDetail();
    		subRefundReturnDetail.setPayType(subSettlement.getPayTypeName());
    		subRefundReturnDetail.setRefundType(refundType);
    		subRefundReturnDetail.setRefundId(refundReturn.getRefundId());
    		subRefundReturnDetail.setRefundStatus(true);
    		subRefundReturnDetail.setRefundAmount(new BigDecimal(alpRefundAmount));
    		subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

		}

		RefundLog.info(" ####### 第三方支付退款处理成功  payTypeId={} ,开始本地退款业务处理 #######",subSettlement.getPayTypeId());
		/**
		 * 这里说支付外部API支付成功,下面操作本地数据库事务
		 * TODO 会有数据一致性的问题, 外部成功,本地回调不成功问题 ?? 采用事后补偿机制
		 */
		try {
			//refundHelper.orderReturnOnlineSettle(refundReturn.getRefundId(), outRefundNo,alpRefundAmount,prePayType);
			if(platformAmount > 0) { //还需要平台退款
				String message = "订单退款,订单号【"+refundReturn.getSubNumber()+"】 退款申请单号:"+refundReturn.getRefundSn();
				boolean result = refundHelper.plateformPrePayReturn2(refundReturn.getUserId(), refundReturn.getUserName(), platformAmount, subSettlement.getSubSettlementSn(), message);

				//保存退款详细记录
	    		SubRefundReturnDetail subRefundReturnDetail =  new SubRefundReturnDetail();
	    		subRefundReturnDetail.setPayType("预存款退款");
	    		subRefundReturnDetail.setRefundType(refundType);
	    		subRefundReturnDetail.setRefundStatus(result);
	    		subRefundReturnDetail.setRefundId(refundReturn.getRefundId());
	    		subRefundReturnDetail.setRefundAmount(new BigDecimal(platformAmount));
	    		subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

				resultMap.put("status", result);
				resultMap.put("fundType", subSettlement.getPayTypeName() + "、预存款退款");  //是否平台退款
				resultMap.put("alpRefundAmount", alpRefundAmount);  //第三方退款金额
				resultMap.put("platformAmount", platformAmount); //平台退款金额
				resultMap.put("outRefundNo", null); //退款操作流水号
				return resultMap;
			}else {
				resultMap.put("status", true);
				resultMap.put("fundType", subSettlement.getPayTypeName());  //是否平台退款
				resultMap.put("alpRefundAmount", alpRefundAmount);  //第三方退款金额
				resultMap.put("platformAmount", platformAmount); //平台退款金额
				resultMap.put("outRefundNo", outRefundNo); //退款操作流水号
				return resultMap;
			}
		} catch (Exception e) {
			log.error(" ##################  外部成功,本地回调不成功问题 {} #################",e);
			log.error(" 本台处理退款业务失败, RefundId = {},outRefundNo={}, alpRefundAmount={}, prePayType={} ", refundReturn.getRefundId(), outRefundNo, alpRefundAmount, prePayType);
			resultMap.put("status", false);
			return resultMap;
		}

	}
	
	/**
	 * 退款退货成功后处理
	 */
	public void refundAndReturnSuccessHandler(Long returnPayId, String adminMessage, SubRefundReturn refundReturn) {
		if (refundReturn.getApplyType() == RefundReturnTypeEnum.REFUND.value()) {//（单退款）处理退款成功后的逻辑
			adminAuditRefund(returnPayId, adminMessage);
		}
		
		if (refundReturn.getApplyType() == RefundReturnTypeEnum.REFUND_RETURN.value()) {//（退货退款）处理退款成功后的逻辑
			adminAuditReturn(returnPayId, adminMessage);
		}
		//退款成功后，回退分销佣金
		cancleOrderCommis(refundReturn);
	}

	
	/**
	 * 支付通知接口
	 * 
	 * @param request
	 * @param response
	 * @returnk
	 */
	@RequestMapping(value = "/refundQuery/{returnPayId}", method = RequestMethod.POST)
	public @ResponseBody String refundQuery(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long returnPayId) {
		KeyValueEntity entity = new KeyValueEntity();
		entity.setKey("false");
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(returnPayId);
		if (subRefundReturn == null) {
			return "找不到该退款申请单";
		}
		if (subRefundReturn.getIsHandleSuccess() == 0) {
			return "退款正在处理中或者是未提交支付退款,请稍后查询";
		} else if (subRefundReturn.getIsHandleSuccess() == -1) {
			return "退款失败,请联系平台管理员";
		} else {
			return "退款成功,退款流水号=" + subRefundReturn.getOutRefundNo();
		}
	}
	
	/**
	 * （退货退款）退款成功后，处理退款单状态以及订单状态。退款记录等操作（相当于之前同意退款后，提交处理的动作）
	 * @param returnPayId
	 * @param adminMessage
	 */
	private void adminAuditReturn(Long returnPayId,String adminMessage){
		
		if (null == returnPayId || AppUtils.isBlank(adminMessage)) {
			throw new NullPointerException("refundId or adminMessage isNull");
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(returnPayId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		
		//TODO by:linzh 去掉RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() != subRefundReturn.getApplyState()待管理员处理状态的的判断，因为强制退款状态本来就是阻断流程来进行退款的
		if (RefundReturnTypeEnum.REFUND_RETURN.value() != subRefundReturn.getApplyType()
				/*|| RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() != subRefundReturn.getApplyState()*/) {
			throw new BusinessException("Illegal Operate");
		}
		subRefundReturnService.adminAuditReturn(subRefundReturn, adminMessage);
		// 后台审核退货 退款给用户 , 订单项退款状态更新为已完成 ,检查当前订单项是否是订单中最后一个在退款中的订单项
		// 是则也把订单的状态以及订单的退款状态更新为已完成
	}
	
	/**
	 * (单退款)退款成功后，处理退款单状态以及订单状态。退款记录等操作（相当于之前同意退款后，提交处理的动作）
	 * @param returnPayId
	 * @param adminMessage
	 */
	private void adminAuditRefund(Long returnPayId,String adminMessage){
		
		if (null == returnPayId || AppUtils.isBlank(adminMessage)) {
			throw new NullPointerException("refundId or adminMessage isNull");
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(returnPayId);
		if (null == subRefundReturn) {
			throw new BusinessException("subRefundReturn is Null");
		}
		if (RefundReturnTypeEnum.REFUND.value() != subRefundReturn.getApplyType()
				|| RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value() != subRefundReturn.getApplyState()) {
			throw new BusinessException("Illegal Operate");
		}
		if (subRefundReturn.getIsHandleSuccess() == 0) {
			throw new BusinessException("请先处理退款,才能处理审核");
		}
		subRefundReturnService.adminAuditRefund(subRefundReturn, adminMessage);
	}
	
	/**
	 * 退款成功后，回退分销佣金
	 * @param refundReturn
	 */
	private void cancleOrderCommis(SubRefundReturn refundReturn){
		//如果是分销订单，则取消分销佣金
		if(refundReturn.getSubItemId()==0L){ //订单回退
			List<SubItem> distSubItems = subItemService.getDistSubItemBySubNumber(refundReturn.getSubNumber());
			if(AppUtils.isNotBlank(distSubItems)){
				for (SubItem subItem : distSubItems) {
					promotionCommisService.cancleOrderCommis(subItem); //订单退款时，退款佣金比例默认为1，佣金全部回退
				}
			}
		}else { //订单项回退
			SubItem subItem = subItemService.getSubItem(refundReturn.getSubItemId());
			if(subItem.getHasDist()==1){
				promotionCommisService.cancleOrderCommis(subItem);
			}
		}
	}
	/**
	 * 订单日志
	 * @param sub
	 */
	private void saveOrderHistory(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_PAY.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]平台").append("于").append(time).append("完成了订单退货/退款的审核");
		subHistory.setUserName("平台");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

}
