/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.NewsCategory;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.NewsCategoryService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 文章分类控制器.
 */
@Controller
@RequestMapping("/admin/newsCategory")
public class NewsCategoryAdminController extends BaseController {
	
	/** The news category service. */
	@Autowired
	private NewsCategoryService newsCategoryService;
	
	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查找文章分类.
	 * 
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, NewsCategory newsCategory) {
		PageSupport<NewsCategory> ps = newsCategoryService.getNewsCategoryListPage(curPageNO, newsCategory);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", newsCategory);
		return PathResolver.getPath(AdminPage.NEWSCAT_LIST_PAGE);
	}

	@RequestMapping(value = "/querylanmu", method = RequestMethod.GET)
	public String querylanmu(HttpServletRequest request, HttpServletResponse response) {
		List<NewsCategory> newsCategory = newsCategoryService.getNewsCategoryList();
		if (!AppUtils.isBlank(newsCategory)) {
			request.setAttribute("newsCategoryList", newsCategory);
		}
		String path = PathResolver.getPath(BackPage.NEWSCAT_CREAT_PAGE);
		return path;
	}

	/**
	 * 保存文章分类.
	 * 
	 */
	@SystemControllerLog(description="保存栏目")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, NewsCategory newsCategory) {//TODO 多事务
		SecurityUserDetail user = UserManager.getUser(request);
		String filename = null;
		MultipartFile formFile = newsCategory.getFile();// 取得上传的文件
		try {
			if (newsCategory.getNewsCategoryId() != null) {// update
				NewsCategory origin = newsCategoryService.getNewsCategoryById(newsCategory.getNewsCategoryId());

				if ((formFile != null) && (formFile.getSize() > 0)) {
					// upload file
					filename = attachmentManager.upload(formFile);
					// 保存附件表
					attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.NEWS);
					origin.setPic(filename);
				}

				origin.setNewsCategoryDate(new Date());
				origin.setUserId(user.getUserId());
				origin.setUserName(user.getUsername());
				origin.setSeq(newsCategory.getSeq());
				origin.setNewsCategoryName(newsCategory.getNewsCategoryName());
				origin.setStatus(newsCategory.getStatus());
				newsCategoryService.save(origin);

			} else {
				newsCategory.setNewsCategoryDate(new Date());
				newsCategory.setUserId(user.getUserId());
				newsCategory.setUserName(user.getUsername());
				// upload file
				if ((formFile != null) && (formFile.getSize() > 0)) {
					filename = attachmentManager.upload(formFile);
					newsCategory.setPic(filename);
				}
				
				newsCategoryService.save(newsCategory);
				if (AppUtils.isNotBlank(filename)) {
					// 保存附件表
					attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.NEWS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.NEWSCAT_LIST_QUERY);
	}

	/**
	 * 删除文章分类.
	 * 
	 */
	@SystemControllerLog(description="删除栏目")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		newsCategoryService.delete(id);
		saveMessage(request, ResourceBundleHelper.getDeleteString());
		return PathResolver.getPath(RedirectPage.NEWSCAT_LIST_QUERY);
	}

	/**
	 * 跳转编辑页面.
	 * 
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		NewsCategory newsCategory = newsCategoryService.getNewsCategoryById(id);
		request.setAttribute("bean", newsCategory);
		return PathResolver.getPath(AdminPage.NEWSCAT_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面.
	 * 
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.NEWSCAT_EDIT_PAGE);
	}

	/**
	 * 更新文章分类.
	 * 
	 */
	@SystemControllerLog(description="更新栏目")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable NewsCategory newsCategory) {
		newsCategoryService.update(newsCategory);
		return PathResolver.getPath(RedirectPage.NEWSCAT_LIST_QUERY);
	}

}
