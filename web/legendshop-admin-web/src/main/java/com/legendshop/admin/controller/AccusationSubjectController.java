/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.AccusationType;
import com.legendshop.spi.service.AccusationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.spi.service.AccusationSubjectService;

/**
 * 举报主体控制器
 *
 */
@Controller
@RequestMapping("/admin/accusationSubject")
public class AccusationSubjectController extends BaseController {

	@Autowired
	private AccusationSubjectService accusationSubjectService;

	@Autowired
	private AccusationTypeService accusationTypeService;

	/**
	 * 保存
	 * 
	 * @param request
	 * @param response
	 * @param accusationSubject
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, AccusationSubject accusationSubject) {
		accusationSubject.setRecDate(new Date());
		accusationSubjectService.saveAccusationSubject(accusationSubject);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.ACCUSATIONTYPE_LIST_QUERY);
	}

	/**
	 * 删除举报
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AccusationSubject accusationSubject = accusationSubjectService.getAccusationSubject(id);

		List<AccusationSubject> subjectList = accusationTypeService.getSubjectByTypeId(accusationSubject.getTypeId());
		if (subjectList.size() <= 1){
			// 同步下线主题类型
			AccusationType accusationType = accusationTypeService.getAccusationType(accusationSubject.getTypeId());

			accusationType.setStatus(0L);
			accusationTypeService.saveAccusationType(accusationType);
		}

		accusationSubjectService.deleteAccusationSubject(accusationSubject);

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.ACCUSATIONTYPE_LIST_QUERY);
	}

	/**
	 * 根据Id加载
	 * 
	 * @param request
	 * @param response
	 * @param typeId
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{typeId}/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long typeId, @PathVariable Long id) {
		AccusationSubject accusationSubject = accusationSubjectService.getAccusationSubject(id);
		request.setAttribute("accusationSubject", accusationSubject);
		request.setAttribute("typeId", typeId);
		 return PathResolver.getPath(AdminPage.ACCUSATIONSUBJECT_EDIT_PAGE);
	}

	/**
	 * 举报主题编辑
	 * 
	 * @param request
	 * @param response
	 * @param typeId
	 * @return
	 */
	@RequestMapping(value = "/load/{typeId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long typeId) {
		request.setAttribute("typeId", typeId);
		return PathResolver.getPath(AdminPage.ACCUSATIONSUBJECT_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody Long updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long status) {
		AccusationSubject accusationSubject = accusationSubjectService.getAccusationSubject(id);
		if (accusationSubject == null) {
			return (long) -1;
		}

		List<AccusationSubject> subjectList = accusationTypeService.getSubjectByTypeId(accusationSubject.getTypeId());
		if (status == 0 && subjectList.size() <= 1){
			// 同步下线主题类型
			AccusationType accusationType = accusationTypeService.getAccusationType(accusationSubject.getTypeId());

			accusationType.setStatus(0L);
			accusationTypeService.saveAccusationType(accusationType);
		}

		if (!status.equals(accusationSubject.getStatus())) {
			accusationSubject.setStatus(status);
			accusationSubjectService.saveAccusationSubject(accusationSubject);
		}
		return accusationSubject.getStatus();
	}
}
