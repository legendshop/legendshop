/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.spi.service.FloorItemService;
import com.legendshop.spi.service.FloorLayoutFactoryService;
import com.legendshop.spi.service.FloorService;
import com.legendshop.spi.service.SubFloorService;
import com.legendshop.util.AppUtils;

/**
 * 楼层管理
 *
 */
@Controller
@RequestMapping("/admin/floor")
public class FloorController extends BaseController{
   
	@Autowired
    private FloorService floorService;
    
    @Autowired
    private SubFloorService subFloorService;
    
    @Autowired
    private FloorItemService floorItemService; 
    
    @Autowired
    private FloorLayoutFactoryService floorLayoutFactoryService;
    
    /**
     * 后台楼层管理列表
     */
    @RequestMapping("/batchOffline/{floorIdStr}")
    public @ResponseBody String batchOffline(HttpServletRequest request, HttpServletResponse response, @PathVariable String floorIdStr) {
    	try{
    		floorService.batchOffline(floorIdStr);
    		return "OK";
    	}catch(Exception e){
    		e.printStackTrace();
    		return "fail";
    	}
    }
    
    /**
     * 后台楼层管理列表
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Floor floor) {
        PageSupport<Floor> ps = floorService.getFloorPage(curPageNO,floor);
		List<Floor> floorList = (List<Floor>) ps.getResultList();
        if(AppUtils.isNotBlank(floorList)){
        	List<SubFloor> subFloorList = subFloorService.getAllSubFloor(floorList);
        	for(Floor f : floorList){
        		for(SubFloor subfloor : subFloorList){
        			f.addSubFloor(subfloor);
        		}
        	}
        }
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("floor",floor);
        return PathResolver.getPath(AdminPage.FLOOR_LIST_PAGE);
    }

    /**
     * 保存楼层
     */
    @SystemControllerLog(description="保存楼层")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Floor floor) {
    	if(AppUtils.isBlank(floor.getName()) || AppUtils.isBlank(floor.getLayout())){
    		return PathResolver.getPath(AdminPage.ADMIN_404);
    	}
    	floor.setRecDate(new Date());
        floorService.saveFloor(floor);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.FLOOR_LIST_QUERY);
    }

    /**
     * 删除楼层
     */
    @SystemControllerLog(description="删除楼层")
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
    	
        Floor floor = floorService.getFloor(id);
        
        floorLayoutFactoryService.deleteFloor(floor);
		
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(RedirectPage.FLOOR_LIST_QUERY);
    }

    /**
     * 修改编辑楼层
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Floor floor = floorService.getFloor(id);
        request.setAttribute("floor", floor);
        return PathResolver.getPath(AdminPage.FLOOR_EDIT_PAGE);
    }
    
    /**
     * 新建楼层
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.FLOOR_EDIT_PAGE);
	}

	/**
	 * 更新楼层
	 */
    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Floor floor = floorService.getFloor(id);
		request.setAttribute("floor", floor);
		return PathResolver.getPath(BackPage.FLOOR_EDIT_PAGE);
    }
    
    /**
     * 更新状态
     * @param request
     * @param response
     * @param id
     * @param status
     * @return
     */
    @SystemControllerLog(description="更改楼层状态")
    @RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
			@PathVariable Integer status) {
		Floor floor = floorService.getFloor(id);
		if (floor == null) {
			return -1;
		}
        
		if (!status.equals(floor.getStatus())) {
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				floor.setStatus(status);
				floor.setRecDate(new Date());
				floorService.updateFloor(floor);
			}
		}
		return floor.getStatus();
	}
    
    /**
     * 加载楼层编辑
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/loadFloorEdit/{id}")
	public String loadFloorEdit(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id) {
    	Floor floor = floorService.getFloor(id);
        //检查楼层的类型;
        if(AppUtils.isBlank(floor.getLayout())){
        	return floorWideBlend(request,response,floor);
        }
        String path="";
        switch(FloorLayoutEnum.fromString(floor.getLayout())){
		case WIDE_ADV_RECTANGLE_FOUR:
			path=floorWideAdvRectangleFour(request,response,floor);
			break;
		case WIDE_ADV_BRAND:
			path=floorWideAdvBrand(request,response,floor);
			break;
		case WIDE_GOODS:
			path=floorWideGoods(request,response,floor);
			break;
		case WIDE_ADV_FIVE:
			path=floorWideAdvFive(request,response,floor);
			break;
		case WIDE_ADV_SQUARE_FOUR:
			path=floorWideAdvSquareFour(request,response,floor);
			break;
		case WIDE_ADV_EIGHT:
			path=floorWideAdvEight(request,response,floor);
			break;
		case WIDE_ADV_ROW:
			path=floorWideAdvRow(request,response,floor);
			break;
		case WIDE_BLEND:
			path=floorWideBlend(request,response,floor);
			break;
        }
		return path;
	}
    
    
    /**
     * (4X矩形广告)
     */
    private String floorWideAdvRectangleFour(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	List<FloorItem> advList =floorLayoutFactoryService.findFloorAdvs(floor,FloorTemplateEnum.RIGHT_ADV.value());
    	request.setAttribute("advList",advList);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_RECTANGLE_FOUR);
    }
    
    /**
     * (4x方形广告)
     */
    private String floorWideAdvSquareFour(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	List<FloorItem> advList =floorLayoutFactoryService.findFloorAdvs(floor,FloorTemplateEnum.RIGHT_ADV.value());
    	request.setAttribute("advList",advList);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_SQUARE_FOUR);
    }
    
    /**
     * (8x广告)
     */
    private String floorWideAdvEight(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	List<FloorItem> advList =floorLayoutFactoryService.findFloorAdvs(floor,FloorTemplateEnum.RIGHT_ADV.value());
    	request.setAttribute("advList",advList);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_EIGHT);
    }
  
    
    /**
     * 横排广告
     */
    private String floorWideAdvRow(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	FloorItem adv =floorLayoutFactoryService.findFloorAdv(floor,FloorTemplateEnum.RIGHT_ADV.value());
    	request.setAttribute("adv",adv);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_ROW);
    }
    
    
    
    /**
     * (4X广告+品牌)
     */
    private String floorWideAdvBrand(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	
    	 //装载品牌
		List<FloorItem> brandList=floorLayoutFactoryService.findFloorAdvs(floor,FloorTemplateEnum.BRAND.value());
        request.setAttribute("brandList", brandList);
       
        //装载广告
        FloorItem adv=floorLayoutFactoryService.findFloorAdv(floor,FloorTemplateEnum.RIGHT_ADV.value());
        request.setAttribute("adv", adv);
    	
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_BRAND);
    }
   
    
    /**
     * 5x广告
     */
    private String floorWideAdvFive(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	List<FloorItem> advList =floorLayoutFactoryService.findFloorAdvs(floor,FloorTemplateEnum.RIGHT_ADV.value());
    	request.setAttribute("advList",advList);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_ADV_FIVE);
    }
    
    
    /**
     * 5x商品
     * @param request
     * @param response
     * @param floor
     * @return
     */
    private String floorWideGoods(HttpServletRequest request, HttpServletResponse response,Floor floor){
    	FloorDto floorDto=floorLayoutFactoryService.findAdminFloorDto(floor);
    	List<SubFloorItem> items = floorDto.getSubFloorItems();
    	request.setAttribute("items", items);
    	request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.WIDE_GOODS);
    }
    
    
    private String floorWideBlend(HttpServletRequest request,HttpServletResponse response, Floor floor){
    	FloorDto floorDto=floorLayoutFactoryService.findAdminFloorDto(floor);
    	Set<SubFloor> subFloorList=floorDto.getSubFloors();
    	
    	 //装载商品分类的楼层信息
    	List<FloorItem> floorSortItemList=null;	
    	FloorItem leftAdv=null;
    	Map<String, List<FloorItem>> floorItems =floorDto.getFloorItems();
    	if(AppUtils.isNotBlank(floorItems) && floorItems.size()>0){
    	  floorSortItemList=floorItems.get(FloorTemplateEnum.SORT.value());
    	  List<FloorItem> items=floorItems.get(FloorTemplateEnum.LEFT_ADV.value());
    	  if(AppUtils.isNotBlank(items)){
    		  leftAdv=items.get(0);
    	  }
    	}
    	request.setAttribute("subFloors", subFloorList);
        request.setAttribute("floorSortItemList", floorSortItemList);
        request.setAttribute("leftAdv",leftAdv);
        request.setAttribute("floor", floor);
    	return PathResolver.getPath(BackPage.FLOOR_WIDE_BLEND);
    } 
    
    @RequestMapping(value = "/brandContent/{flId}")
   	public String brandContent(HttpServletRequest request, HttpServletResponse response,@PathVariable
		    Long flId,String layout) {
    	List<FloorItem> brandList = floorItemService.getBrandList(flId,layout,40); 
        request.setAttribute("brandList", brandList);
        request.setAttribute("flId", flId);
       	return PathResolver.getPath(BackPage.BRAND_CONTENT);
       }
    
    @RequestMapping(value = "/groupContent/{flId}")
   	public String groupContent(HttpServletRequest request, HttpServletResponse response,@PathVariable
		    Long flId) {
    	FloorItem group = floorItemService.getFloorGroup(flId);
        request.setAttribute("group", group);
       	return PathResolver.getPath(BackPage.GROUP_CONTENT);
       } 
       
    @RequestMapping(value = "/rankContent/{flId}")
    public String rankContent(HttpServletRequest request, HttpServletResponse response,@PathVariable
   		    Long flId) {
    	List<FloorItem> rankList = floorItemService.getRankList(flId);
        request.setAttribute("rankList", rankList);
        return PathResolver.getPath(BackPage.RANK_CONTENT);
          }
    
    @RequestMapping(value = "/newsContent/{flId}")
   	public String newsContent(HttpServletRequest request, HttpServletResponse response,@PathVariable
		    Long flId) {
    	List<FloorItem> newsList = floorItemService.getNewsList(flId);
    	request.setAttribute("newsList", newsList);
    	request.setAttribute("flId", flId);
       	return PathResolver.getPath(BackPage.NEWS_CONTENT);
    } 
}
