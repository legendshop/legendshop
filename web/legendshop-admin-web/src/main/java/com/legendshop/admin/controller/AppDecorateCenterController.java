/*
 * LegendShop 多用户商城系统
 *  版权所有,并保留所有权利。
 */
package com.legendshop.admin.controller;

import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.APPTemplateCategoryEnum;
import com.legendshop.model.constant.AppDecoratePageStatusEnum;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.appdecorate.DecorateCategoryDto;
import com.legendshop.model.dto.appdecorate.DecorateProdDto;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 后台-移动端装修服务中心
 */
@RestController
@RequestMapping("/admin/decorate/action")
public class AppDecorateCenterController{
	
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppDecorateCenterController.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryManagerService categoryManagerService;
	
	@Autowired
	private AppPageManageService appPageManageService;
	
	@Autowired
	private ProdGroupService prodGroupService;
	
	@Autowired
	private AttachmentTreeService attachmentTreeService;
	
	@Autowired
	private AttachmentService attachmentService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	
	/**
	 * 挑选商品弹层
	 */
	@PostMapping("/prodList")
	public ResultDto<PageSupport<DecorateProdDto>> prodList(String curPageNO,String prodName) {
		
		PageSupport<Product> ps = productService.getProductList(prodName, curPageNO,null);
		
		PageSupport<DecorateProdDto> convertPageSupport = PageUtils.convert(ps, new PageUtils.ResultListConvertor<Product, DecorateProdDto>() {

			@Override
			public List<DecorateProdDto> convert(List<Product> form) {

				List<DecorateProdDto> toList = new ArrayList<DecorateProdDto>();
				for (Product product : form) {
					
					DecorateProdDto decorateProdDto = convertToDecorateProdDto(product);
					toList.add(decorateProdDto);
				}
				return toList;
			}
		});
		return ResultDtoManager.success(convertPageSupport);
	}	
	
	
	/**
	 * 挑选分类弹层
	 */
	@PostMapping("/category")
	public ResultDto<List<DecorateCategoryDto>> category(String curPageNO,String prodName) {
		
		try {
			
	    	TreeNode rootTreeNode = categoryManagerService.getTreeNodeById(Constants.PRODUCT_CATEGORY_ROOT);
	    	List<DecorateCategoryDto> catDtoList = convert(rootTreeNode);
	    	
			return ResultDtoManager.success(catDtoList);
			
		} catch (Exception e) {
			log.error("获取分类数据异常",e);
		}
		return ResultDtoManager.fail();
	}	
	
	
	/**
	 * 挑选页面弹层
	 */
	@PostMapping("/page")
	public ResultDto<PageSupport<AppPageManage>> page(String curPageNO,AppPageManage appPageManage) {
		
		try {
			
			//只查询发布过的海报页
			appPageManage.setCategory(APPTemplateCategoryEnum.POSTER.value());
			appPageManage.setStatus(AppDecoratePageStatusEnum.RELEASED.value());
			PageSupport<AppPageManage> ps = appPageManageService.queryAppPageManage(curPageNO,5, appPageManage);
			
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			log.error("获取页面数据异常",e);
		}
		return ResultDtoManager.fail();
	}	
	
	
	/**
	 * 挑选商品分组弹层
	 */
	@PostMapping("/prodGroup")
	public ResultDto<PageSupport<ProdGroup>> prodGroup(String curPageNO,ProdGroup prodGroup) {
		
		try {
			
			PageSupport<ProdGroup> ps = prodGroupService.queryProdGroupListPage(curPageNO, prodGroup, 5);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			log.error("获取商品分组异常",e);
		}
		return ResultDtoManager.fail();
	}	
	
	
	
	/**
	 * 上传商品图片文件
	 * 
	 * @param request
	 * @param files
	 * @return
	 * @throws IOException
	 */
	@PostMapping(value = "/uploadPicture")
	public ResultDto<Object> uploadPicture(MultipartHttpServletRequest request, @RequestParam("files[]") MultipartFile[] files) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(files)) {
			try {
				for (MultipartFile file : files) {
					if (file.getSize() > 0) {
						String pic = attachmentManager.upload(file);
						attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.ADVERTISEMENT);
					}
				}
				return ResultDtoManager.success();
			} catch (Exception e) {
				ResultDtoManager.fail();
			}
		} else {
			return ResultDtoManager.fail("请上传图片");
		}
		return ResultDtoManager.fail();
	}

	
	
	
	
	/**
	 * 获取图片空间目录
	 */
	@PostMapping("/imagesTree")
	public ResultDto<List<AttachmentTree>> imagesTree(HttpServletRequest request) {
		
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			if(AppUtils.isBlank(user)){
				return null;
			}
			
			//组装树结构 
			List<AttachmentTree> attmntTrees = attachmentTreeService.getAttachmentTreeDtoByUserName(user.getUsername());
			if (AppUtils.isBlank(attmntTrees)){

				attmntTrees = new ArrayList<AttachmentTree>();
				AttachmentTree attachmentTree = new AttachmentTree();
				attachmentTree.setId(0l);
				attachmentTree.setName("根目录");
				attachmentTree.setParentId(0l);
				attachmentTree.setUserName(user.getUsername());
				attachmentTree.setIsParent(true);
				attachmentTree.setOpen(true);
				attmntTrees.add(attachmentTree);
			}

			return ResultDtoManager.success(attmntTrees);
		} catch (Exception e) {
			log.error("获取图片空间目录异常",e);
		}
		return ResultDtoManager.fail();
	}	
	

	/**
	 * 图片控件 获取目录下的图片
	 * @param treeId 树id
	 * @param curPageNO 当前页
	 * @param orderBy  排序
	 * @param searchName  搜索名称
	 */
	@PostMapping("/images")
	public ResultDto<PageSupport<Attachment>> images(HttpServletRequest request,Long treeId, String curPageNO,String orderBy,String searchName) {
		
		try {
			
			SecurityUserDetail user = UserManager.getUser(request);
			if(AppUtils.isBlank(user)){
				log.warn("visit img getPropChildImg, userId is null and return");
				return ResultDtoManager.fail();
			}
			Long shopId = user.getShopId();

			PageSupport<Attachment> ps = attachmentService.getPropChildImg(treeId, curPageNO, orderBy, searchName,shopId);
			
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			log.error("获取图片数据异常",e);
		}
		return ResultDtoManager.fail();
	}
	
	
	
	/**
	 * 分类数结构转换成分类Dto列表
	 * @param rootTreeNode
	 * @return
	 */
	private List<DecorateCategoryDto> convert(TreeNode rootTreeNode){
		if(AppUtils.isNotBlank(rootTreeNode)){
			List<DecorateCategoryDto> dtoList = new ArrayList<DecorateCategoryDto>();
			for (TreeNode node : rootTreeNode.getChildList()) {
				DecorateCategoryDto dto = convertDto(node);
				traverse(dto, node);
				dtoList.add(dto);
			}
			return dtoList;
		}
		return null;
	}
	
	/**
	 * 分类数结构转换成分类Dto
	 * @param category
	 * @return
	 */
	private DecorateCategoryDto convertDto(TreeNode category){
		DecorateCategoryDto dto = new DecorateCategoryDto();
		dto.setId(category.getSelfId());
		dto.setName(category.getNodeName());
		dto.setGrade(category.getLevel());
		dto.setParentId(category.getParentId());
		return dto;
	}
	
	/**
	 * 深度优先遍历树
	 * @param dto
	 * @param node
	 */
	public void traverse(DecorateCategoryDto dto, TreeNode node) {
		List<TreeNode> childList = node.getChildList();
		if (childList == null || childList.isEmpty()){
			return;
		}else{
			int childNumber = childList.size();
			for (int i = 0; i < childNumber; i++) {
				TreeNode child = childList.get(i);
				DecorateCategoryDto categoryDto = convertDto(child);
				dto.addChildren(categoryDto);
				traverse(categoryDto, child);
			}
		}
			
	}
	
	/**
	 * 转换弹层商品
	 * @param product
	 */
	private DecorateProdDto convertToDecorateProdDto(Product product){
		  DecorateProdDto decorateProdDto = new DecorateProdDto();
		  
		  decorateProdDto.setProdId(product.getProdId());
		  decorateProdDto.setSpuName(product.getName());
		  decorateProdDto.setPrice(product.getCash());
		  decorateProdDto.setPic(product.getPic());
		  return decorateProdDto;
	}
	
}
