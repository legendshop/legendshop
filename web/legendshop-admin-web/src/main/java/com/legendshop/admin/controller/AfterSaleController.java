/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AfterSaleService;
import com.legendshop.util.AppUtils;

/**
 * 商品售后服务
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/admin/afterSale")
public class AfterSaleController extends BaseController {

	@Autowired
	private AfterSaleService afterSaleService;

	/**
	 * 去往售后弹框页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/overlay")
	public String afterSaleOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		PageSupport<AfterSale> ps = afterSaleService.getAfterSale(curPageNO, userName, userId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(AdminPage.AFTERSALE_OVERLAY);
	}

	/**
	 * 保存或更新售后说明
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param title
	 * @param content
	 * @return
	 */
	@RequestMapping(value = "/save")
	public @ResponseBody Long saveAfterSale(HttpServletRequest request, HttpServletResponse response, Long id, String title, String content) {
	
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		Long shopId = user.getShopId();

		AfterSale afterSale = new AfterSale();
		afterSale.setId(id);
		afterSale.setRecDate(new Date());
		afterSale.setTitle(title);
		afterSale.setContent(content);
		afterSale.setUserId(userId);
		afterSale.setUserName(userName);
		afterSale.setShopId(shopId);
		
		Long afterSaleId = afterSaleService.saveAfterSale(afterSale);
		return afterSaleId;
	}

	/**
	 * 删除售后说明
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete")
	public @ResponseBody String deleteAfterSale(HttpServletRequest request, HttpServletResponse response, Long id) {
		if (AppUtils.isBlank(id)) {
			return Constants.FAIL;
		} else {
			afterSaleService.deleteByAfterSaleId(id);
			return Constants.SUCCESS;
		}
	}
}
