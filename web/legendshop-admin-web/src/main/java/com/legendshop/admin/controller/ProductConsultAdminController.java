/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;

/**
 * 咨询
 */
@Controller
@RequestMapping("/admin/productConsult")
public class ProductConsultAdminController extends BaseController {

	@Autowired
	private ProductConsultService productConsultService;

	@Autowired
	private ProductService productService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 查看咨询.
	 *
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductConsult productConsult) {
		request.setAttribute("productConsult", productConsult);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.PROD_CONSULT_LIST_PAGE);
	}
	
	/**
	 * 查看咨询列表
	 *
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductConsult productConsult) {
		PageSupport<ProductConsult> ps = productConsultService.getProductConsult(curPageNO, productConsult);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productConsult", productConsult);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.PROD_CONSULT_CONTENT_LIST_PAGE);
	}
	
	/**
	 * 批量删除咨询.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="批量删除商品咨询")
	@RequestMapping(value = "/batchDelete/{consIds}")
	public @ResponseBody String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String consIds) {
		if(AppUtils.isBlank(consIds)){
			return "参数错误";
		}
		try{
			int num=productConsultService.batchDeleteConsultByIds(consIds);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return Constants.FAIL;
		}
	}

	/**
	 * 得到尚未回复的商品评论
	 *
	 */
	@RequestMapping("/queryUnReply")
	public String queryUnReply(HttpServletRequest request, HttpServletResponse response) {
		List<ProductConsult> productConsultList = productConsultService.queryUnReply();
		request.setAttribute("productConsultList", productConsultList);
		return PathResolver.getPath(BackPage.UNREPLY_PROD_CONSULT_LIST_PAGE);
	}

	/**
	 * 保存.
	 *
	 */
	@SystemControllerLog(description="回复商品咨询")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ProductConsult productConsult) {
		// only for update
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		productConsultService.updateProductConsult(userName, productConsult);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PROD_CONSULT_LIST_QUERY);
	}

	/**
	 * 删除.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="删除商品咨询")
	@RequestMapping(value = "/delete/{consId}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long consId) {
		productConsultService.deleteProductConsultById(consId);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.PROD_CONSULT_LIST_QUERY);
	}

	/**
	 * 查看咨询详情.
	 *
	 */
	@RequestMapping(value = "/load/{consId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long consId) {
		ProductConsult productConsult = productConsultService.getProductConsult(consId);
		if (productConsult == null) {
			throw new NotFoundException("productConsult not found with Id " + consId);
		}
		Product product = productService.getProductById(productConsult.getProdId());
		productConsult.setProdName(product.getName());
		request.setAttribute("productConsult", productConsult);
		return PathResolver.getPath(AdminPage.PROD_CONSULT_EDIT_PAGE);
	}

}
