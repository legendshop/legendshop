/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinBackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.weixin.WeixinNewsItemsDto;
import com.legendshop.model.entity.weixin.WeixinImgFile;
import com.legendshop.model.entity.weixin.WeixinImgGroup;
import com.legendshop.model.entity.weixin.WeixinNewsitem;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.WeixinImgFileService;
import com.legendshop.spi.service.WeixinImgGroupService;
import com.legendshop.spi.service.WeixinNewsitemService;
import com.legendshop.spi.service.WeixinNewstemplateService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 微信素材管理
 *
 */
@Controller
@RequestMapping("/admin/weixinNewstemplate")
public class WeixinNewstemplateController extends BaseController{
    @Autowired
    private WeixinNewstemplateService weixinNewstemplateService;
    
    @Autowired
    private WeixinNewsitemService weixinNewsitemService;
    
    @Autowired
    private WeixinImgFileService weixinImgFileService;
    
    @Autowired
    private WeixinImgGroupService weixinImgGroupService;
    
    @Autowired
    private AttachmentManager attachmentManager;
    
    private final Logger logger = LoggerFactory.getLogger(WeixinNewstemplateController.class);

    /**
     *  微信素材管理列表
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinNewstemplate weixinNewstemplate) {
        int pageSize = 15;
        PageSupport<WeixinNewstemplate> ps = weixinNewstemplateService.getWeixinNewstemplate(curPageNO,weixinNewstemplate,pageSize);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("weixinNewstemplate", weixinNewstemplate);
       return PathResolver.getPath(WeixinAdminPage.WEIXINNEWSTEMPLATE_LIST_PAGE);
    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, WeixinNewstemplate weixinNewstemplate) {
        weixinNewstemplateService.saveWeixinNewstemplate(weixinNewstemplate);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return "forward:/admin/weixinNewstemplate/query.htm";
    }
    
    @RequestMapping(value = "/uploadFile")
    @ResponseBody
    public String uploadFile(HttpServletRequest request, HttpServletResponse response,@RequestParam MultipartFile[] files,@RequestParam Long groupId){
    	
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
    	for (MultipartFile file : files) {
			String fileName = attachmentManager.upload(file);
			WeixinImgFile weixinImgFile = new WeixinImgFile();
			weixinImgFile.setName(file.getOriginalFilename());
			weixinImgFile.setFilePath(fileName);
			weixinImgFile.setFileSize(Long.valueOf(file.getSize()).intValue());
			weixinImgFile.setFileType(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1).toLowerCase());
			weixinImgFile.setCreateDate(new Date());
			weixinImgFile.setCreateUserId(userId);
			weixinImgFile.setImgGoupId(groupId);
			weixinImgFileService.saveWeixinImgFile(weixinImgFile);
		}
    	return "<script>window.parent.uploadSuccess();</script>";
    }
    
    @RequestMapping(value = "/uploadOneFile")
    @ResponseBody
    public String uploadOneFile(HttpServletRequest request, HttpServletResponse response,@RequestParam MultipartFile file){

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
    	
		String fileName = attachmentManager.upload(file);
		WeixinImgFile weixinImgFile = new WeixinImgFile();
		weixinImgFile.setName(file.getOriginalFilename());
		weixinImgFile.setFilePath(fileName);
		weixinImgFile.setFileSize(Long.valueOf(file.getSize()).intValue());
		weixinImgFile.setFileType(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1).toLowerCase());
		weixinImgFile.setCreateDate(new Date());
		weixinImgFile.setCreateUserId(userId);
		weixinImgFile.setImgGoupId(0l);
		weixinImgFileService.saveWeixinImgFile(weixinImgFile);
    	return "<script>window.parent.uploadSuccess('"+fileName+"');</script>";
    }
    
    /**
     * 新建分组
     * @param request
     * @param response
     * @param groupName
     * @return
     */
    @RequestMapping(value = "/saveImgGroup")
    @ResponseBody
    public String saveImgGroup(HttpServletRequest request, HttpServletResponse response,String groupName){

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
    	
    	WeixinImgGroup  weixinImgGroup  = weixinImgGroupService.getWeixinImgGroupByName(groupName);
    	if(AppUtils.isNotBlank(weixinImgGroup)){
    		return Constants.FAIL;
    	}
    	weixinImgGroup = new WeixinImgGroup();
    	weixinImgGroup.setName(groupName);
    	weixinImgGroup.setCreateDate(new Date());
    	weixinImgGroup.setCreateUserId(userId);
    	weixinImgGroupService.saveWeixinImgGroup(weixinImgGroup);
    	return Constants.SUCCESS;
    }
    
    /**
     * 重命名分组
     * @param request
     * @param response
     * @param groupName
     * @param groupId
     * @return
     */
    @RequestMapping(value = "/renameImgGroup")
    @ResponseBody
    public String renameImgGroup(HttpServletRequest request, HttpServletResponse response,String groupName,Long groupId){

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
    	
    	WeixinImgGroup  weixinImgGroup  = weixinImgGroupService.getWeixinImgGroupByName(groupName);
    	if(AppUtils.isNotBlank(weixinImgGroup)){
    		return Constants.FAIL;
    	}
    	
    	weixinImgGroup = weixinImgGroupService.getWeixinImgGroup(groupId);
    	weixinImgGroup.setName(groupName);
    	weixinImgGroup.setUpdateDate(new Date());
    	weixinImgGroup.setUpdateUser(userId);
    	weixinImgGroupService.saveWeixinImgGroup(weixinImgGroup);
    	return Constants.SUCCESS;
    }
    
    /**
     * 删除分组
     * @param request
     * @param response
     * @param groupId
     * @return
     */
    @RequestMapping(value = "/delImgGroup")
    @ResponseBody
    public String delImgGroup(HttpServletRequest request, HttpServletResponse response,Long groupId){
    	WeixinImgGroup  weixinImgGroup = new WeixinImgGroup();
    	weixinImgGroup.setId(groupId);
    	//删除分组
    	weixinImgGroupService.deleteWeixinImgGroup(weixinImgGroup);
/*    	//获取分组下的图片
    	List<WeixinImgFile> weixinImgFiles = weixinImgFileService.getWeixinImgFilesByGroupId(groupId);
    	//将图片的分组改成 未分组
    	for (WeixinImgFile weixinImgFile : weixinImgFiles) {
    		weixinImgFile.setImgGoupId(0l);
		}
    	//更新
    	weixinImgFileService.updateWeixinImgFiles(weixinImgFiles);*/
    	return Constants.SUCCESS;
    }
    
    /**
     * 移动分组
     * @param request
     * @param response
     * @param groupId
     * @param idStr
     * @return
     */
    @RequestMapping(value = "/changeImgGroup")
    @ResponseBody
    public String changeImgGroup(HttpServletRequest request, HttpServletResponse response,Long groupId,String idStr){
    	String[] idArr = idStr.split(",");
    	List<Long> idList = new ArrayList<Long>();
    	for (String str : idArr) {
    		idList.add(Long.valueOf(str));
		}
    	//更新图片的分组
    	weixinImgFileService.changeImgsGroup(idList,groupId);
    	return Constants.SUCCESS;
    }
    
    @RequestMapping(value = "/renameImg")
    @ResponseBody
    public String renameImg(HttpServletRequest request, HttpServletResponse response,Long imgId,String imgName){
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
    	WeixinImgFile  weixinImgFile  = weixinImgFileService.getWeixinImgFile(imgId);
    	weixinImgFile.setName(imgName);
    	weixinImgFile.setUpdateDate(new Date());
    	weixinImgFile.setUpdateUser(userId);
    	weixinImgFileService.updateWeixinImgFile(weixinImgFile);
    	return Constants.SUCCESS;
    }
    
    @RequestMapping(value = "/delImgs")
    @ResponseBody
    public String delImg(HttpServletRequest request, HttpServletResponse response,String idStr){
    	String[] idArr = idStr.split(",");
    	List<Long> idList = new ArrayList<Long>();
    	for (String str : idArr) {
    		idList.add(Long.valueOf(str));
		}
    	weixinImgFileService.deleteWeixinImgFile(idList);
    	return Constants.SUCCESS;
    }
    /**
     * 图片库
     * @param request
     * @param response
     * @param type 分组id
     * @return
     */
    @RequestMapping(value = "/filePage")
    public String filePage(HttpServletRequest request, HttpServletResponse response, String curPageNO,Long type){
    	String groupName = null;
    	if(AppUtils.isBlank(type)||type.equals(0l)){
    		type=0L;
    		groupName="未分组";
    	}else{
    		WeixinImgGroup  weixinImgGroup  = weixinImgGroupService.getWeixinImgGroup(type);
    		groupName = weixinImgGroup.getName();
    	}
    	int pageSize = 12;
    	PageSupport<WeixinImgFile> ps = weixinImgFileService.getWeixinImgFile(curPageNO,pageSize,type);
    	PageSupportHelper.savePage(request, ps);
        
        List<WeixinImgGroup> imgGroups = weixinImgGroupService.getAllGroupsInfo();
        request.setAttribute("imgGroups", imgGroups);
        request.setAttribute("type", type);
        request.setAttribute("groupName", groupName);
    	return PathResolver.getPath(WeixinAdminPage.WEIXIN_FILE_PAGE);
    }
    
    @RequestMapping(value = "/imageDialog")
    public String imageDialog(HttpServletRequest request, HttpServletResponse response, String curPageNO,Long type){
    	int pageSize = 15;
    	String groupName = null;
    	if(AppUtils.isBlank(type)||type.equals(0l)){
    		type=0L;
    		groupName="未分组";
    	}else{
    		WeixinImgGroup  weixinImgGroup  = weixinImgGroupService.getWeixinImgGroup(type);
    		groupName = weixinImgGroup.getName();
    	}
    	PageSupport<WeixinImgFile> ps = weixinImgFileService.getWeixinImgFile(curPageNO,pageSize,type);
    	PageSupportHelper.savePage(request, ps);
        
        List<WeixinImgGroup> imgGroups = weixinImgGroupService.getAllGroupsInfo();
        request.setAttribute("imgGroups", imgGroups);
        request.setAttribute("type", type);
        request.setAttribute("groupName", groupName);
    	return PathResolver.getPath(WeixinBackPage.WEIXIN_IMAGE_DIALOG);
    }
    
    /**
     * 保存图文消息
     * @param request
     * @param response
     * @param newsItemsDto
     * @return
     */
    @RequestMapping(value = "/saveNewsItems")
    public @ResponseBody String saveNewsItems(HttpServletRequest request, HttpServletResponse response, WeixinNewsItemsDto newsItemsDto) {
    	try {

    		SecurityUserDetail user = UserManager.getUser(request);
    		String userId = user.getUserId();
    		
			weixinNewsitemService.saveWeixinNewsitemList(newsItemsDto,userId);
			return Constants.SUCCESS;
		} catch (Exception e) {
			logger.error("保存微信素材失败：{}",e);
			return Constants.FAIL;
		}
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinNewstemplate weixinNewstemplate = weixinNewstemplateService.getWeixinNewstemplate(id);
		weixinNewstemplateService.deleteWeixinNewstemplate(weixinNewstemplate);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/weixinNewstemplate/query.htm";
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        List<WeixinNewsitem> newsItems = weixinNewsitemService.getNewsItemsByTempId(id);
        if(AppUtils.isNotBlank(newsItems)){
        	request.setAttribute("coverItem", newsItems.get(0));
        }
        request.setAttribute("newsItems", newsItems);
        return PathResolver.getPath(WeixinAdminPage.WEIXINNEWSTEMPLATE_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(WeixinAdminPage.WEIXINNEWSTEMPLATE_EDIT_PAGE);
	}

}
