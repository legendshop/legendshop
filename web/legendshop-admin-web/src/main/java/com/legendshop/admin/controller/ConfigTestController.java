/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.admin.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.web.helper.IPHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.dto.ConfigTestDto;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.sms.event.processor.SendMailProcessor;
import com.legendshop.spi.service.SystemParameterService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.EnvironmentConfig;
import com.legendshop.util.FileConfig;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.StringUtil;
import com.legendshop.util.SystemUtil;

/**
 * 系统参数控制器.
 */
@Controller
@RequestMapping("/admin/system/config")
public class ConfigTestController extends BaseController {

    /**
     * The logger.
     */
	private final static Logger logger = LoggerFactory.getLogger(ConfigTestController.class);

    /**
     * 内容容器.
     */
    private static Map<String, Properties> register = new HashMap<String, Properties>();

    @Autowired
    private SystemParameterService systemParameterService;

    @Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private SendMailProcessor sendMailProcessor;

	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;


    /**
     * 获取配置测试列表
     *
     * @param request
     * @param response
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response) {
        initList(request);
        return PathResolver.getPath(AdminPage.CONFIG_TEST_LIST_PAGE);
    }


    /**
     * 获取短信和邮件配置
     *
     * @param groupId
     * @return
     */
    public List<ConfigTestDto> queryOther(String groupId) {
        List<ConfigTestDto> list = new ArrayList<ConfigTestDto>();
        List<SystemParameter> systemParameterList = systemParameterService.getSystemParameterByGroupId(groupId);
        for (SystemParameter systemParameter : systemParameterList) {
            ConfigTestDto dto = new ConfigTestDto();
            dto.setConfigItemName(systemParameter.getMemo());
            dto.setConfigItemParas(systemParameter.getValue());
            list.add(dto);
        }
        return list;
    }


    /**
     * 获取邮件配置，转换为json格式
     *
     * @param groupId
     * @return
     */
    public ConfigTestDto querySMS(String groupId) {
        List<SystemParameter> systemParameterList = systemParameterService.getSystemParameterByGroupId(groupId);
        Map<String, String> map = new HashMap<String, String>();
        for (SystemParameter systemParameter : systemParameterList) {
            map.put(systemParameter.getMemo(), systemParameter.getValue());
        }
        ConfigTestDto dto = new ConfigTestDto();
        dto.setConfigItemName("邮件配置");
        dto.setConfigItemParas(JSONUtil.getJson(map));
        return dto;
    }


	/**
	 * 发送信息测试
	 *
	 * @param request
	 * @param response
	 * @param tel
	 */
	@RequestMapping(value = "/sendSMSCode", method = RequestMethod.POST)
	public String sendSMSCode(HttpServletRequest request, HttpServletResponse response, String tel) {
		String mobileCode = "000000";

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		String ip = "";
		String requestUrl = "";
		try {
			requestUrl = request.getRequestURL().toString();
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("UserController sendSMSCode 发送短信验证码 mobile：{}，ip：{}", tel, ip);

		sendSMSProcessor.process(new SendSMSEvent(tel, mobileCode, userName, StringUtils.isNotBlank(requestUrl) ? requestUrl : "/sendSMSCode", ip, new Date()).getSource());
		request.setAttribute("sendSMSResult", "已发送短信，请检查是否有收到短信");

		initList(request);
		return PathResolver.getPath(AdminPage.CONFIG_TEST_LIST_PAGE);
	}


    /**
     * 发送邮件测试
     *
     * @param request
     * @param response
     * @param email
     */
    @RequestMapping(value = "/sendMail", method = RequestMethod.POST)
    public String sendMail(HttpServletRequest request, HttpServletResponse response, String email) {
        if(StringUtil.isBlank(email))
        {
            return PathResolver.getRedirectPath("/admin/system/config/query");
        }
        String result = sendMailProcessor.sendMailTest(email);
        request.setAttribute("sendMailResult", result);
        initList(request);
        return PathResolver.getPath(AdminPage.CONFIG_TEST_LIST_PAGE);
    }


    /**
     * 上传图片测试
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/sendPic")
    @ResponseBody
    public String sendPic(HttpServletRequest request, HttpServletResponse response, MultipartFile file) {
        return attachmentManager.upload(file);
    }


    /**
     * 传递配置文件路径
     *
     * @param fileName
     * @param name
     */
    private ConfigTestDto getProperties(String fileName, String name) {
        ConfigTestDto dto = new ConfigTestDto();
        Properties p = register.get(fileName);// 将fileName存于一个HashTable
        if (p == null) {
            synchronized (EnvironmentConfig.class) {
                /**
                 * 如果为空就尝试输入进文件
                 */
                try {
                    InputStream is = null;// 定义输入流is
                    try {
                        is = new FileInputStream(fileName);// 创建输入流
                    } catch (Exception e) {
                        if (fileName.startsWith("/"))
                            // 用getResourceAsStream()方法用于定位并打开外部文件。
                            is = EnvironmentConfig.class.getResourceAsStream(fileName);
                        else
                            is = EnvironmentConfig.class.getResourceAsStream("/" + fileName);
                    }
                    p = new Properties();
                    p.load(is);// 加载输入流
                    register.put(fileName, p);// 将其存放于HashTable
                    is.close();// 关闭输入流

                } catch (Exception e) {
                    logger.warn("getProperties: ", e);
                }
            }
        }
        dto.setConfigItemName(name);
        dto.setConfigItemParas(JSONUtil.getJson(p));
        return dto;
    }


    private void initList(HttpServletRequest request){
        List<ConfigTestDto> list = new ArrayList<ConfigTestDto>();
        list.add(getProperties(FileConfig.ConfigFile, "系统配置"));
        list.add(getProperties(FileConfig.GlobalFile, "公用配置"));
        list.add(getProperties(FileConfig.AppConfFile, "App配置"));
        list.add(getProperties(FileConfig.WeiXinConfigFile, "微信配置"));
        list.add(getProperties(FileConfig.JdbcFile, "数据库配置"));
        list.add(getProperties("config/redis.properties", "Redis配置"));
        //获取短信配置
        List<ConfigTestDto> smsList = queryOther("SMS");
        list.addAll(smsList);
        //获取邮件配置
        ConfigTestDto test = querySMS("MAIL");
        list.add(test);

        request.setAttribute("offset", 1);
        request.setAttribute("list", list);
        String contextPath = SystemUtil.getContextPath();
        request.setAttribute("contextPath", contextPath);
    }


}
