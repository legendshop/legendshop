/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CategoryTree;
import com.legendshop.model.entity.AppDecorate;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Theme;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AppDecorateService;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ThemeService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * APP 装修服务中心
 * 
 *
 */
@Controller
@RequestMapping("/admin/app/decorate")
public class AppdecorateAdminController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@Autowired
	private BrandService brandService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private AppDecorateService appDecorateService;

	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	private static ReentrantLock lock = new ReentrantLock();

	/**
	 * APP装饰
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="query",method = RequestMethod.GET)
	public String decorate(HttpServletRequest request, HttpServletResponse response) {
		Long shopId =systemParameterUtil.getDefaultShopId();
		AppDecorate appDecorate = appDecorateService.getAppDecorate(shopId);
		PhotoPathDto photoPathDto = PhotoPathResolver.getInstance().calculateContextPath();
		if (AppUtils.isBlank(appDecorate)) {
			if (lock.tryLock()) {
				try {
					StringBuffer buffer = new StringBuffer(200);
					buffer.append("{\"photoPath\":").append("\"").append(photoPathDto.getFilePath()).append("\"");
					buffer.append(",").append("\"sliders\"").append(":[]");
					buffer.append(",").append("\"adverts\"").append(":[");
					buffer.append("{\"id\":\"\",\"text\":\"\",\"img\":\"\",\"action\":\"\",\"actionParam\":\"\",\"ordering\":1,\"price\":1},");
					buffer.append("{\"id\":\"\",\"text\":\"\",\"img\":\"\",\"action\":\"\",\"actionParam\":\"\",\"ordering\":1,\"price\":2},");
					buffer.append("{\"id\":\"\",\"text\":\"\",\"img\":\"\",\"action\":\"\",\"actionParam\":\"\",\"ordering\":1,\"price\":3},");
					buffer.append("{\"id\":\"\",\"text\":\"\",\"img\":\"\",\"action\":\"\",\"actionParam\":\"\",\"ordering\":1,\"price\":4},");
					buffer.append("]");
					buffer.append(",").append("\"floors\"").append(":[]");
					buffer.append(",").append("\"menus\"").append(":''");
					buffer.append("}");
					appDecorate = new AppDecorate();
					appDecorate.setCreateDate(new Date());
					appDecorate.setShopId(shopId);
					appDecorate.setDecotateData(buffer.toString());
					appDecorateService.saveAppDecorate(appDecorate);
				} finally {
					lock.unlock();
				}
			}
		} else {
			String data = appDecorate.getDecotateData();
			if (AppUtils.isNotBlank(data) && data.indexOf(photoPathDto.getFilePath()) < 0) {
				data = data.substring(data.indexOf(","), data.length());
				StringBuffer buffer = new StringBuffer(250);
				buffer.append("{\"photoPath\":\"").append(photoPathDto.getFilePath()).append("\"").append(data);
				appDecorate.setDecotateData(buffer.toString());
			}
		}
		request.setAttribute("decotateData", appDecorate.getDecotateData());
		String path = PathResolver.getPath(AdminPage.APP_DECORATE);
		return path;
	}

	/**
	 * 保存APP装饰
	 * 
	 * @param request
	 * @param response
	 * @param decotateData
	 * @return
	 */
	@SystemControllerLog(description="保存APP装饰")
	@RequestMapping(value = "/saveAppIndex", method = RequestMethod.POST)
	public @ResponseBody String saveAppIndex(HttpServletRequest request, HttpServletResponse response, String decotateData) {
		if (AppUtils.isBlank(decotateData)) {
			return "请装修";
		}
		Long shopId = systemParameterUtil.getDefaultShopId();
		AppDecorate appDecorate = appDecorateService.getAppDecorate(shopId);
		if (AppUtils.isBlank(appDecorate)) {
			if (lock.tryLock()) {
				try {
					appDecorate = new AppDecorate();
					appDecorate.setCreateDate(new Date());
					appDecorate.setShopId(shopId);
				} finally {
					lock.unlock();
				}
			}
		}
		appDecorate.setDecotateData(decotateData);
		appDecorateService.updateAppDecorate(appDecorate);
		return Constants.SUCCESS;
	}

	/**
	 * 挑选类别
	 * 
	 * @param request
	 * @param response
	 * @param sourceId
	 * @return
	 */
	@RequestMapping(value = "/loadAllCategory")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response, String sourceId) {
		List<Category> catList = categoryService.getCategoryList("P");
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		for (Category cat : catList) {
			CategoryTree tree = new CategoryTree(cat.getId(), cat.getParentId(), cat.getName());
			catTrees.add(tree);
		}
		request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
		request.setAttribute("sourceId", sourceId);
		return PathResolver.getPath(BackPage.APPDECORATE_LOADCATEGORY_PAGE);
	}

	/**
	 * 挑选商品弹层
	 */
	@RequestMapping("/loadProds")
	public String loadProds(HttpServletRequest request, HttpServletResponse response, String curPageNO, String name, String sourceId, Integer maxChoose) {

		PageSupport<Product> ps = productService.getProductList(name, curPageNO,null);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("name", name);
		request.setAttribute("sourceId", sourceId);
		request.setAttribute("maxChoose", maxChoose);
		String result = PathResolver.getPath(BackPage.APPDECORATE_LOADPRODS_PAGE);
		return result;
	}

	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadThemes")
	public String loadThemes(HttpServletRequest request, HttpServletResponse response, String curPageNO, String name, String sourceId, Integer maxChoose) {
		PageSupport<Theme> ps = themeService.getThemePage(curPageNO, name);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("name", name);
		request.setAttribute("sourceId", sourceId);
		String result = PathResolver.getPath(BackPage.APPDECORATE_LOADTHEMES_PAGE);
		return result;
	}

	/**
	 * 挑选品牌弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadBrands")
	public String loadBrands(HttpServletRequest request, HttpServletResponse response, String curPageNO, String brandName, String sourceId) {
		PageSupport<Brand> ps = brandService.getBrandsPage(curPageNO, brandName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("brandName", brandName);
		request.setAttribute("sourceId", sourceId);
		String result = PathResolver.getPath(BackPage.APPDECORATE_LOADBRANDS_PAGE);
		return result;
	}

	/**
	 * 挑选图片弹层
	 * 
	 * @param request
	 * @param response
	 * @param sourceId
	 * @param showsize
	 *            720*200
	 * @return
	 */
	@RequestMapping("/loadImages")
	public String loadImages(HttpServletRequest request, HttpServletResponse response, String sourceId, String showsize) {
		request.setAttribute("sourceId", sourceId);
		request.setAttribute("showsize", showsize);
		String result = PathResolver.getPath(BackPage.APPDECORATE_LOADIMAGES_PAGE);
		return result;
	}

	/**
	 * 上传商品图片文件
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/prodpicture/save")
	public @ResponseBody String savePhoto(MultipartHttpServletRequest request, HttpServletResponse response, @RequestParam("files[]") MultipartFile[] files) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(files)) {
			try {
				for (MultipartFile file : files) {
					if (file.getSize() > 0) {
						String pic = attachmentManager.upload(file);
						attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.ADVERTISEMENT);

					}
				}
				return Constants.SUCCESS;
			} catch (Exception e) {
				return Constants.FAIL;
			}
		} else {
			return "请上传图片";
		}
	}

}
