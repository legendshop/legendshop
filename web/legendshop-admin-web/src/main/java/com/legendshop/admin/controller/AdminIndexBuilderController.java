package com.legendshop.admin.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdvancedSearchAdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.advanced.search.model.IndexReindexArgs;
import com.legendshop.advanced.search.service.IndexBuilderHandler;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.util.AppUtils;

/**
 * 后台索引构架服务
 *
 */
@Controller
@RequestMapping("/admin/system/index")
public class AdminIndexBuilderController extends BaseController {

	private static final Logger LOGGER=LoggerFactory.getLogger(AdminIndexBuilderController.class);

	@Autowired
	private IndexBuilderHandler indexBuilderHandler;
	
	
	/**
	 * Query.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		String result= PathResolver.getPath(AdvancedSearchAdminPage.LUCENE_EDIT_PAGE);
		return result;
	}
	
	/**
	 * 重建索引
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@SystemControllerLog(description="重建索引")
	@RequestMapping("/rebuildIndex")
	public String reindex(HttpServletRequest request, HttpServletResponse response) {
		IndexReindexArgs args = this.buildReindexArgs(request);
		boolean recreate = "recreate".equals(request.getParameter("indexOperationType"));
		LOGGER.info("rebuildIndex starting, recreate {} ", recreate);
		indexBuilderHandler.startIndexBuildProcess(args, recreate);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.SYSTEM_INDEX_QUERY);
	}
	
	private IndexReindexArgs buildReindexArgs(HttpServletRequest request) {
			String fromDateStr = request.getParameter("fromDate");
			String toDateStr=request.getParameter("toDate");
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			Date fromDate=null;
			Date toDate=null;
			try {
				if (AppUtils.isNotBlank(fromDateStr) && AppUtils.isNotBlank(toDateStr)) {
					fromDate = dateFormat.parse(fromDateStr);
					toDate = dateFormat.parse(toDateStr);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			int firstPostId = 0;
			int lastPostId = 0;
			String entityType = null;

			if (AppUtils.isNotBlank(request.getParameter("firstPostId"))) {
				firstPostId = Integer.valueOf(request.getParameter("firstPostId"));
			}

			if (AppUtils.isNotBlank(request.getParameter("lastPostId"))) {
				lastPostId = Integer.valueOf(request.getParameter("lastPostId"));
			}

			if (AppUtils.isNotBlank(request.getParameter("entityType"))) {
				entityType = request.getParameter("entityType");
			}

			return new IndexReindexArgs(fromDate, toDate, firstPostId, lastPostId, Integer.valueOf(request.getParameter("type")), entityType);
		}
}
