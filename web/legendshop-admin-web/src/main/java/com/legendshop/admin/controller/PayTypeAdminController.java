/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.ConflictException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 支付方式控制器
 */
@Controller
@RequestMapping("/admin/paytype")
public class PayTypeAdminController extends BaseController {

	/** The pay type service. */
	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查找支付方式.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param payType
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, PayType payType) {
		PageSupport<PayType> ps = payTypeService.getPayTypeListPage(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", payType);
		return PathResolver.getPath(AdminPage.PAYTYPE_LIST_PAGE);
	}

	// 打开支付设置编辑
	@RequestMapping(value = "/paySiteSetting/{payTypeId}")
	public String paySiteSetting(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String payTypeId) {
		
		PayType payType = payTypeService.getPayTypeById(payTypeId);
		if (AppUtils.isBlank(payType)) {
			throw new ConflictException("未找到支付方式");
		}
		request.setAttribute("payType", payType);
		JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
		request.setAttribute("jsonObject", jsonObject);
		if (PayTypeEnum.ALI_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.ALIPAY_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.WX_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.WX_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.WX_APP_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.WX_APP_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.WX_MP_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.WX_MP_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.TONGLIAN_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.TONGLIAN_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.TENPAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.TENPAY_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.SIMULATE_PAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.SIMULATE_PAYTYPE_EDIT);
		}
		if (PayTypeEnum.UNIONPAY.value().equals(payTypeId)) {
			return PathResolver.getPath(BackPage.UNION_PAYTYPE_EDIT);
		}
		return payTypeId;
	}
	
	
	/**
	 * 编辑更新支付宝支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新支付宝支付")
	@RequestMapping(value = "/payEditAlipay", method = RequestMethod.POST)
	@ResponseBody
	public String payEditAlipay(HttpServletRequest request, HttpServletResponse response, String seller_email, String partner, String key,
			String appId, String rsa_private, String rsa_publish, String[] use, Integer isEnable) {
		
		if (AppUtils.isBlank(seller_email))
			return "请输入支付宝企业账户";
		if (AppUtils.isBlank(partner))
			return "请输入合作者身份(PID)";
		if (AppUtils.isBlank(key))
			return "请输入安全校验码";
		if (AppUtils.isBlank(appId))
			return "请输入开放平台应用ID";
		if (AppUtils.isBlank(rsa_private))
			return "请输入商户应用私钥";
		if (AppUtils.isBlank(rsa_publish))
			return "请输入支付宝公钥";
		if (AppUtils.isBlank(isEnable))
			isEnable = 0;

		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.ALI_PAY.value());
		JSONObject object = new JSONObject();
		object.put("seller_email", seller_email);
		object.put("partner", partner);
		object.put("key", key);
		object.put("appId", appId);
		object.put("rsa_private", rsa_private);
		object.put("rsa_publish", rsa_publish);
		if (AppUtils.isNotBlank(use)) {
			JSONObject useJson = new JSONObject();
			for (String str : use) {
				useJson.put(str, str);
			}
			object.put("use", useJson);
		}
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(object.toJSONString());

		payTypeService.update(pay);
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 编辑更新微信APP支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新微信APP支付")
	@RequestMapping(value = "/payEditWxApppay", method = RequestMethod.POST)
	@ResponseBody
	public String payEditWxApppay(HttpServletRequest request, HttpServletResponse response, String APPID, String MCH_ID,
			String APPSCECRET,
			String PARTNERKEY,
			Integer isEnable,
			Integer isRefund,
			/** The file. */
			MultipartFile file
			) {
		if (AppUtils.isBlank(APPID))
			return "请输入APPID";
		if (AppUtils.isBlank(MCH_ID))
			return "请输入Mchid";
		if (AppUtils.isBlank(APPSCECRET))
			return "请输入APPSCECRET";
		if (AppUtils.isBlank(PARTNERKEY))
			return "请输入PARTNERKEY";
		if (AppUtils.isBlank(isEnable))
			isEnable = 0;
		if (AppUtils.isBlank(isRefund))
			isRefund = 0;
		
		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.WX_APP_PAY.value());
		
		
		JSONObject paymentConfig = new JSONObject();
		String path = "";
		/** 处理退款证书文件 **/
		if (file != null && file.getSize() > 0) {
			String fileName = file.getOriginalFilename();
			String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
			if (!"p12".equals(prefix)) {
				return "请上传有效的文件格式";
			}
			path = attachmentManager.uploadPrivateFile(file);
			paymentConfig.put("refundFile", path);
		}

		JSONObject object = JSONObject.parseObject(pay.getPaymentConfig());
		if (object != null) {
			if ( AppUtils.isNotBlank(object.getString("refundFile")) && AppUtils.isNotBlank(path)) {
				attachmentManager.deleteTruely(object.getString("refundFile"));
			}else{
				paymentConfig.put("refundFile", object.getString("refundFile"));
			}
		}
		
		paymentConfig.put("APPID", APPID);
		paymentConfig.put("MCH_ID", MCH_ID);
		paymentConfig.put("APPSCECRET", APPSCECRET);
		paymentConfig.put("PARTNERKEY", PARTNERKEY);
		paymentConfig.put("isRefund", isRefund);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);
		
		return Constants.SUCCESS;
	}
	
	/**
	 * 编辑更新微信小程序支付
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新微信小程序支付")
	@RequestMapping(value = "/payEditWxMpPay", method = RequestMethod.POST)
	@ResponseBody
	public String payEditWxMpPay(HttpServletRequest request, HttpServletResponse response, String APPID, String MCH_ID,
			String APPSECRET,
			String PARTNERKEY,
			Integer isEnable,
			Integer isRefund,
			/** The file. */
			MultipartFile file
			) {
		if (AppUtils.isBlank(APPID))
			return "请输入APPID";
		if (AppUtils.isBlank(MCH_ID))
			return "请输入Mchid";
		if (AppUtils.isBlank(APPSECRET))
			return "请输入APPSECRET";
		if (AppUtils.isBlank(PARTNERKEY))
			return "请输入PARTNERKEY";
		if (AppUtils.isBlank(isEnable))
			isEnable = 0;
		if (AppUtils.isBlank(isRefund))
			isRefund = 0;
		
		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.WX_MP_PAY.value());
		
		
		JSONObject paymentConfig = new JSONObject();
		String path = "";
		/** 处理退款证书文件 **/
		if (file != null && file.getSize() > 0) {
			String fileName = file.getOriginalFilename();
			String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
			if (!"p12".equals(prefix)) {
				return "请上传有效的文件格式";
			}
			path = attachmentManager.uploadPrivateFile(file);
			paymentConfig.put("refundFile", path);
		}
		
		JSONObject object = JSONObject.parseObject(pay.getPaymentConfig());
		if (object != null) {
			if ( AppUtils.isNotBlank(object.getString("refundFile")) && AppUtils.isNotBlank(path)) {
				attachmentManager.deleteTruely(object.getString("refundFile"));
			}else{
				paymentConfig.put("refundFile", object.getString("refundFile"));
			}
		}
		
		paymentConfig.put("APPID", APPID);
		paymentConfig.put("MCH_ID", MCH_ID);
		paymentConfig.put("APPSECRET", APPSECRET);
		paymentConfig.put("PARTNERKEY", PARTNERKEY);
		paymentConfig.put("isRefund", isRefund);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);
		
		return Constants.SUCCESS;
	}

	

	/**
	 * 编辑更新微信公众号支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新微信公众号支付")
	@RequestMapping(value = "/payEditWxpay", method = RequestMethod.POST)
	@ResponseBody
	public String payEditWxpay(HttpServletRequest request, HttpServletResponse response, String APPID, String APPSECRET, String MCH_ID,
			String PARTNERKEY, String TOKEN, Integer isEnable, Integer isRefund,
			/** The file. */
			MultipartFile file) {
		if (AppUtils.isBlank(APPID))
			return "请输入APPID";
		if (AppUtils.isBlank(APPSECRET))
			return "请输入APPSECRET";
		if (AppUtils.isBlank(MCH_ID))
			return "请输入Mchid";
		if (AppUtils.isBlank(PARTNERKEY))
			return "请输入PARTNERKEY";
		if (AppUtils.isBlank(TOKEN))
			return "请输入TOKEN";
		if (AppUtils.isBlank(isEnable))
			isEnable = 0;
		if (AppUtils.isBlank(isRefund))
			isRefund = 0;

		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		JSONObject paymentConfig = new JSONObject();
		String path = "";

		/** 处理退款证书**/
		if (file != null && file.getSize() > 0) {
			String fileName = file.getOriginalFilename();
			String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
			if (!"p12".equals(prefix)) {
				return "请上传有效的文件格式";
			}
			path = attachmentManager.uploadPrivateFile(file);
			paymentConfig.put("refundFile", path);
		}

		JSONObject object = JSONObject.parseObject(pay.getPaymentConfig());
		if (object != null) {
			if ( AppUtils.isNotBlank(object.getString("refundFile")) && AppUtils.isNotBlank(path)) {
				attachmentManager.deleteTruely(object.getString("refundFile"));
			}else{
				paymentConfig.put("refundFile", object.getString("refundFile"));
			}
		}

		paymentConfig.put("APPID", APPID);
		paymentConfig.put("APPSECRET", APPSECRET);
		paymentConfig.put("MCH_ID", MCH_ID);
		paymentConfig.put("PARTNERKEY", PARTNERKEY);
		paymentConfig.put("TOKEN", TOKEN);
		paymentConfig.put("isRefund", isRefund);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);

		return Constants.SUCCESS;
	}

	
	/**
	 * 编辑更新通联支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新通联支付")
	@RequestMapping(value = "/payEditTongLian", method = RequestMethod.POST)
	@ResponseBody
	public String payEditTongLian(HttpServletRequest request, HttpServletResponse response, String key,  String merchantId,
			Integer isEnable,
			MultipartFile file) {
		
		if (AppUtils.isBlank(key))
			return "请输入商家密钥";
		if (AppUtils.isBlank(merchantId))
			return "请输入商家的商户号";

		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.TONGLIAN_PAY.value());
		JSONObject paymentConfig = new JSONObject();
		String path = "";
		
		/** 处理品牌 logo **/
		if (file != null && file.getSize() > 0) {
			String fileName = file.getOriginalFilename();
			String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
			if (!"cer".equals(prefix)) {
				return "请上传有效的文件格式";
			}
			path = attachmentManager.upload(file);
			paymentConfig.put("refundFile", path);
		}
		
		JSONObject object = JSONObject.parseObject(pay.getPaymentConfig());
		if (object != null) {
			if ( AppUtils.isNotBlank(object.getString("payFile")) && AppUtils.isNotBlank(path)) {
				attachmentManager.deleteTruely(object.getString("payFile"));
			}else{
				paymentConfig.put("refundFile", object.getString("payFile"));
			}
		}
		
		paymentConfig.put("key", key);
		paymentConfig.put("merchantId", merchantId);
		paymentConfig.put("payFile", path);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 编辑更新财付通支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新财付通支付")
	@RequestMapping(value = "/payEditTDP", method = RequestMethod.POST)
	@ResponseBody
	public String payEditTDP(HttpServletRequest request, HttpServletResponse response, String key,  String bargainorId,
			Integer isEnable) {
		
		if (AppUtils.isBlank(key))
			return "请输入商家密钥";
		if (AppUtils.isBlank(bargainorId))
			return "请输入商家的商户号";

		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.TENPAY.value());
		JSONObject paymentConfig = new JSONObject();
		paymentConfig.put("key", key);
		paymentConfig.put("bargainorId", bargainorId);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);
		return Constants.SUCCESS;
	}
	
	

	/**
	 * 编辑更新模拟支付
	 * 
	 * @param request
	 * @param response
	 * @param payType
	 * @return
	 */
	@SystemControllerLog(description="编辑更新模拟支付")
	@RequestMapping(value = "/payEditSimulate", method = RequestMethod.POST)
	@ResponseBody
	public String payEditSimulate(HttpServletRequest request, HttpServletResponse response, String key,  String token,
			Integer isEnable) {
		
		if (AppUtils.isBlank(key))
			return "请输入商家密钥";
		if (AppUtils.isBlank(token))
			return "请输入商家的token";

		PayType pay = payTypeService.getPayTypeById(PayTypeEnum.SIMULATE_PAY.value());
		JSONObject paymentConfig = new JSONObject();
		paymentConfig.put("key", key);
		paymentConfig.put("token", token);
		pay.setIsEnable(isEnable);
		pay.setPaymentConfig(paymentConfig.toJSONString());
		payTypeService.update(pay);
		return Constants.SUCCESS;
	}
	
	


}
