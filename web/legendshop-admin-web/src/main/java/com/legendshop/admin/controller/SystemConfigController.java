/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.vo.WechatCodeVO;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 系统配置控制器
 *
 */
@Controller
@RequestMapping("/admin/system/systemConfig")
public class SystemConfigController extends BaseController {
	
	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 保存系统全局配置
	 * @param request
	 * @param response
	 * @param systemConfig
	 * @return
	 */
	@SystemControllerLog(description="保存系统全局配置")
	@RequestMapping(value = "/save")
	public String save(MultipartHttpServletRequest request, HttpServletResponse response, SystemConfig systemConfig) {
		String logoPic = null;
		String wechatTitlePic = null;
		String wechatOrderPic = null;
		String wechatServicePic = null;
		SystemConfig originSystemConfig = systemConfigService.getSystemConfig();
		originSystemConfig.setIcpInfo(systemConfig.getIcpInfo());
		originSystemConfig.setShopName(systemConfig.getShopName());
		originSystemConfig.setUrl(systemConfig.getUrl());
		originSystemConfig.setSupportMail(systemConfig.getSupportMail());
		originSystemConfig.setTelephone(systemConfig.getTelephone());
		originSystemConfig.setQqNumber(systemConfig.getQqNumber());
		originSystemConfig.setInternationalCode(systemConfig.getInternationalCode());
		originSystemConfig.setAreaCode(systemConfig.getAreaCode());
		originSystemConfig.setTitle(systemConfig.getTitle());
		originSystemConfig.setKeywords(systemConfig.getKeywords());
		originSystemConfig.setDescription(systemConfig.getDescription());
		SecurityUserDetail user = UserManager.getUser(request);
		// String subPath = "/common/images/";
		if (systemConfig.getFile() != null && systemConfig.getFile().getSize() > 0) {

			// logoPic =
			// FileProcessor.uploadFileAndCallback(systemConfig.getFile(),
			// subPath, "");
			logoPic = attachmentManager.upload(systemConfig.getFile());
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), logoPic, systemConfig.getFile(), AttachmentTypeEnum.SYSTEMCONFIG);
			// 删除原引用
			if (AppUtils.isNotBlank(systemConfig.getLogo())) {
				attachmentManager.delAttachmentByFilePath(systemConfig.getLogo());
				attachmentManager.deleteTruely(systemConfig.getLogo());
			}
			originSystemConfig.setLogo(logoPic);
		}

		String adminLogoCode = null;
		if (systemConfig.getAdminLogoFile() != null && systemConfig.getAdminLogoFile().getSize() > 0) {
			adminLogoCode = attachmentManager.upload(systemConfig.getAdminLogoFile());
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), adminLogoCode, systemConfig.getAdminLogoFile(), AttachmentTypeEnum.SYSTEMCONFIG);
			// 删除原引用
			if (AppUtils.isNotBlank(systemConfig.getLogo())) {
				attachmentManager.delAttachmentByFilePath(systemConfig.getAdminLogoCode());
				attachmentManager.deleteTruely(systemConfig.getAdminLogoCode());
			}
			originSystemConfig.setAdminLogoCode(adminLogoCode);
		}

		if (systemConfig.getWechatCodeFile() != null && systemConfig.getWechatCodeFile().getSize() > 0) {
			//
			// //logoPic =
			// FileProcessor.uploadFileAndCallback(systemConfig.getFile(),
			// subPath, "");
			wechatOrderPic = attachmentManager.upload(systemConfig.getWechatCodeFile());
		}

		if (systemConfig.getWechatServiceCodeFile() != null && systemConfig.getWechatServiceCodeFile().getSize() > 0) {
			wechatServicePic = attachmentManager.upload(systemConfig.getWechatServiceCodeFile());
		}
		if (AppUtils.isNotBlank(wechatOrderPic) || AppUtils.isNotBlank(wechatServicePic)) {
			WechatCodeVO wechatCodeVO = JSONUtil.getObject(systemConfig.getWechatCode(), WechatCodeVO.class);
			if (AppUtils.isNotBlank(wechatOrderPic)) {
				wechatCodeVO.setWechatOrderCode(wechatOrderPic);
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), wechatOrderPic, systemConfig.getWechatCodeFile(), AttachmentTypeEnum.SYSTEMCONFIG);
			}
			if (AppUtils.isNotBlank(wechatServicePic)) {
				wechatCodeVO.setWechatServiceCode(wechatServicePic);
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), wechatServicePic, systemConfig.getWechatServiceCodeFile(), AttachmentTypeEnum.SYSTEMCONFIG);
			}

			// 删除原引用
			if (AppUtils.isNotBlank(systemConfig.getWechatCode())) {
				attachmentManager.delAttachmentByFilePath(systemConfig.getWechatCode());
				attachmentManager.deleteTruely(systemConfig.getWechatCode());
			}
			String wechatCodeString = JSONUtil.getJson(wechatCodeVO);
			originSystemConfig.setWechatCode(wechatCodeString);
		}
		if (systemConfig.getWechatTitlePicFile() != null && systemConfig.getWechatTitlePicFile().getSize() > 0) {

			wechatTitlePic = attachmentManager.upload(systemConfig.getWechatTitlePicFile());
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), wechatTitlePic, systemConfig.getWechatTitlePicFile(), AttachmentTypeEnum.SYSTEMCONFIG);
			// 删除原引用
			if (AppUtils.isNotBlank(systemConfig.getWechatTitlePic())) {
				attachmentManager.delAttachmentByFilePath(systemConfig.getWechatTitlePic());
				attachmentManager.deleteTruely(systemConfig.getWechatTitlePic());
			}
			originSystemConfig.setWechatTitlePic(wechatTitlePic);
		}

		systemConfigService.updateSystemConfig(originSystemConfig);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.SYSTEM_CONFIG_QUERY);
	}

	/**
	 * 加载全局配置页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String wechatCode = systemConfig.getWechatCode();
		WechatCodeVO wechatCodeVO = JSONUtil.getObject(wechatCode, WechatCodeVO.class);
		request.setAttribute("systemConfig", systemConfig);
		request.setAttribute("wechatCodeVO", wechatCodeVO);
		return PathResolver.getPath(AdminPage.SYSTEMCONFIG_EDIT_PAGE);
	}

}
