/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Hotsearch;
import com.legendshop.spi.service.HotsearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 热门搜索控制器.
 */
@Controller
@RequestMapping("/admin/hotsearch")
public class HotsearchAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(HotsearchAdminController.class);

	/** 热门搜索服务. */
	@Autowired
	private HotsearchService hotsearchService;

	/**
	 * 查找热门搜索.
	 *
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Hotsearch hotsearch) {
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request,new String[]{"seq"});
		
		PageSupport<Hotsearch> ps = hotsearchService.queryHotsearch(result,curPageNO,hotsearch);

		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", hotsearch);
		return PathResolver.getPath(AdminPage.HOT_LIST_PAGE);
	}

	/**
	 * 保存热门搜索.
	 *
	 */
	@SystemControllerLog(description="保存热门搜索商品")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Hotsearch hotsearch) {
		
		hotsearch.setMsg(XssFilterUtil.cleanXSS(hotsearch.getMsg()));
		hotsearch.setTitle(XssFilterUtil.cleanXSS(hotsearch.getTitle()));
		if(hotsearch.getId() != null){//update
				Hotsearch entity = hotsearchService.getHotsearchById(hotsearch.getId());
				if (entity != null) {
					//update
					entity.setDate(new Date());
					entity.setMsg(hotsearch.getMsg());
					entity.setTitle(hotsearch.getTitle());
					entity.setSeq(hotsearch.getSeq());
					entity.setStatus(hotsearch.getStatus());
					hotsearchService.updateHotsearch(entity);
				}
		}else{//save
			hotsearch.setDate(new Date());
			hotsearchService.saveHotsearch(hotsearch);
		}
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(RedirectPage.HOT_LIST_QUERY);
	}

	/**
	 * 删除热门搜索.
	 *
	 */
	@SystemControllerLog(description="删除热门搜索商品")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Hotsearch hotsearch = hotsearchService.getHotsearchById(id);
		if(hotsearch != null){
			log.info("{} delete Hotsearch Title {}, Msg {}",hotsearch.getTitle(), hotsearch.getMsg());
			hotsearchService.delete(id);
			saveMessage(request, ResourceBundleHelper.getDeleteString());
		}
		return PathResolver.getPath(RedirectPage.HOT_LIST_QUERY);
	}

	/**
	 * 根据id加载热门商品编辑页.
	 *
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Hotsearch hotsearch = hotsearchService.getHotsearchById(id);
		request.setAttribute("bean", hotsearch);
		return PathResolver.getPath(AdminPage.HOT_EDIT_PAGE);
	}

	/**
	 * 更新热门商品
	 *
	 */
	@SystemControllerLog(description="更新热门搜索商品")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Hotsearch hotsearch) {
		Hotsearch origin = hotsearchService.getHotsearchById(hotsearch.getId());
		log.info("{} update Hotsearch Title{}", origin.getTitle());
		hotsearchService.updateHotsearch(hotsearch);
		return PathResolver.getPath(FowardPage.HOT_LIST_QUERY);
	}

	/**
	 * 加载热门商品编辑页.
	 *
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.HOT_EDIT_PAGE);
	}
	
	/**
	 * 更新状态
	 */
	@SystemControllerLog(description="更新热门搜索商品状态")
	@RequestMapping(value = "/updatestatus/{hotId}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long hotId,
			@PathVariable Integer status) {
		Hotsearch hotsearch = hotsearchService.getHotsearchById(hotId);
		if (hotsearch == null) {
			return -1;
		}
		if (!status.equals(hotsearch.getStatus())) {
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				hotsearch.setStatus(status);
				hotsearchService.updateHotsearch(hotsearch);
			}
		}
		return hotsearch.getStatus();
	}

}
