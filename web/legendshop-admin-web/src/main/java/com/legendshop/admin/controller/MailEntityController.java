/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailEntity;
import com.legendshop.spi.service.MailEntityService;

/**
 * 邮件
 *
 */
@Controller
@RequestMapping("/admin/mailEntity")
public class MailEntityController extends BaseController{
    @Autowired
    private MailEntityService mailEntityService;
    
    /**
     * 查询邮件
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, MailEntity mailEntity) {
        
        PageSupport<MailEntity> ps = mailEntityService.getMailEntityPage(curPageNO);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("mailEntity", mailEntity);
        
        return "/mailEntity/mailEntityList";
        //TODO, replace by next line, need to predefined BackPage parameter
       // return PathResolver.getPath(request, response, BackPage.MAILENTITY_LIST_PAGE);
    }

    /**
     * 保存邮件
     */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, MailEntity mailEntity) {
        mailEntityService.saveMailEntity(mailEntity);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return "forward:/admin/mailEntity/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
		//return PathResolver.getPath(request, response, FowardPage.MAILENTITY_LIST_QUERY);
    }
    
    /**
     * 删除邮件
     */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        MailEntity mailEntity = mailEntityService.getMailEntity(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), mailEntity.getUserName());
		//if(result!=null){
			//return result;
		//}
		mailEntityService.deleteMailEntity(mailEntity);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/mailEntity/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
        //return PathResolver.getPath(request, response, FowardPage.MAILENTITY_LIST_QUERY);
    }
    
    /**
     * 根据id加载邮件编辑
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        MailEntity mailEntity = mailEntityService.getMailEntity(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), mailEntity.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("mailEntity", mailEntity);
        return "/mailEntity/mailEntity";
         //TODO, replace by next line, need to predefined FowardPage parameter
         //return PathResolver.getPath(request, response, BackPage.MAILENTITY_EDIT_PAGE);
    }
    
    /**
     * 加载邮件编辑
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/mailEntity/mailEntity";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.MAILENTITY_EDIT_PAGE);
	}
	
	/**
	 * 更新
	 */
    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        MailEntity mailEntity = mailEntityService.getMailEntity(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), mailEntity.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("mailEntity", mailEntity);
		return "forward:/admin/mailEntity/query.htm";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.MAILENTITY_EDIT_PAGE);
    }

}
