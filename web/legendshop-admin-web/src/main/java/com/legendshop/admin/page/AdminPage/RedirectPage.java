/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.AdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 管理员的页面定义.
 */
public enum RedirectPage implements PageDefinition {

	/** 商品管理首页 */
	ADMIN_INDEX_QUERY("/admin/index"),

	/** 首页楼层 */
	FLOOR_LIST_QUERY("/admin/floor/query"),

	/** 商品类目编辑 */
	CATEGORY_LIST_QUERY("/admin/category/query"),

	/**  运单模版列表 */
	PRINTTMPL_LIST_QUERY("/admin/printTmpl/query"),

	/** 品牌列表 */
	BRAND_LIST_QUERY("/admin/brand/query"),

	/** 店铺列表 */
	SHOP_DETAIL_LIST_QUERY("/admin/shopDetail/query"),

	/** 商品参数列表 */
	PRODUCTPROPERTY_LIST_QUERY("/admin/productProperty/query"),

	/** 参数组列表 */
	PARAMGROUP_LIST_QUERY("/admin/paramGroup/query"),

	/** 规格 类型管理 */
	PRODTYPE_LIST_QUERY("/admin/prodType/query"),

	/** 热门商品列表 */
	HOT_LIST_QUERY("/admin/hotsearch/query"),

	/** 咨询管理 */
	PROD_CONSULT_LIST_QUERY("/admin/productConsult/query"),

	/** 图片管理  */
	IJPG_LIST_QUERY("/admin/indexjpg/query"),

	/** 图片列表 */
	IJPG_MOBILE_QUERY("/admin/indexjpg/mobile/query"),

	/** 公告管理 */
	PUB_LIST_QUERY("/admin/pub/query"),

	/** 菜单管理 */
	HEADMENU_LIST_QUERY("/admin/headmenu/query"),

	/** 链接管理 */
	LINK_LIST_QUERY("/admin/externallink/query"),

	/** 支付管理 */
	PAY_TYPE_LIST_QUERY("/admin/paytype/query"),

	/** 广告管理 */
	ADV_LIST_QUERY("/admin/advertisement/query"),

	/** 供应商管理 */
	PARTNER_LIST_QUERY("/admin/partner/query"),

	/** 物流公司管理 */
	DELIVERYCORP_LIST_QUERY("/admin/deliveryCorp/query"),

	/** 配送方式管理 */
	DELIVERYTYPE_LIST_QUERY("/admin/deliveryType/query"),

	/** 举报管理 */
	ACCUSATION_LIST_QUERY("/admin/accusation/query"),

	/** 举报类型 */
	ACCUSATIONTYPE_LIST_QUERY("/admin/accusationType/query"),

	/** 栏目管理 */
	NEWSCAT_LIST_QUERY("/admin/newsCategory/query"),

	/**  文章管理 */
	NEWS_LIST_QUERY("/admin/news/query"),
	
	/** 用户等级 */
	USERGRADE_LIST_QUERY("/admin/system/userGrade/query"),

	/**  商家等级 */
	SHOPGRADE_LIST_QUERY("/admin/system/shopGrade/query"),

	/** 全局配置 */
	SYSTEM_CONFIG_QUERY("/admin/system/systemConfig/load"),

	/** 参数管理 */
	PARAM_LIST_QUERY("/admin/system/systemParameter/query"),

	/** 导航管理 */
	NAVIGATION_LIST_QUERY("/admin/system/navigation/query"),

	/** 敏感字管理 */
	SENSITIVEWORD_LIST_QUERY("/admin/system/sensitiveWord/query"),

	/** 发送系统通知 */
	SYSTEM_MESSAGES_QUERY("/admin/system/Message/query"),

	/** 管理员列表 */
	ADMINUSER_LIST_QUERY("/admin/adminUser/query"),

	/** 首页楼层 */
	MOBILEFLOOR_LIST_QUERY("/admin/mobileFloor/query"),

	/**标签管理  */
	TAG_QUERY("/admin/tag/query"),

	/** 地区管理 */
	DISTRICT_LIST_PAGE("/admin/system/district/query"),

	/** 分类导航管理 */
	CATEGORY_RECOMM_LIST("/admin/category/recomm/query"),

	/** 订单列表页 */
	PROCESSING_ORDER_LIST("/admin/order/processing"),

	/** 头条管理 */
	HEADLINE_LIST("/admin/headlineAdmin/query"),

	/** 帮助管理 */
	ADMIN_DOC_LIST("/admin/adminDoc/query"),

	/** 文章管理 */
	ARTICLE_lIST("/admin/article/query"),

	/** 文章评论管理 */
	ARTICLE_COMMENT_LIST("/admin/article/comment/query"),
	
	/** 搜索  **/
	SYSTEM_INDEX_QUERY("/admin/system/index/query"), 

	;

	/** The value. */
	private final String value;

	private RedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 */
	private RedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
