/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Accusation;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.AccusationService;

/**
 * 投诉
 *
 */
@Controller
@RequestMapping("/admin/accusation")
public class AccusationAdminController extends BaseController {
	
	@Autowired
	private AccusationService accusationService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 查询举报
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param accusation
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Accusation accusation) {
		request.setAttribute("accusation", accusation);
		return PathResolver.getPath(AdminPage.ACCUSATION_LIST_PAGE);
	}
	
	/**
	 * 查询举报列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param accusation
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Accusation accusation) {
		PageSupport<Accusation> ps = accusationService.query(accusation, curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("accusation", accusation);
		adminMenuUtil.parseMenu(request, 3); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.ACCUSATION_CONTENT_LIST_PAGE);
	}

	/**
	 * 查询尚未处理的投诉
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/queryUnhandler")
	public String queryUnHandler(HttpServletRequest request, HttpServletResponse response) {

		request.setAttribute("accusationList", accusationService.queryUnHandler());
		 return PathResolver.getPath(AdminPage.UNHANDLE_ACCUSATION_LIST_PAGE);
	}

	/**
	 * 保存
	 * 
	 * @param request
	 * @param response
	 * @param accusation
	 * @return
	 */
	@SystemControllerLog(description="处理举报")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Accusation accusation) {
		Accusation handleAccusation = accusationService.getAccusation(accusation.getId());
		handleAccusation.setHandleInfo(accusation.getHandleInfo());
		handleAccusation.setResult(accusation.getResult());
		handleAccusation.setHandleTime(new Date());
		handleAccusation.setStatus((long) 1);
		handleAccusation.setUserDelStatus(accusation.getUserDelStatus());
		handleAccusation.setIllegalOff(accusation.getIllegalOff());
		accusationService.saveAccusation(accusation,handleAccusation);
		return PathResolver.getPath(RedirectPage.ACCUSATION_LIST_QUERY);
	}

	/**
	 * 根据Id加载
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Accusation accusation = accusationService.getDetailedAccusation(id);
		request.setAttribute("accusation", accusation);
		return PathResolver.getPath(AdminPage.ACCUSATION_EDIT_PAGE);
	}

	/**
	 * 举报处理
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/handle/{id}")
	public String handle(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Accusation accusation = accusationService.getDetailedAccusation(id);
		request.setAttribute("accusation", accusation);
		return PathResolver.getPath(AdminPage.REPROT_PROCESSING_PAGE);
	}

}
