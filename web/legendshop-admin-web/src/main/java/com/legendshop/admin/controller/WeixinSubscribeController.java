/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinRedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinSubscribe;
import com.legendshop.spi.service.WeixinSubscribeService;

/**
 * The Class WeixinSubscribeController
 *
 */
@Controller
@RequestMapping("/admin/weixinSubscribe")
public class WeixinSubscribeController extends BaseController{
    @Autowired
    private WeixinSubscribeService weixinSubscribeService;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinSubscribe weixinSubscribe) {
        //DataFunctionUtil.hasAllDataFunction(cq, request, StringUtils.trim(cash.getUserName()));
        /*
           //TODO add your condition
        */
        PageSupport<WeixinSubscribe> ps = weixinSubscribeService.getWeixinSubscribePage(curPageNO);
        PageSupportHelper.savePage(request,ps);
        request.setAttribute("weixinSubscribe", weixinSubscribe);
        return PathResolver.getPath(WeixinAdminPage.WEIXINSUBSCRIBE_LIST_PAGE);
    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, WeixinSubscribe weixinSubscribe) {
        weixinSubscribeService.saveWeixinSubscribe(weixinSubscribe);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(WeixinRedirectPage.WEIXINSUBSCRIBE_LIST_QUERY);
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinSubscribe weixinSubscribe = weixinSubscribeService.getWeixinSubscribe(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinSubscribe.getUserName());
		//if(result!=null){
			//return result;
		//}
		weixinSubscribeService.deleteWeixinSubscribe(weixinSubscribe);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(WeixinRedirectPage.WEIXINSUBSCRIBE_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        WeixinSubscribe weixinSubscribe = weixinSubscribeService.getWeixinSubscribe(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinSubscribe.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("weixinSubscribe", weixinSubscribe);
        return PathResolver.getPath(WeixinAdminPage.WEIXINSUBSCRIBE_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(WeixinAdminPage.WEIXINSUBSCRIBE_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        WeixinSubscribe weixinSubscribe = weixinSubscribeService.getWeixinSubscribe(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weixinSubscribe.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("weixinSubscribe", weixinSubscribe);
		return PathResolver.getPath(WeixinRedirectPage.WEIXINSUBSCRIBE_LIST_QUERY);
    }

}
