/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.MobileAdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum WeixinAdminPage.
 */
public enum MobileAdminPage implements PageDefinition {
	
	/** 手机端html 首页图片 **/
	INDEXJPG_MOBILE_LIST_PAGE("/mobileIndexJpg/indexjpgList"),

	/** 手机端html 首页图片 编辑 **/
	INDEXJPG_MOBILE_EDIT_PAGE("/mobileIndexJpg/indexjpg"),

	/** 手机端html 首页楼层 列表 **/
	MOBILE_FLOOR_LIST_PAGE("/mobileFloor/mobileFloorList"),

	/** 手机端html 首页楼层 编辑 **/
	MOBILE_FLOOR_EDIT_PAGE("/mobileFloor/mobileFloor"),

	/** 手机端html 首页楼层 装修 **/
	MOBILE_FLOOR_DECORATE_PAGE("/mobileFloor/decoratePage"),;
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value the value
	 */
	private MobileAdminPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
