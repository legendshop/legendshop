/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.CacheManageAdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 缓存管理页面
 * 
 */
public enum CacheManageFowardPage implements PageDefinition {

	/** 查看系统参数 */
	PARAM_LIST_QUERY("/admin/system/systemParameter/query");

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new group foward page.
	 * 
	 * @param value
	 * @param template
	 */
	private CacheManageFowardPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */

	public String getNativeValue() {
		return value;
	}
}
