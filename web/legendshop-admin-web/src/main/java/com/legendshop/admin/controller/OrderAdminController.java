/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.business.dao.SubDao;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.DeliveryPrintDataUtils;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.core.utils.ListExcelWriterUtils;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.IdTextEntity;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.dto.OrderExprotDto;
import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 订单控制器。 用于显示用户订单信息,管理员操作.
 */
@Controller
@RequestMapping("/admin/order")
public class OrderAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(OrderAdminController.class);

	/** The sub service. */
	@Autowired
	private SubService subService;

	@Autowired
	private DeliveryTypeService deliveryTypeService;

	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private KuaiDiService kuaiDiService;

	@Autowired
	private PrintTmplService printTmplService;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserAddressSubService userAddressSubService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private DelayCancelHelper delayCancelHelper;
	
	@Autowired
	private SubDao subDao;
	
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Autowired
	private SubHistoryService subHistoryService;
	
	@Autowired
	private AdminMenuUtil adminMenuUtil;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 查找未处理订单.
	 *
	 */
	@RequestMapping("/processing")
	public String queryProcessingOrder(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(AdminPage.PROCESSING_ORDER_LIST_PAGE);
	}
	
	/**
	 * 查找未处理订单内容.
	 *
	 */
	@RequestMapping("/processingContent")
	public String queryProcessingOrderContent(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		// 不显示已删除的订单
		paramDto.setPageSize(10);
		paramDto.setIncludeDeleted(true);
		
		if (AppUtils.isNotBlank(paramDto.getEndDate())) {
			paramDto.setEndDate(DateUtils.getEndMonment(paramDto.getEndDate()));// 结束日期更新为最后一刻
		}
		
		PageSupport<MySubDto> pageSupport = orderService.getAdminOrderDtos(paramDto);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("paramDto", paramDto);
		adminMenuUtil.parseMenu(request, 3); // 固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.PROCESSING_ORDER_CONTENT_LIST_PAGE);
	}

	/**
	 * 查询预售订单
	 * 
	 * @param request
	 * @param response
	 * @param paramDto
	 * @return
	 */
	@RequestMapping("/queryPresellOrders")
	public String queryPresellOrders(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(AdminPage.PRESELL_ORDER_LIST_PAGE);
	}
	
	/**
	 * 查询预售订内容
	 * 
	 * @param request
	 * @param response
	 * @param paramDto
	 * @return
	 */
	@RequestMapping("/queryPresellOrdersContent")
	public String queryPresellOrdersContent(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		if (AppUtils.isBlank(paramDto.getPageSize())) {
			paramDto.setPageSize(10);
		}
		//是否包含用户删除的订单
		paramDto.setIncludeDeleted(true);

		PageSupport<PresellSubDto> pageSupport = orderService.getPresellOrderList(paramDto);
		PageSupportHelper.savePage(request, pageSupport);
		
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(AdminPage.PRESELL_ORDER_CONTENT_LIST_PAGE);
	}

	/**
	 * 查询预售订单详情
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping("/presellOrderAdminDetail/{subNumber}")
	public String presellOrderAdminDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(BackPage.ADMIN_LOGIN);
		}
		PresellSubDto presellSubDto = subService.findPresellOrderDetail(subNumber);
		
		
		//浮点精度计算预售订单应付金额
		Double payableAmount = Arith.add(presellSubDto.getDepositPrice(), presellSubDto.getFinalPrice());
		presellSubDto.setPayableAmount(payableAmount);
//		if(presellSubDto.getPayPctType()==1){
//			double percent = presellSubDto.getDepositPrice()/100;
//			Double prodCash = presellSubDto.getActualTotalPrice();
//			double preVal = prodCash*percent;
//			double finalPayVal = prodCash-preVal;
//			presellSubDto.setDepositPrice(preVal);
//			presellSubDto.setFinalPrice(finalPayVal);
//		}
		request.setAttribute("order", presellSubDto);
		return PathResolver.getPath(AdminPage.PRESELL_ORDER_ADMIN_DETAIL);
	}

	/**
	 * 查看订单页
	 * 
	 * @param request
	 * @param response
	 * @param subNum
	 * @return
	 */
	@RequestMapping("/orderAdminDetail/{subNum}")
	public String orderAdminDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNum) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(BackPage.ADMIN_LOGIN);
		}
		getOrderSub(request, response, subNum);
		return PathResolver.getPath(AdminPage.ORDER_ADMIN_DETAIL);
	}

	/**
	 * 跳到收到货款页面
	 * 
	 * @param request
	 * @param response
	 * @param subNum
	 * @return
	 */
	@RequestMapping("/receiveMoneyPage/{subNum}")
	public String receiveMoneyPage(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNum) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(BackPage.ADMIN_LOGIN);
		}
		Sub sub = subService.getSubBySubNumber(subNum);
		request.setAttribute("order", sub);
		return PathResolver.getPath(AdminPage.ORDER_RECEIVE_MONEY);
	}

	/**
	 * 收到货款action
	 *
	 */
	@SystemControllerLog(description="收到货款")
	@RequestMapping("/receiveMoney")
	public String receiveMoney(HttpServletRequest request, HttpServletResponse response, Sub sub) {
		Sub oldSub = subService.getSubById(sub.getSubId());
		if (null == oldSub || !OrderStatusEnum.UNPAY.value().equals(oldSub.getStatus())) {
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}
		PayType payType = payTypeService.getPayTypeById(sub.getPayId());
		if (null == payType) {
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}
		Date currDate = new Date();
		String subSettlementSn = CommonServiceUtil.getRandomSn();
		oldSub.setPayDate(currDate);
		oldSub.setSubSettlementSn(subSettlementSn);
		oldSub.setPayId(payType.getPayId());
		oldSub.setPayManner(PayMannerEnum.ONLINE_PAY.value());
		oldSub.setPayTypeId(payType.getPayTypeId());
		oldSub.setPayTypeName(payType.getPayTypeName());
		oldSub.setPayed(true);
		oldSub.setFlowTradeNo(sub.getFlowTradeNo());
		oldSub.setStatus(OrderStatusEnum.PADYED.value());

		SubSettlement subSettlement = new SubSettlement();
		subSettlement.setSubSettlementSn(subSettlementSn);
		subSettlement.setFlowTradeNo(oldSub.getFlowTradeNo());
		subSettlement.setCashAmount(oldSub.getActualTotal());
		subSettlement.setPayTypeId(oldSub.getPayTypeId());
		subSettlement.setPayTypeName(oldSub.getPayTypeName());
		subSettlement.setUserId(oldSub.getUserId());
		subSettlement.setIsClearing(true);
		subSettlement.setClearingTime(currDate);
		subSettlement.setCreateTime(currDate);
		subSettlement.setPdAmount(0D);
		subSettlement.setSettlementFrom(PaySourceEnum.PC.name());
		subSettlement.setType(SubSettlementTypeEnum.USER_ORDER_PAY.value());

		subService.receiveMoney(oldSub, subSettlement);
		return PathResolver.getPath(RedirectPage.PROCESSING_ORDER_LIST);
	}

	/**
	 * 订单设置
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/setting")
	public String orderSetting(HttpServletRequest request, HttpServletResponse response) {
		ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
		}
		request.setAttribute("orderSetting", orderSetting);
		return PathResolver.getPath(AdminPage.ORDER_SETTING);
	}

	/**
	 * 订单设置
	 * 
	 * @param request
	 * @param response
	 * @param orderSetMgDto
	 * @return
	 */
	@SystemControllerLog(description="订单设置")
	@RequestMapping("/settingManage")
	@ResponseBody
	public String orderSettingMg(HttpServletRequest request, HttpServletResponse response, OrderSetMgDto orderSetMgDto) {
		try {
			if (orderSetMgDto == null || !orderSetMgDto.validate()) {
				return "对不起,请输入正确的订单设置参数!";
			}
			String setting = JSONUtil.getJson(orderSetMgDto);
			constTableService.updateConstTableByType("ORDER_SETTING", "ORDER_SETTING", setting);

			return Constants.SUCCESS;
		} catch (Exception e) {
			log.debug("更新订单设置异常!", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 打印订单
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping("/orderPrint/{subNumber}")
	public String orderPrint(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(BackPage.ADMIN_LOGIN);
		}
		getOrderSub(request, response, subNumber);
		return PathResolver.getPath(BackPage.PRINT_ORDER_PAGE);
	}

	/**
	 * 发货
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping("/deliverGoods/{subNumber}")
	public String deliverGoods(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "";
		}

		// shop id change 后台发货，B2C不需要shopId
		boolean isCod = sub.isCod();// 是否货到付款订单
		if (OrderStatusEnum.PADYED.value().equals(sub.getStatus()) || OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus()) || isCod) {// 或者是货到付款
			List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId();
			request.setAttribute("sub", sub);
			request.setAttribute("deliveryTypes", deliveryTypes);
			return PathResolver.getPath(BackPage.DELIVER_GOODS);
		}
		return "";
	}

	/**
	 * B2C后台修改物流单号
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping("/changeDeliverNum/{subNumber}")
	public String changeDeliverNum(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "";
		}
		Long shopId = sub.getShopId();
		boolean isCod = sub.isCod();// 是否货到付款订单
		if (OrderStatusEnum.PADYED.value().equals(sub.getStatus()) || OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus()) || isCod) {
			List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId(shopId);
			request.setAttribute("sub", sub);
			request.setAttribute("deliveryTypes", deliveryTypes);
			return PathResolver.getPath(BackPage.CHANGE_DELIVER_NUM);
		}
		return "";
	}

	/**
	 * 获取订单
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 */
	private void getOrderSub(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		MySubDto myorder = subService.findOrderDetail(subNumber);
		request.setAttribute("order", myorder);
	}

	/**
	 * 更新订单状态
	 *
	 */
	@SystemControllerLog(description="更新订单状态")
	@RequestMapping("/orderCancel")
	public @ResponseBody String orderCancel(HttpServletRequest request, HttpServletResponse response, String subNumber, Integer status, String other) {
		try {
			if(AppUtils.isNotBlank(subNumber)){
				Sub sub = subService.getSubBySubNumber(subNumber);
				if(AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){  //订单是未支付的状态
					Date date = new Date();
					sub.setUpdateDate(date);
					sub.setStatus(OrderStatusEnum.CLOSE.value());
					sub.setCancelReason(other);
					boolean result= subService.cancleOrder(sub);
					if(result){
						SubHistory subHistory = new SubHistory();
						String time = subHistory.DateToString(date);
						subHistory.setRecDate(date);
						subHistory.setSubId(sub.getSubId());
						subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
						StringBuilder sb=new StringBuilder();
						sb.append(sub.getUserName()).append("于").append(time).append("取消订单 ");
						subHistory.setUserName(sub.getUserName());
						subHistory.setReason(sb.toString());
						subHistoryService.saveSubHistory(subHistory);
						
						// remove delay queue ordre
						delayCancelHelper.remove(sub.getSubNumber());
						
						return Constants.SUCCESS;
					}
				}else{
					return Constants.FAIL;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constants.FAIL;
	}
	
	/**
	 * 调整订单费用
	 *
	 */
	@SystemControllerLog(description="调整订单费用")
	@RequestMapping(value = "/changeOrderFee", method = RequestMethod.POST)
	public @ResponseBody String changeOrderFee(HttpServletRequest request, HttpServletResponse response, String subNumber, Double freight, Double orderAmount) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		if (subNumber == null || AppUtils.isBlank(orderAmount) || orderAmount == 0) {
			log.warn("Missing parameter");
			return Constants.FAIL;
		}
		if (orderAmount == null || orderAmount < 0 || orderAmount > 9999999999999D) {
			log.warn("orderAmount parameter error");
			return Constants.FAIL;
		}
		if (AppUtils.isBlank(freight)) {
			freight = 0.00;
		}
		boolean result = false;
		try {
			Sub subBySubNumber = subService.getSubBySubNumber(subNumber);
			result = subService.updateSubPrice(subNumber, freight, orderAmount, userName);
			
			if (!subBySubNumber.getTotal().equals(orderAmount) || !subBySubNumber.getFreightAmount().equals(freight)){
                // 发送站内信 通知
                sendSiteMessageProcessor.process(new SendSiteMsgEvent(subBySubNumber.getUserName(),  "价格变更通知", "亲，您的待支付订单[" + subNumber + "]价格已变更").getSource());
			}
		} catch (Exception e) {
			log.error("{}",e);
			throw new BusinessException("update price failed");
		}
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 更新预售订单状态
	 *
	 */
	@SystemControllerLog(description="更新预售订单状态")
	@RequestMapping(value = "/changePreOrderFee", method = RequestMethod.POST)
	public @ResponseBody String changePreOrderFee(HttpServletRequest request, HttpServletResponse response, PresellSubEntity presellSub) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		PresellSubEntity oldPresellsub = subDao.getPreSubBySubNo(presellSub.getSubNumber());
		if (AppUtils.isBlank(presellSub.getSubNumber()) || AppUtils.isBlank(presellSub.getPreDepositPrice()) || AppUtils.isBlank(presellSub.getFinalPrice())) {
			log.warn("Missing parameter");
			return Constants.FAIL;
		}
		if (presellSub.getPreDepositPrice().doubleValue() <= 0 || presellSub.getPreDepositPrice().doubleValue() > 9999999999999D) {
			log.warn("PreDepositPrice parameter error");
			return Constants.FAIL;
		}
		if (oldPresellsub.getPayPctType().equals(1) && (presellSub.getFinalPrice().doubleValue() <= 0 || presellSub.getFinalPrice().doubleValue() > 9999999999999D)) {
			log.warn("FinalPrice parameter error");
			return Constants.FAIL;
		}
		return subService.updateSubPrice(presellSub, user.getUsername()) ? Constants.SUCCESS : Constants.FAIL;
	}

	/**
	 * 修改价格
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping(value = "/modifyPrice")
	public String modifyPrice(HttpServletRequest request, HttpServletResponse response, Long id) {
		return PathResolver.getPath(BackPage.MODIFYPRICE);
	}

	/**
	 * 管理员后台修改价格
	 * 
	 * @param subId
	 * @param totalPrice
	 * @return
	 */
	@SystemControllerLog(description="管理员后台修改订单价格")
	@RequestMapping(value = "/adminChangeSubPrice")
	public @ResponseBody String adminChangeSubPrice(HttpServletRequest request, HttpServletResponse response, Long subId, String totalPrice) {
		if (subId == null || totalPrice == null) {
			return Constants.FAIL;
		}
		Double price = null;
		try {
			price = Double.valueOf(totalPrice.trim());
		} catch (Exception e) {
			return Constants.FAIL;
		}
		if (price == null || price < 0 || price > 9999999999999D) {
			return Constants.FAIL;
		}
		Sub sub = subService.getSubById(subId);
		// 检查权限
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		if (!sub.getUserName().equals(userName) && !sub.getShopName().equals(userName)) {
			log.warn("can not change sub {} does not belongs to you", subId);
			return Constants.FAIL;
		}
		boolean result = false;
		try {
			result = subService.updateSubPrice(sub, userName, price);
		} catch (Exception e) {
			throw new BusinessException("update price failed");
		}

		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}

	}

	/**
	 * 改变物流信息
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param deliv
	 * @param dvyFlowId
	 * @param info
	 * @return
	 */
	@SystemControllerLog(description="改变订单物流信息")
	@RequestMapping(value = "/changeDeliverGoods")
	public @ResponseBody String changeDeliverGoods(HttpServletRequest request, HttpServletResponse response, String subNumber, Long dvyTypeId, String dvyFlowId,
			String info) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (subNumber == null || dvyTypeId == null || dvyFlowId == null) {
			return Constants.FAIL;
		}
		boolean result = false;
		try {
			result = subService.changeDeliverGoods(subNumber, dvyTypeId, dvyFlowId, info, user.getUsername());
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 发货
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param dvyTypeId
	 * @param dvyFlowId
	 * @param info
	 * @return
	 */
	@SystemControllerLog(description="订单发货")
	@RequestMapping(value = "/fahuo", method = RequestMethod.POST)
	public @ResponseBody String fahuo(HttpServletRequest request, HttpServletResponse response, @RequestParam String subNumber, @RequestParam Long dvyTypeId,
			@RequestParam String dvyFlowId, @RequestParam String info) {
		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "未找到该订单";
		}
		String result;
		try {
			result = subService.fahuo(subNumber, dvyTypeId, dvyFlowId, info, sub.getShopId());
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		return result;
	}

	/**
	 * 修改物流单号
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param deliv
	 * @param dvyFlowId
	 * @param info
	 * @return
	 */
	@SystemControllerLog(description="修改物流单号")
	@RequestMapping(value = "/changeDeliverNum", method = RequestMethod.POST)
	public @ResponseBody String changeDeliverNum(HttpServletRequest request, HttpServletResponse response, @RequestParam String subNumber,
			@RequestParam Long deliv, @RequestParam String dvyFlowId, String info) {
		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "未找到该订单";
		}
		String result;
		try {
			result = subService.changeDeliverNum(subNumber, deliv, dvyFlowId, info, sub.getShopId());
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		return result;
	}

	/**
	 * 获取订单历史记录
	 * 
	 * @param request
	 * @param response
	 * @param subId
	 * @return
	 */
	@RequestMapping(value = "/findSubHisInfo", method = RequestMethod.POST)
	public @ResponseBody String findSubHisInfo(HttpServletRequest request, HttpServletResponse response, Long subId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		if (subId == null || userName == null) {
			return Constants.FAIL;
		}
		return subService.findSubhisInfo(subId);
	}

	/**
	 * 订单用户信息
	 * 
	 * @param request
	 * @param response
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/orderUserInfo/{userName}", method = RequestMethod.GET)
	public @ResponseBody String orderUserInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable String userName) {
		UserDetail detail = userDetailService.getUserDetail(userName);
		if (AppUtils.isBlank(detail)) {
			return Constants.FAIL;
		} else {
			List<String> str = new ArrayList<String>();
			str.add("[用户帐号] " + detail.getUserName());
			str.add("[用户昵称] " + detail.getNickName());
			str.add("[用户手机] " + detail.getUserMobile());
			str.add("[用户邮件] " + detail.getUserMail());
			str.add("[用户邮件] " + detail.getUserMail());
			return JSONUtil.getJson(str);
		}
	}

	/**
	 * 获取快递信息
	 * 
	 * @param request
	 * @param response
	 *            配送方式ID
	 * @param dvyFlowId
	 *            物流单据号
	 *  
	 *    status:  200 成功
	 * returnCode:
	 *		400: 提交的数据不完整，或者贵公司没授权
	 *		500: 表示查询失败，或没有POST提交
	 *		501: 服务器错误，快递100服务器压力过大或需要升级，暂停服务
	 *		502: 服务器繁忙，详细说明见2.2《查询接口并发协议》
	 *		503: 验证签名失败。
	 * @return
	 */
	@RequestMapping(value = "/findKuaidiInfo", method = RequestMethod.POST)
	public @ResponseBody String findKuaidiInfo(HttpServletRequest request, HttpServletResponse response, Long dvyId, String dvyFlowId,String reciverMobile) {
		
		if (AppUtils.isBlank(dvyId) || AppUtils.isBlank(dvyFlowId) || AppUtils.isBlank(reciverMobile)) {
			return Constants.FAIL;
		}
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyId);
		if (AppUtils.isNotBlank(deliveryCorp)) {
			
			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) { //查询到没配置密钥则 是免费的  
				
				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size()>0) {
					return JSONUtil.getJson(logs.getEntries());
				}
				
			}else{  //快递100
				
				if(AppUtils.isBlank(deliveryCorp.getCompanyCode())){
					return Constants.FAIL;
				}
				
				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);
				
				//组装参数密钥
				String param ="{\"com\":\""+deliveryCorp.getCompanyCode()+"\",\"num\":\""+dvyFlowId+"\",\"mobiletelephone\":\""+reciverMobile+"\"}";
				String sign = ExpressMD5.encode(param+express.getKey()+express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param",param);
				params.put("sign",sign);
				params.put("customer",express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return Constants.FAIL;
				}
				
				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if(AppUtils.isBlank(result) || !"200".equals(result)){ 
					log.warn("Express inquiry failed message = {},returnCode = {}", jsonObject.getString("message"),jsonObject.getString("returnCode"));
					return Constants.FAIL;
				}
				return jsonObject.getString("data");
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 查找用户订单收获地址
	 * 
	 * @param request
	 * @param response
	 * @param addrOrderId
	 * @return
	 */
	@RequestMapping(value = "/findUsrAddrSub/{addrOrderId}", method = RequestMethod.POST)
	public @ResponseBody UserAddressSub findUsrAddrSub(HttpServletRequest request, HttpServletResponse response, @PathVariable Long addrOrderId) {
		UserAddressSub userAddressSub = userAddressSubService.getUserAddressSub(addrOrderId);
		return userAddressSub;
	}

	/**
	 * 商品信息导出
	 * 
	 * @param request
	 * @param response
	 * @param items
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> exportPordExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "items[]") Long[] items) throws Exception {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		Map<String, String> map = new HashMap<String, String>();
		map.put("result", "false");
		if (AppUtils.isBlank(userName)) {
			map.put("msg", "请登录");
			return map;
		}
		List<OrderExcelWriterDto> excelWriterVos = subService.getOutPutExcelOrder(items);
		if (AppUtils.isBlank(excelWriterVos)) {
			map.put("msg", "找不到相应的导出数据");
			return map;
		}
		String xlsTemplatePath = request.getSession().getServletContext().getRealPath("/") + "xlsxtemplate/orderInfExport.xlsx";
		String fileName = "/xlsxoutput/" + getUUIDPath(userName);
		String xlsxoutput = systemParameterUtil.getDownloadFilePath() + fileName;
		ListExcelWriterUtils<OrderExcelWriterDto> excelWriterUtils = new ListExcelWriterUtils<OrderExcelWriterDto>(xlsTemplatePath);
		String result = excelWriterUtils.validateExcel();
		if (!Constants.SUCCESS.equals(result)) {
			map.put("msg", result);
			return map;
		}
		excelWriterUtils.install();
		excelWriterUtils.fillToFile(excelWriterVos, xlsxoutput, OrderExcelWriterDto.class);
		map.put("result", "true");
		map.put("msg", fileName);
		return map;
	}

	/**
	 * 生成UUIDPath
	 * 
	 * @param userName
	 * @return
	 */
	// 生成文件名 userName + 日期 + 随机数 + **.xlsx
	private String getUUIDPath(String userName) {
		String fileName = ".xlsx";
		StringBuilder builder = new StringBuilder();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		String uuid = UUID.randomUUID().toString();
		builder.append(cal.get(Calendar.YEAR)).append("_").append(cal.get(Calendar.MONTH) + 1).append("_").append(cal.get(Calendar.DAY_OF_MONTH)).append("_")
				.append(uuid).append(fileName.substring(fileName.lastIndexOf('.')));
		fileName = userName + "_" + builder.toString();
		return fileName;
	}

	/**
	 * 打印运单文件
	 * 
	 * @param request
	 * @param response
	 * @param orderId
	 * @param delvId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/printDeliveryDocument/{orderId}/{delvId}")
	public String printDeliveryDocument(HttpServletRequest request, HttpServletResponse response, @PathVariable Long orderId, @PathVariable Long delvId)
			throws Exception {
		PrintTmpl printTmpl = printTmplService.getPrintTmplByDelvId(delvId);
		if (AppUtils.isBlank(printTmpl)) {
			throw new BusinessException(ErrorCodes.BUSINESS_ERROR, "打印失败,没有找到你运单模版信息");
		} else if (AppUtils.isBlank(printTmpl.getBgimage())) {
			throw new BusinessException(ErrorCodes.BUSINESS_ERROR, "打印失败,请设置打印背景图");
		} else if (AppUtils.isBlank(printTmpl.getPrtTmplData())) {
			throw new BusinessException(ErrorCodes.BUSINESS_ERROR, "打印失败,请设置打印背景的参数数据！");
		}
		DeliveryPrintDataUtils printData = new DeliveryPrintDataUtils(printTmpl.getPrtTmplHeight(), printTmpl.getPrtTmplWidth(), printTmpl.getPrtTmplData());
		if (!printTmpl.getBgimage().startsWith("/resources/")) {
			printTmpl.setBgimage(PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage()).getFilePath());
			request.setAttribute("isUpdate", true);
		}
		printData.setTitle(printTmpl.getPrtTmplTitle());
		printData.setBackgroung(printTmpl.getBgimage());
		printData.setIsSystem(printTmpl.getIsSystem());
		printData.setSubId(orderId);
		printData.setIsSystem(printTmpl.getIsSystem());
		if (printTmpl.getIsSystem().intValue() == 0) {
			printData.setBackgroung(PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage()).getFilePath());
		}
		request.setAttribute("printData", printData);
		return PathResolver.getPath(BackPage.PRINT_DELIVERY);
	}

	/**
	 * 获取网站名字
	 *
	 */
	@RequestMapping(value = "/getShopDetailList", method = RequestMethod.GET)
	@ResponseBody
	public List<IdTextEntity> getSiteName(HttpServletRequest request, HttpServletResponse response, String q) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (AppUtils.isBlank(user)) {
			return null;
		}
		List<IdTextEntity> shopDetails = shopDetailService.getShopDetailList(q);
		return shopDetails;
	}

	/**
	 * 导出订单 TODO
	 */
	@SystemControllerLog(description="导出订单")
	@RequestMapping(value = "/export")
	@ResponseBody
	public String export(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto orderSearchParamDto) throws ParseException, IOException {
		List<OrderExprotDto> dataset = orderService.exportOrder(orderSearchParamDto);
		ExportExcelUtil<OrderExprotDto> ex = new ExportExcelUtil<OrderExprotDto>();
		String[] headers = { "订单编号","订单类型","订单状态", "支付状态","退款状态","订单总价","运费","优惠券金额","红包优惠","配送方式","支付类型名称(非预售)", "买家名称","收货人","手机号码","买家地址", "买家留言", "商品名称", "商品数量", "商品属性",
				"下单时间","店铺名称", "商品价格", "商品条形码","商家编码","退款状态2","支付方式","付款方式","订金支付流水号","订金金额","订金支付类型名称(预售)","尾款支付流水号","尾款金额","尾款支付类型名称(预售)","尾款支付状态"};
		response.setHeader("Content-Disposition", "attachment;filename=orderList.xls");
		ex.exportSubExcelUtil(headers, dataset, response.getOutputStream());
		return "";
	}
	
	/**
	 * 导出预售订单
	 * @param request
	 * @param response
	 * @param data
	 * @return
	 */
	@SystemControllerLog(description="导出预售订单")
	@RequestMapping(value="/exportPresellOrders")
	@ResponseBody
	public String exportPresellOrders(HttpServletRequest request, HttpServletResponse response, String data){
		try {
			List<String> str = JSONUtil.getArray(data,String.class);
			OrderSearchParamDto orderSearchParamDto = new OrderSearchParamDto();
			if (AppUtils.isNotBlank(str.get(0))) {
				orderSearchParamDto.setStatus(Integer.parseInt(str.get(0)));
			}
			if(AppUtils.isNotBlank(str.get(1))){
				orderSearchParamDto.setShopId(Long.parseLong(str.get(1)));
			}
			orderSearchParamDto.setSubNumber(str.get(2));
			orderSearchParamDto.setUserName(str.get(3));
			if (AppUtils.isNotBlank(str.get(4))) {
				Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).parse(str.get(4));
				orderSearchParamDto.setStartDate(date);
			}
			if (AppUtils.isNotBlank(str.get(5))) {
				Date date = new SimpleDateFormat("yyyy-MM-dd", Locale.UK).parse(str.get(5));
				orderSearchParamDto.setEndDate(date);
			}
			List<OrderExprotDto> dataset = this.orderService.exportPresellOrders(orderSearchParamDto);
			ExportExcelUtil<OrderExprotDto> ex = new ExportExcelUtil<OrderExprotDto>();
			String[] headers = { "订单编号", "订单状态", "用户名", "下单时间", "订单金额", "支付状态", "支付类型", "支付方式", "支付类型名称", "店铺名称", "运费",  "订金支付流水号", "订金金额", "尾款金额", "尾款支付状态", "商品名称",
					 "商品属性", "商品价格" };
			response.setHeader("Content-Disposition", "attachment;filename=presellOrderList.xls");
			ex.exportExcelUtil(headers, dataset, response.getOutputStream());
		}catch (Exception e){
			e.printStackTrace();
		}
		return "";
	}
	
}
