/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.Partner;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.PartnerService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 供应商管理控制器
 * 
 */
@Controller
@RequestMapping("/admin/partner")
public class PartnerController extends BaseController {

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查找供应商
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Partner partner) {
		PageSupport<Partner> ps = partnerService.getPartnerPage(curPageNO, partner);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("partner", partner);
		return PathResolver.getPath(AdminPage.PARTNER_LIST_PAGE);
	}

	/**
	 * 保存供应商
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Partner partner) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(partner.getPartnerId())) {// update
			Partner orgin = partnerService.getPartner(partner.getPartnerId());
			// set some value
			orgin.setModifyTime(new Date());
			orgin.setPassword(partner.getPassword());
			orgin.setUserId(user.getUserId());
			orgin.setUserName(user.getUsername());

			orgin.setCommentGood(partner.getCommentGood());
			orgin.setCommentNone(partner.getCommentNone());
			orgin.setCommentBad(partner.getCommentBad());

			if (partner.getImageFile().getSize() > 0) {
				String image = attachmentManager.upload(partner.getImageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image, partner.getImageFile(), AttachmentTypeEnum.USER);
				String originImage = orgin.getImage();
				if (AppUtils.isNotBlank(originImage)) {
					// 删除原图片
					attachmentManager.deleteTruely(originImage);
					// 删除附件表记录
					attachmentManager.delAttachmentByFilePath(originImage);
				}
				partner.setImage(image);
			}
			if (partner.getImageFile1().getSize() > 0) {
				String image1 = attachmentManager.upload(partner.getImageFile1());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image1, partner.getImageFile1(), AttachmentTypeEnum.USER);
				String originImage1 = orgin.getImage1();
				if (AppUtils.isNotBlank(originImage1)) {
					// 删除原图片
					attachmentManager.deleteTruely(originImage1);
					// 删除附件表记录
					attachmentManager.delAttachmentByFilePath(originImage1);
				}
				partner.setImage1(image1);
			}
			if (partner.getImageFile2().getSize() > 0) {
				String image2 = attachmentManager.upload(partner.getImageFile2());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image2, partner.getImageFile2(), AttachmentTypeEnum.USER);
				String originImage2 = orgin.getImage2();
				if (AppUtils.isNotBlank(originImage2)) {
					// 删除原图片
					attachmentManager.deleteTruely(originImage2);
					// 删除附件表记录
					attachmentManager.delAttachmentByFilePath(originImage2);
				}
				partner.setImage2(image2);
			}

			partnerService.updatePartner(orgin);
		} else {// add

			Date date = new Date();
			partner.setCreateTime(date);
			partner.setModifyTime(date);

			partner.setCommentBad(0);
			partner.setCommentGood(0);
			partner.setCommentNone(0);

			partner.setShopId(user.getShopId());
			partner.setUserId(user.getUserId());
			partner.setUserName(user.getUsername());

			if (partner.getImageFile().getSize() > 0) {
				String image = attachmentManager.upload(partner.getImageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image, partner.getImageFile(), AttachmentTypeEnum.USER);
				partner.setImage(image);
			}

			if (partner.getImageFile1().getSize() > 0) {
				String image1 = attachmentManager.upload(partner.getImageFile1());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image1, partner.getImageFile1(), AttachmentTypeEnum.USER);
				partner.setImage1(image1);
			}
			if (partner.getImageFile2().getSize() > 0) {
				String image2 = attachmentManager.upload(partner.getImageFile2());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), image2, partner.getImageFile2(), AttachmentTypeEnum.USER);
				partner.setImage2(image2);
			}

			partnerService.savePartner(partner);

		}

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.PARTNER_LIST_QUERY);
	}

	/**
	 * 删除供应商
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Partner partner = partnerService.getPartner(id);
		partnerService.deletePartner(partner);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.PARTNER_LIST_QUERY);
	}

	/**
	 * 跳转编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Partner partner = partnerService.getPartner(id);
		request.setAttribute("partner", partner);
		return PathResolver.getPath(AdminPage.PARTNER_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.PARTNER_EDIT_PAGE);
	}

	/**
	 * 更新供应商
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Partner partner = partnerService.getPartner(id);
		request.setAttribute("partner", partner);
		return PathResolver.getPath(RedirectPage.PARTNER_LIST_QUERY);
	}

	/**
	 * 更改密码
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/changePassword/{id}")
	public String changePassword(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Partner partner = partnerService.getPartner(id);
		request.setAttribute("partner", partner);
		return PathResolver.getPath(AdminPage.PARTNER_CHANGE_PASSWORD_PAGE);
	}

	/**
	 * 保存密码
	 * 
	 * @param request
	 * @param response
	 * @param partner
	 * @return
	 */
	@RequestMapping(value = "/savePassword")
	public String savePassword(HttpServletRequest request, HttpServletResponse response, Partner partner) {
		Partner p = null;
		if (AppUtils.isNotBlank(partner.getPartnerId()) && AppUtils.isNotBlank(partner.getPassword())) {// update
			p = partnerService.getPartner(partner.getPartnerId());
			p.setPassword(partner.getPassword());
		}
		p.setModifyTime(new Date());
		partnerService.savePartner(p);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.PARTNER_LIST_QUERY);
	}

}
