/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.MobileAdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum BackPage.
 */
public enum MobileBackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 楼层内容 */
	FLOOR_ITEM_OVERLAY("/mobileFloor/floorItemOverlay"),
	
	/** 楼层广告 */
	MOBILE_FLOORITEM_ADV("/mobileFloor/floorItemAdv"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value the value
	 */
	private MobileBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
	 	return PagePathCalculator.calculatePluginBackendPath("mobile", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
