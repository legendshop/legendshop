/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;
import com.legendshop.spi.service.HeadlineService;

/**
 * 标题
 *
 */
@Controller
@RequestMapping("/admin/headline")
public class HeadlineAdminController extends BaseController{
    @Autowired
    private HeadlineService headlineService;
    
    /**
     * 查找标题
     */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Headline headline) {
       
        PageSupport<Headline> ps = headlineService.getHeadlinePage(curPageNO);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("headline", headline);
        
        return "/headline/headlineList";
        //TODO, replace by next line, need to predefined BackPage parameter
       // return PathResolver.getPath(request, response, BackPage.HEADLINE_LIST_PAGE);
    }
    
    /**
     * 保存标题
     */
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Headline headline) {
        headlineService.saveHeadline(headline);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return "forward:/admin/headline/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
		//return PathResolver.getPath(request, response, FowardPage.HEADLINE_LIST_QUERY);
    }
    
    /**
     * 删除标题
     */
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Headline headline = headlineService.getHeadline(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), headline.getUserName());
		//if(result!=null){
			//return result;
		//}
		headlineService.deleteHeadline(headline);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/headline/query.htm";
        //TODO, replace by next line, need to predefined FowardPage parameter
        //return PathResolver.getPath(request, response, FowardPage.HEADLINE_LIST_QUERY);
    }
    
    /**
     * 跳转编辑页面
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Headline headline = headlineService.getHeadline(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), headline.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("headline", headline);
        return "/headline/headline";
         //TODO, replace by next line, need to predefined FowardPage parameter
         //return PathResolver.getPath(request, response, BackPage.HEADLINE_EDIT_PAGE);
    }
    
    /**
     * 跳转新建页面
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/headline/headline";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.HEADLINE_EDIT_PAGE);
	}
	
	/**
	 * 更新标题
	 */
    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        Headline headline = headlineService.getHeadline(id);
		//String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), headline.getUserName());
		//if(result!=null){
			//return result;
		//}
		request.setAttribute("headline", headline);
		return "forward:/admin/headline/query.htm";
		//TODO, replace by next line, need to predefined BackPage parameter
		//return PathResolver.getPath(request, response, BackPage.HEADLINE_EDIT_PAGE);
    }

}
