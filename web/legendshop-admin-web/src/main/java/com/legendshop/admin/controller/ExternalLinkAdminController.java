/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.ExternalLink;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ExternalLinkService;
import com.legendshop.uploader.AttachmentManager;

/**
 * 友情链接控制器.
 */
@Controller
@RequestMapping("/admin/externallink")
public class ExternalLinkAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ExternalLinkAdminController.class);

	/** 友情链接服务. */
	@Autowired
	private ExternalLinkService externalLinkService;

	/** 附件上传文件 */
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查询友情链接
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param externalLink
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ExternalLink externalLink) {
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "userName", "bs" });

		PageSupport<ExternalLink> ps = externalLinkService.getDataByCriteriaQuery(curPageNO, externalLink, pageSize, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", externalLink);
		return PathResolver.getPath(AdminPage.EXTERNALLINK_LIST_PAGE);
	}

	/**
	 * 保存友情链接.
	 * 
	 * @param request
	 * @param response
	 * @param externalLink
	 * @return the string
	 */
	@SystemControllerLog(description="保存友情链接")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ExternalLink externalLink) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		String picUrl = null;
		if ((externalLink != null) && (externalLink.getId() != null)) { // update
			if (externalLink.getFile() != null && externalLink.getFile().getSize() > 0) {// 上传附件
				picUrl = attachmentManager.upload(externalLink.getFile());
			}
		}else{
			if (externalLink.getFile() != null && externalLink.getFile().getSize() > 0) {
				picUrl = attachmentManager.upload(externalLink.getFile());
			}
		}
		externalLinkService.saveOrUpdate(userId,shopId,userName,externalLink,picUrl);
		
		saveMessage(request, ResourceBundleHelper.getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.LINK_LIST_QUERY);
	}

	/**
	 * 删除友情链接.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="删除友情链接")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ExternalLink externalLink = externalLinkService.getExternalLinkById(id);
		if (externalLink != null) {
			log.info("{} delete ExternalLink Url{}", externalLink.getUrl());
			externalLinkService.delete(id,externalLink);
		}

		saveMessage(request, ResourceBundleHelper.getDeleteString());
		return PathResolver.getPath(RedirectPage.LINK_LIST_QUERY);
	}

	/**
	 * 根据id加载链接编辑.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ExternalLink externalLink = externalLinkService.getExternalLinkById(id);
		request.setAttribute("bean", externalLink);
		String path = PathResolver.getPath(AdminPage.EXTERNALLINK_EDIT_PAGE);
		return path;
	}

	/**
	 * 加载链接编辑
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.EXTERNALLINK_EDIT_PAGE);
	}

	/**
	 * 更新加载链接编辑.
	 * 
	 * @param request
	 * @param response
	 * @param externalLink
	 * @return the string
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable ExternalLink externalLink) {
		externalLinkService.getExternalLinkById(externalLink.getId());
		log.info("{} update ExternalLink Url{}", externalLink.getUrl());
		externalLinkService.update(externalLink);
		return PathResolver.getPath(FowardPage.LINK_LIST_QUERY);
	}

}
