/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.model.constant.BrandStatusEnum;
import com.legendshop.model.dto.BrandExportDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.admin.util.ExportExcel;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.Select2Dto;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.entity.Brand;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BrandService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 产品品牌控制器。
 */
@Controller
@RequestMapping("/admin/brand")
public class BrandAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(BrandAdminController.class);

	/** The brand service. */
	@Autowired
	private BrandService brandService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 查询品牌
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Brand brand) {
		request.setAttribute("brand", brand);
		return PathResolver.getPath(AdminPage.BRAND_LIST_PAGE);
	}

	/**
	 * 查询品牌
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Brand brand) {

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[] { "applyTime" });
		PageSupport<Brand> ps = brandService.queryBrandListPage(curPageNO, brand, result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("brand", brand);
		adminMenuUtil.parseMenu(request, 2); // 固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		String path = PathResolver.getPath(AdminPage.BRAND_CONTENT_LIST_PAGE);
		return path;
	}

	/**
	 * 批量删除
	 */
	@SystemControllerLog(description = "批量删除品牌")
	@RequestMapping(value = "/batchDel/{ids}", method = RequestMethod.GET)
	public @ResponseBody Object batchDel(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		AjaxJson ajaxJson = new AjaxJson();
		if (AppUtils.isBlank(ids)) {
			ajaxJson.setMsg("参数错误。");
			ajaxJson.setSuccess(false);
			ajaxJson.setSuccessValue("-1");

			return ajaxJson;
		}
		try {
			brandService.batchDel(ids);
			ajaxJson.setMsg("批量删除成功。");
			ajaxJson.setSuccess(true);
			ajaxJson.setSuccessValue("Y");

			return ajaxJson;
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setMsg("删除出错。");
			ajaxJson.setSuccess(false);
			ajaxJson.setSuccessValue("-1");

			return ajaxJson;
		}

	}

	/**
	 * 批量上下线
	 */
	@SystemControllerLog(description = "批量上下线")
	@RequestMapping(value = "/batchOffOrOnLine/{ids}", method = RequestMethod.GET)
	public @ResponseBody Object batchOffOrOnLine(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids, @RequestParam Integer brandStatus) {
		AjaxJson ajaxJson = new AjaxJson();
		if (AppUtils.isBlank(ids)) {
			ajaxJson.setMsg("参数错误");
			ajaxJson.setSuccess(false);
			ajaxJson.setSuccessValue("-1");

			return ajaxJson;
		}
		try {
			brandService.batchOffOrOnLine(ids, brandStatus);

			if (brandStatus.equals(1)) {
				ajaxJson.setMsg("批量上线成功。");
			} else {
				ajaxJson.setMsg("批量下线成功。");
			}
			ajaxJson.setSuccess(true);
			ajaxJson.setSuccessValue("Y");

			return ajaxJson;
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setMsg("删除出错。");
			ajaxJson.setSuccess(false);
			ajaxJson.setSuccessValue("-1");

			return ajaxJson;
		}

	}

	/**
	 * 品牌管理：导出
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/export")
	public String Export(HttpServletRequest request, HttpServletResponse response, String data) {
		List<String> str = JSONUtil.getArray(data, String.class);
		Brand brand = new Brand();
		if (AppUtils.isNotBlank(str.get(0))) {
			brand.setStatus(Integer.parseInt(str.get(0)));
		}
		if (AppUtils.isNotBlank(str.get(1))) {
			brand.setShopId(Long.parseLong(str.get(1)));
		}
		if (AppUtils.isNotBlank(str.get(2))) {
			brand.setCommend(Integer.parseInt(str.get(2)));
		}
		if (AppUtils.isNotBlank(str.get(3))) {
			brand.setBrandName(str.get(3));
		}
		/*ExportExcel<Brand> ex = new ExportExcel<Brand>();
		List<Brand> dataset = this.brandService.getExportBrands(brand);
		String[] headers = { "顺序", "品牌名称", "图片路径", "卖点", "大图路径" };*/
		ExportExcelUtil<BrandExportDTO> ex = new ExportExcelUtil<BrandExportDTO>();
		List<Brand> list = this.brandService.getExportBrands(brand);
		List<BrandExportDTO> dataset = new ArrayList<>();
		//给导出的列表增加序号列
		int sequence = 1;
		for (Brand brand1 : list) {
			BrandExportDTO brandExportDTO = new BrandExportDTO();
			brandExportDTO.setSequence(sequence++);
			brandExportDTO.setApplyTime(brand1.getApplyTime());
			brandExportDTO.setBrandName(brand1.getBrandName());
			brandExportDTO.setBrandPic(brand1.getBrandPic());
			brandExportDTO.setShopName(brand1.getShopName());
			if (BrandStatusEnum.BRAND_ONLINE.value().equals(brand1.getStatus())) {
				brandExportDTO.setStatus("上线中");
			}
			if (BrandStatusEnum.BRAND_OFFLINE.value().equals(brand1.getStatus())) {
				brandExportDTO.setStatus("下线中");
			}
			if (BrandStatusEnum.BRAND_AUDIT.value().equals(brand1.getStatus())) {
				brandExportDTO.setStatus("审核中");
			}
			if (BrandStatusEnum.BRAND_AUDIT_FAIL.value().equals(brand1.getStatus())) {
				brandExportDTO.setStatus("审核失败");
			}
			dataset.add(brandExportDTO);
		}
		String[] headers = { "序号", "店铺","品牌名称", "品牌logo路径", "申请时间", "状态" };
		try {
			response.setHeader("Content-Disposition", "attachment;filename=brandList.xls");
			//ex.exportExcel(headers, dataset, response.getOutputStream());
			ex.exportExcelUtil(headers, dataset, response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 保存
	 */
	@RequestMapping(value = "/save")
	@SystemControllerLog(description = "保存品牌")
	public String save(HttpServletRequest request, HttpServletResponse response, Brand brand) {
		Brand originbrand = null;

		// PC品牌LOGO
		String brandPic = null;
		// 移动端品牌LOGO
		String brandPicMobile = null;
		// 品牌大图
		String bigImage = null;
		// 授权书
		String brandAuthorize = null;

		SecurityUserDetail user = UserManager.getUser(request);

		// update
		if ((brand != null) && (brand.getBrandId() != null)) {
			originbrand = brandService.getBrand(brand.getBrandId());
			if (originbrand == null) {
				throw new NotFoundException("Origin Brand is empty");
			}

			originbrand.setMemo(brand.getMemo());
			originbrand.setCommend(brand.getCommend());
			originbrand.setBrandName(brand.getBrandName());
			originbrand.setSeq(brand.getSeq() == null ? 0 : brand.getSeq());
			originbrand.setBrief(brand.getBrief());
			originbrand.setContent(brand.getContent());
			/** 处理PC品牌LOGO **/
			if (brand.getFile() != null && brand.getFile().getSize() > 0) {
				brandPic = attachmentManager.upload(brand.getFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPic, brand.getFile(), AttachmentTypeEnum.BRAND);
				originbrand.setBrandPic(brandPic);
			}
			/** 处理移动端品牌LOGO **/
			if (brand.getMobileLogoFile() != null && brand.getMobileLogoFile().getSize() > 0) {
				brandPicMobile = attachmentManager.upload(brand.getMobileLogoFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPicMobile, brand.getMobileLogoFile(), AttachmentTypeEnum.BRAND);
				originbrand.setBrandPicMobile(brandPicMobile);
			}
			/** 处理品牌 大图片 **/
			if (brand.getBigImagefile() != null && brand.getBigImagefile().getSize() > 0) {
				bigImage = attachmentManager.upload(brand.getBigImagefile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), bigImage, brand.getBigImagefile(), AttachmentTypeEnum.BRAND);
				originbrand.setBigImage(bigImage);
			}
			/** 处理授权书 **/
			if (brand.getBrandAuthorizefile() != null && brand.getBrandAuthorizefile().getSize() > 0) {
				brandAuthorize = attachmentManager.upload(brand.getBrandAuthorizefile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandAuthorize, brand.getBrandAuthorizefile(), AttachmentTypeEnum.BRAND);
				originbrand.setBrandAuthorize(brandAuthorize);
			}
			brandService.update(originbrand);

		} else {
			if (brand.getFile() != null && brand.getFile().getSize() > 0) {
				brandPic = attachmentManager.upload(brand.getFile());
				brand.setBrandPic(brandPic);
			}
			if (brand.getMobileLogoFile() != null && brand.getMobileLogoFile().getSize() > 0) {
				brandPicMobile = attachmentManager.upload(brand.getMobileLogoFile());
				brand.setBrandPicMobile(brandPicMobile);
			}

			if (brand.getBigImagefile() != null && brand.getBigImagefile().getSize() > 0) {
				bigImage = attachmentManager.upload(brand.getBigImagefile());
				brand.setBigImage(bigImage);
			}
			if (brand.getBrandAuthorizefile() != null && brand.getBrandAuthorizefile().getSize() > 0) {
				brandAuthorize = attachmentManager.upload(brand.getBrandAuthorizefile());
				brand.setBrandAuthorize(brandAuthorize);
			}

			Date data = new Date();

			brand.setUserId(user.getUserId());
			brand.setUserName(user.getUsername());
			brand.setApplyTime(data);
			brand.setStatus(Constants.ONLINE);
			brand.setShopId(user.getShopId());
			if (AppUtils.isBlank(brand.getSeq())) {
				brand.setSeq(0);
			}
			brandService.save(brand);
			if (AppUtils.isNotBlank(brandPic)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPic, brand.getFile(), AttachmentTypeEnum.BRAND);
			}
			if (AppUtils.isNotBlank(brandPicMobile)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPicMobile, brand.getMobileLogoFile(), AttachmentTypeEnum.BRAND);
			}

			if (AppUtils.isNotBlank(bigImage)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), bigImage, brand.getBigImagefile(), AttachmentTypeEnum.BRAND);
			}
			if (AppUtils.isNotBlank(brandAuthorize)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandAuthorize, brand.getBrandAuthorizefile(), AttachmentTypeEnum.BRAND);
			}
		}
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		String result = PathResolver.getPath(RedirectPage.BRAND_LIST_QUERY);
		return result;
	}

	/**
	 * 删除
	 */
	@SystemControllerLog(description = "删除品牌")
	@RequestMapping(value = "/delete/{id}", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Brand brand = brandService.getBrand(id);
		if (brandService.hasChildProduct(brand.getBrandId())) {
			// throw new ConflictException("商品品牌下有产品, 该品牌不能删除！");
			return "fail";
		}

		// 判断商品的是否上线，上线不能删除
		if (brand.getStatus() == 1) {
			// throw new ConflictException("商品品牌已上线, 该品牌不能删除！");
			return "fail";
		}

		log.info("{}, delete Brand Picture {}", brand.getUserName(), brand.getBrandPic());
		brandService.delete(id);

		saveMessage(request, ResourceBundleHelper.getDeleteString());

		return "success";
		// return PathResolver.getPath(request, response,
		// RedirectPage.BRAND_LIST_QUERY);
	}

	/**
	 * 加载品牌编辑页
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.BRAND_EDIT_PAGE);
	}

	/**
	 * 更新品牌
	 */
	@RequestMapping(value = "/update/{id}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Brand brand = brandService.getBrand(id);
		request.setAttribute("bean", brand);
		return PathResolver.getPath(AdminPage.BRAND_EDIT_PAGE);
	}

	/**
	 * 根据id加载品牌检查页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/check/{id}")
	public String check(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Brand brand = brandService.getBrand(id);
		request.setAttribute("brand", brand);
		return PathResolver.getPath(AdminPage.BRAND_CHECK_PAGE);
	}

	/**
	 * 审核品牌
	 * 
	 * @param request
	 * @param response
	 * @param brandId
	 * @param status
	 * @param opinion
	 * @return
	 */
	@SystemControllerLog(description = "审核品牌")
	@RequestMapping(value = "/checkBrand", method = RequestMethod.POST)
	public @ResponseBody String checkBrand(HttpServletRequest request, HttpServletResponse response, Long brandId, Integer status, String opinion) {
		Brand brand = brandService.getBrand(brandId);
		if (AppUtils.isBlank(brand)) {
			return Constants.FAIL;
		}
		if (AppUtils.isBlank(status)) {
			return Constants.FAIL;
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		brand.setStatus(status);
		brand.setCheckTime(new Date());
		brand.setCheckUserName(userName);
		brand.setCheckOpinion(opinion);
		brandService.update(brand);
		// 发送站内信 通知
		String brandName = brand.getBrandName();
		String resule = null;
		if (status == 1) {
			resule = "成功";
		} else {
			resule = "失败";
		}
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(brand.getUserName(), "品牌审核提醒", "亲，您申请的品牌【" + brandName + "】审核" + resule).getSource());
		return Constants.SUCCESS;
	}

	/**
	 * 更新状态
	 * 
	 * @param request
	 * @param response
	 * @param brandId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description = "更新品牌状态")
	@RequestMapping(value = "/updatestatus/{brandId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long brandId, @PathVariable Integer status) {
		Brand brand = brandService.getBrand(brandId);
		if (brand == null) {
			return -1;
		}
		if (!status.equals(brand.getStatus())) {
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				brand.setStatus(status);
				brandService.update(brand);
			}
		}
		return brand.getStatus();
	}

	/**
	 * 保存品牌条款
	 * 
	 * @param request
	 * @param response
	 * @param idJson
	 * @param nameJson
	 * @param nsortId
	 * @return
	 */
	@RequestMapping(value = "/saveBrandItem/{nsortId}")
	public @ResponseBody String saveBrandItem(HttpServletRequest request, HttpServletResponse response, String idJson, String nameJson, @PathVariable Long nsortId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		return brandService.saveBrandItem(idJson, nameJson, nsortId, userName);
	}

	/**
	 * 根据品牌名检查品牌是否已存在
	 * 
	 * @param brandName
	 * @return
	 */
	@RequestMapping(value = "/checkBrandByName")
	public @ResponseBody boolean checkBrandByName(String brandName, Long brandId) {
		if (AppUtils.isNotBlank(brandName)) {
			return brandService.checkBrandByName(brandName, brandId);
		}
		return false;
	}

	/**
	 * 获取简单的品牌信息数据
	 */
	@RequestMapping(value = "/getBrandSelect2")
	public @ResponseBody Object getShopUserByShopName(HttpServletRequest request, HttpServletResponse response, String q, Integer currPage, Integer pageSize) {
		List<Select2Dto> list = brandService.getBrandSelect2(q, currPage, pageSize);
		int total = brandService.getBrandSelect2Count(q);
		JSONObject result = new JSONObject();
		result.put("results", list);
		result.put("total", total);
		return result;
	}

}
