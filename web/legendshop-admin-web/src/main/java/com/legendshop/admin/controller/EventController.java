/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserEvent;
import com.legendshop.spi.service.EventService;

/**
 * 系统日志
 * 
 */
@Controller
@RequestMapping("/admin/event")
public class EventController extends BaseController{

	@Autowired
	private EventService eventService;

	/**
	 * 查询系统日志
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, UserEvent userEvent) {
		PageSupport<UserEvent> ps = eventService.getEventPage(curPageNO, userEvent);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("event", userEvent);
		return PathResolver.getPath(AdminPage.EVENT_LIST_PAGE);
	}

	/**
	 * 保存系统日志
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, UserEvent userEvent) {
		eventService.saveEvent(userEvent);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(FowardPage.EVENT_QUERY);
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserEvent userEvent = eventService.getEvent(id);
		eventService.deleteEvent(userEvent);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.EVENT_QUERY);
	}

	/**
	 * 根据id加载系统日志编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserEvent userEvent = eventService.getEvent(id);
		request.setAttribute("event", userEvent);
		return PathResolver.getPath(AdminPage.EVENT_EDIT_PAGE);
	}

	/**
	 * 加载系统日志编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.EVENT_EDIT_PAGE);
	}

	/**
	 * 更新系统日志
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserEvent userEvent = eventService.getEvent(id);
		request.setAttribute("event", userEvent);
		return PathResolver.getPath(FowardPage.EVENT_QUERY);
	}



	public static void main(String args[]) {
		String Str = new String("<p style='background-color:#ffffff;'=''>");
		System.out.println(Str.replaceAll("'='", "" ));

	}


}
