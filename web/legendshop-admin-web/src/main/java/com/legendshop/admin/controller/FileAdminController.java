/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.AppUtils;

/**
 * 注册协议.
 */
@Controller
public class FileAdminController extends BaseController {
	
	@Autowired
    private SystemConfigService systemConfigService;

	/**
	 * 编辑注册协议.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/register")
	public String register(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getRegProtocolTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "regProtocol");
		request.setAttribute("title", "编辑注册协议");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}

	/**
	 * 重置密码邮件内容.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/resetpassmail")
	public String resetpassmail(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getMailValidateTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "mailValidate");
		request.setAttribute("title", "编辑邮箱验证");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}

	/**
	 * Registersuccess. 注册成功邮件内容
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/registersuccess")
	public String registersuccess(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getMailRegsuccessTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "mailRegsuccess");
		request.setAttribute("title", "编辑注册成功");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}
	
	/**
	 * 入驻协议
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/settled")
	public String settled(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getSettledProtocolTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "settled");
		request.setAttribute("title", "编辑入驻协议");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}
	
	/**
	 * 拍卖保证金
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/auctionsPay")
	public String auctionsPay(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getAuctionsPayProtocolTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "auctionsPay");
		request.setAttribute("title", "编辑拍卖协议");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}

	/**
	 * 开店协议
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/openShop")
	public String openShop(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getOpenShopProtocolTemplate();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "openShop");
		request.setAttribute("title", "编辑开店协议");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}
	
	/**
	 * 后台关于
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/admin/system/file/backAbout")
	public String backAbout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content =systemConfig.getBackAbout();
		request.setAttribute("content", content);
		request.setAttribute("fileType", "backAbout");
		request.setAttribute("title", "编辑后台关于");
		return PathResolver.getPath(AdminPage.FILE_EDIT_PAGE);
	}


	// ajax call
	/**
	 * 保存协议.
	 * 
	 * @param request
	 * @param response
	 * @param content
	 * @param parentFilePath
	 * @param fileName
	 * @return the integer
	 */
	@SystemControllerLog(description="保存协议")
	@RequestMapping("/admin/system/file/save")
	public @ResponseBody
	Integer save(HttpServletRequest request, HttpServletResponse response, String content, String fileType) {
		if (AppUtils.isBlank(content)) {
			return -1;
		}
		try {
			SystemConfig systemConfig = systemConfigService.getSystemConfig();
			if("mailValidate".equals(fileType)){
				systemConfig.setMailValidateTemplate(content);
			}else if("regProtocol".equals(fileType)){
				systemConfig.setRegProtocolTemplate(content);
			}else if("mailRegsuccess".equals(fileType)){
				systemConfig.setMailRegsuccessTemplate(content);
			}else if("settled".equals(fileType)){
				systemConfig.setSettledProtocolTemplate(content);
			}else if("openShop".equals(fileType)){
				systemConfig.setOpenShopProtocolTemplate(content);
			}else if("auctionsPay".equals(fileType)){
				systemConfig.setAuctionsPayProtocolTemplate(content);
			}else if("backAbout".equals(fileType)){
				systemConfig.setBackAbout(content);
			}
			systemConfigService.updateSystemConfig(systemConfig);
		} catch (Exception e) {
			return -2;
		}
		return 0;
	}

	/**
	 * 加载用户注册协议.
	 * 
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping("/regItem")
	public @ResponseBody
	String readRegItem(HttpServletRequest request, HttpServletResponse response) {
		//加载用户注册协议
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		return systemConfig.getRegProtocolTemplate();
	}

}
