/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.admin.page.MobileAdminPage.MobileAdminPage;
import com.legendshop.admin.page.MobileAdminPage.MobileBackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.MobileFloor;
import com.legendshop.model.entity.MobileFloorItem;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MobileFloorItemService;
import com.legendshop.spi.service.MobileFloorService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 楼层
 *
 */
@Controller
@RequestMapping("/admin/mobileFloor")
public class MobileFloorController extends BaseController{
	
    @Autowired
    private MobileFloorService mobileFloorService;
    
    @Autowired
    private MobileFloorItemService mobileFloorItemService;
    
    @Autowired
    private AttachmentManager attachmentManager;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, MobileFloor mobileFloor) {
        PageSupport<MobileFloor> ps = mobileFloorService.getMobileFloorPage(curPageNO,mobileFloor);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("mobileFloor", mobileFloor);
        return PathResolver.getPath(AdminPage.MOBILE_FLOOR_LIST_PAGE);
    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, MobileFloor mobileFloor) {
    	
    	SecurityUserDetail user = UserManager.getUser(request);
    	
    	mobileFloor.setRecDate(new Date());
    	mobileFloor.setShopId(user.getShopId());
        mobileFloorService.saveMobileFloor(mobileFloor);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(RedirectPage.MOBILEFLOOR_LIST_QUERY);
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        MobileFloor mobileFloor = mobileFloorService.getMobileFloor(id);
		mobileFloorService.deleteMobileFloor(mobileFloor);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(RedirectPage.MOBILEFLOOR_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        MobileFloor mobileFloor = mobileFloorService.getMobileFloor(id);
        request.setAttribute("mobileFloor", mobileFloor);
        return PathResolver.getPath(AdminPage.MOBILE_FLOOR_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(AdminPage.MOBILE_FLOOR_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        MobileFloor mobileFloor = mobileFloorService.getMobileFloor(id);
		request.setAttribute("mobileFloor", mobileFloor);
		return PathResolver.getPath(RedirectPage.MOBILEFLOOR_LIST_QUERY);
    }

    /**
     * 更新状态
     * @param request
     * @param response
     * @param id
     * @param status
     * @return
     */
    @RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
			@PathVariable Integer status) {
    	MobileFloor mobileFloor = mobileFloorService.getMobileFloor(id);
		if (mobileFloor == null) {
			return -1;
		}
		if (!status.equals(mobileFloor.getStatus())) {
//			if (!FoundationUtil.haveViewAllDataFunction(request)) {
//				Long shopId = UserManager.getShopId(request.getSession());
//				// user
//				if (!shopId.equals(mobileFloor.getShopId())) {
//					return -1;
//				}
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)) {
//					mobileFloor.setStatus(status);
//					mobileFloor.setRecDate(new Date());
//					mobileFloorService.updateMobileFloor(mobileFloor);
//				}
//			} else {
//				// admin
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
//					mobileFloor.setStatus(status);
//					mobileFloor.setRecDate(new Date());
//					mobileFloorService.updateMobileFloor(mobileFloor);
//				}
//			}
			
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				mobileFloor.setStatus(status);
				mobileFloor.setRecDate(new Date());
				mobileFloorService.updateMobileFloor(mobileFloor);
			}
		}
		return mobileFloor.getStatus();
	}
    
    
    @RequestMapping(value = "/decorate/{id}")
	public String loadFloorEdit(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id) {
    	MobileFloor mobileFloor = mobileFloorService.getMobileFloor(id);
    	List<MobileFloorItem> floorItemList = mobileFloorItemService.queryMobileFloorItem(id);
    	request.setAttribute("fId", id);
    	request.setAttribute("floorItemList", floorItemList);
    	request.setAttribute("mobileFloor", mobileFloor);
    	return PathResolver.getPath(MobileAdminPage.MOBILE_FLOOR_DECORATE_PAGE);
	}
    
    
	@RequestMapping(value = "/floorItemOverlay/{id}")
	public String floorItemOverlay(HttpServletRequest request, HttpServletResponse response,String curPageNO,String title,@PathVariable Long id) {
        PageSupport<MobileFloorItem> ps = mobileFloorItemService.getMobileFloorItem(curPageNO,title,id);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("title", title);
        request.setAttribute("fId", id);
        //获取list长度
        request.setAttribute("table_length", ps.getResultList().size());
    	return PathResolver.getPath(MobileBackPage.FLOOR_ITEM_OVERLAY);
	}
	
	@RequestMapping(value = "/updateFloorItem/{parentId}/{id}")
	public String updateFloorItem(HttpServletRequest request, HttpServletResponse response,@PathVariable Long parentId,@PathVariable Long id) {
		if(AppUtils.isNotBlank(id)){
			MobileFloorItem mobileFloorItem =  mobileFloorItemService.getMobileFloorItem(id);
			request.setAttribute("floorItem", mobileFloorItem);
		}
		request.setAttribute("parentId", parentId);
		return PathResolver.getPath(MobileBackPage.MOBILE_FLOORITEM_ADV);
	}
	
	@RequestMapping(value = "/loadFloorItem/{parentId}")
	public String loadFloorItem(HttpServletRequest request, HttpServletResponse response,@PathVariable Long parentId) {
		request.setAttribute("parentId", parentId);
		return PathResolver.getPath(MobileBackPage.MOBILE_FLOORITEM_ADV);
	}
	
	@RequestMapping(value = "/saveFloorItem")
   	public @ResponseBody Map<String, Object> saveFloorItem(HttpServletRequest request, HttpServletResponse response,MobileFloorItem mobileFloorItem) {//TODO upload
		Map<String, Object> result = new HashMap<String, Object>();
		SecurityUserDetail user = UserManager.getUser(request);
		try {
			String filename = null;
			MultipartFile formFile = mobileFloorItem.getFile();
			
			if(mobileFloorItem.getId() != null){
				// update mobileFloorItem
				String orginImg = null;
				MobileFloorItem origin = mobileFloorItemService.getMobileFloorItem(mobileFloorItem.getId());
				
				if ((formFile != null) && (formFile.getSize() > 0)) {
					orginImg = origin.getPic();
					// upload file
					filename = attachmentManager.upload(formFile);
					//保存附件表
	    			Long attmntId = attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.MOBILEADV);
					origin.setPic(filename);
				}
				
				origin.setTitle(mobileFloorItem.getTitle());
				origin.setLinkPath(mobileFloorItem.getLinkPath());
				origin.setRecDate(new Date());
				origin.setSeq(mobileFloorItem.getSeq());
				origin.setStatus(Constants.ONLINE);
				mobileFloorItemService.updateMobileFloorItem(origin);
				
			}else{
				//表示是新增
				int size = mobileFloorItemService.getCountMobileFloorItem(mobileFloorItem.getParentId()).intValue();
				//判断数量是否大于3个，广告不能超过3个
				if(size>=3){
					result.put("success","error");
					return result;
				}
				// upload file
				if ((formFile != null) && (formFile.getSize() > 0)) {
					filename = attachmentManager.upload(formFile);
					mobileFloorItem.setPic(filename);
				}
				
				mobileFloorItem.setStatus(Constants.ONLINE);
				mobileFloorItem.setRecDate(new Date());
				mobileFloorItemService.saveMobileFloorItem(mobileFloorItem);
				if(AppUtils.isNotBlank(filename)){
					//保存附件表
					attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.MOBILEADV);
				}
			}
			
			result.put("success", true);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
			return result;
		}
	}
	
	  @RequestMapping(value = "/deleteFloorItem/{id}")
	  public @ResponseBody String deleteFloorItem(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		  
		  	MobileFloorItem mobileFloorItem = mobileFloorItemService.getMobileFloorItem(id);
		  	
		  	if(AppUtils.isBlank(id)|| AppUtils.isBlank(mobileFloorItem)){
		  		return Constants.FAIL;
		  	}else{
		  		mobileFloorItemService.deleteMobileFloorItem(mobileFloorItem);
		  		return Constants.SUCCESS;
		  	}
	    }
}
