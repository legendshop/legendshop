/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.LoginUserType;
import com.legendshop.redis.UserOnlineStatusHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.UserDetailExportDto;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.UserDetailService;

/**
 * 用户信息管理.
 */
@Controller
@RequestMapping("/admin/system/userDetail")
public class UserDetailAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(UserDetailAdminController.class);

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	@Autowired
	private UserOnlineStatusHelper userOnlineStatusHelper;
	
	/**
	 * 批量改变用户状态
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="批量改变用户状态")
	@RequestMapping(value = "/batchOfflineUser/{userIds}", method = RequestMethod.PUT)
	public @ResponseBody String batchOfflineUser(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String userIds) {
		try {
			userDetailService.batchOfflineUser(userIds);
			return Constants.SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return "批量下线用户失败。";
		}

	}

	/**
	 * 查询用户产品评论列表.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param userDetail
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			UserDetail userDetail) {
		String haveShop = request.getParameter("haveShop") == null ? "" : request.getParameter("haveShop");

		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request,
				new String[] { "u.user_name", "u.nick_name", "s.enabled" });

		PageSupport<UserDetail> ps = userDetailService.getUserDetailListPage(haveShop, result, curPageNO, userDetail);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("haveShop", haveShop);
		adminMenuUtil.parseMenu(request, 6); // 固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面

		return PathResolver.getPath(AdminPage.USER_DETAIL_LIST_PAGE);
	}

	/**
	 * 删除用户所有信息
	 * 
	 * @param userId
	 * @param userName
	 * @return
	 */
	@SystemControllerLog(description="删除用户所有信息")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String deleteUserDetail(String userId, String userName) {
		if (userId == null) {
			log.warn("delete user with user id");
			return Constants.FAIL;
		}
		try {
			return userDetailService.deleteUserDetail(userId, userName);
		} catch (Exception e) {
			log.error("deleteUserDetail", e);
			return Constants.FAIL;
		}

	}

	/**
	 * 更新用户信息状态.
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @param status
	 * @return the integer
	 */
	@SystemControllerLog(description="更新用户信息状态")
	@RequestMapping(value = "/updatestatus/{userId}/{status}", method = RequestMethod.GET)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String userId, @PathVariable String status) {
		User user = userDetailService.getUser(userId);
		if (user == null) {
			return "-1";
		}
		if (!status.equals(user.getEnabled())) {
			user.setEnabled(status);
			userDetailService.updateUser(user);
		}
		return status;
	}

	/**
	 * 改变用户状态
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="改变用户状态")
	@RequestMapping(value = "/changeStatus/{userId}/{status}", method = RequestMethod.POST)
	public @ResponseBody String changeUserStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String userId, @PathVariable String status) {
		User user = userDetailService.getUser(userId);
		if (user == null) {
			return "对不起,您要操作的用户不存在或已被";
		}
		if (!status.equals(user.getEnabled())) {
			user.setEnabled(status);
			Integer status1 = Integer.valueOf(status);
			userDetailService.changeUserStatus(user, status1);

			//删除Redis中的记录，让用户被动退出
			if(status1.equals(0)){
				//用户下线
				userOnlineStatusHelper.offline(LoginUserType.USER_TYPE, user.getId());
			}

		}
		return Constants.SUCCESS;
	}

	/**
	 * 导出用户信息
	 * 
	 * @param request
	 * @param response
	 * @param data
	 * @return
	 */
	@SystemControllerLog(description="导出用户信息")
	@RequestMapping(value = "/export")
	@ResponseBody
	public String export(HttpServletRequest request, HttpServletResponse response, UserDetail user) {
		List<UserDetailExportDto> dataset = this.userDetailService.findExportUserDetail(user);
		ExportExcelUtil<UserDetailExportDto> ex = new ExportExcelUtil<UserDetailExportDto>();
		String[] headers = { "用户名", "昵称", "真实名称", "性别", "生日", "身份证", "爱好", "邮箱", "地址", "手机", "qq", "积分", "金币", "预存款",
				"修改时间", "注册时间", "最后登录时间" };
		try {
			response.setHeader("Content-Disposition", "attachment;filename=userList.xls");
			ex.exportExcelUtil(headers, dataset, response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}
