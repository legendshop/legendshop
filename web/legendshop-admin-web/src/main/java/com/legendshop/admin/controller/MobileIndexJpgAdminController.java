/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.admin.page.MobileAdminPage.MobileAdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.model.UserMessages;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Indexjpg;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.IndexJpgService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 手机端首页轮换图片.
 */
@Controller
@RequestMapping("/admin/indexjpg")
public class MobileIndexJpgAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(MobileIndexJpgAdminController.class);

	/** The index jpg service. */
	@Autowired
	private IndexJpgService indexJpgService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	
	/** 手机端html5  首页图片管理 查询 **/
	@RequestMapping("/mobile/query")
	public String mobileQuery(HttpServletRequest request, HttpServletResponse response, String curPageNO, Indexjpg indexjpg) {
		
		this.criteriaQuery(request, response, curPageNO, indexjpg, 1);
		
		return PathResolver.getPath(MobileAdminPage.INDEXJPG_MOBILE_LIST_PAGE);
	}
	
	/** 手机端html5  首页图片管理 保存 **/
	@RequestMapping(value = "/mobile/save")
	public String mobileSave(HttpServletRequest request, HttpServletResponse response, Indexjpg indexjpg) {
		indexjpg.setLocation(1); // 0： pc， 1: mobile
		String resultPath = this.indexSave(request, response, indexjpg);
		if(resultPath != null){
			return resultPath;
		}else{
			return PathResolver.getPath(RedirectPage.IJPG_MOBILE_QUERY);
		}
	}
	
	/** 手机端html5  首页图片管理 加载**/
	@RequestMapping(value = "/mobile/load")
	public String mobileLoad(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(MobileAdminPage.INDEXJPG_MOBILE_EDIT_PAGE);
	}
	
	
	/** 手机端html5  首页图片管理  更新 **/
	@RequestMapping(value = "/mobile/update/{id}")
	public String mobileUpdate(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		this.updateIndexjpg(request,response,id);
		return PathResolver.getPath(MobileAdminPage.INDEXJPG_MOBILE_EDIT_PAGE);
	}
	
	/** 手机端html5  首页图片管理  删除 **/
	@RequestMapping(value = "/mobile/delete/{id}")
	public String mobileDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id){
		String result = indexDelete(request,response,id);
		if (result != null) {
			return result;
		}
		return PathResolver.getPath(RedirectPage.IJPG_MOBILE_QUERY);
	}
	
	/**
	 * updateIndexjpg
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	private void updateIndexjpg(HttpServletRequest request, HttpServletResponse response,Long id){
		if (AppUtils.isBlank(id)) {
			throw new NotFoundException("indexjpg Id is non nullable",ErrorCodes.NON_NULLABLE);
		}
		Indexjpg indexjpg = indexJpgService.getIndexJpgById(id);
		request.setAttribute("index", indexjpg);
	}  
	

	/**
	 * Check max jpg num.
	 * 
	 * @param request
	 * @param num
	 * @return the string
	 */
	private String checkMaxJpgNum(HttpServletRequest request, HttpServletResponse response, Long num) {
		String result = null;
		Integer maxNum = systemParameterUtil.getMaxIndexJpg();
		if ((num != null) && (num >= maxNum)) {
			UserMessages uem = new UserMessages();
			uem.setTitle("系统设置不能上传多于" + maxNum + "张图片");
			uem.setCode(ErrorCodes.LIMITATION_ERROR);
			uem.addCallBackList("重新上传", "", PathResolver.getPath(BackPage.IJPG_EDIT_PAGE));
			request.setAttribute(UserMessages.MESSAGE_KEY, uem);
			result = handleException(request, uem);
		}
		return result;
	}

	/**
	 * CriteriaQuery
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param indexjpg
	 * @param location
	 */
	private void criteriaQuery(HttpServletRequest request, HttpServletResponse response,String curPageNO,Indexjpg indexjpg,int location){
		
		Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
 		}
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"start_time","end_time"});
		
		PageSupport<Indexjpg> ps = indexJpgService.getIndexJpgPageByData(curPageNO,indexjpg,location,pageSize,result);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("indexJpg", indexjpg);
	}
	
	/**
	 * indexSave
	 * 
	 * @param request
	 * @param response
	 * @param indexjpg
	 * @return
	 */
	private String indexSave(HttpServletRequest request, HttpServletResponse response, Indexjpg indexjpg){
		SecurityUserDetail user = UserManager.getUser(request);
		Long num = 0l;
		String result = checkMaxJpgNum(request, response, num);
		if (result != null) {
			return result;
		}
		String filename = null;
		MultipartFile formFile = indexjpg.getFile();// 取得上传的文件
		try {
			if (indexjpg.getId() != null) {
				// update indexjpg
				Indexjpg origin = indexJpgService.getIndexJpgById(indexjpg.getId());
				if ((formFile != null) && (formFile.getSize() > 0)) {
					// upload file
					filename = attachmentManager.upload(formFile);
					//保存附件表
	    			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.INDEXJPG);
					origin.setImg(filename);
				}
				
				origin.setDes(indexjpg.getDes());
				origin.setLink(indexjpg.getLink());
				origin.setTitle(indexjpg.getTitle());
				origin.setUploadTime(new Date());
				origin.setSeq(indexjpg.getSeq());
				
				indexJpgService.updateIndexjpg(origin);
				
			} else {
				// save indexjpg
				// upload file
				if ((formFile != null) && (formFile.getSize() > 0)) {
					filename = attachmentManager.upload(formFile);
					indexjpg.setImg(filename);
				}

				indexJpgService.saveIndexjpg(indexjpg);
				if(AppUtils.isNotBlank(filename)){
					//保存附件表
					attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filename, formFile, AttachmentTypeEnum.INDEXJPG);
				}
			}

		} catch (Exception e) {
			throw new BusinessException(e, "Save Indexjpg error", ErrorCodes.BUSINESS_ERROR);
		}
		
		//清理IndexJpg
		indexJpgService.evictIndexJpgCache();
		
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return null;
	}
	
	/**
	 * indexDelete
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	private String indexDelete(HttpServletRequest request, HttpServletResponse response,Long id){
		Indexjpg indexjpg = indexJpgService.getIndexJpgById(id);
		checkNullable("indexjpg", indexjpg);
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		log.debug("{} delete indexjpg {}", userName, id);
		indexJpgService.deleteIndexJpg(indexjpg);
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return null;
	}
}
