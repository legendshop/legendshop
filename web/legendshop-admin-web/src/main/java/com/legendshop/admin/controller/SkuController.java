/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;

/**
 * 单品SKU控制器
 *
 */
@Controller
@RequestMapping("/admin/sku")
public class SkuController extends BaseController{
	
	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductPropertyValueService productPropertyValueService;
	
	/**
	 * 查询单品SKU列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Sku sku) {
		PageSupport<Sku> ps = skuService.getSkuPage(curPageNO, sku);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("sku", sku);

		return "/sku/skuList";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.SKU_LIST_PAGE);
	}
	
	/**
	 * 保存单品SKU
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Sku sku) {
		skuService.saveSku(sku);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/sku/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.SKU_LIST_QUERY);
	}
	
	/**
	 * 删除单品SKU表
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Sku sku = skuService.getSku(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()), sku.getUserName());
		// if(result!=null){
		// return result;
		// }
		skuService.deleteSku(sku);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/sku/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.SKU_LIST_QUERY);
	}
	
	/**
	 * 查看单品SKU表详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Sku sku = skuService.getSku(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()), sku.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("sku", sku);
		return "/sku/sku";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.SKU_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/sku/sku";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.SKU_EDIT_PAGE);
	}
	
	/**
	 * 更新单品SKU
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Sku sku = skuService.getSku(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()), sku.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("sku", sku);
		return "forward:/admin/sku/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.SKU_EDIT_PAGE);
	}
	
	/**
	 * 判断是否删除规格属性
	 * @param propId
	 * @return
	 */
	@RequestMapping(value="/ifDeleteProductPropertyValue")
	@ResponseBody
	public boolean ifDeleteProductPropertyValue(Long propId,String propValue){
		if(AppUtils.isBlank(propValue)){
			return false;
		}		
		ProductPropertyValue productPropertyValue = productPropertyValueService.getProductPropertyValueInfo(propValue,propId);
		if(productPropertyValue != null) {
			List<Sku> list = skuService.findSkuByProp(String.format("%s:%s",propId,productPropertyValue.getValueId()));
			if (AppUtils.isBlank(list)) {
				return false;
			}
			return true;
		}
		return false;
	}

}
