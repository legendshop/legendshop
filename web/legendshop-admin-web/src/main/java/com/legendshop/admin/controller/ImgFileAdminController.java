/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 产品图片控制器.
 */
@Controller
@RequestMapping("/admin/imgFile")
public class ImgFileAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ImgFileAdminController.class);

	/** 产品图片服务. */
	@Autowired
	private ImgFileService imgFileService;
	
	@Autowired
    private AttachmentManager attachmentManager;

	/**
	 * 查找产品图片.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param imgFile
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ImgFile imgFile) {
		Long productId = (Long) request.getAttribute("productId");
		if (productId == null) {
			String productIdStr = request.getParameter("productId");
			if (AppUtils.isNotBlank(productIdStr))
				productId = Long.valueOf(productIdStr);
		}
		if (productId == null) {
			throw new NotFoundException("miss productId", ErrorCodes.NON_NULLABLE);
		}
		if (productId != null) {
			imgFile.setProductId(productId);
			Product product = imgFileService.getProd(productId);
			request.setAttribute("prod", product);

		}
		PageSupport<ImgFile> ps = imgFileService.getImgFileList(curPageNO,productId,imgFile);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.IMG_LIST_PAGE);
	}

	/**
	 * 保存.
	 * 
	 * @param request
	 * @param response
	 * @param imgFile
	 * @return the string
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ImgFile imgFile) {
		if (imgFile.getFile() != null) {
			SecurityUserDetail user = UserManager.getUser(request);
			String filePath = attachmentManager.upload(imgFile.getFile());
			imgFileService.saveImgFile(user.getUsername(), user.getUserId(),user.getShopId(),imgFile,filePath);
			saveMessage(request, ResourceBundleHelper.getSucessfulString());
		}
		request.setAttribute("productId", imgFile.getProductId());
		return PathResolver.getPath(FowardPage.IMG_LIST_QUERY);

	}

	/**
	 * 更新.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ImgFile imgFile = imgFileService.getImgFileById(id);
		log.info("{}, delete ImgFile filePath {}", imgFile.getUserName(), imgFile.getFilePath());
		imgFileService.delete(id);
		request.setAttribute("productId", imgFile.getProductId());
		saveMessage(request, ResourceBundleHelper.getSucessfulString());
		return PathResolver.getPath(FowardPage.IMG_LIST_QUERY);
	}

	/**
	 * 根据id加载产品图片.
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ImgFile imgFile = imgFileService.getImgFileById(id);
		request.setAttribute("bean", imgFile);
		return PathResolver.getPath(BackPage.IMG_EDIT_PAGE);
	}

	/**
	 * 更新.
	 * 
	 * @param imgFile
	 * @return the string
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable ImgFile imgFile) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		imgFile.setUserName(userName);
		imgFileService.update(imgFile);
		return PathResolver.getPath(FowardPage.IMG_LIST_QUERY);
	}
	
	/**
	 * 商品图片上下线
	 * @param request
	 * @param response
	 * @param fileId
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{fileId}/{status}", method = RequestMethod.GET)
	public @ResponseBody
	Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long fileId,
			@PathVariable Integer status) {
		ImgFile imgFile = imgFileService.getImgFileById(fileId);
		if (imgFile == null) {
			return -1;
		}
		if (!status.equals(imgFile.getStatus())) {
//			if (!FoundationUtil.haveViewAllDataFunction(request)) {
//				String loginName = UserManager.getUserName(request.getSession());
//				// user
//				if (!loginName.equals(imgFile.getUserName())) {
//					return -1;
//				}
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)) {
//					imgFile.setStatus(status);
//					imgFileService.update(imgFile);
//				}
//			} else {
//				// admin
//				if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
//					imgFile.setStatus(status);
//					imgFileService.update(imgFile);
//				}
//			}
			
			// admin
			if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status) || Constants.STOPUSE.equals(status)) {
				imgFile.setStatus(status);
				imgFileService.update(imgFile);
			}
			
		}
		return imgFile.getStatus();
	}

}
