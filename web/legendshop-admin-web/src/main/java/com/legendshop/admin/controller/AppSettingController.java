/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.AppSetting;
import com.legendshop.model.dto.CategorySettingDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * The Class AppSettingController
 * 移动端基础设置控制器
 */
@Controller
@RequestMapping("/admin/appSetting")
public class AppSettingController {
	
	@Autowired
	private ConstTableService constTableService;
	
    /**
	 * 主题风格
	 */
	@RequestMapping(value="/themeStyle")
	public String themeStyle(HttpServletRequest request, HttpServletResponse response){
		
		//获取基本设置
		AppSetting appSetting = getAppSetting();
		request.setAttribute("appSetting", appSetting);
		
		return PathResolver.getPath(AdminPage.THEME_STYLE);
	}
	
	
	
	/**
	 * 保存主题风格
	 * @return
	 */
	@RequestMapping(value="/saveThemeStyle")
	@ResponseBody
	public String saveThemeStyle(HttpServletRequest request, HttpServletResponse response, AppSetting appSettingDto){
		
		if (appSettingDto == null || appSettingDto.getTheme() == null || appSettingDto.getColor() == null) {
			return "请选择主题风格!";
		}
		
		//获取原有的配置
		AppSetting appSetting = getAppSetting();
		if (AppUtils.isBlank(appSetting)) {
			return "保存失败，该基础设置已不存在，请联系后台";
		}
		
		//更新为最新选择的主题风格
		appSetting.setTheme(appSettingDto.getTheme());
		appSetting.setColor(appSettingDto.getColor());
		
		String setting = JSONUtil.getJson(appSetting);
		constTableService.updateConstTableByType("APP_SETTING", "APP_SETTING", setting);
		
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 分类设置
	 */
	@RequestMapping(value="/categorySetting")
	public String saveCategorySetting(HttpServletRequest request, HttpServletResponse response){
		
		//获取基本设置
		AppSetting appSetting = getAppSetting();
		
		//获取分类设置
		CategorySettingDto categorySettingDto = null;
		if (AppUtils.isNotBlank(appSetting) && AppUtils.isNotBlank(appSetting.getCategorySetting())) {
			
			categorySettingDto = JSONUtil.getObject(appSetting.getCategorySetting(), CategorySettingDto.class);
		}
		
		request.setAttribute("categorySetting", categorySettingDto);
		return PathResolver.getPath(AdminPage.CATEGORY_SETTING);
	}
	
	
	/**
	 * 保存分类设置
	 * @return
	 */
	@RequestMapping(value="/saveCategorySetting")
	@ResponseBody
	public String saveCategorySetting(HttpServletRequest request, HttpServletResponse response, @RequestBody String categorySetting){
		
		if (categorySetting == null) {
			return "请选择展示分类!";
		}
		
		AppSetting appSetting = getAppSetting();
		if (AppUtils.isBlank(appSetting)) {
			return "保存失败，该基础设置已不存在，请联系后台";
		}
		
		appSetting.setCategorySetting(categorySetting);
		String setting = JSONUtil.getJson(appSetting);
		constTableService.updateConstTableByType("APP_SETTING", "APP_SETTING", setting);
		
		return Constants.SUCCESS;
	}
    
	
	/**
	 * 获取App装修基本设置
	 * @return  AppSetting
	 */
	private AppSetting getAppSetting(){
		
		ConstTable constTable = constTableService.getConstTablesBykey("APP_SETTING", "APP_SETTING");
		AppSetting appSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			
			String setting = constTable.getValue();
			appSetting = JSONUtil.getObject(setting, AppSetting.class);
		}
		return appSetting;
	}

}
