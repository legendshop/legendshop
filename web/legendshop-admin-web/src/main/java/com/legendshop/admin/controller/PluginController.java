/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.util.UpdateMenuProcessor;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.plugins.Plugin;

/**
 * 插件控制器.
 */
@Controller
@RequestMapping("/admin/system/plugin")
public class PluginController extends BaseController {

	@Autowired
	private UpdateMenuProcessor updateMenuProcessor;
	/**
	 * 查找系统缓存.
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param systemParameter
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response) {
		List<Plugin> plugins = PluginRepository.getInstance().getPlugins();
		request.setAttribute("pluginList", plugins);
		return PathResolver.getPath(AdminPage.PLUGIN_LIST_PAGE);
	}

	/**
	 * 打开
	 * 
	 * @param request
	 * @param response
	 * @param pluginId
	 * @return
	 */
	@SystemControllerLog(description="打开插件")
	@RequestMapping("/turnon/{pluginId}")
	public @ResponseBody String turnOn(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String pluginId) {
		String result = PluginRepository.getInstance().turnOn(pluginId);
		// 发布更新菜单指令
		updateMenuProcessor.process(null);
		return result;
	}

	/**
	 * 关闭
	 * 
	 * @param request
	 * @param response
	 * @param pluginId
	 * @return
	 */
	@SystemControllerLog(description="关闭插件")
	@RequestMapping("/turnoff/{pluginId}")
	public @ResponseBody String turnOff(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String pluginId) {
		String result = PluginRepository.getInstance().turnOff(pluginId);
		// 发布更新菜单指令
		updateMenuProcessor.process(null);
		return result;
	}
}
