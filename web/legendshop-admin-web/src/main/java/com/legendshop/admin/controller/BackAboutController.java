/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.AppUtils;

/**
 * 关于后台
 * @author 关开发
 */
@Controller
@RequestMapping("/admin/system/about")
public class BackAboutController extends BaseController {
	
	@Autowired
    private SystemConfigService systemConfigService;
	
    @RequestMapping("/query")
    @ResponseBody
    public String query(HttpServletRequest request, HttpServletResponse response) {
    	SystemConfig systemConfig = systemConfigService.getSystemConfig();
    	
    	String content = null;
    	if(AppUtils.isNotBlank(systemConfig)){
    		content = systemConfig.getBackAbout();
    	}
        return content;
    }
}
