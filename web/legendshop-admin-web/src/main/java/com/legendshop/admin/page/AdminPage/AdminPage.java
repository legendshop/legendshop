/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.AdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum AdminPage.
 */
public enum AdminPage implements PageDefinition {
	
	//首页
	ADMIN_INDEX_PAGE("/frame/index"),
	
	//商品
	PRODUCT_LIST_PAGE("/prod/prodList"),
	
	//商品列表详情
	PRODUCT_CONTENT_LIST_PAGE("/prod/prodContentList"),
	
	//热门商品
	HOT_LIST_PAGE("/hotsearch/hotsearchList"),
	
	HOT_EDIT_PAGE("/hotsearch/hotsearch"),
	
	//商品咨询
	PROD_CONSULT_LIST_PAGE("/prodConsult/productConsultList"),
	
	//商品咨询列表
	PROD_CONSULT_CONTENT_LIST_PAGE("/prodConsult/productConsultContentList"), 
	
	PROD_CONSULT_EDIT_PAGE("/prodConsult/productConsult"),
	
	//商品分组
	PROD_GROUP_LIST_PAGE("/prod/prodGroupList"),
	
	PROD_GROUP_EDIT_PAGE("/prod/prodGroup"),
	
	DISCOVER_ARTICLE_PAGE("/discoverArticle/discoverArticle"),
	
	DISCOVER_ARTICLE_PROD_PAGE("/discoverArticle/discoverArticleProd"),
	//分组内容列表
	PROD_CONTENT_GROUP_LIST_PAGE("/prod/prodGroupContentList"),

	//分组商品列表
	GROUP_PRODUCT_LIST_PAGE("/prod/groupProductList"),

	// 分组商品列表内容
	GROUP_PRODUCT_CONTENT_LIST_PAGE("/prod/groupProductContentList"),

	//商品评论
	PROD_COMM_LIST_PAGE("/prodComment/prodCommentList"),
	
	//商品评论列表
	PROD_COMM_CONTENT_LIST_PAGE("/prodComment/prodCommentContentList"), 
	
	PROD_COMM_EDIT_PAGE("/prodComment/prodComment"),
	
	PROD_COMM_DETAIL_PAGE("/prodComment/prodCommentDetail"),
	
	//商品规格属性
	SPECPROPERTY_LIST_PAGE("/productProperty/specProperty"),
	
	PRODUCTPROPERTY_EDIT_PAGE("/productProperty/productProperty"),
	
	/***加载商品**/
	PRODUCT_THEME_LOAD("/theme/productThemeLoad"),
	
	//参数组页面
	PARAMGROUP_LIST_PAGE("/paramGroup/paramGroupList"),
	
	PARAMGROUP_EDIT_PAGE("/paramGroup/paramGroup"),
	//类型页面
	PRODTYPE_LIST_PAGE("/prodType/prodTypeList"),
	
	PRODTYPE_EDIT_PAGE("/prodType/prodTypeEdit"),
	
	PRODTYPE_PAGE("/prodType/prodType"),
	
	AFTERSALE_OVERLAY("/afterSale/afterSaleOverlay"),
	
	
	CATEGORY_EDIT_PAGE("/category/category"), 
	
	CATEGORY_LOADCATEGORY_PAGE("/category/loadCategory"),
	
	CATEGORY_LOADCATEGORY_CHECKBOX_PAGE("/category/loadCategoryCheckbox"),
	
	//分类页面
	CATEGORY_LIST_PAGE("/category/categoryList"),
	
	CATEGORY_RECOMM_LIST("/category/recommList"),
	
	CATEGORY_RECOMM_EDIT("/category/recommEdit"),
	
	/** The sort prod type edit page. */
	CATEGORY_PROD_TYPE_PAGE("/category/prodType"),
	
	//品牌页面
	BRAND_LIST_PAGE("/brand/brandList"),
	
	//品牌内容列表
	BRAND_CONTENT_LIST_PAGE("/brand/brandContentList"),
	
	BRAND_EDIT_PAGE("/brand/brand"),
	
	BRAND_CHECK_PAGE("/brand/checkbrand"),
	
	//公告页面
	PUB_LIST_PAGE("/pub/pubList"),
	
	PUB_EDIT_PAGE("/pub/pub"),
	
	//添加商品页面
	DISCOVER_ADDFULLPROD_PAGE("/discoverArticle/discoverAddFullProd"),
	
	//商品table页面
	PRODTABLE_PAGE("/discoverArticle/prodTable"),
	
	//广告栏管理
	/** 广告管理 **/
	ADV_LIST_PAGE("/advertisement/advertisementList"),
	
	/** 广告编辑 **/
	ADV_EDIT_PAGE("/advertisement/advertisement"),
		
	//图片管理
	INDEXJPG_LIST_PAGE("/indexjpg/indexjpgList"),
	
	INDEXJPG_EDIT_PAGE("/indexjpg/indexjpg"),
	
	//链接管理
	EXTERNALLINK_LIST_PAGE("/externallink/externallinkList"),
	
	EXTERNALLINK_EDIT_PAGE("/externallink/externallink"),
	
	//楼层管理
	FLOOR_LIST_PAGE("/floor/floorList"),
	
	FLOOR_EDIT_PAGE("/floor/floor"),
	
	SUBFLOOR_EDIT_PAGE("/subFloor/subFloor"),

	//店铺管理
	SHOPDETAIL_LIST_PAGE("/shopDetail/shopDetailList"),
	
	SHOPDETAIL_EDIT_PAGE("/shopDetail/shopDetail"),
	
	SHOP_COMPANY_PAGE("/shopDetail/shopCompanyDetail"),
	
	SHOPDETAIL_AUDIT_LIST_PAGE("/shopDetail/shopAuditLogtList"),
	
	//二级域名
	SEC_DOMAIN_NAME_LIST("/secDomainName/secDomainNameList"),
	
	SEC_DOMAIN_NAME_SETTING("/secDomainName/secDomainNameSetting"),
	
	//商家结算
	SHOPORDERBILL_LIST("/shopBill/shopBillList"),
	
	//商家结算列表
	SHOPORDERBILL_CONTENT_LIST("/shopBill/shopBillContentList"),
	
	SHOPORDERBILL_DETAIL("/shopBill/shopBill"),
	
	//商家结算档期
	SHOPBILLPERIOD_LIST_PAGE("/shopBillPeriod/shopBillPeriodList"),
	
	//支付管理
	PAYTYPE_LIST_PAGE("/payType/payTypeList"),
	
	PAYTYPE_EDIT_PAGE("/payType/payType"),
	
	//物流公司管理
	DELIVERYCORP_LIST_PAGE("/deliveryCorp/deliveryCorpList"),
	
	DELIVERYCORP_EDIT_PAGE("/deliveryCorp/deliveryCorp"),
	
	//新手帮助管理
	MINECRAFTLIST_PAGE("/help/minecraftList"),
	
	TABLELIST_PAGE("/help/tablelist"),
		
	MINECRAFT_PAGE("/help/minecraft"),
		
	//打印模板管理
	PRINTTMPL_LIST_PAGE("/printTmpl/printTmplList"),
	
	PRINTTMPL_EDIT_PAGE("/printTmpl/printTmpl_edit"),
	
	PRINTTMPL_ADD_PAGE("/printTmpl/printTmpl"),
	
	//配送方式管理
	DELIVERYTYPE_LIST_PAGE("/deliveryType/deliveryTypeList"),
	
	DELIVERYTYPE_EDIT_PAGE("/deliveryType/deliveryType"),
	
	//标签管理
	TAG_LIST_PAGE("/tag/tagList"),
	
	TAG_EDIT_PAGE("/tag/tag"),
	
	TAGITEMS("/tag/tagItem"),
	
	//库存管理
	STOCK_CONTROL_PAGE("/stockAdmin/stockAdminList"),
	
	STOCK_CAUTION_PAGE("/stockAdmin/stockAdmin"),
	
	//手机端HTML 首页图片管理
	INDEXJPG_MOBILE_LIST_PAGE("/mobileIndexJpg/indexjpgList"),
	
	INDEXJPG_MOBILE_EDIT_PAGE("/mobileIndexJpg/indexjpg"),
	
	//手机端HTML 首页楼层管理
	MOBILE_FLOOR_LIST_PAGE("/mobileFloor/mobileFloorList"),
	
	MOBILE_FLOOR_EDIT_PAGE("/mobileFloor/mobileFloor"),
	
	MOBILE_FLOOR_DECORATE_PAGE("/mobileFloor/decoratePage"),
	
	//订单管理
	PROCESSING_ORDER_LIST_PAGE("/order/processingOrder"),
	
	//订单内容
	PROCESSING_ORDER_CONTENT_LIST_PAGE("/order/processingOrderContent"), 
	
	PRESELL_ORDER_LIST_PAGE("/order/presellOrderList"),
	
	//预售订单列表
	PRESELL_ORDER_CONTENT_LIST_PAGE("/order/presellOrderContentList"),
	
	PRESELL_ORDER_ADMIN_DETAIL("/order/presellOrderAdminDetail"),
	
	ORDER_ADMIN_DETAIL("/order/orderAdminDetail"),
	
	ORDER_RECEIVE_MONEY("/order/receiveMoney"),
	
	ORDER_SETTING("/order/orderSetting"),
	
	PROCESSED_ORDER_LIST_PAGE("/order/processedOrder"),
	
	//退货管理
	RETURN_LIST("/refund/returnList"),
	
	//退货列表
	RETURN_CONTENT_LIST("/refund/returnContentList"), 
	
	//退款退货详情
	RETURN_DETAIL("/refund/returnDetail"),
	
	//退款管理
	REFUND_LIST("/refund/refundList"),
	
	//退款管理列表
	REFUND_CONTENT_LIST("/refund/refundContentList"),
	
	//退款详情
	REFUND_DETAIL("/refund/refundDetail"),
	
	//举报管理
	ACCUSATION_LIST_PAGE("/accusation/accusationList"),
	
	//举报列表
	ACCUSATION_CONTENT_LIST_PAGE("/accusation/accusationContentList"),
	
	//应用服务管理
	FUNCTIONSERVICE_LIST_PAGE("/functionService/functionServiceList"),
	
	FUNCTIONSERVICEDETAIL_PAGE("/functionService/functionServiceDetail"),
	
	SUBSRCIBESERVICE_ORDER_LIST_PAGE("/functionService/subsrcibeServiceOrderList"),
	
	SUBSRCIBESERVICEDETAIL_PAGE("/functionService/subsrcibeServiceDetail"),
	
	/**套餐详情页**/
	SERVICE_SETMEAL_DETAIL("/functionService/serviceSetmeal"),
	
	/** 尚未处理的投诉. */
	UNHANDLE_ACCUSATION_LIST_PAGE("/accusation/unHandleaccusationList"),
	
	
	ACCUSATION_EDIT_PAGE("/accusation/accusation"),
	
	REPROT_PROCESSING_PAGE("/accusation/reportProcessing"),
	
	//举报类型管理
	ACCUSATIONTYPE_LIST_PAGE("/accusationType/accusationTypeList"),
	
	ACCUSATIONTYPE_EDIT_PAGE("/accusationType/accusationType"),
	
	ACCUSATIONSUBJECT_EDIT_PAGE("/accusationSubject/accusationSubject"),
	
	NEWS_CATEGORY_LIST("/newsCategory/newsCategoryList"),
	
	NEWS_CATEGORY("/newsCategory/newsCategory"),
	
	NEWS_LIST("/news/newsList"),
	
	NEWS_POSITION_LIST("/news/newsPositionList"),
	
	NEWS_EDIT_PAGE("/news/news"),
	
	FEEDBACK_LIST_PAGE("/userFeedBack/userFeedBackList"),
	
	USER_DETAIL_LIST_PAGE("/userDetail/userDetailList"),
	
	/**商家用户管理页面*/
	SHOPUSER_LIST_PAGE("/shopUser/shopUserList"),
	
	SHOPUSER_DETAIL_PAGE("/shopUser/shopUserDetail"),

	USER_INFO("/userInfo/userInfo"),

	USER_ORDER_INFO("/userInfo/userOrderInfo"),
	
	EXPENSES_RECORD("/userInfo/expensesRecord"),
	
	USER_VISIT_LOG("/userInfo/userVisitLog"),
	
	USER_PROD_COMM("/userInfo/userProdComm"),
	
	USER_PRODCONS("/userInfo/userProdCons"),
	
	USERGRADE_LIST_PAGE("/userGrade/userGradeList"),
	
	USERGRADE_EDIT_PAGE("/userGrade/userGrade"),
	
	LOGIN_HIST_LIST_PAGE("/loginhistory/loginHistoryList"),
	
	LOGIN_HIST_SUM_PAGE("/loginhistory/loginHistorySum"),
	
	SHOPGRADE_LIST_PAGE("/shopGrade/shopGradeList"),
	
	SHOPGRADE_EDIT_PAGE("/shopGrade/shopGrade"),
	
	SYSTEMCONFIG_EDIT_PAGE("/systemConfig/systemConfig"),
	
	PARAM_LIST_PAGE("/systemParameter/systemParameterList"),
	
	PARAM_EDIT_PAGE("/systemParameter/systemParameter"),
	
	PLUGIN_LIST_PAGE("/plugin/pluginList"),
	
	NAVIGATION_LIST_PAGE("/navigation/navigationList"),
	
	NAVIGATION_EDIT_PAGE("/navigation/navigation"),
	
	NAVIGATIONITEM_EDIT_PAGE("/navigationItem/navigationItem"),
	
	SENSITIVEWORD_LIST_PAGE("/sensitiveWord/sensitiveWordList"),
	
	SENSITIVEWORD_EDIT_PAGE("/sensitiveWord/sensitiveWord"),
	
	DISTRICT_LIST_PAGE("/district/districtList"),
	
	RPOVINCE_PAGE("/district/province"),
	
	CITY_PAGE("/district/city"),
	
	AREA_PAGE("/district/area"),
	
	/** The province content list page. */
	PROVINCE_CONTENT_LIST_PAGE("/district/provinceContent"),
	
	/** The city content list page. */
	CITY_CONTENT_LIST_PAGE("/district/cityContent"),
	
	/** The area content list page. */
	AREA_CONTENT_LIST_PAGE("/district/areaContent"),
	
	SMSLOG_LIST_PAGE("/sMSLog/sMSLogList"),
	
	SYS_SMSLOG_LIST_PAGE("/sMSLog/sysSmsLogList"),
	
	EVENT_LIST_PAGE("/event/eventList"),
	
	EVENT_EDIT_PAGE("/event/event"),
	
	SYSTEM_MESSAGES_LIST_PAGE("/systemMessages/systemMessageList"),
	
	SYSTEM_MESSAGES_EDIT_PAGE("/systemMessages/systemMessages"),
	
	SYSTEM_MESSAGES_SHOP_LIST("/systemMessages/systemMessagesShopList"),
	
	FILE_EDIT_PAGE("/file/edit"),
	
	CACHE_LIST_PAGE("cache/cacheList"),
	
	HTML_LIST_PAGE("/cache/htmlList"),
	
	PARAM_LIST_QUERY("/admin/system/systemParameter/query"),

	VARIABLE(""), 
	
	ADMINUSER_LIST_PAGE("/adminUser/adminUserList"), 
	
	ADMINUSER_UPDATE_PAGE("/adminUser/updateAdminUser"), 
	
	ADMINUSER_EDIT_PAGE("/adminUser/adminUser"),
	
	/** 更新管理员密码 **/
	UPDATE_ADMIN_PWD("/adminUser/updatePwd"), 
	
	SHOP_DETAIL_STORE_QUERY("/shopDetail/queryStore"),
	
	SHOP_DETAIL_STORE_ALL_QUERY("/shopDetail/queryAllStore"),
	
	SHOP_DETAIL_SEL_SHOP("/shopDetail/selShop"),
	
	SHOP_REAL_NAME_LIST("/storeRealName/storeRealList"),
	/**实名审核*/
	SHOP_REAL_NAME_AUDIT("/storeRealName/storeRealAudit"),
	
	SHOP_WIDTH_DRAW_LIST("/storeWidthDraw/storeWidthDrawList"),
	/**导购列表*/
	SHOP_DETAIL_STOREGUIDE_QUERY("/shopDetail/storeGuideQuery"), 
	
	SHOP_DETAIL_STOREGUIDE_ALL_QUERY("/shopDetail/storeGuideAllQuery"),
	
	SHOP_STORE_DETAIL("/shopDetail/storeDetail"),
	
	SHOP_STOREGUIDE_DETAIL("/shopDetail/storeGuideDetail"),
	
	SHOP_AUDIT("/audit/auditList"),
	
	LUCENE_EDIT_PAGE("/lucene/reindexer"), 
	
	USER_PDCASHLOG("/preDeposit/userPdCashLog"),
	
	/**图片空间*/
	IMAGE_LIST_QUERY("/images/images"),
	
	/**图片控件*/
	IMAGE_PROP_QUERY("/images/imagesProp"),
	
	/**图片控件*/
	IMAGE_REMOTE_QUERY("/images/remoteImages"), 
	
	IMAGE_REMOTE_PAGE("/images/imagePage"), 
	
	/**帮助管理*/
	ADMIN_DOC_LIST_PAGE("/adminDoc/adminDocList"),
	
	ADMIN_DOC_EDIT_PAGE("/adminDoc/adminDocEdit"),
	
	ADMIN_DOC_PAGE("/adminDoc/adminDoc"),
	
	/**支付设置*/
	PAY_SITE("/payment/paySite"),
	
	ALIPAY_PAYTYPE_EDIT("/payment/aliPayPayTypeEdit"),
	
	WX_APP_PAYTYPE_EDIT("/payment/WxAppPayTypeEdit"),
	
	WX_PAYTYPE_EDIT("/payment/WxPayTypeEdit"),
	
	SIMULATE_PAY_EDIT("/payment/simulateTypeEdit"),
	
	/**提现相关*/
	SHOP_WITHDRAWCASH_lIST("/shopPreDeposit/shopWithdrawCashList"),
	
	/**充值相关*/
	SHOP_RECHARGE_LIST("/shopPreDeposit/shopRechargeList"),

	PDWITHDRAWCASH_EDIT("/shopPreDeposit/shopWithdrawCash"),
	
	SHOP_WITHDRAWCASH_INFO_PAGE("/shopPreDeposit/shopWithdrawCashInfo"),
	
	/**发票相关*/
	SHOP_INVOICE_MANAGE_LIST("/shopInvoice/shopInvoiceManageList"),
	
	SHOP_INVOICE_DETAIL("/shopInvoice/shopInvoiceDetail"),
	
	STORE_PUSH_SETTING("/storePushConfig/storePushSetting"),
	
	NEW_TEMPLATE("/systemParameter/systemNewTemplate"), 
	
	EDIT_NEW_TEMPLATE("/systemParameter/systemEditTemplate"),
	
	/*销售统计*/
	SALES_OVERVIEW("/data/salesOverview"),
	
	SALES_OVERVIEW_LIST("/data/salesOverviewList"),
	
	SALES_STORE_OVERVIEW("/data/salesStoreOverview"),
	
	SALES_STORE_OVERVIEW_LIST("/data/salesStoreOverviewList"),
	
	
	/**操作错误**/
	OPERATION_ERROR("/admin-404"),
	
	/** 微信概况统计 */
	WEIXIN_SURVEY_OVERVIEW("/microMall/weChatSurveyCount"),
	
	/**实时消息**/
	WECHAT_RECEIVETEXT("/microMall/receivetext"),
	WECHAT_RECEIVETEXT_LIST("/microMall/receivetextList"),
	WECHAT_RECEIVETEXT_CHATWITH("/microMall/receievetextChatWith"),
	WECHAT_RECEIVETEXT_CHATWITH_LIST("/microMall/receieveChatWithList"),
	WECHAT_MEDIA("/microMall/weixinMedia"),
	WECHAT_TEMPLATE_MEDIA("/microMall/weixinTemplateMedia"),
	WECHAT_REPLYNEWSITEM("/microMall/replyNewsItem"),
	
	
	/*消息回复*/
	FOLLOW_MSG("/microMall/followMsg"),
	EMPTYKEY_MSG("/microMall/emptyKeyMsg"),
	KEY_MSG("/microMall/keyMsg"),
	
	
	/*微信图文*/
	WECHAT_GRAPHIC("/microMall/wechatGraphic"),
	WECHAT_GRAPHIC_LIST("/microMall/wechatGraphicList"),
	WECHAT_GRAPHIC_ADD("/microMall/wechatGraphicAdd"),
	
	/*高级图文*/
	SUPER_GRAPHIC("/microMall/superGraphic"),
	SUPER_GRAPHIC_ADD("/microMall/superGraphicAdd"),
	
	/*图文弹层*/
	URL_POP("/microMall/weChatPop"),
	CUSTOMIZE_POP("/microMall/customizePop"),
	OPERATION_POP("/microMall/operationPop"),
	CATEAGE_POP("/microMall/cateagePop"),
	PRODUCT_POP("/microMall/productPop"),
	PRODUCT_POP_LIST("/microMall/productPopList"),
	WECHAT_GRAPHIC_POP("/microMall/graphicPop"),
	WECHAT_GRAPHIC_POP_LIST("/microMall/graphicPopList"),
	WECHAT_ADD_EMPTY_KEY_MSG("/microMall/addEmptyKeyMsg"),//自动回复及无关键词回复弹层
	WECHAT_ADD_KEY_MSG("/microMall/addKeyMsg"),//关键词弹层回复
	WECHAT_NEWS_POPBACK("/microMall/myNewsBack"),
	
	/*自定义菜单*/
	CUSTOM_CLASS("/microMall/customClass"),
	
	CUSTOM_MENU("/microMall/customMenu"),
	
	
	/*系统管理 图片管理*/
	IMAGE_SITE("/imageSite/imageSite"),
	
	OPEN_IMAGE_SITE("/imageSite/openImageSite"),
	
	OPEN_BATCH_IMAGE_SITE("/imageSite/openBatchImageSite"), 
	
	OPEN_IMAGE_PAGE_SITE("/imageSite/imagePage"),
	
	OPEN_IMAGE_PAGE_SITE_MOBILE("/imageSite/imageMobilePage"),
	
	OPEN_IMAGE_PAGE_OPEN_SITE("/imageSite/imageOpenPage"),
	
	//编辑Android界面
	ANDROID_VERSION_EDIT("/appVersion/androidVersionEdit"),
	
	//编辑IOS界面
	IOS_VERSION_EDIT("/appVersion/iosVersionEdit"),
	
	/** APP版本上传页面 */
	APP_UPLOAD_PAGE("/appVersion/uploadPage"),
	
	
	/**微信小程序模板草稿列表**/
	DRAFT_TEMPLATE_LIST_PAGE("/microProgram/draftTemplateList"),
	
	/**微信小程序代码模板列表**/
	CODE_TEMPLATE_LIST_PAGE("/microProgram/codeTemplateList"),
	
	/**已授权微信小程序**/
	AUTHORIZEDPROGRAM_LIST_PAGE("/microProgram/authorizedProgramList"),
	
	/**小程序详情**/
	AUTHORIZEDPROGRAM_DETAIL_PAGE("/microProgram/authorizedProgramDetail"),
	
	/**基础信息**/
	BASEINFO_PAGE("/microProgram/baseInfo"),
	
	/**服务器域名**/
	SERVICE_DOMAIN_NAME_PAGE("/microProgram/serviceDomainName"),
	
	/**编辑服务器域名**/
	UPDATE_DOMAIN_NAME_PAGE("/microProgram/updateServiceDomainName"),
	
	/**体验者管理**/
	PROGRAM_EXPERIENCE_LIST_PAGE("/microProgram/programExperience"),
	
	/**绑定体验者**/
	BIND_EXPERIENCE_PAGE("/microProgram/bindExperience"),
	
	/**小程序代码管理**/
	PROGRAM_CODE_MANAGE("/microProgram/programCodeManage"),
	
	/**上传代码页面*/
	UPLOAD_PROGRAM_CODE("/microProgram/uploadCode"),
	
	/**代码提交审核页面*/
	AUDIT_PROGRAM_CODE("/microProgram/auditCode"),
	
	
	ADV_ANALYSIS_PAGE("/visitLog/advAnalysisList"),
	
	ADV_ANALYSISDETAIL_PAGE("/visitLog/advAnalysisDetail"),
	
	
	APP_DECORATE("/appdecorate/decorate"),
	
	
	APPSTART_ADV_LIST("/appStartAdv/appStartAdvList"),
	
	APPSTART_ADV_EDIT("/appStartAdv/appStartAdvEdit"),
	
	
	ARTICLE_LIST_PAGE("/article/articleList"),
	
	ARTICLE_EDIT_PAGE("/article/articleEdit"),
	
	ARTICLE_COMMENT_LIST_PAGE("/articleComment/articleCommentList"),
	
	ARTICLE_COMMENT_INFO_PAGE("/articleComment/articleCommentInfo"),	
	
	CONFIG_TEST_LIST_PAGE("/configTest/configTestList"),
	
	HEADMENU_LIST("/headMenu/headMenuList"),
	
	HEADMENU_EDIT("/headMenu/headMenu"),
	
	
	ADMIN_404("/admin-404"),
	
	HEADLINE_LIST("/headline/headlineList"),
	
	HEADLINE_UPDATE("/headline/headlineUpdate"),
	
	HEADLINE_SAVE("/headline/headlineSave"),
	
	ADMIN_UPGRADE_PAGE("/dashboard/upgrade"),

	ADMIN_UPGRADE_BYKEY_PAGE("/dashboard/upgradeByKey"),
	
	MAILLOG_LIST_PAGE("/mailLog/mailLogList"),
	
	DISCOVER_ARTICLE_LIST_PAGE("/discoverArticle/discoverArticleList"),
	
	GRASS_LABEL_LIST_PAGE("/grass/grassLabelList"),
	
	GRASS_ARTICLE_LIST_PAGE("/grass/grassArticleList"),
	
	GRASS_ARTICLE_PAGE("/grassArticle/grassArticle"),
	
	GRASS_LABEL_PAGE("/grass/grassLabel"),
	
	NEWS_LIST_PAGE("/news/newsList"),
	
	NEWSCAT_LIST_PAGE("/newsCategory/newsCategoryList"),
	
	NEWSCAT_EDIT_PAGE("/newsCategory/newsCategory"),
	
	PARTNER_LIST_PAGE("/partner/partnerList"),
	
	PARTNER_EDIT_PAGE("/partner/partner"),
	
	PARTNER_CHANGE_PASSWORD_PAGE("/partner/partnerChangePassword"),
	
	REPORT_PURCHASERATE_PAGE("/report/purchaserate"),
	
	/*访问购买率列表*/
	REPORT_PURCHASERATE_CONTENT_PAGE("/report/purchaserateContent"),
	
	REPORT_SALEANALYSIS_PAGE("/report/saleAnalysis"),
	
	/*销售额总览页面*/
	REPORT_SALES_PAGE("/report/sales"),
	
	REPORT_SALERANK_PAGE("/report/salerank"),
	
	/*销售排名列表*/
	REPORT_SALERANK_CONTENT_PAGE("/report/salerankContent"),
	
	REPORT_PURCHASERANK_PAGE("/report/purchaserank"),
	
	THEME_LIST("/theme/themeList"),
	
	THEME_EDIT_PAGE("/theme/theme"),

	LOAD_SELECT_THEME("/theme/loadSelectTheme"),

	VLOG_LIST_PAGE("/visitLog/visitLogList"),

    /** 访问历史列表*/
	VLOG_CONTENT_LIST_PAGE("/visitLog/visitLogContentList"), 
	
	/** 店铺订单列表 */
	SHOP_PROCESSING_ORDER_LIST_PAGE("/order/shopProcessingOrder"),
	
	/** 商城商品列表页. */
	SHOP_PRODUCT_LIST_PAGE("/prod/shopProdList"),
	
	/** 店铺审核历史 **/
	SHOPAUDIT_LIST_PAGE("/shopDetail/shopAuditDetail"), 
	
	/** 客服 */
	IM("/im/imAdmin"),
	
	/** 后台 移动端装修页面 start  */
	PAGE_MANAGEMENT("/newAppDecorate/pageManagement"),
	
	PAGE_MANAGEMENT_CONTENT("/newAppDecorate/pageManagementContent"),
	
	CREATE_PAGE("/newAppDecorate/createPage"),
	
	//主题风格
	THEME_STYLE("/newAppDecorate/themeStyle"),
	
	//分类设置
	CATEGORY_SETTING("/newAppDecorate/categorySetting"),
	
	/** 后台 移动端装修页面  end */
	
	/** 移动端店铺首页装修管理  */
	SHOP_INDEX_APPDECORATE("/shopIndexAppDecorate/shopIndexAppDecorate"),
	
	SHOP_INDEX_APPDECORATE_CONTENT("/shopIndexAppDecorate/shopIndexAppDecorateContent"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value the value
	 */
	private AdminPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
