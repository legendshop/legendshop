/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.UserGradeService;

/**
 * 用户等级控制器
 *
 */
@Controller
@RequestMapping("/admin/system/userGrade")
public class UserGradeController extends BaseController {
	
	@Autowired
	private UserGradeService userGradeService;

	/**
	 * 查询用户等级列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param userGrade
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			UserGrade userGrade) {
		PageSupport<UserGrade> ps = userGradeService.getUserGradePage(curPageNO, userGrade);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userGrade", userGrade);
		return PathResolver.getPath(AdminPage.USERGRADE_LIST_PAGE);
	}

	/**
	 * 保存用户等级管理
	 * @param request
	 * @param response
	 * @param userGrade
	 * @return
	 */
	@SystemControllerLog(description="保存用户等级")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, UserGrade userGrade) {
		userGradeService.saveUserGrade(userGrade);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.USERGRADE_LIST_QUERY);
	}

	/**
	 * 删除用户等级管理
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除用户等级")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		UserGrade userGrade = userGradeService.getUserGrade(id);

		userGradeService.deleteUserGrade(userGrade);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.USERGRADE_LIST_QUERY);
	}

	/**
	 * 查看用户等级详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		UserGrade userGrade = userGradeService.getUserGrade(id);
		request.setAttribute("userGrade", userGrade);
		return PathResolver.getPath(AdminPage.USERGRADE_EDIT_PAGE);
	}

	/**
	 * 跳转用户等级编辑页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.USERGRADE_EDIT_PAGE);
	}

	/**
	 * 更新用户等级
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="更新用户等级")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		UserGrade userGrade = userGradeService.getUserGrade(id);
		request.setAttribute("userGrade", userGrade);
		return PathResolver.getPath(BackPage.USERGRADE_EDIT_PAGE);
	}
}
