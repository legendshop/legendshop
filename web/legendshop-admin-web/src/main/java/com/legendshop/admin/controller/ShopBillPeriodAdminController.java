/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopBillPeriod;
import com.legendshop.spi.service.ShopBillPeriodService;

/**
 * 结算档期控制器
 *
 */
@Controller
@RequestMapping("/admin/shopBillPeriod")
public class ShopBillPeriodAdminController extends BaseController {
	
	@Autowired
	private ShopBillPeriodService shopBillPeriodService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查询结算档期列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopBillPeriod
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ShopBillPeriod shopBillPeriod) {
		PageSupport<ShopBillPeriod> ps = shopBillPeriodService.getShopBillPeriod(curPageNO, shopBillPeriod);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopBillPeriod", shopBillPeriod);
		
		//获取商家结算周期类型
		String shopBillPeriodType = systemParameterUtil.getShopBillPeriodType();
		request.setAttribute("shopBillPeriodType", shopBillPeriodType);
		
		//获取商家结算日（每月结算）
		Integer shopBillDay = systemParameterUtil.getShopBillDay();
		request.setAttribute("shopBillDay", shopBillDay);
		return PathResolver.getPath(AdminPage.SHOPBILLPERIOD_LIST_PAGE);
	}

}
