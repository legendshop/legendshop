/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.spi.service.ProductPropertyValueService;

/**
 * 规格管理控制器
 *
 */
@Controller
@RequestMapping("/admin/productPropertyValue")
public class ProductPropertyValueController extends BaseController{
	
	@Autowired
	private ProductPropertyValueService productPropertyValueService;
	
	/**
	 * 查看规格属性
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProductPropertyValue productPropertyValue) {
		PageSupport<ProductPropertyValue> ps = productPropertyValueService.getProductPropertyValuePage(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("productPropertyValue", productPropertyValue);

		return "/productPropertyValue/productPropertyValueList";
	}
	
	/**
	 * 保存规格
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response,
			ProductPropertyValue productPropertyValue) {
		productPropertyValueService.saveProductPropertyValue(productPropertyValue);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/productPropertyValue/query.htm";
	}
	
	/**
	 * 删除规格
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductPropertyValue productPropertyValue = productPropertyValueService.getProductPropertyValue(id);
		productPropertyValueService.deleteProductPropertyValue(productPropertyValue);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(FowardPage.PRODUCTPROPERTY_LIST_QUERY);
	}
	
	/**
	 * 查看规格详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductPropertyValue productPropertyValue = productPropertyValueService.getProductPropertyValue(id);
		request.setAttribute("productPropertyValue", productPropertyValue);
		return PathResolver.getPath(BackPage.PRODUCTPROPERTY_VALUE_PAGE);
	}
	
	/**
	 * 跳转新建页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/productPropertyValue/productPropertyValue";
	}
	
	/**
	 * 更新规格
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProductPropertyValue productPropertyValue = productPropertyValueService.getProductPropertyValue(id);
		request.setAttribute("productPropertyValue", productPropertyValue);
		return "forward:/admin/productPropertyValue/query.htm";
	}
	
	/**
	 * 更新状态
	 * @param request
	 * @param response
	 * @param valueId
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{valueId}/{status}", method = RequestMethod.GET)
	public @ResponseBody Short updateStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long valueId, @PathVariable Short status) {
		ProductPropertyValue productPropertyValue = productPropertyValueService.getProductPropertyValue(valueId);
		if (productPropertyValue == null) {
			return -1;
		}
		if (!status.equals(productPropertyValue.getStatus())) {
			productPropertyValue.setStatus(status);
			productPropertyValueService.updateProductPropertyValue(productPropertyValue);
		}
		return productPropertyValue.getStatus();
	}
}
