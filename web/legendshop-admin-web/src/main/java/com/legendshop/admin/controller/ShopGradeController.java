/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopGrade;
import com.legendshop.spi.service.ShopGradeService;

/**
 * 商家等级管理控制器.
 */
@Controller
@RequestMapping("/admin/system/shopGrade")
public class ShopGradeController extends BaseController {

	@Autowired
	private ShopGradeService shopGradeService;

	/**
	 * 查询商家等级列表.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopGrade
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ShopGrade shopGrade) {
		PageSupport<ShopGrade> ps = shopGradeService.getShopGradePage(curPageNO, shopGrade);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopGrade", shopGrade);
		return PathResolver.getPath(AdminPage.SHOPGRADE_LIST_PAGE);
	}

	/**
	 * 保存商家等级.
	 *
	 * @param request
	 * @param response
	 * @param shopGrade
	 * @return the string
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ShopGrade shopGrade) {
		shopGradeService.saveShopGrade(shopGrade);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.SHOPGRADE_LIST_QUERY);
	}

	/**
	 * 删除商家等级.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		ShopGrade shopGrade = shopGradeService.getShopGrade(id);
		shopGradeService.deleteShopGrade(shopGrade);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.SHOPGRADE_LIST_QUERY);
	}

	/**
	 * 查看商家等级详情.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		ShopGrade shopGrade = shopGradeService.getShopGrade(id);
		request.setAttribute("shopGrade", shopGrade);
		return PathResolver.getPath(AdminPage.SHOPGRADE_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.SHOPGRADE_EDIT_PAGE);
	}

	/**
	 * 更新商家等级.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer id) {
		ShopGrade shopGrade = shopGradeService.getShopGrade(id);
		request.setAttribute("shopGrade", shopGrade);
		return PathResolver.getPath(BackPage.SHOPGRADE_EDIT_PAGE);
	}
}
