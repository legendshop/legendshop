/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SecDomainNameSetting;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopDomain;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.ShopDomainService;
import com.legendshop.util.AppUtils;

/**
 * @描述 二级域名
 * @author 关开发
 */
@Controller
@RequestMapping("/admin/secDomainName")
public class SecDomainNameAdminController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SecDomainNameAdminController.class);

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ShopDomainService shopDomainService;

	@Autowired
	private ConstTableService constTableService;

	/**
	 * 查询二级域名列表
	 * 
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail,
			String curPageNO) {

		PageSupport<ShopDetail> ps = shopDetailService.getShopDetailPage(curPageNO, shopDetail);
		PageSupportHelper.savePage(request, ps);

		/*
		 * SimpleSqlQuery query = new
		 * SimpleSqlQuery(ShopDomain.class,20,curPageNO); QueryMap map = new
		 * QueryMap();
		 * 
		 * if(AppUtils.isNotBlank(shopDomain.getShopName())){
		 * map.put("shopName", "%"+shopDomain.getShopName()+"%"); }
		 * if(AppUtils.isNotBlank(shopDomain.getSecDomainName())){
		 * map.put("secDomainName", "%"+shopDomain.getSecDomainName()+"%"); }
		 * map.put("status", shopDomain.getStatus());
		 * 
		 * String querySQL = ConfigCode.getInstance().getCode(
		 * "secDomainName.getSecDomainNameList", map); String queryAllSQL =
		 * ConfigCode.getInstance().getCode(
		 * "secDomainName.getSecDomainNameListCount", map);
		 * query.setAllCountString(queryAllSQL); query.setQueryString(querySQL);
		 * 
		 * query.setParam(map.toArray());
		 * 
		 * PageSupport ps = shopDetailService.getShopDetail(query);
		 * 
		 * ps.savePage(request);
		 */

		return PathResolver.getPath(AdminPage.SEC_DOMAIN_NAME_LIST);
	}

	/**
	 * 二级域名全局配置页面跳转
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/setting")
	public String setting(HttpServletRequest request, HttpServletResponse response) {

		ConstTable constTable = constTableService.getConstTablesBykey("SHOP_SEC_DOMAIN_NAME", "SHOP_SEC_DOMAIN_NAME");

		SecDomainNameSetting secDomainNameSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			secDomainNameSetting = JSONObject.parseObject(constTable.getValue(), SecDomainNameSetting.class);
		}

		request.setAttribute("secDomainNameSetting", secDomainNameSetting);

		return PathResolver.getPath(AdminPage.SEC_DOMAIN_NAME_SETTING);
	}

	/**
	 * 修改二级域名全局配置
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/settingUpdate")
	public String settingUpdate(HttpServletRequest request, HttpServletResponse response,
			SecDomainNameSetting secDomainNameSetting) {

		if (AppUtils.isNotBlank(secDomainNameSetting)) {
			if (AppUtils.isNotBlank(secDomainNameSetting.getIsEnable())
					&& AppUtils.isNotBlank(secDomainNameSetting.getRetain())
					&& AppUtils.isNotBlank(secDomainNameSetting.getSizeRange())) {

				secDomainNameSetting.setRetain(secDomainNameSetting.getRetain().toLowerCase());
				String jsonStr = JSONObject.toJSONString(secDomainNameSetting);

				constTableService.updateConstTableByType("SHOP_SEC_DOMAIN_NAME", "SHOP_SEC_DOMAIN_NAME", jsonStr);

				request.setAttribute("secDomainNameSetting", secDomainNameSetting);
			}
		}

		return PathResolver.getPath(AdminPage.SEC_DOMAIN_NAME_SETTING);
	}

	/**
	 * 审核二级域名
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/audit")
	@ResponseBody
	public String audit(HttpServletRequest request, HttpServletResponse response, ShopDomain shopDomain) {

		JSONObject message = new JSONObject();

		if (AppUtils.isNotBlank(shopDomain)) {

			Long id = shopDomain.getId();
			String auditOpinion = shopDomain.getAuditOpinion();
			Long status = shopDomain.getStatus();

			ShopDomain existShopDomain = shopDomainService.getShopDomain(id);

			if (AppUtils.isNotBlank(existShopDomain)) {
				existShopDomain.setAuditOpinion(auditOpinion);
				existShopDomain.setStatus(status);
				existShopDomain.setAuditTime(new Date());

				shopDomainService.updateShopDomain(existShopDomain);

				message.put("status", "success");
				message.put("msg", "恭喜您,操作成功!");
			} else {
				message.put("status", "fail");
				message.put("msg", "操作异常,请联系管理员!");
			}
		} else {
			message.put("status", "fail");
			message.put("msg", "操作异常,请联系管理员!");
		}

		return message.toJSONString();
	}

	/**
	 * 删除二级域名
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/delete/{shopId}")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId) {

		try {
			if (AppUtils.isNotBlank(shopId)) {
				ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

				shopDetail.setSecDomainName(null);

				shopDetailService.update(shopDetail);

				return Constants.SUCCESS;
			} else {
				return Constants.FAIL;
			}

		} catch (Exception e) {
			log.error("未知错误", e);
			return Constants.FAIL;
		}
	}
}
