/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.BackPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ProdUserParam;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductService;

/**
 * 用户参数控制器
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/userParam")
public class UserParamController extends BaseController {

	@Autowired
	private ProductService productService;

	/**
	 * 查询 用户自定义 参数
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param dynamicTemp
	 * @param sortId
	 * @return
	 */
	@RequestMapping("/overlay")
	public String paramOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ProdUserParam prodUserParam) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ProdUserParam> ps = productService.getProdUserParamPage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(BackPage.USERPARAM_OVERLAY);
	}

	/**
	 * 删除 用户自定义 参数
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	public @ResponseBody String deleteParam(HttpServletRequest request, HttpServletResponse response, Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		productService.deleteProdUserParam(id, user.getShopId());
		return Constants.SUCCESS;
	}

	/**
	 * 使用此参数
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param id
	 * @return
	 */
	@RequestMapping("/use/{id}")
	public @ResponseBody String useParam(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			@PathVariable Long id) {
		ProdUserParam prodUserParam = productService.getProdUserParam(id);
		return prodUserParam.getContent();
	}

	/**
	 * 去往保存参数弹框的div
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param dynamicTemp
	 * @param sortId
	 * @return
	 */
	@RequestMapping("/toSave")
	public String toSave(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(BackPage.SAVE_USERPARAM);
	}

	/**
	 * 保存用户自定义参数模板
	 * 
	 * @param request
	 * @param response
	 * @param content
	 * @param propSpecId
	 * @return
	 */
	@RequestMapping("/save")
	public @ResponseBody String saveParam(HttpServletRequest request, HttpServletResponse response, String content,
			String name) {
		SecurityUserDetail user = UserManager.getUser(request);
		ProdUserParam prodUserParam = new ProdUserParam();
		prodUserParam.setContent(content);
		prodUserParam.setName(name);
		prodUserParam.setRecDate(new Date());
		prodUserParam.setUserId(user.getUserId());
		prodUserParam.setShopId(user.getShopId());
		productService.saveProdUserParam(prodUserParam);
		return Constants.SUCCESS;
	}
}
