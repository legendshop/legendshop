/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MessageService;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 系统通知
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/system/Message")
public class MessageController extends BaseController{

	@Autowired
    private UserGradeService userGradeService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 批量删除发送系统通知
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@SystemControllerLog(description="批量删除发送系统通知")
    @RequestMapping(value = "/batchDelete/{msgIds}",method=RequestMethod.DELETE)
    public @ResponseBody String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String msgIds) {
    	try{
    		messageService.batchDelete(msgIds);
    		return Constants.SUCCESS;
    	}catch(Exception e){
    		e.printStackTrace();
    		return "批量删除系统通知成功";
    	}
    }
	
	
	/**
	 * 查询系统通知
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param title
	 * @return
	 */
    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, String title) {
    	Integer pageSize = null;
		if (!CommonServiceWebUtil.isDataForExport(request)) {// 非导出情况
			pageSize = systemParameterUtil.getPageSize();
 		}
		 PageSupport<SiteInformation> ps = messageService.querySiteInformation(curPageNO,title,pageSize);
		 PageSupportHelper.savePage(request, ps);
		 request.setAttribute("title", title);
	        return PathResolver.getPath(AdminPage.SYSTEM_MESSAGES_LIST_PAGE);
    }
	
    /**
     * 加载发送系统通知编辑
     * @param request
     * @param response
     * @return
     */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
    	List<StatusKeyValueEntity> userGradeList = retrieveUserGrade();
		String userGrade = JSONUtil.getJson(userGradeList);
		request.setAttribute("userGrade", userGrade);
		return PathResolver.getPath(AdminPage.SYSTEM_MESSAGES_EDIT_PAGE);
	}
	  /**
     * 加载发送系统通知编辑
     * @param request
     * @param response
     * @return
     */
	@RequestMapping(value = "/load/{msgId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long msgId) {
		SiteInformation siteInformation=messageService.getSystemMessages(msgId);
		String levelStr=siteInformation.getUserLevel();
    	List<StatusKeyValueEntity> userGradeList = retrieveUserGrade();
    	if(AppUtils.isNotBlank(levelStr)){
    		String[] userLevels=levelStr.split(",");
    		for(String userLevel:userLevels){
    			for(StatusKeyValueEntity statusKeyValueEntity:userGradeList){
    				String value=statusKeyValueEntity.getValue();
    				userLevel=userLevel.trim();
    				if(userLevel.equals(value)){
    					statusKeyValueEntity.setStatus("Y");//Y：选中，N：未勾选
    				}
    			}
    		}
    	}
    	
		String userGrade = JSONUtil.getJson(userGradeList);
		request.setAttribute("userGrade", userGrade);
		request.setAttribute("siteInformation", siteInformation);
		return PathResolver.getPath(AdminPage.SYSTEM_MESSAGES_EDIT_PAGE);
	}
	
	/**
	 * 删除发送系统通知
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@SystemControllerLog(description="删除发送系统通知")
    @RequestMapping(value = "/delete/{msgId}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long msgId) {
    	messageService.deleteSystemMsgStatus(msgId);
    	saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(RedirectPage.SYSTEM_MESSAGES_QUERY);
    }
	
    /**
     * 保存发送系统通知
     * @param request
     * @param response
     * @param siteInformation
     * @param userGradeArray
     * @return
     */
	@SystemControllerLog(description="保存发送系统通知")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, SiteInformation siteInformation, Integer[] userGradeArray) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
    	siteInformation.setSendName("平台消息");
    	messageService.saveSystemMessages(siteInformation,userGradeArray);
    	saveMessage(request, ResourceBundleHelper.getSucessfulString());
    	return PathResolver.getPath(RedirectPage.SYSTEM_MESSAGES_QUERY);
    }
    
    /**
     * 查找所有用户等级
     * @return
     */
    private List<StatusKeyValueEntity> retrieveUserGrade(){
    	return userGradeService.retrieveUserGrade();
    }
    
}
