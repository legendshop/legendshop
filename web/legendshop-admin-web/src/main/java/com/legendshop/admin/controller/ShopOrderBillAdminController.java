/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.ShopOrderBillTypeEnum;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopOrderBillStatusEnum;
import com.legendshop.model.dto.SubDto;
import com.legendshop.model.dto.SubRefundRetuenDto;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 卖家中心 商家结算
 *
 */
@Controller
@RequestMapping("/admin/shopBill")
public class ShopOrderBillAdminController extends BaseController {

	@Autowired
	private ShopOrderBillService shopOrderBillService;

	@Autowired
	private SubService subService;

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;
	
	/**
	 * 查询商家结算页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopOrderBill
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopOrderBill shopOrderBill) {
		
		//获取商家结算周期类型
		String shopBillPeriodType = systemParameterUtil.getShopBillPeriodType();
		request.setAttribute("shopBillPeriodType", shopBillPeriodType);
		
		//获取商家结算日（每月结算）
		Integer shopBillDay = systemParameterUtil.getShopBillDay();
		request.setAttribute("shopBillDay", shopBillDay);
		
		request.setAttribute("shopOrderBill", shopOrderBill);
		return PathResolver.getPath(AdminPage.SHOPORDERBILL_LIST);
	}
	
	/**
	 * 查询商家结算列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopOrderBill
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			ShopOrderBill shopOrderBill) {
		PageSupport<ShopOrderBill> ps = shopOrderBillService.getShopOrderBillPage(curPageNO, shopOrderBill);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopOrderBill", shopOrderBill);
		
		int shopBillDay = systemParameterUtil.getShopBillDay();
		request.setAttribute("shopBillDay", shopBillDay);
		adminMenuUtil.parseMenu(request, 5); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(AdminPage.SHOPORDERBILL_CONTENT_LIST);
	}
	
	/**
	 * 查看商家结算详情
	 * @param request
	 * @param response
	 * @param id
	 * @param type
	 * @param subNumber
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, Long type,
			String subNumber, String curPageNO) {
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		if (shopOrderBill != null) {
			// 结算订单列表
			if (type == null || ShopOrderBillTypeEnum.ORDER.value().equals(type)) {
				PageSupport<Sub> ps = subService.getSubListPage(curPageNO, shopOrderBill.getShopId(), subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

				// 结算退款单列表
			} else if(ShopOrderBillTypeEnum.RETURN_ORDER.value().equals(type)) {
				PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnPage(curPageNO, shopOrderBill.getShopId(), subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

				// 结算分销订单列表
			}else if(ShopOrderBillTypeEnum.DIST_ORDER.value().equals(type)) {

				PageSupport<SubItem> ps = subItemService.queryDistSubItemList(curPageNO, shopOrderBill.getShopId(), subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

				// 结算预售订单列表
			}else if(ShopOrderBillTypeEnum.PRESELL_ORDER.value().equals(type)){

				PageSupport<PresellSub> ps = presellSubService.queryPresellSubList(curPageNO,shopOrderBill.getShopId(),subNumber,shopOrderBill);
				PageSupportHelper.savePage(request, ps);

				// 结算拍卖保证金记录
			}else if (ShopOrderBillTypeEnum.AUCTION_ORDER.value().equals(type)){

				PageSupport<AuctionDepositRec> ps = auctionDepositRecService.queryAuctionDepositRecList(curPageNO,shopOrderBill.getShopId(),shopOrderBill);
				PageSupportHelper.savePage(request, ps);
			}
			request.setAttribute("shopOrderBill", shopOrderBill);
			request.setAttribute("subNumber", subNumber);
			request.setAttribute("type", type);
			return PathResolver.getPath(AdminPage.SHOPORDERBILL_DETAIL);
		} else {
			return null;
		}
	}

	/**
	 * 平台审核
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/confirm/{id}")
	public @ResponseBody String confirm(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long id) {
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		if (shopOrderBill.getStatus().equals(ShopOrderBillStatusEnum.SHOP_CONFIRM.value())) {
			shopOrderBill.setStatus(ShopOrderBillStatusEnum.ADMIN_CONFIRM.value());
			shopOrderBillService.updateShopOrderBill(shopOrderBill);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 付款完成
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/finishPay/{id}")
	public @ResponseBody String finishPay(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long id, Date payDate, String payContent) {
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		if (payDate == null || AppUtils.isBlank(payContent)) {
			return Constants.FAIL;
		}
		if (shopOrderBill.getStatus().equals(ShopOrderBillStatusEnum.ADMIN_CONFIRM.value())) {
			shopOrderBill.setPayDate(payDate);
			shopOrderBill.setPayContent(payContent);
			shopOrderBill.setStatus(ShopOrderBillStatusEnum.FINISH.value());
			shopOrderBillService.updateShopOrderBill(shopOrderBill);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}
	
	/**
	 * 导出数据
	 * @param request
	 * @param response
	 * @param data
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/export", method = RequestMethod.POST)
	@ResponseBody
	public String export(HttpServletRequest request, HttpServletResponse response, String data) throws IOException, ParseException {
		List<String> params = JSONUtil.getArray(data, String.class);
		String type = params.get(0);
		String id = params.get(1);
		String subNumber = params.get(2);
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(Long.valueOf(id));
		if(type.equals("1")){  //订单列表
			List<SubDto> dataset = subService.exportSubOrder(shopOrderBill, subNumber);
			ExportExcelUtil<SubDto> ex = new ExportExcelUtil<SubDto>();
			String[] headers = { "订单编号", "下单时间", "成交时间", "订单金额", "运费", "红包金额"};
			response.setHeader("Content-Disposition", "attachment;filename=orderList.xls");
			ex.exportExcelUtil(headers, dataset, response.getOutputStream());
			return "";
		}else {
			List<SubRefundRetuenDto> dataset = subRefundReturnService.exportOrder(shopOrderBill, subNumber);
			ExportExcelUtil<SubRefundRetuenDto> ex = new ExportExcelUtil<SubRefundRetuenDto>();
			String[] headers = { "订单编号", "类型", "退款金额", "退单时间（申请）","退单红包金额", "完成时间"};
			response.setHeader("Content-Disposition", "attachment;filename=orderList.xls");
			ex.exportExcelUtil(headers, dataset, response.getOutputStream());
			return "";
		}
	}

}
