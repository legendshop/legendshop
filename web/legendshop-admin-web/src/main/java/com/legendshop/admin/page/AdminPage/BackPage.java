/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.AdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum BackPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 错误页面. */
	BACK_ERROR_PAGE(ERROR_PAGE_PATH),

	/** The img list page. */
	IMG_LIST_PAGE("/imgFile/imgFileList"),

	/** 创建商品 */
	IMG_EDIT_PAGE("/prod/prod"),

	/** 导航 */
	ADMIN_TOP("/frame/top"),

	/** 图片管理编辑 */
	IJPG_EDIT_PAGE("/indexjpg/indexjpg"),

	/** 用户登录历史列表 */
	LOGIN_HIST_LIST_PAGE("/loginhistory/loginHistoryList"),

	NEWSCAT_CREAT_PAGE("/news/creatNews"),

	PROD_EDIT_NEW_PAGE("/prod/prodNew"),

	/** 规格管理 */
	PROD_EDIT_PROPERTY_PAGE("/prod/productProperty"),

	/** 规格详情 */
	PROD_EDIT_DETAILS_PAGE("/prod/productDetails"),

	APPEND_PRODUCT("/prod/appendProduct"),
	
	GROUP_PRODUCT("/prod/groupProduct"),

	CREATEP_RODUCT_STEP("/prod/createProductStep"),

	/** 商品类型 */
	CATEGORY_PROD_TYPE_PAGE("/category/prodType"),

	/** 修改价格 */
	MODIFYPRICE("/order/modifyPrice"),

	/** 尚未回复的商品评论列表. */
	UNREPLY_PROD_CONSULT_LIST_PAGE("/prodConsult/unReplyProductConsultList"),

	/** 商品参数 */
	PROPERTYCONTENT_PAGE("/productProperty/propertyContent"),

	/** 规格管理 */
	PRODUCTPROPERTY_EDIT_PAGE("/productProperty/productProperty"),

	/** 规格编辑 */
	PRODUCTPROPERTY_VALUE_PAGE("/productPropertyValue/productPropertyValue"),

	// 后台菜单
	/** 菜单 */
	MENU("/frame/menu"),

	/** 导航管理 */
	NAVIGATION_LIST_PAGE("/navigation/navigationList"),

	/** 地区管理 */
	DISTRICT_LIST_PAGE("/district/districtList"),

	/** 省份编辑 */
	PROVINCE_CONTENT_LIST_PAGE("/district/provinceContent"),

	/** 城市编辑 */
	CITY_CONTENT_LIST_PAGE("/district/cityContent"),

	/** 地区编辑 */
	AREA_CONTENT_LIST_PAGE("/district/areaContent"),

	/** 用户等级编辑 */
	USERGRADE_EDIT_PAGE("/userGrade/userGrade"),

	/** 商家等级编辑 */
	SHOPGRADE_EDIT_PAGE("/shopGrade/shopGrade"),

	/** 楼层编辑 */
	FLOOR_EDIT_PAGE("/floor/floor"),

	/** 子楼层管理 */
	SUBFLOOR_LIST_PAGE("/subFloor/subFloorList"),

	/** 编辑分类 */
	SORT_FLOOR_OVERLAY("/floorItem/sortFloorOverlay"),

	/** 编辑产品 */
	PRODUCT_FLOOR_OVERLAY("/subFloorItem/productFloorOverlay"),

	/** 加载第二第三级楼层 */
	PRODUCT_FLOOR_LOAD("/subFloorItem/productFloorLoad"),

	/** 商品楼层 */
	PRODUCT_FLOOR_LIMIT("/subFloorItem/productFloorLimit"),

	/** 加载楼层 */
	LOAD_FLOOR_NSORT("/floorItem/loadFloorNsort"),

	/** 楼层编辑界面 */
	FLOOR_WIDE_BLEND("/floor/layout/floorWideBlend"),

	/** 4x矩形广告楼层编辑界面 */
	WIDE_ADV_RECTANGLE_FOUR("/floor/layout/floorAdvRectangleFour"),

	/** 4x方形广告楼层编辑界面 */
	WIDE_ADV_SQUARE_FOUR("/floor/layout/floorAdvSquareFour"),

	/** 楼层编辑界面 */
	WIDE_ADV_EIGHT("/floor/layout/floorWideAdvEight"),

	/** 楼层编辑界面 */
	WIDE_ADV_BRAND("/floor/layout/floorWideAdvBrand"),

	/** 楼层编辑界面 */
	WIDE_ADV_ROW("/floor/layout/floorWideAdvRow"),

	/** 楼层编辑界面 */
	WIDE_GOODS("/floor/layout/floorWideGoods"),

	/** 楼层编辑界面 */
	WIDE_ADV_FIVE("/floor/layout/floorWideAdvFive"),

	/** 编辑团购 */
	GROUP_FLOOR_OVERLAY("/groupOverlay/groupFloorOverlay"),

	/** 编辑加载 */
	GROUP_FLOOR_LOAD("/groupOverlay/groupFloorLoad"),

	/** 编辑排行 */
	RANK_FLOOR_OVERLAY("/rankOverlay/rankFloorOverlay"),

	/** 编辑品牌 */
	BRAND_FLOOR_OVERLAY("/brandOverlay/brandFloorOverlay"),

	/** 品牌楼层加载 */
	BRAND_FLOOR_LOAD("/brandOverlay/brandFloorLoad"),

	/** 品牌内容 */
	BRAND_CONTENT("/floor/brandContent"),

	/** 团购内容 */
	GROUP_CONTENT("/floor/groupContent"),

	/** 特价促销 -> 文章编辑 */
	NEWS_CONTENT("/floor/newsContent"),

	/** 等级中心 */
	RANK_CONTENT("/floor/rankContent"),

	/** 广告覆盖 */
	ADV_FLOOR_OVERLAY("/floor/advOverlay"),

	/** 广告覆盖 */
	ADV_LIST_OVERLAY("/floor/advListOverlay"),

	/** 编辑文章 */
	NEWS_FLOOR_OVERLAY("/newsFloorOverlay/newsFloorOverlay"),

	/** 新楼层加载 */
	NEWS_FLOOR_LOAD("/newsFloorOverlay/newsFloorLoad"),

	/** 尚未处理的投诉. */
	UNHANDLE_ACCUSATION_LIST_PAGE("/accusation/unHandleaccusationList"),

	/** 商品回复 */
	PRODUCTREPLY_LIST_PAGE("/productReply/ProductReplyList"),

	/** 创建商品回复 */
	PRODUCTREPLY_EDIT_PAGE("/productReply/productReply"),

	/** 类型管理弹框 */
	PRODTYPE_LIST_OVERLAY("/prodType/prodTypeOverlay"),

	/** 规格类型编辑 */
	PRODTYPE_EDIT_PAGE("/prodType/prodTypeEdit"),

	/** 规格类型属性列表 */
	PROPERTY_LIST_PAGE("/prodType/propertyList"),

	/** 类型管理 品牌列表 */
	BRAND_LIST("/prodType/brandList"),

	/** 类型管理 参数属性 */
	PARAMETER_PROPERTY_PAGE("/prodType/parameterProperty"),

	/** 类型管理 参数 编辑页面 */
	PARAM_PROP_EDIT_PAGE("/prodType/paramPropEdit"),

	/** 参数分组 搜索下拉框 */
	TYPE_PARAM_GROUP_EDIT_PAGE("/prodType/paramGroup"),

	/** 商品列表 品牌 搜索下拉框 */
	PROD_BRAND_EDIT_PAGE("/prod/loadBrands"),

	/** 购物清单打印 */
	PRINT_ORDER_PAGE("/order/printOrder"),

	/** 保存物流信息 */
	DELIVER_GOODS("/order/deliverGoods"),

	/** 修改物流信息 */
	CHANGE_DELIVER_NUM("/order/changeDeliverNum"),

	/** 后台登录界面 **/
	ADMIN_LOGIN("/login/login"),

	/** 设置运单模版 */
	PRINTTMPL_DESIGN("/printTmpl/printTmpl_design"),

	/** 打印物流信息 */
	PRINT_DELIVERY("/order/print_delivery"),

	/** 销售属性模板 */
	USERATTRIBUTE_OVERLAY("/userAttribute/userAttrOverlay"),

	/** 保存参属性模板 */
	SAVE_ATTRIBUTE("/userAttribute/saveUserAttribute"),

	/** 参数模板 */
	USERPARAM_OVERLAY("/userParam/userParamOverlay"),

	/** 保存参数模板 */
	SAVE_USERPARAM("/userParam/saveUserParam"),

	/** 售后弹框页面 */
	AFTERSALE_OVERLAY("/afterSale/afterSaleOverlay"),

	/** 创建公用的产品类目 */
	CATEGORY_EDIT_PAGE("/category/category"),

	/** 加载商品类目 */
	CATEGORY_LOADCATEGORY_PAGE("/category/loadCategory"),

	/** 关联分类 */
	CATEGORY_LOADCATEGORY_CHECKBOX_PAGE("/category/loadCategoryCheckbox"),

	/** 加载所有分类 */
	PRODLIST_LOADCATEGORY_PAGE("/prod/loadAllCategory"),

	/** 专题内容 */
	THEME_CONTENT("/theme/themeContent"),

	/** 专题详情 */
	THEME_DETAIL("/theme/themeDetail"),

	/** 专题精选 */
	THEME_DETAIL2("/theme/themeDetail2"),

	/** 更新管理员密码 **/
	UPDATE_ADMIN_PWD("/adminUser/updatePwd"),

	/** 加载商品页面 */
	LOAD_PROP_LIST_PAGE("/productProperty/loadPropPage"),

	/** APP 装修 **/
	APPDECORATE_LOADCATEGORY_PAGE("/appdecorate/loadAllCategory"),

	/** 商品加载 */
	APPDECORATE_LOADPRODS_PAGE("/appdecorate/loadProds"),

	/** 主题加载 */
	APPDECORATE_LOADTHEMES_PAGE("/appdecorate/loadThemes"),

	/** 品牌加载 */
	APPDECORATE_LOADBRANDS_PAGE("/appdecorate/loadBrands"),

	/** 图片加载 */
	APPDECORATE_LOADIMAGES_PAGE("/appdecorate/loadImages"),

	/** APP版本上传页面 */
	APP_UPLOAD_PAGE("/appVersion/uploadPage"),
	
	/** 支付宝编辑页面  */
	ALIPAY_PAYTYPE_EDIT("/payType/AliPayPayTypeEdit"),
	
	/** 微信APP编辑配置  */
	WX_APP_PAYTYPE_EDIT("/payType/WxAppPayTypeEdit"),
	
	/** 微信小程序编辑配置  */
	WX_MP_PAYTYPE_EDIT("/payType/WxMpPayTypeEdit"),
	
	/** 微信支付编辑配置  */
	WX_PAYTYPE_EDIT("/payType/WxPayTypeEdit"),
	
	/** 通联支付编辑配置  */
	TONGLIAN_PAYTYPE_EDIT("/payType/TLPayTypeEdit"),
	
	
	/** 腾讯支付编辑配置  */
	TENPAY_PAYTYPE_EDIT("/payType/TDPPayTypeEdit"),
	
	/** 模拟支付编辑配置  */
	SIMULATE_PAYTYPE_EDIT("/payType/SimulatePayTypeEdit"),
	
	/** 银联支付编辑配置  */
	UNION_PAYTYPE_EDIT("/payType/UnionPayTypeEdit"),
	
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value
	 *            the value
	 */
	private BackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
