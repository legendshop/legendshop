/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinRedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;
import com.legendshop.spi.service.WeiXinKeywordResponseService;

/**
 * The Class WeiXinKeywordResponseController
 *
 */
@Controller
@RequestMapping("/admin/weiXinKeywordResponse")
public class WeiXinKeywordResponseController extends BaseController{
    @Autowired
    private WeiXinKeywordResponseService weiXinKeywordResponseService;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeiXinKeywordResponse weiXinKeywordResponse) {
        PageSupport<WeiXinKeywordResponse> ps = weiXinKeywordResponseService.getWeiXinnKeywordResponse(curPageNO);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("weiXinKeywordResponse", weiXinKeywordResponse);
        return PathResolver.getPath(WeixinAdminPage.WEIXINKEYWORDRESPONSE_LIST_PAGE);
    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, WeiXinKeywordResponse weiXinKeywordResponse) {
        weiXinKeywordResponseService.saveWeiXinKeywordResponse(weiXinKeywordResponse);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(WeixinRedirectPage.WEIXINKEYWORDRESPONSE_LIST_QUERY);
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeiXinKeywordResponse weiXinKeywordResponse = weiXinKeywordResponseService.getWeiXinKeywordResponse(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weiXinKeywordResponse.getUserName());
		//if(result!=null){
			//return result;
		//}
		weiXinKeywordResponseService.deleteWeiXinKeywordResponse(weiXinKeywordResponse);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(WeixinRedirectPage.WEIXINKEYWORDRESPONSE_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeiXinKeywordResponse weiXinKeywordResponse = weiXinKeywordResponseService.getWeiXinKeywordResponse(id);
        //String result = checkPrivilege(request, UserManager.getUsername(request.getSession()), weiXinKeywordResponse.getUserName());
		//if(result!=null){
			//return result;
		//}
        request.setAttribute("weiXinKeywordResponse", weiXinKeywordResponse);
        return PathResolver.getPath(WeixinAdminPage.WEIXINKEYWORDRESPONSE_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		  return PathResolver.getPath(WeixinAdminPage.WEIXINKEYWORDRESPONSE_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        WeiXinKeywordResponse weiXinKeywordResponse = weiXinKeywordResponseService.getWeiXinKeywordResponse(id);
		request.setAttribute("weiXinKeywordResponse", weiXinKeywordResponse);
		return PathResolver.getPath(WeixinRedirectPage.WEIXINKEYWORDRESPONSE_LIST_QUERY);
    }

}
