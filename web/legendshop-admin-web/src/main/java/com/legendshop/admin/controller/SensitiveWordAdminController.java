/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SensitiveWord;
import com.legendshop.spi.service.SensitiveWordService;

/**
 * 敏感字管理控制器
 *
 */
@Controller
@RequestMapping("/admin/system/sensitiveWord")
public class SensitiveWordAdminController extends BaseController{

	@Autowired
	private SensitiveWordService sensitiveWordService;

	/**
	 * 查询敏感字列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, SensitiveWord sensitiveWord) {
		PageSupport<SensitiveWord> ps = sensitiveWordService.getSensitiveWord(curPageNO, sensitiveWord);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("sensitiveWord", sensitiveWord);

		return PathResolver.getPath(AdminPage.SENSITIVEWORD_LIST_PAGE);
	}

	/**
	 * 保存敏感字
	 */
	@SystemControllerLog(description="保存敏感字")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, SensitiveWord sensitiveWord) {
		sensitiveWord.setIsGlobal(1);
		sensitiveWordService.saveSensitiveWord(sensitiveWord);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.SENSITIVEWORD_LIST_QUERY);
	}

	/**
	 * 删除敏感字
	 */
	@SystemControllerLog(description="删除敏感字")
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SensitiveWord sensitiveWord = sensitiveWordService.getSensitiveWord(id);
		sensitiveWordService.deleteSensitiveWord(sensitiveWord);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(RedirectPage.SENSITIVEWORD_LIST_QUERY);
	}

	/**
	 * 查看敏感字详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SensitiveWord sensitiveWord = sensitiveWordService.getSensitiveWord(id);
		request.setAttribute("sensitiveWord", sensitiveWord);
		return PathResolver.getPath(AdminPage.SENSITIVEWORD_EDIT_PAGE);
	}

	/**
	 * 跳转新建页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.SENSITIVEWORD_EDIT_PAGE);
	}

	/**
	 * 更新敏感字
	 */
	@SystemControllerLog(description="更新敏感字")
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SensitiveWord sensitiveWord = sensitiveWordService.getSensitiveWord(id);
		request.setAttribute("sensitiveWord", sensitiveWord);
		return PathResolver.getPath(AdminPage.SENSITIVEWORD_EDIT_PAGE);
	}

}
