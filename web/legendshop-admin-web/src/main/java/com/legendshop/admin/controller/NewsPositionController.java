/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.NewsPosition;
import com.legendshop.spi.service.NewsPositionService;

/**
 * 文章中间表
 *
 */
@Controller
@RequestMapping("/admin/newsPosition")
public class NewsPositionController extends BaseController {
   
	@Autowired
    private NewsPositionService newsPositionService;
    
	/**
	 * 保存文章中间表
	 * @param request
	 * @param response
	 * @param catid
	 * @param newsid
	 * @param positionid
	 * @param seq
	 * @return
	 */
	@RequestMapping(value = "/save",method=RequestMethod.POST)
    @ResponseBody
	public String save(HttpServletRequest request, HttpServletResponse response,Long catid,Long newsid,int positionid,Long seq) {
	    	NewsPosition newsPosition = new NewsPosition();
	    	newsPosition.setNewsid(newsid);
	    	newsPosition.setPosition(positionid);
	    	newsPosition.setSeq(seq);
	    	newsPositionService.getNewsPositionList();
	    	newsPositionService.saveNewsPosition(newsPosition);
			return Constants.SUCCESS;
			
    }
    
	/**
	 * 删除文章中间表
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
    @RequestMapping(value = "/delete",method=RequestMethod.POST)
    @ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response,Long id) {
    	if(id!=0&&id!=null){
	    	newsPositionService.delete(id);
			return Constants.SUCCESS;
    	}else{
    		return Constants.FAIL;
    	}
	}
    	
    /**
     * 更新文章中间表
     * @param request
     * @param response
     * @param positionid
     * @param seq
     * @return
     */
    @RequestMapping(value = "/update",method=RequestMethod.POST)
    @ResponseBody
	public String update(HttpServletRequest request, HttpServletResponse response,Long positionid,Long seq) {
    	if(positionid!=0&&positionid!=null&&seq!=0&&seq!=null){
    		NewsPosition newsPosition= newsPositionService.getNewsPositionyById(positionid);
	    	newsPosition.setSeq(seq);
	    	newsPositionService.updateNewsPosition(newsPosition);
			return Constants.SUCCESS;
    	}
    	return Constants.FAIL;
	}
}
