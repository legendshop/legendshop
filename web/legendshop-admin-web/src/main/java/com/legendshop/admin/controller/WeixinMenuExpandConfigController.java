/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.admin.page.WeixinAdminPage.WeixinAdminPage;
import com.legendshop.admin.page.WeixinAdminPage.WeixinRedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.WeixinMenuExpandConfigService;
import com.legendshop.util.AppUtils;

/**
 *  微信菜单管理
 *
 */
@Controller
@RequestMapping("/admin/weixin/expandConfig")
public class WeixinMenuExpandConfigController extends BaseController{
    @Autowired
    private WeixinMenuExpandConfigService weixinMenuExpandConfigService;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, WeixinMenuExpandConfig weixinMenuExpandConfig) {
        PageSupport<WeixinMenuExpandConfig> ps = weixinMenuExpandConfigService.getWeixinMenuExpandConfig(curPageNO,weixinMenuExpandConfig);
        PageSupportHelper.savePage(request,ps);
        request.setAttribute("config", weixinMenuExpandConfig);
        return PathResolver.getPath(WeixinAdminPage.WEIXIN_MENU_EXPANDCONFIG_LIST);
    }

    @RequestMapping(value = "/save")
    @ResponseBody
    public String save(HttpServletRequest request, HttpServletResponse response, WeixinMenuExpandConfig weixinMenuExpandConfig) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
    	
    	if(AppUtils.isBlank(weixinMenuExpandConfig.getClassService())){
    		return "服务service 不能为空";
    	}

    	weixinMenuExpandConfig.setCreateUserid(userId);
        weixinMenuExpandConfigService.saveWeixinMenuExpandConfig(weixinMenuExpandConfig);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return Constants.SUCCESS;
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinMenuExpandConfig weixinMenuExpandConfig = weixinMenuExpandConfigService.getWeixinMenuExpandConfig(id);
		weixinMenuExpandConfigService.deleteWeixinMenuExpandConfig(weixinMenuExpandConfig);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(WeixinRedirectPage.WEIXINMENUEXPANDCONFIG_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        WeixinMenuExpandConfig weixinMenuExpandConfig = weixinMenuExpandConfigService.getWeixinMenuExpandConfig(id);
        request.setAttribute("config", weixinMenuExpandConfig);
   	   return PathResolver.getPath(WeixinAdminPage.MENU_EXPAND_CONFIG_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(WeixinAdminPage.MENU_EXPAND_CONFIG_EDIT_PAGE);
	}


}
