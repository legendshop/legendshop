/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.AppStartAdv;
import com.legendshop.spi.service.AppStartAdvService;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * APP启动页管理
 */
@Controller
@RequestMapping("/admin/appStartAdv")
public class AppStartAdvAdminController extends BaseController {

	private static final int MAX_AMOUNT = 5;

	@Autowired
	private AppStartAdvService appStartAdvService;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 加载APP启动页广告
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param appStartAdv
	 * @return
	 */
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, AppStartAdv appStartAdv) {
		PageSupport<AppStartAdv> ps = appStartAdvService.getAppStartAdvPage(curPageNO, appStartAdv);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("search", appStartAdv);

		return PathResolver.getPath(AdminPage.APPSTART_ADV_LIST);
	}

	/**
	 * APP广告编辑
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.APPSTART_ADV_EDIT);
	}

	/**
	 * 根据id编辑广告
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editId(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AppStartAdv appStartAdv = appStartAdvService.getAppStartAdv(id);
		if (null == appStartAdv) {
			throw new BusinessException("对不起, 您要编辑的广告不存在或已被删除!");
		}
		request.setAttribute("bean", appStartAdv);
		return PathResolver.getPath(AdminPage.APPSTART_ADV_EDIT);
	}

	/**
	 * 添加APP启动广告
	 * 
	 * @param request
	 * @param response
	 * @param appStartAdv
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(HttpServletRequest request, HttpServletResponse response, AppStartAdv appStartAdv) {

		if (null != appStartAdv.getId()) {
			return "对不起,请不要非法操作!";
		}

		int total = appStartAdvService.getAllCount();
		if (total >= MAX_AMOUNT) {
			return "对不起, 最多只能创建 " + MAX_AMOUNT + "个APP启动广告!";
		}

		if (null == appStartAdv.getImgFile() || appStartAdv.getImgFile().getSize() <= 0) {
			return "对不起,广告图片不能为空!";
		}

		if (appStartAdvService.checkNameIsExits(appStartAdv.getName())) {
			return "对不起, 存在与 " + appStartAdv.getName() + " 同名的广告!";
		}

		Date curDate = new Date();

		appStartAdv.setCreateTime(curDate);

		// 处理图片上传
		String filePath = attachmentManager.upload(appStartAdv.getImgFile());
		appStartAdv.setImgUrl(filePath);

		// 处理广告图片上传
		appStartAdvService.saveAppStartAdv(appStartAdv);

		updateVersion();// 更新广告标记
		return Constants.SUCCESS;
	}

	/**
	 * 更新APP启动广告
	 * 
	 * @param request
	 * @param response
	 * @param appStartAdv
	 * @return
	 */
	@SystemControllerLog(description="更新APP启动广告")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request, HttpServletResponse response, AppStartAdv appStartAdv) {
		AppStartAdv originAppStartAdv = appStartAdvService.getAppStartAdv(appStartAdv.getId());

		if (null == originAppStartAdv) {
			return "对不起,您操作的广告不存在或已被删除!";
		}
		if (AppUtils.isBlank(originAppStartAdv.getImgUrl()) && (null == appStartAdv.getImgFile() || appStartAdv.getImgFile().getSize() <= 0)) {
			return "对不起,广告图片不能为空!";
		}

		if (!originAppStartAdv.getName().equals(appStartAdv.getName()) && appStartAdvService.checkNameIsExits(appStartAdv.getName())) {
			return "对不起, 存在与 " + appStartAdv.getName() + " 同名的广告!";
		}

		// 处理广告图片上传
		MultipartFile imgFile = appStartAdv.getImgFile();
		if (null != imgFile && imgFile.getSize() > 0) {
			String originImgUrl = originAppStartAdv.getImgUrl();
			String filePath = attachmentManager.upload(imgFile);
			originAppStartAdv.setImgUrl(filePath);

			// 删除之前的图片
			if (AppUtils.isNotBlank(originImgUrl)) {
				attachmentManager.deleteTruely(originImgUrl);
			}
		}

		originAppStartAdv.setName(appStartAdv.getName());
		originAppStartAdv.setUrl(appStartAdv.getUrl());
		originAppStartAdv.setDescription(appStartAdv.getDescription());

		appStartAdvService.updateAppStartAdv(originAppStartAdv);
		updateVersion();// 更新广告标记
		return Constants.SUCCESS;
	}

	/**
	 * APP启动页广告上线
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="APP启动页广告上线")
	@RequestMapping(value = "/online/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String online(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AppStartAdv appStartAdv = appStartAdvService.getAppStartAdv(id);

		if (null == appStartAdv) {
			return "对不起,您操作的广告不存在或已被删除!";
		}
		if (appStartAdv.getStatus().equals(Constants.ONLINE)) {
			return "对不起,您操作的广告已是上线状态!";
		}

		appStartAdvService.updateStatus(id, Constants.ONLINE);
		updateVersion();// 更新广告标记
		return Constants.SUCCESS;
	}

	/**
	 * APP启动页广告下线
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="APP启动页广告下线")
	@RequestMapping(value = "/offline/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String offline(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AppStartAdv appStartAdv = appStartAdvService.getAppStartAdv(id);

		if (null == appStartAdv) {
			return "对不起,您操作的广告不存在或已被删除!";
		}
		if (appStartAdv.getStatus().equals(Constants.OFFLINE)) {
			return "对不起,您操作的广告已是下线状态!";
		}

		appStartAdvService.updateStatus(id, Constants.OFFLINE);
		updateVersion();// 更新广告标记
		return Constants.SUCCESS;
	}

	/**
	 * 删除APP启动页广告
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除APP启动页广告")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AppStartAdv appStartAdv = appStartAdvService.getAppStartAdv(id);

		if (null == appStartAdv) {
			return "对不起,您操作的广告不存在或已被删除!";
		}
		if (appStartAdv.getStatus().equals(Constants.ONLINE)) {
			return "对不起,已上线的广告不能删除!";
		}

		if (AppUtils.isNotBlank(appStartAdv.getImgUrl())) {
			attachmentManager.deleteTruely(appStartAdv.getImgUrl());
		}

		appStartAdvService.deleteAppStartAdv(appStartAdv);
		updateVersion();// 更新广告标记
		return Constants.SUCCESS;
	}

	/**
	 * 检查是否重名
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/checkNameIsExits", method = RequestMethod.GET)
	@ResponseBody
	public Boolean checkNameIsExits(HttpServletRequest request, HttpServletResponse response, Long id, String name) {

		if (null != id) {
			String originName = appStartAdvService.getName(id);
			if (AppUtils.isNotBlank(originName) && originName.equals(name)) {
				return true;
			}
		}

		return !appStartAdvService.checkNameIsExits(name);
	}

	/**
	 * 更新广告版本
	 */
	private void updateVersion() {
		// 更新广告标记
		String version = String.valueOf(new Date().getTime());
		constTableService.updateConstTableByType("APP_START_ADV", "version", version);
	}
}
