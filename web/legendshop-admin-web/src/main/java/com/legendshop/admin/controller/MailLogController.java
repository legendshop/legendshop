/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailLog;
import com.legendshop.spi.service.MailLogService;

/**
 * 邮件发送历史
 *
 */
@Controller
@RequestMapping("/admin/system/mailLog")
public class MailLogController extends BaseController {

	@Autowired
	private MailLogService mailLogService;

	/**
	 * 查询邮件发送历史
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param mailLog
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, MailLog mailLog) {
		PageSupport<MailLog> ps = mailLogService.getMailLogPage(curPageNO, mailLog);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("mailLog", mailLog);

		// return PathResolver.getPath(request, response,
		// BackPage.MAILLOG_LIST_PAGE);
		return PathResolver.getPath(AdminPage.MAILLOG_LIST_PAGE);
	}
}
