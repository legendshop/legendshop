/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.admin.page.AdminPage.FowardPage;
import com.legendshop.admin.page.AdminPage.RedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.Headline;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.HeadlineAdminService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 头条管理
 * 
 * @author quanzc
 *
 */
@Controller
@RequestMapping("/admin/headlineAdmin")
public class HeandlineAdminController {

	@Autowired
	private HeadlineAdminService headlineAdminService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查找头条管理
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param headline
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Headline headline) {
		PageSupport<Headline> ps = this.headlineAdminService.getHeadlineAdmin(curPageNO, headline);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("headline", headline);
		return PathResolver.getPath(AdminPage.HEADLINE_LIST);
	}

	/**
	 * 保存头条管理
	 * 
	 * @param request
	 * @param response
	 * @param headline
	 * @param imageFile
	 * @return
	 */
	@SystemControllerLog(description="移动管理保存头条")
	@RequestMapping("/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Headline headline, MultipartFile imageFile) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(headline) && AppUtils.isNotBlank(user)) {
			String pic = this.attachmentManager.upload(imageFile);
			this.headlineAdminService.saveHeadlineAdmin(user.getUsername(), user.getUserId(), user.getShopId(), pic, imageFile,headline);
			return PathResolver.getPath(RedirectPage.HEADLINE_LIST);
		}
		request.setAttribute("msg", "添加失败");
		request.setAttribute("headline", headline);
		return PathResolver.getPath(FowardPage.HEADLINE_SAVE);
	}

	/**
	 * 删除头条管理
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="移动管理删除头条")
	@RequestMapping("/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		this.headlineAdminService.deleteHeadlineAdmin(id);
		return PathResolver.getPath(FowardPage.HEADLINE_LIST);
	}

	/**
	 * 跳转到编辑页
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Headline headline = this.headlineAdminService.getHeadlineAdmin(id);
		request.setAttribute("headline", headline);
		return PathResolver.getPath(AdminPage.HEADLINE_UPDATE);
	}

	/**
	 * 跳转到新建页
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(AdminPage.HEADLINE_SAVE);
	}

	/**
	 * 更新头条管理
	 * 
	 * @param request
	 * @param response
	 * @param headline
	 * @param imageFile
	 * @return
	 */
	@SystemControllerLog(description="移动管理更新头条")
	@RequestMapping("/update")
	public String update(HttpServletRequest request, HttpServletResponse response, Headline headline, MultipartFile imageFile) {//TODO 多事务
		Headline oldHeadline = this.headlineAdminService.getHeadlineAdmin(headline.getId());
		SecurityUserDetail user = UserManager.getUser(request);
		String filePath = null;
		if (imageFile != null && imageFile.getSize() > 0) {
			filePath = attachmentManager.upload(imageFile);
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), filePath, imageFile, AttachmentTypeEnum.NEWS);
			attachmentManager.deleteTruely(headline.getPic());
		}
		if (AppUtils.isNotBlank(headline)) {
			oldHeadline.setTitle(headline.getTitle());
			oldHeadline.setSeq(headline.getSeq());
			oldHeadline.setDetail(headline.getDetail());
			if (AppUtils.isNotBlank(filePath)) {
				oldHeadline.setPic(filePath);
			}
			this.headlineAdminService.updateHeadlineAdmin(oldHeadline);
			return PathResolver.getPath(RedirectPage.HEADLINE_LIST);
		}
		return PathResolver.getPath(FowardPage.HEADLINE_UPDATE) + headline.getId();
	}
}
