/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.page.WeixinAdminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum WeixinAdminPage.
 */
public enum WeixinAdminPage implements PageDefinition {
	
	
	WEIXINAUTORESPONSE_LIST_PAGE("/message/weixinAutoresponseList"),
	
	WEIXINAUTORESPONSE_EDIT_PAGE("/message/weixinAutoresponse"),
	
	WEIXINKEYWORDRESPONSE_LIST_PAGE("/message/weiXinKeywordResponseList"),
	
	WEIXINKEYWORDRESPONSE_EDIT_PAGE("/message/weiXinKeywordResponse"),
	
	WEIXIN_MENU_LIST("/menu/weixinMenuList"),
	
	MENU_EDIT_PAGE("/menu/weixinMenu"),
	
	WEIXIN_MENU_EXPANDCONFIG_LIST("/menu/weixinMenuExpandConfigList"),
	
	MENU_EXPAND_CONFIG_EDIT_PAGE("/menu/weixinMenuExpandConfig"),
	
	WEIXINNEWSTEMPLATE_LIST_PAGE("/newstemplate/weixinNewstemplateList"),
	
	WEIXINNEWSTEMPLATE_EDIT_PAGE("/newstemplate/weixinNewstemplate"),
	
	WEIXINSUBSCRIBE_LIST_PAGE("/message/weixinSubscribeList"),
	
	WEIXINSUBSCRIBE_EDIT_PAGE("/message/weixinSubscribe"),
	
	WEIXINGZUSER_DETAIL_PAGE("/user/weixinUserDetail"),
	
	WEIXINGZUSER_LIST_PAGE("/user/weixinUserList"),
	
	/** 微信素材管理 */
	WEIXIN_FILE_PAGE("/newstemplate/weixinImgFile")
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new back page.
	 *
	 * @param value the value
	 */
	private WeixinAdminPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("weixinadmin", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
