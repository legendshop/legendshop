/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.admin.page.AdminPage.AdminPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SMSLog;
import com.legendshop.spi.service.SMSLogService;

/**
 * 短信发送历史控制器
 *
 */
@Controller
@RequestMapping("/admin/system/smsLog")
public class SMSLogController extends BaseController {
	
	@Autowired
	private SMSLogService smsLogService;

	/**
	 * 查询短信发送历史列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param sMSLog
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, SMSLog sMSLog) {
		PageSupport<SMSLog> ps = smsLogService.getSMSLogPage(curPageNO, sMSLog);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("sMSLog", sMSLog);

		// return PathResolver.getPath(request, response,
		// BackPage.SMSLOG_LIST_PAGE);
		return PathResolver.getPath(AdminPage.SMSLOG_LIST_PAGE);
	}

}
