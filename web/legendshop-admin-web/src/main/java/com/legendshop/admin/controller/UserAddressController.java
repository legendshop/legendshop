/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.admin.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.UserAddressService;

/**
 * 用户地址控制器
 * 
 */
@Controller
@RequestMapping("/admin/userAddress")
public class UserAddressController extends BaseController{
	
	@Autowired
	private UserAddressService userAddressService;

	/**
	 * 查询用户地址列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			UserAddress userAddress) {
		PageSupport<UserAddress> ps = userAddressService.getUserAddressPage(curPageNO, userAddress);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userAddress", userAddress);
		return "/userAddress/userAddressList";
	}

	/**
	 * 保存用户地址
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, UserAddress userAddress) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		userAddressService.saveUserAddress(userAddress, userId);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/userAddress/query.htm";
	}

	/**
	 * 删除用户地址
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserAddress userAddress = userAddressService.getUserAddress(id);

		userAddressService.deleteUserAddress(userAddress);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/userAddress/query.htm";
	}

	/**
	 * 查看用户地址详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserAddress userAddress = userAddressService.getUserAddress(id);

		request.setAttribute("#entityClassInstance", userAddress);
		return "/userAddress/userAddress";
	}

	/**
	 * 跳转用户地址编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/userAddress/userAddress";
	}

	/**
	 * 更新用户地址
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		UserAddress userAddress = userAddressService.getUserAddress(id);

		request.setAttribute("userAddress", userAddress);
		return "forward:/admin/userAddress/query.htm";
	}

}
