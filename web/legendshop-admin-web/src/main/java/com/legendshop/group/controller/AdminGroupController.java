/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.GroupSelProdDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.group.page.GroupAdminPage;
import com.legendshop.group.page.GroupAdminRedirectPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.GroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 团购后台控制器.
 */
@Controller
@RequestMapping("/admin/group")
public class AdminGroupController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(AdminGroupController.class);

	@Autowired
	private GroupService groupService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;
	
	@Autowired
	private SubService subService;
	
	/**
	 * 团购产品查询页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, Group group, String curPageNO) {
		request.setAttribute("group", group);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(GroupAdminPage.GROUP_LIST_PAGE);
	}
	
	/**
	 * 团购产品查询列表
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, Group group, String curPageNO) {
		
		PageSupport<Group> ps = groupService.getGroupPage(curPageNO, group);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("group", group);
		adminMenuUtil.parseMenu(request, 8); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(GroupAdminPage.GROUP_CONTENT_LIST_PAGE);
	}


	/**
	 * 团购活动下线
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="团购活动下线")
	@RequestMapping("/offlineGroup/{id}")
	public @ResponseBody String offlineGroup(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Group group = groupService.getGroup(id);
		if (AppUtils.isBlank(group)) {
			return "找不到该活动";
		}
		
		if (group.getStatus() == 1) {
			String result = groupService.offlineGroup(group);
			return result;
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除团购活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除团购活动")
	@RequestMapping("/delete/{id}")
	public String deleteGroup(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new BusinessException("group id is null");
		}
		Group group = groupService.getGroup(id);
		if (AppUtils.isBlank(group)) {
			throw new BusinessException("error");
		}
		groupService.deleteGroup(group);
		return PathResolver.getPath(GroupAdminRedirectPage.GROUP_QUERY);
	}

	/**
	 * @Description: 查看活动
	 * @date 2016-7-20
	 */
	@RequestMapping(value = "/groupView/{id}", method = RequestMethod.GET)
	public String groupView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Group group = groupService.getGroup(id);
		if (AppUtils.isBlank(group)) {
			throw new BusinessException("group is null");
		}
		List<GroupSelProdDto> prodLists = productService.queryGroupProductByGroupId(group.getId());
		request.setAttribute("prodLists", prodLists);
		request.setAttribute("group", group);
		return PathResolver.getPath(GroupAdminPage.GROUP_VIEW);
	}

	/**
	 * @Description: 审核团购活动
	 * @date 2016-7-20
	 */
	@SystemControllerLog(description="审核团购活动")
	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	public @ResponseBody String audit(HttpServletRequest request, HttpServletResponse response, Group group) {
		if (AppUtils.isBlank(group) || AppUtils.isBlank(group.getAuditOpinion()) || AppUtils.isBlank(group.getStatus())) {
			return Constants.FAIL;
		}
		Group groupPo = groupService.getGroup(group.getId());
		if (AppUtils.isBlank(groupPo)) {
			return Constants.FAIL;
		}
		groupService.auditGroup(groupPo,group);
		
		return Constants.SUCCESS;
	}
	
	/**
	 * 活动运营列表
	 */
	@RequestMapping("/operation/{groupId}")
	public String queryOperationList(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long groupId) {
		
		Integer pageSize = 10;
		// 获取运营结果统计数据
		OperateStatisticsDTO operateStatistics = groupService.getOperateStatistics(groupId);
		
		PageSupport<Sub> ps = subService.getGroupOperationList(curPageNO, pageSize, groupId, null);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("groupId", groupId);
		request.setAttribute("operateStatistics",operateStatistics);
		return PathResolver.getPath(GroupAdminPage.GROUP_OPERATION_LIST_PAGE);
	}
	


}
