/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 前台页面定义.
 */
public enum GroupAdminFrontPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 商品信息 */
	GROUP_PROD("/prod/groupProd");
	
	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的团购后台页面.
	 * 
	 * @param value
	 * @param template
	 */
	private GroupAdminFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String, java.util.List)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("group", path);
	}

	public String getNativeValue() {
		return value;
	}

}
