/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum GroupAdminBackPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 团购商品  */
	GROUP_PROD_LAYOUT("/prod/groupProdLayout"),
	
	/** 团购商品类目 */
	CATEGORY_LOADCATEGORY_PAGE("/category/loadCategory");
	

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的团购后台页面.
	 *
	 * @param value the value
	 * @param template the template
	 */
	private GroupAdminBackPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("group", path);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
