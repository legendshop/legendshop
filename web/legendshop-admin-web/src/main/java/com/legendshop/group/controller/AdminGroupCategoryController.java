/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.group.page.GroupAdminBackPage;
import com.legendshop.group.page.GroupAdminPage;
import com.legendshop.group.page.GroupAdminRedirectPage;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.CategoryTree;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 团购商品分类控制器.
 *
 */
@Controller
@RequestMapping("/admin/group")
public class AdminGroupCategoryController extends BaseController {

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;

	/** 附件上传文件 */
	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 查询团购分类类目列表.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param category
	 * @return the string
	 */
	@RequestMapping("/category/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Category category) {
		List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.GROUP.value());
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		catTrees.add(new CategoryTree(0L, null, "团购分类", true));
		for (Category cat : catList) {
			catTrees.add(new CategoryTree(cat.getId(), cat.getParentId(), cat.getName()));
		}
		request.setAttribute("catTrees", JSONUtil.getJson(catTrees));

		PageSupport<Category> ps = categoryService.getCategoryPage(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("category", category);

		String result = PathResolver.getPath(GroupAdminPage.GROUP_CATEGORY_LIST);
		return result;
	}

	/**
	 * 保存团购商品类目.
	 *
	 * @param request
	 * @param response
	 * @param category
	 * @return the string
	 */
	@SystemControllerLog(description="保存团购商品类目")
	@RequestMapping(value = "/category/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Category category) {
		Category orginCategory = null;
		String catPic = null;
		SecurityUserDetail user = UserManager.getUser(request);
		if (category.getId() != null) {
			orginCategory = categoryService.getCategory(category.getId());
			if (orginCategory == null) {
				throw new NullPointerException("Origin Category is empty");
			}
			orginCategory.setCatDesc(category.getCatDesc());
			orginCategory.setHeaderMenu(false);
			orginCategory.setKeyword(category.getKeyword());
			// orginCategory.setLevel(category.getLevel());
			orginCategory.setName(category.getName());
			orginCategory.setNavigationMenu(false);
			// orginCategory.setParentId(category.getParentId());
			orginCategory.setRecDate(new Date());
			orginCategory.setSeq(category.getSeq());
			orginCategory.setStatus(category.getStatus());
			orginCategory.setTitle(category.getTitle());
			orginCategory.setType(ProductTypeEnum.GROUP.value());
			orginCategory.setTypeId(category.getTypeId());

			String originCatPic = orginCategory.getPic();
			if (category.getCatPicFile() != null && category.getCatPicFile().getSize() > 0) {
				catPic = attachmentManager.upload(category.getCatPicFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catPic, category.getCatPicFile(), AttachmentTypeEnum.CATEGORY);
				orginCategory.setPic(catPic);
			}
			// 删除原图片
			if (AppUtils.isNotBlank(originCatPic)) {
				attachmentManager.delAttachmentByFilePath(originCatPic);
				attachmentManager.deleteTruely(originCatPic);
			}
			categoryService.updateCategory(orginCategory);
		} else {
			if (category.getCatPicFile() != null && category.getCatPicFile().getSize() > 0) {
				catPic = attachmentManager.upload(category.getCatPicFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), catPic, category.getCatPicFile(), AttachmentTypeEnum.CATEGORY);
				category.setPic(catPic);
			}
			category.setType(ProductTypeEnum.GROUP.value());
			category.setRecDate(new Date());
			category.setHeaderMenu(false);
			category.setNavigationMenu(false);
			if (category.getParentId() == null || category.getParentId() == 0) {
				category.setParentId(0l);
				category.setGrade(1);
			} else {
				Category catParent = categoryService.getCategory(category.getParentId());
				category.setGrade(catParent.getGrade() + 1);
			}
			categoryService.saveCategory(category);
		}
		return PathResolver.getPath(GroupAdminRedirectPage.CATEGORY_LIST_QUERY);
	}

	/**
	 * 加载类目编辑页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/category/loadCategory")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response) {
		List<Category> catList = categoryService.getCategoryList(ProductTypeEnum.GROUP.value());
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		catTrees.add(new CategoryTree(0L, null, "顶级类目", true));
		for (Category cat : catList) {
			catTrees.add(new CategoryTree(cat.getId(), cat.getParentId(), cat.getName()));
		}
		request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
		String result = PathResolver.getPath(GroupAdminBackPage.CATEGORY_LOADCATEGORY_PAGE);
		return result;
	}

	/**
	 * 删除团购商品类目
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@SystemControllerLog(description="删除团购商品类目")
	@RequestMapping(value = "/category/delete/{id}")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Category category = categoryService.getCategory(id);
		if (AppUtils.isBlank(category)) {
			throw new NullPointerException("category is null");

		}
		List<Product> lists = productService.getProdByCateId(id);
        if(AppUtils.isBlank(lists)){
        	categoryService.deleteCategory(category);
        	return "ok";
        }
		return "error";
	}

	/**
	 * 清空团购商品类目缓存.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@SystemControllerLog(description="清空团购商品类目缓存")
	@RequestMapping(value = "/cleanCategoryCache", method = RequestMethod.POST)
	@ResponseBody
	public String cleanCategoryCache(HttpServletRequest request, HttpServletResponse response) {
		categoryService.cleanCategoryCache();
		return Constants.SUCCESS;
	}
}