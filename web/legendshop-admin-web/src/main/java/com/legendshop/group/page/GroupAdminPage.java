/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * GroupAdminPage
 */
public enum GroupAdminPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 ,如果不设置Templates默认支持所有的template */
	VARIABLE(""),

	/** 积分商城 商品类目 */
	GROUP_CATEGORY_LIST("/category/categoryList"),
	
	/** 团购活动管理 */
	GROUP_LIST_PAGE("/prod/groupList"),
	
	/** 团购活动列表 */
	GROUP_CONTENT_LIST_PAGE("/prod/groupContentList"),
	
	/** 编辑团购活动 */
	GROUP_PAGE("/prod/group"),
	
	/** 查看团购活动 */
	GROUP_VIEW("/prod/groupView"),
	
	/** 运营数据列表  */
	GROUP_OPERATION_LIST_PAGE("/prod/groupOperation"), 
	;
	
	/** The value. */
	private String value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("group", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private GroupAdminPage(String value, String... template) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
