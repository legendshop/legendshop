/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.filter;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.biz.service.AppBizTokenService;
import com.legendshop.app.biz.util.AppTokenUtil;
import com.legendshop.biz.model.dto.ShopTokenContext;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 商家端App过滤器.
 */

@Order(-999)
@Component
@WebFilter(urlPatterns = "/*", filterName = "legendAppFilter")
public class LegendAppFilter implements Filter {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(LegendAppFilter.class);

	@Autowired
	private AppBizTokenService appBizTokenService;

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		try {
			String requestURI = request.getRequestURI();
			String userId = request.getHeader("userId");
			String accessToken = request.getHeader("accessToken");
			String securiyCode = request.getHeader("securiyCode");
			String verId = request.getHeader("verId");
			String sign = request.getHeader("sign");
			String platform = request.getHeader("platform");
			String shopId = request.getHeader("shopId");

			if(AppUtils.isBlank(verId)){
				verId = "1.0";
			}

			log.info("<!-- Access User App URL {} With Parameters: userId={}, accessToken={}, securiyCode={} ", requestURI, userId, accessToken, securiyCode);

			// 是否需要验证token
			boolean needAuth = requestURI.indexOf("/s/")  != -1;

			Long shopID = null;
			if (AppUtils.isNotBlank(shopId)){
				shopID = Long.valueOf(shopId);
			}
			// 验证token
			AppToken appToken = appBizTokenService.getAppToken(userId,shopID,platform);

			// 校验参数签名
			ResultDto<Object> result = AppTokenUtil.checkAppBizToken(appToken, userId, verId, sign, accessToken, securiyCode,shopId);

			//成功并放行
			if(result.getStatus() == 1) {
				AppTokenDto token = new AppTokenDto(appToken.getUserId(), appToken.getUserName(), accessToken, verId, Long.valueOf(shopId));
				ShopTokenContext.setToken(token);
			}else {
				//需要校验,返回错误页面
				if(needAuth) {
					String data = JSONObject.toJSONString(result);
					PrintWriter writer = response.getWriter();
					writer.write(data);
					response.setStatus(200);
					return;
				}
			}

			chain.doFilter(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			String data = "{\"msg\":\"System Exception！\" ,\"result\":null,\"status\":\"-999\",\"verId\":\"1.0\"}";
			PrintWriter writer = response.getWriter();
			writer.write(data);
			response.setStatus(200);
		} finally {
			ShopTokenContext.clean();
		}

	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

}
