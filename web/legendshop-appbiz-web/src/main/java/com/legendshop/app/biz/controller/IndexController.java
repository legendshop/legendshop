/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  首页
 */
@ApiIgnore
@RestController
public class IndexController {


    /**
     * 首页
     *
     */
    @GetMapping("/")
    public String index(HttpServletRequest request,HttpServletResponse response){
        return "Welcome to LegendShop! 广州朗尊软件科技有限公司";
    }
}
