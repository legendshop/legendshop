package com.legendshop.app.biz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;

@EnableCaching
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        RedisAutoConfiguration.class, AopAutoConfiguration.class,
        AopAutoConfiguration.class}, scanBasePackages = {"com.legendshop"})
@ImportResource({"classpath:spring/applicationContext.xml"})
public class LegendShopAppBizApplication {
    /**
     * 日志.
     */
    private final static Logger log = LoggerFactory.getLogger(LegendShopAppBizApplication.class);

    /**
     * 主方法
     */
    public static void main(String[] args) {
        SpringApplication.run(LegendShopAppBizApplication.class, args);
        // 将hook线程添加到运行时环境中去
        Runtime.getRuntime().addShutdownHook(new CleanWorkThread());
    }

    /**
     * hook线程,关闭时调用
     */
    static class CleanWorkThread extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            log.info(" LegendShop Frontend System shutdowned.");
        }
    }


}
