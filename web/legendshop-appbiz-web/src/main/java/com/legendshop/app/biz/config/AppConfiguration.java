/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.config;

import com.legendshop.spi.service.LoginedUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  配置标准版本的配置
 *
 */
@Configuration
public class AppConfiguration {
	
	/**
	 *  登录用到的信息
	 */
	@Bean
	public LoginedUserService loginedUserService() {
		LoginedUserService service  = new DefaultBizLoginedUserServiceImpl();
		return service;
	}
   
}
