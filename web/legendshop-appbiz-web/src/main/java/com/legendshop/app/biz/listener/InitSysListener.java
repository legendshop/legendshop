/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.listener;

import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysDestoryEvent;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 *
 */
public class InitSysListener implements ServletContextListener {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(InitSysListener.class);

	/** The systemInited. */
	private static boolean systemInited = false;

	private final static Object systemInitedMonitor = new Object();

	/**
	 * 系统初始化.
	 * 
	 * @param event
	 *            the event
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		log.debug("system destory start");
		EventHome.publishEvent(new SysDestoryEvent(event));
	}



	/**
	 *
	 * @param event
	 * @see  ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		synchronized (systemInitedMonitor) {
			log.info("********* Initializing LegendShop System now *************");
			initSystem(event);
			if (!systemInited) {
				systemInited = true;
				// 3.ConfigCode
//				ConfigCode sQlCode = ConfigCode.getInstance("classpath*:DAL.cfg.xml");
//			    PropertiesUtil propertiesUtil = PropertiesUtilManager.getPropertiesUtil();
//				boolean debugMode = propertiesUtil.isSQLInDebugMode();
//				log.debug("System in DEBUG MODE is {}", debugMode);
//				sQlCode.setDebug(debugMode);

				// 初始化事件系统
				/*
				 * @SuppressWarnings("rawtypes") Map<String, BaseEventListener> eventListeners =
				 * EventHome.initBaseEventListener(); SpringBeanManager springBeanManager =
				 * (SpringBeanManager) ContextServiceLocator.getBean( "springBeanManager");
				 * springBeanManager.removeBean(event.getServletContext(),
				 * eventListeners.keySet());
				 */
			
//				SystemConfigService systemConfigService =  (SystemConfigService) ContextServiceLocator.getBean("systemConfigService");
//				SystemConfig systemConfig = systemConfigService.getSystemConfig();
//				
//				event.getServletContext().setAttribute(CommonConfigPropertiesEnum.SYSTEM_CONFIG.name(), systemConfig);
				
				// 发送系统启动事件
//				EventHome.publishEvent(new SysInitEvent(propertiesUtil.getBizAppDomainName()));

				printBeanFactory(ContextServiceLocator.getApplicationContext());
			}
			log.info("*********  LegendShop System Initialized successful **********");
		}
	}
	
	private void initSystem(ServletContextEvent event){
		
		// 1. 初始化系统真实路径
		String realPath = event.getServletContext().getRealPath("/");
				if (realPath != null && !realPath.endsWith("/")) {
					realPath = realPath + "/";
				}
		SystemUtil.setSystemRealPath(realPath);
		
		ServletContext servletContext = event.getServletContext();
		//相对路径
		String contextPath = servletContext.getContextPath();
		SystemUtil.setContextPath(contextPath);

	}

	private void printBeanFactory(ApplicationContext ctx) {
		if (log.isWarnEnabled()) {
			int i = 0;
			String[] beans = ctx.getBeanDefinitionNames();
			StringBuffer sb = new StringBuffer("系统配置的Spring Bean [ \n");
			if (!AppUtils.isBlank(beans)) {
				for (String bean : beans) {
					sb.append(++i).append(" ").append(bean).append("\n");
				}
				sb.append(" ]");
				log.warn(sb.toString());
			}
		}

	}

}
