/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.config;


import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.legendshop.app.biz.filter.LegendAppFilter;
import com.legendshop.base.MyStringToDateConverter;
import com.legendshop.core.helper.MappingExceptionResolver;
import com.legendshop.core.utils.MyMultipartResolver;
import com.legendshop.web.common.filter.ImageCacheFilter;
import com.thetransactioncompany.cors.CORSFilter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web mvc的基本配置
 *
 * @author linzh
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	
	/**
	 * 增加日期转化器
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new MyStringToDateConverter());
	}

	/**
	 * &#x6587;&#x4ef6;&#x4e0a;&#x4f20;&#x5904;&#x7406;
	 * @return
	 */
	@Bean
	public MultipartResolver parseMultipartResolver() {
		return new MyMultipartResolver();
	}


	/**
	 * 异常处理
	 */
	@Bean(name = "exceptionResolver")
	public HandlerExceptionResolver parseMappingExceptionResolver() {
		MappingExceptionResolver resolver = new MappingExceptionResolver();
		resolver.setDefaultErrorView("/pages/common/errorFrame");
		resolver.setDefaultAdminErrorView("/pages/common/errorAdminFrame");
		resolver.setWarnLogCategory("WARN");
		resolver.setDefaultStatusCode(500);

		return resolver;
	}

	/**
	 * fastjson支持
	 */
	@Bean
	public HttpMessageConverters fastjsonHttpMessageConverter() {
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
		
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		//返回值加入括号
		fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteMapNullValue, SerializerFeature.QuoteFieldNames);

		fastConverter.setFastJsonConfig(fastJsonConfig);
		
		HttpMessageConverter<?> converter = fastConverter;
		return new HttpMessageConverters(converter);
	}

	@Bean
	public FilterRegistrationBean<CORSFilter> someFilterRegistration() {
		FilterRegistrationBean<CORSFilter> registration = new FilterRegistrationBean<CORSFilter>();
		// 是否开启二级域名跨域
		registration.addInitParameter("cors.allowSubdomains","true");
		// 放行的域名list 以"," 号分割
		registration.addInitParameter("cors.allowOrigin","*");
		registration.addInitParameter("cors.supportedMethods", "GET,POST,HEAD,PUT,DELETE,OPTIONS");
		registration.addInitParameter("cors.supportedHeaders", "Accept,Origin,X-Requested-With,Content-Type,Last-Modified,id,accesstoken,securiycode,sign,time,userid,username,verid,platform,shopid");
		registration.addInitParameter("cors.exposedHeaders", "Set-Cookie");
		registration.addInitParameter("cors.supportsCredentials", "true");
		registration.addUrlPatterns("/*");
		CORSFilter corsFilter= new CORSFilter();
		registration.setName("CORSFilter");
		registration.setFilter(corsFilter);
		registration.setOrder(-1000);
		return registration;
	}


	/**
	 * 权限拦截器,如果采用oauth2的话,这个拦截器就没用了.
	 */
	@Bean
	public FilterRegistrationBean<LegendAppFilter> filterRegistrationBean() {
		FilterRegistrationBean<LegendAppFilter> filterRegistrationBean = new FilterRegistrationBean<LegendAppFilter>();
		filterRegistrationBean.setFilter(new LegendAppFilter());
		filterRegistrationBean.setName("legendAppFilter");
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.setOrder(1);
		return filterRegistrationBean;
	}

	/**
	 * 图片过滤器
	 */
	@Bean
	public FilterRegistrationBean<ImageCacheFilter> indexImageCacheFilterRegistration() {
		FilterRegistrationBean<ImageCacheFilter> registration = new FilterRegistrationBean<ImageCacheFilter>(
				new ImageCacheFilter());
		registration.addInitParameter("Cache-Control", "max-age=315360000");
		registration.addInitParameter("Access-Control-Max-Age", "315360000");
		registration.addInitParameter("Expires", "315360000");
		registration.addUrlPatterns("/photoserver/*");
		return registration;
	}
	
	
}
