package com.legendshop.test.bean;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.biz.model.dto.ShopAddressDto;
import com.legendshop.util.JSONUtil;

public class AddressDtoJsonUtil {
	
	public static void main(String[] args) {
		List<ShopAddressDto> addrList = new ArrayList<ShopAddressDto>();
		ShopAddressDto dto = new ShopAddressDto();
		dto.setAddrId(1l);
		/*dto.setAliasAddr("aliasAddr");*/
		
		addrList.add(dto);
		System.out.println(JSONUtil.getJson(addrList));
		
	}

}
