package com.legendshop.test.bean;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.legendshop.dao.Utils;
import com.legendshop.test.bean.model.BrandDto;
import com.legendshop.test.bean.model.ShopBrandDto;

public class NewBeanUtil {
	
    /** The Constant SUPPORTED_SQL_OBJECTS. */
    static final Set<Class<?>> SUPPORTED_SQL_OBJECTS = new HashSet<Class<?>>();

    static {
        Class<?>[] classes = {
                boolean.class, Boolean.class,
                short.class, Short.class,
                int.class, Integer.class,
                long.class, Long.class,
                float.class, Float.class,
                double.class, Double.class,
                String.class,
                Date.class,
                Timestamp.class,
                List.class,
                Set.class,
                Map.class,
                ArrayList.class,
                HashMap.class,
                HashSet.class,
                TreeSet.class,
                TreeMap.class,
                MultipartFile.class,
                CommonsMultipartFile.class
        };
        SUPPORTED_SQL_OBJECTS.addAll(Arrays.asList(classes));
    }
    

	public void printCode(Class<?> target){

		String targetName = target.getSimpleName();
		String targetName1 = toLowCase(targetName);
		
		StringBuffer sb = new StringBuffer();
		sb.append("public ").append(targetName).append(" create").append(targetName).append("(").append("){").append("\n");
		printMethod(target, sb, 1);
		appendTab(sb,1);
		sb.append("return ").append(targetName1).append(";\n");
		sb.append("}");
		System.out.println(sb);
		
	}
	
	public BrandDto  newBean(){
		BrandDto dto = new BrandDto();
		dto.setBrandId(1l);
		dto.setBrandName("brandName1");
		ShopBrandDto shopBrandDto = new ShopBrandDto();
		dto.setShopBrandDto(shopBrandDto);
		return dto;
	}
	
	private void printMethod(Class<?> target, StringBuffer sb, int tabNum){
		Map<String, Method> targetSetters = Utils.findPublicSetters(target);
		Map<String, Method> targetGetters = Utils.findPublicGetters(target);
		String targetName = target.getSimpleName();
		String targetName1 = toLowCase(targetName);
		appendTab(sb, tabNum);
		sb.append(targetName).append(" ").append(targetName1).append(" = ").append("new ").append(targetName).append("();\n");
		for (String setter : targetSetters.keySet()) {
			Method targetSetter = targetSetters.get(setter);
			Method targetGetter = targetGetters.get(setter);
			if(targetSetter != null && targetGetter != null){
				if(isBaseType(targetGetter.getReturnType())){
					appendTab(sb,tabNum);
					sb.append(targetName1).append(".").append(targetSetter.getName()).append("(").append(getResult(targetGetter)).append(");\n");
					}else{
						appendTab(sb, tabNum);
						sb.append("//子类调用，请手工检查是否正确\n");
						int deep = tabNum;
						printMethod(targetGetter.getReturnType(),  sb, deep);//递归调用子类
						
						appendTab(sb,tabNum);
						sb.append(targetName1).append(".").append(targetSetter.getName()).append("(").append(toLowCase(targetGetter.getReturnType().getSimpleName())).append(");\n");
						}
		}

		}
	}
	
	private String getResult(Method targetGetter){
		Class clazz = targetGetter.getReturnType();
		if(String.class.equals(clazz)){
			return "\"" + targetGetter.getName() +  "\"";
		}else if(Long.class.equals(clazz)){
			return "10l";
		}else if(Integer.class.equals(clazz)){
			return "2";
		}else if(Short.class.equals(clazz)){
			return "3";
		}else if(Float.class.equals(clazz)){
			return "0.1f";
		}else if(Double.class.equals(clazz)){
			return "0.2d";
		}else if(BigDecimal.class.equals(clazz)){
			return "new BigDecimal(4)";
		}else{
			return "1l";
		}
		
	}
	

	private void appendTab(StringBuffer sb, int tabNum){
		for (int i = 0; i < tabNum; i++) {
			sb.append("	");
		}
	}
	
	private String toLowCase(String sosurce){
		char[] cbuf = sosurce.toCharArray();
		cbuf[0] = Character.toLowerCase(cbuf[0]);
		return new String(cbuf);
	}
	
    /**
     * Checks if is supported sql object.
     *
     * @param clazz the clazz
     * @return true, if is supported sql object
     */
    public  static boolean isBaseType(Class<?> clazz) {
        return clazz.isEnum() || SUPPORTED_SQL_OBJECTS.contains(clazz);
    }

}
