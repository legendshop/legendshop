/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

import com.legendshop.util.DateUtil;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.spi.service.DeliverGoodsAddrService;
import com.legendshop.spi.service.LocationService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserAddressSubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 用于订单运dan打印
 * 
 * @author tony
 * 
 */
public class DeliveryPrintDataUtils {

	public static final String SHIP_NAME = "ship_name"; // 收货人-姓名
	public static final String SHIP_AREA = "ship_area"; // 收货人-地区
	public static final String SHIP_ADDR = "ship_addr"; // 收货人-地址
	public static final String SHIP_TEL = "ship_tel"; // 收货人-电话
	public static final String SHIP_MOBILE = "ship_mobile"; // 收货人-手机
	public static final String SHIP_ZIP = "ship_zip"; // 收货人-邮编
	public static final String DLY_NAME = "dly_name"; // 发货人-姓名
	public static final String DLY_AREA = "dly_area"; // 发货人-地区
	public static final String DLY_ADDRESS = "dly_address"; // 发货人-地址
	public static final String DLY_TEL = "dly_tel"; // 发货人-电话
	public static final String DLY_MOBILE = "dly_mobile"; // 发货人-手机
	public static final String DLY_ZIP = "dly_zip"; // 发货人-邮编
	public static final String DATE_Y = "date_y"; // 当日日期-年
	public static final String DATE_M = "date_m"; // 当日日期-月
	public static final String DATE_D = "date_d"; // 当日日期-日
	public static final String ORDER_NUMBER = "order_number"; // 订单-订单号
	public static final String ORDER_PRICE = "order_price"; // 订单总金额
	public static final String SHIP_PRICE = "ship_price"; // 订单运费金额
	public static final String ORDER_WEIGHT = "order_weight"; // 订单物品总重量
	public static final String ORDER_VOLUME = "order_volume"; // 订单物品总重量
	public static final String ORDER_MEMO = "order_memo"; // 订单-备注
	public static final String ORDER_COUNT = "order_count"; // 订单-物品数量
	public static final String SHIP_TIME = "ship_time"; // 订单-送货时间
	public static final String SHOP_NAME = "shop_name"; // 网店名称
	public static final String ORDER_NAME = "order_name"; // shangpin名称
	public static final String TICK = "tick"; // 对号 - √
	public static final String TEXT = "text"; // 自定义内容

	/**
	 * 模版数据集
	 * 
	 * 
	 * 
	 * 
	 */
	private String title = "打印";
	private double height = 0;
	private double width = 0;
	private String backgroung;
	private String tmplData = "<printer picpos=\"0:0\"></printer>";
	private Long subId;
	private Long shopId;
	
	private Integer isSystem;

	private static SubService subService;
	
	private static UserAddressSubService userAddressSubService;

	private static DeliverGoodsAddrService deliverGoodsAddrService;

	private static LocationService locationService;
	
	private static ShopDetailService shopDetailService;

	static {
		subService = (SubService) ContextServiceLocator.getBean("subService");
		userAddressSubService = (UserAddressSubService) ContextServiceLocator.getBean("userAddressSubService");
		deliverGoodsAddrService = (DeliverGoodsAddrService) ContextServiceLocator.getBean("deliverGoodsAddrService");
		locationService = (LocationService) ContextServiceLocator.getBean("locationService");
		shopDetailService = (ShopDetailService) ContextServiceLocator.getBean("shopDetailService");
	}

	public DeliveryPrintDataUtils(double height, double width, String tmplData) {
		this.height = height;
		this.width = width;
		this.tmplData = tmplData;
	}

	public String getXmlData(){
		Sub sub = null; 
		if(AppUtils.isNotBlank(shopId)){
			 sub = subService.getSubByShopId(subId, shopId);
		}else{
			 sub = subService.getSubById(subId);
			 shopId = sub.getShopId();
		}
		
		// 发货人信息
		String dlyName = "";
		String dlyArea ="";
		String dlyAddress ="";
		String dlyMobile = "";
		String dlyTel = "";
		String dlyZip = "";
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if(AppUtils.isNotBlank(shopDetail) && AppUtils.isNotBlank(shopDetail.getProvinceid())
				&& AppUtils.isNotBlank(shopDetail.getCityid()) && AppUtils.isNotBlank(shopDetail.getAreaid())
				){
			dlyName = shopDetail.getContactName();
			Province province = locationService.getProvinceById(shopDetail.getProvinceid());
			City city = locationService.getCityById(shopDetail.getCityid());
			Area area = locationService.getAreaById(shopDetail.getAreaid());
			if(AppUtils.isNotBlank(area)){
				dlyArea = province.getProvince() + " " + city.getCity() + " " + area.getArea();
			}else{
				dlyArea = province.getProvince() + " " + city.getCity();
			}
			dlyAddress = dlyArea + " " + shopDetail.getShopAddr();
			dlyMobile = shopDetail.getContactMobile();
			dlyTel = shopDetail.getContactTel();
			dlyZip = shopDetail.getCode();
		}

		UserAddressSub addressSub = userAddressSubService.getUserAddressSub(sub.getAddrOrderId());

		String shipName = addressSub.getReceiver();
		String shipTel = addressSub.getTelphone();
		/*String shipArea = addressSub.getProvince() + " " + addressSub.getCity()
				+ " " + addressSub.getArea();*/
		String shipAddr = addressSub.getSubAdds();
		String shipMobile = addressSub.getMobile();
		String shipZip = addressSub.getSubPost();

		String tmplData=getTmplData();
		
		
		Date date = new Date();
		Calendar cal =Calendar.getInstance();
		cal.setTime(date);
		String year = cal.get(Calendar.YEAR) +"" ;
		String month = cal.get(Calendar.MONTH) + 1 + "" ;
		String day = cal.get(Calendar.DATE)+"";
		
		String result = tmplData.replaceAll("收货人-姓名", shipName==null?"":shipName);
		result = result.replaceAll("收货人-地址",addressSub.getDetailAddress()==null?"":addressSub.getDetailAddress());
		/*result = result.replaceAll("收货人-地址", shipAddr==null?"":shipAddr);*/
		result = result.replaceAll("收货人-电话", shipTel==null?"":shipTel);
		result = result.replaceAll("收货人-手机", shipMobile==null?"":shipMobile);
		result = result.replaceAll("收货人-邮编",shipZip==null?"":shipZip);
		result = result.replaceAll("发货人-姓名",dlyName==null?"":dlyName);
		/*result = result.replaceAll("发货人-地区",dlyArea==null?"":dlyArea);*/
		result = result.replaceAll("发货人-地址", dlyAddress==null?"":dlyAddress);
		result = result.replaceAll("发货人-邮编", dlyZip==null?"":dlyZip);
		result = result.replaceAll("发货人-电话", dlyTel==null?"":dlyTel);
		result = result.replaceAll("发货人-手机",dlyMobile==null?"":dlyMobile);
		result = result.replaceAll("当日日期-年", year==null?"":year);
		result = result.replaceAll("当日日期-月", month==null?"":month);
		result = result.replaceAll("当日日期-日", day==null?"":day);
		result = result.replaceAll("订单-订单号", sub.getSubNumber()==null?"":sub.getSubNumber());
		result = result.replaceAll("订单总金额",  String.valueOf(sub.getActualTotal())==null?"":String.valueOf(sub.getActualTotal()));
		result = result.replaceAll("订单运费金额",  String.valueOf(sub.getFreightAmount())==null?"":String.valueOf(sub.getFreightAmount()));
		result = result.replaceAll("订单物品总重量", String.valueOf(sub.getWeight())==null?"":String.valueOf(sub.getWeight()));
		result = result.replaceAll("订单物品总体积", String.valueOf(sub.getVolume())==null?"":String.valueOf(sub.getVolume()));
		result = result.replaceAll("订单-物品数量", String.valueOf(sub.getProductNums())==null?"":String.valueOf(sub.getProductNums()));
		result = result.replaceAll("订单-备注", sub.getOrderRemark()==null?"":sub.getOrderRemark());
		result = result.replaceAll("订单-送货时间", DateUtil.DateToString(cal.getTime(),"yyyy-MM-dd hh:mm:ss")==null?"": DateUtil.DateToString(cal.getTime(),"yyyy-MM-dd hh:mm:ss"));
		result = result.replaceAll("店铺名称",sub.getShopName()==null?"":sub.getShopName());
		result = result.replaceAll("订单商品名称",sub.getProdName()==null?"":sub.getProdName());
		return result;
	}
	
	/**
	 * 替换字符串
	 * @param result
	 * @param oldStr
	 * @param newStr
	 * @return
	 */
	private String replaceAll(String result, String oldStr, String newStr){
		if(newStr==null){
			newStr = "";
		}
		return result.replaceAll(oldStr,newStr);
	}
		

	@Deprecated
	public String getTemlData_old() {

		// 第一种方式：创建文档，并创建根元素
		// 创建文档:使用了一个Helper类
		Document document = DocumentHelper.createDocument();

		// 创建根节点并添加进文档
		Element root = DocumentHelper.createElement("data");
		document.setRootElement(root);

		Sub sub = subService.getSubById(subId);
		// 发货人信息
		String dlyName = "";
		String dlyArea = "";
		String dlyAddress = "";
		String dlyTel = "";
		String dlyMobile = "";
		String dlyZip = "";

		UserAddressSub addressSub = userAddressSubService.getUserAddressSub(sub
				.getAddrOrderId());

		String shipName = addressSub.getReceiver();
		String shipTel = addressSub.getTelphone();
		/*String shipArea = addressSub.getProvince() + " " + addressSub.getCity()
				+ " " + addressSub.getArea();*/
		String shipAddr = addressSub.getSubAdds();
		String shipMobile = addressSub.getMobile();
		String shipZip = addressSub.getSubPost();

		// "ship_name""收货人-姓名"
		root.addElement(SHIP_NAME).addText(shipName);
		// "ship_area""收货人-地区"
		//root.addElement(SHIP_AREA).addText(shipArea);
		// "ship_addr"" 收货人-地址"
		root.addElement(SHIP_ADDR).addText(addressSub.getDetailAddress());
		// "ship_tel"" 收货人-电话"
		root.addElement(SHIP_TEL).addText(shipTel);
		root.addElement(SHIP_MOBILE).addText(shipMobile);
		root.addElement(SHIP_ZIP).addText(shipZip);

		/* 发货信息 */
		root.addElement(DLY_NAME).addText(dlyName);
		root.addElement(DLY_AREA).addText(dlyArea);
		root.addElement(DLY_ADDRESS).addText(dlyAddress);
		root.addElement(DLY_ZIP).addText(dlyZip);
		root.addElement(DLY_TEL).addText(dlyTel);
		root.addElement(DLY_MOBILE).addText(dlyMobile);
		Calendar calendar = Calendar.getInstance();
		root.addElement(DATE_Y).addText(calendar.get(Calendar.YEAR) + "");
		root.addElement(DATE_M).addText(calendar.get(Calendar.MONTH) + 1 + "");
		root.addElement(DATE_D).addText(calendar.get(Calendar.DATE) + "");
		root.addElement(ORDER_NUMBER).addText(sub.getSubNumber());
		root.addElement(ORDER_PRICE).addText(
				String.valueOf(sub.getActualTotal()));
		root.addElement(SHIP_PRICE).addText(
				String.valueOf(sub.getFreightAmount()));
		root.addElement(ORDER_WEIGHT).addText(String.valueOf(sub.getWeight()));
		root.addElement(ORDER_VOLUME).addText(String.valueOf(sub.getVolume()));
		root.addElement(ORDER_MEMO).addText(sub.getOrderRemark());
		root.addElement(ORDER_COUNT).addText(
				String.valueOf(sub.getProductNums()));
		root.addElement(SHIP_TIME)
				.addText(
						DateUtil.DateToString(calendar.getTime(),
								"yyyy-MM-dd hh:mm:ss"));
		root.addElement(SHOP_NAME).addText(sub.getShopName());
		root.addElement(ORDER_NAME).addText(sub.getProdName());
		root.addElement(TICK).addText("√");
		StringWriter writer = new StringWriter();
		XMLWriter xmlWriter = null;
		try {
			OutputFormat format = OutputFormat.createCompactFormat();
			format.setSuppressDeclaration(true);
			xmlWriter = new XMLWriter(writer, format);
			xmlWriter.write(document);
		} catch (IOException e) {
			throw new RuntimeException(e);// NOSONAR
		} finally {
			if (xmlWriter != null) {
				try {
					xmlWriter.close();
				} catch (IOException e) {
					throw new RuntimeException(e);// NOSONAR
				}
			}
		}
		String str = writer.getBuffer().toString();
		return str;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public String getBackgroung() {
		return backgroung;
	}

	public void setBackgroung(String backgroung) {
		this.backgroung = backgroung;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getTmplData() {
		return tmplData;
	}

	public void setTmplData(String tmplData) {
		this.tmplData = tmplData;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}
	
	

}
