package com.legendshop.core.randing;

import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.dto.RandNumDto;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;

public class ValidImgRander {

    private Random random = new Random();
    private String randString = "23456789abcdefghijkmnpqrstuvwxyz";//随机产生的字符串
    
    private int width = 100;//图片宽
    private int height = 36;//图片高
    private int lineSize = 5;//干扰线数量
    private int stringNum = 4;//随机产生字符数量

	private static CacheClient cacheClient;
	
	static {
		cacheClient = ContextServiceLocator.getBean(CacheClient.class, "redisCacheClient");
	}
    
    /**
     * 生成随机图片, 存放到Session
     */
    public void produceRandcodeToSession(HttpServletRequest request,
            HttpServletResponse response) {

		HttpSession session = request.getSession();
		
		RandNumDto randNum = produceRandcode(response);
		
		session.setAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY ,  randNum);
    }
    
    /**
     * 生成随机图片, 存放到Cache
     * @param response
     */
	public void produceRandcodeToCache(HttpServletResponse response) {
		RandNumDto randNum = produceRandcode(response);
		cacheClient.put(AttributeKeys.RANDOM_VALIDATE_CODE_KEY  + ":" + randNum.getRandomString(), 60 * 5, randNum);//默认5分钟
	}

    private RandNumDto produceRandcode(HttpServletResponse response){
    	
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
        Graphics2D g = image.createGraphics();//产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.fillRect(0, 0, width, height);
        g.setFont(new Font("Arial",Font.ITALIC,30));
        g.setColor(getRandColor(0, 100));
        g.setColor(new Color(random.nextInt(100),random.nextInt(100),random.nextInt(100)));
        
        //绘制干扰线
        for(int i=1;i<=lineSize;i++){
            drowLine(g);
        }
        
        //绘制随机字符
        String randomString = "";
        for(int i=0;i<stringNum;i++){
            randomString = drowString(g,randomString,i);
        }
        
        g.dispose();
        
        try {
            ImageIO.write(image, "JPEG", response.getOutputStream());//将内存中的图片通过流动形式输出到客户端
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return new RandNumDto(randomString);
    }
    
        
    /*
     * 获得字体
     */
    private Font getFont(){
        return new Font("Fixedsys",Font.CENTER_BASELINE,30);
    }
    /*
     * 获得颜色
     */
    private Color getRandColor(int fc,int bc){
        if(fc > 255)
            fc = 255;
        if(bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc-fc-20);
        int g = fc + random.nextInt(bc-fc-10);
        int b = fc + random.nextInt(bc-fc-30);
        return new Color(r,g,b);
    }
    
    /*
     * 绘制字符串
     */
    private String drowString(Graphics2D g,String randomString,int i){
        g.setFont(getFont());
        String rand = String.valueOf(getRandomString(random.nextInt(randString.length())));
        randomString +=rand;
        g.translate(random.nextInt(3), random.nextInt(3));
        AffineTransform trans = new AffineTransform();
        trans.rotate((random.nextInt(10)%2==0?1:-1)*random.nextInt(7)/10d,24*i+5 , 25);
        g.setTransform(trans);
        g.drawString(rand, 24*i+5, 25);
        return randomString;
    }
    /*
     * 绘制干扰线
     */
    private void drowLine(Graphics2D g){
    	int x = random.nextInt(5);
    	int y = random.nextInt(height);
    	int x1 = 75 + random.nextInt(5);
    	int y1 = random.nextInt(height);
    	g.drawLine(x, y, x1, y1);
    }
    /*
     * 获取随机的字符
     */
    private String getRandomString(int num){
        return String.valueOf(randString.charAt(num));
    }
}
