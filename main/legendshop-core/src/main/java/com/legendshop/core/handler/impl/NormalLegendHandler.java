package com.legendshop.core.handler.impl;

import com.legendshop.base.exception.ApplicationException;
import com.legendshop.base.exception.BaseException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.core.handler.LegendHandler;
import com.legendshop.core.handler.XssHttpServletRequestWrapper;
import com.legendshop.framework.handler.RequestHolder;
import com.legendshop.util.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器
 * @author hewq
 *
 */
@Component
@Slf4j
public class NormalLegendHandler implements LegendHandler {


	/** 这种情况才会增加权限和spring mvc的判断. */
    public static final String CONTROLLER_APPLIED = "__controller_applied__";

    @Override
	public void doHandle(boolean isSupportUrl, HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
		try {
			// 针对action做的请求
			if (isSupportUrl) {
				// 过滤XSS攻击
				XssHttpServletRequestWrapper wrapperRequest = new XssHttpServletRequestWrapper(request);

				request.setAttribute(CONTROLLER_APPLIED, Boolean.TRUE);
				RequestHolder.setRequest(wrapperRequest);
				chain.doFilter(wrapperRequest, response);

			}else{
				chain.doFilter(request, response);
			}
		
		} catch (BaseException be) {
			log.error("Business error: ", be);
			throw be;
		} catch (Exception e) {
			log.error("Error: in path: " + request.getRequestURI(), e);
			throw new ApplicationException(e, "",ErrorCodes.SYSTEM_ERROR);
		} finally {
			if(isSupportUrl){
				RequestHolder.clean();
			}
		}
	}

}
