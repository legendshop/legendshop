/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.core.handler.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.legendshop.core.handler.ShopCartHandler;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.BasketCookieDto;
import com.legendshop.model.entity.Basket;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.util.CookieUtil;

/**
 * 购物车登录前处理器.
 *
 */
@Service("shopCartHandler")
public class ShopCartHandlerImpl implements ShopCartHandler {

    private final Logger log = LoggerFactory.getLogger(ShopCartHandlerImpl.class);
    
    //1.配置是否采用COOKIE or SESSION 去记录用户的购物车信息 从登录Session获取用户的购物车信息 2.从Cookie获取用户的购物信息
    private String ENABLE_COOKIE_SESSION = "COOKIE";

    //2. 购物车Cookies最大的生命周期, 可以修改
    private Integer COOKIE_MAX_TIME = 604800;
    
    /**
     * 获取购物车商品
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Basket> getShopCart(HttpServletRequest request) {
        List<Basket> baskets = new ArrayList<Basket>();
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            List<Basket> basketList = (List<Basket>) request.getSession().getAttribute(Constants.BASKET_KEY);
            if (AppUtils.isBlank(basketList)) {
                return null;
            }
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);
            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    log.warn("get basket list error", e);
                }
                if (AppUtils.isBlank(list)) {
                	return null;
                }
                for (BasketCookieDto dto : list) {
                    Basket b = new Basket();
                    b.setProdId(dto.getProdId());
                    b.setSkuId(dto.getSkuId());
                    b.setShopId(dto.getShopId());
                    b.setDistUserName(dto.getDistUserName());
                    b.setBasketCount(dto.getBasketCount());
                    b.setCheckSts(dto.getCheckSts());
                    b.setStoreId(dto.getStoreId());
                    baskets.add(b);
                }
                return baskets;
            }
        }
        return baskets;
    }

    /*
     * 移除购物车商品
     * @see com.legendshop.spi.service.ShopCartHandler#deleteShopCartByProdId(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Long, java.lang.Long)
     */
    @Override
    public void deleteShopCartByProdId(HttpServletRequest request, HttpServletResponse response, Long prod_id, Long sku_id) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            @SuppressWarnings("unchecked")
            List<Basket> baskets = (List<Basket>) request.getSession().getAttribute(Constants.BASKET_KEY);
            Iterator<Basket> it = baskets.iterator();  //
            while (it.hasNext()) {
                Basket b = it.next();
                if (isTheSameValue(prod_id, b.getProdId()) && isTheSameValue(sku_id, b.getSkuId())) {//prodId 和 skuId都相等认为是同一个商品
                    it.remove();
                }
            }
            request.getSession().setAttribute(Constants.BASKET_KEY, baskets);
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);
            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Iterator<BasketCookieDto> it = list.iterator();  //
                List<BasketCookieDto> newList = new ArrayList<BasketCookieDto>();
                while (it.hasNext()) {
                    BasketCookieDto dto = (BasketCookieDto) it.next();
                    Long prodId_cookie = dto.getProdId();
                    Long skuId_cookie = dto.getSkuId();
                    if (isTheSameValue(prod_id, prodId_cookie) && isTheSameValue(sku_id, skuId_cookie)) {//prodId 和 skuId都相等认为是同一个商品
                        it.remove();
                    } else {
                        newList.add(dto);
                    }
                }
                list = null;
                it = null;
                CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(newList));
            }
        }
    }

    /**
     * 判断2个值是否一致
     *
     * @param value
     * @param value2
     * @return
     */
    private boolean isTheSameValue(Long value, Long value2) {
        if (AppUtils.isBlank(value) && AppUtils.isBlank(value2)) { //2个都为空，则认为相同
            return true;
        }
        if (AppUtils.isBlank(value) || AppUtils.isBlank(value2)) {//任意一个为空就不相等
            return false;
        }
        return value.equals(value2);
    }


    /**
     * 存入购物车
     */
    @Override
    public void setShopCart(HttpServletRequest request,HttpServletResponse response, List<Basket> baskets) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            request.getSession().setAttribute(Constants.BASKET_KEY, baskets);
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            List<BasketCookieDto> list = new ArrayList<BasketCookieDto>();
            for (Basket basket : baskets) {
                BasketCookieDto basketJson = new BasketCookieDto();
                basketJson.setBasketCount(basket.getBasketCount());
                basketJson.setProdId(basket.getProdId());
                basketJson.setSkuId(basket.getSkuId());
                basketJson.setShopId(basket.getShopId());
                basketJson.setDistUserName(basket.getDistUserName());
                basketJson.setCheckSts(basket.getCheckSts());
                basketJson.setStoreId(basket.getStoreId());
                list.add(basketJson);
            }

            CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(list));
        }
    }


    /**
     * 清理购物车
     */
    @Override
    public void clearShopCarts(HttpServletRequest request,
                               HttpServletResponse response) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            request.getSession().removeAttribute(Constants.BASKET_KEY);
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            CookieUtil.deleteCookie(request, response, Constants.BASKET_KEY);
        }
    }


    /**
     * 修改购物车的商品数量
     */
    @Override
    public void changeShopCartNum(HttpServletRequest request,
                                  HttpServletResponse response, Long prod_id, Long sku_id, Integer num) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            @SuppressWarnings("unchecked")
            List<Basket> baskets = (List<Basket>) request.getSession().getAttribute(Constants.BASKET_KEY);
            Iterator<Basket> it = baskets.iterator();
            while (it.hasNext()) {
                Basket basket = it.next();
                if (prod_id.equals(basket.getProdId())) {
                    basket.setBasketCount(num);
                }
            }
            request.getSession().setAttribute(Constants.BASKET_KEY, baskets);
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);

            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Iterator<BasketCookieDto> it = list.iterator();
                while (it.hasNext()) {
                    BasketCookieDto cookieDto = it.next();
                    Long prodId = cookieDto.getProdId();
                    Long skuId = cookieDto.getSkuId();
                    if (prod_id.equals(prodId)) {
                        if (AppUtils.isNotBlank(skuId) && skuId != 0) {
                            if (skuId.equals(sku_id)) {
                                cookieDto.setBasketCount(num);
                            }
                        } else {
                            cookieDto.setBasketCount(num);
                        }
                    }
                    cookieDto.setCheckSts(1);
                }
                CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(list));
            }
        }
    }

    /**
     * 获取购物车商品数量
     */
    @Override
    public int getShopCartAllCount(HttpServletRequest request) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return 0;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            @SuppressWarnings("unchecked")
            List<Basket> basketList = (List<Basket>) request.getSession().getAttribute(Constants.BASKET_KEY);
            if (AppUtils.isBlank(basketList)) {
                return 0;
            }
            int number = 0;
            for (Basket dto : basketList) {
                number += dto.getBasketCount();
            }
            return number;
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);
            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    log.warn("get basket list error", e);
                }
                if (AppUtils.isBlank(list))
                    return 0;

                int number = 0;
                for (BasketCookieDto dto : list) {
                    number += dto.getBasketCount();
                }
                return number;
            }
        }
        return 0;
    }


    /**
     * 未登录----加入购物车
     */
    @Override
    public String addShopCart(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId, int count, Long shopId, String distUserName, Long skuStock,Long storeId) {
        List<Basket> baskets = getShopCart(request);
        if (AppUtils.isBlank(baskets)) {//第一次加入购物车
            baskets = new ArrayList<Basket>();
            Basket b = new Basket();
            b.setProdId(prodId);
            b.setBasketCount(count);
            b.setSkuId(skuId);
            b.setShopId(shopId);
            b.setDistUserName(distUserName);
            b.setCheckSts(1);
            b.setStoreId(storeId);
            baskets.add(b);
            //加入购物车
            setShopCart(request, response, baskets);
            return Constants.SUCCESS;
        } else {//第二次以上加入购物车
            Basket b = null;
            for (int j = 0; j < baskets.size(); j++) {
                Basket basket = baskets.get(j);
                if (prodId.equals(basket.getProdId()) && skuId.equals(basket.getSkuId())) {
                    b = basket;
                    break;
                }
            }
            if (b == null) {//第一次加入该商品
                b = new Basket();
                b.setProdId(prodId);
                b.setBasketCount(count);
                b.setSkuId(skuId);
                b.setDistUserName(distUserName);
                b.setShopId(shopId);
                b.setCheckSts(1);
                b.setStoreId(storeId);
                //加入购物车
                baskets.add(b);
                setShopCart(request, response, baskets);
                return Constants.SUCCESS;
            } else {//第二次以上加入该商品，重新计算数量
                if (skuStock - (b.getBasketCount() + count) < 0) {
                    return "亲,目前商品库存不足, 购买数不能超过" + skuStock + "件哦!";
                }
					
            	if (storeId != null && !storeId.equals(b.getStoreId()) ) {//更换重新选择的门店
                	b.setStoreId(storeId);
				}
                
                b.setBasketCount(b.getBasketCount() + count);
                b.setCheckSts(1);
                setShopCart(request, response, baskets);
                return Constants.SUCCESS;
            }
        }
    }

    /**
     * 改变  购物车项 的选中状态
     */
    @Override
    public void updateBasketChkSts(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId, int checkSts) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            //TODO
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);

            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Iterator<BasketCookieDto> it = list.iterator();
                while (it.hasNext()) {
                    BasketCookieDto cookieDto = it.next();
                    Long prod_id = cookieDto.getProdId();
                    Long sku_id = cookieDto.getSkuId();
                    if (prodId != null && skuId != null) {
                        if (prodId.equals(prod_id) && skuId.equals(sku_id)) {
                            cookieDto.setCheckSts(checkSts);
                        }
                    }
                }
                CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(list));
            }
        }
    }

    /**
     * 改变  购物车项 的选中状态
     */
    @Override
    public void updateBasketChkSts(HttpServletRequest request, HttpServletResponse response,  List<BasketCookieDto> basketCookieDtoList) {
        if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            //TODO
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);

            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                for (BasketCookieDto basketCookieDto : basketCookieDtoList) {
                	for (BasketCookieDto cookieDto : list) {
                         Long prod_id = cookieDto.getProdId();
                         Long sku_id = cookieDto.getSkuId();
                         if (basketCookieDto.getProdId() != null && basketCookieDto.getSkuId() != null) {
                             if (basketCookieDto.getProdId() .equals(prod_id) && basketCookieDto.getSkuId().equals(sku_id)) {
                                 cookieDto.setCheckSts(basketCookieDto.getCheckSts());
                                 break;
                             }
                         }
					}
                         
				}
              
                CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(list));
            }
        }
    }

    /**
     * 更改购物车已选门店
     */
	@Override
	public void changeShopStore(HttpServletRequest request, HttpServletResponse response, Long prod_id, Long sku_id,
			Long storeId) {
		
		if (AppUtils.isBlank(ENABLE_COOKIE_SESSION)) {
            return;
        }
        if ("SESSION".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            @SuppressWarnings("unchecked")
            List<Basket> baskets = (List<Basket>) request.getSession().getAttribute(Constants.BASKET_KEY);
            Iterator<Basket> it = baskets.iterator();
            while (it.hasNext()) {
                Basket basket = it.next();
                if (prod_id.equals(basket.getProdId())) {
                    basket.setStoreId(storeId);
                }
            }
            request.getSession().setAttribute(Constants.BASKET_KEY, baskets);
        } else if ("COOKIE".equals(ENABLE_COOKIE_SESSION.toUpperCase())) {
            Cookie cookie = CookieUtil.getCookie(request, Constants.BASKET_KEY);

            if (cookie != null) {
                List<BasketCookieDto> list = null;
                try {
                    list = JSONUtil.getArray(URLDecoder.decode(cookie.getValue(), "UTF-8"), BasketCookieDto.class);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Iterator<BasketCookieDto> it = list.iterator();
                while (it.hasNext()) {
                    BasketCookieDto cookieDto = it.next();
                    Long prodId = cookieDto.getProdId();
                    Long skuId = cookieDto.getSkuId();
                    if (prod_id.equals(prodId)) {
                        if (AppUtils.isNotBlank(skuId) && skuId != 0) {
                            if (skuId.equals(sku_id)) {
                                cookieDto.setStoreId(storeId);
                            }
                        } else {
                            cookieDto.setStoreId(storeId);
                        }
                    }
                    cookieDto.setCheckSts(1);
                }
                CookieUtil.addCookie(response, COOKIE_MAX_TIME, Constants.BASKET_KEY, JSONUtil.getJson(list));
            }
        }
		
	}
}
