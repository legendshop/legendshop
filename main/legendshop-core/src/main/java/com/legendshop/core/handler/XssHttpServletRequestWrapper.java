/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.handler;

import com.legendshop.core.utils.JsoupUtil;
import com.legendshop.util.AppUtils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 过滤xss攻击
 * 
 */
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

	
    /**
     * The request parameters for this request.  This is initialized from the
     * wrapped request, but updates are allowed.
     */
    protected Map<String, String[]> parameters = null;

	public XssHttpServletRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String[] getParameterValues(String parameter) {
		return parseParameters(parameter);
	}

	@Override
	public String getParameter(String parameter) {
        String[] value =parseParameters(parameter);
        if (value == null) {
            return null;
        }
        return value[0];
	}

	@Override
	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null){
			return null;
		}
		return cleanXSS(value);

	}
	
	
	private  String[] parseParameters(String name) {
        if (parameters == null) {
        	 parameters = new HashMap<String, String[]>();
        }
        String[] values = parameters.get(name);
        if(values != null){
        	return values;
        }else{
        	values = super.getParameterValues(name);
        	if(values != null){
        		int count = values.length;
        		String[] encodedValues = new String[count];
        		for (int i = 0; i < count; i++) {

					//转换
        			encodedValues[i] = cleanXSS(values[i]);
        		}
				//放置转换好的参数
        		parameters.put(name, encodedValues);
				return encodedValues;
        	}
        }
        return null;
    }

	/**
	 * 过滤Html javascript 和sql
	 * @param value
	 * @return
	 */
	private  String cleanXSS(String value) {
		if(AppUtils.isBlank(value)){
			return value;
		}
		String cleanValue = JsoupUtil.clean(value);
		return cleanValue;
	}
    

}
