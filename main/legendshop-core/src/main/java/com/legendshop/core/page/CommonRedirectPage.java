/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 公共页面定义.
 */
public enum CommonRedirectPage implements PageDefinition {

	/** 系统默认首页 **/
	HOME_QUERY("/home"),

	/** 去往首页 **/
	INDEX_QUERY("/index"),
	
	/** 我的订单 **/
	MYORDER("/p/myorder"),

	/** 用户登录. */
	LOGIN("/login");

	/** The value. */
	private final String value;

	private CommonRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private CommonRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
