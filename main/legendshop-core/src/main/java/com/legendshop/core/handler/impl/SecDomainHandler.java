package com.legendshop.core.handler.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.core.handler.AbstractHandler;
import com.legendshop.core.handler.Handler;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SecDomainNameSetting;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;

/**
 * @Description 二级域名过滤
 * @author 关开发
 */
public class SecDomainHandler extends AbstractHandler implements Handler {
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SecDomainHandler.class);
	
	private ConstTableService constTableService; 
	
	private ShopDetailService shopDetailService;

	public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String uri = request.getRequestURI();
		if(uri.length() > 1){
			return;
		}
		//获取全局配置对象
		SecDomainNameSetting secDomainNameSetting = this.getSecDomainNameSetting();
		
		if(AppUtils.isNotBlank(secDomainNameSetting)){
			if(!secDomainNameSetting.getIsEnable()){
				//如果没有开启
				return;
			}
			
			String secDomainName = this.getSecDomainName(request);//获取二级域名
			
			if(AppUtils.isBlank(secDomainName)){
				//如果二级域名为空
				return;
			}
			
			if(secDomainNameSetting.isRetain(secDomainName)){
				//如果该二级域名是否系统保留
				return;
			}
			
			try{
				Long shopId = shopDetailService.getShopIdBySecDomain(secDomainName);//根据二级域名找到对应的shopId
				
				if(AppUtils.isNotBlank(shopId)){
					//把shopId设置到上下文
					log.debug("processing domainName {}, shopId {}", secDomainName, shopId);
					
					//采用这种方式会导致一旦进入卖家店铺首页,就再也出进入不了平台首页了
					//ThreadLocalContext.setCurrentShopName(request, response, shopId);
					request.getRequestDispatcher("/store/"+shopId).forward(request, response);
					return;
				}
			}catch(Exception e){
				//log.error("查询出错!",e);
				return;
			}
		}
	}
	
	/**
	 * 获取二级域名全局配置实体
	 * @return
	 */
	@Cacheable(value = "ConstTable", key = "'SecDomainNameSetting'")
	public SecDomainNameSetting getSecDomainNameSetting(){
		ConstTable  constTable = 
				constTableService.getConstTablesBykey("SHOP_SEC_DOMAIN_NAME", "SHOP_SEC_DOMAIN_NAME");
		
		SecDomainNameSetting secDomainNameSetting = null;
		
		if(AppUtils.isNotBlank(constTable)){
			//获取二级域名全局配置
			secDomainNameSetting = JSONObject.parseObject(constTable.getValue(), SecDomainNameSetting.class);
		}
		
		return secDomainNameSetting;
	}

	/**
	 * 获取二级域名,不能有后缀
	 * @param request
	 * @return
	 */
	public String getSecDomainName(HttpServletRequest request){
		
		String serverName  = request.getServerName();
		int endIndex = serverName.indexOf(".");
		
		String secDomainName = null;
		if(endIndex>0){
			secDomainName = serverName.substring(0, endIndex);
		}
		
		log.info("incoming serverName = {} ,secDomainName = {}", serverName, secDomainName );
		
		return secDomainName;
	}

	public void setConstTableService(ConstTableService constTableService) {
		this.constTableService = constTableService;
	}

	public void setShopDetailService(ShopDetailService shopDetailService) {
		this.shopDetailService = shopDetailService;
	}
}
