package com.legendshop.core.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.support.StandardServletMultipartResolver;

//import org.springframework.web.multipart.commons.CommonsMultipartResolver;


public class MyMultipartResolver extends StandardServletMultipartResolver {

	@Override
	public boolean isMultipart(HttpServletRequest request) {
		if(request.getRequestURI().contains("/editor/uploadJson")) {
			return false;
		} else {
			return super.isMultipart(request);
		}
	}
	
	

	
}
