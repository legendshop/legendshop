package com.legendshop.core.handler;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局过滤器
 *
 */
public interface LegendHandler {
	
	public void doHandle(boolean isSupportUrl, HttpServletRequest request, HttpServletResponse response, FilterChain chain);
	
}
