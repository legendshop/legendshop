/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.pageprovider;


import java.util.Locale;
import java.util.ResourceBundle;

import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.ToolBarProvider;


/**
 * 默认工具条
 */
public class SimplePageProviderImpl extends DefaultPagerProvider implements ToolBarProvider{
	
	public static final String LOCALE_FILE = "i18n/ApplicationResources";
	
	private String actionUrl;
	
	public SimplePageProviderImpl(long total, String curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public SimplePageProviderImpl(long total, int curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public SimplePageProviderImpl(PageSupport ps, String actionUrl) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl =actionUrl;
	}
	
	public SimplePageProviderImpl(PageSupport ps) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl = "javascript:pager";//默认的动作
	}

	@Override
	public String getToolBar() {
		if( hasMutilPage()){//有分页条
			return this.getBar(null, actionUrl);
		}else{
			return null;
		}
	}
	
	/**
	 * 前台工具条
	 */
	public String getBar(Locale locale, String url) {
		if(locale == null){
			locale = Locale.CHINA;
		}
		StringBuilder str = new StringBuilder();
		//str.append("<span class='pagerTotal'>共").append(total).append("个记录</span>");
		str.append("<span class='pages'><span class='page-panel'>");

		//1. 显示上一页
		if (isFirst()) {//位于第一页
			// 第一页不用加超链接
			str.append("<a class='item prev default' >").append("上一页").append("</a>");
		} else {
			str.append("<a class='item prev' href='").append(url).append("(").append(previous()).append(")'>")
					.append("&nbsp;").append(getString(locale, "pager.previous", "Previous")).append("&nbsp;").append("</a>");
		}
		
		//2. 显示首页
		appendItem( str,  1, url);
		
		//3. 显示当前页前后2页
		int d1 = curPageNO - 1; //当前页与第一页的间距
		int d2 = pageCount - curPageNO; //当前页与最后一页的间距
		int start = Math.max(curPageNO - 2, 2);
		int end =  Math.min(curPageNO + 2, pageCount - 1) ;
		//3.1 加入点号
		if(d1 > 3){
			appendEllipsisItem(str);
		}
		for (int i = start; i <= end; i++) {
			appendItem(str, i,url);
		}
		//3.2 加入点号
		if(d2 > 3){
			appendEllipsisItem(str);
		}
		//4. 显示尾页
		appendItem( str,  pageCount, url);
		//5. 显示下一页
		if (isLast() || (total== 0)) {
			//str.append("&nbsp;&nbsp;");
			str.append("<a class='item next default' >下一页</a>");
		} else {
			str.append("<a class='item next' href='").append(url).append("(").append(next()).append(")'>").append("下一页</a>");
		}
		
		String totalStr = "";
		if(total > 10000){//防止数据太大,除以1万
			totalStr = total/10000 +"w+";
		}else{
			totalStr = total + "";
		}
		
		str.append("<span class='go-page'><select name='page' onChange=\"")
		.append(url).append("(this.options[this.selectedIndex].value)\">");
		int beginPage = (getCurPageNO() > 10) ? getCurPageNO() - 10 : 1;
		int endPage = (getPageCount() - getCurPageNO() > 10) ? getCurPageNO() + 10 : getPageCount();
		for (int i = beginPage; i <= endPage; i++) {
			if (i == getCurPageNO()) {
				str.append("<option value='").append(i).append("' selected>").append(i).append("</option>");
			} else {
				str.append("<option value='").append(i).append("'>").append(i).append("</option>");
			}
		
		}
		str.append("</select></span>");
		str.append("<span class='total-num'>共<span class='num'>").append(totalStr).append("</span>条</span></span></div>");

//		str.append("<span class='go-page'>到第<input type='text' class='page-input'>页<a href='").append(url).append("(-1)")
//		   .append("' class='page-btn'>GO</a></span><span class='total-num'>共<span class='num'>").append(totalStr).append("</span>条</span></span></div>");

		return str.toString();
	}
	
	
	/**
	 * 加入页码
	 * @param str
	 * @param i
	 * @param url
	 */
	private void appendItem(StringBuilder str, int i,String url){
		if (i == getCurPageNO()) {
			str.append("<a href='javascript:void(0);' class='item cur'>").append(i).append("</a>");
		} else {
			str.append("<a class='item' href='").append(url).append("(").append(i).append(")'>").append(i).append("</a>");
		}
	}
	
	/**
	 * 加入省略号
	 * @param str
	 */
	private void appendEllipsisItem(StringBuilder str){
		//str.append("<span class='pagerUnselected' style='font-size: 7px'>\u2026</span>");
		str.append("<span class='ellipsis' href='javascript:void(0);' >...</span>");
	}
	
	/**
	 * 获得I18n的国际化语言
	 */
	protected String getString(Locale locale, String key, String defaultValue) {
		String value;
		try {
			if (locale != null) {
				value = ResourceBundle.getBundle(LOCALE_FILE, locale).getString(key);
			} else {
				value = ResourceBundle.getBundle(LOCALE_FILE).getString(key);
			}
			if (value == null) {
				value = defaultValue;
			}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			value = defaultValue;
		}

		return value;
	}

}
