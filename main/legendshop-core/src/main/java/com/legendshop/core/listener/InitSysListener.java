/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.listener;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysDestoryEvent;
import com.legendshop.framework.event.SysInitEvent;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.Constants;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.management.*;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 系统初始化监听器
 */
public class InitSysListener implements ServletContextListener {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(InitSysListener.class);

	/**
	 * 系统初始化.
	 * 
	 * @param event the event
	 */
	public void contextDestroyed(ServletContextEvent event) {
		log.debug("system destory start");
		EventHome.publishEvent(new SysDestoryEvent(event));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet
	 * .ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event) {
		log.info("********* Initializing LegendShop System now *************");

		PropertiesUtil propertiesUtil = PropertiesUtilManager.getPropertiesUtil();
		// 1. 初始化系统
		initSystem(event, propertiesUtil);

		// 4. 发送系统启动事件
		EventHome.publishEvent(new SysInitEvent(propertiesUtil.getPcDomainName()));

		printBeanFactory(ContextServiceLocator.getApplicationContext());
		log.info("*********  LegendShop System Initialized successful **********");
	}

	private void initSystem(ServletContextEvent event, PropertiesUtil propertiesUtil) {

		// 1. 初始化系统真实路径
		String realPath = event.getServletContext().getRealPath("/");
		if (realPath != null && !realPath.endsWith("/") && !realPath.endsWith("\\")) {
			realPath = realPath + "/";
		}
		SystemUtil.setSystemRealPath(realPath);

		ServletContext servletContext = event.getServletContext();
		// 相对路径
		String contextPath = servletContext.getContextPath();
		SystemUtil.setContextPath(contextPath);

		// 域名
		servletContext.setAttribute(Constants.PC_DOMAIN_NAME, propertiesUtil.getPcDomainName());

		log.info("*********  LegendShop System Initialized PC domain name is " + propertiesUtil.getPcDomainName());

	}

	/**
	 * 打印spring context
	 * 
	 * @param ctx
	 */
	private void printBeanFactory(ApplicationContext ctx) {
		if (log.isWarnEnabled()) {
			int i = 0;
			String[] beans = ctx.getBeanDefinitionNames();
			StringBuffer sb = new StringBuffer("系统配置的Spring Bean [ \n");
			if (AppUtils.isNotBlank(beans)) {
				for (String bean : beans) {
					sb.append(++i).append(" ").append(bean).append("\n");
				}
				sb.append(" ]");
				log.warn(sb.toString());
			}
		}

	}

	/**
	 * Tomcat启动时获取访问地址和端口号
	 * 
	 * @return
	 * @throws MalformedObjectNameException
	 * @throws NullPointerException
	 * @throws UnknownHostException
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws MBeanException
	 * @throws ReflectionException
	 */
	private List<String> getEndPoints() throws MalformedObjectNameException, NullPointerException, UnknownHostException, AttributeNotFoundException,
			InstanceNotFoundException, MBeanException, ReflectionException {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		Set<ObjectName> objs = mbs.queryNames(new ObjectName("*:type=Connector,*"), Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
		String hostname = InetAddress.getLocalHost().getHostName();
		InetAddress[] addresses = InetAddress.getAllByName(hostname);
		ArrayList<String> endPoints = new ArrayList<String>();
		for (Iterator<ObjectName> i = objs.iterator(); i.hasNext();) {
			ObjectName obj = i.next();
			String scheme = mbs.getAttribute(obj, "scheme").toString();
			String port = obj.getKeyProperty("port");
			for (InetAddress addr : addresses) {
				String host = addr.getHostAddress();
				String ep = scheme + "://" + host + ":" + port;
				endPoints.add(ep);
			}
		}
		return endPoints;
	}
}
