package com.legendshop.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.firewall.StrictHttpFirewall;

/**
 * 核心处理类
 *
 */
@Configuration
public class CoreConfiguration {
	
	@Bean
	public StrictHttpFirewall httpFirewall(){
	    StrictHttpFirewall firewall = new StrictHttpFirewall();
	    firewall.setAllowSemicolon(true);
	    return firewall;
	}
	
}
