package com.legendshop.core.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.legendshop.core.handler.XssHttpServletRequestWrapper;



/**
 * 用于搜索查询过滤XSS
 * @author tony
 *
 */
public class ProductSearchXssFilter implements Filter  {

	 FilterConfig filterConfig = null;  
	  
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
	  
	@Override
	public void destroy() {
		 this.filterConfig = null;  
	}

	@Override
	public void doFilter(ServletRequest request,
			ServletResponse response, FilterChain filterchain)
			throws IOException, ServletException {
		filterchain.doFilter(new XssHttpServletRequestWrapper( (HttpServletRequest) request), response);  
	}



}
