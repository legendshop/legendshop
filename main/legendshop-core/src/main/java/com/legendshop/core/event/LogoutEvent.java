/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.event;

import com.legendshop.base.event.EventId;
import com.legendshop.framework.event.SystemEvent;

/**
 * 注销时触发的事件.
 */
public class LogoutEvent extends SystemEvent<HttpWrapper> {

	public LogoutEvent(HttpWrapper wrapper) {
		super(EventId.LOGOUT_EVENT);
		this.setSource(wrapper);
	}

}
