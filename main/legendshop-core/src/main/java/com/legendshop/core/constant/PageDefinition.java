/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.constant;

/**
 * 页面定义.
 * 
 */
public interface PageDefinition {

	/** 前端页面 */
	public final int FRONT_PAGE = 1;

	/** 后台页面. */
	public final int BACK_PAGE = 2;

	/** tiles页面. */
	public final int TILES = 3;

	/**FOWARD 页面. */
	public final int FOWARD = 4;

	/** REDIRECT页面. */
	public final int REDIRECT = 5;

	/** 登录页面. */
	public final String LOGIN_PATH = "login.";

	/** 错误页面. */
	public final String ERROR_PAGE_PATH = "/common/error";
	
	/**错误页面. */
	public final String ERROR_FRAMEPAGE_PATH = "/common/errorFrame";

	/** 默认页面. */
	public String DEFAULT_THEME_PATH = "default";

	/**
	 * 获取当前页面定义的全路径
	 * 
	 */
	public String getValue();

	/**
	 * 获取当前页面的定义路径.
	 * 
	 */
	public String getNativeValue();

	/**
	 * 根据页面定义计算页面的全路径
	 */
	public String getValue(String path);

}
