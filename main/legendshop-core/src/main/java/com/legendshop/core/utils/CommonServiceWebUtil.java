/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.utils;

import javax.servlet.http.HttpServletRequest;

import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;

import com.legendshop.model.constant.DataSortResult;
import com.legendshop.util.AppUtils;

/**
 * for web project使用工具类
 *
 */
public class CommonServiceWebUtil {
	
	public static String RESULT ="result";

	/**
	 *
	 * 是否外部排序
	 *
	 */
	public static boolean isDataForExport(HttpServletRequest request) {
		String exportValue = request.getParameter(TableTagParameters.PARAMETER_EXPORTING);
		if (AppUtils.isNotBlank(exportValue)) {// 导出
			return true;
		}
		return false;
	}
	
	/**
	 * 是否外部排序.
	 *
	 */
	public static DataSortResult isDataSortByExternal(HttpServletRequest request) {
		String sortName = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_SORT)));
		String sortOrder = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_ORDER)));
		if(sortName != null){
			return isDataSortByExternal(sortName, sortOrder);
		}else{
			return null;
		}
		
	}
	
	/**
	 * 是否外部排序.
	 *
	 * @param request
	 *            the request
	 * @param expectedCause
	 * 	          期待的字段内容
	 * @return DataSortResult  排序的结果
	 */
	public static DataSortResult isDataSortByExternal(HttpServletRequest request, String[] expectedCause) {
		String sortName = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_SORT)));
		String sortOrder = request.getParameter((new ParamEncoder("item").encodeParameterName(TableTagParameters.PARAMETER_ORDER)));
		sortName = parseExpectedCause(sortName,expectedCause);
		if(sortName != null){
			return isDataSortByExternal(sortName, sortOrder);
		}else{
			return new DataSortResult();
		}
	}
	
	private static String parseExpectedCause(String sortName, String[] expectedCause){
		if(AppUtils.isBlank(expectedCause)){
			return null;
		}
		for (String cause : expectedCause) {
			if(cause.equals(sortName)){
				return cause;
			}
		}
		return null;
	}

	/**
	 * 是否外部排序
	 *
	 */
	private static DataSortResult isDataSortByExternal(String sortName, String sortOrder) {
		DataSortResult result = new DataSortResult();
		if (AppUtils.isNotBlank(sortName)) {
			if ("1".equals(sortOrder)) {
				result.setOrderIndicator("desc");
			} else {
				result.setOrderIndicator("asc");
			}
			result.setSortName(sortName);
			result.setSortExternal(true);
		}else{
			result.setSortExternal(false);
		}
		return result;
	}
	
	
}
