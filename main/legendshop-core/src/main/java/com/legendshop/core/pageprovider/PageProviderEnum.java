/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.pageprovider;

/**
 * 
 * 分页模板
 */
public enum PageProviderEnum {
	// //////////////////////前台有多套模板////////////////////////////////////////

	/** 前后台, 工具条 **/
	SIMPLE_PAGE_PROVIDER("simple"),

	/** 用于PC 商品列表分页 **/
	PRODUCTLIST_SIMPLEPAGE_PROVIDER("product"),

	/** 用于手机端的商品列表分页 **/
	MOBILE_PRODUCTLIST_SIMPLEPAGE_PROVIDER("mobile"),

	// ///////////////////后台只有一套模板/////////////////////////////////////

	/** 后台, 默认工具条 **/
	PAGE_PROVIDER("default"),

	/** 后台, 用于订单分页 **/
	PROCESS_ORDER_PAGEP_ROVIDER("order"),

	// /////////////////////////////////////////////////////
	/** 无需分页 **/
	NO_PAGE_PROVIDER("nopage");

	/** The value. */
	private final String value;

	/**
	 * 
	 * @param value
	 *            the value
	 */
	private PageProviderEnum(String value) {
		this.value = value;
	}

	/**
	 * 采用抽象工厂模式来获取工具条
	 */
	public String value() {
		return this.value;
	}

}
