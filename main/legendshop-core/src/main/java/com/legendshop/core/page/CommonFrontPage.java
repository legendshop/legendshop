/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;
import com.legendshop.core.helper.ThreadLocalContext;

/**
 * The Enum FrontPage.
 */
public enum CommonFrontPage implements PageDefinition {
	/** 可变路径 */
	VARIABLE(""),
	
	/** 错误页面. */
	ERROR_PAGE(ERROR_PAGE_PATH),
	
	/** 错误页面. */
	FAIL_PAGE("/failPage"), 

	//操作错误
	OPERATION_ERROR("/operationError"),
	
	/** 错误页面 **/	
	ERROR("/error"),
	
	/** 权限不足页面 **/	
	INSUFFICIENT_AUTHORITY("/error401"), 
	
	/** 商品不存在的提示 **/	
	PROD_NON_EXISTENT("/nonExitProd"),
	
	/** 登录提示 **/
	LOGIN_HINT("/login"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new front page.
	 * 
	 * @param value
	 *            the value
	 * @param template
	 *            the template
	 */
	private CommonFrontPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, java.util.List)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath(ThreadLocalContext.getFrontType(), path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
