package com.legendshop.core.event;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

/**
 * 封装了request 和 response
 *
 */
public class HttpWrapper implements Serializable{
	
	private static final long serialVersionUID = -7002332509117934288L;

	private final HttpServletRequest request;
	
	private final HttpServletResponse response;
	
	private final Authentication authentication;

	public HttpWrapper(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		this.request = request;
		this.response = response;
		this.authentication = authentication;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public Authentication getAuthentication() {
		return authentication;
	}

}
