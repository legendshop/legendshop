/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.randing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.legendshop.util.AppUtils;

/**
 * 图片验证码
 *
 */
@Controller
public class ValidImgController {
	
	/**
	 * 出现图片验证码的地方
	 * 要把validCoder.random换为validCoderRandom. 否则session会出现问题
	 * @param request
	 * @param response
	 * @param source 默认传app, 如果有传, 说明是前后端分离的架构使用验证码, 比如小程序是没有cookie的, 做不了会话保持. 验证码要放redis
	 */
	@GetMapping("/validCoderRandom")
	public void validCoder(HttpServletRequest request, HttpServletResponse response, String source) {
		response.setContentType("image/jpeg");//设置相应类型,告诉浏览器输出的内容为图片
        response.setHeader("Pragma", "No-cache");//设置响应头信息，告诉浏览器不要缓存此内容
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expire", 0);
        ValidImgRander validImgRander = new ValidImgRander();
        try {
        	//如果有传, 说明是前后端分离的架构使用验证码, 比如小程序是没有cookie的, 做不了会话保持. 验证码要放redis
        	if(AppUtils.isBlank(source)){
        		validImgRander.produceRandcodeToSession(request, response);
        	}else {
        		validImgRander.produceRandcodeToCache(response);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
