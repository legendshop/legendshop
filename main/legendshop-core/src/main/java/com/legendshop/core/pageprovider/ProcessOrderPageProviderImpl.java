/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.pageprovider;

import java.util.Locale;

import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.ToolBarProvider;


/**
 * 处理中的订单工具条
 */
public class ProcessOrderPageProviderImpl extends DefaultPagerProvider implements ToolBarProvider{
	
	
	private String actionUrl;
	
	public ProcessOrderPageProviderImpl(long total, String curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public ProcessOrderPageProviderImpl(long total, int curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public ProcessOrderPageProviderImpl(PageSupport ps, String actionUrl) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl =actionUrl;
	}
	
	public ProcessOrderPageProviderImpl(PageSupport ps) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl = "javascript:pager";//默认的动作
	}

	
	@Override
	public String getToolBar() {
		if( hasMutilPage()){//有分页条
			return this.getBar(null, actionUrl);
		}else{
			return null;
		}
	}
	
	/**
	 * 前台工具条
	 */
	
	public String getBar(Locale locale, String url) {
		if(locale == null){
			locale = Locale.CHINA;
		}
		StringBuilder str = new StringBuilder();
		str.append("<div class='pagination'><ul>");
		if (isFirst()) {
			str.append("<li><span>首页</span></li>");
			str.append("<li><span>上一页</span></li>");
		} else {
			str.append("<li>").append(" <a class='demo' href='").append(url).append("(").append(1).append(")'>").append("<span>首页</span>").append("</a>").append("</li>");
			str.append("<li>").append(" <a class='demo' href='").append(url).append("(").append(previous()).append(")'>").append("<span>上一页</span>").append("</a>").append("</li>");
		}
		appendItem( str,  1, url);
		int d1 = curPageNO - 1; //当前页与第一页的间距
		int d2 = pageCount - curPageNO; //当前页与最后一页的间距
		int start = Math.max(curPageNO - 2, 2);
		int end =  Math.min(curPageNO + 2, pageCount - 1) ;
		//3.1 加入点号
		if(d1 > 3){
			appendEllipsisItem(str);
		}
		for (int i = start; i <= end; i++) {
			appendItem(str, i,url);
		}
		//3.2 加入点号
		if(d2 > 3){
			appendEllipsisItem(str);
		}
		appendItem( str,  pageCount, url);
		//5. 显示下一页
		if (isLast() || (total== 0)) {
			
			str.append("<li><span>下一页</span></li>");
			str.append("<li><span>末页</span></li>");
		} else {
			str.append("<li>").append(" <a class='demo' href='").append(url).append("(").append(next()).append(")'>").append("<span>下一页</span>").append("</a>").append("</li>");
			str.append("<li>").append(" <a class='demo' href='").append(url).append("(").append(pageCount).append(")'>").append("<span>末页</span>").append("</a>").append("</li>");
		}
		str.append("</ul></div>");
		
		return str.toString();
	}
	
	/**
	 * 加入页码
	 * @param str
	 * @param i
	 * @param url
	 */
	private void appendItem(StringBuilder str, int i,String url){
		if (i == getCurPageNO()) {
		    str.append("<li><span class='currentpage'>").append(i).append("</span></li>");
		    
		} else {
			str.append("<li><a class='demo'  href='").append(url).append("(").append(i).append(")'>").append("<span>").append(i).append("</span>").append("</a></li>");
		}
		
	}
	
	/**
	 * 加入省略号
	 * @param str
	 */
	private void appendEllipsisItem(StringBuilder str){
		str.append("<li><span >...</span></li>");
	}

}
