package com.legendshop.core.listener;

import com.legendshop.framework.plugins.PluginClassLoader;

/**
 * classloader加载器
 * @author leo
 *
 */
public class PluginClassLoaderFactory {
	
	private static PluginClassLoaderFactory instance = null;
	
	private PluginClassLoader loader;
	
	public static PluginClassLoaderFactory getInstance(){
		if(instance == null){
			synchronized (PluginClassLoaderFactory.class) {
				if(instance == null){
					instance = new PluginClassLoaderFactory();
				}
			}
		}
		
		return instance;
	}

	public PluginClassLoader getLoader() {
		return loader;
	}

	public void setLoader(PluginClassLoader loader) {
		this.loader = loader;
	}
	
	

}
