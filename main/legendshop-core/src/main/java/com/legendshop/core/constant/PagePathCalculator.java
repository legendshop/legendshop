/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.core.constant;

import com.legendshop.base.constants.PagesParameterEnum;

/**
 * 用于计算页面的位置.
 */
public class PagePathCalculator {

    /**
     * 后台文件路径，放在admin插件中
     */
    private static String backEndPath = "/plugins/" + PagesParameterEnum.BACKEND_TEMPLET.getValue();

    /**
     * 计算后台页面所在位置.
     */
    public static String calculateBackendPath(String pathValue) {
        return backEndPath + pathValue;
    }

    /**
     * 计算插件的后台路径.
     */
    public static String calculatePluginBackendPath(String pluginName, String pathValue) {
        return new StringBuilder("/plugins/").append(pluginName).append(pathValue).toString();
    }

    /**
     * 计算插件的前台路径
     */
    public static String calculatePluginFronendPath(String pluginName, String pathValue) {
        return new StringBuilder("/plugins/").append(pluginName).append(pathValue).toString();
    }


    /**
     * 计算手机端插件的前台路径
     */
    public static String calculateMobilePluginFronendPath(String pluginName, String pathValue) {
        return new StringBuilder("/plugins/").append(pluginName).append(pathValue).toString();
    }

    /**
     * 计算手机端前端页面的路径.
     */
    public static String calculateMobileFronendPath(String pathValue) {
        return "/frontend/red"  + pathValue;

    }


    /**
     * 计算Action的路径
     *
     * @param actionType the action type
     * @param pathValue  the path value
     * @return the string
     */
    public static String calculateActionPath(String actionType, String pathValue) {
        return actionType + pathValue;
    }

}
