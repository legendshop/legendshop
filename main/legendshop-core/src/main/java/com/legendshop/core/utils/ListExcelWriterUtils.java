/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.utils;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.legendshop.util.DateUtil;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.model.constant.Constants;
import com.legendshop.util.DateUtils;

/**
 * Excel模版导出中心
 * @author tony
 * 
 */
public class ListExcelWriterUtils<T> {

	private final static Logger LOGGER = LoggerFactory.getLogger(ListExcelWriterUtils.class);

	private List<Integer> columnWidthList = new ArrayList<Integer>();

	private List<CellStyle> titleStyle = new ArrayList<CellStyle>();
	
	private List<CellStyle> rowStyle = new ArrayList<CellStyle>();

	private List<RichTextString> titleText = new ArrayList<RichTextString>();

	private List<String> dataTemplata = new ArrayList<String>();

	int defaultColumnWidth;

	short defaultRowHeight;
	
	private static Pattern pattern = Pattern.compile("\\{\\=(.+?)\\}");
	
	private String path;
	

	/**
	 * Instantiates a new list excel writer utils.
	 * 
	 * @param path
	 *            the path
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public ListExcelWriterUtils(String path)  {
	    this.path=path;
	}
	
	public void install() throws Exception{
		
		FileInputStream fileInputStream = new FileInputStream(path);
		// 声明一个工作薄
		Workbook workbook =null;
        if (isExcel2003(path))  
        {  
        	workbook = new HSSFWorkbook(fileInputStream);  
        }  
        else if (isExcel2007(path)) 
        {  
        	workbook = new XSSFWorkbook(fileInputStream);  
        } else{
        	throw new IllegalArgumentException("模版格式不正确");
        }

		Sheet sheet = workbook.getSheetAt(0);
		

		Row titleRow = sheet.getRow(0);
		defaultColumnWidth = sheet.getDefaultColumnWidth();
		defaultRowHeight = sheet.getDefaultRowHeight();

		Iterator<Cell> cellIterator = titleRow.cellIterator();
		for (int i = 0; cellIterator.hasNext(); i++) {
			Cell titleCell = cellIterator.next();
			titleStyle.add(titleCell.getCellStyle());
			columnWidthList.add(sheet.getColumnWidth(i));
			titleText.add(titleCell.getRichStringCellValue());
		}

		Row dataRow = sheet.getRow(1);
		cellIterator = dataRow.cellIterator();
		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			rowStyle.add(cell.getCellStyle());
			dataTemplata.add(cell.getStringCellValue());
		}
		
	}
	
	
	public String validateExcel()  
    {  
  
        /** 检查文件名是否为空或者是否是Excel格式的文件 */  
  
        if (path == null || !(isExcel2003(path) || isExcel2007(path)))  
        {  
            return "文件名不是excel格式";
        }  
  
        /** 检查文件是否存在 */  
        File file = new File(path);  
  
        if (file == null || !file.exists())  
        {  
            return "文件不存在";
        }  
        return Constants.SUCCESS;  
    }  
  
	
	public  boolean isExcel2003(String filePath)  
    {  
  
        return filePath.matches("^.+\\.(?i)(xls)$");  
  
    } 
	
	  public  boolean isExcel2007(String filePath)  
    {  
  
        return filePath.matches("^.+\\.(?i)(xlsx)$");  
  
    } 
	
	

	public boolean fillToFile(List<T> dataset, String xlsxoutput,Object obj) {

	
		// 声明一个工作薄
		Workbook workbook = null;
		if (isExcel2003(xlsxoutput)) {
			workbook = new HSSFWorkbook();
		} else {
			workbook = new XSSFWorkbook();
		}
		Sheet sheet = workbook.createSheet();
		// 生成一个表格
		// 设置表格默认列宽度为15个字节
		sheet.setDefaultColumnWidth(defaultColumnWidth);
		sheet.setDefaultRowHeight(defaultRowHeight);

		for (int i = 0; i < columnWidthList.size(); i++) {
			sheet.setColumnWidth(i, (int) (columnWidthList.get(i) * 1.15));
		}
		Row titleRow = sheet.createRow(0);
		// 产生表格标题行
		List<CellStyle> rowStyles = new ArrayList<CellStyle>();

		/**
		 * TODO 2007格式的颜色样式赋值 没有调试通过 03格式就可以通过
		 */
		for (int i = 0; i < titleStyle.size(); i++) {
			Cell cell = titleRow.createCell(i);
    		CellStyle style = workbook.createCellStyle();
			style.cloneStyleFrom(titleStyle.get(i));
			cell.setCellStyle(style);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(titleText.get(i));
			
    		CellStyle astyle = workbook.createCellStyle();
    		astyle.cloneStyleFrom(rowStyle.get(i));
    		rowStyles.add(astyle);
		}

		for (int j = 0; j < dataset.size(); j++) {
			fillRows(sheet, dataset.get(j), rowStyles, j);
		}
		FileOutputStream fileOut = null;
		try {

            File file = new File(xlsxoutput);
            //父目录不存在则创建
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
            }
			fileOut = new FileOutputStream(file);
			workbook.write(fileOut);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage(), e);
					return false;
				}
			}
		}
		return true;

	}

	@Deprecated
	private Map<String, String> reflectObj(Object t) {
		// 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
		Field[] fields = t.getClass().getDeclaredFields();
		Map<String, String> map = new HashMap<String, String>();
		for (short i = 0; i < fields.length; i++) {
			try {
				Field field = fields[i];
				field.setAccessible(true);
				String fieldName = field.getName();
				// 过滤内容为空的
				if (field.get(t) == null) {
					continue;
				}
				Object value = field.get(t);
				String textValue = null;
				if (value instanceof Date) {
					Date date = (Date) value;
					textValue = DateUtil.DateToString(date,"yyyy-MM-dd HH:mm:ss");
				}
				textValue = String.valueOf(value);
				map.put(fieldName, textValue);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return map;
	}

	private void fillRows(Sheet sheet, Object rowData,
			List<CellStyle> rowStyles, int rowNum) {
		JXPathContext objectContext = JXPathContext.newContext(rowData);
		Row row=sheet.createRow(1 + rowNum);
		for(int i = 0; i < rowStyle.size(); i ++){
			Cell cell = row.createCell(i);
			cell.setCellStyle(rowStyles.get(i));
			String templateCellValue = dataTemplata.get(i);
			Matcher matcher = pattern.matcher(templateCellValue);
			Map<String, Object> replacerMap = new HashMap<String, Object>();
			while(matcher.find()){
				String xpath = matcher.group(1);
				Object valueObj = null;
				try{
					System.out.println(objectContext.getValue(xpath)+"------------------------------");
					valueObj = objectContext.getValue(xpath);
				}catch(JXPathNotFoundException e){
					//forget it
				}
				replacerMap.put(matcher.group(0), valueObj == null ? "" : valueObj);
			}
			
			for(Entry<String, Object> entry: replacerMap.entrySet()){
				templateCellValue = templateCellValue.replace(entry.getKey(), entry.getValue().toString());
			}
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue(templateCellValue);
		}

	}

	private XSSFCellStyle getXssfCellStyle1(XSSFWorkbook workbook) {
		// 生成一个样式
		XSSFCellStyle style = workbook.createCellStyle();
		// 设置这些样式
		style.setFillForegroundColor(new XSSFColor(new Color(30, 130, 180)));
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		XSSFFont font = createFont(workbook, (short) 16);
		style.setFont(font);

		return style;
	}

	/**
	 * 创建字体
	 * 
	 * @param workbook
	 * @param size
	 *            字体大小
	 * @return
	 */
	private static XSSFFont createFont(XSSFWorkbook workbook, short size) {
		XSSFFont font = workbook.createFont();
		// 字体样式
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		// 字体颜色
		font.setColor(XSSFFont.COLOR_NORMAL);
		// 字体大小
		if (0 == size) {
			font.setFontHeightInPoints(XSSFFont.DEFAULT_FONT_SIZE);
		} else {
			font.setFontHeightInPoints(size);
		}
		font.setFontName("微软雅黑");
		return font;
	}

}
