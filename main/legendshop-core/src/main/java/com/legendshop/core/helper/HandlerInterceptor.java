/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class HandlerInterceptor extends HandlerInterceptorAdapter {//此处一般继承HandlerInterceptorAdapter适配器即可  
	
    @Override  
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
       // System.out.println("=========== preHandle : " + request.getRequestURI()  + "   =  "+ handler);  
    	//System.out.println("_csrf preHandle ==================== "+request.getParameter("_csrf"));
        return true;  
    }  
    
    @Override  
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {  
        //System.out.println("===========HandlerInterceptor1 postHandle :" + request.getRequestURI());  
    }  
    
    @Override  
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
       //System.out.println("===========HandlerInterceptor1 afterCompletion: " + request.getRequestURI());  
    }  
}  