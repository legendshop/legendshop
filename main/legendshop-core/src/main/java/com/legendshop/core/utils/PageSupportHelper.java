package com.legendshop.core.utils;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.dao.support.PageSupport;

/**
 * 设置工具条翻页信息
 * 
 */
public class PageSupportHelper {

	/** 记录的位置 **/
	public static String OFFSET = "offset";

	/** 数据 **/
	public static String LIST = "list";

	/** 当前页 **/
	public static String CURRENT_PAGE_NO = "curPageNO";

	/** 总页码 **/
	public static String PAGE_COUNT = "pageCount";

	/** 每页大小 **/
	public static String PAGE_SIZE = "pageSize";

	/** 总记录数 **/
	public static String TOTAL = "total";

	/**
	 * 设置进入request
	 * 
	 * @param request
	 * @param ps 返回的结果集
	 */
	public static void savePage(HttpServletRequest request, PageSupport<?> ps) {
		if(ps == null){
			throw new BusinessException("Page Support can not be null");
		}
		request.setAttribute(OFFSET, ps.getOffset() + 1);
		request.setAttribute(LIST, ps.getResultList());
		request.setAttribute(CURRENT_PAGE_NO, ps.getCurPageNO());
		request.setAttribute(PAGE_COUNT, ps.getPageCount());
		request.setAttribute(PAGE_SIZE, ps.getPageSize());
		request.setAttribute(TOTAL, ps.getTotal());
	}

	/**
	 * 如果一个页面有多个分页则可以加上名字加以区分 设置进入request
	 * 
	 * @param request
	 * @param ps
	 *            后台返回的结果
	 * @param name
	 *            名字
	 */
	public static void savePage(HttpServletRequest request, PageSupport<?> ps, String name) {
		request.setAttribute(OFFSET + name, ps.getOffset() + 1);
		request.setAttribute(LIST + name, ps.getResultList());
		request.setAttribute(CURRENT_PAGE_NO + name, ps.getCurPageNO());
		request.setAttribute(PAGE_COUNT + name, ps.getPageCount());
		request.setAttribute(PAGE_SIZE + name, ps.getPageSize());
		request.setAttribute(TOTAL + name, ps.getTotal());
	}
}
