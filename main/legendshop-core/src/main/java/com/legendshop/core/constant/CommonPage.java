/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.constant;

/**
 * The Enum FrontPage.
 */
public enum CommonPage implements PageDefinition {

	/** The error page. */
	ERROR_PAGE(ERROR_PAGE_PATH), 
	
	/** The error frame page. 以404错误为主，包括页面上下部分框架*/
	ERROR_FRAME_PAGE(ERROR_FRAMEPAGE_PATH), 
	
	/**图片空间*/
	IMAGE_LIST_QUERY("/common/images/images"),
	
	/**图片控件*/
	IMAGE_PROP_QUERY("/common/images/imagesProp"),
	
	/**图片控件*/
	IMAGE_REMOTE_QUERY("/common/images/remoteImages"),
	
	/**视频空间*/
	VIDEO_LIST_QUERY("/common/video/videos"),
	
	/**视频控件*/ 
	VIDEO_PROP_QUERY("/common/video/videosProp"),
	
	/**视频控件*/
	VIDEO_REMOTE_QUERY("/common/video/remoteVideos");

	/** The value. */
	private String value;

	/**
	 * Instantiates a new common page.
	 *
	 * @param value the value
	 */
	private CommonPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/**
	 * 计算公共页面的位置
	 */
	public String getValue(String path) {
		return path;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
