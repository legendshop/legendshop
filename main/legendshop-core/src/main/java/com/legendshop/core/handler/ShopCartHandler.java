/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.BasketCookieDto;
import com.legendshop.model.entity.Basket;

/**
 * 购物车登录前处理器.
 * @author tony
 */
public interface ShopCartHandler {
	

	List<Basket> getShopCart(HttpServletRequest request);

	/**
	 * 删除购物车信息
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 */
	void deleteShopCartByProdId(HttpServletRequest request,
			HttpServletResponse response, Long prodId, Long skuId);

	/**
	 * 存入购物车
	 * @param request
	 * @param response
	 * @param baskets
	 */
	void setShopCart(HttpServletRequest request, HttpServletResponse response,
			List<Basket> baskets);

	/**
	 * 清空购物车数据
	 * @param request
	 * @param response
	 */
	void clearShopCarts(HttpServletRequest request, HttpServletResponse response);

	/**
	 * 更新购物车的商品数量信息
	 * @param request
	 * @param response
	 * @param prod_id
	 * @param sku_id
	 * @param num
	 */
	void changeShopCartNum(HttpServletRequest request,
			HttpServletResponse response, Long prod_id, Long sku_id, Integer num);

	
	public int getShopCartAllCount(HttpServletRequest request) ;

	/**
	 * 加入购物车

	 * @param prodId 商品ID
	 * @param skuId  单品ID
	 * @param count  数量
	 * @param shpoId 店铺ID
	 * @param distUserName 分销用户 
	 * @param skuStock  单品虚拟库存
	 * @param Long storeId 门店ID
	 */
	String addShopCart(HttpServletRequest request, HttpServletResponse response,
			Long prodId, Long skuId, int count, Long shpoId,String distUserName, Long skuStock,Long storeId);

	/**
	 *  改变 单个购物车项 的选中状态
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 * @param checkSts
	 */
	void updateBasketChkSts(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId, int checkSts);


	/**
	 *  改变多个购物车项 的选中状态
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 * @param checkSts
	 */
	void updateBasketChkSts(HttpServletRequest request, HttpServletResponse response,List<BasketCookieDto> basketCookieList);

	/**
	 * 更改购物车已选门店
	 * @param productId
	 * @param skuId
	 * @param storeId
	 */
	void changeShopStore(HttpServletRequest request, HttpServletResponse response, Long productId, Long skuId,
			Long storeId);
}
