/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.pageprovider;

import java.util.Locale;
import java.util.ResourceBundle;

import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.ToolBarProvider;
import com.legendshop.util.SystemUtil;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 * 
 * 默认的后台工具条,不同的系统需要改写这个实现
 */
public class PageProviderImpl extends DefaultPagerProvider implements ToolBarProvider{
	
	public static final String LOCALE_FILE = "i18n/ApplicationResources";
	
	private String actionUrl;
	
	private String path = null;

	private String imgPrefix = "/resources/common/toobar";
	
	public PageProviderImpl(long total, String curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public PageProviderImpl(long total, int curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public PageProviderImpl(PageSupport ps, String actionUrl) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl =actionUrl;
	}
	
	public PageProviderImpl(PageSupport ps) {
		super(ps.getTotal(), ps.getCurPageNO(), ps.getPageSize());
		this.actionUrl = "javascript:pager";//默认的动作
	}

	@Override
	public String getToolBar() {
		if( hasMutilPage()){//有分页条
			return this.getBar(null, actionUrl);
		}else{
			return null;
		}
	}

	/**
	 * 后台的工具条
	 */
	public String getBar(Locale locale, String url) {
		String path = getPath();
		StringBuilder str = new StringBuilder();
		if (isFirst()) {
			//第一页不可用
			str.append("<img src=\"").append(path).append("/firstPageDisabled.gif\"><img src=\"").append(path)
					.append("/prevPageDisabled.gif\">&nbsp;");
		} else {
			//第一页
			str.append("<a data-type='pager' data-value='1' href='").append(url).append("(1)'><img src=\"").append(path)
			.append("/firstPage.gif\"></a>&nbsp;");
			
			//上一页
			int previous = previous();
			String prefix = "<a data-type='pager' data-value='"+ previous + "' href='";
			str.append(prefix).append(url).append("(")
					.append(previous).append(")'><img src=\"").append(path).append("/prevPage.gif\"></a>&nbsp;");
		}
		//最后一页不可用
		if (isLast() || (total == 0)) {
			str.append("<img src=\"").append(path).append("/nextPageDisabled.gif\"> <img src=\"").append(path)
					.append("/lastPageDisabled.gif\">&nbsp;");
		} else {
			
			//下一页
			int next = next();
			String prefix = "<a data-type='pager' data-value='"+ next + "' href='";
			str.append(prefix).append(url).append("(").append(next).append(")'><img src=\"").append(path).append("/nextPage.gif\"></a>&nbsp;");
			
			//最后一页
			int pageCount = getPageCount();
			str.append("<a data-type='pager' data-value='").append(pageCount).append("' href='").append(url).append("(")
					.append(pageCount).append(")'><img src=\"").append(path).append("/lastPage.gif\"></a>&nbsp;");
		}
		
		//共几页，并提供下拉控件，例如 第 1 - 15 共  85 条记录
		str.append(getString(locale, "pager.from", "  From ")).append("<b>").append(getOffset() + 1)
				.append("</b>").append(getString(locale, "pager.to", " To ")).append("<b>")
				.append(getNextPageOffset()).append("</b>");
		str.append(getString(locale, "pager.total", ", Total ")).append("<b>").append(total)
				.append("</b>").append(getString(locale, "pager.items", " Items "));
		str.append("&nbsp;<img src=\"").append(path).append("/gotoPage.gif\">&nbsp;<select name='page' onChange=\"")
				.append(url).append("(this.options[this.selectedIndex].value)\">");
		int begin = (getCurPageNO() > 10) ? getCurPageNO() - 10 : 1;
		int end = (getPageCount() - getCurPageNO() > 10) ? getCurPageNO() + 10 : getPageCount();
		for (int i = begin; i <= end; i++) {
			if (i == getCurPageNO()) {
				str.append("<option value='").append(i).append("' selected>").append(i).append("</option>");
			} else {
				str.append("<option value='").append(i).append("'>").append(i).append("</option>");
			}

		}
		str.append("</select>");
		return str.toString();
	}
	
	
	public void setImgPrefix(String imgPrefix) {
		this.imgPrefix = imgPrefix;
	}

	//计算图片的前缀
	private String getPath(){
		if(path == null){
			path =  SystemUtil.getContextPath() + imgPrefix;
		}
		return path;
	}
	
	/**
	 * 获得I18n的国际化语言
	 */
	protected String getString(Locale locale, String key, String defaultValue) {
		String value;
		try {
			if (locale != null) {
				value = ResourceBundle.getBundle(LOCALE_FILE, locale).getString(key);
			} else {
				value = ResourceBundle.getBundle(LOCALE_FILE).getString(key);
			}
			if (value == null) {
				value = defaultValue;
			}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			value = defaultValue;
		}

		return value;
	}


}
