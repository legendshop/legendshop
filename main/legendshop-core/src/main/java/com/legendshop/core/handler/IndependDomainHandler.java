package com.legendshop.core.handler;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.spi.service.ShopService;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 
 * 独立域名
 * 此处是用xml配置,所以就不用配置类级别的annotation
 */
public class IndependDomainHandler extends AbstractHandler implements Handler {
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(IndependDomainHandler.class);
	
	@Autowired
	private ShopService shopService;
	

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (systemParameterUtil.getIndependDonain()) {
			String serverName = request.getServerName();
			// String uri = request.getRequestURI();
			log.info("incoming serverName = {} ,remoteAddr = {},   X-Real-IP = {}",
					new Object[] { serverName, request.getRemoteAddr(), request.getHeader("X-Real-IP") });
			String domainName = resolveDomainName(serverName);
			if (AppUtils.isNotBlank(domainName)) {
				Long shopId = shopService.getShopIdByDomain(domainName);
				if (AppUtils.isNotBlank(shopId)) {
					log.debug("processing domainName {}, shopId {}", domainName, shopId);
					//ThreadLocalContext.setCurrentShopId(request, response, shopId);
				}
			}
		}
	}

	private String resolveDomainName(String serverName) {
		if (AppUtils.isBlank(serverName)) {
			return null;
		}
		if (serverName.toLowerCase().startsWith("www.")) {
			return serverName.substring(4);
		}
		return serverName;
	}




}
