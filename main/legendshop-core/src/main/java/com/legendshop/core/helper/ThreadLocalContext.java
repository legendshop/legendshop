/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.helper;

import com.legendshop.base.constants.PagesParameterEnum;

/**
 * 系统运行上下文.
 */
public class ThreadLocalContext {

	/**
	 * 得到前台模版 ,
	 * 1. 先从request中获取
	 * 2. 再从session获取，获取成功后并设置到request中
	 * 3. 如果request和session都没有设置则从配置文件中获取
	 * 
	 * 看配置：common.properties FRONTEND_TEMPLET
	 */
	public static String getFrontType() {
		return PagesParameterEnum.FRONTEND_TEMPLET.getValue();
	}
	
}
