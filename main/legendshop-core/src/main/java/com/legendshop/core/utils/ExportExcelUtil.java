package com.legendshop.core.utils;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.legendshop.util.DateUtils;
import com.legendshop.model.dto.TenderSkuImportDTO;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;  
  
public class ExportExcelUtil<T>  
{  
    public void exportExcelUtil(Collection<T> dataset, OutputStream out)  
    {  
        exportExcelUtil("测试POI导出EXCEL文档", null, dataset, out, "yyyy-MM-dd");
    }  
  
    public void exportExcelUtil(String[] headers, Collection<T> dataset,  
            OutputStream out)  
    {  
        exportExcelUtil("测试POI导出EXCEL文档", headers, dataset, out, "yyyy-MM-dd HH:mm:ss");
    }  
  
    public void exportExcelUtil(String[] headers, Collection<T> dataset,  
            OutputStream out, String pattern)  
    {  
        exportExcelUtil("测试POI导出EXCEL文档", headers, dataset, out, pattern);  
    }  
    
    public void exportSubExcelUtil(String[] headers, Collection<T> dataset,  
            OutputStream out)  
    {  
    	exportSubExcelUtil("测试POI导出EXCEL文档", headers, dataset, out, "yyyy-MM-dd HH:mm:ss");  
    }

	public void exportProdExcelUtil(String[] headers, Collection<T> dataset,
									OutputStream out)
	{
		exportProdExcelUtil("商品导出EXCEL文档", headers, dataset, out, "yyyy-MM-dd HH:mm:ss");
	}
    
    @SuppressWarnings("unchecked")  
    public void exportSubExcelUtil(String title, String[] headers, Collection<T> dataset, OutputStream out, String pattern){  
        // 声明一个工作薄  
        HSSFWorkbook workbook = new HSSFWorkbook();  
        // 生成一个表格  
        HSSFSheet sheet = workbook.createSheet(title);  
        // 设置表格默认列宽度为15个字节  
        sheet.setDefaultColumnWidth((short) 20);  
        // 生成一个样式  
        HSSFCellStyle style = workbook.createCellStyle();  
        // 设置这些样式  
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);  
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);  
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
        // 生成一个字体  
        HSSFFont font = workbook.createFont();  
        font.setColor(HSSFColor.VIOLET.index);  
        font.setFontHeightInPoints((short) 12);  
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
        // 把字体应用到当前的样式  
        style.setFont(font);  
        // 生成并设置另一个样式  
        HSSFCellStyle style2 = workbook.createCellStyle();  
        style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);  
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);  
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  
        style2.setWrapText(true);
        // 生成另一个字体  
        HSSFFont font2 = workbook.createFont();  
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);  
        // 把字体应用到当前的样式  
        style2.setFont(font2);  
  
        // 声明一个画图的顶级管理器  
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();  
        // 定义注释的大小和位置,详见文档  
        HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,  
                0, 0, 0, (short) 4, 2, (short) 6, 5));  
        // 设置注释内容  
        comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));  
        // 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.  
        comment.setAuthor("");  
  
        // 产生表格标题行  
        HSSFRow row = sheet.createRow(0);  
        for (short i = 0; i < headers.length; i++){  
            HSSFCell cell = row.createCell(i);  
            cell.setCellStyle(style);  
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }  
  
        // 遍历集合数据，产生数据行  
        Iterator<T> it = dataset.iterator();  
        int index = 0;  
        String lastSubNumber = null;
        Integer count = 0;
        while (it.hasNext()){  
            index++;  
            row = sheet.createRow(index);  
            T t = (T) it.next();  
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值  
            Field[] fields = t.getClass().getDeclaredFields();  
            for (short i = 0; i < fields.length; i++){  
                HSSFCell cell = row.createCell(i);  
                cell.setCellStyle(style2);  
                Field field = fields[i];  
                String fieldName = field.getName();  
                String getMethodName = "get"  + fieldName.substring(0, 1).toUpperCase()  + fieldName.substring(1);  
                try {
					Class tCls = t.getClass();
					Method getMethod = tCls.getMethod(getMethodName, new Class[]{});
					Object value = getMethod.invoke(t, new Object[] {});
					//判断是否同一订单，合并单元格
					if(index==1 && i==0){ //导出第一行时，保存订单号
						lastSubNumber = value.toString();
					}else { //从第二行开始比较，判断是否合并
						if(i==0 && lastSubNumber.equals(value.toString())){
							count++;
						}
						if(i==0){
							if (!lastSubNumber.equals(value.toString()) || index == dataset.size()){
								if(count>0){
									int num = 0;
									if (index == dataset.size()){
										num = index;
										index++;
									}
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 0 ,0));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 1 ,1));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 2 ,2));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 3 ,3));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 4 ,4));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 5 ,5));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 6 ,6));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 8 ,8));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 9 ,9));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 10 ,10));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 11 ,11));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 12 ,12));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 13 ,13));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 14 ,14));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 15 ,15));
									//sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 18 ,18));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 19 ,19));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 20 ,20));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 25 ,25));
									sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 26 ,26));
									/*sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 21, 21));*/
									count = 0;
									if (num > 0){
										index = num;
									}
								}
							}
						}
                    	if(i==0){
                    		lastSubNumber = value.toString();
                    	}
                    }
                    // 判断值的类型后进行强制类型转换  
                    String textValue = null;   
                    if (value instanceof Boolean)  
                    {  
                        boolean bValue = (Boolean) value;  
                        textValue = "男";  
                        if (!bValue)  
                        {  
                            textValue = "女";  
                        }  
                    }  
                    else if (value instanceof Date)  
                    {  
                        Date date = (Date) value;  
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);  
                        textValue = sdf.format(date);  
                    }  
                    else if (value instanceof byte[])  
                    {  
                        // 有图片时，设置行高为60px;  
                        row.setHeightInPoints(60);  
                        // 设置图片所在列宽度为80px,注意这里单位的一个换算  
                        sheet.setColumnWidth(i, (short) (35.7 * 80));  
                        // sheet.autoSizeColumn(i);  
                        byte[] bsValue = (byte[]) value;  
                        HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0,  
                                1023, 255, (short) 6, index, (short) 6, index);  
                        anchor.setAnchorType(2);  
                        patriarch.createPicture(anchor, workbook.addPicture(  
                                bsValue, HSSFWorkbook.PICTURE_TYPE_JPEG));  
                    }  
                    else 
                    {  
                    	if(value!=null){
                    		// 其它数据类型都当作字符串简单处理  
                    		textValue = value.toString();  
                    	}
                    }  
                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成  
                    if (textValue != null)  
                    {  
                        Pattern p = Pattern.compile("^//d+(//.//d+)?$");  
                        Matcher matcher = p.matcher(textValue);  
                        if (matcher.matches())  
                        {  
                            // 是数字当作double处理  
                            cell.setCellValue(Double.parseDouble(textValue));  
                        }  
                        else  
                        {  
                            cell.setCellValue(textValue);  
                        }  
                    }  
                    
                }  
                catch (SecurityException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (NoSuchMethodException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (IllegalArgumentException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (IllegalAccessException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (InvocationTargetException e)  
                {  
                    e.printStackTrace();  
                }  
                finally  
                {  
                    // 清理资源  
                }  
            }  
        }  
        try  
        {  
        	workbook.write(out);
			out.flush();
			out.close();  
        }  
        catch (IOException e)  
        {  
            e.printStackTrace();  
        }  
    }


	@SuppressWarnings("unchecked")
	public void exportProdExcelUtil(String title, String[] headers, Collection<T> dataset, OutputStream out, String pattern){
		// 声明一个工作薄
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 生成一个表格
		HSSFSheet sheet = workbook.createSheet(title);
		// 设置表格默认列宽度为15个字节
		sheet.setDefaultColumnWidth((short) 20);
		// 生成一个样式
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置这些样式
		style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 生成一个字体
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.VIOLET.index);
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 把字体应用到当前的样式
		style.setFont(font);
		// 生成并设置另一个样式
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		style2.setWrapText(true);
		// 生成另一个字体
		HSSFFont font2 = workbook.createFont();
		font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style2.setFont(font2);

		// 声明一个画图的顶级管理器
		HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
		// 定义注释的大小和位置,详见文档
		HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,
				0, 0, 0, (short) 4, 2, (short) 6, 5));
		// 设置注释内容
		comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));
		// 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.
		comment.setAuthor("");

		// 产生表格标题行
		HSSFRow row = sheet.createRow(0);
		for (short i = 0; i < headers.length; i++){
			HSSFCell cell = row.createCell(i);
			cell.setCellStyle(style);
			HSSFRichTextString text = new HSSFRichTextString(headers[i]);
			cell.setCellValue(text);
		}

		// 遍历集合数据，产生数据行
		Iterator<T> it = dataset.iterator();
		int index = 0;
		String lastSubNumber = null;
		Integer count = 0;
		while (it.hasNext()){
			index++;
			row = sheet.createRow(index);
			T t = (T) it.next();
			// 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
			Field[] fields = t.getClass().getDeclaredFields();
			for (short i = 0; i < fields.length; i++){
				HSSFCell cell = row.createCell(i);
				cell.setCellStyle(style2);
				Field field = fields[i];
				String fieldName = field.getName();
				String getMethodName = "get"  + fieldName.substring(0, 1).toUpperCase()  + fieldName.substring(1);
				try {
					Class tCls = t.getClass();
					Method getMethod = tCls.getMethod(getMethodName, new Class[]{});
					Object value = getMethod.invoke(t, new Object[] {});
					//判断是否同一订单，合并单元格
					if(index==1 && i==0){ //导出第一行时，保存订单号
						lastSubNumber = value.toString();
					}else { //从第二行开始比较，判断是否合并
						if(i==0 && lastSubNumber.equals(value.toString())){
							count++;
						}
						if(i==0 && !lastSubNumber.equals(value.toString())){
							if(count>0){
								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 0 ,0));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 1 ,1));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 2 ,2));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 3 ,3));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 4 ,4));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 5 ,5));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 6 ,6));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 8 ,8));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 9 ,9));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 10 ,10));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 11 ,11));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 12 ,12));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 13 ,13));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 14 ,14));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 15 ,15));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 18 ,18));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 19 ,19));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 20 ,20));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 25 ,25));
//								sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 26 ,26));
								/*sheet.addMergedRegion(new CellRangeAddress(index-count-1, index-1, 21, 21));*/
								count = 0;
							}
						}
						if(i==0){
							lastSubNumber = value.toString();
						}
					}
					// 判断值的类型后进行强制类型转换
					String textValue = null;
					if (value instanceof Boolean)
					{
						boolean bValue = (Boolean) value;
						textValue = "男";
						if (!bValue)
						{
							textValue = "女";
						}
					}
					else if (value instanceof Date)
					{
						Date date = (Date) value;
						SimpleDateFormat sdf = new SimpleDateFormat(pattern);
						textValue = sdf.format(date);
					}
					else if (value instanceof byte[])
					{
						// 有图片时，设置行高为60px;
						row.setHeightInPoints(60);
						// 设置图片所在列宽度为80px,注意这里单位的一个换算
						sheet.setColumnWidth(i, (short) (35.7 * 80));
						// sheet.autoSizeColumn(i);
						byte[] bsValue = (byte[]) value;
						HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0,
								1023, 255, (short) 6, index, (short) 6, index);
						anchor.setAnchorType(2);
						patriarch.createPicture(anchor, workbook.addPicture(
								bsValue, HSSFWorkbook.PICTURE_TYPE_JPEG));
					}
					else
					{
						if(value!=null){
							// 其它数据类型都当作字符串简单处理
							textValue = value.toString();
						}
					}
					// 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
					if (textValue != null)
					{
						Pattern p = Pattern.compile("^//d+(//.//d+)?$");
						Matcher matcher = p.matcher(textValue);
						if (matcher.matches())
						{
							// 是数字当作double处理
							cell.setCellValue(Double.parseDouble(textValue));
						}
						else
						{
							cell.setCellValue(textValue);
						}
					}

				}
				catch (SecurityException e)
				{
					e.printStackTrace();
				}
				catch (NoSuchMethodException e)
				{
					e.printStackTrace();
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					e.printStackTrace();
				}
				finally
				{
					// 清理资源
				}
			}
		}
		try
		{
			workbook.write(out);
			out.flush();
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
    
    /** 
     * 这是一个通用的方法，利用了JAVA的反射机制，可以将放置在JAVA集合中并且符号一定条件的数据以EXCEL 的形式输出到指定IO设备上 
     *  
     * @param title 
     *            表格标题名 
     * @param headers 
     *            表格属性列名数组 
     * @param dataset 
     *            需要显示的数据集合,集合中一定要放置符合javabean风格的类的对象。此方法支持的 
     *            javabean属性的数据类型有基本数据类型及String,Date,byte[](图片数据) 
     * @param out 
     *            与输出设备关联的流对象，可以将EXCEL文档导出到本地文件或者网络中 
     * @param pattern 
     *            如果有时间数据，设定输出格式。默认为"yyy-MM-dd" 
     */  
    @SuppressWarnings("unchecked")  
    public void exportExcelUtil(String title, String[] headers,  
            Collection<T> dataset, OutputStream out, String pattern)
    {  
        // 声明一个工作薄  
        HSSFWorkbook workbook = new HSSFWorkbook();  
        // 生成一个表格  
        HSSFSheet sheet = workbook.createSheet(title);  
        // 设置表格默认列宽度为15个字节  
        sheet.setDefaultColumnWidth((short) 20);  
        // 生成一个样式  
        HSSFCellStyle style = workbook.createCellStyle();  
        // 设置这些样式  
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);  
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);  
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
        // 生成一个字体  
        HSSFFont font = workbook.createFont();  
        font.setColor(HSSFColor.VIOLET.index);  
        font.setFontHeightInPoints((short) 12);  
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
        // 把字体应用到当前的样式  
        style.setFont(font);  
        // 生成并设置另一个样式  
        HSSFCellStyle style2 = workbook.createCellStyle();  
        style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);  
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);  
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);  
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  
        style2.setWrapText(true);
        // 生成另一个字体  
        HSSFFont font2 = workbook.createFont();  
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);  
        // 把字体应用到当前的样式  
        style2.setFont(font2);  
  
        // 声明一个画图的顶级管理器  
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();  
        // 定义注释的大小和位置,详见文档  
        HSSFComment comment = patriarch.createComment(new HSSFClientAnchor(0,  
                0, 0, 0, (short) 4, 2, (short) 6, 5));  
        // 设置注释内容  
        comment.setString(new HSSFRichTextString("可以在POI中添加注释！"));  
        // 设置注释作者，当鼠标移动到单元格上是可以在状态栏中看到该内容.  
        comment.setAuthor("");  
  
        // 产生表格标题行  
        HSSFRow row = sheet.createRow(0);  
        for (short i = 0; i < headers.length; i++)  
        {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellStyle(style);  
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }  
  
        // 遍历集合数据，产生数据行  
        Iterator<T> it = dataset.iterator();  
        int index = 0;  
        while (it.hasNext())  
        {  
            index++;  
            row = sheet.createRow(index);  
            T t = (T) it.next();  
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值  
            Field[] fields = t.getClass().getDeclaredFields();  
            for (short i = 0; i < fields.length; i++)  
            {  
                HSSFCell cell = row.createCell(i);  
                cell.setCellStyle(style2);  
                Field field = fields[i];  
                String fieldName = field.getName();  
                String getMethodName = "get"  
                        + fieldName.substring(0, 1).toUpperCase()  
                        + fieldName.substring(1);  
                try  
                {  
                    Class tCls = t.getClass();  
                    Method getMethod = tCls.getMethod(getMethodName,  
                            new Class[]  
                            {});  
                    Object value = getMethod.invoke(t, new Object[]  
                    {});  
                    // 判断值的类型后进行强制类型转换  
                    String textValue = null;   
                    if (value instanceof Boolean)  
                    {  
                        boolean bValue = (Boolean) value;  
                        textValue = "男";  
                        if (!bValue)  
                        {  
                            textValue = "女";  
                        }  
                    }  
                    else if (value instanceof Date)  
                    {  
                        Date date = (Date) value;  
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);  
                        textValue = sdf.format(date);  
                    }  
                    else if (value instanceof byte[])  
                    {  
                        // 有图片时，设置行高为60px;  
                        row.setHeightInPoints(60);  
                        // 设置图片所在列宽度为80px,注意这里单位的一个换算  
                        sheet.setColumnWidth(i, (short) (35.7 * 80));  
                        // sheet.autoSizeColumn(i);  
                        byte[] bsValue = (byte[]) value;  
                        HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0,  
                                1023, 255, (short) 6, index, (short) 6, index);  
                        anchor.setAnchorType(2);  
                        patriarch.createPicture(anchor, workbook.addPicture(  
                                bsValue, HSSFWorkbook.PICTURE_TYPE_JPEG));  
                    }  
                    else 
                    {  
                    	if(value!=null){
                    		// 其它数据类型都当作字符串简单处理  
                    		textValue = value.toString();  
                    	}
                    }  
                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成  
                    if (textValue != null)  
                    {  
                        Pattern p = Pattern.compile("^//d+(//.//d+)?$");  
                        Matcher matcher = p.matcher(textValue);  
                        if (matcher.matches())  
                        {  
                            // 是数字当作double处理  
                            cell.setCellValue(Double.parseDouble(textValue));  
                        }  
                        else  
                        {  
                            cell.setCellValue(textValue);  
                        }  
                    }  
                    
                }  
                catch (SecurityException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (NoSuchMethodException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (IllegalArgumentException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (IllegalAccessException e)  
                {  
                    e.printStackTrace();  
                }  
                catch (InvocationTargetException e)  
                {  
                    e.printStackTrace();  
                }  
                finally  
                {  
                    // 清理资源  
                }  
            }  
        }  
        try  
        {  
        	workbook.write(out);
			out.flush();
			out.close();  
        }  
        catch (IOException e)  
        {  
            e.printStackTrace();  
        }  
    }
    /**
     * 下载本地导入商品excel/导出中标商品
     * @param title 表格标题名
     * @param headers 表格属性列名数组
     * @param dataset 需要显示的数据集合,集合中一定要放置符合javabean风格的类的对象。此方法支持的
     *                  javabean属性的数据类型有基本数据类型及String,Date,byte[](图片数据)
     * @param out 与输出设备关联的流对象，可以将EXCEL文档导出到本地文件或者网络中
     * @param pattern 如果有时间数据，设定输出格式。默认为"yyy-MM-dd"
     * @param noteArr 增加一个备注页
     */
    @SuppressWarnings("unchecked")
    public void exportExcelUtil(String title, String[] headers,Collection<T> dataset, OutputStream out, String pattern,String[] noteArr) {
        // 声明一个工作薄
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet(title);
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 22);
        sheet.setDefaultRowHeight((short)(20*20));
        // 生成一个样式
        HSSFCellStyle style = workbook.createCellStyle();
        // 设置这些样式
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成一个字体
        HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(font);
        // 生成并设置另一个样式
        HSSFCellStyle style2 = workbook.createCellStyle();
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        // 把字体应用到当前的样式
        style2.setFont(font2);

        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);
        row.setHeight((short)(20*20));
        for (short i = 0; i < headers.length; i++){
            //红色标题样式
            HSSFCellStyle red_style = workbook.createCellStyle();
            red_style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            red_style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            // 生成一个字体
            HSSFFont red_font = workbook.createFont();
            red_font.setColor(HSSFColor.RED.index);
            red_font.setFontHeightInPoints((short) 12);
            red_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            // 把字体应用到当前的样式
            red_style.setFont(red_font);
            HSSFCell cell = row.createCell(i);
            String headerText = headers[i];
            if(headerText.contains("#")){
                headerText = headerText.substring(0,headerText.length()-1);
                red_style.setFont(red_font);
                cell.setCellStyle(red_style);
            }else{
                cell.setCellStyle(style);
            }
            HSSFRichTextString text = new HSSFRichTextString(headerText);
            cell.setCellValue(text);
        }

        // 遍历集合数据，产生数据行
        Iterator<T> it = dataset.iterator();
        int index = 0;
        while (it.hasNext()){
            index++;
            row = sheet.createRow(index);
            row.setHeight((short)(20*20));
            T t = (T) it.next();
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
            Field[] fields = t.getClass().getDeclaredFields();
            //spu字段数(去除了skulist)
            int spuFieldCount = fields.length-1;
            for (short i = 0; i < fields.length; i++){
                HSSFCell cell = row.createCell(i);
                cell.setCellStyle(style2);
                Field field = fields[i];
                String fieldName = field.getName();
                String getMethodName = "get"+ fieldName.substring(0, 1).toUpperCase()+ fieldName.substring(1);
                try{
                    Class tCls = t.getClass();
                    Method getMethod = tCls.getMethod(getMethodName,new Class[]{});
                    Object value = getMethod.invoke(t, new Object[]{});
                    // 判断值的类型后进行强制类型转换
                    String textValue = null;
                    if (value instanceof List){
                        textValue = null;
                        List<T> list = (List<T>)value;
                        // 遍历集合数据，产生数据行
                        Iterator<T> it2 = list.iterator();
                        while (it2.hasNext()){
                            int spuFieldCount2 = spuFieldCount;
                            index++;
                            row = sheet.createRow(index);
                            row.setHeight((short)(20*20));
                            T t2 = (T) it2.next();
                            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
                            Field[] fields2 = t2.getClass().getDeclaredFields();
                            for (short j = 0; j < fields2.length; j++){
                                //过滤sku里面的spu编码
                                if(t2 instanceof TenderSkuImportDTO && j==0){
                                    spuFieldCount2 -- ;
                                    continue;
                                }
                                HSSFCell cell2 = row.createCell(j+spuFieldCount2);
                                cell2.setCellStyle(style2);
                                Field field2 = fields2[j];
                                String fieldName2 = field2.getName();
                                String getMethodName2 = "get"+ fieldName2.substring(0, 1).toUpperCase()+ fieldName2.substring(1);
                                Class tCls2 = t2.getClass();
                                Method getMethod2 = tCls2.getMethod(getMethodName2,new Class[]{});
                                Object value2 = getMethod2.invoke(t2, new Object[]{});
                                // 判断值的类型后进行强制类型转换
                                String textValue2 = null;
                                if (value2 instanceof Boolean){
                                    boolean bValue2 = (Boolean) value2;
                                    textValue2 = "是";
                                    if (!bValue2){
                                        textValue2 = "否";
                                    }
                                }
                                else if (value2 instanceof Date){
                                    Date date2 = (Date) value2;
                                    textValue2 = DateUtils.format(date2);
                                }
                                else{
                                    if(value2!=null){
                                        // 其它数据类型都当作字符串简单处理
                                        textValue2 = value2.toString();
                                    }
                                }
                                // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
                                if (textValue2 != null){
                                    Pattern p2 = Pattern.compile("^//d+(//.//d+)?$");
                                    Matcher matcher2 = p2.matcher(textValue2);
                                    if (matcher2.matches()){
                                        // 是数字当作double处理
                                        cell2.setCellValue(Double.parseDouble(textValue2));
                                    }
                                    else{
                                        cell2.setCellValue(textValue2);
                                    }
                                }
                            }
                        }
                        //置空value
                        value = null;
                    }
                    if (value instanceof Boolean){
                        boolean bValue = (Boolean) value;
                        textValue = "是";
                        if (!bValue){
                            textValue = "否";
                        }
                    }else if (value instanceof Date){
                        Date date = (Date) value;
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                        textValue = sdf.format(date);
                    }else{
                        if(value!=null){
                            // 其它数据类型都当作字符串简单处理
                            textValue = value.toString();
                        }
                    }
                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
                    if (textValue != null){
                        Pattern p = Pattern.compile("^//d+(//.//d+)?$");
                        Matcher matcher = p.matcher(textValue);
                        if (matcher.matches()){
                            // 是数字当作double处理
                            cell.setCellValue(Double.parseDouble(textValue));
                        }
                        else{
                            cell.setCellValue(textValue);
                        }
                    }

                }
                catch (SecurityException e){
                    e.printStackTrace();
                }
                catch (NoSuchMethodException e){
                    e.printStackTrace();
                }
                catch (IllegalArgumentException e){
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                finally {
                    // 清理资源
                }
            }
        }
        try {
            if(noteArr != null && noteArr.length > 0){
                HSSFSheet noteSheet = workbook.createSheet("填写说明");
                HSSFCellStyle noteStyle = workbook.createCellStyle();
                //设置自动换行
                noteStyle.setWrapText(true);
                //文本靠左
                noteStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
                // 设置表格默认列宽度
                noteSheet.setColumnWidth(0,400*100);
                noteSheet.setDefaultColumnWidth((short) 1000);
                noteSheet.setDefaultRowHeight((short)(20*20));
                for (short i = 0; i < noteArr.length; i++){
                    HSSFRow noteRow = noteSheet.createRow(i);
                    noteRow.setHeight((short)(20*20));
                    HSSFCell noteCell = noteRow.createCell(0);
                    noteCell.setCellStyle(noteStyle);
                    // 把字体应用到当前的样式
                    String noteText = noteArr[i];
                    noteCell.setCellStyle(noteStyle);
                    HSSFRichTextString text = new HSSFRichTextString(noteText);
                    noteCell.setCellValue(text);
                }
            }

            workbook.write(out);
            out.flush();
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    }