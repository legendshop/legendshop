/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.legendshop.core.handler.LegendHandler;
import com.legendshop.core.handler.impl.NormalLegendHandler;

/**
 * The Class LegendFilter.
 */
public class LegendFilter extends OncePerRequestFilter {
	
	
	private LegendHandler legendHandler = new NormalLegendHandler();;

	/**
	 * 过滤器
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
			String uri = request.getRequestURI();

			//不包括有后缀的URL,只是支持Controller的动作（不带后缀）
			boolean isSupportUrl = supportURL(uri);
			legendHandler.doHandle(isSupportUrl, request, response, chain);
	}

	/**
	 * 不包括有后缀的URL,只是支持Controller的动作（不带后缀）
	 * 
	 * @param url
	 * @return
	 */
	private boolean supportURL(String url) {
		if (url == null) {
			return false;
		}
		return url.indexOf(".") < 0;
	}


	@Override
	public void destroy() {

	}

	@Override
	protected void initFilterBean() throws ServletException {

	}

}
