package com.legendshop.core.pageprovider;

import java.util.Locale;

import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.ToolBarProvider;

/**
 * 手机端工具条
 *
 */
public class AppPageProviderImpl extends DefaultPagerProvider implements ToolBarProvider{
	
	private String actionUrl;
	
	public AppPageProviderImpl(long total, String curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}
	
	public AppPageProviderImpl(long total, int curPage, int pageSize, String actionUrl) {
		super(total, curPage, pageSize);
		this.actionUrl =actionUrl;
	}

	@Override
	public String getToolBar() {
		if( hasMutilPage()){//有分页条
			return this.getBar(null, actionUrl);
		}else{
			return null;
		}
	}
	
	
	public String getBar(Locale locale, String url) {
		StringBuilder str = new StringBuilder();
		//1. 显示上一页
		if (isFirst()) {//位于第一页
					// 第一页不用加超链接
			str.append("<a class='pn-prev disabled'><em>上一页</em></a>");
		} else {
			str.append("<a class='pn-prev' href='").append(url).append("(").append(previous()).append(")'>").append("<em>上一页</em></a>");
		}
		//2. 显示首页
		appendItem( str,  1, url);

		//3. 显示当前页前后2页
		int d1 = curPageNO - 1; //当前页与第一页的间距
		int d2 = pageCount - curPageNO; //当前页与最后一页的间距
		int start = Math.max(curPageNO - 2, 2);
		int end =  Math.min(curPageNO + 2, pageCount - 1) ;
		//3.1 加入点号
		if(d1 > 3){
			appendEllipsisItem(str);
		}
		for (int i = start; i <= end; i++) {
			appendItem(str, i,url);
		}
		//3.2 加入点号
		if(d2 > 3){
			appendEllipsisItem(str);
		}
		//4. 显示尾页
		appendItem( str,  pageCount, url);
		
		//5. 显示下一页
		if (isLast() || (total== 0)) {
			str.append("<a class='pn-next disabled'><em>下一页</em></a>");
		} else {
			str.append("<a class='pn-next' href='").append(url).append("(").append(next()).append(")'>")
			 .append("<em>下一页</em></a>");
		}
		return  str.toString();
	}
	
	/**
	 * 加入页码
	 * @param str
	 * @param i
	 * @param url
	 */
	private void appendItem(StringBuilder str, int i,String url){
		if (i == getCurPageNO()) {
			str.append("<a href='javascript:void(0);' class='curr'>").append(i).append("</a>");
		} else {
			str.append("<a  href='").append(url).append("(").append(i).append(")'>").append(i).append("</a>");
		}
	}
	
	/**
	 * 加入省略号
	 * @param str
	 */
	private void appendEllipsisItem(StringBuilder str){
		str.append("<b class='pn-break '>…</b>");
	}


	

}
