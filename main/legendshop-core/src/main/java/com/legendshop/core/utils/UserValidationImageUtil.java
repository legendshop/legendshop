package com.legendshop.core.utils;

import javax.servlet.http.HttpSession;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.AttributeKeys;
/**
 * 图片验证码是否需要验证
 *
 */
public class UserValidationImageUtil {

	private static SystemParameterUtil systemParameterUtil;

	static{
		systemParameterUtil = ContextServiceLocator.getBean(SystemParameterUtil.class, "systemParameterUtil");
	}

	// 是否需要验证码
	public static boolean needToValidation(HttpSession session) {
		if (session == null) {
			return false;
		}
		Integer count = (Integer) session.getAttribute(AttributeKeys.TRY_LOGIN_COUNT);

		Boolean needToValidation = true;
		if (count == null || count < 3) {
			needToValidation = false;
		}
		Boolean result = systemParameterUtil.getValidationImage();
		return (result != null && result) && needToValidation;
	}
}
