/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 计算页面的路径.
 */
public class PathResolver {

	private final static Logger log = LoggerFactory.getLogger(PathResolver.class);

	/**
	 * 计算当前页的位置
	 */
	public static String getPath(PageDefinition page) {
		String path = page.getValue();
		if (log.isDebugEnabled()) {
			log.debug("enter page {},  path = {}", page, path);
		}
		return path;
	}

	/**
	 * 计算pathValue的位置
	 */
	public static String getPath(String pathValue, PageDefinition page) {
		String path = page.getValue(pathValue);
		if (log.isDebugEnabled()) {
			log.debug("enter page {},  path = {}", page, path);
		}
		return path;
	}
	
	/**
	 * 计算pathValue的重定向位置
	 */
	public static String getRedirectPath(String pathValue) {
		return PagePathCalculator.calculateActionPath("redirect:", pathValue);
	}
	
	/**
	 * 计算pathValue的前进位置
	 */
	public static String getForwardPath(String pathValue) {
		return PagePathCalculator.calculateActionPath("forward:", pathValue);
	}

}
