package com.legendshop.core.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.order.ProdExcelDTO;
import com.legendshop.model.dto.order.TenderProdExcelDTO;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.legendshop.model.dto.CoinImportResult;
import com.legendshop.model.dto.CouponUserImportResult;
import com.legendshop.model.dto.ProductImportResult;
import com.legendshop.model.entity.ProductImport;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.util.AppUtils;

public class POIReadeExcelUtil {
	private  Workbook wb;
    private Sheet sheet;
    private Row row;
    
    
    /**
     * 读取Excel表格表头的内容
     * @param InputStream
     * @return String 表头内容的数组
     */
    public  String[] readExcelTitle(InputStream is) {
        try {
            wb = create(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sheet = wb.getSheetAt(0);
        row = sheet.getRow(0);
        // 标题总列数
        int colNum = row.getPhysicalNumberOfCells();
        String[] title = new String[colNum];
        for (int i = 0; i < colNum; i++) {
            title[i] = getCellFormatValue(row.getCell((short) i));
        }
        return title;
    }


    /**
     * 读取excel(普通商品)
     * @param excelMaps
     * @return
     * @throws Exception
     */
    public List<ProdExcelDTO> readBasicProductExcels(Map<Integer, String> excelMaps) throws Exception {
        List<ProdExcelDTO> dataModels = new ArrayList<ProdExcelDTO>();
        for (Map.Entry<Integer, String> entries:excelMaps.entrySet() ) {
            String row[] = entries.getValue().split(",");
            ProdExcelDTO prodExcelDTO = new ProdExcelDTO();
            prodExcelDTO.setProdName(row[0]);
            prodExcelDTO.setCategroyId(row[1]);
            prodExcelDTO.setShopCategroy(row[2]);
            prodExcelDTO.setFirstParameter(row[3]);
            prodExcelDTO.setFirstParameterValue(row[4]);
            prodExcelDTO.setSecondParameter(row[5]);
            prodExcelDTO.setSecondParameterValue(row[6]);
            prodExcelDTO.setThirdParameter(row[7]);
            prodExcelDTO.setThirdParameterValue(row[8]);
            prodExcelDTO.setFourthParameter(row[9]);
            prodExcelDTO.setFourthParameterValue(row[10]);
            prodExcelDTO.setSpuPrice(row[11]);
//            prodExcelDTO.setPicture(row[12]);
            prodExcelDTO.setContent(row[12]);
            prodExcelDTO.setSkuName(row[13]);
            prodExcelDTO.setFirstProperties(row[14]);
            prodExcelDTO.setFirstPropertiesValue(row[15]);
            prodExcelDTO.setSecondProperties(row[16]);
            prodExcelDTO.setSecondPropertiesValue(row[17]);
            prodExcelDTO.setThirdProperties(row[18]);
            prodExcelDTO.setThirdPropertiesValue(row[19]);
            prodExcelDTO.setFourthProperties(row[20]);
            prodExcelDTO.setFourthPropertiesValue(row[21]);
            prodExcelDTO.setSkuPrice(row[22]);
            prodExcelDTO.setStocks(row[23]);
            prodExcelDTO.setWeight(row[24]);
            prodExcelDTO.setPartyCode(row[25]);
            prodExcelDTO.setModelId(row[26]);
            prodExcelDTO.setStatus(row[27]);
            dataModels.add(prodExcelDTO);
        }
        return dataModels;
    }


    /**
     * 根据表头内容和定义好的变量和中文map，进行变量名的匹配
     * @param path
     * @param tabelHead
     * @return
     * @throws Exception
     */
    public String[] impotrHead(InputStream is, Map<String, String> tabelHead ) throws Exception{
    	String []  ss=readExcelTitle(is);
		String headName = "";
		boolean flag = true;
		for (int i = 0; i < ss.length; i++) {
			if(flag){
				flag = false;
				for(Map.Entry<String, String> entry: tabelHead.entrySet()) {
					if(ss[i].equals(entry.getKey())){
						if(headName.equals("")){
							headName = entry.getValue();
						}
						else{
							headName = headName +","+entry.getValue();
						}
						flag = true;
						break;
					}
				}
			}
			else{
				break;
			}	
		}
		if(flag){
			String[] fieldNames = headName.split(",");
			return fieldNames;
		}
		return null;
		
	}

    /**
     * 读取Excel数据内容
     * @param InputStream
     * @return Map 包含单元格数据内容的Map对象
     */
    public Map<Integer, String> readExcelContent(InputStream is) {
        Map<Integer, String> content = new HashMap<Integer, String>();
        String str = "";
        try {
            wb = create(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sheet = wb.getSheetAt(0);
        // 得到总行数
        int rowNum = sheet.getLastRowNum();
        row = sheet.getRow(0);
        int colNum = row.getPhysicalNumberOfCells();
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int j = 0;
            while (j < colNum) {
                str += getCellFormatValue(row.getCell((short) j)) + ",";
                j++;
            }
            content.put(i, str);
            str = "";
        }
        return content;
    }

    /**
     * 根据HSSFCell类型设置数据
     * @param cell
     * @return
     */
    private String getCellFormatValue(Cell cell) {
        String cellvalue = " ";
        if (cell != null) {
            // 判断当前Cell的Type
            switch (cell.getCellType()) {
            // 如果当前Cell的Type为NUMERIC
            case HSSFCell.CELL_TYPE_NUMERIC:
            	 // 取得当前Cell的数值
                cellvalue = String.valueOf(cell.getNumericCellValue());
            case HSSFCell.CELL_TYPE_FORMULA: {
                // 判断当前的cell是否为Date
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    // 如果是Date类型则，转化为Data格式
                    //方法1：这样子的data格式是带时分秒的：2011-10-12 0:00:00
                    //cellvalue = cell.getDateCellValue().toLocaleString();
                    //方法2：这样子的data格式是不带带时分秒的：2011-10-12
                    Date date = cell.getDateCellValue();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    cellvalue = sdf.format(date);
                }
                // 如果是纯数字
                else {
                    // 取得当前Cell的数值
                    double value =  cell.getNumericCellValue();
                    double eps = 1e-10;  // 精度范围  
                    boolean isInt =  value-Math.floor(value) < eps;  //判断是否为整数
                    if(isInt){
                    	cellvalue = String.valueOf((int) value);
                    }else{
                    	// 取得当前Cell的数值
                        cellvalue = String.valueOf(value);
                    }
                }
                break;
            }
            // 如果当前Cell的Type为STRIN
            case HSSFCell.CELL_TYPE_STRING:
                // 取得当前的Cell字符串
                cellvalue = cell.getRichStringCellValue().getString();
                break;
            // 默认的Cell值
            default:
                cellvalue = " ";
            }
        } else {
            cellvalue = " ";
        }
        return cellvalue;

    }
    
    protected <T> Object parseValueWithType(String value, Class<?> type) {
		 Object result = null;
	        try { // 根据属性的类型将内容转换成对应的类型
	            if (Boolean.TYPE == type) {
	                result = Boolean.parseBoolean(value);
	            } else if (Byte.TYPE == type) {
	                result = Byte.parseByte(value);
	            } else if (Short.TYPE == type) {
	                result = Short.parseShort(value);
	            } else if (Integer.TYPE == type) {
	                result = Integer.parseInt(value);
	            } else if (Long.TYPE == type) {
	                result = Long.parseLong(value);
	            } else if (Float.TYPE == type) {
	                result = Float.parseFloat(value);
	            } else if (Double.TYPE == type) {
	                result = Double.parseDouble(value);
	            } else if (Class.forName("java.lang.Double") == type){
	            	result = Double.parseDouble(value);
	            } else {
	                result = (Object) value;
	            }
	        } catch (Exception e) {
	            // 把异常吞掉直接返回null
	        }
	        return result;
   }
    
    /**
     * 读取批量充值中exels表中的数据
     * @param is
     * @return
     */
    public CoinImportResult batchRecharge(InputStream is){
    	CoinImportResult importResult = new CoinImportResult();
    	List<UserDetail> result = new ArrayList<UserDetail>();
    	try {
            wb = create(is);
        }catch (Exception e) {
            e.printStackTrace();
        }
    	sheet = wb.getSheetAt(0);
        
        int rowNum = sheet.getLastRowNum();  // 得到总行数
        if(rowNum > 20000||rowNum<=0){
        	importResult.setMessage("超出最大记录数或没数据");
        	return importResult;
        }
        row = sheet.getRow(0); //获得表头标题那一行
        int colNum = row.getPhysicalNumberOfCells();
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int j = 0;
            UserDetail recharge_user = new UserDetail();
            while (j < colNum) {
            	if(row != null){
            		if(j==0){  //获取手机号码
            			BigDecimal mobile_big = new BigDecimal(getCellFormatValue(row.getCell(j)));
            			String mobile_str = mobile_big.toPlainString();
            			recharge_user.setUserMobile(mobile_str);
            		}else{  //获取充值金币数量
            			String amount = getCellFormatValue(row.getCell(j));
            			if(AppUtils.isNotBlank(amount)){
            				recharge_user.setUserCoin(Double.valueOf(amount));
            			}
            			result.add(recharge_user);
            		}
            		j++;
            	}else{
            		j++;
            	}
            }
        }
        importResult.setResult(result);
        return importResult;
    }
    /**
     * 读取excel(中标商品) del
     * @param excelMaps
     * @return
     */
    public List<TenderProdExcelDTO> readTenderProductExcels(Map<Integer, String> excelMaps) throws Exception {
        List<TenderProdExcelDTO> dataModels = new ArrayList<TenderProdExcelDTO>();
        for (Map.Entry<Integer, String> entries:excelMaps.entrySet() ) {
            String row[] = entries.getValue().split(",");
            TenderProdExcelDTO tenderProdExcelDTO = new TenderProdExcelDTO();
            tenderProdExcelDTO.setSpuId(row[0]);
            tenderProdExcelDTO.setProdName(row[1]);
            tenderProdExcelDTO.setShopCategroy(row[2]);
            tenderProdExcelDTO.setFirstParameter(row[3]);
            tenderProdExcelDTO.setFirstParameterValue(row[4]);
            tenderProdExcelDTO.setSecondParameter(row[5]);
            tenderProdExcelDTO.setSecondParameterValue(row[6]);
            tenderProdExcelDTO.setThirdParameter(row[7]);
            tenderProdExcelDTO.setThirdParameterValue(row[8]);
            tenderProdExcelDTO.setFourthParameter(row[9]);
            tenderProdExcelDTO.setFourthParameterValue(row[10]);
            tenderProdExcelDTO.setFifthParameter(row[11]);
            tenderProdExcelDTO.setFifthParameterValue(row[12]);
//            tenderProdExcelDTO.setUnit(row[13]);
            tenderProdExcelDTO.setSpuPrice(row[14]);
//            tenderProdExcelDTO.setPayType(row[15]);
//            tenderProdExcelDTO.setPicture(row[16]);
            tenderProdExcelDTO.setContent(row[17]);
            tenderProdExcelDTO.setSkuId(row[18]);
            tenderProdExcelDTO.setSkuName(row[19]);
            tenderProdExcelDTO.setProjectNum(row[20]);
            tenderProdExcelDTO.setMaterialCode(row[21]);
            tenderProdExcelDTO.setEffectiveDate(row[22]);
            tenderProdExcelDTO.setFirstProperties(row[23]);
            tenderProdExcelDTO.setFirstPropertiesValue(row[24]);
            tenderProdExcelDTO.setSkuPrice(row[25]);
            tenderProdExcelDTO.setTenderPrice(row[26]);
            tenderProdExcelDTO.setStocks(row[27]);
//            tenderProdExcelDTO.setWeight(row[28]);
            tenderProdExcelDTO.setPartyCode(row[29]);
            tenderProdExcelDTO.setModelId(row[30]);
            tenderProdExcelDTO.setStatus(row[31]);
            tenderProdExcelDTO.setRemark(row[32]);
            dataModels.add(tenderProdExcelDTO);
        }
        return dataModels;
    }
    
    /**
     * 读取excel内容
     * @param is
     * @return
     * @throws Exception
     */
	public ProductImportResult readExcels(InputStream is){
		ProductImportResult importResult = new ProductImportResult();
        List<ProductImport> result = new ArrayList<ProductImport>();
        try {
            wb = create(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sheet = wb.getSheetAt(0);
        // 得到总行数
        int rowNum = sheet.getLastRowNum();
        if(rowNum > 20000||rowNum<=0){
        	importResult.setMessage("超出最大记录数或没数据");
        	return importResult;
        }
        row = sheet.getRow(0); //获得表头标题那一行
        int colNum = row.getPhysicalNumberOfCells();
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int j = 0;
        	String[] rowList = new String[colNum];
            while (j < colNum) {
            	rowList[j] = getCellFormatValue(row.getCell(j));
                j++;
            }
            ProductImport dto = convertProductImport(rowList);
            if(dto.getConvertResult()!=null){
            	importResult.setMessage(dto.getConvertResult());
            	return importResult;
            }else{
            	  result.add(dto);
            }
          
        }
        
        importResult.setResult(result);
        return importResult;
	}
	
	private ProductImport convertProductImport(String[] row){
		ProductImport productImport = new ProductImport();
		try {
			productImport.setProdName(row[0]);
			productImport.setBrief(row[1]);
			productImport.setPrice(Double.valueOf(row[2]));
			productImport.setCash(Double.valueOf(row[3]));
			Double stockArm = Double.parseDouble(row[4]);
			productImport.setStocksArm(stockArm.longValue());
			productImport.setModelId(row[5]);
			productImport.setPartyCode(row[6]);
			productImport.setMetaTitle(row[7]);
			productImport.setKeyWord(row[8]);
			productImport.setMetaDesc(row[9]);
			 
			productImport.checkData();//检查数据
		} catch (Exception e) {
			productImport.setConvertResult(e.getMessage());
		}
	  
		return productImport;
	}
    
    
    public  <T> List<T> readExcels1(Class<T> clazz, Map<String, String> oldMap, String[] fieldNames,Map<Integer, String> excelMaps) throws Exception {
        List<T> dataModels = new ArrayList<T>();
    	for (int i = 1; i <= excelMaps.size(); i++) {
    		String row[] = excelMaps.get(i).split(",");
            T target = clazz.newInstance(); 
            for (int j = 0; j < fieldNames.length; j++) {
    			String fieldName = fieldNames[j];
    			if (fieldName == null) {
    				continue; // 过滤serialVersionUID属性
    			}
    			if(j==row.length){
    				break;
    			}
    			// 获取excel单元格的内容
    			String cell = row[j];
    			String content = "";
    			if (cell != null) {
    				content = cell;
    			}
    			
    		/*	target 指定对象
    			fieldName 属性名
    			argTypes 参数类型
    			args 参数列表*/
    			Field field = clazz.getDeclaredField(fieldName);
    			 invokeSetter(target, fieldName,
                         parseValueWithType(content, field.getType()));
    		}
            dataModels.add(target); 
        }
    	
    	
    	
		return dataModels;
	}
    
    
    public <T> void invokeSetter(T target, String fieldName, Object args)
            throws NoSuchFieldException, SecurityException,
            NoSuchMethodException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        // 如果属性名为xxx，则方法名为setXxx
        String methodName = "set" + firstCharUpperCase(fieldName);
        Class<?> clazz = target.getClass();
        Field field = clazz.getDeclaredField(fieldName);
        Method method = clazz.getMethod(methodName, field.getType());
        method.invoke(target, args);
    }
    public  String firstCharUpperCase(String str) {
        StringBuffer buffer = new StringBuffer(str);
        if (buffer.length() > 0) {
            char c = buffer.charAt(0);
            buffer.setCharAt(0, Character.toUpperCase(c));
        }
        return buffer.toString();
    }
    
    
    public  Workbook create(InputStream inp) throws IOException, InvalidFormatException {  
        if(! inp.markSupported()) {
            inp = new PushbackInputStream(inp, 8);  //先读取8个流
        }  
          
        if(POIFSFileSystem.hasPOIFSHeader(inp)) {  //判断是2003还是2007再返回对象
            return new HSSFWorkbook(inp);  
        }  
        if(POIXMLDocument.hasOOXMLHeader(inp)) {  
            return new XSSFWorkbook(OPCPackage.open(inp));  
        }  
        throw new IllegalArgumentException("Your InputStream was neither an OLE2 stream, nor an OOXML stream");  
    }
    
    public static void main(String[] args) {
    	
    	String str="1,2,3, ,,,,";
    	System.out.println(str.split(",").length);
    	
	}



    public CouponUserImportResult batch(InputStream is){
    	CouponUserImportResult importResult = new CouponUserImportResult();
    	List<String> result = new ArrayList<String>();
    	try {
            wb = create(is);
        }catch (Exception e) {
            e.printStackTrace();
        }
    	sheet = wb.getSheetAt(0);
        
        int rowNum = sheet.getLastRowNum();  // 得到总行数
        if(rowNum > 20000||rowNum<=0){
        	importResult.setMessage("超出最大记录数或没数据");
        	return importResult;
        }
        row = sheet.getRow(0); //获得表头标题那一行
        int colNum = row.getPhysicalNumberOfCells();
        // 正文内容应该从第二行开始,第一行为表头的标题
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            BigDecimal mobile_big = new BigDecimal(getCellFormatValue(row.getCell(0)));
			String mobile_str = mobile_big.toPlainString();
			result.add(mobile_str);
        }
        importResult.setResult(result);
        return importResult;
    }
    
    
}
