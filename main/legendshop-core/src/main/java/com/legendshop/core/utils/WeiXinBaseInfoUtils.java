package com.legendshop.core.utils;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.legendshop.base.model.WeiXinBaseInfo;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;

/**
 * 
 * 获取微信的基本信息, TODO 目前已经不使用 
 *
 */
public class WeiXinBaseInfoUtils {
	
	/** 系统配置. */
	private static PropertiesUtil propertiesUtil = PropertiesUtilManager.getPropertiesUtil();
	
	/*
	 * 获取微信的基本信息, 在内网中访问微信服务去获取微信的基本信息
	 */
	private static final String WEIXIN_BASEINFO_ADDR= propertiesUtil.getWeiXinService()+"/admin/weixin/base/getJsApiInfo";

	private static WeiXinBaseInfo baseInfo=null;
	
	private static String[] mobileAgents = { "iphone", "android", "phone", "mobile", "wap", "netfront", "java", "opera mobi",
			"opera mini", "ucweb", "windows ce", "symbian", "series", "webos", "sony", "blackberry", "dopod",
			"nokia", "samsung", "palmsource", "xda", "pieplus", "meizu", "midp", "cldc", "motorola", "foma",
			"docomo", "up.browser", "up.link", "blazer", "helio", "hosin", "huawei", "novarra", "coolpad", "webos",
			"techfaith", "palmsource", "alcatel", "amoi", "ktouch", "nexian", "ericsson", "philips", "sagem",
			"wellcom", "bunjalloo", "maui", "smartphone", "iemobile", "spice", "bird", "zte-", "longcos",
			"pantech", "gionee", "portalmmm", "jig browser", "hiptop", "benq", "haier", "^lct", "320x320",
			"240x320", "176x220", "w3c ", "acs-", "alav", "alca", "amoi", "audi", "avan", "benq", "bird", "blac",
			"blaz", "brew", "cell", "cldc", "cmd-", "dang", "doco", "eric", "hipt", "inno", "ipaq", "java", "jigs",
			"kddi", "keji", "leno", "lg-c", "lg-d", "lg-g", "lge-", "maui", "maxo", "midp", "mits", "mmef", "mobi",
			"mot-", "moto", "mwbp", "nec-", "newt", "noki", "oper", "palm", "pana", "pant", "phil", "play", "port",
			"prox", "qwap", "sage", "sams", "sany", "sch-", "sec-", "send", "seri", "sgh-", "shar", "sie-", "siem",
			"smal", "smar", "sony", "sph-", "symb", "t-mo", "teli", "tim-", "tosh", "tsm-", "upg1", "upsi", "vk-v",
			"voda", "wap-", "wapa", "wapi", "wapp", "wapr", "webc", "winw", "winw", "xda", "xda-",
			"Googlebot-Mobile" };
	
	public static String getAppId(){
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo.getAppId();
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo.getAppId();
		}else{
			return null;
		}
	}
	
     public static WeiXinBaseInfo getWeiXinBaseInfo(){
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo;
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo;
		}else{
			return null;
		}
	}


	
	public static String getPartnerId() {
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo.getPartnerId();
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo.getPartnerId();
		}else{
			return null;
		}
	}
	
	
	
	public static String getAppSecret() {
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo.getAppSecret();
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo.getAppSecret();
		}else{
			return null;
		}
	}
	
	
	public static String getPartnerKey() {
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo.getPartnerKey();
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo.getPartnerKey();
		}else{
			return null;
		}
	}
	
	public static String getToken() {
		if(AppUtils.isNotBlank(baseInfo)){
			return baseInfo.getToken();
		}
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String token = RandomStringUtils.randomNumeric(4);
		paramMap.put("token", token);
		paramMap.put("secret",MD5Util.toMD5(token + propertiesUtil.getWeiXinKey()));
		String result=HttpUtil.httpPost(WEIXIN_BASEINFO_ADDR, paramMap);
		if(AppUtils.isNotBlank(result)){
			baseInfo=JSONUtil.getObject(result, WeiXinBaseInfo.class);
			if(AppUtils.isBlank(baseInfo)){
				return null;
			}
			return baseInfo.getToken();
		}else{
			return null;
		}
	}
	
	
	public static boolean isMoblie(HttpServletRequest request) {
		try {
			boolean isMoblie = false;
			if (request.getHeader("User-Agent") != null) {
				for (String mobileAgent : mobileAgents) {
					if (request.getHeader("User-Agent").toLowerCase().indexOf(mobileAgent) >= 0) {
						isMoblie = true;
						break;
					}
				}
			}
			return isMoblie;	
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public static boolean isWeiXin(HttpServletRequest request) {
		try {
			String userAgent = request.getHeader("User-Agent");
			if (StringUtils.isNotBlank(userAgent)) {
				Pattern p = Pattern.compile("MicroMessenger/(\\d+).+");
				Matcher m = p.matcher(userAgent);
				String version = null;
				if (m.find()) {
					version = m.group(1);
				}
				return (null != version && NumberUtils.toInt(version) >= 5);
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	/**
	 * 不包括有后缀的URL,只是支持Controller的动作（不带后缀）
	 * 
	 * @param url
	 * @return
	 */
	public static boolean supportURL(String url) {
		if (url == null) {
			return false;
		}
		return url.indexOf(".") < 0;
	}

}
