package com.legendshop.core.handler;

import java.util.Date;
import java.util.List;

import cn.hutool.core.util.ObjectUtil;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.spi.service.PresellSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.order.CancelOrderDto;
import com.legendshop.model.entity.Sub;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.processor.helper.DelayCancelHelper.OnDelayedListener;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 定期延迟取消队列初始化
 *
 */
public class DeplyCancelQueueInitProcessor extends BaseProcessor<String> {

	private static Logger log = LoggerFactory.getLogger(DeplyCancelQueueInitProcessor.class);
	
	private SubService subService;

	private PresellSubService presellSubService;
	
	private DelayCancelHelper delayCancelHelper;
	
	@Override
	public void process(String task) {
		log.info(" DeplyCancelQueueInitProcessor start .....");
		
		//查询后台设置的订单过期时间
		final int expireTime=subService.getOrderCancelExpireDate();
		
		//自动取消订单
		delayCancelHelper.start(new OnDelayedListener(){  
            @Override  
            public void onDelayedArrived(final CancelOrderDto order) {
            	/*ExecutorService service=Executors.newSingleThreadExecutor();
            	service.execute(new Runnable() {
					@Override
					public void run() {
					 
					}
				});
               service.shutdown();*/
            	  //查库判断是否需要自动收货  
            	try {
                	Sub sub =subService.getSubBySubNumber(order.getSubNumber());

					if (!SubTypeEnum.PRESELL.value().equals(sub.getSubType())) {
						if(AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){
							log.info(" **  DeplyCancelQueue auto cancel order by subNumber={} ** ",sub.getSubNumber());
							Date date = new Date();
							sub.setUpdateDate(date);
							sub.setStatus(OrderStatusEnum.CLOSE.value());
							sub.setCancelReason("超时未支付");
							//	System.out.println(JSONUtil.getJson(sub));
							subService.cancleOrder(sub);
						}
					}

					//预售订单特殊处理
					String subType = sub.getSubType();
					if (ObjectUtil.isNotEmpty(subType) && CartTypeEnum.PRESELL.toCode().equals(subType)) {
						PresellSub presellSub = presellSubService.getPresellSub(sub.getSubNumber());
						//如果定金都未支付
						if (presellSub.getIsPayDeposit() == 0) {
							if(AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){
								log.info(" **  DeplyCancelQueue auto cancel order by subNumber={} ** ",sub.getSubNumber());
								Date date = new Date();
								sub.setUpdateDate(date);
								sub.setStatus(OrderStatusEnum.CLOSE.value());
								sub.setCancelReason("超时未支付");
								subService.cancleOrder(sub);
							}
						}
						//如果超过尾款支付时间
						long finalMEndTime = presellSub.getFinalMEnd().getTime();
						if (finalMEndTime < new Date().getTime()) {
							if(AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){
								log.info(" **  DeplyCancelQueue auto cancel order by subNumber={} ** ",sub.getSubNumber());
								Date date = new Date();
								sub.setUpdateDate(date);
								sub.setStatus(OrderStatusEnum.CLOSE.value());
								sub.setCancelReason("超时未支付");
								subService.cancleOrder(sub);
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					 log.info("自动取消订单 error {} ",order.getSubNumber());  
				}finally{
					delayCancelHelper.remove(order);
				}
           }  
        });  
		
		//查找需要入队的订单  
		List<Sub> subs=subService.findCancelOrders();
		if(AppUtils.isNotBlank(subs)){
			 log.info("查找需要入队的订单");  
			 for (Sub sub:subs ) {
				 //暂时排除秒杀 和预售的订单 和拍卖订单
				 if((!SubTypeEnum.SECKILL.value().equals(sub.getSubType())) && (!SubTypeEnum.PRESELL.value().equals(sub.getSubType())) 
						 && (!SubTypeEnum.auctions.value().equals(sub.getSubType())) 
						 ){
					 delayCancelHelper.offer(new CancelOrderDto(sub.getSubNumber(),sub.getSubDate().getTime(),expireTime));      
	                 log.info("订单自动入队：{} ", sub.getSubNumber());  
				 }
			 }
		}
		log.info(" DeplyCancelQueueInitProcessor  end .....");
	}

	public void setSubService(SubService subService) {
		this.subService = subService;
	}

	public void setDelayCancelHelper(DelayCancelHelper delayCancelHelper) {
		this.delayCancelHelper = delayCancelHelper;
	}
	
	

}
