/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum FowardPage.
 */
public enum CommonFowardPage implements PageDefinition {
	/** 可变路径 */
	VARIABLE(""),
	
	/** 去往首页**/
	INDEX_QUERY("/index"),
	
	HOME_QUERY("/home"),
    ;

	/** The value. */
	private final String value;

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}


	private CommonFowardPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
