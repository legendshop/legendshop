package com.legendshop.core.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * 没有安装时跳转到安装页面
 * 
 */
public class UnInstallHandler extends AbstractHandler implements Handler {

	/**
	 * 重新到首页
	 */
	public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("plugins/install/index.jsp");
	}

}
