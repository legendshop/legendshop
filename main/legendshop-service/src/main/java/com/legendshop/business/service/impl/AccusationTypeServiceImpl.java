/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.AccusationSubjectDao;
import com.legendshop.business.dao.AccusationTypeDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.model.entity.AccusationType;
import com.legendshop.spi.service.AccusationTypeService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 举报类型服务.
 */
@Service("accusationTypeService")
public class AccusationTypeServiceImpl  implements AccusationTypeService{
	
	@Autowired
    private AccusationTypeDao accusationTypeDao;
    
	@Autowired
    private AccusationSubjectDao accusationSubjectDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public AccusationType getAccusationType(Long id) {
        return accusationTypeDao.getAccusationType(id);
    }

    public void deleteAccusationType(AccusationType accusationType) {
    	accusationSubjectDao.deleteSubjectByTypeId(accusationType.getTypeId());
        accusationTypeDao.deleteAccusationType(accusationType);
    }

    public Long saveAccusationType(AccusationType accusationType) {
        if (!AppUtils.isBlank(accusationType.getTypeId())) {
            updateAccusationType(accusationType);
            return accusationType.getTypeId();
        }
        return (Long) accusationTypeDao.save(accusationType);
    }

    public void updateAccusationType(AccusationType accusationType) {
        accusationTypeDao.updateAccusationType(accusationType);
    }

	@Override
	public List<AccusationSubject> getAllAccusationSubject() {
		return accusationSubjectDao.getAllAccusationSubject();
	}


	@Override
	public PageSupport<AccusationType> getAccusationType(String curPageNO, AccusationType accusationType) {
        CriteriaQuery cq = new CriteriaQuery(AccusationType.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        if(AppUtils.isNotBlank(accusationType.getName())) {        	
        	cq.like("name","%" + accusationType.getName().trim() + "%");
        }
        cq.addDescOrder("recDate");
        PageSupport<AccusationType> ps = accusationTypeDao.queryPage(cq);
		List<AccusationType> typeList = ps.getResultList();
        if(AppUtils.isNotBlank(typeList)){
        	List<AccusationSubject> subjectList = getAllAccusationSubject();
        	for(AccusationType type :typeList){
        		for(AccusationSubject subject:subjectList){
        			type.addSubjectList(subject);
        		}
        	}
        }
        return ps;
	}

	/**
	 * 根据与举报类型 获取关联的举报主题
	 */
	@Override
	public List<AccusationSubject> getSubjectByTypeId(Long typeId) {
		
		return accusationSubjectDao.getSubjectByTypeId(typeId);
	}
}
