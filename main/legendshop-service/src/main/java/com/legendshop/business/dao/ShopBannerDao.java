/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopBanner;

/**
 * 商家默认的轮换图Dao
 *
 */
public interface ShopBannerDao extends GenericDao<ShopBanner, Long> {
     
	public abstract ShopBanner getShopBanner(Long id);
	
	public List<ShopBanner>  getShopBannerByShopId(Long shopId);
	
    public abstract int deleteShopBanner(ShopBanner shopBanner);
	
	public abstract Long saveShopBanner(ShopBanner shopBanner);
	
	public abstract int updateShopBanner(ShopBanner shopBanner);
	
	public abstract PageSupport<ShopBanner> getShopBanner(String curPageNO, Long shopId);
	
 }
