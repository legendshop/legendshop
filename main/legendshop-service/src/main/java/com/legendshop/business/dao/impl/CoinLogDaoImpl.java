/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CoinLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.coin.CoinConsumeDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.vo.CoinVo;
import com.legendshop.util.AppUtils;

/**
 * The Class CouponDaoImpl.
 */
@Repository
public class CoinLogDaoImpl extends GenericDaoImpl<CoinLog, Long> implements CoinLogDao  {
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	/**
	 * 保存金币日志
	 */
	@Override
	public Long saveCoinLog(CoinLog entity){
		return this.save(entity);
	}

	@Override
	public PageSupport<CoinConsumeDto> getRechargeDetailsList(String curPageNO, String userName, String mobile) {
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(userName)){
			map.put("userName", "%"+userName+"%"); 
		}
		if(AppUtils.isNotBlank(mobile)){
			map.put("mobile", "%"+mobile+"%"); 
		}
		SimpleSqlQuery hql = new SimpleSqlQuery(CoinConsumeDto.class);
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("coin.queryCountRechargeLog", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("coin.queryRechargeLog", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<CoinConsumeDto> getConsumeDetailsListPage(String curPageNO, String userName, String mobile) {
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(userName)){
			map.put("userName", "%"+userName+"%"); 
		}
		if(AppUtils.isNotBlank(mobile)){
			map.put("mobile", "%"+mobile+"%"); 
		}
		SimpleSqlQuery hql = new SimpleSqlQuery(CoinConsumeDto.class);
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("coin.queryCountConsumeDetails", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("coin.queryConsumeDetails", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<CoinLog> getUserCoinPage(String curPageNO, String userId) {
		CriteriaQuery cq=new CriteriaQuery(CoinLog.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public CoinVo findUserCoin(String userId, String curPageNO) {
		UserDetail userDetail=userDetailDao.getUserDetailByIdNoCache(userId);
		CoinVo coinVo=new CoinVo();
		if(AppUtils.isNotBlank(userDetail)){
			coinVo.setCoin(userDetail.getUserCoin());//金币剩余数量
		}
		CriteriaQuery cq=new CriteriaQuery(CoinLog.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		PageSupport<CoinLog> coinLog = queryPage(cq);
		coinVo.setCoinLog(coinLog);
		return coinVo;
	}

	public void setUserDetailDao(UserDetailDao userDetailDao) {
		this.userDetailDao = userDetailDao;
	}
	
}







