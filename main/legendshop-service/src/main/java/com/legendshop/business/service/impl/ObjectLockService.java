package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ObjectLockDao;
import com.legendshop.model.constant.ObjectLockEnum;

/**
 * 对象锁
 * @author newway
 *
 */
@Service("objectLockService")
public class ObjectLockService {
	
	@Autowired
	private ObjectLockDao objectLockDao;
	
	/**
	 * 加载行锁
	 * @return
	 */
	public Long loadDBLock(ObjectLockEnum prod, Long prodId){
		return objectLockDao.loadDBLock(prod, prodId);
	}
	
	/**
	 * 取消行锁
	 * @return
	 */
	public void releaseDBLock(ObjectLockEnum prod, Long prodId){
		objectLockDao.releaseDBLock(prod, prodId);
	}
}
