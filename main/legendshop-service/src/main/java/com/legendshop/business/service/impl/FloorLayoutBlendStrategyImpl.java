package com.legendshop.business.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.business.dao.SubFloorDao;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;


/**
 *【混合模式】
 */
@Service("floorLayoutBlendStrategy")
public class FloorLayoutBlendStrategyImpl implements FloorLayoutStrategy {

	@Autowired
	private SubFloorDao subFloorDao;
	
	@Autowired
	private FloorDao floorDao;
	
	@Autowired
	private FloorItemDao floorItemDao;
	
	@Autowired
	private FloorSubItemDao floorSubItemDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Override
	public void deleteFloor(Floor floor) {
		
		
		//删除子楼层的产品
    	List<SubFloor> subFloors = subFloorDao.getSubFloorByfId(floor.getFlId());
    	for(SubFloor subFloor : subFloors){
    		subFloorDao.deleteSubFloorItems(subFloor.getId()); 
    	}
    	//删除子楼层
    	floorDao.deleteSubFloors(floor.getFlId());
    	
    	 //装载商品分类的楼层信息
        List<FloorItem> floorSortItemList =floorItemDao.getAllSortFloorItem(floor.getFlId());
        if(AppUtils.isNotBlank(floorSortItemList)){
        	for(FloorItem fI:floorSortItemList){
            	List<FloorSubItem> floorSubItemList = floorSubItemDao.getAllFloorSubItem(fI.getFiId());
            	if(AppUtils.isNotBlank(floorSubItemList)){
            		floorSubItemDao.deleteFloorSubItem(floorSubItemList);
             	}
           }
        }
        
        //删除装载的广告信息;
        FloorItem leftAdv = floorItemDao.getAdvFloorItem(floor.getFlId(), FloorTemplateEnum.LEFT_ADV.value(),FloorLayoutEnum.WIDE_BLEND.value());
        deleteAdvItem(leftAdv);
        
        //删除FloorItem中floorId = floor.getFlId() 但类型不为'ST'的floorItem
    	floorItemDao.deleteFloorItem(floor.getFlId());
    	//最后删除自己本身
        floorDao.deleteFloor(floor);
        
	}
	
	 private void deleteAdvItem(FloorItem leftAdv ){
	    	if(leftAdv==null){
	    		return;
	    	}
	    	if(AppUtils.isNotBlank(leftAdv.getPicUrl())){
	    		if(AppUtils.isNotBlank(leftAdv.getPicUrl())){
					//delete  attachment
					attachmentManager.delAttachmentByFilePath(leftAdv.getPicUrl());
					
					//delete file image
					attachmentManager.deleteTruely(leftAdv.getPicUrl());
	    		}
			
			}
	}

	@Override
	public FloorDto findFloorDto(Floor floor) {
		
		FloorDto floorDto=new FloorDto();
		floorDto.setCssStyle(floor.getCssStyle());
		floorDto.setFlId(floor.getFlId());
        floorDto.setLayout(floor.getLayout());
        floorDto.setName(floor.getName());
        floorDto.setSeq(floor.getSeq());
		
		//装载商品分类的楼层信息
        //3. 装载商品分类的楼层信息
        List<FloorItem> floorSortItemList = floorItemDao.getUserSortFloorItem(floor.getFlId(),FloorLayoutEnum.WIDE_BLEND.value());
        if(AppUtils.isNotBlank(floorSortItemList)){
        	floorDto.addFloorItems(FloorTemplateEnum.SORT.value(), floorSortItemList);
		}
		
		//2. 子楼层和其下面的所有产品
		List<SubFloor> subFloorList = subFloorDao.getSubFloorAndProduct(floor.getFlId());
    	if(AppUtils.isNotBlank(subFloorList)){
    		for(SubFloor subfloor : subFloorList){
        		floorDto.addSubFloor(subfloor);
    		}
    	}		
    	
        //3/装载广告
        FloorItem leftAdv = floorItemDao.getAdvFloorItem(floor.getFlId(), FloorTemplateEnum.LEFT_ADV.value(),FloorLayoutEnum.WIDE_BLEND.value());
		if(AppUtils.isNotBlank(leftAdv)){
			 List<FloorItem> items=new LinkedList<FloorItem>();
			 items.add(leftAdv);
			 floorDto.addFloorItems(FloorTemplateEnum.LEFT_ADV.value(), items);
		}
        
		return floorDto;
	}
}
