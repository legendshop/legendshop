/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.SubFloorItem;

/**
 * 首页主楼层子内容Dao.
 */
public interface FloorSubItemDao extends GenericDao<FloorSubItem, Long>{
     
	public abstract FloorSubItem getFloorSubItem(Long id);
	
    public abstract void deleteFloorSubItem(FloorSubItem floorSubItem);
	
	public abstract Long saveFloorSubItem(FloorSubItem floorSubItem);
	
	public abstract void updateFloorSubItem(FloorSubItem floorSubItem);
	
	public abstract List<FloorSubItem> getAllFloorSubItem(Long id);

	public abstract List<FloorSubItem> getUserFloorSubItem();
	
	public abstract void deleteFloorSubItemById(Long floorId);

	public abstract List<SubFloorItem> findSubFloorItem(Long flId,
			String layout, int limit);

	public abstract void deleteFloorSubItem(List<FloorSubItem> floorSubItemList);

	public abstract PageSupport<FloorSubItem> getFloorSubItemPage(String curPageNO);
	
 }
