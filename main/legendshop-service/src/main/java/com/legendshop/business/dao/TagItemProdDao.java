/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.TagItemProd;

/**
 *标签里的商品
 */
public interface TagItemProdDao extends GenericDao<TagItemProd, Long> {

	public abstract TagItemProd getTagItemProd(Long id);
	
    public abstract int deleteTagItemProd(TagItemProd tagItemProd);
	
	public abstract Long saveTagItemProd(TagItemProd tagItemProd);
	
	public abstract int updateTagItemProd(TagItemProd tagItemProd);
	
	public abstract List<TagItemProd> getTagItemProdsByPid(Long prodId);

	public abstract void deleteAllByProductId(Long prodId);

	public abstract void compareProdTags(Long prodId, String[] items);
	
 }
