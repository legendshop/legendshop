/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DrawWXUserDao;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.spi.service.DrawWXUserService;

/**
 * 微信用户抽奖服务实现类.
 */
@Service("drawWXUserService")
public class DrawWXUserServiceImpl implements DrawWXUserService {
	
	@Autowired
	private DrawWXUserDao drawWXUserDao;

	/**
	 * Gets the gzuser info by user id.
	 *
	 * @param userId the user id
	 * @return the gzuser info by user id
	 * @Description: TODO
	 * @date 2016-4-28
	 */
	@Override
	public WeixinGzuserInfo getGzuserInfoByUserId(String userId) {
		return drawWXUserDao.getGzuserInfoByUserId(userId);
	}

}
