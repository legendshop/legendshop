/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ShopPerm;
import com.legendshop.model.entity.ShopPermId;

/**
 *商家权限服务Dao
 */
public interface ShopPermDao extends Dao<ShopPerm, Long> {
     
	public abstract ShopPerm getShopPerm(Long id);
	
    public abstract int deleteShopPerm(ShopPerm shopPerm);
	
	public abstract Long saveShopPerm(ShopPerm shopPerm);
	
	public abstract int updateShopPerm(ShopPerm shopPerm);
	
	public abstract void saveShopPerm(List<ShopPerm> shopPermList);

	public abstract List<String> getShopPermsByRoleId(Long shopRoleId);

	void deleteUserRole(List<ShopPerm> shopPermList);

	/** 根据职位id删除相关联的职位权限  */
	public abstract void deleteShopPermByRoleId(Long id);

	/** 根据职位Id和菜单标识获取权限 */
	public abstract String getFunction(Long shopRoleId, String label);

	/**
	 * 根据职位id获取所有权限
	 * @param shopRoleId 职位ID
	 */
	public abstract List<ShopPermId> queryShopPermsByRoleId(Long shopRoleId);

	
	/**
	 * 根据职位ID与菜单名称更新子权限
	 * @param function 子权限
	 * @param shopRoleId 职位ID
	 * @param menuLable 菜单名称
	 */
	public abstract void updateFunction(String function, Long shopRoleId, String menuLable);
	
 }
