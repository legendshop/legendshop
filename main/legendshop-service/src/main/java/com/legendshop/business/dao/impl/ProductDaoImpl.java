/**
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.ProductIdUpdate;
import com.legendshop.base.cache.ProductUpdate;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.ImgFileDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.constant.SupportDistEnum;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.dto.stock.StockArmDto;
import com.legendshop.model.entity.AdminDashboard;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.util.AppUtils;

/**
 * 产品Dao.
 */
@Repository("productDao")
public class ProductDaoImpl extends GenericDaoImpl<Product, Long> implements ProductDao {

	private static final String getHotViewProdSQL = "SELECT prod.prod_id AS prodId, prod.category_id AS categoryId, prod.shop_first_cat_id AS shopFirstCatId,prod.shop_second_cat_id AS shopSecondCatId,"
			+ "prod.shop_third_cat_id AS shopThirdCatId,prod.name,prod.pic,prod.price,prod.cash,prod.proxy_price AS proxyPrice,prod.views,prod.buys,prod.comments ,prod.rec_date as recDate,prod.actual_stocks AS actualStocks FROM ls_prod prod, ls_shop_detail sd  "
			+ "where  prod.shop_id = sd.shop_id  and prod.status = 1  and sd.status = 1  and prod.prod_type ='P'  and prod.shop_id = ? "
			+ "order by prod.buys, prod.views desc";

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(ProductDaoImpl.class);

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Autowired
	private ImgFileDao imgFileDao;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private CategoryManagerService categoryManagerService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.ProductDao#getCommendProd(java.lang.
	 * String, int)
	 */
	@Override
	@Cacheable(value = "ProductDetailList")
	public List<Product> getCommendProd(final Long shopId, final int total) {
		log.debug("getCommendProd, shopId = {}", shopId);
		return queryLimit(ConfigCode.getInstance().getCode("prod.getCommend"), Product.class, 0, total, shopId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.spi.dao.ProductDao#getCommendProdBySort(java.lang.String,
	 * java.lang.Long, int)
	 */
	@Cacheable(value = "ProductDetailList")
	public List<Product> getCommendProdBySort(final String shopName, final Long sortId, final int total) {
		log.debug("getCommendProd, shopName = {}, sortId = {}", shopName, sortId);
		return queryLimit(ConfigCode.getInstance().getCode("prod.getCommendBySort"), Product.class, 0, total, shopName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.ProductDao#getNewestProd(java.lang.
	 * String , int)
	 */
	@Override
	@Cacheable(value = "ProductDetailList")
	public List<Product> getNewestProd(final Long shopId, final int total) {
		return queryLimit(ConfigCode.getInstance().getCode("prod.getNewestProd"), Product.class, 0, total, shopId);
	}

	@Override
	public PageSupport<ProductDetail> getAdvancedSearchProds(Integer curPageNO, ProductSearchParms parms) {
		Integer pageSize = parms.getPageSize() == null ? 10 : parms.getPageSize();
		String curpage = AppUtils.isBlank(curPageNO) ? "1" : curPageNO.toString();
		SimpleSqlQuery query = new SimpleSqlQuery(ProductDetail.class, pageSize, curpage);
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(parms.getKeyword())) {
			map.put("prodName", "%" + parms.getKeyword() + "%");
		}
		map.put("startPrice", parms.getStartPrice());
		map.put("endPrice", parms.getEndPrice());
		if (AppUtils.isNotBlank(parms.getHasProd()) && parms.getHasProd()) {
			map.put("hasProd", 0); // prod.actualStocks > $hasProd$ hasProd = 0
		}
		map.put("provinceid", parms.getProvinceid());
		map.put("cityid", parms.getCityid());
		map.put("areaid", parms.getAreaid());
		if (AppUtils.isNotBlank(parms.getCategoryId())) {
			TreeNode treeNode = categoryManagerService.getTreeNodeById(parms.getCategoryId());

			if (AppUtils.isBlank(treeNode)) {
				map.put("categoryId", parms.getCategoryId());
			} else {
				List<TreeNode> nodes = treeNode.getJuniors();
				if (AppUtils.isNotBlank(nodes)) {
					StringBuffer ca = new StringBuffer();
					ca.append(parms.getCategoryId() + ",");
					for (int i = 0; i < nodes.size(); i++) {
						ca.append(nodes.get(i).getSelfId()).append(",");
					}
					ca.setLength(ca.length() - 1);
					map.put("categoryId", ca.toString());
				} else {
					map.put("categoryId", parms.getCategoryId());
				}
			}
		}

		if (AppUtils.isNotBlank(parms.getProp())) {
			String brandStr[] = parms.getProp().split(":");
			if (brandStr.length == 2 && brandStr[0].equals("brandId")) {
				map.put("brandId", brandStr[1]);
			}
		}

		if (AppUtils.isNotBlank(parms.getOrders())) {
			String[] order = StringUtils.split(parms.getOrders(), ",");
			if (order != null && order.length == 2) {
				map.put("orderByAndDir", "order by " + order[0] + " " + order[1]);
			}
		}

		String QueryNsortCount = ConfigCode.getInstance().getCode("prod.getAdvancedSearchProdCount", map);
		String QueryNsort = ConfigCode.getInstance().getCode("prod.getAdvancedSearchProd", map);
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		map.remove("orderByAndDir");
		map.remove("categoryId");
		query.setParam(map.toArray());

		PageSupport<ProductDetail> ps = querySimplePage(query);
		return ps;
	}

	@Override
	public PageSupport getAdvancedSearchProds(SimpleSqlQuery sqlQuery) {
		PageSupport ps = querySimplePage(sqlQuery);
		return ps;
	}

	/**
	 * 查询热门商品
	 */
	@Override
	@Cacheable(value = "ProductDetailList")
	public List<Product> getHotOn(Long shopId, Long categoryId) {
		if (AppUtils.isBlank(categoryId)) {
			return Collections.EMPTY_LIST;
		}
		return queryLimit(ConfigCode.getInstance().getCode("prod.getHotOnProd"), Product.class, 0, 21, shopId, categoryId);
	}
	
	/**
	 * 查询热门商品
	 */
	@Override
	public List<Product> getHotAll(Integer max) {
		if (AppUtils.isBlank(max)) {
			return Collections.EMPTY_LIST;
		}
		return queryLimit(ConfigCode.getInstance().getCode("prod.getHotAll"), Product.class,0,max);
	}

	/**
	 * 查看某个商城的热门商品 ->查看整个商城的热门商品
	 * 
	 */
	@Override
	public List<Product> getHotViewProd(Long shopId, Integer number) {
		return queryLimit(getHotViewProdSQL, Product.class, 0, number, shopId);
	}

	/**
	 * 获取某个分类下的热门商品
	 */
	@Override
	@Cacheable(value = "ProductDetailList")
	public List<Product> getHotComment(Long shopId, Long categoryId, int maxNum) {
		return queryLimit(ConfigCode.getInstance().getCode("prod.getHotComment"), Product.class, 0, maxNum, shopId, categoryId);
	}

	/**
	 * 根据Id获取商品
	 */
	@Override
	@Cacheable(value = "Product", key = "#prodId")
	public Product getProduct(Long prodId) {
		return getById(prodId);
	}

	/**
	 * 根据Id获取商品 不从缓存中取值
	 */
	public Product getProductById(Long prodId) {
		return getById(prodId);
	}

	/**
	 * 更新商品查看数，在查看商品详情页时增加
	 * 
	 */
	@Override
	public void updateProdViews(Long prodId) {
		update("update ls_prod set views = views+1 where prod_id = ?", prodId);
	}

	/**
	 * 更新产品并发布产品更新事件，通知Lucene等更新Index
	 */
	@Override
	@ProductUpdate
	public void updateProduct(Product product) {
		updateOnly(product);
	}

	/**
	 * 更新产品并更新缓存
	 */
	@Override
	@ProductUpdate
	public void updateOnly(Product product) {
		update(product);
	}

	/**
	 * 保存产品
	 * 
	 */
	@Override
	public Long saveProduct(Product product) {
		return save(product);
	}

	/**
	 * 根据商品Id删除商品
	 */
	@Override
	public String deleteProd(Long shopId, Long prodId, boolean isAdmin) {
		if (prodId == null) {
			return "商品ID不能为空";
		}
		// 删除商品
		Product product = getProduct(prodId);
		if (product == null) {
			log.warn("Product with Id {} does not exists ", prodId);
			return "找不到该商品";
		}
		// 检查权限
		if (!isAdmin) {
			if (!product.getShopId().equals(shopId)) {
				log.warn("can not delete Id {} does not belongs to you", prodId);
				return "您沒有修改所有用户数据的权限，不能删除该商品，请登录超级管理员";
			}
		}

		// 检查是否有未处理的举报
		int prodAccCount = this.get("select count(1) from ls_accusation la,ls_prod lp where la.prod_id=lp.prod_id and la.status=0 and lp.prod_id=?",
				Integer.class, prodId);
		if (prodAccCount > 0) {
			return "该商品还有未处理的举报信息，不能删除";
		}

		// 删除产品收藏
		update("delete from ls_favorite where prod_id = ?", prodId);

		// 删除产品咨询
		update("delete from ls_prod_cons where prod_id = ?", prodId);

		// 删除相关产品
		// relProductDao.deleteRelProduct(product.getProdId(),
		// product.getUserName());

		// 删除相关图片 的引用
		List<ImgFile> imgFileList = imgFileDao.getAllProductPics(product.getProdId());
		if (AppUtils.isNotBlank(imgFileList)) {
			for (ImgFile imgFile : imgFileList) {
				imgFileDao.deleteImgFile(imgFile);
			}
		}

		// 删除商品评论的回复
		update("delete from ls_prod_reply  where prod_comm_id in (select id from ls_prod_comm where prod_id = ?)", prodId);
		// 删除商品评论
		update("delete from ls_prod_comm where prod_id = ?", prodId);
		// 删除评论统计
		update("delete from ls_prod_comm_stat where prod_id = ?", prodId);
		// 删除举报
		update("delete from ls_accusation where prod_id = ?", prodId);
		// 删除sku图片
		update("delete from ls_prop_image where prod_id = ?", prodId);
		// 删除sku
		update("delete from ls_sku where prod_id = ?", prodId);

		//删除购物车的信息 20180317 fix by newway, 针对其他的遗留商品可以这么做： DELETE FROM ls_basket WHERE basket_id IN (SELECT a.basket_id FROM (SELECT b.basket_id FROM ls_basket b WHERE NOT EXISTS (SELECT 1 FROM ls_prod p WHERE p.prod_id = b.prod_id)) a );
		update("delete from ls_basket where prod_id = ?", prodId);
		// 删除商品记录
		deleteProd(product);
		// 更新商城的产品数量
		shopDetailDao.updateShopDetailProdNum(product.getShopId());

		return Constants.SUCCESS;
	}

	/**
	 * 删除商品记录
	 */
	@ProductUpdate
	public void deleteProd(Product product) {
		delete(product);
	}

	/**
	 * 获取商品
	 */
	@Override
	public Product getProd(Long prodId, Long shopId) {
		return getByProperties(new EntityCriterion().eq("prodId", prodId).eq("shopId", shopId));
	}

	/**
	 * 获取商品的动态参数
	 */
	@Override
	public String getProdParameter(Long prodId) {
		return get("select prod.parameter from ls_prod prod where prod.prod_id = ?", String.class, prodId);
	}

	/**
	 * 获取热门推荐商品
	 */
	@Cacheable(value = "ProductDetailList")
	public List<Product> getHotRecommend(Long shopId, Long sortId, int maxNum) {
		if (AppUtils.isBlank(sortId)) {
			return Collections.EMPTY_LIST;
		}
		return queryLimit(ConfigCode.getInstance().getCode("prod.getHotRecommend"), Product.class, 0, maxNum, shopId, sortId);
	}

	/**
	 * 装载全局分类
	 * 
	 */
	@Override
	@Cacheable(value = "ProductDetail", key = "#prodId")
	public ProductDetail getProdDetail(final Long prodId) {
		List<ProductDetail> list = null;
		String sql = ConfigCode.getInstance().getCode("prod.getGlobalProdDetail");
		log.debug("getProdDetail run sql {}, prodId = {}", sql, prodId);
		list = query(sql, new Object[] { prodId }, new ProductDetailRowMapper());
		if (AppUtils.isBlank(list)) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 根据商品Id列表获取商品详情
	 */
	@Override
	public List<ProductDetail> getProdDetail(Long[] prodId) {
		List<Long> postIdList = new ArrayList<Long>();
		StringBuffer sql = new StringBuffer(ConfigCode.getInstance().getCode("prod.getProdDetailList"));
		for (int i = 0; i < prodId.length; i++) {
			if (prodId[i] != null) {
				sql.append("?,");
				postIdList.add(prodId[i]);
			}
		}
		if (postIdList.isEmpty()) {
			return new ArrayList<ProductDetail>();
		}

		sql.setLength(sql.length() - 1);
		sql.append(")");
		if (log.isDebugEnabled()) {
			log.debug("getProdDetail run sql {}, param {}", sql.toString(), postIdList.toArray());
		}
		return super.query(sql.toString(), postIdList.toArray(), new ProductDetailRowMapper());
	}

	/**
	 * 商品详情RowMapper.
	 */
	class ProductDetailRowMapper implements RowMapper<ProductDetail> {

		@Override
		public ProductDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
			ProductDetail product = new ProductDetail();
			product.setProdId(rs.getLong("prodId"));
			product.setVersion(rs.getInt("version"));
			product.setName(rs.getString("name"));
			product.setPrice(rs.getDouble("price"));
			product.setCash(rs.getDouble("cash"));
			product.setProxyPrice(rs.getDouble("proxy_price"));
			product.setCarriage(rs.getDouble("carriage"));
			product.setBrief(rs.getString("brief"));
			product.setContent(rs.getString("content"));
			product.setContentM(rs.getString("contentM"));
			product.setViews(rs.getInt("views"));
			product.setBuys(rs.getInt("buys"));
			product.setRecDate(rs.getTimestamp("rec_date"));
			product.setPic(rs.getString("pic"));
			product.setStatus(rs.getInt("status"));
			product.setModifyDate(rs.getDate("modify_date"));
			product.setUserId(rs.getString("user_id"));
			product.setUserName(rs.getString("user_name"));
			product.setStartDate(rs.getTimestamp("start_date"));
			product.setEndDate(rs.getTimestamp("end_date"));
			product.setStocks(rs.getInt("stocks"));
			product.setProdType(rs.getString("prod_type"));
			product.setKeyWord(rs.getString("key_word"));
			product.setMetaTitle(rs.getString("meta_title"));
			product.setMetaDesc(rs.getString("meta_desc"));
			product.setParameter(rs.getString("parameter"));
			product.setUserParameter(rs.getString("userParameter"));
			product.setBrandId(rs.getLong("brand_id"));
			product.setBrandName(rs.getString("brand_name"));
			product.setProdType(rs.getString("prod_type"));
			product.setModelId(rs.getString("model_id"));
			product.setComments(rs.getInt("comments"));
			product.setReviewScores(rs.getInt("reviewScores"));
			product.setShopId(rs.getLong("shop_id"));
			product.setWeight(rs.getDouble("weight"));
			product.setVolume(rs.getDouble("volume"));
			product.setCategoryId(rs.getLong("categoryId"));
			product.setCategoryName(rs.getString("categoryName"));
			product.setTransportId(rs.getLong("transport_id"));
			product.setSupportTransportFree(rs.getInt("support_transport_free"));
			product.setTransportType(rs.getInt("transport_type"));
			product.setEmsTransFee(rs.getDouble("ems_trans_fee"));
			product.setMailTransFee(rs.getDouble("mail_trans_fee"));
			product.setExpressTransFee(rs.getDouble("express_trans_fee"));
			product.setAfterSaleId(rs.getLong("afterSaleId"));
			product.setSupportDist(rs.getInt("supportDist"));
			product.setDistCommisRate(rs.getDouble("distCommisRate"));
			product.setAcitveId(rs.getInt("activeId"));
			product.setIsGroup(rs.getInt("isGroup"));
            product.setProVideoUrl(rs.getString("proVideoUrl"));

			if (product.getReviewScores() != 0 && product.getComments() != 0) {
				product.setAvgScores(Math.round(product.getReviewScores() / product.getComments())); // 四舍五入
			} else {
				product.setAvgScores(0);
			}
			return product;
		}
	}



	/**
	 * 加行锁查找库存
	 */
	@Override
	public Integer getStocksByLockMode(Long prodId) {
		return get("select stocks from ls_prod where prod_id = ? for update", Integer.class, prodId);
	}

	/**
	 * 加行锁查找库存
	 */
	@Override
	public Integer getStocksByMode(Long prodId) {
		return get("select stocks from ls_prod where prod_id = ? ", Integer.class, prodId);
	}

	/**
	 * 更新实际库存
	 */
	@Override
	@ProductIdUpdate
	public void updateProductActualStocks(Long prodId, int stocks) {
		update("update ls_prod set actual_stocks = ? where prod_id = ?", stocks, prodId);
	}

	@Override
	public synchronized void updateReviewScoresAndComments(Integer scores, Integer comments, Long prodId) {
		update("UPDATE ls_prod SET reviewScores = reviewScores + ?, comments = ? WHERE prod_id = ?", scores, comments, prodId);
	}

	@Override
	@ProductIdUpdate
	public void updateStatus(Integer status, Long prodId, String auditOpin) {
		if (AppUtils.isBlank(auditOpin)) {
			update("update ls_prod set pre_status=status, status = ? where prod_id = ?", status, prodId);
		} else {
			update("update ls_prod set pre_status=status, status = ? ,audit_opinion = ? where prod_id = ?", status, auditOpin, prodId);
		}
	}

	@Override
	public List<ProdParamDto> getPropGroupByProdId(Long prodId) {
		String sql = " select lpp.prop_id as propId,lpg.name as groupName from ls_prod lp,ls_category lc, ls_prod_type_param lpp "
				+ " left join ls_param_group lpg on lpp.group_id=lpg.id" + " where lp.category_id=lc.id and lc.type_id=lpp.type_id "
				+ "and lp.prod_id=? order by lpg.seq,lpp.seq";
		return this.query(sql, ProdParamDto.class, prodId);
	}

	@Override
	@Cacheable(value = "ProductList")
	public List<Product> getHotsaleProdByCategoryId(Long categoryId, int total) {
		return queryLimit(ConfigCode.getInstance().getCode("prod.getHotsaleByCategory"), Product.class, 0, total, categoryId);
	}


	@Override
	public List<AdminDashboard> queryAdminDashboard() {
		return this.query("select dashboard_name as dashboardName , value as value from ls_admin_dashboard", AdminDashboard.class);
	}

	@Override
	@ProductIdUpdate
	public void updateProd(Long prodId, String sql, Map<String, Object> params) {
		this.updateNamedMap(sql, params);
	}

	@Override
	@ProductIdUpdate
	public void clearProductDto(Long prodId) {
		log.debug("Clear product dto cache by prodId {}", prodId);

	}

	/**
	 * 支付完成后，更新商品购买数， 包括普通订单，预存款和预售的回调
	 */
	@Override
	public void updateBuys(long buys, Long prodId) {
		this.update("UPDATE ls_prod SET ls_prod.buys=? WHERE ls_prod.prod_id=?", buys, prodId);
	}

	@Override
	public List<SalesRankDto> getSalesRank(Long shopId, Date startDate) {
		String sql = "select a.prod_id as prodId,a.name as prodName,a.pic as prodPic, sum(basket_count) as buys from ( "
				+ "select p.pic, p.name,p.prod_id,si.basket_count from ls_sub_item si, " + "ls_prod p where p.prod_id = si.prod_id and p.shop_id = ? "
				+ "AND sub_item_date > ? ) a group by a.prod_id,a.name,a.pic  order by buys desc ";

		return queryLimit(sql, SalesRankDto.class, 0, 5, shopId, startDate);
	}

	/**
	 * 获取商品的数量
	 */
	@Override
	public List<KeyValueEntity> getProdCounts(Long shopId) {

		List<KeyValueEntity> result = this.query("select status,count(prod_id) as value from ls_prod where shop_id = ? and prod_type = 'P'  group by status ",
				new Object[] { shopId }, new RowMapper<KeyValueEntity>() {
			@Override
			public KeyValueEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new KeyValueEntity(String.valueOf(rs.getLong("status")), rs.getString("value"));
			}
		});

		return result;
	}

	@Override
	public void updateProdVersion(Long prodId) {
		this.update("update ls_prod set version=version+1 where prod_id=?", prodId);
	}

	@Override
	@ProductIdUpdate
	public void updateProdType(Long prodId, String type, Long seckillId) {
		this.update("UPDATE ls_prod set prod_type=?,active_id=? WHERE prod_id=?", new Object[] { type, seckillId, prodId });
	}

	@Override
	@ProductUpdate
	public void updateProdDistCommis(Product product) {
		this.update("update ls_prod set support_dist=?,dist_commis_rate=?,modify_date=? where prod_id=?", product.getSupportDist(), product.getDistCommisRate(),
				new Date(), product.getProdId());
	}

	/**
	 * 获取拍卖对应的商品或者单品
	 */
	@Override
	public AuctionsDto getAuctionProd(Long prodId, Long skuId) {
		String sql = "SELECT s.name AS prodName,p.pic AS prodPic,s.price AS prodPrice,s.cn_properties AS cnProperties,p.status AS prodStatus,s.status AS skuStatus  FROM ls_prod p LEFT JOIN ls_sku s ON p.prod_id=s.prod_id WHERE p.prod_id=? AND s.sku_id=?";
		Object[] par = new Object[] { prodId, skuId };
		/*if (AppUtils.isBlank(skuId) || skuId.intValue() == 0) {
			sql = "select p.name as prodName,p.pic as prodPic,p.cash as prodPrice from ls_prod p where prod_id=?";
			par = new Object[] { prodId };

		} else {
			sql = "SELECT s.name AS prodName,p.pic AS prodPic,s.price AS prodPrice FROM ls_prod p LEFT JOIN ls_sku s ON p.prod_id=s.prod_id WHERE p.prod_id=? AND s.sku_id=?";
			par = new Object[] { prodId, skuId };
		}*/
		AuctionsDto result = get(sql, par, new RowMapper<AuctionsDto>() {
			public AuctionsDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				AuctionsDto auction = new AuctionsDto();
				auction.setProdName(rs.getString("prodName"));
				auction.setProdPic(rs.getString("prodPic"));
				auction.setProdPrice(rs.getString("prodPrice"));
				auction.setCnProperties(rs.getString("cnProperties"));
				auction.setProdStatus(rs.getLong("prodStatus"));
				auction.setSkuStatus(rs.getLong("skuStatus"));
				return auction;
			}
		});
		if (AppUtils.isBlank(result)) {
			return null;
		}
		return result;
	}

	@Override
	public int checkStock(Long prodId, Long skuId) {
		String sql = null;
		Object[] par = null;
		if (AppUtils.isBlank(skuId) || skuId.intValue() == 0) {
			sql = "select stocks from ls_prod p where prod_id=?";
			par = new Object[] { prodId };

		} else {
			sql = "SELECT stocks  FROM  ls_sku s WHERE s.sku_id=?";
			par = new Object[] { skuId };
		}
		Long number = getLongResult(sql, par);
		return number.intValue();
	}

	/**
	 * 查询该商城下的所有商品数
	 */
	@Override
	public long getProdTotalCount(Long shopId) {
		String sql = "SELECT COUNT(prod_id) FROM ls_prod WHERE shop_id= ?";
		return this.getLongResult(sql, shopId);
	}

	@Override
	public List<Long> getProdByPage(Long shopId, int currPage, Integer pageSize) {
		return this.queryLimit("SELECT prod_id FROM ls_prod WHERE shop_id= ?", Long.class, (currPage - 1) * pageSize, pageSize, shopId);
	}

	/**
	 * 更新商品的库存 = 所有sku库存总和
	 */
	@Override
	@ProductIdUpdate
	public void updateProdStocks(Long prodId) {
		this.update("UPDATE ls_prod SET stocks = (SELECT SUM(stocks) FROM ls_sku WHERE prod_id =?) WHERE prod_id=?", prodId, prodId);
	}

	/**
	 * 更新商品的库存 = 所有sku库存总和
	 */
	@Override
	@ProductIdUpdate
	public void updateProdAllStocks(Long prodId) {
		this.update("UPDATE ls_prod SET stocks = (SELECT SUM(stocks) FROM ls_sku WHERE prod_id =?), actual_stocks = (SELECT SUM(actual_stocks) FROM ls_sku WHERE prod_id = ?) WHERE prod_id=?", prodId, prodId, prodId);
	}

	@Override
	public Boolean updateSearchWeight(Long prodId, Double searchWeight) {
		return this.update("UPDATE ls_prod SET search_weight = ? WHERE prod_id=?", searchWeight,prodId)>0;
	}

	@Override
	public List<Long> getAllprodIdStr(Long shopId,Integer status) {
		String sql="SELECT prod_id FROM ls_prod WHERE shop_id=? and status=?";
		List<Long> prodList = this.query(sql,new Object[]{shopId,status},(rs,index)->rs.getLong("prod_id"));
		return prodList;
	}
	/**
	 * 更新商品的实际库存 = 所有sku实际库存总和
	 */
	@Override
	public void updateProdActualStocks(Long prodId) {
		this.update("UPDATE ls_prod SET actual_stocks = (SELECT SUM(actual_stocks) FROM ls_sku WHERE prod_id =?) WHERE prod_id=?", prodId, prodId);
	}

	@Override
	public boolean updataIsGroup(Long prodId, Integer status) {
		return this.update("UPDATE ls_prod  SET is_group=? WHERE ls_prod.prod_id=?", status, prodId) > 0;
	}

	@Override
	@Cacheable(value = "Group", key = "#prodId")
	public Group getGroupPrice(Long prodId) {
		return this.get(
				"SELECT g.id AS id ,g.group_price AS groupPrice,g.status AS STATUS,g.end_time AS endTime FROM ls_group g  WHERE g.product_id=? AND g.status=1",
				Group.class, prodId);
	}
	
	@Override
	public Seckill getSeckillPrice(Integer seckillId,Long prodId,Long skuId) {
		
		String sql = "SELECT a.start_time AS startTime,a.end_time AS endTime,s.s_id AS seckillId ,s.seckill_price AS seckillPrice,s.prod_id AS prodId,s.sku_id AS skuId,s.seckill_stock AS seckillStock FROM ls_seckill_prod AS s INNER JOIN ls_seckill_activity a on s.s_id=a.id WHERE s.s_id=? AND s.prod_id=? AND s.sku_id=? AND a.status=1";
		Seckill seckill = this.get(sql,Seckill.class,seckillId, prodId,skuId);
		return seckill;
	}
	
	@Override
	public Auctions getAuctionsPrice(Long prodId,Long skuId) {
		String sql = " SELECT id AS id,start_time AS startTime,end_time AS endTime,floor_price AS floorPrice,sku_id AS skuId,cur_price AS curPrice FROM ls_auctions WHERE prod_id=? AND sku_id=? AND STATUS=1";
		Auctions auctions = this.get(sql,Auctions.class,prodId,skuId);
		return auctions;
	}
	
	@Override
	public PresellProd getPresellProdPrice(Long prodId,Long skuId) {
		String sql = "  SELECT p.id AS id,p.pre_sale_start AS preSaleStart,p.pre_sale_end AS preSaleEnd,s.prsell_price AS prePrice,s.sku_id AS skuId \n" +
				"FROM `ls_presell_prod` p\n" +
				"LEFT JOIN `ls_presell_sku` s\n" +
				"ON p.`id`=s.p_id\n" +
				"WHERE p.prod_id=? AND s.sku_id=?\n" +
				"AND p.STATUS=0";
		PresellProd presellProd = this.get(sql,PresellProd.class,prodId,skuId);
		return presellProd;
	}
	
	
	@Override
	public int getveryBuyNum(Long productId, Long skuId) {
		return this.get("SELECT stocks from ls_sku WHERE prod_id=? and sku_id=? ", Integer.class, productId, skuId);
	}

	/**
	 * 根据skuId分别获取库存
	 */
	@Override
	public List<UpdSkuStockDto> getStockBySkuId(Long skuId) {
		return this.query(
				"SELECT s.prod_id AS prodId,s.sku_id AS skuId,s.stocks AS stocks,s.actual_stocks AS actualStocks,s.name as name ,s.cn_properties as cnProperties FROM ls_sku s WHERE s.sku_id=?",
				UpdSkuStockDto.class, skuId);
	}

	@Override
	/**
	 * 猜你喜欢
	 */
	@Cacheable(value = "GuessYouLikeProd", key = "#categoryId")
	public List<ProductDetail> getLikeProd(Long categoryId) {
		String sql = "SELECT * FROM ls_prod p WHERE p.category_id=? ORDER BY p.buys DESC";
		return this.query(sql, ProductDetail.class, categoryId);
	}

	@Override
	public List<ProductExportDto> findExport(String sql, Object[] args) {
		return this.query(sql, ProductExportDto.class, args);
	}

	@Override
	public List<Product> getProdByCateId(Long id) {
		String sql = "SELECT * FROM ls_prod p WHERE p.category_id=?";
		return this.query(sql, Product.class, id);
	}

	@Override
	public int sumBuys(Long count, Long prodId) {
		return this.update("UPDATE ls_prod SET buys = buys + ? WHERE prod_id = ?", count, prodId);
	}

	@Override
	public List<Product> getZhiboAppProdList(Long shopId) {
		return query(
				"select prod.stocks AS stocks,prod.cash AS price,prod.pic AS pic,prod.name AS name,prod.prod_id AS prodId from ls_prod prod where shop_id =?",
				Product.class, shopId);
	}

	@Override
	public PageSupport<Product> getProductList(String name, String curPageNO,Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(6);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		if (AppUtils.isNotBlank(name)) {
			cq.like("name", name, MatchMode.ANYWHERE);
		}
		if (AppUtils.isNotBlank(shopId)) {
			cq.eq("shopId",shopId);
		}
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> storeProdctList(Long shopId, String curPageNO, Long fireCat, Long secondCat, Long thirdCat, String keyword, String order) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(10);
		if (AppUtils.isNotBlank(fireCat)) {
			cq.eq("shopFirstCatId", fireCat);
		}
		if (AppUtils.isNotBlank(secondCat)) {
			cq.eq("shopSecondCatId", secondCat);
		}
		if (AppUtils.isNotBlank(thirdCat)) {
			cq.eq("shopThirdCatId", thirdCat);
		}
		if (AppUtils.isNotBlank(keyword)) {
			cq.like("name", "%" + keyword + "%");
		}
		if (AppUtils.isNotBlank(order)) {
			String[] orders = order.split(",");
			if (orders != null && orders.length == 2) {
				if (StringUtils.equals("asc", orders[1])) {
					cq.addAscOrder(orders[0]);
					cq.addDescOrder("modifyDate");
				} else {
					cq.addDescOrder(orders[0]);
				}
			}
		} else {
			cq.addDescOrder("comments");
			//cq.addDescOrder("hot");
			cq.addDescOrder("buys");
			cq.addDescOrder("views");
		}
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> newProd(Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Product.class);
		cq.setPageSize(9);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProductListPage(String curPageNO, Product product) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, 6, curPageNO);
		QueryMap map = new QueryMap();

		if (AppUtils.isNotBlank(product.getCategoryId())) {
			map.put("categoryId1", product.getCategoryId());
			map.put("parentId1", product.getCategoryId());
			map.put("categoryId2", product.getCategoryId());
			map.put("parentId2", product.getCategoryId());
		}

		if (AppUtils.isNotBlank(product.getName())) {
			map.put("name", "%"+ product.getName().trim() +"%");
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getFloorProductCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getFloorProduct", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getAuctionProdLayout(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(9);
		cq.eq("shopId", shopId);
		cq.eq("isGroup", 0);
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProductListPage(String curPageNO, Long shopId, Product product) {
		return this.getProductListPage(curPageNO, 5, shopId, product.getName());
	}

	@Override
	public PageSupport<Product> getProductListPage(String curPageNO, int pageSize, Long shopId, String prodName) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, curPageNO);
		query.setPageSize(pageSize);
		QueryMap queryMap = new QueryMap();
		queryMap.put("status", ProductStatusEnum.PROD_ONLINE.value());
		queryMap.put("prodType", ProductTypeEnum.PRODUCT.value());
		queryMap.put("shopId", shopId);
		queryMap.like("name", prodName);
		query.setAllCountString(ConfigCode.getInstance().getCode("prod.getProductListPageCount", queryMap));
		query.setQueryString(ConfigCode.getInstance().getCode("prod.getProductListPage", queryMap));
		query.setParam(queryMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getloadProdListPage(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId", shopId);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getDustbinProdListPage(String curPageNO, String name, String siteName) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(Product.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);
		if (!AppUtils.isBlank(name)) {
			map.like("name", name);
		}
		if (!AppUtils.isBlank(siteName)) {
			map.put("siteName", siteName);
		}
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("prod.getDustbinProdListCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("prod.getDustbinProdList", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<StockAdminDto> getStockcontrol(String curPageNO, String productName) {
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(productName)) {
 			map.put("productName", "%" + productName + "%");
		}
		SimpleSqlQuery hql = new SimpleSqlQuery(StockAdminDto.class);
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("report.getAllStockCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("report.getAllStock", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<Product> getloadSelectProd(Long shopId, String curPageNO, Product product) {
		return getProdductByShopId(curPageNO, product.getName(), shopId);
	}

	@Override
	public PageSupport<Product> getProductFloorLoad(String curPageNO, String name, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, 6, curPageNO);
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(name)) {
			map.like("name", name);
		}
		map.put("shopId", shopId);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getShopCommonsProductCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getShopCommonsProduct", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getGroupProdLayout(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("isGroup", 0);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> groupProdLayout(String curPageNO, Long shopId, Product product) {
//		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
//		cq.setPageSize(5);
//		cq.eq("shopId", shopId);
//		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
////		cq.eq("isGroup", 0); // 是否有参加过团购
//		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());// 普通商品
//		cq.like("name", product.getName(), MatchMode.ANYWHERE);
//		return queryPage(cq);
		SimpleSqlQuery hql = new SimpleSqlQuery(Product.class, curPageNO);
		hql.setPageSize(5);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if (AppUtils.isNotBlank(product.getName())) {
			map.put("productName", " AND p.name LIKE '%" + product.getName() + "%'");
		}
		hql.setAllCountString(ConfigCode.getInstance().getCode("prod.getCountAddProdSkuLayoutList", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("prod.getAddProdSkuLayoutList", map));
		map.remove("productName");
		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<Product> getloadProdListPageByshopId(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId", shopId);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getloadWantDistProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.eq("supportDist", SupportDistEnum.NOT_SUPPORT.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getSellingProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(13);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("shopFirstCatId", product.getShopFirstCatId());
		cq.eq("shopSecondCatId", product.getShopSecondCatId());
		cq.eq("shopThirdCatId", product.getShopThirdCatId());
		cq.eq("brandId", product.getBrandId());
		if(product.getName()!= null)
			cq.like("name", product.getName().trim(), MatchMode.ANYWHERE);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProdInStoreHouse(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(8);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		/*cq.eq("prodType", ProductTypeEnum.PRODUCT.value());*/
		cq.eq("status", ProductStatusEnum.PROD_OFFLINE.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getViolationProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(8);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("prodType", "P");
		cq.eq("status", ProductStatusEnum.PROD_ILLEGAL_OFF.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getAuditingProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(8);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", ProductStatusEnum.PROD_AUDIT.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getAuditFailProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(8);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", ProductStatusEnum.PROD_AUDIT_FAIL.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProdInDustbin(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(8);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", ProductStatusEnum.PROD_DELETE.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getJoinDistProd(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.setPageSize(10);
		cq.eq("supportDist", SupportDistEnum.SUPPORT.value());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getReportForbit(String curPageNO, String userName, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.eq("userName", userName);
		cq.setPageSize(5);
		cq.eq("categoryId", product.getCategoryId());
		cq.eq("brandId", product.getBrandId());
		cq.like("name", product.getName());
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("status", -1);
		return queryPage(cq);
	}

	@Override
	public PageSupport<StockArmDto> getStockwarning(Long shopId, String curPageNO, String productName) {
		SimpleSqlQuery hql = new SimpleSqlQuery(StockArmDto.class);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if (AppUtils.isNotBlank(productName)) {
			map.put("productName", "%" + productName + "%");
		}
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("shop.getCountOfSkuStockArmByShopId", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("shop.getSkuStockArmByShopId", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<Product> queryPage(String curPageNO, int pageSize, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(18);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getSeckillLayer(String curPageNO, Long shopId, int pageSize, Product product) {
		return getProductListPage(curPageNO, pageSize, shopId, product.getName());
	}

	@Override
	public PageSupport<Product> getProductLists(String curPageNO, int pageSize, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(9);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		// 普通商品或打折商品才可以添加到门店,
		// 参与预售的SKU也不能添加到门店,这个在页面做异步校验和添加到门店时再做校验
		Criterion cqs = cq.or(Restrictions.eq("prodType", ProductTypeEnum.PRODUCT.value()), Restrictions.eq("prodType", ProductTypeEnum.DISCOUNT.value()));
		cq.add(cqs);
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getStoreProdcts(Long shopId, String curPageNO, Long fireCat, Long secondCat, Long thirdCat, String keyword, String order) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(12);
		if (AppUtils.isNotBlank(fireCat)) {
			cq.eq("shopFirstCatId", fireCat);	
		}
		if (AppUtils.isNotBlank(secondCat)) {
			cq.eq("shopSecondCatId", secondCat);
		}
		if (AppUtils.isNotBlank(thirdCat)) {
			cq.eq("shopThirdCatId", thirdCat);
		}
		if (AppUtils.isNotBlank(keyword)) {
			cq.like("name", "%" + keyword + "%");
		}
		if (AppUtils.isNotBlank(order)) {
			String[] orders = order.split(",");
			if (orders.length == 2) {
				if (StringUtils.equals("asc", orders[1])) {
					cq.addAscOrder(orders[0]);
				} else {
					cq.addDescOrder(orders[0]);
				}
			}
		} else {
			cq.addDescOrder("buys");
			cq.addDescOrder("views");
		}
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getAddProdSkuLayoutPage(String curPageNO, Product product, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		// 普通商品且非团购才能参与预售
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("isGroup", 0);// 0:非团购
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getAddProdSkuLayout(String curPageNO, Long shopId, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(9);
		cq.eq("shopId", shopId);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		// 普通商品且非团购商品才能参与预售
		cq.eq("prodType", ProductTypeEnum.PRODUCT.value());
		cq.eq("isGroup", 0);// 0非团购
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProductListPage(Product product, String curPageNO, Integer pageSize, DataSortResult result) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(Product.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);

		if (!AppUtils.isBlank(product.getProdId())) {
			map.put("prodId", product.getProdId());
		}
		if (!AppUtils.isBlank(product.getCategoryId())) {
			map.put("categoryId", product.getCategoryId());
		}
		if (!AppUtils.isBlank(product.getBrandId())) {
			map.put("brandId", product.getBrandId());
		}
		if (!AppUtils.isBlank(product.getStatus())) {
			if (product.getStatus() == 4) {
			} else {
				map.put("status", product.getStatus());
			}
		}
		if (!AppUtils.isBlank(product.getName())) {
			map.like("name", product.getName().trim());
		}
		if (!AppUtils.isBlank(product.getSiteName())) {
			map.like("siteName", product.getSiteName().trim());
		}

		if (!AppUtils.isBlank(product.getShopId())) {
			map.put("shopId", product.getShopId());
		}
		if (!AppUtils.isBlank(product.getGroupId())) {
			map.put("groupId", product.getGroupId());
		}

		hql.setParam(map.toArray());
		// 非导出情况
		if (AppUtils.isNotBlank(pageSize)) {
			hql.setPageSize(pageSize);
		}
		// 支持外部排序
		if (result.isSortExternal()) {
			map.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		hql.setAllCountString(ConfigCode.getInstance().getCode("prod.getAdminProdListCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("prod.getAdminProdList", map));
		map.remove(Constants.ORDER_INDICATOR);
		return querySimplePage(hql);
	}


	@Override
	public PageSupport<ProductPurchaseRateDto> getPurchaseRate(String curPageNO, Long shopId, String prodName, DataSortResult result,Date startDate,Date endDate) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductPurchaseRateDto.class, curPageNO);
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(shopId)) {
			map.put("shopId", shopId);
		}
		if (AppUtils.isNotBlank(prodName)) {
			map.like("name", prodName.trim());
		}
		/*延后
		 * map.put("startTime",startDate);
		if(AppUtils.isNotBlank(endDate) ){
			Date endDate1 = DateUtil.getDay(endDate, 1);
			map.put("endTime", endDate1);
		}*/

		// 支持外部排序
		if (result.isSortExternal()) {
			map.put(AttributeKeys.ORDER_INDICATOR, result.parseSeq());
		}
		String querySQL = ConfigCode.getInstance().getCode("report.getPurchaserate", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("report.getPurchaserateCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove(AttributeKeys.ORDER_INDICATOR);// 去掉排序的条件
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getLoadSelectProd(String curPageNO, Product product) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.like("name", product.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Product> getProductListMobile(String curPageNO, Long brandId) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(4);
		cq.eq("brandId", brandId);
		cq.eq("status", Constants.ONLINE);
		return queryPage(cq);
	}

	@Override
	public PageSupport<SalesRankDto> querySimplePage(String curPageNO, Long shopId, String prodName, Date startDate, Date endDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleSqlQuery query = new SimpleSqlQuery(SalesRankDto.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if (AppUtils.isNotBlank(prodName)) {
			map.like("prodName", prodName);
		}
		Date start_Date = null;
		Date end_Date = null;
		try {
			start_Date = dateFormat.parse(dateFormat.format(startDate));
			end_Date = dateFormat.parse(dateFormat.format(endDate));
			Calendar c = Calendar.getInstance();
			c.setTime(end_Date);
			c.add(Calendar.DAY_OF_MONTH, 1);
			end_Date = c.getTime();
		} catch (Exception e) {
			// TODO: handle exception
		}
		map.put("startDate", start_Date);
		map.put("endDate", end_Date);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryProdAnalysisCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.queryProdAnalysis", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 更新库存
	 */
	@Override
	@ProductIdUpdate
	public void updateProductStocks(Long prodId, long stocks) {
		update("update ls_prod set stocks = ? where prod_id = ?", stocks, prodId);
	}

	/**
	 * 更新库存量和实际库存
	 */
	@Override
	public int upStocks(Long prodId, Long stocks) {
		Long result = this.get("SELECT SUM(stocks) FROM ls_sku WHERE prod_id = ?", Long.class, prodId);
		update("update ls_prod set stocks = ?,actual_stocks = ? where prod_id = ?", result, result, prodId);
		return 1;
	}

	@Override
	public Integer batchOffline(List<Long> prodIdList) {
		StringBuffer buffer=new StringBuffer();
		buffer.append("UPDATE ls_prod SET status=2 WHERE prod_id IN(");
		for(Long prodId:prodIdList){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		buffer.append(" and status != -2");
		return update(buffer.toString(),prodIdList.toArray());
	}

	@Override
	public void batchOnlineLine(List<Long> prodIdList) {
		StringBuffer buffer=new StringBuffer();
		buffer.append("UPDATE ls_prod SET status=1 WHERE prod_id IN(");

		for(Long prodId:prodIdList){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		buffer.append(" and status != -2");
		update(buffer.toString(),prodIdList.toArray());
	}


	@Override
	public PageSupport<FullActiveProdDto> getFullActiveProdDto(Long shopId, String curPageNo, String prodName) {
		SimpleSqlQuery hql = new SimpleSqlQuery(FullActiveProdDto.class);
        QueryMap map = new QueryMap();
        map.put("shopId", shopId);
        hql.setPageSize(10);
        if (AppUtils.isBlank(curPageNo)) {
            curPageNo = "1";
        }
        List<Object> args = new ArrayList<Object>();
        args.add(shopId);
        args.add(shopId);
        if (AppUtils.isNotBlank(prodName)) {
            map.put("prodName", prodName);
            args.add("%" + prodName + "%");
        }
        hql.setCurPage(curPageNo);
        hql.setParam(args.toArray());
        hql.setAllCountString(ConfigCode.getInstance().getCode("prod.getFullActiveProdDtoCount", map));
        hql.setQueryString(ConfigCode.getInstance().getCode("prod.getFullActiveProdDto", map));

        return querySimplePage(hql);
	}
 
	@Override
	public Group getGroup(Long activeId) {
		String sql = "SELECT * FROM ls_group WHERE id = ?";
		return get(sql, Group.class, activeId);
	}

	@Override
	@CacheEvict(value = "Group", key = "#group.productId")
	public void update(Group group) {
		String sql = "UPDATE ls_group SET buyer_count = ? WHERE id = ?";
		update(sql, group.getBuyerCount(),group.getId());
	}

	@Override
	public List<MergeGroupSelProdDto> queryMergeGroupProductToSku(Long shopId, Long[] prodIds) {
		String sql ="SELECT p.prod_id AS prodId,k.sku_id AS skuId,p.name AS prodName,p.price AS price,p.cash AS cash,"+
				"p.pic AS prodPic,p.actual_stocks AS actualStocks,p.stocks AS stocks,k.cn_properties AS cnProperties,"+
				"k.price AS skuPrice,k.stocks AS skuStocks,k.actual_stocks AS skuActualStocks,k.name AS skuName,"+
				"k.status AS skuStatus,k.party_code AS partyCode,k.pic AS skuPic FROM "+
				"(SELECT p.* FROM ls_prod p WHERE p.shop_id="+shopId+" AND p.status=1 AND p.prod_id in (";

		StringBuffer sb = new StringBuffer(sql);
		for (int i = 0,length=prodIds.length;i<length;i++) {
			sb.append("?,");
		}
		sb.setLength(sb.length()-1);
		sb.append(")) p LEFT JOIN ls_sku k ON p.prod_id=k.prod_id WHERE k.status=1");

		List<MergeGroupSelProdDto> prodlists = this.query(sb.toString(), MergeGroupSelProdDto.class,prodIds);

		Map<Long,MergeGroupSelProdDto> map = getListMap(prodlists);

		List<MergeGroupSelProdDto> lists = new ArrayList<MergeGroupSelProdDto>();
		for(Long key:map.keySet()){
			MergeGroupSelProdDto st=map.get(key);
			lists.add(st);
		}	

		return lists;
	}

	private Map<Long, MergeGroupSelProdDto> getListMap(List<MergeGroupSelProdDto> prodlists) {
		Map<Long,MergeGroupSelProdDto> map = new HashMap<Long,MergeGroupSelProdDto>();
		for(MergeGroupSelProdDto prod:prodlists){
			if(map.containsKey(prod.getProdId())){
				MergeGroupSelProdDto dto=map.get(prod.getProdId());
				//为SKU prod
				dto.addMergeGroupSelProdDto(prod);
			}else{
				MergeGroupSelProdDto dto=new MergeGroupSelProdDto();

				//共有的
				dto.setProdId(prod.getProdId());
				dto.setProdName(prod.getProdName());
				dto.setPrice(prod.getPrice());
				dto.setCash(prod.getCash());
				dto.setProdPic(prod.getProdPic());
				dto.setStocks(prod.getStocks());
				dto.setActualStocks(prod.getActualStocks());

				dto.addMergeGroupSelProdDto(prod);
				map.put(prod.getProdId(), dto);
			}
		}
		return map;
	}


	private Map<Long, GroupSelProdDto> getListGroupMap(List<GroupSelProdDto> prodlists) {
		Map<Long,GroupSelProdDto> map = new HashMap<Long,GroupSelProdDto>();
		for(GroupSelProdDto prod:prodlists){
			if(map.containsKey(prod.getProdId())){
				GroupSelProdDto dto=map.get(prod.getProdId());
				//为SKU prod
				dto.addMergeGroupSelProdDto(prod);
			}else{
				GroupSelProdDto dto=new GroupSelProdDto();

				//共有的
				dto.setProdId(prod.getProdId());
				dto.setProdName(prod.getProdName());
				dto.setPrice(prod.getPrice());
				dto.setCash(prod.getCash());
				dto.setProdPic(prod.getProdPic());
				dto.setStocks(prod.getStocks());
				dto.setActualStocks(prod.getActualStocks());

				dto.addMergeGroupSelProdDto(prod);
				map.put(prod.getProdId(), dto);
			}
		}
		return map;
	}



	@Override
	public List<MergeGroupSelProdDto> queryMergeGroupProductByMergeId(Long id) {
		String sql ="SELECT m.prod_id AS prodId,m.sku_id AS skuId,m.merge_price AS mergePrice,sku.prodPic,sku.prodName,sku.cash,sku.prodStocks AS stocks,"+
				"sku.pic AS skuPic,sku.cn_properties AS cnProperties,sku.price AS skuPrice,sku.stocks AS skuStocks,sku.name AS skuName,sku.prodPrice AS price,sku.actualStocks "+
				"FROM ls_merge_product m LEFT JOIN "+
				"(SELECT p.prod_id AS prodId,p.pic AS prodPic,p.name AS prodName,p.cash,p.price AS prodPrice,p.stocks AS prodStocks,p.actual_stocks AS actualStocks,s.* "+
				"FROM ls_prod p LEFT JOIN ls_sku s ON p.prod_id=s.prod_id)sku ON m.sku_id=sku.sku_id WHERE m.merge_id=?";

		List<MergeGroupSelProdDto> prodlists = this.query(sql, MergeGroupSelProdDto.class,id);

		Map<Long,MergeGroupSelProdDto> map = getListMap(prodlists);

		List<MergeGroupSelProdDto> lists = new ArrayList<MergeGroupSelProdDto>();
		for(Long key:map.keySet()){
			MergeGroupSelProdDto st=map.get(key);
			lists.add(st);
		}	
		return lists;
	}

	@Override
	public PageSupport<Product> queryMergeGroupProductList(Long shopId, String curPageNO, String prodName) {
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, 5, curPageNO);
		QueryMap map = new QueryMap();
		map.put("status", ProductStatusEnum.PROD_ONLINE.value());
		map.put("shopId", shopId);
		if(AppUtils.isNotBlank(prodName)){
			map.like("prodName", prodName);
		}
		map.put("startTime", new Date());
		map.put("endTime", new Date());
		query.setParam(map.toArray());
		query.setAllCountString(ConfigCode.getInstance().getCode("prod.queryMergeGroupProdCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("prod.queryMergeGroupProd", map));
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> queryMergeGroupProduct(Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, 5, "1");
		QueryMap map = new QueryMap();
		map.put("status", ProductStatusEnum.PROD_ONLINE.value());
		map.put("shopId", shopId);
		map.put("startTime", new Date());
		map.put("endTime", new Date());
		query.setParam(map.toArray());
		query.setAllCountString(ConfigCode.getInstance().getCode("prod.queryMergeGroupProdCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("prod.queryMergeGroupProd", map));
		return querySimplePage(query);
	}

	@Override
	public String getProdPicByMergeId(Long mergeId) {
		String sql = "SELECT p.pic FROM ls_prod p INNER JOIN ls_merge_product m ON p.prod_id = m.prod_id WHERE m.merge_id = ?";
		return get(sql, String.class, mergeId);
	}
	
	@Override
	public List<Product> getProductsTop(Long shopId, Integer prodNums) {
		String sql = "SELECT * FROM ls_prod  WHERE shop_id =  ? and status = 1 ORDER BY prod_id LIMIT ?";
		return query(sql, Product.class, shopId, prodNums);
	}

	

	@Override
	public boolean checkShopSubCategoryUse(Long subCatId) {
		String sql = "SELECT COUNT(*) FROM ls_prod WHERE shop_third_cat_id = ?";
		Integer count = get(sql, Integer.class, subCatId);
		return count>0;
	}

	@Override
	public boolean checkShopCategoryUse(Long shopCatId) {
		String sql = "SELECT COUNT(*) FROM ls_prod WHERE shop_first_cat_id = ?";
		Integer count = get(sql, Integer.class, shopCatId);
		return count>0;
	}

	@Override
	public boolean checkShopNextCategoryUse(Long nextCatId) {
		String sql = "SELECT COUNT(*) FROM ls_prod WHERE shop_second_cat_id = ?";
		Integer count = get(sql, Integer.class, nextCatId);
		return count>0;
	}

	@Override
	public Boolean isAttendActivity(Long prodId) {
		Date date = new Date();
		
		String sql1 = "SELECT COUNT(1) FROM ls_group WHERE product_id = ? and status IN (1,-1)  AND end_time > ?";
		Long count1 = this.getLongResult(sql1, prodId,date);
		String sql2 = "SELECT COUNT(1) FROM ls_seckill_activity sa inner JOIN ls_seckill_prod sp ON sa.id= sp.s_id WHERE sa.status IN (1,-1)  AND sa.end_time > ? AND sp.prod_id = ?";
		Long count2 = this.getLongResult(sql2,date,prodId); 
		String sql3 = "SELECT COUNT(1) FROM ls_presell_prod WHERE prod_id = ? and status IN (-2,-1,0)  AND pre_sale_end > ?";
		Long count3 = this.getLongResult(sql3,prodId,date);
		String sql4 = "SELECT COUNT(1) FROM ls_auctions a WHERE a.prod_id = ? AND a.status IN (-1,1) AND end_time > ?";
		Long count4 = this.getLongResult(sql4, prodId,date);
		String sql5 = "SELECT COUNT(1) FROM ls_merge_group mg inner JOIN ls_merge_product mp ON mg.id= mp.merge_id WHERE mg.status IN (0,1)  AND mg.end_time > ? AND mp.prod_id = ?";
		Long count5 = this.getLongResult(sql5,date,prodId); 
		Long total = count1+count2+count3+count4+count5; 
		return total>0;
	}
	
	
	@Override
	public PageSupport<Product> getCouponProds(Long shopId, List<Long> prodIds, String curPageNO, String orders, String prodName) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNO);
		cq.setPageSize(8);
		if(AppUtils.isNotBlank(shopId)) {
			cq.eq("shopId", shopId);
		}
		if(AppUtils.isNotBlank(prodIds) && prodIds.size() > 0) {
			cq.in("prodId", prodIds.toArray());
		}
		if(AppUtils.isNotBlank(prodName)) {
			cq.like("name", prodName, MatchMode.ANYWHERE);
		}
		
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());		
		if(AppUtils.isNotBlank(orders)) {
			String [] parms = orders.split(",");
			String ordervalue = parms[0];
			String ordername = parms[1];
			cq.addOrder(ordername, ordervalue);
		}
		
		return queryPage(cq);
	}

	@Override
	public List<Product> findAllProduct(Long shopId) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
	}

	@Override
	public int updateProdList(List<Product> list) {
		return this.update(list);
	}

	/**
	 * 分页查询
	 */
	@Override
	public PageSupport<StockArmDto> getStockList(Long shopId, String curPageNO, int pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(StockArmDto.class);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		query.setPageSize(pageSize);
		query.setCurPage(curPageNO);
		query.setParam(map.toArray());
		query.setAllCountString(ConfigCode.getInstance().getCode("app.shop.getCountOfSkuStockArmByShopId", map));
		query.setQueryString(ConfigCode.getInstance().getCode("app.shop.getSkuStockArmByShopId", map));

		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getProdByUser(String currPage, int size, List<Long> prodIds) {
		CriteriaQuery query = new CriteriaQuery(Product.class, currPage);
		query.setPageSize(size);
		if (AppUtils.isNotBlank(prodIds)) {
			query.in("prodId", prodIds.toArray());
		}
		PageSupport<Product> ps =queryPage(query);
		return ps;
	}

    @Override
    public List<PresellSkuDto> queryPresellProductByPresellId(Long id) {
		String sql ="SELECT s.prod_id AS prodId,s.sku_id AS skuId,s.prsell_price AS prsellPrice,sku.prodPic,sku.prodName,sku.cash,sku.prodStocks AS stocks,\n" +
				"\t\t\t\tsku.pic AS skuPic,sku.cn_properties AS cnProperties,sku.price AS skuPrice,sku.stocks AS skuStocks,sku.name AS skuName,sku.prodPrice AS price,sku.actualStocks \n" +
				"\t\t\t\tFROM ls_presell_sku s LEFT JOIN \n" +
				"\t\t\t\t(SELECT p.prod_id AS prodId,p.pic AS prodPic,p.name AS prodName,p.cash,p.price AS prodPrice,p.stocks AS prodStocks,p.actual_stocks AS actualStocks,s.* \n" +
				"\t\t\t\tFROM ls_prod p LEFT JOIN ls_sku s ON p.prod_id=s.prod_id)sku ON s.sku_id=sku.sku_id WHERE s.p_id=?";

		List<PresellSkuDto> prodlists = this.query(sql, PresellSkuDto.class,id);
		return prodlists;
    }

	@Override
	public List<Long> getProdductIdByShopId(Long shopId) {
		String sql="SELECT  `prod_id` FROM `ls_prod` WHERE `shop_id`=?";
		return query(sql,Long.class,shopId);
	}

	@Override
	public List<Product> findProdByPropId(Long propId) {
		String sql = "SELECT `prod_id`,`parameter` FROM `ls_prod` WHERE parameter LIKE ?;";
		return query(sql,Product.class,"%"+propId+"%");
	}

	@Override
	public List<GroupSelProdDto> queryGroupProductByGroupId(Long id) {
		String sql ="SELECT g.prod_id AS prodId,g.sku_id AS skuId,g.`group_price` AS groupPrice,sku.prodPic,sku.prodName,sku.cash,sku.prodStocks AS stocks,\n" +
				"sku.pic AS skuPic,sku.cn_properties AS cnProperties,sku.price AS skuPrice,sku.stocks AS skuStocks,sku.name AS skuName,sku.prodPrice AS price,sku.actualStocks \n" +
				"FROM ls_group_sku g \n" +
				"LEFT JOIN \n" +
				"(SELECT p.prod_id AS prodId,p.pic AS prodPic,p.name AS prodName,p.cash,p.price AS prodPrice,p.stocks AS prodStocks,p.actual_stocks AS actualStocks,s.* \n" +
				"FROM ls_prod p LEFT JOIN ls_sku s ON p.prod_id=s.prod_id)sku ON g.sku_id=sku.sku_id \n" +
				"WHERE g.group_id=?";

		List<GroupSelProdDto> prodlists = this.query(sql, GroupSelProdDto.class,id);

		Map<Long,GroupSelProdDto> map = getListGroupMap(prodlists);

		List<GroupSelProdDto> lists = new ArrayList<GroupSelProdDto>();
		for(Long key:map.keySet()){
			GroupSelProdDto st=map.get(key);
			lists.add(st);
		}
		return lists;
	}


	@Override
	public PageSupport<Product> getProdductByName(String curPageNo, String prodName) {
		CriteriaQuery query = new CriteriaQuery(Product.class, curPageNo);
		query.setPageSize(5);
		if (AppUtils.isNotBlank(prodName)) {
			query.like("name", prodName.trim(),MatchMode.ANYWHERE);
		}
		query.eq("status",ProductStatusEnum.PROD_ONLINE.value());
		PageSupport<Product> ps =queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<Product> getProdductByShopId(String curPageNo, String prodName,Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, curPageNo);
		query.setPageSize(5);
		QueryMap queryMap = new QueryMap();
		queryMap.put("status", ProductStatusEnum.PROD_ONLINE.value());
		queryMap.put("shopId", shopId);
		queryMap.like("name", prodName);
		query.setAllCountString(ConfigCode.getInstance().getCode("prod.getProdductByShopIdCount", queryMap));
		query.setQueryString(ConfigCode.getInstance().getCode("prod.getProdductByShopId", queryMap));
		query.setParam(queryMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> queryProductList(String curPageNO, Long shopId, Integer status, Integer supportDist,String prodName) {
		CriteriaQuery query = new CriteriaQuery(Product.class, curPageNO);
		query.setPageSize(15);
		if (AppUtils.isNotBlank(shopId)) {
			query.eq("shopId", shopId);
		}
		if (AppUtils.isNotBlank(status)) {
			query.eq("status", status);
		}
		if (AppUtils.isNotBlank(supportDist)) {
			query.eq("supportDist", supportDist);
		}
		if (AppUtils.isNotBlank(prodName)) {
			query.like("name", "%"+prodName+"%");
		}

		query.addDescOrder("modifyDate");
		PageSupport<Product> ps = queryPage(query);
		return ps;
	}

	/**
	 * 根据IDS查询商品
	 * @param productIds
	 * @return
	 */
	@Override
	public List<Product> getProductListByIds(List<Long> productIds) {
		return queryAllByIds(productIds);
	}

	/**
	 * 批量更新商品
	 * @param selProdictList
	 */
	@Override
	public void batchUpdateProduct(List<Product> selProdictList) {
		this.update(selProdictList);
	}

	@Override
	public PageSupport<Product> getProdductByShopId(String curPageNo, String prodName,Long shopId,Long shopCatId, Integer shopCatType) {
		CriteriaQuery query = new CriteriaQuery(Product.class, curPageNo);
		query.setPageSize(5);
		if (AppUtils.isNotBlank(prodName)) {
			query.like("name", prodName.trim(),MatchMode.ANYWHERE);
		}
		if (AppUtils.isNotBlank(shopId)){
			query.eq("shopId",shopId);
		}
		if (AppUtils.isNotBlank(shopCatId) && AppUtils.isNotBlank(shopCatType)){
			if(shopCatType.equals(1)){
//				1 级
				query.eq("shopFirstCatId",shopCatId);
			}else if(shopCatType.equals(2)){
				query.eq("shopSecondCatId",shopCatId);
//				2 级
			}else if(shopCatType.equals(3)){
				query.eq("shopThirdCatId",shopCatId);
//				3 级
			}
		}
		query.eq("status",ProductStatusEnum.PROD_ONLINE.value());
		PageSupport<Product> ps =queryPage(query);

		return ps;
	}

	@Override
	public PageSupport<Product> queryProdByShopIds(List<Long> shopIds, String curPageNo, String orders) {
		CriteriaQuery cq = new CriteriaQuery(Product.class, curPageNo);
		cq.setPageSize(8);
		if(AppUtils.isNotBlank(shopIds) && shopIds.size() > 0) {
			cq.in("shopId", shopIds.toArray());
		}
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		if(AppUtils.isNotBlank(orders)) {
			String [] parms = orders.split(",");
			String ordervalue = parms[0];
			String ordername = parms[1];
			cq.addOrder(ordername, ordervalue);
		}

		return queryPage(cq);
	}

	@Override
	@ProductUpdate
	public void cleanProductCache(Product product) {

	}

	/**
	 * app 商家端 获取包邮商品列表
	 * @param shopId
	 * @param curPageNO
	 * @return
	 */
	@Override
	public PageSupport<FullActiveProdDto> getAppFullActiveProdDto(Long shopId, String curPageNO,String name,Long shopCatId ,Integer shopCatType) {
		SimpleSqlQuery hql = new SimpleSqlQuery(FullActiveProdDto.class);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);

		hql.setPageSize(5);
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		List<Object> args = new ArrayList<Object>();
		args.add(shopId);
		args.add(shopId);
		if(AppUtils.isNotBlank(shopCatId) && AppUtils.isNotBlank(shopCatType)){
			if (shopCatType.equals(1)){
				map.put("shopFirstCatId",shopCatId);
				args.add(shopCatId);
			}else if(shopCatType.equals(2)){
				map.put("shopSecondCatId",shopCatId);
				args.add(shopCatId);
			}else if(shopCatType.equals(3)){
				map.put("shopThirdCatId",shopCatId);
				args.add(shopCatId);
			}
		}
		if(AppUtils.isNotBlank(name)){
			map.put("name",name);
			args.add("%" + name + "%");
		}
		hql.setCurPage(curPageNO);
		hql.setParam(args.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("prod.getAppFullActiveProdDtoCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("prod.getAppFullActiveProdDto", map));
		return 	querySimplePage(hql);
	}

	@Override
	public PageSupport<Product> getFreeShippingProductByShopId(String curPageNO, String prodName, Long shopId, Long shopCatId, Integer shopCatType) {
		SimpleSqlQuery sq = new SimpleSqlQuery(Product.class, curPageNO);
		sq.setPageSize(5);
		QueryMap queryMap = new QueryMap();
		List<Object> args = new ArrayList<>();
		if (AppUtils.isNotBlank(shopId)){
			queryMap.put("shopId",shopId);
			args.add(shopId);
		}
		Date nowDate = new Date();
		queryMap.put("endDate", nowDate);
		args.add(nowDate);
		args.add(shopId);
		if (AppUtils.isNotBlank(prodName)) {
			queryMap.like("prodName", prodName.trim(),MatchMode.ANYWHERE);
			args.add(queryMap.get("prodName"));
		}
		if (AppUtils.isNotBlank(shopCatId) && AppUtils.isNotBlank(shopCatType)){
			if(shopCatType.equals(1)){
//				1 级
				queryMap.put("shopFirstCatId",shopCatId);
				args.add(shopCatId);
			}else if(shopCatType.equals(2)){
				queryMap.put("shopSecondCatId",shopCatId);
				args.add(shopCatId);
//				2 级
			}else if(shopCatType.equals(3)){
				queryMap.put("shopThirdCatId",shopCatId);
				args.add(shopCatId);
//				3 级
			}
		}
		sq.setAllCountString(ConfigCode.getInstance().getCode("prod.getFreeShippingProductByShopIdCount", queryMap));
		sq.setQueryString(ConfigCode.getInstance().getCode("prod.getFreeShippingProductByShopId", queryMap));
		sq.setParam(args.toArray());
		return querySimplePage(sq);
	}

	@Override
	public String getServerShowById(Long prodId) {
		String sql = "select lp.service_show  from ls_prod lp where lp.prod_id = ?";
		return get(sql,String.class,prodId);
	}
}
