/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DeliveryTypeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.util.AppUtils;

/**
 * 物流配送方式Dao
 */
@Repository
public class DeliveryTypeDaoImpl extends GenericDaoImpl<DeliveryType, Long> implements DeliveryTypeDao {


	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 根据商城Id获取物流配送方式，包括系统默认的
	 */
	public List<DeliveryType> getDeliveryTypeByShopId(Long shopId) {
		return query("SELECT * FROM ls_dvy_type WHERE shop_id = ? OR is_system = 1 ORDER BY is_system ASC, create_time DESC", DeliveryType.class, shopId);
	}
	
	
	@Override
	public List<DeliveryType> getDeliveryTypeByShopId() {
		return this.queryByProperties(new EntityCriterion());
	}

	public DeliveryType getDeliveryType(Long id) {
		return getById(id);
	}

	public void deleteDeliveryType(DeliveryType deliveryType) {
		delete(deliveryType);
	}

	public Long saveDeliveryType(DeliveryType deliveryType) {
		return (Long) save(deliveryType);
	}

	public void updateDeliveryType(DeliveryType deliveryType) {
		update(deliveryType);
	}

	/**
	 * 伤处商家的物流配送方式
	 */
	@Override
	public void deleteDeliveryTypeByShopId(Long shopId) {
		update("delete from ls_dvy_type where shop_id = ?", shopId);
	}
	
	@Override
	public DeliveryType getDeliveryType(Long shopId, Long id) {
		return getByProperties(new EntityCriterion().eq("dvyTypeId", id).eq("shopId", shopId));
	}


	@Override
	public long checkMaxNumber(Long shopId) {
		return this.getLongResult("select count(dvy_type_id) from ls_dvy_type where shop_id=? ", shopId);
	}


	@Override
	public PageSupport<DeliveryType> getDeliveryTypePage(String curPageNO, DeliveryType deliveryType) {
		CriteriaQuery cq = new CriteriaQuery(DeliveryType.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		if(AppUtils.isNotBlank(deliveryType.getUserName())) {			
			cq.like("userName","%" + deliveryType.getUserName().trim() + "%");
		}
		cq.addDescOrder("modifyTime");
		cq.addDescOrder("dvyTypeId");
		if (!AppUtils.isBlank(deliveryType.getName())) {
			cq.like("name", "%" + deliveryType.getName().trim() + "%");
		}
		return queryPage(cq);
	}


	@Override
	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(DeliveryType.class, "1");
		cq.setPageSize(10);
		Criterion cqs= cq.or(Restrictions.eq("shopId",shopId), Restrictions.eq("isSystem", "1"));
		cq.add(cqs);
		cq.addAscOrder("isSystem");
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId, String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(DeliveryType.class, curPageNO);
		cq.setPageSize(10);
		Criterion cqs= cq.or(Restrictions.eq("shopId",shopId), Restrictions.eq("isSystem", "1"));
		cq.add(cqs);
		cq.addAscOrder("isSystem");
		return queryPage(cq);
	}


	@Override
	public PageSupport getAppDeliveryTypePage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(DeliveryTypeDto.class, curPageNO);
		cq.setPageSize(10);
		cq.add(Restrictions.or(Restrictions.eq("shopId", shopId), Restrictions.eq("isSystem", 1)));
		cq.addAscOrder("isSystem");
		cq.addDescOrder("createTime");
		return queryPage(cq);
	}


	@Override
	public Integer getDeliveryTypeByprintTempId(Long printTempId) {
		List<DeliveryType> list = this.query("select * from ls_dvy_type where printtemp_id = ?", DeliveryType.class, printTempId);
		return list.size();
	}
	
}
