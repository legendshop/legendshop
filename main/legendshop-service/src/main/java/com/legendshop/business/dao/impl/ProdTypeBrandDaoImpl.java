/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTypeBrandDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProdTypeBrand;

/**
 * The Class ProdTypeBrandDaoImpl.
 */
@Repository
public class ProdTypeBrandDaoImpl extends GenericDaoImpl<ProdTypeBrand, Long> implements ProdTypeBrandDao  {
	
	@Override
	public ProdTypeBrand getProdTypeBrand(Long id){
		return getById(id);
	}

	@Override
	public int deleteProdTypeBrand(ProdTypeBrand prodTypeBrand) {
		return delete(prodTypeBrand);
	}

	@Override
	public Long saveProdTypeBrand(ProdTypeBrand prodTypeBrand) {
		return save(prodTypeBrand);
	}

	@Override
	public List<Long> saveProdTypeBrand(List<ProdTypeBrand> prodTypeBrandList) {
		return save(prodTypeBrandList);
	}
  
	
 }
