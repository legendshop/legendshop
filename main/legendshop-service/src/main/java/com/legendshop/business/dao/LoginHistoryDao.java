/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.LoginHistorySum;
import com.legendshop.model.entity.LoginHistory;

/**
 * 登录历史Dao.
 */
public interface LoginHistoryDao{

	/** 根据 用户名 查取用户上一次登录时间 **/
	LoginHistory getLastLoginTime(String userName);
	
	/** 保存登录历史 **/
	Long saveLoginHistory(LoginHistory LoginHistory);

	PageSupport<LoginHistory> getLoginHistoryPage(String curPageNO, LoginHistory login);

	PageSupport<LoginHistorySum> getLoginHistoryBySQL(String curPageNO, LoginHistory login);
}
