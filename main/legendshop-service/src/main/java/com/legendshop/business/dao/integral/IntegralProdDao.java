/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralProd;

/**
 * 积分商品Dao接口.
 */

public interface IntegralProdDao extends GenericDao<IntegralProd, Long> {

	/** 获取积分商品 */
	public abstract IntegralProd getIntegralProd(Long id);

	/** 删除取积分商品 */
	public abstract int deleteIntegralProd(IntegralProd integralProd);

	/** 保存取积分商品 */
	public abstract Long saveIntegralProd(IntegralProd integralProd);

	/** 更新取积分商品 */
	public abstract int updateIntegralProd(IntegralProd integralProd);

	/** 获取积分商品分类 */
	public abstract List<Category> findIntegralCategory();

	/** 获取所有积分商品分类 */
	public abstract List<Category> findAllIntegralCategory();

	/** 获取金额分兑换列表 */
	public abstract List<IntegralProd> findExchangeList(Long firstCid, Long twoCid, Long thirdCid);

	/** 更新商品库存 */
	public abstract int updateStock(Integer count, Long id);

	/** 获取不同提供方的热门优惠券 */
	public abstract List<Coupon> getCouponByProvider(String yType, String pType, Date date);

	/** 获取热销积分商品 */
	public abstract List<IntegralProd> getHotIntegralProds();

	/** 获取优惠卷 */
	public abstract Coupon getCoupon(Long cid);

	/** 获取积分商品页面 */
	public abstract PageSupport<IntegralProd> getIntegralProdPage(String curPageNO);

	/** 获取积分商品 */
	public abstract PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid);

	/** 获取积分商品 */
	public abstract PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid, Integer startIntegral,
			Integer endIntegral, String integralKeyWord, String orders);

	/** 获取积分商品页面 */
	public abstract PageSupport<IntegralProd> getIntegralProdPage(String curPageNO, IntegralProd integralProd, DataSortResult result);

	public abstract Integer batchDelete(String ids);

	public abstract Integer batchOffline(String ids);
	
	/** 积分商品库存回滚 */
	public abstract Integer releaseStock(Long prodId, Integer productNums); 

}
