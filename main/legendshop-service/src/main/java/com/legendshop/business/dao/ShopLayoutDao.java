/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.shopDecotate.ShopLayout;

/**
 * 布局Dao.
 */
public interface ShopLayoutDao extends GenericDao<ShopLayout, Long> {
     
    public abstract List<ShopLayout> getShopLayout(Long shopId,Long decotateId);
    
    public abstract List<ShopLayout> getShopLayout(Long shopId,Long decotateId,Integer disable);

	public abstract ShopLayout getShopLayout(Long id);
	
    public abstract int deleteShopLayout(ShopLayout shopLayout);
	
	public abstract Long saveShopLayout(ShopLayout shopLayout);
	
	public abstract List<Long> saveShopLayout(List<ShopLayout> shopLayout);
	
	public abstract int updateShopLayout(ShopLayout shopLayout);
	
	public abstract Long lastSeq(Long shopId, Long decId);

	List<ShopLayout> getBottomShopLayout(Long shopId, Long decotateId);
	
	List<ShopLayout> getMainShopLayout(Long shopId, Long decotateId);
	
	public abstract List<ShopLayout> getTopShopLayout(Long shopId,Long decotateId);

	/**
	 * 更新商家布局
	 */
	public abstract void updateShopLayouts(List<ShopLayout> updateShopLayouts);

	/**
	 * 清除布局内容
	 * @param shopId
	 * @param layoutId
	 */
	public abstract void deleteShopLayout(Long shopId, Long layoutId);
	
	
 }
