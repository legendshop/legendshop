/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.promotor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.CashLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.promoter.DistSetDao;
import com.legendshop.business.dao.promoter.UserCommisDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.dto.promotor.SubLevelUserDto;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.UserCommisService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 用户佣金ServiceImpl
 */
@Service("userCommisService")
public class UserCommisServiceImpl implements UserCommisService {

	@Autowired
	private UserCommisDao userCommisDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private PdCashLogDao pdCashLogDao;
	
	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private DistSetDao distSetDao;

	/**
	 * 加载下级用户列表
	 */
	public UserCommis getUserCommis(String userId) {
		return userCommisDao.getUserCommis(userId);
	}

	/**
	 * 删除用户佣金
	 */
	public void deleteUserCommis(UserCommis userCommis) {
		userCommisDao.deleteUserCommis(userCommis);
	}

	/**
	 * 保存用户佣金
	 */
	public String saveUserCommis(UserCommis userCommis) {
		if (!AppUtils.isBlank(userCommis.getId())) {
			updateUserCommis(userCommis);
			return userCommis.getId();
		}
		return userCommisDao.saveUserCommis(userCommis);
	}

	/**
	 * 更新用户佣金
	 */
	public void updateUserCommis(UserCommis userCommis) {
		userCommisDao.updateUserCommis(userCommis);
	}

	/**
	 * 查询各级用户数量 
	 */
	@Override
	public SubUserCountDto getSubUserCount(String userName) {
		return userCommisDao.getSubUserCount(userName);
	}

	/**
	 * 处理用户佣金提现
	 */
	@Override
	public Map<String, Object> processWithDraw(String userName, Double money) {
		Map<String, Object> result = new HashMap<String, Object>();
		UserCommis userCommis = userCommisDao.getUserCommisByName(userName);
		Double settledDistCommis = userCommis.getSettledDistCommis();
		// 校验用户输入的金额是否有效
		if (money.doubleValue() <= 0 || money.doubleValue() > settledDistCommis) {
			CashLog.log(" User Commis insufficient amount by userId = {}  ------- !>  ", userName);
			result.put("status", Constants.FAIL);
			result.put("msg", "对不起,您输入的提现金额无效!");
			return result;
		}

		// 扣减用户的已结算佣金,并更新
		Double originalValue = userCommis.getSettledDistCommis();
		Double updateValue = Arith.sub(settledDistCommis, money);
		int commisNum = userCommisDao.updateSettledDistCommis(updateValue, originalValue, userCommis.getUserId());

		if (commisNum <= 0) {
			CashLog.log(" update user_commis settledDistCommis fail,settledDistCommis : {} => {},userName = {}  ------- !>  ", originalValue, updateValue,
					userName);
			result.put("status", Constants.FAIL);
			result.put("msg", "对不起,系统异常,提现失败!");
			return result;
		}

		// 增加用户的可用预存款,并更新
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		Double originalPredeposit = userDetail.getAvailablePredeposit();
		Double updatePredeposit = Arith.add(userDetail.getAvailablePredeposit(), money);
		int predepositNum = userDetailDao.updateAvailablePredeposit(updatePredeposit, originalPredeposit, userDetail.getUserId());

		if (predepositNum <= 0) {
			CashLog.log(" update user_detail availablePredeposit fail,availablePredeposit : {} => {},userName = {}  ------- !>  ", originalPredeposit,
					updatePredeposit, userName);
			result.put("status", Constants.FAIL);
			result.put("msg", "对不起,系统异常,提现失败!");
			return result;
		}

		// 创建预存款变更日志
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setAddTime(new Date());
		pdCashLog.setSn(CommonServiceUtil.getRandomSn());
		pdCashLog.setLogType(PdCashLogEnum.PROMOTER.value());
		pdCashLog.setAmount(money);
		pdCashLog.setUserId(userDetail.getUserId());
		pdCashLog.setUserName(userName);
		pdCashLog.setLogDesc("分销提现,提现金额为: " + money);

		pdCashLogDao.savePdCashLog(pdCashLog);
		// userCommisDao.savePdCashLog(pdCashLog);

		result.put("status", Constants.SUCCESS);
		result.put("msg", "恭喜您,提现成功!");
		return result;
	}

	/**
	 * 新增用户佣金 
	 */
	@Override
	public void addSaveUserCommis(UserCommis userCommisPo) {
		userCommisDao.saveUserCommis(userCommisPo);
	}

	/**
	 * 查询本月发展会员数 
	 */
	@Override
	public Long getMonthUser(Date startTime, Date endTime, String userName) {
		return userCommisDao.getMonthUser(startTime, endTime, userName);
	}

	/**
	 * 根据用户名获取下级用户 
	 */
	@Override
	public UserCommis getUserCommisByName(String userName) {
		return userCommisDao.getUserCommisByName(userName);
	}

	/**
	 * 加载下级用户列表 
	 */
	@Override
	public PageSupport<UserCommis> getUserCommis(String curPageNO, String userName, UserCommis userCommis, int pageSize) {
		return userCommisDao.getUserCommisByName(curPageNO, userName, userCommis, pageSize);
	}

	/**
	 * 加载下级用户列表 
	 */
	@Override
	public PageSupport<UserCommis> getUserCommis(String curPageNO, UserCommis userCommis) {
		return userCommisDao.getUserCommis(curPageNO, userCommis);
	}

	/**
	 * 加载下级用户列表 
	 */
	@Override
	public PageSupport<SubLevelUserDto> getUserCommis(String curPageNO, String level, String userName, String nickName) {
		return userCommisDao.getUserCommis(curPageNO, level, userName, nickName);
	}

	@Override
	public Integer batchCloseCommis(String userIdStr) {
		return userCommisDao.batchCloseCommis(userIdStr);
	}

	/* 
	 * 提交申请
	 */
	@Override
	public String submitPromoter(UserDetail userDetail, String userId, String userName) {
		DistSet originDistSet = distSetDao.getDistSet(1L);
		UserDetail userPo = userDetailDao.getUserDetailById(userId);
		if (AppUtils.isBlank(userDetail) || AppUtils.isBlank(originDistSet) || AppUtils.isBlank(userPo)) {
			return Constants.FAIL;
		}
		if (AppUtils.isNotBlank(originDistSet.getChkMobile()) && originDistSet.getChkMobile() == 1) {
			boolean flag = smsLogService.verifyCodeAndClear(userDetail.getUserMobile(), userDetail.getVerifyCode());
			if (!flag) { // 需要手机号码才验证
				return Constants.ERROR_CODE;
			}
			userPo.setUserMobile(userDetail.getUserMobile());
		}
		if (AppUtils.isNotBlank(originDistSet.getChkRealName()) && originDistSet.getChkRealName() == 1) {
			userPo.setRealName(userDetail.getRealName());
		}
//		if (AppUtils.isNotBlank(originDistSet.getChkMail()) && originDistSet.getChkMail() == 1) {
//			userPo.setUserMail(userDetail.getUserMail());
//		}
		if (AppUtils.isNotBlank(originDistSet.getChkAddr()) && originDistSet.getChkAddr() == 1) {
			userPo.setProvinceid(userDetail.getProvinceid());
			userPo.setCityid(userDetail.getCityid());
			userPo.setAreaid(userDetail.getAreaid());
			userPo.setUserAdds(userDetail.getUserAdds());
		}
		userDetailDao.updateUserDetail(userPo); // 更新提交用户基础信息

		UserCommis userCommis = this.getUserCommis(userId);
		if (userCommis == null) {
			userCommis = new UserCommis();
			userCommis.setUserId(userId);
			userCommis.setUserName(userName);
			userCommis.setId(userId);
			userCommis.setTotalDistCommis(0d);
			userCommis.setFirstDistCommis(0d);
			userCommis.setSecondDistCommis(0d);
			userCommis.setThirdDistCommis(0d);
			userCommis.setSettledDistCommis(0d);
			userCommis.setUnsettleDistCommis(0d);
			userCommis.setPromoterSts(-2l);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			this.addSaveUserCommis(userCommis); // 新增用户佣金
		} else {
			userCommis.setPromoterSts(-2l);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			this.updateUserCommis(userCommis);
		}
		return Constants.SUCCESS;
	}

	/* 
	 * 手机端提交申请
	 */
	@Override
	public String mobileSubmitPromoter(UserDetail userDetail, String userId, String userName) {
		DistSet originDistSet = distSetDao.getDistSet(1L);
		UserDetail userPo = userDetailDao.getUserDetailById(userId);
		if (AppUtils.isBlank(userDetail) || AppUtils.isBlank(originDistSet) || AppUtils.isBlank(userPo)) {
			return Constants.FAIL;
		}
		if (originDistSet.getChkMobile() == 1) {
			boolean flag = smsLogService.verifyCodeAndClear(userDetail.getUserMobile(), userDetail.getVerifyCode());
			if (!flag) { // 需要手机号码才验证
				return Constants.ERROR_CODE;
			}
			userPo.setUserMobile(userDetail.getUserMobile());
		}
		if (originDistSet.getChkRealName() == 1) {
			userPo.setRealName(userDetail.getRealName());
		}
		//检查邮箱是否已被使用
		if(!userDetailDao.isExitMail(userDetail.getUserMail())){
			return Constants.MAIL_EXISTS;
		}
		
		if (originDistSet.getChkMail() == 1) {
			userPo.setUserMail(userDetail.getUserMail());
		}
		if (originDistSet.getChkAddr() == 1) {
			userPo.setProvinceid(userDetail.getProvinceid());
			userPo.setCityid(userDetail.getCityid());
			userPo.setAreaid(userDetail.getAreaid());
			userPo.setUserAdds(userDetail.getUserAdds());
		}
		userDetailDao.updateUserDetail(userPo); // 更新提交用户基础信息

		UserCommis userCommis = this.getUserCommis(userId);
		if (userCommis == null) {
			userCommis = new UserCommis();
			userCommis.setUserId(userId);
			userCommis.setUserName(userName);
			userCommis.setId(userId);
			userCommis.setTotalDistCommis(0d);
			userCommis.setFirstDistCommis(0d);
			userCommis.setSecondDistCommis(0d);
			userCommis.setThirdDistCommis(0d);
			userCommis.setSettledDistCommis(0d);
			userCommis.setUnsettleDistCommis(0d);
			userCommis.setPromoterSts(-2l);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			this.addSaveUserCommis(userCommis); // 新增用户佣金
		} else {
			userCommis.setPromoterSts(-2l);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			this.updateUserCommis(userCommis);
		}

		return Constants.SUCCESS;
	}

	@Override
	public UserCommis getUserCommisByUserId(String userId) {
		
		return userCommisDao.getUserCommisByUserId(userId);
	}
}
