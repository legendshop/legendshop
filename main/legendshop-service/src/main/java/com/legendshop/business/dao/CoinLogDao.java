package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.coin.CoinConsumeDto;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.vo.CoinVo;

/**
 * 金币操作日志
 */
public interface CoinLogDao extends GenericDao<CoinLog, Long>{
	
	public Long saveCoinLog(CoinLog entity);

	public PageSupport<CoinConsumeDto> getRechargeDetailsList(String curPageNO, String userName, String mobile);

	public PageSupport<CoinConsumeDto> getConsumeDetailsListPage(String curPageNO, String userName, String mobile);

	public PageSupport<CoinLog> getUserCoinPage(String curPageNO, String userId);

	public CoinVo findUserCoin(String userId, String curPageNO);

	
}
