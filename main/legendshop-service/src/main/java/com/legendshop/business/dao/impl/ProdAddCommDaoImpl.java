/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdAddCommDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ProdAddComm;

/**
 * 商品二次评论服务Dao
 */
@Repository
public class ProdAddCommDaoImpl extends GenericDaoImpl<ProdAddComm, Long> implements ProdAddCommDao  {
     
	public ProdAddComm getProdAddComm(Long id){
		return getById(id);
	}
	
    public int deleteProdAddComm(ProdAddComm prodAddComm){
    	return delete(prodAddComm);
    }
	
	public Long saveProdAddComm(ProdAddComm prodAddComm){
		return save(prodAddComm);
	}
	
	public int updateProdAddComm(ProdAddComm prodAddComm){
		return update(prodAddComm);
	}

	@Override
	public ProdAddComm getProdAddCommByCommId(Long commId) {
		return this.getByProperties(new EntityCriterion().eq("prodCommId", commId));
	}
	
 }
