/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopUserDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ShopUser;

/**
 * 商城用户Dao.
 */
@Repository
public class ShopUserDaoImpl extends GenericDaoImpl<ShopUser, Long> implements ShopUserDao  {
     
    public List<ShopUser> getShopUseByShopId(Long  shopId){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
    }
    

	@Override
	public ShopUser getShopUser(Long shopId, String name) {
		return this.getByProperties(new EntityCriterion().eq("shopId", shopId).eq("name", name).eq("enabled", "1"));
	}

	public ShopUser getShopUser(Long id){
		return getById(id);
	}
	
    public int deleteShopUser(ShopUser shopUser){
    	return delete(shopUser);
    }
	
	public Long saveShopUser(ShopUser shopUser){
		return save(shopUser);
	}
	
	public int updateShopUser(ShopUser shopUser){
		return update(shopUser);
	}

	@Override
	public List<String> queryPerm(Long shopUserId) {
		return query("SELECT p.label FROM ls_shop_user u, ls_shop_perm p WHERE u.shop_role_id = p.role_id and u.id = ?", String.class, shopUserId);
	}

	@Override
	public ShopUser getShopUserByNameAndShopId(String name, Long shopId) {
		return this.getByProperties(new EntityCriterion().eq("name", name).eq("shopId", shopId));
	}
	

	@Override
	public List<ShopUser> getShopUseRoleId(Long shopRoleId) {
		return this.queryByProperties(new EntityCriterion().eq("shopRoleId", shopRoleId));
	}


	@Override
	public PageSupport<ShopUser> queryShopUser(String curPageNO, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ShopUser.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryShopUserCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.queryShopUser", map); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
 }
