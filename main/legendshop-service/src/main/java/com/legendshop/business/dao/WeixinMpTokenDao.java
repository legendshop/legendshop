/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMpToken;

/**
 * The Class WeixinMpTokenDao.
 */
public interface WeixinMpTokenDao extends Dao<WeixinMpToken, Long> {

	public abstract WeixinMpToken getWeixinMpToken(Long id);
	
    public abstract int deleteWeixinMpToken(WeixinMpToken weixinMpToken);
	
	public abstract Long saveWeixinMpToken(WeixinMpToken weixinMpToken);
	
	public abstract int updateWeixinMpToken(WeixinMpToken weixinMpToken);
	
	public abstract PageSupport<WeixinMpToken> getWeixinMpToken(CriteriaQuery cq);

	/**
	 * 获取微信小程序token
	 * @return
	 */
	public abstract WeixinMpToken getWeixinMpToken();
	
 }
