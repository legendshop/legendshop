/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ZhiboAccount;


/**
 * 直播账号Dao接口
 */
public interface ZhiboAccountDao extends Dao<ZhiboAccount, String> {

   	/**
	 *  根据Id获取
	 */
	public abstract ZhiboAccount getZhiboAccountById(String id);
	
   /**
	 *  删除
	 */
    public abstract int deleteZhiboAccount(ZhiboAccount zhiboAccount);
    
   /**
	 *  保存
	 */	
	public abstract String saveZhiboAccount(ZhiboAccount zhiboAccount);

   /**
	 *  更新
	 */		
	public abstract int updateZhiboAccount(ZhiboAccount zhiboAccount);

	public int insertZhiboAccount(ZhiboAccount zhiboAccount);
	
	public int loginUpdate(ZhiboAccount zhiboAccount);

	public ZhiboAccount getAccountByToken(String token,String type);

	public abstract ZhiboAccount getZhiboAccountByType(String userName, String type);
 }
