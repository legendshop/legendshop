/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Repository;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.business.comparer.TagItemProdComparer;
import com.legendshop.business.dao.TagItemProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.TagItemProd;
import com.legendshop.util.AppUtils;

/**
 *标签里的商品
 */
@Repository
public class TagItemProdDaoImpl extends GenericDaoImpl<TagItemProd, Long> implements TagItemProdDao  {
     
	private final static String TAG_ITEMP_RODS="select ls_tag_item_prod.tag_item_id as tagItemId,ls_tag_items.item_name as tagName from ls_tag_item_prod,ls_tag_items where ls_tag_items.item_id=ls_tag_item_prod.tag_item_id and ls_tag_item_prod.prod_id=? ";
	
	public TagItemProd getTagItemProd(Long id){
		return getById(id);
	}
	
    public int deleteTagItemProd(TagItemProd tagItemProd){
    	return delete(tagItemProd);
    }
	
	public Long saveTagItemProd(TagItemProd tagItemProd){
		return save(tagItemProd);
	}
	
	public int updateTagItemProd(TagItemProd tagItemProd){
		return update(tagItemProd);
	}
	

	@Override
	@Deprecated
	@CachePut(value="TagItemProds",key="#prodId")
	public List<TagItemProd> getTagItemProdsByPid(Long prodId) {
		return query(TAG_ITEMP_RODS, TagItemProd.class, prodId);
	}

	@Override
	public void deleteAllByProductId(Long prodId) {
		String sql = "delete from ls_tag_item_prod where prod_id=?";
		super.update(sql, prodId);
	}

	@Override
	@CacheEvict(value="TagItemProds",key="#prodId")
	public void compareProdTags(Long prodId, String[] items) {
		List<TagItemProd> dbItemProds=query("select it.prod_id as prodId,it.tag_item_id as tagItemId from  ls_tag_item_prod  it where  it.prod_id=?", TagItemProd.class, prodId);
		List<TagItemProd> paramItemProds=new ArrayList<TagItemProd>();
		if(AppUtils.isNotBlank(items)){
			for (String item : items) {
				TagItemProd itemProd=new TagItemProd();
				itemProd.setProdId(prodId);
				itemProd.setRecDate(new Date());
				itemProd.setTagItemId(Long.valueOf(item));
				paramItemProds.add(itemProd);
			}
		}
		DataListComparer<TagItemProd, TagItemProd> comparer=new DataListComparer<>(new TagItemProdComparer());
		Map<Integer, List<TagItemProd>> map=comparer.compare(paramItemProds, dbItemProds, null);
		List<TagItemProd>  delItemProds=map.get(-1);
		List<TagItemProd>  saveItemProds=map.get(1);
		
		if(AppUtils.isNotBlank(delItemProds)){
			List<Object[]> batchArgs=new ArrayList<Object[]>();
			for (int i = 0; i < delItemProds.size(); i++) {
				batchArgs.add(new Object[]{delItemProds.get(i).getProdId(),delItemProds.get(i).getTagItemId()});
			}
			batchUpdate("delete from ls_tag_item_prod where prod_id=? and tag_item_id=?",batchArgs);
		}
		if(AppUtils.isNotBlank(saveItemProds)){
			save(saveItemProds);
		}
	}

	
 }
