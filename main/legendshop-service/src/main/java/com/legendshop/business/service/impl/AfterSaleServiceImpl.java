package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AfterSaleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.spi.service.AfterSaleService;
import com.legendshop.util.AppUtils;

@Service("afterSaleService")
public class AfterSaleServiceImpl implements AfterSaleService{
	
	@Autowired
	private AfterSaleDao afterSaleDao;

	@Override
	@Cacheable(value = "AfterSale")
	public AfterSale getAfterSale(Long id) {
		return afterSaleDao.getById(id);
	}

	@Override
	@CacheEvict(value = "AfterSale", key = "#afterSale.id")
	public void deleteAfterSale(AfterSale afterSale) {
		afterSaleDao.delete(afterSale);
	}

	@Override
	@CacheEvict(value = "AfterSale", key = "#afterSale.id")
	public Long saveAfterSale(AfterSale afterSale) {
		Long id = null;
		if(AppUtils.isNotBlank(afterSale.getId())){
			id = afterSale.getId();
			afterSaleDao.update(afterSale);
		}else{
			id = afterSaleDao.saveAfterSale(afterSale);
		}
		return id;
	}

	@Override
	@CacheEvict(value = "AfterSale", key = "#afterSale.id")
	public void updateAfterSale(AfterSale afterSale) {
		afterSaleDao.update(afterSale);
	}

	@Override
	@CacheEvict(value = "AfterSale", key = "#id")
	public void deleteByAfterSaleId(Long id) {
		afterSaleDao.deleteByAfterSaleId(id);
	}

	@Override
	public PageSupport<AfterSale> getAfterSale(String curPageNO, String userName, String userId) {
		return afterSaleDao.getAfterSale(curPageNO, userName, userId);
	}

	@Override
	public PageSupport<AfterSale> getAfterSalePage(String curPageNO, String userName, String userId) {
		return afterSaleDao.getAfterSalePage(curPageNO,userName,userId);
	}
	
}
