/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ZhiboInteractAvRoomDao;
import com.legendshop.model.entity.ZhiboAvRoom;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.spi.service.ZhiboInteractAvRoomService;
import com.legendshop.util.AppUtils;

/**
 * The Class ZhiboInteractAvRoomServiceImpl.
 *  服务实现类
 */
@Service("zhiboInteractAvRoomService")
public class ZhiboInteractAvRoomServiceImpl implements ZhiboInteractAvRoomService{
    
    @Autowired
    private ZhiboInteractAvRoomDao zhiboInteractAvRoomDao;

   	/**
	 *  根据Id获取
	 */
    public ZhiboInteractAvRoom getZhiboInteractAvRoom(String id) {
        return zhiboInteractAvRoomDao.getZhiboInteractAvRoom(id);
    }

   /**
	 *  删除
	 */ 
    public void deleteZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom) {
        zhiboInteractAvRoomDao.deleteZhiboInteractAvRoom(zhiboInteractAvRoom);
    }

   /**
	 *  保存
	 */	    
    public String saveZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom) {
        if (!AppUtils.isBlank(zhiboInteractAvRoom.getUid())) {
            updateZhiboInteractAvRoom(zhiboInteractAvRoom);
            return zhiboInteractAvRoom.getUid();
        }
        return zhiboInteractAvRoomDao.saveZhiboInteractAvRoom(zhiboInteractAvRoom);
    }

   /**
	 *  更新
	 */	
    public void updateZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom) {
        zhiboInteractAvRoomDao.updateZhiboInteractAvRoom(zhiboInteractAvRoom);
    }

    public int createRoom(String uid) {
    	return zhiboInteractAvRoomDao.createRoom(uid);
    }
    
    public ZhiboAvRoom getLastAvRoom(String uid){
    	return zhiboInteractAvRoomDao.getLastAvRoom(uid);
    }
    
   
    public int enterRoom(String uid,Long av_room_id,String status,Date modify_time,Long role){
    	return zhiboInteractAvRoomDao.enterRoom(uid, av_room_id, status, modify_time, role);
    }
    
    public int updateRoomInfoById(String uid,Long av_room_id,String title,String cover){
    	return zhiboInteractAvRoomDao.updateRoomInfoById(uid, av_room_id, title, cover);
    }
    
    public int saveNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord){
    	return zhiboInteractAvRoomDao.saveNewLiveRecord(zhiboNewLiveRecord);
    }

    /**
	 * 踢人
	 * @return
	 */
	@Override
	public int kickingMemberByUid(String uid, Integer roomid) {
		return zhiboInteractAvRoomDao.kickingMemberByUid(uid, roomid);
	}
}
