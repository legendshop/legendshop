/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopLayoutDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.util.AppUtils;

/**
 * 商家装修布局Dao.
 */
@Repository
public class ShopLayoutDaoImpl extends GenericDaoImpl<ShopLayout, Long> implements ShopLayoutDao  {
	
	private String getShopLayoutSQL = "SELECT  l.layout_id AS layoutId,l.shop_id AS shopId,l.shop_decotate_id AS shopDecotateId,l.layout_type AS layoutType,ld.layout_div AS layoutDiv,l.layout_module_type AS layoutModuleType" +
			",l.layout_content AS layoutContent,l.seq AS seq,l.title as title,l.font_img as fontImg,l.font_color as fontColor,l.back_color as backColor," +
			"ld.layout_div_id AS layoutDivId,ld.layout_module_type as layoutDivModuleType,ld.layout_content as layoutDivContent FROM ls_shop_layout l LEFT JOIN ls_shop_layout_div ld ON l.layout_id=ld.layout_id WHERE l.shop_id=? AND l.shop_decotate_id=? ORDER BY l.seq ASC";
     
	private String getShopLayoutSQL2 = "SELECT  l.layout_id AS layoutId,l.shop_id AS shopId,l.shop_decotate_id AS shopDecotateId,l.layout_type AS layoutType,ld.layout_div AS layoutDiv,l.layout_module_type AS layoutModuleType" +
			",l.layout_content AS layoutContent,l.seq AS seq,l.title as title,l.font_img as fontImg,l.font_color as fontColor,l.back_color as backColor," +
			"ld.layout_div_id AS layoutDivId,ld.layout_module_type as layoutDivModuleType,ld.layout_content as layoutDivContent FROM ls_shop_layout l LEFT JOIN ls_shop_layout_div ld ON l.layout_id=ld.layout_id WHERE l.shop_id=? AND l.shop_decotate_id=?  and l.disable=? ORDER BY l.seq ASC";
	
	/**
	 * 加载商家的装修页面
	 */
    public List<ShopLayout> getShopLayout(Long shopId,Long shopDecotateId){
    	return query(getShopLayoutSQL, ShopLayout.class, shopId,shopDecotateId);
    }

	/**
	 * 根据状态，加载商家的装修页面
	 */
    public List<ShopLayout> getShopLayout(Long shopId,Long shopDecotateId,Integer disable){
    	return query(getShopLayoutSQL2, ShopLayout.class, shopId,shopDecotateId,disable);
    }
    
    
	public ShopLayout getShopLayout(Long id){
		return getById(id);
	}
	
    public int deleteShopLayout(ShopLayout shopLayout){
    	return delete(shopLayout);
    }
	
	public Long saveShopLayout(ShopLayout shopLayout){
		return save(shopLayout);
	}
	
	public List<Long> saveShopLayout(List<ShopLayout> shopLayout){
		return save(shopLayout);
	}
	
	
	public int updateShopLayout(ShopLayout shopLayout){
		return update(shopLayout);
	}
	
	/**
	 * 获取最后一个装修layout的Id
	 */
	@Override
	public Long lastSeq(Long shopId, Long decId) {
		List<Long> list= queryLimit("SELECT seq FROM ls_shop_layout WHERE ls_shop_layout.shop_id=? AND ls_shop_layout.shop_decotate_id=? ORDER BY seq DESC ",Long.class, 0,1, shopId,decId);
	   if(AppUtils.isBlank(list)){
		   return 0l;
	    }
	   return list.get(0);
	}

	@Override
	public List<ShopLayout> getTopShopLayout(Long shopId, Long decotateId) {
		return queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("shopDecotateId", decotateId).eq("layoutType", ShopLayoutTypeEnum.LAYOUT_0.value()));
	}
	
	
	@Override
	public List<ShopLayout> getBottomShopLayout(Long shopId, Long decotateId) {
		return queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("shopDecotateId", decotateId).eq("layoutType", ShopLayoutTypeEnum.LAYOUT_1.value()));
	}
	
	
	/**
	 * 查找商家下的中部的装修结构, SQL后面必须要加上括号
	 */
	@Override
	public List<ShopLayout> getMainShopLayout(Long shopId, Long decotateId) {
		return query("SELECT * FROM ls_shop_layout WHERE shop_id=? AND shop_decotate_id=? AND (layout_type='layout2' OR  layout_type='layout4' OR  layout_type='layout3')", ShopLayout.class, shopId,decotateId);
	}

	/**
	 * 更新商家布局
	 */
	@Override
	public void updateShopLayouts(List<ShopLayout> updateShopLayouts) {
			List<Object[]> objects = new ArrayList<Object[]>();
			for (ShopLayout shopLayoutDto : updateShopLayouts) {
				// update ls_shop_layout
				objects.add(new Object[] { shopLayoutDto.getLayoutType(), shopLayoutDto.getLayoutModuleType(),
						shopLayoutDto.getLayoutContent(), shopLayoutDto.getSeq(), "1", shopLayoutDto.getTitle(),
						shopLayoutDto.getFontImg(), shopLayoutDto.getFontColor(), shopLayoutDto.getBackColor(),
						shopLayoutDto.getLayoutId() });
			}
			batchUpdate("update ls_shop_layout set layout_type=?, layout_module_type=?, layout_content=?,seq=?, disable=?, title=?, font_img=?, font_color=?, back_color=? where layout_id =? ",
					objects);
			objects = null;
	}

	/**
	 * 清除布局内容
	 */
	@Override
	public void deleteShopLayout(Long shopId, Long layoutId) {
		//update("update ls_shop_layout set layout_content='' where  shop_id=? and layout_id=? ", shopId, layoutId);
		update("delete from ls_shop_layout where  shop_id=? and layout_id=? ", shopId, layoutId);
	}

 }
