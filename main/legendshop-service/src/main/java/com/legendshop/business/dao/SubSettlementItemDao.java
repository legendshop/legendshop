/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SubSettlementItem;

/**
 * 支付单据
 *
 */
public interface SubSettlementItemDao extends GenericDao<SubSettlementItem, Long> {
     
	/**
	 * 获取所有的支付单据项
	 * @param subSettlementSn
	 * @return
	 */
    public abstract List<SubSettlementItem> getSubSettlementItems(String subSettlementSn);

    /**
     * 保存支付单据
     * @param items
     */
	public abstract void saveSubSettlementItem(List<SubSettlementItem> items);
   
	
 }
