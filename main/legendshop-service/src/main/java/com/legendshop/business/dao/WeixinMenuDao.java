/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.weixin.WeixinMenu;

/**
 * 微信菜单Dao
 */
public interface WeixinMenuDao extends Dao<WeixinMenu, Long> {
     
	public abstract WeixinMenu getWeixinMenu(Long id);
	
    public abstract int deleteWeixinMenu(WeixinMenu weixinMenu);
	
	public abstract Long saveWeixinMenu(WeixinMenu weixinMenu);
	
	public abstract int updateWeixinMenu(WeixinMenu weixinMenu);
	
	public abstract boolean hasSubWeixinMenu(Long menuId);

	public abstract boolean isExitMenuKey(String menukey);

	public abstract PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, WeixinMenu weixinMenu, DataSortResult result);

	public abstract PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, Long parentId, int pageSize,
			DataSortResult result);
	
 }
