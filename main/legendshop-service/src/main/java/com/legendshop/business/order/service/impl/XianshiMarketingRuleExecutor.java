package com.legendshop.business.order.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.legendshop.spi.service.MarketingShopRuleExecutor;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
/**
 * 限时折扣运算规则
 *
 */
@Component("xianshiMarketingRuleExecutor")
public class XianshiMarketingRuleExecutor  implements MarketingShopRuleExecutor{

	@Override
	public void execute(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		if(marketingDto.getIsAllProds() == 1){
			//全场
			globalRuleResolver(marketingDto,shopCarts,context);
		}else{
			//部分商品
			prodRuleResolver(marketingDto,shopCarts,context);
		}
	}
	
	/*
	 * 全部商品
	 */
	private void globalRuleResolver(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		List<ShopCartItem> cartItems = shopCarts.getCartItems();
		List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();
		Double hitProdTotalPrice = 0d;//命中的商品总金额
		int hitProdCount = 0;//命中的商品总数
		//判断是否执行过 满减、满折、限时折扣
		for (ShopCartItem item : cartItems) {
			if(!context.isManJianExecuted(item.getProdId(), item.getSkuId()) 
					&& !context.isManZeExecuted(item.getProdId(), item.getSkuId())
					&& !context.isXianShiExecuted(item.getProdId(), item.getSkuId())){
				if(item.isCalculateMarketing()){//秒杀、团购、预售和拍卖不计算营销活动
					hitCartItems.add(item);
					context.executeXianShi(item.getProdId(), item.getSkuId());
					if(item.getCheckSts()==1){//选中状态才计算价格
						hitProdTotalPrice = Arith.add(hitProdTotalPrice, item.getTotal());
						hitProdCount+=item.getBasketCount();
					}
				}
			}
		}
		
		//找到命中的商品
		marketingDto.setHitCartItems(hitCartItems);
		marketingDto.setHitProdCount(hitProdCount);
		marketingDto.setHitProdTotalPrice(hitProdTotalPrice);

		if(hitProdTotalPrice<0.1)
			return;
		
		//计算满折规则 
		calMarketingRule(marketingDto);
	}
	
	/*
	 * 部分商品
	 */
	private void prodRuleResolver(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		
		List<ShopCartItem> cartItems = shopCarts.getCartItems();
		List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();
		Double hitProdTotalPrice = 0d;//命中的商品总金额
		int hitProdCount = 0;//命中的商品总数
		List<MarketingProdDto> marketingProds = marketingDto.getMarketingProdDtos();
		if(AppUtils.isNotBlank(marketingProds)){
			for (MarketingProdDto marketingProdDto : marketingProds) {
				//判断是否执行过 满减、满折、限时折扣
				if(!context.isManJianExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId()) 
						&& !context.isManZeExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId())
						&& !context.isXianShiExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId())){
					for (ShopCartItem item : cartItems) {
						//找到命中的商品 ，并累计商品价格
						if(marketingProdDto.getProdId().equals(item.getProdId()) && marketingProdDto.getSkuId().equals(item.getSkuId())){
							if(item.isCalculateMarketing()){//秒杀、团购、预售和拍卖不计算营销活动
								hitCartItems.add(item);
								context.executeXianShi(item.getProdId(), item.getSkuId());
								if(item.getCheckSts()==1){//选中状态才计算价格
									hitProdTotalPrice = Arith.add(hitProdTotalPrice, item.getTotal());
									hitProdCount+=item.getBasketCount();
								}
							}
							break;
						}
					}
				}
			}
			//找到命中的商品
			marketingDto.setHitCartItems(hitCartItems);
			marketingDto.setHitProdCount(hitProdCount);
			marketingDto.setHitProdTotalPrice(hitProdTotalPrice);
			
			//计算满折规则 
			calMarketingRule(marketingDto);
		}
	}
	
	
	/**
	 * 计算限时折扣规则 
	 */
	private void calMarketingRule(MarketingDto marketingDto) {
		
		//拿到限时折扣的规则
		List<MarketingRuleDto> rules = marketingDto.getMarketingRuleDtos();
		if(AppUtils.isNotBlank(rules)){
			MarketingRuleDto rule= marketingDto.getMarketingRuleDtos().get(0);
			marketingDto.setTotalOffPrice(getDiscountPrice(marketingDto.getHitProdTotalPrice(),rule.getOffDiscount()));
			
//			//新增逻辑
//			List<ShopCartItem> shopCartItemList=marketingDto.getHitCartItems();
//			double usedXianShiAmout=0.0;
//			int shopCartItemSize=shopCartItemList.size();
//			double reductionRate=Arith.div(marketingDto.getTotalOffPrice(), marketingDto.getHitProdTotalPrice());//每一块钱所占的比例
//			for(int i=0;i<shopCartItemSize;i++){
//				ShopCartItem cartItem=shopCartItemList.get(i);
//				double xianShiOffPeice=0.0;
//				if(i==shopCartItemSize-1){
//					xianShiOffPeice=Arith.sub(marketingDto.getTotalOffPrice(), usedXianShiAmout);
//				}else{
//					xianShiOffPeice=Arith.mul(reductionRate,cartItem.getCalculatedCouponAmount());//此购物车项限时优惠的金额
//					usedXianShiAmout=Arith.add(usedXianShiAmout, xianShiOffPeice);
//				}
//				double discountedPrice=Arith.sub(cartItem.getDiscountedPrice(),xianShiOffPeice);
//				//减去一个优惠活动之后的价格
//				cartItem.setDiscountedPrice(discountedPrice);
//			}
			marketingDto.setPromotionInfo("限时折扣活动商品（已打"+rule.getOffDiscount()+"折） ");
		}
	}
	
	/**
	 * 得到优惠价格
	 * @param price
	 * @param dicount
	 * @return
	 */
	public  Double getDiscountPrice(Double price,Float dicount)
    {
		Double discount=Arith.mul(price,Arith.div(dicount, 10, 2));
		discount=Arith.sub(price,discount);
		return discount;
    }
	
}
