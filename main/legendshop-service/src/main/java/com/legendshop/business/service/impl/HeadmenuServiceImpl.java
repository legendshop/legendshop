/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.HeadmenuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headmenu;
import com.legendshop.spi.service.HeadmenuService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
/**
 * 头部菜单服务
 */
@Service("headmenuService")
public class HeadmenuServiceImpl  implements HeadmenuService{

	@Autowired
    private HeadmenuDao headmenuDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

    public Headmenu getHeadmenu(Long id) {
        return headmenuDao.getHeadmenu(id);
    }

    public void deleteHeadmenu(Headmenu headmenu) {
        headmenuDao.deleteHeadmenu(headmenu);
        attachmentManager.deleteTruely(headmenu.getPic());
    }

    public Long saveHeadmenu(Headmenu headmenu) {
        if (!AppUtils.isBlank(headmenu.getId())) {
            updateHeadmenu(headmenu);
            return headmenu.getId();
        }
        return headmenuDao.saveHeadmenu(headmenu);
    }

    public void updateHeadmenu(Headmenu headmenu) {
        headmenuDao.updateHeadmenu(headmenu);
    }

	@Override
	public List<Headmenu> getHeadmenuAll(String type) {
		return headmenuDao.getHeadmenuAll(type);
	}

	@Override
	public PageSupport<Headmenu> getHeadmenu(String curPageNO, Headmenu headmenu, String type) {
		return headmenuDao.getHeadmenu(curPageNO,headmenu,type);
	}
}
