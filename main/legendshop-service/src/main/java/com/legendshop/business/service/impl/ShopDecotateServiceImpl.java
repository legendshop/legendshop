/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.comparer.ShopLayoutBannerComparer;
import com.legendshop.business.comparer.ShopLayoutComparer;
import com.legendshop.business.comparer.ShopLayoutDivComparer;
import com.legendshop.business.comparer.ShopLayoutProdComparer;
import com.legendshop.business.comparer.ShopLoyoutComparator;
import com.legendshop.business.dao.ShopBannerDao;
import com.legendshop.business.dao.ShopDecotateDao;
import com.legendshop.business.dao.ShopLayoutBannerDao;
import com.legendshop.business.dao.ShopLayoutDao;
import com.legendshop.business.dao.ShopLayoutDivDao;
import com.legendshop.business.dao.ShopLayoutHotDao;
import com.legendshop.business.dao.ShopLayoutProdDao;
import com.legendshop.business.dao.ShopNavDao;
import com.legendshop.framework.cache.LegendCacheManager;
import com.legendshop.model.constant.ShopLayoutDivTypeEnum;
import com.legendshop.model.constant.ShopLayoutModuleTypeEnum;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.HotDataDto;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.shopDecotate.ShopBanner;
import com.legendshop.model.entity.shopDecotate.ShopDecotate;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;
import com.legendshop.model.entity.shopDecotate.ShopLayoutHot;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.processor.DeepCopyDecoateObjProcessor;
import com.legendshop.processor.DeleteShopLayoutProcessor;
import com.legendshop.spi.service.ShopDecotateService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 布局Dao实现类.
 */
@Service("shopDecotateService")
public class ShopDecotateServiceImpl implements ShopDecotateService {
	
	@Resource(name="cacheManager")
	private LegendCacheManager legendCacheManager;

	@Autowired
	private ShopDecotateDao shopDecotateDao;

	@Autowired
	private ShopLayoutDao shopLayoutDao;

	@Autowired
	private ShopNavDao shopNavDao;

	@Autowired
	private ShopBannerDao shopBannerDao;

	@Autowired
	private ShopLayoutDivDao shopLayoutDivDao;

	@Autowired
	private ShopLayoutBannerDao shopLayoutBannerDao;

	@Autowired
	private ShopLayoutProdDao shopLayoutProdDao;

	@Autowired
	private ShopLayoutHotDao shopLayoutHotDao;
	
	@Autowired
	private DeleteShopLayoutProcessor deleteShopLayoutProcessor;
	
	@Autowired
	private DeepCopyDecoateObjProcessor deepCopyDecoateObjProcessor;

	public ShopDecotate getShopDecotate(Long id) {
		return shopDecotateDao.getShopDecotate(id);
	}

	public void deleteShopDecotate(ShopDecotate shopDecotate) {
		shopDecotateDao.deleteShopDecotate(shopDecotate);
	}

	public Long saveShopDecotate(ShopDecotate shopDecotate) {
		if (!AppUtils.isBlank(shopDecotate.getId())) {
			updateShopDecotate(shopDecotate);
			return shopDecotate.getId();
		}
		return shopDecotateDao.saveShopDecotate(shopDecotate);
	}

	public void updateShopDecotate(ShopDecotate shopDecotate) {
		shopDecotateDao.updateShopDecotate(shopDecotate);
	}

	@Override
	public ShopDecotate getShopDecotateByShopId(Long shopId) {
		return shopDecotateDao.getShopDecotateByShopId(shopId);
	}

	/**
	 * 获取店铺装修
	 */
	@Override
	@Cacheable(value = "ShopDecotateDto", key = "'ShopDecotate_'+#decotate.shopId")
	public ShopDecotateDto findShopDecotateDto(ShopDecotate decotate) {
		if (AppUtils.isBlank(decotate.getId()) || AppUtils.isBlank(decotate.getShopId()))
			return null;
		ShopDecotateDto decotateDto = new ShopDecotateDto();
		decotateDto.setDecId(decotate.getId());
		decotateDto.setShopId(decotate.getShopId());
		decotateDto.setIsBanner(decotate.getIsbanner() == null ? 0 : decotate.getIsbanner());
		decotateDto.setIsInfo(decotate.getIsInfo() == null ? 0 : decotate.getIsInfo());
		decotateDto.setIsNav(decotate.getIsNav() == null ? 0 : decotate.getIsNav());
		decotateDto.setIsSlide(decotate.getIsSlide() == null ? 0 : decotate.getIsSlide());
		decotateDto.setBgColor(decotate.getBgColor());
		decotateDto.setBgImgId(decotate.getBgImgId());
		decotateDto.setBgStyle(decotate.getBgStyle());
		decotateDto.setTopCss(decotate.getTopCss());
		List<ShopLayout> shopLayouts = shopLayoutDao.getShopLayout(decotate.getShopId(), decotate.getId());

		baseShopDecodate(decotate, decotateDto, shopLayouts);

		shopLayouts = null;
		return decotateDto;
	}

	/**
	 * 店铺商品列表
	 */
	@Override
	@Cacheable(value = "StoreListDto", key = "'StoreList_'+#shopId")
	public StoreListDto findStoreListDto(Long shopId) {
		StoreListDto decotateDto = new StoreListDto();
		decotateDto.setShopId(shopId);

		/*
		 * 默认头部菜单
		 */
		List<ShopNav> shopNavs = shopNavDao.getLayoutNav(shopId);
		decotateDto.setShopDefaultNavs(shopNavs);

		/*
		 * 默认商家信息
		 */
		ShopLayoutShopInfoDto shopDefaultInfo = shopDecotateDao.findShopInfo(shopId);
		decotateDto.setShopDefaultInfo(shopDefaultInfo);

		/*
		 * 热销
		 */
		List<ShopLayoutHotProdDto> hotProds = shopDecotateDao.findShopHotProds(shopId);
		decotateDto.setHotProds(hotProds);

		/*
		 * 店铺分类
		 */
		List<ShopLayoutCateogryDto> cateogryDtos = findShopCateogryDtos(shopId);
		decotateDto.setCateogryDtos(cateogryDtos);

		return decotateDto;
	}

	private void baseShopDecodate(ShopDecotate decotate, ShopDecotateDto decotateDto, List<ShopLayout> shopLayouts) {
		if (AppUtils.isNotBlank(shopLayouts)) {
			/*
			 * 组装头部布局
			 */
			List<ShopLayoutDto> topShopLayouts = constructionTopLayoutDtos(shopLayouts, decotate);
			decotateDto.setTopShopLayouts(topShopLayouts);

			/*
			 * 组装中部布局
			 */
			List<ShopLayoutDto> mainShopLayouts = constructionMainLayoutDtos(shopLayouts, decotate);
			decotateDto.setMainShopLayouts(mainShopLayouts);
			/*
			 * 组装底部布局
			 */
			List<ShopLayoutDto> bottomShopLayouts = constructionBottomLayoutDtos(shopLayouts, decotate);
			decotateDto.setBottomShopLayouts(bottomShopLayouts);
		}
		/*
		 * 默认Banner
		 */
		List<ShopBanner> banners = shopBannerDao.getShopBannerByShopId(decotate.getShopId());
		decotateDto.setShopDefaultBanners(banners);
		/*
		 * 默认Nav
		 */
		List<ShopNav> shopNavs = shopNavDao.getLayoutNav(decotate.getShopId());
		decotateDto.setShopDefaultNavs(shopNavs);

		/*
		 * 默认商家信息
		 */
		ShopLayoutShopInfoDto shopDefaultInfo = shopDecotateDao.findShopInfo(decotate.getShopId());
		decotateDto.setShopDefaultInfo(shopDefaultInfo);
	}

	private List<ShopLayoutDto> constructionBottomLayoutDtos(List<ShopLayout> shopLayouts, ShopDecotate decotate) {
		List<ShopLayoutDto> layoutDtos = new ArrayList<ShopLayoutDto>();
		for (ShopLayout shopLayout : shopLayouts) {
			if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(shopLayout.getLayoutType())) { // 头部布局
				ShopLayoutDto shopLayoutDto = constructionShopLayoutDto(shopLayout);
				ShopLayoutModuleDto layoutModuleDto = constructionLayoutModuleDto(shopLayoutDto);
				layoutModuleDto.setLayoutContent(shopLayout.getLayoutContent());
				shopLayoutDto.setShopLayoutModule(layoutModuleDto);
				layoutDtos.add(shopLayoutDto);
			}
		}
		Collections.sort(layoutDtos, new ShopLoyoutComparator());
		return layoutDtos;
	}

	private ShopLayoutDto constructionShopLayoutDto(ShopLayout shopLayout) {
		ShopLayoutDto shopLayoutDto = new ShopLayoutDto();
		shopLayoutDto.setLayoutId(shopLayout.getLayoutId());
		shopLayoutDto.setShopId(shopLayout.getShopId());
		shopLayoutDto.setShopDecotateId(shopLayout.getShopDecotateId());
		shopLayoutDto.setLayoutType(shopLayout.getLayoutType());
		shopLayoutDto.setLayoutModuleType(shopLayout.getLayoutModuleType());
		shopLayoutDto.setSeq(shopLayout.getSeq());
		shopLayoutDto.setTitle(shopLayout.getTitle());
		shopLayoutDto.setBackColor(shopLayout.getBackColor());
		shopLayoutDto.setFontColor(shopLayout.getFontColor());
		shopLayoutDto.setFontImg(shopLayout.getFontImg());
		return shopLayoutDto;
	}

	/**
	 * 头部布局[导航模块、轮播图模块、热点模块、自定义模块]
	 *
	 * @param shopLayout
	 * @return
	 */
	private List<ShopLayoutDto> constructionTopLayoutDtos(List<ShopLayout> shopLayouts, ShopDecotate decotate) {
		List<ShopLayoutDto> layoutDtos = new ArrayList<ShopLayoutDto>();
		for (ShopLayout shopLayout : shopLayouts) {
			if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(shopLayout.getLayoutType())) { // 头部布局
				ShopLayoutDto shopLayoutDto = constructionShopLayoutDto(shopLayout);
				ShopLayoutModuleDto layoutModuleDto = constructionLayoutModuleDto(shopLayoutDto);
				layoutModuleDto.setLayoutContent(shopLayout.getLayoutContent());
				shopLayoutDto.setShopLayoutModule(layoutModuleDto);
				layoutDtos.add(shopLayoutDto);
			}
		}
		Collections.sort(layoutDtos, new ShopLoyoutComparator());
		return layoutDtos;
	}

	/*
	 * 组装中部布局
	 */
	private List<ShopLayoutDto> constructionMainLayoutDtos(List<ShopLayout> shopLayouts, ShopDecotate decotate) {
		Map<Long, ShopLayoutDto> map = new HashMap<Long, ShopLayoutDto>();
		for (ShopLayout shopLayout : shopLayouts) {
			if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(shopLayout.getLayoutType())
					|| ShopLayoutTypeEnum.LAYOUT_4.value().equals(shopLayout.getLayoutType())) {
				if (map.containsKey(shopLayout.getLayoutId())) {
					// 属于同一块居中布局
					ShopLayoutDto dtos = map.get(shopLayout.getLayoutId());
					List<ShopLayoutDivDto> divlayoutDtos = dtos.getShopLayoutDivDtos();
					ShopLayoutDivDto shopLayoutDivDto = constructionShopDivLayoutDto(shopLayout);
					divlayoutDtos.add(shopLayoutDivDto);
				} else {
					ShopLayoutDto shopLayoutDto = constructionShopLayoutDto(shopLayout);
					ShopLayoutModuleDto layoutModuleDto = constructionLayoutModuleDto(shopLayoutDto);
					shopLayoutDto.setShopLayoutModule(layoutModuleDto);
					List<ShopLayoutDivDto> divlayoutDtos = new ArrayList<ShopLayoutDivDto>();
					ShopLayoutDivDto shopLayoutDivDto = constructionShopDivLayoutDto(shopLayout);
					divlayoutDtos.add(shopLayoutDivDto);
					shopLayoutDto.setShopLayoutDivDtos(divlayoutDtos);
					map.put(shopLayout.getLayoutId(), shopLayoutDto);
				}
			} else if (ShopLayoutTypeEnum.LAYOUT_2.value().equals(shopLayout.getLayoutType())) {
				ShopLayoutDto shopLayoutDto = constructionShopLayoutDto(shopLayout);
				ShopLayoutModuleDto layoutModuleDto = constructionLayoutModuleDto(shopLayoutDto);
				layoutModuleDto.setLayoutContent(shopLayout.getLayoutContent());
				shopLayoutDto.setShopLayoutModule(layoutModuleDto);
				map.put(shopLayout.getLayoutId(), shopLayoutDto);
			}
		}
		List<ShopLayoutDto> layoutDtos = new ArrayList<ShopLayoutDto>();
		for (Entry<Long, ShopLayoutDto> shopLayoutDto : map.entrySet()) {
			layoutDtos.add(shopLayoutDto.getValue());
		}
		Collections.sort(layoutDtos, new ShopLoyoutComparator());
		return layoutDtos;
	}

	/*
	 * 组装中部DIV布局
	 */
	private ShopLayoutDivDto constructionShopDivLayoutDto(ShopLayout shopLayout) {
		ShopLayoutDivDto shopLayoutDto = new ShopLayoutDivDto();
		shopLayoutDto.setLayoutId(shopLayout.getLayoutId());
		shopLayoutDto.setShopId(shopLayout.getShopId());
		shopLayoutDto.setShopDecotateId(shopLayout.getShopDecotateId());
		shopLayoutDto.setLayoutType(shopLayout.getLayoutType());
		shopLayoutDto.setLayoutDiv(shopLayout.getLayoutDiv());
		shopLayoutDto.setLayoutDivId(shopLayout.getLayoutDivId());
		shopLayoutDto.setLayoutModuleType(shopLayout.getLayoutDivModuleType());
		shopLayoutDto.setLayoutContent(shopLayout.getLayoutDivContent());
		return shopLayoutDto;
	}

	private ShopLayoutModuleDto constructionLayoutModuleDto(ShopLayoutDto shopLayout) {
		ShopLayoutModuleDto layoutModuleDto = new ShopLayoutModuleDto();
		layoutModuleDto.setLayoutId(shopLayout.getLayoutId());
		layoutModuleDto.setShopDecotateId(shopLayout.getShopDecotateId());
		layoutModuleDto.setShopId(shopLayout.getShopId());
		return layoutModuleDto;
	}


	/**
	 * 获取装修缓存,如果失败则连续取3次
	 */
	@Override
	public ShopDecotateDto findShopDecotateCache(Long shopId) {
		// get cache
		Cache cache = legendCacheManager.getCache("ShopDecotateDto");
		if (AppUtils.isNotBlank(cache)) {
			String cacheKey = "ShopDecotate_" + shopId;
			SimpleValueWrapper valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);

			if (valueWrapper == null) {
				try {
					// 休息0.3秒再取一次,防止网络出现问题
					Thread.sleep(300l);
					valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (valueWrapper == null) {
					try {
						// 休息0.6秒再取一次,防止网络出现问题
						Thread.sleep(600l);
						valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					throw new BusinessException("Can not find ShopDecotateDto valueWrapper, shopId = " + shopId);
				}

			}

			return (ShopDecotateDto) valueWrapper.get();
		} else {
			throw new BusinessException("cont not exist ShopDecotateDto chache");
		}
	}

	@Override
	public ShopDecotateDto findShopCache(Long shopId) {
		// get cache
		Cache cache = legendCacheManager.getCache("ShopDecotateDto");
		if (AppUtils.isNotBlank(cache)) {
			String cacheKey = "ShopDecotate_" + shopId;
			SimpleValueWrapper valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
			if (valueWrapper == null) {
				try {
					// 休息0.3秒再取一次,防止网络出现问题
					Thread.sleep(300l);
					valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (valueWrapper == null) {
					try {
						// 休息0.6秒再取一次,防止网络出现问题
						Thread.sleep(600l);
						valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return null;
				}
			}

			return (ShopDecotateDto) valueWrapper.get();
		} else {
			return null;
		}
	}

	@Override
	public StoreListDto findShopStoreListDtoCache(Long shopId) {
		// get cache
		Cache cache = legendCacheManager.getCache("StoreListDto");
		if (AppUtils.isNotBlank(cache)) {
			String cacheKey = "StoreList_" + shopId;
			SimpleValueWrapper valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
			if (valueWrapper == null) {
				
				try {
					// 休息0.3秒再取一次,防止网络出现问题
					Thread.sleep(300l);
					valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (valueWrapper == null) {
					try {
						// 休息0.6秒再取一次,防止网络出现问题
						Thread.sleep(600l);
						valueWrapper = (SimpleValueWrapper) cache.get(cacheKey);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return null;
				}
				return null;
			}
			return (StoreListDto) valueWrapper.get();
		} else {
			return null;
		}
	}


	@Override
	public ShopLayoutDto decorateModuleShopNavs(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		List<ShopNav> shopNavs = shopNavDao.getLayoutNav(layoutParam.getShopId());
		layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value());
		ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
		if (layoutModuleDto == null) {
			layoutModuleDto = constructionLayoutModuleDto(layoutDto);
		}
		layoutModuleDto.setShopNavs(null);
		layoutModuleDto.setShopNavs(shopNavs);
		layoutDto.setShopLayoutModule(layoutModuleDto);
		return layoutDto;

	}

	/**
	 * 保存退出商家编辑
	 */
	@Override
	public ShopDecotateDto decorateSaveQuite(ShopDecotateDto decotateDto) {
		List<ShopLayout> deleteShopLayouts = new ArrayList<ShopLayout>();
		List<ShopLayout> updateShopLayouts = new ArrayList<ShopLayout>();

		List<ShopLayoutDiv> delDivs = new ArrayList<ShopLayoutDiv>();
		List<ShopLayoutDiv> addDivs = new ArrayList<ShopLayoutDiv>();
		List<ShopLayoutDiv> updateDivs = new ArrayList<ShopLayoutDiv>();

		Long shopId = decotateDto.getShopId();
		Long decId = decotateDto.getDecId();

		List<ShopLayoutDto> topShopLayouts = decotateDto.getTopShopLayouts();

		Map<Integer, List<ShopLayout>> topMap = shoplayoutTopMap(topShopLayouts, shopId, decId);
		// 更新 layout
		deleteShopLayouts.addAll(topMap.get(-1));
		updateShopLayouts.addAll(topMap.get(0));
		disposeLayout(topShopLayouts); // 处理 cacheLayout or layout module
		decotateDto.setTopShopLayouts(null);
		decotateDto.setTopShopLayouts(topShopLayouts);

		// 处理 ShopLayoutDiv
		List<ShopLayoutDto> mainShopLayoutDtos = decotateDto.getMainShopLayouts();
		Map<Integer, List<ShopLayoutDiv>> divMap = shoplayoutDivMainMap(mainShopLayoutDtos, shopId, decId);
		delDivs = divMap.get(-1);
		addDivs = divMap.get(1);
		updateDivs = divMap.get(0);

		Map<Integer, List<ShopLayout>> mainMap = shoplayoutMainMap(mainShopLayoutDtos, shopId, decId);
		deleteShopLayouts.addAll(mainMap.get(-1));
		updateShopLayouts.addAll(mainMap.get(0));
		disposeLayout(mainShopLayoutDtos); // 处理 cacheLayout or layout module
		decotateDto.setMainShopLayouts(null);
		decotateDto.setMainShopLayouts(mainShopLayoutDtos);

		List<ShopLayoutDto> bottomShopLayouts = decotateDto.getBottomShopLayouts();
		Map<Integer, List<ShopLayout>> bottomMap = shoplayoutbottomMap(bottomShopLayouts, shopId, decId);
		deleteShopLayouts.addAll(bottomMap.get(-1));
		// addShopLayouts.addAll(bottomMap.get(1));
		updateShopLayouts.addAll(bottomMap.get(0));
		disposeLayout(bottomShopLayouts); // 处理 cacheLayout or layout module
		decotateDto.setBottomShopLayouts(null);
		decotateDto.setBottomShopLayouts(bottomShopLayouts);

		/*
		 * 处理layout
		 */
		if (AppUtils.isNotBlank(updateShopLayouts)) {
			shopLayoutDao.updateShopLayouts(updateShopLayouts);
		}
		
		if (AppUtils.isNotBlank(delDivs)) {
			shopLayoutDivDao.deleteShopLayoutDivs(delDivs);
		}
		if (AppUtils.isNotBlank(addDivs)) {
			shopLayoutDivDao.saveShopLayoutDivs(addDivs);
		}

		if (AppUtils.isNotBlank(updateShopLayouts)) {
			shopLayoutDivDao.updateShopLayoutDivs(updateDivs, shopId);
		}
		/*
		 * 更新ls_shop_decotate信息
		 */
		shopDecotateDao.updateShopDecotate(decotateDto);

		// 处理删除的Layout
		disposeDelLayout(deleteShopLayouts);

		// 执行深度复制 后台操作的缓存数据--->复制到前台对象数据
		deepCopyDecoateObjProcessor.process(decotateDto);
		deleteShopLayouts = null;
		updateShopLayouts = null;
		delDivs = null;
		addDivs = null;
		return decotateDto;
	}

	/**
	 * 删除缓存
	 * @param deleteShopLayouts
	 */
	private void disposeDelLayout(List<ShopLayout> deleteShopLayouts) {
		if (AppUtils.isBlank(deleteShopLayouts)) {
			return;
		}
		
		deleteShopLayoutProcessor.process(deleteShopLayouts);
	}

	/**
	 * 处理 shoplayout module
	 *
	 * @param shopLayoutDtos
	 */
	private void disposeLayout(List<ShopLayoutDto> shopLayoutDtos) {
		if (AppUtils.isBlank(shopLayoutDtos)) {
			return;
		}
		for (ShopLayoutDto shopLayoutDto : shopLayoutDtos) {
			if (shopLayoutDto == null)
				continue;
			restModuleEmpty(shopLayoutDto, shopLayoutDto.getLayoutModuleType());
		}
	}

	private void restModuleEmpty(ShopLayoutDto shopLayoutDto, String layoutModuleType) {
		if (shopLayoutDto == null)
			return;

		ShopLayoutModuleDto shopLayoutModule = shopLayoutDto.getShopLayoutModule();
		if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(layoutModuleType)) {
			saveBannerModule(shopLayoutModule); // 处理Banner图潘
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopNavs(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setCateogryDtos(null);
			shopLayoutModule.setHotProds(null);
			shopLayoutModule.setLayoutProdDtos(null);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value().equals(layoutModuleType)) {
			shopLayoutModule.setShopLayoutBanners(null);
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setCateogryDtos(null);
			shopLayoutModule.setHotProds(null);
			shopLayoutModule.setLayoutProdDtos(null);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value().equals(layoutModuleType)) {
			shopLayoutModule.setShopLayoutBanners(null);
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setHotProds(null);
			shopLayoutModule.setLayoutProdDtos(null);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value().equals(layoutModuleType)) {
			shopLayoutModule.setShopLayoutBanners(null);
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setCateogryDtos(null);
			shopLayoutModule.setLayoutProdDtos(null);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value().equals(layoutModuleType)) {
			shopLayoutModule.setShopLayoutBanners(null);
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setCateogryDtos(null);
			shopLayoutModule.setLayoutProdDtos(null);
			shopLayoutModule.setHotProds(null);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value().equals(layoutModuleType)) {
			saveCustomProdModule(shopLayoutModule); // 处理自定义商品模块
			shopLayoutModule.setLayoutContent(null);
			shopLayoutModule.setShopNavs(null);
			shopLayoutModule.setShopInfo(null);
			shopLayoutModule.setCateogryDtos(null);
			shopLayoutModule.setHotProds(null);
			shopLayoutModule.setShopLayoutBanners(null);
		} else { // 说明是layou div 来决定 layoutModuleType 如 :loyout_module_hot
			saveModuleHot(shopLayoutModule);
		}
	}

	private void saveModuleHot(ShopLayoutModuleDto shopLayoutModule) {
		ShopLayoutHotDto layoutHotDto = shopLayoutModule.getShopLayoutHotDto();
		if (layoutHotDto == null) {
			return;
		}
		ShopLayoutHot layoutHot = null;
		if (AppUtils.isBlank(layoutHotDto.getHotId())) {
			layoutHot = new ShopLayoutHot();
			layoutHot.setHotDatas(layoutHotDto.getHotDatas());
			layoutHot.setImageFile(layoutHotDto.getImageFile());
			layoutHot.setLayoutDivId(layoutHotDto.getLayoutDivId());
			layoutHot.setShopDecotateId(shopLayoutModule.getShopDecotateId());
			layoutHot.setShopId(shopLayoutModule.getShopId());
			layoutHot.setLayoutId(shopLayoutModule.getLayoutId());
			Long id = shopLayoutHotDao.save(layoutHot);
			layoutHotDto.setHotId(id);
		} else {
			layoutHot = shopLayoutHotDao.getById(layoutHotDto.getHotId());
			if (layoutHot != null) {
				layoutHot.setHotDatas(layoutHotDto.getHotDatas());
				layoutHot.setImageFile(layoutHotDto.getImageFile());
				shopLayoutHotDao.update(layoutHot);
			}
		}
	}

	private Map<Integer, List<ShopLayout>> shoplayoutTopMap(List<ShopLayoutDto> topShopLayouts, Long shopId,
			Long decId) {
		// 更新 layout
		List<ShopLayout> dbLayouts = shopLayoutDao.getTopShopLayout(shopId, decId);
		DataListComparer<ShopLayoutDto, ShopLayout> comparer = new DataListComparer<ShopLayoutDto, ShopLayout>(
				new ShopLayoutComparer());
		Map<Integer, List<ShopLayout>> compareResult = comparer.compare(topShopLayouts, dbLayouts, null);
		return compareResult;
	}

	private Map<Integer, List<ShopLayout>> shoplayoutMainMap(List<ShopLayoutDto> shopLayouts, Long shopId, Long decId) {
		// 更新 layout
		List<ShopLayout> dbLayouts = shopLayoutDao.getMainShopLayout(shopId, decId);
		DataListComparer<ShopLayoutDto, ShopLayout> comparer = new DataListComparer<ShopLayoutDto, ShopLayout>(
				new ShopLayoutComparer());
		Map<Integer, List<ShopLayout>> compareResult = comparer.compare(shopLayouts, dbLayouts, null);
		return compareResult;
	}

	private Map<Integer, List<ShopLayoutDiv>> shoplayoutDivMainMap(List<ShopLayoutDto> shopLayouts, Long shopId,
			Long decId) {

		/*
		 * 获取当前装修 所有的 layoutDiv 数据
		 */
		List<ShopLayoutDivDto> shopLayoutDivDtos = new ArrayList<ShopLayoutDivDto>();
		Iterator<ShopLayoutDto> iterator = shopLayouts.iterator();
		while (iterator.hasNext()) {
			ShopLayoutDto layoutDto = iterator.next();
			if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutDto.getLayoutType())
					|| ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutDto.getLayoutType())) {
				if (AppUtils.isNotBlank(layoutDto.getShopLayoutDivDtos())) {
					shopLayoutDivDtos.addAll(layoutDto.getShopLayoutDivDtos());
				}
			}
		}
		// 更新 layout
		List<ShopLayoutDiv> dbshopLayoutDivs = shopLayoutDivDao.getShopLayoutDivsByShop(shopId, decId);
		DataListComparer<ShopLayoutDivDto, ShopLayoutDiv> comparer = new DataListComparer<ShopLayoutDivDto, ShopLayoutDiv>(
				new ShopLayoutDivComparer());
		Map<Integer, List<ShopLayoutDiv>> compareResult = comparer.compare(shopLayoutDivDtos, dbshopLayoutDivs, null);
		return compareResult;
	}

	private Map<Integer, List<ShopLayout>> shoplayoutbottomMap(List<ShopLayoutDto> shopLayouts, Long shopId,
			Long decId) {
		// 更新 layout
		List<ShopLayout> dbLayouts = shopLayoutDao.getBottomShopLayout(shopId, decId);
		DataListComparer<ShopLayoutDto, ShopLayout> comparer = new DataListComparer<ShopLayoutDto, ShopLayout>(
				new ShopLayoutComparer());
		Map<Integer, List<ShopLayout>> compareResult = comparer.compare(shopLayouts, dbLayouts, null);
		return compareResult;
	}

	/**
	 * 处理banner模块
	 *
	 * @param shopLayoutModule
	 */
	private void saveBannerModule(ShopLayoutModuleDto shopLayoutModule) {

		List<ShopLayoutBanner> shopLayoutBanners = shopLayoutModule.getShopLayoutBanners();

		List<ShopLayoutBanner> layoutBanners = shopLayoutBannerDao
				.getShopLayoutBanner(shopLayoutModule.getShopDecotateId(), shopLayoutModule.getLayoutId());

		DataListComparer<ShopLayoutBanner, ShopLayoutBanner> comparer = new DataListComparer<ShopLayoutBanner, ShopLayoutBanner>(
				new ShopLayoutBannerComparer());

		Map<Integer, List<ShopLayoutBanner>> compareResult = comparer.compare(shopLayoutBanners, layoutBanners, null);

		// List<Permission> lsUpdateSkuList = compareResult.get(0); //不会有update
		// 列表，直接更新
		List<ShopLayoutBanner> delPermList = compareResult.get(-1);
		List<ShopLayoutBanner> addPermList = compareResult.get(1);

		if (AppUtils.isNotBlank(addPermList))
			shopLayoutBannerDao.saveShopLayoutBanner(addPermList);

		if (AppUtils.isNotBlank(delPermList))
			shopLayoutBannerDao.delete(delPermList);

		delPermList = null;
		addPermList = null;
		compareResult = null;
	}

	/**
	 * 处理自定义模块
	 *
	 * @param shopLayoutModule
	 */
	private void saveCustomProdModule(ShopLayoutModuleDto shopLayoutModule) {

		List<ShopLayoutProdDto> layoutProdDtos = shopLayoutModule.getLayoutProdDtos();
		if (AppUtils.isBlank(layoutProdDtos)) {
			return;
		}

		List<ShopLayoutProd> dbLayoutProds = shopLayoutProdDao.getShopLayoutProd(shopLayoutModule.getShopId(),
				shopLayoutModule.getLayoutId());

		DataListComparer<ShopLayoutProdDto, ShopLayoutProd> comparer = new DataListComparer<ShopLayoutProdDto, ShopLayoutProd>(
				new ShopLayoutProdComparer());

		Map<Integer, List<ShopLayoutProd>> compareResult = comparer.compare(layoutProdDtos, dbLayoutProds, null);
		List<ShopLayoutProd> delPermList = compareResult.get(-1);
		List<ShopLayoutProd> addPermList = compareResult.get(1);
		List<ShopLayoutProd> updatePermList = compareResult.get(0);

		if (AppUtils.isNotBlank(addPermList)) {
			for (ShopLayoutProd shopLayoutProd : addPermList) {
				long id = shopLayoutProdDao.save(shopLayoutProd);
				ShopLayoutProdDto layoutProdDto = existShopLayoutProd(layoutProdDtos, shopLayoutProd.getShopId(),
						shopLayoutProd.getLayoutId(), shopLayoutProd.getProdId());
				if (layoutProdDto != null) {
					layoutProdDto.setLayoutProdId(id);
				}
			}
		}

		if (AppUtils.isNotBlank(delPermList))
			shopLayoutProdDao.delete(delPermList);

		if (AppUtils.isNotBlank(updatePermList)) {
			List<Object[]> objects = new ArrayList<Object[]>();
			for (ShopLayoutProd layoutProd : updatePermList) {
				objects.add(new Object[] { layoutProd.getSeq(), layoutProd.getId() });
			}
			shopLayoutProdDao.batchUpdate("update ls_shop_layout_prod set seq=?  where id=? ", objects);
		}
		delPermList = null;
		addPermList = null;
		updatePermList = null;
		compareResult = null;
		shopLayoutModule.setLayoutProdDtos(null);
		shopLayoutModule.setLayoutProdDtos(layoutProdDtos);
	}

	private ShopLayoutProdDto existShopLayoutProd(List<ShopLayoutProdDto> layoutProdDtos, Long shopId, Long layoutId,
			Long prodId) {
		for (ShopLayoutProdDto shopLayoutProdDto : layoutProdDtos) {
			if (shopLayoutProdDto.getShopId().equals(shopId) && shopLayoutProdDto.getLayoutId().equals(layoutId)
					&& shopLayoutProdDto.getProdId().equals(prodId)) {
				return shopLayoutProdDto;
			}
		}
		return null;
	}

	@Override
	public List<ShopLayoutCateogryDto> findShopCateogryDtos(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		List<ShopLayoutCateogryDto> list = findShopCateogryDtos(layoutParam.getShopId());

		List<ShopLayoutDivDto> shopLayoutDivDtos = layoutDto.getShopLayoutDivDtos();
		if (AppUtils.isBlank(shopLayoutDivDtos)) { // 说明这个Layout 有问题
			return null;
		}
		for (ShopLayoutDivDto divDto : shopLayoutDivDtos) {
			if (divDto.getLayoutDivId().equals(layoutParam.getDivId())) {
				divDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value());
			}
		}
		ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
		if (layoutModuleDto == null) {
			layoutModuleDto = constructionLayoutModuleDto(layoutDto);
		}
		layoutModuleDto.setCateogryDtos(null);
		layoutModuleDto.setCateogryDtos(list);
		layoutDto.setShopLayoutModule(layoutModuleDto);
		layoutDto.setShopLayoutDivDtos(null);
		layoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
		return list;
	}

	private List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId) {
		List<ShopLayoutCateogryDto> cateogryDtos = shopDecotateDao.findShopCateogryDtos(shopId); // 一级
		if (AppUtils.isBlank(cateogryDtos)) {
			return null;
		}
		List<ShopLayoutCateogryDto> list = new ArrayList<ShopLayoutCateogryDto>();
		for (ShopLayoutCateogryDto shopLayoutCateogryDto : cateogryDtos) {
			List<ShopLayoutCateogryDto> dtos = shopDecotateDao.findShopCateogryDtos(shopId,
					shopLayoutCateogryDto.getId()); // 查询子集
			// add 3级
			if (AppUtils.isNotBlank(dtos)) {
				for (ShopLayoutCateogryDto shopLayoutCateogryDto3 : dtos) {
					List<ShopLayoutCateogryDto> dtos3 = shopDecotateDao.findShopCateogryDtos(shopId,
							shopLayoutCateogryDto3.getId());
					shopLayoutCateogryDto3.setChildrentDtos(dtos3);
				}
			}
			// add 3级
			if (AppUtils.isNotBlank(dtos)) {
				for (ShopLayoutCateogryDto shopLayoutCateogryDto3 : dtos) {
					List<ShopLayoutCateogryDto> dtos3 = shopDecotateDao.findShopCateogryDtos(shopId,
							shopLayoutCateogryDto3.getId());
					shopLayoutCateogryDto3.setChildrentDtos(dtos3);
				}
			}
			shopLayoutCateogryDto.setChildrentDtos(dtos);
			list.add(shopLayoutCateogryDto);
		}
		return list;
	}

	@Override
	public List<ShopLayoutHotProdDto> findShopHotProds(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		List<ShopLayoutHotProdDto> hotProdDtos = shopDecotateDao.findShopHotProds(layoutParam.getShopId());
		if (AppUtils.isNotBlank(hotProdDtos)) {
			List<ShopLayoutDivDto> shopLayoutDivDtos = layoutDto.getShopLayoutDivDtos();
			if (AppUtils.isBlank(shopLayoutDivDtos)) { // 说明这个Layout 有问题
				return null;
			}
			for (ShopLayoutDivDto divDto : shopLayoutDivDtos) {
				if (divDto.getLayoutDivId().equals(layoutParam.getDivId())) {
					divDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value());
				}
			}
			ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
			if (layoutModuleDto == null) {
				layoutModuleDto = constructionLayoutModuleDto(layoutDto);
			}
			layoutModuleDto.setHotProds(null);
			layoutModuleDto.setHotProds(hotProdDtos);
			layoutDto.setShopLayoutModule(layoutModuleDto);
			layoutDto.setShopLayoutDivDtos(null);
			layoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
		}
		return hotProdDtos;
	}

	@Override
	public ShopLayoutShopInfoDto findShopInfo(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		ShopLayoutShopInfoDto shopInfo = shopDecotateDao.findShopInfo(layoutParam.getShopId());
		if (AppUtils.isNotBlank(shopInfo)) {
			List<ShopLayoutDivDto> shopLayoutDivDtos = layoutDto.getShopLayoutDivDtos();
			if (AppUtils.isBlank(shopLayoutDivDtos)) { // 说明这个Layout 有问题
				return null;
			}
			for (ShopLayoutDivDto divDto : shopLayoutDivDtos) {
				if (divDto.getLayoutDivId().equals(layoutParam.getDivId())) {
					divDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value());
				}
			}
			ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
			if (layoutModuleDto == null) {
				layoutModuleDto = constructionLayoutModuleDto(layoutDto);
			}
			layoutModuleDto.setShopInfo(shopInfo);
			layoutDto.setShopLayoutModule(layoutModuleDto);
			layoutDto.setShopLayoutDivDtos(null);
			layoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
		}
		return shopInfo;
	}

	@Override
	public List<ShopLayoutProdDto> findShopLayoutProdDtos(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		List<ShopLayoutProdDto> layoutProdDtos = shopDecotateDao.findShopLayoutProdDtos(layoutParam.getShopId(),
				layoutParam.getLayoutId());
		// 处理cache layoutProdDtos
		if (AppUtils.isNotBlank(layoutProdDtos)) {
			ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
			if (layoutModuleDto == null) {
				layoutModuleDto = constructionLayoutModuleDto(layoutDto);
			}
			List<ShopLayoutDivDto> shopLayoutDivDtos = null;
			if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutParam.getLayout())
					|| ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutParam.getLayout())) {
				if (ShopLayoutDivTypeEnum.DIV_2.value().equals(layoutParam.getLayoutDiv())) {
					shopLayoutDivDtos = layoutDto.getShopLayoutDivDtos();
					if (AppUtils.isBlank(shopLayoutDivDtos)) { // 说明这个Layout 有问题
						return null;
					}
					for (ShopLayoutDivDto divDto : shopLayoutDivDtos) {
						if (divDto.getLayoutDivId().equals(layoutParam.getDivId())) {
							divDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value());
						}
					}
				}
			}
			layoutModuleDto.setLayoutProdDtos(null);
			layoutModuleDto.setLayoutProdDtos(layoutProdDtos);
			layoutDto.setShopLayoutDivDtos(null);
			layoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
		}
		return layoutProdDtos;
	}

	/**
	 * 热点
	 */
	@Override
	public ShopLayoutHotDto findShopLayoutHotDto(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto) {
		ShopLayoutHot layoutHot = null;
		if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutParam.getLayout())
				|| ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutParam.getLayout())) {
			layoutHot = shopLayoutHotDao.findShopLayoutHot(layoutParam.getShopId(), layoutParam.getDivId(),
					layoutParam.getLayoutId());
		} else {
			layoutHot = shopLayoutHotDao.findShopLayoutHot(layoutParam.getShopId(), layoutParam.getLayoutId());
		}
		if (layoutHot == null) {
			return null;
		}
		ShopLayoutHotDto hotDto = constructShopLayoutHotDto(layoutHot);
		ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
		if (layoutModuleDto == null) {
			layoutModuleDto = constructionLayoutModuleDto(layoutDto);
		}
		layoutModuleDto.setShopLayoutHotDto(hotDto);
		layoutDto.setShopLayoutModule(null);
		layoutDto.setShopLayoutModule(layoutModuleDto);
		return hotDto;
	}

	private ShopLayoutHotDto constructShopLayoutHotDto(ShopLayoutHot layoutHot) {
		ShopLayoutHotDto hotDto = new ShopLayoutHotDto();
		hotDto.setHotId(layoutHot.getId());
		hotDto.setImageFile(layoutHot.getImageFile());
		hotDto.setLayoutDivId(layoutHot.getLayoutDivId());
		hotDto.setLayoutId(layoutHot.getLayoutId());
		hotDto.setHotDatas(layoutHot.getHotDatas());
		String hot_datas = layoutHot.getHotDatas(); // [{"x1":3,"hotspotUrl":"fsdgsfdgsfdgsd","top":5,"width":2}]
		if (layoutHot != null) {
			List<HotDataDto> dataDtos = JSONUtil.getArray(hot_datas, HotDataDto.class);
			hotDto.setDataDtos(dataDtos);
		}
		return hotDto;
	}

	public static void main(String[] args) {

		List<HotDataDto> dataDtos = new ArrayList<HotDataDto>();
		HotDataDto dataDto = new HotDataDto();
		dataDto.setHotspotUrl("fsdgsfdgsfdgsd");// left 288,123,462,233
		dataDto.setX1(288);
		dataDto.setY1(123);
		dataDto.setX2(462);
		dataDto.setY2(233);
		dataDtos.add(dataDto);

		HotDataDto dataDto2 = new HotDataDto();
		dataDto2.setHotspotUrl("阿萨德发送到分撒");// left 288,123,462,233
		dataDto2.setX1(363);
		dataDto2.setY1(21);
		dataDto2.setX2(537);
		dataDto2.setY2(131);

		dataDtos.add(dataDto2);

		System.out.println(JSONUtil.getJson(dataDtos));
	}

	/**
	 * 前台商家主页
	 */
	@Override
	@Cacheable(value = "OnlineShopDecotateDto", key = "'OnlineShopDecotate_'+#shopId")
	public ShopDecotateDto findOnlineShopDecotateCache(Long shopId) {
		ShopDecotate decotate = shopDecotateDao.getShopDecotateByShopId(shopId);
		if (AppUtils.isBlank(decotate)) {
			return null;
		}
		ShopDecotateDto decotateDto = new ShopDecotateDto();
		decotateDto.setDecId(decotate.getId());
		decotateDto.setShopId(decotate.getShopId());
		decotateDto.setIsBanner(decotate.getIsbanner() == null ? 0 : decotate.getIsbanner());
		decotateDto.setIsInfo(decotate.getIsInfo() == null ? 0 : decotate.getIsInfo());
		decotateDto.setIsNav(decotate.getIsNav() == null ? 0 : decotate.getIsNav());
		decotateDto.setIsSlide(decotate.getIsSlide() == null ? 0 : decotate.getIsSlide());
		decotateDto.setBgColor(decotate.getBgColor());
		decotateDto.setBgImgId(decotate.getBgImgId());
		decotateDto.setBgStyle(decotate.getBgStyle());
		decotateDto.setTopCss(decotate.getTopCss());
		List<ShopLayout> shopLayouts = shopLayoutDao.getShopLayout(decotate.getShopId(), decotate.getId(), 1);
		baseShopDecodate(decotate, decotateDto, shopLayouts);
		shopLayouts = null;
		return decotateDto;
	}

	@Override
	public List<Coupon> getCouponByShopId(Long shopId) {
		return shopDecotateDao.getCouponByShopId(shopId);
	}
}
