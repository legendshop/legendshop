/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.DiscoverProd;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DiscoverProdDao;

/**
 * The Class DiscoverProdDaoImpl. 发现文章商品关联表Dao实现类
 */
@Repository
public class DiscoverProdDaoImpl extends GenericDaoImpl<DiscoverProd, Long> implements DiscoverProdDao{

	/**
	 * 根据Id获取发现文章商品关联表
	 */
	public DiscoverProd getDiscoverProd(Long id){
		return getById(id);
	}

	/**
	 *  删除发现文章商品关联表
	 *  @param discoverProd 实体类
	 *  @return 删除结果
	 */	
    public int deleteDiscoverProd(DiscoverProd discoverProd){
    	return delete(discoverProd);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteDiscoverProd(Long id){
		return deleteById(id);
	}

	/**
	 * 保存发现文章商品关联表
	 */
	public Long saveDiscoverProd(DiscoverProd discoverProd){
		return save(discoverProd);
	}

	/**
	 *  更新发现文章商品关联表
	 */		
	public int updateDiscoverProd(DiscoverProd discoverProd){
		return update(discoverProd);
	}

	/**
	 * 分页查询发现文章商品关联表列表
	 */
	public PageSupport<DiscoverProd>queryDiscoverProd(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(DiscoverProd.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public List<DiscoverProd> getByDisId(Long disId) {
		String sql="SELECT id,dis_id,prod_id FROM ls_discover_prod WHERE dis_id=?";
		return query(sql, DiscoverProd.class, disId);
	}

	@Override
	public int deleteByDisId(Long disId) {
		String sql="delete from ls_discover_prod WHERE dis_id=?";
		return update(sql, disId);
	}

	@Override
	public List<DiscoverProd> getDiscoverProdByDisId(Long disId) {
		String sql="SELECT id,dis_id,prod_id FROM ls_discover_prod WHERE dis_id=?";
		return query(sql, DiscoverProd.class,disId);
	}

}
