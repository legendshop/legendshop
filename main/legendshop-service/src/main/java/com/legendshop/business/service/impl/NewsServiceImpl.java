/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.legendshop.base.util.XssFilterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.business.comparer.NewsPositionComparer;
import com.legendshop.business.comparer.TagItemNewsComparer;
import com.legendshop.business.dao.NewsDao;
import com.legendshop.business.dao.NewsPositionDao;
import com.legendshop.business.dao.TagItemNewsDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.NewsPositionEnum;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.NewsPosition;
import com.legendshop.model.entity.TagItemNews;
import com.legendshop.spi.service.NewsService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;

/**
 * 新闻服务
 */
@Service("newsService")
public class NewsServiceImpl  implements NewsService {

	/** The base dao. */
	@Autowired
	private NewsDao newsDao;

	@Autowired
	private TagItemNewsDao tagItemNewsDao;
	
	@Autowired
	private NewsPositionDao newsPositionDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.NewsService#list(java.lang.String)
	 */
	@Override
	public List<News> getNewsList() {
		return newsDao.getAllNews();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.NewsService#load(java.lang.Long)
	 */
	@Override
	@Cacheable(value = "News", key="#id")
	public News getNewsById(Long id) {
		return  newsDao.getNewsById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.NewsService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long newsId) {
		List<TagItemNews> tagItemNews=tagItemNewsDao.getTagItemNewsByNewsId(newsId);
		if(AppUtils.isNotBlank(tagItemNews)){
			tagItemNewsDao.deleteTagItemNews(tagItemNews);
		}
		List<NewsPosition> newsPosition=newsPositionDao.getNewsPositionByNewsId(newsId);
		if(AppUtils.isNotBlank(newsPosition)){
			newsPositionDao.deleteNewsPositionList(newsPosition);
		}
		newsDao.deleteNewsById(newsId);
	}

	@Override
	@Caching(evict={@CacheEvict(value = "News", key = "#news.newsId"),@CacheEvict(value = "NewsList", allEntries=true)})
	public Long save(News news) {

			news.setNewsDate(new Date());
			news.setNewsBrief(XssFilterUtil.cleanXSS(news.getNewsBrief()));
			news.setNewsTitle(XssFilterUtil.cleanXSS(news.getNewsTitle()));
			news.setNewsContent(news.getNewsContent());
			news.setSeq(news.getSeq());
			//获得保存之后返回的Id
			Long newsid=newsDao.save(news);
			//文章的标签
			String newsTags=news.getNewsTags();
			//文章的位置
			String positionTags = news.getPositionTags();
			if(AppUtils.isNotBlank(newsTags)){
				List<TagItemNews> itemNewsList=new ArrayList<TagItemNews>();
				//文章的标签数组
				String[] newsTag= splitTags(newsTags);
				for (String tag : newsTag) {
					TagItemNews tagNew=new TagItemNews();
					tagNew.setNewsId(newsid);
					tagNew.setRecDate(new Date());
					tagNew.setTagItemId(Long.valueOf(tag));
					itemNewsList.add(tagNew);
				}
				if(AppUtils.isNotBlank(itemNewsList)){
						//保存
					tagItemNewsDao.saveItemList(itemNewsList);
				}
				
				
			}
			if(AppUtils.isNotBlank(positionTags)){
				//位置的标签数组
				List<NewsPosition> newsPositionList=new ArrayList<NewsPosition>();
				String[] positionTag= splitTags(positionTags);
				for (String position : positionTag) {
					NewsPosition newsPositon=new NewsPosition();
					newsPositon.setNewsid(newsid);
					newsPositon.setPosition(Integer.parseInt(position));
					newsPositon.setSeq(news.getSeq());
					newsPositionList.add(newsPositon);
				}
				
				if(AppUtils.isNotBlank(newsPositionList)&&newsPositionList.size()>0){
					newsPositionDao.save(newsPositionList);
				}
			}
			return null;
		
	}
	
	
	private String[] splitTags(String tags) {
			String [] items=tags.split(",");
			if(AppUtils.isBlank(items)){
				return null;
			}
			return items;
		}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.NewsService#update(com.legendshop.model
	 * .entity.News)
	 */
	@Override
	public void update(News news) {
		newsDao.updateNews(news);
		updateNewsTags(news.getId(), news.getNewsTags());
		updateNewsPosition(news.getId(), news.getPositionTags());
	}
	
	private void updateNewsPosition(Long newsId, String positionTags) {
		if(AppUtils.isNotBlank(positionTags)){
			String [] items=positionTags.split(",");
			if(AppUtils.isBlank(items)){
				return;
			}
			comparePositionTags(newsId,items);
		}else{
			List<NewsPosition> dbNewsPosition=newsPositionDao.getNewsPositionByNewsId(newsId);
			newsPositionDao.deleteNewsPositionList(dbNewsPosition);
		}
	}
	
	private void updateNewsTags(Long newsId, String tags) {
		if(AppUtils.isNotBlank(tags)){
			String [] items=tags.split(",");
			if(AppUtils.isBlank(items)){
				return;
			}
			compareNewsTags(newsId,items);
		}else{
			List<TagItemNews> dbItemNews=tagItemNewsDao.getTagItemNewsByNewsId(newsId);
			tagItemNewsDao.deleteTagItemNews(dbItemNews);
		}
	}
	
	private void compareNewsTags(Long newsId, String[] items) {
		List<TagItemNews> dbItemNews=tagItemNewsDao.getTagItemNewsByNewsId(newsId);
		List<TagItemNews> paramItemNews=new ArrayList<TagItemNews>();
		if(AppUtils.isNotBlank(items)){
			for (String item : items) {
				TagItemNews tagItemNews=new TagItemNews();
				tagItemNews.setNewsId(newsId);
				tagItemNews.setTagItemId(Long.valueOf(item));
				tagItemNews.setRecDate(new Date());
				paramItemNews.add(tagItemNews);
			}
		}
		DataListComparer<TagItemNews, TagItemNews> comparer=new DataListComparer<>(new TagItemNewsComparer());
		Map<Integer, List<TagItemNews>> map=comparer.compare(paramItemNews, dbItemNews, null);
		List<TagItemNews>  delTagItemNews=map.get(-1);
		List<TagItemNews>  saveTagItemNews=map.get(1);
		
		if(AppUtils.isNotBlank(delTagItemNews)){
			tagItemNewsDao.deleteTagItemNews(delTagItemNews);
		}
		if(AppUtils.isNotBlank(saveTagItemNews)){
			tagItemNewsDao.saveItemList(saveTagItemNews);
		}
	}
	
	private void comparePositionTags(Long newsId, String[] items) {
		List<NewsPosition> dbNewsPosition=newsPositionDao.getNewsPositionByNewsId(newsId);
		List<NewsPosition> paramNewsPosition=new ArrayList<NewsPosition>();
		if(AppUtils.isNotBlank(items)){
			for (String item : items) {
				NewsPosition newsPosition = new NewsPosition();
				newsPosition.setNewsid(newsId);
				newsPosition.setPosition(Integer.valueOf(item));
				paramNewsPosition.add(newsPosition);
			}
		}
		DataListComparer<NewsPosition, NewsPosition> comparer=new DataListComparer<>(new NewsPositionComparer());
		Map<Integer, List<NewsPosition>> map=comparer.compare(paramNewsPosition, dbNewsPosition, null);
		List<NewsPosition>  delNewsPosition=map.get(-1);
		List<NewsPosition>  saveNewsPosition=map.get(1);
		
		if(AppUtils.isNotBlank(delNewsPosition)){
			newsPositionDao.deleteNewsPositionList(delNewsPosition);
		}
		if(AppUtils.isNotBlank(saveNewsPosition)){
			newsPositionDao.saveNewsPositionList(saveNewsPosition);
		}
	}

	@Override
	public List<News> getNews(NewsPositionEnum newsPositionEnum, Integer num) {
		return newsDao.getNews(newsPositionEnum, num);
	}

	@Override
	public PageSupport<News> getNews(String curPageNO, Long newsCategoryId) {
		return newsDao.getNews(curPageNO, newsCategoryId);
	}

	@Override
	public Map<KeyValueEntity, List<News>> getNewsByCategory() {
		return newsDao.getNewsByCategory();
	}

	@Override
	@Cacheable(value = "NewsList", key="#shopId + '_' + #id")
	public List<News> getNearNews(Long id) {
		return newsDao.getNearNews(id);
	}

	@Override
	@Cacheable(value = "NewsList", key="'catid_' + #catid")
	public List<News> getNewsByCatId(Long catid) {
		return newsDao.getNewsBycatId(catid);
	}

	@Override
	public List<News> getNewsByposition(Long catid) {
		return newsDao.getNewsByposition(catid);
	}

	@Override
	public PageSupport<News> getSerachNews(String keyword,String curPageNO) {
		return newsDao.getSerachNews(keyword,curPageNO);
	}
	
	@Override
	public PageSupport<News> help(String curPageNO) {
		return newsDao.help(curPageNO);
	}


	@Override
	public PageSupport<News> getNewsListPage(String curPageNO, News news, Integer pageSize, DataSortResult result) {
		return newsDao.getNewsListPage(curPageNO,news,pageSize,result);
	}


	@Override
	public PageSupport<News> getNewsListResultDto(String curPageNO, Integer pageSize, DataSortResult result) {
		return newsDao.getNewsListResultDto(curPageNO,pageSize,result);
	}


	@Override
	public void updateStatus(News news) {
		newsDao.updateStatus(news);
	}

	@Override
	public PageSupport<News> getNewsList(String curPageNO, Long shopId) {
		return newsDao.getNewsList(curPageNO, shopId);
	}

	@Override
	public Map<KeyValueEntity, List<News>> findByCategoryId() {
		return this.newsDao.findByCategoryId();
	}


}
