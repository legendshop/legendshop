/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TagLibDao;
import com.legendshop.dao.GenericJdbcDao;

/**
 * The Class TagLibDaoImpl.
 */
@Repository
public class TagLibDaoImpl implements TagLibDao {
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	/**
	 * Find by hql.
	 * 
	 * @param hql
	 *            the hql
	 * @return the list
	 */
	public List<Object[]> query(String sql) {
		return genericJdbcDao.getJdbcTemplate().query(sql, new ObjectListResultSetExtractor());
	}

	/**
	 * Find by sql.
	 * 
	 * @param sql
	 *            the sql
	 * @return the list
	 */
	public List<Object[]> query(String sql, Object... params) {
		return genericJdbcDao.getJdbcTemplate().query(sql, params, new ObjectListResultSetExtractor());
	}
	
	private class ObjectListResultSetExtractor implements ResultSetExtractor<List<Object[]>>{
		public List<Object[]> extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<Object[]> results = new ArrayList<Object[]>();
			if (!rs.next()) {
				return results;
			}
			do {
				Object[] obj = new Object[2];
				obj[0] = rs.getObject(1);
				obj[1] = rs.getObject(2);
				results.add(obj);
			} while (rs.next());
			return results;
		}

		
	}

}
