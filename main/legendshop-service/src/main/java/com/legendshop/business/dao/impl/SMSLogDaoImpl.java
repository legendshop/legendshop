/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Calendar;
import java.util.Date;
import java.util.List;
 import com.legendshop.model.constant.SmsTemplateTypeEnum;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SMSLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.entity.SMSLog;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
/**
 * 短信记录服务Dao
 */
@Repository
public class SMSLogDaoImpl extends GenericDaoImpl<SMSLog, Long> implements SMSLogDao {
	
	private int allowMaxSendNum = 5;
	
	private int usefulMin = 5; //超时时间， 单位分钟 min

	@Autowired
	private SystemParameterUtil systemParameterUtil;



    public SMSLog getSmsLog(Long id){
		return getById(id);
	}
	
    public void deleteSMSLog(SMSLog sMSLog){
    	delete(sMSLog);
    }
	
	public Long saveSMSLog(SMSLog sMSLog){
		return save(sMSLog);
	}
	
	public void updateSMSLog(SMSLog sMSLog){
		 update(sMSLog);
	}

	/**
	 * 获取某个手机号码所发送的有效短信记录
	 * 对于验证码要做超时检查
	 */
	@Override
	public SMSLog getValidSMSCode(String mobile, SMSTypeEnum type) {
		List<SMSLog> result =  this.queryLimit("select mobile_code as mobileCode, rec_date as recDate,user_phone AS userPhone from ls_sms_log where user_phone = ?  and type= ? and status=? order by rec_date desc", SMSLog.class, 0,1, mobile, type.value(),1);
		if(AppUtils.isBlank(result)){
			return null;
		}else{
			SMSLog log =	result.iterator().next();
			// 提现的超时时间
			if(type.equals(SMSTypeEnum.VAL) || type.equals(SMSTypeEnum.TIX)){
				//检查是否超时
				Date recDate = log.getRecDate();
				//超时分钟数
				if(System.currentTimeMillis()>(recDate.getTime()+60000 * usefulMin)){
					 return null;
				}
			}
			return log;
		}
	}


    @Override
    public SMSLog getValidSMSCode(String mobile, SmsTemplateTypeEnum type) {
        List<SMSLog> result =  this.queryLimit("select mobile_code as mobileCode, rec_date as recDate,user_phone AS userPhone from ls_sms_log where user_phone = ?  and type= ? and status=? order by rec_date desc", SMSLog.class, 0,1, mobile, type.value(),1);
        if(AppUtils.isBlank(result)){
            return null;
        }else{
            SMSLog log =	result.iterator().next();
            //检查是否超时
            Date recDate = log.getRecDate();
            //超时分钟数
            if(System.currentTimeMillis()>(recDate.getTime()+60000 * usefulMin)){
                return null;
            }
            return log;
        }
    }


    /**
	 * 获取最近1个小时短信发送历史, 限制每个号码一个小时只能发50条
	 */
	@Override
	public boolean allowToSend(String mobile) {
		Date now = new Date();
		Date oneHourAgo = DateUtil.add(now, Calendar.HOUR, -1);
		String sql = "SELECT COUNT(id) FROM ls_sms_log WHERE user_phone=? AND rec_date BETWEEN  ? AND ?";
		int count = this.get(sql, Integer.class, mobile, now, oneHourAgo);
		if(count< allowMaxSendNum){
			return true;
		}else{
			return false;
		}
	}
	

	/**
	 * 在限制的时间内可以发送的条数
	 */
	@Override
	public boolean allowToSend(String mobile,Date oneHourAgo,Integer maxSendNum) {
		Date now = new Date();
		String sql = "SELECT COUNT(id) FROM ls_sms_log WHERE user_phone=? AND rec_date BETWEEN  ? AND ?";
		int count = this.get(sql, Integer.class, mobile, oneHourAgo, now);
		if(count< maxSendNum){
			return true;
		}else{
			return false;
		}
	}
	
	
	public void setAllowMaxSendNum(int allowMaxSendNum) {
		this.allowMaxSendNum = allowMaxSendNum;
	}

	public int getUsefulMin() {
		return usefulMin;
	}

	public void setUsefulMin(int usefulMin) {
		this.usefulMin = usefulMin;
	}

	@Override
	public void clearValidSMSCode(String mobile, SMSTypeEnum type) {
		this.update("update ls_sms_log set status=0 where user_phone = ?  and type= ? and status=?", mobile,type.value(),1);
	}

    @Override
    public void clearValidSMSCode(String mobile, SmsTemplateTypeEnum type) {
        this.update("update ls_sms_log set status=0 where user_phone = ?  and type= ? and status=?", mobile,type.value(),1);
    }

    @Override
	public int getTodayLimit(String mobile) {
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MINUTE,0);
		return get("SELECT COUNT(id) FROM ls_sms_log WHERE   user_phone= ? AND rec_date > ? ", Integer.class, mobile, cal.getTime());
	}

	@Override
	public PageSupport<SMSLog> getSMSLogPage(String curPageNO, SMSLog sMSLog) {
		CriteriaQuery cq = new CriteriaQuery(SMSLog.class, curPageNO);
		if(AppUtils.isNotBlank(sMSLog.getUserName())) {			
			cq.like("userName", "%" + sMSLog.getUserName().trim() + "%");
		}
		if(AppUtils.isNotBlank(sMSLog.getUserPhone())) {			
			cq.like("userPhone", "%" + sMSLog.getUserPhone().trim() + "%");
		}
        cq.addDescOrder("recDate");
        cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public boolean verifyMobileCode(String validateCode, String mobile) {
		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}
		//根据手机和验证码查询log
		List<SMSLog> list = this.queryByProperties(new EntityCriterion().eq("userPhone", mobile).eq("mobileCode", validateCode).eq("status", 1).addDescOrder("recDate"));
		if(AppUtils.isBlank(list)){
			return false;
		}else{
			//判断验证码是否过期
			SMSLog smsLog = list.get(0);
			//获取发送时间
			Date recDate = smsLog.getRecDate();
			//获取当前时间
			Date curDate = new Date();
			//计算，大于5分钟则为无效
			Long count = (curDate.getTime() - recDate.getTime())/1000;
			if(count > 300){
				return false;
			}
			
			return true;
		}
	}
	
 }
