package com.legendshop.business.dao.security.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.business.dao.security.ShopAuthDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.dto.shop.ShopAccountUserDto;
import com.legendshop.model.entity.ShopUser;


/**
 * 获取商家的登录信息
 *
 */
@Repository
public class ShopAuthDaoImpl implements ShopAuthDao {

	/** The log. */
	private static final Logger log = LoggerFactory.getLogger(ShopAuthDaoImpl.class);

	@Autowired
	protected GenericJdbcDao genericJdbcDao;

	// 根据用户名称用户相关资料
	private static String sqlForFindUserByName = "SELECT u.seller_id as sellerId,u.seller_name as sellerName,u.shop_id as shopId, u.password,u.enabled,u.is_store_user as isStoreUser, u.available_predeposit AS availablePredeposit,u.freeze_predeposit AS freezePredeposit,sp.site_name as shopName  "
			+ "FROM ls_shop_user u INNER JOIN  ls_shop_detail sp ON u.seller_id=sp.user_id  WHERE  u.shop_id=sp.shop_id AND u.enabled = '1' AND u.seller_name=? ";


	// 根据用户名称用户相关资料
	private static String sqlForFindUserByMobile = "SELECT u.seller_id as sellerId,u.seller_name as sellerName,u.shop_id as shopId, u.password,u.enabled,u.is_store_user as isStoreUser, u.available_predeposit AS availablePredeposit,u.freeze_predeposit AS freezePredeposit,sp.site_name as shopName  "
			+ "FROM ls_shop_user u INNER JOIN  ls_shop_detail sp ON u.seller_id=sp.user_id  WHERE  u.shop_id=sp.shop_id AND u.enabled = '1' AND sp.user_mobile=? ";

	// 根据用户名称用户相关资料
	private static String sqlForFindShopAccountByName = "SELECT u.id AS id,u.shop_role_id AS shopRoleId,u.shop_id AS shopId,u.enabled AS enabled,u.name AS shopAccountName,u.password AS shopAccountPassword "+
			"FROM ls_shop_user u INNER JOIN ls_usr_detail ud ON ud.shop_id = u.shop_id INNER JOIN ls_shop_detail sd ON sd.shop_id = u.shop_id WHERE u.enabled = '1' AND sd. STATUS = 1 AND u. NAME = ? AND ud.user_name =?";

    private static String sqlQueryPermByShopAccountRole = "SELECT label FROM ls_shop_perm where role_id = ?";
    
	// 根据用户名称用户相关资料
	private static String sqlForFindShopUserByMobileCode = "SELECT user_phone FROM ls_sys_sms_log WHERE user_phone = ? and mobile_code = ?  AND TYPE = 'login' AND rec_date > ? ORDER BY id DESC LIMIT 0,1";

	
	
	/**
	 * 商家子账号不支持
	 * remember me会调用
	 */
	public ShopUser loadUserByUsername(String sellerName) {
		// 获取用户信息 for rememberMe 
		return null;
	}
	
	/**
	 * 根据手机号码来加载商家,前提是已经验证过短信验证码
	 */
	public ShopUser loadUserByMobile(String mobile) {
		// 获取用户信息
		ShopUser shopUser=genericJdbcDao.get(sqlForFindUserByMobile, ShopUser.class, mobile);
		if (shopUser == null) {
			log.debug("Can not find user by sellerName {}", mobile);
			return null;
		}
		if (!shopUser.getEnabled()) {
			log.debug("ShopUser Enabled by sellerName {}", mobile);
			return null;
		}
		//返回用户
		return shopUser;
	}
	
	/**
	 * 普通用户登录，查找用户
	 * 包括商家子账号登录
	 * 
	 */
	@Override
	public ShopUser loadUserByUsername(String sellerName, String  password)  {
		// 获取用户信息
		ShopUser shopUser=genericJdbcDao.get(sqlForFindUserByName, ShopUser.class, sellerName);
		if (shopUser == null) {
			log.debug("Can not find user by sellerName {}", sellerName);
			return null;
		}
		if (!shopUser.getEnabled()) {
			log.debug("ShopUser Enabled by sellerName {}", sellerName);
			return null;
		}
		return shopUser;
	}
	
	/**
	 * 商家手机验证码登录
	 */
	@Override
	public ShopUser loadUserByUsername(String sellerName, String mobile, String mobileCode) {
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.MINUTE, -5);
		// 获取用户发送的短信，有效时间是5分钟
		Date date = cal.getTime();
		String mobileResult=genericJdbcDao.get(sqlForFindShopUserByMobileCode, String.class, mobile, mobileCode,date);
		if (mobileResult == null) {
			log.debug("Can not find mobile by sellerName {}", sellerName);
			return null;
		}

		ShopUser shopUser = loadUserByMobile(mobile);
		return shopUser;
	}
	
	
	
	/**
	 * 普通用户登录，查找用户
	 * 包括商家子账号登录
	 * 
	 */
	@Override
	public ShopAccountUserDto loadShopAccountByUsername(String sellerName, String password,  String shopUserNikeName)  {
		try {
			// 获取用户信息 
			ShopAccountUserDto shopUser=genericJdbcDao.get(sqlForFindShopAccountByName, ShopAccountUserDto.class, sellerName, shopUserNikeName);
			if (shopUser == null) {
				log.debug("Can not find user by sellerName {}", sellerName);
				return null;
			}
			if (!shopUser.isEnabled() && !shopUser.isShopAccountStatus() ) {
				log.debug("ShopAccountUser disabled by sellerName {}", sellerName);
				return null;
			}
			//get perm from shop role perm 获取子账号的权限
			List<String> permList=genericJdbcDao.query(sqlQueryPermByShopAccountRole, String.class, shopUser.getShopRoleId());
			shopUser.setPermList(permList);
			//返回用户
			return shopUser;
		} catch (Exception e) {
			log.error("",e);
			throw new BusinessException(e,"",ErrorCodes.UNAUTHORIZED);
		}
		

	}
	
}
