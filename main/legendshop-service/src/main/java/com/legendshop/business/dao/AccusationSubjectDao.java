/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AccusationSubject;

/**
 * 
 *  举报主题Dao
 *
 */
public interface AccusationSubjectDao extends GenericDao<AccusationSubject, Long> {
     
	public abstract AccusationSubject getAccusationSubject(Long id);
	
    public abstract void deleteAccusationSubject(AccusationSubject accusationSubject);
	
	public abstract Long saveAccusationSubject(AccusationSubject accusationSubject);
	
	public abstract void updateAccusationSubject(AccusationSubject accusationSubject);
	
	public abstract List<AccusationSubject> queryAccusationSubject();
	
	public abstract List<AccusationSubject> getAllAccusationSubject();
	
	public abstract void deleteSubjectByTypeId(Long typeId);
	
	public abstract List<AccusationSubject> getSubjectByTypeId(Long typeId);
 }
