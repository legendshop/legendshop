package com.legendshop.business.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.ShopDetailUpdate;
import com.legendshop.base.cache.UserDetailIdNameUpdate;
import com.legendshop.business.dao.CacheManualManager;
import com.legendshop.model.entity.ShopDetail;
/**
 * 清除用户缓存
 */
@Repository("cacheManualManager")
public class CacheManualManagerImpl implements CacheManualManager {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(CacheManualManagerImpl.class);

	/**
	 * 清除用户缓存
	 * @param userId 必填项
	 * @param userName 必填项
	 */
	@Override
	@UserDetailIdNameUpdate
	public void clearUserDetailCache(String userId, String userName) {
		log.info("clear user detail cache, userId = {}, userName = {}", userId, userName);

	}

	/**
	 * 清除商城的缓存
	 */
	@Override
	@ShopDetailUpdate
	public void clearShopDetailCache(ShopDetail shopDetail) {
		log.info("clear shop detail cache, shopId = {}", shopDetail.getShopId());
	}

}
