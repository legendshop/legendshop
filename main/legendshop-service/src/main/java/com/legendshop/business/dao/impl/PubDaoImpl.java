/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PubDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.PubTypeEnum;
import com.legendshop.model.entity.Pub;
import com.legendshop.util.AppUtils;

/**
 * 公告Dao
 */
@Repository
public class PubDaoImpl extends GenericDaoImpl<Pub, Long> implements PubDao {

    private final Logger log = LoggerFactory.getLogger(PubDaoImpl.class);

    @Autowired
    private SystemParameterUtil systemParameterUtil;

    private static String queryShopPubSQL = "select id,start_date as startDate,title as title,rec_date as recDate,status,end_date as endDate,type,msg " +
            "FROM ls_pub WHERE  ((start_date <= ? or start_date is null) AND (end_date >= ? or end_date is null)) AND type=? AND status=? order by rec_date desc LIMIT 5";

    @Override
    @Cacheable(value = "PubList")
    public List<Pub> getPubByShopId() {
        Date today = new Date();
        EntityCriterion cq = new EntityCriterion();
        cq.eq("status", Constants.ONLINE);
        cq.eq("type", PubTypeEnum.PUB_BUYERS.value());
        // 有效期检查
        cq.add(cq.or(Restrictions.ge("endDate", today), Restrictions.isNull("endDate")));
        cq.add(cq.or(Restrictions.le("startDate", today), Restrictions.isNull("startDate")));
        cq.addOrder("desc", "id");
        return queryByProperties(cq, 0, 9);
    }

    @Override
    @CacheEvict(value = "Pub", key = "#pub.id")
    public void deletePub(Pub pub) {
        delete(pub);
    }

    /**
     * 更新公告
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "PubList", allEntries = true),
            @CacheEvict(value = "Pub", key = "#pub.id")})
    public void updatePub(Pub pub) {
        update(pub);

    }

    @Override
    @CacheEvict(value = "PubList", allEntries = true)
    public Long savePub(Pub pub) {
        return (Long) save(pub);
    }

    @Override
    public Pub getPubById(Long id) {
        return getById(id);
    }

    @Override
    public List<Pub> getPub(String userName, Long id, boolean isShopUser) {
        String queryNewsSQL = "";
        if(isShopUser)
        	queryNewsSQL= ConfigCode.getInstance().getCode("biz.getPubs");
        else
        	queryNewsSQL= ConfigCode.getInstance().getCode("biz.getPubsByUser");
        
        return query(queryNewsSQL, Pub.class, userName, id, userName, id);
    }

    /**
     * 卖家公告
     */
    @Override
    public List<Pub> queryPubByShopId() {
        return this.queryByProperties(new EntityCriterion().eq("type", PubTypeEnum.PUB_SELLER.value()).addAscOrder("recDate"));
    }

    @Override
    public List<Pub> queryShopPub() {
        Date date = new Date();
        List<Pub> pubList = this.query(queryShopPubSQL, Pub.class, date, date, PubTypeEnum.PUB_SELLER.value(), Constants.ONLINE);
        return pubList;
    }

    @Override
    public List<Pub> queryUserPub() {
        Date date = new Date();
        List<Pub> pubList = this.query(queryShopPubSQL, Pub.class, date, date, PubTypeEnum.PUB_BUYERS.value(), Constants.ONLINE);
        return pubList;
    }

    @Override
    public PageSupport<Pub> getPubListPage(String curPageNO, Pub pub, DataSortResult result) {
        QueryMap map = new QueryMap();
        SimpleSqlQuery hql = new SimpleSqlQuery(Pub.class);
        hql.setPageSize(systemParameterUtil.getPageSize());
        hql.setCurPage(curPageNO);
        if (AppUtils.isNotBlank(pub.getTitle())) {
            map.like("title", pub.getTitle().trim());
        }
        if (AppUtils.isNotBlank(pub.getType())) {
            map.put("type", pub.getType());
        }

        if (!result.isSortExternal()) {//支持外部排序
        }


        hql.setAllCountString(ConfigCode.getInstance().getCode("pub.getPubListCount", map));
        hql.setQueryString(ConfigCode.getInstance().getCode("pub.getPubList", map));

        map.remove(AttributeKeys.ORDER_INDICATOR);//去掉排序的条件
        hql.setParam(map.toArray());
        return querySimplePage(hql);
    }

}
