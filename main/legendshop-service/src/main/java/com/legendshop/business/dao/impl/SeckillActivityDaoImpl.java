/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.model.constant.ActivitySearchTypeEnum;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.dto.AppSecKillActivityQueryDTO;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.seckill.Seckill;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动DaoImpl.
 */
@SuppressWarnings("all")
@Repository
public class SeckillActivityDaoImpl extends GenericDaoImpl<SeckillActivity, Long> implements SeckillActivityDao {

	/**
	 * 获取秒杀活动数据
	 */
	public SeckillActivity getSeckillActivity(Long id) {
		return getById(id);
	}

	/**
	 * 删除秒杀活动数据
	 */
	public int deleteSeckillActivity(SeckillActivity seckillActivity) {
		return delete(seckillActivity);
	}

	/**
	 * 保存秒杀活动商品
	 */
	public Long saveSeckillActivity(SeckillActivity seckillActivity) {
		return save(seckillActivity);
	}

	/**
	 * 更新秒杀活动商品
	 */
	public int updateSeckillActivity(SeckillActivity seckillActivity) {
		return update(seckillActivity);
	}

	/**
	 * 获取秒杀活动数据
	 */
	public PageSupport getSeckillActivity(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/**
	 * 获取秒杀活动详情
	 */
	@Override
	public SeckillActivity getSeckillDetail(Long seckillId) {
		String sql = ConfigCode.getInstance().getCode("seckill.getSeckillDetail");
		SeckillActivity dto = get(sql, new Object[] { seckillId }, new SeckillDtoRowMapper());
		return dto;
	}

	/**
	 * The Class ProductDetailRowMapper.
	 */
	class SeckillDtoRowMapper implements RowMapper<SeckillActivity> {
		@Override
		public SeckillActivity mapRow(ResultSet rs, int rowNum) throws SQLException {
			SeckillActivity seckillDto = new SeckillActivity();
			seckillDto.setId(rs.getLong("sid"));
			seckillDto.setUserId(rs.getString("userId"));
			seckillDto.setShopId(rs.getLong("shopId"));
			seckillDto.setShopName(rs.getString("shopName"));
			seckillDto.setSeckillTitle(rs.getString("seckillTitle"));
			seckillDto.setStartTime(rs.getTimestamp("startTime"));
			seckillDto.setEndTime(rs.getTimestamp("endTime"));
			seckillDto.setStatus(rs.getLong("sec_status"));
			seckillDto.setSeckillDesc(rs.getString("secDesc"));
			seckillDto.setSeckillBrief(rs.getString("seckillBrief"));
			seckillDto.setSeckillLowPrice(rs.getBigDecimal("seckillLowPrice"));
			return seckillDto;
		}
	}

	private String EXECUTE_SQL= " UPDATE ls_seckill_prod LEFT JOIN ls_seckill_activity ON ls_seckill_activity.id=ls_seckill_prod.s_id  "
			 + " SET ls_seckill_prod.seckill_stock=ls_seckill_prod.seckill_stock-1  WHERE  ls_seckill_prod.seckill_stock > 0 AND ls_seckill_prod.s_id=? "
			 + " AND ls_seckill_prod.prod_id=? AND ls_seckill_prod.sku_id=?  "
	         + " AND ls_seckill_activity.start_time < ?  AND ls_seckill_activity.end_time > ?  AND ls_seckill_activity.status=1  ";
	
	/**
	 * 执行秒杀
	 */
	@Override
	public int executeSeckill(final long seckillId, final long prodId, final long skuId, final String userId, final Double price,
			final int sckNumber) {
		final Timestamp seckillTime = new Timestamp(System.currentTimeMillis());

		String sql="select count(id) from ls_seckill_success where seckill_id=? and user_id=?  and prod_id=? and sku_id=? ";
		Long number= getLongResult(sql, seckillId,userId,prodId,skuId);
		if(number>0){
			return -2; //重复秒杀
		}
		int result=update(EXECUTE_SQL, seckillId,prodId,skuId,seckillTime,seckillTime);
		if(result <=0){
			return 0; //秒杀失败
		}
		sql="INSERT IGNORE INTO ls_seckill_success(user_id,seckill_id,prod_id,sku_id,seckill_num,seckill_price,seckill_time,STATUS)  VALUES (?,?,?,?,?,?,?,0)";
		result=update(sql, userId,seckillId,prodId,skuId,sckNumber,price,seckillTime);
		if(result <=0){
			return 0; //秒杀失败
		}
		return 1;
	}

	/**
	 * 查询所有在线的秒杀活动
	 */
	@Override
	public List<Long> findOnActive() {
		java.util.Date date = new java.util.Date();
		String sql = ConfigCode.getInstance().getCode("seckill.findOnActive");
		return query(sql, Long.class, new Object[] { date, date });
	}

	/**
	 * 获取用户秒杀成功的记录
	 */
	@Override
	public SeckillSuccess getSeckillSucess(String userId, Long seckillId, long prodId, long skuId) {
		String sql = ConfigCode.getInstance().getCode("seckill.getSeckillSucess");
		SeckillSuccess seckillSuccess = get(sql, SeckillSuccess.class, new Object[] { userId, seckillId, prodId, skuId });
		return seckillSuccess;
	}
	
	/**
	 * 根据秒杀活动id获取秒杀成功记录
	 */
	@Override
	public List<SeckillSuccess> getSeckillSucessBySeckillId(Long seckillId) {
		String sql = ConfigCode.getInstance().getCode("seckill.getSeckillSucessBySeckIllId");
		List<SeckillSuccess> list = query(sql, SeckillSuccess.class, seckillId);
		return list;
	}

    @Override
    public OperateStatisticsDTO getOperateStatistics(Long id) {
        String sql = "  SELECT COALESCE(SUM(s.actual_total),0) as totalAmount,COUNT(1) as succeedCount " +
				"  FROM ls_sub s INNER JOIN ls_seckill_activity g " +
				"  ON s.active_id = g.id " +
				"  WHERE s.sub_type = 'SECKILL' AND  s.active_id = ? ";
		return get(sql,OperateStatisticsDTO.class,id);
    }

	@Override
	public Integer getPersonCount(Long id) {
		String sql = "SELECT COUNT(DISTINCT user_id) FROM ls_seckill_success WHERE seckill_id=?";
		return get(sql,Integer.class,id);
	}

	@Override
	public SeckillActivity getSeckillByProd(Long prodId, Long skuId) {
		String sql = " select a.*,p.s_id as activeId from ls_seckill_activity a " +
				" LEFT JOIN ls_seckill_prod p ON a.id = p.s_id " +
				" WHERE a.status=1 AND p.prod_id =? AND p.sku_id=?";
		return get(sql,SeckillActivity.class,prodId,skuId);
	}

	/**
	 * 删除秒杀记录 
	 */
	@Override
	public void deleteRecord(Long id, String userId) {
		this.update("delete from ls_seckill_success where status=-1 and user_id=? and id=? ", userId, id);
	}

	/**
	 * 查找拍卖列表 
	 */
	@Override
	public PageSupport<SeckillActivity> queryAuctionList(String startTime) {
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class);
		QueryMap map = new QueryMap();
		Calendar periodStart = Calendar.getInstance();
		periodStart.setTime(new Date());
		periodStart.set(Calendar.MILLISECOND, 0);
		periodStart.set(Calendar.SECOND, 0);
		periodStart.set(Calendar.MINUTE, 0);
		Calendar periodEnd = Calendar.getInstance();
		periodEnd.setTime(new Date());
		periodEnd.set(Calendar.MILLISECOND, 0);
		periodEnd.set(Calendar.SECOND, 0);
		periodEnd.set(Calendar.MINUTE, 0);

		Integer startTimeInt = null;
		try{
			startTimeInt = Integer.parseInt(startTime);
		}catch (Exception e){
			startTimeInt = null;
		}

		if (startTime != null) {
			periodStart.set(Calendar.HOUR_OF_DAY, startTimeInt);// 当前点击的时间段
																				// 起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = startTimeInt + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, foo); // 当前点击的时间段 结束时间点
			map.put("periodEnd", periodEnd.getTime());
		} else {
			periodStart.set(Calendar.HOUR_OF_DAY, getPeriodTime(periodStart.get(Calendar.HOUR_OF_DAY)));// 当前时间所在时间段
																										// 起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = periodStart.get(Calendar.HOUR_OF_DAY) + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, getPeriodTime(foo));// 当前时间所在时间段
																	// 结束时间点
			map.put("periodEnd", periodEnd.getTime());
		}
		// 获得当前选择的时间段
		// 活动开始时间和结束时间在该时段之内
		String querySQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 获得活动所在时间段
	 * 
	 * @param cur_hour
	 * @return
	 */
	private Integer getPeriodTime(int cur_hour) {
		if (cur_hour < 9) {
			return 0;
		} else if (9 <= cur_hour && cur_hour < 12) {
			return 9;
		} else if (12 <= cur_hour && cur_hour < 15) {
			return 12;
		} else if (15 <= cur_hour && cur_hour < 18) {
			return 15;
		} else if (18 <= cur_hour && cur_hour < 21) {
			return 18;
		} else if (21 <= cur_hour && cur_hour < 24) {
			return 21;
		} else {
			return 0;
		}
	}

	/**
	 * 查询秒杀成功的记录信息 
	 */
	@Override
	public PageSupport<SeckillSuccessRecord> querySimplePage(String curPageNO, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillSuccessRecord.class, 30, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String querySQL = ConfigCode.getInstance().getCode("seckill.querySeckillRecord", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.querySeckillRecordCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 查找秒杀列表 
	 */
	@Override
	public PageSupport<SeckillActivity> queryAuctionList(String curPageNO, int pageSize, SeckillActivity seckillActivity) {

		QueryMap map = new QueryMap();
		
		map.put("shopId", seckillActivity.getShopId());
		if(AppUtils.isNotBlank(seckillActivity.getStatus()) && seckillActivity.getStatus()==4){
			
			map.put("statusIsFinished", "AND sec.status = 4");
		}else {
			map.put("status", seckillActivity.getStatus());
		}
		
		if (AppUtils.isNotBlank(seckillActivity.getSeckillTitle())) {
			map.put("seckillTitle", "%" + seckillActivity.getSeckillTitle().trim() + "%");
		}
		
		if(AppUtils.isNotBlank(seckillActivity.getShopName())) {			
			map.put("shopName",  "%" + seckillActivity.getShopName().trim() + "%");
		}
		
		if (AppUtils.isNotBlank(seckillActivity.getOverdue())) {// 查询已过期
			
			map.put("overdue", seckillActivity.getOverdue());
			map.put("status", 1);
		}
		
//		if (AppUtils.isNotBlank(seckillActivity.getOverdate())) {// 其他查询中不显示已过期的活动
//			map.put("overDate", seckillActivity.getOverdate());
//		}
		
		map.put("startTime", seckillActivity.getStartTime());
		map.put("endTime", seckillActivity.getEndTime());
		
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class);
		query.setPageSize(pageSize);
		query.setCurPage(curPageNO);
		
		query.setAllCountString(ConfigCode.getInstance().getCode("seckill.querySeckillActivityCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("seckill.querySeckillActivity", map));
		
		map.remove("statusIsFinished");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	

	/**
	 * 商家获取秒杀活动列表  TODO
	 */
	@Override
	public PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, Long shopId, int pageSize, SeckillActivity seckillActivity) {
		
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if (AppUtils.isNotBlank(seckillActivity.getSeckillTitle())) {
			map.put("seckillTitle", "%" + seckillActivity.getSeckillTitle().trim() + "%");
		}
		//根据searchType,组装不同的筛选条件
		String tab = seckillActivity.getSearchType();
		if (AppUtils.isNotBlank(tab)) {
			Date currDate = new Date();
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			switch(ActivitySearchTypeEnum.matchType(tab)){
				case WAIT_AUDIT :
					map.put("status", SeckillStatusEnum.VALIDATING.value());
					map.put("unFinished", "AND sec.end_time > "+ "'" +  sd.format(currDate) + "'");
					break;
				case NOT_PASS:
					map.put("status", SeckillStatusEnum.FAILED.value());
					map.put("unFinished", "AND sec.end_time > "+ "'" +  sd.format(currDate) + "'");
					break;
				case NOT_STARTED :
					map.put("status", SeckillStatusEnum.ONLINE.value());
					map.put("unStarted", "AND sec.start_time > "+"'" +  sd.format(currDate) + "'");
					break;
				case ONLINE :
					map.put("status", SeckillStatusEnum.ONLINE.value());
					map.put("isStarted", "AND sec.start_time < "+"'" +  sd.format(currDate) + "'");
					map.put("unFinished", "AND sec.end_time > "+ "'" +  sd.format(currDate) + "'");
					break;
				case FINISHED :
					map.put("isFinished", "AND sec.end_time < "+ "'" +  sd.format(currDate) + "'");
					break;
				case EXPIRED :
					map.put("status", GroupStatusEnum.OFFLINE.value());
					map.put("unFinished", "AND sec.end_time > "+ "'" +  sd.format(currDate) + "'");
					break;
				default :
			}
		}


		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class);
		query.setPageSize(pageSize);
		query.setCurPage(curPageNO);
		query.setAllCountString(ConfigCode.getInstance().getCode("seckill.querySeckillActivityListCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("seckill.querySeckillActivityList", map));
		map.remove("isStarted");
		map.remove("unStarted");
		map.remove("isFinished");
		map.remove("unFinished");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 查找拍卖列表 
	 */
	@Override
	public PageSupport<SeckillActivity> queryAuctionList(String curPageNO, String startTime) {
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		Calendar periodStart = Calendar.getInstance();
		periodStart.setTime(new Date());
		periodStart.set(Calendar.SECOND, 0);
		periodStart.set(Calendar.MINUTE, 0);
		periodStart.set(Calendar.MILLISECOND, 0);
		Calendar periodEnd = Calendar.getInstance();
		periodEnd.setTime(new Date());
		periodEnd.set(Calendar.SECOND, 0);
		periodEnd.set(Calendar.MINUTE, 0);
		periodEnd.set(Calendar.MILLISECOND, 0);


		Integer startTimeInt = null;
		try{
			startTimeInt = Integer.parseInt(startTime);
		}catch (Exception e){
			startTimeInt = null;
		}

		if (AppUtils.isBlank(startTime)) {
			periodStart.set(Calendar.HOUR_OF_DAY, getPeriodTime(periodStart.get(Calendar.HOUR_OF_DAY)));// 当前时间所在时间段
			// 起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = periodStart.get(Calendar.HOUR_OF_DAY) + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, getPeriodTime(foo));// 当前时间所在时间段
			// 结束时间点
			map.put("periodEnd", periodEnd.getTime());
		} else {
			periodStart.set(Calendar.HOUR_OF_DAY, startTimeInt);// 当前点击的时间段
			// 起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = startTimeInt + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, foo); // 当前点击的时间段 结束时间点
			// 结束时间点
			map.put("periodEnd", periodEnd.getTime());
		}
		// 获得当前选择的时间段
		// 活动开始时间和结束时间在该时段之内
		String querySQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 获取秒杀活动列表 
	 */
	@Override
	public PageSupport<SeckillActivity> queryFrontSeckillList(String curPageNO, String startTime) {
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		Calendar periodStart = Calendar.getInstance();
		periodStart.setTime(new Date());
		periodStart.set(Calendar.SECOND, 0);
		periodStart.set(Calendar.MINUTE, 0);
		periodStart.set(Calendar.MILLISECOND, 0);
		Calendar periodEnd = Calendar.getInstance();
		periodEnd.setTime(new Date());
		periodEnd.set(Calendar.SECOND, 0);
		periodEnd.set(Calendar.MINUTE, 0);
		periodEnd.set(Calendar.MILLISECOND, 0);


		Integer startTimeInt = null;
		try{
			startTimeInt = Integer.parseInt(startTime);
		}catch (Exception e){
			startTimeInt = null;
		}
		
		//商品正常状态下，前台才显示秒杀活动
		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		
		if (startTime != null) {
			periodStart.set(Calendar.HOUR_OF_DAY, startTimeInt);// 当前点击的时间段
																				// 起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = startTimeInt + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, foo); // 当前点击的时间段 结束时间点
			map.put("periodEnd", periodEnd.getTime());
		} else {
			periodStart.set(Calendar.HOUR_OF_DAY, getPeriodTime(periodStart.get(Calendar.HOUR_OF_DAY)));// 当前时间所在时间段起始时间点
			map.put("periodStart", periodStart.getTime());
			int foo = periodStart.get(Calendar.HOUR_OF_DAY) + 3;
			periodEnd.set(Calendar.HOUR_OF_DAY, getPeriodTime(foo));// 当前时间所在时间段结束时间点
			map.put("periodEnd", periodEnd.getTime());

			// periodStart.set(Calendar.HOUR_OF_DAY,
			// Integer.parseInt(startTime));//当前时间所在时间段 起始时间点
			// map.put("periodStart", periodStart.getTime());
			// int foo =Integer.parseInt(startTime) + 3;
			// periodEnd.set(Calendar.HOUR_OF_DAY, foo);//当前时间所在时间段 结束时间点
			// map.put("periodEnd", periodEnd.getTime());
		}
		// 获得当前选择的时间段
		// 活动开始时间和结束时间在该时段之内
		String querySQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.getFrontSeckillListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 查询用户秒杀记录 
	 */
	@Override
	public PageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(SeckillSuccessRecord.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String querySQL = ConfigCode.getInstance().getCode("seckill.querySeckillRecord", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.querySeckillRecordCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public long findSeckillSuccess(Long seckillId, long prodId, long skuId, String userId) {
		String sql="select count(id) from ls_seckill_success where seckill_id=? and user_id=?  and prod_id=? and sku_id=? ";
		return getLongResult(sql, seckillId,userId,prodId,skuId);
	}

	@Override
	public boolean findIfJoinSeckill(Long productId) {
		String sql = "SELECT count(1) FROM ls_seckill_activity INNER JOIN  ls_seckill_prod  ON ls_seckill_activity.id=ls_seckill_prod.s_id " +
				" WHERE ls_seckill_activity.status=1 AND ls_seckill_prod.prod_id=? AND ls_seckill_activity.end_time>=?";
		return get(sql, Integer.class, productId,new Date())>0;
	}

	@Override
	public SeckillActivity getSeckillActivityByShopIdAndActiveId(Long shopId, Integer activeId) {
		return getByProperties(new EntityCriterion().eq("shopId", shopId).eq("id", activeId));
	}

	/**
	 * 获取过期未完成的秒杀活动
	 */
	@Override
	public List<SeckillActivity> getSeckillActivityByTime(Date now) {
		return queryByProperties(new EntityCriterion().notEq("status", SeckillStatusEnum.FINISH.value()).le("endTime", now));
	}

	@Override
	public PageSupport<SeckillSuccess> querySeckillRecordListPage(String curPageNO, Long id) {
		QueryMap map = new QueryMap();
		map.put("seckillId", id);
		SimpleSqlQuery hql = new SimpleSqlQuery(SeckillSuccess.class);
		hql.setPageSize(5);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("seckill.querySeckillRecordListCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("seckill.querySeckillRecordList", map));
		return querySimplePage(hql);
	}

	/**
	 * 查找拍卖列表
	 */
	@Override
	public PageSupport<SeckillActivity> queryAuctionList(AppSecKillActivityQueryDTO queryDTO) {
		if (queryDTO.getCurPage() == null || queryDTO.getCurPage() < 1) {
			queryDTO.setCurPage(1);
		}

		if (queryDTO.getPageSize() == null || queryDTO.getPageSize() < 1) {
			queryDTO.setPageSize(10);
		}

		SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class, queryDTO.getPageSize(), queryDTO.getCurPage().toString());
		QueryMap map = new QueryMap();

		if (null != queryDTO.getStatus()) {
			Date now = new Date();
			switch (queryDTO.getStatus()){
				case -1:
					map.put("over",now);
					break;
				case 0:
					map.put("waiting",now);
					break;
				case 1:
					map.put("processing",now);
					break;
				default:
					break;
			}
		}

		// 获得当前选择的时间段
		// 活动开始时间和结束时间在该时段之内
		String querySQL = ConfigCode.getInstance().getCode("seckill.findSecKillList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("seckill.findSecKillListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
}
