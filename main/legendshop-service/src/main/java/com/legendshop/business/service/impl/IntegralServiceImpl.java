package com.legendshop.business.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IntegralLogTypeEnum;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.dto.integral.IntegralRuleDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.spi.service.IntegralService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 送积分服务
 */
@Service("integralService")
public class IntegralServiceImpl implements IntegralService {

	private final static Logger LOG = LoggerFactory.getLogger(IntegralServiceImpl.class);
	
	@Autowired
	private ConstTableDao constTableDao;
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private IntegraLogDao integraLogDao;
	
	/**
	 * 赠送积分
	 */
	@Override
	public void addScore(String userId,SendIntegralRuleEnum ruleEnum) {
		if(AppUtils.isBlank(userId) || AppUtils.isBlank(ruleEnum) ){
			LOG.warn("userId or ruleEnum is Null");
			return;
		}
		ConstTable constTable =constTableDao.getConstTablesBykey("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING");
		if(AppUtils.isNotBlank(constTable) ){
			String setting=constTable.getValue();
			IntegralRuleDto releSetting=JSONUtil.getObject(setting, IntegralRuleDto.class);
			if(AppUtils.isNotBlank(releSetting) && Constants.ONLINE.equals(releSetting.getStatus())){
				Score score=getRuleScore(ruleEnum, releSetting);
				if(score!=null && score.getScore() != null && score.getScore()>0){
					LOG.info("<!------send score {} for userId={} , ruleEnum={}" , score.getScore(),userId,ruleEnum.value());
					 //更新用户积分
					int result=userDetailDao.updateScore(score.getScore(),userId);
					if(result>0){
						//记录积分明细
						IntegraLog integraLog=new IntegraLog();
						integraLog.setIntegralNum(score.getScore());
						integraLog.setLogType(score.getLogTypeEnum().value());
						integraLog.setAddTime(new Date());
						integraLog.setLogDesc(score.getLogDesc());
						integraLog.setUserId(userId);
						integraLogDao.saveIntegraLog(integraLog);
					}
					LOG.info("-----!>");
				}
			}
		}
	}
	
	private Score getRuleScore(SendIntegralRuleEnum task,IntegralRuleDto releSetting){
		Score score=null;
		switch (task) {
			case USER_REGISTER: // 用户注册
				if(AppUtils.isNotBlank(releSetting.getReg())){
					score=new Score(releSetting.getReg(), "用户注册送积分", IntegralLogTypeEnum.LOG_REG);
				}
				break;
	       case USER_LOGIN: //用户登录
	    	   if(AppUtils.isNotBlank(releSetting.getLogin())){
	    		   score=new Score(releSetting.getLogin(), "用户登录送积分", IntegralLogTypeEnum.LOG_LOGIN);
	    	   }
			    break;
	       case VAL_EMAIL:  //校验邮箱
	    	   if(AppUtils.isNotBlank(releSetting.getVerifyEmail())){
	    		   score=new Score(releSetting.getVerifyEmail(), "完善用户资料送积分-验证邮箱", IntegralLogTypeEnum.PERFECT_USER_INFO);
	    	   }
	    	   break;
	       case VAL_PHONE:  //校验短信
	    	   if(AppUtils.isNotBlank(releSetting.getVerifyMobile())){
	    		   score=new Score(releSetting.getVerifyMobile(), "完善用户资料送积分-验证手机", IntegralLogTypeEnum.PERFECT_USER_INFO);
	    	   }
				break;
	       case ORDER_COMMENT: //订单评论
	    	   if(AppUtils.isNotBlank(releSetting.getProductReview())){
	    		   score=new Score(releSetting.getProductReview(), "订单评论送积分", IntegralLogTypeEnum.ORDER_COMMENT);
	    	   }
				break;
	       case RECOMMEND_USER: //推荐用户
	    	   if(AppUtils.isNotBlank(releSetting.getRecUser())){
	    		   score=new Score(releSetting.getRecUser(), "推荐用户送积分", IntegralLogTypeEnum.RECOMMEND_USER);
	    	   }
				break;
	       case SUN_ORDER:  //晒单
	    	   if(AppUtils.isNotBlank(releSetting.getShowOrder())){
			    score=new Score(releSetting.getShowOrder(), "用户晒单送积分", IntegralLogTypeEnum.SUN_ORDER);
	    	   }
				break;
	       case ORDER_CONSUMER: //订单送积分
	    	   if(AppUtils.isNotBlank(releSetting.getIntegralConvertRelu())){
	    		   score=new Score(releSetting.getIntegralConvertRelu(), "订单送积分", IntegralLogTypeEnum.ORDER_CONSUMER);
	    	   }
				break;
			default:
				break;
		}
		return score;
	}
	
	class Score{
		
		private Integer score;  //赠送积分
		
		private String logDesc;
		
		
		private IntegralLogTypeEnum logTypeEnum;


		public Integer getScore() {
			return score;
		}


		public void setScore(Integer score) {
			this.score = score;
		}


		public String getLogDesc() {
			return logDesc;
		}


		public void setLogDesc(String logDesc) {
			this.logDesc = logDesc;
		}


		public IntegralLogTypeEnum getLogTypeEnum() {
			return logTypeEnum;
		}


		public void setLogTypeEnum(IntegralLogTypeEnum logTypeEnum) {
			this.logTypeEnum = logTypeEnum;
		}


		public Score(Integer score, String logDesc,
				IntegralLogTypeEnum logTypeEnum) {
			super();
			this.score = score;
			this.logDesc = logDesc;
			this.logTypeEnum = logTypeEnum;
		}
		
	}
}
