/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SystemParameterDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.util.AppUtils;

/**
 * 系统参数Dao
 */
@Repository
public class SystemParameterDaoImpl extends GenericDaoImpl<SystemParameter, String> implements SystemParameterDao {

	@Override
	public void deleteSystemParameterById(String id) {
		deleteById(id);
	}

	@Override
	public void updateSystemParameter(SystemParameter systemParameter) {
		update(systemParameter);
	}

	@Override
	public List<SystemParameter> loadAll() {
		return queryAll();
	}

	@Override
	public SystemParameter getSystemParameterById(String name) {
		return this.getByProperties(new EntityCriterion().eq("name", name));
	}

	@Override
	public List<SystemParameter> getSystemParameterByGroupId(String groupId) {
		return this.queryByProperties(new EntityCriterion().eq("groupId", groupId));
	}

	@Override
	public PageSupport<SystemParameter> getSystemParameterListPage(String curPageNO, String name, String groupId) {
		CriteriaQuery cq = new CriteriaQuery(SystemParameter.class, curPageNO);
		cq.setPageSize(30);
		if(AppUtils.isNotBlank(name)) {
			cq.like("memo","%" + name.trim() + "%");	 //中文说明		
		}
		if(AppUtils.isNotBlank(groupId)) {
			cq.eq("groupId",groupId);
		}
		//cq.eq("changeOnline", "Y");
		cq.addOrder("asc", "displayOrder");
		return queryPage(cq);
	}

}
