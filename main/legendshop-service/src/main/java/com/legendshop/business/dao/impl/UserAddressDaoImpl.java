/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserAddressDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.util.AppUtils;

/**
 * 用户订单地址Dao
 */
@Repository
public class UserAddressDaoImpl extends GenericDaoImpl<UserAddress, Long> implements UserAddressDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public List<UserAddress> getUserAddress(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName).addDescOrder("commonAddr").addDescOrder("createTime"));
	}

	public UserAddress getUserAddress(Long id) {
		return get(ConfigCode.getInstance().getCode("biz.getAddressByIdSQL"),UserAddress.class,id);
	}

	public int deleteUserAddress(UserAddress userAddress) {
		return  delete(userAddress);
	}

	public Long saveUserAddress(UserAddress userAddress) {
		return (Long) save(userAddress);
	}

	public void updateUserAddress(UserAddress userAddress) {
		update(userAddress);
	}

	@Override
	public Long getMaxNumber(String userName) {
		return getLongResult("select count(*) from ls_usr_addr where user_name = ?", userName);
	}

	@Override
	public void updateDefaultUserAddress(Long addrId,String userId) {
		//1.更新原来的收货地址
		update("update ls_usr_addr set common_addr = 0 where common_addr = 1 and user_id = ?", userId);
		
		//2.更新新的收货地址
		update("update ls_usr_addr set common_addr = 1 where addr_id = ?  and user_id = ? ", addrId, userId);
	}

	/**
	 * 取用户的默认地址
	 */
	@Override
	public UserAddress getDefaultAddress(String userId) {
		return get(ConfigCode.getInstance().getCode("biz.getDefaultAddressSQL"),UserAddress.class,userId);
	}

	@Override
	public long getUserAddressCount(String userId) {
		return getLongResult("select count(*) from ls_usr_addr where user_id = ?", userId);
	}

	@Override
	public UserAddress getFirstAddress(String userId) {
		
		String sql = ConfigCode.getInstance().getCode("biz.getFirstAddressSQL");
		List<UserAddress> userAddress = queryLimit(sql, UserAddress.class, 0, 1, userId);
		
		if(AppUtils.isNotBlank(userAddress)){
			return userAddress.get(0);
		}
		
		return null;
	}
	
	@Override
	public PageSupport<UserAddress> UserAddressPage(String curPageNO,String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(UserAddress.class,20, curPageNO);
        QueryMap map = new QueryMap();
		map.put("userId", userId);
        String queryAllSQL = ConfigCode.getInstance().getCode("uc.prod.getUserAddressCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.prod.getUserAddress", map); 
		query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}
 
	@Override
	public PageSupport<UserAddress> queryUserAddress(String curPageNO,String userId) {
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getUserAddressCount",map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getUserAddress",map);
		
		//地址最多显示20条，pagesize=20
		SimpleSqlQuery simpleSqlQuery =new SimpleSqlQuery(UserAddress.class,20,curPageNO);
		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(map.toArray());
		return querySimplePage(simpleSqlQuery);
	}

	@Override
	public PageSupport<UserAddress> getUserAddressPage(String curPageNO, UserAddress userAddress) {
		CriteriaQuery cq = new CriteriaQuery(UserAddress.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		cq.eq("userName", userAddress.getUserName());
		return queryPage(cq);
	}

	@Override
	public PageSupport<UserAddress> queryPage(String curPageNO, String userName) {
		CriteriaQuery cq = new CriteriaQuery(UserAddress.class, curPageNO);
        cq.eq("userName", userName);
        cq.addOrder("desc", "commonAddr");
        cq.addOrder("desc", "createTime");
		return queryPage(cq);
	}

	@Override
	public void updateOtherDefault(Long curAddrId, String userId, String commonAddr) {
		String sql = "update ls_usr_addr set common_addr = ? where addr_id <> ? AND user_id = ? AND common_addr = 1  ";
		
		this.update(sql, commonAddr, curAddrId, userId);
		
	}

	@Override
	public PageSupport<UserAddress> queryUserAddress(String userId, String curPageNO, int pageSize) {
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getUserAddressCount",map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getUserAddress",map);
		SimpleSqlQuery simpleSqlQuery =new SimpleSqlQuery(UserAddress.class,curPageNO);
		simpleSqlQuery.setPageSize(pageSize);
		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(map.toArray());
		return querySimplePage(simpleSqlQuery);
	}
}
