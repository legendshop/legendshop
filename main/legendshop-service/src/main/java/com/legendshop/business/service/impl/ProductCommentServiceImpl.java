/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.biz.model.dto.AppQueryProductCommentsParams;
import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.business.dao.DvyTypeCommStatDao;
import com.legendshop.business.dao.ProdAddCommDao;
import com.legendshop.business.dao.ProdUsefulDao;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.business.dao.ProductCommentStatDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ShopCommStatDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ProductCommentsQueryParams;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.dto.ShopCommProdDto;
import com.legendshop.model.dto.UserAddCommentDto;
import com.legendshop.model.dto.UserCommentDto;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.model.entity.ShopCommStat;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.form.ProductForm;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 产品评论服务.
 */
@Service("productCommentService")
public class ProductCommentServiceImpl implements ProductCommentService {

	@Autowired
	private ProductCommentDao productCommentDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private SubDao subDao;
	
	@Autowired
	private SubItemDao subItemDao;
	
	@Autowired
	private  ConstTableDao constTableDao;
	
	@Autowired
	private ProductCommentStatDao productCommentStatDao;
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private ProdAddCommDao prodAddCommDao;
	
	@Autowired
	private ProdUsefulDao prodUsefulDao;
	
	@Autowired
	private ShopCommStatDao shopCommStatDao;
	
	@Autowired
	private DvyTypeCommStatDao dvyTypeCommStatDao;

	@Autowired
	private ProductCommentService productCommentService;
	
	@Resource(name="productIndexUpdateProcessor")
	private EventProcessor<ProductIdDto> productIndexUpdateProcessor;

	@Override
	public ProductComment getProductCommentById(Long id) {
		return productCommentDao.getById(id);
	}

	@Override
	public void delete(Long id) {
		productCommentDao.deleteProductCommentById(id);
	}

	/**
	 * 保存商品评论
	 */
	@Override
	public Long save(ProductComment productComment) {
		if (!AppUtils.isBlank(productComment.getId())) {
			update(productComment);
			return productComment.getId();
		}
		
		if(AppUtils.isBlank(productComment.getOwnerName()) && AppUtils.isNotBlank(productComment.getProdId())){
			String ownerName = productCommentDao.getOwnerName(productComment.getProdId());
			productComment.setOwnerName(ownerName);
		}
		
		//prod更新reviewScores 字段
		//productDao.updateReviewScores(productComment.getScore(), productComment.getComments(), productComment.getProdId());
		
		return (Long) productCommentDao.save(productComment);
	}

	@Override
	public void update(ProductComment productComment) {
		productCommentDao.updateProductComment(productComment);
	}

	@Override
	public String validateComment(Long prodId, String userName) {
		return productCommentDao.validateComment(prodId, userName);
	}

	@Override
	public ProductCommentCategory initProductCommentCategory(Long prodId) {
		return productCommentDao.initProductCommentCategory(prodId);
	}

	@Override
	public String getproductName(Long prodId) {
		return productCommentDao.getproductName(prodId);
	}

	@Override
	public List<ProductForm> getProductList(String userId, Long prodId) {
		return productCommentDao.getProductList(userId, prodId);
	}

	@Override
	public void delete(List<Long> idList) {
		if(AppUtils.isNotBlank(idList)){
			for (Long id : idList) {
				productCommentDao.deleteProductCommentById(id);
			}
		}
	}
	
	@Override
	public ProductComment getProdCommentByprodId(Long subItemId) {
		return productCommentDao.getProdCommentByprodId(subItemId);
	}

	@Override
	public int updateUsefulCounts(Long prodComId, String userId) {
		return productCommentDao.updateUsefulCounts(prodComId,userId);
	}

	/**
	 * 查询用户是否 可以评论此商品
	 */
	@Override
	public boolean canCommentThisProd(Long prodId, Long subItemId, String userId) {
		return productCommentDao.canCommentThisProd(prodId,subItemId,userId);
	}

	/**
	 * 获取商品的评论
	 */
	@Override
	@Cacheable(value = "ProductCommentList",  condition = "T(Integer).parseInt(#params.curPageNO) < 3", key="#params.curPageNO + '.' + #params.condition + '.' + #params.prodId")
	public PageSupport<ProductCommentDto> queryProductComment(ProductCommentsQueryParams params) {
		String curPageNO = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(ProductCommentDto.class, 8, curPageNO);
		QueryMap map = new QueryMap();
		
		map.put("prodId", params.getProdId());
		String condition = params.getCondition();
		
		if(!"all".equals(condition)){//如果不是全部评论
			if("good".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 4"); 
			}else if("medium".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 3 AND c.score < 4"); 
			}else if("poor".equals(condition)){
				map.put("scoreOperater", " AND c.score < 3");
			}else if("photo".equals(condition)){
				map.put("isHasPhoto", " AND (c.photos IS NOT NULL OR ac.photos IS NOT NULL)");
			}else if("append".equals(condition)){
				map.put("isAddComm", 1);
			}
		}
	     
	  	 String queryAllSQL = ConfigCode.getInstance().getCode("prod.queryProductCommentsCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("prod.queryProductComments", map); 
		 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 
		 map.remove("scoreOperater");
		 map.remove("isHasPhoto");
		 
		 query.setParam(map.toArray());
		 
		 return productCommentDao.querySimplePage(query);
	}

	@Override
	public boolean commentTimeout(Long subItemId) {
		ConstTable constTable=constTableDao.getConstTablesBykey("ORDER_SETTING","ORDER_SETTING");
		OrderSetMgDto orderSetting=null;
		Integer commenTime = null;
		if(AppUtils.isNotBlank(constTable)){
			String setting=constTable.getValue();
			orderSetting=JSONUtil.getObject(setting, OrderSetMgDto.class);
			if(AppUtils.isNotBlank(orderSetting)&&AppUtils.isNotBlank(orderSetting.getAuto_order_comment())){
				commenTime=Integer.valueOf(orderSetting.getAuto_order_comment());
				if(commenTime==0){
					return true;
				}
			}else{
				return true;
			}
		}
		SubItem subitem=subItemDao.getSubItem(subItemId);
		Date buyTime=subitem.getSubItemDate();
		//获得往后面推的时间
		Date x=getfinishUnAcklodgeDate(commenTime);
		if(x.getTime()<buyTime.getTime()){ //可以评论
			return true;
		}
		return false;
	}
	
	/**
	 * 得到跟现在若干天时间，如果为负数则向前推.
	 * 
	 * @param days
	 *            the days
	 * @return the date
	 */
	private Date getfinishUnAcklodgeDate(int days) {
		Date myDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.DATE, -days);
		return cal.getTime();
	}

	@Override
	public void setGoodComments(List<Product> productList) {
		NumberFormat nt = NumberFormat.getPercentInstance();
		nt.setMinimumFractionDigits(1);
		if(AppUtils.isNotBlank(productList)){
			for(int i=0;i<productList.size();i++){
				if(AppUtils.isNotBlank(productList.get(i).getComments()) && productList.get(i).getComments()!=0){
					//通过商品ID找到改商品好评(score>3)的数目
					Long num = productCommentDao.setGoodComments(productList.get(i).getProdId());
					double percent = (double)num / (double)productList.get(i).getComments();
					if (percent > 1d) {
						percent = 1d;
					}
					productList.get(i).setGoodCommentsPercent(nt.format(percent));
				}
			}
		}
	}

	@Override
	public boolean saveProductComment(ProductComment productComment) {
		
		Long commId = productCommentDao.saveProductComment(productComment);
		
		if(null == commId){
			return false;
		}
		
		String userId = productComment.getUserId();
		Long subItemId = productComment.getSubItemId();
		
		//更新订单项 的评论状态
		subItemDao.updateSubItemCommSts(subItemId, userId);
		
		if(ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus())){
			//更新评论统计信息, 包括评论统计, 店铺评分统计, 物流评分统计, 以及商品的评论数和分数
			updateCommentStatInfo(productComment);
		}
		
		return true;
	}
	
	/**
	 * 更新评论统计信息, 包括评论统计, 店铺评分统计, 物流评分统计, 以及商品的评论数和分数
	 * @param productComment
	 */
	@Override
	public void updateCommentStatInfo(ProductComment productComment){
		Long prodId = productComment.getProdId();
		Integer score = productComment.getScore();
		Long subItemId = productComment.getSubItemId();
		
		//保存评论统计表
		ProductCommentStat productCommentStat = productCommentStatDao.getProductCommentStatByProdIdForUpdate(prodId);
		if(AppUtils.isBlank(productCommentStat)){
			productCommentStat = new ProductCommentStat();
			productCommentStat.setProdId(prodId);
			productCommentStat.setScore(score);
			//评论数
			productCommentStat.setComments(1);
			productCommentStatDao.saveProductCommentStat(productCommentStat);
		}else{
			productCommentStat.setScore(productCommentStat.getScore() + score);
			productCommentStat.setComments(productCommentStat.getComments() + 1);
			productCommentStatDao.updateProductCommentStat(productCommentStat);
		}
		
		Sub sub = subDao.getSubBySubItemId(subItemId);
		
		//保存或更新店铺评分统计
		ShopCommStat shopCommStat = shopCommStatDao.getShopCommStatByShopIdForUpdate(sub.getShopId());
		if(null == shopCommStat){
			shopCommStat = new ShopCommStat();
			shopCommStat.setShopId(sub.getShopId());
			shopCommStat.setCount(1);
			shopCommStat.setScore(productComment.getShopScore());
			shopCommStatDao.saveShopCommStat(shopCommStat);
		}else{
			shopCommStat.setCount(shopCommStat.getCount() + 1);
			shopCommStat.setScore(shopCommStat.getScore() + productComment.getShopScore());
			shopCommStatDao.updateShopCommStat(shopCommStat);
		}
		
		//保存或更新物流评分统计表
		DvyTypeCommStat dvyTypeCommStat = dvyTypeCommStatDao.getDvyTypeCommStatByDvyTypeIdForUpdate(sub.getDvyTypeId());
		if(null == dvyTypeCommStat){
			dvyTypeCommStat = new DvyTypeCommStat();
			dvyTypeCommStat.setDvyTypeId(sub.getDvyTypeId());
			dvyTypeCommStat.setCount(1);
			dvyTypeCommStat.setScore(productComment.getLogisticsScore());
			dvyTypeCommStatDao.saveDvyTypeCommStat(dvyTypeCommStat);
		}else{
			dvyTypeCommStat.setCount(dvyTypeCommStat.getCount() + 1);
			dvyTypeCommStat.setScore(dvyTypeCommStat.getScore() + productComment.getLogisticsScore());
			dvyTypeCommStatDao.updateDvyTypeCommStat(dvyTypeCommStat);
		}
		
		/** 更新评论得分 和 评论数 , 评论得分是累加, 评论数是直接更新*/
		productDao.updateReviewScoresAndComments(productComment.getScore(), productCommentStat.getComments(), productComment.getProdId());
		
		ProductIdDto productIdDto = new ProductIdDto(prodId);
		
		/** 更新了评论数  需要更新商品索引 */
		productIndexUpdateProcessor.process(productIdDto);
	}

	@Override
	public Boolean findByProdIdandUserIdandItemId(Long prodId, String userId,Long itemId) {
		Long count=productCommentDao.findByProdIdandUserIdandItemId(prodId,userId,itemId);
		return count!=null&&count>0;
	}

	@Override
	public Map<String, Object> handleAnonymousComments(String userName, boolean isAnonymous) {
		
		Map<String, Object> result = new HashedMap<String, Object>();
		
		String name = userDetailDao.getNickNameByUserName(userName);
		if(AppUtils.isBlank(name)){
			name = userName;
		}
		
		if(isAnonymous){
			result.put("isAnonymous", 1);
			result.put("userName", userNameSubstitution(name));
		}else{
			result.put("isAnonymous", 0);
			result.put("userName", name);
		}
		
		return result;

	}
	
	/**
	 * 匿名处理
	 * @param userName
	 * @return
	 */
	private String userNameSubstitution(String userName){
		int userNameLength = userName.length();
		
		StringBuilder sb = new StringBuilder();
		if(userNameLength > 2){
			String prefix = userName.substring(0, 1);
			sb.append(prefix);
			for (int i = 1; i < userNameLength - 1; i++) {
				sb.append("*");
			}
			String suffix = userName.substring(userNameLength -1, userNameLength);
			sb.append(suffix);
		}else if(userNameLength == 2){
			String prefix = userName.substring(0, 1);
			sb.append(prefix);
			sb.append("*******");
		}else{
			sb.append("********");
		}
		
		return sb.toString();	
	}

	@Override
	public String isCanAddComment(Long prodCommId, String userId) {
		ProductComment productComment = productCommentDao.getById(prodCommId);
		
		if(null == productComment || !ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus()) || productComment.getIsAddComm()){
			return "对不起，您要追评的评论不存在了！!";
		}
		
		Product prod = productDao.getProduct(productComment.getProdId());
		if(null == prod){
			return "对不起, 您要追加评论的商品不存在了!";
		}
		
		return Constants.SUCCESS;
	}

	@Override
	public boolean addProdComm(ProdAddComm prodAddComm) {
		
		Long id = prodAddCommDao.saveProdAddComm(prodAddComm);
		
		if(null != id){
			
			int result = productCommentDao.updateIsAddComm(true, prodAddComm.getProdCommId());
			if(result == 0){
				throw new RuntimeException("更新isAddComm失败!");
			}
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isAlreadyUseful(Long prodComId, String userId) {
		
		return prodUsefulDao.getUseful(userId, prodComId) != 0;
	}

	@Override
	public ProductComment getProductComment(Long commId, String userId) {
		
		return productCommentDao.getProductComment(commId, userId);
	}

	@Override
	public ProductCommentDto getProductCommentDetail(Long prodComId,String userId) {
		ProductCommentDto prodComm = productCommentDao.getProductCommentDetail(prodComId);
		boolean isAlreadyUseful = productCommentService.isAlreadyUseful(prodComm.getId(), userId);
		prodComm.setAlreadyUseful(isAlreadyUseful);
		//如果追评不为空，则获取多少天后追评的
		prodComm.setAppendDays(prodComm.getAppendDays());
		return prodComm;
	}

	@Override
	public UserCommentDto getUserCommentDto(Long prodId, Long subItemId,
			String userId) {
		String sql = "SELECT subItem.sub_item_id AS subItemId, subItem.prod_id AS prodId, subItem.prod_name AS prodName, " +
				"subItem.attribute AS attribute, subItem.pic AS prodPic, subItem.sub_item_date AS buyTime " +
				"FROM ls_sub_item subItem " +
				"WHERE subItem.sub_item_id = ? " +
				"AND subItem.prod_id = ? " +
				"AND subItem.user_id = ?";
		
		return productCommentDao.get(sql, UserCommentDto.class, subItemId, prodId, userId);
	}

	@Override
	public UserAddCommentDto getUserAddCommentDto(Long prodCommId, String userId) {
		
		String sql = "SELECT c.id AS prodCommId, subItem.sub_item_id AS subItemId, subItem.prod_id AS prodId, " +
				"subItem.prod_name AS prodName, subItem.attribute AS attribute, subItem.pic AS prodPic, " +
				"subItem.sub_item_date AS buyTime, c.score AS score, c.content AS content, " +
				"c.photos AS photos, c.addtime AS  commentTime " +
				"FROM ls_sub_item subItem, ls_prod_comm c " +
				"WHERE subItem.sub_item_id = c.sub_item_id " +
				"AND c.id = ? " + 
				"ANd c.user_id = ? ";
		
		return productCommentDao.get(sql, UserAddCommentDto.class, prodCommId, userId);
	}

	@Override
	public void auditFirstComment(ProductComment productComment, Integer auditStatus) {
		
		productComment.setStatus(auditStatus);
		int result = productCommentDao.update(productComment);
		
		if(result > 0){
			if(ProdCommStatusEnum.SUCCESS.value().equals(auditStatus)){//如果是审核通过
				//更新评论统计信息, 包括评论统计, 店铺评分统计, 物流评分统计, 以及商品的评论数和分数
				updateCommentStatInfo(productComment);
			}
		}
	}
	
	@Override
	public PageSupport<ProductReply> replyCommentList(String curPageNO, Long parentReplyId) {
		return productCommentDao.replyCommentList(curPageNO,parentReplyId);
	}
	
	@Override
	public PageSupport<ProductComment> prodComment(String curPageNO,Long subId,String userName) {
		return productCommentDao.prodComment(curPageNO,subId,userName);
	}
	
	@Override
	public PageSupport<ProductComment> prodCommentState(String curPageNO,String state,String userName) {
		return productCommentDao.prodCommentState(curPageNO,state,userName);
	}
	
	@Override
	public PageSupport<ProductCommentDto> queryProdCommentByState(String curPageNO,String state,String userName) {
		return productCommentDao.queryProdCommentByState(curPageNO,state,userName);
	}

	@Override
	public PageSupport<ProductComment> query(String curPageNO, String userName, Long subId) {
		return productCommentDao.querySimplePage(curPageNO,userName,subId);
	}

	@Override
	public PageSupport<ProductComment> query(String curPageNO, String userName) {
		return productCommentDao.querySimplePage(curPageNO,userName);
	}
	
	@Override
	public PageSupport<ShopCommProdDto> queryProdComment(String curPageNO, Long shopId) {
		PageSupport<ShopCommProdDto> ps = productCommentDao.queryProdComment(curPageNO,shopId);
		for (ShopCommProdDto shopCommProdDto : ps.getResultList()) {
			Integer count = productCommentDao.getWaitReplyShopComm(shopCommProdDto.getProdId());
			shopCommProdDto.setWaitReply(count);
		}
		return ps;
	}

	@Override
	public PageSupport<ProductCommentDto> queryProductCommentDto(ProductCommentsQueryParams params) {
		return productCommentDao.queryProductCommentDto(params);
	}

	@Override
	public PageSupport<ProductCommentDto> getProductCommentList(String curPageNO, ProductComment productComment,
			Integer pageSize, String status, DataSortResult result) {
		return productCommentDao.getProductCommentList(curPageNO,productComment,pageSize,status,result);
	}

	@Override
	public PageSupport<ProductCommentDto> getProductCommentListPage(String curPageNO, ProductComment productComment,
			String userId, String userName, Integer pageSize, DataSortResult result) {
		return productCommentDao.getProductCommentListPage(curPageNO,productComment,userId,userName,pageSize,result);
	}

	@Override
	public PageSupport getProdCommentList(Long shopId, String curPageNO, int pageSize) {
		return productCommentDao.getProdCommentList(shopId, curPageNO, pageSize);
	}

	/**
	 * 给App商家端使用
	 * @param params
	 * @param pageSize
	 * @return
	 */
	@Override
	public PageSupport getProdComment(AppQueryProductCommentsParams params, int pageSize) {
		return productCommentDao.getProdComment(params, pageSize);
	}

	@Override
	public PageSupport<UserCommentProdDto> queryComments(String userName, String curPageNO, int pageSize) {
		return productCommentDao.queryComments(userName, curPageNO, pageSize);
	}

}
