package com.legendshop.business.comparer;

import java.util.Date;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.entity.ProdPropImage;
import com.legendshop.model.entity.Product;
import com.legendshop.util.AppUtils;

public class PropValueImageComparator implements DataComparer<PropValueImgDto,ProdPropImage>{

	@Override
	public boolean needUpdate(PropValueImgDto newPropImg, ProdPropImage originPropImage, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(PropValueImgDto newPropImg, ProdPropImage originPropImage) {
		if(AppUtils.isNotBlank(newPropImg) && AppUtils.isNotBlank(originPropImage)){
			if(originPropImage.getPropId().equals(newPropImg.getPropId()) &&
					originPropImage.getValueId().equals(newPropImg.getValueId()) &&
						originPropImage.getUrl().equals(newPropImg.getValueImage()) &&
							originPropImage.getSeq().equals(newPropImg.getSeq()) ){
				return true;
			}else{
				//newPropImg.setSeq(originPropImage.getSeq());
				return false;
			}
		}
		return false;
	}

	@Override
	public ProdPropImage copyProperties(PropValueImgDto newPropImg, Object obj) {
		Product product =(Product)obj;
		
		ProdPropImage prodPropImage = new ProdPropImage();
		prodPropImage.setProdId(product.getProdId());
		prodPropImage.setCreateDate(new Date());
		prodPropImage.setPropId(newPropImg.getPropId());
		prodPropImage.setSeq(newPropImg.getSeq());
		prodPropImage.setUrl(newPropImg.getValueImage());
		prodPropImage.setValueId(newPropImg.getValueId());
		prodPropImage.setValueName(newPropImg.getValueName());
		
		return prodPropImage;
	}

}
