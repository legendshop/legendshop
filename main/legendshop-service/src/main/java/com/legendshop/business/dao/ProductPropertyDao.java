/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;

/**
 * 商品属性Dao.
 */
public interface ProductPropertyDao extends GenericDao<ProductProperty, Long>{
	
	public List<ProductPropertyDto> getPublishProductDto(Long sortId, Long nsortId, Long subsortId);

    /** 根据prodid和属性值名称查询 */
    ProductPropertyValue getProductPropertyValue(Long propId, String propertiesValue);

	public abstract void clearUserPropcache(Long prodId);
     
    public abstract List<ProductProperty> getProductProperty(String shopName);

	public abstract ProductProperty getProductProperty(Long id);
	
	public ProductProperty getProductProperty(Long prodId,Long propId);
	
    public abstract void deleteProductProperty(ProductProperty productProperty);
	
	public abstract Long saveProductProperty(ProductProperty productProperty, String userId, Long shopId);
	
	public abstract void updateProductProperty(ProductProperty productProperty);
	
	public List<ProductProperty> getProductPropertyList(Long proTypeId);
	
	public List<ProductProperty> getParameterPropertyList(Long proTypeId);

	public abstract PublishProductDto getProductProperty(Long sortId, Long nsortId, Long subsortId);

	/** 直接为自定义属性 获取id **/
	public Long saveCustomProperty(ProductProperty newProperty);
	
	/** 根据 商品ID 查找商品自定义属性 **/
	public List<ProductPropertyDto> queryUserProp(Long prodId);
	
	/** 根据 商品ID 查找商品自定义属性 **/
	public List<ProductProperty> queryUserPropByProdId(Long prodId);
	
	public List<ProductProperty> getProductProperty(List<Long> propIds, Long categoryId);
	
	public List<ProductProperty> getProductProperty(List<Long> ids);

	/** 根据商品Id删除用户自定义属性  **/
	public void deleteUserPropByProdId(Long prodId);
	
	/** 根据 分类Id 查找对应的参数和属性**/
	public PublishProductDto queryProductProperty(Long categoryId);

	List<ProductPropertyDto> findProductProper(Long categoryId);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId);

	public PageSupport<ProductProperty> getProductPropertyParamPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty);

	public PageSupport<ProductProperty> getProductPropertyByPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty);

	/** 根据规格id查找对应的商品id */
	public Integer findProdId(Long propId);
    /** 根据prodid和属性名查询 */
    ProductProperty getProductProperty(Long prodId, String properties);

 }
