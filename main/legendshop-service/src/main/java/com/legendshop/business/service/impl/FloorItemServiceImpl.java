/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AdvertisementDao;
import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Advertisement;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.model.floor.NewsFloor;
import com.legendshop.model.floor.ProductFloor;
import com.legendshop.model.floor.SortFloor;
import com.legendshop.spi.service.FloorItemService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 子楼层服务.
 */
@Service("floorItemService")
public class FloorItemServiceImpl  implements FloorItemService{
	
	@Autowired
    private FloorItemDao floorItemDao;
    
	@Autowired
    private FloorSubItemDao floorSubItemDao;
    
	@Autowired
    private FloorDao floorDao;
    
	@Autowired
	private AdvertisementDao advertisementDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
    public FloorItem getFloorItem(Long id) {
        return floorItemDao.getFloorItem(id);
    }

    @CacheEvict(value = "FloorList", allEntries=true)
    public Long saveFloorItem(FloorItem floorItem) {
        if (!AppUtils.isBlank(floorItem.getFiId())) {
            updateFloorItem(floorItem);
            return floorItem.getFiId();
        }
        return (Long) floorItemDao.save(floorItem);
    }

    public void updateFloorItem(FloorItem floorItem) {
        floorItemDao.updateFloorItem(floorItem);
    }

   /**
    * 装载商品分类楼层
    */
	@Override
	public List<FloorItem> getAllSortFloorItem(Long flId) {
		return floorItemDao.getAllSortFloorItem(flId);
	}

	/**
	 * 保存楼层项目
	 */
	@Override
	@CacheEvict(value = "FloorList", allEntries=true)
	public void saveFloorItem(List<SortFloor> sortFloorList, Long floorId,String layout) {
		//删除原来依赖子项目
		floorSubItemDao.deleteFloorSubItemById(floorId);
		floorItemDao.deleteFloorItem(FloorTemplateEnum.SORT.value(), floorId);
		//处理楼层Sort项目
		List<FloorItem> floorItemList = parseSortFloorItem(sortFloorList, floorId);
		 Map<Long,Long> idMap = new HashMap<Long,Long>(floorItemList.size());
		for (FloorItem floorItem : floorItemList) {
			Long oldFiId = floorItem.getFiId();
			floorItem.setFiId(null);
			floorItem.setLayout(layout);
			Long newFiId = (Long)floorItemDao.save(floorItem);
			Long referId = floorItem.getReferId();
			if(oldFiId != null){ //数据库已经存在,有旧的ID,表示FloorId
				referId = oldFiId;
			}
			idMap.put(referId, newFiId);
		}
		//保存楼层二级Sort项目
		List<FloorSubItem> subFloorItemList = parseSubSortFloorItem(idMap,sortFloorList, floorId);
		for (FloorSubItem floorSubItem : subFloorItemList) {
			floorSubItemDao.save(floorSubItem);
		}
		
		//清除缓存
		Floor floor=floorDao.getById(floorId);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		
	}

	@Override
	public List<FloorItem> getBrandList(Long id,String layout,int limit) {
		return floorItemDao.getBrandList(id,layout,limit);
	}
	
	@Override
	@CacheEvict(value = "FloorList", allEntries=true)
	public void saveBrands(List<ProductFloor> brandFloorList, Long flId,String layout) {
		//删除原来的品牌
		floorItemDao.deleteFloorItem(FloorTemplateEnum.BRAND.value(), flId);
		//添加品牌
		for(ProductFloor b:brandFloorList){
			FloorItem floorItem = new FloorItem();
			floorItem.setFloorId(flId);
			floorItem.setRecDate(new Date());
			floorItem.setReferId(b.getId());
			floorItem.setType(FloorTemplateEnum.BRAND.value());
			floorItem.setLayout(layout);
			floorItemDao.saveFloorItem(floorItem);
		}
		
		//清除缓存
		Floor floor=floorDao.getById(flId);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		
		
	}
	
	@Override
	public List<FloorItem> getRankList(Long id) {
		return floorItemDao.getRankList(id);
	}
	
	/**
	 * 保存商品排行
	 */
	@Override
	public void saveRanks(List<ProductFloor> rankFloorList, Long flId) {
		//删除原来的商品排行
		floorItemDao.deleteFloorItem(FloorTemplateEnum.PRODUCT.value(), flId);
		//添加商品排行
		for(ProductFloor floor : rankFloorList){
			FloorItem floorItem = new FloorItem();
			floorItem.setFloorId(flId);
			floorItem.setRecDate(new Date());
			floorItem.setReferId(floor.getId());
			floorItem.setType(FloorTemplateEnum.PRODUCT.value());
			floorItemDao.saveFloorItem(floorItem);
		}
		
		//清除缓存
		Floor floor=floorDao.getById(flId);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		
		
	}
	
	/**
	 * 保存文章楼层信息
	 */
	@Override
	public void saveNews(List<NewsFloor> newsFloorList, Long flId) {
		//删除原来的文章
		floorItemDao.deleteFloorItem(FloorTemplateEnum.NEWS.value(), flId);
		if(newsFloorList !=  null){
			for (NewsFloor newsFloor : newsFloorList) {
				FloorItem floorItem = new FloorItem();
				floorItem.setFloorId(flId);
				floorItem.setRecDate(new Date());
				floorItem.setReferId(newsFloor.getId());
				floorItem.setType(FloorTemplateEnum.NEWS.value());
				floorItemDao.saveFloorItem(floorItem);
			}
		}
		//清除缓存
		Floor floor=floorDao.getById(flId);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
	}
	
	/**
	 * 保存团购
	 */
	@Override
	public void saveGroups(List<ProductFloor> groupFloorList, Long flId) {
		//删除原来的团购商品
		floorItemDao.deleteFloorItem(FloorTemplateEnum.GROUP_PRODUCT.value(), flId);
		//添加团购商品
		for(ProductFloor g : groupFloorList){
			FloorItem floorItem = new FloorItem();
			floorItem.setFloorId(flId);
			floorItem.setRecDate(new Date());
			floorItem.setReferId(g.getId());
			floorItem.setType(FloorTemplateEnum.GROUP_PRODUCT.value());
			floorItemDao.saveFloorItem(floorItem);
		}
		
		//清除缓存
		Floor floor=floorDao.getById(flId);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		
	}

	@Override
	public FloorItem getFloorGroup(Long id) {
		return floorItemDao.getFloorGroup(id);
	}

	@Override
	public List<FloorItem> getNewsList(Long flId) {
		return floorItemDao.getNewsList(flId);
	}

	private List<FloorItem> parseSortFloorItem(List<SortFloor> sortFloorList, Long flId){
		if(sortFloorList == null){
			return null;
		}
		 List<FloorItem> floorItemList = new ArrayList<FloorItem>();
		int seq = 0;
		for (SortFloor sortFloor : sortFloorList) {
			if(sortFloor.getPid() == null && sortFloor.getId() != null){//顶级元素
				FloorItem floorItem = new FloorItem();
				floorItem.setRecDate(new Date());
				floorItem.setFloorId(flId);
				if(sortFloor.getReferId() != null){//如果referId不为空，则表示是原来的数据
					floorItem.setReferId(sortFloor.getReferId());
				}else{
					floorItem.setReferId(sortFloor.getId());
				}
				floorItem.setFiId(sortFloor.getId());
				floorItem.setType(FloorTemplateEnum.SORT.value());
				floorItem.setSortLevel(sortFloor.getLevel());
				floorItem.setSeq(seq ++);
				floorItemList.add(floorItem);
			}
		}
		return floorItemList;
	}
	
	private List<FloorSubItem> parseSubSortFloorItem(Map<Long,Long> idMap, List<SortFloor> sortFloorList, Long floorId) {
		if(sortFloorList == null){
			return null;
		}
		 List<FloorSubItem> subFloorItemList = new ArrayList<FloorSubItem>();
		int seq = 0;
		for (SortFloor sortFloor : sortFloorList) {
			if(sortFloor.getPid() != null && sortFloor.getId() != null){//二级元素
				FloorSubItem floorItem = new FloorSubItem();
				floorItem.setFiId(idMap.get(sortFloor.getPid()));
				if(sortFloor.getReferId() != null){
					floorItem.setSubSortId(sortFloor.getReferId());
				}else{
					floorItem.setSubSortId(sortFloor.getId());
				}
				floorItem.setSeq(seq ++);
				subFloorItemList.add(floorItem);
			}
		}
		return subFloorItemList;
	}

	@Override
	public FloorItem getAdvFloorItem(Long flId,Long fiId,String type) {
		return floorItemDao.getAdvFloorItem(flId,fiId,type);
	}

	@Override
	public List<FloorItem> queryAdvFloorItem(Long flId, String type,String layout) {
		return floorItemDao.queryAdvFloorItem(flId, type,layout);
	}

	@Override
	public void deleteFloorItem(Long id) {
		floorItemDao.deleteById(id);
		//清除缓存
		Floor floor=floorDao.getById(id);
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
	}

	@Override
	public FloorItem getAdvFloorItem(Long flId, String type,String layout) {
		return floorItemDao.getAdvFloorItem(flId,type,layout);
	}
	
	@Override
	public String deleteAdvtItem(Long fiId, Long advId) {
		Advertisement orin=advertisementDao.getAdvertisementById(advId);
		if(orin==null){
			return "广告信息不存在";
		}
		if(AppUtils.isNotBlank(orin.getPicUrl())){
			//delete  attachment
			attachmentManager.delAttachmentByFilePath(orin.getPicUrl());
			
			//delete file image
			attachmentManager.deleteTruely(orin.getPicUrl());
		}
		floorItemDao.deleteById(fiId);
		
		advertisementDao.delete(orin);
		
		//清除缓存
		Floor floor=floorDao.getById(orin.getFloorId());
		if(floor!=null && floor.getStatus().intValue()==1){
		  floorDao.updateFloorCache(floor);
		}
		
		return "ok";
	}

	@Override
	public List<SubFloorItem> findSubFloorItem(Long flId, String layout,int limit) {
		return floorSubItemDao.findSubFloorItem(flId,layout,limit);
	}

	@Override
	public PageSupport<Brand> getBrandListPage(String curPageNO, String brandName) {
		return floorItemDao.getBrandListPage(curPageNO,brandName);
	}

	@Override
	public PageSupport<Product> getGroupListPage(String curPageNO, String groupName) {
		return floorItemDao.getGroupListPage(curPageNO,groupName);
	}

	@Override
	public PageSupport<FloorItem> queryAdvFloorItemPage(String curPageNO, Long flId, String layout, String title) {
		return floorItemDao.queryAdvFloorItemPage(curPageNO,flId,layout,title);
	}

	@Override
	public PageSupport<News> getNewsListPage(String curPageNO, Long newsCategoryId, String newsTitle,String userName) {
		return floorItemDao.getNewsListPage(curPageNO,newsCategoryId,newsTitle,userName);
	}
}
