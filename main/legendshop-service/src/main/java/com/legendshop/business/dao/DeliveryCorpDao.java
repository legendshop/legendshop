/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryCorp;

/**
 * 物流方式Dao.
 */

public interface DeliveryCorpDao extends GenericDao<DeliveryCorp, Long>{

	/**
	 * Gets the delivery corp.
	 *
	 * @param id the id
	 * @return the delivery corp
	 */
	public abstract DeliveryCorp getDeliveryCorp(Long id);

	/**
	 * Delete delivery corp.
	 *
	 * @param deliveryCorp the delivery corp
	 */
	public abstract void deleteDeliveryCorp(DeliveryCorp deliveryCorp);

	/**
	 * Save delivery corp.
	 *
	 * @param deliveryCorp the delivery corp
	 * @return the long
	 */
	public abstract Long saveDeliveryCorp(DeliveryCorp deliveryCorp);

	/**
	 * Update delivery corp.
	 *
	 * @param deliveryCorp the delivery corp
	 */
	public abstract void updateDeliveryCorp(DeliveryCorp deliveryCorp);

	public abstract DeliveryCorp getDeliveryCorpByDvyId(Long dvyId);

	public abstract void deleteDeliveryCorpByShopId(Long shopId);

	public abstract List<DeliveryCorp> getDeliveryCorpByShop();

	public abstract PageSupport<DeliveryCorp> getDeliveryCorpPage(String curPageNO, DeliveryCorp deliveryCorp);
	
	public abstract List<DeliveryCorp> getAllDeliveryCorp();

}
