/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.spi.service.FloorSubItemService;
import com.legendshop.util.AppUtils;

/**
 * 首页主楼层子内容服务.
 */
@Service("floorSubItemService")
public class FloorSubItemServiceImpl  implements FloorSubItemService{
	
	@Autowired
    private FloorSubItemDao floorSubItemDao;

    public FloorSubItem getFloorSubItem(Long id) {
        return floorSubItemDao.getFloorSubItem(id);
    }

    public void deleteFloorSubItem(FloorSubItem floorSubItem) {
        floorSubItemDao.deleteFloorSubItem(floorSubItem);
    }

    public Long saveFloorSubItem(FloorSubItem floorSubItem) {
        if (!AppUtils.isBlank(floorSubItem.getFsiId())) {
            updateFloorSubItem(floorSubItem);
            return floorSubItem.getFsiId();
        }
        return (Long) floorSubItemDao.save(floorSubItem);
    }

    public void updateFloorSubItem(FloorSubItem floorSubItem) {
        floorSubItemDao.updateFloorSubItem(floorSubItem);
    }

	@Override
	public List<FloorSubItem> getAllFloorSubItem(Long id) {
		return floorSubItemDao.getAllFloorSubItem(id);
	}

	@Override
	public void deleteFloorSubItem(List<FloorSubItem> floorSubItemList) {
		floorSubItemDao.delete(floorSubItemList);		
	}

	@Override
	public PageSupport<FloorSubItem> getFloorSubItemPage(String curPageNO) {
		return floorSubItemDao.getFloorSubItemPage(curPageNO);
	}
}
