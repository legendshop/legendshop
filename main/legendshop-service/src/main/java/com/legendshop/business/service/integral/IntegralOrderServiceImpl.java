/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.integral;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.integral.IntegralOrderDao;
import com.legendshop.business.dao.integral.IntegralOrderItemDao;
import com.legendshop.business.dao.integral.IntegralProdDao;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IntegralLogTypeEnum;
import com.legendshop.model.constant.IntegralOrderStatusEnum;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.DeliveryDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.model.entity.integral.IntegralOrderItem;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.spi.service.DeliveryCorpService;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.spi.service.UserAddressSubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 积分订单服务类
 */
@Service("integralOrderService")
public class IntegralOrderServiceImpl implements IntegralOrderService {

	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private IntegralOrderDao integralOrderDao;

	@Autowired
	private IntegralOrderItemDao integralOrderItemDao;

	@Autowired
	private IntegraLogDao integraLogDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private UserAddressSubService userAddressSubService;

	@Autowired
	private IntegralProdDao integralProdDao;

	private static String ADMIN_COUNT_ORDER_SQL = "SELECT COUNT(*) FROM  ls_integral_order o INNER JOIN ls_usr_addr_sub addr ON addr.addr_order_id=o.addr_order_id INNER JOIN ls_integral_order_item item ON item.order_id=o.id";

	private static String USER_BUY_SHOP_COUNT = "SELECT IFNULL(SUM(basket_count),0) FROM ls_integral_order_item WHERE user_id=? AND prod_id=?";

	private static String ORDER_SQL = "SELECT o.id AS orderId,o.order_sn AS orderSn,o.user_id AS userId,o.user_name AS userName ,o.product_nums AS productNums,o.integral_total AS integralTotal,o.dvy_type_id AS dvyTypeId,o.dvy_flow_id AS dvyFlowId,o.order_status AS orderStatus,o.addr_order_id AS addrOrderId,o.add_time AS addTime,"
			+ "o.dvy_time AS dvyTime,o.finally_time AS finallyTime,o.order_desc AS orderDesc"
			+ ",item.id AS orderItemId,item.prod_id AS prodId,item.prod_name AS prodName,item.prod_pic AS prodPic,item.basket_count AS basketCount,"
			+ " item.exchange_integral AS exchangeIntegral,item.price AS price,addr.receiver  AS receiver,addr.detail_address AS subAdds,addr.sub_post AS subPost,addr.province_id AS province,addr.city_id AS city,"
			+ " addr.area_id AS AREA,addr.mobile AS mobile, addr.telphone AS telphone,addr.email AS email ,addr.sub_post AS subPost from  FROM_SUB_SQL "
			+ " INNER JOIN ls_usr_addr_sub addr ON addr.addr_order_id=o.addr_order_id INNER JOIN ls_integral_order_item item ON item.order_id=o.id order by o.add_time desc ";

	/**
	 * 获取积分订单
	 */
	public IntegralOrder getIntegralOrder(Long id) {
		return integralOrderDao.getIntegralOrder(id);
	}

	/**
	 * 删除积分订单
	 */
	public void deleteIntegralOrder(IntegralOrder integralOrder) {
		integralOrderDao.deleteIntegralOrder(integralOrder);
	}

	/**
	 * 保存积分订单
	 */
	public Long saveIntegralOrder(IntegralOrder integralOrder) {
		if (!AppUtils.isBlank(integralOrder.getId())) {
			updateIntegralOrder(integralOrder);
			return integralOrder.getId();
		}
		return integralOrderDao.saveIntegralOrder(integralOrder);
	}

	/**
	 * 更新积分订单
	 */
	public void updateIntegralOrder(IntegralOrder integralOrder) {
		integralOrderDao.updateIntegralOrder(integralOrder);
	}


	/**
	 * 
	 */
	@Override
	public Long getUserBuyShopCount(String userId, Long prodId) {
		return integralOrderItemDao.getLongResult(USER_BUY_SHOP_COUNT, userId, prodId);
	}

	/**
	 * 获取积分订单Dto
	 */
	@Override
	public PageSupport<IntegralOrderDto> getIntegralOrderDtos(OrderSearchParamDto paramDto) {

		int curPageNO = Integer.valueOf(paramDto.getCurPageNO());
		int pageSize = paramDto.getPageSize();
		int offset = (curPageNO - 1) * paramDto.getPageSize();

		StringBuilder orderSQl = new StringBuilder();
		// 不显示已删除的订单
		List<Object> args = new ArrayList<Object>();
		if (AppUtils.isNotBlank(paramDto.getUserName())) {
			args.add(paramDto.getUserName());
			orderSQl.append(" and o.user_name =?  ");
		}
		if (AppUtils.isNotBlank(paramDto.getUserId())) {
			args.add(paramDto.getUserId());
			orderSQl.append(" and o.user_id =?  ");
		}

		if (AppUtils.isNotBlank(paramDto.getSubNumber())) {
			args.add("%" + paramDto.getSubNumber().trim() + "%");
			orderSQl.append(" and o.order_sn like?  ");
		}
		if (AppUtils.isNotBlank(paramDto.getStatus())) {
			args.add(paramDto.getStatus());
			orderSQl.append(" and o.order_status =?  ");
		}

		if (AppUtils.isNotBlank(paramDto.getStartDate())) {
			args.add(paramDto.getStartDate());
			orderSQl.append(" and o.add_time >= ?  ");
		}
		if (AppUtils.isNotBlank(paramDto.getEndDate())) {
			args.add(paramDto.getEndDate());
			orderSQl.append(" and o.add_time < ?  ");
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append(ADMIN_COUNT_ORDER_SQL);
		countSql.append(orderSQl.toString());

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT o.* FROM ls_integral_order o WHERE 1=1 ");
		sb.append(orderSQl.toString());
		sb.append("ORDER BY o.add_time DESC,o.order_status ASC");

		StringBuilder limitSql = new StringBuilder("(");
		integralOrderDao.getDialect().getLimitString(limitSql, sb.toString(), offset, pageSize); // 分页函数，跟数据库相关
		limitSql.append(") o ");

		String subSQl = ORDER_SQL.replace("FROM_SUB_SQL", limitSql.toString());

		Long total = integralOrderDao.getLongResult(countSql.toString(), args.toArray());
		List<IntegralOrderDto> integralOrderDtos = integralOrderDao.getIntegralOrderDtos(subSQl, args);
		PageSupport<IntegralOrderDto> pageSupport = initPageProvider(total, curPageNO, pageSize, integralOrderDtos);
		return pageSupport;
	}

	/**
	 * 初始化页面
	 */
	private PageSupport<IntegralOrderDto> initPageProvider(long total, Integer curPageNO, Integer pageSize, List<IntegralOrderDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<IntegralOrderDto> ps = new PageSupport<IntegralOrderDto>(resultList, pageProvider);
		return ps;
	}

	/**
	 * 取消订单
	 */
	@Override
	public String orderCancel(String orderSn, String userId, String updateUserId, String logDesc) {
		// 取消订单操作
		IntegralOrder integralOrder = integralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if (AppUtils.isBlank(integralOrder)) {
			return "更新失败,没有找到该订单信息";
		}
		if (!IntegralOrderStatusEnum.SUBMIT.value().equals(integralOrder.getOrderStatus())) {
			return "更新失败,该订单状态不是提交状态";
		}
		if (AppUtils.isNotBlank(userId) && userId.equals(integralOrder.getUserId())) {
			return "您没有权限操作该订单！";
		}

		int result = integralOrderDao.orderCancel(orderSn);
		if (result > 0) {
			//积分商品库存回滚
			List<IntegralOrderItem> list = integralOrderItemDao.queryByOrderId(integralOrder.getId());
			if(AppUtils.isNotBlank(list)){
				for (IntegralOrderItem integralOrderItem : list) {
					integralProdDao.releaseStock(integralOrderItem.getProdId(), integralOrder.getProductNums());
				}
			}
			
			// 回滚订单积分
			userDetailDao.updateScore(integralOrder.getIntegralTotal(), integralOrder.getUserId());

			// 记录积分明细
			IntegraLog integraLog = new IntegraLog();
			integraLog.setIntegralNum(integralOrder.getIntegralTotal());
			integraLog.setLogType(IntegralLogTypeEnum.LOG_DEFAULT.value());
			integraLog.setAddTime(new Date());
			integraLog.setLogDesc(logDesc);
			integraLog.setUpdateUserId(updateUserId);
			integraLog.setUserId(integralOrder.getUserId());
			integraLogDao.saveIntegraLog(integraLog);
		}
		return Constants.SUCCESS;
	}

	/**
	 * 根据订单流水号获取积分订单
	 */
	@Override
	public IntegralOrder getIntegralOrderByOrderSn(String orderSn) {
		return integralOrderDao.getIntegralOrderByOrderSn(orderSn);
	}

	/**
	 * 发货
	 */
	@Override
	public void fahuo(String orderSn, Long deliv, String dvyFlowId) {
		integralOrderDao.fahuo(orderSn, deliv, dvyFlowId);
	}

	/**
	 * 查找及格分订单明细
	 */
	@Override
	public IntegralOrderDto findIntegralOrderDetail(String ordeSn) {
		IntegralOrderDto integralOrderDto = integralOrderDao.findIntegralOrderDetail(ordeSn);
		if (integralOrderDto != null) {
			DeliveryCorp corp = deliveryCorpService.getDeliveryCorpByDvyId(integralOrderDto.getDvyTypeId());
			if (AppUtils.isNotBlank(corp)) {
				integralOrderDto.setDvyTime(integralOrderDto.getDvyTime());
				DeliveryDto delivery = new DeliveryDto();
				delivery.setDvyTypeId(integralOrderDto.getDvyTypeId());
				delivery.setDvyFlowId(integralOrderDto.getDvyFlowId());
				delivery.setDelName(corp.getName());
				delivery.setDelUrl(corp.getCompanyHomeUrl());
				delivery.setQueryUrl(corp.getQueryUrl());
				integralOrderDto.setDelivery(delivery);
			}
		}
		return integralOrderDto;
	}

	/**
	 * 收货
	 */
	@Override
	public String shouhuo(String orderSn) {
		if (AppUtils.isBlank(orderSn)) {
			return Constants.FAIL;
		}
		IntegralOrder order = integralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if (AppUtils.isBlank(order)) {
			return Constants.FAIL;
		}
		order.setOrderStatus(IntegralOrderStatusEnum.SUCCESS.value());
		order.setFinallyTime(new Date());
		integralOrderDao.update(order);
		return Constants.SUCCESS;
	}

	/**
	 * 积分下单
	 */
	@Override
	public String shopBuyIntegral(String userId, UserAddress userAddress, IntegralProd integralProd, Integer count, String orderDesc) {
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		Double score = Arith.mul(integralProd.getExchangeIntegral(), count);

		/** 更新用户的积分信息 **/
		int result = userDetailDao.updateScore(score.intValue(), userDetail.getScore(), userId);
		if (result == 0) {
			return "对不起, 您的积分不足!";
		}

		String orderSn = CommonServiceUtil.getSubNember(userDetail.getUserName());

		/** 保存订单用户地址信息 **/
		UserAddressSub userAddressSub = saveUserAddressSub(userAddress);

		/** 保存订单 **/
		IntegralOrder order = new IntegralOrder();
		order.setOrderSn(orderSn);
		order.setUserId(userId);
		order.setUserName(userDetail.getUserName());
		order.setProductNums(count);
		order.setIntegralTotal(score.intValue());
		order.setOrderStatus(IntegralOrderStatusEnum.SUBMIT.value());
		order.setAddrOrderId(userAddressSub.getAddrOrderId());
		order.setAddTime(new Date());
		order.setOrderDesc(orderDesc);
		Long orderId = integralOrderDao.save(order);

		/** 保存订单项 **/
		IntegralOrderItem item = new IntegralOrderItem();
		item.setOrderId(orderId);
		item.setUserId(userId);
		item.setProdId(integralProd.getId());
		item.setProdName(integralProd.getName());
		item.setProdPic(integralProd.getProdImage());
		item.setBasketCount(count);
		item.setExchangeIntegral(integralProd.getExchangeIntegral());
		item.setPrice(integralProd.getPrice());
		integralOrderItemDao.save(item);

		/** 保存积分消费记录 **/
		IntegraLog integraLog = new IntegraLog();
		integraLog.setUserId(userId);
		integraLog.setIntegralNum(-score.intValue());
		integraLog.setLogType(IntegralLogTypeEnum.LOG_ORDER.value());
		integraLog.setLogDesc("订单消费,积分订单流水号:" + orderSn);
		integraLog.setAddTime(new Date());
		integraLogDao.save(integraLog);

		/** 更新商品的库存 **/
		result = integralProdDao.updateStock(count, integralProd.getId());
		if (result <= 0) {
			throw new BusinessException("兑换订单失败,该商品库存不足或者已经被下线！");
		}
		return Constants.SUCCESS;
	}

	/**
	 * 保存订单用户地址信息
	 * 
	 * @param userAddress
	 * @return
	 */
	public UserAddressSub saveUserAddressSub(UserAddress userAddress) {
		UserAddressSub userAddressSub = new UserAddressSub();
		userAddressSub.setUserId(userAddress.getUserId());
		userAddressSub.setReceiver(userAddress.getReceiver());
		userAddressSub.setSubPost(userAddress.getSubPost());
		userAddressSub.setProvinceId(userAddress.getProvinceId());
		userAddressSub.setCityId(userAddress.getCityId());
		userAddressSub.setAreaId(userAddress.getAreaId());
		userAddressSub.setSubAdds(userAddress.getSubAdds());
		userAddressSub.setAliasAddr(userAddress.getAliasAddr());
		StringBuffer buffer = new StringBuffer();
		buffer.append(userAddress.getProvince()).append(" ").append(userAddress.getCity()).append(" ").append(userAddress.getArea()).append(" ")
				.append(userAddress.getSubAdds());
		userAddressSub.setDetailAddress(buffer.toString());
		userAddressSub.setEmail(userAddress.getEmail());
		userAddressSub.setMobile(userAddress.getMobile());
		userAddressSub.setTelphone(userAddress.getTelphone());
		Long addrOrderId = userAddressSubService.saveUserAddressSub(userAddressSub);
		userAddressSub.setAddrOrderId(addrOrderId);
		return userAddressSub;
	}

	/**
	 * 订单删除
	 */
	@Override
	public String orderDel(String orderSn) {
		IntegralOrder integralOrder = integralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if (integralOrder != null) {
			if (IntegralOrderStatusEnum.CANCEL.value().equals(integralOrder.getOrderStatus())
					|| IntegralOrderStatusEnum.SUCCESS.value().equals(integralOrder.getOrderStatus())) {
				return integralOrderDao.orderDel(integralOrder);
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 订单删除
	 */
	@Override
	public String orderDel(String sn, String userId) {
		IntegralOrder integralOrder = integralOrderDao.getIntegralOrderByOrderSn(sn);
		if (!userId.equals(integralOrder.getUserId())) {
			return Constants.FAIL;
		}
		if (integralOrder != null) {
			if (IntegralOrderStatusEnum.CANCEL.value().equals(integralOrder.getOrderStatus())
					|| IntegralOrderStatusEnum.SUCCESS.value().equals(integralOrder.getOrderStatus())) {
				return integralOrderDao.orderDel(integralOrder);
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 获取热销积分商品
	 */
	@Override
	public List<IntegralProd> getHotIntegralProds() {
		return integralProdDao.getHotIntegralProds();
	}

	/**
	 * 获取优惠卷列表页面
	 */
	@Override
	public PageSupport<Coupon> getCouponListPage(String curPageNO, String type) {
		return integralOrderDao.getCouponListPage(curPageNO, type);
	}

	@Override
	public IntegralOrder getIntegralOrder(String orderSn, String userId) {
		
		return integralOrderDao.getIntegralOrder(orderSn, userId);
	}
}
