/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TagDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Tag;
import com.legendshop.util.AppUtils;

/**
 * 标签Dao.
 */
@Repository
public class TagDaoImpl extends GenericDaoImpl<Tag, Long> implements TagDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Cacheable(value = "TagList")
	public List<Tag> getPageTag(Long shopId, String page) {
		List<Tag> tagList =  this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
		// 用户的产品分类
		if (AppUtils.isNotBlank(tagList)) {
			for (int i = 0; i < tagList.size(); i++) {
				Tag tag = tagList.get(i);
			}
		}
		return tagList;
	}

	public Tag getTag(Long id) {
		return getById(id);
	}

	@CacheEvict(value = "Tag", key = "#tag.tagId")
	public void deleteTag(Tag tag) {
		delete(tag);
	}

	public Long saveTag(Tag tag) {
		return (Long) save(tag);
	}


	@CacheEvict(value = "Tag", key = "#tag.tagId")
	public void updateTag(Tag tag) {
		update(tag);
	}

	@Override
	public Tag getTag(String tagName, String userName) {
		List<Tag> result = this.queryByProperties(new EntityCriterion().eq("name", tagName).eq("userName", userName));
		if (AppUtils.isNotBlank(result)) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public PageSupport<Tag> getTagPage(String curPageNO, Tag tag) {
		CriteriaQuery cq = new CriteriaQuery(Tag.class, curPageNO);
	 	cq.eq("name", tag.getName());
        cq.setPageSize(systemParameterUtil.getPageSize());
        cq.addOrder("asc", "seq");
		return queryPage(cq);
	}


}
