/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CouponShopDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.CouponShop;
/**
 * 发货地址服务.
 */
@Repository
public class CouponShopDaoImpl extends GenericDaoImpl<CouponShop, Long> implements CouponShopDao  {

	@Override
	public List<Long> saveCouponShops(List<CouponShop> couponShops) {
		return this.save(couponShops);
	}

	@Override
	public List<CouponShop> getCouponShopByCouponIds(List<Long> couponIds) {
		StringBuffer sql = new StringBuffer("select * from ls_coupon_shop where coupon_id in (");
		for (int i = 0; i < couponIds.size(); i++) {
			sql.append("?,");
		}
		sql.setLength(sql.length()-1);
		sql.append(")");
		return this.query(sql.toString(), CouponShop.class, couponIds.toArray());
	}
	
 }
