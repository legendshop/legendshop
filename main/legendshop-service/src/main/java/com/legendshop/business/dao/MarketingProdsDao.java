/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.entity.MarketingProds;

/**
 * 参加营销活动的商品Dao.
 */
public interface MarketingProdsDao extends GenericDao<MarketingProds, Long> {
     
    public abstract List<MarketingProds> getMarketingProdsByMarketId(Long marketId);

	public abstract MarketingProds getMarketingProds(Long id);
	
    public abstract int deleteMarketingProds(MarketingProds marketingProds);
	
	public abstract Long saveMarketingProds(MarketingProds marketingProds);
	
	public abstract int updateMarketingProds(MarketingProds marketingProds);

	public abstract PageSupport<MarketingProdDto> findMarketingRuleProds(String curPage, Long shopId, Long marketingId);

	public abstract PageSupport<MarketingProdDto> findMarketingProds(String curPage, Long shopId, String name);

	/**
	 * 获取商铺某个商品参与的所有在线进行活动信息
	 */
	List<MarketingDto> findMarketings(Long prodId, Long skuId, Long shopId);
	
	public abstract List<MarketingRuleDto> findMarketinRule(Long shopId, List<MarketingDto> resultRuleList);

	public abstract List<MarketingProdDto> findMarketinProd(Long shopId, List<MarketingDto> resultRuleList);
	
	public List<MarketingDto> findProdMarketing(Long prodId,Long shopId, Date time);
	
	public List<MarketingDto> findGlobalMarketing(Long shopId, Date time);
	
	/**
	 * 按店铺查找所有的购物车商品的营销活动
	 */
	public abstract List<MarketingDto> findProdMarketing(List<ShopCartItem> cartItems, Long shopId, Date time);

	public abstract List<MarketingProds> getmarketingProdByProdId(Long prodId);

	/**
	 * 查找当前商品sku是否参加了其他促销活动
	 */
	public abstract MarketingProdDto iSHaveMarketing(Long prodId, Long skuId);

}
