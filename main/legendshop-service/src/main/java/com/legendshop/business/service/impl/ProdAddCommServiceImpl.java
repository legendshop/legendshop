/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdAddCommDao;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.spi.service.ProdAddCommService;
import com.legendshop.util.AppUtils;

/**
 * 商品二次评论服务
 */
@Service("prodAddCommService")
public class ProdAddCommServiceImpl  implements ProdAddCommService{
	
	@Autowired
    private ProdAddCommDao prodAddCommDao;

    public ProdAddComm getProdAddComm(Long id) {
        return prodAddCommDao.getProdAddComm(id);
    }

    public void deleteProdAddComm(ProdAddComm prodAddComm) {
        prodAddCommDao.deleteProdAddComm(prodAddComm);
    }

    public Long saveProdAddComm(ProdAddComm prodAddComm) {
        if (!AppUtils.isBlank(prodAddComm.getId())) {
            updateProdAddComm(prodAddComm);
            return prodAddComm.getId();
        }
        return prodAddCommDao.saveProdAddComm(prodAddComm);
    }

    public void updateProdAddComm(ProdAddComm prodAddComm) {
        prodAddCommDao.updateProdAddComm(prodAddComm);
    }

	@Override
	public ProdAddComm getProdAddCommByCommId(Long commId) {
		
		return prodAddCommDao.getProdAddCommByCommId(commId);
	}
}
