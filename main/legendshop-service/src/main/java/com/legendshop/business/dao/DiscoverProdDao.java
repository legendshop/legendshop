/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverProd;

/**
 * The Class DiscoverProdDao. 发现文章商品关联表Dao接口
 */
public interface DiscoverProdDao extends Dao<DiscoverProd,Long>{

	 /**
	 *  通过发现文章id删除
	 */	    
	public int deleteByDisId(Long disId);
	
	/**
	 * 根据Id获取发现文章商品关联表
	 */
	public abstract DiscoverProd getDiscoverProd(Long id);
	/**
	 * 根据disId获取发现文章商品关联表
	 */
	public abstract List<DiscoverProd> getDiscoverProdByDisId(Long id);
	

	public abstract List<DiscoverProd> getByDisId(Long id);

	/**
	 *  根据Id删除发现文章商品关联表
	 */
    public abstract int deleteDiscoverProd(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteDiscoverProd(DiscoverProd discoverProd);

	/**
	 * 保存发现文章商品关联表
	 */
	public abstract Long saveDiscoverProd(DiscoverProd discoverProd);

	/**
	 *  更新发现文章商品关联表
	 */		
	public abstract int updateDiscoverProd(DiscoverProd discoverProd);

	/**
	 * 分页查询发现文章商品关联表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<DiscoverProd> queryDiscoverProd(String curPageNO, Integer pageSize);

}
