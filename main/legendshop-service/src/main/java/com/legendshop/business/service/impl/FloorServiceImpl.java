/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;
import com.legendshop.spi.service.FloorService;
import com.legendshop.util.AppUtils;

/**
 * 首页楼层服务.
 */
@Service("floorService")
public class FloorServiceImpl  implements FloorService{
	
	@Autowired
    private FloorDao floorDao;

    public Floor getFloor(Long id) {
        return floorDao.getFloor(id);
    }

    public Long saveFloor(Floor floor) {
        if (!AppUtils.isBlank(floor.getFlId())) {
        	Floor orgin=floorDao.getFloor(floor.getFlId());
        	if(orgin!=null){
        		orgin.setName(floor.getName());
        		orgin.setSeq(floor.getSeq());
        		orgin.setCssStyle(floor.getCssStyle());
        		orgin.setLayout(floor.getLayout());
                updateFloor(orgin);
                return orgin.getFlId();
        	}
        }
       floor.setStatus(0);
       Long floorId =  floorDao.save(floor);
       floor.setFlId(floorId);
       floorDao.updateFloorCache(floor);
       return floorId;
    }

    public void updateFloor(Floor floor) {
        floorDao.updateFloor(floor);
        floorDao.updateFloorCache(floor);
    }

	@Override
	public boolean checkPrivilege(Long floorId) {
		return floorDao.checkPrivilege(floorId);
	}

	@Override
	public Integer getLeftSwitch(Long floorId) {
		return floorDao.getLeftSwitch(floorId);
	}

	@Override
	public PageSupport<Floor> getFloorPage(String curPageNO, Floor floor) {
		return floorDao.getFloorPage(curPageNO,floor);
	}

	@Override
	public Integer batchOffline(String floorIdStr) {
		return floorDao.batchOffline(floorIdStr);
	}

}
