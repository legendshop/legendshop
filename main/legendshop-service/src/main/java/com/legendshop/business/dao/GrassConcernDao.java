/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassConcern;

/**
 * The Class GrassConcernDao. 种草社区关注表Dao接口
 */
public interface GrassConcernDao extends Dao<GrassConcern,Long>{

	/**
	 * 根据Id获取种草社区关注表
	 */
	public abstract GrassConcern getGrassConcern(Long id);

	/**
	 *  根据Id删除种草社区关注表
	 */
    public abstract int deleteGrassConcern(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassConcern(GrassConcern grassConcern);

	/**
	 * 保存种草社区关注表
	 */
	public abstract Long saveGrassConcern(GrassConcern grassConcern);

	/**
	 *  更新种草社区关注表
	 */		
	public abstract int updateGrassConcern(GrassConcern grassConcern);

	/**
	 * 分页查询种草社区关注表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassConcern> queryGrassConcern(String curPageNO, Integer pageSize);
	
	public abstract Integer rsConcern(String grassId, String uid);
	
	 /**
  	 *  根据grassUserId删除种草和发现文章关注表
  	 */
     public void deleteGrassConcernByDiscoverId(String grassUserId,String uid);
     
     public Long getConcernCountByGrassUid(String graUserId);
     
     public Long getPursuerCountByUid(String UserId);
     
     public PageSupport<GrassConcern> getConcernByGrassUid(String curPageNO, Integer pageSize,String graUserId);
     
     public PageSupport<GrassConcern> getPursuerByUid(String curPageNO, Integer pageSize,String UserId);

}
