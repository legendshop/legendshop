/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.service.impl;

import com.legendshop.business.dao.CategoryDao;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.recomm.RecommConDto;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * 商品分类的内存树实现类
 *
 */
@Service("categoryManagerUtil")
public final class CategoryManagerUtilImpl implements CategoryManagerUtil {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(CategoryManagerUtilImpl.class);
		
	/** 分类Dao **/
	@Autowired
	private CategoryDao categoryDao;
	
	/** 上线的导航栏显示的 分类树 */
	private TreeNode root;
	
	/** 所有上线的分类 */
	private TreeNode rootOnline;;
	
	/** 所有的分类，包括下线的  */
	private TreeNode rootAll;
	
	/** The temp node list. */
	private  Set<TreeNode> tempNodeList;
	
	/** The Constant lock. */
	private final static Lock lock =new ReentrantLock();   //创建一个锁

	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#getTreeNodeById(com.legendshop.model.dto.TreeNode, long)
	 */
	@Override
	public  TreeNode getTreeNodeById(TreeNode tree, long id) {
		if (tree == null)
			return null;
		TreeNode treeNode = tree.findTreeNodeById(id);
		return treeNode;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#getTreeNodeById(long)
	 */
	@Override
	public TreeNode getTreeNodeById(long id){
		if(id < 0)
			return null;
		TreeNode rootNode = getRoot();
		if(null == rootNode){
			return null;
		}
		return rootNode.findTreeNodeById(id);
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#getTreeNodeByIdForAll(long)
	 */
	@Override
	public TreeNode getTreeNodeByIdForAll(long id){
		if(id < 0)
			return null;
		TreeNode rootAllNode = getRootAll();
		if(null == rootAllNode){
			return null;
		}
		return rootAllNode.findTreeNodeById(id);
	}
	
	/**
	 * 建立TreeNode节点树,会刷新时间戳,导致其他的节点会跟着时间戳变化
	 */
	@Override
	public void generateTree() {
		Set<TreeNode> hashSet = null;
		Set<TreeNode> hashSetAll = null;
		 List<Category> categories=categoryDao.getProductCategory();
		 if(AppUtils.isNotBlank(categories)){
			 hashSet=changeEnititiesToTreeNodes(categories); //转换TreeNode
		 }
		 
		 List<Category> allCategories=categoryDao.getAllProductCategory();
		 if(AppUtils.isNotBlank(allCategories)){
			 hashSetAll = changeEnititiesToTreeNodes(allCategories); //转换TreeNode
		 }
		 //生成内存树
		 generateTree(hashSet,hashSetAll);
	}
	
	
	/**
	 * 重新生成分类树
	 * @param tempNodeList
	 * @param tempNodeListAll
	 */
	private void generateTree(Set<TreeNode> tempNodeList,Set<TreeNode> tempNodeListAll) {
		lock.lock();
		try {
			if(tempNodeList == null){
				//防止在没有数据的情况下就不再更新缓存
				tempNodeList = new HashSet<TreeNode>();
			}
			
			if(tempNodeListAll == null){
				//防止在没有数据的情况下就不再更新缓存
				tempNodeListAll = new HashSet<TreeNode>();
			}

			LOGGER.info("Creating category root cache");
			generateTreeNode(tempNodeList, null);

			LOGGER.info("Creating category all cache");
			generateTreeNode(tempNodeListAll, "all");

		}finally{
			lock.unlock();
		}
		
	}
	
	/**
	 * 刷新TreeNode节点树,不会刷新时间戳
	 */
	@Override
	public void refreshTree() {
		Set<TreeNode> hashSet = null;
		Set<TreeNode> hashSetAll = null;
		 List<Category> categories=categoryDao.getProductCategory();
		 if(AppUtils.isNotBlank(categories)){
			 hashSet=changeEnititiesToTreeNodes(categories); //转换TreeNode
		 }
		 
		 List<Category> allCategories=categoryDao.getAllProductCategory();
		 if(AppUtils.isNotBlank(allCategories)){
			 hashSetAll = changeEnititiesToTreeNodes(allCategories); //转换TreeNode
		 }
		 //生成内存树
		 refreshTree(hashSet,hashSetAll);
	}	
	
	/**
	 * 刷新缓存
	 */
	private void refreshTree(Set<TreeNode> tempNodeList,Set<TreeNode> tempNodeListAll) {
		lock.lock();
		try {
			if(tempNodeList == null){
				//防止在没有数据的情况下就不再更新缓存
				tempNodeList = new HashSet<>();
			}

			if(tempNodeListAll == null){
				//防止在没有数据的情况下就不再更新缓存
				tempNodeListAll = new HashSet<>();
			}

			LOGGER.info("Creating category root cache");
			generateTreeNode(tempNodeList, null);

			LOGGER.info("Creating category all cache");
			generateTreeNode(tempNodeListAll, "all");

		}finally{
			lock.unlock();
		}
		
	}

	 /**
 	 * generate a tree from the given treeNode or entity list
 	 * 生成树或实体由给定的treeNode名单.
 	 *
 	 * @param tempNodeList the temp node list
 	 */
	private void generateTreeNode(Set<TreeNode> tempNodeList,String type) {
		try {
			if("all".equals(type)){
				this.rootAll=null;
			}else{
				this.root=null;
			}
			this.tempNodeList=null;
			this.setTempNodeList(tempNodeList); //初始化tempNodeList
			//组装树
			HashMap<String,TreeNode> nodeMap = getNodesIntoMap(type);
			Iterator<TreeNode> it = nodeMap.values().iterator();
			while (it.hasNext()) {
				TreeNode treeNode = (TreeNode) it.next();
				long parentId = treeNode.getParentId();
				String parentKeyId = String.valueOf(parentId);
				if (nodeMap.containsKey(parentKeyId)) {
					TreeNode parentNode = (TreeNode) nodeMap.get(parentKeyId);
					if (parentNode == null) {
						LOGGER.error("categoryManagerUtil generateTree can not find parent= {} for  treeNode id = {}",parentKeyId,treeNode.getSelfId());
						return;
					} else {
						parentNode.addChildNode(treeNode);
						treeNode.setParentNode(parentNode);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("GenerateTreeNode error",e);
		}
		LOGGER.debug("generateTreeNode done");
	}

	 /**
 	 * set the parent nodes point to the child nodes
 	 * 设置子节点集合添加到父节点上。.
 	 *
 	 * @param nodeMap a hashmap that contains all the treenodes by its id as the key
 	 * 一个hashmap,包括所有treenodes由其id作为关键
 	 */
	@Deprecated
	public  void putChildIntoParent(HashMap nodeMap) {
		Iterator it = nodeMap.values().iterator();
		while (it.hasNext()) {
			TreeNode treeNode = (TreeNode) it.next();
			long parentId = treeNode.getParentId();
			String parentKeyId = String.valueOf(parentId);
			if (nodeMap.containsKey(parentKeyId)) {
				TreeNode parentNode = (TreeNode) nodeMap.get(parentKeyId);
				if (parentNode == null) {
					return;
				} else {
					parentNode.addChildNode(treeNode);
				}
			}
		}
	}
	
	/**
	 * put all the treeNodes into a hash table by its id as the key
	 * 把所有的treeNodes成一张哈希表由其id作为关键.
	 *
	 * @return hashmap that contains the treenodes
	 */
	private HashMap<String,TreeNode> getNodesIntoMap(String type) {
		long maxId = Long.MAX_VALUE;
		HashMap<String,TreeNode> nodeMap = new HashMap<String, TreeNode>();
		Iterator<TreeNode> it = tempNodeList.iterator();
		while (it.hasNext()) {
			TreeNode treeNode = (TreeNode) it.next();
			long id = treeNode.getSelfId();
			if (id < maxId) {
				maxId = id;
				if("all".equals(type)){
					rootAll = treeNode;
				}else{
					root = treeNode;
				}
			}
			String keyId = String.valueOf(id);
			nodeMap.put(keyId, treeNode);
		}
		return nodeMap;
	}


	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#addTreeNode(com.legendshop.model.dto.TreeNode)
	 */
	@Override
	public void addTreeNode(TreeNode treeNode) {
		lock.lock();
		try {
			  root.insertJuniorNode(treeNode); //将已经产生的树节点插入到树
			/*   if (insertFlag) {
				   tempNodeList.add(treeNode);
			   }
			   else{
				   throw new BusinessException(treeNode.getParentId()+"这父节点不存在", ErrorCode.BUSINESS_ERROR);
			   }*/
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#addTreeNodeForAll(com.legendshop.model.dto.TreeNode)
	 */
	@Override
	public void addTreeNodeForAll(TreeNode treeNode) {
		lock.lock();
		try {
			  rootAll.insertJuniorNode(treeNode); //将已经产生的树节点插入到树
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
	
	/* 删除节点和它下面的晚辈 */
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#delTreeNode(com.legendshop.model.dto.TreeNode)
	 */
	@Override
	public void delTreeNode(TreeNode treeNode){
	/*	boolean absent = tempNodeList.contains(treeNode);
		if(absent){*/
			lock.lock();
			try {
			/*	List<TreeNode> delTempNodeList = new ArrayList<TreeNode>();
				List<TreeNode> childList = treeNode.getJuniors();
				if (AppUtils.isNotBlank(childList)) {
					for (int i = 0; i < childList.size(); i++) {
						delTempNodeList.add(childList.get(i));
					}
				}*/
				/*delTempNodeList.add(treeNode);*/
			/*	tempNodeList.removeAll(delTempNodeList); // 更新tempNodeList
*/
				if(treeNode.getLevel()==1){ //说明是一级菜单
					getRoot().deleteChildNode(treeNode.getSelfId());
				}else{
					treeNode.deleteNode();
				}	
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				lock.unlock();
			}
		/*}*/
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#delTreeNodeForAll(com.legendshop.model.dto.TreeNode)
	 */
	@Override
	public void delTreeNodeForAll(TreeNode treeNode){
		lock.lock();
		try {
			if(treeNode.getLevel()==1){ //说明是一级菜单
				getRootAll().deleteChildNode(treeNode.getSelfId());
			}else{
				treeNode.deleteNode();
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
	
	/* 删除当前节点的某个子节点 */
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#delTreeNode(com.legendshop.model.dto.TreeNode, long)
	 */
	@Override
	public void delTreeNode(TreeNode treeNode,long childId){
		/*boolean absent = tempNodeList.contains(treeNode);
		if(absent){*/
			lock.lock();
			try {
				List<TreeNode> childList = treeNode.getChildList();
				if(AppUtils.isNotBlank(childList)){
					int childNumber = childList.size();
					for (int i = 0; i < childNumber; i++) {
						TreeNode child = childList.get(i);
						if (child.getSelfId() == childId) {
							/*tempNodeList.remove(child); */
							treeNode.deleteChildNode(childId);
						}
					}
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				lock.unlock();
			}
		/*}*/
	}
	
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#updateTreeNode(com.legendshop.model.dto.TreeNode, com.legendshop.model.entity.Category)
	 */
	@Override
	public void updateTreeNode(TreeNode sourceTreeNode,Category category){
		/*boolean absent = tempNodeList.contains(sourceTreeNode);
		if(absent){*/
			lock.lock();
			try {
				//组装 分类导航 的推荐内容
				if(category.getGrade().intValue()==1 && AppUtils.isNotBlank(category.getRecommCon())){
					RecommConDto recommConDto = JSONUtil.getObject(category.getRecommCon(), RecommConDto.class);
					category.setRecommConDto(recommConDto);
				}
				sourceTreeNode.setNodeName(category.getName());
				sourceTreeNode.setPic(category.getPic());
				sourceTreeNode.setObj(category);
				sourceTreeNode.setSeq(category.getSeq());
				LOGGER.info(" update Memory Category name={} , categoryId={} ",category.getName(),category.getId());
			   /* tempNodeList.add(sourceTreeNode);*/
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				lock.unlock();
			}
		/*}*/
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.util.CategoryManagerUtilIF#updateTreeNodeForAll(com.legendshop.model.dto.TreeNode, com.legendshop.model.entity.Category)
	 */
	@Override
	public void updateTreeNodeForAll(TreeNode sourceTreeNode,Category category){
		if(sourceTreeNode == null || category == null){
			LOGGER.warn("updateTreeNodeForAll failed for null");
			return;
		}
		
		lock.lock();
		try {
			//组装 分类导航 的推荐内容
			if(category.getGrade().intValue()==1 && AppUtils.isNotBlank(category.getRecommCon())){
				RecommConDto recommConDto = JSONUtil.getObject(category.getRecommCon(), RecommConDto.class);
				category.setRecommConDto(recommConDto);
			}
			sourceTreeNode.setNodeName(category.getName());
			sourceTreeNode.setPic(category.getPic());
			sourceTreeNode.setObj(category);
			sourceTreeNode.setSeq(category.getSeq());
			LOGGER.info(" update Memory Category name={} , categoryId={} ",category.getName(),category.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}

	

  /**
  	 * adapt the entities to the corresponding treeNode
  	 * 适应于相应的treeNode的实体.
  	 *
  	 * @param entityList list that contains the entities       列表包含的实体
  	 * @return the list containg the corresponding treeNodes of the entities
  	 * containg相应的treeNodes名单的实体
  	 */
	public Set<TreeNode> changeEnititiesToTreeNodes(List<Category> entityList) {
		Set<TreeNode> tempNodeList = new HashSet<TreeNode>(); //new TreeNodeComparator()
		TreeNode treeNode;
		TreeNode rootNode = new TreeNode(-1l, 0l, "商品分类根",0,0, null);
		Iterator<Category> it = entityList.iterator();
		while (it.hasNext()) {
			Category orgEntity = (Category) it.next();
			treeNode = new TreeNode();
			
			//组装 分类导航 的推荐内容
			if(orgEntity.getGrade().intValue()==1 && AppUtils.isNotBlank(orgEntity.getRecommCon())){
				RecommConDto recommConDto = JSONUtil.getObject(orgEntity.getRecommCon(), RecommConDto.class);
				orgEntity.setRecommConDto(recommConDto);
			}
			
			treeNode.setObj(orgEntity);
			treeNode.setParentId(orgEntity.getParentId());
			treeNode.setSelfId(orgEntity.getId());
			treeNode.setPic(orgEntity.getPic());
			treeNode.setNodeName(orgEntity.getName());
			treeNode.setPic(orgEntity.getPic());
			treeNode.setSeq(orgEntity.getSeq());
			treeNode.setLevel(orgEntity.getGrade());
			tempNodeList.add(treeNode);
		}
		tempNodeList.add(rootNode);
		return  tempNodeList;
	}
	
	/**
	 * 获取根节点
	 */
	private TreeNode getRoot() {
		return root;
	}

	/**
	 * 获取所有的节点
	 */
	private TreeNode getRootAll() {
		return rootAll;
	}
	
	/**
	 * 获取所有上线的节点
	 */
	private TreeNode getRootOnline() {
		return rootOnline;
	}
	
	
	/**
	 * Gets the temp node list.
	 *
	 * @return the temp node list
	 */
	public Set<TreeNode> getTempNodeList() {
		return tempNodeList;
	}

	/**
	 * Sets the temp node list.
	 *
	 * @param tempNodeList the new temp node list
	 */
	public void setTempNodeList(Set<TreeNode> tempNodeList) {
		this.tempNodeList = tempNodeList;
	}

	/**
	 * 根据父节点找到子节点
	 */
	@Override
	public List<Category> getAllChildrenByFunction(Long parentId,Integer navigationMenu,String type,Integer status) {
		//从缓存中查找
		List<Category> categories=null;
		TreeNode node = getTreeNodeById(parentId);
		if(node != null){
			categories = new ArrayList<Category>();
			List<TreeNode> childNodes = node.getChildList();
			if(childNodes != null){
				for (TreeNode treeNode : childNodes) {
					Category category = treeNode.getObj();
					if(shouldAddCategory(navigationMenu,type,status,category)){
						categories.add(category);
				}
			}
		}
		}
		return categories;
	}
	
	/**
	 * 
	 * 从对象的子集中查找满足条件的记录
	 * @param navigationMenu
	 * @param type
	 * @param status
	 * @param category
	 * @return
	 */
	private boolean shouldAddCategory(Integer navigationMenu,String type,Integer status, Category category){
		boolean result = true;
		if(category.getNavigationMenu()){
			if(navigationMenu != null && navigationMenu.intValue() == 1){
				result = true;
			}else{
				result = false;
			}
		}else{
			if(navigationMenu == null){
				result = true;
			}else{
				result = false;
			}
		}
		
		if(result && category.getType().equals(type)){
			result = true;
		}else{
			result = false;
		}
		
		if(result && category.getStatus().equals(status)){
			result = true;
		}else{
			result = false;
		}
		
		return result;
	}
	

	/**
	 * 根据 categoryId 查找 分类下的类型id，从最低层找起，找到就返回第一个
	 */
	@Override
	public Integer getRelationTypeId(Long categoryID) {
		if(categoryID==null){
			return null;
		}
		TreeNode node  = getTreeNodeByIdForAll(categoryID);
		if(node!=null && node.getObj()!=null){
			Integer typeId = node.getObj().getTypeId();
			if(typeId != null){
				return typeId;
			}else{
				TreeNode parentNode = node.getParentNode();
				if(parentNode == null){
					return null;
				}else{
					while (parentNode  != null &&  parentNode.getObj() != null) {
						typeId = parentNode.getObj().getTypeId();
						if(typeId != null){
							return typeId;
						}
						parentNode = parentNode.getParentNode();
					}
				}
				
			}
			return typeId;
		}else{
			return null;
		}
	}
	

}
