package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.TransCity;
import com.legendshop.model.entity.Transfee;
import com.legendshop.util.AppUtils;

public class TransCityComparer implements DataComparer<Long,TransCity>{

	@Override
	public boolean needUpdate(Long newCityId, TransCity originCityId, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(Long newCityId, TransCity originCity) {
		if(AppUtils.isNotBlank(newCityId) && AppUtils.isNotBlank(originCity) && newCityId.equals(originCity.getCityId())){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public TransCity copyProperties(Long newCityId, Object obj) {
		Transfee transfee = (Transfee)obj;
		TransCity transCity = new TransCity();
		transCity.setCityId(newCityId);
		transCity.setTransfeeId(transfee.getId());
		return transCity;
	}

}
