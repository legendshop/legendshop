/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import cn.hutool.core.util.ObjectUtil;
import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.BrandDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.entity.Brand;
import com.legendshop.util.AppUtils;

/**
 * 
 * 品牌Dao
 */
@Repository
public class BrandDaoImpl extends GenericDaoImpl<Brand, Long> implements BrandDao {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(BrandDaoImpl.class);

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	private final String sqlForGetBrandLimitBySortId = "select b.brand_id as brandId,b.brand_name as brandName,b.brand_pic as brandPic, b.user_id as userId,b.user_name as userName, b.memo,b.seq, b.status, b.is_commend as commend  from ls_nsort ns, ls_ns_brand nb, ls_brand b where ns.nsort_id = nb.nsort_id and nb.brand_id = b.brand_id and ns.sort_id = ?";

	private final String sqlForQueryBrandListByProdTypeId = "select lb.brand_id as brandId, lb.brand_name as brandName, lb.brand_pic as brandPic  " +
			"from ls_brand lb, ls_prod_type_brand lptb, ls_prod_type lpt where lb.brand_id = lptb.brand_id and lptb.type_id = lpt.id  and lptb.type_id = ? order by lptb.id";
	
	@Override
	public void deleteBrandById(Long brandId) {
		deleteById(brandId);
	}


	@Override
	@CacheEvict(value = "Brand", key = "#brand.brandId")
	public void updateBrand(Brand brand) {
		update(brand);
	}


	@Override
	public boolean hasChildProduct(Long brandId) {
		return getLongResult("select count(*) from ls_prod where brand_id = ?", brandId) > 0;
	}

	/**
	 * 查找出默认商城所有精品的品牌
	 */
	public List<Brand> getAllCommentBrand() {
		 List<Brand> list= queryByProperties(new EntityCriterion().eq("status", 1).eq("commend", 1).addAscOrder("seq"));
		 return list;
	}
	

	@Override
	public List<Brand> getAllBrand() {
		 List<Brand> list= queryByProperties(new EntityCriterion().eq("status", 1).addAscOrder("seq"));
		 return list;
	}
	
	@Override
	public List<Brand> getAvailableBrands() {
		 List<Brand> list = queryByProperties(new EntityCriterion().eq("status", 1).addAscOrder("seq"));
		 return list;
	}

	/**
	 * 根据一级商品的id 查找出前30对应的品牌
	 */
	public List<Brand> getBrandBySortId(Long sortId) {
		return getBrandLimitBySortId(sortId, 0, 29);
	}

	/**
	 * 根据一级商品的id 查找出后30对应的品牌
	 * 
	 * @param sortId
	 * @return
	 */
	@Cacheable(value = "BrandList")
	public List<Brand> getMoreBrandList(Long sortId) {
		return getBrandLimitBySortId(sortId, 30, 99);
	}

//	@Cacheable(value = "BrandList")
	private List<Brand> getBrandLimitBySortId(Long sortId, int start, int offset) {
		List<Brand> list = queryLimit(sqlForGetBrandLimitBySortId, Brand.class, start, offset, sortId);
		return list;
	}

	@Override
	public List<Brand> getBrandByUserName(String userName) {
		return queryByProperties(new EntityCriterion().eq("userName", userName));
	}

	@Override
	public Brand getBrandById(Long id) {
		return getById(id);
	}

	@Override
	public List<Brand> getBrandList(Long prodTypeId) {
		return query(sqlForQueryBrandListByProdTypeId,Brand.class,prodTypeId);
	}

	@Override
	public String saveBrandItem(List<String> idList, Long nsortId, String userName) {
		return null;
	}

	@Override
	public List<Brand> queryAllBrand() {
		List<Brand> list = queryByProperties(new EntityCriterion().addAscOrder("seq"));
		return list;
	}

	@Override
	public PageSupport<Brand> queryBrands(String curPageNO, Integer status) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class,curPageNO);
		cq.setPageSize(42);
		cq.addAscOrder("seq");
		cq.eq("status", status);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Brand> brand(String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(Brand.class, systemParameterUtil.getFrontPageSize(), curPageNO);

		QueryMap map = new QueryMap();
		map.put("status", Constants.ONLINE);
		query.setParam(map.toArray());

		String queryBrandCount = ConfigCode.getInstance().getCode("biz.getBrandCount", map);
		String queryBrand = ConfigCode.getInstance().getCode("biz.getBrand", map);
		
		query.setAllCountString(queryBrandCount);
		query.setQueryString(queryBrand);
		return querySimplePage(query);
	}
	
	
	@Override
	public PageSupport<Brand> getBrandsPage(String curPageNO, String brandName) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("status", "1");
		if(AppUtils.isNotBlank(brandName)){
			cq.like("brandName",brandName,MatchMode.ANYWHERE);
		}
		return queryPage(cq);
	}


	@Override
	public PageSupport<Brand> getDataByCriteriaQueryPage(String curPageNO, String brandName, Long proTypeId) {
//		SimpleSqlQuery query = new SimpleSqlQuery(Brand.class, curPageNO);
//    	QueryMap map = new QueryMap();
//    	query.setPageSize(10);
//    	map.like("brandName", brandName,MatchMode.ANYWHERE);
//    	map.put("proTypeId", proTypeId);
//    	query.setAllCountString(ConfigCode.getInstance().getCode("prod.queryProdBrandCount", map));
//    	query.setQueryString(ConfigCode.getInstance().getCode("prod.queryProdBrandList",map));
//    	query.setParam(map.toArray());
//		return querySimplePage(query);

		SimpleSqlQuery query = new SimpleSqlQuery(Brand.class, 10, curPageNO);
		QueryMap map = new QueryMap();
    	map.like("brandName", brandName,MatchMode.ANYWHERE);
    	map.put("proTypeId", proTypeId);

		query.setSqlAndParameter("prod.queryProdBrandList", map);
		PageSupport<Brand> page = querySimplePage(query);

		return page;
	}


	@Override
	public PageSupport<Brand> getBrandsPage() {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, "1");
        cq.setPageSize(999);
		return queryPage(cq);
	}


	@Override
	public PageSupport<Brand> getDataByCriteriaQuery(String userName, String brandName) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, "1");
        cq.setPageSize(30);
        cq.like("brandName", brandName,MatchMode.START);
        cq.eq("userName",userName );
		return queryPage(cq);
	}


	@Override
	public PageSupport<Brand> getDataByPage(String curPageNO, String userName) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("userName", userName);
		cq.addDescOrder("applyTime");
		return queryPage(cq);
	}


	//
	@Override
	public PageSupport<Brand> getDataByPageByUserId(String curPageNO, String userId) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("userId", userId);
		cq.addDescOrder("applyTime");
		return queryPage(cq);
	}
	
	

	@Override
	public PageSupport<Brand> getDataByBrandName(String brandName, String userName) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, "1");
        cq.setPageSize(30);
        cq.like("brandName", brandName,MatchMode.START);
        cq.eq("userName", userName);
		return queryPage(cq);
	}


	@Override
	public PageSupport<Brand> queryBrandListPage(String curPageNO, Brand brand, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(Brand.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(brand.getBrandName())) {
			map.like("brandName", brand.getBrandName().trim());
		}
		
		map.put("status", brand.getStatus());
		map.put("userName", brand.getUserName());
		map.put("commend", brand.getCommend());
		
		if(AppUtils.isNotBlank(brand.getShopId())) {			
			map.put("shopId", brand.getShopId());
		}
		if(AppUtils.isNotBlank(brand.getShopName())) {			
			map.like("shopName", brand.getShopName().trim());
		}
		/*//判断是否为外部排序
		if (!result.isSortExternal()) {
			map.put(Constants.ORDER_INDICATOR, "order by b.is_commend desc,b.seq asc,b.applyTime desc");
 		}else{
 			map.put(Constants.ORDER_INDICATOR,  result.parseSeq());
 		}*/
		
		String querySQL = ConfigCode.getInstance().getCode("brand.getbrandList", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("brand.getbrandListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove(Constants.ORDER_INDICATOR);
		
		query.setParam(map.toArray());
		return querySimplePage(query);
	}


	@Override
	public PageSupport<Brand> getBrandsByNamePage(String brandName) {
		CriteriaQuery cq = new CriteriaQuery(Brand.class, "1");
	    cq.setPageSize(20);
	    cq.like("brandName", brandName,MatchMode.START);
		return queryPage(cq);
	}


	@Override
	public boolean checkBrandByName(String brandName,Long brandId) {
		List<String> list = null;
		if(AppUtils.isNotBlank(brandId)){
			list = query("select brand_id from ls_brand where brand_name = ? and brand_id <> ?", String.class, brandName,brandId);
		}else{
			list = query("select brand_id from ls_brand where brand_name = ?", String.class, brandName);
		}
	
		return AppUtils.isNotBlank(list);
		
	}

	@Override
	public int batchDel(String ids) {
		String[] idList=ids.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("DELETE FROM ls_brand WHERE brand_id IN( ");
		for(String id:idList){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(), idList);
	}


	@Override
	public int batchOffOrOnLine(String ids,Integer brandStatus) {
		
		String[] idList=ids.split(",");
		StringBuffer buffer=new StringBuffer();
		
		ArrayList<String> paraList=new ArrayList<>();
		paraList.add(brandStatus.toString());
		//buffer.append(brandStatus);
		buffer.append("UPDATE ls_brand SET status=? WHERE brand_id IN( ");
		
		for(String id:idList){
			buffer.append("?,");
			paraList.add(id);
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		Object[] sss=paraList.toArray();
		return update(buffer.toString(), sss);
	}


	@Override
	public List<BrandDto> likeBrandName(String brandName,Integer typeId) {
		String sql = "SELECT b.brand_id AS brandId, b.brand_name AS brandName  FROM ls_brand b INNER JOIN ls_prod_type_brand ptb on ptb.brand_id=b.brand_id where b.status=1 AND b.brand_name LIKE ? AND  ptb.type_id=? ";
		brandName = "%"+brandName+"%";
		
		return query(sql, BrandDto.class, brandName,typeId);
	}


	@Override
	public boolean checkBrandByNameByShopId(String brandName, Long brandId, Long shopId) {
		List<String> list = null;
		if(AppUtils.isNotBlank(brandId)){
			list = query("select brand_id from ls_brand where brand_name = ? and brand_id <> ? and shop_id = ?", String.class, brandName,brandId,shopId);
		}else{
			list = query("select brand_id from ls_brand where brand_name = ? and shop_id = ?", String.class, brandName,shopId);
		}
	
		return AppUtils.isNotBlank(list);
	}

	@Override
	public List<Brand> getExportBrands(Brand brand) {
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(brand.getBrandName())) {
			map.like("brandName", brand.getBrandName().trim());
		}		
		map.put("status", brand.getStatus());
		map.put("userName", brand.getUserName());
		map.put("commend", brand.getCommend());
		
		if(AppUtils.isNotBlank(brand.getShopId())) {			
			map.put("shopId", brand.getShopId());
		}
		if(AppUtils.isNotBlank(brand.getShopName())) {			
			map.like("shopName", brand.getShopName().trim());
		}
		String querySQL = ConfigCode.getInstance().getCode("brand.getbrandList", map);
		return query(querySQL, Brand.class, map.toArray());
	}

	@Override
	public List<Brand> getBrandsByName(String name) {
		if (AppUtils.isNotBlank(name)) {
			String sql = "SELECT brand_id,brand_name,STATUS,seq,brand_pic_mobile FROM ls_brand WHERE brand_name LIKE ?";
			return query(sql, Brand.class, "%" + name + "%");
		}
		String sql = "SELECT brand_id,brand_name,STATUS,seq,brand_pic_mobile FROM ls_brand";
		return query(sql, Brand.class);
	}

}
