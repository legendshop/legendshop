/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.business.comparer.WeiXinGroupsComparator;
import com.legendshop.business.dao.WeixinGroupDao;
import com.legendshop.model.dto.weixin.WeiXinGroups;
import com.legendshop.model.entity.weixin.WeixinGroup;
import com.legendshop.spi.service.WeixinGroupService;
import com.legendshop.util.AppUtils;

/**
 * 微信组服务
 */
@Service("weixinGroupService")
public class WeixinGroupServiceImpl  implements WeixinGroupService{
	
	@Autowired
    private WeixinGroupDao weixinGroupDao;
    
    private DataListComparer<WeiXinGroups,WeixinGroup> weixinGoupListComparer = null;

    public List<WeixinGroup> getWeixinGroup(String userName) {
        return weixinGroupDao.getWeixinGroup(userName);
    }

    public WeixinGroup getWeixinGroup(Long id) {
        return weixinGroupDao.getWeixinGroup(id);
    }

    public void deleteWeixinGroup(WeixinGroup weixinGroup) {
        weixinGroupDao.deleteWeixinGroup(weixinGroup);
    }

    public Long saveWeixinGroup(WeixinGroup weixinGroup) {
        return weixinGroupDao.saveWeixinGroup(weixinGroup);
    }

    public void updateWeixinGroup(WeixinGroup weixinGroup) {
        weixinGroupDao.updateWeixinGroup(weixinGroup);
    }

	@Override
	public List<WeixinGroup> getWeixinGroup() {
		return weixinGroupDao.getWeixinGroup();
	}

	@Override
	public void synchronousGroup(List<WeiXinGroups> dtList,List<WeixinGroup> dbList, Object obj) {
		Map<Integer,List<WeixinGroup> > map=getWeixinGoupListComparer().compare(dtList,dbList , null);
		if(AppUtils.isNotBlank(map)){
			 List<WeixinGroup> saveGroups=map.get(1);
			 List<WeixinGroup> delGroups=map.get(-1);
			 List<WeixinGroup> updateGroups=map.get(0);
			 if(saveGroups!=null){
				 for (int i = 0; i < saveGroups.size(); i++) {
					 weixinGroupDao.saveWeixinGroup(saveGroups.get(i));
				}
			 }
			 if(delGroups!=null){
				 for (int i = 0; i < delGroups.size(); i++) {
					 weixinGroupDao.deleteWeixinGroup(delGroups.get(i));
				}
			 }
			 if(updateGroups!=null){
				 for (int i = 0; i < updateGroups.size(); i++) {
					 weixinGroupDao.updateWeixinGroup(updateGroups.get(i));
				}
			 }
		}
	}
	
	public DataListComparer<WeiXinGroups,WeixinGroup> getWeixinGoupListComparer() {
		if (weixinGoupListComparer == null) {
			weixinGoupListComparer = new DataListComparer<WeiXinGroups,WeixinGroup>(new WeiXinGroupsComparator());
		}
	    return weixinGoupListComparer;
		} 
}
