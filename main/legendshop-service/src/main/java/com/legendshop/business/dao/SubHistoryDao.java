/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;

/**
 * 订单历史
 */
public interface SubHistoryDao extends Dao<SubHistory, Long> {

	/**
	 * 保存订单历史.
	 *
	 * @param sub the sub
	 * @param subAction the sub action
	 */
	public abstract void saveSubHistory(Sub sub, String subAction);
	
	/**
	 * 保存订单历史..
	 *
	 * @param subHistory the sub history
	 */
	public abstract void saveSubHistory(SubHistory subHistory);

	/**
	 * 根据订单号找到订单历史
	 *
	 * @param subId the sub id
	 * @return the list< sub history>
	 */
	public abstract List<SubHistory> findSubHistoryBySubId(Long subId);

	public abstract void saveSubHistoryList(List<SubHistory> his);
	
}
