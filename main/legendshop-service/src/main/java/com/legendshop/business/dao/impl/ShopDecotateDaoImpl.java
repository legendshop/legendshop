/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopDecotateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;
import com.legendshop.model.entity.shopDecotate.ShopDecotate;
import com.legendshop.util.AppUtils;

/**
 * 商家装修Dao.
 */
@Repository
public class ShopDecotateDaoImpl extends GenericDaoImpl<ShopDecotate, Long> implements ShopDecotateDao  {
	
	private String shopLayoutShopInfoDtoSQL = "SELECT s.shop_id as shopId,s.credit as credit,s.contact_mobile as contactMobile,s.brief_desc as briefDesc,s.shop_addr as addr,  s.site_name as  siteName,s.contact_QQ as qq ,s.banner_pic as bannerPic,s.shop_pic as shopPic,ls_provinces.province as province" +
			",ls_cities.city as city,ls_areas.area as area FROM ls_shop_detail s  LEFT JOIN ls_provinces ON ls_provinces.id=s.provinceid " +
			"LEFT JOIN ls_cities ON ls_cities.id=s.cityid LEFT JOIN  ls_areas ON ls_areas.id=s.areaid WHERE s.shop_id=? ";

	public ShopDecotate getShopDecotate(Long id){
		return getById(id);
	}
	
    public int deleteShopDecotate(ShopDecotate shopDecotate){
    	return delete(shopDecotate);
    }
	
	public Long saveShopDecotate(ShopDecotate shopDecotate){
		return save(shopDecotate);
	}
	
	public int updateShopDecotate(ShopDecotate shopDecotate){
		return update(shopDecotate);
	}
	
	@Override
	public ShopDecotate getShopDecotateByShopId(Long shopId) {
		ShopDecotate decotates= this.getByProperties(new EntityCriterion().eq("shopId", shopId).addDescOrder("createDate"));
		return  decotates;
	}

	@Override
	//@Cacheable(value="ShopLayoutCateogryDtoList",key="#shopId")
	public List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId) {
		return query("select id,parent_id as parentId,name,seq from ls_shop_cat where shop_id=? and status=1 and parent_id is null order by seq asc", ShopLayoutCateogryDto.class, shopId);
	}
	
	@Override
	//@Cacheable(value="ShopLayoutCateogryDtoList",key="#shopId+#categoryId")
	public List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId,Long categoryId) {
		return query("select id,parent_id as parentId,name,seq from ls_shop_cat where shop_id=? and status=1 and parent_id=? order by seq asc  ", ShopLayoutCateogryDto.class, shopId,categoryId);
	}
	
	
	/**
	 * 获取商家信息
	 */
	@Override
	//@Cacheable(value="ShopLayoutShopInfoDto",key="#shopId")
	public ShopLayoutShopInfoDto findShopInfo(Long shopId) {
		ShopLayoutShopInfoDto infoDto= get(shopLayoutShopInfoDtoSQL, ShopLayoutShopInfoDto.class, shopId);
		if(AppUtils.isNotBlank(infoDto.getQq())){
			String [] str=infoDto.getQq().split(",");
			infoDto.setQq(str[0]);
		}
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		StringBuilder sb=new StringBuilder();
		if(AppUtils.isNotBlank(infoDto.getProvince())){
			sb.append(infoDto.getProvince());
		}
		if(AppUtils.isNotBlank(infoDto.getCity())){
			sb.append(infoDto.getCity());
		}
		if(AppUtils.isNotBlank(infoDto.getArea())){
			sb.append(infoDto.getArea());
		}
		infoDto.setShopAddr(sb.toString());
		
		//计算该店铺所有已评分商品的平均分
		String sqlprod="SELECT  SUM(s.score) AS score,SUM(s.comments) AS comments FROM ls_prod_comm_stat s, ls_prod p WHERE s.prod_id = p.prod_id AND p.shop_id = ?";
		ProductCommentStat prodCommentStat = this.get(sqlprod, ProductCommentStat.class, shopId);
		if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
			prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
		}
		
		//计算该店铺所有已评分店铺的平均分
		String sqlshop="SELECT  SUM(s.score) AS score,SUM(s.count) AS count FROM ls_shop_comm_stat s WHERE s.shop_id = ?";
		ShopCommStat shopCommentStat = this.get(sqlshop, ShopCommStat.class, shopId);
		if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
			shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
		}
		//计算该店铺所有已评分物流的平均分
		String sqldvy="SELECT SUM(s.score) AS score,SUM(s.count) AS count FROM ls_dvy_type_comm_stat s INNER JOIN ls_sub t ON s.dvy_type_id=t.dvy_type_id WHERE t.shop_id=?";
		DvyTypeCommStat  dvyTypeCommStat = this.get(sqldvy, DvyTypeCommStat.class, shopId);
		if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
		    logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
		}
		Integer sum=0;
		if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
			sum=(int)Math.floor((shopscore+prodscore+logisticsScore)/3);
		}else if(shopscore!=0&&logisticsScore!=0){
			sum=(int)Math.floor((shopscore+logisticsScore)/2);
		}
		infoDto.setCredit(sum);
		return infoDto;
	
	}

	

	/**
	 * 获取热销商品排行
	 */
	@Override
	//@Cacheable(value="ShopLayoutHotProdDtoList",key="#shopId")
	public List<ShopLayoutHotProdDto> findShopHotProds(Long shopId) {
		return queryLimit("SELECT p.prod_id AS prodId,p.name AS prodName,p.pic AS pic,p.cash AS cash  FROM ls_prod p WHERE p.shop_id=? AND p.status=1 ORDER BY p.buys DESC",ShopLayoutHotProdDto.class, 0,5, shopId);
	}
	
	

	/**
	 * 获取自定义的商品信息
	 */
	@Override
	//@Cacheable(value="ShopLayoutProdDtoList",key="#shopId+#layoutId")
	public List<ShopLayoutProdDto> findShopLayoutProdDtos(Long shopId,Long layoutId) {
		return query("SELECT lp.id as layoutProdId, lp.prod_id AS prodId,p.name AS prodName,p.pic AS pic,p.cash AS cash,lp.seq AS seq,lp.shop_id as shopId,lp.shop_decotate_id as shopDecotateId,lp.layout_id as layoutId FROM  ls_shop_layout_prod lp LEFT JOIN ls_prod p ON lp.prod_id=p.prod_id WHERE p.status=1 AND lp.shop_id=? AND lp.layout_id=? ORDER BY lp.seq ASC", ShopLayoutProdDto.class, shopId,layoutId);
	}
	
	@Override
	public List<ShopLayoutProdDto> findShopLayoutProdDtos(Long shopId) {
		return query("SELECT lp.id as layoutProdId, lp.prod_id AS prodId,p.name AS prodName,p.pic AS pic,p.cash AS cash,lp.seq AS seq,lp.shop_id as shopId,lp.shop_decotate_id as shopDecotateId,lp.layout_id as layoutId FROM  ls_shop_layout_prod lp LEFT JOIN ls_prod p ON lp.prod_id=p.prod_id WHERE p.status=1 AND lp.shop_id=? ORDER BY lp.seq ASC", ShopLayoutProdDto.class, shopId);
	}

	/**
	 * 当前店铺免费领取的优惠卷
	 * */
	@Override
	public List<Coupon> getCouponByShopId(Long shopId) {
		return queryLimit("SELECT c.coupon_id AS couponId,c.coupon_name AS couponName,c.coupon_number AS couponNumber,c.coupon_num_prefix AS couponNumPrefix,c.full_price AS fullPrice,c.off_price AS offPrice,c.coupon_type AS couponType FROM ls_coupon  c  WHERE c.shop_id=? AND c.coupon_provider='shop' AND c.status=1 AND c.get_type='free' AND c.is_dsignated_user=0 AND c.shop_del=0 AND end_Date>? ORDER BY c.create_date DESC ", Coupon.class, 0, 20, shopId,new Date());
	}

	/**
	 * 根据Dto来更新装修的信息
	 */
	@Override
	public void updateShopDecotate(ShopDecotateDto decotateDto) {
		Object[] object = new Object[] { decotateDto.getIsBanner(), decotateDto.getIsInfo(), decotateDto.getIsNav(),
				decotateDto.getIsSlide(), decotateDto.getTopCss(), decotateDto.getBgImgId(), decotateDto.getBgColor(),
				decotateDto.getBgStyle(), decotateDto.getDecId(), decotateDto.getShopId() };
		update("update ls_shop_decotate set is_banner=?,is_info=?,is_nav=?,is_slide=?,top_css=?,bg_img_id=?,bg_color=?,bg_style=? where id =? and shop_id=?",
				object);
		
		
	}
	
 }
