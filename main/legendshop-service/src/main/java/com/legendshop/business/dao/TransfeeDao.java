package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Transfee;
/**
 * 运输费用Dao
 */
public interface TransfeeDao extends GenericDao<Transfee, Long>{

	public abstract List<Transfee> getTransfee();

	public abstract Transfee getTransfee(Long id);
	
    public abstract void deleteTransfee(Transfee transfee);
	
	public abstract Long saveTransfee(Transfee transfee);
	
	public abstract void updateTransfee(Transfee transfee);
	
	public List<Transfee> getTranfee(Long transportId);

	public abstract List<Transfee> getTranfeeByShopId(Long shopId);
	
	public abstract List<Transfee> getTranfeeById(Long id);

	List<Transfee> getOrderTranfeeById(Long id);

	public abstract PageSupport<Transfee> getTransfeePage(String curPageNO, Transfee transfee);
}
