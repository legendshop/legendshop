/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ThemeRelatedDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.dto.ThemeRelatedDto;
import com.legendshop.model.entity.ThemeRelated;

/**
 *相关专题表Dao
 */
@Repository
public class ThemeRelatedDaoImpl extends GenericDaoImpl<ThemeRelated, Long> implements ThemeRelatedDao  {

	private final static String DELETESQL = " delete from ls_theme_related  where ls_theme_related.theme_id=? ";

	@Override
	public List<ThemeRelated> getThemeRelatedByTheme(Long themeId) {
		return this.queryByProperties(new EntityCriterion().eq("themeId", themeId));
	}

	@Override
	public ThemeRelated getThemeRelated(Long id){
		return getById(id);
	}
	
    @Override
	public int deleteThemeRelated(ThemeRelated themeRelated){
    	return delete(themeRelated);
    }
	
	@Override
	public Long saveThemeRelated(ThemeRelated themeRelated){
		return save(themeRelated);
	}
	
	@Override
	public int updateThemeRelated(ThemeRelated themeRelated){
		return update(themeRelated);
	}

    @Override
    public List<ThemeRelatedDto> getThemeRelatedDtoByTheme(Long themeId) {
		List<ThemeRelatedDto> query = this.query(
				"SELECT tr.related_theme_id AS relatedThemeId,tr.m_related_theme_id AS mRelatedThemeId,tr.img,tr.theme_id AS themeId,tr.related_id AS relatedId,t.title AS pcTitle,t2.title AS mobileTitle " +
						"FROM ls_theme_related tr " +
						"INNER JOIN ls_theme t " +
						"ON tr.related_theme_id = t.theme_id " +
						"INNER JOIN ls_theme t2 " +
						"ON tr.m_related_theme_id = t2.theme_id " +
						"WHERE tr.theme_id = ?"
				, ThemeRelatedDto.class, themeId);
		return query;
    }

    @Override
	public int deleteThemeRelatedByTid(Long theme_id) {
		return this.update(DELETESQL,theme_id);
	}

 }
