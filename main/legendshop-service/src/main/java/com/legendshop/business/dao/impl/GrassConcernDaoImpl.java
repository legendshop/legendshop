/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.GrassConcern;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassConcernDao;

/**
 * The Class GrassConcernDaoImpl. 种草社区关注表Dao实现类
 */
@Repository
public class GrassConcernDaoImpl extends GenericDaoImpl<GrassConcern, Long> implements GrassConcernDao{

	/**
	 * 根据Id获取种草社区关注表
	 */
	public GrassConcern getGrassConcern(Long id){
		return getById(id);
	}

	/**
	 *  删除种草社区关注表
	 *  @param grassConcern 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassConcern(GrassConcern grassConcern){
    	return delete(grassConcern);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassConcern(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草社区关注表
	 */
	public Long saveGrassConcern(GrassConcern grassConcern){
		return save(grassConcern);
	}

	/**
	 *  更新种草社区关注表
	 */		
	public int updateGrassConcern(GrassConcern grassConcern){
		return update(grassConcern);
	}

	/**
	 * 分页查询种草社区关注表列表
	 */
	public PageSupport<GrassConcern>queryGrassConcern(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassConcern.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public Integer rsConcern(String grassId, String uid) {
		String sql="select count(1) from ls_grass_concern where grauser_id=? and user_id=?";
		return get(sql, Integer.class, grassId,uid);
	}

	@Override
	public void deleteGrassConcernByDiscoverId(String grassUserId, String uid) {
		String sql="DELETE FROM ls_grass_concern WHERE grauser_id=? AND user_id=?";
		update(sql, grassUserId,uid);
	}

	@Override
	public Long getConcernCountByGrassUid(String graUserId) {
		String sql="select count(1) from ls_grass_concern WHERE grauser_id=?";
		return get(sql, Long.class, graUserId);
	}

	@Override
	public Long getPursuerCountByUid(String UserId) {
		String sql="select count(1) from ls_grass_concern WHERE user_id=?";
		return get(sql, Long.class, UserId);
	}

	@Override
	public PageSupport<GrassConcern> getConcernByGrassUid(String curPageNO, Integer pageSize, String graUserId) {
		CriteriaQuery criteriaQuery=new CriteriaQuery(GrassConcern.class, curPageNO);
		criteriaQuery.setPageSize(pageSize);
		criteriaQuery.eq("userId", graUserId);
		return queryPage(criteriaQuery);
	}

	@Override
	public PageSupport<GrassConcern> getPursuerByUid(String curPageNO, Integer pageSize, String UserId) {
		CriteriaQuery criteriaQuery=new CriteriaQuery(GrassConcern.class, curPageNO);
		criteriaQuery.setPageSize(pageSize);
		criteriaQuery.eq("grauserId", UserId);
		return queryPage(criteriaQuery);
	}

}
