/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.VisitLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.spi.service.VisitLogService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.ip.IPSeeker;

/**
 * 
 * 浏览历史实现类
 */
@Service("visitLogService")
public class VisitLogServiceImpl implements VisitLogService {

	@Autowired
	private VisitLogDao visitLogDao;

	@Autowired
	private ProductDao productDao;

	@Override
	public VisitLog getVisitLogById(Long id) {
		return visitLogDao.getVisitLogById(id);
	}

	@Override
	public void delete(Long id) {
		visitLogDao.deleteVisitLogById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.VisitLogService#save(com.legendshop.model
	 * .entity.VisitLog)
	 */
	@Override
	public Long save(VisitLog visitLog) {
		if (!AppUtils.isBlank(visitLog.getVisitId())) {
			update(visitLog);
			return visitLog.getVisitId();
		}
		return visitLogDao.save(visitLog);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.VisitLogService#update(com.legendshop
	 * .model.entity.VisitLog)
	 */
	@Override
	public void update(VisitLog visitLog) {
		visitLogDao.updateVisitLog(visitLog);
	}

	/**
	 * 保存访问历史
	 */
	@Override
	public void process(VisitLog visitLog) {
		visitLog.setArea(IPSeeker.getInstance().getArea(visitLog.getIp()));
		visitLog.setCountry(IPSeeker.getInstance().getCountry(visitLog.getIp()));
		VisitLog origin = null;
		if (VisitTypeEnum.INDEX.value().equals(visitLog.getPage())) {
			origin = visitLogDao.getVisitedIndexLog(visitLog);
		} else {
			origin = visitLogDao.getVisitedProdLog(visitLog);
		}
		// 半个小时之内的访问只算一次
		if (origin != null) {
			Integer num = origin.getVisitNum();
			if (num == null) {
				num = 1;
			} else {
				num++;
			}
			origin.setVisitNum(num);
			origin.setDate(new Date());
			//恢复用户删除状态
			origin.setIsUserDel(false);
			visitLogDao.updateVisitLog(origin);
		} else {
			visitLog.setVisitNum(1);
			visitLogDao.save(visitLog);
			// 更新商品的访问数量，同一个IP访问同一个商品，半个小时只能算一次
			if (VisitTypeEnum.PROD.value().equals(visitLog.getPage())) {
				productDao.updateProdViews(visitLog.getProductId());
			}
		}

	}

	@Override
	public PageSupport<VisitLog> getVisitLogListPage(String curPageNO, String userName, VisitLog visitLog) throws ParseException {
		return visitLogDao.getVisitLogListPage(curPageNO, userName, visitLog);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogListPage(String curPageNO, VisitLog visitLog, DataSortResult result, Integer pageSize) {
		return visitLogDao.getVisitLogListPage(curPageNO, visitLog, result, pageSize);
	}

	/**
	 * 根据用户名获取浏览历史统计数量
	 */
	@Override
	public Long getVisitLogCountByUserName(String userName) {
		
		return visitLogDao.getVisitLogCountByUserName(userName);
	}


}
