/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.CouponProd;

/**
 * 优惠券商品
 */
public interface CouponProdDao extends Dao<CouponProd, Long> {
     
	public abstract CouponProd getCouponProd(Long id);
	
    public abstract int deleteCouponProd(CouponProd couponProd);
	
	public abstract Long saveCouponProd(CouponProd couponProd);
	
	public abstract int updateCouponProd(CouponProd couponProd);
	
	public abstract List<CouponProd> getCouponProdByCouponId(Long couponId);

	public abstract void deleteByCouponId(Long couponId);

	public abstract List<CouponProd> getCouponProdByCouponIds(List<Long> couponIds);

	public abstract List<CouponProd> getCouponProdByProdId(Long prodId);

	public abstract List<Long> getCouponProdIds(Long couponId);
	
 }
