/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.framework.cache.listener.UpdateSystemParameterCache;
import com.legendshop.model.constant.SysParamGroupEnum;
import com.legendshop.model.constant.SystemCacheEnum;
import com.legendshop.model.dto.SystemParameterDto;
import com.legendshop.processor.MailPropertiesUpdatedProcessor;
import com.legendshop.sms.SMSSenderProxy;
import com.legendshop.spi.util.CategoryManagerUtil;

/**
 * 更新本地缓存.
 */
@Service("updateCache")
public class UpdateSystemParameterCacheImpl implements UpdateSystemParameterCache {
	
	private final Logger logger = LoggerFactory.getLogger(UpdateSystemParameterCacheImpl.class);
	
	@Autowired
	private CategoryManagerUtil categoryManagerUtil;
	
	/** 系统配置. */
	@Autowired
	private SystemParameterProvider systemParameterProvider;
	
	/** 系统配置. */
	@Autowired
	private SMSSenderProxy smsSenderProxy;
	
	@Autowired
	private MailPropertiesUpdatedProcessor mailPropertiesUpdatedProcessor;

	/**
	 * 更新本地缓存的操作
	 */
	@Override
	public void onMessage(CacheMessage cacheMessage) {
		if(SystemCacheEnum.CATEGORY.value().equals(cacheMessage.getCacheName())){
			categoryManagerUtil.generateTree();//在内存中重新生成树
			logger.warn("notify other node to update their cache {} ", cacheMessage.getCacheName());
		}else if(SystemCacheEnum.PROPERTIES.value().equals(cacheMessage.getCacheName())){//属性配置文件
			SystemParameterDto keyValue = (SystemParameterDto)cacheMessage.getKey();
			logger.warn("notify other node to update their cache {}, key {}", cacheMessage.getCacheName(), keyValue.getKey());
			systemParameterProvider.refreshLocalCache(keyValue.getGroupId(), keyValue.getKey(), keyValue.getValue(), true);
			
			//更新本地邮件配置
			if(SysParamGroupEnum.MAIL.value().equals(keyValue.getGroupId())) {
				mailPropertiesUpdatedProcessor.process(null);
			}
			
			//更新本地短信配置
			if(SysParamGroupEnum.SMS.value().equals(keyValue.getGroupId())) {
				smsSenderProxy.init();
			}
		}
	}
}
