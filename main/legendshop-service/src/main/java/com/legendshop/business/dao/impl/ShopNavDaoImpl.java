/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopNavDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopNav;

/**
 * 店铺的头部导航菜单.
 */
@Repository
public class ShopNavDaoImpl extends GenericDaoImpl<ShopNav, Long> implements ShopNavDao  {

	public ShopNav getShopNav(Long id){
		return getById(id);
	}
	
	@Caching(evict={
	@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopNav.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopNav.shopId")})
    public int deleteShopNav(ShopNav shopNav){
    	return delete(shopNav);
    }
	
	@Caching(evict={
	@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopNav.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopNav.shopId")})
	public Long saveShopNav(ShopNav shopNav){
		return save(shopNav);
	}
	
	@Caching(evict={
	@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopNav.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopNav.shopId")})
	public int updateShopNav(ShopNav shopNav){
		return update(shopNav);
	}

	@Override
	public List<ShopNav> getLayoutNav(Long shopId) {
		List<ShopNav> navs= queryByProperties(new EntityCriterion().eq("status", 1).eq("shopId", shopId).addAscOrder("seq"));
		return navs;
	}

	@Override
	public PageSupport<ShopNav> getShopNavPage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ShopNav.class, curPageNO);
	    cq.eq("shopId", shopId);
	    cq.setPageSize(10);
		return queryPage(cq);
	}
	
 }
