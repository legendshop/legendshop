/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassLabelDao;
import com.legendshop.model.entity.GrassLabel;
import com.legendshop.spi.service.GrassLabelService;

/**
 * The Class GrassLabelServiceImpl.
 *  种草社区标签表服务实现类
 */
@Service("grassLabelService")
public class GrassLabelServiceImpl implements GrassLabelService{

    /**
     *
     * 引用的种草社区标签表Dao接口
     */
	@Autowired
    private GrassLabelDao grassLabelDao;
   
   	/**
	 *  根据Id获取种草社区标签表
	 */
    public GrassLabel getGrassLabel(Long id) {
        return grassLabelDao.getGrassLabel(id);
    }
    
    /**
	 *  根据Id删除种草社区标签表
	 */
    public int deleteGrassLabel(Long id){
    	return grassLabelDao.deleteGrassLabel(id);
    }

   /**
	 *  删除种草社区标签表
	 */ 
    public int deleteGrassLabel(GrassLabel grassLabel) {
       return  grassLabelDao.deleteGrassLabel(grassLabel);
    }

   /**
	 *  保存种草社区标签表
	 */	    
    public Long saveGrassLabel(GrassLabel grassLabel) {
        if (!AppUtils.isBlank(grassLabel.getId())) {
            updateGrassLabel(grassLabel);
            return grassLabel.getId();
        }
        return grassLabelDao.saveGrassLabel(grassLabel);
    }

   /**
	 *  更新种草社区标签表
	 */	
    public void updateGrassLabel(GrassLabel grassLabel) {
        grassLabelDao.updateGrassLabel(grassLabel);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassLabel> queryGrassLabel(String curPageNO, Integer pageSize){
     	return grassLabelDao.queryGrassLabel(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassLabelDao(GrassLabelDao grassLabelDao) {
        this.grassLabelDao = grassLabelDao;
    }

	@Override
	public PageSupport<GrassLabel> queryGrassLabelPage(String curPageNO, String name, Integer pageSize) {
		return grassLabelDao.queryGrassLabelPage(curPageNO, name, pageSize);
	}

	@Override
	public PageSupport<GrassLabel> getGrassLabelByLabel(String curPageNo,GrassLabel grassLabel,Integer PageSize){
		return grassLabelDao.getGrassLabelByLabel( curPageNo, grassLabel, PageSize);
	}

    @Override
    public void addRefNum(Long lid) {
        grassLabelDao.addRefNum(lid);
    }

}
