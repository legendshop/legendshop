/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PassportDao;
import com.legendshop.business.dao.PassportSubDao;
import com.legendshop.model.dto.IdPassord;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.UserResultDto;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.PassportSub;
import com.legendshop.model.entity.User;
import com.legendshop.model.form.UserForm;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 第三方登陆
 * The Class PassportServiceImpl.
 */
@Service("passportService")
public class PassportServiceImpl  implements PassportService{
	
	@Autowired
    private PassportDao passportDao;
    
	@Autowired
    private UserDetailService userDetailService;
	
	@Autowired
	private PassportSubDao passportSubDao;

    public List<Passport> getPassport(String userId) {
        return passportDao.getPassport(userId);
    }

    public Passport getPassport(Long id) {
        return passportDao.getPassport(id);
    }

    public void deletePassport(Passport passport) {
        passportDao.deletePassport(passport);
    }

    public Long savePassport(Passport passport) {
    	Passport origin = passportDao.getUserIdByOpenId(passport.getType(), passport.getOpenId());
    	if(origin != null){//had exists
    		origin.setAccessToken(passport.getAccessToken());
    		origin.setProps(passport.getProps());
    		origin.setUserId(passport.getUserId());
    		origin.setCreateTime(new Date());
    		origin.setLastLoginTime(origin.getCreateTime());
    		origin.setNickName(AppUtils.removeYanText(passport.getNickName()));
    		origin.setUserName(passport.getUserName());
    		 return new Long(passportDao.updatePassport(origin));
    	}else{
    		passport.setNickName(AppUtils.removeYanText(passport.getNickName()));//去掉颜文字
    		 return passportDao.savePassport(passport);
    	}
    }

    public void updatePassport(Passport passport) {
        passportDao.updatePassport(passport);
    }
    
	@Override
	public int updateLastLoginTime(Passport passport) {
		return passportDao.updateLastLoginTime(passport);
	}

	@Override
	public Passport getUserIdByOpenId(String type, String openId) {
		 return passportDao.getUserIdByOpenId(type, openId);
	}

	@Override
	public void deletePassport(String userId, String type) {
		List<Passport> passportList = passportDao.getPassport(userId, type);
		if(AppUtils.isNotBlank(passportList)){
			for (Passport passport : passportList) {
				passportDao.deletePassport(passport);//方便清除缓存
			}
			
		}
		
	}

	/**
	 * 第三方登陆， 注册并绑定
	 */
	@Override
	public UserResultDto savePassportAndReg(String ip, UserForm userForm, Passport passport, String encodedPassword) {
		if(passport.getNickName()!=null){
			userForm.setNickName(passport.getNickName());
		}
		UserResultDto userResultDto = userDetailService.saveUserReg(ip, userForm,encodedPassword);
		if(userResultDto != null && userResultDto.getUser() !=null){
			User user = userResultDto.getUser();
			passport.setUserId(user.getId());//保存绑定关系
			passport.setUserName(user.getName());
			savePassport(passport);
		}
		return userResultDto;
	}

	@Override
	public boolean isAccountBind(String userName, String type) {
		return passportDao.isAccountBind(userName,type);
	}

	@Override
	public Passport getUserIdByOpenIdForUpdate(String type, String openId) {
		return passportDao.getUserIdByOpenIdForUpdate(type, openId);
	}

	@Override
	public int bindingUser(Passport passport) {
		return passportDao.bindingUser(passport);
	}


	@Override
	public IdPassord validateMobilePassword(String mobile, String password) {
		return passportDao.validateMobilePassword(mobile, password);
	}

	@Override
	public boolean isBindWeixinAccount(String userId) {
		Passport passport = passportDao.getPassportByBindingUserId(userId);
		if(passport==null){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public void unbindWeixinAccount(String userId) {
		Passport passport = passportDao.getPassportByBindingUserId(userId);
		passport.setBindingUserId(null);
		passport.setCurrentUserId(null);
		passportDao.updatePassport(passport);
	}

	@Override
	public boolean isExistPassport(String userId, String type, String source) {
		
		return false;
	}

	@Override
	public PassportFull getPassportByOpenId(String openid) {

		return passportDao.getPassportByOpenId(openid);
	}

	@Override
	public Passport getPassportByUnionid(String unionid) {
		
		return passportDao.getPassportByUnionid(unionid);
	}

	@Override
	public PassportFull generatePassport(ThirdUserInfo thridUserInfo, String passportType,
			String passportSource, Passport passport) {
		
		//如果没有已存在passport, 则需要创建
		if(null == passport){
			passport = new Passport();
			passport.setUnionid(thridUserInfo.getUnionid());
			passport.setNickName(thridUserInfo.getNickName());
			passport.setGender(thridUserInfo.getGender());
			passport.setCountry(thridUserInfo.getCountry());
			passport.setProvince(thridUserInfo.getProvince());
			passport.setCity(thridUserInfo.getCity());
			passport.setHeadPortraitUrl(thridUserInfo.getAvatarUrl());
			passport.setProps(null);
			passport.setType(passportType);
			
			Long passportId = passportDao.savePassport(passport);
			passport.setPassPortId(passportId);
		}else{//否则更新一下
			passport.setNickName(thridUserInfo.getNickName());
			passport.setGender(thridUserInfo.getGender());
			passport.setCountry(thridUserInfo.getCountry());
			passport.setProvince(thridUserInfo.getProvince());
			passport.setCity(thridUserInfo.getCity());
			passport.setHeadPortraitUrl(thridUserInfo.getAvatarUrl());
			passportDao.updatePassport(passport);
		}
		
		//在创建子通行证
		PassportSub passportSub = new PassportSub();
		passportSub.setPassportId(passport.getPassPortId());
		passportSub.setAccessToken(thridUserInfo.getAccessToken());
//		passportSub.setRefreshToken(t);
//		passportSub.setExpiresIn(expiresIn);
//		passportSub.setValidateTime(validateTime);
		passportSub.setOpenId(thridUserInfo.getOpenId());
		passportSub.setSource(passportSource);
		passportSub.setCreateTime(new Date());
		Long id = passportSubDao.savePassportSub(passportSub);
		passportSub.setId(id);
		
		PassportFull passportFull = new PassportFull(passport, passportSub);
		return passportFull;
	}

	@Override
	public PassportFull generatePassport(ThirdUserInfo thridUserInfo, String passportType,
			String passportSource) {
		
		return this.generatePassport(thridUserInfo, passportType, passportSource, null);
	}

	@Override
	public PassportFull mergePassport(PassportFull passportFull, String unionid) {
		
		Passport unionidPassport = passportDao.getPassportByUnionid(unionid);
		if(null == unionidPassport){//如果为空则代表不需要合并
			return passportFull;
		}
		
		Passport currPassport = passportDao.getPassport(passportFull.getPassPortId());
		
		//默认是将当前的passport合并到unionid的passport
		Passport fromPassport  = currPassport;
		Passport toPassport  = unionidPassport;
		
		//如果当前的passport是已经关联用户的了, 则以关联用户的为准
		if(AppUtils.isNotBlank(currPassport.getUserId())){
			toPassport  = currPassport;
			fromPassport = unionidPassport;
		}else{//说明还是当前的passport合并到unionid的passport
			
			//应为当前passport含有的用户资料时最新的, 所以要覆盖掉unionid的passport的用户信息
			toPassport.setNickName(fromPassport.getNickName());
			toPassport.setGender(fromPassport.getGender());
			toPassport.setCountry(fromPassport.getCountry());
			toPassport.setProvince(fromPassport.getProvince());
			toPassport.setCity(fromPassport.getCity());
			toPassport.setHeadPortraitUrl(fromPassport.getHeadPortraitUrl());
			
			passportFull.setPassPortId(toPassport.getPassPortId());
		}
		
		passportSubDao.updatePassportIdByPassportId(toPassport.getPassPortId(), fromPassport.getPassPortId());
		passportDao.deletePassport(fromPassport);
		
		return passportFull;
	}

	@Override
	public void updatePassportFull(PassportFull passportFull) {
		
		PassportSub passportSub = passportSubDao.getPassportSub(passportFull.getId());
		
		passportSub.setAccessToken(passportFull.getAccessToken());
		passportSubDao.updatePassportSub(passportSub);
		
		Passport passport = passportDao.getPassport(passportFull.getPassPortId());
		passport.setNickName(passportFull.getNickName());
		passport.setGender(passportFull.getGender());
		passport.setCountry(passportFull.getCountry());
		passport.setProvince(passportFull.getProvince());
		passport.setCity(passportFull.getCity());
		passport.setHeadPortraitUrl(passportFull.getHeadPortraitUrl());
		passportDao.updatePassport(passport);
	}

	@Override
	public PassportFull getPassportFullById(Long passportId) {
		
		return passportDao.getPassportFullById(passportId);
	}

	@Override
	public int bindUser(Long passPortId, String userId, String userName) {
		
		return passportDao.bindUser(passPortId, userId, userName);
	}

	@Override
	public PassportFull getPassportFullByUserIdAndTypeAndSource(String userId, String type, String source) {
		return passportDao.getPassportFullByUserIdAndTypeAndSource(userId, type, source);
	}
}
