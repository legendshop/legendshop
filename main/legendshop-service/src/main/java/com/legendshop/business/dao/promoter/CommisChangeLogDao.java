/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.promoter.model.CommisChangeLog;

/**
 * 佣金变更历史Dao
 */
public interface CommisChangeLogDao extends Dao<CommisChangeLog, Long> {

	/** 获取佣金变更历史 */
	public abstract CommisChangeLog getCommisChangeLog(Long id);

	/** 删除佣金变更历史 */
	public abstract int deleteCommisChangeLog(CommisChangeLog commisChangeLog);

	 /** 保存佣金变更历史 */
	public abstract Long saveCommisChangeLog(CommisChangeLog commisChangeLog);

	 /** 更新佣金变更历史 */
	public abstract int updateCommisChangeLog(CommisChangeLog commisChangeLog);

	// public abstract PageSupport getCommisChangeLog(CriteriaQuery cq);

	 /** 获取佣金变更历史 */
	public abstract CommisChangeLog getCommisChangeLog(String userName, String sn);

	/** 获取佣金变更历史 */
	public abstract PageSupport<CommisChangeLog> getCommisChangeLogByCq(String curPageNO);

	/** 获取佣金变更历史 */
	public abstract PageSupport<CommisChangeLog> getCommisChangeLog(int pageSize, String userId);

	/** 获取佣金变更历史 */
	public abstract PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO, int pageSize, String userId, AwardSearchParamDto paramDto);

	/**
	 * 获取用户的佣金收入明细
	 */
	PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO, int pageSize, String userId);

}
