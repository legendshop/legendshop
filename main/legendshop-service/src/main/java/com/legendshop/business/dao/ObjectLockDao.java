/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.constant.ObjectLockEnum;
import com.legendshop.model.entity.ObjectLock;

/**
 * 对象行锁.
 */

public interface ObjectLockDao extends Dao<ObjectLock, Long> {

	/**
	 * 加载行锁
	 * @return
	 */
	public Long loadDBLock(ObjectLockEnum prod, Long prodId);
	
	/**
	 * 取消行锁
	 * @return
	 */
	public void releaseDBLock(ObjectLockEnum prod, Long prodId);
 }
