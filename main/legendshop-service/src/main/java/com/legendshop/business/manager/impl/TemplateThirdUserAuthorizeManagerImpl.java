/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.ThirdUserToken;
import com.legendshop.spi.manager.ThirdUserAuthorizeManager;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.ThirdLoginService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 第三方用户授权认证管理的模板方法
 * @author 开发很忙
 */
@Service
public abstract class TemplateThirdUserAuthorizeManagerImpl implements ThirdUserAuthorizeManager {
	
	/** 缓存的前缀 */
	protected static final String CACHE_PREFIX = "THRID_USER_AUTHORIZE";
	
	/** 单位是秒, 默认5分钟 */
	protected static final int STATE_VALID_TIME = 5 * 60;
	
	@Autowired
	protected CacheClient cacheClient;
	
	@Autowired
	protected PassportService passportService;
	
	@Autowired
	protected UserDetailService userDetailService;
	
	@Autowired
	protected ThirdLoginService thirdLoginService;
	
	@Override
	public final String constructAuthorizeUrl(String source) {
		
		// 1.构建授权url
		String url = authorize(source);
		
		return url;
	}
	
	/**
	 * 生成state, 并放入redis, 用于callback回来验证
	 * @return
	 */
	protected String gennerateState(){
	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timestamp = sdf.format(new Date());
		String nonce = RandomStringUtils.randomNumeric(7);
		
		String state = DigestUtils.sha1Hex(timestamp + nonce);
		cacheClient.put(CACHE_PREFIX + ":" + state, STATE_VALID_TIME, state);
		
		return state;
	}
	
	/**
	 * 构造授权url
	 * @param param
	 * @return
	 */
	protected abstract String authorize(String source);
	
	
	/** -------------------------------------- 华丽分割线, 下面是关于callback的算法架构 ----------------------------------- */

	@Override
	public final ThirdUserAuthorizeResult authorizeCallback(String code, String state, String source) {
		
		// 1.验证state是否合法
		if(!verifyState(state)){
			return new ThirdUserAuthorizeResult("state 不合法!");
		}
		
		// 2.验证code
		if(!verifyCode(code)){
			return new ThirdUserAuthorizeResult("用户拒绝了授权!");
		}
		
		// 2.通过code换取accessToken以及refreshToken
		ThirdUserToken token = getAccessToken(code, source);
		if(null == token){
			return new ThirdUserAuthorizeResult("accessToken获取失败!");
		}
		
		// 3.通过accessToken获取用户基本资料
		ThirdUserInfo thirdUserInfo = getUserInfo(token);
		
		// 4.对第三方用户进行认证
		ThirdUserAuthorizeResult result = authThirdUser(thirdUserInfo, source);
		
		if(result.isNeedBindAccount()){
			String passportIdKey = thirdLoginService.cachePassportID(result.getPassport().getId());
			result.setPassportIdKey(passportIdKey);
		}
		
		return result;
	}
	
	@Override
	public final ThirdUserAuthorizeResult appAuthorizeCallback(ThirdUserInfo thirdUserInfo) {
		
		if(AppUtils.isBlank(thirdUserInfo.getOpenId())){
			return new ThirdUserAuthorizeResult("openId 无效!");
		}
		
		// 2.对第三方用户进行认证
		ThirdUserAuthorizeResult result = authThirdUser(thirdUserInfo, PassportSourceEnum.APP.value());
		if(result.isNeedBindAccount()){
			String passportIdKey = thirdLoginService.cachePassportID(result.getPassport().getId());
			result.setPassportIdKey(passportIdKey);
		}
		return result;
	}
	
	@Override
	public final ThirdUserAuthorizeResult mpAuthorizeCallback(String code, String encryptedData, String iv) {
		
		// 1.获取第三方用户信息
		ThirdUserInfo thirdUserInfo = getMpUserInfo(code, encryptedData, iv);
		
		// 2.对第三方用户进行认证
		ThirdUserAuthorizeResult result = authThirdUser(thirdUserInfo, PassportSourceEnum.MP.value());
		
		if(result.isNeedBindAccount()){
			String passportIdKey = thirdLoginService.cachePassportID(result.getPassport().getId());
			result.setPassportIdKey(passportIdKey);
		}
		
		return result;
	}
	
	/**
	 * 验证state
	 * @return
	 */
	protected boolean verifyState(String state){
		
		
		if(AppUtils.isBlank(state)){
			return false;
		}
		
		Object stateObj = cacheClient.get(CACHE_PREFIX + ":" + state);
		if(null == stateObj){
			return false;
		}
		String _state = (String) stateObj;
		
		return _state.equals(state);
	}
	
	/**
	 * 验证code
	 * @return
	 */
	protected boolean verifyCode(String code){
		return AppUtils.isNotBlank(code);
	}
	
	/**
	 * 通过code换取AccessToken
	 * @param code
	 * @param source 来源
	 * @return
	 */
	protected abstract ThirdUserToken getAccessToken(String code, String source);
	
	/**
	 * 通过token获取第三方用户信息
	 * @param token
	 * @return
	 */
	protected abstract ThirdUserInfo getUserInfo(ThirdUserToken token);
	
	/**
	 * 通过code获取第三方小程序用户信息
	 * @param code
	 * @param encryptedData
	 * @param iv
	 * @return
	 */
	protected abstract ThirdUserInfo getMpUserInfo(String code, String encryptedData, String iv);
	
	/**
	 * 第三方用户认证
	 * @param thirdUserInfo 第三方用户信息
	 * @param source 来源, 哪个端
	 * @return
	 */
	protected abstract ThirdUserAuthorizeResult authThirdUser(ThirdUserInfo thirdUserInfo, String source);
}
