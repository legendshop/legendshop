/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.StockLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.StockLog;

/**
 *库存历史服务Dao
 */
@Repository
public class StockLogDaoImpl extends GenericDaoImpl<StockLog, Long> implements StockLogDao  {

	public StockLog getStockLog(Long id){
		return getById(id);
	}
	
    public int deleteStockLog(StockLog stockLog){
    	return delete(stockLog);
    }
	
	public Long saveStockLog(StockLog stockLog){
		return save(stockLog);
	}
	
	public int updateStockLog(StockLog stockLog){
		return update(stockLog);
	}

	@Override
	public void saveStockLogs(List<StockLog> stockLogs) {
		if(stockLogs == null){
			return;
		}
		 save(stockLogs);
	}

	@Override
	public void deleteStockLogById(String skuIdList) {
		this.update("delete from ls_stock_log where sku_id in (" + skuIdList + ")");
	}

	@Override
	public PageSupport<StockLog> loadStockLog(Long prodId, Long skuId, String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(StockLog.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("prodId", prodId);
		cq.eq("skuId", skuId);
		cq.addDescOrder("updateTime");
		return queryPage(cq);
	}
 }
