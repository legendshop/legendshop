package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CategoryDao;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.api.NavigationCategory;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.NavigationCategoryApiService;
import com.legendshop.util.AppUtils;

/**
 * 首页导航条服务
 *
 */
@Service("navigationCategoryApiService")
public class NavigationCategoryApiServiceImpl implements NavigationCategoryApiService {
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	@Cacheable(value="NavigationCategoryList")
	public List<NavigationCategory> findNavigationCategory() {
		List<Category> list=categoryDao.getCategory(ProductTypeEnum.PRODUCT.value(),null, null, 1,null);
		if(AppUtils.isBlank(list)){
			return null;
		}
		
		if(AppUtils.isBlank(list)){
		   return null;	
		}
		List<NavigationCategory> navigationCategories=new ArrayList<NavigationCategory>();
		for (int i = 0; i < list.size(); i++) {
			Category category=list.get(i);
			NavigationCategory navigationCategory=buildNavigationCategory(category);
			if(navigationCategories!=null){
				navigationCategories.add(navigationCategory);
			}
		}
		if(AppUtils.isBlank(navigationCategories)){
			 return null;	
		}
		List<NavigationCategory> categories=null;
		try {
			categories=parseMenu(navigationCategories);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return categories;
	}
	

	private NavigationCategory buildNavigationCategory(Category category) {
		if(category==null || category.getStatus()==0){
			return null;
		}
		if(category.getGrade()==1 &&!category.getHeaderMenu()){
			return null;
		}
		NavigationCategory nav=new NavigationCategory();
		nav.setId(category.getId());
		nav.setLevel(category.getGrade());
		nav.setName(category.getName());
		nav.setParentId(category.getParentId());
		return nav;
	}


	/**
	 * 按照3级菜单来组装
	 *
	 */
	private List<NavigationCategory> parseMenu(List<NavigationCategory> menuList) {
		Map<Long, NavigationCategory> menuMap = new LinkedHashMap<Long, NavigationCategory>();
		for (NavigationCategory menu : menuList) {
			if(AppUtils.isBlank(menu)){
				continue;
			}
			if (menu.getLevel() == 1) { // for 顶级菜单
				menuMap.put(menu.getId(), menu);
			} else if (menu.getLevel() == 2) { // 二级菜单
				// 拿到一级菜单先
				NavigationCategory menuLevel1 = menuMap.get(menu.getParentId());
				if(AppUtils.isBlank(menuLevel1)){
					continue;
				}
				menu.setParentNode(menuLevel1);
				menuLevel1.addChildNode(menu);
			} else if (menu.getLevel() == 3) { // 三级菜单
				// 拿到二级菜单
				NavigationCategory secondMenu = getParentMenu(menuList, menu);
				if (secondMenu != null) {
				    menu.setParentNode(secondMenu);
					// 拿到一级菜单先
					NavigationCategory menuLevel1 = menuMap.get(secondMenu.getParentId());
					if (menuLevel1 == null) {
						 continue; // 可能是由于上下线的关系
					}
					List<NavigationCategory> menuLevel2 = menuLevel1.getChildrenList();
					for (NavigationCategory menu2 : menuLevel2) {
						if (menu2.getId().equals(menu.getParentId())) {
							// 找到对应的二级菜单
							menu2.addChildNode(menu);
							break;
						}
					}
				}
			}
		}
		return new ArrayList<NavigationCategory>(menuMap.values());
	}
	
	/**
	 * 得到父节点
	 * @param menuList
	 * @param menu
	 * @return
	 */
	private NavigationCategory getParentMenu(Collection<NavigationCategory> menuList, NavigationCategory menu) {
		for (NavigationCategory item : menuList) {
			if(item==null){
				continue;
			}
			if (item.getId().equals(menu.getParentId())) {
				return item;
			}
		}
		return null;
	}


	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}
	
	

	
}
