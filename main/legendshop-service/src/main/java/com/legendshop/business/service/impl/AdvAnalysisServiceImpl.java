package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AdvAnalysisDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdvAnalysis;
import com.legendshop.spi.service.AdvAnalysisService;

/**
 * 广告统计
 * @author zs
 *
 */
@Service("advAnalysisService")
public class AdvAnalysisServiceImpl implements AdvAnalysisService{
	
	@Autowired
	private AdvAnalysisDao advAnalysisDao;

	@Override
	public Long save(AdvAnalysis advAnalysis) {
		return advAnalysisDao.saveAdvAnalysis(advAnalysis);
	}


	@Override
	public PageSupport<AdvAnalysis> query(String curPageNO, String url) {
		return advAnalysisDao.query(curPageNO, url);
	}

	@Override
	public PageSupport<AdvAnalysis> groupQuery(String userId, String curPageNO, String url) {
		return advAnalysisDao.groupQuery(userId, curPageNO, url);
	}

}
