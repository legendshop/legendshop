/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.ShopDetailUpdate;
import com.legendshop.business.dao.LogoDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.entity.ShopDetail;

/**
 * 系统logoDao.
 */
@Repository
public class LogoDaoImpl implements LogoDao {

	@Autowired
	private GenericJdbcDao genericJdbcDao;

	@Override
	@ShopDetailUpdate
	public void deleteLogo(ShopDetail shopDetail) {
		updateLogo(shopDetail.getUserId(), shopDetail.getUserName(), null);
	}

	@Override
	@ShopDetailUpdate
	public void updateLogo(ShopDetail shopDetail) {
		updateLogo(shopDetail.getUserId(), shopDetail.getUserName(), shopDetail.getLogoPic());
	}

	private void updateLogo(String userId, String userName, String logoPic) {
		genericJdbcDao.update("update ls_shop_detail set logo_pic = ? where user_id = ?", logoPic, userId);
	}

}
