/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.store.StoreOrder;

/**
 * The Class DeliveryOrderDao.
 */

public interface StoreOrderDao  extends GenericDao<StoreOrder, Long> {

	public abstract StoreOrder getDeliveryOrder(Long id);
	
    public abstract int deleteDeliveryOrder(StoreOrder deliveryOrder);
	
	public abstract Long saveDeliveryOrder(StoreOrder deliveryOrder);
	
	public abstract int updateDeliveryOrder(StoreOrder deliveryOrder);
	
	public abstract ShopCartItem findStoreOrder(long storeId, long prodId, long skuId);

	public abstract int addHold(Long storeId, Long prodId, Long skuId, int count);

	public abstract PageSupport getStorePageSupport(QueryMap queryMap,List<Object> args,Integer curPageNO,Integer pageSize);

	public abstract StoreOrder getDeliveryOrderBySubNumber(String subNumber);

	public abstract int updateDeliveryOrderStatus(String subNumber,Integer status);

	/**
	 * 店铺订单取消
	 * @param subNumber
	 * @param userId
	 * @return
	 */
	public abstract long cancleOrder(String subNumber, String userId);

	
 }
