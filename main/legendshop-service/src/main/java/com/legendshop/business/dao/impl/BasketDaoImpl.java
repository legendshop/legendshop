/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.BasketDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.Basket;
import com.legendshop.util.AppUtils;

/**
 * 购物车Dao.
 * @author Tony
 *
 */
@Repository
public class BasketDaoImpl extends GenericDaoImpl<Basket, Long> implements BasketDao {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(BasketDaoImpl.class);

	@Override
	public void deleteBasketById(Long basketId) {
		deleteById(basketId);
	}

	/**
	 * 获取用户的购物车
	 */
	@Override
	public List<BasketDto> getBasketByUserId(String userId) {
		if(AppUtils.isBlank(userId)){
			return null;
		}
		String sql=ConfigCode.getInstance().getCode("basket.getBasketByUserId");
		List<BasketDto>  basketDtos=query(sql, new Object[]{userId}, new RowMapper<BasketDto>() {
			@Override
			public BasketDto mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				BasketDto basketDto=new BasketDto();
				basketDto.setBasketId(rs.getLong("basketId"));
				basketDto.setBasketCount(rs.getInt("basketCount"));
				basketDto.setProdId(rs.getLong("prodId"));
				basketDto.setPic(rs.getString("pic"));
				basketDto.setCarriage(rs.getDouble("carriage"));
				basketDto.setShopName(rs.getString("shopName"));
				basketDto.setShopId(rs.getLong("shopId"));
				basketDto.setSkuId(rs.getLong("skuId"));
				basketDto.setStocks(rs.getInt("stocks"));
				basketDto.setStatus(rs.getInt("status"));
				StringBuffer buffer=new StringBuffer(rs.getString("productName"));
				if(AppUtils.isNotBlank(rs.getString("cnProperties"))){
					buffer.append("-").append(rs.getString("cnProperties"));
				}
				basketDto.setProductName(buffer.toString());
				basketDto.setPrice(rs.getDouble("price"));
				basketDto.setCash(rs.getDouble("cash"));
				basketDto.setVolume(rs.getDouble("volume"));
				basketDto.setWeight(rs.getDouble("weight"));
				return basketDto;
			}
		});
		return basketDtos;
		
		
	}

	// 得到有效订单总数
	@Override
	public Long getTotalBasketByUserId(String userId) {
		String sql=ConfigCode.getInstance().getCode("basket.getTotalBasketByUserId");
		return getLongResult(sql,  userId);
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.BasketDao#getBasketById(java.lang.Long)
	 */
	@Override
	public Basket getBasketById(Long id) {
		return getById(id);

	}
	
	/**
	 * 根据商品和SKUID和用户ID找出对应的购物车
	 */
	@Override
	public Basket getBasketByUserId(Long prodId, String  userId, Long sku_id) {
		EntityCriterion criterion = new EntityCriterion().eq("prodId", prodId).eq("userId", userId);
		if(sku_id!=null){
			criterion.eq("skuId", sku_id);
		}
		return getByProperties(criterion);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.BasketDao#saveBasket(com.legendshop.
	 * model.entity.Basket)
	 */
	@Override
	public Long saveBasket(Basket basket) {
		return (Long) save(basket);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.BasketDao#updateBasket(com.legendshop
	 * .model.entity.Basket)
	 */
	@Override
	public void updateBasket(Basket basket) {
		update(basket);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.BasketDao#getBasket(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<Basket> getBasket(String prodId, String userName) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId).eq("userName", userName));
	}

	/**
	 * 移除购物车
	 * 
	 * @see
	 * com.com.legendshop.business.dao.BasketDao#deleteBasketByUserName(java
	 * .lang.String)
	 */
	@Override
	public void deleteBasketByUserId(String userId) {
		List<Basket> list = queryByProperties(new EntityCriterion().eq("userId", userId));
		delete(list);
	}

	/**
	 * 加入购物车.在购物车中的产品并不会影响库存量，如果在下单时库存不足，则会造成无法下单。
	 * @return 
	 *
	 */
	@Override
	public Long saveToCart(String userId, Long prodId, Integer basketCount, Long skuId,Long shopId,String distUserName,Long storeId) {
		//查找购物车是否有同样的商品
		Basket basket = getBasketByUserId(prodId, userId, skuId);
		if (basket == null) {
			Basket b = new Basket();
			b.setProdId(prodId);
			b.setSkuId(skuId);
			b.setBasketCount(basketCount);
			b.setBasketDate(new Date());
			b.setShopId(shopId);
			b.setDistUserName(distUserName);
			b.setUserId(userId);
			b.setStoreId(storeId);
			return save(b);
		} else {
			//购物车商品过多时，加入失败
			if(basket.getBasketCount()>=9999){
				return -1l;
			}else{
				String sql = "";
				if(basket.isFailure()){
					sql = ConfigCode.getInstance().getCode("basket.resaveToCart");
				}else{
					sql = ConfigCode.getInstance().getCode("basket.saveToCart");
				}
				update(sql,basketCount,storeId,userId,basket.getBasketId());
			    return basket.getBasketId();
			}
		}
	}


	/**
	 * 删除商城购物车相关的记录
	 */
	@Override
	public void deleteBasket(Long shopId) {
		update("delete from ls_basket where shop_id = ?", shopId);
	}

	@Override
	public void deleteUserBasket(String userId) {
		update("delete from ls_basket  where user_id = ?", userId);
	}

	@Override
	public void deleteBasketById(Long basketId, String userId) {
		log.debug("deleteBasketById, basketId = {}", basketId);
		jdbcTemplate.update("delete from ls_basket where basket_id=? and user_id=?", basketId,userId);
	}


	@Override
	public void updateShopCartNumber(Long basket_id, String userId,
			Integer num) {
		jdbcTemplate.update("update ls_basket set basket_count=? where basket_id=? and user_id=?",num,basket_id,userId);
	}
	
	@Override
	public List<ShopCartItem> loadBasketBySkuIds(Long[] skuIds) {
		StringBuilder builder=new StringBuilder();
		List<Object> params=new ArrayList<Object>();
		for (int i = 0; i < skuIds.length; i++) {
			builder.append("?,");
			params.add(skuIds[i]);
		}
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids);
		String sql=ConfigCode.getInstance().getCode("basket.loadBasketBySkuIds",queryMap);
		List<ShopCartItem> cartItem=query(sql,params.toArray(), new CartProdItemRowMapper());
		return cartItem;
	}
	
	@Override
	public List<ShopCartItem> loadBasketByIds(String userId) {
		String sql=ConfigCode.getInstance().getCode("basket.getShopCartItemByUserId");
		List<ShopCartItem> cartItem=query(sql,new Object[] { userId }, new ShopCartItemRowMapper());
		return cartItem;
	}
	
	
	/**
	 * 获取秒杀的商品购物车信息
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@Override
	public ShopCartItem findSeckillCart(Long seckillId,Long prodId,Long skuId) {
		String sql = ConfigCode.getInstance().getCode("seckill.findSeckillCart");
		ShopCartItem cartItem= get(sql, new Object[]{seckillId,prodId,skuId}, new RowMapper<ShopCartItem>(){
			@Override
			public ShopCartItem mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ShopCartItem cartItem=new ShopCartItem();
				cartItem.setBasketId(rs.getLong("secId"));
				cartItem.setBasketCount(rs.getInt("seckillNum")); //中标数量
				cartItem.setCalculateMarketing(false); //不计算营销活动
				convertShopCartItem(cartItem,rs);
				cartItem.setPromotionPrice(rs.getDouble("seckillPrice"));
				cartItem.setPrice(rs.getDouble("seckillPrice"));
				return cartItem;
			}
		});
		return cartItem;
	}
	
	@Override
	public ShopCartItem findBiddersWinCart(Long paimaiId, Long prodId,
			Long skuId) {
		String sql=ConfigCode.getInstance().getCode("bidders.findBiddersWinCart"); 
		ShopCartItem cartItem=get(sql,new Object[]{paimaiId,prodId,skuId}, new RowMapper<ShopCartItem>(){
			@Override
			public ShopCartItem mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ShopCartItem cartItem=new ShopCartItem();
				cartItem.setBasketId(rs.getLong("basketId"));
				convertShopCartItem(cartItem,rs);
				return cartItem;
			}
		});
		return cartItem;
	}
	
	/**
	 * 查询团购的购物车信息
	 * 团购没有购物车，直接查找团购商品
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @return
	 */
	@Override
	public ShopCartItem findGroupCart(Long groupId, Long productId, Long skuId) {
		//查找团购商品
		String sql=ConfigCode.getInstance().getCode("group.findGroupCart");
		ShopCartItem cartItem=get(sql,new Object[] { groupId,productId,skuId,skuId}, new  RowMapper<ShopCartItem>(){
			@Override
			public ShopCartItem mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ShopCartItem cartItem=new ShopCartItem();
				cartItem.setBasketId(rs.getLong("groupId"));
				cartItem.setCalculateMarketing(false); //不计算营销活动
				convertShopCartItem(cartItem,rs);
				return cartItem;
			}
		});
		return cartItem;
	}
	
		
	/**
	 * 根据购物车ID获取对应的商品和SKU属性
	 */
	@Override
	public List<ShopCartItem> loadBasketByIds(List<Long> basketIds,String userId) {
		StringBuilder builder=new StringBuilder(100);
		Object[] params = new Object[basketIds.size()+1];
		params[0]=userId;
		for (int i = 1; i <= basketIds.size(); i++) {
			builder.append("?,");
			params[i]=basketIds.get(i-1);
		}
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids);
		String sql=ConfigCode.getInstance().getCode("basket.loadBasketByBasketIds",queryMap);
		List<ShopCartItem> cartItem=query(sql,params, new ShopCartItemRowMapper());
		return cartItem;
	}
	
	
	@Override
	public List<ShopCartItem> loadBasketByIds(List<Long> basketIds,
			String userId,
			Long shopId) {
		StringBuilder builder=new StringBuilder(100);
		Object[] params=new Object[basketIds.size()+2];
		params[0]=userId;
		for (int i = 1; i <= basketIds.size(); i++) {
			builder.append("?,");
			params[i]=basketIds.get(i-1);
		}
		params[params.length-1]=shopId;
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids);
		String sql=ConfigCode.getInstance().getCode("basket.loadBasketByBasketIds_2",queryMap);
		List<ShopCartItem> cartItem=query(sql,params, new ShopCartItemRowMapper());
		return cartItem;
	}
	
	
	class ShopCartItemRowMapper implements RowMapper<ShopCartItem> {
		@Override
		public ShopCartItem mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			ShopCartItem cartItem =new ShopCartItem();
			cartItem.setBasketId(rs.getLong("basketId"));
			cartItem.setBasketCount(rs.getInt("basketCount"));
			cartItem.setDistUserName(rs.getString("distUserName"));
			convertShopCartItem(cartItem,rs);
			cartItem.setIsFailure(rs.getBoolean("isFailure"));
			return cartItem;
		}
	}

	class CartProdItemRowMapper implements RowMapper<ShopCartItem> {
		@Override
		public ShopCartItem mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			ShopCartItem cartItem =new ShopCartItem();
			cartItem.setBasketId(0l);
			cartItem.setBasketCount(0);
			convertShopCartItem(cartItem,rs);
			return cartItem;
		}
	}
	
	private void convertShopCartItem(ShopCartItem cartItem, ResultSet rs) throws SQLException {

		cartItem.setProdId(rs.getLong("prodId"));
		cartItem.setSkuId(rs.getLong("skuId"));
		cartItem.setShopId(rs.getLong("shopId"));
		cartItem.setPrice(rs.getDouble("price"));
		cartItem.setPromotionPrice(rs.getDouble("price"));
		if(AppUtils.isNotBlank(rs.getString("skuPic"))){
			cartItem.setPic(rs.getString("skuPic"));
		}else{
			cartItem.setPic(rs.getString("prodPic"));
		}
		if(AppUtils.isNotBlank(rs.getString("skuName"))){
			cartItem.setProdName(rs.getString("skuName"));
		}else{
			cartItem.setProdName(rs.getString("productName"));
		}
		cartItem.setStatus(rs.getInt("status"));
		cartItem.setStockCounting(rs.getInt("stockCounting"));
		cartItem.setStocks(rs.getInt("stocks"));
		cartItem.setActualStocks(rs.getInt("actualStocks"));
		cartItem.setVolume(rs.getDouble("volume"));
		cartItem.setWeight(rs.getDouble("weight"));
		cartItem.setTransportId(rs.getLong("transportId")==0?null:rs.getLong("transportId"));
		cartItem.setTransportFree(rs.getInt("supportTransportFree"));
		cartItem.setTransportType(rs.getInt("transportType"));
		cartItem.setEmsTransFee(rs.getFloat("emsTransFee"));
		cartItem.setExpressTransFee(rs.getFloat("expressTransFee"));
		cartItem.setMailTransFee(rs.getFloat("mailTransFee"));
		cartItem.setTransportFree(rs.getInt("supportTransportFree"));
		cartItem.setSupportCod(rs.getBoolean("isSupportCod"));
		cartItem.setIsGroup(rs.getInt("isGroup"));
		cartItem.setCnProperties(rs.getString("cnProperties"));
		cartItem.setSupportDist(rs.getInt("supportDist"));
		cartItem.setDistCommisRate(rs.getDouble("distCommisRate"));
		cartItem.setFirstLevelRate(rs.getDouble("firstLevelRate"));
		cartItem.setSecondLevelRate(rs.getDouble("secondLevelRate"));
		cartItem.setThirdLevelRate(rs.getDouble("thirdLevelRate"));
		cartItem.setCheckSts(rs.getInt("checkSts"));
		
		boolean isExis = isExistColumn(rs, "storeId");
		if (isExis) {
			cartItem.setStoreId(rs.getLong("storeId"));
		}
		//此段代码有些问题，后面需要改动 TODO  要改造 
		/*try{
			if(AppUtils.isNotBlank(rs.getBoolean("isFailure"))){
				cartItem.setIsFailure(rs.getBoolean("isFailure"));
				if(cartItem.getIsFailure()){
					cartItem.setCheckSts(0);
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}*/
	}
	
	@Override
	public int getBasketCountByUserId(String userId) {
		return get("select sum(basket_count) from ls_basket where user_id=? and is_failure = 0 ",Integer.class,userId);
	}

	/**
	 * 更新购物车数量
	 */
	@Override
	public void updateBasket(Long productId, Long skuId, Integer basketCount, String userId) {
		jdbcTemplate.update("update ls_basket set basket_count=? ,check_sts=1 where prod_id=? and sku_id=? and  user_id=?",basketCount,productId,skuId,userId);
	}

	/**
	 * 删除购物车
	 */
	@Override
	public void deleteBasketById(Long productId, Long skuId, String userId) {
		jdbcTemplate.update("delete from ls_basket  where prod_id=? and sku_id=? and  user_id=?",productId,skuId,userId);
	}
	
	/**
	 * 通过prodId获取购物车中的商品
	 */
	@Override
	public List<Basket> getBasketProdsByProdId(Long prodId){
		return this.query("SELECT * FROM ls_basket b WHERE b.prod_id = ?",Basket.class,prodId);
	}

	/**
	 * 更新商品是否失效. 0: 否, 1:是
	 */
	@Override
	public int updateIsFailureByProdId(Boolean isFailure, Long prodId) {
		return update("UPDATE ls_basket set is_failure = ? WHERE prod_id = ?", isFailure, prodId);
	}

	/**
	 * 
	 * 批量删除购物车商品
	 */
	@Override
	public void deleteBasketByIds(String[] skuIds, String userId) {
		List<Object[]> arrayList = new ArrayList<Object[]>(skuIds.length);
		for(String skuId : skuIds){
			Object[] param = new Object[2];
			param[0] = userId;
			param[1] = skuId;
			arrayList.add(param);
		}
		this.batchUpdate("DELETE FROM ls_basket WHERE user_id =?  AND sku_id = ?", arrayList);
	}

	@Override
	public ShopCartItem findMergeGroupCart(Long mergeGroupId, Long productId, Long skuId) {
		//查找拼团商品
		String sql=ConfigCode.getInstance().getCode("basket.findMergeGroupCart");
		ShopCartItem cartItem=get(sql,new Object[] { mergeGroupId,productId,skuId }, new  RowMapper<ShopCartItem>(){
			@Override
			public ShopCartItem mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ShopCartItem cartItem=new ShopCartItem();
				cartItem.setBasketId(rs.getLong("mergeId"));
				cartItem.setCalculateMarketing(false); //不计算营销活动
				convertShopCartItem(cartItem,rs);
				return cartItem;
			}
		});
		return cartItem;
	}

	@Override
	public void clearInvalidBaskets(String userId) {
		String sql = "delete from ls_basket where user_id = ? and is_failure = 1";
		update(sql, userId);
	}

	/**
	 * 更改购物车已选门店
	 */
	@Override
	public void updateShopStore(Long productId, Long skuId, Long storeId, String userId) {
		String sql = "update ls_basket set store_id=?,check_sts=1 where prod_id=? and sku_id=? and  user_id=?";
		
		update(sql,storeId,productId,skuId,userId);
	}

	@Override
	public void batchUpdateIsFailureByIds(Boolean isFailure, List<Long> productIds) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(productIds.size());
		for (Long prodId : productIds) {
			Object[] param = new Object[2];
			param[0] = isFailure;
			param[1] = prodId;
			batchArgs.add(param);
		}
		this.batchUpdate("UPDATE ls_basket set is_failure = ? WHERE prod_id = ?", batchArgs);
	}
	
	 /**
     * 判断查询结果集中是否存在某列
     * @param rs 查询结果集
     * @param columnName 列名
     * @return true 存在; false 不存在
     */
    private boolean isExistColumn(ResultSet rs, String columnName) {
        try {
            if (rs.findColumn(columnName) > 0 ) {
                return true;
            } 
        }
        catch (SQLException e) {
            return false;
        }
        
        return false;
    }


	/**
	 * 
	 * 批量删除购物车商品
	 */
/*	@Override
	public void deleteBasketByIds(List<Long> skuIds, String userId) {
		
		StringBuilder builder = new StringBuilder(100);
		Object[] params = new Object[skuIds.size()+1];
		for(int i = 0; i < skuIds.size(); i++){
			builder.append("?,");
			params[i] = skuIds.get(i);
		}
		params[params.length-1] = userId;
		
		String ids = builder.substring(0, builder.length()-1);
		
		QueryMap queryMap = new QueryMap();
		queryMap.put("ids", ids);
		
		String sql=ConfigCode.getInstance().getCode("basket.deleteBasketByIds",queryMap);
		update(sql, params);
		
		StringBuilder builder=new StringBuilder(100);
		Object[] params=new Object[skuIds.size()+1];
		for (int i = 0; i < skuIds.size(); i++) {
			builder.append("?,");
			params[i]=skuIds.get(i);
		}
		params[params.length-1]=userId;
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids);
		String sql=ConfigCode.getInstance().getCode("basket.deleteBasketByIds",queryMap);
		update(sql, params);
	}*/
}
