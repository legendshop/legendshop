package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.TagItemNews;

public class TagItemNewsComparer implements DataComparer<TagItemNews, TagItemNews> {

	@Override
	public boolean needUpdate(TagItemNews dto, TagItemNews dbObj, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(TagItemNews dto, TagItemNews dbObj) {
		if(dto==null && dbObj==null){
		    return true;	
		}
		if(dto==null || dbObj==null){
			return false;
		}
		if(dto.getNewsId().equals(dbObj.getNewsId()) && dto.getTagItemId().equals(dbObj.getTagItemId())){
			return true;
		}
		return false;
	}

	@Override
	public TagItemNews copyProperties(TagItemNews dtoj, Object obj) {
		return dtoj;
	}


}
