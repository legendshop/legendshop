/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

/**
 * The Interface TagLibDao.
 */
public interface TagLibDao{

	/**
	 * Find by sql.
	 * 
	 * @param hql
	 *            the hql
	 * @return the list
	 */
	public List<Object[]> query(String sql);

	/**
	 * Find by sql.
	 * 
	 * @param sql
	 *            the sql
	 * @return the list
	 */
	public List<Object[]> query(String sql, Object... params);
}
