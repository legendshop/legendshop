/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.entity.MergeGroupAdd;

/**
 * The Class MergeGroupAddDao.
 * 拼团参加表Dao接口
 */
public interface MergeGroupAddDao extends Dao<MergeGroupAdd, Long> {

	/**
	 *  根据Id获取拼团参加表
	 */
	public abstract MergeGroupAdd getMergeGroupAdd(Long id);

	/**
	 *  删除拼团参加表
	 */
	public abstract int deleteMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 *  保存拼团参加表
	 */	
	public abstract Long saveMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 *  更新拼团参加表
	 */		
	public abstract int updateMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 * 查询正在进行的团列表
	 * @param mergeId 拼团活动ID
	 * @param count 查询的数量, 0或null代表查询所有
	 * @return
	 */
	public abstract List<MergeGroupingDto> getMergeGroupingByMergeId(Long mergeId, Integer count);

	//修改该团所有参团记录状态
	public abstract void updateMergeGroupAddStatus(Long operateId);

	public abstract List<String> queryUserPicByMergeGroupSub(Long mergeId, Long operateId);

	public abstract List<MergeGroupAdd> getMergeGroupAddByIds(List<Long> ids);

	public abstract List<MergeGroupAdd> findHaveInMergeGroupAdd(Long operateId);

	public abstract List<Long> findGroupExemption();

	/**
	 * 查询团下面的已参团列表
	 * @param operateId 团ID
	 * @param count 查询数量, 为空或0代表不限制
	 * @return
	 */
	public abstract List<MergeGroupAdd> queryMergeGroupAddList(Long operateId, Integer count);

	/**
	 * 根据团Id和用户ID查询参团记录
	 * @param operateId
	 * @param userId
	 * @return
	 */
	public abstract MergeGroupAdd getMergeGroupAddByOperateIdAndUserId(Long operateId, String userId);

	/**
	 * 判断订单是否为拼团的团长免单订单
	 * @param subId
	 * @return
	 */
	MergeGroupAdd isMergeGroupHeadFreeSub(Long subId);
}
