/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.business.dao.GrassThumbDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassThumb;
import org.springframework.stereotype.Repository;

/**
 * The Class GrassThumbDaoImpl. 种草文章点赞表Dao实现类
 */
@Repository
public class GrassThumbDaoImpl extends GenericDaoImpl<GrassThumb, Long> implements GrassThumbDao{

	/**
	 * 根据Id获取种草文章点赞表
	 */
	public GrassThumb getGrassThumb(Long id){
		return getById(id);
	}

	/**
	 *  删除种草文章点赞表
	 *  @param grassThumb 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassThumb(GrassThumb grassThumb){
    	return delete(grassThumb);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassThumb(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草文章点赞表
	 */
	public Long saveGrassThumb(GrassThumb grassThumb){
		return save(grassThumb);
	}

	/**
	 *  更新种草文章点赞表
	 */		
	public int updateGrassThumb(GrassThumb grassThumb){
		return update(grassThumb);
	}

	/**
	 * 分页查询种草文章点赞表列表
	 */
	public PageSupport<GrassThumb>queryGrassThumb(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassThumb.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public Integer rsThumb(Long grassId, String uid) {
	   String sql="select count(1) from ls_app_grass_thumb where gra_id=? and user_id=?";
	   return get(sql, Integer.class, grassId,uid);
	}

	@Override
	public void deleteGrassThumbByDiscoverId(Long grassId, String uid) {
		String sql="DELETE FROM ls_app_grass_thumb WHERE gra_id=? AND user_id=?";
		update(sql, grassId,uid);
	}

}
