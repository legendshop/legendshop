/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PubDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Pub;
import com.legendshop.spi.service.PubService;
import com.legendshop.util.AppUtils;

/**
 * 公告服务
 */
@Service("pubService")
public class PubServiceImpl implements PubService {

    @Autowired
    private PubDao pubDao;


    /*
     * (non-Javadoc)
     * 
     * @see com.legendshop.business.service.PubService#load(java.lang.Long)
     */
    @Override
    public Pub getPubById(Long id) {
        return pubDao.getPubById(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.legendshop.business.service.PubService#delete(java.lang.Long)
     */
    @Override
    public void deletePub(Pub pub) {
        pubDao.deletePub(pub);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.legendshop.business.service.PubService#save(com.legendshop.model.
     * entity.Pub, java.lang.String, boolean)
     */
    @Override
    public Long save(Pub pub) {
        if (!AppUtils.isBlank(pub.getId())) {
            Pub entity = pubDao.getPubById(pub.getId());
            if (entity != null) {
                entity.setRecDate(new Date());
                entity.setMsg(pub.getMsg());
                entity.setTitle(pub.getTitle());
                entity.setType(pub.getType());
                entity.setEndDate(pub.getEndDate());
                entity.setStartDate(pub.getStartDate());
                update(entity);
                return pub.getId();
            } else {
                return null; //can not find pub
            }

        } else {
            pub.setRecDate(new Date());
            pub.setStatus(Constants.ONLINE);
            return (Long) pubDao.savePub(pub);
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.legendshop.business.service.PubService#update(com.legendshop.model
     * .entity.Pub)
     */
    @Override
    public void update(Pub pub) {
        pubDao.updatePub(pub);
    }

    @Override
    public List<Pub> getPub(String userName, Long id, boolean isUser) {
        return pubDao.getPub(userName, id, isUser);
    }

    @Override
    @Cacheable(value = "PubByShopId")
    public List<Pub> queryPubByShopId() {
        return pubDao.queryPubByShopId();
    }

    @Override
    public List<Pub> queryShopPub() {
        return pubDao.queryShopPub();
    }

    @Override
    public List<Pub> queryUserPub() {
        return pubDao.queryUserPub();
    }

    @Override
    public List<Pub> getPubByShopId() {
        return pubDao.getPubByShopId();
    }

    @Override
    public PageSupport<Pub> getPubListPage(String curPageNO, Pub pub, DataSortResult result) {
        return pubDao.getPubListPage(curPageNO, pub, result);
    }
}
