/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Group;

/**
 * 团购Dao接口.
 */

public interface GroupDao extends GenericDao<Group, Long> {
     
	public abstract Group getGroup(Long id);
	
    public abstract int deleteGroup(Group group);
	
	public abstract Long saveGroup(Group group);
	
	public abstract int updateGroup(Group group);
	
	public abstract PageSupport<Group> getGroup(CriteriaQuery cq);

	public abstract Group getGroup(Long shopId, Long id);

	public abstract List<Category> findgroupCategory();

	public abstract void updataStatus(Long id, Long status);

	public abstract PageSupport<Group> getGroupPage(String curPageNO, Group group);

	public abstract PageSupport<Group> queryGouplist(String curPageNO, GroupProdSeachParam param);

	public abstract PageSupport<Group> queryGroupPage(String curPageNO, Integer firstCid, Integer twoCid, Integer thirdNote);

	public abstract PageSupport<Group> queryGroupList(String curPageNO, GroupProdSeachParam param);

	public abstract PageSupport<Group> getGroupPage(String curPageNO, DataSortResult result, Group group);

	public abstract boolean findIfJoinGroup(Long productId);

	/**
	 * 逻辑删除团购活动
	 * @param shopId 店铺ID
	 * @param id 团购ID
	 * @param deleteStatus 删除状态
	 */
	public abstract void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus);

	/**
	 * 获取团购交易总金额
	 * @param groupId 团购ID
	 * @return
	 */
	public abstract OperateStatisticsDTO getTotalAmount(Long groupId);

	/**
	 * 获取活动交易成功的金额
	 * @param groupId 团购ID
	 * @return
	 */
	public abstract Double getSucceedAmount(Long groupId);

	
 }
