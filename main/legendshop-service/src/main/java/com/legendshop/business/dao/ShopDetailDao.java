/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.IdTextEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BusinessMessageDTO;
import com.legendshop.model.dto.ShopDetailTimerDto;
import com.legendshop.model.dto.ShopSeekDto;
import com.legendshop.model.dto.UserDetailExportDto;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopDetailView;
import com.legendshop.model.entity.UserDetail;

/**
 * 商城信息lDao.
 */
public interface ShopDetailDao extends GenericDao<ShopDetail, Long> {
	
	/**
	 * Gets the shop detail.
	 *
	 */
	public abstract List<ShopDetailView> getShopDetail(final Long[] shopId);

	/**
	 * Gets the product num.
	 *
	 */
	public abstract long getProductNum(Long shopId);

	/**
	 * Gets the off product num.
	 */
	public abstract long getOffProductNum(Long shopId);

	/**
	 * 更新产品.
	 * 
	 * @param shopdetail the shopdetail
	 */
	public abstract void updateShopDetail(ShopDetail shopdetail);

	/**
	 * Update shop detail.
	 * 
	 * @param shopId 商城Id
	 */
	public abstract void updateShopDetailProdNum(Long shopId);

	/**
	 * Update shop.
	 *
	 * @param userId     the user id
	 * @param shopDetail the shop detail
	 * @param status     the status
	 * @return true, if successful
	 */
	public abstract boolean updateShop(String userId, ShopDetail shopDetail, Integer status);

	/**
	 * Save shop detail.
	 * 
	 * @param shopDetail the shop detail
	 */
	public abstract void saveShopDetail(ShopDetail shopDetail);

	/**
	 * Gets the shop detail by shop id.
	 * 
	 * @param shopId the shop id
	 * @return the shop detail by shop id
	 */
	public abstract ShopDetail getShopDetailByShopId(final Long shopId);

	/**
	 * Gets the shop detail view.
	 * 
	 * @param userName the store name
	 * @return the shop detail view
	 */
	public abstract ShopDetailView getShopDetailView(final Long shopId);

	/**
	 * Delete shop detail.
	 * 
	 * @param shopDetail the shop detail
	 */
	public abstract String deleteShopDetail(ShopDetail shopDetail);

	/**
	 * 删除商城
	 * 
	 * @param shopId
	 */
	public abstract int deleteShopDetailById(Long shopId);

	/**
	 * Gets the all shop count.
	 * 
	 * @return the all shop count
	 */
	public abstract Long getAllShopCount();

	/**
	 * Gets the shop id by name.
	 *
	 * @param userName the user name
	 * @return the shop id by name
	 */
	public abstract Long getShopIdByName(String userName);

	public abstract Long getShopIdByUserId(String userId);

	public abstract Long getShopIdByDomain(String domainName);

	/**
	 * 开店用户找到自己的商城
	 * 
	 * @param userId
	 * @return
	 */
	public abstract ShopDetail getShopDetailByUserId(String userId);

	public abstract List<ShopDetail> getAllShopDetail();

	public abstract Long getMaxShopId();

	public abstract Long getMinShopId();

	public abstract void changeOptionStatus(String userId, Long shopId, Integer option);

	public abstract void changeStatus(String userId, Long shopId, Integer status);

	public abstract String getShopName(Long shopId);

	/**
	 * 获取店铺ID 和 店铺名,用于Select2
	 * 
	 * @return
	 */
	public abstract List<IdTextEntity> getShopDetailList(String shopName);

	/**
	 * @param secDomainName
	 * @return
	 */
	public abstract Long getShopIdBySecDomain(String secDomainName);

	public abstract Integer updateShopType(Long shopId, Long type);

	/**
	 * 根据用户Id更新商城状态
	 * 
	 * @param userId 用户ID
	 * @param status 状态
	 * @return
	 */
	public abstract int updateShopDetailStatus(String userId, Integer status);

	public abstract List<ShopDetailTimerDto> getShopDetailList(Long firstShopId, Integer commitInteval);

	public abstract List<UserDetailExportDto> findExportUserDetail(String string, Object[] array);

	long getCountShopId();

	public abstract List<ShopSeekDto> getShopSeekList(String sql, Object[] params);

	public abstract PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail);

	public abstract PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail, Integer pageSize, DataSortResult result);

	public abstract PageSupport<ShopDetail> getloadSelectShop(String curPageNO, String shopName);

	public abstract int updateShopCommision(Long shopId, Integer commision);

	/**
	 * 获取从某个位置开始的获取n个商家
	 * 
	 * @param start 从第几条记录开始
	 * @param size  获取多少个商家
	 * @return
	 */
	public abstract List<ShopDetail> getShopDetailListTop(Integer start, Integer size);

	/**
	 * 关联查询店铺及其类型信息
	 * 
	 * @param userId
	 * @return
	 */
	public abstract ShopDetail getTypeShopDetailByUserId(String userId);

	/**
	 * 根据shopId获取商城状态
	 * 
	 * @param shopId
	 * @return
	 */
	public abstract Integer getShopStatus(Long shopId);

	/**
	 * 保存商城， 返回商城开通状态
	 * 
	 * @param userDetail the user detail
	 * @param shopDetail the shop detail
	 * @return the integer
	 */
	abstract Integer saveShopDetailAndRole(UserDetail userDetail, ShopDetail shopDetail);

	/**
	 * Update shop detail.
	 * 
	 * @param userDetail the user detail
	 * @param shopDetail the shop detail
	 * @param isAdmin    the is admin
	 */
	abstract void updateShopDetail(UserDetail userDetail, ShopDetail shopDetail, boolean isAdmin);

	/**
	 * UserId 一定不能为空，返回当前商城开通状态
	 * 
	 * @param userDetail the user detail
	 * @param shopDetail the shop detail
	 * @return the integer
	 */
	abstract Integer saveShopDetail(UserDetail userDetail, ShopDetail shopDetail);
	
	
	
	/**
	 * 删除商城的所有信息.
	 *
	 * @param userId the user id
	 * @param userName the user name
	 * @param deleteShopOnly the delete shop only
	 */
	abstract String deleteShopDetail(Long shopId,String userId, String userName, boolean deleteShopOnly);
	
	/**
	 * 店铺是否存在
	 * 
	 */
	abstract boolean isShopExist(Long shopId);

	ShopDetail getShopDetailNoCacheByShopId(Long shopId);

	/**
	 * 根据店铺id获取营业执照信息
	 * @param shopId
	 * @return
	 */
	BusinessMessageDTO getBusinessMessage(Long shopId);
}