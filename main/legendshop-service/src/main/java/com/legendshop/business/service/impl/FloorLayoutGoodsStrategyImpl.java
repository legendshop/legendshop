package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.util.AppUtils;


/**
 * //(5x商品)
 * @author Tony
 *
 */
@Service("floorLayoutGoodsStrategy")
public class FloorLayoutGoodsStrategyImpl implements FloorLayoutStrategy {
	
	@Autowired
	private FloorItemDao floorItemDao;
	
	@Autowired
	private FloorDao floorDao;
	
	@Autowired
	private FloorSubItemDao floorSubItemDao;
	
	@Override
	public void deleteFloor(Floor floor) {
		
		//delete ffrom ls_sub_floor_item
		floorDao.update("delete from ls_sub_floor_item where layout=? and f_id=?", new Object[]{FloorLayoutEnum.WIDE_GOODS.value(),floor.getFlId()});
		
		floorDao.update("delete from ls_sub_floor where floor_id = ? ", floor.getFlId());
		
    	floorItemDao.deleteFloorItem(floor.getFlId());
		
     	//最后删除自己本身
        floorDao.deleteFloor(floor);
        
	}

	@Override
	public FloorDto findFloorDto(Floor floor) {
		FloorDto floorDto=new FloorDto();
		floorDto.setCssStyle(floor.getCssStyle());
		floorDto.setFlId(floor.getFlId());
        floorDto.setLayout(floor.getLayout());
        floorDto.setName(floor.getName());
        floorDto.setSeq(floor.getSeq());
    	List<SubFloorItem> subFloorItems = floorSubItemDao.findSubFloorItem(floor.getFlId(),FloorLayoutEnum.WIDE_GOODS.value(),5);
		if(AppUtils.isNotBlank(subFloorItems)){
			floorDto.setSubFloorItems(subFloorItems);
		}
		return floorDto;
	}
}
