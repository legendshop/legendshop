package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.entity.UserRole;
import com.legendshop.model.entity.UserRoleId;

public interface UserRoleDao {

	List<UserRole> getUserRoleByUserId(String userId);
	
	List<UserRole> getUserRoleByRoleId(String roleId);
	
	UserRole getUserRoleById(UserRoleId id);
	
	int deleteUserRoleById(UserRoleId id);
	
	int deleteUserRole(List<UserRole> userRoleList);
	
	int saveUserRole(UserRole userRole);

    int deleteUserRole(UserRole userRole);

	void saveUserRoleList(List<UserRole> userRoles);
	
	/**
	 * 删除用户对应的角色
	 * @param id
	 * @param backEnd
	 */
	void deleteUserRoleByUserId(String id, ApplicationEnum backEnd);
	
	void deleteByUserIdAndRoleId(String id, String roldId);
	
	Integer isExistUserRole(String userId, String roldId);
	
}
