/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserIdcardDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.overseas.UserIdcard;

/**
 *用户实名认证Dao
 */
@Repository
public class UserIdcardDaoImpl extends GenericDaoImpl<UserIdcard, Long> implements UserIdcardDao  {
     
    public List<UserIdcard> getUserIdcard(String userId){
   		return this.queryByProperties(new EntityCriterion().eq("userId", userId));
    }

	public UserIdcard getUserIdcard(Long id){
		return getById(id);
	}
	
    public int deleteUserIdcard(UserIdcard userIdcard){
    	return delete(userIdcard);
    }
	
	public Long saveUserIdcard(UserIdcard userIdcard){
		return save(userIdcard);
	}
	
	public int updateUserIdcard(UserIdcard userIdcard){
		return update(userIdcard);
	}

	@Override
	public UserIdcard getDefaultUserIdcard(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId).eq("defaultSts", 1));
	}
	
 }
