/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AccusationAppealDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationAppeal;

/**
 * 商家上诉Dao实现类.
 */
@Repository
public class AccusationAppealDaoImpl extends GenericDaoImpl<AccusationAppeal, Long> implements AccusationAppealDao {

	/** 获取商家上诉 */
	public AccusationAppeal getAccusationAppeal(Long id) {
		return getById(id);
	}

	/** 删除商家上诉 */
	public void deleteAccusationAppeal(AccusationAppeal accusationAppeal) {
		delete(accusationAppeal);
	}

	/** 保存商家上诉 */
	public Long saveAccusationAppeal(AccusationAppeal accusationAppeal) {
		return (Long) save(accusationAppeal);
	}
	
	/** 更新商家上诉 */
	public void updateAccusationAppeal(AccusationAppeal accusationAppeal) {
		update(accusationAppeal);
	}

	/** 获取商家上诉列表 */
	@Override
	public PageSupport<AccusationAppeal> getAccusationAppealPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(AccusationAppeal.class, curPageNO);
		return queryPage(cq);
	}

}
