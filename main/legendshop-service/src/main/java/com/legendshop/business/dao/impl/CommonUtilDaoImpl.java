/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CommonUtilDao;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.util.AppUtils;

/**
 * 公共权限角色服务Dao.
 */
@Repository
public class CommonUtilDaoImpl implements CommonUtilDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** The admin right sql. */
	private static String adminRightSQL = "select id from ls_role where role_type = 'ROLE_SUPPLIER' or role_type = 'ROLE_USER'";

	/** The user right sql. */
	private static String userRightSQL = "select id from ls_role where role_type = 'ROLE_USER'";

	/**
	 * 删除权限
	 */
	@Override
	public void removeRole(List<String> roles, String userId) {
		if(AppUtils.isBlank(roles) || AppUtils.isBlank(userId)){
			return;
		}
		String sql = "delete from ls_usr_role where user_id = ? and role_id = ?";
		List<Object[]> batchArgs = new ArrayList<Object[]>(roles.size());
		for (int i = 0; i < roles.size(); i++) {
			Object[] param = new Object[2];
			param[0] = userId;
			param[1] = roles.get(i);
			batchArgs.add(param);
		}
		jdbcTemplate.batchUpdate(sql, batchArgs);
	}
	/**
	 * 删除用户所有的权限
	 * @param userId
	 */
	public void removeAllRole(String userId) {
		jdbcTemplate.update("delete from ls_usr_role where user_id = ? and app_no = ?", userId, ApplicationEnum.FRONT_END.value());
	}

	/**
	 * Save role.
	 * 
	 * @param roles
	 *            the roles
	 * @param userId
	 *            the user id
	 */
	private void saveRole(List<String> roles, String userId) {
		if(AppUtils.isBlank(roles) || AppUtils.isBlank(userId)){
			return;
		}
		
		String sql = "insert into ls_usr_role(user_id, role_id,app_no) values(?,?,?)";
		List<Object[]> batchArgs = new ArrayList<Object[]>(roles.size());
		for (int i = 0; i < roles.size(); i++) {
			
			List<String> list = jdbcTemplate.query("select  user_id from ls_usr_role where user_id = ? and role_id = ?", new Object[]{userId, roles.get(i)}, new RowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					String userId = rs.getString("user_id");
					return userId;

				}
			});
			
			if(AppUtils.isBlank(list)){
				Object[] param = new Object[3];
				param[0] = userId;
				param[1] = roles.get(i);
				param[2] = ApplicationEnum.FRONT_END.value();
				batchArgs.add(param);
			}
		}
		if(AppUtils.isNotBlank(batchArgs)){
			jdbcTemplate.batchUpdate(sql, batchArgs);
		}
		
   }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.CommonUtilDao#saveAdminRight(java.lang.String
	 * )
	 */
	@Override
	public void saveAdminRight(String userId) {
		List<String> roles =   jdbcTemplate.queryForList(adminRightSQL, String.class); 
		saveRole(roles, userId);

	}

	/**
	 * 保存用户权限
	 * 
	 */
	@Override
	public void saveUserRight(String userId) {
		List<String> roles = jdbcTemplate.queryForList(userRightSQL, String.class);
		saveRole(roles, userId);
	}

	/**
	 * 删除管理员权限
	 */
	@Override
	public void removeAdminRight(String userId) {
		//先去掉所有的权限再保存用户权限
		removeAllRole(userId);
		saveUserRight(userId);
	}

	/**
	 * 删除用户权限
	 * )
	 */
	@Override
	public void removeUserRight(String userId) {
		List<String> roles =  jdbcTemplate.queryForList(userRightSQL, String.class);
		removeRole(roles, userId);
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
