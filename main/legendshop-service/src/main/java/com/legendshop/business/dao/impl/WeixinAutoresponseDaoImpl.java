/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinAutoresponseDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;

/**
 * 微信响应Dao
 */
@Repository
public class WeixinAutoresponseDaoImpl extends GenericDaoImpl<WeixinAutoresponse, Long> implements WeixinAutoresponseDao  {

	public WeixinAutoresponse getWeixinAutoresponse(Long id){
		return getById(id);
	}
	
    public int deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
    	return delete(weixinAutoresponse);
    }
	
	public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
		return save(weixinAutoresponse);
	}
	
	public int updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
		return update(weixinAutoresponse);
	}

	@Override
	public PageSupport<WeixinAutoresponse> getWeixinnAutoresponse(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(WeixinAutoresponse.class, curPageNO);
        cq.addDescOrder("id");
		return queryPage(cq);
	}
	
 }
