/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.appDecorate.AppPageManage;

/**
 * The Class AppPageManageDao. 移动端页面装修页面Dao接口
 * @author legendshop
 */
public interface AppPageManageDao extends Dao<AppPageManage,Long>{

	/**
	 * 根据Id获取移动端页面装修页面
	 * @param id
	 * @return
	 */
	AppPageManage getAppPageManage(Long id);

	/**
	 * 根据Id删除移动端页面装修页面
	 * @param id
	 * @return
	 */
    int deleteAppPageManage(Long id);

	/**
	 * 根据对象删除
	 * @param appPageManage
	 * @return
	 */
    int deleteAppPageManage(AppPageManage appPageManage);

	/**
	 * 保存移动端页面装修页面
	 * @param appPageManage
	 * @return
	 */
	Long saveAppPageManage(AppPageManage appPageManage);

	/**
	 * 更新移动端页面装修页面
	 * @param appPageManage
	 * @return
	 */
	int updateAppPageManage(AppPageManage appPageManage);

	/**
	 * 分页查询移动端页面装修页面列表
	 * @param curPageNO 当前页码
	 * @param pageSize 页数
	 * @param appPageManage
	 * @return
	 */
	PageSupport<AppPageManage> queryAppPageManage(String curPageNO, Integer pageSize,AppPageManage appPageManage);

	/**
	 * 将使用中的页面更新为未使用
	 * @param use
	 */
	void updatePageToUnUse(Integer use);

	/**
	 * 根据使用状态获取页面    
	 * @param isUse 使用状态 1：使用中 0 ：未使用
	 */
	AppPageManage getUseAppPageByUse(Integer isUse);

}
