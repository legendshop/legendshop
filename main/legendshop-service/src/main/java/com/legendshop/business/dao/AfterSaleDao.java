package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AfterSale;

public interface AfterSaleDao extends GenericDao<AfterSale, Long>{

	public abstract List<AfterSale> getAfterSale();

	public abstract AfterSale getAfterSale(Long id);
	
    public abstract void deleteAfterSale(AfterSale afterSale);
	
	public abstract Long saveAfterSale(AfterSale afterSale);
	
	public abstract void updateAfterSale(AfterSale afterSale);
	
	public abstract PageSupport <AfterSale>getAfterSale(CriteriaQuery cq);
	
	public abstract void deleteByAfterSaleId(Long id);

	public abstract PageSupport<AfterSale> getAfterSale(String curPageNO, String userName, String userId);

	public abstract PageSupport<AfterSale> getAfterSalePage(String curPageNO, String userName, String userId);
}
