/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassArticleDao;
import com.legendshop.model.entity.GrassArticle;
import com.legendshop.spi.service.GrassArticleService;

/**
 * The Class GrassArticleServiceImpl.
 *  种草社区文章表服务实现类
 */
@Service("grassArticleService")
public class GrassArticleServiceImpl implements GrassArticleService{

    /**
     *
     * 引用的种草社区文章表Dao接口
     */
	@Autowired
    private GrassArticleDao grassArticleDao;
   
   	/**
	 *  根据Id获取种草社区文章表
	 */
    public GrassArticle getGrassArticle(Long id) {
        return grassArticleDao.getGrassArticle(id);
    }
    
    /**
	 *  根据Id删除种草社区文章表
	 */
    public int deleteGrassArticle(Long id){
    	return grassArticleDao.deleteGrassArticle(id);
    }

   /**
	 *  删除种草社区文章表
	 */ 
    public int deleteGrassArticle(GrassArticle grassArticle) {
       return  grassArticleDao.deleteGrassArticle(grassArticle);
    }

   /**
	 *  保存种草社区文章表
	 */	    
    public Long saveGrassArticle(GrassArticle grassArticle) {
        if (!AppUtils.isBlank(grassArticle.getId())) {
            updateGrassArticle(grassArticle);
            return grassArticle.getId();
        }
        return grassArticleDao.saveGrassArticle(grassArticle);
    }

   /**
	 *  更新种草社区文章表
	 */	
    public void updateGrassArticle(GrassArticle grassArticle) {
        grassArticleDao.updateGrassArticle(grassArticle);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassArticle> queryGrassArticle(String curPageNO, Integer pageSize){
     	return grassArticleDao.queryGrassArticle(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassArticleDao(GrassArticleDao grassArticleDao) {
        this.grassArticleDao = grassArticleDao;
    }

	@Override
	public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo, String name,
			Integer pageSize) {
		return grassArticleDao.queryGrassArticlePage(curPageNo,name, pageSize);
	}
    
}
