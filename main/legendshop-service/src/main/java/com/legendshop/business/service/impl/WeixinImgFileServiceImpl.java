/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinImgFileDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinImgFile;
import com.legendshop.spi.service.WeixinImgFileService;
import com.legendshop.util.AppUtils;

/**
 *微信图片服务
 */
@Service("weixinImgFileService")
public class WeixinImgFileServiceImpl  implements WeixinImgFileService{
	
	@Autowired
    private WeixinImgFileDao weixinImgFileDao;

    public WeixinImgFile getWeixinImgFile(Long id) {
        return weixinImgFileDao.getWeixinImgFile(id);
    }

    public void deleteWeixinImgFile(WeixinImgFile weixinImgFile) {
        weixinImgFileDao.deleteWeixinImgFile(weixinImgFile);
    }

    public Long saveWeixinImgFile(WeixinImgFile weixinImgFile) {
        if (!AppUtils.isBlank(weixinImgFile.getId())) {
            updateWeixinImgFile(weixinImgFile);
            return weixinImgFile.getId();
        }
        return weixinImgFileDao.saveWeixinImgFile(weixinImgFile);
    }

    public void updateWeixinImgFile(WeixinImgFile weixinImgFile) {
        weixinImgFileDao.updateWeixinImgFile(weixinImgFile);
    }

	@Override
	public List<WeixinImgFile> getWeixinImgFilesByGroupId(Long groupId) {
		return weixinImgFileDao.queryByProperties(new EntityCriterion().eq("imgGoupId", groupId));
	}


	@Override
	public void updateWeixinImgFiles(List<WeixinImgFile> weixinImgFiles) {
		weixinImgFileDao.update(weixinImgFiles);
	}


	@Override
	public void changeImgsGroup(List<Long> idList, Long groupId) {
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (int i=0;i<idList.size();i++) {
			Object[] params = new Object[2];
			params[0]=groupId;
			params[1]=idList.get(i);
			batchArgs.add(params);
		}
		weixinImgFileDao.batchUpdate("update ls_weixin_img_file set img_goup_id=? where id =?", batchArgs);
	}


	@Override
	public void deleteWeixinImgFile(List<Long> idList) {
		weixinImgFileDao.deleteById(idList);
	}


	@Override
	public PageSupport<WeixinImgFile> getWeixinImgFile(String curPageNO, int pageSize, Long type) {
		return weixinImgFileDao.getWeixinImgFile(curPageNO,pageSize,type);
	}
}
