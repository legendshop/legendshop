/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DiscoverCommeDao;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.spi.service.DiscoverCommeService;

/**
 * The Class DiscoverCommeServiceImpl.
 *  发现文章评论表服务实现类
 */
@Service("discoverCommeService")
public class DiscoverCommeServiceImpl implements DiscoverCommeService{

    /**
     *
     * 引用的发现文章评论表Dao接口
     */
	@Autowired
    private DiscoverCommeDao discoverCommeDao;
   
   	/**
	 *  根据Id获取发现文章评论表
	 */
    public DiscoverComme getDiscoverComme(Long id) {
        return discoverCommeDao.getDiscoverComme(id);
    }
    
    /**
	 *  根据Id删除发现文章评论表
	 */
    public int deleteDiscoverComme(Long id){
    	return discoverCommeDao.deleteDiscoverComme(id);
    }

   /**
	 *  删除发现文章评论表
	 */ 
    public int deleteDiscoverComme(DiscoverComme discoverComme) {
       return  discoverCommeDao.deleteDiscoverComme(discoverComme);
    }

   /**
	 *  保存发现文章评论表
	 */	    
    public Long saveDiscoverComme(DiscoverComme discoverComme) {
        if (!AppUtils.isBlank(discoverComme.getId())) {
            updateDiscoverComme(discoverComme);
            return discoverComme.getId();
        }
        return discoverCommeDao.saveDiscoverComme(discoverComme);
    }

   /**
	 *  更新发现文章评论表
	 */	
    public void updateDiscoverComme(DiscoverComme discoverComme) {
        discoverCommeDao.updateDiscoverComme(discoverComme);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverComme> queryDiscoverComme(String curPageNO, Integer pageSize){
     	return discoverCommeDao.queryDiscoverComme(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setDiscoverCommeDao(DiscoverCommeDao discoverCommeDao) {
        this.discoverCommeDao = discoverCommeDao;
    }
    
}
