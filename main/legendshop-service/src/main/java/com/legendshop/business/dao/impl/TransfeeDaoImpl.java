package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TransCityDao;
import com.legendshop.business.dao.TransfeeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.TransCity;
import com.legendshop.model.entity.Transfee;
import com.legendshop.util.AppUtils;
/**
 * 运输费用Dao
 */
@Repository
public class TransfeeDaoImpl extends GenericDaoImpl<Transfee, Long> implements TransfeeDao{
	
	@Autowired
	private TransCityDao transCityDao;
	
	String getTranfeeSQL ="select ltf.freight_mode as freightMode,ltf.trans_add_fee as transAddFee,ltf.trans_fee as transFee,ltf.trans_add_weight as transAddWeight,ltf.trans_weight as transWeight " +
			" from ls_transport ltp,ls_transfee ltf where ltp.id = ltf.transport_id and ltp.id = ?";
	
	private static String sqlForGetTranfeeByShopId = "select ltf.id, ltp.id as transportId, ltf.freight_mode as freightMode,ltf.trans_add_fee as transAddFee,ltf.trans_fee as transFee," +
  "ltf.trans_add_weight as transAddWeight,ltf.trans_weight as transWeight, ltc.city_id  as cityId, lc.city as city  from ls_transport ltp inner join ls_transfee ltf on ltp.id = ltf.transport_id " +
  "left join ls_transcity ltc on  ltf.id = ltc.transfee_id left join ls_cities lc on ltc.city_id = lc.id  where ltp.shop_id = ? order by ltf.freight_mode desc,ltf.id";
	
	//加载运费
	private static String sqlForGetTranfeeById = "select ltf.id, ltp.id as transportId, ltf.freight_mode as freightMode,ltf.trans_add_fee as transAddFee,ltf.trans_fee as transFee," +
			  "ltf.trans_add_weight as transAddWeight,ltf.trans_weight as transWeight, ltc.city_id  as cityId, lc.city as city  from ls_transport ltp inner join ls_transfee ltf on ltp.id = ltf.transport_id " +
			  "left join ls_transcity ltc on  ltf.id = ltc.transfee_id left join ls_cities lc on ltc.city_id = lc.id  where ltp.id = ? order by ltf.freight_mode desc,ltf.id";

	public List<Transfee> getTransfee() {
		return null;
	}

	public Transfee getTransfee(Long id) {
		return getById(id);
	}

	public void deleteTransfee(Transfee transfee) {
		delete(transfee);
	}

	public Long saveTransfee(Transfee transfee) {
		transfee.setStatus(Constants.ONLINE);
		Long  transfeeId = save(transfee);
		List<TransCity> transCityList = new ArrayList<TransCity>();
		
		List<Long> cityIdList = transfee.getCityIdList();
		if(AppUtils.isNotBlank(cityIdList)){
			for(Long cityId :cityIdList){
				TransCity transCity = new TransCity();
				transCity.setCityId(cityId);
				transCity.setTransfeeId(transfeeId);
				transCityList.add(transCity);
			}
			transCityDao.saveTransCity(transCityList);
		}
		
		return transfeeId;
	}

	public void updateTransfee(Transfee transfee) {
		update(transfee);
	}

	public List<Transfee> getTranfee(Long transportId) {
		return query(getTranfeeSQL,Transfee.class,transportId);
	}

	public List<Transfee> getTranfeeByShopId(Long shopId) {
		//id : Transfee
		Map<Long, Transfee> result = new LinkedHashMap<Long, Transfee>();
		List<Transfee> transfeeList=  query(sqlForGetTranfeeByShopId,Transfee.class,shopId);
		for (Transfee transfee : transfeeList) {
			Transfee tf = result.get(transfee.getId());
			if(tf == null){
				tf = transfee;
				result.put(transfee.getId(), tf);
			}
			tf.addCityName(transfee.getCity());
		}
		return new ArrayList<Transfee>(result.values());
	}

	public void setTransCityDao(TransCityDao transCityDao) {
		this.transCityDao = transCityDao;
	}

	/**
	 * 根据特定的运费模板加载运费
	 * id为ls_transport的id
	 */
	public List<Transfee> getTranfeeById(Long id) {
		Map<Long, Transfee> transFeeMap = new LinkedHashMap<Long, Transfee>();
		List<Transfee> transfeeList = query(sqlForGetTranfeeById,Transfee.class,id);
		for (Transfee transfee : transfeeList) {
			//把城市组装到同一个运费项
			Transfee tf = transFeeMap.get(transfee.getId());
			if(tf == null){
				tf = transfee;
				transFeeMap.put(transfee.getId(), tf);
			}
			tf.addCityId(transfee.getCityId());
			if(AppUtils.isNotBlank(transfee.getCityId())){
				tf.getCityList().add(transfee.getCityId().intValue());
				tf.getCityIdList().add(transfee.getCityId());
			}
			tf.addCityName(transfee.getCity());
		}
		List<Transfee> result = new ArrayList<Transfee>(transFeeMap.values());
		return result;
	}
	
	/**
	 * 用于订单结算的方法
	 * @param id
	 * @return
	 */
	@Override
	public List<Transfee> getOrderTranfeeById(Long id) {
		List<Transfee> transfeeList = query(sqlForGetTranfeeById,Transfee.class,id);
		return transfeeList;
	}

	@Override
	public PageSupport<Transfee> getTransfeePage(String curPageNO, Transfee transfee) {
		 CriteriaQuery cq = new CriteriaQuery(Transfee.class, curPageNO);
		return queryPage(cq);
	}

	
}
