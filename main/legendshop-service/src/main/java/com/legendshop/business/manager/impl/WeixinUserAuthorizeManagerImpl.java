/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.manager.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.des.WeixinEncryptUtil;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.business.service.weixin.WeixinUserAuthorizeApi;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.ThirdUserToken;
import com.legendshop.model.dto.weixin.Code2sessionResult;
import com.legendshop.model.dto.weixin.WeixinAuthorizeUserInfo;
import com.legendshop.model.dto.weixin.WeixinMpUserInfo;
import com.legendshop.model.dto.weixin.WeixinUserToken;
import com.legendshop.model.entity.PayType;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.util.AppUtils;

/**
 * 微信用户授权认证管理器实现
 * @author 开发很忙
 */
@Service
public class WeixinUserAuthorizeManagerImpl extends TemplateThirdUserAuthorizeManagerImpl {

	private static Logger log = LoggerFactory.getLogger(WeixinUserAuthorizeManagerImpl.class);

	/** PC 授权范围 */
	private static final String PC_SCOPE = "snsapi_login";

	/** H5 授权范围 */
	private static final String H5_SCOPE = "snsapi_base";

	/** 授权类型 */
	private static final String RESPONSE_TYPE = "code";

	/** 回调地址路径 */
	private static final String REDIRECT_URL_PATH= "/thirdlogin/weixin/{source}/callback";

	/** PC授权地址模板 */
	private static final String PC_AUTHORIZE_URL = "https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=RESPONSE_TYPE&scope=SCOPE&state=STATE#wechat_redirect";

	/** 手机授权地址模板 */
	private static final String H5_AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=RESPONSE_TYPE&scope=SCOPE&state=STATE#wechat_redirect";

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PayTypeService payTypeService;

	@Override
	protected String authorize(String source) {

		String appId = null;
		String scope = null;
		String redirectURI = null;
		String AUTHORIZE_URL = null;
		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5

			// TODO 这里不应该要从支付里面去取appId的, 配置不合理
			PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value());
			JSONObject paymentConfig = JSONObject.parseObject(payType.getPaymentConfig());
	        appId = paymentConfig.getString("APPID");

			log.info(payType.getPaymentConfig());
	        if(AppUtils.isBlank(appId)){
				log.info(propertiesUtil.getMobileDomainName() + "/accountModules/login/login");
				return propertiesUtil.getMobileDomainName() + "/accountModules/login/login";
			}

	        scope = H5_SCOPE;
			redirectURI = propertiesUtil.getUserAppDomainName() + REDIRECT_URL_PATH.replace("{source}", source);

			AUTHORIZE_URL = H5_AUTHORIZE_URL;

		}else{//否则就是PC了

			appId = systemParameterUtil.getWeixinLoginAppId();
			scope = PC_SCOPE;
			redirectURI = propertiesUtil.getPcDomainName() + REDIRECT_URL_PATH.replace("{source}", source);

			AUTHORIZE_URL = PC_AUTHORIZE_URL;
		}
		String state = gennerateState();

		String authorizeURL = AUTHORIZE_URL.replace("RESPONSE_TYPE", RESPONSE_TYPE)
				.replace("APPID", appId)
				.replace("REDIRECT_URI", redirectURI)
				.replace("STATE", state)
				.replace("SCOPE", scope);

		return authorizeURL;
	}

	@Override
	protected ThirdUserToken getAccessToken(String code, String source) {

		String appId = null;
		String appSecret = null;

		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5

			// TODO 这里不应该要从支付里面去取appId的, 配置不合理
			PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value());
			JSONObject paymentConfig = JSONObject.parseObject(payType.getPaymentConfig());

	        appId = paymentConfig.getString("APPID");
	        appSecret = paymentConfig.getString("APPSECRET");

		}else{//否则就是PC
			appId = systemParameterUtil.getWeixinLoginAppId();
			appSecret = systemParameterUtil.getWeixinLoginAppsecret();
		}

		WeixinUserToken weixinUserToken = WeixinUserAuthorizeApi.getAccessToken(appId, appSecret, code);
		if(null == weixinUserToken){
			log.info("############## 获取token失败 = {} ####################");
			return null;
		}

		if(!weixinUserToken.isSuccess()){
			String errorMsg = weixinUserToken.getErrmsg();
			log.info("############## 获取token失败 = {} ####################",errorMsg);
			return null;
		}

		return new ThirdUserToken(weixinUserToken);
	}

	@Override
	protected ThirdUserInfo getUserInfo(ThirdUserToken token) {

		WeixinAuthorizeUserInfo userInfo = null;
		if(AppUtils.isNotBlank(token.getScope()) && !token.getScope().contains(H5_SCOPE)){
			userInfo = WeixinUserAuthorizeApi.getUserInfo(token.getAccessToken(), token.getOpenId());

			if(!userInfo.isSuccess()){
				String errorMsg = userInfo.getErrmsg();
				log.info("############## 获取token失败 = {} ####################",errorMsg);
			}
		}

		if(null == userInfo){
			userInfo = new WeixinAuthorizeUserInfo();
			userInfo.setOpenid(token.getOpenId());
		}

		return new ThirdUserInfo(token, userInfo);
	}

	@Override
	protected ThirdUserInfo getMpUserInfo(String code, String encryptedData, String iv) {

		// TODO 这里不应该要从支付里面去取appId的, 配置不合理
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_MP_PAY.value());
		JSONObject paymentConfig = JSONObject.parseObject(payType.getPaymentConfig());
		String appId = paymentConfig.getString("APPID");
		String appSecret = paymentConfig.getString("APPSECRET");

		/*   code 换取 session_key*/
		Code2sessionResult code2sessionResult = WeixinMiniProgramApi.jscode2session(appId, appSecret, code);
		if(null == code2sessionResult){
			log.info("############## 微信小程序获取session失败={} ####################", "code2sessionResult is null");
			return null;
		}

		if (!code2sessionResult.isSuccess()) {//如果不成功
			String errorMsg = code2sessionResult.getErrmsg();
			log.info("############## 微信小程序获取session失败=={} ####################",errorMsg);
			return null;
		}

		String openid = code2sessionResult.getOpenid();
		String sessionKey = code2sessionResult.getSession_key();
		String unionid = code2sessionResult.getUnionid();

		//解密获得用户数据
		WeixinMpUserInfo weixinUserInfo = null;
		if(AppUtils.isNotBlank(encryptedData) && AppUtils.isNotBlank(iv)){
			weixinUserInfo = WeixinEncryptUtil.decodeEncryptedData(encryptedData, iv, sessionKey, WeixinMpUserInfo.class);
		}

		if(null == weixinUserInfo){
			weixinUserInfo = new WeixinMpUserInfo();
			weixinUserInfo.setOpenId(openid);
			weixinUserInfo.setUnionid(unionid);
		}else{
			if(AppUtils.isBlank(weixinUserInfo.getUnionid()) && AppUtils.isNotBlank(unionid)){
				weixinUserInfo.setUnionid(unionid);
			}
		}

		return new ThirdUserInfo(weixinUserInfo);
	}

	@Override
	protected ThirdUserAuthorizeResult authThirdUser(ThirdUserInfo thirdUserInfo, String source) {

		return thirdLoginService.authThridUser(thirdUserInfo, PassportTypeEnum.WEIXIN.value(), source);
	}
}
