/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ShopCommStat;

/**
 * 物流评分统计服务.
 */
public interface DvyTypeCommStatDao extends Dao<DvyTypeCommStat, Long> {
     
	public abstract DvyTypeCommStat getDvyTypeCommStatByShopId(Long shopId);
	
	public abstract DvyTypeCommStat getDvyTypeCommStat(Long id);
	
    public abstract int deleteDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);
	
	public abstract Long saveDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);
	
	public abstract int updateDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);
	
	public abstract PageSupport getDvyTypeCommStat(CriteriaQuery cq);

	public abstract DvyTypeCommStat getDvyTypeCommStatByDvyTypeIdForUpdate(Long dvyTypeId);
	
 }
