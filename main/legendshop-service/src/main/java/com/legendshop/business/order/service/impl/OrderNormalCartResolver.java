package com.legendshop.business.order.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.util.AppUtils;


/**
 * 普通购物车
 *
 */
@Service("orderNormalCartResolver")
public class OrderNormalCartResolver extends AbstractAddOrder implements IOrderCartResolver {
	
	/**
	 * 计算普通购物车
	 */
	@Override
	@SuppressWarnings("unchecked")
	public UserShopCartList queryOrders(Map<String, Object> params) {
		List<Long> ids = (List<Long>) params.get("ids"); //购物车ID集合
		String userId=(String) params.get("userId");//用户Id
		String userName=(String) params.get("userName");//用户名称
		Long adderessId = (Long) params.get("adderessId");//用户收货地址
		
		//拿到购物车对应的商品信息
		List<ShopCartItem> shopCartItems = null;
		if(AppUtils.isNotBlank(params.get("buyNow")) && (boolean)params.get("buyNow")){
			shopCartItems=new ArrayList<ShopCartItem>(1);
			ShopCartItem shopCartItem=  (ShopCartItem) params.get("shopCartItem");
			shopCartItems.add(shopCartItem);
		}else{
			shopCartItems = basketService.loadBasketByIds(ids, userId);
		}
		
		if(AppUtils.isBlank(shopCartItems)){
			return null;
		}
		
		//获取用户订单
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, shopCartItems, adderessId,CartTypeEnum.NORMAL.toCode());
		if(userShopCartList==null){
			return null;
		}
		
		userShopCartList.setType(CartTypeEnum.NORMAL.toCode());
		return userShopCartList;
	}
	
	
	
	/**
	 * 普通商品下单操作
	 * @param 
	 * @return
	 */
	@Override
	public List<String> addOrder(UserShopCartList userShopCartList){
		List<ShopCarts> shopCarts=userShopCartList.getShopCarts();//每个店铺一个ShopCarts
		List<String> subIds=super.submitOrder(userShopCartList);//返回每个店铺的订单号的集合
		if(AppUtils.isBlank(subIds)){
			return null;
		}
		for (Iterator<ShopCarts> shopIterator = shopCarts.iterator(); shopIterator.hasNext();) {
			ShopCarts shop=shopIterator.next();
			List<ShopCartItem> items = shop.getCartItems();
			List<Long> removeBaskets = new ArrayList<Long>();
			for (Iterator<ShopCartItem> iterator = items.iterator(); iterator.hasNext();) {
				  ShopCartItem cartItem = (ShopCartItem) iterator.next();
				  removeBaskets.add(cartItem.getBasketId()); //删除购物车
				  cartItem=null;
			}
			/**清除购物车商品 **/
			basketService.deleteById(removeBaskets);
		}
		shopCarts=null;
		return subIds;
	}


}
