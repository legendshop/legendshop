/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.JobStatusDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.JobStatus;

/**
 * Job状态控制Dao.
 */
@Repository
public class JobStatusDaoImpl extends GenericDaoImpl<JobStatus, Long> implements JobStatusDao  {
     
	public JobStatus getJobStatus(Long id){
		return getById(id);
	}
	
    public int deleteJobStatus(JobStatus jobStatus){
    	return delete(jobStatus);
    }
	
	public Long saveJobStatus(JobStatus jobStatus){
		return save(jobStatus);
	}
	
	public int updateJobStatus(JobStatus jobStatus){
		return update(jobStatus);
	}

	@Override
	public JobStatus getJobStatusByJobName(String jobName) {
		return this.getByProperties(new EntityCriterion().eq("jobName", jobName));
	}
	
 }
