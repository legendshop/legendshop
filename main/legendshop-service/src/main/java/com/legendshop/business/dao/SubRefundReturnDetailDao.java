/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.SubRefundReturnDetail;

/**
 *退款明细Dao
 */
public interface SubRefundReturnDetailDao extends Dao<SubRefundReturnDetail, Long> {

	/**
	 * 根据Id获取
	 */
	public abstract SubRefundReturnDetail getSubRefundReturnDetail(Long id);

	/**
	 * 删除
	 */
	public abstract int deleteSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);

	/**
	 * 保存
	 */
	public abstract Long saveSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);

	/**
	 * 更新
	 */
	public abstract int updateSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);

	/**
	 * 根据refundId查询列表
	 * @param refundId
	 * @return
	 */
	public abstract List<SubRefundReturnDetail> getSubRefundReturnDetailByRefundId(Long refundId);

}
