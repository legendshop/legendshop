/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.integral;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.spi.service.IntegraLogService;
import com.legendshop.util.AppUtils;

/**
 * 积分日志服务实现类
 */
@Service("integraLogService")
public class IntegraLogServiceImpl implements IntegraLogService {

	@Autowired
	private IntegraLogDao integraLogDao;

	public IntegraLog getIntegraLog(Long id) {
		return integraLogDao.getIntegraLog(id);
	}

	/**
	 * 删除积分日志
	 */
	public void deleteIntegraLog(IntegraLog integraLog) {
		integraLogDao.deleteIntegraLog(integraLog);
	}

	/**
	 * 保存积分日志
	 */
	public Long saveIntegraLog(IntegraLog integraLog) {
		if (!AppUtils.isBlank(integraLog.getId())) {
			updateIntegraLog(integraLog);
			return integraLog.getId();
		}
		return integraLogDao.saveIntegraLog(integraLog);
	}

	/**
	 * 更新积分日志
	 */
	public void updateIntegraLog(IntegraLog integraLog) {
		integraLogDao.updateIntegraLog(integraLog);
	}

	/**
	 * 获取积分日志
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLog(String userId, String curPageNO, String logDesc, Long logType,Date startDate,Date endDate) {
		PageSupport<IntegraLog> ps = integraLogDao.getIntegraLog(userId,curPageNO,logDesc,logType,startDate,endDate);
		return ps;
	}

	/**
	 * 获取积分日志页面
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLogPage(String userId, String curPageNO) {
		return integraLogDao.getIntegraLogPage(userId, curPageNO);
	}

	/**
	 * 获取积分日志页面
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, Integer pageSize, DataSortResult result) {
		return integraLogDao.getIntegraLogListPage(curPageNO, integraLog, pageSize, result);
	}

	/**
	 * 获取积分日志页面
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, String userId, Integer pageSize, DataSortResult result) {
		return integraLogDao.getIntegraLogListPage(curPageNO, integraLog, userId, pageSize, result);
	}
}
