/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DrawWinRecordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.draw.DrawWinRecord;
import com.legendshop.util.AppUtils;

/**
 * 中奖纪录Dao实现类.
 */
@Repository
public class DrawWinRecordDaoImpl extends GenericDaoImpl<DrawWinRecord, Long> implements DrawWinRecordDao {

	public List<DrawWinRecord> getDrawWinRecordsByActId(Long actId) {
		return this.queryByProperties(new EntityCriterion().eq("actId", actId));
	}

	public DrawWinRecord getDrawWinRecord(Long id) {
		return getById(id);
	}

	public int deleteDrawWinRecord(DrawWinRecord drawWinRecord) {
		return delete(drawWinRecord);
	}

	public Long saveDrawWinRecord(DrawWinRecord drawWinRecord) {
		return save(drawWinRecord);
	}

	public int updateDrawWinRecord(DrawWinRecord drawWinRecord) {
		return update(drawWinRecord);
	}

	@Override
	public int delDrawWinRecordsByActId(Long actId) {
		return update("DELETE FROM ls_draw_win_record  WHERE act_id= ?", actId);
	}

	/**
	 * @Description: TODO
	 * @param @param
	 *            userID
	 * @param @param
	 *            actId
	 * @param @return
	 * @date 2016-4-26
	 */
	@Override
	public List<DrawWinRecord> getDrawWinRecords(String userID, Long actId) {
		List<DrawWinRecord> winList = query("select * from ls_draw_win_record where user_id = ? and act_id = ?",
				DrawWinRecord.class, userID, actId);
		return winList;
	}

	@Override
	public PageSupport<DrawWinRecord> queryDrawWinRecordPage(String curPageNO, DrawWinRecord drawWinRecord) {
		SimpleSqlQuery query = new SimpleSqlQuery(DrawWinRecord.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("actId", drawWinRecord.getActId());
		if (AppUtils.isNotBlank(drawWinRecord.getSendStatus())) {
			map.put("sendStatus", drawWinRecord.getSendStatus());
		}
		if (AppUtils.isNotBlank(drawWinRecord.getStartTime())) {
			map.put("startTime", drawWinRecord.getStartTime());
		}
		if (AppUtils.isNotBlank(drawWinRecord.getEndTime())) {
			map.put("endTime", drawWinRecord.getEndTime());
		}
		if (AppUtils.isNotBlank(drawWinRecord.getAwardsType())) {
			map.put("awardsType", drawWinRecord.getAwardsType());
		}
		String querySQL = ConfigCode.getInstance().getCode("adminDraw.getDrawWinRecord", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("adminDraw.getDrawWinRecordCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
