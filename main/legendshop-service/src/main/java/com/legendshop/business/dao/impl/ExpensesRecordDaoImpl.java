/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ExpensesRecord;

/**
 * 消费记录DaoImpl.
 */
@Repository("expensesRecordDao")
public class ExpensesRecordDaoImpl extends GenericDaoImpl<ExpensesRecord, Long> implements ExpensesRecordDao  {

	public ExpensesRecord getExpensesRecord(Long id){
		return getById(id);
	}
	
    public int deleteExpensesRecord(ExpensesRecord expensesRecord){
    	return delete(expensesRecord);
    }
	
	public Long saveExpensesRecord(ExpensesRecord expensesRecord){
		return save(expensesRecord);
	}
	
	public int updateExpensesRecord(ExpensesRecord expensesRecord){
		return update(expensesRecord);
	}
	
	public PageSupport<ExpensesRecord> getExpensesRecord(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public boolean deleteExpensesRecord(String userId, Long id) {
		return update("delete from ls_expenses_record where user_id = ? and id = ?  ",userId, id) > 0;
	}

	@Override
	public void deleteExpensesRecord(String userId, List<Long> idList) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(idList.size());
		for (Long id : idList) {
			Object[] param = new Object[2];
			param[0] = userId;
			param[1] = id;
			batchArgs.add(param);
		}
		this.batchUpdate("delete from ls_expenses_record where user_id = ? and id = ?", batchArgs);
	}

	@Override
	public PageSupport<ExpensesRecord> getExpensesRecordPage(String curPageNO, String userId) {
		CriteriaQuery cq = new CriteriaQuery(ExpensesRecord.class, curPageNO);
    	cq.eq("userId", userId);
    	cq.addDescOrder("recordDate");
    	cq.setPageSize(15);
		return queryPage(cq);
	}

	@Override
	public PageSupport<ExpensesRecord> getExpensesRecord(String curPageNO, String userId, int pageSize) {
		CriteriaQuery cq = new CriteriaQuery(ExpensesRecord.class, curPageNO);
    	cq.eq("userId", userId);
    	cq.addDescOrder("recordDate");
    	cq.setPageSize(pageSize);
		return queryPage(cq);
	}
	
 }
