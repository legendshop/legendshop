/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.shopDecotate.ShopDecotate;

/**
 * The Class ShopDecotateDao.
 */

public interface ShopDecotateDao extends GenericDao<ShopDecotate, Long> {
     

	public abstract ShopDecotate getShopDecotate(Long id);
	
    public abstract int deleteShopDecotate(ShopDecotate shopDecotate);
	
	public abstract Long saveShopDecotate(ShopDecotate shopDecotate);
	
	public abstract int updateShopDecotate(ShopDecotate shopDecotate);
	
	public abstract ShopDecotate getShopDecotateByShopId(Long shopId);

	/**
	 * 获取店铺的分类信息
	 * @param shopId
	 * @return
	 */
	public abstract List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId);

	/**
	 * 查询店铺的下级分类
	 * @param shopId
	 * @param categoryId
	 * @return
	 */
	List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId,Long categoryId);

	/**
	 * 店铺热销
	 * @param shopId
	 * @return
	 */
	public abstract List<ShopLayoutHotProdDto> findShopHotProds(Long shopId);
	

	/**
	 * 根据商家Id获取商家的装修信息
	 * @param shopId
	 * @return
	 */
	public abstract ShopLayoutShopInfoDto findShopInfo(Long shopId);

	/**
	 * 商品自定义
	 * @param shopId
	 * @param layoutId
	 * @return
	 */
	public abstract List<ShopLayoutProdDto> findShopLayoutProdDtos(Long shopId,
			Long layoutId);

	/**
	 *  商品自定义
	 * @param shopId
	 * @return
	 */
	public List<ShopLayoutProdDto> findShopLayoutProdDtos(Long shopId);

	/**
	 * 根据商家id获取优惠券
	 * @param shopId
	 * @return
	 */
	public abstract List<Coupon> getCouponByShopId(Long shopId);

	/**
	 * 根据Dto更新商家装修
	 * @param decotateDto
	 */
	public abstract void updateShopDecotate(ShopDecotateDto decotateDto);
	
 }
