/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinGroupDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinGroup;

/**
 * 微信组Dao
 */
@Repository
public class WeixinGroupDaoImpl extends GenericDaoImpl<WeixinGroup, Long> implements WeixinGroupDao  {
     
    public List<WeixinGroup> getWeixinGroup(String GroupName){
   		return this.queryByProperties(new EntityCriterion().eq("name", GroupName));
    }
    /**
     * 得到所有的微信组
     */
    public List<WeixinGroup> getWeixinGroup(){
   		return this.queryAll();
    }

	public WeixinGroup getWeixinGroup(Long id){
		return getById(id);
	}
	
    public int deleteWeixinGroup(WeixinGroup weixinGroup){
    	return delete(weixinGroup);
    }
	
	public Long saveWeixinGroup(WeixinGroup weixinGroup){
		return save(weixinGroup);
	}
	
	public int updateWeixinGroup(WeixinGroup weixinGroup){
		return update(weixinGroup);
	}

 }
