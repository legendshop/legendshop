/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DeliveryCorpDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.spi.service.DeliveryCorpService;
import com.legendshop.util.AppUtils;

/**
 * The Class DeliveryCorpServiceImpl.
 */
@Service("deliveryCorpService")
public class DeliveryCorpServiceImpl implements DeliveryCorpService {

	@Autowired
	private DeliveryCorpDao deliveryCorpDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.DeliveryCorpService#getDeliveryCorp(java
	 * .lang.String)
	 */
	@Override
	public List<DeliveryCorp> getDeliveryCorpByShop() {
		return deliveryCorpDao.getDeliveryCorpByShop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.DeliveryCorpService#getDeliveryCorp(java
	 * .lang.Long)
	 */
	@Override
	public DeliveryCorp getDeliveryCorp(Long id) {
		return deliveryCorpDao.getDeliveryCorp(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.DeliveryCorpService#delete(java.lang.
	 * Long)
	 */
	@Override
	public void deleteDeliveryCorp(DeliveryCorp deliveryCorp) {
		deliveryCorpDao.deleteDeliveryCorp(deliveryCorp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.DeliveryCorpService#save(com.legendshop
	 * .model.entity.DeliveryCorp)
	 */
	@Override
	public Long saveDeliveryCorp(DeliveryCorp deliveryCorp) {
		if (!AppUtils.isBlank(deliveryCorp.getDvyId())) {
			updateDeliveryCorp(deliveryCorp);
			return deliveryCorp.getDvyId();
		}
		return deliveryCorpDao.saveDeliveryCorp(deliveryCorp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.DeliveryCorpService#update(com.legendshop
	 * .model.entity.DeliveryCorp)
	 */
	@Override
	public void updateDeliveryCorp(DeliveryCorp deliveryCorp) {
		deliveryCorpDao.updateDeliveryCorp(deliveryCorp);
	}

	@Override
	public DeliveryCorp getDeliveryCorpByDvyId(Long dvyId) {
		return deliveryCorpDao.getDeliveryCorpByDvyId(dvyId);
	}

	@Override
	public PageSupport<DeliveryCorp> getDeliveryCorpPage(String curPageNO, DeliveryCorp deliveryCorp) {
		return deliveryCorpDao.getDeliveryCorpPage(curPageNO,deliveryCorp);
	}

	@Override
	public List<DeliveryCorp> getAllDeliveryCorp() {
		return deliveryCorpDao.getAllDeliveryCorp();
	}
	
}
