/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SystemConfig;
import org.springframework.cache.annotation.Cacheable;

/**
 * The Class SystemConfigDao.
 */

public interface SystemConfigDao extends GenericDao<SystemConfig, Long>{

//	@Cacheable(value = "SystemConfig")
	public abstract SystemConfig getSystemConfig();
	
	public abstract Long saveSystemConfig(SystemConfig systemConfig);

	public abstract void updateSystemConfig(SystemConfig systemConfig);
	
	/**
	 * @return
	 * 获取二维码
	 */
	@Cacheable(value = "SysWechatCode")
	public abstract SystemConfig getSysWechatCode();
	
	
 }
