package com.legendshop.business.service.presell;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.constant.StockCountingEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.spi.service.IPaymentResolver;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 预售订单服务
 */
@Service("preSellOrderPaymentResolver")
public class PreSellOrderPaymentResolver implements IPaymentResolver {

	@Autowired
	private PresellSubDao presellSubDao;

	@Autowired
	private SubSettlementDao subSettlementDao;

	@Autowired
	private SubSettlementItemDao subSettlementItemDao;

	@Autowired
	private SubDao subDao;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private StockService stockService;

	@Autowired
	private ExpensesRecordDao expensesRecordDao;
	
	@Autowired
	private SubHistoryService subHistoryService;
	
	@Resource(name="predepositPaySuccessProcessor")
	private EventProcessor<PredepositPaySuccess> predepositPaySuccessProcessor;

	@Override
	public Map<String, Object> queryPayto(Map<String, String> params) {

		String userId = params.get("userId");
		String subNumber = params.get("subNumber");
		Integer payPctType = Integer.valueOf(params.get("payPctType"));

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("result", false);

		Sub sub = subDao.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			response.put("message", "没有需要支付单据");
			return response;
		}

		PresellSub presellSub = presellSubDao.getPresellSub(userId, subNumber);
		if (AppUtils.isBlank(presellSub)) {
			response.put("message", "没有需要支付单据");
			return response;
		}

		String subject = "预售订单:" + presellSub.getSubNumber();
		Double totalAmount = 0d;
		if (payPctType == 1||payPctType == 0) { // 订金支付
			if (presellSub.getIsPayDeposit().intValue() == 1) {
				response.put("message", "请勿重复支付");
				return response;
			}
			totalAmount = presellSub.getPreDepositPrice().doubleValue();
			subject = "预售订单订金支付:" + presellSub.getSubNumber();
		} else {
			if (presellSub.getIsPayDeposit().intValue() == 0) {
				response.put("message", "请先支付订金");
				return response;
			}
			if (presellSub.getIsPayFinal().intValue() == 1) {
				response.put("message", "请勿重复支付");
				return response;
			}
			Date now = new Date();
			if (presellSub.getFinalMStart().after(now)) {
				response.put("message", "尾款支付时间未到");
				return response;
			}
			if (presellSub.getFinalMEnd().before(now)) {
				response.put("message", "尾款支付过期");
				return response;
			}
			totalAmount = Arith.add(presellSub.getFinalPrice().doubleValue(),sub.getFreightAmount());
			subject = "预售订单尾款支付:" + presellSub.getSubNumber();
		}
		response.put("result", true);
		response.put("subject", subject);
		response.put("totalAmount", totalAmount);
		return response;
	}

	/**
	 * 预售订单支付回调
	 */
	@Override
	public void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallback){
		PaymentLog.info("############################## 预售商品回调 ###########################");

		if (subSettlement.getIsClearing()) { // 没有清算
			PaymentLog.info("###################### doBankCallback subSettlementSn={} 订单已经支付请勿重复支付 ########################",
					subSettlement.getSubSettlementSn());
			
			return;
		}

		if (OrderStatusEnum.UNPAY.value().equals(bankCallback.getValidateStatus().value())) {
			// 没有付款
			PaymentLog.info("###################### doBankCallback subSettlementSn={} 开始处理 拍卖订单支付业务 ########################",
					subSettlement.getSubSettlementSn());
			Date now = new Date();
			subSettlement.setFlowTradeNo(bankCallback.getFlowTradeNo());
			subSettlement.setClearingTime(now);
			subSettlement.setIsClearing(true);
			int settlementResult = subSettlementDao.updateSubSettlementForPay(subSettlement);

			if (settlementResult == 0) {
				PaymentLog.info("Settlement record has settled。 TradeNo is  {}", bankCallback.getFlowTradeNo());
				return;
			}

			List<SubSettlementItem> subSettlementItems = subSettlementItemDao
					.getSubSettlementItems(subSettlement.getSubSettlementSn());
			if (subSettlementItems == null) {
				PaymentLog.error("支付异常,订单单据缺失, doBankCallback error subSettlementItems is null by subSettlementSn={}",
						subSettlement.getSubSettlementSn());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}

			SubSettlementItem settlementItem = subSettlementItems.get(0);
			
			Sub sub = subDao.getSubBySubNumberByUserId(settlementItem.getSubNumber(), subSettlement.getUserId());
			if (sub == null) {
				PaymentLog.error("支付异常,订单单据缺失, doBankCallback error sub is null by subSettlementSn={}",
						subSettlement.getSubSettlementSn());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}

			/*
			 * 判断是否使用全款预付款支付
			 */
			if (subSettlement.isFullPay()) {
				PaymentLog.info("############################## 使用全部的钱包支付 全部金额={}  ###########################", subSettlement.getPdAmount());
				PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(), subSettlement.getPdAmount(), subSettlement.getUserId(), subSettlement.getPrePayType());
				predepositPaySuccessProcessor.process(paySuccess);
				String result = paySuccess.getResult();
				if (!PreDepositErrorEnum.OK.value().equals(result)) {
					PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}

			} else if (subSettlement.getPdAmount() > 0) { // 部分使用钱包
				PaymentLog.info("############################## 使用部分的钱包支付  部分金额={} ###########################", subSettlement.getPdAmount());

				PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(), subSettlement.getPdAmount(), subSettlement.getUserId(), subSettlement.getPrePayType());
				predepositPaySuccessProcessor.process(paySuccess);
				String result = paySuccess.getResult();
				if (!PreDepositErrorEnum.OK.value().equals(result)) {
					PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
			}

			String recordRemark = "";
			
			SubHistory subHistory = new SubHistory();
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			if(PayPctTypeEnum.FRONT_MONEY.value().equals(subSettlement.getPayPctType())) {
				//1.支付订金支付
				String time = subHistory.DateToString(now);
				recordRemark=new StringBuilder().append("于").append(time)
						.append("完成预售订金支付,支付金额为:" + Arith.add(subSettlement.getCashAmount(), subSettlement.getPdAmount()) + ",订单流水号:" +
				sub.getSubNumber()).toString();
				
				
				PaymentLog.info("###################### 开始 处理 订金支付 sub by subNumber={} ########################",sub.getSubNumber());
				
				sub.setPayed(false);// 订金支付不更改订单状态
				sub.setPayDate(now);
				sub.setPayTypeId(subSettlement.getPayTypeId());
				sub.setPayTypeName(subSettlement.getPayTypeName());
				sub.setStatus(OrderStatusEnum.UNPAY.value());
				sub.setFlowTradeNo(bankCallback.getFlowTradeNo());
				sub.setSubSettlementSn(subSettlement.getSubSettlementSn());
				int result = subDao.updateSubForPay(sub, bankCallback.getValidateStatus().value()); // 更新为未支付状态
				if (result == 0) {
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				PaymentLog.info("###################### sub by subNumber={} 处理订金支付成功 ########################",
						sub.getSubNumber());
				
				//更新预售单状态
				subDao.update("update ls_presell_sub set deposit_pay_name=?,deposit_trade_no=?,deposit_pay_time=?,is_pay_deposit=? where sub_id=?",
						subSettlement.getPayTypeName(), bankCallback.getFlowTradeNo(), now, 1, sub.getSubId());
				
				
			}else if(PayPctTypeEnum.FINAL_MONEY.value().equals(subSettlement.getPayPctType())) {
				//2.尾款支付
				String time = subHistory.DateToString(now);
				StringBuilder sb = new StringBuilder().append("于").append(time).append("完成预售尾款支付,支付金额为:" + Arith.add(subSettlement.getCashAmount(),
						subSettlement.getPdAmount()) + ",订单流水号:" + sub.getSubNumber());
				recordRemark=sb.toString();
				
				PaymentLog.info("###################### 开始 处理 尾款支付 sub by subNumber={} ########################",sub.getSubNumber());
				
				sub.setPayed(true);// 尾款支付更改订单状态
				sub.setStatus(bankCallback.getUpdateStatus().value());
				// 更新 SubSettlementSn 为 尾款支付的
				sub.setSubSettlementSn(subSettlement.getSubSettlementSn());
				sub.setPayDate(now);
				int result = subDao.updateSubForPay(sub, bankCallback.getValidateStatus().value()); // 更新为未支付状态
				if (result == 0) {
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				
				//更新预售订单 update by jzh -> 订单到这一步时，整个订单已经支付完成了，所以在这里将预售订单尾款加上运费
				Double freightAmount = sub.getFreightAmount();
				if (AppUtils.isBlank(freightAmount)) {
					freightAmount = 0d;
				}
				subDao.update(
						"update ls_presell_sub set final_price=final_price+?, final_settlement_sn=?, final_pay_type_id = ?, final_pay_name= ?, final_trade_no=?,final_pay_time=?,is_pay_final=? where sub_id=?",
						freightAmount, subSettlement.getSubSettlementSn(),subSettlement.getPayTypeId(), subSettlement.getPayTypeName(), bankCallback.getFlowTradeNo(), now, 1, sub.getSubId());
				
				// 处理商品库存
				payDeposit(sub);
				
				PaymentLog.info("###################### sub by subNumber={} 处理 尾款支付成功 ########################",
						sub.getSubNumber());
				
			}else if(PayPctTypeEnum.FULL_PAY.value().equals(subSettlement.getPayPctType())) {
				
				PaymentLog.info("###################### 开始 处理预售订单全款支付 sub by subNumber={} ########################",sub.getSubNumber());
				
				//3. 全款支付
				String time = subHistory.DateToString(now);
				StringBuilder sb = new StringBuilder().append("于").append(time).append("完成预售全额支付,支付金额为:" + Arith.add(subSettlement.getCashAmount(),
						subSettlement.getPdAmount()) + ",订单流水号:" + sub.getSubNumber());
				recordRemark=sb.toString();
				
		
				sub.setPayed(true);// 订金支付不更改订单状态
				sub.setPayDate(now);
				sub.setPayTypeId(subSettlement.getPayTypeId());
				sub.setPayTypeName(subSettlement.getPayTypeName());
				sub.setStatus(bankCallback.getUpdateStatus().value());
				sub.setFlowTradeNo(bankCallback.getFlowTradeNo());
				sub.setSubSettlementSn(subSettlement.getSubSettlementSn());
				int result = subDao.updateSubForPay(sub, bankCallback.getValidateStatus().value()); // 更新为未支付状态
				if (result == 0) {
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				PaymentLog.info("###################### sub by subNumber={} 处理订金支付成功 ########################",
						sub.getSubNumber());
				
				//更新预售单状态
				subDao.update("update ls_presell_sub set deposit_pay_name=?, deposit_trade_no=?, deposit_pay_time=?, is_pay_deposit=? where sub_id=?",
						subSettlement.getPayTypeName(), bankCallback.getFlowTradeNo(), now, 1, sub.getSubId());
				
				// 处理商品库存
				payDeposit(sub);
				
				PaymentLog.info("###################### sub by subNumber={} 处理预售订单全款支付成功 ########################",
						sub.getSubNumber());
			}
			
			PaymentLog.info("###################### 开始保存订单历史 sub by subNumber={} ########################",sub.getSubNumber());
			
			subHistory.setRecDate(now);
			subHistory.setSubId(sub.getSubId());
			subHistory.setStatus(SubHistoryEnum.ORDER_PAY.value());
			subHistory.setUserName(sub.getUserName());
			subHistory.setReason(sub.getUserName() + recordRemark);
			
			subHistoryService.saveSubHistory(subHistory);
			
			// 异步处理
			ExpensesRecord expensesRecord = new ExpensesRecord();
			expensesRecord.setRecordDate(now);
			expensesRecord.setRecordMoney(-Arith.add(subSettlement.getCashAmount(), subSettlement.getPdAmount()));
			expensesRecord.setRecordRemark(recordRemark);
			expensesRecord.setUserId(sub.getUserId());
			expensesRecordDao.saveExpensesRecord(expensesRecord);

			PaymentLog.info(
					"###################### doBankCallback subSettlementSn={} 预售订单处理业务成功  ########################",
					subSettlement.getSubSettlementSn());
		}else {
			PaymentLog.warn(
					"###################### doBankCallback subSettlementSn={} 预售订单已经支付完毕，无需再次支付  ########################",
					subSettlement.getSubSettlementSn());
		}

	}


	/**
	 * 订金支付
	 */
	private void payDeposit(Sub sub) {
		List<SubItem> items = subItemDao.getSubItem(sub.getSubNumber(), sub.getUserId());
		if (AppUtils.isBlank(items)) {
			PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
			throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
		}
		String firstDistUserName = null;
		Map<Long, Long> map = new HashMap<Long, Long>();
		for (Iterator<SubItem> iterator = items.iterator(); iterator.hasNext();) {
			SubItem subItem = (SubItem) iterator.next();
			if (subItem == null)
				iterator.remove();
			Product product = productDao.getById(subItem.getProdId());
			if (StockCountingEnum.STOCK_BY_PAY.value().equals(product.getStockCounting())) {
				PaymentLog.info("###################### 付款减库存 by subItemId={},productId={} ########################",
						subItem.getSubItemId(), subItem.getProdId());
				boolean config = stockService.addHold(product.getProdId(), subItem.getSkuId(), subItem.getBasketCount());
				if (!config) {
					PaymentLog.error("预售订单订金支付出现超卖,请人工处理 , 订单号是: {}, 商品名字是: {}", sub.getSubNumber(),
							subItem.getProdName());
				}
			}
			//Jason 2018-12-12 预售商品数量，第一次sku 需要添加原来的buys
			Long buys = product.getBuys();
			if (map.containsKey(product.getProdId())) {
				Long target = map.get(product.getProdId());
				map.put(product.getProdId(), target + subItem.getBasketCount());
			} else {
				map.put(product.getProdId(), buys+subItem.getBasketCount());
			}
			if (firstDistUserName == null && AppUtils.isNotBlank(subItem.getDistUserName())) {
				firstDistUserName = subItem.getDistUserName();
			}
		}

		/**
		 * 可以异步处理 未免影响主流程
		 */
		for (Entry<Long, Long> entry1 : map.entrySet()) {
			productDao.updateBuys(entry1.getValue(), entry1.getKey());
		}
	}
}
