/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserGradeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.util.AppUtils;

/**
 * 用户等级Dao.
 */
@Repository
public class UserGradeDaoImpl extends GenericDaoImpl<UserGrade, Integer> implements UserGradeDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public UserGrade getUserGrade(Integer id) {
		return getById(id);
	}

	public void deleteUserGrade(UserGrade userGrade) {
		delete(userGrade);
	}

	public Integer saveUserGrade(UserGrade userGrade) {
		return (Integer) save(userGrade);
	}

	public void updateUserGrade(UserGrade userGrade) {
		update(userGrade);
	}

	@Override
	public List<StatusKeyValueEntity> retrieveUserGrade() {
		List<UserGrade> userGradeList = queryAll();
		if (AppUtils.isBlank(userGradeList)) {
			return null;
		}
		List<StatusKeyValueEntity> list = new ArrayList<StatusKeyValueEntity>(userGradeList.size());
		for (UserGrade userGrade : userGradeList) {
			list.add(new StatusKeyValueEntity(userGrade.getGradeId(), userGrade.getName()));
		}
		return list;
	}

	@Override
	public PageSupport<UserGrade> getUserGradePage(String curPageNO, UserGrade userGrade) {
		CriteriaQuery cq = new CriteriaQuery(UserGrade.class, curPageNO);
		if(AppUtils.isNotBlank(userGrade.getName())) {			
			cq.like("name","%" + userGrade.getName().trim() + "%");
		}
        cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

}
