package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.UserCenterPersonInfoDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.entity.MyPersonInfo;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.util.AppUtils;

@Repository
public class UserCenterPersonInfoDaoImpl  implements UserCenterPersonInfoDao {
	protected Logger logger = LoggerFactory.getLogger(UserCenterPersonInfoDaoImpl.class);
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	private String sqlForGetPersonInfo = " select ls.id,lud.nick_name as nickName ,lud.user_name as userName, lud.real_name as realName, lud.user_mail as userMail, lud.qq, lug.name as gradeName,	"+
			"lud.user_mobile as userMobile, lud.id_card as idCard, lud.user_adds as userAdds, lud.birth_date as birthDate, lud.sex ,lud.provinceid, lud.cityid, lud.areaid,	" +
			" lud.marry_status as marryStatus, lud.income_level as incomeLevel , lud.interest,lud.portrait_pic as portraitPic, lud.score as score, lud.available_predeposit as availablePredeposit  "+
			"  from ls_user ls,ls_usr_detail lud,ls_usr_grad lug 	where ls.id=lud.user_id and lug.grade_id = lud.grade_id   and lud.user_id=? ";
	
	private String sqlForUpdatePersonInfo = " update ls_usr_detail set nick_name = :nick_name, real_name= :real_name, sex= :sex,   qq= :qq, " +
			"birth_date= :birth_date,user_adds= :user_adds , id_card= :id_card,provinceid= :provinceid, cityid= :cityid,areaid= :areaid , marry_status= :marry_status,  income_level= :income_level, interest= :interest   where user_id = :user_id  ";
	
	private String sqlForGetUserGrade = "select lug.grade_id as gradeId,lug.name as name,lug.score as score,lud.grouth_value as grouthValue from ls_usr_detail lud,ls_usr_grad lug where lug.grade_id = lud.grade_id and lud.user_id = ?";
	
	public MyPersonInfo getPersonInfo(String userId) {
		return genericJdbcDao.get(sqlForGetPersonInfo, MyPersonInfo.class, userId);
	}


	public void updatePersonInfo(final MyPersonInfo myPersonInfo) {
		
		Map<String, Object> formMap = new HashMap<String, Object>(){{
			put("nick_name", myPersonInfo.getNickName());
			put("real_name", myPersonInfo.getRealName());	
			put("sex", myPersonInfo.getSex());	
			put("user_id", myPersonInfo.getId());	
			put("qq", myPersonInfo.getQq());	
			put("birth_date", myPersonInfo.getBirthDate());	
			put("user_adds", myPersonInfo.getUserAdds());	
			put("id_card", myPersonInfo.getIdCard());
			if(AppUtils.isBlank(myPersonInfo.getProvinceid())){
				put("provinceid", null);
			}else{
				put("provinceid", myPersonInfo.getProvinceid());
			}
			if(AppUtils.isBlank(myPersonInfo.getCityid())){
				put("cityid", null);
			}else{
				put("cityid", myPersonInfo.getCityid());
			}
			if(AppUtils.isBlank(myPersonInfo.getAreaid())){
				put("areaid", null);
			}else{
				put("areaid", myPersonInfo.getAreaid());
			}
			put("marry_status",myPersonInfo.getMarryStatus());
			put("income_level",myPersonInfo.getIncomeLevel());
			put("interest",myPersonInfo.getInterest());
		}};
		genericJdbcDao.updateNamedMap(sqlForUpdatePersonInfo, formMap);
	}
	
   /**
    * 更新支付密码和支付强度
    */
	public void updatePaymentPassword(String userName, String paymentPassword, Integer payStrength) {
		genericJdbcDao.update("update ls_usr_detail set pay_password = ? where user_name = ?",  paymentPassword, userName);
		genericJdbcDao.update("update ls_usr_security set pay_strength = ? where user_name = ?", payStrength, userName);
	}


	public boolean verifySMSCode(String userName, String validateCode) {
		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}
		if(AppUtils.isBlank(validateCode)){
			return false;
		}
		UserSecurity userSecurity = genericJdbcDao.get("select validate_code as  validateCode,   send_sms_time  as sendSmsTime  from ls_usr_security where user_name = ?", UserSecurity.class, userName);
		
		//判断下验证码时间
		Date end = new Date();
		long hour =( (userSecurity.getSendSmsTime().getTime()-end.getTime())/1000 )%(24*3600)/3600;
		if(hour >=1){
			return false;
		}
		
		return validateCode.equals(userSecurity.getValidateCode());
	}


	public UserGrade getUserGrade(String userId) {
		return genericJdbcDao.get(sqlForGetUserGrade, UserGrade.class, userId);
	}


	public String getNextGradeName(Integer gradeId) {
		return genericJdbcDao.getJdbcTemplate().queryForObject("select name from ls_usr_grad lug where lug.grade_id = (?+1);", String.class,gradeId);
	}

}
