/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.group;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.business.dao.*;
import com.legendshop.model.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.log.TimerLog;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ScheduleTypeEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.constant.SubGroupStatusEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.BackRefundResponseDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.ProdPicDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.SkuDto;
import com.legendshop.model.dto.group.GroupDto;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.spi.service.BackRefundProcessService;
import com.legendshop.spi.service.GroupService;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 团购服务.
 *
 */
@Service("groupService")
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProdPropImageDao prodPropImageDao;

	@Autowired
	private ImgFileService imgFileService;

	@Autowired
	private ScheduleListDao scheduleListDao;

	@Autowired
	private SubService subService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private BackRefundProcessService backRefundProcessService;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private  GroupSkuDao groupSkuDao;

	@Autowired
	private SubHistoryService subHistoryService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	public Group getGroup(Long id) {
		return groupDao.getGroup(id);
	}

	/**
	 * 删除团购
	 */
	public void deleteGroup(Group group) {
		Long prodId = group.getProductId();
		// 修改商品为团购商品
		groupDao.deleteGroup(group);

		productDao.updataIsGroup(prodId, 0);

		List<Sku> skus = skuDao.getSkuByProd(group.getProductId());
		//是否重置 sku营销状态
		Boolean isReSet = false;
		for (Sku sku : skus) {
			if(SkuActiveTypeEnum.GROUP.value().equals(sku.getSkuType())) {
				isReSet = true;
			}else {
				isReSet = false;
			}
		}
		if(isReSet) {
			//重置sku营销状态
			skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.GROUP.value());
		}
	}

	/**
	 * 保存团购
	 */
	public Long saveGroup(Group group) {
		if (!AppUtils.isBlank(group.getId())) {
			updateGroup(group);
			return group.getId();
		}
		return groupDao.saveGroup(group);
	}

	/**
	 * 更新团购
	 */
	public void updateGroup(Group group) {
		groupDao.updateGroup(group);
	}

	/**
	 *  查询团购数据
	 */
	public PageSupport<Group> getGroup(CriteriaQuery cq) {
		return groupDao.getGroup(cq);
	}

	/**
	 * 保存团购商品
	 */
	@Override
	public boolean saveGroupProd(Group group, Long shopId, String userName, String userId, String shopName, Long status) {
		if (AppUtils.isBlank(group) || !validateGroup(group)) {
			return false;
		}
		group.setShopName(shopName);
		group.setStatus(status.intValue());
		group.setShopId(shopId);
		group.setBuyerCount(0l);
		if (AppUtils.isNotBlank(group.getGroupProductList())){
			BigDecimal groupPrice = group.getGroupProductList().get(0).getGroupPrice();
			group.setGroupPrice(groupPrice);
		}
		group.setDeleteStatus(0);
		String pic = "";
		if (group.getGroupPicFile() != null && group.getGroupPicFile().getSize() > 0) {
			pic = attachmentManager.upload(group.getGroupPicFile());
			group.setGroupImage(pic);
		}

		Long result = groupDao.saveGroup(group);
		if (result > 0) {
			/*无论是否要审核，只要商家发布了该活动，商品都需绑定在该营销活动下
			 * if(group.getStatus() == 1){*/
				// 将商品的类型转为团购商品
				productDao.updataIsGroup(group.getProductId(), 1);
				//标记sku为为团购营销状态
				skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.GROUP.value(),SkuActiveTypeEnum.PRODUCT.value());
			/*}*/
			//保存团购商品
			for (GroupSku groupSku : group.getGroupProductList()) {
				groupSku.setGroupId(group.getId());
				groupSku.setShopId(group.getShopId());
				groupSkuDao.saveGroupSku(groupSku);
			}
		}

		if(AppUtils.isNotBlank(pic)){
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, pic, group.getGroupPicFile(), AttachmentTypeEnum.GROUP);
		}
		return result > 0;
	}

	/**
	 * 验证团购
	 *
	 * @param group
	 * @return
	 */
	private boolean validateGroup(Group group) {
		if (AppUtils.isBlank(group)) {
			return false;
		}
		if (AppUtils.isBlank(group.getGroupName())) {
			return false;
		}
		if (AppUtils.isBlank(group.getProductId())) {
			return false;
		}
		if (AppUtils.isBlank(group.getBuyQuantity())) {
			return false;
		}
		if (AppUtils.isBlank(group.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(group.getEndTime())) {
			return false;
		}
		return true;
	}

	/**
	 * 更新团购数据
	 */
	@Override
	public boolean updateGroup(Group group, Group oldGroup, Long shopId, String userName, String userId, Long status) {
		if (AppUtils.isBlank(group) || !validateGroup(group)) {
			return false;
		}
		//重置sku营销状态
		if(GroupStatusEnum.FINISH.value().equals(status) ) {//完成状态
			skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.GROUP.value());
		}else if(GroupStatusEnum.VALIDATING.value().equals(status)) { //审核中，修改时进来
			if(oldGroup.getProductId() != group.getProductId()) {//修改了商品
				groupSkuDao.deleteGroupSkuByGroupId(group.getId());
				// 将商品的类型转为普通商品
				productDao.updataIsGroup(oldGroup.getProductId(), 0);

				//先恢复原来sku的营销状态，重新选择商品
				skuDao.updateSkuTypeByProdId(oldGroup.getProductId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.GROUP.value());
				skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.GROUP.value(),SkuActiveTypeEnum.PRODUCT.value());
				//保存团购商品
				for (GroupSku groupSku : group.getGroupProductList()) {
					groupSku.setGroupId(group.getId());
					groupSku.setShopId(shopId);
					groupSkuDao.saveGroupSku(groupSku);
				}
			}
		}
		oldGroup.setGroupName(group.getGroupName());
		oldGroup.setProductId(group.getProductId());
		oldGroup.setCategoryName(group.getCategoryName());
		oldGroup.setBuyQuantity(group.getBuyQuantity());
		oldGroup.setGroupPrice(group.getGroupPrice());
		oldGroup.setStartTime(group.getStartTime());
		oldGroup.setEndTime(group.getEndTime());
		oldGroup.setStatus(status.intValue());
		oldGroup.setGroupDesc(group.getGroupDesc());
		oldGroup.setPeopleCount(group.getPeopleCount());
		if (AppUtils.isNotBlank(group.getGroupProductList())){
			BigDecimal groupPrice = group.getGroupProductList().get(0).getGroupPrice();
			oldGroup.setGroupPrice(groupPrice);
		}
		String pic = "";
		if (group.getGroupPicFile().getSize() > 0) {
			pic = attachmentManager.upload(group.getGroupPicFile());
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId,shopId, pic, group.getGroupPicFile(), AttachmentTypeEnum.GROUP);
			// 删除原引用
			if (AppUtils.isNotBlank(oldGroup.getGroupImage())) {
				attachmentManager.delAttachmentByFilePath(oldGroup.getGroupImage());
				attachmentManager.deleteTruely(oldGroup.getGroupImage());
			}
			oldGroup.setGroupImage(pic);
		}
		return groupDao.updateGroup(oldGroup) > 0;
	}

	/**
	 * 获取团购
	 */
	@Override
	public Group getGroup(Long shopId, Long id) {
		return groupDao.getGroup(shopId, id);
	}

	/**
	 * 获取团购类目
	 */
	@Override
	public List<Category> findgroupCategory() {
		List<Category> categories = groupDao.findgroupCategory();
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		return categories;
	}

	/**
	 * 获取GroupDto
	 */
	@Override
	public GroupDto getGroupDto(Long id) {
		Group group = groupDao.getGroup(id);

		if (AppUtils.isBlank(group)) {
			return null;
		}
		Long prodId = group.getProductId();

		ProductDetail prod = productDao.getProdDetail(prodId);
		if (prod == null || !ProductStatusEnum.PROD_ONLINE.value().equals(prod.getStatus())) {
			return null;
		}
		// 设置属性
		GroupDto groupDto = new GroupDto();
		groupDto.setId(group.getId());
		groupDto.setGroupName(group.getGroupName());
		groupDto.setGroupPrice(group.getGroupPrice());
		groupDto.setStartTime(group.getStartTime());
		groupDto.setEndTime(group.getEndTime());
		groupDto.setGroupDesc(group.getGroupDesc());
		groupDto.setBuyQuantity(group.getBuyQuantity());
		groupDto.setProductId(group.getProductId());
		groupDto.setShopId(group.getShopId());
		groupDto.setStatus(group.getStatus());
		groupDto.setPeopleCount(group.getPeopleCount());
		groupDto.setBuyerCount(group.getBuyerCount());
		groupDto.setGroupImage(group.getGroupImage());

		ProductDto prodDto = new ProductDto();
		copyProdField(prodDto, prod);

		// 获取商品sku信息
		List<SkuDto> skuDtoList = new ArrayList<SkuDto>();
		List<Sku> skuList = skuService.getSkuByProd(prodId);
		if (AppUtils.isNotBlank(skuList)) {
			for (Sku sku : skuList) {
				SkuDto skuDto = new SkuDto();
				skuDto.setSkuId(sku.getSkuId());
				skuDto.setCash(sku.getPrice());
				skuDto.setName(processName(sku.getName()));
				skuDto.setPrice(groupSkuDao.getGropPrice(group.getId(),prodId,sku.getSkuId()));
				skuDto.setStocks(sku.getStocks());
				skuDto.setProperties(sku.getProperties());
				skuDto.setStatus(sku.getStatus());
				skuDtoList.add(skuDto);
			}
			prodDto.setSkuDtoList(skuDtoList);
			prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));
		}

		// 获取属性图片
		List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
		Map<Long, List<String>> valueImagesMap = new HashMap<Long, List<String>>();
		if (AppUtils.isNotBlank(skuList)) {
			// 获取到商品 所有的属性图片
			List<ProdPropImage> prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
			if (AppUtils.isNotBlank(prodPropImages)) {
				// 根据属性值 进行分组,封装成map
				for (ProdPropImage prodPropImage : prodPropImages) {
					Long valueId = prodPropImage.getValueId();
					List<String> imgs = valueImagesMap.get(valueId);
					if (AppUtils.isBlank(imgs)) {
						imgs = new ArrayList<String>();
					}
					imgs.add(prodPropImage.getUrl());
					valueImagesMap.put(valueId, imgs);
				}
				// 将map转换成 DTO LIST
				Iterator<Long> it = valueImagesMap.keySet().iterator();
				while (it.hasNext()) {
					Long valId = it.next();
					List<String> imgs = valueImagesMap.get(valId);
					PropValueImgDto propValueImgDto = new PropValueImgDto();
					propValueImgDto.setValueId(valId);
					propValueImgDto.setImgList(imgs);
					propValueImgList.add(propValueImgDto);
				}
			}
			prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));
		}
		// 获取商品属性和属性值
		List<ProductPropertyDto> prodPropDtoList = skuService.getPropValListByProd(prodDto, skuList, valueImagesMap);
		prodDto.setProdPropDtoList(prodPropDtoList);
		prodDto.setPropValueImgList(propValueImgList);

		// 商品图片列表(没有属性图片时，展示商品图片)
		List<ProdPicDto> prodPics = new ArrayList<ProdPicDto>();
		if (AppUtils.isBlank(propValueImgList)) {
			List<ImgFile> imgFileList = imgFileService.getAllProductPics(prodId);
			if (AppUtils.isNotBlank(imgFileList)) {
				for (ImgFile imgFile : imgFileList) {
					ProdPicDto prodPicDto = new ProdPicDto();
					prodPicDto.setFilePath(imgFile.getFilePath());
					prodPics.add(prodPicDto);
				}
			}
		} else {
			ProdPicDto prodPicDto = new ProdPicDto();
			prodPicDto.setFilePath(prodDto.getPic());
			prodPics.add(prodPicDto);
		}
		prodDto.setProdPics(prodPics);
		groupDto.setProductDto(prodDto);

		Date nowDate = new Date();
		if (nowDate.getTime() > groupDto.getEndTime().getTime()) {// 如果当前时间大于结束时间
			groupDao.updataStatus(id, GroupStatusEnum.FINISH.value());
		}
		return groupDto;
	}

	/**
	 * 拷贝部分字段值
	 *
	 * @param prodDto
	 * @param prodDetail
	 */
	private void copyProdField(ProductDto prodDto, ProductDetail prodDetail) {
		prodDto.setProdId(prodDetail.getProdId());
		prodDto.setName(processName(prodDetail.getName()));
		prodDto.setCash(prodDetail.getCash());
		prodDto.setPic(prodDetail.getPic());
		prodDto.setBrief(prodDetail.getBrief());
		prodDto.setModelId(prodDetail.getModelId());
		prodDto.setStatus(prodDetail.getStatus());
		prodDto.setContent(prodDetail.getContent());
		prodDto.setStocks(prodDetail.getStocks());
		prodDto.setProVideoUrl(prodDetail.getProVideoUrl());
		prodDto.setCategoryId(prodDetail.getCategoryId());
	}

	/**
	 * 处理name
	 * @param name
	 * @return
	 */
	private String processName(String name) {
		if (name == null) {
			return null;
		}
		name = name.replace("\"", "〞");
		name = name.replace("'", "’");
		return name;
	}

	/**
	 * 团购数据
	 */
	@Override
	public PageSupport<Group> getGroupPage(String curPageNO, Group group) {

		PageSupport<Group> ps = groupDao.getGroupPage(curPageNO, group);

		//根据查询出来的团购商品信息，查询商品状态，返回
		List<Group> groupList = ps.getResultList();
		if (AppUtils.isNotBlank(groupList)) {

			for (Group groupDetail : groupList) {
				Product product = productDao.getProduct(groupDetail.getProductId());

				if (AppUtils.isNotBlank(product)) {
					groupDetail.setProdStatus(product.getStatus());
				}
			}
		}
		return ps;
	}

	/**
	 * 获取团购列表
	 */
	@Override
	public PageSupport<Group> queryGouplist(String curPageNO, GroupProdSeachParam param) {
		return groupDao.queryGouplist(curPageNO, param);
	}

	/**
	 * 查询团购数据
	 */
	@Override
	public PageSupport<Group> queryGroupPage(String curPageNO, Integer firstCid, Integer twoCid, Integer thirdCid) {
		return groupDao.queryGroupPage(curPageNO, firstCid, twoCid, thirdCid);
	}

	/**
	 * 查询团购数据
	 */
	@Override
	public PageSupport<Group> queryGroupList(String curPageNO, GroupProdSeachParam param) {
		return groupDao.queryGroupList(curPageNO, param);
	}

	/**
	 * 查询团购数据
	 */
	@Override
	public PageSupport<Group> getGroupPage(String curPageNO, DataSortResult result, Group group) {
		return groupDao.getGroupPage(curPageNO, result, group);
	}

	@Override
	public int batchOffline(String ids) {
		String[] groupIdList=ids.split(",");
		Integer result=0;
		for(String groupId:groupIdList){
			Group group = this.getGroup(Long.valueOf(groupId));
			if (group.getStatus() == 1) {
				group.setStatus(0);
				this.updateGroup(group);
				// 将商品的类型转为普通商品
				productDao.updataIsGroup(group.getProductId(), 0);
			}
			result++;
		}
		return result;
	}

	/**
	 * 审核团购,oldGroup 从数据库查询出来, newGroup是指页面传递过来的对象
	 */
	@Override
	public void auditGroup(Group oldGroup, Group newGroup) {

		if (GroupStatusEnum.ONLINE.value().intValue() == newGroup.getStatus().intValue()) {
			productDao.updataIsGroup(oldGroup.getProductId(), 1);
			insertScheduleList(oldGroup,ScheduleTypeEnum.TUTO_RETURN_GROUP.value());
		}

		//如果审核不通过，则释放sku
		if (GroupStatusEnum.FAILED.value().intValue() == newGroup.getStatus().intValue()) {
			// 将商品的类型转为普通商品
			productDao.updataIsGroup(oldGroup.getProductId(), 0);
			//取消sku团购营销状态
			skuDao.updateSkuTypeByProdId(oldGroup.getProductId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.GROUP.value());
		}

		oldGroup.setStatus(newGroup.getStatus());
		oldGroup.setAuditTime(new Date());
		oldGroup.setAuditOpinion(newGroup.getAuditOpinion());
		groupDao.updateGroup(oldGroup);
	}

	/**
	 * 加入调度任务
	 * @param group
	 * @param type
	 */
	private void insertScheduleList(Group group, Integer type) {
		Date date = new Date();
		ScheduleList scheduleList = new ScheduleList();
		scheduleList.setScheduleJson("");
		scheduleList.setScheduleSn(String.valueOf(group.getId()));
		scheduleList.setScheduleType(type);
		scheduleList.setShopId(group.getShopId());
		scheduleList.setStatus(0);
		scheduleList.setTriggerTime(group.getEndTime());
		scheduleList.setCreateTime(date);
		scheduleListDao.save(scheduleList);

	}

	/**
	 * 团购下线
	 */
	@Override
	public String offlineGroup(Group group) {

		int count = 0;
		/**
		 * 查询所有 已支付 未成团成功的参与团购订单;
		 */
	    List<Sub> groupSubLists= subService.findHaveInGroupSub(group.getId(),SubGroupStatusEnum.MERGE_GROUP_IN.value());
	    int groupNumber=0;
	    if (AppUtils.isNotBlank(groupSubLists)){
	    	count = groupSubLists.size();
	    	for(Sub sub:groupSubLists ){
				TimerLog.info("######### groupSubList={} ########",JSON.toJSONString(groupSubLists));
				/**
				 * 处理拍卖保证金的原路退还操作
				 */
				try {

					BackRefundResponseDto refundResponseDto=backRefundProcessService.backRefund("COMMON_GROUP",sub.getSubNumber());

					if(refundResponseDto==null){
						TimerLog.error("处理原路退还操作失败 = {}", JSON.toJSONString(sub.getSubNumber()));
					}else{
						if(!refundResponseDto.isResult()){
							TimerLog.warn("######### 退款中心反馈退款结果={} ########",refundResponseDto.getMessage());
						}
					}
					sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", sub.getUserName(),"您参加的团购活动已下线" ,"团购活动已下线,资金将原路返回." ).getSource());
				} catch (Exception e) {
					TimerLog.error("处理原路退还操作 异常", e);
				}
				groupNumber++;
	    	}
	    }

	    //处理未支付的参团订单
	    List<Sub> unpaySubLists = subService.findHaveInGroupSub(group.getId(),SubGroupStatusEnum.UNPAY.value());
	    if(AppUtils.isNotBlank(unpaySubLists)){
	    	count += unpaySubLists.size();
	    	for (Sub sub : unpaySubLists) {

	    		Date date = new Date();
				sub.setUpdateDate(date);
				sub.setGroupStatus(SubGroupStatusEnum.MERGE_GROUP_FAIL.value());
				sub.setStatus(OrderStatusEnum.CLOSE.value());
				sub.setCancelReason("因参加的团购活动已下线,取消订单！");
				boolean result= subService.cancleOrder(sub);
				if(result){
					SubHistory subHistory = new SubHistory();
					String time = subHistory.DateToString(date);
					subHistory.setRecDate(date);
					subHistory.setSubId(sub.getSubId());
					subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
					StringBuilder sb=new StringBuilder();
					sb.append("系统于").append(time).append("因参加的团购活动已下线,取消订单 ");
					subHistory.setUserName("系统");
					subHistory.setReason(sb.toString());
					subHistoryService.saveSubHistory(subHistory);
					delayCancelHelper.remove(sub.getSubNumber());
				}
	    		groupNumber++;
			}
	    }

		if (groupNumber == count) { // 全部退款处理成功,更新服务
			group.setStatus(GroupStatusEnum.OFFLINE.value().intValue());
			groupDao.updateGroup(group);
			// 将商品的类型转为普通商品
			productDao.updataIsGroup(group.getProductId(), 0);

			//重置sku的营销标记
			//groupSkuDao.deleteGroupSkuByGroupId(group.getId()); //团购活动失效后，需要保留所绑定商品以供查看详情信息
			skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.GROUP.value());
			scheduleListDao.cancelScheduleList(group.getShopId(), String.valueOf(group.getId()), ScheduleTypeEnum.TUTO_RETURN_GROUP.value());//关闭定时任务
			TimerLog.info("######### 更新团购活动的状态 ########");
		}


		return Constants.SUCCESS;
	}

	@Override
	public boolean findIfJoinGroup(Long productId) {
		return groupDao.findIfJoinGroup(productId);
	}

	/*  上下线
	 */
	@Override
	public String updateGroup(Group groupPo, Integer status) {
		Date nowDate = new Date();
		if (GroupStatusEnum.OFFLINE.value().equals(status.longValue())) {
			/* 下线：未开始才能下线 */
			if (groupPo.getStartTime().after(nowDate)) {
				groupPo.setStatus(status);
				this.updateGroup(groupPo);
				//重置sku的营销标记
				skuDao.updateSkuTypeByProdId(groupPo.getProductId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.GROUP.value());
				// 将商品的类型转为普通商品
				productDao.updataIsGroup(groupPo.getProductId(), 0);
				return Constants.SUCCESS;
			} else {
				return "此活动还在活动进行中,不能下线操作";
			}
		} else if (GroupStatusEnum.ONLINE.value().equals(status.longValue())) {
			/* 上线：未过期才能上线(发布) */
			if (groupPo.getEndTime().after(nowDate)) {
				groupPo.setStatus(status);
				this.updateGroup(groupPo);
				// 将商品的类型转为团购商品
				productDao.updataIsGroup(groupPo.getProductId(), 1);
				return Constants.SUCCESS;
			} else {
				return "此活动已过期,不能上线操作";
			}
		}else  if (GroupStatusEnum.FINISH.value().equals(status.longValue())) { //完成
			//重置sku的营销标记
			skuDao.updateSkuTypeByProdId(groupPo.getProductId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.GROUP.value());
		}
		return null;
	}

	/**
	 * 逻辑删除团购活动
	 */
	@Override
	public void updateDeleteStatus(Long shopId,Group group, Integer deleteStatus) {

		Long prodId = group.getProductId();
		// 修改商品为团购商品
		productDao.updataIsGroup(prodId, 0);

		List<Sku> skus = skuDao.getSkuByProd(group.getProductId());
		//是否重置 sku营销状态
		Boolean isReSet = false;
		for (Sku sku : skus) {
			if(SkuActiveTypeEnum.GROUP.value().equals(sku.getSkuType())) {
				isReSet = true;
			}else {
				isReSet = false;
			}
		}
		if(isReSet) {
			//重置sku营销状态
			skuDao.updateSkuTypeByProdId(group.getProductId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.GROUP.value());
		}
		groupDao.updateDeleteStatus(shopId,group.getId(),deleteStatus);
	}

	/**
	 * 运营数据统计
	 */
	@Override
	public OperateStatisticsDTO getOperateStatistics(Long groupId) {

		// 获取交易总金额、参团人数、成团人数
		OperateStatisticsDTO operate = groupDao.getTotalAmount(groupId);

		// 获取活动交易成功的金额
		Double succeedAmount =  groupDao.getSucceedAmount(groupId);
		operate.setSucceedAmount(succeedAmount);

		return operate;
	}

}
