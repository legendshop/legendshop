/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.integral;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.integral.IntegralOrderItemDao;
import com.legendshop.model.entity.integral.IntegralOrderItem;
import com.legendshop.spi.service.IntegralOrderItemService;
import com.legendshop.util.AppUtils;

/**
 * 积分订单项目服务实现类.
 */
@Service("integralOrderItemService")
public class IntegralOrderItemServiceImpl implements IntegralOrderItemService {

	@Autowired
	private IntegralOrderItemDao integralOrderItemDao;

	/**
	 * 获取积分订单项目
	 */
	public IntegralOrderItem getIntegralOrderItem(Long id) {
		return integralOrderItemDao.getIntegralOrderItem(id);
	}

	/**
	 * 删除积分订单项目
	 */
	public void deleteIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		integralOrderItemDao.deleteIntegralOrderItem(integralOrderItem);
	}

	/**
	 * 保存积分订单项目
	 */
	public Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		if (!AppUtils.isBlank(integralOrderItem.getId())) {
			updateIntegralOrderItem(integralOrderItem);
			return integralOrderItem.getId();
		}
		return integralOrderItemDao.saveIntegralOrderItem(integralOrderItem);
	}

	/**
	 * 更新积分订单项目
	 */
	public void updateIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		integralOrderItemDao.updateIntegralOrderItem(integralOrderItem);
	}

	/**
	 * 根据用户id和商品id获取积分订单项目
	 */
	@Override
	public List<IntegralOrderItem> queryByUserIdAndProdId(String userId, Long prodId) {
		return integralOrderItemDao.queryByUserIdAndProdId(userId, prodId);
	}
}
