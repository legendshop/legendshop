/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 *专题板块
 */
public interface ThemeModuleDao extends Dao<ThemeModule, Long> {
     
    public abstract List<ThemeModule> getThemeModuleByTheme(Long themeId);

	public abstract ThemeModule getThemeModule(Long id);
	
    public abstract int deleteThemeModule(ThemeModule themeModule);
	
	public abstract Long saveThemeModule(ThemeModule themeModule);
	
	public abstract int updateThemeModule(ThemeModule themeModule);
	
	public abstract void deleteThemeModule(List<ThemeModule> objModules);
	
	/**
	 * 查询模块相关产品
	 */
	List<ThemeModuleProd> findModuleProdsByModuleId(Long moduleId);
	
 }
