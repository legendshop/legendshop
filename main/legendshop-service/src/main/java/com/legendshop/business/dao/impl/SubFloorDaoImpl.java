/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubFloorDao;
import com.legendshop.business.dao.SubFloorItemDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.util.AppUtils;

/**
 * The Class SubFloorDaoImpl.
 */

@Repository
public class SubFloorDaoImpl extends GenericDaoImpl<SubFloor, Long> implements SubFloorDao {

	@Autowired
	private SubFloorItemDao subFloorItemDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	private static String sqlForGetSubFloor = "select sf.sf_id as sfId, sf.name, sf.rec_date as recDate, sf.status, sf.floor_id as floorId, sf.seq from ls_sub_floor sf, ls_floor f where sf.floor_id = f.fl_id";

	private static String sqlForGetSubFloorAndProduct = "select sf.sf_id as sfId, sf.name, sf.rec_date as recDate, sf.status, sf.floor_id as floorId, sf.seq from ls_sub_floor sf, ls_floor f where  sf.status = 1 and sf.floor_id = f.fl_id  order by f.seq, sf.seq";
	
	private static String sqlForGetSubFloorAndProductByFid = "select sf.sf_id as sfId, sf.name, sf.rec_date as recDate, sf.status, sf.floor_id as floorId, sf.seq from ls_sub_floor sf, ls_floor f where  sf.status = 1 and sf.floor_id = f.fl_id and floor_id=? order by f.seq, sf.seq";

	public List<SubFloor> getSubFloorByShopId() {
		return query(sqlForGetSubFloor, SubFloor.class);
	}

	/**
	 * 拿到某用户子楼层下的所有产品
	 */
	@Override
	public List<SubFloor> getSubFloorAndProduct() {

		List<SubFloor> subFloorList = query(sqlForGetSubFloorAndProduct, SubFloor.class);
		
		if (AppUtils.isNotBlank(subFloorList)) {
			List<SubFloorItem> subFloorItemList = subFloorItemDao.queryItem(subFloorList);
			if (AppUtils.isNotBlank(subFloorItemList)) {
				for (SubFloor subFloor : subFloorList) {
					for (SubFloorItem subFloorItem : subFloorItemList) {
						subFloor.addSubFloorItem(subFloorItem);
					}
				}
			}
		}
		return subFloorList;
	}
	
	@Override
	public List<SubFloor> getSubFloorAndProduct(Long floorId) {
       
		List<SubFloor> subFloorList = query(sqlForGetSubFloorAndProductByFid, SubFloor.class, new Object[]{floorId});
		
		if (AppUtils.isNotBlank(subFloorList)) {
			List<SubFloorItem> subFloorItemList = subFloorItemDao.queryItem(subFloorList);
			if (AppUtils.isNotBlank(subFloorItemList)) {
				for (SubFloor subFloor : subFloorList) {
					for (SubFloorItem subFloorItem : subFloorItemList) {
						subFloor.addSubFloorItem(subFloorItem);
					}
				}
			}
		}
		return subFloorList;
	}
	

	/**
	 * 需要增加参数楼层ID public List<SubFloor> getAllSubFloor(Long floorId)
	 */
	public List<SubFloor> getAllSubFloor(List<Floor> floorList) {
		if (AppUtils.isBlank(floorList)) {
			return null;
		}
		StringBuilder sql = new StringBuilder(
				"select sf.sf_id as sfId, sf.name, sf.rec_date as recDate, sf.status, sf.floor_id as floorId, sf.seq from ls_sub_floor sf, ls_floor f where sf.floor_id = f.fl_id and f.fl_id in (");
		Object[] floorIds = new Object[floorList.size()];
		int size = floorList.size() - 1;
		for (int i = 0; i < size; i++) {
			floorIds[i] = floorList.get(i).getFlId();
			sql.append("?,");
		}
		sql.append("?)");
		floorIds[size] = floorList.get(size).getFlId();
		return query(sql.toString(), SubFloor.class, floorIds);
	}

	public SubFloor getSubFloor(Long id) {
		return getById(id);
	}

	public void deleteSubFloor(SubFloor subFloor) {
		delete(subFloor);
	}

	public Long saveSubFloor(SubFloor subFloor) {
		return (Long) save(subFloor);
	}

	public void updateSubFloor(SubFloor subFloor) {
		update(subFloor);
	}

	public PageSupport getSubFloor(CriteriaQuery cq) {
		return queryPage(cq);
	}

	public List<SubFloor> getSubFloorByfId(Long id) {
		return this.queryByProperties(new EntityCriterion().eq("floorId", id));
	}

	/**
	 * 检查权限
	 */
	@Override
	public boolean checkPrivilege(String userName, Long floorId) {
		String name = get("select user_name from ls_floor where fl_id = ?", String.class, floorId);
		if (!userName.equals(name)) {
			return false;
		} else {
			return true;
		}
	}


	@Override
	public void deleteSubFloorItems(Long subFloorId) {
		update("delete from ls_sub_floor_item where sf_id = ?", subFloorId);
	}

	@Override
	public PageSupport<SubFloor> getSubFloorPage(String curPageNO, SubFloor subFloor) {
		CriteriaQuery cq = new CriteriaQuery(SubFloor.class, curPageNO);
	    cq.eq("name", subFloor.getName());
	    cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}


}
