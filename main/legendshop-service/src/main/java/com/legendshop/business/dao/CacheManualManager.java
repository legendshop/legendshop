package com.legendshop.business.dao;

import com.legendshop.model.entity.ShopDetail;

public interface CacheManualManager {
	/**
	 * 清除用户缓存
	 * @param userId 必填项
	 * @param userName 必填项
	 */
	public void clearUserDetailCache(String userId, String userName);
	
	/**
	 * 清除商城缓存
	 */
	public void clearShopDetailCache(ShopDetail shopDetail);
}
