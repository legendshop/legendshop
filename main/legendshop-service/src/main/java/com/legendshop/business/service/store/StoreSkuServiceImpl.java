/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.store;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.store.StoreSkuDao;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.spi.service.StoreSkuService;
import com.legendshop.util.AppUtils;

/**
 * 门店的单品Sku服务实现类
 */
@Service("storeSkuService")
public class StoreSkuServiceImpl  implements StoreSkuService{
    
	@Autowired
	private StoreSkuDao storeSkuDao;

    /**
     * 获取商品Sku 
     */
    public StoreSku getStoreSku(Long id) {
        return storeSkuDao.getStoreSku(id);
    }

    /**
     * 删除商品Sku 
     */
    public void deleteStoreSku(StoreSku storeSku) {
        storeSkuDao.deleteStoreSku(storeSku);
    }

    /**
     * 保存商品Sku 
     */
    public Long saveStoreSku(StoreSku storeSku) {
        if (!AppUtils.isBlank(storeSku.getId())) {
            updateStoreSku(storeSku);
            return storeSku.getId();
        }
        return storeSkuDao.saveStoreSku(storeSku);
    }

    /**
     * 更新商品Sku 
     */
    public void updateStoreSku(StoreSku storeSku) {
        storeSkuDao.updateStoreSku(storeSku);
    }

	/**
	 *  加载商品的sku列表
	 */
	@Override
	public List<StoreSku> getStoreSku(Long storeId, Long prodId) {
		return storeSkuDao.getStoreSku(storeId, prodId);
	}

	/**
	 *  加载商品的sku列表
	 */
	@Override
	public StoreSku getStoreSkuBySkuId(Long storeId, Long skuId) {
		return storeSkuDao.getStoreSkuBySkuId(storeId, skuId);
	}
	
}
