/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SensitiveWordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.SensitiveWord;
import com.legendshop.util.AppUtils;
/**
 * 敏感字过滤Dao
 */
@Repository
public class SensitiveWordDaoImpl extends GenericDaoImpl<SensitiveWord, Long> implements SensitiveWordDao {

	public SensitiveWord getSensitiveWord(Long id){
		return getById(id);
	}
	
    public void deleteSensitiveWord(SensitiveWord sensitiveWord){
    	delete(sensitiveWord);
    }
	
	public Long saveSensitiveWord(SensitiveWord sensitiveWord){
		return (Long)save(sensitiveWord);
	}
	
	public void updateSensitiveWord(SensitiveWord sensitiveWord){
		 update(sensitiveWord);
	}
	
	@Override
	public List<String> getWords() {
		QueryMap map = new QueryMap();
		String queryWordsSQL = ConfigCode.getInstance().getCode("biz.getWords",map);
		List<String> list=getJdbcTemplate().queryForList(queryWordsSQL, map.toArray(), String.class);
		return list;
	}

	@Override
	public boolean checkSensitiveWordsNameExist(String nickName) {
		String sql = "SELECT COUNT(1) FROM ls_sens_word WHERE words=?";
		Long num = this.getLongResult(sql, nickName);
		if(num>0){
			return true;
		}
		return false;
	}

	@Override
	public PageSupport<SensitiveWord> getSensitiveWord(String curPageNO, SensitiveWord sensitiveWord) {
		CriteriaQuery cq = new CriteriaQuery(SensitiveWord.class,curPageNO);
		if(AppUtils.isBlank(sensitiveWord.getWords())){
			cq.eq("words",sensitiveWord.getWords());
		}else{
			cq.like("words","%"+sensitiveWord.getWords().trim()+"%");
		}
		return queryPage(cq);
	}

 }
