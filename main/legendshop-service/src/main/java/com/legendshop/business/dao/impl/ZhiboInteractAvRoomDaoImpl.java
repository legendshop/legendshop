/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ZhiboInteractAvRoomDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ZhiboAvRoom;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.util.AppUtils;

/**
 * The Class ZhiboInteractAvRoomDaoImpl.
  * Dao实现类
 */
@Repository
public class ZhiboInteractAvRoomDaoImpl extends GenericDaoImpl<ZhiboInteractAvRoom, String> implements ZhiboInteractAvRoomDao  {

   	/**
	 *  根据Id获取
	 */
	public ZhiboInteractAvRoom getZhiboInteractAvRoom(String id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom){
    	return delete(zhiboInteractAvRoom);
    }

   /**
	 *  保存
	 */		
	public String saveZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom){
		return save(zhiboInteractAvRoom);
	}

   /**
	 *  更新
	 */		
	public int updateZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom){
		return update(zhiboInteractAvRoom);
	}

	
	public int createRoom(String uid){
		return update("INSERT INTO ls_zhibo_av_room (uid) VALUES (?)",uid);
	}
	
	public ZhiboAvRoom getLastAvRoom(String uid){
		return get("SELECT   *   FROM   ls_zhibo_av_room  WHERE uid=? ORDER   BY   id   DESC   LIMIT   1",ZhiboAvRoom.class,uid);
	}
	
	public int enterRoom(String uid,Long av_room_id,String status,Date modify_time,Long role){
		return update("REPLACE INTO ls_zhibo_interact_av_room (uid, av_room_id, status, modify_time, role) VALUES (?, ?, ?, ?, ?)",uid, av_room_id, status, modify_time, role);
	}
	
	public int updateRoomInfoById(String uid,Long av_room_id,String title,String cover){
		return update("update ls_zhibo_av_room set title=?,cover=? where uid=? and id=?",title, cover, uid, av_room_id);
	}
	
	public int saveNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord){
		String sql = "REPLACE INTO ls_zhibo_new_live_record (";
		String valueSql = "";
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getId())){
			paramMap.put("id", zhiboNewLiveRecord.getId());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getCreateTime())){
			paramMap.put("create_time", zhiboNewLiveRecord.getCreateTime());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getModifyTime())){
			paramMap.put("modify_time", zhiboNewLiveRecord.getModifyTime());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getAppid())){
			paramMap.put("appid", zhiboNewLiveRecord.getAppid());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getTitle())){
			paramMap.put("title", zhiboNewLiveRecord.getTitle());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getCover())){
			paramMap.put("cover", zhiboNewLiveRecord.getCover());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getHostUid())){
			paramMap.put("host_uid", zhiboNewLiveRecord.getHostUid());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getAvRoomId())){
			paramMap.put("av_room_id", zhiboNewLiveRecord.getAvRoomId());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getChatRoomId())){
			paramMap.put("chat_room_id", zhiboNewLiveRecord.getChatRoomId());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getRoomType())){
			paramMap.put("room_type", zhiboNewLiveRecord.getRoomType());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getVideoType())){
			paramMap.put("video_type", zhiboNewLiveRecord.getVideoType());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getDevice())){
			paramMap.put("device", zhiboNewLiveRecord.getDevice());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getAdmireCount())){
			paramMap.put("admire_count", zhiboNewLiveRecord.getAdmireCount());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getPlayUrl1())){
			paramMap.put("play_url1", zhiboNewLiveRecord.getPlayUrl1());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getPlayUrl2())){
			paramMap.put("play_url2", zhiboNewLiveRecord.getPlayUrl2());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getPlayUrl3())){
			paramMap.put("play_url3", zhiboNewLiveRecord.getPlayUrl3());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getLongitude())){
			paramMap.put("longitude", zhiboNewLiveRecord.getLongitude());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getLatitude())){
			paramMap.put("latitude", zhiboNewLiveRecord.getLatitude());
		}
		if(AppUtils.isNotBlank(zhiboNewLiveRecord.getAddress())){
			paramMap.put("address", zhiboNewLiveRecord.getAddress());
		}
		
		for (Map.Entry<String,Object> item : paramMap.entrySet()){
			sql = sql + item.getKey() + ",";
			if(item.getValue() instanceof String){
				valueSql = valueSql + "'" + item.getValue() + "'"+ ",";
			}else if(item.getValue() instanceof Date){
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				valueSql = valueSql + "'" + formatter.format(item.getValue())+ "'" +  ",";
			}else{
				valueSql = valueSql + item.getValue() +  ",";
			}
		}
		
		sql = sql.substring(0,sql.length()-1) + ") VALUES (" + valueSql.substring(0,valueSql.length()-1) + ")";
		return update(sql);
	}

	/**
	 * 踢人
	 */
	@Override
	public int kickingMemberByUid(String uid, Integer roomid) {
		return update("DELETE FROM ls_zhibo_interact_av_room WHERE uid = ? and av_room_id = ?", uid, roomid);
	}
	
 }
