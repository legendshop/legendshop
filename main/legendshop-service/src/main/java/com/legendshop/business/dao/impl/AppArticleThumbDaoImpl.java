/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.AppArticleThumb;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AppArticleThumbDao;

/**
 * The Class AppArticleThumbDaoImpl. 种草和发现文章点赞表Dao实现类
 */
@Repository
public class AppArticleThumbDaoImpl extends GenericDaoImpl<AppArticleThumb, Long> implements AppArticleThumbDao{

	/**
	 * 根据Id获取种草和发现文章点赞表
	 */
	public AppArticleThumb getAppArticleThumb(Long id){
		return getById(id);
	}

	/**
	 *  删除种草和发现文章点赞表
	 *  @param appArticleThumb 实体类
	 *  @return 删除结果
	 */	
    public int deleteAppArticleThumb(AppArticleThumb appArticleThumb){
    	return delete(appArticleThumb);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteAppArticleThumb(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草和发现文章点赞表
	 */
	public Long saveAppArticleThumb(AppArticleThumb appArticleThumb){
		return save(appArticleThumb);
	}

	/**
	 *  更新种草和发现文章点赞表
	 */		
	public int updateAppArticleThumb(AppArticleThumb appArticleThumb){
		return update(appArticleThumb);
	}

	/**
	 * 分页查询种草和发现文章点赞表列表
	 */
	public PageSupport<AppArticleThumb>queryAppArticleThumb(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(AppArticleThumb.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public int deleteAppArticleThumbByDiscoverId(Long disId, String uid) {
		String sql="delete from ls_app_article_thumb where arti_id=? and user_id=?";
		return update(sql, disId,uid);
	}
	
	@Override
	public Integer rsThumb(Long disId, String uid) {
		String sql="select count(1) from ls_app_article_thumb where arti_id=? and user_id=?";
		return get(sql, Integer.class, disId,uid);
	}

}
