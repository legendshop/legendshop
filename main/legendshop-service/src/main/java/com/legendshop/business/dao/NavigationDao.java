/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Navigation;

/**
 * The Class NavigationDao.
 */

public interface NavigationDao extends GenericDao<Navigation, Long>{

	/**
	 * Gets the navigation.
	 *
	 * @param id the id
	 * @return the navigation
	 */
	public abstract Navigation getNavigation(Long id);

	/**
	 * Delete navigation.
	 *
	 * @param navigation the navigation
	 */
	public abstract void deleteNavigation(Navigation navigation);

	/**
	 * Save navigation.
	 *
	 * @param navigation the navigation
	 * @return the long
	 */
	public abstract Long saveNavigation(Navigation navigation);

	/**
	 * Update navigation.
	 *
	 * @param navigation the navigation
	 */
	public abstract void updateNavigation(Navigation navigation);

	/**
	 * Gets the navigation list.
	 *
	 * @return the navigation list
	 */
	public abstract List<Navigation> getNavigationList();

	public abstract PageSupport<Navigation> getNavigationPage(String curPageNO);
	
}
