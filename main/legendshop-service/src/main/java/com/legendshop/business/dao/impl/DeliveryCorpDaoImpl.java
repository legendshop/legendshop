/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DeliveryCorpDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.util.AppUtils;

/**
 * 物流公司服务
 */
@Repository
public class DeliveryCorpDaoImpl extends GenericDaoImpl<DeliveryCorp, Long> implements DeliveryCorpDao {


	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.DeliveryCorpDao#getDeliveryCorp(java.lang
	 * .String)
	 */
	@Override
	public List<DeliveryCorp> getDeliveryCorpByShop() {
		return this.queryByProperties(new EntityCriterion());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.DeliveryCorpDao#getDeliveryCorp(java.lang
	 * .Long)
	 */
	@Override
	public DeliveryCorp getDeliveryCorp(Long id) {
		return getById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.DeliveryCorpDao#deleteDeliveryCorp(java.lang
	 * .Long)
	 */
	@Override
	public void deleteDeliveryCorp(DeliveryCorp deliveryCorp) {
		delete(deliveryCorp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.DeliveryCorpDao#saveDeliveryCorp(com.legendshop
	 * .model.entity.DeliveryCorp)
	 */
	@Override
	public Long saveDeliveryCorp(DeliveryCorp deliveryCorp) {
		return (Long) save(deliveryCorp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.DeliveryCorpDao#updateDeliveryCorp(com.legendshop
	 * .model.entity.DeliveryCorp)
	 */
	@Override
	public void updateDeliveryCorp(DeliveryCorp deliveryCorp) {
		update(deliveryCorp);
	}

	@Override
	public DeliveryCorp getDeliveryCorpByDvyId(Long dvyId) {
		return get("select ld.* from ls_delivery ld left join ls_dvy_type ldt on ld.dvy_id=ldt.dvy_id where ldt.dvy_type_id=?",DeliveryCorp.class,dvyId);
	}

	@Override
	public void deleteDeliveryCorpByShopId(Long shopId) {
		update("delete from ls_delivery where shop_id = ?", shopId);
	}

	@Override
	public PageSupport<DeliveryCorp> getDeliveryCorpPage(String curPageNO, DeliveryCorp deliveryCorp) {
		CriteriaQuery cq = new CriteriaQuery(DeliveryCorp.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		
		cq.eq("userName", deliveryCorp.getUserName());
		if (!AppUtils.isBlank(deliveryCorp.getName())) {
			cq.like("name", "%" + deliveryCorp.getName().trim() + "%");
		}
		return queryPage(cq);
	}

	@Override
	public List<DeliveryCorp> getAllDeliveryCorp() {
		return queryAll();
	}


}
