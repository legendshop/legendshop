/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DomainDao;
import com.legendshop.spi.service.DomainService;

/**
 * 域名设置服务实现类
 */
@Service("domainService")
public class DomainServiceImpl implements DomainService{
	
	@Autowired
	private DomainDao domainDao;

	/**
	 * 查询该二级域名是否已经绑定.
	 *
	 * @param domainName the domain name
	 * @return true, if query domain name binded
	 */
	public boolean queryDomainNameBinded(String domainName) {
		return domainDao.queryDomainNameBinded(domainName);
	}

	/**
	 * 更新二级域名
	 */
	public int updateDomainName(String userId, String domainName,Date registDate) {
		return domainDao.updateDomainName(userId, domainName,registDate);
	}
}
