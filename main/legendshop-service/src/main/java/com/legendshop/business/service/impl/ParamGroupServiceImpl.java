/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ParamGroupDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ParamGroup;
import com.legendshop.spi.service.ParamGroupService;
import com.legendshop.util.AppUtils;

/**
 * 参数组列表ServiceImpl.
 */
@Service("paramGroupService")
public class ParamGroupServiceImpl  implements ParamGroupService{

	@Autowired
    private ParamGroupDao paramGroupDao;

    public ParamGroup getParamGroup(Long id) {
        return paramGroupDao.getParamGroup(id);
    }

    public void deleteParamGroup(ParamGroup paramGroup) {
        paramGroupDao.deleteParamGroup(paramGroup);
    }

    public Long saveParamGroup(ParamGroup paramGroup) {
        if (!AppUtils.isBlank(paramGroup.getId())) {
            updateParamGroup(paramGroup);
            return paramGroup.getId();
        }
        return paramGroupDao.saveParamGroup(paramGroup);
    }

    public void updateParamGroup(ParamGroup paramGroup) {
        paramGroupDao.updateParamGroup(paramGroup);
    }

	@Override
	public List<ParamGroup> getParamGroupList(Long groupId) {
		return paramGroupDao.getParamGroupList(groupId);
	}

	@Override
	public PageSupport<ParamGroup> getParamGroupPage(String curPageNO, ParamGroup paramGroup) {
		return paramGroupDao.getParamGroupPage(curPageNO,paramGroup);
	}

	@Override
	public PageSupport<ParamGroup> getParamGroupPage(String groupName) {
		return paramGroupDao.getParamGroupPage(groupName);
	}
}
