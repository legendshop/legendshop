/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassComm;
import com.legendshop.model.entity.GrassProd;

/**
 * The Class GrassProdDao. 种草文章关联商品表Dao接口
 */
public interface GrassProdDao extends Dao<GrassProd,Long>{

	/**
	 * 根据Id获取种草文章关联商品表
	 */
	public abstract GrassProd getGrassProd(Long id);

	/**
	 *  根据Id删除种草文章关联商品表
	 */
    public abstract int deleteGrassProd(Long id);
    
    public abstract void deleteByGrassId(Long id);
    

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassProd(GrassProd grassProd);

	/**
	 * 保存种草文章关联商品表
	 */
	public abstract Long saveGrassProd(GrassProd grassProd);

	/**
	 *  更新种草文章关联商品表
	 */		
	public abstract int updateGrassProd(GrassProd grassProd);

	/**
	 * 分页查询种草文章关联商品表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassProd> queryGrassProd(String curPageNO, Integer pageSize);
	
	public abstract List<GrassProd> getGrassProdByGrassId(Long grassId);

}
