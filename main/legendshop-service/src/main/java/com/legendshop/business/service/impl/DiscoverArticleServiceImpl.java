/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DiscoverArticleDao;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.spi.service.DiscoverArticleService;

/**
 * The Class DiscoverArticleServiceImpl.
 *  发现文章表服务实现类
 */
@Service("discoverArticleService")
public class DiscoverArticleServiceImpl implements DiscoverArticleService{

    /**
     *
     * 引用的发现文章表Dao接口
     */
	@Autowired
    private DiscoverArticleDao discoverArticleDao;
   
   	/**
	 *  根据Id获取发现文章表
	 */
    public DiscoverArticle getDiscoverArticle(Long id) {
        return discoverArticleDao.getDiscoverArticle(id);
    }
    
    /**
	 *  根据Id删除发现文章表
	 */
    public int deleteDiscoverArticle(Long id){
    	return discoverArticleDao.deleteDiscoverArticle(id);
    }

   /**
	 *  删除发现文章表
	 */ 
    public int deleteDiscoverArticle(DiscoverArticle discoverArticle) {
       return  discoverArticleDao.deleteDiscoverArticle(discoverArticle);
    }

   /**
	 *  保存发现文章表
	 */	    
    public Long saveDiscoverArticle(DiscoverArticle discoverArticle) {
        if (!AppUtils.isBlank(discoverArticle.getId())) {
            updateDiscoverArticle(discoverArticle);
            return discoverArticle.getId();
        }
        return discoverArticleDao.saveDiscoverArticle(discoverArticle);
    }

   /**
	 *  更新发现文章表
	 */	
    public void updateDiscoverArticle(DiscoverArticle discoverArticle) {
        discoverArticleDao.updateDiscoverArticle(discoverArticle);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverArticle> queryDiscoverArticle(String curPageNO, Integer pageSize,String condition,String order){
     	return discoverArticleDao.queryDiscoverArticle(curPageNO, pageSize,condition,order);
    }


	@Override
	public PageSupport<DiscoverArticle> queryDiscoverArticlePage(String curPageNO, DiscoverArticle discoverArticle,
			Integer pageSize) {
		return discoverArticleDao.queryDiscoverArticlePage(curPageNO,discoverArticle,pageSize);
	}
    
}
