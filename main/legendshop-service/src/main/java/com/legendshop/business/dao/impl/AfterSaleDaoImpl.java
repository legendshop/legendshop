package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AfterSaleDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AfterSale;
/**
 *售后服务说明表
 */
@Repository
public class AfterSaleDaoImpl extends GenericDaoImpl<AfterSale, Long> implements AfterSaleDao{

	@Override
	public List<AfterSale> getAfterSale() {
		return queryAll();
	}

	@Override
	public AfterSale getAfterSale(Long id) {
		return getById(id);
	}

	@Override
	public void deleteAfterSale(AfterSale afterSale) {
		delete(afterSale);
	}

	@Override
	public Long saveAfterSale(AfterSale afterSale) {
		return save(afterSale);
	}

	@Override
	public void updateAfterSale(AfterSale afterSale) {
		update(afterSale);
	}

	@Override
	public PageSupport<AfterSale> getAfterSale(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public void deleteByAfterSaleId(Long id) {
		update("delete from ls_after_sale where id=?",id);
	}

	@Override
	public PageSupport<AfterSale> getAfterSale(String curPageNO, String userName, String userId) {
		CriteriaQuery cq = new CriteriaQuery(AfterSale.class, curPageNO);
		cq.eq("userName", userName);
		cq.eq("userId", userId);
		cq.setPageSize(3);
		cq.addDescOrder("id");
		return queryPage(cq);
	}

	@Override
	public PageSupport<AfterSale> getAfterSalePage(String curPageNO, String userName, String userId) {
		CriteriaQuery cq = new CriteriaQuery(AfterSale.class, curPageNO);
		cq.eq("userName", userName);
		cq.eq("userId", userId);
		cq.setPageSize(3);
		cq.addDescOrder("id");
		return queryPage(cq);
	}

}
