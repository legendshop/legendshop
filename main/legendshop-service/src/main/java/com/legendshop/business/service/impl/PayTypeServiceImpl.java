/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.exception.ConflictException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.business.dao.PayTypeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.util.AppUtils;

/**
 * 付费类型服务.
 */
@Service("payTypeService")
public class PayTypeServiceImpl implements PayTypeService {

	@Autowired
	private PayTypeDao payTypeDao;
	
	@Override
	public PayType getPayTypeById(String payTypeId) {
		return payTypeDao.getPayTypeById(payTypeId);
	}

	/**
	 * 根据payTypeId查找支付的类型
	 */
	@Override
	public PayType getPayTypeById(Long id) {
		return payTypeDao.getPayTypeById(id);
	}
	
	
	/**
	 * 根据payTypeId查找支付的类型
	 */
	@Override
	public PayType getPayTypeByIdFromDb(Long id) {
		return payTypeDao.getPayTypeByIdFromDb(id);
	}

	@Override
	public void delete(Long id) {
		PayType payType = getPayTypeById(id);
		if (payType != null) {
			payTypeDao.deletePayTypeById(id);
		}

	}
	
	@Override
	public Map<String, Integer> getPayTypeMap() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<PayType> payTypes = payTypeDao.getPayTypeList();
		for (PayType payType : payTypes) {
			if(PayTypeEnum.ALI_PAY.value().equals(payType.getPayTypeId())){
				JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
				String use=jsonObject.getString("use");
				if(AppUtils.isNotBlank(use)){
					JSONObject useObject=JSONObject.parseObject(use);
					if(useObject.containsValue("app")){
						map.put("ALP_APP",payType.getIsEnable());
					}
					if(useObject.containsValue("mobile")){
						map.put("ALP_MOBILE",payType.getIsEnable());
					}
					if(useObject.containsValue("pc")){
						map.put("ALP_PC",payType.getIsEnable());
					}
				}
			}else{
				map.put(payType.getPayTypeId(),payType.getIsEnable());
			}
		}
		return map;
	}

	@Override
	public List<PayType> getPayTypeList() {
		List<PayType> payTypes = payTypeDao.getPayTypeList();
		return payTypes;
	}

	@Override
	public Long save(PayType payType) {
		return (Long) payTypeDao.savePayType(payType);
	}

	@Override
	public void update(PayType payType) {
		PayType origin = payTypeDao.getPayTypeByIdFromDb(payType.getPayId());
		origin.setMemo(payType.getMemo());
		origin.setPaymentConfig(payType.getPaymentConfig());
		origin.setIsEnable(payType.getIsEnable());
		origin.setInterfaceType(payType.getInterfaceType());
		try {
			payTypeDao.updatePayType(origin);
		} catch (DataIntegrityViolationException e) {
			throw new ConflictException(e, "你已经创建一个叫 “" + payType.getPayTypeName() + "” 的支付方式",ErrorCodes.RESOURCE_CONFLICT);
		}
	}

	/**
	 * 根据支付类型Id来查找支付
	 */
	@Override
	public PayType getPayTypeByPayTypeId(String payTypeId) {
		return payTypeDao.getPayTypeByPayTypeId(payTypeId);
	}


	@Override
	public PageSupport<PayType> getPayTypeListPage(String curPageNO) {
		return payTypeDao.getPayTypeListPage(curPageNO);
	}
}
