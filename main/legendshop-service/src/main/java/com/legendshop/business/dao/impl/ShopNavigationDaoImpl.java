/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopNavigationDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopNavigation;
/**
 * 商家装修导航Dao
 */
@Repository
public class ShopNavigationDaoImpl extends GenericDaoImpl<ShopNavigation, Long> implements ShopNavigationDao {
	
    public List<ShopNavigation> getShopNavigationByShopId(Long shopId){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
    }

	public ShopNavigation getShopNavigation(Long id){
		return getById(id);
	}
	
    public void deleteShopNavigation(ShopNavigation shopNavigation){
    	delete(shopNavigation);
    }
	
	public Long saveShopNavigation(ShopNavigation shopNavigation){
		return (Long)save(shopNavigation);
	}
	
	public void updateShopNavigation(ShopNavigation shopNavigation){
		 update(shopNavigation);
	}
	
	public PageSupport<ShopNavigation> getShopNavigation(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public PageSupport<ShopNavigation> getShopNavigationPage(String curPageNO, ShopNavigation shopNavigation) {
		CriteriaQuery cq = new CriteriaQuery(ShopNavigation.class, curPageNO);
		return queryPage(cq);
	}
	
 }
