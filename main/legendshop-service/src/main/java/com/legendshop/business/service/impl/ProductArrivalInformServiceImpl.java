package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductArrivalInformDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.entity.ProdArrivalInform;
import com.legendshop.spi.service.ProductArrivalInformService;

/**
 * 商品到货通知
 */
@Service("productArrivalInformService")
public class ProductArrivalInformServiceImpl implements ProductArrivalInformService{
	
	@Autowired
	private ProductArrivalInformDao productArrivalInformDao;

	/**
	 * 获得用户已经添加到货通知的商品
	 * @param userId 用户id
	 * @param skuId 
	 * @param status 状态。查询尚未通知的
	 */
	@Override
	public ProdArrivalInform getAlreadySaveUser(String userId,long skuId,int status){
		ProdArrivalInform user = productArrivalInformDao.getAlreadySaveUser(userId, skuId, status);
		return user;
	}
	
	/**
	 * 保存消费者添加的到货通知
	 */
	@Override
	public Long saveProdArriInfo(ProdArrivalInform prodArrivalInform) {
		Date curDate = new Date();
		prodArrivalInform.setCreateTime(curDate);
		return productArrivalInformDao.saveProdArriInfo(prodArrivalInform);
	}
	
	/**
	 * 批量更新到货通知表
	 * @return
	 */
	@Override
	public int updateArrivalInform(List<ProdArrivalInform> arrivalInformList){
		return productArrivalInformDao.update(arrivalInformList);
	}
	
	/**
	 * 根据skuId查看需要进行到货通知的用户
	 */
	@Override
	public List<ProdArrivalInform> getUserBySkuIdAndWhId(long skuId){
		List<ProdArrivalInform> list = productArrivalInformDao.getUserBySkuIdAndWhId(skuId);
		return list;
	}

	@Override
	public PageSupport<ProdArrivalInformDto> getProdArrival(Long shopId, String productName, String curPageNO) {
		return productArrivalInformDao.getProdArrival(shopId,productName,curPageNO);
	}

	@Override
	public PageSupport<ProdArrivalInformDto> getSelectArrival(String curPageNO, String skuId,
			Long shopId) {
		return productArrivalInformDao.getSelectArrival(curPageNO,skuId,shopId);
	}
}
