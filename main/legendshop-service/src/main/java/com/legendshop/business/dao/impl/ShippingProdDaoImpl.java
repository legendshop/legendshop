/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShippingProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.FullActiveProdDto;

/**
 * 包邮活动商品Dao
 */
@Repository
public class ShippingProdDaoImpl extends GenericDaoImpl<ShippingProd, Long> implements ShippingProdDao  {

	public ShippingProd getShippingProd(Long id){
		return getById(id);
	}
	
    public int deleteShippingProd(ShippingProd shippingProd){
    	return delete(shippingProd);
    }
	
	public Long saveShippingProd(ShippingProd shippingProd){
		return save(shippingProd);
	}
	
	public int updateShippingProd(ShippingProd shippingProd){
		return update(shippingProd);
	}

	@Override
	public void batchShippingProd(List<ShippingProd> lists) {
		this.save(lists);
	}

	@Override
	public List<FullActiveProdDto> getFullActiveProdDtoByActiveId(Long shopId, Long id) {
		String sql = "SELECT p.prod_id AS prodId,p.name AS prodName,p.pic,p.actual_stocks AS actualStocks,p.cash as price,p.rec_date "+
				 "FROM ls_prod p INNER JOIN (SELECT * FROM ls_shipping_prod WHERE shipping_id =? AND shop_id = ?)o "+
				 "ON p.prod_id = o.prod_id";	
	return this.query(sql, FullActiveProdDto.class, id,shopId);
	}

	@Override
	public List<ShippingProd> getActiveProdByActiveId(Long id, Long shopId) {
		String sql = "SELECT * FROM ls_shipping_prod WHERE shipping_id = ? AND shop_id = ?";
		return query(sql, ShippingProd.class, id,shopId);
	}

	@Override
	public void deleteShippingProdByShippingId(Long shippingId, Long shopId) {
		String sql = "DELETE FROM ls_shipping_prod WHERE shipping_id = ? AND shop_id = ?";
		update(sql, shippingId, shopId);
	}
	
 }
