/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import com.legendshop.base.util.XssFilterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.NotFoundException;
import com.legendshop.business.dao.ExternalLinkDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.ExternalLink;
import com.legendshop.spi.service.ExternalLinkService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;


/**
 * 友情链接服务.
 */
@Service("externalLinkService")
public class ExternalLinkServiceImpl implements ExternalLinkService {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ExternalLinkServiceImpl.class);
	
	@Autowired
	private ExternalLinkDao externalLinkDao;

	@Autowired
	private AttachmentManager attachmentManager;


	@Override
	public List<ExternalLink> getExternalLink() {
		return externalLinkDao.getExternalLink();
	}


	@Override
	public ExternalLink getExternalLinkById(Long id) {
		return externalLinkDao.getById(id);
	}

	@Override
	public void delete(Long id) {
		externalLinkDao.deleteExternalLinkById(id);
	}

	@Override
	public Long save(ExternalLink externalLink) {
		if (!AppUtils.isBlank(externalLink.getId())) {
			ExternalLink entity = externalLinkDao.getById(externalLink.getId());
			if (entity != null) {
				entity.setUrl(externalLink.getUrl());
				entity.setWordlink(externalLink.getWordlink());
				entity.setContent(externalLink.getContent());
				entity.setBs(externalLink.getBs());
				update(entity);
				return externalLink.getId();
			}
			return null;
		}
		return (Long) externalLinkDao.save(externalLink);
	}

	@Override
	public void update(ExternalLink externalLink) {
		externalLinkDao.updateExternalLink(externalLink);
	}

	@Override
	public PageSupport<ExternalLink> getFriendLink(String curPageNO, String userName) {
		return externalLinkDao.getFriendLink(curPageNO,userName);
	}

	@Override
	public PageSupport<ExternalLink> getDataByCriteriaQuery(String curPageNO, ExternalLink externalLink,
			Integer pageSize, DataSortResult result) {
		return externalLinkDao.getDataByCriteriaQuery(curPageNO,externalLink,pageSize,result);
	}

	@Override
	public void delete(Long id, ExternalLink externalLink) {
		externalLinkDao.deleteExternalLinkById(id);
	}

	@Override
	public void saveOrUpdate(String userId, Long shopId, String userName, ExternalLink externalLink,String picUrl) {
		externalLink.setContent(XssFilterUtil.cleanXSS(externalLink.getContent()));
		externalLink.setWordlink(XssFilterUtil.cleanXSS(externalLink.getWordlink()));
		
		if ((externalLink != null) && (externalLink.getId() != null)) { // update
			ExternalLink origin = null;
			origin = getExternalLinkById(externalLink.getId());
			if (origin == null) {
				throw new NotFoundException("Origin ExternalLink is empty");
			}

			origin.getPicture();
			origin.setUrl(externalLink.getUrl());
			origin.setWordlink(externalLink.getWordlink());
			origin.setContent(externalLink.getContent());
			origin.setBs(externalLink.getBs());
			if (externalLink.getFile() != null && externalLink.getFile().getSize() > 0) {// 上传附件
				// 保存附件表
				attachmentManager.saveImageAttachment(userName,userId,shopId, picUrl, externalLink.getFile(), AttachmentTypeEnum.EXTERNALLINK);
				origin.setPicture(picUrl);
			}
			update(origin);
		} else { // save
			if (externalLink.getFile() != null && externalLink.getFile().getSize() > 0) {
				// picUrl =
				// FileProcessor.uploadFileAndCallback(externalLink.getFile(),
				// subPath, "");
				externalLink.setPicture(picUrl);
			}
			log.info("{} save ExternalLink Url {} ", userName, externalLink.getUrl());
			save(externalLink);

			if (AppUtils.isNotBlank(picUrl)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(userName,userId,shopId, picUrl, externalLink.getFile(), AttachmentTypeEnum.EXTERNALLINK);
			}
		}
	}
}
