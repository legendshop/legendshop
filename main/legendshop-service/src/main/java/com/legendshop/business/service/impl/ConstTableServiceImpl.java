package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.ConstTableService;

@Service("constTableService")
public class ConstTableServiceImpl implements ConstTableService {
	
	@Autowired
	private  ConstTableDao constTableDao;

	@Override
	public ConstTable getConstTablesBykey(String key, String keyValue) {
		return constTableDao.getConstTablesBykey(key, keyValue);
	}

	@Override
	public void updateConstTableByType(String payType, String payTypeId,
			String payParams) {
		constTableDao.updateConstTableByTepe(payType, payTypeId, payParams);
	}

	@Override
	public ConstTable getConstTablesBykeyForNoCache(String type, String keyValue) {
		
		return constTableDao.getConstTablesBykeyForNoCache(type, keyValue);
	}

	

}
