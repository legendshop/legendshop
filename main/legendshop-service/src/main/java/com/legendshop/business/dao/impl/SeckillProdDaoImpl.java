/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.seckill.SuccessProdDto;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * 秒杀商品DaoImpl.
 */
@Repository
public class SeckillProdDaoImpl extends GenericDaoImpl<SeckillProd, Long> implements SeckillProdDao  {
	
	/**
	 * 根据sId获取秒杀商品 
	 */
    public List<SeckillProd> getSeckillProdBySid(Long sId){
   		return this.queryByProperties(new EntityCriterion().eq("sId", sId));
    }

    /**
     * 获取秒杀商品 
     */
	public SeckillProd getSeckillProd(Long id){
		return getById(id);
	}
	
	/**
	 * 删除秒杀商品 
	 */
    public int deleteSeckillProd(SeckillProd seckillProd){
    	return delete(seckillProd);
    }
	
    /**
     * 保存秒杀商品 
     */
	public Long saveSeckillProd(SeckillProd seckillProd){
		return save(seckillProd);
	}
	
	/**
	 * 更新秒杀商品 
	 */
	public int updateSeckillProd(SeckillProd seckillProd){
		return update(seckillProd);
	}

	/**
	 * 删除秒杀商品列表 
	 */
	@Override
	public int deleteSeckillProds(List<SeckillProd> seckillProd) {
		return delete(seckillProd);
	}
	
	/**
	 * 保存秒杀商品列表 
	 */
	@Override
	public List<Long> saveSeckillProdList(List<SeckillProd> seckillProdList) {
		return save(seckillProdList);
	}

	/**
	 * 根据seckillId获取秒杀商品 
	 */
	@Override
	public List<SeckillProd> getSeckillProdBySId(Long seckillId) {
		return this.queryByProperties(new EntityCriterion().eq("sId", seckillId));
	}
	
	/**
	 * 查找秒杀成功商品Dto 
	 */
	@Override
	public SuccessProdDto findSuccessProdDto(SeckillSuccess seckillSuccess) {
		String sql = ConfigCode.getInstance().getCode("seckill.findSuccessProdDto");
		SuccessProdDto prodDto= get(sql, SuccessProdDto.class, new Object[]{seckillSuccess.getProdId(), seckillSuccess.getSkuId()});
		prodDto.setProdId(seckillSuccess.getProdId());
		prodDto.setSucessId(seckillSuccess.getId());
		prodDto.setSeckillId(seckillSuccess.getSeckillId());
		prodDto.setSeckillNum(seckillSuccess.getSeckillNum());
		prodDto.setSeckillPrice(seckillSuccess.getSeckillPrice());
		prodDto.setSkuId(seckillSuccess.getSkuId());
		prodDto.setStatus(seckillSuccess.getStatus());
		prodDto.setUserId(seckillSuccess.getUserId());
		return prodDto;
	}

	/**
	 * 根据sId删除秒杀商品 
	 */
	@Override
	public int deleteSeckillProdBySechillId(Long seckillId) {
		return update("delete from ls_seckill_prod where s_id=? ", seckillId);
	}

    /**
     * 检查秒杀商品是否可以参加秒杀活动，检查库存
     * @param prodId
     * @return
     */
	@Override
	public boolean checkSeckillProdSku(Long prodId) {
		return super.getLongResult("select count(sku_id) from ls_sku where prod_id=? and stocks > 0 and status=1 ", prodId)>0;
	}

	/**
	 * 查询秒杀商品列表 
	 */
	@Override
	public List<SeckillProd> querySeckillProdList(List<Long> ids) {
		return queryByProperties(new EntityCriterion().in("sId", ids.toArray()));
	}

	@Override
	public SuccessProdDto getSeckillSuccessInfo(String userId, Long seckillId, Long skuId) {
		String sql = ConfigCode.getInstance().getCode("seckill.findUserSuccessProd");
		return this.get(sql, SuccessProdDto.class, new Object[]{userId, seckillId, skuId});
	}
	
 }
