/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserAddressSubDao;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.spi.service.UserAddressSubService;
import com.legendshop.util.AppUtils;

/**
 *用户配送地址服务
 */
@Service("userAddressSubService")
public class UserAddressSubServiceImpl  implements UserAddressSubService{
    
    @Autowired
    private UserAddressSubDao userAddressSubDao;

    public UserAddressSub getUserAddressSub(Long id) {
        return userAddressSubDao.getUserAddressSub(id);
    }

    public void deleteUserAddressSub(UserAddressSub userAddressSub) {
        userAddressSubDao.deleteUserAddressSub(userAddressSub);
    }

    public Long saveUserAddressSub(UserAddressSub userAddressSub) {
        if (!AppUtils.isBlank(userAddressSub.getAddrOrderId())) {
            updateUserAddressSub(userAddressSub);
            return userAddressSub.getAddrOrderId();
        }
        return userAddressSubDao.saveUserAddressSub(userAddressSub);
    }

    public void updateUserAddressSub(UserAddressSub userAddressSub) {
        userAddressSubDao.updateUserAddressSub(userAddressSub);
    }

	@Override
	public UserAddressSub getUserAddressSub(Long addrId, Long version) {
		return userAddressSubDao.getUserAddressSub(addrId,version);
	}

	@Override
	public UserAddressSub getUserAddressSubForOrderDetail(Long id) {
		return userAddressSubDao.getUserAddressSubForOrderDetail(id);
	}
}
