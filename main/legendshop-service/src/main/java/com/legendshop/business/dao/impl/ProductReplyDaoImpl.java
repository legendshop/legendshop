package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProductReplyDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductReply;

/**
 * 商品评论回复.
 */
@Repository
public class ProductReplyDaoImpl extends GenericDaoImpl<ProductReply, Long> implements ProductReplyDao{

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	public  List<ProductReply> getProductReply(){
		return queryAll();
	}

	public  ProductReply getProductReply(Long id){
		return getById(id);
	}

	public  void deleteProductReply(ProductReply productReply){
		delete(productReply);
	}

	public  Long saveProductReply(ProductReply productReply){
		//每一次回复都会使这条评论的回复次数增加
		update("update ls_prod_comm set replay_counts = (replay_counts +1) where id = ?", productReply.getProdcommId());
		
		return save(productReply);
	}

	public  void updateProductReply(ProductReply productReply){
		update(productReply);
	}

	@Override
	public PageSupport<ProductReply> getProductReplyPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(ProductReply.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

}
