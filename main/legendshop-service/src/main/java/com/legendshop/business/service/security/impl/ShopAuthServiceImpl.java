/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.security.ShopAuthDao;
import com.legendshop.model.dto.shop.ShopAccountUserDto;
import com.legendshop.model.entity.ShopUser;
import com.legendshop.spi.service.security.ShopAuthService;

/**
 * 商家登录服务.
 */
@Service("shopAuthService")
public class ShopAuthServiceImpl implements ShopAuthService{
	
	@Autowired
	private ShopAuthDao shopAuthDao;

	/* (non-Javadoc)
	 * @see com.legendshop.security.dao.ShopAuthDao#loadUserByUsername(java.lang.String)
	 */
	@Override
	public ShopUser loadUserByUsername(String username) {
		return shopAuthDao.loadUserByUsername(username);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.security.dao.ShopAuthDao#loadUserByUsername(java.lang.String, java.lang.String)
	 */
	@Override
	public ShopUser loadUserByUsername(String username, String password) {
		return shopAuthDao.loadUserByUsername(username, password);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.security.dao.ShopAuthDao#loadShopAccountByUsername(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ShopAccountUserDto loadShopAccountByUsername(String sellerName, String password, String loginCode) {
		return shopAuthDao.loadShopAccountByUsername(sellerName, password, loginCode);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.security.dao.ShopAuthDao#loadUserByUsername(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ShopUser loadUserByUsername(String sellerName, String mobile, String mobileCode) {
		return shopAuthDao.loadUserByUsername(sellerName, mobile, mobileCode);
	}

	public void setShopAuthDao(ShopAuthDao shopAuthDao) {
		this.shopAuthDao = shopAuthDao;
	}

}
