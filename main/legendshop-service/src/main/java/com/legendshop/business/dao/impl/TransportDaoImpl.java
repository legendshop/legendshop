package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TransfeeDao;
import com.legendshop.business.dao.TransportDao;
import com.legendshop.dao.PageSupportHandler;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.util.AppUtils;

@Repository
public class TransportDaoImpl extends GenericDaoImpl<Transport, Long> implements TransportDao{
	
	@Autowired
	private TransfeeDao transfeeDao;
	
	/** 根据运费模板ID加载在某个商城下的运费模板 **/
	private String getDetailedTransportSQL = "select ltp.id as id,ltp.trans_name as transName,ltp.trans_time as transTime,ltp.trans_type as transType,ltp.is_regional_sales as isRegionalSales  from ls_transport ltp where ltp.shop_id = ? and ltp.id = ?" ;
	
	private String deleteTransportCitySQL = "delete from ls_transcity where exists  (select 1 from ls_transfee tf, ls_transport tp where tf.transport_id = tp.id and tp.id = ? and tp.shop_id = ? and tf.id = ls_transcity.transfee_id )";

	private String deleteTransportFeeSQL = "delete from ls_transfee where exists (select 1 from ls_transport tp where tp.id = ls_transfee.transport_id and tp.id = ? and tp.shop_id = ?)";
	
	private String deleteTransportSQL = "delete from ls_transport where id = ? and shop_id = ?";
	
	public List<Transport> getTransports(Long shopId) {
		return query("select ltp.id as id,ltp.trans_name as transName from ls_transport ltp where ltp.status = 1 and ltp.shop_id = ?",Transport.class,shopId);
	}

	public Transport getTransport(Long id) {
		return getById(id);
	}

	@CacheEvict(value = "Transport", key = "#transport.shopId + '.' + #transport.id")
	public void deleteTransport(Transport transport) {
		delete(transport);
	}

	public Long saveTransport(Transport transport) {
		Long transportId =  save(transport);
		if(transport.isTransMail()){
			saveTransfee(transportId,Constants.TRANSPORT_MAIL, transport.getMailList());
		}
		if(transport.isTransExpress()){
			saveTransfee(transportId,Constants.TRANSPORT_EXPRESS,transport.getExpressList());
		}
		if(transport.isTransEms()){
			saveTransfee(transportId,Constants.TRANSPORT_EMS, transport.getEmsList());
		}
		return transportId;
	}
	
	private void saveTransfee( Long transportId,String freightMode,List<Transfee> list){
		 if(AppUtils.isNotBlank(list)){
			 for (Transfee tf : list) {
				 tf.setTransportId(transportId);
				 tf.setFreightMode(freightMode);
				 transfeeDao.saveTransfee(tf);
			}
		 }
	}

	@CacheEvict(value = "Transport", key = "#transport.shopId + '.' + #transport.id")
	public void updateTransport(Transport transport) {
		update(transport);
	}

	public PageSupport getTransport(CriteriaQuery cq) {
		return queryPage(cq);
	}

	public List<Area> getCtyName(Long ctyId) {
		return query("select area from ls_areas where  cityid = ?", Area.class, ctyId);
	}

	/**
	 * 根据运费模板ID加载在某个商城下的运费模板
	 */
	@Cacheable(value = "Transport", key = "#shopId + '.' + #id")
	public Transport getDetailedTransport(Long shopId,Long id) {
		Transport transport = get(getDetailedTransportSQL,Transport.class, shopId,id);
		if(transport ==null){
			return null;
		}
		List<Transfee> mailList = new ArrayList<Transfee>();
		List<Transfee> expressList = new ArrayList<Transfee>();
		List<Transfee> emsList = new ArrayList<Transfee>();
		List<Transfee> feeList = transfeeDao.getTranfeeById(id);//加载城市的运费
			for(Transfee transfee:feeList){
					if(Constants.TRANSPORT_MAIL.equals(transfee.getFreightMode())){//平邮
						mailList.add(transfee);
					}else if(Constants.TRANSPORT_EXPRESS.equals(transfee.getFreightMode())){//快递 express
						expressList.add(transfee);
					}else{//否则采用ems
						emsList.add(transfee);
					}
			}
			
			transport.setMailList(mailList);
			transport.setExpressList(expressList);
			transport.setEmsList(emsList);
			
			transport.setTransMail(AppUtils.isNotBlank(mailList)?true:false);
			transport.setTransExpress(AppUtils.isNotBlank(expressList)?true:false);
			transport.setTransEms(AppUtils.isNotBlank(emsList)?true:false);
		return transport;
	}

	public void setTransfeeDao(TransfeeDao transfeeDao) {
		this.transfeeDao = transfeeDao;
	}

	@CacheEvict(value = "Transport", key = "#shopId + '.' + #id")
	public void deleteTransport(Long id, Long shopId) {
		
		//delete transcity
		update(deleteTransportCitySQL, id, shopId);
		
		//delete transfee
		update(deleteTransportFeeSQL, id, shopId);
		
		//delete transport
		update(deleteTransportSQL, id,shopId);
		
	}

	@Override
	public boolean isAppTransport(Long id) {
		List<Integer> list = query("select 1 from ls_prod where transport_id = ?", Integer.class, id);
		if(AppUtils.isNotBlank(list)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public PageSupport<Transport> getFreightChargesTemp(String curPageNO, Long shopId, String transName) {
		CriteriaQuery cq = new CriteriaQuery(Transport.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("shopId", shopId);
		cq.like("transName", transName, MatchMode.ANYWHERE);
		cq.addDescOrder("recDate");
		//在每个运费模板下面加载运费详情
		return queryPage(cq, new PageSupportHandler<Transport>() {
			public void handle(List<Transport> transportList) {
				 List<Transfee> transfeeList = transfeeDao.getTranfeeByShopId(shopId);
				 for(Transport transport : transportList){
					 for(Transfee transfee:transfeeList){
						 if(transport.getId().equals(transfee.getTransportId())){
							 transport.addFeeList(transfee);
						 }
					 }
				 }
				
			}
		});
	}

	@Override
	public Integer checkByName(String name,Long shopId) {
		String sql="select count(1) from ls_transport where trans_name=? and shop_id=?";
		return get(sql,Integer.class,name,shopId);
	}


}
