/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.promotor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.promoter.CommisChangeLogDao;
import com.legendshop.business.dao.promoter.DistSetDao;
import com.legendshop.business.dao.promoter.UserCommisDao;
import com.legendshop.model.dto.promotor.CommisTypeEnum;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.CommisChangeLog;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.spi.service.PromotionCommisService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 推广员订单佣金ServiceImpl
 *
 */
@Service("promotionCommisService")
public class PromotionCommisServiceImpl implements PromotionCommisService {

	@Autowired
	private UserCommisDao userCommisDao;

	@Autowired
	private DistSetDao distSetDao;

	@Autowired
	private CommisChangeLogDao commisChangeLogDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SubItemDao subItemDao;
	
	@Autowired
	private UserDetailDao userDetailDao;

	/**
	 * 推广员 推广注册成功
	 */
	@Override
	public void handlePromotionReg(UserDetail userDetail) {

		Date nowDate = new Date();
		UserCommis userCommis = new UserCommis();
		userCommis.setUserId(userDetail.getUserId());
		userCommis.setUserName(userDetail.getUserName());
		userCommis.setCommisSts(1);
		userCommis.setId(userDetail.getUserId());
		userCommis.setTotalDistCommis(0d);
		userCommis.setFirstDistCommis(0d);
		userCommis.setSecondDistCommis(0d);
		userCommis.setThirdDistCommis(0d);
		userCommis.setSettledDistCommis(0d);
		userCommis.setUnsettleDistCommis(0d);

		// 检查配置 是否 注册会员即为推广员
		DistSet distSet = distSetDao.getDistSet(1L);
		if (AppUtils.isNotBlank(distSet) && AppUtils.isNotBlank(distSet.getRegAsPromoter()) && distSet.getRegAsPromoter().intValue() == 1) {
			userCommis.setPromoterSts(1L);
			userCommis.setPromoterTime(nowDate);
		} else {
			userCommis.setPromoterSts(0L);
		}

		// 获得父级userName
		String parentUserName = userDetail.getParentUserName();
		if (AppUtils.isNotBlank(parentUserName)) {
			// 获得父级推广员详情
			UserCommis parentUserCommis = userCommisDao.getUserCommisByName(parentUserName);
			if (AppUtils.isNotBlank(parentUserCommis)) {
				Long parentPromoterSts = parentUserCommis.getPromoterSts();
				// 检查父级 是否是 推广员
				if (AppUtils.isNotBlank(parentPromoterSts) && parentPromoterSts.intValue() == 1) {

					userCommis.setParentUserName(parentUserName);
					userCommis.setParentTwoUserName(parentUserCommis.getParentUserName());
					userCommis.setParentThreeUserName(parentUserCommis.getParentTwoUserName());
					userCommis.setParentBindingTime(nowDate);

					if (distSet.getRegCommis() > 0) {
						// 保存分佣明细
						CommisChangeLog commisChangeLog = new CommisChangeLog();
						commisChangeLog.setUserId(parentUserCommis.getUserId());
						commisChangeLog.setUserName(parentUserCommis.getUserName());
						commisChangeLog.setAddTime(new Date());
						commisChangeLog.setAmount(distSet.getRegCommis());
						commisChangeLog.setCommisType(CommisTypeEnum.REG_COMMIS.value());
						commisChangeLog.setSettleSts(1);
						commisChangeLog.setSubUserName(userDetail.getUserName());
						commisChangeLogDao.saveCommisChangeLog(commisChangeLog);

						// 父级会员 加佣金
						parentUserCommis.setTotalDistCommis(Arith.add(parentUserCommis.getTotalDistCommis(), distSet.getRegCommis()));
						parentUserCommis.setSettledDistCommis(Arith.add(parentUserCommis.getSettledDistCommis(), distSet.getRegCommis()));
						userCommisDao.updateUserCommis(parentUserCommis);
					}
				}

			}
		}

		// 保存用户的推广信息
		userCommisDao.saveUserCommis(userCommis);

	}

	/**
	 * 订单分配佣金 ---》 支持分销的订单项
	 */
	@Override
	public void handleOrderCommis(List<SubItem> subItems, String userId) {
		UserCommis userCommis = userCommisDao.getUserCommis(userId);
		// 如果没有上级，则 直接返回
		if (AppUtils.isBlank(userCommis) || AppUtils.isBlank(userCommis.getParentUserName())) {
			return;
		}

		// 查询 直接上级 是否开启分佣
		boolean hasFirstCommis = false;
		UserCommis parentUserCommis = userCommisDao.getUserCommisByName(userCommis.getParentUserName());
		if (AppUtils.isNotBlank(parentUserCommis) && parentUserCommis.getCommisSts().intValue() == 1) {
			hasFirstCommis = true;
		}

		// 查询 上两级 是否开启分佣
		boolean hasSecondCommis = false;
		UserCommis parentTwoUserCommis = null;
		if (AppUtils.isNotBlank(userCommis.getParentTwoUserName())) {
			parentTwoUserCommis = userCommisDao.getUserCommisByName(userCommis.getParentTwoUserName());
			if (AppUtils.isNotBlank(parentTwoUserCommis) && parentTwoUserCommis.getCommisSts().intValue() == 1) {
				hasSecondCommis = true;
			}
		}

		// 查询 上三级 是否开启分佣
		boolean hasThirdCommis = false;
		UserCommis parentThreeUserCommis = null;
		if (AppUtils.isNotBlank(userCommis.getParentThreeUserName())) {
			parentThreeUserCommis = userCommisDao.getUserCommisByName(userCommis.getParentThreeUserName());
			if (AppUtils.isNotBlank(parentThreeUserCommis) && parentThreeUserCommis.getCommisSts().intValue() == 1) {
				hasThirdCommis = true;
			}
		}

		// 查询平台分销设置
		DistSet distSet = distSetDao.getDistSet(1L);
		boolean isMultiDist = false;// 是否支持多级分佣
		if (AppUtils.isNotBlank(distSet) && distSet.getSupportDist().intValue() == 1) {
			isMultiDist = true;
		}

		Double distFirstCommisTotal = 0d;// 一级分佣总金额
		Double distSecondCommisTotal = 0d;// 二级分佣总金额
		Double distThirdCommisTotal = 0d;// 三级分佣总金额
		Double distCommisAmount = 0d;
		for (SubItem item : subItems) {
			Product prod = productDao.getById(item.getProdId());
			// 查询商品是否支持分销
			if (AppUtils.isNotBlank(prod) && prod.getSupportDist() == 1) {

				// 订单项 分销佣金总金额
				Double distCommisCash = 0d;
				// 是否有分销
				int hasDist = 0;

				if (hasFirstCommis) {
					// 计算一级的分佣
					// 获得商品直接上级的 分佣比例, 比例是百分比的值，计算时要除以100
					Double firstLevelRate = prod.getFirstLevelRate();
					if (firstLevelRate != null) {
						Double distUserCommis = Arith.mul(item.getActualAmount(), Arith.mul(firstLevelRate, 0.01));
						item.setDistUserName(userCommis.getParentUserName());
						item.setDistUserCommis(distUserCommis);
						distCommisCash = Arith.add(distCommisCash, distUserCommis);
						distFirstCommisTotal = Arith.add(distFirstCommisTotal, distUserCommis);
						hasDist = 1;

						// 保存分佣明细
						CommisChangeLog commisChangeLog = new CommisChangeLog();
						commisChangeLog.setSn(item.getSubItemNumber());
						commisChangeLog.setUserId(parentUserCommis.getUserId());
						commisChangeLog.setUserName(parentUserCommis.getUserName());
						commisChangeLog.setAddTime(new Date());
						commisChangeLog.setAmount(distUserCommis);
						commisChangeLog.setCommisType(CommisTypeEnum.FIRST_COMMIS.value());
						commisChangeLog.setSettleSts(0);
						commisChangeLog.setSubUserName(userCommis.getUserName());
						commisChangeLogDao.saveCommisChangeLog(commisChangeLog);
					}
				}

				if (isMultiDist) {// 是否支持多级分佣

					// 计算二级的分佣
					if (hasSecondCommis) {
						// 获得商品上两级的 分佣比例, 比例是百分比的值，计算时要除以100
						Double secondLevelRate = prod.getSecondLevelRate();
						if (secondLevelRate != null) {
							Double distSecondCommis = Arith.mul(item.getActualAmount(), Arith.mul(secondLevelRate, 0.01));
							item.setDistSecondName(userCommis.getParentTwoUserName());
							item.setDistSecondCommis(distSecondCommis);
							distCommisCash = Arith.add(distCommisCash, distSecondCommis);
							distSecondCommisTotal = Arith.add(distSecondCommisTotal, distSecondCommis);
							hasDist = 1;

							// 保存分佣明细
							CommisChangeLog commisChangeLog = new CommisChangeLog();
							commisChangeLog.setSn(item.getSubItemNumber());
							commisChangeLog.setUserId(parentTwoUserCommis.getUserId());
							commisChangeLog.setUserName(parentTwoUserCommis.getUserName());
							commisChangeLog.setAddTime(new Date());
							commisChangeLog.setAmount(distSecondCommis);
							commisChangeLog.setCommisType(CommisTypeEnum.SECOND_COMMIS.value());
							commisChangeLog.setSettleSts(0);
							commisChangeLog.setSubUserName(userCommis.getUserName());
							commisChangeLogDao.saveCommisChangeLog(commisChangeLog);
						}
					}

					// 计算三级的分佣
					if (hasThirdCommis) {
						// 获得商品上两级的 分佣比例, 比例是百分比的值，计算时要除以100
						Double thirdLevelRate = prod.getThirdLevelRate();
						if (thirdLevelRate != null) {
							Double distThirdCommis = Arith.mul(item.getActualAmount(), Arith.mul(thirdLevelRate, 0.01));
							item.setDistThirdName(userCommis.getParentThreeUserName());
							item.setDistThirdCommis(distThirdCommis);
							distCommisCash = Arith.add(distCommisCash, distThirdCommis);
							distThirdCommisTotal = Arith.add(distThirdCommisTotal, distThirdCommis);
							hasDist = 1;

							// 保存分佣明细
							CommisChangeLog commisChangeLog = new CommisChangeLog();
							commisChangeLog.setSn(item.getSubItemNumber());
							commisChangeLog.setUserId(parentThreeUserCommis.getUserId());
							commisChangeLog.setUserName(parentThreeUserCommis.getUserName());
							commisChangeLog.setAddTime(new Date());
							commisChangeLog.setAmount(distThirdCommis);
							commisChangeLog.setCommisType(CommisTypeEnum.THIRD_COMMIS.value());
							commisChangeLog.setSettleSts(0);
							commisChangeLog.setSubUserName(userCommis.getUserName());
							commisChangeLogDao.saveCommisChangeLog(commisChangeLog);
						}
					}
				}
				distCommisAmount = Arith.add(distCommisAmount, distCommisCash);
				item.setHasDist(hasDist);// 是否有分销
				item.setDistCommisCash(distCommisCash);// 订单项 分销佣金总金额
				item.setCommisSettleSts(0);// 佣金结算状态 0:未结算
				// 更新保存订单项
				subItemDao.updateSubItem(item);
			}
		}

		// 保存直接上级会员的佣金
		if (hasFirstCommis && distFirstCommisTotal > 0) {
			userCommisDao.addFirstDistCommis(parentUserCommis.getUserName(), distFirstCommisTotal);
			userCommisDao.addTotalDistCommis(parentUserCommis.getUserName(), distFirstCommisTotal);
			userCommisDao.addUnsettleDistCommis(parentUserCommis.getUserName(), distFirstCommisTotal);
		}

		// 保存上两级会员的佣金
		if (hasSecondCommis && distSecondCommisTotal > 0) {
			userCommisDao.addSecondDistCommis(parentTwoUserCommis.getUserName(), distSecondCommisTotal);
			userCommisDao.addTotalDistCommis(parentTwoUserCommis.getUserName(), distSecondCommisTotal);
			userCommisDao.addUnsettleDistCommis(parentTwoUserCommis.getUserName(), distSecondCommisTotal);
		}

		// 保存上三级会员的佣金
		if (hasThirdCommis && distThirdCommisTotal > 0) {
			userCommisDao.addThirdDistCommis(parentThreeUserCommis.getUserName(), distThirdCommisTotal);
			userCommisDao.addTotalDistCommis(parentThreeUserCommis.getUserName(), distThirdCommisTotal);
			userCommisDao.addUnsettleDistCommis(parentThreeUserCommis.getUserName(), distThirdCommisTotal);
		}

	}

	/**
	 * 订单佣金结算
	 */
	@Override
	public void settleOrderCommis(SubItem subItem) {
		// 一级用户分佣金
		settleUserCommis(subItem, subItem.getDistUserName(), subItem.getDistUserCommis());
		// 二级用户分佣金
		settleUserCommis(subItem, subItem.getDistSecondName(), subItem.getDistSecondCommis());
		// 三级用户分佣金
		settleUserCommis(subItem, subItem.getDistThirdName(), subItem.getDistThirdCommis());

		// 更新子订单状态
		subItemDao.updateCommisSettleSts(subItem.getSubItemId(), 1);
	}

	/**
	 * 给用户结算 某个订单的分佣
	 * 
	 * @param subItem
	 * @param userName
	 * @param commisCash
	 */
	private void settleUserCommis(SubItem subItem, String userName, Double commisCash) {
		if (commisCash != null && commisCash > 0 && subItem.getCommisSettleSts() == 0 && userName != null) {

			// 增加结算佣金
			userCommisDao.addSettleDistCommis(userName, commisCash);
			// 减少未结算佣金
			userCommisDao.reduceUnSettleDistCommis(userName, commisCash);

			CommisChangeLog commisChangeLog = commisChangeLogDao.getCommisChangeLog(userName, subItem.getSubItemNumber());
			commisChangeLog.setSettleSts(1);
			commisChangeLog.setSettleTime(new Date());
			commisChangeLogDao.updateCommisChangeLog(commisChangeLog);
		}
	}

	/**
	 * 订单佣金回退 
	 */
	@Override
	public void cancleOrderCommis(SubItem subItem) {
		//获取订单项的用户信息
		UserDetail subUser = userDetailDao.getUserDetailById(subItem.getUserId());
		
		// 一级用户分佣金
		cancleUserCommis(subItem, subItem.getDistUserName(), subItem.getDistUserCommis(), subUser, 1);
		// 二级用户分佣金
		cancleUserCommis(subItem, subItem.getDistSecondName(), subItem.getDistSecondCommis(), subUser, 2);
		// 三级用户分佣金
		cancleUserCommis(subItem, subItem.getDistThirdName(), subItem.getDistThirdCommis(), subUser, 3);
		
		// 更新子订单状态
		subItemDao.updateCommisSettleSts(subItem.getSubItemId(), 1);
	}
	
	/**
	 * 给用户回退某个订单的分佣
	 * 
	 * @param subItem
	 * @param userName
	 * @param commisCash
	 */
	private void cancleUserCommis(SubItem subItem, String userName, Double commisCash, UserDetail subUser, Integer rank) {
		if (commisCash != null && commisCash > 0 && subItem.getCommisSettleSts() == 0 && userName != null) {
			// 减少未结算佣金和累积赚得佣金
			BigDecimal b = new BigDecimal(commisCash);
			commisCash = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); //保留2位小数
			userCommisDao.reduceUnSettleDistCommisAndTotalCommis(userName, commisCash);
			
			// 减少相应等级的用户分佣金
			if(rank==1){
				userCommisDao.reduceFirstDistCommis(userName, commisCash);
			}else if(rank==2){
				userCommisDao.reduceSecondDistCommis(userName, commisCash);
			}else {
				userCommisDao.reduceThirdDistCommis(userName, commisCash);
			}
			
			//记录佣金历史
			CommisChangeLog commisChangeLog = new CommisChangeLog();
			commisChangeLog.setSn(subItem.getSubItemNumber());
			//分佣用户
			UserDetail user = userDetailDao.getUserDetailByName(userName);
			commisChangeLog.setUserId(user.getUserId());
			commisChangeLog.setUserName(user.getUserName());
			commisChangeLog.setAmount(commisCash);
			commisChangeLog.setAddTime(new Date());
			commisChangeLog.setCommisType(CommisTypeEnum.REDUCE_COMMIS.value());
			//下级用户
			commisChangeLog.setSubUserName(subUser.getUserName());
			commisChangeLog.setSettleSts(0);
			commisChangeLogDao.saveCommisChangeLog(commisChangeLog);
		}
	}

}
