/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopGradeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.ShopGrade;
import com.legendshop.spi.service.ShopGradeService;
import com.legendshop.util.AppUtils;

/**
 * 用户等级服务.
 */
@Service("shopGradeService")
public class ShopGradeServiceImpl implements ShopGradeService {
	
	@Autowired
	private ShopGradeDao shopGradeDao;

	@Override
	public Integer saveShopGrade(ShopGrade shopGrade) {
		if (!AppUtils.isBlank(shopGrade.getGradeId())) {
			updateShopGrade(shopGrade);
			return shopGrade.getGradeId();
        }
        return shopGradeDao.saveShopGrade(shopGrade);
	}
	@Override
	public ShopGrade getShopGrade(Integer id) {
		return shopGradeDao.getShopGrade(id);
	}
	@Override
	public void deleteShopGrade(ShopGrade shopGrade) {
		shopGradeDao.deleteShopGrade(shopGrade);
	}
	@Override
	public void updateShopGrade(ShopGrade shopGrade) {
		shopGradeDao.updateShopGrade(shopGrade);
	}
	@Override
	public List<StatusKeyValueEntity> retrieveShopGrade() {
		return shopGradeDao.retrieveShopGrade();
	}
	@Override
	public PageSupport<ShopGrade> getShopGradePage(String curPageNO, ShopGrade shopGrade) {
		return shopGradeDao.getShopGradePage(curPageNO,shopGrade);
	}
}
