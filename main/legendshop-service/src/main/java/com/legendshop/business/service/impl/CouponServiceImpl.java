/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.CouponProdDao;
import com.legendshop.business.dao.CouponShopDao;
import com.legendshop.business.dao.DrawActivityDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponSourceEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.dto.DrawAwardsDto;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.model.entity.CouponShop;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 优惠券服务.
 */
@Service("couponService")
public class CouponServiceImpl  implements CouponService{
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(CouponServiceImpl.class);
	
	@Autowired
    private CouponDao couponDao;
    
	@Autowired
    private UserCouponDao userCouponDao;

	@Autowired
    private AttachmentManager attachmentManager;
    
    @Autowired
    private CouponProdDao couponProdDao;
    
    @Autowired
    private CouponShopDao couponShopDao; 
    
    @Autowired
    private DrawActivityDao drawActivityDao;
    
    @Autowired
    private UserDetailDao userDetailDao;
    
    @Autowired
    private UserCouponService userCouponService;

	@Override
    public List<Coupon> getCouponByShopId(Long  shopId){
        return couponDao.getCouponByShopId(shopId);
    }

    @Override
	public Coupon getCoupon(Long id) {
        return couponDao.getCoupon(id);
    }
    
    /**
     * 激活优惠券
     * @param coupon
     * @param userCoupon
     */
    public void activateCoupon(String userId,String userName,Coupon coupon,UserCoupon userCoupon){
    	//更新user_coupon表
    	userCoupon.setUserId(userId);
    	userCoupon.setUserName(userName);
    	userCoupon.setGetTime(new Date());
    	userCoupon.setGetSources(CouponSourceEnum.PC.toString());
    	userCoupon.setUseStatus(1);
    	userCouponDao.update(userCoupon);
    	
    	//更新coupon表
    	coupon.setBindCouponNumber(coupon.getBindCouponNumber()+1);
    	couponDao.updateCoupon(coupon);
    }
    
    @Override
	public void deleteCoupon(Coupon coupon) {
        couponDao.deleteCoupon(coupon);
    }

    @Override
	public Long saveCoupon(Coupon coupon) {
        if (!AppUtils.isBlank(coupon.getCouponId())) {
        	Coupon objCoupon=couponDao.getCoupon(coupon.getCouponId());
        	if(objCoupon!=null){
        		  
        		  objCoupon.setCouponName(coupon.getCouponName());
        		  objCoupon.setDescription(coupon.getDescription());
        		  //objCoupon.setStatus(coupon.getStatus());
        		  objCoupon.setStartDate(coupon.getStartDate());
        		  objCoupon.setEndDate(coupon.getEndDate());
        		 // objCoupon.setFullPrice(coupon.getFullPrice());
        		 // objCoupon.setOffPrice(coupon.getOffPrice());
        		  //objCoupon.setGetLimit(coupon.getGetLimit());
        		  //objCoupon.setFrequency(coupon.getFrequency());
        		  //objCoupon.setSendType(coupon.getSendType());
        		  objCoupon.setCouponNumber(coupon.getCouponNumber());
        		  objCoupon.setMessageType(coupon.getMessageType());
        		  updateCoupon(objCoupon);
                  return coupon.getCouponId();
        	}
        }
        if(CouponTypeEnum.CATEGORY.value().equals(coupon.getCouponType())){
        	coupon.setFullPrice(0d);
        }else{
        	coupon.setFullPrice(coupon.getFullPrice()==null?0d:coupon.getFullPrice());
        }
        coupon.setUseCouponNumber(0l);
        coupon.setBindCouponNumber(0l);
        coupon.setSendCouponNumber(0l);
        coupon.setCreateDate(new Date());
        return couponDao.saveCoupon(coupon);
    }

    @Override
	public void updateCoupon(Coupon coupon) {
        couponDao.updateCoupon(coupon);
    }

	@Override
	public String deleteCoupon(Long id,Long shopId,String source) {
		Date date = new Date();
		try {
			if(id==null){
				return "ID不能为空";
			}
			Coupon coupon=couponDao.getCoupon(id);
			if(coupon==null){
				return "找不到该礼券";
			}else if(coupon.getStatus()!=Constants.OFFLINE.intValue()){
				if(coupon.getEndDate().getTime() > date.getTime()){
					return "该礼券处于上线状态,请先下线";
				}
			}else if(coupon.getBindCouponNumber()>0){
				return "优惠券已被使用，不能删除！";
			}
			else if(shopId!=null && !shopId.equals(coupon.getShopId())){
				return "该礼券不属于本商城";
			}
//			if (coupon.getBindCouponNumber() != 0) {
//				return "该礼券已经有绑定使用情况,不能删除";
//			}
			if("admin".equals(source)){
				couponDao.deleteCoupon(coupon);
			}else{
				couponDao.shopDeleteCoupon(coupon.getCouponId());
			}
//			userCouponDao.deleteByCouponId(coupon.getCouponId());
//			if(coupon.getCouponType().equals("product")){
//				couponProdDao.deleteByCouponId(coupon.getCouponId());
//			}			
		}  catch (Exception e) {
			log.error("deleteCoupon fail with id " + id, e);
			return Constants.FAIL;
		}
		return  Constants.SUCCESS;
	}

	@Override
	public Long saveByCoupon(Coupon coupon, String userName, String userId, Long shopId, String fileName,MultipartFile rightFile) {
		
        if (AppUtils.isNotBlank(coupon.getCouponId())) {
        	Coupon orginCoupon = couponDao.getCoupon(coupon.getCouponId());
        	String orginRightTag = orginCoupon.getCouponPic();
	
        	if(!rightFile.isEmpty()){
        		//保存右上标签图片
            	orginCoupon.setCouponPic(savePicture(rightFile,userName,userId, shopId, coupon.getCouponId()));
        	}        	
     		
    		orginCoupon.setModifyDate(new Date());    	    	
    		couponDao.updateCoupon(orginCoupon);
            return coupon.getCouponId();
        }else{
        	coupon.setCreateDate(new Date());
        	coupon.setBindCouponNumber(0L);
        	coupon.setUseCouponNumber(0L);
        	coupon.setCouponProvider(coupon.getCouponProvider());        
        	coupon.setShopDel(1);  
        	Long couponId = couponDao.saveCoupon(coupon);
        	
        	if(AppUtils.isNotBlank(fileName)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, fileName, rightFile, AttachmentTypeEnum.SORT);
			}
        	
        	//保存红包 店铺 关联表
        	if(CouponTypeEnum.SHOP.value().equals(coupon.getCouponType())){
    			//选择店铺时，要拆分成店铺id	
    			if(AppUtils.isNotBlank(coupon.getShopIdList())){
    				String[] strArray = null; 
    				List<CouponShop> couponShops = new ArrayList<CouponShop>();
    				strArray = coupon.getShopIdList().split(",");
    				for(String str : strArray){
    					if(AppUtils.isNotBlank(str)){
    						try {
    							CouponShop couponShop = new CouponShop();
    							couponShop.setCouponId(couponId);
								Long shopIds = Long.valueOf(str);
								couponShop.setShopId(shopIds);
								couponShops.add(couponShop);
							} catch (Exception e) {
								
							}
    					}
    				}
    				if(AppUtils.isNotBlank(couponShops)){
    					couponShopDao.saveCouponShops(couponShops);
    				}
    			}
    		}
        	
        	return couponId;
        }
	}
	
	/** 保存类目图片 **/
	private String savePicture(MultipartFile formFile,String userName, String userId, Long shopId, Long shopCatId){//TODO upload 问题
		String filename= null;
		if ((formFile != null) && (formFile.getSize() > 0)) {
			filename = attachmentManager.upload(formFile);
			//保存附件表
			attachmentManager.saveImageAttachment(userName,userId,shopId, filename, formFile, AttachmentTypeEnum.SORT);
		}
		
		return filename;
	}

	//TODO upload 问题
	@Override
	public Long saveByCouponAndShopId(Coupon coupon, String userName,String userId, Long shopId) {
		MultipartFile rightFile = coupon.getFile();// 取得上传的文件		
		
        if (AppUtils.isNotBlank(coupon.getCouponId())) {
        	Coupon orginCoupon = couponDao.getCoupon(coupon.getCouponId());
        	String orginRightTag = orginCoupon.getCouponPic();
        	
        	//判断shopId 是否一致
        	if(AppUtils.isBlank(shopId) && !orginCoupon.getShopId().equals(shopId)){
        		throw new BusinessException("shop id not correct");
        	}
	
        	if(!rightFile.isEmpty()){
        		//保存右上标签图片
            	orginCoupon.setCouponPic(savePicture(rightFile,userName,userId, shopId, coupon.getCouponId()));
        	}        	
     		
    		orginCoupon.setModifyDate(new Date());    	    	
    		couponDao.updateCoupon(orginCoupon);
            return coupon.getCouponId();
        }else{
        	String fileName = null;

        	if ((rightFile != null) && (rightFile.getSize() > 0)) {
        		fileName = attachmentManager.upload(rightFile);
        		coupon.setCouponPic(fileName);
			}

        	coupon.setShopDel(0);
        	coupon.setCreateDate(new Date());
        	if(AppUtils.isBlank(coupon.getBindCouponNumber())){
        		coupon.setBindCouponNumber(0L);
        	}
        	//判断是否指定用户，IsDsignatedUser 不可能为空。
        	if(AppUtils.isBlank(coupon.getIsDsignatedUser())){
        		coupon.setIsDsignatedUser(0);//非针对特定用户发放
        	}
        	coupon.setUseCouponNumber(0L);  
        	coupon.setStatus(1);
        	Long couponId = couponDao.saveCoupon(coupon);
        	
        	if(AppUtils.isNotBlank(fileName)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, fileName, rightFile, AttachmentTypeEnum.SORT);
			}
        	
        	return couponId;
        }
	}

	/**
	 * 获取红包或者优惠券
	 */
	@Override
	public List<Coupon> getCouponByProvider(String getType, String provider, Date date) {
		return couponDao.getCouponByProvider(getType, provider, date);
	}

	@Override
	public PageSupport<Coupon> getCouponsPage(String curPageNO, String type) {
		return couponDao.getCouponsPage(curPageNO,type);
	}

	@Override

	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(String userId, int sizePage, String curPageNO,
			Integer useStatus, String couPro) {
		return couponDao.queryPersonalCouponDto(userId,sizePage,curPageNO,useStatus,couPro);
	}

	@Override
	public PageSupport<Coupon> queryCouponPage(String curPageNO, String userName) {
		return couponDao.queryCouponPage(curPageNO,userName);
	}

	@Override
	public PageSupport<Coupon> queryCoupon(StringBuffer sql,String curPageNO, StringBuffer sqlCount, List<Object> objects) {
		return couponDao.queryCoupon(sql,curPageNO,sqlCount,objects);
	}

	@Override
	public PageSupport<UserCoupon> queryUserCoupon(String sqlString, StringBuilder sqlCount, List<Object> objects,
			String curPageNO) {
		return userCouponDao.queryUserCoupon(sqlString,sqlCount,objects,curPageNO);
	}

	/**
	 * 获取优惠券或者红包
	 */
	@Override
	public PageSupport<Coupon> getIntegralCenterList(String curPageNO, String type) {
		return couponDao.getIntegralCenterList(curPageNO,type);
	}
	
	@Override
	public long getCouponCountByShopId(String type, String shopId) {
		return couponDao.getCouponCountByShopId(type, shopId);
	}
	
	@Override
	public PageSupport<Coupon> getIntegralList(String curPageNO, String type, String shopId) {
		return couponDao.getIntegralList(curPageNO,type,shopId);
	}

	@Override
	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(int sizePage, String curPageNO, String userId,
			String couPro) {
		return couponDao.queryPersonalCouponDto(sizePage,curPageNO,userId,couPro);
	}

	@Override
	public PageSupport<Coupon> queryCoupon(String curPageNO, Coupon coupon, DataSortResult result) {
		return couponDao.queryCoupon(curPageNO,coupon,result);
	}

	@Override
	public PageSupport<UserCoupon> queryUsecouponView(String curPageNO, String sqlString, StringBuilder sqlCount,
			List<Object> objects) {
		return couponDao.queryUsecouponView(curPageNO,sqlString,sqlCount,objects);
	}

	@Override
	public PageSupport<Coupon> getPlatform(String curPageNO, Coupon coupon) {
		return couponDao.getPlatform(curPageNO,coupon);
	}

	@Override
	public Integer batchDelete(String platfromIds) {
		String[] platfromIdList=platfromIds.split(",");
		int result=0;
		for(String platfromId:platfromIdList){
			Coupon coupon=couponDao.getCoupon(Long.valueOf(platfromId));
			String message=deleteCoupon(coupon.getId(), coupon.getShopId(),"admin");
			
			if(!message.equals(Constants.SUCCESS)){
				throw new RuntimeException(message);
			}
			
			result++;
		}
		
		
		return result;
	}

	@Override
	public int batchOffline(String platfromIds) {
		return couponDao.batchOffline(platfromIds);
	}

	@Override
	public PageSupport<Coupon> loadSelectCouponByDraw(String curPageNO, String couponName, Date startDate, Date endDate) {
		// 查找已经参加活动的红包
		List<DrawActivity> drawActivities = drawActivityDao.getCouponByDate(startDate, endDate);
		List<Long> couponIds = new ArrayList<>();
		for (DrawActivity drawActivity : drawActivities) {
			List<DrawAwardsDto> daDtos = JSONUtil.getArray(drawActivity.getAwardInfo(), DrawAwardsDto.class);
			if(AppUtils.isBlank(daDtos)) {
				continue;
			}
			for (DrawAwardsDto drawAwardsDto : daDtos) {
				if(drawAwardsDto.getAwardsType() == 4) {//红包
					couponIds.add(drawAwardsDto.getCoupon());
				}
			}
		}
		//查询可用的红包，排除在这个时间内已经参加的红包
		PageSupport<Coupon> ps = couponDao.loadSelectCouponByDraw(curPageNO, couponName, endDate, couponIds);
		return ps;
	}

	/**
	 * 获取可用商品优惠券
	 */
	@Override
	public List<Coupon> getProdsCoupons(Long shopId, Long prodId) {
		return couponDao.getProdsCoupons(shopId, prodId);
	}
	
	@Override
	public List<Coupon> getShopAndProdsCoupons(Long shopId, Long prodId) {
		List<Coupon> prodsCoupons = couponDao.getProdsCoupons(shopId, prodId);
		List<Coupon> PlatformCoupons = couponDao.getPlatformCoupons(shopId);
		prodsCoupons.addAll(PlatformCoupons);
		return prodsCoupons;
	}

	@Override
	public Long saveCouponByCoupon(Coupon coupon, String userName, String userId, Long shopId, String fileName, MultipartFile rightFile) {
		Long couponId = saveByCoupon(coupon, userName,userId, shopId, fileName,rightFile);

		// 根据券数量批量添加到userConpon, ps：如果是卡密的话才进行操作
		// 每次批量保存100条
		if (CouponGetTypeEnum.PWD.value().equals(coupon.getGetType())) {
			int saveCount = 100;// 每次批量保存100条
			int totalNum = coupon.getCouponNumber().intValue();// 需要保存的总条数
			int beginNum = 0;
			List<String> snList = new ArrayList<String>();// 券号的集合

			while (beginNum < totalNum) {
				int toNum = beginNum + saveCount;
				if (toNum > totalNum) {
					toNum = totalNum;
				}

				// 批量保存，如果券号或者卡密重复 报错，继续生成并保存，最多循环3次
				boolean saveSuccess = true;
				int loopCount = 0;// 循环次数
				do {
					List<UserCoupon> userCoupons = new ArrayList<UserCoupon>();
					for (int i = beginNum; i < toNum; i++) {

						// 生成券号
						String sn = null;
						do {
							sn = CommonServiceUtil.getRandomCouponNumberGenerator();
						} while (snList.contains(sn));
						snList.add(sn);

						UserCoupon userCoupon = new UserCoupon();
						userCoupon.setCouponId(couponId);
						userCoupon.setCouponName(coupon.getCouponName());
						userCoupon.setCouponSn(sn);
						userCoupon.setUseStatus(1);
						// 生成卡密
						String pwd = CommonServiceUtil.getRandomCouponPwdGenerator();
						userCoupon.setCouponPwd(pwd);
						userCoupons.add(userCoupon);
					}

					try {
						loopCount++;
						userCouponDao.save(userCoupons);
						saveSuccess = true;
					} catch (Exception e) {
						saveSuccess = false;
					}
				} while (!saveSuccess && loopCount < 3);

				beginNum = toNum;
			}
		}
		return couponId;
	}

	/* 
	 * 发送优惠卷
	 */
	@Override
	public Integer updateCoupon(Coupon coupon, List<String> result, Long couponId, List<String> result2, List<UserCoupon> lists) {
		Integer successCount = userCouponService.BindCouponByUser(result, lists);
		coupon.setBindCouponNumber(Long.valueOf(successCount));
		this.updateCoupon(coupon);
		return successCount;
	}

	/* 
	 * 添加优惠券
	 * */
	@Override
	public void saveByCouponAndShopId(Coupon coupon, String userName,String userId, Long shopId, String prodidList) {
		String userIdLists = coupon.getUserIdLists();
		//针对用户的优惠券发放
		if(AppUtils.isNotBlank(userIdLists)  && AppUtils.isNotBlank(coupon.getIsDsignatedUser()) && coupon.getIsDsignatedUser().equals(1) && !coupon.getGetType().equals("pwd")
				&& !coupon.getGetType().equals("points")){
			String[] userIds = userIdLists.split(",");
			Long  number = (long) userIds.length;
			coupon.setBindCouponNumber(number);
			coupon.setCouponNumber(number);
			coupon.setGetLimit(0L);
		}
		
		Long id = this.saveByCouponAndShopId(coupon, userName, userId, shopId);

		// 选择商品券时，要拆分成商品id
		if (AppUtils.isNotBlank(prodidList)) {
			String[] strArray = null;
			CouponProd couponProd = null;
			strArray = prodidList.split(",");
			for (String str : strArray) {
				couponProd = new CouponProd();
				couponProd.setCouponId(id);
				couponProd.setProdId(Long.parseLong(str));
				 if (!AppUtils.isBlank(couponProd.getCouponProdId())) {
					 couponProdDao.updateCouponProd(couponProd);
			        }else{
			        	couponProdDao.saveCouponProd(couponProd);
			        }
			}
		}
		if (AppUtils.isNotBlank(coupon.getIsDsignatedUser()) && coupon.getIsDsignatedUser().equals(1) && !coupon.getGetType().equals("pwd")
				&& !coupon.getGetType().equals("points")) {// 对指定用户发放,暂不支持卡密和积分
			//Long getLimit = coupon.getGetLimit(); //指定用户发放，前台没有填写限制个人领取数量后台接收默认为0导致，修改为默认每个人为一张
			String[] userIds = userIdLists.split(",");
			for (String couponUserId : userIds) {
				UserDetail userDetail = userDetailDao.getUserDetailById(couponUserId);
				//for (int i = 0; i < getLimit;) {
					String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
					UserCoupon userCoupon = userCouponService.getCouponByCouponSn(sn);
					if (AppUtils.isBlank(userCoupon)) {
						userCoupon = new UserCoupon();
						userCoupon.setCouponId(id);
						userCoupon.setCouponName(coupon.getCouponName());
						userCoupon.setCouponSn(sn);
						userCoupon.setUseStatus(1);
						userCoupon.setUserId(couponUserId);
						userCoupon.setUserName(userDetail.getUserName());
						userCoupon.setGetTime(new Date());
						userCouponService.saveUserCoupon(userCoupon);
						//i++;
					}
				//}
			}
		}
		// 根据券数量批量添加到userConpon, ps：如果是卡密的话才进行操作
		if (coupon.getGetType().equals("pwd")) {
			Long num = coupon.getCouponNumber();
			String sn = null;
			UserCoupon userCoupon = null;
			for (int i = 0; i < num; i++) {
				sn = CommonServiceUtil.getRandomCouponNumberGenerator();
				if (AppUtils.isBlank(userCouponService.getCouponByCouponSn(sn))) {
					userCoupon = new UserCoupon();
					userCoupon.setCouponId(id);
					userCoupon.setCouponName(coupon.getCouponName());
					userCoupon.setCouponSn(sn);
					userCoupon.setUseStatus(1);
					// 如果是卡密兑换的话，要增加卡密
					if (coupon.getGetType().equals("pwd")) {
						String pwd = CommonServiceUtil.getRandomCouponPwdGenerator();
						if (AppUtils.isBlank(userCouponService.getCouponByCouponPwd(pwd))) {
							userCoupon.setCouponPwd(pwd);
						} else {
							num++;
							continue;
						}
					}
					userCouponService.saveUserCoupon(userCoupon);
				} else {
					num++;
				}
			}
		}
	}

	@Override
	public Coupon getCouponByCouponId(Long couponId) {
		return couponDao.getCouponByCouponId(couponId);
	}

}
