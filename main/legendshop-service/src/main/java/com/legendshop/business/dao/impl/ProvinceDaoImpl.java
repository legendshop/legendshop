/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProvinceDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.Region;

/**
 * The Class ProvinceDaoImpl.
 */
@Repository
public class ProvinceDaoImpl extends GenericDaoImpl<Province, Integer> implements ProvinceDao {

	@Override
	public List<KeyValueEntity> getProvincesList() {
		List<Province> provinceList = this.queryAll();
		
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>();
		for (Province province : provinceList) {
			KeyValueEntity entity = new KeyValueEntity(province.getId(),province.getProvince());
			result.add(entity);
		}
		return result;
	}

	@Override
	@CacheEvict(value = "ProvinceList", allEntries=true)
	public void deleteProvince(Integer id) {
		deleteById(id);
	}

	@Override
	@CacheEvict(value = "ProvinceList", allEntries=true)
	public void updateProvince(Province province) {
		update(province);
	}

	@Override
	@Cacheable(value = "ProvinceList", key = "'getRegionList'")
	public List<Region> getRegionList() {
		return query("select distinct groupby as groupby from ls_provinces",Region.class);
	}

	@Override
	public Province getProvinceByProvinceid(String provinceid) {
		return this.getByProperties(new EntityCriterion().eq("provinceid", provinceid));
	}

	@Override
	@Cacheable(value = "ProvinceList", key = "'provinceName_'+ #provinceName")
	public List<Province> getProvinceByName(String provinceName) {
		return this.query("select * from ls_provinces where province = ?", Province.class, provinceName);
	}

}
