/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.auction;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.business.dao.auction.AuctionsDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.*;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.model.vo.FreezePredeposit;
import com.legendshop.processor.auction.scheduler.AuctionsSchedulerJob;
import com.legendshop.processor.auction.scheduler.SchedulerConstant;
import com.legendshop.processor.auction.scheduler.SchedulerMamager;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 拍卖活动服务实现类.
 *
 * @author legendshop
 */
@Service("auctionsService")
public class AuctionsServiceImpl implements AuctionsService {

	private final static Logger log = LoggerFactory.getLogger(AuctionsService.class);

	@Autowired
	private AuctionsDao auctionsDao;

	@Autowired
	private AuctionDepositRecDao auctionDepositRecDao;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private ImgFileService imgFileService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private ProdPropImageService prodPropImageService;

	@Resource(name = "freezePredepositProcessor")
	private EventProcessor<FreezePredeposit> freezePredepositProcessor;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private SubSettlementService subSettlementService;

	@Autowired
	private PaymentRefundService paymentRefundService;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private PdCashLogDao pdCashLogDao;

	/**
	 * 根据状态获取拍卖活动
	 */
	@Override
	public List<Auctions> getAuctionsBystatus(Long firstId, Long lastId, Long status) {
		return auctionsDao.getAuctionsBystatus(firstId, lastId, status);
	}

	/**
	 * 根据id获取拍卖活动
	 */
	@Override
	@Cacheable(value = "AuctionsDto", key = "#id")
	public Auctions getAuctions(Long id) {
		return auctionsDao.getAuctions(id);
	}

	/**
	 * 删除拍卖活动
	 */
	@Override
	public void deleteAuctions(Auctions auctions) {
		auctionsDao.deleteAuctions(auctions);
		//去除定时器
		String jobName = SchedulerConstant.AUCTIONS_SCHEDULER_ + auctions.getId();
		SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);

		Sku sku = skuDao.getSkuById(auctions.getSkuId());
		if (AppUtils.isNotBlank(sku)) {
			if (SkuActiveTypeEnum.AUCTION.value().equals(sku.getSkuType())) {
				//重置sku营销标记
				skuDao.updateSkuTypeById(auctions.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.AUCTION.value());
			}
		}
	}

	/**
	 * 保存拍卖活动
	 */
	@Override
	public Long saveAuctions(Auctions auctions) {
		if (!AppUtils.isBlank(auctions.getId())) {
			updateAuctions(auctions);
			return auctions.getId();
		}
		//标记拍卖活动的sku
		Sku sku = skuDao.getSkuById(auctions.getSkuId());
		sku.setSkuType(SkuActiveTypeEnum.AUCTION.value());
		skuDao.update(sku);
		return auctionsDao.saveAuctions(auctions);
	}

	/**
	 * 更新拍卖活动，没过期则Add 任务调度去
	 */
	@Override
	public void updateAuctions(Auctions auctions) {

		// 审核中（修改拍卖活动的时候）
		if (AuctionsStatusEnum.VALIDATING.value().intValue() == auctions.getStatus().intValue()) {
			Auctions oldAuctions = auctionsDao.getAuctions(auctions.getId());
			//修改了拍卖的sku
			if (!auctions.getSkuId().equals(oldAuctions.getSkuId())) {
				//先恢复原来sku标记的营销状态, 然后重新标记新的sku
				skuDao.updateSkuTypeById(oldAuctions.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.AUCTION.value());
				skuDao.updateSkuTypeById(auctions.getSkuId(), SkuActiveTypeEnum.AUCTION.value(), SkuActiveTypeEnum.PRODUCT.value());
			}
		}

		int count = auctionsDao.updateAuctions(auctions);
		if (count > 0) {

			//上线中
			if (AuctionsStatusEnum.ONLINE.value().intValue() == auctions.getStatus().intValue()) {

				// 没有过期的
				if (auctions.getEndTime().after(new Date())) {
					String jobName = SchedulerConstant.AUCTIONS_SCHEDULER_ + auctions.getId();
					SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
					// 活动上线 同时Add 任务调度去
					Auctions _object = new Auctions();

					_object.setId(auctions.getId());
					_object.setAuctionsTitle(auctions.getAuctionsTitle());
					_object.setStartTime(auctions.getStartTime());
					_object.setEndTime(auctions.getEndTime());
					_object.setProdId(auctions.getProdId());
					_object.setSkuId(auctions.getSkuId());
					_object.setDelayTime(auctions.getDelayTime());
					_object.setIsSecurity(auctions.getIsSecurity());

					if (AppUtils.isNotBlank(auctions.getFixedPrice())) {
						_object.setFixedPrice(auctions.getFixedPrice());
					}
					String jsonValue = JSONUtil.getJson(_object);
					long time = auctions.getEndTime().getTime() - 5000;
					Date earlier_date = new Date(time);
					SchedulerMamager.getInstance().addJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME,
							AuctionsSchedulerJob.class, earlier_date, jsonValue);
				}

				//完成状态
			} else if (AuctionsStatusEnum.FINISH.value().intValue() == auctions.getStatus().intValue()) {
				Sku sku = skuDao.getSkuById(auctions.getSkuId());
				sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
				skuDao.update(sku);
				//拒绝
			} else if (AuctionsStatusEnum.FAILED.value().intValue() == auctions.getStatus().intValue()) {
				Sku sku = skuDao.getSkuById(auctions.getSkuId());
				sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
				skuDao.update(sku);
			}
		}
	}

	/**
	 * 获取当前商品的价格
	 *
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@Override
	public String currentQueryPrice(Long prodId, Long skuId) {
		if (AppUtils.isNotBlank(skuId) && skuId != 0) {
			return auctionsDao.get("select price from ls_sku where sku_id=? ", String.class, skuId);
		} else {
			return auctionsDao.get("select cash from ls_prod where prod_id=? ", String.class, prodId);
		}
	}

	/**
	 * 更改拍卖活动状态为完成
	 */
	@Override
	public int finishAuction(Long id) {
		return auctionsDao.finishAuction(id);
	}

	/**
	 * 更新中标记录,定时器使用
	 */
	@Override
	@Caching(evict = {@CacheEvict(value = "AuctionsDetailDto", key = "#id"), @CacheEvict(value = "AuctionsDto", key = "#id")})
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice, Date date, Long id) {

		auctionsDao.update("update ls_auctions set crowd_watch=? , bidding_number=? , cur_price=?,end_time=?  where id=? ",
				crowdWatch, bidCount, curPrice, date, id);

		//重置SKU的营销标记
		Auctions auctions = auctionsDao.getAuctions(id);
		if (auctions != null) {
			skuDao.updateSkuTypeById(auctions.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.AUCTION.value());
		}
	}

	/**
	 * 查询所有在线并且不过期的活动
	 */
	@Override
	public List<Auctions> getonlineList() {
		return auctionsDao.getonlineList();
	}

	@Override
	public Long getMinId() {
		return auctionsDao.getMinId();
	}

	@Override
	public Long getMaxId() {
		return auctionsDao.getMaxId();
	}

	/**
	 * 支付保证金
	 *
	 * @param paimaiId
	 * @param amount
	 * @return
	 */
	@Override
	public String payAuction(Long paimaiId, double amount, String userId, Long shopId, String auctionsTitle) {
		FreezePredeposit predeposit = new FreezePredeposit();
		predeposit.setUserId(userId);
		predeposit.setAmount(amount);
		String sn = CommonServiceUtil.getRandomSn();
		predeposit.setSn(sn);
		freezePredepositProcessor.process(predeposit);
		String result = predeposit.getResult();
		// 支付成功
		if (Constants.SUCCESS.equals(result)) {
			AuctionDepositRec depositRec = new AuctionDepositRec();
			depositRec.setAId(paimaiId);
			depositRec.setSubNumber(sn);
			depositRec.setPayMoney(new BigDecimal(amount));
			depositRec.setUserId(userId);
			depositRec.setOrderStatus(1);
			depositRec.setPayName("预付款支付");
			depositRec.setPayTime(new Date());
			depositRec.setFlagStatus(0l);
			depositRec.setShopId(shopId);
			depositRec.setAuctionsTitle(auctionsTitle);
			auctionDepositRecDao.save(depositRec);
		}
		return result;
	}

	/**
	 * 加载拍卖活动详情和图片
	 */
	@Override
	@Cacheable(value = "AuctionsDetailDto", key = "#paimaiId")
	public Auctions getAuctionsDetails(Long paimaiId) {
		Auctions auction = auctionsDao.getAuctions(paimaiId);

		// 下线或者未通过状态
		if (auction == null || AuctionsStatusEnum.OFFLINE.value().equals(auction.getStatus()) || AuctionsStatusEnum.FAILED.value().equals(auction.getStatus())) {
			return auction;
		}
		//sku或者商品的图片
		List<String> skuPicList = new ArrayList<String>();
		//获取拍卖对应的商品或者单品
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
			auction.setCnProperties(dto.getCnProperties());
		}
		if (auction.getSkuId().intValue() == 0) {
			//获取商品图片
			skuPicList = imgFileService.queryProductPics(auction.getProdId());
			auction.setSkuPicList(skuPicList);
		} else {
			Sku sku = skuService.getSkuByProd(auction.getProdId(), auction.getSkuId());
			Long prodId = sku.getProdId();
			List<KeyValueEntity> keyVal = skuService.getSkuImg(sku.getProperties());
			if (AppUtils.isNotBlank(keyVal)) {
				for (Iterator<KeyValueEntity> iterator = keyVal.iterator(); iterator.hasNext(); ) {
					KeyValueEntity entity = (KeyValueEntity) iterator.next();
					List<ProdPropImage> prodImg = prodPropImageService.getProdPropImageByKeyVal(prodId, Long.valueOf(entity.getKey()),
							Long.valueOf(entity.getValue()));
					if (AppUtils.isNotBlank(prodImg)) {
						for (Iterator<ProdPropImage> iterator2 = prodImg.iterator(); iterator2.hasNext(); ) {
							ProdPropImage prodPropImage = (ProdPropImage) iterator2.next();
							skuPicList.add(prodPropImage.getUrl());
						}
					}
				}
			}
		}
		auction.setSkuPicList(skuPicList);
		return auction;
	}

	/**
	 * 排除预错商品
	 *
	 * @return
	 */
	@Override
	public boolean excludePresell(Long prodId, Long skuId) {
		return auctionsDao.excludePresell(prodId, skuId);
	}

	/**
	 * 更新出价记录
	 */
	@Override
	public void updateAuctionsBidNumber(Long id) {
		auctionsDao.update("update ls_auctions set bidding_number=bidding_number+1 where id=? ", id);
	}

	/**
	 * 更新参加人数
	 */
	@Override
	public void updateAuctionsEnrolNumber(Long id, Long count) {
		auctionsDao.update("update ls_auctions set enrol_number=? where id=? ", count, id);
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Auctions auctions) {
		return auctionsDao.queryAuctionListPage(curPageNO, auctions);
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Long shopId, Auctions auctions) {
		//根据查询出来的团购商品信息，查询商品状态，返回
		PageSupport<Auctions> ps = auctionsDao.queryAuctionListPage(curPageNO, shopId, auctions);
		List<Auctions> auctionsList = ps.getResultList();
		if (AppUtils.isNotBlank(auctionsList)) {

			for (Auctions auctionDetail : auctionsList) {
				Product product = productDao.getProduct(auctionDetail.getProdId());
				if (AppUtils.isNotBlank(product)) {
					auctionDetail.setProdStatus(Long.parseLong(product.getStatus().toString()));
				}
			}
		}

		return ps;
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, ProductSearchParms parms) {
		return auctionsDao.queryAuctionListPage(curPageNO, parms);
	}

	@Override
	public PageSupport<Auctions> queryPageAuctionList(String curPageNO, ProductSearchParms parms) {
		return auctionsDao.queryPageAuctionList(curPageNO, parms);
	}

	@Override
	public int updateAuctionStatus(Long auctionStatus, Long auctionId) {
		return auctionsDao.updateAuctionStatus(auctionStatus, auctionId);
	}

	@Override
	public boolean findIfJoinAuctions(Long productId) {
		return auctionsDao.findIfJoinAuctions(productId);
	}

	@Override
	public Auctions isAuction(Long prodId, Long skuId) {
		return auctionsDao.isAuction(prodId, skuId);
	}

	/**
	 * 终止拍卖活动,退还拍卖保证金以及释放sku
	 *
	 * @param id
	 * @return
	 */
	@Override
	public String offlineAuctions(Long id) {

		// 获得此次活动的竞拍用户集合
		List<AuctionDepositRec> auctionDepositRecList = auctionDepositRecService.getAuctionDepositRecByAid(id);

		if (AppUtils.isNotBlank(auctionDepositRecList)) {

			for (AuctionDepositRec auctionDepositRec : auctionDepositRecList) {

				// 进行退款操作
				boolean result = this.bidUserRefund(auctionDepositRec);
				// 如果成功 形成退款标识动作
				if (result) {
					auctionDepositRec.setFlagStatus(1L);
					// update FlagStatus
					auctionDepositRecService.updateAuctionDepositRec(auctionDepositRec);
				} else {
					return "终止活动退款失败，请联系平台处理";
				}
			}
		}
		// 更改活动为已失效
		auctionsDao.updateAuctionStatus(AuctionsStatusEnum.OFFLINE.value(), id);

		//处理该活动 0为已处理，1未处理
		auctionsDao.updateAuctionsFlagStatus(id, 1);

		//重置SKU的营销标记
		Auctions auctions = auctionsDao.getAuctions(id);
		if (auctions != null) {
			skuDao.updateSkuTypeById(auctions.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.AUCTION.value());
		}
		//auctionsDao.(id);
		return Constants.SUCCESS;
	}

	/**
	 * 竞拍失败的用户退款操作 回滚用户的保证金
	 *
	 * @param auctionDepositRec
	 * @return
	 */
	@Override
	public boolean bidUserRefund(AuctionDepositRec auctionDepositRec) {

		String userId = auctionDepositRec.getUserId();
		String subNumbers = auctionDepositRec.getSubNumber();
		double money = auctionDepositRec.getPayMoney().doubleValue();
		String outRefundNo = CommonServiceUtil.getRandomSn();
		if (!"FULL_PAY".equals(auctionDepositRec.getPayType())) {
			SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(auctionDepositRec.getSubSettlementSn());
			SysSubReturnForm returnForm = new SysSubReturnForm();
			returnForm.setOutRequestNo(outRefundNo);
			returnForm.setFlowTradeNo(subSettlement.getFlowTradeNo());
			returnForm.setSubSettlementSn(subSettlement.getSubSettlementSn());
			returnForm.setOrderDateTime(subSettlement.getCreateTime());
			returnForm.setReturnAmount(subSettlement.getCashAmount());
			returnForm.setRefundTypeId(subSettlement.getPayTypeId());
			returnForm.setPayAmount(subSettlement.getCashAmount());

			/**
			 * 第三方支付退款处理--->
			 */
			Map<String, Object> refundMap = paymentRefundService.refund(returnForm);
			return true;
		}

		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return false;
		}
		Integer count = auctionDepositRecDao.update(
				"update ls_pd_cash_holding set status= ?,release_time=?  where sn=?  and user_id=?  and status=0 and type=?",
				new Object[]{1, new Date(), subNumbers, userId, "auctions_freeze"}); // 更新
		// hoding
		// 如果更新成功才执行下面的动作
		if (count == 0) {
			return false;
		}
		Double availablePredeposit = userDetail.getAvailablePredeposit(); // 可用金额
		Double freezePredeposit = userDetail.getFreezePredeposit(); // 冻结金额
		Double updatePredeposit = Arith.add(availablePredeposit, money); // avaialbe_amout
		// +
		// 保证金
		Double updatefreePredeposit = Arith.sub(freezePredeposit, money); // hold_amount
		// -
		// 保证金
		count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);


		if (count == 0) {
			log.error("回滚用户的拍卖保证金 失败 by userId ={} refundMoney={}", userId, money);
			throw new BusinessException("回滚用户的拍卖保证金 失败");
		} else {
			/*
			 * insert PdCashLog  退还用户保证金
			 */
			PdCashLog pdCashLog = new PdCashLog();
			pdCashLog.setUserId(userId);
			pdCashLog.setUserName(userDetailDao.getUserNameByUserId(userId));
			pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
			pdCashLog.setAmount(money);
			pdCashLog.setAddTime(new Date());
			pdCashLog.setLogDesc("拍卖活动终止，退还保证金:" + money);
			pdCashLog.setSn(subNumbers);
			pdCashLogDao.savePdCashLog(pdCashLog);
		}

		return true;
	}
}
