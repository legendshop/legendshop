/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.JobStatusDao;
import com.legendshop.model.entity.JobStatus;
import com.legendshop.spi.service.JobStatusService;
import com.legendshop.util.AppUtils;

/**
 * 定时任务接口.
 */
@Service("jobStatusService")
public class JobStatusServiceImpl implements JobStatusService {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(JobStatusServiceImpl.class);

	@Autowired
	private JobStatusDao jobStatusDao;

	public JobStatus getJobStatus(Long id) {
		return jobStatusDao.getJobStatus(id);
	}

	public void deleteJobStatus(JobStatus jobStatus) {
		jobStatusDao.deleteJobStatus(jobStatus);
	}

	public Long saveJobStatus(JobStatus jobStatus) {
		if (!AppUtils.isBlank(jobStatus.getId())) {
			updateJobStatus(jobStatus);
			return jobStatus.getId();
		}
		return jobStatusDao.saveJobStatus(jobStatus);
	}

	public void updateJobStatus(JobStatus jobStatus) {
		jobStatusDao.updateJobStatus(jobStatus);
	}

	@Override
	public JobStatus getJobStatusByJobName(String jobName) {
		return jobStatusDao.getJobStatusByJobName(jobName);
	}

	/**
	 * 启动一个Job 返回true 表示成功启动，false表示启动失败，或者正在运行
	 * 
	 */
	@Override
	public JobStatus startJob(String jobName) {
		// 查询当前job的状态
		JobStatus jobStatus = getJobStatusByJobName(jobName);
		if (jobStatus == null || jobStatus.getStatus().intValue() == 0) {
			log.info("<!--- distDay is today, settle distribution begin");
			// 如果没有该JOB的记录，则插入，否则 更改JOB的状态为 running
			if (jobStatus == null) {
				jobStatus = new JobStatus();
				jobStatus.setJobName(jobName);
				jobStatus.setStatus(1L);//正在运行
				jobStatus.setUpdateTime(new Date());
				Long jobStatusId = saveJobStatus(jobStatus);
				jobStatus.setId(jobStatusId);
			} else {
				jobStatus.setStatus(1L);
				jobStatus.setUpdateTime(new Date());
				updateJobStatus(jobStatus);
			}
		}
		jobStatus.setStartSuccessed(true);//成功启动
		return jobStatus;
	}

	/**
	 * 结束一个Job
	 */
	@Override
	public boolean stopJob(JobStatus jobStatus) {
		boolean result = jobStatus.getStatus() != 0;
		//分销结算结束，更新 JOB的状态
		jobStatus.setStatus(0L);
		jobStatus.setUpdateTime(new Date());
		updateJobStatus(jobStatus);
		return result;
	}
}
