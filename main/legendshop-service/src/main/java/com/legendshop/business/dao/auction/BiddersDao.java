/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Bidders;

/**
 * 竞拍Dao接口.
 */

public interface BiddersDao extends GenericDao<Bidders, Long> {
     
	public abstract Bidders getBidders(Long id);
	
    public abstract int deleteBidders(Bidders bidders);
	
	public abstract Long saveBidders(Bidders bidders);
	
	public abstract int updateBidders(Bidders bidders);

	List<Bidders> getBiddersByAuctionId(Long auctionsId,int offset, int limit);

	public abstract long findBidsCount(Long paimaiId);

	public abstract List<Bidders> getBidders(Long aId, Long prodId, Long skuId);

	public abstract PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long id);

	public abstract PageSupport<Bidders> queryBiddListPage(String curPageNO, Long id);

	public abstract PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long aId, Long prodId, Long skuId);

	public abstract PageSupport<Bidders> getBiddersPage(String curPageNO, Bidders bidders);
	
 }
