/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.dto.ThemeRelatedDto;
import com.legendshop.model.entity.ThemeRelated;

/**
 *相关专题表Dao
 */
public interface ThemeRelatedDao extends Dao<ThemeRelated, Long> {
	
	public abstract List<ThemeRelated> getThemeRelatedByTheme(Long themeId);
     

	public abstract ThemeRelated getThemeRelated(Long id);
	
    public abstract int deleteThemeRelated(ThemeRelated themeRelated);
    
    public abstract int deleteThemeRelatedByTid(Long theme_id);
	
	public abstract Long saveThemeRelated(ThemeRelated themeRelated);
	
	public abstract int updateThemeRelated(ThemeRelated themeRelated);

    List<ThemeRelatedDto> getThemeRelatedDtoByTheme(Long themeId);
}
