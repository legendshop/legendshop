/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.CommonUtilDao;
import com.legendshop.spi.service.CommonUtil;

/**
 * 公共角色权限服务.
 */
@Service("commonUtil")
public class CommonUtilImpl implements CommonUtil {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(CommonServiceUtil.class);

	@Autowired
	private CommonUtilDao commonUtilDao;

	/**
	 * 保存后台管理员角色
	 *
	 */
	@Override
	public void saveAdminRight(String userId) {
		commonUtilDao.saveAdminRight(userId);
	}

	// 保存用户角色
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.CommonUtil#saveUserRight(com.legendshop
	 * .core.dao.impl.BaseDaoImpl, java.lang.String)
	 */
	@Override
	public void saveUserRight(String userId) {
		commonUtilDao.saveUserRight(userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.CommonUtil#removeAdminRight(com.legendshop
	 * .core.dao.impl.BaseDaoImpl, java.lang.String)
	 */
	@Override
	public void removeAdminRight(String userId) {
		commonUtilDao.removeAdminRight(userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.CommonUtil#removeUserRight(com.legendshop
	 * .core.dao.impl.BaseDaoImpl, java.lang.String)
	 */
	@Override
	public void removeUserRight(String userId) {
		commonUtilDao.removeUserRight(userId);
	}
}
