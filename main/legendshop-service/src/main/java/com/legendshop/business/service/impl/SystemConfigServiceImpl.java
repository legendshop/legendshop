/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SystemConfigDao;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.SystemConfigService;

/**
 * 系统配置
 */
@Service("systemConfigService")
public class SystemConfigServiceImpl implements SystemConfigService {
	
	@Autowired
	private SystemConfigDao systemConfigDao;
	
	public SystemConfig getSystemConfig() {
		return systemConfigDao.getSystemConfig();
	}

	public void updateSystemConfig(SystemConfig systemConfig) {
		systemConfigDao.updateSystemConfig(systemConfig);
	}

	@Override
	public SystemConfig getSysWechatCode() {
		return systemConfigDao.getSysWechatCode();
	}

}
