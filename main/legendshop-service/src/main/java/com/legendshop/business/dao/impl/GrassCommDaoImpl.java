/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.GrassComm;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassCommDao;

/**
 * The Class GrassCommDaoImpl. 种草文章评论表Dao实现类
 */
@Repository
public class GrassCommDaoImpl extends GenericDaoImpl<GrassComm, Long> implements GrassCommDao{

	/**
	 * 根据Id获取种草文章评论表
	 */
	public GrassComm getGrassComm(Long id){
		return getById(id);
	}

	/**
	 *  删除种草文章评论表
	 *  @param grassComm 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassComm(GrassComm grassComm){
    	return delete(grassComm);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassComm(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草文章评论表
	 */
	public Long saveGrassComm(GrassComm grassComm){
		return save(grassComm);
	}

	/**
	 *  更新种草文章评论表
	 */		
	public int updateGrassComm(GrassComm grassComm){
		return update(grassComm);
	}

	/**
	 * 分页查询种草文章评论表列表
	 */
	public PageSupport<GrassComm>queryGrassComm(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassComm.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public List<GrassComm> getGrassCommByGrassId(Long grassId) {
		String sql="SELECT  id,user_id,content,create_time,`STATUS`,user_name,user_image FROM ls_grass_comm WHERE gra_id=?";
		return query(sql, GrassComm.class,grassId);
	}

	@Override
	public PageSupport<GrassComm> queryGrassCommByGrassId(String currPage, int pageSize, Long grassId) {
		CriteriaQuery query = new CriteriaQuery(GrassComm.class, currPage);
		query.setPageSize(pageSize);
		query.eq("graId",grassId);
		query.addDescOrder("createTime");
		PageSupport ps = queryPage(query);
		return ps;
	}

}
