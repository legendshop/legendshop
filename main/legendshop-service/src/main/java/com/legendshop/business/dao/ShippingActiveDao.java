/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShippingActive;

/**
 * 包邮活动Dao
 */
public interface ShippingActiveDao extends Dao<ShippingActive, Long> {
    
    /**
     * 根据Id获取包邮活动
     */
	public abstract ShippingActive getShippingActive(Long id);
	
	/**
     * 删除包邮活动
     */
    public abstract int deleteShippingActive(ShippingActive shippingActive);
	
    /**
     * 保存包邮活动
     */
	public abstract Long saveShippingActive(ShippingActive shippingActive);
	
	/**
     * 删除包邮活动
     */
	public abstract int updateShippingActive(ShippingActive shippingActive);

	 /**
     * 获取包邮活动列表
     */
	public abstract PageSupport<ShippingActive> queryShippingActiveList(String curPageNO, Long shopId, ShippingActive shippingActive);

	/**
	 * 根据ID获取包邮活动
	 */
	public abstract ShippingActive getShippingActiveById(Long activeId, Long shopId);

	public abstract List<ShippingActive> getShopRuleEngineShippingActive(Long shopId);

	public abstract List<ShippingActive> queryOnLineShippingActive(Long shopId, Long prodId);

	/**
	 * 清除查询缓存
	 * @param shopId
	 * @param prodIds
	 */
	void clearShippingActiveProductCache(Long shopId, Long[] prodIds);
 }
