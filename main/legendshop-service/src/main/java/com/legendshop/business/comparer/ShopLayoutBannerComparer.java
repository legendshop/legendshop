package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;


/**
 * 
 * @author tony
 *
 */
public class ShopLayoutBannerComparer implements DataComparer<ShopLayoutBanner, ShopLayoutBanner> {

	@Override
	public boolean needUpdate(ShopLayoutBanner dto, ShopLayoutBanner dbObj,
			Object obj) {
		return false;
	}

	@Override
	public boolean isExist(ShopLayoutBanner dto, ShopLayoutBanner dbObj) {
		if(dto.getImageFile().equals(dbObj.getImageFile())){
			return true;
		}
		return false;
	}

	
	@Override
	public ShopLayoutBanner copyProperties(ShopLayoutBanner dtoj, Object obj) {
		return dtoj;
	}

}
