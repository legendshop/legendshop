package com.legendshop.business.resolver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.legendshop.model.constant.MarketingRuleCalTypeEnum;
import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.spi.resolver.order.IActiveRuleResolver;
import com.legendshop.util.AppUtils;


/**
 * 满减
 * @author Tony
 *
 */
@Component
public class ManjianRuleResolver  implements IActiveRuleResolver {

	@Override
	public void execute(MarketingDto marketingDto, RuleResolver item,MarketRuleContext context) {
		if(marketingDto.getIsAllProds() == 1){//全场
			gobalRuleResolver(marketingDto,item,context);
		}else{
			prodRuleResolver(marketingDto,item,context);
		}
	}
	
	
	/*
	 * 部分商品
	 */
	private void prodRuleResolver(MarketingDto marketingDto,RuleResolver item, MarketRuleContext context) {
		if(!context.isManJianExecuted(item.getProdId(), item.getSkuId()) 
				&& !context.isManZeExecuted(item.getProdId(), item.getSkuId())
				&& !context.isXianShiExecuted(item.getProdId(), item.getSkuId())){
			if(AppUtils.isBlank(marketingDto.getMarketingRuleDtos())){
				return;
			}
			MarketingProdDto marketingProdDto=context.filterMarketing(marketingDto.getMarketingProdDtos(),item.getProdId(),item.getSkuId());
			if(marketingProdDto!=null){
				List<MarketingRuleDto> arrays=sortRule(marketingDto.getMarketingRuleDtos());
				for(MarketingRuleDto rule:arrays){
					CartMarketRules rules=new CartMarketRules();
					rules.setMarketId(marketingDto.getMarketId());
					rules.setMarketType(marketingDto.getType());
					rules.setRuleId(rule.getRuleId());
					StringBuilder sb=new StringBuilder();
					//满金额
					if(MarketingRuleCalTypeEnum.BY_MONEY.value().equals(rule.getCalType())){
						sb.append("满减活动 [满"+rule.getFullPrice()+"元减"+rule.getOffAmount()+"]");
					}
					//满件
					else if(MarketingRuleCalTypeEnum.BY_NUMBER.value().equals(rule.getCalType())){
						sb.append("满减活动 [满"+rule.getFullPrice().intValue()+"件减"+rule.getOffAmount()+"]");
					}
					rules.setPromotionInfo(sb.toString());
					item.addCartMarketRules(rules);
				}
				context.executeManJian(item.getProdId(), item.getSkuId());
			}
		}
	}
	
	
	/*
	 * 全部商品
	 */
	private void gobalRuleResolver(MarketingDto marketingDto,RuleResolver item, MarketRuleContext context) {
		if(!context.isManJianExecuted(item.getProdId(), item.getSkuId()) 
				&& !context.isManZeExecuted(item.getProdId(), item.getSkuId())
				&& !context.isXianShiExecuted(item.getProdId(), item.getSkuId())){
			if(AppUtils.isBlank(marketingDto.getMarketingRuleDtos())){
				return;
			}
			List<MarketingRuleDto> arrays=sortRule(marketingDto.getMarketingRuleDtos());
			for(MarketingRuleDto rule:arrays){
				CartMarketRules rules=new CartMarketRules();
				rules.setMarketId(marketingDto.getMarketId());
				rules.setMarketType(marketingDto.getType());
				rules.setRuleId(rule.getRuleId());
				StringBuilder sb=new StringBuilder();
				//满金额
				if(MarketingRuleCalTypeEnum.BY_MONEY.value().equals(rule.getCalType())){
					sb.append("满减活动 [满"+rule.getFullPrice()+"元减"+rule.getOffAmount()+"]");
				}
				//满件
				else if(MarketingRuleCalTypeEnum.BY_NUMBER.value().equals(rule.getCalType())){
					sb.append("满减活动 [满"+rule.getFullPrice().intValue()+"件减"+rule.getOffAmount()+"]");
				}
				rules.setPromotionInfo(sb.toString());
				item.addCartMarketRules(rules);
			}
			context.executeManJian(item.getProdId(), item.getSkuId());
		}
	}

	/**
	 * 从高到低排序
	 * @return
	 */
	private List<MarketingRuleDto> sortRule(List<MarketingRuleDto> rules ){
		if(AppUtils.isBlank(rules)){
			return null;
		}
		Object[] arrays=rules.toArray();
		for (int i = 0; i < arrays.length; i++) {
			for (int j = i+1; j < arrays.length; j++) {
				MarketingRuleDto dtoi=(MarketingRuleDto)arrays[i];
				MarketingRuleDto dtoj=(MarketingRuleDto)arrays[j];
				if(dtoi.getOffAmount()<dtoj.getOffAmount()){
					MarketingRuleDto temp=dtoi;
					arrays[i]=arrays[j];
					arrays[j]=temp;
				}
			}
		}
		List<MarketingRuleDto> list=new ArrayList<MarketingRuleDto>();
		for (Object object : arrays) {
			list.add((MarketingRuleDto)object);
		}
		arrays=null;
		return list;
	}

}
