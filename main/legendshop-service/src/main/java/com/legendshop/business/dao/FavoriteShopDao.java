/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FavoriteShop;
/**
 * 店铺收藏Dao.
 */
public interface FavoriteShopDao extends GenericDao< FavoriteShop, Long> {
     
	public abstract FavoriteShop getfavoriteShop(Long id);
	
    public abstract void deletefavoriteShop(FavoriteShop favoriteShop);
	
	public abstract Long savefavoriteShop(FavoriteShop favoriteShop);
	
	public abstract void updatefavoriteShop(FavoriteShop favoriteShop);
	
	public abstract boolean deletefavoriteShop(Long id, String userId);
	
	public abstract boolean isExistsFavoriteShop(String userId,Long shopId);
	
	public abstract Long getShopId(String shopName);
	
	public abstract void deletefavoriteShop(Long id);

	public abstract void deletefavoriteShop(String userId, List<Long> idList);

	public abstract void deleteAllfavoriteShop(String userId);

	public abstract int deletefavoriteShopByShopIdAndUserId(Long shopId, String userId);
	
	public abstract Long getfavoriteShopLength(String userId);

	PageSupport<FavoriteShop> collectShop(String curPageNO, String userId);

	public abstract PageSupport<FavoriteShop> querySimplePage(String curPageNO, String userId);

	public abstract void deletefavoriteShops(String[] favIds, String userId);
	
	public abstract Long getfavoriteLength(String userId);

	/**获取收藏的店铺列表
	 * 
	 * @param curPageNO
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	public abstract PageSupport<FavoriteShop> queryFavoriteShop(String curPageNO, Integer pageSize, String userId);
 }
