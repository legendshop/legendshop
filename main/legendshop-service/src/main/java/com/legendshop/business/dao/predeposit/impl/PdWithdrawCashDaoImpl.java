/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.predeposit.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.predeposit.PdWithdrawCashDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.util.AppUtils;

/**
 * 预存款提现Dao实现类
 * 
 */
@Repository
public class PdWithdrawCashDaoImpl extends GenericDaoImpl<PdWithdrawCash, Long> implements PdWithdrawCashDao {

	/**
	 * 获取预存款提现
	 */
	public PdWithdrawCash getPdWithdrawCash(Long id) {
		return getById(id);
	}

	/**
	 * 删除预存款提现
	 */
	public int deletePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		return delete(pdWithdrawCash);
	}

	/**
	 * 保存预存款提现
	 */
	public Long savePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		return save(pdWithdrawCash);
	}

	/**
	 * 更新预存款提现
	 */
	public int updatePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		return update(pdWithdrawCash);
	}

	/**
	 * 获取预存款提现
	 */
	@Override
	public PdWithdrawCash getPdWithdrawCash(Long id, String userId) {
		return getByProperties(new EntityCriterion().eq("id", id).eq("userId", userId));
	}

	/**
	 * 获取预存款提现页面
	 */
	@Override
	public PageSupport<PdWithdrawCash> getPdWithdrawCashPage(String curPageNO, String userId, String state) {
		
		curPageNO = AppUtils.isBlank(curPageNO) ? "1" : curPageNO;
		CriteriaQuery cq = new CriteriaQuery(PdWithdrawCash.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("userId", userId);
		cq.like("paymentState", state);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	/**
	 * 获取预存款提现列表页面
	 */
	@Override
	public PageSupport<PdWithdrawCash> getPdWithdrawCashListPage(String curPageNO, PdWithdrawCash pdWithdrawCash, DataSortResult result, Integer pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(PdWithdrawCash.class, curPageNO);
		QueryMap paramMap = new QueryMap();
		if(AppUtils.isNotBlank(pdWithdrawCash.getNickName())) {
			paramMap.like("nickName", pdWithdrawCash.getNickName().trim());
		}
		if (AppUtils.isNotBlank(pdWithdrawCash.getUserName())) {
			paramMap.like("userName", pdWithdrawCash.getUserName().trim());
		}
		paramMap.put("paymentState", pdWithdrawCash.getPaymentState());
		if (AppUtils.isNotBlank(pageSize)) {
			query.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "order by c.payment_time desc,c.add_time desc");
		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("preDeposit.getPdWithdrawCashListCount", paramMap);
		String QueryNsort = ConfigCode.getInstance().getCode("preDeposit.getPdWithdrawCashList", paramMap);
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	/**
	 * 获取余额提现
	 */
	@Override
	public PageSupport<PdWithdrawCash> getFindBalanceWithdrawal(String curPageNO, String pdcSn, Integer status, String userId) {
		CriteriaQuery cq = new CriteriaQuery(PdWithdrawCash.class, curPageNO);
		cq.setPageSize(10);
		if (AppUtils.isNotBlank(pdcSn)) {
			cq.eq("pdcSn", pdcSn);
		}
		if (AppUtils.isNotBlank(status)) {
			cq.eq("paymentState", status);
		}
		cq.eq("userId", userId);
		cq.addAscOrder("paymentState");
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport getPdWithdrawCashByState(String userId, String state, String curPageNO, Integer pageSize) {
		CriteriaQuery cq = new CriteriaQuery(PdWithdrawCash.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("userId", userId);
		cq.like("paymentState", state);
		return null;
	}

}
