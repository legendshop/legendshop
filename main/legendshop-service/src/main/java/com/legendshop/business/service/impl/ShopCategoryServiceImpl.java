/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.ShopCategoryDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.model.dto.ShopCategoryDto;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;


/**
 *商家的商品类目服务
 */
@Service("shopCategoryService")
public class ShopCategoryServiceImpl  implements ShopCategoryService{

	@Autowired
    private ShopCategoryDao shopCategoryDao;
    
	@Autowired
    private AttachmentManager attachmentManager;
    
    public ShopCategory getShopCategory(Long id) {
        return shopCategoryDao.getShopCategory(id);
    }

    public void deleteShopCategory(ShopCategory shopCategory) {
        shopCategoryDao.deleteShopCategory(shopCategory);
    }

    public Long saveShopCategory(ShopCategory shopCategory,String userName, String userId, Long shopId) {
    	MultipartFile formFile = shopCategory.getFile();// 取得上传的文件
    	
        if (AppUtils.isNotBlank(shopCategory.getId())) {
        	ShopCategory orginCategory = shopCategoryDao.getById(shopCategory.getId());
        	orginCategory.getPic();
        	
        	//判断shopId 是否一致
        	if(AppUtils.isBlank(shopId) && !orginCategory.getShopId().equals(shopId)){
        		throw new BusinessException("shop id not correct");
        	}
        	
        	//保存类目图片
        	orginCategory.setPic(savePicture(formFile,userName,userId, shopId, shopCategory.getId()));
    		
    		orginCategory.setStatus(Constants.ONLINE);
    		orginCategory.setRecDate(new Date());
    		orginCategory.setName(shopCategory.getName());
    		orginCategory.setSeq(shopCategory.getSeq());
            updateShopCategory(orginCategory);
            return shopCategory.getId();
        }else{
        	String filename = null;
        	
        	if ((formFile != null) && (formFile.getSize() > 0)) {
				filename = attachmentManager.upload(formFile);
				shopCategory.setPic(filename);
			}
        	shopCategory.setStatus(Constants.ONLINE);
        	shopCategory.setRecDate(new Date());
        	shopCategory.setShopId(shopId);
        	Long shopCategoryId = shopCategoryDao.saveShopCategory(shopCategory);
        	
        	if(AppUtils.isNotBlank(filename)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName,userId, shopId, filename, formFile, AttachmentTypeEnum.SORT);
			}
        	return shopCategoryId;
        }
    }

	@Override
	public Long saveShopCategory(Integer grade, Long shopId ,String name,Long parentId) {
		ShopCategory shopCategory=new ShopCategory();
		shopCategory.setName(name);
		shopCategory.setStatus(Constants.ONLINE);
		if(AppUtils.isNotBlank(parentId)){
			shopCategory.setParentId(parentId);
		}
		shopCategory.setShopId(shopId);
		shopCategory.setGrade(grade);
		shopCategory.setRecDate(new Date());
		return shopCategoryDao.saveShopCategory(shopCategory);
	}

	public void updateShopCategory(ShopCategory shopCategory) {
        shopCategoryDao.updateShopCategory(shopCategory);
    }

	@Override
	public int deleteShopCategory(Long id) {
		return shopCategoryDao.deleteById(id);
	}

	/** 保存类目图片 **/
	private String savePicture(MultipartFile formFile,String userName, String userId, Long shopId, Long shopCatId){
		String filename= null;
		if ((formFile != null) && (formFile.getSize() > 0)) {
			filename = attachmentManager.upload(formFile);
			//保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, filename, formFile, AttachmentTypeEnum.SORT);
		}
		
		return filename;
	}

	@Override
	public List<ShopCategory> subCatQuery(List<ShopCategory> nextCatList) {
		return shopCategoryDao.subCatQuery(nextCatList);
	}

	@Override
	public void deleteNextCategory(Long nextCatId) {
		List<ShopCategory> subcatList = shopCategoryDao.queryByParentId(nextCatId);
		if(AppUtils.isNotBlank(subcatList)){
			shopCategoryDao.delete(subcatList);
		}
		
		shopCategoryDao.deleteById(nextCatId);
	}

	@Override
	public List<ShopCategory> queryByParentId(Long shopCatId) {
		return shopCategoryDao.queryByParentId(shopCatId);
	}

	@Override
	public List<KeyValueEntity> loadCat(Long shopId) {
		List<ShopCategory> shopCatList = shopCategoryDao.queryByShopId(shopId);
		
		if(AppUtils.isBlank(shopCatList)){
			 return null;
		 }
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>(shopCatList.size());
		for (ShopCategory shopCategory : shopCatList) {
			KeyValueEntity entity = new KeyValueEntity();
			 entity.setKey(String.valueOf(shopCategory.getId()));
			 entity.setValue(shopCategory.getName());
			 result.add(entity);
		}
		return result;
	}

	@Override
	public List<KeyValueEntity> loadNextCat(Long shopCatId, Long shopId) {
		List<ShopCategory> nextShopCatList = shopCategoryDao.loadNextCat(shopCatId,shopId);
		
		if(AppUtils.isBlank(nextShopCatList)){
			 return null;
		 }
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>(nextShopCatList.size());
		for (ShopCategory shopCategory : nextShopCatList) {
			KeyValueEntity entity = new KeyValueEntity();
			 entity.setKey(String.valueOf(shopCategory.getId()));
			 entity.setValue(shopCategory.getName());
			 result.add(entity);
		}
		return result;
	}

	@Override
	public Set<CategoryDto> getAllCategoryByType(Long shopId, String name) {
		List<ShopCategory> shopCategory = shopCategoryDao.getShopCategory(shopId,name);
		HashMap<String,CategoryDto> hashMap = getTreeIntoMap(shopCategory);
		if(hashMap != null){
			Set<CategoryDto> categoryDtos = new TreeSet<CategoryDto>(new Comparator<CategoryDto>() {
				public int compare(CategoryDto o1, CategoryDto o2) {
					if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
						return -1;
					} else if (o1.getSeq() <= o2.getSeq()) {
						return -1;
					} else {
						return 1;
					}
				}
			});
			Iterator<CategoryDto> it = hashMap.values().iterator();
			while (it.hasNext()) {
				CategoryDto categoryDto = (CategoryDto) it.next();
				if(AppUtils.isNotBlank(categoryDto.getParentId())){
					long parentId = categoryDto.getParentId();
					String parentKeyId = String.valueOf(parentId);
					if (hashMap.containsKey(parentKeyId)) {
						CategoryDto parentCategory = (CategoryDto) hashMap.get(parentKeyId);
						parentCategory.addChildCategory(categoryDto);
						categoryDto.setParentCategory(parentCategory);
					}
				}else{
				    categoryDtos.add(categoryDto);
				}
			}
			return categoryDtos;
		}
		return null;
	}
	
	//组装树
	private	HashMap<String,CategoryDto> getTreeIntoMap(List<ShopCategory> categories){
		if(AppUtils.isNotBlank(categories)){
			HashMap<String,CategoryDto> nodeMap = new HashMap<String, CategoryDto>();
			Iterator<ShopCategory> it = categories.iterator();
			while (it.hasNext()) {
				ShopCategory category = (ShopCategory) it.next();
				long id = category.getId();
				String keyId = String.valueOf(id);
				CategoryDto categoryDto=build(category);
				nodeMap.put(keyId, categoryDto);
			}
			return nodeMap;
		}
		return null;
	}
	
	
	/**
     * 组装 CategoryDto对象
     * @param category
     * @return
     */
    private CategoryDto build(ShopCategory category)  {
    	return new CategoryDto(category.getId(), category.getParentId(), category.getName(),category.getSeq(),category.getStatus());
    }

	@Override
	public Long getNextCategoryCount(Long shopCatId) {
		return shopCategoryDao.getNextCategoryCount(shopCatId);
	}

	@Override
	public PageSupport<ShopCategory> queryShopCategory(String curPageNO, ShopCategory shopCategory, Long shopId) {
		return shopCategoryDao.queryShopCategory(curPageNO,shopCategory,shopId);
	}

	@Override
	public PageSupport<ShopCategory> getNextShopCat(String curPageNO, ShopCategoryDto shopCatDto) {
		return shopCategoryDao.getNextShopCat(curPageNO,shopCatDto);
	}

	@Override
	public PageSupport<ShopCategory> getSubShopCat(String curPageNO, ShopCategoryDto shopCatDto) {
		return shopCategoryDao.getSubShopCat(curPageNO,shopCatDto);
	}

	@Override
	public List<ShopCategory> getFirstShopCategory(Long shopId) {
		return shopCategoryDao.getFirstShopCategory(shopId);
	}

	@Override
	public List<ShopCategory> getShopCategoryList(Long shopId) {
		return shopCategoryDao.getShopCategoryList(shopId);
	}

	/**
	 * 获取店铺分类列表
	 */
	@Override
	public List<DecorateShopCategoryDto> getShopCategoryDtoList(Long shopId) {
		
		return shopCategoryDao.getShopCategoryDtoList(shopId);
	}

	@Override
	public String getShopCategoryName(Long id) {

		return shopCategoryDao.getShopCategoryName(id);
	}

	@CacheEvict(value = "StoreListDto", key = "'StoreList_'+#shopId")
	@Override
	public void expireShopCategory(Long shopId) {

	}

	@CacheEvict(value = "OnlineShopDecotateDto", key = "'OnlineShopDecotate_'+#shopId")
	@Override
	public void expireShopCategoryIndex(Long shopId) {}
}
