/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloorItem;

/**
 * 手机端html5 楼层内容关联服务Dao
 */
public interface MobileFloorItemDao extends GenericDao<MobileFloorItem, Long> {
     
	public abstract MobileFloorItem getMobileFloorItem(Long id);
	
    public abstract int deleteMobileFloorItem(MobileFloorItem mobileFloorItem);
	
	public abstract Long saveMobileFloorItem(MobileFloorItem mobileFloorItem);
	
	public abstract int updateMobileFloorItem(MobileFloorItem mobileFloorItem);
	
	public abstract List<MobileFloorItem> queryMobileFloorItem(Long parentId);

	public abstract List<MobileFloorItem> queryFloorItemByIds(List<Long> floorIds);

	public abstract Long getCountMobileFloorItem(Long parentId);

	public abstract PageSupport<MobileFloorItem> getMobileFloorItem(String curPageNO, String title, Long id);
 }
