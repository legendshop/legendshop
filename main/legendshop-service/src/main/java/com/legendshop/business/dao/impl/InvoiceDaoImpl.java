package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.dao.criterion.MatchMode;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.InvoiceDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Invoice;

/**
 * 发票Dao
 */
@Repository
public class InvoiceDaoImpl extends GenericDaoImpl<Invoice, Long> implements InvoiceDao{

	@Override
	public List<Invoice> getInvoice(String userName) {
		return query("select * from ls_invoice where user_name = ? ", Invoice.class, userName);
	}

	@Override
	public Invoice getInvoice(Long id) {
		return getById(id);
	}

	@Override
	public int deleteInvoice(Invoice invoice) {
		return delete(invoice);
	}

	@Override
	public Long saveInvoice(Invoice invoice) {
		return save(invoice);
	}

	@Override
	public int updateInvoice(Invoice invoice) {
		return update(invoice);
	}

	@Override
	public void updateDefaultInvoice(Long invoiceId, String userName) {
		//1.更新原来的发票信息
		update("update ls_invoice set common_invoice = 0 where common_invoice = 1 and user_name = ?",userName);
		
		//2.更新新的发票信息
		update("update ls_invoice set common_invoice =1 where id = ? and user_name = ?",invoiceId,userName);
	}

	@Override
	public void delById(Long id) {
		update("delete from ls_invoice where id = ?",id);
	}

	@Override
	public Invoice getDefaultInvoice(String userId) {
		return get("SELECT id as id,user_id as userId,user_name as userName,company as company,type as type,title as title,content as content,invoice_hum_number as invoiceHumNumber,register_addr as registerAddr,register_phone as registerPhone,deposit_bank as depositBank,bank_account_num as bankAccountNum FROM ls_invoice WHERE user_id=? AND common_invoice=1 ORDER BY create_time", Invoice.class, userId);
	}
	
	@Override
	public Invoice getInvoice(Long id, String userId) {
		
		return getByProperties(new EntityCriterion().eq("id", id).eq("userId", userId));
	}

	@Override
	public PageSupport<Invoice> getInvoicePage(String userName, String curPageNO, Integer pageSize) {
		CriteriaQuery cq = new CriteriaQuery(Invoice.class, curPageNO);
		cq.eq("userName", userName);
		cq.addDescOrder("commonInvoice");
		cq.setPageSize(pageSize);
		return queryPage(cq);
	}

	@Override
	public Invoice getInvoiceByTitleId(String userId, Integer titleId) {
		return getByProperties(new EntityCriterion().eq("userId", userId).eq("titleId", titleId));
	}

	@Override
	public void removeDefaultInvoiceStatus(String userId) {
		update("update ls_invoice set common_invoice = 0 where user_id = ?",userId);
	}

	/**
	 * 根据发票类型获取发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @param curPageNo 当前页码
	 * @return
	 */
	@Override
	public PageSupport<Invoice> queryInvoice(String userName, Integer invoiceType, String curPageNo) {

		curPageNo = curPageNo == null ? "1" : curPageNo;

		CriteriaQuery cq = new CriteriaQuery(Invoice.class, curPageNo);
		cq.eq("userName", userName);
		cq.eq("type",invoiceType);
		cq.addDescOrder("commonInvoice");
		cq.addDescOrder("createTime");
		cq.setPageSize(10);
		return queryPage(cq);



	}

	/**
	 * 根据类型获取用户发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @return
	 */
    @Override
    public List<Invoice> getInvoiceByType(String userName, Integer invoiceType) {

		EntityCriterion criterion = new EntityCriterion();
		criterion.eq("userName", userName);
		criterion.eq("type", invoiceType);
		criterion.addDescOrder("commonInvoice");
		criterion.addDescOrder("createTime");

		List<Invoice> invoices = queryByProperties(criterion);
		return invoices;
    }

}
