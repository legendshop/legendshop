/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;

/**
 * WAP头条新闻表Dao.
 */
public interface HeadlineDao extends Dao<Headline, Long> {
     
	Headline getHeadline(Long id);
	
    int deleteHeadline(Headline headline);
	
	Long saveHeadline(Headline headline);
	
	int updateHeadline(Headline headline);
	
	List<Headline> queryByAll();

	PageSupport<Headline> query(String curPageNO);
	
	PageSupport<Headline> getHeadlinePage(String curPageNO);
	
 }
