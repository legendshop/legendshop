/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutHot;

/**
 * 布局热点Dao.
 */
public interface ShopLayoutHotDao extends GenericDao<ShopLayoutHot, Long> {
     
	public abstract ShopLayoutHot getShopLayoutHot(Long id);
	
    public abstract int deleteShopLayoutHot(ShopLayoutHot shopLayoutHot);
	
	public abstract Long saveShopLayoutHot(ShopLayoutHot shopLayoutHot);
	
	public abstract int updateShopLayoutHot(ShopLayoutHot shopLayoutHot);
	

	public abstract ShopLayoutHot findShopLayoutHot(Long shopId, Long divId, Long layoutId);

	public abstract ShopLayoutHot findShopLayoutHot(Long shopId, Long layoutId);

	public List<ShopLayoutHot> findShopLayoutHot(Long shopId);

	public abstract void deleteShopLayoutHot(Long shopId, Long layoutId);
	
 }
