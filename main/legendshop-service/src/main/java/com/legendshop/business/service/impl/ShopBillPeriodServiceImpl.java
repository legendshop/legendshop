/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopBillPeriodDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopBillPeriod;
import com.legendshop.spi.service.ShopBillPeriodService;
import com.legendshop.util.AppUtils;

/**
 * 结算档期服务
 */
@Service("shopBillPeriodService")
public class ShopBillPeriodServiceImpl  implements ShopBillPeriodService{

	@Autowired
    private ShopBillPeriodDao shopBillPeriodDao;

    public ShopBillPeriod getShopBillPeriod(Long id) {
        return shopBillPeriodDao.getShopBillPeriod(id);
    }

    public void deleteShopBillPeriod(ShopBillPeriod shopBillPeriod) {
        shopBillPeriodDao.deleteShopBillPeriod(shopBillPeriod);
    }

    public Long saveShopBillPeriod(ShopBillPeriod shopBillPeriod) {
        if (!AppUtils.isBlank(shopBillPeriod.getId())) {
            updateShopBillPeriod(shopBillPeriod);
            return shopBillPeriod.getId();
        }
        return shopBillPeriodDao.saveShopBillPeriod(shopBillPeriod);
    }

    public void updateShopBillPeriod(ShopBillPeriod shopBillPeriod) {
        shopBillPeriodDao.updateShopBillPeriod(shopBillPeriod);
    }

	@Override
	public void saveShopBillPeriodWithId(ShopBillPeriod shopBillPeriod, Long id) {
		shopBillPeriodDao.saveShopBillPeriodWithId(shopBillPeriod, id);
	}

	@Override
	public Long getShopBillPeriodId() {
		return shopBillPeriodDao.getShopBillPeriodId();
	}

	@Override
	public PageSupport<ShopBillPeriod> getShopBillPeriod(String curPageNO, ShopBillPeriod shopBillPeriod) {
		return shopBillPeriodDao.getShopBillPeriod(curPageNO,shopBillPeriod);
	}
}
