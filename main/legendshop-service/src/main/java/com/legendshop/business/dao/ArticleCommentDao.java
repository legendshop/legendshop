/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleComment;

/**
 * 文章评论接口
 */
public interface ArticleCommentDao extends GenericDao<ArticleComment, Long> {

   	/**
	 *  根据Id获取
	 */
	public abstract ArticleComment getArticleComment(Long id);
	
   /**
	 *  删除
	 */
    public abstract int deleteArticleComment(ArticleComment articleComment);
    
   /**
	 *  保存
	 */	
	public abstract Long saveArticleComment(ArticleComment articleComment);

   /**
	 *  更新
	 */		
	public abstract int updateArticleComment(ArticleComment articleComment);

	public abstract ArticleComment getArticleCommentLinkArt(Long id);

	PageSupport<ArticleComment> queyArticleComment(String curPageNO, Long id);

	PageSupport<ArticleComment> queyArticleCommentScroll(String curPageNO, Long id);
	
	public abstract PageSupport<ArticleComment> getArticleCommentPage(String curPageNO, ArticleComment articleComment);
	
 }
