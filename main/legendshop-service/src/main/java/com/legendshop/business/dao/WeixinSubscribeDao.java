/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinSubscribe;

/**
 * 微信订阅的消息Dao
 */
public interface WeixinSubscribeDao extends Dao<WeixinSubscribe, Long> {
     
	public abstract WeixinSubscribe getWeixinSubscribe(Long id);
	
    public abstract int deleteWeixinSubscribe(WeixinSubscribe weixinSubscribe);
	
	public abstract Long saveWeixinSubscribe(WeixinSubscribe weixinSubscribe);
	
	public abstract int updateWeixinSubscribe(WeixinSubscribe weixinSubscribe);
	
	public abstract PageSupport<WeixinSubscribe> getWeixinSubscribePage(String curPageNO);
	
 }
