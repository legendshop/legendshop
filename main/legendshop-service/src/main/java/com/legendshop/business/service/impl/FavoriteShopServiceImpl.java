/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FavoriteShopDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FavoriteShop;
import com.legendshop.spi.service.FavoriteShopService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 店铺收藏服务.
 */
@Service("favoriteShopService")
public class FavoriteShopServiceImpl  implements FavoriteShopService{
	
	@Autowired
	
    private FavoriteShopDao favoriteShopDao;

    public FavoriteShop getfavoriteShop(Long id) {
        return favoriteShopDao.getfavoriteShop(id);
    }

    public void deletefavoriteShop(FavoriteShop favoriteShop) {
        favoriteShopDao.deletefavoriteShop(favoriteShop);
    }

    public Long savefavoriteShop(FavoriteShop favoriteShop) {
        if (!AppUtils.isBlank(favoriteShop.getFsId())) {
            updatefavoriteShop(favoriteShop);
            return favoriteShop.getFsId();
        }
        return  favoriteShopDao.save(favoriteShop);
    }

    public void updatefavoriteShop(FavoriteShop favoriteShop) {
        favoriteShopDao.updatefavoriteShop(favoriteShop);
    }

	public boolean deletefavoriteShop(Long id, String userId) {
		return favoriteShopDao.deletefavoriteShop(id, userId);
	}

	public boolean isExistsFavoriteShop(String userId,Long shopId) {
		return favoriteShopDao.isExistsFavoriteShop(userId, shopId);
	}

	public Long getShopId(String shopName) {
		return favoriteShopDao.getShopId(shopName);
	}

	public void deletefavoriteShop(Long id) {
		favoriteShopDao.deletefavoriteShop(id);
	}

	public void deletefavoriteShop(String userId, String ids) {
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		favoriteShopDao.deletefavoriteShop(userId,idList);
	}

	@Override
	public void deleteAllfavoriteShop(String userId) {
		favoriteShopDao.deleteAllfavoriteShop(userId);
	}    
	
	@Override
	public boolean deletefavoriteShopByShopIdAndUserId(Long shopId,
			String userId) { 
		
		int result = favoriteShopDao.deletefavoriteShopByShopIdAndUserId(shopId, userId);
		return result > 0;
	}    
	
	@Override
	public Long getfavoriteShopLength(String userId) {
		return favoriteShopDao.getfavoriteShopLength(userId);
	}

	@Override
	public PageSupport<FavoriteShop> collectShop(String curPageNO,String userId) {
		return favoriteShopDao.collectShop(curPageNO,userId);
	}

	@Override
	public PageSupport<FavoriteShop> query(String curPageNO, String userId) {
		return favoriteShopDao.querySimplePage(curPageNO,userId);
	}
}
