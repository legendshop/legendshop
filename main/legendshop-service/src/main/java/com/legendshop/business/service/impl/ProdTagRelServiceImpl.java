package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdTagRelDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTagRel;
import com.legendshop.model.entity.Product;
import com.legendshop.spi.service.ProdTagRelService;
/**
 * 商品标签关联服务
 */
@Service("prodTagRelService")
public class ProdTagRelServiceImpl implements ProdTagRelService{

	@Autowired
	private ProdTagRelDao prodTagRelDao;

	@Override
	@CacheEvict(value="ProdTagRel",key="#prodTagRel.prodId")
	public void saveProdTagRel(ProdTagRel prodTagRel) {
		prodTagRelDao.save(prodTagRel);		
	}

	@Override
	@Cacheable(value="ProdTagRel", key="#prodId")
	public ProdTagRel getProdTagRelByProdId(Long prodId) {
		return prodTagRelDao.get("select * from ls_prod_tag_rel where prod_id = ?", ProdTagRel.class, prodId);
	}

	@Override
	@CacheEvict(value="ProdTagRel",key="#prodId")
	public boolean deleteProdTagRelByProdId(Long prodId) {
		int result = prodTagRelDao.update("delete from ls_prod_tag_rel where prod_id = ?", prodId);
		return result > 0;
	}

	@Override
	public void deleteProdTagRelByTagId(Long id) {
		ProdTagRel prodTagRel = prodTagRelDao.getById(id);
		if(prodTagRel != null){
			prodTagRelDao.deleteProdTagRel(prodTagRel);
		}
	}

	@Override
	public PageSupport<ProdTagRel> queryProdTagRel(String curPageNO, Product product, Long shopId) {
		return prodTagRelDao.queryProdTagRel(curPageNO,product,shopId);
	}

	@Override
	public PageSupport<ProdTagRel> queryBindProdManage(String curPageNO, Long id, String prodName) {
		return prodTagRelDao.queryBindProdManage(curPageNO,id,prodName);
	}

}
