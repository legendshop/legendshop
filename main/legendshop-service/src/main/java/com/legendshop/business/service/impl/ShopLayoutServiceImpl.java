/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopLayoutBannerDao;
import com.legendshop.business.dao.ShopLayoutDao;
import com.legendshop.business.dao.ShopLayoutDivDao;
import com.legendshop.business.dao.ShopLayoutHotDao;
import com.legendshop.business.dao.ShopLayoutProdDao;
import com.legendshop.model.constant.ShopLayoutDivTypeEnum;
import com.legendshop.model.constant.ShopLayoutModuleTypeEnum;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;
import com.legendshop.spi.service.ShopLayoutService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 布局服务实现类.
 */
@Service("shopLayoutService")
public class ShopLayoutServiceImpl  implements ShopLayoutService{
	
	@Autowired
    private ShopLayoutDao shopLayoutDao;
    
	@Autowired
    private ShopLayoutDivDao shopLayoutDivDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private ShopLayoutBannerDao shopLayoutBannerDao;
    
    @Autowired
    private ShopLayoutProdDao shopLayoutProdDao;
    
    @Autowired
    private ShopLayoutHotDao shopLayoutHotDao;

    public ShopLayout getShopLayout(Long id) {
        return shopLayoutDao.getShopLayout(id);
    }

    public void deleteShopLayout(ShopLayout shopLayout) {
        shopLayoutDao.deleteShopLayout(shopLayout);
    }

    public Long saveShopLayout(ShopLayout shopLayout) {
        if (!AppUtils.isBlank(shopLayout.getLayoutId())) {
            updateShopLayout(shopLayout);
            return shopLayout.getLayoutId();
        }
        return shopLayoutDao.saveShopLayout(shopLayout);
    }

    public void updateShopLayout(ShopLayout shopLayout) {
        shopLayoutDao.updateShopLayout(shopLayout);
    }

	@Override
	public ShopLayoutDto saveShopLayout(String layout, Long shopId, Long decId) {
		Long seq=shopLayoutDao.lastSeq(shopId,decId);
		int number=seq.intValue();
		number++;
		ShopLayout shopLayout=new ShopLayout();
		shopLayout.setShopId(shopId);
		shopLayout.setShopDecotateId(decId);
		shopLayout.setLayoutType(layout);
		shopLayout.setDisable(0);
		shopLayout.setSeq(number);
		ShopLayoutDto shopLayoutDto=saveShopLayoutDto(shopLayout);
		return shopLayoutDto;
	}
	
	
	
	private ShopLayoutDto saveShopLayoutDto(ShopLayout shopLayout){
		Long id=shopLayoutDao.saveShopLayout(shopLayout);
		shopLayout.setLayoutId(id);
		ShopLayoutDto shopLayoutDto=constracutShopLayoutDto(shopLayout);
		if(ShopLayoutTypeEnum.LAYOUT_3.value().equals(shopLayout.getLayoutType())){
			List<ShopLayoutDivDto> shopLayoutDivDtos=new ArrayList<ShopLayoutDivDto>();
			
			ShopLayoutDiv shopLayoutDiv1=new ShopLayoutDiv();
			shopLayoutDiv1.setLayoutDiv(ShopLayoutDivTypeEnum.DIV_1.value());
			shopLayoutDiv1.setLayoutType(ShopLayoutTypeEnum.LAYOUT_3.value());
			shopLayoutDiv1.setLayoutId(id);
			shopLayoutDiv1.setShopId(shopLayout.getShopId());
			shopLayoutDiv1.setShopDecotateId(shopLayout.getShopDecotateId()); 
			Long divId=shopLayoutDivDao.saveShopLayoutDiv(shopLayoutDiv1);
			shopLayoutDiv1.setLayoutDivId(divId);
			
			 ShopLayoutDivDto shopLayoutDivDto=constracutShopLayoutDivDto(shopLayoutDiv1);
			 shopLayoutDivDtos.add(shopLayoutDivDto);
			 
			 shopLayoutDiv1=new ShopLayoutDiv();
			 shopLayoutDiv1.setLayoutDiv(ShopLayoutDivTypeEnum.DIV_2.value());
			 shopLayoutDiv1.setLayoutType(ShopLayoutTypeEnum.LAYOUT_3.value());
			 shopLayoutDiv1.setLayoutId(id);
			 shopLayoutDiv1.setShopId(shopLayout.getShopId());
			 shopLayoutDiv1.setShopDecotateId(shopLayout.getShopDecotateId()); 
			 divId=shopLayoutDivDao.saveShopLayoutDiv(shopLayoutDiv1);
			 shopLayoutDiv1.setLayoutDivId(divId);
			 
			 shopLayoutDivDto=constracutShopLayoutDivDto(shopLayoutDiv1);
			 shopLayoutDivDtos.add(shopLayoutDivDto);
			 shopLayoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
			 shopLayoutDiv1=null;
			 divId=null;
		}else if(ShopLayoutTypeEnum.LAYOUT_4.value().equals(shopLayout.getLayoutType())){
			
            List<ShopLayoutDivDto> shopLayoutDivDtos=new ArrayList<ShopLayoutDivDto>();
			ShopLayoutDiv shopLayoutDiv1=new ShopLayoutDiv();
			shopLayoutDiv1.setLayoutDiv(ShopLayoutDivTypeEnum.DIV_2.value());
			shopLayoutDiv1.setLayoutType(ShopLayoutTypeEnum.LAYOUT_4.value());
			shopLayoutDiv1.setLayoutId(id);
			shopLayoutDiv1.setShopId(shopLayout.getShopId());
			shopLayoutDiv1.setShopDecotateId(shopLayout.getShopDecotateId()); 
			Long divId=shopLayoutDivDao.saveShopLayoutDiv(shopLayoutDiv1);
			shopLayoutDiv1.setLayoutDivId(divId);
			
			 ShopLayoutDivDto shopLayoutDivDto=constracutShopLayoutDivDto(shopLayoutDiv1);
			 shopLayoutDivDtos.add(shopLayoutDivDto);
			 
			 shopLayoutDiv1=new ShopLayoutDiv();
			 shopLayoutDiv1.setLayoutDiv(ShopLayoutDivTypeEnum.DIV_1.value());
			 shopLayoutDiv1.setLayoutType(ShopLayoutTypeEnum.LAYOUT_4.value());
			 shopLayoutDiv1.setLayoutId(id);
			 shopLayoutDiv1.setShopId(shopLayout.getShopId());
			 shopLayoutDiv1.setShopDecotateId(shopLayout.getShopDecotateId()); 
			 divId=shopLayoutDivDao.saveShopLayoutDiv(shopLayoutDiv1);
			 shopLayoutDiv1.setLayoutDivId(divId);
			 
			 shopLayoutDivDto=constracutShopLayoutDivDto(shopLayoutDiv1);
			 shopLayoutDivDtos.add(shopLayoutDivDto);
			 shopLayoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
			 shopLayoutDiv1=null;
			 divId=null;
		}
		return shopLayoutDto;
	}
	
	
	private ShopLayoutDto constracutShopLayoutDto(ShopLayout shopLayout){
		ShopLayoutDto shopLayoutDto=new ShopLayoutDto();
		shopLayoutDto.setShopId(shopLayout.getShopId());
		shopLayoutDto.setShopDecotateId(shopLayout.getShopDecotateId());
		shopLayoutDto.setLayoutId(shopLayout.getLayoutId());
		shopLayoutDto.setLayoutType(shopLayout.getLayoutType());
		//shopLayoutDto.setLayoutDiv(shopLayout.getLayoutDiv());
		shopLayoutDto.setLayoutModuleType(shopLayout.getLayoutModuleType());
		shopLayoutDto.setSeq(shopLayout.getSeq());
		
		ShopLayoutModuleDto moduleDto=new ShopLayoutModuleDto();
		moduleDto.setLayoutContent(shopLayout.getLayoutContent());
		moduleDto.setLayoutId(shopLayout.getLayoutId());
		moduleDto.setShopDecotateId(shopLayout.getShopDecotateId());
		moduleDto.setShopId(shopLayout.getShopId());
		
		shopLayoutDto.setShopLayoutModule(moduleDto);
		return shopLayoutDto;
	}
	
	private ShopLayoutDivDto constracutShopLayoutDivDto(ShopLayoutDiv shopLayout){
		ShopLayoutDivDto shopLayoutDto=new ShopLayoutDivDto();
		shopLayoutDto.setShopId(shopLayout.getShopId());
		shopLayoutDto.setShopDecotateId(shopLayout.getShopDecotateId());
		shopLayoutDto.setLayoutId(shopLayout.getLayoutId());
		shopLayoutDto.setLayoutType(shopLayout.getLayoutType());
		shopLayoutDto.setLayoutDiv(shopLayout.getLayoutDiv());
		shopLayoutDto.setLayoutId(shopLayout.getLayoutId());
		shopLayoutDto.setLayoutDivId(shopLayout.getLayoutDivId());
		shopLayoutDto.setLayoutModuleType(shopLayout.getLayoutModuleType());
		shopLayoutDto.setLayoutContent(shopLayout.getLayoutContent());
		return shopLayoutDto;
	}

	/**
	 * 后台装修删除布局
	 */
	@Override
	public ShopDecotateDto deleLayout(ShopDecotateDto decotateDto,
			ShopLayoutParam layoutParam) {
		 String layout=layoutParam.getLayout();
		 Long layoutId=layoutParam.getLayoutId();
		 layoutParam.setShopId(decotateDto.getShopId());
		 layoutParam.setDecId(decotateDto.getDecId());
		 List<ShopLayoutDto> shopLayoutDtos=null;
		 if(ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)){
			 shopLayoutDtos=decotateDto.getTopShopLayouts();
	     }else if(ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)){
	    	 shopLayoutDtos=decotateDto.getBottomShopLayouts();
	     }else if(ShopLayoutTypeEnum.LAYOUT_2.value().equals(layout)){
	    	 shopLayoutDtos=decotateDto.getMainShopLayouts();
	     }else {
	    	 shopLayoutDtos=decotateDto.getMainShopLayouts();
	     }
		 if(shopLayoutDtos==null){
			 return decotateDto;
		 }
		 ShopLayoutDto shopLayoutDto=existShopLayout(layoutId,shopLayoutDtos); // 是否存在布局
		 if(AppUtils.isBlank(shopLayoutDto)){
			 return decotateDto;
		 }
		 
		 /*
		  * 如果改布局是Banner 要把module的File文件删除
		  */
		 if(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(shopLayoutDto.getLayoutModuleType())){
			 ShopLayoutModuleDto shopLayoutModuleDto=shopLayoutDto.getShopLayoutModule();
			 List<ShopLayoutBanner> shopLayoutBanners=shopLayoutModuleDto.getShopLayoutBanners();
			 if(AppUtils.isNotBlank(shopLayoutBanners)){
				 for (ShopLayoutBanner shopLayoutBanner : shopLayoutBanners) {
					 if(AppUtils.isNotBlank(shopLayoutBanner.getImageFile())){
						 attachmentManager.deleteTruely(shopLayoutBanner.getImageFile());
						 shopLayoutBannerDao.deleteById(shopLayoutBanner.getId());
					 }
				}
			 }
			 shopLayoutBanners=null;
		 }
		 if(ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)){
			//通栏布局上
			 shopLayoutDtos=removeLayoutDtos(layoutId,shopLayoutDtos);
			 decotateDto.setTopShopLayouts(shopLayoutDtos);
	     }else if(ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)){
	    	 //通栏布局下
	    	 shopLayoutDtos=removeLayoutDtos(layoutId,shopLayoutDtos);
			 decotateDto.setBottomShopLayouts(shopLayoutDtos);
	     }else if(ShopLayoutTypeEnum.LAYOUT_2.value().equals(layout)){
	         //1:2布局
	    	 shopLayoutDtos=removeLayoutDtos(layoutId,shopLayoutDtos);
			 decotateDto.setMainShopLayouts(shopLayoutDtos);
	     }else {
	    	//2:1布局
	    	 shopLayoutDtos=removeDivLayoutDtos(layoutParam,shopLayoutDtos);
			 decotateDto.setMainShopLayouts(shopLayoutDtos);
	     }
		 return decotateDto;
	}
	
	/**
	 * 移除CacheLayout
	 * @param layoutId
	 * @param layoutDtos
	 * @return
	 */
	private List<ShopLayoutDto> removeLayoutDtos(Long layoutId,List<ShopLayoutDto> layoutDtos){
		if(AppUtils.isBlank(layoutDtos)){
			return null;
		}
		Iterator<ShopLayoutDto> iterator= layoutDtos.iterator();
		while (iterator.hasNext()) {
			ShopLayoutDto layoutDto=iterator.next();
			if(layoutDto.getLayoutId().equals(layoutId)){
				shopLayoutDao.deleteShopLayout(layoutDto.getShopId(), layoutId);//从数据库中删除
				layoutDto=null;
				iterator.remove();
			}
		}
		return layoutDtos;
	}
	
	
	private ShopLayoutDto existShopLayout(Long layoutId,List<ShopLayoutDto> layoutDtos){
		Iterator<ShopLayoutDto> iterator= layoutDtos.iterator();
		while (iterator.hasNext()) {
			ShopLayoutDto layoutDto=iterator.next();
			if(layoutDto.getLayoutId().equals(layoutId)){
				return layoutDto;
			}
		}
		return null;
	}
	
	
	/**
	 * 移除 Div CacheLayout
	 * @param layoutId
	 * @param layoutDtos
	 * @return
	 */
	private List<ShopLayoutDto> removeDivLayoutDtos(ShopLayoutParam shopLayout,List<ShopLayoutDto> layoutDtos){
		if(AppUtils.isBlank(layoutDtos)){
			return null;
		}
		boolean delLayout=false;
		Iterator<ShopLayoutDto> iterator= layoutDtos.iterator();
		while (iterator.hasNext()) {
			ShopLayoutDto layoutDto=iterator.next();
			if(layoutDto.getLayoutId().equals(shopLayout.getLayoutId())){
				List<ShopLayoutDivDto> shopLayoutDivDtos= layoutDto.getShopLayoutDivDtos();
				if(AppUtils.isBlank(shopLayoutDivDtos)){
					shopLayoutDao.deleteShopLayout(layoutDto.getShopId(), shopLayout.getLayoutId());//从数据库中删除
					layoutDto=null;
					iterator.remove();
				}else{
					Iterator<ShopLayoutDivDto> divIterator= shopLayoutDivDtos.iterator();
					while (divIterator.hasNext()) {
						ShopLayoutDivDto layoutDivDto=divIterator.next();
						if(layoutDivDto.getLayoutDivId().equals(shopLayout.getDivId())){
							shopLayoutDivDao.deleteShopLayoutDiv(layoutDivDto.getLayoutDivId());
							divIterator.remove();
						}
					}
				}
				if(AppUtils.isBlank(shopLayoutDivDtos)){
					shopLayoutDao.deleteShopLayout(layoutDto.getShopId(), shopLayout.getLayoutId());//从数据库中删除
					layoutDto=null;
					iterator.remove();
					delLayout=true; //用于删除 layout div 
				}else {
				   layoutDto.setShopLayoutDivDtos(shopLayoutDivDtos);
				}
			}
		}
		//异步删除布局信息
		shopLayout.setDelLayout(delLayout);
		//EventHome.publishEvent(new DeleteShopLayouEvent(shopLayout));
		
		return layoutDtos;
	}

	@Override
	public Long getLayoutCount(Long shopId,Long decotate_id) {
		return shopLayoutDao.getLongResult("select count(layout_id) from ls_shop_layout where shop_id=? and shop_decotate_id=? ",shopId,decotate_id);
	}


	@Override
	public Long getLayoutCountOnline(Long shopId, Long decotate_id) {
		return shopLayoutDao.getLongResult("select count(layout_id) from ls_shop_layout where shop_id=?",shopId);
	}


	@Override
	public void delete(ShopLayout shopLayout) {
		shopLayoutDao.delete(shopLayout);
	}

	/**
	 * 删除布局相关的信息
	 */
	@Override
	public void deleteFullShopLayout(ShopLayout shopLayout) {
		 String layout=shopLayout.getLayoutType();
		 deleteLayoutModule(shopLayout);
		 delete(shopLayout);
		 if(ShopLayoutTypeEnum.LAYOUT_3.value().equals(layout) || ShopLayoutTypeEnum.LAYOUT_4.value().equals(layout) ){
			 shopLayoutDivDao.deleteShopLayoutDiv(shopLayout.getShopId(),shopLayout.getLayoutId());
		 }
		 
		
	}
	

	@Override
	public void deleteShopLayout(Long shopId, Long layoutId) {
		shopLayoutDao.deleteShopLayout(shopId, layoutId);
		
	}
	
	/**
	 * 删除布局相关数据.
	 *
	 * @param shopLayout the shop layout
	 */
	private void deleteLayoutModule(ShopLayout shopLayout){
		 List<ShopLayoutBanner>  banners= shopLayoutBannerDao.getShopLayoutBanner(shopLayout.getShopDecotateId(),shopLayout.getLayoutId());
		 if(AppUtils.isNotBlank(banners)){
			 for (ShopLayoutBanner shopLayoutBanner : banners) {
				 if(AppUtils.isNotBlank(shopLayoutBanner.getImageFile())){
					 attachmentManager.delAttachmentByFilePath(shopLayoutBanner.getImageFile());
				 }
			}
			 shopLayoutBannerDao.delete(banners);
		 }

		 shopLayoutProdDao.deleteShopLayoutProd(shopLayout.getShopId(),shopLayout.getLayoutId());
		 
		 shopLayoutHotDao.deleteShopLayoutHot(shopLayout.getShopId(),shopLayout.getLayoutId());
		 
		 shopLayoutDao.deleteShopLayout(shopLayout.getShopId(),shopLayout.getLayoutId());
		 
	}
}
