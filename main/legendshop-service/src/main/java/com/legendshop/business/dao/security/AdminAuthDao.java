/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.security;

import com.legendshop.model.security.UserEntity;

/**
 * 后台验证登陆服务
 * @author Tony
 *
 */
public interface AdminAuthDao{
	
	/**
	 *  查找用户, remember me 使用
	 */
	public UserEntity loadUserByUsername(String username);
	

	/**
	 * 管理员登录
	 * @param username 用户名
	 * @param presentedPassword 密码,如果密码不正确则不反悔信息
	 * @return 管理员
	 */
	public UserEntity loadAdminUserByUsername(String username, String presentedPassword);
	
	
}