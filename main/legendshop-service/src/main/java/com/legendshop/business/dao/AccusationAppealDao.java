/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationAppeal;

/**
 * 商家上诉Dao接口
 * 
 */

public interface AccusationAppealDao extends GenericDao<AccusationAppeal, Long> {

	/** 获取商家上诉 */
	public abstract AccusationAppeal getAccusationAppeal(Long id);

	/** 删除商家上诉 */
	public abstract void deleteAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 保存商家上诉 */
	public abstract Long saveAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 更新商家上诉 */
	public abstract void updateAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 获取商家上诉列表 */
	public abstract PageSupport<AccusationAppeal> getAccusationAppealPage(String curPageNO);

}
