/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.ProdPropImage;

/**
 * 属性图片Dao.
 */
public interface ProdPropImageDao extends GenericDao<ProdPropImage, Long> {

	void clearProdPropImageCache(Long prodId);

	ProdPropImage getProdPropImage(Long id);

	int deleteProdPropImage(ProdPropImage prodPropImage);

	Long saveProdPropImage(ProdPropImage prodPropImage);

	int updateProdPropImage(ProdPropImage prodPropImage);

	/**
	 * 根据prodId查取该商品所有属性值图片
	 **/
	List<ProdPropImage> getProdPropImageByProdId(Long prodId);

	List<ProdPropImage> queryPropImageList(Long prodId, ProductPropertyDto productPropertyDto, List<ProductPropertyValueDto> valueDtoList);

	List<ProdPropImage> getProdPropImageByKeyVal(Long prodId, Long key, Long val);

	/**
	 * 根据prodId,propId,valueId查询属性值图片列表
	 */
	List<String> getImageUrlList(Long prodId, Long propId, Long valueId);

	void deleteByProdId(Long id);
}
