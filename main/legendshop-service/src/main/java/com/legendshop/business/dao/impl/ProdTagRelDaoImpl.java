package com.legendshop.business.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTagRelDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ProdTagRel;
import com.legendshop.model.entity.Product;
import com.legendshop.util.AppUtils;
/**
 * 商品标签关系Dao
 *
 */
@Repository
public class ProdTagRelDaoImpl extends GenericDaoImpl<ProdTagRel, Long> implements ProdTagRelDao{
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(ProdTagRelDaoImpl.class);


	@Override
	public List<ProdTagRel> queryProdTagRel(Long tagId) {
		return query("select * from ls_prod_tag_rel where tag_id = ?", ProdTagRel.class, tagId);
	}

	/**
	 * 清除缓存
	 */
	@Override
	@CacheEvict(value="ProdTagRel",key="#prodTagRel.prodId")
	public boolean updateProdTagCache(ProdTagRel prodTagRel) {
		log.info("clear cache prodTagRel id is {}",prodTagRel.getId() );
		return true;
	}

	@Override
	@CacheEvict(value="ProdTagRel",key="#prodTagRel.prodId")
	public boolean deleteProdTagRel(ProdTagRel prodTagRel) {
		log.info("clear cache prodTagRel id is {}",prodTagRel.getId() );
		int result = this.delete(prodTagRel);
		return result > 0;
	}

	@Override
	public PageSupport<ProdTagRel> queryProdTagRel(String curPageNO, Product product, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProdTagRel.class,4, curPageNO);
        QueryMap map = new QueryMap();
		map.put("shopId", shopId);		
		if(AppUtils.isNotBlank(product)){
			map.put("prodName", "%"+product.getName()+"%");
		}
        String queryAllSQL = ConfigCode.getInstance().getCode("shop.getProdCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.getProd", map); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProdTagRel> queryBindProdManage(String curPageNO, Long id, String prodName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProdTagRel.class,10, curPageNO);
        QueryMap map = new QueryMap();
		map.put("tagId", id);		
		if(AppUtils.isNotBlank(prodName)){
			map.put("prodName", "%"+prodName+"%");
		}		
        String queryAllSQL = ConfigCode.getInstance().getCode("shop.getProdTagRelCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.getProdTagRel", map); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	

}
