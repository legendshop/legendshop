/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.EventDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserEvent;
import com.legendshop.util.AppUtils;

/**
 * 事件Dao.
 */
@Repository
public class EventDaoImpl extends GenericDaoImpl<UserEvent, Long> implements EventDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Override
	public UserEvent getEvent(Long id) {
		return getById(id);
	}

	@Override
	public void deleteEvent(UserEvent userEvent) {
		delete(userEvent);
	}

	@Override
	public Long saveEvent(UserEvent userEvent) {
		return (Long) save(userEvent);
	}

	@Override
	public void updateEvent(UserEvent userEvent) {
		update(userEvent);
	}

	@Override
	public PageSupport getEvent(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public PageSupport<UserEvent> getEventPage(String curPageNO, UserEvent userEvent) {
		CriteriaQuery cq = new CriteriaQuery(UserEvent.class, curPageNO);
		cq.setPageSize(2);
		cq.setPageSize(systemParameterUtil.getPageSize());

		if (!AppUtils.isBlank(userEvent.getUserName())) {
			cq.like("userName",  StringUtils.trim(userEvent.getUserName()) + "%");
		}
		cq.ge("createTime", userEvent.getStartTime());
		if(AppUtils.isNotBlank(userEvent.getEndTime())){
			cq.le("createTime", DateUtils.getEndMonment(userEvent.getEndTime()));
		}
		cq.addOrder("desc", "createTime");
		return queryPage(cq);
	}

}
