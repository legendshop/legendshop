/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.GrassArticle;
import com.legendshop.util.AppUtils;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassArticleDao;

import java.util.List;
import java.util.Set;

/**
 * The Class GrassArticleDaoImpl. 种草社区文章表Dao实现类
 */
@Repository
public class GrassArticleDaoImpl extends GenericDaoImpl<GrassArticle, Long> implements GrassArticleDao{

	/**
	 * 根据Id获取种草社区文章表
	 */
	public GrassArticle getGrassArticle(Long id){
		return getById(id);
	}

	/**
	 *  删除种草社区文章表
	 *  @param grassArticle 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassArticle(GrassArticle grassArticle){
    	return delete(grassArticle);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassArticle(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草社区文章表
	 */
	public Long saveGrassArticle(GrassArticle grassArticle){
		return save(grassArticle);
	}

	/**
	 *  更新种草社区文章表
	 */		
	public int updateGrassArticle(GrassArticle grassArticle){
		return update(grassArticle);
	}

	/**
	 * 分页查询种草社区文章表列表
	 */
	public PageSupport<GrassArticle>queryGrassArticle(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassArticle.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("thumbNum");
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<GrassArticle> ps = queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo, String name,
			Integer pageSize) {
		CriteriaQuery query = new CriteriaQuery(GrassArticle.class,curPageNo);
		query.setPageSize(pageSize);
		query.addDescOrder("thumbNum");
		query.addDescOrder("createTime");
		query.addDescOrder("id");
		if (AppUtils.isNotBlank(name)) {
			query.like("title", name,MatchMode.ANYWHERE);
		}
		PageSupport<GrassArticle> ps=queryPage(query);
		return ps;
	}

	@Override
	public Long getThumbCountByUid(String uid) {
		String sql="SELECT SUM(thumb_num) FROM ls_grass_article WHERE user_id=?";
		return get(sql, Long.class, uid);
	}

	@Override
	public PageSupport<GrassArticle> queryGrassArticleByLabels(String pageNo, int size, Set<Long> ids) {
		CriteriaQuery query = new CriteriaQuery(GrassArticle.class,pageNo);
		query.setPageSize(size);
		query.addDescOrder("thumbNum");
		query.addDescOrder("createTime");
		query.addDescOrder("id");
		query.in("id",ids.toArray());
		PageSupport<GrassArticle> ps=queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo, Integer pageSize, String condition,
			String order) {
		CriteriaQuery query = new CriteriaQuery(GrassArticle.class,curPageNo);
		query.setPageSize(pageSize);
		if ("synthesize".equals(order)) {
			query.addDescOrder("thumbNum");
			query.addDescOrder("publishTime");
		}else if ("hot".equals(order)) {
			query.addDescOrder("thumbNum");
		}else if ("new".equals(order)) {
			query.addDescOrder("publishTime");
		}
		if (AppUtils.isNotBlank(condition)) {
			Criterion cqs= query.or(Restrictions.like("title","%"+condition+"%"), Restrictions.like("writerName","%"+condition+"%"));
			query.add(cqs);
		}
		PageSupport<GrassArticle> ps=queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<GrassArticle> queryArticleBywriterId(String curPageNo, String writerId,
														   Integer pageSize) {
		CriteriaQuery query = new CriteriaQuery(GrassArticle.class,curPageNo);
		query.setPageSize(pageSize);
		query.addDescOrder("thumbNum");
		query.addDescOrder("createTime");
		query.addDescOrder("id");
		if (AppUtils.isNotBlank(writerId)) {
			query.eq("userId", writerId);
		}
		PageSupport<GrassArticle> ps=queryPage(query);
		return ps;
	}
}
