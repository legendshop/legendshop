/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.LocationDao;
import com.legendshop.business.dao.UserAddressDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.util.AppUtils;

/**
 * 用户地址服务.
 */
@Service("userAddressService")
public class UserAddressServiceImpl implements UserAddressService {

	@Autowired
	private UserAddressDao userAddressDao;

	@Autowired
	private LocationDao locationDao;

	public List<UserAddress> getUserAddress(String userName) {
		return userAddressDao.getUserAddress(userName);
	}

	public UserAddress getUserAddress(Long id) {
		return userAddressDao.getUserAddress(id);
	}

	public int deleteUserAddress(UserAddress userAddress) {
		return userAddressDao.deleteUserAddress(userAddress);
	}
	
	/**保存或者更新 **/
	public Long saveUserAddress(UserAddress userAddress,String userId) {
		if (!AppUtils.isBlank(userAddress.getAddrId())) {
			updateUserAddress(userAddress,userId);
			return userAddress.getAddrId();
		}else{
			return  userAddressDao.save(userAddress);
		}
	}
	
	/**保存或者更新,且设置为默认地址 **/
	public Long saveUserAddressAndDefault(UserAddress userAddress,String userId) {
		if (!AppUtils.isBlank(userAddress.getAddrId())) {
			updateUserAddress(userAddress,userId);
			Long addrId =  userAddress.getAddrId();
			if(addrId!=null){
				userAddressDao.updateDefaultUserAddress(addrId,userId);
			}
			return addrId;
		}else{//保存且设置为默认地址
			Long addrId = userAddressDao.save(userAddress);
			if(addrId!=null){
				userAddressDao.updateDefaultUserAddress(addrId,userId);
			}
			return addrId;
		}
	}

	/**
	 * 分页查询地址
	 *
	 * @param userId
	 * @param curPageNO
	 * @param pageSize
	 */
	@Override
	public PageSupport<UserAddress> queryUserAddress(String userId, String curPageNO, int pageSize) {
		return userAddressDao.queryUserAddress(userId, curPageNO, pageSize);
	}

	public void updateUserAddress(UserAddress userAddress,String userId) {
		UserAddress origin = userAddressDao.getById(userAddress.getAddrId());
		if(origin == null){
			return;
		}
		if(userId.equals(origin.getUserId())){
			origin.setAreaId(userAddress.getAreaId());
			origin.setCityId(userAddress.getCityId());
			origin.setModifyTime(new Date());
			origin.setMobile(userAddress.getMobile());
			origin.setProvinceId(userAddress.getProvinceId());
			origin.setReceiver(userAddress.getReceiver());
			origin.setCommonAddr(userAddress.getCommonAddr());
			origin.setStatus(userAddress.getStatus());
			origin.setSubAdds(userAddress.getSubAdds());
			origin.setEmail(userAddress.getEmail());
			origin.setSubPost(userAddress.getSubPost());
			origin.setAliasAddr(userAddress.getAliasAddr());
			origin.setVersion(origin.getVersion()+1);
			origin.setTelphone(userAddress.getTelphone());
			userAddressDao.updateUserAddress(origin);
		}
	}

	@Override
	public Long getMaxNumber(String userName) {
		return userAddressDao.getMaxNumber(userName);
	}

	@Override
	public Area getAreaById(Integer id) {
		return locationDao.getAreaById(id);
	}

	@Override
	public void updateDefaultUserAddress(Long addrId,String userId) {
		userAddressDao.updateDefaultUserAddress(addrId,userId);
	}

	@Override
	public UserAddress getDefaultAddress(String userId) {
		return userAddressDao.getDefaultAddress(userId);
	}

	@Override
	public PageSupport<UserAddress> UserAddressPage(String curPageNO,String userId) {
		return userAddressDao.UserAddressPage(curPageNO,userId);
	}
	
	@Override
	public PageSupport<UserAddress> queryUserAddress(String curPageNO,String userId) {
		return userAddressDao.queryUserAddress(curPageNO,userId);
	}

	@Override
	public PageSupport<UserAddress> getUserAddressPage(String curPageNO, UserAddress userAddress) {
		return userAddressDao.getUserAddressPage(curPageNO,userAddress);
	}

	@Override
	public PageSupport<UserAddress> getUserAddress(String curPageNO, String userName) {
		return userAddressDao.queryPage(curPageNO,userName);
	}

	@Override
	public void updateOtherDefault(Long addrId, String userId, String commonAddr) {
		userAddressDao.updateOtherDefault(addrId,userId, commonAddr);
	}
}
