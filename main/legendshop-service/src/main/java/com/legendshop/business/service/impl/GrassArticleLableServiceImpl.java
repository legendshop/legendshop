/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassArticleLableDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticleLable;
import com.legendshop.spi.service.GrassArticleLableService;
import com.legendshop.util.AppUtils;

/**
 * The Class GrassArticleLableServiceImpl.
 *  种草社区文章关联标签表服务实现类
 */
@Service("grassArticleLableService")
public class GrassArticleLableServiceImpl implements GrassArticleLableService{

    /**
     *
     * 引用的种草社区文章关联标签表Dao接口
     */
	@Autowired
    private GrassArticleLableDao grassArticleLableDao;
   
   	/**
	 *  根据Id获取种草社区文章关联标签表
	 */
    public GrassArticleLable getGrassArticleLable(Long id) {
        return grassArticleLableDao.getGrassArticleLable(id);
    }
    
    /**
	 *  根据Id删除种草社区文章关联标签表
	 */
    public int deleteGrassArticleLable(Long id){
    	return grassArticleLableDao.deleteGrassArticleLable(id);
    }

   /**
	 *  删除种草社区文章关联标签表
	 */ 
    public int deleteGrassArticleLable(GrassArticleLable grassArticleLable) {
       return  grassArticleLableDao.deleteGrassArticleLable(grassArticleLable);
    }

   /**
	 *  保存种草社区文章关联标签表
	 */	    
    public Long saveGrassArticleLable(GrassArticleLable grassArticleLable) {
        if (!AppUtils.isBlank(grassArticleLable.getId())) {
            updateGrassArticleLable(grassArticleLable);
            return grassArticleLable.getId();
        }
        return grassArticleLableDao.saveGrassArticleLable(grassArticleLable);
    }

   /**
	 *  更新种草社区文章关联标签表
	 */	
    public void updateGrassArticleLable(GrassArticleLable grassArticleLable) {
        grassArticleLableDao.updateGrassArticleLable(grassArticleLable);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassArticleLable> queryGrassArticleLable(String curPageNO, Integer pageSize){
     	return grassArticleLableDao.queryGrassArticleLable(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassArticleLableDao(GrassArticleLableDao grassArticleLableDao) {
        this.grassArticleLableDao = grassArticleLableDao;
    }

	@Override
	public void deleteByGrassId(Long graid) {
		// TODO Auto-generated method stub
		  grassArticleLableDao.deleteByGrassId(graid);
	}
    
}
