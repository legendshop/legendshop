/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.integral.IntegralOrderItem;

/**
 * 积分订单项目Dao接口.
 */
public interface IntegralOrderItemDao extends GenericDao<IntegralOrderItem, Long> {

	/** 获取积分订单项目 */
	public abstract IntegralOrderItem getIntegralOrderItem(Long id);

	/** 删除积分订单项目 */
	public abstract int deleteIntegralOrderItem(IntegralOrderItem integralOrderItem);

	/** 保存积分订单项目 */
	public abstract Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem);

	/** 更新积分订单项目 */
	public abstract int updateIntegralOrderItem(IntegralOrderItem integralOrderItem);

	public abstract List<IntegralOrderItem> queryByUserIdAndProdId(String userId, Long prodId);
	
	public abstract List<IntegralOrderItem> queryByOrderId(Long orderId);

}
