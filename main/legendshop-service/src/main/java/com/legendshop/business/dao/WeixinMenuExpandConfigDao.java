/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;

/**
 *微信菜单扩展Dao
 */
public interface WeixinMenuExpandConfigDao extends GenericDao<WeixinMenuExpandConfig, Long> {
     
    public abstract List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig();

	public abstract WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id);
	
    public abstract int deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract int updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract PageSupport<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String curPageNO,
			WeixinMenuExpandConfig weixinMenuExpandConfig);
	
 }
