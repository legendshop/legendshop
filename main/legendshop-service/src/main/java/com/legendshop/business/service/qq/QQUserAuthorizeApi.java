package com.legendshop.business.service.qq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.dto.qq.QQAuthorizeUserInfo;
import com.legendshop.model.dto.qq.QQMeResponse;
import com.legendshop.model.dto.qq.QQUserToken;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

/**
 * QQ用户授权认证相关API
 * @author 开发很忙
 */
public class QQUserAuthorizeApi {
	
	private static Logger log = LoggerFactory.getLogger(QQUserAuthorizeApi.class);

	/** 通过 code 换取 accesstoken */
	private final static String OAUTH2_TOKEN= "https://graph.qq.com/oauth2.0/token?grant_type=GRANT_TYPE&client_id=CLIENT_ID&client_secret=CLIENT_SECRET&code=CODE&redirect_uri=REDIRECT_URI";
	
	/** 获取用户openId */
	private final static String OAUTH2_ME = "https://graph.qq.com/oauth2.0/me?access_token=ACCESS_TOKEN&unionid=UNIONID";
	
	/** 获取用户信息 */
	private final static String USER_GETUSERINFO = "https://graph.qq.com/user/get_user_info?access_token=ACCESS_TOKEN&oauth_consumer_key=OAUTH_CONSUMER_KEY&openid=OPENID";

	/**
	 * 获取accssToken
	 * @param appId 
	 * @param appSecret 
	 * @param code 
	 * @return 
	 */
	public static QQUserToken getAccessToken(String grantType, String clientId, String clientSecret, String code, String redirectURI) {
		
        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = OAUTH2_TOKEN.replace("GRANT_TYPE", grantType)
        		.replace("CLIENT_ID", clientId)
        		.replace("CLIENT_SECRET", clientSecret)
        		.replace("CODE", code)
        		.replace("REDIRECT_URI", redirectURI);
        
        String text = HttpUtil.httpsRequest(url, "GET", null);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取用户 accessToken 失败 ####################");
        	return null;
        }
        else
		{
			log.info("############## 获取用户 accessToken 成功 #################### , {}",text);
		}
        
        QQUserToken token = new QQUserToken();

		String accessToken;
		String expiresIn;

		try {
			log.info("获取token的返回值：{}", text);
			String[] split = text.split("&");
			accessToken = split[0].split("=")[1];
			expiresIn = split[1].split("=")[1];

			token.setAccess_token(accessToken);
			token.setExpires_in(Integer.parseInt(expiresIn));

		} catch (Exception e) {
			throw new RuntimeException("获取accessToken失败");
		}

		return token;
	}
	
	/**
	 * 通过accessToken获取openId
	 * @param accessToken 
	 */
	public static QQMeResponse getOpenid(String accessToken) {
		
        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = OAUTH2_ME.replace("ACCESS_TOKEN", accessToken)
        		.replace("UNIONID", "1");//代表是获取unionid
        
        String text = HttpUtil.httpsRequest(url, "GET", null);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取QQ用户 openId 失败 ####################");
        	return null;
        }
        else
		{
			QQMeResponse me = new QQMeResponse();
			log.info("[QQ登录] 获取OpenId的 Result：{}", text);
			String[] split = text.split("\"");
			String openId = split[split.length - 2];
			log.info("[QQ登录] 获取OpenId的 openId：{}", openId);
			me.setOpenid(openId);
			return me;
		}
	}
	
	/**
	 * 获取用户信息
	 * @param accessToken
	 * @param oauthConsumerKey 也就是appId
	 * @param openid
	 * @return
	 */
	public static QQAuthorizeUserInfo getUserInfo(String accessToken, String oauthConsumerKey, String openid) {
		
        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = USER_GETUSERINFO.replace("ACCESS_TOKEN", accessToken)
        		.replace("OAUTH_CONSUMER_KEY", oauthConsumerKey)
        		.replace("OPENID", openid);
        
        String text = HttpUtil.httpsRequest(url, "GET", null);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取QQ用户信息 失败 ####################");
        	return null;
        }
        
        QQAuthorizeUserInfo userInfo = JSONObject.parseObject(text, QQAuthorizeUserInfo.class);

		return userInfo;
	}
}
