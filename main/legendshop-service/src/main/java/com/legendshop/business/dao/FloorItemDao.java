/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Product;

/**
 * 楼层元素Dao
 *
 */
public interface FloorItemDao extends GenericDao<FloorItem, Long>{
     
    public abstract List<FloorItem> getFloorItem(String shopName);
    
    public abstract List<FloorItem> getFloorItemST(Long flId);

	public abstract FloorItem getFloorItem(Long id);
	
    public abstract void deleteFloorItem(String type, Long floorId);
	
	public abstract Long saveFloorItem(FloorItem floorItem);
	
	public abstract void updateFloorItem(FloorItem floorItem);
	
	public abstract PageSupport getFloorItem(CriteriaQuery cq);
	
	public abstract List<FloorItem> getAllSortFloorItem(Long flId);
	
	public abstract void deleteFloorSubItems(Long FiId);
	
	public abstract void deleteFloorItem(Long FlId);
	
	public abstract List<FloorItem> getBrandList(Long floorId,String layout,int limit);
	
	public abstract List<FloorItem> getRankList(Long floorId);
	
	public abstract FloorItem getFloorGroup(Long floorId);

	public abstract List<FloorItem> getNewsList(Long floorId); 
	
	public FloorItem getAdvFloorItem(Long flId,Long fiId,String type);
	
	public List<FloorItem> queryAdvFloorItem(Long flId,String type,String layout);
	
	public FloorItem getAdvFloorItem(Long flId, String type,String  layout);

	public abstract void updateFloorItemForAdv(FloorItem floorAdv);

	List<FloorItem> getUserSortFloorItem(Long FloorId, String layout);

	public abstract PageSupport<Brand> getBrandListPage(String curPageNO, String brandName);

	public abstract PageSupport<Product> getGroupListPage(String curPageNO, String groupName);

	public abstract PageSupport<FloorItem> queryAdvFloorItemPage(String curPageNO, Long flId, String layout,
			String title);

	public abstract PageSupport<News> getNewsListPage(String curPageNO, Long newsCategoryId, String newsTitle,String userName);
 }
