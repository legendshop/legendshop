/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProductCommentStatDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProductCommentStat;

/**
 * 商品评论统计表
 */
@Repository
public class ProductCommentStatDaoImpl extends GenericDaoImpl<ProductCommentStat, Long> implements ProductCommentStatDao  {
     
	public ProductCommentStat getProductCommentStat(Long id){
		return getById(id);
	}
	
	@CacheEvict(value = "ProductCommentStat", key = "#productCommentStat.prodId")
    public int deleteProductCommentStat(ProductCommentStat productCommentStat){
    	return delete(productCommentStat);
    }
	@CacheEvict(value = "ProductCommentStat", key = "#productCommentStat.prodId")
	public Long saveProductCommentStat(ProductCommentStat productCommentStat){
		return save(productCommentStat);
	}
	
    @CacheEvict(value = "ProductCommentStat", key = "#productCommentStat.prodId")
	public int updateProductCommentStat(ProductCommentStat productCommentStat){
		return update(productCommentStat);
	}

	@Override
	@Cacheable(value = "ProductCommentStat", key = "#prodId")
	public ProductCommentStat getProductCommentStatByProdId(Long prodId) {
		String sql = "select * from ls_prod_comm_stat where prod_id = ?";
		return this.get(sql, ProductCommentStat.class, prodId);
	}

	@Override
	public ProductCommentStat getProductCommentStatByProdIdForUpdate(Long prodId) {
		String sql = "select * from ls_prod_comm_stat where prod_id = ? for update";
		return this.get(sql, ProductCommentStat.class, prodId);
	}

	@Override
	@Cacheable(value = "ProductCommentStat", key = "#shopId")
	public ProductCommentStat getProductCommentStatByShopId(Long shopId) {
		String sql="SELECT  SUM(s.score) AS score,SUM(s.comments) AS comments FROM ls_prod_comm_stat s, ls_prod p WHERE s.prod_id = p.prod_id AND p.shop_id = ?";
		return this.get(sql, ProductCommentStat.class, shopId);
	}

	@Override
	@CacheEvict(value = "ProductCommentStat", key = "#prodId")
	public void updateCommentsAndScore(Integer comments, Integer score,
			Long prodId) {
		this.update("update ls_prod_comm_stat set comments=?, score=? where prod_id=?", comments, score, prodId);
	}

 }
