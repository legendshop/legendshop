/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Theme;

/**
 * 专题Dao
 *
 */
public interface ThemeDao extends Dao<Theme, Long> {
     
    public abstract List<Theme> getTheme();
    
	public abstract Theme getTheme(Long id);
	
    public abstract int deleteTheme(Theme theme);
	
	public abstract Long saveTheme(Theme theme);
	
	public abstract int updateTheme(Theme theme);
	
	public abstract boolean hasThemes(Long id);
	
	/**
	 * 分页查询专题，查询出是否过期
	 * @param curPage
	 * @param pageSize
	 * @return
	 * @since 2015-4-10 下午2:24:28
	 * @version v1.0
	 */
	PageSupport getThemePage(String curPage, Integer pageSize);

	/**
	 * 清理专题详情缓存
	 * @param themeId
	 */
	void clearThemeDetailCache(Long themeId);

	PageSupport<Theme> allThemes(String curPageNO);
	
	public abstract PageSupport<Theme> getThemePage(String curPageNO, String name);

	public abstract PageSupport<Theme> getThemePage(String curPageNO);

	public abstract PageSupport<Theme> getThemePage(String curPageNO, Theme entity, DataSortResult result);

	public abstract PageSupport<Theme> getThemeQueryContent(String curPageNO,Integer status, DataSortResult result, String title);

    PageSupport<Theme> getSelectTheme(String curPageNO, String title,Long themeId);
}
