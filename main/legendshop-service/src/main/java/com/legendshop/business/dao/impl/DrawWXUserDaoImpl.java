/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DrawWXUserDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

/**
 * 微信用户抽奖Dao实现类.
 */
@Repository
public class DrawWXUserDaoImpl extends GenericDaoImpl<WeixinGzuserInfo, Long>  implements DrawWXUserDao {

	/* (non-Javadoc)
	 * @see com.legendshop.draw.dao.DrawWXUserDao#getGzuserInfo(java.lang.String)
	 */
	@Override
	public WeixinGzuserInfo getGzuserInfo(String openId) {
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("openid", openId));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
	}

	/**
	 * Gets the gzuser info by user id.
	 *
	 * @param userId the user id
	 * @return the gzuser info by user id
	 * @Description: 根据用户ID获取openID
	 * @date 2016-4-28
	 */
	@Override
	public WeixinGzuserInfo getGzuserInfoByUserId(String userId) {
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("userId", userId));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
	}

}
