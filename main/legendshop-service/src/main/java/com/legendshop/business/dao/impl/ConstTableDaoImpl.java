/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.Sorting;
import com.legendshop.dao.support.Sorting.Ordering;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.ConstTableId;
import com.legendshop.util.AppUtils;

/**
 * 常量记录Dao.
 */
@Repository
public class ConstTableDaoImpl extends GenericDaoImpl<ConstTable, ConstTableId> implements ConstTableDao {


	private final static String UPDATECONSTTABLESQL_STRING="update ls_cst_table set value=? where type=? and key_value=?";

	/**
	 * 查找所有的常量记录
	 */
	@Override
	public List<ConstTable> loadAllConstTable() {
		Sorting sort = new Sorting(Ordering.asc("seq"));
		sort.addOrder(Ordering.asc("id.type"));
		return queryAll(sort);
	}

	@Override
//	@Cacheable(value = "ConstTable", key = "#type + #keyValue ")
	public ConstTable getConstTablesBykey(String type, String keyValue) {
		List<ConstTable> constTables=this.queryByProperties(new EntityCriterion().eq("id.key", keyValue).eq("id.type", type).addAscOrder("seq"));
		if(AppUtils.isNotBlank(constTables)){
			return constTables.get(0);
		}
		return null;
	}


	@Override
	@CacheEvict(value = "ConstTable", key = "#type + #key ")
	public void updateConstTableByTepe(String type, String key, String value) {
		update(UPDATECONSTTABLESQL_STRING, value, type,key);
	}

	@Override
	public ConstTable getConstTablesBykeyForNoCache(String type, String keyValue) {
		List<ConstTable> constTables=this.queryByProperties(new EntityCriterion().eq("id.key", keyValue).eq("id.type", type).addAscOrder("seq"));
		if(AppUtils.isNotBlank(constTables)){
			return constTables.get(0);
		}
		return null;
	}
}
