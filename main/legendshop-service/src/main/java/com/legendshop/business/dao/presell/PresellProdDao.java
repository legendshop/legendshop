/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.model.dto.presell.PresellSearchParamDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.PresellProd;

/**
 * 预售商品Dao
 * 
 */
public interface PresellProdDao extends GenericDao<PresellProd, Long> {

	/** 获取预售商品 */
	public abstract PresellProd getPresellProd(Long id);

	/** 删除预售商品 */
	public abstract int deletePresellProd(PresellProd presellProd);

	/** 保存预售商品 */
	public abstract Long savePresellProd(PresellProd presellProd);

	/** 更新预售商品 */
	public abstract int updatePresellProd(PresellProd presellProd);

	/** 获取预售商品Dto */
	public abstract PresellProdDto getPresellProdDto(Long id);

	/** 获取预售商品 */
	public abstract PresellProd getPresellProd(String schemeName, Long shopId);

	/** 获取预售商品详情Dto */
	public abstract PresellProdDetailDto getPresellProdDetailDto(Long presellId);

	/** 根据主键更新预售活动状态 */
	public abstract int updatePresellStatus(Long id,Long prodId,Long status);

	/** 获取预售商品 */
	public abstract PresellProd getPresellProd(Long prodId, Long skuId);

	/** 将预售商品转换成购物车对象 */
	public abstract ShopCartItem findPreSellCart(Long id,Long skuId);

	/** 查询预售商品页面 */
	public abstract PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, boolean isExpires, PresellProdDto presellProd);

	/** 查询预售商品页面 */
	public abstract PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, Long shopId, PresellSearchParamDto paramDto);

	public abstract PageSupport<PresellProd> queryPresellProdListPage(String curPageNO, ProductSearchParms parms);

	public abstract PresellProd batchDelete(String ids);

	/***
	 * 获取已过期未完成的预售活动
	 * @param now
	 * @return
	 */
	public abstract List<PresellProd> getPresellProdByTime(Date now);

	/**
	 * 获取审核过程中，未审核或审核拒绝后导致未能正常上线过期的预售活动
	 * @param now 当前时间
	 * @return
	 */
	public abstract List<PresellProd> getAuditExpiredPresellProd(Date now);

	/**
	 *获取运营数据
	 * @param id
	 * @return
	 */
    OperateStatisticsDTO getOperateStatistics(Long id);
}
