/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.NewsDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.NewsPositionEnum;
import com.legendshop.model.entity.News;
import com.legendshop.util.AppUtils;

/**
 * 文章Dao实现类.
 */
@Repository
public class NewsDaoImpl extends GenericDaoImpl<News, Long> implements NewsDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	private static String sqlForGetNewsByCategory = "select n.news_id as newsId, c.news_category_id as newsCategoryId,c.news_category_name as newsCategoryName, n.news_title as newsTitle from ls_news n,ls_news_cat c where c.news_category_id = n.news_category_id and n.status = 1 and c.status = 1 order by c.seq,n.seq";
	
	private static String sqlForGetNewsBPosition="SELECT n.news_id AS newsId, n.news_title AS newsTitle FROM ls_news n WHERE  n.status ='1' AND n.news_id IN ( SELECT  ls_news.news_id  FROM ls_news LEFT JOIN ls_news_position ON  ls_news.news_id = ls_news_position.news WHERE ls_news_position.news_cat=? )";

	private static String sqlForParseNews = "SELECT n.*, np.position AS POSITION ,nc.news_category_name AS newsCategoryName,nc.pic as newsCategoryPic " +
			"FROM ls_news n LEFT JOIN ls_news_position np ON np.news= n.news_id " +
			"JOIN ls_news_cat nc ON nc.news_category_id= n.news_category_id " +
			"WHERE np.position = ? AND n.status = ? " +
			"AND nc.status=? "+
			"ORDER BY nc.seq,np.seq";
	

	@Override
	@Cacheable(value = "NewsList")
	public List<News> getNews(final NewsPositionEnum newsPositionEnum, final Integer num) {
		List<News> list = parseNews(newsPositionEnum, num);
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.NewsDao#getNews(com.legendshop.core.
	 * dao.support.CriteriaQuery)
	 */
	@Override
	/*@Cacheable(value = "NewsList", condition = "T(Integer).parseInt(#curPageNO) < 3")*/
	public PageSupport<News> getNews(String curPageNO,Long newsCategoryId) {
		
		curPageNO = curPageNO == null ? "1":curPageNO;
		// Qbc查找方式
		CriteriaQuery cq = new CriteriaQuery(News.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("status", Constants.ONLINE);
		cq.eq("newsCategoryId", newsCategoryId);
		cq.addAscOrder("seq");
		PageSupport<News> ps = queryPage(cq);
		return ps;
	}

	@Override
	@Caching(evict={@CacheEvict(value = "News", key = "#news.newsId"),@CacheEvict(value = "NewsList", allEntries=true)})
	public void updateNews(News news) {
		update(news);
	}

	@Override
	@Caching(evict={@CacheEvict(value = "News", key = "#id"),@CacheEvict(value = "NewsList", allEntries=true)})
	public void deleteNewsById(Long id) {
		News news = getNewsById(id);
		if (news != null) {
			delete(news);
		}
	}
	
	/**
	 * 获取系统中所有的文章的条数.
	 *
	 * @return the all news
	 */

	@Override
	public Long getAllNewsCount() {
		return getCount();
	}

	
	/**
	 *  获取系统中所有的文章.
	 *
	 * @return the all news
	 */
	public  List<News> getAllNews(){
		return queryAll();
	}

	@Override
	@Cacheable(value = "NewsList")
	public Map<KeyValueEntity, List<News>> getNewsByCategory() {
		List<News> newsList = query(sqlForGetNewsByCategory, News.class);
		Map<KeyValueEntity, List<News>> newsMap = null;
		if (AppUtils.isNotBlank(newsList)) {
			newsMap = new LinkedHashMap<KeyValueEntity, List<News>>();

			for (News news : newsList) {
				KeyValueEntity keyValueEntity = new KeyValueEntity(news.getNewsCategoryId().toString(),
						news.getNewsCategoryName());
				List<News> newsCatList = newsMap.get(keyValueEntity);
				if (newsCatList == null) {
					newsCatList = new ArrayList<News>();
				}
				newsCatList.add(news);
				newsMap.put(keyValueEntity, newsCatList);
			}
		}
		return newsMap;
	}

	/**
	 * 获取该文章的上一条或者下一条
	 */
	@Override
	public List<News> getNearNews(Long id) {
		String queryNewsSQL = ConfigCode.getInstance().getCode("biz.getNews");
		return query(queryNewsSQL, News.class, id,  id);
	}

	/**
	 * 处理文章
	 *
	 */
	private List<News> parseNews( final NewsPositionEnum newsPositionEnum, final Integer num) {
		return queryLimit(sqlForParseNews, News.class, 0, num,  newsPositionEnum.value(), Constants.ONLINE, Constants.ONLINE);
	}

	@Override
	public News getNewsById(Long id) {
		News news = getByProperties(new EntityCriterion().eq("newsId", id));
		return news;
	}

	@Override
	public List<News> getNewsBycatId(Long catid) {
		return queryByProperties(new EntityCriterion().eq("newsCategoryId", catid));
	}

	@Override
	public List<News> getNewsByposition(Long catid) {
		return query(sqlForGetNewsBPosition, News.class, catid);
	}

	@Override
	public PageSupport<News> getSerachNews(String keyword,String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(News.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("status", Constants.ONLINE);
		if(AppUtils.isNotBlank(keyword)){
			Criterion cqs= cq.or(Restrictions.like("newsTitle", "%"+keyword+"%"), Restrictions.like("newsContent", "%"+keyword+"%"));
			cq.add(cqs);
		}
		PageSupport<News> ps = queryPage(cq);
		return ps;
	}
	
	@Override
	public PageSupport<News> help(String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(News.class, systemParameterUtil.getPageSize(), curPageNO);
		 QueryMap paramMap = new QueryMap();
		// 条件,注意条件顺序跟SQL对应
		 paramMap.put("status", Constants.ONLINE);
	     paramMap.put("position", NewsPositionEnum.NEWS_APP.value());

		String QueryNsortCount  = ConfigCode.getInstance().getCode("biz.QueryPotionNewsCount", paramMap);
	    String QueryNsort = ConfigCode.getInstance().getCode("biz.QueryPotionNews", paramMap);
		
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<News> getNewsListPage(String curPageNO, News news, Integer pageSize, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(News.class, systemParameterUtil.getPageSize(), curPageNO);
		 QueryMap paramMap = new QueryMap();
		// 条件,注意条件顺序跟SQL对应
		 paramMap.put("userName", news.getUserName());
		 paramMap.put("newsCategoryId", news.getNewsCategoryId());
		 if(AppUtils.isNotBlank(news.getNewsTitle())){			 
			 paramMap.like("newsTitle", news.getNewsTitle().trim());
		 }
		 paramMap.put("status", news.getStatus());
		 paramMap.put("position", news.getPosition());
		 paramMap.put("newsTag", news.getNewsTags());
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			query.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "order by n.seq asc, n.news_date desc");
		}else {
			paramMap.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("biz.QueryNewsCount", paramMap);
		String	QueryNsort = ConfigCode.getInstance().getCode("biz.QueryNews", paramMap);
		
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<News> getNewsListResultDto(String curPageNO, Integer pageSize, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(News.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap paramMap = new QueryMap();
		// 条件,注意条件顺序跟SQL对应
		paramMap.put("status", Constants.ONLINE);
	    paramMap.put("position", NewsPositionEnum.NEWS_APP.value());
	    
	    if (AppUtils.isNotBlank(pageSize)) {
			query.setPageSize(pageSize);
		}
	    if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "order by n.news_date desc");
 		}
		
		String QueryNsortCount  = ConfigCode.getInstance().getCode("biz.QueryPotionNewsCount", paramMap);
	    String QueryNsort = ConfigCode.getInstance().getCode("biz.QueryPotionNews", paramMap);
	    
	    query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	@Caching(evict={@CacheEvict(value = "News", key = "#news.newsId"),@CacheEvict(value = "NewsList", allEntries=true)})
	public void updateStatus(News news) {
		update(news);
	}

	@Override
	public PageSupport<News> getNewsList(String curPageNO, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(News.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap paramMap = new QueryMap();
		// 条件,注意条件顺序跟SQL对应
		paramMap.put("status", Constants.ONLINE);
		paramMap.put("position", -2);
		paramMap.put("shopId",shopId);

		String QueryNsortCount  = ConfigCode.getInstance().getCode("biz.QueryPotionNewsCount", paramMap);
		String QueryNsort = ConfigCode.getInstance().getCode("biz.QueryPotionNews", paramMap);

		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public Map<KeyValueEntity, List<News>> findByCategoryId() {

		List<News> newsList = query("select n.news_id as newsId, c.news_category_id as newsCategoryId,c.news_category_name as newsCategoryName, n.news_title as newsTitle from ls_news n,ls_news_cat c where c.news_category_id = n.news_category_id and n.status = 1 and c.status = 1 and c.news_category_id = 1 order by c.seq,n.seq", News.class);
		Map<KeyValueEntity, List<News>> newsMap = null;
		if (AppUtils.isNotBlank(newsList)) {
			newsMap = new LinkedHashMap<>();

			for (News news : newsList) {
				KeyValueEntity keyValueEntity = new KeyValueEntity(news.getNewsCategoryId().toString(),
						news.getNewsCategoryName());
				List<News> newsCatList = newsMap.get(keyValueEntity);
				if (newsCatList == null) {
					newsCatList = new ArrayList<News>();
				}
				newsCatList.add(news);
				newsMap.put(keyValueEntity, newsCatList);
			}
		}
		return newsMap;
	}
}
