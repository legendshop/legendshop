/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.CateGoryExportDTO;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.CategoryDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.entity.Category;
import com.legendshop.util.AppUtils;

/**
 * 
 * 分类Dao实现类, category内存计算参考CategoryManagerUtil
 *
 */
@Repository
public class CategoryDaoImpl extends GenericDaoImpl<Category, Long> implements CategoryDao  {
	
	/** 基础的商品分类SQL. */
	private static String BASE_SQL_FOR_CATEGORY = "select n.id as id, n.parent_id as parentId, n.name as name ,n.pic as pic ,n.status  as status,n.type, n.navigation_menu as navigationMenu, " +
			"n.type_id as typeId,n.keyword as keyword,n.cat_desc as catDesc,n.title as title,n.grade as grade,n.seq as seq,n.recomm_con as recommCon,n.icon as icon from ls_category n ";
	
	/** The sql for get navigation nsort. */
	private static String sqlForGetNavigationNsort = BASE_SQL_FOR_CATEGORY + "where  n.status = 1 and  n.type= ? and n.navigation_menu = ?  and  n.parent_id=? order by n.seq";
	
	/** The sql for get navigation nsort. */
	private static String sqlGetProductCategory = BASE_SQL_FOR_CATEGORY + "where  n.status = 1 and n.navigation_menu = 1 and  n.type= ? order by n.seq";
	
	private static String sqlGetAllProductCategory = BASE_SQL_FOR_CATEGORY + "where n.type= ? order by n.seq";
	
	/** The sql for get sort. **/
	private static String sqlForFindCategory = BASE_SQL_FOR_CATEGORY + "where  n.status = 1 and  n.type= ? and n.parent_id = ? order by n.seq";

	/**
	 * 根据Id查找对象
	 */
	public Category getCategory(Long id){
		return getById(id);
	}
	
    /**
     * 删除商品分类
     */
    public int deleteCategory(Category category){
    	return delete(category);
    }

    @Override
    public Boolean isExistThirdCategory(Long categoryId) {
        String querySQL = ConfigCode.getInstance().getCode("shop.isExistThirdCategory");
        return getLongResult(querySQL,categoryId) > 0;
    }


    /**
	 * 保存商品分类
	 */
	public Long saveCategory(Category category){
		return save(category);
	}
	
	/**
	 * 更新商品分类
	 */
	public int updateCategory(Category category){
		return update(category);
	}
	
	/**
	 * 查找商品分类分页
	 */
	public PageSupport<Category> getCategory(CriteriaQuery cq){
		return queryPage(cq);
	}

	/** 
	 * 查看团购商品类目列表
	 */
	@Override
	public List<Category> getCategoryList(String type) {
		return this.queryByProperties(new EntityCriterion().eq("type", type).addAscOrder("seq"));
	}

    @Override
    public List<CateGoryExportDTO> getCateGoryExportDTOList() {
        String querySQL = ConfigCode.getInstance().getCode("shop.getCateGoryExportDTOList");
        return query(querySQL,CateGoryExportDTO.class);
    }

    @Override
    public List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId) {
        String querySQL = ConfigCode.getInstance().getCode("shop.getShopCateGoryExportDTOList");
        return query(querySQL,CateGoryExportDTO.class,shopId);
    }

    /**
	 * 查看团购商品类目列表（除去已下线的）
	 */
	@Override
	public List<Category> getCategoryListByStatus(String type) {
		return this.queryByProperties(new EntityCriterion().eq("type", type).eq("status",1).addAscOrder("seq"));
	}

	/** 
	 * 获取商品分类的子类
	 */
	@Override
	public List<Category> getChildren(Long parentId) {
		return query(sqlForGetNavigationNsort, Category.class,  ProductTypeEnum.PRODUCT.value(), 1,parentId);	
	}
	
	@Override
	public List<Category> getProductCategory() {
		return query(sqlGetProductCategory, Category.class,  ProductTypeEnum.PRODUCT.value());	
	}

	/**
	 * 在List对象中不再支持列表缓存,针对部分返回很多对象的情况,不再适宜记录关联关系
	 */
	@Cacheable(value = "CategoryList",key = "'NQ_' + #sortType + #headerMenu + #navigationMenu + #status")
	public List<Category> getCategory(String sortType,  Integer headerMenu,  Integer navigationMenu,Integer status,Integer grade) {
		EntityCriterion criterion =new EntityCriterion();
		if (sortType != null) {
			criterion.eq("type", sortType);
		}
		if (headerMenu != null) {
			criterion.eq("headerMenu", headerMenu);
		}
		if (navigationMenu != null) {
			criterion.eq("navigationMenu", navigationMenu);
		}
		
		if(AppUtils.isNotBlank(grade)){
			criterion.eq("grade", grade);//只查取等级为1的分类
		}
		criterion.addAscOrder("grade");
		criterion.addAscOrder("seq");
		criterion.eq("status", status);
		List<Category> list = queryByProperties(criterion);
		return list;

	}


	@Override
	public List<Category> findCategory(String cateName,Integer grade,Long parentId) {
		List<Category> categoryList = null;
		if(AppUtils.isNotBlank(cateName)){
			 categoryList = this.queryByProperties(new EntityCriterion().eq("status", Constants.ONLINE).eq("parentId", parentId).like("name", cateName, MatchMode.START).eq("grade", grade).eq("type", ProductTypeEnum.PRODUCT.value()).addAscOrder("seq"));
		}else{
			 categoryList = this.queryByProperties(new EntityCriterion().eq("status", Constants.ONLINE).eq("parentId", parentId).eq("grade", grade).eq("type", ProductTypeEnum.PRODUCT.value()).addAscOrder("seq"));
		}
		addChildren(categoryList);
		return categoryList;
	}
	
	
	//组装 子分类
	private void addChildren(List<Category> categoryList){
		if(AppUtils.isNotBlank(categoryList)){
			
			StringBuffer sb = new StringBuffer(BASE_SQL_FOR_CATEGORY + "where  n.status = 1 and  n.type= ? and n.parent_id in (");
			
			List<Object> paramList = new ArrayList<Object>();
			paramList.add(ProductTypeEnum.PRODUCT.value());
			for (Category category : categoryList) {
				sb.append("?,");
				paramList.add(category.getId());
			}
			sb.setLength(sb.length()-1);
			sb.append(") order by n.seq");
			
			List<Category> childrens = query(sb.toString(),Category.class,paramList.toArray());
			
			for (Category cate : categoryList) {
				for (Category children : childrens) {
					cate.addChildren(children);
				}
			}
		}
	}

	@Override
	public List<Category> findCategory(Long parentId) {
		if(AppUtils.isBlank(parentId)){
			throw new BusinessException("parentId id can not be null");
		}
		List<Category> categoryList = query(sqlForFindCategory, Category.class, ProductTypeEnum.PRODUCT.value(), parentId);
		addChildren(categoryList);
		return categoryList;
	}

	@Override
	public List<Category> getCategoryByIds(List<Long> catIdList) {
		return this.queryAllByIds(catIdList);
	}

	@Override
	public void updateCategoryList(List<Category> categorys) {
		this.update(categorys);
	}

	@Override
	public List<Category> getCategorysByTypeId(Integer typeId) {
		return this.queryByProperties(new EntityCriterion().eq("typeId", typeId));
	}

	/**
	 * 取商品的列表
	 */
	@Override
	public List<Category> queryCategoryList() {
		List<Category> categoryList = query("select id as id,name as name,pic as pic from ls_category where grade = 1 and type = 'P' order by seq asc", Category.class);
		return categoryList;
	}

	@Override
	public List<Category> getCategoryListByName(String categoryName) {
		return this.queryByProperties(new EntityCriterion().like("name", categoryName, MatchMode.ANYWHERE).addDescOrder("grade"));
	}

	@Override
	public List<Category> getAllProductCategory() {
		return query(sqlGetAllProductCategory, Category.class,  ProductTypeEnum.PRODUCT.value());	
	}

	@Override
	public PageSupport<Category> getCategoryPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(Category.class, curPageNO);
		return queryPage(cq);
	}

	@Override
	public List<Category> findProdTypeId(Integer prodTypeId) {
		List<Category> list = queryByProperties(new EntityCriterion().eq("typeId", prodTypeId));
		return list;
	}

	@Override
	public List<Category> findCategoryByGradeAndParentId(Integer grade, Long parentId) {
		EntityCriterion criterion =new EntityCriterion();
		criterion.eq("type", ProductTypeEnum.PRODUCT.value());
		if(AppUtils.isNotBlank(grade)){
			criterion.eq("grade", grade);
		}
		criterion.eq("status", 1);
		criterion.eq("parentId", parentId);
		criterion.addAscOrder("seq");
		return queryByProperties(criterion);
	}
	
 }
