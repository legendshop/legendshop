package com.legendshop.business.comparer;

import java.util.Date;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.weixin.WeiXinGroups;
import com.legendshop.model.entity.weixin.WeixinGroup;
import com.legendshop.util.AppUtils;

public class WeiXinGroupsComparator implements DataComparer<WeiXinGroups,WeixinGroup> {
	/**
	 * 微信本地分组信息和服务器分组信息对比类
	 * @author liyuan
	 */
	@Override
	public boolean needUpdate(WeiXinGroups dto, WeixinGroup dbObj, Object obj) {
		if(isChage(dto,dbObj)){
			dbObj.setName(dto.getName());
			return true;
		}
		return false;
	}

	@Override
	public boolean isExist(WeiXinGroups dto, WeixinGroup dbObj) {
		if(AppUtils.isNotBlank(dto) && AppUtils.isNotBlank(dbObj) && dto.getId().equals(dbObj.getId().toString())){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public WeixinGroup copyProperties(WeiXinGroups dtoj, Object obj) {
		WeixinGroup group=new WeixinGroup();
		group.setId(Long.valueOf(dtoj.getId()));
		group.setName(dtoj.getName());
		group.setAddtime(new Date());
		return group;
	}

	/** 是否有改变 **/
	/**
	 * grouId 如果是0 1 2 不让其改变
	 * @param a
	 * @param b
	 * @return
	 */
	private  boolean isChage(WeiXinGroups a, WeixinGroup b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if(b.getId()==0 || b.getId()==1 || b.getId() ==2 ){
				return false;
			}
			if(!a.getName().equals(b.getName())){
				return true;
			}else{
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}
	
}
