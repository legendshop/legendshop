package com.legendshop.business.helper.impl;

import java.util.Map;

import com.legendshop.base.util.MailHelper;


/**
 * The Class MailHelperImpl.
 */
public class MailHelperImpl implements MailHelper{
	
	/** The mail map. */
	private Map<String,String> mailMap;
	
	/**
	 * Sets the mail map.
	 *
	 * @param mailMap the mail map
	 */
	public void setMailMap(Map<String, String> mailMap) {
		this.mailMap = mailMap;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.business.helper.MailHelper#getMailAddress(java.lang.String)
	 */
	@Override
	public String getMailAddress(String mailPrefix) {
		return mailMap.get(mailPrefix);
	}

}
