/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopDomainDao;
import com.legendshop.model.entity.ShopDomain;
import com.legendshop.spi.service.ShopDomainService;
import com.legendshop.util.AppUtils;

/**
 * 店铺域名服务
 */
@Service("shopDomainService")
public class ShopDomainServiceImpl  implements ShopDomainService{
	
	@Autowired
    private ShopDomainDao shopDomainDao;

    public ShopDomain getShopDomain(Long id) {
        return shopDomainDao.getShopDomain(id);
    }

    public void deleteShopDomain(ShopDomain shopDomain) {
        shopDomainDao.deleteShopDomain(shopDomain);
    }

    public Long saveShopDomain(ShopDomain shopDomain) {
        if (!AppUtils.isBlank(shopDomain.getId())) {
            updateShopDomain(shopDomain);
            return shopDomain.getId();
        }
        return shopDomainDao.saveShopDomain(shopDomain);
    }

    public void updateShopDomain(ShopDomain shopDomain) {
        shopDomainDao.updateShopDomain(shopDomain);
    }
}
