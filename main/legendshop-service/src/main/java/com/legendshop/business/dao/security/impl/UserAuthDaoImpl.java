package com.legendshop.business.dao.security.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.legendshop.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.security.AuthDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.constant.RoleEnum;
import com.legendshop.model.security.ShopUser;
import com.legendshop.model.security.StoreEntity;
import com.legendshop.model.security.UserEntity;
import com.legendshop.util.AppUtils;

/**
 * 获取用户的登录信息
 */
@Repository
public class UserAuthDaoImpl implements AuthDao {

    /**
     * The log.
     */
    private static final Logger log = LoggerFactory.getLogger(UserAuthDaoImpl.class);

    // 根据用户名称查找用户相关资料
    private static String sqlForFindUserByName = "SELECT u.id,u.name,u.note, u.password,u.dept_id AS deptId,u.enabled, u.open_id as openId,ud.portrait_pic AS portraitPic,ud.nick_name AS nickName,ud.grade_id AS gradeId, ud.shop_id as shopId  "
            + "FROM ls_user u INNER JOIN  ls_usr_detail ud ON u.id=ud.user_id  WHERE u.enabled = '1'  AND u.name = ?";

    // 根据用户名称,手机号码或者邮件查找用户相关资料
    private static String sqlForFindUserByNameOrMobileOrMail = "SELECT u.id,u.name,u.note, u.password,u.dept_id AS deptId,u.enabled, u.open_id as openId,ud.portrait_pic AS portraitPic,ud.nick_name AS nickName,ud.grade_id AS gradeId, ud.shop_id as shopId  "
            + "FROM ls_user u INNER JOIN  ls_usr_detail ud ON u.id=ud.user_id  WHERE u.enabled = '1'  AND (u.name = ? OR ud.user_mobile = ? OR ud.user_mail = ?)";

    // 根据手机号码查找用户相关资料
    private static String sqlForFindUserByMobile = "SELECT u.id,u.name,u.note, u.password,u.dept_id AS deptId,u.enabled,u.open_id as openId, ud.portrait_pic AS portraitPic,ud.nick_name AS nickName,ud.grade_id AS gradeId, ud.shop_id as shopId  "
            + "FROM ls_user u INNER JOIN  ls_usr_detail ud ON u.id=ud.user_id  WHERE u.enabled = '1'  AND ud.user_mobile = ?";

    // 查询第三方用户
	//查询，去掉 AND ps.source = ? 原因，QQ H5登录和PC端的OPENID，是一样的OPENID。
	//查询，以open_id为唯一就可以，
    private static String sqlForFindThirdParty = "SELECT u.id, u.name, u.note, u.enabled, ps.open_id AS openId, u.dept_id AS deptId, ps.access_token AS password, "
    		+ "ps.access_token AS accessToken, ud.nick_name as nickName, ud.portrait_pic as portraitPic, ud.grade_id as gradeId, ud.shop_id as shopId "
            + "FROM ls_user u "
            + "INNER JOIN ls_usr_detail ud ON u.id = ud.user_id "
            + "INNER JOIN ls_passport p ON u.id = p.user_id "
            + "INNER JOIN ls_passport_sub ps ON ps.passport_id = p.pass_port_id "
            + "WHERE u.enabled = '1' AND u.name = ? "
            + "AND ps.open_id = ? AND p.type = ? ";




    // 根据商城Id来查找商城
    private String sqlForfindShopUserById = "SELECT shop_id,status, user_id, user_name,sd.site_name as shopName FROM ls_shop_detail sd WHERE shop_id =  ?";

    // 根据商城名字来登录商城子账号
    private String sqlForfindShopUserBySiteName = "SELECT shop_id,status, user_id, user_name,sd.site_name as shopName FROM ls_shop_detail sd WHERE site_name =  ?";

    // 根据门店用户名，查找门店
    private String sqlForFindStoreByName = "SELECT u.id as storeId, u.user_name as userName,u.is_disable as enabled,u.shop_id as shopId,name as name,u.passwd as password,shop.user_id as shopUserId,shop.user_name as shopUserName,shop.site_name as shopName FROM ls_store u "
            + "left join ls_shop_detail as shop on shop.shop_id=u.shop_id  where  u.is_disable = '1' and u.user_name = ?";

    private String sqlForFindRolesByUser = "SELECT DISTINCT r.role_type as name FROM ls_usr_role ur ,ls_role r WHERE r.enabled ='1'  AND ur.role_id=r.id AND r.app_no =? AND ur.user_id= ? ";

    private String sqlForFindFunctionsByUser = "select DISTINCT f.protect_function as name from ls_usr_role ur ,ls_role r,ls_perm p, ls_func f where r.enabled = '1'  and ur.role_id=r.id and r.id=p.role_id and p.function_id=f.id and r.app_no = ? and f.app_no = ? and ur.user_id= ?";

    @Autowired
    private GenericJdbcDao genericJdbcDao;

    /**
     * 免密码登录方式
     */
    public UserEntity loadUserByUsername(String username) {
        // 获取用户信息 for rememberMe
        UserEntity user = findUserByName(username);
        if (user == null) {
            log.debug("Can not find user by name {}", username);
            return null;
        }
        setUserRoleAndFunction(user);
        // 返回用户
        return user;
    }

	/**
	 *  根据用户名,手机号, 邮件查找用户
	 *
	 */
	public UserEntity loadUserByUsernameOrMobileOrMail(String username) {
        List<UserEntity> userList = genericJdbcDao.query(sqlForFindUserByNameOrMobileOrMail, new Object[]{username,username,username}, new UserEntityMapper());
        UserEntity user = null;
        if (AppUtils.isNotBlank(userList)) {
            user = userList.iterator().next();
            // 加载商家详情
            user.setShopUser(findShopUser(user.getShopId()));
        }
        if (user == null) {
            log.debug("Can not find user by name {}", username);
            return null;
        }
        setUserRoleAndFunction(user);
        // 返回用户
        return user;
	}

    /**
     * 普通用户登录，查找用户 包括商家子账号登录
     */
    @Override
    public UserEntity loadUserByUsername(String username, String presentedPassword) {
        // 获取用户信息
        UserEntity user = findUserByName(username);
        if (user == null) {
            log.debug("Can not find user by name {}", username);
            return null;
        }

        setUserRoleAndFunction(user);

        // 返回用户
        return user;
    }

    @Override
    public UserEntity loadThirdPartyUser(String username, String accessToken, String openId, String type, String source) {
        // 获取用户信息
        UserEntity user = findThirdParty(username, openId, type, source);
        if (user == null) {
            log.debug("Can not find user by name {}", username);
            return null;
        }

		log.info("additionalThirdPartAuthenticationChecks user info: {} ,accessToken {}",JSONUtil.getJson(user),accessToken);

        //OPEN ID 登录没有验证密码，此处不需要
//        boolean isValid = additionalThirdPartAuthenticationChecks(user, accessToken);
//        if (!isValid) {
//            log.debug("User {} password is not valid", user.getName());
//            return null;
//        }

        setUserRoleAndFunction(user);
        // 返回用户
        return user;
    }

    /**
     * 找到第三方登录的用户
     * @param openId TODO
     */
    private UserEntity findThirdParty(String name, String openId, String type, String source) {

        List<UserEntity> userList = genericJdbcDao.query(sqlForFindThirdParty, new Object[]{name, openId, type, source}, new RowMapper<UserEntity>() {
            public UserEntity mapRow(ResultSet rs, int index) throws SQLException {
                UserEntity user = new UserEntity();
                user.setEnabled(rs.getString("enabled"));
                user.setId(rs.getString("id"));
                user.setName(rs.getString("name"));
                user.setNote(rs.getString("note"));
                user.setPassword(rs.getString("accessToken"));
                user.setPortraitPic(rs.getString("portraitPic"));
                user.setNickName(rs.getString("nickName"));
                user.setDeptId(rs.getInt("deptId"));
                user.setGradeId(rs.getInt("gradeId"));
                user.setShopId(rs.getLong("shopId"));
                user.setOpenId(rs.getString("openId"));
                return user;
            }
        });

        if (AppUtils.isNotBlank(userList)) {
            UserEntity user = userList.iterator().next();
            user.setShopUser(findShopUser(user.getShopId()));
            return user;
        } else {
            return null;
        }
    }

    @Override
    public UserEntity loadUserByMobile(String mobile){
        List<UserEntity> userList = genericJdbcDao.query(sqlForFindUserByMobile, new Object[]{mobile}, new UserEntityMapper());
        if (AppUtils.isNotBlank(userList)) {
            UserEntity user = userList.iterator().next();
            user.setShopUser(findShopUser(user.getShopId()));
            setUserRoleAndFunction(user);
            return user;
        } else {
            return null;
        }
    }

    /**
     * 找到平台普通外部用户
     */
    private UserEntity findUserByName(String name) {
        List<UserEntity> userList = genericJdbcDao.query(sqlForFindUserByName, new Object[]{name}, new UserEntityMapper());
        if (AppUtils.isNotBlank(userList)) {
            UserEntity user = userList.iterator().next();
            // 加载商家详情
            user.setShopUser(findShopUser(user.getShopId()));
            return user;
        } else {
            return null;
        }
    }

    /**
     * 找到用户
     */
    private class UserEntityMapper implements RowMapper<UserEntity> {

        @Override
        public UserEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            UserEntity user = new UserEntity();
            user.setEnabled(rs.getString("enabled"));
            user.setId(rs.getString("id"));
            user.setName(rs.getString("name"));
            user.setNote(rs.getString("note"));
            user.setPassword(rs.getString("password"));
            // 用户头像
            user.setPortraitPic(rs.getString("portraitPic"));
            user.setNickName(rs.getString("nickName"));
            user.setDeptId(rs.getInt("deptId"));
            user.setGradeId(rs.getInt("gradeId"));
            user.setShopId(rs.getLong("shopId"));
            user.setOpenId(rs.getString("openId"));
            return user;
        }

    }

    /**
     * 设置用户的权限
     */
    private void setUserRoleAndFunction(UserEntity user) {

        // 获取角色信息
        Collection<String> roles = findRolesByUser(user.getId(), ApplicationEnum.FRONT_END.value());
        user.setRoles(roles);

        // 获取权限信息
        List<String> functions = this.findFunctionsByUser(user.getId(), ApplicationEnum.FRONT_END.value());
        user.setFunctions(functions);

    }

    /**
             * 第三方登录的验证
     */
    private boolean additionalThirdPartAuthenticationChecks(UserEntity userDetails, String accessToken) {
        // 验证密码
        if (!userDetails.getPassword().equals(accessToken)) {
            log.debug("Authentication failed: password does not match stored value");
            return false;
        }
        return true;
    }

    /**
             * 查找用户所在的商城用户(店铺拥有者)和商城相关信息
     */
    ShopUser findShopUser(String siteName) {
        if (siteName == null) {
            return null;
        }
        log.debug("findShopUserBySiteName, run sql {}, siteName {}", sqlForfindShopUserBySiteName, siteName);
        List<ShopUser> result = null;
        result = genericJdbcDao.query(sqlForfindShopUserBySiteName, new Object[]{siteName}, new ShopUserMapper());
        if (AppUtils.isBlank(result)) {
            return null;
        } else {
            return result.iterator().next();
        }
    }

    /**
             * 查找用户所在的商城用户(店铺拥有者)和商城相关信息
     */
    ShopUser findShopUser(Long shopId) {
        if (shopId == null) {
            return null;
        }
        log.debug("findShopUser, run sql {}, shopId {}", sqlForfindShopUserById, shopId);
        List<ShopUser> result = null;
        result = genericJdbcDao.query(sqlForfindShopUserById, new Object[]{shopId}, new ShopUserMapper());
        if (AppUtils.isBlank(result)) {
            return null;
        } else {
            return result.iterator().next();
        }
    }

    class ShopUserMapper implements RowMapper<ShopUser> {
        public ShopUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            ShopUser shopUser = new ShopUser();
            shopUser.setUserName(rs.getString("user_name"));
            shopUser.setUserId(rs.getString("user_id"));
            shopUser.setShopId(rs.getLong("shop_id"));
            shopUser.setStatus(rs.getInt("status"));
            shopUser.setShopName(rs.getString("shopName"));
            return shopUser;
        }
    }

    /**
             * 加载门店用户
     */
    @Override
    public StoreEntity loadStoreByName(String username, String password) {
        StoreEntity user = findStoreByName(username);
        if (user == null || !user.getEnabled()) {
            log.warn("ShopUser is null or enabled by shopUserId={} and by storeName={} ", user.getShopId(), username);
            return null;
        }

        Collection<String> roles = new ArrayList<String>();
        roles.add(RoleEnum.ROLE_STORE.name()); // 门店登录角色
        user.setRoles(roles);

        Collection<String> functions = new ArrayList<String>();
        functions.add(FunctionEnum.FUNCTION_USER.value());
        user.setFunctions(functions);

        ShopUser shopUser = new ShopUser();
        shopUser.setShopId(user.getShopId());
        shopUser.setStatus(1);
        shopUser.setUserId(user.getShopUserId());
        shopUser.setUserName(user.getShopUserName());
        shopUser.setShopName(user.getShopName());

        user.setShopUser(shopUser);

        return user;
    }

    /**
     * 找到有效时间内的用户
     */
    protected StoreEntity findStoreByName(String name) {
        log.debug("findStoreByName, run sql {}, name {}", sqlForFindUserByName, name);
        List<StoreEntity> list = genericJdbcDao.query(sqlForFindStoreByName, new Object[]{name}, new RowMapper<StoreEntity>() {
            public StoreEntity mapRow(ResultSet rs, int index) throws SQLException {
                StoreEntity user = new StoreEntity();
                user.setEnabled(rs.getBoolean("enabled"));
                user.setId(rs.getLong("storeId"));
                user.setUserName(rs.getString("userName"));
                user.setPassword(rs.getString("password"));
                user.setShopId(rs.getLong("shopId"));
                user.setName(rs.getString("name"));
                user.setShopUserId(rs.getString("shopUserId"));
                user.setShopUserName(rs.getString("shopUserName"));
                user.setShopName(rs.getString("shopName"));
//				user.setOpenId(rs.getString("openId"));
                return user;
            }
        });
        if (AppUtils.isBlank(list)) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
             * 根据用户ID取得对应的角色
     */
    public List<String> findRolesByUser(String userId, String appNo) {
        log.debug("findRolesByUser,run sql {}, userId {}", sqlForFindRolesByUser, userId);
        return genericJdbcDao.query(sqlForFindRolesByUser, new Object[]{appNo, userId}, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getString("name");
            }
        });
    }

    /**
             * 根据用户找到权限点
     */
    public List<String> findFunctionsByUser(String userId, String appNo) {
        log.debug("findFunctionsByUser,run sql {}, userId {}", sqlForFindFunctionsByUser, userId);
        return genericJdbcDao.query(sqlForFindFunctionsByUser, new Object[]{appNo, appNo, userId}, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getString("name");
            }
        });
    }

}
