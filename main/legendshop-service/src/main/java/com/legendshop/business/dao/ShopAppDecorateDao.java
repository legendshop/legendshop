/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopAppDecorate;

/**
 * The Class ShopAppDecorateDao. 移动端店铺主页装修Dao接口
 */
public interface ShopAppDecorateDao extends Dao<ShopAppDecorate,Long>{

	/**
	 * 根据Id获取移动端店铺主页装修
	 */
	public abstract ShopAppDecorate getShopAppDecorate(Long id);

	/**
	 *  根据Id删除移动端店铺主页装修
	 */
    public abstract int deleteShopAppDecorate(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteShopAppDecorate(ShopAppDecorate shopAppDecorate);

	/**
	 * 保存移动端店铺主页装修
	 */
	public abstract Long saveShopAppDecorate(ShopAppDecorate shopAppDecorate);

	/**
	 *  更新移动端店铺主页装修
	 */		
	public abstract int updateShopAppDecorate(ShopAppDecorate shopAppDecorate);


	 /***
     * 查询移动端店铺主页装修列表
     * @param curPageNO 当前页码
     * @param pageSize 页数
     * @param shopAppDecorate 查询参数
     * @return
     */
	public abstract PageSupport<ShopAppDecorate> queryShopAppDecorate(String curPageNO, Integer pageSize,
			ShopAppDecorate shopAppDecorate);

	/**
	 * 根据店铺id获取移动店铺首页装修数据
	 * @param shopId 店铺Id
	 * @return
	 */
	public abstract ShopAppDecorate getShopAppDecorateByShopId(Long shopId);

	/**
	 * 获取正常上线的店铺首页装修数据
	 * @param shopId 店铺Id
	 * @return
	 */
	public abstract String getReleaseDecorateDataByShopId(Long shopId);

}
