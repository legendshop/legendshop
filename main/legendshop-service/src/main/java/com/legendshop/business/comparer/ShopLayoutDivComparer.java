package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;
import com.legendshop.util.AppUtils;


/**
 * 
 * @author tony
 *
 */
public class ShopLayoutDivComparer implements DataComparer<ShopLayoutDivDto, ShopLayoutDiv> {

	@Override
	public boolean needUpdate(ShopLayoutDivDto dto, ShopLayoutDiv dbObj,
			Object obj) {
		int num=0;
		if(isChage(dto.getLayoutModuleType(),dbObj.getLayoutModuleType())){
			dbObj.setLayoutModuleType(dto.getLayoutModuleType());
			num++;
		}
		if(isChage(dto.getLayoutContent(),dbObj.getLayoutContent())){
			dbObj.setLayoutContent(dto.getLayoutContent());
			num++;
		}
		if(num>0){
			return true;
		}
		
		return false;
	}
	
	/** 是否有改变 **/
	private  boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isExist(ShopLayoutDivDto dto, ShopLayoutDiv dbObj) {
		
		if(dto.getLayoutDivId()== null && dbObj.getLayoutDivId()==null){
			return true;
		}
		if(dto.getLayoutDivId()== null || dbObj.getLayoutDivId()== null){
			return false;
		}
		return  dto.getLayoutDivId().equals(dbObj.getLayoutDivId());
	}

	@Override
	public ShopLayoutDiv copyProperties(ShopLayoutDivDto dtoj, Object obj) {
		ShopLayoutDiv shopLayoutDiv=new ShopLayoutDiv();
		shopLayoutDiv.setLayoutDiv(dtoj.getLayoutDiv());
		shopLayoutDiv.setLayoutDivId(dtoj.getLayoutDivId());
		shopLayoutDiv.setLayoutId(dtoj.getLayoutId());
		shopLayoutDiv.setLayoutType(dtoj.getLayoutType());
		shopLayoutDiv.setShopDecotateId(dtoj.getShopDecotateId());
		shopLayoutDiv.setShopId(dtoj.getShopId());
		return shopLayoutDiv;
	}

}
