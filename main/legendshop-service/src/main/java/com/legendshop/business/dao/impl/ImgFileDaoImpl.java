/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ImgFileDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.util.AppUtils;

/**
 * 获取图片Dao.
 */
@Repository
public class ImgFileDaoImpl extends GenericDaoImpl<ImgFile, Long> implements ImgFileDao {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ImgFileDaoImpl.class);

	@Override
	@CacheEvict(value = "ImgFile", key = "#fileId")
	public void deleteImgFileById(Long fileId) {
		deleteById(fileId);
	}

	@Override
	@CacheEvict(value = "ImgFile", key = "#imgFile.fileId")
	public void updateImgFile(ImgFile imgFile) {
		update(imgFile);
	}

	@Override
	@CacheEvict(value = "ImgFile", key = "#imgFile.fileId")
	public void deleteImgFile(ImgFile imgFile) {
		delete(imgFile);
	}

	@Override
	@Cacheable(value = "ImgFileList", key = "#prodId")
	public List<ImgFile> getAllProductPics( Long prodId) {
		return queryByProperties(new EntityCriterion().eq("productType", 1).eq("productId", prodId).addAscOrder("seq"));
	}

	/**
	 * 更新缓存
	 */
	@Override
	@CacheEvict(value = "ImgFileList", key = "#prodId")
	public void clearProductPicsCache(Long prodId) {
		log.debug("remove cache by prodId {}", prodId);
	}

	/**
	 * 获取商品图片
	 */
	@Override
	public List<String> queryProductPics(Long prodId) {
		List<String> result =  jdbcTemplate.query("SELECT i.file_path AS prodPic FROM ls_auctions a LEFT JOIN ls_img_file i ON a.prod_id=i.product_id WHERE i.product_id=?", new Object[]{prodId}, new RowMapper<String>(){
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("prodPic");
			}
		});
		if(AppUtils.isBlank(result)){
			return null;
		}
		return result;
	}

	@Override
	public PageSupport<ImgFile> getImgFileList(String curPageNO, Long productId, ImgFile imgFile) {
		CriteriaQuery cq = new CriteriaQuery(ImgFile.class, curPageNO);//javascript:imgPager
		cq.setPageSize(6);
		cq.eq("userName", imgFile.getUserName());
		if (productId != null) {
			cq.eq("productId", productId);

		}
		return queryPage(cq);
	}

}
