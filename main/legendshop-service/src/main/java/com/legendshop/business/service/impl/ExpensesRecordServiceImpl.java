/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.spi.service.ExpensesRecordService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 消费记录服务.
 */
@Service("expensesRecordService")
public class ExpensesRecordServiceImpl  implements ExpensesRecordService{

	@Autowired
    private ExpensesRecordDao expensesRecordDao;

    public ExpensesRecord getExpensesRecord(Long id) {
        return expensesRecordDao.getExpensesRecord(id);
    }

    public void deleteExpensesRecord(ExpensesRecord expensesRecord) {
        expensesRecordDao.deleteExpensesRecord(expensesRecord);
    }

    public Long saveExpensesRecord(ExpensesRecord expensesRecord) {
        if (!AppUtils.isBlank(expensesRecord.getId())) {
            updateExpensesRecord(expensesRecord);
            return expensesRecord.getId();
        }
        return expensesRecordDao.saveExpensesRecord(expensesRecord);
    }

    public void updateExpensesRecord(ExpensesRecord expensesRecord) {
        expensesRecordDao.updateExpensesRecord(expensesRecord);
    }

	@Override
	public boolean deleteExpensesRecord(String userId, Long id) {
		return expensesRecordDao.deleteExpensesRecord(userId,id);
	}

	@Override
	public void deleteExpensesRecord(String userId, String ids) {
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		expensesRecordDao.deleteExpensesRecord(userId,idList);
	}

	@Override
	public PageSupport<ExpensesRecord> getExpensesRecordPage(String curPageNO, String userId) {
		return expensesRecordDao.getExpensesRecordPage(curPageNO,userId);
	}

	@Override
	public PageSupport<ExpensesRecord> getExpensesRecord(String curPageNO, String userId, int pageSize) {
		  return expensesRecordDao.getExpensesRecord(curPageNO,userId,pageSize);
	}
}
