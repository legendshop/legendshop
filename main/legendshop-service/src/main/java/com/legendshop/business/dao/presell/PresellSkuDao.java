/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSku;

import java.util.List;

/**
 * The Class PresellSkuDao. 预售活动skuDao接口
 */
public interface PresellSkuDao extends Dao<PresellSku,Long>{

	/**
	 * 根据Id获取预售活动sku
	 */
	public abstract PresellSku getPresellSku(Long id);

	/**
	 *  根据Id删除预售活动sku
	 */
    public abstract int deletePresellSku(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deletePresellSku(PresellSku presellSku);

	/**
	 * 保存预售活动sku
	 */
	public abstract Long savePresellSku(PresellSku presellSku);

	/**
	 *  更新预售活动sku
	 */		
	public abstract int updatePresellSku(PresellSku presellSku);

	/**
	 * 分页查询预售活动sku列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<PresellSku> queryPresellSku(String curPageNO, Integer pageSize);

	/**
	 * 根据预售活动id删除预售商品
	 * @param presellId
	 * @return
	 */
	public abstract void deletePresellSkuByPid(Long presellId);

	/**
	 * 根据活动获取预售sku
	 * @param skuId
	 * @param id
	 * @return
	 */
	PresellSku getPresellSkuByActive(Long skuId, Long id);

	/**
	 * 根据skuId获取预售sku
	 * @param skuId
	 * @return
	 */
	PresellSku getPresellSkuBySkuId(Long presellId,Long skuId);

	/**
	 * 根据活动id获取SkuId集合
	 * @param id
	 * @return
	 */
	List<Long> getSkuIdByPresellId(Long id);
}
