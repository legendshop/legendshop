/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.business.dao.ShopCategoryDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.*;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CateGoryExportDTO;
import com.legendshop.model.dto.ShopCategoryDto;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 *商家的商品类目Dao
 */
@Repository
public class ShopCategoryDaoImpl extends GenericDaoImpl<ShopCategory, Long> implements ShopCategoryDao  {
	
	static String queryByShopIdSql = "SELECT id,parent_id,rec_date,status,shop_id,name,seq,pic FROM ls_shop_cat WHERE shop_id=? AND status=? AND parent_id is null ORDER BY seq asc";

	public ShopCategory getShopCategory(Long id){
		return getById(id);
	}
	
    public int deleteShopCategory(ShopCategory shopCategory){
    	return delete(shopCategory);
    }
	
	public Long saveShopCategory(ShopCategory shopCategory){
		return save(shopCategory);
	}
	
	public int updateShopCategory(ShopCategory shopCategory){
		return update(shopCategory);
	}

	@Override
	public List<ShopCategory> subCatQuery(List<ShopCategory> nextCatList) {
		if(AppUtils.isBlank(nextCatList)){
			return null;
		}
		StringBuffer sb = new StringBuffer("select id as id,parent_id as parentId,name as name,pic as pic,seq as seq,status as status," +
				"shop_id as shopId,rec_date as recDate from  ls_shop_cat where parent_id in( ");
		List<Long> ids = new ArrayList<Long>();
		for (ShopCategory shopCategory : nextCatList) {
			sb.append("?,");
			ids.add(shopCategory.getId());
		}
		sb.setLength(sb.length()-1);
		sb.append(") order by seq asc");
		return query(sb.toString(),ShopCategory.class,ids.toArray());
	}

	@Override
	public List<ShopCategory> queryByParentId(Long nextCatId) {
		return queryByProperties(new EntityCriterion().eq("parentId", nextCatId));
	}

	@Override
	public List<ShopCategory> queryByShopId(Long shopId) {
		return query(queryByShopIdSql,ShopCategory.class,shopId,Constants.ONLINE);
	}

	@Override
	public List<ShopCategory> loadNextCat(Long shopCatId, Long shopId) {
		return queryByProperties(new EntityCriterion().eq("parentId", shopCatId).eq("shopId",shopId).eq("status", Constants.ONLINE).addAscOrder("seq"));
	}
    @Override
    public List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId) {
        String querySQL = ConfigCode.getInstance().getCode("shop.getShopCateGoryExportDTOList");
        return query(querySQL,CateGoryExportDTO.class,shopId);
    }

	@Override
	public List<ShopCategory> getShopCategory(Long shopId, String name) {
		if("".equals(name) || null==name){
			String sql = "select id as id,parent_id as parentId,name as name,pic as pic,seq as seq,status as status,shop_id as shopId,rec_date as recDate from ls_shop_cat where shop_id=?";
			List<ShopCategory> list = this.query(sql, ShopCategory.class, shopId);
			return list;
		}else{
			String sql = "select id as id,parent_id as parentId,name as name,pic as pic,seq as seq,status as status,shop_id as shopId,rec_date as recDate from ls_shop_cat where name like '%"+name +"%' and shop_id=?";
			List<ShopCategory> list = this.query(sql, ShopCategory.class,shopId);
			return list;
		}
		
	}

	@Override
	public Long getNextCategoryCount(Long shopCatId) {
		String sql = "SELECT COUNT(1) FROM ls_shop_cat WHERE parent_id = ? AND STATUS = 1";
		Long count = this.getLongResult(sql, shopCatId);
		return count;
	}

	@Override
	public PageSupport<ShopCategory> queryShopCategory(String curPageNO, ShopCategory shopCategory, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ShopCategory.class, curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(shopCategory.getName())){
			map.put("name", "%"+shopCategory.getName()+"%");
		}
		map.put("shopId", shopId);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryShopCatCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.queryShopCat", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ShopCategory> getNextShopCat(String curPageNO, ShopCategoryDto shopCatDto) {
		CriteriaQuery cq = new CriteriaQuery(ShopCategory.class, curPageNO);
	 	cq.eq("parentId", shopCatDto.getShopCatId());
	 	cq.eq("name", shopCatDto.getName());
	 	cq.setPageSize(10);
	 	cq.addAscOrder("seq");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ShopCategory> getSubShopCat(String curPageNO, ShopCategoryDto shopCatDto) {
		CriteriaQuery cq = new CriteriaQuery(ShopCategory.class, curPageNO);
	 	cq.eq("parentId", shopCatDto.getNextCatId());
	 	cq.eq("name", shopCatDto.getName());
	 	cq.addAscOrder("seq");
		return queryPage(cq);
	}
	@Override
	public List<ShopCategory> getFirstShopCategory(Long shopId) {
		String sql = "SELECT lsc.* FROM ls_shop_cat lsc WHERE lsc.shop_id = ? AND  lsc.parent_id IS NULL";
		return query(sql, ShopCategory.class, shopId);
	}

	@Override
	public List<ShopCategory> getShopCategoryList(Long shopId) {
		return queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("status", 1));
	}

    @Override
    public ShopCategory getShopCategoryByName(Long shopId,Long upCategoryId, String categoryName) {
        if(upCategoryId == null){
            String sql = "SELECT c.* FROM `ls_shop_cat` c WHERE c.`parent_id` IS NULL AND c.`shop_id` = ?  AND c.`name` = ?";
            return get(sql,ShopCategory.class,shopId,categoryName);
        }else{
            String sql = "SELECT c.* FROM `ls_shop_cat` c WHERE c.`shop_id` = ? AND c.`parent_id` = ? AND c.`name` = ?";
            return get(sql,ShopCategory.class,shopId,upCategoryId,categoryName);
        }
    }

    /**
     * 获取店铺分类列表
     */
	@Override
	public List<DecorateShopCategoryDto> getShopCategoryDtoList(Long shopId) {
		
		String sql = "SELECT lsc.id AS id,lsc.name AS name,lsc.parent_id AS parentId,lsc.grade AS grade FROM ls_shop_cat lsc WHERE lsc.shop_id = ? ";
		return query(sql, DecorateShopCategoryDto.class, shopId);
	}

	/**
	 * app 商家端获取 获取店铺分类列表
	 * @param curPageNO
	 * @param shopId
	 */
	@Override
	public PageSupport<ShopCategory> appQueryShopCategory(String curPageNO, Long shopId, Integer grade ,Long parentId) {
		if (AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		CriteriaQuery criteriaQuery=new CriteriaQuery(ShopCategory.class,curPageNO);

		if (AppUtils.isNotBlank(grade)){
			criteriaQuery.eq("grade",grade);
		}
		if (AppUtils.isNotBlank(parentId)){
			criteriaQuery.eq("parentId",parentId);
		}
		criteriaQuery.eq("shopId",shopId);
		criteriaQuery.eq("status",1);
		criteriaQuery.addAscOrder("seq");
		criteriaQuery.setPageSize(10);
		PageSupport<ShopCategory> shopCategoryPageSupport = this.queryPage(criteriaQuery);
		return shopCategoryPageSupport;
	}

	/**
	 * app 商家端获取 获取店铺分类列表
	 * @param shopId
	 */
	@Override
	public List<ShopCategory> appQueryShopCategory(Long shopId, Integer grade ,Long parentId) {
		EntityCriterion entityCriterion = new EntityCriterion();
		if (AppUtils.isNotBlank(grade)){
			entityCriterion.eq("grade",grade);
		}
		if (AppUtils.isNotBlank(parentId)){
			entityCriterion.eq("parentId",parentId);
		}
		entityCriterion.eq("shopId",shopId);
		entityCriterion.eq("status",1);
		entityCriterion.addAscOrder("seq");

		List<ShopCategory> shopCategories = this.queryByProperties(entityCriterion);

		return shopCategories;
	}

	@Override
	public String getShopCategoryName(Long id) {
		String sql = "SELECT lsc.name AS name FROM ls_shop_cat lsc WHERE lsc.id = ? ";
		return get(sql, String.class, id);
	}
}
