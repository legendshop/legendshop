/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.entity.Product;
import com.legendshop.sms.event.processor.SendSiteMessageProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.business.dao.AccusationDao;
import com.legendshop.business.dao.AccusationSubjectDao;
import com.legendshop.business.dao.AccusationTypeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.AccusationDto;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.entity.Accusation;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.model.entity.AccusationType;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;

/**
 * The Class AccusationServiceImpl.
 */
@Service("accusationService")
public class AccusationServiceImpl implements AccusationService {

	@Autowired
	private AccusationDao accusationDao;

	@Autowired
	private AccusationSubjectDao accusationSubjectDao;

	@Autowired
	private AccusationTypeDao accusationTypeDao;
	
	@Autowired
	private ProductService productService;
	
	@Resource(name="productIndexUpdateProcessor")
	private EventProcessor<ProductIdDto> productIndexUpdateProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

    /**
     * 发送站内信
     */
    @Resource(name = "sendSiteMessageProcessor")
    private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;
	
	@Override
	public List<Accusation> getAccusation(String userName) {
		return accusationDao.getAccusation(userName);
	}

	@Override
	public Accusation getAccusation(Long id) {
		return accusationDao.getAccusation(id);
	}

	@Override
	public void deleteAccusation(Accusation accusation) {
		accusationDao.deleteAccusation(accusation);
	}

	@Override
	public Long saveAccusation(Accusation accusation) {
		if (!AppUtils.isBlank(accusation.getId())) {
			updateAccusation(accusation);
			return accusation.getId();
		}
		return accusationDao.saveAccusation(accusation);
	}

	@Override
	public void updateAccusation(Accusation accusation) {
		accusationDao.updateAccusation(accusation);
	}

	@Override
	public List<AccusationSubject> queryAccusationSubject() {
		return accusationSubjectDao.queryAccusationSubject();
	}

	public AccusationSubjectDao getAccusationSubjectDao() {
		return accusationSubjectDao;
	}

	@Override
	public List<AccusationType> queryAccusationType() {
		return accusationTypeDao.queryAccusationType();
	}

	public AccusationTypeDao getAccusationTypeDao() {
		return accusationTypeDao;
	}

	@Override
	public Accusation getDetailedAccusation(Long id) {
		return accusationDao.getDetailedAccusation(id);
	}

	@Override
	public List<KeyValueEntity> AccusationTypeList() {
		List<AccusationType> AccusationTypeList = accusationTypeDao.queryAccusationType();
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>();
		for (AccusationType accusationType : AccusationTypeList) {
			result.add(new KeyValueEntity(String.valueOf(accusationType.getId()), accusationType.getName()));
		}
		return result;
	}

	@Override
	public List<KeyValueEntity> getAccusationSubject(Long typeId) {
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>(1);
		List<AccusationSubject> accusationSubjects = accusationSubjectDao.getSubjectByTypeId(typeId);
		for (AccusationSubject accusationSubject : accusationSubjects) {
			result.add(new KeyValueEntity(String.valueOf(accusationSubject.getAsId()), accusationSubject.getTitle()));
		}
		return result;
	}

	@Override
	public Accusation getAccusationByUserAndProd(String userId, Long prodId) {
		return accusationDao.getByProperties(new EntityCriterion().eq("userId", userId).eq("prodId", prodId));
	}

	@Override
	public void deleteAccusation(List<Accusation> accs) {
		accusationDao.delete(accs);
	}

	/**
	 * 查询尚未处理的投诉
	 */
	@Override
	public List<Accusation> queryUnHandler() {
		return accusationDao.queryUnHandler();
	}

	@Override
	@Cacheable(value = "AccusationCounts")
	public int getAccusationCounts(Long shopId) {
		return accusationDao.getAccusationCounts(shopId);
	}

	@Override
	public PageSupport<Accusation> query(Accusation accusation, String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(Accusation.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(accusation.getUserName())) {			
			map.like("userName", accusation.getUserName().trim());
		}
		if(AppUtils.isNotBlank(accusation.getNickName())) {			
			map.like("nickName", accusation.getNickName().trim());
		}
		if(AppUtils.isNotBlank(accusation.getShopId())) {
			map.put("shopId", accusation.getShopId());			
		}
		if(AppUtils.isNotBlank(accusation.getShopName())) {
			map.like("shopName", accusation.getShopName().trim());			
		}
		map.put("status", accusation.getStatus());
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.queryAccusationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.queryAccusation", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());

		return accusationDao.querySimplePage(query);
	}

	@Override
	public PageSupport<Accusation> queryReportForbit(String curPageNO, Long shopId) {
		return accusationDao.queryReportForbit(curPageNO,shopId);
	}

	@Override
	public PageSupport<Accusation> queryOverlayLoad(Long id) {
		return accusationDao.queryOverlayLoad(id);
	}

	@Override
	public PageSupport<AccusationDto> query(String curPageNO, String userId) {
		return accusationDao.querySimplePage(curPageNO,userId);
	}

	@Override
	public PageSupport<Accusation> query(Long id, String userId) {
		return accusationDao.querySimplePage(id,userId);
	}

	@Override
	public Long saveAccusation(Accusation accusation, Accusation handleAccusation) {
		Long id = this.saveAccusation(handleAccusation);
		// 商品处理
		if (accusation.getIllegalOff() == 1) {
			productService.updateStatus(ProductStatusEnum.PROD_ILLEGAL_OFF.value(), handleAccusation.getProdId(), accusation.getHandleInfo());
			productIndexUpdateProcessor.process(new ProductIdDto(handleAccusation.getProdId()));
            //处理被举报的的商品并且下架发送站内信
            Product prod = productService.getProductById(handleAccusation.getProdId());
            sendSiteMessageProcessor.process(new SendSiteMsgEvent(prod.getUserName(), prod.getUserName(), "违规商品下架", "商品【" + prod.getName() + "】违规下架，违规意见：" + accusation.getHandleInfo()).getSource());
        }


		return id;
	}

}
