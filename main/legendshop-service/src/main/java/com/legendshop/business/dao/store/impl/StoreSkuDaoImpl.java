/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.store.impl;

import com.legendshop.business.dao.store.StoreSkuDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.store.StoreSku;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * The Class StoreSkuDaoImpl.
 */
@Repository
public class StoreSkuDaoImpl extends GenericDaoImpl<StoreSku, Long> implements StoreSkuDao  {
     
	public StoreSku getStoreSku(Long id){
		return getById(id);
	}
	
    public int deleteStoreSku(StoreSku storeSku){
    	return delete(storeSku);
    }
	
	public Long saveStoreSku(StoreSku storeSku){
		return save(storeSku);
	}
	
	public int updateStoreSku(StoreSku storeSku){
		return update(storeSku);
	}

	@Override
	public List<Long> saveStoreSkuBatch(List<StoreSku> storeSkus) {
		return save(storeSkus);
	}

	@Override
	public int updateStoreSkuBatch(List<StoreSku> originStoreSkus) {
		return update(originStoreSkus);
	}

	@Override
	public List<StoreSku> getStoreSkuByIds(List<Long> ids) {
		return queryAllByIds(ids);
	}

	@Override
	public List<StoreSku> getStoreSku(Long storeId, Long prodId) {
		return query("SELECT * FROM ls_store_sku AS ss WHERE ss.store_id = ? AND ss.prod_id = ?", StoreSku.class, storeId,prodId);
	}

	@Override
	public int deleteStoreSku(Long storeId, Long prodId) {
		return update("DELETE FROM ls_store_sku WHERE store_id = ? AND prod_id = ?", storeId,prodId);
	}

	@Override
	public Long getStoreSkuCount(Long storeId, Long prodId) {
		return getLongResult("SELECT count(1) FROM ls_store_sku WHERE store_id = ? AND prod_id = ?", storeId,prodId);
	}

	@Override
	public StoreSku getStoreSkuBySkuId(Long storeId, Long skuId) {
		return get("SELECT * FROM ls_store_sku WHERE store_id = ? AND sku_id = ?", StoreSku.class, storeId, skuId);
	}
 }
