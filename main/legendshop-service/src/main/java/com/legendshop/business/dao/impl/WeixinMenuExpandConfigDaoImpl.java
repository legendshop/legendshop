/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.util.AppUtils;

/**
 *微信菜单扩展Dao
 */
@Repository
public class WeixinMenuExpandConfigDaoImpl extends GenericDaoImpl<WeixinMenuExpandConfig, Long> implements WeixinMenuExpandConfigDao  {
     
    public List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(){
   		return this.queryByProperties(new EntityCriterion());
    }

	public WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id){
		return getById(id);
	}
	
    public int deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
    	return delete(weixinMenuExpandConfig);
    }
	
	public Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
		return save(weixinMenuExpandConfig);
	}
	
	public int updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
		return update(weixinMenuExpandConfig);
	}

	@Override
	public PageSupport<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String curPageNO,
			WeixinMenuExpandConfig weixinMenuExpandConfig) {
		 CriteriaQuery cq = new CriteriaQuery(WeixinMenuExpandConfig.class, curPageNO);
	        if(AppUtils.isNotBlank(weixinMenuExpandConfig.getName())){
	        	cq.like("name", "%"+weixinMenuExpandConfig.getName()+"%");
	        }
	        cq.addDescOrder("createdate");
		return queryPage(cq);
	}
	
 }
