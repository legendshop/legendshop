/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MailEntityDao;
import com.legendshop.business.dao.MailLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.MailInfo;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.MailEntity;
import com.legendshop.model.entity.MailLog;
import com.legendshop.spi.service.MailEntityService;
import com.legendshop.util.AppUtils;


/**
 * 邮件服务.
 */
@Service("mailEntityService")
public class MailEntityServiceImpl  implements MailEntityService{
	
	@Autowired
    private MailEntityDao mailEntityDao;
    
	@Autowired
    private MailLogDao mailLogDao;

	@Override
	public void saveMail(MailInfo task) {
		//call mail service to save db table: ls_mail
		MailEntity mailEntity = new MailEntity();
		Date date =  new Date();
		mailEntity.setMailContent(task.getText());
		mailEntity.setType(task.getType());
		mailEntity.setRecTime(date);
		mailEntity.setSendName(task.getSendName());
		mailEntity.setUserLevel(task.getUserLevel());
		mailEntity.setCategory(task.getCategory());
		mailEntity.setMailCode(task.getMailCode());
		mailEntity.setStatus(1);
		
		Long mailId = mailEntityDao.saveMailEntity(mailEntity);
		
		//call mail service to save db table :ls_mail_log
		MailLog mailLog = new MailLog();
		mailLog.setMailId(mailId);
		mailLog.setReceiveMail(task.getTo());
		mailLog.setSendTime(date);
		mailLog.setReceiveName(task.getRecieveName());
	    mailLogDao.saveMailLog(mailLog);
	}

    public MailEntity getMailEntity(Long id) {
        return mailEntityDao.getMailEntity(id);
    }

    public void deleteMailEntity(MailEntity mailEntity) {
        mailEntityDao.deleteMailEntity(mailEntity);
    }

    public Long saveMailEntity(MailEntity mailEntity) {
        if (!AppUtils.isBlank(mailEntity.getId())) {
            updateMailEntity(mailEntity);
            return mailEntity.getId();
        }
        return (Long) mailEntityDao.save(mailEntity);
    }

    public void updateMailEntity(MailEntity mailEntity) {
        mailEntityDao.updateMailEntity(mailEntity);
    }
	
    @Override
	public MailEntity getMailEntity(String receiveMail, MailCategoryEnum type){
		return mailEntityDao.getMailEntity(receiveMail,type);
	}

	@Override
	public void clearMailCode(Long id) {
		mailEntityDao.clearMailCode(id);
	}

	@Override
	public PageSupport<MailEntity> getMailEntityPage(String curPageNO) {
		return mailEntityDao.getMailEntityPage(curPageNO);
	}
}
