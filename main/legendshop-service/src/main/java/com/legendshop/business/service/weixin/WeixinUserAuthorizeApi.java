package com.legendshop.business.service.weixin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.dto.weixin.WeixinUserToken;
import com.legendshop.model.dto.weixin.WeixinAuthorizeUserInfo;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

/**
 * 微信用户授权认证相关API
 * @author 开发很忙
 */
public class WeixinUserAuthorizeApi {
	
	private static Logger log = LoggerFactory.getLogger(WeixinUserAuthorizeApi.class);

	/** 通过 code 换取 accesstoken */
	private final static String SNS_OAUTH2_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	
	/** 获取用户信息 */
	private final static String SNS_USERINFO = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

	/**
	 * 获取accssToken
	 * @param appId 小程序的APPID
	 * @param appSecret 
	 * @param code 小程序端wx.login()接口获取到的code
	 * @return 
	 */
	public static WeixinUserToken getAccessToken(String appId, String appSecret, String code) {
		
        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = SNS_OAUTH2_ACCESS_TOKEN.replace("APPID", appId)
        		.replace("SECRET", appSecret)
        		.replace("CODE", code);
        
        String text = HttpUtil.httpsRequest(url, "GET", null);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取微信用户 accessToken 失败 ####################");
        	return null;
        }
        
        WeixinUserToken accessToken = JSONObject.parseObject(text, WeixinUserToken.class);

		return accessToken;
	}
	
	/**
	 * 通过accessToken, openId 获取用户信息
	 * @param accessToken
	 * @param openId
	 * @return
	 */
	public static WeixinAuthorizeUserInfo getUserInfo(String accessToken, String openId) {
		
		// 拼接请求地址
		String requestUrl = SNS_USERINFO.replace("ACCESS_TOKEN", accessToken).replace("OPENID", openId);
		
		// 获取用户信息
		 String text = HttpUtil.httpsRequest(requestUrl,"GET", null);
	 	 if(AppUtils.isBlank(text)){
	 		log.info("############## 获取微信用户信息失败 ####################");
        	return null;
	 	 }
	 	 
	 	WeixinAuthorizeUserInfo userInfo = JSONObject.parseObject(text, WeixinAuthorizeUserInfo.class);
	     
		return userInfo;
	}
}
