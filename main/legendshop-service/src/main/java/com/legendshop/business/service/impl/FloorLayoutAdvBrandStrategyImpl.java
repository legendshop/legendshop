package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;


/**
 * //(广告+品牌)
 */
@Service("floorLayoutAdvBrandStrategy")
public class FloorLayoutAdvBrandStrategyImpl implements FloorLayoutStrategy {
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private FloorItemDao floorItemDao;
	
	@Autowired
	private FloorDao floorDao;
	
	@Override
	@Caching(evict={@CacheEvict(value="FloorDto",key="#floor.flId+#floor.layout"),
			@CacheEvict(value="FloorList")})
	public void deleteFloor(Floor floor) {
		
		FloorItem adv = floorItemDao.getAdvFloorItem(floor.getFlId(),FloorTemplateEnum.RIGHT_ADV.value(),FloorLayoutEnum.WIDE_ADV_BRAND.value());
    	if(AppUtils.isNotBlank(adv)){
    		if(AppUtils.isNotBlank(adv.getPicUrl())){
    			//delete  attachment
    			attachmentManager.delAttachmentByFilePath(adv.getPicUrl());
    			
    			//delete file image
    			attachmentManager.deleteTruely(adv.getPicUrl());
    		}
    		
			
    	}
    	floorItemDao.deleteFloorItem(floor.getFlId());
		
     	//最后删除自己本身
        floorDao.deleteFloor(floor);
        
	}

	@Override
	public FloorDto findFloorDto(Floor floor) {
		FloorDto floorDto=new FloorDto();
		floorDto.setCssStyle(floor.getCssStyle());
		floorDto.setFlId(floor.getFlId());
        floorDto.setLayout(floor.getLayout());
        floorDto.setName(floor.getName());
        floorDto.setSeq(floor.getSeq());
        //装载品牌
		List<FloorItem> floorItemList = floorItemDao.getBrandList(floor.getFlId(),FloorLayoutEnum.WIDE_ADV_BRAND.value(),40);
		if(AppUtils.isNotBlank(floorItemList)){
			floorDto.addFloorItems(FloorTemplateEnum.BRAND.value(), floorItemList);
		}
		//装载广告
		List<FloorItem> advList = floorItemDao.queryAdvFloorItem(floor.getFlId(),FloorTemplateEnum.RIGHT_ADV.value(),FloorLayoutEnum.WIDE_ADV_BRAND.value());
    	if(AppUtils.isNotBlank(advList)){
			 floorDto.addFloorItems(FloorTemplateEnum.RIGHT_ADV.value(), advList);
		}
		return floorDto;
	}
	

}
