/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.model.constant.ActivitySearchTypeEnum;
import com.legendshop.model.dto.OperateStatisticsDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.presell.PresellProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.presell.PreSellShopCartItem;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.model.dto.presell.PresellSearchParamDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.util.AppUtils;

/**
 * 预售商品Dao实现类
 */
@Repository
public class PresellProdDaoImpl extends GenericDaoImpl<PresellProd, Long> implements PresellProdDao {

	/**
	 * 获取预售商品 
	 */
	public PresellProd getPresellProd(Long id) {
		return getById(id);
	}

	/**
	 * 删除预售商品 
	 */
	@CacheEvict(value = "PresellProd", key = "#presellProd.prodId")
	public int deletePresellProd(PresellProd presellProd) {
		return delete(presellProd);
	}

	/**
	 * 保存预售商品 
	 */
	public Long savePresellProd(PresellProd presellProd) {
		return save(presellProd);
	}

	/**
	 * 更新预售商品 
	 */
	@CacheEvict(value = "PresellProd", key = "#presellProd.prodId")
	public int updatePresellProd(PresellProd presellProd) {
		return update(presellProd);
	}

	/**
	 * 获取预售商品Dto 
	 */
	@Override
	public PresellProdDto getPresellProdDto(Long id) {
		String sql = ConfigCode.getInstance().getCode("presellProd.common.queryPresellProdDto");
		return this.get(sql, PresellProdDto.class, id);
	}

	/**
	 * 获取预售商品 
	 */
	@Override
	public PresellProd getPresellProd(String schemeName, Long shopId) {
		return this.getByProperties(new EntityCriterion().eq("schemeName", schemeName).eq("shopId", shopId));
	}

	/**
	 * 查询预售商品详情Dto
	 */
	@Override
	public PresellProdDetailDto getPresellProdDetailDto(Long presellId) {
		String sql = ConfigCode.getInstance().getCode("presellProd.user.queryPresellProdDetailDto");
		return this.get(sql, PresellProdDetailDto.class, presellId);
	}

	/**
	 * 获取预售商品 
	 */
	@Override
	//@Cacheable(value = "PresellProd", key = "#prodId + #skuId")
	public PresellProd getPresellProd(Long prodId, Long skuId) {
		return this.getByProperties(new EntityCriterion().eq("prodId", prodId).eq("skuId", skuId).eq("status", PresellProdStatusEnum.ONLINE.value()));
	}

	/**
	 * 根据主键更新预售活动状态 ,
	 * 传入produId和skuId是为了更新缓存
	 * 
	 */
	@Override
	@CacheEvict(value = "PresellProd", key = "#prodId")
	public int updatePresellStatus(Long id,Long prodId, Long status) {
		//状态,-2:未提审,-1:已提审,0:审核通过(上线),1:拒绝,2:已完成
		String sql = "UPDATE  ls_presell_prod  SET status = ?  WHERE id = ?";
		return this.update(sql, status, id);
	}

	/**
	 * 将预售商品转换成购物车对象 
	 */
	@Override
	public PreSellShopCartItem findPreSellCart(Long id,Long skuId) {
		String sql = ConfigCode.getInstance().getCode("presellProd.user.findPreSellCart");
		return load(sql, new Object[] { id,skuId}, new RowMapper<PreSellShopCartItem>() {
			@Override
			public PreSellShopCartItem mapRow(ResultSet rs, int rowNum) throws SQLException {
				PreSellShopCartItem cartItem = new PreSellShopCartItem();

				/** 尾款支付开始时间 */
				cartItem.setPayPctType(rs.getInt("payPctType"));
				cartItem.setPrePrice(rs.getDouble("prePrice"));
				cartItem.setPreDepositPrice(rs.getDouble("preDepositPrice"));
				cartItem.setPreSaleStart(rs.getTimestamp("preSaleStart"));
				cartItem.setPreSaleEnd(rs.getTimestamp("preSaleEnd"));
				cartItem.setFinalMStart(rs.getTimestamp("finalMStart"));
				cartItem.setFinalMEnd(rs.getTimestamp("finalMEnd"));

				cartItem.setProdId(rs.getLong("prodId"));
				cartItem.setSkuId(rs.getLong("skuId"));
				cartItem.setShopId(rs.getLong("shopId"));
				if (rs.getInt("payPctType") == 0) {
					cartItem.setPromotionPrice(rs.getDouble("price"));
					cartItem.setPrice(rs.getDouble("price"));
				} else {
					cartItem.setPromotionPrice(rs.getDouble("prePrice")); // 订金
					cartItem.setPrice(rs.getDouble("prePrice"));
				}
				if (AppUtils.isNotBlank(rs.getString("skuPic"))) {
					cartItem.setPic(rs.getString("skuPic"));
				} else {
					cartItem.setPic(rs.getString("prodPic"));
				}
				if (AppUtils.isNotBlank(rs.getString("skuName"))) {
					cartItem.setProdName(rs.getString("skuName"));
				} else {
					cartItem.setProdName(rs.getString("productName"));
				}
				cartItem.setStatus(rs.getInt("status"));
				cartItem.setStockCounting(rs.getInt("stockCounting"));
				cartItem.setStocks(rs.getInt("stocks"));
				cartItem.setActualStocks(rs.getInt("actualStocks"));
				cartItem.setVolume(rs.getDouble("volume"));
				cartItem.setWeight(rs.getDouble("weight"));
				cartItem.setTransportId(rs.getLong("transportId") == 0 ? null : rs.getLong("transportId"));
				cartItem.setTransportFree(rs.getInt("supportTransportFree"));
				cartItem.setTransportType(rs.getInt("transportType"));
				cartItem.setEmsTransFee(rs.getFloat("emsTransFee"));
				cartItem.setExpressTransFee(rs.getFloat("expressTransFee"));
				cartItem.setMailTransFee(rs.getFloat("mailTransFee"));
				cartItem.setTransportFree(rs.getInt("supportTransportFree"));
				cartItem.setSupportCod(rs.getBoolean("isSupportCod"));
				cartItem.setIsGroup(rs.getInt("isGroup"));
				cartItem.setCnProperties(rs.getString("cnProperties"));
				cartItem.setSupportDist(rs.getInt("supportDist"));
				cartItem.setDistCommisRate(rs.getDouble("distCommisRate"));
				cartItem.setFirstLevelRate(rs.getDouble("firstLevelRate"));
				cartItem.setSecondLevelRate(rs.getDouble("secondLevelRate"));
				cartItem.setThirdLevelRate(rs.getDouble("thirdLevelRate"));
				return cartItem;
			}
		});
	}

	/**
	 * 查询预售商品页面 
	 */
	@Override
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, boolean isExpires, PresellProdDto presellProd) {
		SimpleSqlQuery query = new SimpleSqlQuery(PresellProdDto.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("status", presellProd.getStatus());
		if(AppUtils.isNotBlank(presellProd.getShopId())){			
			map.put("shopId", presellProd.getShopId());
		}
		if(AppUtils.isNotBlank(presellProd.getShopName())){			
			map.like("shopName", presellProd.getShopName().trim());
		}
		if(AppUtils.isNotBlank(presellProd.getSchemeName())) {
			map.like("schemeName", presellProd.getSchemeName().trim());			
		}
		if(AppUtils.isNotBlank(presellProd.getProdCode())) {			
			map.like("prodCode", presellProd.getProdCode().trim());
		}
		/*if (AppUtils.isNotBlank(presellProd.getPreSaleStart())) {
			map.put("preSaleStart",DateUtil.getStartMonment(presellProd.getPreSaleStart()));
		}
		if (AppUtils.isNotBlank(presellProd.getPreSaleEnd())) {
			map.put("preSaleEnd", DateUtil.getEndMonment(presellProd.getPreSaleEnd()));
		}*/
		String timeLimit="";
		if (AppUtils.isNotBlank(presellProd.getStatus()) && 0 == presellProd.getStatus()) {
			// 上线中，把时间过期的去掉
			timeLimit = " AND pp.pre_sale_end > ? ";
			map.put("expired", new Date());
		}
		if(AppUtils.isNotBlank(presellProd.getPreSaleStart())||AppUtils.isNotBlank(presellProd.getPreSaleEnd())){
			if(AppUtils.isNotBlank(presellProd.getPreSaleStart())&&AppUtils.isNotBlank(presellProd.getPreSaleEnd())){
				timeLimit += " AND pp.pre_sale_start<? and pp.pre_sale_end>? ";
				map.put("preSaleEnd", DateUtils.getEndMonment(presellProd.getPreSaleEnd()));
				map.put("preSaleStart", DateUtils.getStartMonment(presellProd.getPreSaleStart()));
			}else if(AppUtils.isNotBlank(presellProd.getPreSaleStart())){
				timeLimit += " AND pp.pre_sale_start>? AND pp.pre_sale_end<? ";
				map.put("preSaleStart", DateUtils.getStartMonment(presellProd.getPreSaleStart()));
				map.put("preSaleEnd", DateUtils.getStartMonment(presellProd.getPreSaleStart()));
			}else{
				timeLimit += " AND pp.pre_sale_start>? AND pp.pre_sale_end<? ";
				map.put("preSaleStart", DateUtils.getStartMonment(presellProd.getPreSaleEnd()));
				map.put("preSaleEnd", DateUtils.getEndMonment(presellProd.getPreSaleEnd()));
			}
		}
		map.put("timeLimit",timeLimit);
		// 查询已过期的预售活动
//		if (isExpires) {
//			map.put("isExpires", " and pp.pre_sale_end <= ?");
//			map.put("endDate", new Date());
//		}
//		else if (AppUtils.isNotBlank(presellProd.getStatus())) {
//			// 过滤已过期
//			map.put("isExpires", " and  pp.pre_sale_end > ?");
//			map.put("endDate", new Date());
//		}
		String querySQL = ConfigCode.getInstance().getCode("presellProd.admin.queryPresellProd", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("presellProd.admin.queryPresellProdCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("isExpires");
		map.remove("timeLimit");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 查询预售商品页面 
	 */
	@Override
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, Long shopId, PresellSearchParamDto paramDto) {
		SimpleSqlQuery query = new SimpleSqlQuery(PresellProdDto.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);

		boolean isFilterExprice = false;// 是否过滤已过期的预售活动

		//根据searchType,组装不同的筛选条件
		String tab =paramDto.getSearchType();
		if (AppUtils.isNotBlank(tab)) {
			Date currDate = new Date();
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			switch (ActivitySearchTypeEnum.matchType(tab)) {
				case WAIT_AUDIT :
					map.put("status", PresellProdStatusEnum.WAIT_AUDIT.value());
					map.put("unFinished", "AND pp.pre_sale_end > "+ "'" +  sd.format(currDate) + "'");
					break;
				case NOT_PASS:
					map.put("status", PresellProdStatusEnum.DENY.value());
					map.put("unFinished", "AND pp.pre_sale_end > "+ "'" +  sd.format(currDate) + "'");
					break;
				case NOT_STARTED :
					map.put("status", PresellProdStatusEnum.ONLINE.value());
					map.put("unStarted", "AND pp.pre_sale_start > "+"'" +  sd.format(currDate) + "'");
					break;
				case ONLINE :
					map.put("status", PresellProdStatusEnum.ONLINE.value());
					map.put("isStarted", "AND pp.pre_sale_start < "+"'" +  sd.format(currDate) + "'");
					map.put("unFinished", "AND pp.pre_sale_end > "+ "'" +  sd.format(currDate) + "'");
					break;
				case FINISHED :
					map.put("isFinished", "AND (pp.pre_sale_end < "+ "'" +  sd.format(currDate) + "' OR pp.status ="+PresellProdStatusEnum.EXPIRED.value()+")");
					break;
				case EXPIRED :
					map.put("status", PresellProdStatusEnum.STOP.value());
					map.put("unFinished", "AND pp.pre_sale_end > "+ "'" +  sd.format(currDate) + "'");
					break;
				default :
			}
		}

		map.like("schemeName", paramDto.getSchemeName());
		map.like("prodName", paramDto.getProdName());
		
//		if (isFilterExprice) {// 过滤已过期
//			map.put("isExpires", " and (pp.pre_sale_start >= ? AND pp.pre_sale_end > ?)");
//			Date nowDate = new Date();
//			map.put("startDate", nowDate);
//			map.put("endDate", nowDate);
//		}
		
		String querySQL = ConfigCode.getInstance().getCode("presellProd.shop.queryPresellProdList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("presellProd.shop.queryPresellProdListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
//		map.remove("timeLimit");
//		map.remove("isExpires");
		map.remove("isStarted");
		map.remove("unStarted");
		map.remove("isFinished");
		map.remove("unFinished");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<PresellProd> queryPresellProdListPage(String curPageNO, ProductSearchParms parms) {
		SimpleSqlQuery query = new SimpleSqlQuery(PresellProdDto.class, 12, curPageNO);
		QueryMap map = new QueryMap();
		
		//商品正常状态前台才展示活动
		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		
		//手机端，根据未开始、进行中、已结束三个状态获取列表
		//pc端没有此字段
		String status = parms.getTag();
		if(AppUtils.isNotBlank(status)){
			if("0".equals(status)){ //未开始
				map.put("readyStartTime", new Date());
			}else if("1".equals(status)){ //进行中
				map.put("startTime", new Date());
				map.put("endTime", new Date());
			}else { //已结束
				map.put("closeEndTime", new Date());
			}
		}
		
		if(AppUtils.isBlank(parms.getOrders())){
			map.put("orders","order by pp.pre_sale_start DESC");
		}else{
			String[] orders = parms.getOrders().split(",");
			if (orders != null && orders.length == 2) {
				map.put("orders", "order by pp." + orders[0] + " " + orders[1]);
			}
		}
		String querySQL = ConfigCode.getInstance().getCode("presellProd.header.queryPresellProd", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("presellProd.header.queryPresellProdCount", map);
		map.remove("orders");
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PresellProd batchDelete(String ids) {
		return null;
	}

	@Override
	public List<PresellProd> getPresellProdByTime(Date now) {
		return queryByProperties(new EntityCriterion().le("preSaleEnd", now).notEq("status", PresellProdStatusEnum.FINISH.value()).notEq("status", PresellProdStatusEnum.STOP.value()));
	}

	/**
	 * 获取审核过程中，未审核或审核拒绝后导致未能正常上线过期的预售活动
	 */
	@Override
	public List<PresellProd> getAuditExpiredPresellProd(Date now) {
		
		//以下状态的秒杀活动过期后都释放商品
        List<Long> statusList = new ArrayList<Long>();
        statusList.add(PresellProdStatusEnum.NOT_PUT_AUDIT.value());
        statusList.add(PresellProdStatusEnum.WAIT_AUDIT.value());
        statusList.add(PresellProdStatusEnum.DENY.value());
		
		return queryByProperties(new EntityCriterion().le("preSaleStart", now).notEq("status", PresellProdStatusEnum.EXPIRED.value()).in("status", statusList.toArray()));
	}

	@Override
	public OperateStatisticsDTO getOperateStatistics(Long id) {
		String sql = "   SELECT COALESCE(SUM(s.actual_total),0) as succeedAmount ,COUNT(1) as succeedCount\n" +
				"  FROM ls_sub s INNER JOIN ls_presell_prod p\n" +
				"  ON s.active_id = p.id \n" +
				"  WHERE s.sub_type = 'PRE_SELL' AND s.status BETWEEN 2 AND 4 AND s.active_id=?";
		return get(sql,OperateStatisticsDTO.class,id);
	}

}
