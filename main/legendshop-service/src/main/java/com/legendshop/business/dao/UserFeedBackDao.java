/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserFeedBack;

/**
 * 用户反馈Dao.
 */
public interface UserFeedBackDao extends Dao<UserFeedBack, Long> {
     
	public abstract UserFeedBack getUserFeedBack(Long id);
	
    public abstract int deleteUserFeedBack(UserFeedBack userFeedBack);
	
	public abstract Long saveUserFeedBack(UserFeedBack userFeedBack);
	
	public abstract int updateUserFeedBack(UserFeedBack userFeedBack);
	
	public abstract PageSupport<UserFeedBack> getUserFeedBackPage(String curPageNO, UserFeedBack userFeedback);
	
 }
