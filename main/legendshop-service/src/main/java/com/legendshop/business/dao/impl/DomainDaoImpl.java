/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DomainDao;
import com.legendshop.dao.GenericJdbcDao;
/**
 * DomainDao实现类
 *
 */
@Repository
public class DomainDaoImpl implements DomainDao {
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	/**
	 * 查询该二级域名是否已经绑定
	 */
	public boolean queryDomainNameBinded(String domainName) {
		return genericJdbcDao.get(" select count(*) from ls_shop_detail where sec_domain_name = ?", Long.class, domainName) > 0;
	}

	/**
	 * 更新二级域名
	 */
	public int updateDomainName(String userId, String domainName,Date registDate) {
		return genericJdbcDao.update("update ls_shop_detail set sec_domain_name = ?,sec_domain_reg_date=? where user_id = ?", domainName,registDate,userId);
	}


}
