/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppStartAdvDto;
import com.legendshop.model.entity.AppStartAdv;

/**
 * APP启动页管理DAO
 */

public interface AppStartAdvDao extends GenericDao<AppStartAdv, Long> {
     
    public abstract List<AppStartAdv> getAppStartAdv(String shopName);

	public abstract AppStartAdv getAppStartAdv(Long id);
	
    public abstract int deleteAppStartAdv(AppStartAdv appStartAdv);
	
	public abstract Long saveAppStartAdv(AppStartAdv appStartAdv);
	
	public abstract int updateAppStartAdv(AppStartAdv appStartAdv);
	
	public abstract PageSupport<AppStartAdv> getAppStartAdv(CriteriaQuery cq);

	public abstract int updateStatus(Long id, Integer status);

	public abstract int getCount(String name);

	public abstract String getName(Long id);

	public abstract PageSupport<AppStartAdv> getAppStartAdvPage(String curPageNO, AppStartAdv appStartAdv);
	
 }
