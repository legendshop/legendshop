/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AdminDashboardDao;
import com.legendshop.model.constant.DashboardTypeEnum;
import com.legendshop.model.entity.AdminDashboard;
import com.legendshop.spi.service.AdminDashboardService;

/**
 * 计算后台首页统计数据
 */
@Service("adminDashboardService")
public class AdminDashboardServiceImpl  implements AdminDashboardService{
	
	@Autowired
    private AdminDashboardDao adminDashboardDao;

    /**
     * 统计数据
     */
	@Override
	public void calAdminDashboard(DashboardTypeEnum typeEnum) {
		adminDashboardDao.calAdminDashboard(typeEnum);
		
	}
	
	@Override
	public  List<AdminDashboard> queryAdminDashboard(){
		return adminDashboardDao.queryAdminDashboard();
	}
	

}
