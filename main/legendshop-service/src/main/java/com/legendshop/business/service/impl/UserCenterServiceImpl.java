/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MyfavoriteDao;
import com.legendshop.business.dao.UserCenterDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Myfavorite;
import com.legendshop.model.entity.User;
import com.legendshop.spi.service.UserCenterService;
import com.legendshop.util.AppUtils;


/**
 * The Class UserCenterServiceImpl.
 */
@Service("userCenterService")
public class UserCenterServiceImpl  implements UserCenterService {
	
	@Autowired
	private UserCenterDao userCenterDao;
	
	@Autowired
	private MyfavoriteDao myfavoriteDao;

	/* (non-Javadoc)
	 * @see com.legendshop.usercenter.service.UserCenterService#deleteFavs(java.lang.String)
	 */
	public void deleteFavs(String userId, String selectedFavs) {
		if (AppUtils.isBlank(selectedFavs)) {
			return;
		}
		String[] favIds = selectedFavs.split(";");
		if(AppUtils.isNotBlank(favIds)){
			myfavoriteDao.deleteFavorite(userId, favIds);
		}
	}

	
	/* (non-Javadoc)
	 * @see com.legendshop.usercenter.service.UserCenterService#deleteAllFavs(java.lang.String)
	 */
	public void deleteAllFavs(String userId) {
		myfavoriteDao.deleteFavoriteByUserId(userId);
	}

	public void saveFavorite(Myfavorite myfavorite) {
		myfavoriteDao.saveFavorite(myfavorite);
	}

	public Boolean isExistsFavorite(Long prodId,String userName) {
		return myfavoriteDao.isExistsFavorite(prodId,userName);
	}

	public User getUser(String userName) {
		return userCenterDao.getUser(userName);
	}

	public Long getfavoriteLength(String userId) {
		return myfavoriteDao.getfavoriteLength(userId);
	}

	@Override
	public PageSupport<Myfavorite> getFavoriteList(String curPageNO,Integer pageSize, String userId) {
		return myfavoriteDao.getFavoriteList(curPageNO,pageSize, userId);
	}

}
