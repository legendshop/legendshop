/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.ShopOrderBill;

import java.util.List;

/**
 *拍卖保证金Dao
 */
public interface AuctionDepositRecDao extends GenericDao<AuctionDepositRec, Long> {
     
	public abstract AuctionDepositRec getAuctionDepositRec(Long id);
	
    public abstract int deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec);
	
	public abstract Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec);
	
	public abstract int updateAuctionDepositRec(AuctionDepositRec auctionDepositRec);

	public abstract boolean isPaySecurity(String userId, Long paimaiId);

	public abstract Long queryAccess(Long paimaiId);

	public abstract List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId);

	public abstract AuctionDepositRec getAucitonRec(Long aId, String userId);

	public abstract AuctionDepositRec getAuctionDepositRecByUserIdAndaId(String userId, Long paimaiId);

	public abstract PageSupport<AuctionDepositRec> queryDepositRecList(String curPageNO, Long id);

	public abstract AuctionDepositRec getAuctionDepositRec(String subNumber, String userId);

	/**
	 * 查询有扣除保证金标识并且未结算的拍卖定金列表
	 * @param shopId
	 * @return
	 */
	List<AuctionDepositRec> getBillAuctionDepositRecList(Long shopId);

	/**
	 * 根据结算当期获取结算的拍卖保证金记录
	 * @param curPageNO
	 * @param shopId
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<AuctionDepositRec> queryAuctionDepositRecList(String curPageNO, Long shopId, ShopOrderBill shopOrderBill);

	/**
	 * 根据订单号获取定金记录
	 * @param subNumber
	 * @return
	 */
	AuctionDepositRec getAuctionDepositRecBySubNumber(String subNumber);
}
