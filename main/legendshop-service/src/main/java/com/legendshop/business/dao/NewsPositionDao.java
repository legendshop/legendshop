/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.entity.NewsPosition;
 


/**
 * The Interface ActivityShopGradeDao.
 */
public interface NewsPositionDao extends GenericDao<NewsPosition, Long>{
	Long saveNewsPosition(NewsPosition p);
	public NewsPosition getNewsPositionById(Long id);
	public  List<NewsDto> getNewsList();
	public  List<NewsPosition> getNewsPositionList();
	public abstract void delete(Long id);
	public abstract void updateNewsPosition(NewsPosition  newsPosition);
	public abstract void saveNewsPositionList(List<NewsPosition>  newsPositionlist);
	public abstract void deleteNewsPositionList(List<NewsPosition>  newsPositionlist);
	List<NewsPosition> getNewsPositionByNewsId(Long newsId);
 }
