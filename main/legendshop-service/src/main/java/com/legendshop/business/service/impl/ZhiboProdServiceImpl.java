/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ZhiboProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.zhibo.ZhiboRoomMemberDto;
import com.legendshop.model.dto.zhibo.ZhiboRoomProdDto;
import com.legendshop.model.entity.ZhiboProd;
import com.legendshop.spi.service.ZhiboProdService;
import com.legendshop.util.AppUtils;

/**
 * The Class ZhiboProdServiceImpl.
 *  服务实现类
 */
@Service("zhiboProdService")
public class ZhiboProdServiceImpl  implements ZhiboProdService{
   
    @Autowired
    private ZhiboProdDao zhiboProdDao;

   	/**
	 *  根据Id获取
	 */
    public ZhiboProd getZhiboProd(Long id) {
        return zhiboProdDao.getZhiboProd(id);
    }

   /**
	 *  删除
	 */ 
    public void deleteZhiboProd(ZhiboProd zhiboProd) {
        zhiboProdDao.deleteZhiboProd(zhiboProd);
    }

   /**
	 *  保存
	 */	    
    public Long saveZhiboProd(ZhiboProd zhiboProd) {
        if (!AppUtils.isBlank(zhiboProd.getId())) {
            updateZhiboProd(zhiboProd);
            return zhiboProd.getId();
        }
        return zhiboProdDao.saveZhiboProd(zhiboProd);
    }

   /**
	 *  更新
	 */	
    public void updateZhiboProd(ZhiboProd zhiboProd) {
        zhiboProdDao.updateZhiboProd(zhiboProd);
    }

    public List<Long> updateAvRoomProd(Long avRoomId,Long[] prodId){
    	return zhiboProdDao.updateAvRoomProd(avRoomId, prodId);
    }
    
	@Override
	public PageSupport<ZhiboRoomProdDto> queryZhiboRoomProd(String curPageNO, String avRoomId, String prodName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ZhiboRoomProdDto.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(avRoomId)){
			map.put("av_room_id", avRoomId);
		}
		if(AppUtils.isNotBlank(prodName)){
			map.put("name", prodName);
		}
		String querySQL = ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomProd", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomProdCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		
		return zhiboProdDao.querySimplePage(query);
	}

	@Override
	public PageSupport<ZhiboRoomMemberDto> queryZhiboRoomProdq(String curPageNO, String avRoomId, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ZhiboRoomMemberDto.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(avRoomId)){
			map.like("av_room_id", avRoomId.trim());
		}
		if(AppUtils.isNotBlank(userName)){
			map.like("name", userName.trim());
		}
		String querySQL = ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomMember", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomMemberCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return zhiboProdDao.querySimplePage(query);
	}
}
