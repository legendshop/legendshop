/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.LimitationException;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.business.dao.AdvertisementDao;
import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.entity.Advertisement;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.spi.service.AdvertisementService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 广告服务.
 */
@Service("advertisementService")
public class AdvertisementServiceImpl  implements AdvertisementService {

	@Autowired
	private AdvertisementDao advertisementDao;
	
	@Autowired
	private FloorItemDao floorItemDao;

	@Autowired
	private FloorDao floorDao;
	
	@Autowired
	private AttachmentManager attachmentManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.AdvertisementService#load(java.lang.Long)
	 */
	@Override
	public Advertisement getAdvertisement(Long id) {
		return advertisementDao.getAdvertisementById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.AdvertisementService#isMaxNum(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public boolean isMaxNum(Long shopId, String type) {
		return advertisementDao.isMaxNum(shopId, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.AdvertisementService#delete(java.lang
	 * .Long)
	 */
	@Override
	@CacheEvict(value = "Advertisement", key = "#id")
	public void delete(Long id) {
		advertisementDao.deleteAdvById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.AdvertisementService#save(com.legendshop
	 * .model.entity.Advertisement)
	 */
	@Override
	public Long save(Advertisement advertisement) {
		if (!AppUtils.isBlank(advertisement.getId())) {
			update(advertisement);
			return advertisement.getId();
		}
		return (Long) advertisementDao.save(advertisement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.AdvertisementService#update(com.legendshop
	 * .model.entity.Advertisement)
	 */
	@Override
	public void update(Advertisement advertisement) {
		advertisementDao.updateAdv(advertisement);
	}

	/**
	 * 更新广告位
	 */
	@Override
	public String updateAdvt(Advertisement advertisement,Advertisement orin,String originPic,String picUrl,String userName, String userId, Long shopId) {
		if(AppUtils.isNotBlank(advertisement.getFile()) && advertisement.getFile().getSize()>0){
			
			//delete  attachment
			attachmentManager.delAttachmentByFilePath(originPic);
			
			//delete file image
			attachmentManager.deleteTruely(originPic);
			
		    //重新保存附件信息
			//保存附件表
			Long attmntId = attachmentManager.saveImageAttachment(userName, userId,shopId, picUrl, advertisement.getFile(), AttachmentTypeEnum.ADVERTISEMENT);
			
			//重新修改 FloorItem 对应的广告信息
			FloorItem floorAdv = new FloorItem();
			floorAdv.setId(advertisement.getFiId()); //自身的子楼层元素ID
			floorAdv.setAttachmentId(attmntId); //附件ID
			floorAdv.setPic(advertisement.getPicUrl()); //图片URL
	    	floorAdv.setReferId(orin.getId()); //广告对象引用ID
	    	floorItemDao.updateFloorItemForAdv(floorAdv);
	    	
		}
		//update advt;
		orin.setPicUrl(picUrl);
		orin.setLinkUrl(advertisement.getLinkUrl());
		orin.setTitle(advertisement.getTitle());
		advertisementDao.updateAdv(orin);

		Floor floor=floorDao.getById(advertisement.getFloorId());
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		return "success";
	}

	
	@Override
	public String saveAdvt(Advertisement advertisement,String pic, String userName, String userId, Long shopId) {
		
		advertisement.setEnabled(1);
		String advType=FloorTemplateEnum.RIGHT_ADV.value();
		if(AppUtils.isNotBlank(advertisement.getType())){
			advType = advertisement.getType();
		}
		advertisement.setLayout(advertisement.getLayout());
		advertisement.setPicUrl(pic);
		Long advtId=advertisementDao.save(advertisement); //保存广告信息 <---> floor_Item[子楼层]
		
		
		//保存附件表
		Long attmntId = attachmentManager.saveImageAttachment(userName,userId, shopId, pic, advertisement.getFile(), AttachmentTypeEnum.ADVERTISEMENT);
		
		//upload  FloorItem 
		FloorItem floorAdv = new FloorItem();
    	floorAdv.setAttachmentId(attmntId);
    	floorAdv.setRecDate(new Date());
    	floorAdv.setPic(advertisement.getPicUrl());
    	floorAdv.setReferId(advtId);
    	floorAdv.setFiId(advertisement.getFiId());
    	floorAdv.setFloorId(advertisement.getFloorId());
    	floorAdv.setLayout(advertisement.getLayout());
    	floorAdv.setType(advType);
    	floorItemDao.saveFloorItem(floorAdv);
    	
    	Floor floor=floorDao.getById(advertisement.getFloorId());
		if(floor!=null && floor.getStatus().intValue()==1){
			floorDao.updateFloorCache(floor);
		}
		
    	return "success";
	}
	
	@Override
	public PageSupport<Advertisement> queryAdvertisement(String curPageNO, Advertisement advertisement) {
		return advertisementDao.queryAdvertisement(curPageNO, advertisement);
	}

	@Override
	public void delete(Long id, Advertisement advertisement) {
		advertisementDao.deleteAdvById(id);
	}

	@Override
	public void saveOrUpdate(String userName, String userId,Long shopId, Advertisement advertisement,String picUrl) {
		Advertisement origin = null;
		if ((advertisement != null) && (advertisement.getId() != null)) { // update
			origin = getAdvertisement(advertisement.getId());
			if (origin == null) {
				throw new NotFoundException("Origin Advertisement is NULL");
			}
			String originPicUrl = origin.getPicUrl();
			origin.setLinkUrl(advertisement.getLinkUrl());
			origin.setType(advertisement.getType());
			origin.setSourceInput(advertisement.getSourceInput());
			origin.setEnabled(advertisement.getEnabled());
			origin.setTitle(advertisement.getTitle());
			if (advertisement.getFile().getSize() > 0) {
				// 保存附件表
				Long attmntId = attachmentManager.saveImageAttachment(userName, userId,shopId, picUrl, advertisement.getFile(), AttachmentTypeEnum.ADVERTISEMENT);
				origin.setPicUrl(picUrl);
			}
			update(origin);

		} else {
			// check it first
			if (!isMaxNum(shopId, advertisement.getType())) {
				throw new LimitationException("您已经达到广告上限，不能再增加");
			}
			if (advertisement.getFile().getSize() > 0) {
				advertisement.setPicUrl(picUrl);
			}
			Long advId = save(advertisement);
			if (AppUtils.isNotBlank(picUrl)) {
				// 保存附件表
				Long attmntId = attachmentManager.saveImageAttachment(userName, userId,shopId, picUrl, advertisement.getFile(), AttachmentTypeEnum.ADVERTISEMENT);
			}
		}
	}
	
	
	
	

}
