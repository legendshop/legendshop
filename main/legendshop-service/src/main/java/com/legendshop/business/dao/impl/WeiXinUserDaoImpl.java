package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeiXinUserDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

/**
 *微信关注用户
 */
@Repository
public class WeiXinUserDaoImpl extends GenericDaoImpl<WeixinGzuserInfo, Long>  implements WeiXinUserDao {

	@Override
	public WeixinGzuserInfo getGzuserInfo(String openId) {
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("openid", openId));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
	}

	@Override
	public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
		return save(weixinGzuserInfo);
	}

}
