/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.*;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.*;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CateGoryExportDTO;
import com.legendshop.model.dto.ProdExportDTO;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.dto.SkuExportDTO;
import com.legendshop.model.dto.order.TenderProdExcelDTO;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.spi.service.ProductImportService;
import com.legendshop.util.AppUtils;

import javax.annotation.Resource;

/**
 * 商品导入Service实现.
 */
@Service("productImportService")
public class ProductImportServiceImpl  implements ProductImportService{
	
	@Autowired
    private ProductImportDao productImportDao;

    @Resource(name="productIndexSaveProcessor")
    private EventProcessor<ProductIdDto> productIndexSaveProcessor;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ShopCategoryDao shopCategoryDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ImgFileDao imgFileDao;

    @Autowired
    private SkuDao skuDao;

    @Autowired
    private BasketService basketService;

   /* @Autowired
    private ProductSearchClient productSearchClient;
*/

    /**
     * 批量保存
     */
	@Override
	public void saveProductImport(List<ProductImport> importList) {
		productImportDao.saveProductImport(importList);
		
	}
    @Override
    public List<CateGoryExportDTO> getCateGoryExportDTOList() {
        return categoryDao.getCateGoryExportDTOList();
    }

    @Override
    public List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId) {
        return shopCategoryDao.getShopCateGoryExportDTOList(shopId);
    }

    /** 保存导入的商品
     * @return*/
    @Override
    public void saveImportProduct(String userId, String userName, Long shopId, ProdExportDTO prodExportDTO) {
        Boolean flag = checkImportProductDate(prodExportDTO);
        if(flag){
            //保存商品数据
            productService.saveImportProduct(userId,userName,shopId,prodExportDTO);
        }else{
            throw new BusinessException("商品数据异常");
        }
    }

    public ProductImport getProductImport(Long id) {
        return productImportDao.getProductImport(id);
    }

    public void deleteProductImport(ProductImport productImport) {
        productImportDao.deleteProductImport(productImport);
    }

    public Long saveProductImport(ProductImport productImport) {
        if (!AppUtils.isBlank(productImport.getId())) {
            updateProductImport(productImport);
            return productImport.getId();
        }
        return productImportDao.saveProductImport(productImport);
    }

    public void updateProductImport(ProductImport productImport) {
        productImportDao.updateProductImport(productImport);
    }

	@Override
	public void deleteProductImport(List<Long> ids) {
		productImportDao.deleyeProductImport(ids);
		
	}

	@Override
	public void deleteProductImportById(Long impId) {
		productImportDao.deleteProductImportById(impId);
	}

	@Override
	public PageSupport<ProductImport> getProductImportPage(String curPageNO, ProductImport productImport, Long shopId) {
		return productImportDao.getProductImportPage(curPageNO,productImport,shopId);
	}
    @Override
    public List<TenderProdExcelDTO> saveImportTenderProduct(String userId, String userName, Long shopId, List<TenderProdExcelDTO> tenderProdExcelDTOList) {
        List<TenderProdExcelDTO> errorList = new ArrayList<TenderProdExcelDTO>();
        for (int i = 0; i < tenderProdExcelDTOList.size() ; i++) {
            TenderProdExcelDTO tenderProdExcelDTO = tenderProdExcelDTOList.get(i);
            String spuIdStr = tenderProdExcelDTO.getSpuId();
            String skuIdStr = tenderProdExcelDTO.getSkuId();
            //修改spu信息
            if(AppUtils.isNotBlank(spuIdStr)){
                try {
                    Product product = productDao.getProduct(Long.parseLong(spuIdStr));
                    //校验商品的数据
                    Boolean flag = validateTenderProdDate(tenderProdExcelDTO,0);
                    if(product == null || !flag){
                        errorList.add(tenderProdExcelDTO);
                        continue;
                    }
                    //修改prod信息
                    product.setName(tenderProdExcelDTO.getProdName().trim());
                    //店铺分类
                    Long shopFirstCategoryId = null;
                    Long shopSecondCategoryId = null;
                    Long shopThirdCategoryId = null;
                    if(AppUtils.isNotBlank(tenderProdExcelDTO.getShopCategroy())){
                        String shopCategroy = tenderProdExcelDTO.getShopCategroy().trim();
                        saveShopCategory(shopId,shopCategroy,shopFirstCategoryId,shopSecondCategoryId,shopThirdCategoryId);
                    }
                    if(shopFirstCategoryId != null && shopSecondCategoryId != null && shopThirdCategoryId != null){
                        product.setShopFirstCatId(shopFirstCategoryId);
                        product.setShopSecondCatId(shopSecondCategoryId);
                        product.setShopThirdCatId(shopThirdCategoryId);
                    }
                    //组装商品自定义参数
                    copyProdParameter(product,tenderProdExcelDTO);
                    product.setCash(Double.parseDouble(tenderProdExcelDTO.getSpuPrice().trim()));
                    //付款方式:1货到付款 2:款到发货 3:预付款
                   /* if(tenderProdExcelDTO.getPayType().trim().equals("1")){
                        product.setPaymentMethod(PayMannerEnum.DELIVERY_ON_PAY.value());
                        product.setAdvancePayment(null);
                    }else if(tenderProdExcelDTO.getPayType().trim().equals("2")){
                        product.setPaymentMethod(PayMannerEnum.ONLINE_PAY.value());
                        product.setAdvancePayment(null);
                    }else if(tenderProdExcelDTO.getPayType().trim().equals("3")){
                        if(product.getPaymentMethod() == null || !product.getPaymentMethod().equals(3)){
                            throw new BusinessException("excel维护中标信息不能修改为预付款方式");
                        }
                    }*/
                    //保存商品图片
                   /* if(AppUtils.isNotBlank(tenderProdExcelDTO.getPicture())){
                        String filePath = tenderProdExcelDTO.getPicture().trim();
                        product.setPic(filePath);
                        ImgFile imgFile = new ImgFile();
                        imgFile.setFilePath(filePath);
                        imgFile.setProductId(product.getProdId());
                        imgFile.setFileSize(null);
                        imgFile.setFileType(filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase());
                        imgFile.setProductType((short) 1);
                        imgFile.setStatus(Constants.ONLINE);
                        imgFile.setUpoadTime(new Date());
                        imgFile.setUserName(userName);
                        imgFile.setSeq(1);
                        imgFile.setShopId(shopId);
                        imgFileDao.save(imgFile);
                    }*/
                    product.setContent(tenderProdExcelDTO.getContent());
                    productDao.update(product);
                } catch (NumberFormatException e) {
                    errorList.add(tenderProdExcelDTO);
                    continue;
                } catch (BusinessException e) {
                    errorList.add(tenderProdExcelDTO);
                    continue;
                }
            }
            //修改sku信息
            if(AppUtils.isNotBlank(skuIdStr)){
                Sku sku = skuDao.getSku(Long.parseLong(skuIdStr.trim()));
                Boolean flag = validateTenderProdDate(tenderProdExcelDTO, 1);
                if(sku == null || !flag){
                    errorList.add(tenderProdExcelDTO);
                    continue;
                }
                try {
                    //修改sku信息
                    sku.setName(tenderProdExcelDTO.getSkuName().trim());
                    sku.setPrice(Double.parseDouble(tenderProdExcelDTO.getSkuPrice().trim()));

                   /* if(AppUtils.isNotBlank(tenderProdExcelDTO.getWeight())){
                        sku.setWeight(Double.parseDouble(tenderProdExcelDTO.getWeight().trim()));
                    }*/
                    if(AppUtils.isNotBlank(tenderProdExcelDTO.getPartyCode())){
                        sku.setPartyCode(tenderProdExcelDTO.getPartyCode().trim());
                    }
                    if(AppUtils.isNotBlank(tenderProdExcelDTO.getModelId())){
                        sku.setModelId(tenderProdExcelDTO.getModelId().trim());
                    }
                    //库存
                    Long stocks = sku.getStocks();
                    Long new_stock = Long.parseLong(tenderProdExcelDTO.getStocks().trim());
                    if(stocks == null){
                        sku.setStocks(new_stock);
                        sku.setActualStocks(new_stock);
                    }else{
                        Long diff = stocks - new_stock;
                        skuDao.updateSkuStocks(sku.getSkuId(),new_stock);
                        skuDao.updateSkuActualStocks(sku.getSkuId(),new_stock);
                    }
                    skuDao.updateSku(sku);
                } catch (NumberFormatException e) {
                    errorList.add(tenderProdExcelDTO);
                    e.printStackTrace();
                }
            }
        }
        tenderProdExcelDTOList.removeAll(errorList);
        if(tenderProdExcelDTOList.size() > 0){
            for (TenderProdExcelDTO  tenderProd : tenderProdExcelDTOList) {
                if(AppUtils.isNotBlank(tenderProd.getSpuId())){
                    Long prodId = Long.parseLong(tenderProd.getSpuId().trim());
                    //更改用户购物车的商品为失效
                    basketService.updateIsFailureByProdId(true, prodId);
                    //更新缓存
                    productDao.clearProductDto(prodId);
                    //更新商品索引
                    //productSearchClient.initProdByProdIdIndexData(prodId);
                }
            }
        }
        return errorList;
    }
    /** 校验(只是校验空值) type 0:spu 1:sku */
    private Boolean validateTenderProdDate(TenderProdExcelDTO tenderProdExcelDTO,int type) {
        if(type == 0){
            if(AppUtils.isBlank(tenderProdExcelDTO.getProdName()) || AppUtils.isBlank(tenderProdExcelDTO.getSpuPrice())){
                return false;
            }
        }else{
            if(AppUtils.isBlank(tenderProdExcelDTO.getSkuName()) || AppUtils.isBlank(tenderProdExcelDTO.getSkuPrice()) || AppUtils.isBlank(tenderProdExcelDTO.getStocks())
                    || AppUtils.isBlank(tenderProdExcelDTO.getStatus()) || !(tenderProdExcelDTO.getStatus().trim().equals("Y") || tenderProdExcelDTO.getStatus().trim().equals("N"))){
                return false;
            }
        }
        return true;
    }
    /** 保存店铺分类 */
    private void saveShopCategory(Long shopId, String shopCategroy, Long shopFirstCategoryId, Long shopSecondCategoryId, Long shopThirdCategoryId) {
        String[] split = shopCategroy.split("\\|");
        if (split.length > 3) {
            throw new BusinessException("店铺分类不能三级");
        }
        //上一店铺分类级别;
        Long upLevel = null;
        //父类分类id
        Long upCategoryId = null;
        Date now = new Date();
        for (int i = 0; i < split.length; i++) {
            if(AppUtils.isBlank(split[i])){
                throw new BusinessException("店铺分类填写错误");
            }
            Long shopCategoryId = null;
            String categoryName = split[i].trim();
            ShopCategory shopCategory = shopCategoryDao.getShopCategoryByName(shopId, upCategoryId, categoryName);
            if (shopCategory == null) {
                //创建店铺分类
                ShopCategory newShopCategory = new ShopCategory();
                newShopCategory.setParentId(upCategoryId);
                newShopCategory.setName(categoryName);
                newShopCategory.setStatus(1);
                newShopCategory.setShopId(shopId);
                newShopCategory.setRecDate(now);
                shopCategoryId = shopCategoryDao.save(newShopCategory);
                upCategoryId = shopCategoryId;

            } else {
                shopCategoryId = shopCategory.getId();
                upCategoryId = shopCategoryId;
            }
            if (i == 0) {
                shopFirstCategoryId = shopCategoryId;
            }
            if (i == 1) {
                shopSecondCategoryId = shopCategoryId;
            }
            if (i == 2) {
                shopThirdCategoryId = shopCategoryId;
            }
        }
    }
    /** 组装商品自定义参数 */
    private void copyProdParameter(Product product, TenderProdExcelDTO tenderProdExcelDTO) {
        List<Map<String, String>> userParameterList = new ArrayList<Map<String, String>>();
        //参数
        if (AppUtils.isNotBlank(tenderProdExcelDTO.getFirstParameter()) && AppUtils.isNotBlank(tenderProdExcelDTO.getFirstParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", tenderProdExcelDTO.getFirstParameter());
            map.put("value", tenderProdExcelDTO.getFirstParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(tenderProdExcelDTO.getSecondParameter()) && AppUtils.isNotBlank(tenderProdExcelDTO.getSecondParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", tenderProdExcelDTO.getSecondParameter());
            map.put("value", tenderProdExcelDTO.getSecondParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(tenderProdExcelDTO.getThirdParameter()) && AppUtils.isNotBlank(tenderProdExcelDTO.getThirdParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", tenderProdExcelDTO.getThirdParameter());
            map.put("value", tenderProdExcelDTO.getThirdParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(tenderProdExcelDTO.getFourthParameter()) && AppUtils.isNotBlank(tenderProdExcelDTO.getFourthParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", tenderProdExcelDTO.getFourthParameter());
            map.put("value", tenderProdExcelDTO.getFourthParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(tenderProdExcelDTO.getFifthParameter()) && AppUtils.isNotBlank(tenderProdExcelDTO.getFifthParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", tenderProdExcelDTO.getFifthParameter());
            map.put("value", tenderProdExcelDTO.getFifthParameterValue());
            userParameterList.add(map);
        }
        String userParameterJson = com.legendshop.util.JSONUtil.getJson(userParameterList);
        product.setUserParameter(userParameterJson);
    }
    /** 校验导入的数据(只是校验空值) */
    private Boolean checkImportProductDate(ProdExportDTO prodExportDTO) {
        //校验spu信息
        if((prodExportDTO == null || AppUtils.isBlank(prodExportDTO.getProdName())|| AppUtils.isBlank(prodExportDTO.getCategroyId())
                 || AppUtils.isBlank(prodExportDTO.getSpuPrice())
                || prodExportDTO.getSkuList() == null || prodExportDTO.getSkuList().size() == 0)){
            return false;
        }else{
            //校验sku
            List<SkuExportDTO> skuList = prodExportDTO.getSkuList();
            for (SkuExportDTO skuExportDTO : skuList) {
                if(AppUtils.isBlank(skuExportDTO.getSkuName()) || AppUtils.isBlank(skuExportDTO.getSkuPrice()) || AppUtils.isBlank(skuExportDTO.getStocks())
                        || AppUtils.isBlank(skuExportDTO.getStatus())){
                    return false;
                }
            }
        }
        return true;
    }

}
