/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.presell;

import com.legendshop.business.dao.presell.PresellSkuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSku;
import com.legendshop.spi.service.PresellSkuService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The Class PresellSkuServiceImpl.
 *  预售活动sku服务实现类
 */
@Service("presellSkuService")
public class PresellSkuServiceImpl implements PresellSkuService {

    /**
     *
     * 引用的预售活动skuDao接口
     */
    @Autowired
    private PresellSkuDao presellSkuDao;
   
   	/**
	 *  根据Id获取预售活动sku
	 */
    public PresellSku getPresellSku(Long id) {
        return presellSkuDao.getPresellSku(id);
    }
    
    /**
	 *  根据Id删除预售活动sku
	 */
    public int deletePresellSku(Long id){
    	return presellSkuDao.deletePresellSku(id);
    }

   /**
	 *  删除预售活动sku
	 */ 
    public int deletePresellSku(PresellSku presellSku) {
       return  presellSkuDao.deletePresellSku(presellSku);
    }

   /**
	 *  保存预售活动sku
	 */	    
    public Long savePresellSku(PresellSku presellSku) {
        if (!AppUtils.isBlank(presellSku.getId())) {
            updatePresellSku(presellSku);
            return presellSku.getId();
        }
        return presellSkuDao.savePresellSku(presellSku);
    }

   /**
	 *  更新预售活动sku
	 */	
    public void updatePresellSku(PresellSku presellSku) {
        presellSkuDao.updatePresellSku(presellSku);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<PresellSku> queryPresellSku(String curPageNO, Integer pageSize){
     	return presellSkuDao.queryPresellSku(curPageNO, pageSize);
    }

    @Override
    public void deletePresellSkuByPid(Long presellId) {
        presellSkuDao.deletePresellSkuByPid(presellId);
    }

	@Override
	public PresellSku getPresellSkuBySkuId(Long presellId,Long skuId) {
		return presellSkuDao.getPresellSkuBySkuId(presellId,skuId);
	}

	@Override
	public List<Long> getSkuIdByPresellId(Long id) {
		return  presellSkuDao.getSkuIdByPresellId(id);
	}

	/**
	 *  设置Dao实现类
	 */	    
    public void setPresellSkuDao(PresellSkuDao presellSkuDao) {
        this.presellSkuDao = presellSkuDao;
    }
    
}
