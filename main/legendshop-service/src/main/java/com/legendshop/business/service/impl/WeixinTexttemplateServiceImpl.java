/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinTexttemplateDao;
import com.legendshop.model.entity.weixin.WeixinTexttemplate;
import com.legendshop.spi.service.WeixinTexttemplateService;
import com.legendshop.util.AppUtils;

/**
 * 微信文字模板服务
 */
@Service("weixinTexttemplateService")
public class WeixinTexttemplateServiceImpl  implements WeixinTexttemplateService{

	@Autowired
    private WeixinTexttemplateDao weixinTexttemplateDao;

    public WeixinTexttemplate getWeixinTexttemplate(Long id) {
        return weixinTexttemplateDao.getWeixinTexttemplate(id);
    }

    public void deleteWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate) {
        weixinTexttemplateDao.deleteWeixinTexttemplate(weixinTexttemplate);
    }

    public Long saveWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate) {
        if (!AppUtils.isBlank(weixinTexttemplate.getId())) {
            updateWeixinTexttemplate(weixinTexttemplate);
            return weixinTexttemplate.getId();
        }
        return weixinTexttemplateDao.saveWeixinTexttemplate(weixinTexttemplate);
    }

    public void updateWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate) {
        weixinTexttemplateDao.updateWeixinTexttemplate(weixinTexttemplate);
    }

}
