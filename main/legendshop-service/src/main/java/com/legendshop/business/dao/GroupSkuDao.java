/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GroupSku;

/**
 * The Class GroupSkuDao. 团购活动参加商品Dao接口
 */
public interface GroupSkuDao extends Dao<GroupSku,Long>{

	/**
	 * 根据Id获取团购活动参加商品
	 */
	public abstract GroupSku getGroupSku(Long id);

	/**
	 *  根据Id删除团购活动参加商品
	 */
    public abstract int deleteGroupSku(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGroupSku(GroupSku groupSku);

	/**
	 * 保存团购活动参加商品
	 */
	public abstract Long saveGroupSku(GroupSku groupSku);

	/**
	 *  更新团购活动参加商品
	 */		
	public abstract int updateGroupSku(GroupSku groupSku);

	/**
	 * 分页查询团购活动参加商品列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GroupSku> queryGroupSku(String curPageNO, Integer pageSize);


	/**
	 * 根据活动删除商品
	 * @param id
	 */
	void deleteGroupSkuByGroupId(Long id);

	/**
	 * 获取团购价格
	 * @param id
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	Double getGropPrice(Long id, Long prodId, Long skuId);
}
