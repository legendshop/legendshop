/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubRefundReturnDetailDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.SubRefundReturnDetail;

/**
 *退款明细Dao
 */
@Repository
public class SubRefundReturnDetailDaoImpl extends GenericDaoImpl<SubRefundReturnDetail, Long> implements SubRefundReturnDetailDao {

	/**
	 * 根据Id获取
	 */
	public SubRefundReturnDetail getSubRefundReturnDetail(Long id) {
		return getById(id);
	}

	/**
	 * 删除
	 */
	public int deleteSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		return delete(subRefundReturnDetail);
	}

	/**
	 * 保存
	 */
	public Long saveSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		return save(subRefundReturnDetail);
	}

	/**
	 * 更新
	 */
	public int updateSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		return update(subRefundReturnDetail);
	}

	@Override
	public List<SubRefundReturnDetail> getSubRefundReturnDetailByRefundId(Long refundId) {
		return queryByProperties(new EntityCriterion().eq("refundId", refundId));
	}

}
