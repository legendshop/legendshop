/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PropValueAliasDao;
import com.legendshop.model.entity.PropValueAlias;
import com.legendshop.spi.service.PropValueAliasService;
import com.legendshop.util.AppUtils;

/**
 *商品属性值别名服务
 */
@Service("propValueAliasService")
public class PropValueAliasServiceImpl  implements PropValueAliasService{
	
	@Autowired
    private PropValueAliasDao propValueAliasDao;

    public PropValueAlias getPropValueAlias(Long id) {
        return propValueAliasDao.getPropValueAlias(id);
    }

    public void deletePropValueAlias(PropValueAlias propValueAlias) {
        propValueAliasDao.deletePropValueAlias(propValueAlias);
    }

    public Long savePropValueAlias(PropValueAlias propValueAlias) {
        if (!AppUtils.isBlank(propValueAlias.getId())) {
            updatePropValueAlias(propValueAlias);
            return propValueAlias.getId();
        }
        return propValueAliasDao.savePropValueAlias(propValueAlias);
    }

    public void updatePropValueAlias(PropValueAlias propValueAlias) {
        propValueAliasDao.updatePropValueAlias(propValueAlias);
    }
}
