/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.IndexJpgDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Indexjpg;
import com.legendshop.spi.service.IndexJpgService;

/**
 * 首页广告服务.
 */
@Service("indexJpgService")
public class IndexJpgServiceImpl implements IndexJpgService {

	@Autowired
	private IndexJpgDao indexJpgDao;

	@Override
	public Indexjpg getIndexJpgById(Long id) {
		return indexJpgDao.queryIndexJpg(id);
	}


	@Override
	public void deleteIndexJpg(Indexjpg indexjpg) {
		indexJpgDao.deleteIndexJpg(indexjpg);
	}


	@Override
	public void updateIndexjpg(Indexjpg indexjpg) {
		indexJpgDao.updateIndexjpg(indexjpg);
	}


	@Override
	public Long saveIndexjpg(Indexjpg indexjpg) {
		return indexJpgDao.saveIndexjpg(indexjpg);
	}

	@Override
	@CacheEvict(value="IndexjpgList",allEntries=true)
	public void evictIndexJpgCache() {
	}


	@Override
	public List<Indexjpg> getIndexJpegByShopId() {
		return indexJpgDao.getIndexJpegByShopId();
	}

	@Override
	public PageSupport<Indexjpg> getIndexJpgPage(String curPageNO, Indexjpg indexjpg, int location, Integer pageSize,
			DataSortResult result) {
		return indexJpgDao.getIndexJpgPage(curPageNO,indexjpg,location,pageSize,result);
	}

	@Override
	public PageSupport<Indexjpg> getIndexJpgPageByData(String curPageNO, Indexjpg indexjpg, int location,
			Integer pageSize, DataSortResult result) {
		return indexJpgDao.getIndexJpgPageByData(curPageNO,indexjpg,location,pageSize,result);
	}
}
