/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.util.AppUtils;

/**
 * 预存款变更日志表Dao.
 */
@Repository("pdCashLogDao")
public class PdCashLogDaoImpl extends GenericDaoImpl<PdCashLog, Long> implements PdCashLogDao  {
     
	public PdCashLog getPdCashLog(Long id){
		return getById(id);
	}
	
    public int deletePdCashLog(PdCashLog pdCashLog){
    	return delete(pdCashLog);
    }
	
	public Long savePdCashLog(PdCashLog pdCashLog){
		return save(pdCashLog);
	}
	
	public int updatePdCashLog(PdCashLog pdCashLog){
		return update(pdCashLog);
	}
	
	public PageSupport<PdCashLog> getPdCashLog(CriteriaQuery cq){
		return queryPage(cq);
	}
	
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO,PdCashLog pdCashLog, int pageSize){
		 CriteriaQuery cq = new CriteriaQuery(PdCashLog.class,curPageNO);
		 if(AppUtils.isNotBlank(pdCashLog.getUserName())) {			 
			 cq.like("userName", "%" + pdCashLog.getUserName().trim() + "%");
		 }
		 cq.gt("addTime", pdCashLog.getStartTime());
		 cq.lt("addTime", pdCashLog.getEndTime());
		 cq.eq("logType", PdCashLogEnum.PROMOTER.value());//改为佣金收入
		 cq.addDescOrder("addTime");
		 cq.setPageSize(pageSize);
		return queryPage(cq);
	}

	@Override
	public PdRecharge getPdRecharge(Long id, String userId) {
		return get("select * from ls_pd_recharge where id=? and user_id=? ", PdRecharge.class, id,userId);
	}

	@Override
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, String uesrId, String logType, int pageSize) {
		CriteriaQuery cq = new CriteriaQuery(PdCashLog.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("userId", uesrId);
		cq.eq("logType", logType);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, int pageSize, String userId, String logType) {
		CriteriaQuery cq = new CriteriaQuery(PdCashLog.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("userId", userId);
		cq.eq("logType", logType);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<PdCashLog> getPresentRecord(String curPageNO, int pageSize, String userId, String logType) {
		CriteriaQuery cq = new CriteriaQuery(PdCashLog.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("userId", userId);
		cq.eq("logType", logType);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}
	

	@Override
	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, String userId, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(PdCashLog.class, curPageNO);
    	QueryMap paramMap = new QueryMap();
    	 paramMap.put("userName", userName);
    	 query.setPageSize(15);
		paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY l.add_time DESC");
		String QueryNsortCount = ConfigCode.getInstance().getCode("preDeposit.getPdCashLogListCount", paramMap);
		String	QueryNsort = ConfigCode.getInstance().getCode("preDeposit.getPdCashLogList", paramMap);
		query.setAllCountString(QueryNsortCount); 
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, PdCashLog pdCashLog, Integer pageSize,
			DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(PdCashLog.class, curPageNO);
    	QueryMap paramMap = new QueryMap();
    	if(AppUtils.isNotBlank(pdCashLog.getNickName())) {
    		paramMap.like("nickName", pdCashLog.getNickName().trim());
    	}
    	if(AppUtils.isNotBlank(pdCashLog.getUserName())) {
    		paramMap.like("userName", pdCashLog.getUserName().trim());
    	}
    	if (AppUtils.isNotBlank(pageSize)) {
			query.setPageSize(pageSize);
		}
    	if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "order by l.add_time desc");
 		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("preDeposit.getPdCashLogListCount", paramMap);
		String	QueryNsort = ConfigCode.getInstance().getCode("preDeposit.getPdCashLogList", paramMap);
		query.setAllCountString(QueryNsortCount); 
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<PdCashLog> getPdCashLogPage(String userId, String curPageNO) {
		CriteriaQuery cq=new CriteriaQuery(PdCashLog.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLogPageByType(String userId, Integer state, String curPageNO) {
		
		CriteriaQuery cq=new CriteriaQuery(PdCashLog.class, curPageNO);
		cq.setPageSize(10);
		if (AppUtils.isNotBlank(state)) {
			if (state == 1) {
				cq.ge("amount", 0);
			}else{
				cq.lt("amount", 0);
			}
		}
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}
	
 }
