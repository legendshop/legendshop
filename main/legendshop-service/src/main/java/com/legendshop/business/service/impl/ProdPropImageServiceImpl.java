/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdPropImageDao;
import com.legendshop.model.entity.ProdPropImage;
import com.legendshop.spi.service.ProdPropImageService;
import com.legendshop.util.AppUtils;

/**
 * 商品属性图片
 */
@Service("prodPropImageService")
public class ProdPropImageServiceImpl  implements ProdPropImageService{

	@Autowired
    private ProdPropImageDao prodPropImageDao;

    public ProdPropImage getProdPropImage(Long id) {
        return prodPropImageDao.getProdPropImage(id);
    }

    public void deleteProdPropImage(ProdPropImage prodPropImage) {
        prodPropImageDao.deleteProdPropImage(prodPropImage);
    }

    public Long saveProdPropImage(ProdPropImage prodPropImage) {
        if (!AppUtils.isBlank(prodPropImage.getId())) {
            updateProdPropImage(prodPropImage);
            return prodPropImage.getId();
        }
        return prodPropImageDao.saveProdPropImage(prodPropImage);
    }

    public void updateProdPropImage(ProdPropImage prodPropImage) {
        prodPropImageDao.updateProdPropImage(prodPropImage);
    }

	@Override
	public List<ProdPropImage> getProdPropImageByProdId(Long prodId) {
		return prodPropImageDao.getProdPropImageByProdId(prodId);
	}

	@Override
	public List<ProdPropImage> getProdPropImageByKeyVal(Long prodId, Long key,
			Long val) {
		return prodPropImageDao.getProdPropImageByKeyVal(prodId,key,val);
	}

}
