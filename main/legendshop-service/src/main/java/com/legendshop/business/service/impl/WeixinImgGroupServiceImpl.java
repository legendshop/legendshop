/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinImgFileDao;
import com.legendshop.business.dao.WeixinImgGroupDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinImgFile;
import com.legendshop.model.entity.weixin.WeixinImgGroup;
import com.legendshop.spi.service.WeixinImgGroupService;
import com.legendshop.util.AppUtils;

/**
 * 微信图片组服务
 */
@Service("weixinImgGroupService")
public class WeixinImgGroupServiceImpl  implements WeixinImgGroupService{

	@Autowired
    private WeixinImgGroupDao weixinImgGroupDao;
    
	@Autowired
    private WeixinImgFileDao weixinImgFileDao;

    public WeixinImgGroup getWeixinImgGroup(Long id) {
        return weixinImgGroupDao.getWeixinImgGroup(id);
    }

    public void deleteWeixinImgGroup(WeixinImgGroup weixinImgGroup) {
    	weixinImgGroupDao.deleteWeixinImgGroup(weixinImgGroup);
    	Long groupId = weixinImgGroup.getId();
    	//获取分组下的图片
    	List<WeixinImgFile> weixinImgFiles = weixinImgFileDao.queryByProperties(new EntityCriterion().eq("imgGoupId", groupId));
    	//将图片的分组改成 未分组
    	for (WeixinImgFile weixinImgFile : weixinImgFiles) {
    		weixinImgFile.setImgGoupId(0l);
		}
    	//更新
    	weixinImgFileDao.update(weixinImgFiles);
    }

    public Long saveWeixinImgGroup(WeixinImgGroup weixinImgGroup) {
        if (!AppUtils.isBlank(weixinImgGroup.getId())) {
            updateWeixinImgGroup(weixinImgGroup);
            return weixinImgGroup.getId();
        }
        return weixinImgGroupDao.saveWeixinImgGroup(weixinImgGroup);
    }

    public void updateWeixinImgGroup(WeixinImgGroup weixinImgGroup) {
        weixinImgGroupDao.updateWeixinImgGroup(weixinImgGroup);
    }

	@Override
	public List<WeixinImgGroup> getAllGroupsInfo() {
		return weixinImgGroupDao.getAllGroupsInfo();
	}

	@Override
	public WeixinImgGroup getWeixinImgGroupByName(String groupName) {
		return weixinImgGroupDao.getWeixinImgGroupByName(groupName);
	}
}
