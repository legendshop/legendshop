/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassLabel;

/**
 * The Class GrassLabelDao. 种草社区标签表Dao接口
 */
public interface GrassLabelDao extends Dao<GrassLabel,Long>{

	/**
	 * 根据Id获取种草社区标签表
	 */
	public abstract GrassLabel getGrassLabel(Long id);

	/**
	 *  根据Id删除种草社区标签表
	 */
    public abstract int deleteGrassLabel(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassLabel(GrassLabel grassLabel);

	/**
	 * 保存种草社区标签表
	 */
	public abstract Long saveGrassLabel(GrassLabel grassLabel);

	/**
	 *  更新种草社区标签表
	 */		
	public abstract int updateGrassLabel(GrassLabel grassLabel);

	/**
	 * 分页查询种草社区标签表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassLabel> queryGrassLabel(String curPageNO, Integer pageSize);
	
    public PageSupport<GrassLabel> queryGrassLabelPage(String curPageNO,String name,Integer pageSize);

    
    public abstract PageSupport<GrassLabel> getGrassLabelByLabel(String curPageNo,GrassLabel grassLabel,Integer PageSize);

	/**
	 *根据文章获取label
	 * @param articleId
	 * @return
	 */
	public abstract List<GrassLabel> getGrassLabelByGrassId(Long articleId);

	/**
	 * 增加标签引用次数
	 * @param lid
	 */
	public abstract void addRefNum(Long lid);
}
