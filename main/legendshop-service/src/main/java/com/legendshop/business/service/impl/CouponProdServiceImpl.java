/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CouponProdDao;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.util.AppUtils;

/**
 * 优惠券商品.
 */
@Service("couponProdService")
public class CouponProdServiceImpl  implements CouponProdService{
	
	@Autowired
    private CouponProdDao couponProdDao;

    public CouponProd getCouponProd(Long id) {
        return couponProdDao.getCouponProd(id);
    }

    public void deleteCouponProd(CouponProd couponProd) {
        couponProdDao.deleteCouponProd(couponProd);
    }

    public Long saveCouponProd(CouponProd couponProd) {
        if (!AppUtils.isBlank(couponProd.getCouponProdId())) {
            updateCouponProd(couponProd);
            return couponProd.getCouponProdId();
        }
        return couponProdDao.saveCouponProd(couponProd);
    }

    public void updateCouponProd(CouponProd couponProd) {
        couponProdDao.updateCouponProd(couponProd);
    }

	@Override
	public List<CouponProd> getCouponProdByCouponId(Long couponId) {
		return couponProdDao.getCouponProdByCouponId(couponId);
	}

	@Override
	public List<CouponProd> getCouponProdByCouponIds(List<Long> couponIds) {
		return couponProdDao.getCouponProdByCouponIds(couponIds);
	}

	@Override
	public List<CouponProd> getCouponProdByProdId(Long prodId) {
		
		return couponProdDao.getCouponProdByProdId(prodId);
	}

	@Override
	public List<Long> getCouponProdIdByCouponId(Long couponId) {
		return couponProdDao.getCouponProdIds(couponId);
	}
}
