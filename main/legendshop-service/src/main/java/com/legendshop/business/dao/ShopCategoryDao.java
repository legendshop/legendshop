/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.CateGoryExportDTO;
import com.legendshop.model.dto.ShopCategoryDto;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.ShopCategory;

import java.util.List;

/**
 *商家的商品类目Dao
 */
public interface ShopCategoryDao extends GenericDao<ShopCategory, Long> {
     
	public abstract ShopCategory getShopCategory(Long id);
	
    public abstract int deleteShopCategory(ShopCategory shopCategory);
	
	public abstract Long saveShopCategory(ShopCategory shopCategory);
	
	public abstract int updateShopCategory(ShopCategory shopCategory);
	
	/** 根据  List<ShopCategory> 来查取  第三级 类目 **/
	public List<ShopCategory> subCatQuery(List<ShopCategory> nextCatList);

	/** 根据 ParentId 查找分类 **/
	public abstract List<ShopCategory> queryByParentId(Long nextCatId);

	/** 根据 ShopId 查找分类 **/
	public abstract List<ShopCategory> queryByShopId(Long shopId);

	/** 用于填充二级商品分类的select options **/
	public abstract List<ShopCategory> loadNextCat(Long shopCatId, Long shopId);

	public abstract List<ShopCategory> getShopCategory(Long shopId, String name);
	/** 获取店铺类目导出list */
    List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId);
	
	/**  获取其处于上线状态的子类目的数量*/
	public abstract Long getNextCategoryCount(Long shopCatId);

	public abstract PageSupport<ShopCategory> queryShopCategory(String curPageNO, ShopCategory shopCategory,
			Long shopId);

	public abstract PageSupport<ShopCategory> getNextShopCat(String curPageNO, ShopCategoryDto shopCatDto);

	public abstract PageSupport<ShopCategory> getSubShopCat(String curPageNO, ShopCategoryDto shopCatDto);
	/** 获取商家的一级分类商品*/
	public abstract List<ShopCategory> getFirstShopCategory(Long shopId);

	/**获取店铺分类**/
	public abstract List<ShopCategory> getShopCategoryList(Long shopId);

    /** 通过上一级分类id和分类名获取店铺分类对象 */
    ShopCategory getShopCategoryByName(Long shopId, Long upCategoryId, String categoryName);

    /**
     * 获取店铺分类列表
     * @param shopId
     * @return
     */
	public abstract List<DecorateShopCategoryDto> getShopCategoryDtoList(Long shopId);

	/**
	 * app商家端 获取店铺分类列表
	 * @param shopId
	 * @return
	 */
	PageSupport<ShopCategory> appQueryShopCategory(String curPageNO, Long shopId,Integer grade,Long parentId);

	/**
	 * app商家端 获取店铺分类列表
	 * @param shopId
	 * @return
	 */
	List<ShopCategory>  appQueryShopCategory(Long shopId,Integer grade,Long parentId);

	/**
	 * 根据id获取Name
	 * @param id
	 * @return
	 */
	String getShopCategoryName(Long id);
}
