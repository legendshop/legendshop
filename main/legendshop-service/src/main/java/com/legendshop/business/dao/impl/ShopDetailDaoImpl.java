/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.BusinessMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.cache.ShopDetailUpdate;
import com.legendshop.base.cache.UserDetailUpdate;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.BasketDao;
import com.legendshop.business.dao.DeliveryCorpDao;
import com.legendshop.business.dao.DeliveryTypeDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.IdTextEntity;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.ShopDetailTimerDto;
import com.legendshop.model.dto.ShopSeekDto;
import com.legendshop.model.dto.UserDetailExportDto;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopDetailView;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.CommonUtil;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.dao.AttachmentDao;
import com.legendshop.util.AppUtils;
import com.legendshop.util.ip.IPSeeker;

/**
 * 商家详情Dao
 */
@Repository
public class ShopDetailDaoImpl extends GenericDaoImpl<ShopDetail, Long> implements ShopDetailDao {
	
    private final Logger log = LoggerFactory.getLogger(ShopDetailDaoImpl.class);

    @Autowired
    private CommonUtil commonUtil;
    
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private AttachmentDao attachmentDao;
	
	// 购物车Dao
	@Autowired
	private BasketDao basketDao;

	@Autowired
	private DeliveryTypeDao deliveryTypeDao;

	@Autowired
	private DeliveryCorpDao deliveryCorpDao;

    @Override
    public long getProductNum(Long shopId) {
        return getLongResult("select count(*) from ls_prod prod where prod.status = 1 and prod.shop_id = ?", shopId);
    }

    @Override
    public long getOffProductNum(Long shopId) {
        return getLongResult("select count(*) from ls_prod prod where prod.status = 0 and prod.shop_id = ?", shopId);
    }

    @Override
    @ShopDetailUpdate
    public void updateShopDetail(ShopDetail shopDetail) {
        update(shopDetail);
    }


    /**
     * 更新数量
     */
    @Override
    @Caching(evict = {
    		@CacheEvict(value = "ShopDetailView", key = "#shopId"), 
    		@CacheEvict(value = "ShopDetail", key = "#shopId")
    })
    public void updateShopDetailProdNum(Long shopId) {
        String sql = "update ls_shop_detail set product_num = (select count(1) from ls_prod where shop_id = ? and status = 1), off_product_num = (select count(1) from ls_prod where shop_id = ? and status != 1) where shop_id = ?";
        this.update(sql, shopId, shopId, shopId);
    }

    /**
     * 审核商城
     */
    @Override
    @ShopDetailUpdate
    public boolean updateShop(String userId, ShopDetail shopDetail, Integer status) {/*

		boolean result = true;
		try {
			String msgText = "";
			if (ShopStatusEnum.REJECT.value().equals(status)
					|| ShopStatusEnum.CLOSE.value().equals(status) || ShopStatusEnum.OFFLINE.value().equals(status)) {
				// 拒绝开店
				commonUtil.removeAdminRight(userId);
				msgText = "开店审核失败。";
				if(AppUtils.isNotBlank(shopDetail.getAuditOpinion())){
					msgText+="审核意见："+shopDetail.getAuditOpinion();
				}
			} else if (ShopStatusEnum.ONLINE.value().equals(status)) {
				commonUtil.saveAdminRight(userId);
				msgText = "恭喜，开店审核成功。您可以进入卖家中心了。";
			}
			
			this.update("update ls_shop_detail set status = ?,audit_opinion = ?, modify_date = ? where shop_id = ?", status, shopDetail.getAuditOpinion(), new Date(),shopDetail.getShopId());
			
			EventHome.publishEvent(new FireEvent(shopDetail, OperationTypeEnum.UPDATE_STATUS));
			
			//发送站内信 通知
			EventHome.publishEvent(new SendSiteMsgEvent(PropertiesUtil.getDefaultShopName(),shopDetail.getUserName(),"开店审核通知",msgText));

		} catch (Exception e) {
			log.error("auditShop ", e);
			result = false;
		}
		return result;
	*/
        return false;
    }

    /**
     * 保存商家
     */
    @Override
    @ShopDetailUpdate
    public void saveShopDetail(ShopDetail shopDetail) {
        save(shopDetail);
        // 保存管理员角色
        commonUtil.saveAdminRight(shopDetail.getUserId());

    }


    /**
     * 删除商城.
     *
     * @param shopDetail the shop detail
     */
    @Override
    @ShopDetailUpdate
    public String deleteShopDetail(ShopDetail shopDetail) {
        String result = deleteShopDetail(shopDetail.getId(), shopDetail.getUserId(), shopDetail.getUserName(), true);
        return result;
    }

    @Override
    @Cacheable(value = "ShopDetail", key = "#shopId")
    public ShopDetail getShopDetailByShopId(final Long shopId) {
        return getById(shopId);
    }

    @Override
    @Cacheable(value = "ShopDetailView", key = "#shopId")
    public ShopDetailView getShopDetailView(final Long shopId) {
        if (AppUtils.isBlank(shopId)) {
            return null;
        }
        log.debug("getSimpleInfoShopDetail shopId : {}", shopId);
        List<ShopDetailView> list = null;
        String sql = ConfigCode.getInstance().getCode("biz.getShopDetailView");
        log.debug("getShopDetailView run sql {}, userName {} ", sql, shopId);
        list = jdbcTemplate.query(sql, new Object[]{shopId}, new ShopDetailRowMapper());
        if (AppUtils.isNotBlank(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<ShopDetailView> getShopDetail(Long[] shopId) {
        List<Long> postIdList = new ArrayList<Long>();
        StringBuffer sb = new StringBuffer(ConfigCode.getInstance().getCode("biz.getShopDetailViewList"));
        for (int i = 0; i < shopId.length - 1; i++) {
            if (shopId[i] != null) {
                sb.append("?,");
                postIdList.add(shopId[i]);
            }
        }
        if (postIdList.size() == 0) {
            return new ArrayList<ShopDetailView>();
        }
        sb.setLength(sb.length() - 1);
        sb.append(")");
        if (log.isDebugEnabled()) {
            log.debug("getShopDetailView run sql {}, postIdList {} ", sb.toString(), postIdList.toArray());
        }
        return jdbcTemplate.query(sb.toString(), postIdList.toArray(), new ShopDetailRowMapper());
    }


    /**
     * 根据用户名获取商城ID
     */
    @Override
    public Long getShopIdByName(String userName) {
        List<Long> result = jdbcTemplate.query("select sd.shop_id as shopId from ls_shop_detail sd where sd.user_name = ?", new Object[]{userName}, new RowMapper<Long>() {
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong("shopId");
            }
        });
        if (AppUtils.isBlank(result)) {
            return null;
        }
        return result.get(0);
    }

    /**
     * 根据创建人找到对应的shopId
     */
    @Override
    public Long getShopIdByUserId(String userId) {
        List<Long> result = jdbcTemplate.query("select sd.shop_id as shopId from ls_shop_detail sd where sd.user_id = ?", new Object[]{userId}, new RowMapper<Long>() {
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getLong("shopId");
            }
        });
        if (AppUtils.isBlank(result)) {
            return null;
        }
        return result.get(0);
    }


    /**
     * The Class ShopDetailRowMapper.
     */
    class ShopDetailRowMapper implements RowMapper<ShopDetailView> {

        @Override
        public ShopDetailView mapRow(ResultSet rs, int rowNum) throws SQLException {
            ShopDetailView shopDetail = new ShopDetailView();
            shopDetail.setShopId(rs.getLong("shopId"));
            shopDetail.setUserId(rs.getString("userId"));
            shopDetail.setUserName(rs.getString("userName"));
            shopDetail.setSiteName(rs.getString("siteName"));
            shopDetail.setShopAddr(rs.getString("shopAddr"));
            shopDetail.setBankCard(rs.getString("bankCard"));
            shopDetail.setPayee(rs.getString("payee"));
            shopDetail.setCode(rs.getString("code"));
            shopDetail.setPostAddr(rs.getString("postAddr"));
            shopDetail.setRecipient(rs.getString("recipient"));
            shopDetail.setStatus(rs.getInt("status"));
            shopDetail.setVisitTimes(rs.getLong("visitTimes"));
            shopDetail.setProductNum(rs.getLong("productNum"));
            shopDetail.setCommNum(rs.getLong("commNum"));
            shopDetail.setOffProductNum(rs.getLong("offProductNum"));
            shopDetail.setModifyDate(rs.getDate("modifyDate"));
            shopDetail.setRecDate(rs.getDate("recDate"));
            shopDetail.setBriefDesc(rs.getString("briefDesc"));
            shopDetail.setDetailDesc(rs.getString("detailDesc"));
            shopDetail.setShopPic(rs.getString("shopPic"));
            shopDetail.setShopPic2(rs.getString("shopPic2"));
            shopDetail.setFrontEndLanguage(rs.getString("frontEndLanguage"));
            shopDetail.setBackEndLanguage(rs.getString("backEndLanguage"));
            shopDetail.setGradeId(rs.getInt("gradeId"));
            shopDetail.setType(rs.getInt("type"));
            shopDetail.setIdCardPic(rs.getString("idCardPic"));
            shopDetail.setIdCardNum(rs.getString("idCardNum"));
            shopDetail.setCreateCountryCode(rs.getString("createCountryCode"));
            shopDetail.setCreateAreaCode(rs.getString("createAreaCode"));
            shopDetail.setProvinceid(rs.getInt("provinceid"));
            shopDetail.setCityid(rs.getInt("cityid"));
            shopDetail.setAreaid(rs.getInt("areaid"));
            shopDetail.setProvince(rs.getString("province"));
            shopDetail.setCity(rs.getString("city"));
            shopDetail.setArea(rs.getString("area"));
            shopDetail.setUserTel(rs.getString("userTel"));
            shopDetail.setNickName(rs.getString("nickName"));
            shopDetail.setUserMobile(rs.getString("userMobile"));
            shopDetail.setQq(rs.getString("qq"));
            shopDetail.setBankCard(rs.getString("msnNumber"));
            shopDetail.setUserPostcode(rs.getString("userPostcode"));
            shopDetail.setFax(rs.getString("fax"));
            shopDetail.setFrontEndStyle(rs.getString("frontEndStyle"));
            shopDetail.setBackEndStyle(rs.getString("backEndStyle"));
            shopDetail.setFrontEndTemplet(rs.getString("frontEndTemplet"));
            shopDetail.setBackEndTemplet(rs.getString("backEndTemplet"));
            shopDetail.setProdRequireAudit((Integer) rs.getObject("prodRequireAudit"));
            if (shopDetail.getQq() != null) {
                String[] qqs = shopDetail.getQq().split(",");
                List<String> qqList = new ArrayList<String>(qqs.length);
                for (int i = 0; i < qqs.length; i++) {
                    if (qqs[i] != null && qqs[i].length() > 0) {
                        qqList.add(qqs[i]);
                    }
                }
                shopDetail.setQqList(qqList);
            }
            shopDetail.setDomainName(rs.getString("domainName"));
            shopDetail.setIcpInfo(rs.getString("icpInfo"));
            shopDetail.setLogoPic(rs.getString("logoPic"));
            return shopDetail;
        }

    }

    @Override
    public Long getAllShopCount() {
        return getCount();
    }

    @Override
    public boolean isShopExist(Long shopId) {
        if (AppUtils.isBlank(shopId)) {
            return false;
        }
        List<String> list = query("select 1 from ls_shop_detail where  status = 1 and shop_id = ?", String.class, shopId);
        if (AppUtils.isNotBlank(list)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long getShopIdByDomain(String domainName) {
        if (AppUtils.isBlank(domainName)) {
            return null;
        }
        String sql = "select shop_id from ls_shop_detail where domain_name = ?";
        log.debug("getShopNameByDomain run sql {}, domainName {} ", sql, domainName);
        List<Long> result = jdbcTemplate.queryForList(sql, new Object[]{domainName}, Long.class);
        if (AppUtils.isNotBlank(result)) {
            return result.get(0);
        }
        return null;
    }

    @Override
    public ShopDetail getShopDetailByUserId(String userId) {
        return getByProperties(new EntityCriterion().eq("userId", userId));
    }

    @Override
    @Caching(evict = {
    		@CacheEvict(value = "ShopDetailView", key = "#shopId"), 
    		@CacheEvict(value = "ShopDetail", key = "#shopId")
    })
    public int deleteShopDetailById(Long shopId) {
        return deleteById(shopId);
    }

    @Override
    public List<ShopDetail> getAllShopDetail() {
        return this.queryAll();
    }

    @Override
    public Long getMaxShopId() {
        return this.get("select max(shop_id) from ls_shop_detail ", Long.class);
    }

    @Override
    public long getCountShopId() {
        return this.getLongResult("select count(shop_id) from ls_shop_detail ");
    }

    @Override
    public Long getMinShopId() {
        return this.get("select min(shop_id) from ls_shop_detail ", Long.class);
    }

    @Override
    public void changeOptionStatus(String userId, Long shopId, Integer option) {
        this.update("update ls_shop_detail set op_status = ? where user_id = ? and shop_id=?", option, userId, shopId);
    }

    @Override
    public void changeStatus(String userId, Long shopId, Integer status) {
        this.update("update ls_shop_detail set status = ? where user_id = ? and shop_id=?", status, userId, shopId);
    }

    @Override
    public String getShopName(Long shopId) {
        List<String> result = jdbcTemplate.query("select sd.site_name as shopName from ls_shop_detail sd where sd.shop_id = ?", new Object[]{shopId}, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("shopName");
            }
        });
        if (AppUtils.isBlank(result)) {
            return null;
        }
        return result.get(0);
    }

    @Override
    public Long getShopIdBySecDomain(String secDomainName) {

        return getLongResult("select shop_id s from ls_shop_detail s where s.sec_domain_name = ?", secDomainName);
    }

    @Override
    public Integer updateShopType(Long shopId, Long type) {
        return this.update("update ls_shop_detail set shop_type = ? where shop_id=?", type, shopId);
    }

    @Override
    public List<IdTextEntity> getShopDetailList(String shopName) {
        StringBuffer sql = new StringBuffer("SELECT sd.shop_id AS id, sd.site_name AS text FROM ls_shop_detail sd WHERE sd.status = 0 OR sd.status = 1 AND sd.op_status = 4");
        if (AppUtils.isNotBlank(shopName)) {
            sql.append(" and sd.site_name like" + "'%" + shopName + "%'");
        }
        return this.queryLimit(sql.toString(), IdTextEntity.class, 0, 20, new Object[]{});
    }

    @Override
    public int updateShopDetailStatus(String userId, Integer status) {
        String sql = "UPDATE ls_shop_detail SET status = ? WHERE user_id = ?";
        return this.update(sql, status, userId);
    }

    @Override
    public List<ShopDetailTimerDto> getShopDetailList(Long firstShopId, Integer commitInteval) {
        String sql = "SELECT shop_id AS shopId,site_name AS siteName,commission_rate AS commissionRate FROM ls_shop_detail WHERE STATUS=1 AND shop_id>=? ORDER BY shop_id ";
        return queryLimit(sql, ShopDetailTimerDto.class, 0, commitInteval, firstShopId);
    }

    @Override
    public List<UserDetailExportDto> findExportUserDetail(String string, Object[] array) {
        return query(string, UserDetailExportDto.class, array);
    }


    @Override
    public List<ShopSeekDto> getShopSeekList(String sql, Object[] params) {
        List<ShopSeekDto> shopList = this.query(sql, params, new RowMapper<ShopSeekDto>() {

            @Override
            public ShopSeekDto mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                ShopSeekDto shopSeekDto = new ShopSeekDto();

                Long shopId = rs.getLong("shopId");

                //封装店铺信息
                shopSeekDto.setShopId(shopId);
                shopSeekDto.setShopName(rs.getString("shopName"));
                shopSeekDto.setShopPic(rs.getString("shopPic"));
                shopSeekDto.setCollects(rs.getInt("collects"));

                //封装店铺下的商品信息
                List<Product> prods = new ArrayList<Product>();

                //判断该店铺是否有商品
                long prodId = rs.getLong("prodId");
                if (prodId == 0) {
                    return shopSeekDto;
                }

                Product prod = new Product();

                prod.setProdId(rs.getLong("prodId"));
                prod.setName(rs.getString("prodName"));
                prod.setPic(rs.getString("pic"));

                prods.add(prod);

                while (rs.next()) {
                    if (!shopId.equals(rs.getLong("shopId"))) {
                        break;
                    }
                    prod = new Product();
                    prod.setProdId(rs.getLong("prodId"));
                    prod.setPic(rs.getString("pic"));

                    prods.add(prod);
                }
                shopSeekDto.setProdList(prods);

                rs.previous();
                return shopSeekDto;
            }
        });
        return shopList;
    }

    @Override
    public PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail) {
        CriteriaQuery cq = new CriteriaQuery(ShopDetail.class, curPageNO);
        cq.setPageSize(10);

        cq.isNotNull("secDomainName");

        if (AppUtils.isNotBlank(shopDetail.getSiteName())) {
            cq.like("siteName", "%" + shopDetail.getSiteName() + "%");
        }

        if (AppUtils.isNotBlank(shopDetail.getSecDomainName())) {
            cq.like("secDomainName", "%" + shopDetail.getSecDomainName() + "%");
        }
        return queryPage(cq);
    }

    @Override
    public PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail, Integer pageSize, DataSortResult result) {
        QueryMap map = new QueryMap();
        if (AppUtils.isNotBlank(shopDetail.getSiteName())) {  //店铺名字
            map.like("siteName", shopDetail.getSiteName().trim());
        }
        if (AppUtils.isNotBlank(shopDetail.getNickName())) {
            map.like("nickName", shopDetail.getNickName().trim());
        }
        if (AppUtils.isNotBlank(shopDetail.getContactName())) {
            map.like("contactName", shopDetail.getContactName().trim());
        }
        if (AppUtils.isNotBlank(shopDetail.getContactMobile())) {
            map.like("contactMobile", shopDetail.getContactMobile().trim());
        }
        map.put("type", shopDetail.getType());
        map.put("status", shopDetail.getStatus());
        map.put("opStatus", shopDetail.getOpStatus());
        map.put("shopType", shopDetail.getShopType());
        map.like("userName", shopDetail.getUserName());

        //采用外部排序
        if (result.isSortExternal()) {
            map.put(Constants.ORDER_INDICATOR, result.parseSeq());
        }

        String queryAllSQL = ConfigCode.getInstance().getCode("biz.getShopDetailCount", map);
        String querySQL = ConfigCode.getInstance().getCode("biz.getShopDetail", map);
        SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(ShopDetail.class, curPageNO);
        if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
            simpleSqlQuery.setPageSize(pageSize);
        }
        simpleSqlQuery.setAllCountString(queryAllSQL);
        simpleSqlQuery.setQueryString(querySQL);

        map.remove(AttributeKeys.ORDER_INDICATOR);
        simpleSqlQuery.setParam(map.toArray());
        return querySimplePage(simpleSqlQuery);
    }

    @Override
    public PageSupport<ShopDetail> getloadSelectShop(String curPageNO, String shopName) {
        QueryMap map = new QueryMap();
        if (AppUtils.isNotBlank(shopName)) {
            map.put("shopName", "%" + shopName + "%");
        }
       /* map.put("endDate", new Date());*/
        SimpleSqlQuery hql = new SimpleSqlQuery(ShopDetail.class);
        hql.setPageSize(5);
        hql.setCurPage(curPageNO);
        hql.setParam(map.toArray());
        //后台红包挑选店铺:获取除了 参与了没有过期红包  的所有店铺
        hql.setAllCountString(ConfigCode.getInstance().getCode("coupon.loadSelectShopCount", map));
        hql.setQueryString(ConfigCode.getInstance().getCode("coupon.loadSelectShop", map));
        return querySimplePage(hql);
    }

	@Override
	public int updateShopCommision(Long shopId, Integer commision) {
		return this.update("update ls_shop_detail set commission_rate = ? where shop_id=?", commision, shopId);
	}

	@Override
	public List<ShopDetail> getShopDetailListTop(Integer start, Integer size) {
		String sql = "SELECT * FROM ls_shop_detail ORDER BY shop_id LIMIT ?,?";
		return query(sql, ShopDetail.class, start, size);
	}

	@Override
	public ShopDetail getTypeShopDetailByUserId(String userId) {
		String sql = "SELECT s.*,c.company_name AS companyName,c.license_pic AS licensePicFilePath,c.license_number AS licenseNumber\n" + 
				"FROM ls_shop_detail s LEFT JOIN ls_shop_company_detail c ON s.shop_id = c.shop_id WHERE s.user_id = ?";
		return this.get(sql, ShopDetail.class, userId);
	}

	@Override
	public Integer getShopStatus(Long shopId) {
		return this.get("SELECT STATUS FROM ls_shop_detail WHERE shop_id = ?", Integer.class, shopId);
	}

	/*
	 * 返回商城开通状态
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#saveShopDetailAndRole(
	 * com.legendshop.model.entity.UserDetail,
	 * com.legendshop.model.entity.ShopDetail)
	 */
	@Override
	@UserDetailUpdate
	public Integer saveShopDetailAndRole(UserDetail userDetail, ShopDetail shopDetail) {
		// 保存管理员角色
		return saveShopDetail(userDetail, shopDetail);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.UserDetailDao#updateShopDetail(com.
	 * legendshop.model.entity.UserDetail,
	 * com.legendshop.model.entity.ShopDetail, boolean)
	 */
	@Override
	@UserDetailUpdate
	public void updateShopDetail(UserDetail userDetail, ShopDetail shopDetail, boolean isAdmin) {
		if (isAdmin) {
			saveShopDetail(userDetail, shopDetail);
		}
	}

	// UserId 一定不能为空，返回当前商城开通状态
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.UserDetailDao#saveShopDetail(com.
	 * legendshop .model.entity.UserDetail,
	 * com.legendshop.model.entity.ShopDetail)
	 */
	@Override
	@UserDetailUpdate
	public Integer saveShopDetail(UserDetail userDetail, ShopDetail shopDetail) {
		Date date = new Date();
		shopDetail.setUserName(userDetail.getUserName());
		shopDetail.setModifyDate(date);
		shopDetail.setUserId(userDetail.getUserId());
		shopDetail.setRecDate(date);
		shopDetail.setVisitTimes(0l);
		shopDetail.setOffProductNum(0l);
		shopDetail.setProductNum(0l);
		shopDetail.setCommNum(0l);
		shopDetail.setCapital(0d);
		shopDetail.setCredit(0);
		// 分开店铺和用户注册，此处用户已经注册，无需保存权限
		shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		// if
		// (PropertiesUtil.getObject(SysParameterEnum.VALIDATION_ON_OPEN_SHOP,
		// Boolean.class)) {//需要管理员验证用户
		// shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		// commonUtil.saveUserRight(userDetail.getUserId());//保存一般用户权限
		// } else {
		// shopDetail.setStatus(ShopStatusEnum.ONLINE.value());
		// commonUtil.saveAdminRight(userDetail.getUserId());//保存商家权限
		// }

		// 得到创建时的国家和地区
		if (shopDetail.getIp() != null) {
			shopDetail.setCreateAreaCode(IPSeeker.getInstance().getArea(shopDetail.getIp()));
			shopDetail.setCreateCountryCode(IPSeeker.getInstance().getCountry(shopDetail.getIp()));
		}
		String userName = userDetail.getUserName();
		String filename = null;
		// 取得上传的文件
		MultipartFile idCardPicFile = shopDetail.getIdCardPicFile();
		MultipartFile idCardBackPicFile = shopDetail.getIdCardBackPicFile();

		if ((idCardPicFile != null) && (idCardPicFile.getSize() > 0)) {
			filename = attachmentManager.upload(idCardPicFile);
			// 保存附件表
			attachmentManager.saveImageAttachment(userName,userDetail.getUserId(),shopDetail.getShopId(), filename, idCardPicFile, AttachmentTypeEnum.USER);
			log.info("{} 上传身份证照片文件 {} ", userDetail.getUserName(), filename);
			shopDetail.setIdCardPic(filename);

		} else {
			throw new BusinessException("idpic null");
		}

		if ((idCardBackPicFile != null) && (idCardBackPicFile.getSize() > 0)) {
			filename = attachmentManager.upload(idCardBackPicFile);
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userDetail.getUserId(),shopDetail.getShopId(), filename, idCardBackPicFile, AttachmentTypeEnum.USER);
			log.info("{} 上传身份证照片文件 {} ", userDetail.getUserName(), filename);
			shopDetail.setIdCardBackPic(filename);
		} else {
			throw new BusinessException("idpic null");
		}

		Long shopId = save(shopDetail);
		shopDetail.setShopId(shopId);
		userDetail.setShopId(shopId);
		this.updateShopId(userDetail);
		return shopDetail.getStatus();
	}
	
	/**
	 * 删除商家的所有信息 TODO
	 * 
	 * @param userName
	 * @param deleteShopOnly
	 *            是否只是删除商城，如果是要删除商城对应的权限
	 */
	public String deleteShopDetail(Long shopId, String userId, String userName, boolean deleteShopOnly) {

		if (hasProd(shopId)) {
			return "不能删除商城" + userName + "， 请先下架其商品";
		}

		if (deleteShopOnly) {// 删除商家的权限,只是剩下用户权限
			commonUtil.removeAdminRight(userId);
		}

		// 商城的购物车
		basketDao.deleteBasket(shopId);

		// 物流方式
		deliveryTypeDao.deleteDeliveryTypeByShopId(shopId);

		// 物流公司
		deliveryCorpDao.deleteDeliveryCorpByShopId(shopId);

		// 用户商品的介绍图片
		update("delete from ls_img_file where shop_id = ?", shopId);

		// 用户商品的相关商品
		// deleteUserItem("ls_prod_rel", userName);
		update("delete from ls_prod_rel where shop_id = ?", shopId);

		// 商品评论
		update("DELETE FROM ls_prod_reply WHERE prod_comm_id IN (SELECT id FROM ls_prod_comm WHERE prod_id IN (SELECT prod_id FROM ls_prod WHERE shop_id = ?))",
				shopId);
		update("DELETE FROM ls_prod_comm WHERE prod_id IN (SELECT prod_id FROM ls_prod WHERE shop_id = ?)", shopId);
		
		update("DELETE FROM ls_prod_comm_stat WHERE prod_id IN (SELECT prod_id FROM ls_prod WHERE shop_id = ?)", shopId);

		// 产品咨询
		update("DELETE FROM ls_prod_cons WHERE prod_id IN (SELECT prod_id FROM ls_prod WHERE shop_id = ?)", shopId);

		// 团购产品
		update("delete from ls_group_prod where exists (select 1 from ls_prod where ls_group_prod.prod_id = ls_prod.prod_id and ls_prod.user_name = ?)",
				userName);

		// 商户（产品供应商）
		deleteUserItem("ls_partner", userName);

		//用户商品
		update("DELETE FROM ls_prod WHERE shop_id = ?", shopId);

		// 商品品牌, 已经申请的品牌不能删除
		update("DELETE FROM ls_brand WHERE status <> 1 and shop_id = ?", shopId);

		//附件
		update("DELETE FROM ls_attmnt_tree WHERE shop_id = ?", shopId);
		update("DELETE FROM ls_attachment WHERE shop_id = ?", shopId);
		
		
		// 类目
		update(" DELETE FROM ls_shop_cat WHERE shop_id = ?", shopId);

		// 装修
		update(" DELETE FROM ls_shop_banner WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_decotate WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_layout WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_layout_banner WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_layout_div WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_layout_hot WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_shop_layout_prod WHERE shop_id = ?", shopId);

		// 删除商品自定义属性和参数
		update(" DELETE FROM ls_prod_user_attr WHERE shop_id = ?", shopId);
		update(" DELETE FROM ls_prod_user_param WHERE shop_id = ?", shopId);

		List<Attachment> attmntList = attachmentDao.getAttachmentByShopId(shopId);
		if (AppUtils.isNotBlank(attmntList)) {
			for (Attachment attachment : attmntList) {
				attachmentDao.deleteAttachment(attachment);
				// 删除用户商品图片目录
				attachmentManager.deleteTruely(attachment.getFilePath());
			}
		}

		// 商城表
		deleteShopDetailById(shopId);

		return null;
	}
	
	
	@UserDetailUpdate
	private void updateShopId(UserDetail userDetail) {
		this.update("update ls_usr_detail set shop_id = ? where user_id = ?", userDetail.getShopId(), userDetail.getUserId());
	}
	
	/**
	 * 根据用户删除用户信息
	 * @param tableName 表名
	 * @param userName 用户名
	 */
	private void deleteUserItem(String tableName, String userName) {
		update("delete from " + tableName + " where user_name = ?", userName);
	}

	/**
	 * 该商家是否有上架商品
	 */
	private boolean hasProd(Long shopId) {
		int result = this.get("select count(*) from ls_prod where status = 1 and shop_id = ?", Integer.class, shopId);
		return result > 0;
	}

	@Override
	public ShopDetail getShopDetailNoCacheByShopId(Long shopId) {
		return getById(shopId);
	}

	@Override
	public BusinessMessageDTO getBusinessMessage(Long shopId) {
		String sql = "select lsd.type,lsd.business_hours,lsd.enterprise_name ,lsd.register_number ,lsd.register_amount ," +
				"lsd.representative ,lsd.business_address ,lsd.available_time ,lsd.business_range,lsd.business_image " +
				"from ls_shop_detail lsd where lsd.shop_id = ?";
		return get(sql,BusinessMessageDTO.class,shopId);
	}


}
