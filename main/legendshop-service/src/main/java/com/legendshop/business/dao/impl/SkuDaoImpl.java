/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.model.entity.Sub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SkuDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.entity.Sku;
import com.legendshop.util.AppUtils;
/**
 * The Class SkuDaoImpl.
 */
@Repository("skuDao")
public class SkuDaoImpl extends GenericDaoImpl<Sku, Long> implements SkuDao {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SkuDaoImpl.class);

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Cacheable(value = "Sku", key = "#id")
	public Sku getSku(Long id){
		return getById(id);
	}
	
	@CacheEvict(value = "Sku", key = "#sku.skuId")
    public void deleteSku(Sku sku){
    	delete(sku);
    }
	
	public Long saveSku(Sku sku){
		return (Long)save(sku);
	}
	
	@CacheEvict(value = "Sku", key = "#sku.skuId")
	public void updateSku(Sku sku){
		 update(sku);
	}
	
	public PageSupport<Sku> getSku(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public void saveSku(List<Sku> skuList) {
		save(skuList);
	}

	@Override
	public List<Sku> getSkuByProd(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}
	
	@Override
	public List<Sku> getSkuByProdId(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}
	
	@Override
	public List<Sku> getSku(Long [] skuId) {
		if(AppUtils.isBlank(skuId)){
			return new ArrayList<>();
		}
		StringBuilder sql = new StringBuilder("select prod_id as prodId,sku_id as skuId,properties as properties,user_properties as userProperties,status as status,price as price,name as name,pic as pic,cn_properties as cnProperties,stocks as stocks from ls_sku  where sku_id in ( ");
		for (int i=0;i < skuId.length;i++) {
			sql.append("?,");
		}
		sql.setLength(sql.length()-1);
		sql.append(")");
		return this.query(sql.toString(), Sku.class, skuId);
	}
	
	
	/**
	 * 清除商品缓存
	 */
	@CacheEvict(value = "SkuList", key = "#prodId")
	public  void clearSkuByProd(Long prodId) {
		log.debug("Clear Sku cache bt prodId {}", prodId);
	}
	
	/**
	 * 清除SKU缓存
	 * @param skuId
	 */
	@CacheEvict(value = "Sku", key = "#skuId")
	public void cleanSkuCache(Long skuId){
		
	}

	@Override
	public Integer getStocksByLockMode(Long skuId) {
		return get("select stocks from ls_sku where sku_id = ? for update", Integer.class, skuId);
	}
	
	/**
	 * SKU的实际库存
	 * @param skuId
	 * @return
	 */
	@Override
	public Integer getActualStocksByLockMode(Long skuId) {
		return get("select actual_stocks from ls_sku where sku_id = ? for update", Integer.class, skuId);
	}
	
	/**
	 * 更新库存
	 * 此方法为按最终结果更新，有可能存在并发更新问题
	 */
	@Override
	@CacheEvict(value = "Sku", key = "#skuId")
	public void updateSkuStocks(Long skuId, long stocks) {
		update("update ls_sku set stocks = ? where sku_id = ?", stocks, skuId);
	}

	/**
	 * 只更新库存变化的数量，应不存在并发问题
	 * @param skuId
	 * @param stocks
	 */
	@Override
	@CacheEvict(value = "Sku", key = "#skuId")
	public void updateSkuStocksByRealTime(Long skuId, long stocks) {
		update("update ls_sku set stocks = stocks + ? where sku_id = ?", stocks, skuId);
	}

	@Override
	@CacheEvict(value = "Sku", key = "#skuId")
	public void updateSkuActualStocks(Long skuId, long stocks) {
		update("update ls_sku set actual_stocks = ? where sku_id = ?", stocks, skuId);
	}

	@Override
	public Sku getSkuByProd(Long prodId, Long skuId) {
		return getByProperties(new EntityCriterion().eq("prodId", prodId).eq("skuId", skuId));
	}

	@Override
	public Integer getStocksByMode(Long skuId) {
		return get("select stocks from ls_sku where sku_id = ? ", Integer.class, skuId);
	}


	@Override
	public BasketDto findBasketDtos(Long prodId,Long skuId) {
		String sqlString="SELECT prod.prod_id AS prodId,prod.pic AS pic,prod.carriage AS carriage,prod.user_name AS shopName,prod.shop_id AS shopId, sku.sku_id AS skuId,sku.stocks AS stocks," +
				"sku.name AS productName,sku.cn_properties AS cnProperties,sku.price AS cash,prod.cash AS price,sku.status as status FROM ls_prod prod LEFT JOIN ls_sku sku ON prod.prod_id=sku.prod_id " +
				" WHERE prod.prod_id=? and sku.sku_id = ? ";
		BasketDto basketDto=get(sqlString, new Object[]{prodId, skuId}, new RowMapper<BasketDto>() {
			@Override
			public BasketDto mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				BasketDto basketDto=new BasketDto();
				basketDto.setProdId(rs.getLong("prodId"));
				basketDto.setPic(rs.getString("pic"));
				basketDto.setCarriage(rs.getDouble("carriage"));
				basketDto.setShopName(rs.getString("shopName"));
				basketDto.setShopId(rs.getLong("shopId"));
				basketDto.setSkuId(rs.getLong("skuId"));
				basketDto.setStocks(rs.getInt("stocks"));
				basketDto.setStatus(rs.getInt("status"));
				StringBuffer buffer=new StringBuffer(rs.getString("productName"));
				if(AppUtils.isNotBlank(rs.getString("cnProperties"))){
					buffer.append("-").append(rs.getString("cnProperties"));
				}
				basketDto.setProductName(buffer.toString());
				basketDto.setPrice(rs.getDouble("price"));
				basketDto.setCash(rs.getDouble("cash"));
				return basketDto;
			}
		});
		return basketDto;
	}

	@Override
	public PageSupport<Sku> getSkuPage(String curPageNO, Sku sku) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class, curPageNO);
        //cq.setPageSize(propertiesUtil.getPageSize());
        //cq = hasAllDataFunction(cq, request, StringUtils.trim(sku.getUserName()));
        /*
           //TODO add your condition
        */
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> getAuctionProdSku(String curPageNO, Long prodId) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class,curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status",1);
		cq.eq("skuType", SkuActiveTypeEnum.PRODUCT.value());
		cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> queryPage(String curPageNO, Long prodId, int status, int stocks) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class,curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status",1);
		cq.ge("stocks",1);
		cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> getseckillProdSku(String curPageNO, Long prodId) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class,curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status", 1);
		cq.ge("stocks", 1);
		cq.eq("skuType", SkuActiveTypeEnum.PRODUCT.value());
		cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> getSku(String curPageNO, Long prodId, int status) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class, curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status", status);
		//这里不需要做分页，但返回对象是分页对象，设置一个每页显示的值，这里为1000
		cq.setPageSize(1000);

//		PageSupport<Sku> result = new PageSupport<Sku>();
//		List<Sku> skuList = queryByProperties(new EntityCriterion().eq("prodId", prodId).eq("status", status));
//		result.setResultList(skuList);
//		return result;
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<Sku> getloadProdSkuPage(String curPageNO, Long prodId) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class,curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status",1);
		cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> getLoadProdSkuByProdId(String curPageNO, Long prodId) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class,curPageNO);
		cq.eq("prodId", prodId);
		cq.eq("status",1);
		cq.eq("skuType", SkuActiveTypeEnum.PRODUCT.value());  //只显示未参加营销活动的商品
		cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sku> loadSkuListPage(String curPageNO, Long prodId, String name) {
		CriteriaQuery cq = new CriteriaQuery(Sku.class, curPageNO);
		cq.eq("prodId", prodId);
		cq.like("name", name,MatchMode.ANYWHERE);
		cq.setPageSize(7);
		return queryPage(cq);
	}

	@Override
	public List<Sku> findSkuByPropId(Long propId) {
		List<Sku> list = queryByProperties(new EntityCriterion().like("properties", propId.toString(), MatchMode.ANYWHERE));
		return list;
	}

	@Override
	public List<Sku> findSkuByProp(String prop) {
		List<Sku> list = queryByProperties(new EntityCriterion().like("properties", prop, MatchMode.ANYWHERE));
		return list;
	}

	@Override
	public List<Sku> findSkuByPropIdAndProdTypeId(Long propId, Long prodTypeId) {
		return queryByProperties(new EntityCriterion().like("properties", prodTypeId+":"+propId, MatchMode.ANYWHERE));
	}

	@Override
	public List<Sku> getSkuForMergeGroup(Long prodId, Long mergeGroupId) {
		String sql = "SELECT s.*,m.merge_price AS mergePrice FROM ls_sku s INNER JOIN ls_merge_product m ON m.sku_id = s.sku_id WHERE m.prod_id = ? AND m.merge_id = ?";
		return query(sql, Sku.class, prodId,mergeGroupId);
	}

	@Override
	public Sku getSkuById(Long skuId) {
		return getByProperties(new EntityCriterion().eq("skuId", skuId));
	}

	/**
	 * 更新sku所占用的活动
	 */
	@Override
	public int updateSkuTypeById(Long skuId, String skuType, String originSkuType) {
		String sql = "UPDATE ls_sku SET sku_type = ? WHERE sku_id = ? and sku_type = ?";
		return update(sql, skuType, skuId, originSkuType);
	}

	@Override
	public void updateSkuTypeByProdId(Long productId, String skuType, String originSkuType ) {
		String sql = "UPDATE ls_sku SET sku_type = ? WHERE prod_id = ?  and sku_type = ?";
		update(sql, skuType, productId, originSkuType);
	}

	@Override
	public void updateSkuAllStocks(Long skuId, Long basketCount) {
		update("UPDATE ls_sku SET stocks = stocks + ?, actual_stocks = actual_stocks + ? WHERE sku_id = ? ", basketCount, basketCount, skuId);
	}
}
