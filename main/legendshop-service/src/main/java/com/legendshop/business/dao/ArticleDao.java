/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Article;

/**
 * The Class ArticleDao. Dao接口
 */
public interface ArticleDao extends Dao<Article, Long> {

	/**
	 * 根据Id获取
	 */
	public abstract Article getArticle(Long id);

	/**
	 * 删除
	 */
	public abstract int deleteArticle(Article article);

	/**
	 * 保存
	 */
	public abstract Long saveArticle(Article article);

	/**
	 * 更新
	 */
	public abstract int updateArticle(Article article);
	/**
	 * 修改点赞数量
	 */
	public int updateThumbNum(Long id);
	
	/**
	 * 更新评论数量
	 */
	public abstract int updateCommentNum(Long artId);

	PageSupport<Article> query(String curPageNO);

	PageSupport<Article> getArticleList(String curPageNO);

	public abstract PageSupport<Article> getArticlePage(String curPageNO, Article article);

}
