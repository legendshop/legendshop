/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.MergeProduct;

/**
 * The Class MergeProductDao.
 * 拼团活动参加商品Dao接口
 */
public interface MergeProductDao extends Dao<MergeProduct, Long> {

   	/**
	 *  根据Id获取拼团活动参加商品
	 */
	public abstract MergeProduct getMergeProduct(Long id);
	
   /**
	 *  删除拼团活动参加商品
	 */
    public abstract int deleteMergeProduct(MergeProduct mergeProduct);
    
   /**
	 *  保存拼团活动参加商品
	 */	
	public abstract Long saveMergeProduct(MergeProduct mergeProduct);

   /**
	 *  更新拼团活动参加商品
	 */		
	public abstract int updateMergeProduct(MergeProduct mergeProduct);
	
	public abstract void batchSaveMergeProduct(List<MergeProduct> mergeProductList);

	/***
	 * 获取某个拼团的拼团商品
	 * @param mergeId
	 * @return
	 */
	public abstract List<MergeProduct> getMergeProductList(Long mergeId);

	/**
	 * 根据id删除拼图商品
	 * @param id
	 */
    void deleteMergeProductBymergeId(Long id);
}
