/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MobileFloorItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloorItem;
import com.legendshop.spi.service.MobileFloorItemService;
import com.legendshop.util.AppUtils;

/**
 * 手机端html5 楼层内容关联服务.
 */
@Service("mobileFloorItemService")
public class MobileFloorItemServiceImpl  implements MobileFloorItemService{
	
	@Autowired
    private MobileFloorItemDao mobileFloorItemDao;

    public MobileFloorItem getMobileFloorItem(Long id) {
        return mobileFloorItemDao.getMobileFloorItem(id);
    }

    public void deleteMobileFloorItem(MobileFloorItem mobileFloorItem) {
        mobileFloorItemDao.deleteMobileFloorItem(mobileFloorItem);
    }

    public Long saveMobileFloorItem(MobileFloorItem mobileFloorItem) {
        if (!AppUtils.isBlank(mobileFloorItem.getId())) {
            updateMobileFloorItem(mobileFloorItem);
            return mobileFloorItem.getId();
        }
        return mobileFloorItemDao.saveMobileFloorItem(mobileFloorItem);
    }

    public void updateMobileFloorItem(MobileFloorItem mobileFloorItem) {
        mobileFloorItemDao.updateMobileFloorItem(mobileFloorItem);
    }

	@Override
	public List<MobileFloorItem> queryMobileFloorItem(Long parentId) {
		return mobileFloorItemDao.queryMobileFloorItem(parentId);
	}

	@Override
	public Long getCountMobileFloorItem(Long parentId) {
		return mobileFloorItemDao.getCountMobileFloorItem(parentId);
	}

	@Override
	public PageSupport<MobileFloorItem> getMobileFloorItem(String curPageNO, String title, Long id) {
		return mobileFloorItemDao.getMobileFloorItem(curPageNO,title,id);
	}

}
