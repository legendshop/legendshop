/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.InvoiceSub;

/**
 * 发票订单信息Dao.
 */
public interface InvoiceSubDao extends Dao<InvoiceSub, Long> {
     
	public abstract InvoiceSub getInvoiceSub(Long id);
	
    public abstract int deleteInvoiceSub(InvoiceSub invoiceSub);
	
	public abstract Long saveInvoiceSub(InvoiceSub invoiceSub);
	
	public abstract int updateInvoiceSub(InvoiceSub invoiceSub);
	
 }
