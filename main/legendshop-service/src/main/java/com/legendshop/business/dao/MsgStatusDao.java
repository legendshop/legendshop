/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.MsgStatus;

/**
 * The Interface MsgStatusDao.
 */
public interface MsgStatusDao extends GenericDao<MsgStatus, Long> {
	
	public MsgStatus getSystemMsgStatus(String userName, Long msgId);

	public void saveMsgStatus(MsgStatus msgStatus);

	public void updateMsgStatus(MsgStatus msgStatus);
}
