package com.legendshop.business.resolver;

import java.util.List;

import org.springframework.stereotype.Component;

import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.spi.resolver.order.IActiveRuleResolver;
import com.legendshop.util.AppUtils;



/**
 * 限时折扣
 * @author Tony
 */
@Component
public class XianShiRuleResolver  implements IActiveRuleResolver {

	/**
	 * 
	 * 执行限时折扣
	 * 
	 */
	@Override
	public void execute(MarketingDto marketingDto, RuleResolver item,MarketRuleContext context) {
		if(context.isXianShiExecuted(item.getProdId(),item.getSkuId())
				|| context.isManJianExecuted(item.getProdId(), item.getSkuId())
						|| context.isManZeExecuted(item.getProdId(), item.getSkuId())){
			return ;
		}
		
		if(marketingDto.getIsAllProds() != 1){//部分,非全场 
			MarketingProdDto marketingProdDto=context.filterMarketing(marketingDto.getMarketingProdDtos(),item.getProdId(),item.getSkuId());
			if(AppUtils.isBlank(marketingProdDto))
				return ;
		}
		
		List<MarketingRuleDto> ruleDtos= marketingDto.getMarketingRuleDtos();
		if(AppUtils.isNotBlank(ruleDtos)){
			MarketingRuleDto ruleDto=ruleDtos.get(0);
			CartMarketRules rules=new CartMarketRules();
			rules.setMarketId(marketingDto.getMarketId());
			rules.setMarketType(marketingDto.getType());
			rules.setRuleId(ruleDto.getRuleId());
			StringBuilder sb=new StringBuilder(100);
			
			sb.append("限时折扣活动").append(" [ ").append("打").append(ruleDto.getOffDiscount()).append("折").append("]");
			
			rules.setPromotionInfo(sb.toString());
			item.addCartMarketRules(rules);
			context.executeXianShi(item.getProdId(), item.getSkuId());
		}
	}


}
