/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminDoc;

/**
 * The Class AdminDocDao.
 * Dao接口
 */
public interface AdminDocDao extends Dao<AdminDoc, Long> {
	/**
	 *  根据商城获取列表
	 */
    public abstract List<AdminDoc> getAdminDoc(String shopName);

   	/**
	 *  根据Id获取
	 */
	public abstract AdminDoc getAdminDoc(Long id);
	
   /**
	 *  删除
	 */
    public abstract int deleteAdminDoc(AdminDoc adminDoc);
    
   /**
	 *  保存
	 */	
	public abstract Long saveAdminDoc(AdminDoc adminDoc);

   /**
	 *  更新
	 */		
	public abstract int updateAdminDoc(AdminDoc adminDoc);
	
   /**
	 *  查询列表
	 */		
	public abstract PageSupport getAdminDoc(CriteriaQuery cq);

	public abstract PageSupport<AdminDoc> queryAdminDocList(String curPageNO, AdminDoc adminDoc);
	
 }
