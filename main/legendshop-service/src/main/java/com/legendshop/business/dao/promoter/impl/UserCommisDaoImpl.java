/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter.impl;

import com.legendshop.business.dao.promoter.UserCommisDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.promotor.SubLevelUserDto;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * 用户佣金DaoImpl.
 * 
 */
@Repository
public class UserCommisDaoImpl extends GenericDaoImpl<UserCommis, String> implements UserCommisDao {

	private static final String getSubUserCountSQL = "SELECT (SELECT COUNT(1) FROM ls_user_commis WHERE parent_user_name =?) AS firstCount,"
			+ "(SELECT COUNT(1) FROM ls_user_commis WHERE parent_two_user_name =?) AS secondCount,"
			+ "(SELECT COUNT(1) FROM ls_user_commis WHERE parent_three_user_name =?) AS thirdCount " + "FROM DUAL";

	/**
	 * 加载下级用户列表 
	 */
	public UserCommis getUserCommis(String userId) {
		return getById(userId);
	}

	/**
	 * 根据用户名查找下级用户
	 */
	@Override
	public UserCommis getUserCommisByName(String userName) {
		return this.getByProperties(new EntityCriterion().eq("userName", userName));
	}

	/**
	 * 删除用户佣金 
	 */
	public int deleteUserCommis(UserCommis userCommis) {
		return delete(userCommis);
	}

	/**
	 * 保存用户佣金 
	 */
	public String saveUserCommis(UserCommis userCommis) {
		return save(userCommis);
	}

	/**
	 * 更新用户佣金 
	 */
	public int updateUserCommis(UserCommis userCommis) {
		return update(userCommis);
	}

	/**
	 * 加载下级用户列表 
	 */
	public PageSupport getUserCommis(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/**
	 * 查询各级用户数量 
	 */
	@Override
	public SubUserCountDto getSubUserCount(String userName) {
		return this.get(getSubUserCountSQL, SubUserCountDto.class, userName, userName, userName);
	}

	/**
	 * 查询本月发展会员数 
	 */
	@Override
	public Long getMonthUser(Date startTime, Date endTime, String userName) {
		return this.get("SELECT COUNT(user_id) FROM ls_user_commis WHERE parent_user_name=? AND  parent_binding_time>=? AND parent_binding_time<=?", Long.class,
				userName, startTime, endTime);
	}

	/**
	 * 根据用户ID更新已结算佣金 
	 */
	@Override
	public int updateSettledDistCommis(Double updateValue, Double originalValue, String userId) {

		return update("update ls_user_commis set settled_dist_commis = ? " + "where user_id = ? and settled_dist_commis = ? ",
				new Object[] { updateValue, userId, originalValue });
	}

	/**
	 * 保存预存款变更记录 
	 */
	@Override
	public void savePdCashLog(PdCashLog pdCashLog) {
		String sql = "INSERT INTO ls_pd_cash_log values(?,?,?,?,?,?,?,?)";
		this.update(pdCashLog.getUserId(), pdCashLog.getUserName(), pdCashLog.getSn(), pdCashLog.getLogType(), pdCashLog.getAmount(), pdCashLog.getLogDesc(),
				pdCashLog.getAddTime());
	}

	/**
	 * 增加 结算佣金 
	 */
	@Override
	public void addSettleDistCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set settled_dist_commis = settled_dist_commis + ? where user_name=? ", commisCash, userName);
	}

	/**
	 * 减少 未结算佣金 
	 */
	@Override
	public void reduceUnSettleDistCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set unsettle_dist_commis = unsettle_dist_commis - ? where user_name=? ", commisCash, userName);
	}

	/**
	 * 增加直接下级贡献总金额 
	 */
	@Override
	public void addFirstDistCommis(String userName, Double firstDistCommis) {
		this.update("update ls_user_commis set first_dist_commis = first_dist_commis + ? where user_name=? ", firstDistCommis, userName);
	}

	/**
	 * 增加下两级贡献总金额 
	 */
	@Override
	public void addSecondDistCommis(String userName, Double secondDistCommis) {
		this.update("update ls_user_commis set second_dist_commis = second_dist_commis + ? where user_name=? ", secondDistCommis, userName);
	}

	/**
	 * 增加下三级贡献总金额 
	 */
	@Override
	public void addThirdDistCommis(String userName, Double thirdDistCommis) {
		this.update("update ls_user_commis set third_dist_commis = third_dist_commis + ? where user_name=? ", thirdDistCommis, userName);
	}

	/**
	 * 增加 累计获得的佣金 
	 */
	@Override
	public void addTotalDistCommis(String userName, Double totalDistCommis) {
		this.update("update ls_user_commis set total_dist_commis = total_dist_commis + ? where user_name=? ", totalDistCommis, userName);
	}

	/**
	 * 增加 未结算佣金 
	 */
	@Override
	public void addUnsettleDistCommis(String userName, Double unsettleDistCommis) {
		this.update("update ls_user_commis set unsettle_dist_commis = unsettle_dist_commis + ? where user_name=? ", unsettleDistCommis, userName);
	}

	/**
	 * 获取 我推广的会员 
	 */
	@Override
	public PageSupport<UserCommis> getUserCommisByName(String curPageNO, String userName, UserCommis userCommis, int pageSize) {

		SimpleSqlQuery query = new SimpleSqlQuery(UserCommis.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("parentUserName", userName);
		map.put("parentStartBindingTime", userCommis.getStartParentBindingTime());
		map.put("parentEndBindingTime", userCommis.getEndParentBindingTime());
		if (AppUtils.isNotBlank(userCommis.getUserName())) {
			map.like("userName", "%" + userCommis.getUserName() + "%");
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("userCommis.queryCount", map);
		String querySQL = ConfigCode.getInstance().getCode("userCommis.query", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	/**
	 * 获取推广员列表数据 
	 */
	@Override
	public PageSupport<UserCommis> getUserCommis(String curPageNO, UserCommis userCommis) {
		SimpleSqlQuery query = new SimpleSqlQuery(UserCommis.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(userCommis.getUserName())) {			
			map.like("userName", userCommis.getUserName().trim());
		}
		map.put("startApplyTime", userCommis.getStartApplyTime());
		map.put("endApplyTime", userCommis.getEndApplyTime());
		map.put("startPromoterTime", userCommis.getStartPromoterTime());
		map.put("endPromoterTime", userCommis.getEndPromoterTime());
		map.put("promoterSts", userCommis.getPromoterSts());
		if(AppUtils.isNotBlank(userCommis.getUserMobile())) {			
			map.like("userMobile", userCommis.getUserMobile().trim());
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("promoter.queryUserCommisCount", map);
		String querySQL = ConfigCode.getInstance().getCode("promoter.queryUserCommis", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 加载下级用户列表 
	 */
	@Override
	public PageSupport<SubLevelUserDto> getUserCommis(String curPageNO, String level, String userName, String nickName) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		SimpleSqlQuery query = new SimpleSqlQuery(SubLevelUserDto.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		if ("first".equals(level)) {// 查找直接下级
			map.put("firstUserName", userName);
		} else if ("second".equals(level)) {// 查找下两级
			map.put("secondUserName", userName);
		} else if ("third".equals(level)) {// 查找下三级
			map.put("thirdUserName", userName);
		} else {
			map.put("firstUserName", userName);
		}
		map.put("nickName", nickName);
		
		String queryAllSQL = ConfigCode.getInstance().getCode("promoter.getUserCommisCount", map);
		String querySQL = ConfigCode.getInstance().getCode("promoter.getUserCommis", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public Integer batchCloseCommis(String userIdStr) {
		String[] userIdList=userIdStr.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("UPDATE ls_user_commis SET commis_sts=0 WHERE user_id IN(");
		for(String userId:userIdList){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(),userIdList);
	}

	@Override
	public void reduceUnSettleDistCommisAndTotalCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set total_dist_commis = total_dist_commis - ?, unsettle_dist_commis = unsettle_dist_commis - ? where user_name=? ", commisCash, commisCash, userName);
	}

	@Override
	public void reduceFirstDistCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set first_dist_commis = first_dist_commis - ? where user_name=? ", commisCash, userName);
	}

	@Override
	public void reduceSecondDistCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set second_dist_commis = second_dist_commis - ? where user_name=? ", commisCash, userName);
	}

	@Override
	public void reduceThirdDistCommis(String userName, Double commisCash) {
		this.update("update ls_user_commis set third_dist_commis = third_dist_commis - ? where user_name=? ", commisCash, userName);
	}

	@Override
	public UserCommis getUserCommisByUserId(String userId) {
		
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}

}
