/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.ProdGroup;

/**
 * The Class ProdGroupDao. Dao接口
 */
public interface ProdGroupDao extends Dao<ProdGroup,Long>{

	/**
	 * 根据Id获取
	 */
	public abstract ProdGroup getProdGroup(Long id);

	/**
	 *  根据Id删除
	 */
    public abstract int deleteProdGroup(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteProdGroup(ProdGroup prodGroup);

	/**
	 * 保存
	 */
	public abstract Long saveProdGroup(ProdGroup prodGroup);

	/**
	 *  更新
	 */		
	public abstract int updateProdGroup(ProdGroup prodGroup);

	/**
	 * 分页查询列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<ProdGroup> queryProdGroup(String curPageNO, Integer pageSize);
	
	public abstract List<ProdGroup> getProdGroupByType(Integer type);
	
	public abstract PageSupport<ProdGroup> queryProdGroupPage(String curPageNO, ProdGroup prodGroup,Integer pageSize);

	public abstract boolean checkNameIsExist(String name, Long id);

}
