/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ImgFileDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.Product;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 
 * 产品图片服务
 */
@Service("imgFileService")
public class ImgFileServiceImpl implements ImgFileService {

	@Autowired
	private ImgFileDao imgFileDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private AttachmentManager attachmentManager;

	@Override
	public ImgFile getImgFileById(Long id) {
		return imgFileDao.getById(id);
	}

	@Override
	public Product getProd(Long id) {
		return productDao.getProduct(id);
	}

	@Override
	public void delete(Long id) {
		imgFileDao.deleteImgFileById(id);
	}

	@Override
	public Long save(ImgFile imgFile) {
		if (!AppUtils.isBlank(imgFile.getFileId())) {
			update(imgFile);
			return imgFile.getFileId();
		}
		return (Long) imgFileDao.save(imgFile);
	}

	@Override
	public void update(ImgFile imgFile) {
		imgFileDao.updateImgFile(imgFile);
	}


	@Override
	public List<ImgFile> getProductPics(Long prodId) {
		return imgFileDao.getAllProductPics(prodId);
	}

	@Override
	public List<String> queryProductPics(Long prodId) {
		return imgFileDao.queryProductPics(prodId);
	}

	@Override
	public List<ImgFile> getAllProductPics(Long prodId) {
		return imgFileDao.getAllProductPics(prodId);
	}

	@Override
	public PageSupport<ImgFile> getImgFileList(String curPageNO, Long productId, ImgFile imgFile) {
		return imgFileDao.getImgFileList(curPageNO,productId,imgFile);
	}

	@Override
	public Long saveImgFile(String userName, String userId, Long shopId, ImgFile imgFile,String filePath) {
		//保存附件表
		attachmentManager.saveImageAttachment(userName, userId, shopId, filePath, imgFile.getFile(), AttachmentTypeEnum.IMGFILE);
		imgFile.setFilePath(filePath);
		imgFile.setFileSize(new Integer((int) imgFile.getFile().getSize()));
		imgFile.setFileType(filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase());
		imgFile.setProductType((short) 1);
		imgFile.setStatus(Constants.ONLINE);
		imgFile.setUpoadTime(new Date());
		imgFile.setUserName(userName);
		imgFile.setShopId(shopId);
		Long id = save(imgFile);
		return id;
	}
}
