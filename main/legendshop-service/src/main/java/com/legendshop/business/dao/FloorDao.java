/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;

/**
 * 首页楼层Dao
 */
public interface FloorDao extends GenericDao<Floor, Long>{
     
	public abstract Floor getFloor(Long id);
	
    public abstract void deleteFloor(Floor floor);
	
	public abstract Long saveFloor(Floor floor);
	
	public abstract void updateFloor(Floor floor);
	
	public abstract boolean checkPrivilege(Long floorId);

	public abstract List<Floor> getActiveFloor();
	
	public abstract void deleteSubFloors(Long flId);

	public Integer getLeftSwitch(Long floorId);

	/**
	 * 更新楼层缓存
	 * @param floor
	 */
	public abstract void updateFloorCache(Floor floor);

	public abstract PageSupport<Floor> getFloorPage(String curPageNO, Floor floor);

	public abstract Integer batchOffline(String floorIdStr);
 }
