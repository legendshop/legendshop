/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DvyTypeCommStatDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ProductCommentStat;

/**
 * 物流评分统计服务.
 */
@Repository
public class DvyTypeCommStatDaoImpl extends GenericDaoImpl<DvyTypeCommStat, Long> implements DvyTypeCommStatDao  {

	public DvyTypeCommStat getDvyTypeCommStat(Long id){
		return getById(id);
	}
	
	@Override
	@Cacheable(value = "DvyTypeCommStat", key = "#shopId")
	public DvyTypeCommStat getDvyTypeCommStatByShopId(Long shopId) {
		String sql="SELECT SUM(s.score) AS score,SUM(s.count) AS count FROM ls_dvy_type_comm_stat s INNER JOIN ls_sub t ON s.dvy_type_id=t.dvy_type_id WHERE t.shop_id=?";
		return this.get(sql, DvyTypeCommStat.class, shopId);
	}
	
    public int deleteDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat){
    	return delete(dvyTypeCommStat);
    }
	
	public Long saveDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat){
		return save(dvyTypeCommStat);
	}
	
	public int updateDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat){
		return update(dvyTypeCommStat);
	}
	
	public PageSupport getDvyTypeCommStat(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public DvyTypeCommStat getDvyTypeCommStatByDvyTypeIdForUpdate(Long dvyTypeId) {
		
		String sql = "SELECT * FROM ls_dvy_type_comm_stat WHERE dvy_type_id = ? FOR UPDATE";
		
		return this.get(sql, DvyTypeCommStat.class, dvyTypeId);
	}
	
 }
