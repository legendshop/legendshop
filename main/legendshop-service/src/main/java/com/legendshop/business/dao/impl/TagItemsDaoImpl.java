/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TagItemsDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.dto.NewsTagDto;
import com.legendshop.model.dto.TagNewsDto;
import com.legendshop.model.entity.Tag;
import com.legendshop.model.entity.TagItems;
import com.legendshop.util.AppUtils;

/**
 *挑选的产品和内容块的对应关系Dao
 */
@Repository
public class TagItemsDaoImpl extends GenericDaoImpl<TagItems, Long> implements TagItemsDao  {
     
    public List<TagItems> getTagItems(String code){
    	List<TagItems> items=query("SELECT item_id AS itemId,item_name AS itemName FROM ls_tag_items LEFT JOIN ls_tag ON ls_tag.tag_id=ls_tag_items.tag_id WHERE ls_tag.code=?", TagItems.class, code);
    	return items;
    }

	public TagItems getTagItems(Long id){
		return getById(id);
	}
	
    public int deleteTagItems(TagItems tagItems){
    	return delete(tagItems);
    }
	
	public Long saveTagItems(TagItems tagItems){
		return save(tagItems);
	}
	
	public int updateTagItems(TagItems tagItems){
		return update(tagItems);
	}

	@Override
	public TagItems getItemsByTagId(Long TagId) {
		return get("select * from ls_tag_items where tag_id = ?",TagItems.class,TagId);
	}

	@Override
	public List<TagItems> queryTagItemsList(List<Tag> tagList) {
		if(AppUtils.isBlank(tagList)){
			return null;
		}
		
		StringBuffer querySQL = new StringBuffer("select ti.* from ls_tag t, ls_tag_items ti where t.tag_id = ti.tag_id and t.tag_id in(");
		for(int i=0;i<tagList.size();i++){
			querySQL.append("?,");
		}
		querySQL.setLength(querySQL.length()-1);
		querySQL.append(")");
		Object[] tagIds = new Long[tagList.size()]; 
		for (int i = 0; i < tagIds.length; i++) {
			tagIds[i] = tagList.get(i).getTagId();
		}
		
		List<TagItems> items = query(querySQL.toString(),TagItems.class,tagIds);
		return items;
	}

	@Override
	public void deleteTagItems(Long itemId) {
		deleteById(itemId);
	}

	@Override
	public List<NewsTagDto> getNewsTagDto(String type) {
		String sql="SELECT ls_news.*,ls_tag_items.item_name,ls_tag_items.item_id FROM ls_tag_item_news LEFT JOIN ls_news ON ls_tag_item_news.news_id=ls_news.news_id LEFT JOIN ls_tag_items ON ls_tag_items.item_id=ls_tag_item_news.tag_item_id LEFT JOIN ls_tag ON ls_tag.tag_id=ls_tag_items.tag_id WHERE ls_news.status=1 and ls_tag.type=? ORDER BY ls_tag_items.item_id,ls_news.seq";
		return this.query(sql, new Object[]{type}, new NewsTagRowMapper());
	}
	
	class NewsTagRowMapper implements RowMapper<NewsTagDto> {
		@Override
		public NewsTagDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			NewsTagDto tagDto=new NewsTagDto();
			tagDto.setItemId(rs.getLong("item_id"));
			tagDto.setItemName(rs.getString("item_name"));
			NewsDto newsDto=new NewsDto();
			newsDto.setNewsId(rs.getLong("news_id"));
			newsDto.setNewsTitle(rs.getString("news_title"));
			tagDto.setNewsDto(newsDto);
			return tagDto;
		}
	}

	@Override
	public List<TagNewsDto> getTagNewsDto(String type) {
		String sql="SELECT ls_tag_items.item_name AS itemName,ls_tag_items.item_id AS itemId,ls_news.news_id AS newsId,ls_news.news_title AS newsTitle FROM ls_tag_items LEFT JOIN ls_tag ON ls_tag_items.tag_id=ls_tag.tag_id LEFT JOIN ls_tag_item_news ON ls_tag_items.item_id=ls_tag_item_news.tag_item_id LEFT JOIN ls_news ON ls_tag_item_news.news_id=ls_news.news_id WHERE ls_tag.type=?";
		return query(sql, TagNewsDto.class, type);
	}

	@Override
	public Long getTagNewsDtoCount(String type) {
		String sql="SELECT COUNT(1) FROM ls_tag_items LEFT JOIN ls_tag ON ls_tag_items.tag_id=ls_tag.tag_id LEFT JOIN ls_tag_item_news ON ls_tag_items.item_id=ls_tag_item_news.tag_item_id LEFT JOIN ls_news ON ls_tag_item_news.news_id=ls_news.news_id WHERE ls_tag.type=?";
		return getLongResult(sql, type);
	}

	@Override
	public List<TagItems> queryTagItemsListByType(String type) {
		String sql="SELECT ls_tag_items.* FROM ls_tag_items LEFT JOIN ls_tag ON ls_tag.tag_id=ls_tag_items.tag_id WHERE ls_tag.type=?";
		return query(sql, TagItems.class, type);
	}

	@Override
	public void saveTagItemsNews(Long newsid, Long itemid) {
		String sql="INSERT INTO ls_tag_item_news(news_id,tag_item_id) VALUES(?,?)";
		jdbcTemplate.update(sql,new Object[]{newsid,itemid});
	}

	@Override
	public void delTagItemsNews(Long newsid, Long itemid) {
		String sql="DELETE FROM ls_tag_item_news WHERE ls_tag_item_news.news_id=? AND ls_tag_item_news.tag_item_id=?";
		jdbcTemplate.update(sql,new Object[]{newsid,itemid});
	}
 }
