/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ScheduleList;

/**
 * 任务调度服务表Dao接口
 */
public interface ScheduleListDao extends GenericDao<ScheduleList, Long> {
	/**
	 * 根据商城获取任务调度服务表列表
	 */
	public abstract List<ScheduleList> getNonexecutionScheduleList(Date triggerTime, Integer value);

	/**
	 * 删除任务调度服务表
	 */
	public abstract int deleteScheduleList(ScheduleList ScheduleList);

	/**
	 * 保存任务调度服务表
	 */
	public abstract List<Long> saveScheduleList(List<ScheduleList> scheduleLists);

	/**
	 * 
	 * @param shopId
	 * @return
	 */
	public abstract List<ScheduleList> getScheduleList(Long shopId, String scheduleSn,Integer scheduleType);
	
    /**
	 *  保存任务调度服务表
	*/	    
	public Long saveScheduleList(ScheduleList ScheduleList);

	/**
	 * 更新任务调度状态
	 * @param status
	 * @param id
	 */
	public abstract int updateScheduleListStatus(Long shopId,int status, Long id);

	/**
	 * 取消任务调度
	 */
	public abstract void cancelScheduleList(Long shopId, String schedule_sn, Integer schedule_type);


}
