/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.integral.IntegralProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.util.AppUtils;

/**
 * 积分商品Dao实现类.
 */
@Repository
public class IntegralProdDaoImpl extends GenericDaoImpl<IntegralProd, Long> implements IntegralProdDao {

	/** 获取积分商品 */
	public IntegralProd getIntegralProd(Long id) {
		return getById(id);
	}

	/** 删除取积分商品 */
	@CacheEvict(value = "IntegralProdDto", key = "#integralProd.id")
	public int deleteIntegralProd(IntegralProd integralProd) {
		return delete(integralProd);
	}

	/** 保存取积分商品 */
	public Long saveIntegralProd(IntegralProd integralProd) {
		return save(integralProd);
	}

	/** 更新取积分商品 */
	@CacheEvict(value = "IntegralProdDto", key = "#integralProd.id")
	public int updateIntegralProd(IntegralProd integralProd) {
		return update(integralProd);
	}

	/** 获取积分商品 */
	public PageSupport getIntegralProd(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/** The sql for get navigation nsort. */
	private static String sqlGetCategory = "select n.id as id, n.parent_id as parentId, n.name as name ,n.pic as pic ,n.status  as status, n.type_id as typeId,n.keyword as keyword,n.cat_desc as catDesc,n.title as title,n.grade as grade,n.seq as seq from ls_category n  where  n.status = 1 and  n.type= ? order by n.grade,n.seq";

	/** 获取积分商品分类 */
	@Override
	@Cacheable(value = "IntegralCategoryDtoList")
	public List<Category> findIntegralCategory() {
		List<Category> categories = query(sqlGetCategory, Category.class, ProductTypeEnum.INTEGRAL.value());
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		// 组装树
		List<Category> list = parseCategory(categories);
		return list;
	}

	/**
	 * 得到父节点
	 * 
	 * @param menuList
	 * @param menu
	 * @return
	 */
	private Category getParentMenu(Collection<Category> menuList, Category menu) {
		for (Category item : menuList) {
			if (item == null) {
				continue;
			}
			if (item.getId().equals(menu.getParentId())) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 按照3级菜单来组装
	 * 
	 * @param menuList
	 * @return
	 */
	private List<Category> parseCategory(List<Category> menuList) {
		Map<Long, Category> menuMap = new LinkedHashMap<Long, Category>();
		for (Iterator<Category> iterator = menuList.iterator(); iterator.hasNext();) {
			Category menu = (Category) iterator.next();
			if (AppUtils.isBlank(menu)) {
				continue;
			}
			if (menu.getGrade() == 1) { // for 顶级菜单
				menu.setChildrenList(new ArrayList<Category>());
				menuMap.put(menu.getId(), menu);
			} else if (menu.getGrade() == 2) { // 二级菜单
				// 拿到一级菜单先
				Category menuLevel1 = menuMap.get(menu.getParentId());
				if (AppUtils.isBlank(menuLevel1)) {
					continue;
				}
				menu.setParentName(menuLevel1.getName());
				menu.setParentId(menuLevel1.getId());
				menuLevel1.addChildren(menu);
			} else if (menu.getGrade() == 3) { // 三级菜单
				// 拿到二级菜单
				Category secondMenu = getParentMenu(menuList, menu);
				if (secondMenu != null) {

					menu.setParentName(secondMenu.getName());
					menu.setParentId(secondMenu.getId());
					// 拿到一级菜单先
					Category menuLevel1 = menuMap.get(secondMenu.getParentId());
					if (menuLevel1 == null) {
						continue; // 可能是由于上下线的关系
					}
					List<Category> menuLevel2 = menuLevel1.getChildrenList();
					for (Category menu2 : menuLevel2) {
						if (menu2.getId().equals(menu.getParentId())) {
							// 找到对应的二级菜单
							menu2.addChildren(menu);
							break;
						}
					}
				}
			}
		}
		return new ArrayList<Category>(menuMap.values());
	}

	/** 获取所有积分商品分类 */
	@Override
	@Cacheable(value = "AllIntegralCategoryDtoList")
	public List<Category> findAllIntegralCategory() {
		List<Category> categories = query(sqlGetCategory, Category.class, ProductTypeEnum.INTEGRAL.value());
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		return categories;
	}

	/** 获取金额分兑换列表 */
	@Override
	public List<IntegralProd> findExchangeList(Long firstCid, Long twoCid, Long thirdCid) {
		EntityCriterion criterion = new EntityCriterion().eq("status", "1");
		if (AppUtils.isNotBlank(firstCid)) {
			criterion.eq("firstCid", firstCid);
		}
		if (AppUtils.isNotBlank(twoCid)) {
			criterion.eq("twoCid", twoCid);
		}
		if (AppUtils.isNotBlank(thirdCid)) {
			criterion.eq("thirdCid", thirdCid);
		}
		criterion.gt("saleNum", 0);
		criterion.addDescOrder("saleNum").addDescOrder("viewNum");
		return this.queryByProperties(criterion, 0, 8);
	}

	/**
	 * 更新商品库存
	 */
	@Override
	@CacheEvict(value = "IntegralProdDto", key = "#id")
	public int updateStock(Integer count, Long id) {
		return update("update ls_integral_prod set prod_stock=prod_stock-?,sale_num=sale_num+? where id=? and (prod_stock-?)>=0 and prod_stock>0 and status=1",
				new Object[] { count, count, id, count });
	}

	/**
	 * 获取不同提供方的热门优惠券 
	 */
	@Override
	public List<Coupon> getCouponByProvider(String yType, String pType, Date date) {
		String sql;
		if (pType.equals(CouponProviderEnum.SHOP.value())) {
			sql = "SELECT c.*,s.site_name as shopName FROM ls_coupon c,ls_shop_detail s WHERE  c.status=1 and c.shop_id=s.shop_id AND c.get_type=? AND c.coupon_provider=? AND c.end_date>? ORDER BY c.bind_coupon_number desc";
		} else {
			sql = "SELECT c.* FROM ls_coupon c left join ls_category cp on c.category_id=cp.id where  c.status=1 and c.get_type=? AND c.coupon_provider=? AND c.end_date>? ORDER BY c.bind_coupon_number desc";
		}
		return this.queryLimit(sql, Coupon.class, 0, 3, yType, pType, date);
	}

	/**
	 * 获取热销积分商品
	 */
	@Override
	public List<IntegralProd> getHotIntegralProds() {
		String sql = "SELECT * FROM ls_integral_prod p WHERE p.status>0 ORDER BY p.sale_num desc,p.view_num desc";
		return this.queryLimit(sql, IntegralProd.class, 0, 15);
	}

	/**
	 * 获取优惠卷 
	 */
	@Override
	public Coupon getCoupon(Long cid) {
		String sql = "SELECT * FROM ls_coupon c WHERE c.coupon_id=?";
		List<Coupon> lists = query(sql, Coupon.class, cid);
		return lists.get(0);
	}

	/**
	 * 获取积分商品页面 
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("status", 1);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	/**
	 * 获取积分商品 
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(20);
		if (!AppUtils.isBlank(firstCid)) {
			cq.eq("firstCid", firstCid);
		}
		if (!AppUtils.isBlank(twoCid)) {
			cq.eq("twoCid", twoCid);
		}
		if (!AppUtils.isBlank(thirdCid)) {
			cq.eq("thirdCid", thirdCid);
		}
		cq.eq("status", 1);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	/**
	 * 获取积分商品 
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid, Integer startIntegral, Integer endIntegral,
			String integralKeyWord, String orders) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("status", 1);
		if (!AppUtils.isBlank(firstCid)) {
			cq.eq("firstCid", firstCid);
		}
		if (!AppUtils.isBlank(twoCid)) {
			cq.eq("twoCid", twoCid);
		}
		if (!AppUtils.isBlank(thirdCid)) {
			cq.eq("thirdCid", thirdCid);
		}
		if (AppUtils.isNotBlank(startIntegral)) {
			cq.ge("exchangeIntegral", startIntegral);
		}
		if (AppUtils.isNotBlank(endIntegral)) {
			cq.le("exchangeIntegral", endIntegral);
		}
		if (AppUtils.isNotBlank(integralKeyWord)) {
			cq.like("name", "%" + integralKeyWord + "%");
		}
		if (AppUtils.isNotBlank(orders)) {
			String[] order = StringUtils.split(orders, ",");
			if (order != null && order.length == 2) {
				if (StringUtils.equals("asc", order[1])) {
					cq.addOrder("asc", order[0]);
				} else {
					cq.addOrder("desc", order[0]);
				}
			} else {
				cq.addDescOrder("addTime");
			}
		}
		return queryPage(cq);
	}

	/**
	 * 获取积分商品页面
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO, IntegralProd integralProd, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(20);
		if (!AppUtils.isBlank(integralProd.getId())) {
			cq.eq("id", integralProd.getId());
		}
		if (!AppUtils.isBlank(integralProd.getFirstCid())) {
			cq.eq("firstCid", integralProd.getFirstCid());
		}
		if (!AppUtils.isBlank(integralProd.getTwoCid())) {
			cq.eq("twoCid", integralProd.getTwoCid());
		}
		if (!AppUtils.isBlank(integralProd.getThirdCid())) {
			cq.eq("thirdCid", integralProd.getThirdCid());
		}
		if (!AppUtils.isBlank(integralProd.getIsCommend())) {
			cq.eq("isCommend", integralProd.getIsCommend());
		}

		if (!AppUtils.isBlank(integralProd.getName())) {
			cq.like("name", "%" + integralProd.getName().trim() + "%");
		}

		if (!result.isSortExternal()) {
		}else{
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}

		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public Integer batchDelete(String ids) {
		String[] integralProdIds=ids.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("DELETE FROM ls_integral_prod WHERE id IN (");
		for(String integralProdId:integralProdIds){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(),integralProdIds);
	}

	@Override
	public Integer batchOffline(String ids) {
		String[] integralProdIds=ids.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("UPDATE ls_integral_prod  SET status=0 WHERE id IN(");
		for(String integralProdId:integralProdIds){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(),integralProdIds);
	}

	@Override
	public Integer releaseStock(Long prodId, Integer productNums) {
		String sql = "UPDATE ls_integral_prod SET prod_stock = prod_stock + ?, sale_num = sale_num - ? WHERE id = ?";
		return this.update(sql, productNums, productNums, prodId);
	}
	
}
