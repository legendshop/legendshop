/*
 * 

 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.manager.impl;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.spi.manager.ThirdUserAuthorizeManager;

/**
 * 第三方用户授权认证管理器 工厂
 * @author 开发很忙
 */
public class ThirdUserAuthorizeManagerFactory {

	/**
	 * 获取 ThirdUserAuthorizeManager 实例
	 * @param type 第三方类型
	 * @return
	 * @throws Exception
	 */
	public static ThirdUserAuthorizeManager getIntance(String type) throws Exception {
		
		ThirdUserAuthorizeManager manager = null;
		
		if(PassportTypeEnum.WEIXIN.value().equals(type)){
			manager = ContextServiceLocator.getBean(WeixinUserAuthorizeManagerImpl.class);
		} else if(PassportTypeEnum.QQ.value().equals(type)){
			manager = ContextServiceLocator.getBean(QQUserAuthorizeManagerImpl.class);
		} else if(PassportTypeEnum.SINAWEIBO.value().equals(type)){
			manager = ContextServiceLocator.getBean(WeiboUserAuthorizeManagerImpl.class);
		} 

		if(manager == null) {
			throw new BusinessException("ThirdUserAuthorizeManager can not empty");
		}
		
		return manager;
	}

}
