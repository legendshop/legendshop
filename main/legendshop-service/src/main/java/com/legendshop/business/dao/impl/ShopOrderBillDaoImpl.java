/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopOrderBillDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.util.AppUtils;

/**
 *商家订单结算表Dao
 */
@Repository
public class ShopOrderBillDaoImpl extends GenericDaoImpl<ShopOrderBill, Long> implements ShopOrderBillDao  {

	public ShopOrderBill getShopOrderBill(Long id){
		return getById(id);
	}
	
    public int deleteShopOrderBill(ShopOrderBill shopOrderBill){
    	return delete(shopOrderBill);
    }
	
	public Long saveShopOrderBill(ShopOrderBill shopOrderBill){
		return save(shopOrderBill);
	}
	
	public int updateShopOrderBill(ShopOrderBill shopOrderBill){
		return update(shopOrderBill);
	}

	@Override
	public ShopOrderBill getLastShopOrderBillByShopId(Long shopId) {
		List<ShopOrderBill> shopOrderBills =  this.queryByProperties(new EntityCriterion().eq("shopId", shopId).addDescOrder("endDate"));
		if(AppUtils.isNotBlank(shopOrderBills)){
			return shopOrderBills.get(0);
		}else{
			return null;
		}
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBillPage(String curPageNO, ShopOrderBill shopOrderBill) {
		CriteriaQuery cq = new CriteriaQuery(ShopOrderBill.class, curPageNO);
        cq.setPageSize(10);
        if(AppUtils.isNotBlank(shopOrderBill.getSn())) {        	
        	cq.like("sn","%" + shopOrderBill.getSn().trim() + "%");
        }
        cq.eq("status", shopOrderBill.getStatus());
        if(AppUtils.isNotBlank(shopOrderBill.getShopId())) {        	
        	cq.eq("shopId",shopOrderBill.getShopId());
        }
        if(AppUtils.isNotBlank(shopOrderBill.getShopName())) {        	
        	cq.like("shopName","%" + shopOrderBill.getShopName().trim() + "%");
        }
        if(AppUtils.isNotBlank(shopOrderBill.getFlag())) {      	
        	cq.like("flag","%" + shopOrderBill.getFlag().trim() + "%");
        }
        cq.addAscOrder("status");
        cq.addDescOrder("createDate");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO, ShopOrderBill shopOrderBill) {
		CriteriaQuery cq = new CriteriaQuery(ShopOrderBill.class, curPageNO);
        cq.setPageSize(5);
        cq.eq("shopId", shopId);
        if(AppUtils.isNotBlank(shopOrderBill.getSn())){
        	  cq.like("sn","%"+ shopOrderBill.getSn().trim()+"%");
        }
        //cq.eq("sn", shopOrderBill.getSn());
        cq.eq("status", shopOrderBill.getStatus());
        cq.addDescOrder("createDate");
		return queryPage(cq);
	}

	/**
	 * 检查是否有存在的结算单
	 */
	@Override
	public boolean getShopOrderBill(Long shopId, Date startDate, Date endDate) {
		String sql = "select count(1) from ls_shop_order_bill where shop_id = ? and start_date = ? and end_date = ?";
		Integer result = get(sql, Integer.class, shopId, startDate, endDate);
		return result > 0;
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBill(String curPageNO, Long shopId, Integer status, int pageSize) {
		CriteriaQuery cq = new CriteriaQuery(ShopOrderBill.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("shopId", shopId);
		cq.eq("status", status);
		cq.addDescOrder("createDate");
		return queryPage(cq);
	}

}
