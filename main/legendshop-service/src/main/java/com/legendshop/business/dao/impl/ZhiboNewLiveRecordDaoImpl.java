/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ZhiboNewLiveRecordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.util.AppUtils;

/**
 * The Class ZhiboNewLiveRecordDaoImpl. 新版直播记录表Dao实现类
 */
@Repository
public class ZhiboNewLiveRecordDaoImpl extends GenericDaoImpl<ZhiboNewLiveRecord, Long> implements ZhiboNewLiveRecordDao {

	/**
	 * 根据Id获取新版直播记录表
	 */
	public ZhiboNewLiveRecord getZhiboNewLiveRecord(Long id) {
		return getById(id);
	}

	/**
	 * 删除新版直播记录表
	 */
	public int deleteZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		return delete(zhiboNewLiveRecord);
	}

	/**
	 * 保存新版直播记录表
	 */
	public Long saveZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		return save(zhiboNewLiveRecord);
	}

	/**
	 * 更新新版直播记录表
	 */
	public int updateZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		return update(zhiboNewLiveRecord);
	}

	/**
	 * 查询新版直播记录表列表
	 */
	public PageSupport getZhiboNewLiveRecord(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/**
	 * 查询房间列表
	 */
	@Override
	public List<ZhiboNewLiveRecord> getRoomList(Integer index, Integer size) {
		String sql = "SELECT lz.*,ls.shop_pic AS pic FROM ls_zhibo_new_live_record lz LEFT JOIN ls_shop_detail ls ON lz.host_uid = ls.contact_mobile";
		return queryLimit(sql, ZhiboNewLiveRecord.class, index, size);
	}

	/**
	 * 将用户hostUid的直播记录删除。一个用户同一时间只能开启一个直播，成功则返回1
	 * @return
	 */
	@Override
	public int deleteUid(String id, Integer roomid) {
		return update("DELETE FROM ls_zhibo_new_live_record WHERE host_uid = ? and av_room_id = ?", id, roomid);
	}


	/**
	 * 清空房间成员,用于直播结束清空房间成员；成功则返回1
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public int clearRoom(Integer id) {
		return update("delete from ls_zhibo_interact_av_room where av_room_id = ?", id);
	}

	/**
	 * 检查房间ID是否存在
	 */
	@Override
	public ZhiboNewLiveRecord getRoomById(Integer roomnum) {
		return get("SELECT * from ls_zhibo_new_live_record where av_room_id = ?", ZhiboNewLiveRecord.class, roomnum);
	}
	
	/**
	 * 获取房间成员,成功返回房间成员信息，失败返回空
	 * 
	 * @return
	 */
	@Override
	public List<ZhiboInteractAvRoom> getRoomidList(Integer index, Integer size, Integer roomid) {
		return queryLimit(
				"SELECT uid, role FROM ls_zhibo_interact_av_room where av_room_id = ?",
				ZhiboInteractAvRoom.class, index, size, roomid);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List getInfoByUid(String uid) {
		return query(
				"SELECT * from ls_zhibo_interact_av_room where uid = ?",
				ZhiboInteractAvRoom.class, uid);
	}
	
	/**
	 * 成员进入房间
	 */
	@Override
	public int enterRoom(String uid, Integer roomid, String status, String modifyTime, Integer role) {
		return update(
				"insert into ls_zhibo_interact_av_room (uid, av_room_id, status, modify_time, role) values (?, ?, ?, ?, ?)",
				uid, roomid, status, modifyTime, role);
	}
	
	@Override
	public int exitRoom(String uid) {
		return update("delete from ls_zhibo_interact_av_room where uid = ?", uid);
	}

	@Override
	public PageSupport<ZhiboNewLiveRecord> getRoom(String curPageNO, String avRoomId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ZhiboNewLiveRecord.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(avRoomId)){
			map.put("av_room_id", avRoomId);
		}
		String querySQL = ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomList", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
}
