/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * 微信用户抽奖Dao
 */
public interface DrawWXUserDao extends GenericDao<WeixinGzuserInfo, Long> {

	/**
	 * Gets the gzuser info.
	 *
	 * @param openId the open id
	 * @return the gzuser info
	 */
	WeixinGzuserInfo getGzuserInfo(String openId);
	
	/**
	 * Gets the gzuser info by user id.
	 *
	 * @param userId the user id
	 * @return the gzuser info by user id
	 */
	WeixinGzuserInfo getGzuserInfoByUserId(String userId);


}
