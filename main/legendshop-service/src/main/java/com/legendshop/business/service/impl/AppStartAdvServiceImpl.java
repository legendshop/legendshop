/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AppStartAdvDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AppDecorateActionSubTypeEnum;
import com.legendshop.model.constant.AppDecorateActionTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppDecorateActionDto;
import com.legendshop.model.dto.app.AppStartAdvDto;
import com.legendshop.model.entity.AppStartAdv;
import com.legendshop.spi.service.AppStartAdvService;
import com.legendshop.util.AppUtils;

/**
 * APP启动广告服务.
 */
@Service("appStartAdvService")
public class AppStartAdvServiceImpl  implements AppStartAdvService{
	
	@Autowired
    private AppStartAdvDao appStartAdvDao;

    public AppStartAdv getAppStartAdv(Long id) {
        return appStartAdvDao.getAppStartAdv(id);
    }

    public void deleteAppStartAdv(AppStartAdv appStartAdv) {
        appStartAdvDao.deleteAppStartAdv(appStartAdv);
    }

    public Long saveAppStartAdv(AppStartAdv appStartAdv) {
        if (!AppUtils.isBlank(appStartAdv.getId())) {
            updateAppStartAdv(appStartAdv);
            return appStartAdv.getId();
        }
        return appStartAdvDao.saveAppStartAdv(appStartAdv);
    }

    public void updateAppStartAdv(AppStartAdv appStartAdv) {
        appStartAdvDao.updateAppStartAdv(appStartAdv);
    }

	@Override
	public boolean updateStatus(Long id, Integer status) {
		int result = appStartAdvDao.updateStatus(id, status);
		return result > 0;
	}

	@Override
	public int getAllCount() {
		return (int) appStartAdvDao.getCount();
	}

	@Override
	public boolean checkNameIsExits(String name) {
		int result = appStartAdvDao.getCount(name);
		return result > 0;
	}

	@Override
	public String getName(Long id) {
		
		return appStartAdvDao.getName(id);
	}

	@Override
	public List<AppStartAdvDto> getOnlines() {
		
		List<AppStartAdvDto> appStartAdvDtoList = new ArrayList<AppStartAdvDto>();
		
		List<AppStartAdv> appStartAdvs = appStartAdvDao.queryByProperties(new EntityCriterion().eq("status", Constants.ONLINE).addDescOrder("createTime"));
		for(AppStartAdv appStartAdv : appStartAdvs){
			
			AppStartAdvDto appStartAdvDto = new AppStartAdvDto();
			appStartAdvDto.setId(appStartAdv.getId());
			appStartAdvDto.setPhoto(appStartAdv.getImgUrl());
			
			AppDecorateActionDto action = new AppDecorateActionDto();
			action.setType(AppDecorateActionTypeEnum.PROD_DETAIL.value());
			action.setSubType(AppDecorateActionSubTypeEnum.OUT_URL.value());
			action.setTarget(appStartAdv.getUrl());
			
			appStartAdvDto.setAction(action);
			
			appStartAdvDtoList.add(appStartAdvDto);
		}
		
		return appStartAdvDtoList;
	}

	@Override
	public PageSupport<AppStartAdv> getAppStartAdvPage(String curPageNO, AppStartAdv appStartAdv) {
		return appStartAdvDao.getAppStartAdvPage(curPageNO,appStartAdv);
	}
}
