/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.legendshop.base.util.XssFilterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.business.dao.ProductConsultDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.dto.ProductConsultDto;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.util.SafeHtml;

/**
 * 商品咨询服务实现类
 */
@Service("productConsultService")
public class ProductConsultServiceImpl implements ProductConsultService {

	@Autowired
	private ProductConsultDao productConsultDao;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Override
	public List<ProductConsult> getProductConsultList(Long prodId) {
		return productConsultDao.getProductConsultList(prodId);
	}

	@Override
	public ProductConsult getProductConsult(Long id) {
		return productConsultDao.getProductConsult(id);
	}

	@Override
	public Long saveProductConsult(ProductConsult productConsult) {
		return productConsultDao.saveProductConsult(productConsult);
	}

	@Override
	public void updateProductConsult(String name, ProductConsult productConsult) {
		ProductConsult consult = productConsultDao.getProductConsult(productConsult.getConsId());
		if (consult == null) {
			throw new NotFoundException("ProductConsult Not Found");
		}
			consult.setAnswer(XssFilterUtil.cleanXSS(productConsult.getAnswer()));
			consult.setAnsUserName(name);
			consult.setAnswertime(new Date());
			consult.setReplySts(1);
			productConsultDao.updateProductConsult(consult);
			//发送通知站内信
			sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", consult.getAskUserName(), "咨询回复" , "亲，您的咨询商家已回复，回复内容为【"+consult.getAnswer()+"】").getSource());
	}


	@Override
	public PageSupport<ProductConsult> getProductConsult(String curPageNO, ProductConsult productConsult) {
		return productConsultDao.getProductConsult(curPageNO, productConsult);
	}

	@Override
	@Cacheable(value = "ProductConsultList",  condition = "T(Integer).parseInt(#curPageNO) < 3", key="#curPageNO + '.' + #pointType + '.' + #prodId")
	public PageSupport<ProductConsult> getProductConsultForList(String curPageNO, Integer pointType, Long prodId) {
		return productConsultDao.getProductConsultForList(curPageNO, pointType, prodId);
	}

	@Override
	public long checkFrequency(ProductConsult consult) {
		return productConsultDao.checkFrequency(consult);
	}

	@Override
	public void deleteProductConsultById(Long consId) {
		productConsultDao.deleteProductConsultById(consId);
	}
	
	@Override
	public void deleteProductConsultByShopUser(Long consId) {
		productConsultDao.deleteProductConsultByShopUser(consId);
	}

	@Override
	public ProductConsultDto getConsult(String userId, Long consId) {
		return productConsultDao.getConsult(userId, consId);
	}

	@Override
	public int updateConsult(String userId, ProductConsultDto consult) {
		return productConsultDao.updateConsult(userId, consult);
	}

	@Override
	public int deleteProductConsult(String userId, Long consId) {
		return productConsultDao.deleteProductConsult(userId, consId);
	}

	@Override
	public List<ProductConsult> queryUnReply() {
		return productConsultDao.queryUnReply();
	}

	@Override
	@Cacheable(value = "ConsultCounts")
	public int getConsultCounts(Long shopId) {
		return productConsultDao.getConsultCounts(shopId);
	}

	@Override
	public PageSupport<ProductConsult> getProductConsultPage(String userName, ProductConsult productConsult,
			String curPageNO) {
		return productConsultDao.getProductConsultPage(userName,productConsult,curPageNO);
	}

	@Override
	public PageSupport<ProductConsult> queryQarseConsult(String curPageNO, Long shopId, Integer replyed) {
		return productConsultDao.queryQarseConsult(curPageNO,shopId,replyed);
	}

	@Override
	public PageSupport<ProductConsult> queryProdcons(String curPageNO, Long shopId, ProductConsult productConsult) {
		return productConsultDao.queryProdcons(curPageNO,shopId,productConsult);
	}

	@Override
	public PageSupport<ProductConsult> query(String curPageNO, String userName) {
		return productConsultDao.queryConsult(curPageNO,userName);
	}

	@Override
	public int batchDeleteConsultByIds(String consIdStr) {
		return productConsultDao.batchDeleteConsultByIds(consIdStr);
	}

}
