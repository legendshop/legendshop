/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.StockLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.StockLog;
import com.legendshop.spi.service.StockLogService;
import com.legendshop.util.AppUtils;

/**
 *库存历史服务
 */
@Service("stockLogService")
public class StockLogServiceImpl  implements StockLogService{

	@Autowired
    private StockLogDao stockLogDao;

    public StockLog getStockLog(Long id) {
        return stockLogDao.getStockLog(id);
    }

    public void deleteStockLog(StockLog stockLog) {
        stockLogDao.deleteStockLog(stockLog);
    }

    public Long saveStockLog(StockLog stockLog) {
        if (!AppUtils.isBlank(stockLog.getId())) {
            updateStockLog(stockLog);
            return stockLog.getId();
        }
        return stockLogDao.saveStockLog(stockLog);
    }

    public void updateStockLog(StockLog stockLog) {
        stockLogDao.updateStockLog(stockLog);
    }

	@Override
	public PageSupport<StockLog> loadStockLog(Long prodId, Long skuId, String curPageNO) {
		return stockLogDao.loadStockLog(prodId,skuId,curPageNO);
	}
}
