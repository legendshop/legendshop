/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.promotor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.promoter.DistSetDao;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.util.AppUtils;

/**
 * 分销设置Service实现类
 */
@Service("distSetService")
public class DistSetServiceImpl  implements DistSetService{
    
	@Autowired
	private DistSetDao distSetDao;

    /**
     * 获取分销设置 
     */
    public List<DistSet> getDistSet(String userName) {
        return distSetDao.getDistSet(userName);
    }

    /**
     * 获取分销设置 
     */
    public DistSet getDistSet(Long id) {
        return distSetDao.getDistSet(id);
    }

    /**
     * 删除分销设置
     */
    public void deleteDistSet(DistSet distSet) {
        distSetDao.deleteDistSet(distSet);
    }

    /**
     * 保存分销设置 
     */
    public Long saveDistSet(DistSet distSet) {
        if (!AppUtils.isBlank(distSet.getDistSetId())) {
            updateDistSet(distSet);
            return distSet.getDistSetId();
        }
        return distSetDao.saveDistSet(distSet);
    }

    /**
     * 更新分销设置 
     */
    public void updateDistSet(DistSet distSet) {
        distSetDao.updateDistSet(distSet);
    }

}
