/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopNavDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.spi.service.ShopNavService;
import com.legendshop.util.AppUtils;

/**
 *商家导航表
 */
@Service("shopNavService")
public class ShopNavServiceImpl  implements ShopNavService{
	
	@Autowired
    private ShopNavDao shopNavDao;

	public ShopNav getShopNav(Long id) {
        return shopNavDao.getShopNav(id);
    }

    @CacheEvict(value="StoreListDto",key="'StoreList_'+#shopNav.shopId")
    public void deleteShopNav(ShopNav shopNav) {
        shopNavDao.deleteShopNav(shopNav);
    }

    @CacheEvict(value="StoreListDto",key="'StoreList_'+#shopNav.shopId")
    public Long saveShopNav(ShopNav shopNav) {
        if (!AppUtils.isBlank(shopNav.getId())) {
        	 ShopNav orgin= getShopNav(shopNav.getId()) ;
        	 if(AppUtils.isNotBlank(orgin)){
        		 orgin.setStatus(shopNav.getStatus());
        		 orgin.setTitle(shopNav.getTitle());
        		 orgin.setUrl(shopNav.getUrl());
        		 orgin.setWinType(shopNav.getWinType());
        		 orgin.setSeq(shopNav.getSeq());
        		 updateShopNav(orgin);
        	 }
        	 return shopNav.getId();
        }
        return shopNavDao.saveShopNav(shopNav);
    }

    public void updateShopNav(ShopNav shopNav) {
        shopNavDao.updateShopNav(shopNav);
    }

	@Override
	public List<ShopNav> getLayoutNav(Long shopId) {
		return shopNavDao.getLayoutNav(shopId);
	}


	@Override
	public PageSupport<ShopNav> getShopNavPage(String curPageNO, Long shopId) {
		return shopNavDao.getShopNavPage(curPageNO,shopId);
	}
}
