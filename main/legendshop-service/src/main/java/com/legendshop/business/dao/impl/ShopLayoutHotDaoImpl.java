/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopLayoutHotDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.shopDecotate.ShopLayoutHot;
import com.legendshop.util.AppUtils;

/**
 * 布局热点Dao.
 */
@Repository
public class ShopLayoutHotDaoImpl extends GenericDaoImpl<ShopLayoutHot, Long> implements ShopLayoutHotDao  {

	public ShopLayoutHot getShopLayoutHot(Long id){
		return getById(id);
	}
	
    public int deleteShopLayoutHot(ShopLayoutHot shopLayoutHot){
    	return delete(shopLayoutHot);
    }
	
	public Long saveShopLayoutHot(ShopLayoutHot shopLayoutHot){
		return save(shopLayoutHot);
	}
	
	public int updateShopLayoutHot(ShopLayoutHot shopLayoutHot){
		return update(shopLayoutHot);
	}
	
	@Override
	public ShopLayoutHot findShopLayoutHot(Long shopId, Long divId,
			Long layoutId) {
		List<ShopLayoutHot> hots=  this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("layoutId", layoutId).eq("layoutDivId", divId));
		if(AppUtils.isNotBlank(hots)){
			return hots.get(0);
		}
		return null;
	}

	@Override
	public ShopLayoutHot findShopLayoutHot(Long shopId, Long layoutId) {
		List<ShopLayoutHot> hots=  this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("layoutId", layoutId));
		if(AppUtils.isNotBlank(hots)){
			return hots.get(0);
		}
		return null;
	}
	
	@Override
	public List<ShopLayoutHot> findShopLayoutHot(Long shopId) {
		List<ShopLayoutHot> hots=  this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
		return hots;
	}

	/**
	 * 删除装修的热点
	 */
	@Override
	public void deleteShopLayoutHot(Long shopId, Long layoutId) {
		update("delete from ls_shop_layout_hot where shop_id=? and layout_id=? ", shopId, layoutId);
	}
	
	
	
 }
