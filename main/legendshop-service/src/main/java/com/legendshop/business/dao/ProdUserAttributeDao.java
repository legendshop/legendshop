/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdUserAttribute;

import java.util.List;

/**
 * The Class ProdUserAttributeDao.
 */

public interface ProdUserAttributeDao extends Dao<ProdUserAttribute, Long> {
     
	public abstract ProdUserAttribute getProdUserAttribute(Long id);
	
    public abstract void deleteProdUserAttribute(Long id,Long shopId);
	
	public abstract Long saveProdUserAttribute(ProdUserAttribute prodUserAttribute);
	
	public abstract int updateProdUserAttribute(ProdUserAttribute prodUserAttribute);
	
	public abstract PageSupport<ProdUserAttribute> getProdUserAttributePage(String curPageNO, Long shopId);

	public abstract PageSupport<ProdUserAttribute> getProdUserAttributeByPage(String curPageNO, Long shopId);

	List<ProdUserAttribute> getShopProdAttribute(Long shopId);
}
