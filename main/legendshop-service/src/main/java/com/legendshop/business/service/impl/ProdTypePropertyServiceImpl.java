/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 */
import com.legendshop.business.dao.ProdTypePropertyDao;
import com.legendshop.model.entity.ProdTypeProperty;
import com.legendshop.spi.service.ProdTypePropertyService;
/**
 *
 * 商品类型跟属性的关系服务
 *
 */
@Service("prodTypePropertyService")
public class ProdTypePropertyServiceImpl  implements ProdTypePropertyService{
	
	@Autowired
    private ProdTypePropertyDao prodTypePropertyDao;

    @Override
    public void updateProdTypeProperty(ProdTypeProperty prodTypeProperty) {
    	prodTypePropertyDao.updateProdTypeProperty(prodTypeProperty);
    }
    
    @Override
    public ProdTypeProperty getProdTypeProperty(Long id) {
    	return prodTypePropertyDao.getProdTypeProperty(id);
    }

	public ProdTypePropertyDao getProdTypePropertyDao() {
		return prodTypePropertyDao;
	}

	@Override
	public ProdTypeProperty getProdTypeProperty(Long propId, Long typeId) {
		return prodTypePropertyDao.getProdTypeProperty(propId, typeId);
	}
}
