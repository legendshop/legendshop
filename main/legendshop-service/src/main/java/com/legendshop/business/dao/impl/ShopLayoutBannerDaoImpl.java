/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopLayoutBannerDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;

/**
 * The Class ShopLayoutBannerDaoImpl.
 */
@Repository
public class ShopLayoutBannerDaoImpl extends GenericDaoImpl<ShopLayoutBanner, Long> implements ShopLayoutBannerDao  {
     
    public List<ShopLayoutBanner> getShopLayoutBannerByShopId(Long shopId ){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
    }

	public ShopLayoutBanner getShopLayoutBanner(Long id){
		return getById(id);
	}
	
    public int deleteShopLayoutBanner(ShopLayoutBanner shopLayoutBanner){
    	return delete(shopLayoutBanner);
    }
	
	public Long saveShopLayoutBanner(ShopLayoutBanner shopLayoutBanner){
		return save(shopLayoutBanner);
	}
	
	public int updateShopLayoutBanner(ShopLayoutBanner shopLayoutBanner){
		return update(shopLayoutBanner);
	}

	@Override
	public List<ShopLayoutBanner> getShopLayoutBanner(Long decId, Long layoutId) {
		return this.queryByProperties(new EntityCriterion().eq("shopDecotateId", decId).eq("layoutId", layoutId));
	}

	@Override
	public void saveShopLayoutBanner(List<ShopLayoutBanner> shopLayoutBanner) {
		save(shopLayoutBanner);
	}
	
 }
