/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.spi.service.SubHistoryService;

/**
 * 订单历史服务
 */
@Service("subHistoryService")
public class SubHistoryServiceImpl  implements SubHistoryService{
	
	private final Logger log = LoggerFactory.getLogger(SubHistoryServiceImpl.class);

	@Autowired
    private SubHistoryDao subHistoryDao;

	/**
	 * 保存订单历史
	 */
	@Override
	public void saveSubHistory(SubHistory subHistory) {
		if(subHistory==null){
			log.warn("OrderHistoryProcessor without subHistory");
			return;
		}
		if (log.isDebugEnabled()) {
			log.debug("OrderHistoryProcessor calling by subId {}", subHistory.getSubId());
		}
		subHistoryDao.saveSubHistory(subHistory);
	}
}
