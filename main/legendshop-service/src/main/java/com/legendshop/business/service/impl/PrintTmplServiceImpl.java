/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PrintTmplDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PrintTmpl;
import com.legendshop.spi.service.PrintTmplService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 *打印模板
 */
@Service("printTmplService")
public class PrintTmplServiceImpl  implements PrintTmplService{
	
	@Autowired
    private PrintTmplDao printTmplDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

    public List<PrintTmpl> getPrintTmplByShop(Long shopId){
        return printTmplDao.getPrintTmplByShop(shopId);
    }

    public PrintTmpl getPrintTmpl(Long id) {
        return printTmplDao.getPrintTmpl(id);
    }

    public void deletePrintTmpl(PrintTmpl printTmpl) {
    	if(AppUtils.isNotBlank(printTmpl.getBgimage())){ //更新背景图片
		      //删除附件记录
			attachmentManager.delAttachmentByFilePath(printTmpl.getBgimage());
			//删除图片文件
			attachmentManager.deleteTruely(printTmpl.getBgimage());
		}
    	
        printTmplDao.deletePrintTmpl(printTmpl);
    }

    public Long savePrintTmpl(PrintTmpl printTmpl) {
        if (!AppUtils.isBlank(printTmpl.getPrtTmplId())) {
            updatePrintTmpl(printTmpl);
            return printTmpl.getPrtTmplId();
        }
        return printTmplDao.savePrintTmpl(printTmpl);
    }

    public void updatePrintTmpl(PrintTmpl printTmpl) {
        printTmplDao.updatePrintTmpl(printTmpl);
    }

	@Override
	public PrintTmpl getPrintTmplByDelvId(Long delvId) {
		return printTmplDao.getPrintTmplByDelvId(delvId);
	}

	@Override
	public PageSupport<PrintTmpl> getPrintTmplPage(String curPageNO, PrintTmpl printTmpl) {
		return printTmplDao.getPrintTmplPage(curPageNO,printTmpl);
	}

	@Override
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId) {
		return printTmplDao.getPrintTmplPageByShopId(shopId);
	}
	
	@Override
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId, String curPageNO) {
		return printTmplDao.getPrintTmplPageByShopId(shopId, curPageNO);
	}

	@Override
	public void updatePrintTmpl( PrintTmpl origin,PrintTmpl printTmpl) {
		if (AppUtils.isNotBlank(printTmpl.getBgimage())) { // 更新背景图片
			// 删除附件记录
			attachmentManager.delAttachmentByFilePath(origin.getBgimage());
			// 删除图片文件
			attachmentManager.deleteTruely(origin.getBgimage());

			origin.setBgimage(printTmpl.getBgimage());
		}
		printTmplDao.updatePrintTmpl(origin);
	}
	
}
