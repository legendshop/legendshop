/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Date;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ArticleDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ArticleStatusEnum;
import com.legendshop.model.entity.Article;
import com.legendshop.util.AppUtils;

/**
 * 文章Dao实现类
 */
@Repository
public class ArticleDaoImpl extends GenericDaoImpl<Article, Long> implements ArticleDao  {

   	/**
	 *  根据Id获取
	 */
	public Article getArticle(Long id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteArticle(Article article){
    	return delete(article);
    }

   /**
	 *  保存
	 */		
	public Long saveArticle(Article article){
		return save(article);
	}

   /**
	 *  更新
	 */		
	public int updateArticle(Article article){
		return update(article);
	}
	
	/**
	 * 修改点赞数量
	 */
	public int updateThumbNum(Long id){
		String sql="UPDATE ls_article SET thumb_num = (select COUNT(1) from ls_article_thumb where art_id=?) WHERE id = ? ";
		return update(sql,id,id);
	}

	/**
	 *  更新评论数量,设置为审核通过的评论总数
	 * 
	 */
	@Override
	public int updateCommentNum(Long artId) {
		String sql="UPDATE ls_article SET comments_num = (select COUNT(1) from ls_article_comment where art_id=? AND status = 1) WHERE id = ? ";
		return update(sql, artId,artId);
	}

	@Override
	public PageSupport<Article> getArticlePage(String curPageNO, Article article) {
		CriteriaQuery cq = new CriteriaQuery(Article.class, curPageNO);
		if (AppUtils.isNotBlank(article.getName())) {
			cq.like("name", article.getName().trim(), MatchMode.ANYWHERE);
		}
		if (AppUtils.isNotBlank(article.getAuthor())) {
			cq.like("author", article.getAuthor().trim(), MatchMode.ANYWHERE);
		}
		// 排序
		cq.addDescOrder("createTime");
		// 状态
		cq.eq("status", article.getStatus());
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<Article> query(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(Article.class, curPageNO);
		// 排序
		cq.addDescOrder("createTime");
		// 发布时间小于当前时间
		cq.lt("issueTime", new Date());
		Integer status = ArticleStatusEnum.ONLINE_DISPALY.value();
		cq.eq("status", status);
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<Article> getArticleList(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(Article.class, curPageNO);
		// 排序
		cq.addDescOrder("createTime");
		// 发布时间小于当前时间
		cq.lt("issueTime", new Date());
		cq.eq("status", ArticleStatusEnum.ONLINE_DISPALY.value());
		return queryPage(cq);
	}
	
 }
