package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AdvAnalysisDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.AdvAnalysis;
import com.legendshop.util.AppUtils;

/**
 * 广告统计Dao
 *
 */
@Repository
public class AdvAnalysisDaoImpl extends GenericDaoImpl<AdvAnalysis, Long>  implements AdvAnalysisDao{

	
	@Override
	public Long saveAdvAnalysis(AdvAnalysis advAnalysis) {
		return save(advAnalysis);
	}


	@Override
	public PageSupport<AdvAnalysis> query(String curPageNO, String url) {
		StringBuffer sql = new StringBuffer();
		StringBuffer sqlCount = new StringBuffer();
		List<Object> objects = new ArrayList<Object>();
		sql.append("SELECT a.*,u.NAME as userName FROM ls_adv_analysis AS a LEFT JOIN ls_user AS u ON u.id=a.user_id");
		sqlCount.append("SELECT COUNT(*) FROM ls_adv_analysis");
		if(AppUtils.isNotBlank(url)){
			sql.append(" WHERE url=? ORDER BY create_time DESC");
			sqlCount.append(" WHERE url=?");
			objects.add(url);
		}
		SimpleSqlQuery hqlQuery = new SimpleSqlQuery(AdvAnalysis.class,10,curPageNO);
		hqlQuery.setQueryString(sql.toString());
		hqlQuery.setAllCountString(sqlCount.toString());
		hqlQuery.setParam(objects.toArray());
		return querySimplePage(hqlQuery);
	}


	@Override
	public PageSupport<AdvAnalysis> groupQuery(String userId, String curPageNO, String url) {
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		if(AppUtils.isNotBlank(url)) {			
			map.like("url", url.trim());
		}
		String sql = ConfigCode.getInstance().getCode("report.getAdmiAdvAnalysis", map);
		String sqlCount = ConfigCode.getInstance().getCode("report.getAdmiAdvAnalysisCount", map);
		SimpleSqlQuery hqlQuery = new SimpleSqlQuery(AdvAnalysis.class,10,curPageNO);
		hqlQuery.setQueryString(sql.toString());
		hqlQuery.setAllCountString(sqlCount.toString());
		hqlQuery.setParam(map.toArray());
		return querySimplePage(hqlQuery);
	}


}
