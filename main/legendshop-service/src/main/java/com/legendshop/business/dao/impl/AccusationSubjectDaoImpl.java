/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AccusationSubjectDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.AccusationSubject;

/**
 * 举报主题Dao实现类.
 */
@Repository
public class AccusationSubjectDaoImpl extends GenericDaoImpl<AccusationSubject, Long> implements AccusationSubjectDao {

	public AccusationSubject getAccusationSubject(Long id){
		return getById(id);
	}
	
    public void deleteAccusationSubject(AccusationSubject accusationSubject){
    	delete(accusationSubject);
    }
	
	public Long saveAccusationSubject(AccusationSubject accusationSubject){
		return (Long)save(accusationSubject);
	}
	
	public void updateAccusationSubject(AccusationSubject accusationSubject){
		 update(accusationSubject);
	}

	@Override
	public List<AccusationSubject> queryAccusationSubject() {
		return query("select as_id as asId,title as title  from ls_accu_subject where status = 1", AccusationSubject.class);
	}

	@Override
	public List<AccusationSubject> getAllAccusationSubject() {
		return queryAll();
	}

	@Override
	public void deleteSubjectByTypeId(Long typeId) {
		update("delete from ls_accu_subject where type_id = ?", typeId);
	}

	@Override
	public List<AccusationSubject> getSubjectByTypeId(Long typeId) {
		return query("select * from ls_accu_subject where type_id = ? and status=1",AccusationSubject.class,typeId);
	}
	
 }
