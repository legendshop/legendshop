/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.HeadmenuDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headmenu;
import com.legendshop.util.AppUtils;

/**
 * 头部菜单Dao.
 */
@Repository
public class HeadmenuDaoImpl extends GenericDaoImpl<Headmenu, Long> implements HeadmenuDao  {

	public Headmenu getHeadmenu(Long id){
		return getById(id);
	}
	
	@Caching(evict = { 
			@CacheEvict(value = "HeadmenuList", allEntries = true), 
			@CacheEvict(value = "PAGE_CACHE", key = "'NAV'")})//更新导航页缓存
	public int deleteHeadmenu(Headmenu headmenu){
    	return delete(headmenu);
    }

	@Caching(evict = { 
			@CacheEvict(value = "HeadmenuList", allEntries = true), 
			@CacheEvict(value = "PAGE_CACHE", key = "'NAV'")})//更新导航页缓存
	public Long saveHeadmenu(Headmenu headmenu){
		return save(headmenu);
	}

	@Caching(evict = { 
			@CacheEvict(value = "HeadmenuList", allEntries = true), 
			@CacheEvict(value = "PAGE_CACHE", key = "'NAV'")})//更新导航页缓存
	public int updateHeadmenu(Headmenu headmenu){
		return update(headmenu);
	}

	@Override
	@Cacheable(value = "HeadmenuList", key = "#type")
	public List<Headmenu> getHeadmenuAll(String type) {
		return  this.queryByProperties(new EntityCriterion().eq("status", 1).addAscOrder("seq").eq("type", type));
	}

	@Override
	public PageSupport<Headmenu> getHeadmenu(String curPageNO, Headmenu headmenu, String type) {
		CriteriaQuery cq = new CriteriaQuery(Headmenu.class, curPageNO);
		if (AppUtils.isNotBlank(headmenu.getName())) {
			cq.like("name", "%" + headmenu.getName().trim() + "%");
		}
		if (AppUtils.isNotBlank(type)) {
			cq.eq("type", type);
		}
		cq.addAscOrder("seq");
		cq.setPageSize(10);
		return queryPage(cq);
	}
	
 }
