/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SystemParameter;

/**
 * 系统参数Dao
 */
public interface SystemParameterDao extends Dao<SystemParameter, String>{

	/**
	 * Delete system parameter by id.
	 * 
	 * @param id
	 *            the id
	 */
	void deleteSystemParameterById(String id);

	/**
	 * Update system parameter.
	 * 
	 * @param systemParameter
	 *            the system parameter
	 */
	void updateSystemParameter(SystemParameter systemParameter);

	List<SystemParameter> loadAll();

	SystemParameter getSystemParameterById(String id);

	/**
	 *根据配置分类查询系统配置
	 * @param groupId 配置分类
	 * @return
	 */
	List<SystemParameter> getSystemParameterByGroupId(String groupId);

	PageSupport<SystemParameter> getSystemParameterListPage(String curPageNO, String name, String groupId);
}