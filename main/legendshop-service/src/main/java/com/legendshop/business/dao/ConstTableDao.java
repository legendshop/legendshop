/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.ConstTableId;

/**
 * 常量表服务
 */
public interface ConstTableDao extends GenericDao<ConstTable, ConstTableId> {

	public List<ConstTable> loadAllConstTable();

	public ConstTable getConstTablesBykey(String type, String keyValue);

	public void updateConstTableByTepe(String payType, String payTypeId, String payParams);

	public ConstTable getConstTablesBykeyForNoCache(String type, String keyValue);

}
