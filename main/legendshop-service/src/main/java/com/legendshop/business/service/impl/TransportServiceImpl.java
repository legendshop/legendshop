/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.business.comparer.TransCityComparer;
import com.legendshop.business.comparer.TransfeeComparer;
import com.legendshop.business.dao.TransCityDao;
import com.legendshop.business.dao.TransfeeDao;
import com.legendshop.business.dao.TransportDao;
import com.legendshop.dao.PageSupportHandler;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.TransCity;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.spi.service.TransportService;
import com.legendshop.util.AppUtils;

/**
 * 运费模板ServiceImpl.
 */
@Service("transportService")
public class TransportServiceImpl implements TransportService{
	
	@Autowired
	private TransportDao transportDao;
	
	@Autowired
	private TransfeeDao transfeeDao;
	
	@Autowired
	private TransCityDao transCityDao;
	
	private DataListComparer<Long, TransCity> transCityComparer = null;
	
	private DataListComparer<Transfee,Transfee> transfeeComparer = null;

	public List<Transport> getTransports(Long shopId) {
		return transportDao.getTransports(shopId);
	}

	public Transport getTransport(Long id) {
		return transportDao.getById(id);
	}

	public void deleteTransport(Transport transport) {
		transportDao.deleteTransport(transport);
	}

	@CacheEvict(value="Transport", key="#transport.shopId + '.' + #transport.id")
	public void saveTransport(Transport transport) {
		if(AppUtils.isNotBlank(transport.getId())){//更新运费模板
			
			Transport dbTransport = transportDao.getById(transport.getId());
			List<Transfee> dbTransfeeList = transfeeDao.getTranfeeById(transport.getId());
			
			//对比更新运费模板的运费
			updateTransfee(transport,dbTransfeeList,dbTransport);
			
			dbTransport.setTransName(transport.getTransName());
			dbTransport.setStatus(transport.getStatus());
			dbTransport.setTransTime(transport.getTransTime());
			dbTransport.setIsRegionalSales(transport.getIsRegionalSales());
			dbTransport.setTransType(transport.getTransType());
			transportDao.updateTransport(dbTransport);
		}else{
			transportDao.saveTransport(transport);
		}
			
	}

	/* (non-Javadoc)
	 * @see com.legendshop.multishop.service.TransportService#updateTransport(com.legendshop.model.entity.Transport)
	 */
	public void updateTransport(Transport transport) {
		transportDao.updateTransport(transport);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.multishop.service.TransportService#getTransport(com.legendshop.dao.support.CriteriaQuery, java.lang.Long)
	 */
	public PageSupport getTransport(CriteriaQuery cq, final Long shopId) {
		return transportDao.queryPage(cq, new PageSupportHandler<Transport>() {
			public void handle(List<Transport> transportList) {
				 List<Transfee> transfeeList = transfeeDao.getTranfeeByShopId(shopId);
				 for(Transport transport : transportList){
					 for(Transfee transfee:transfeeList){
						 if(transport.getId().equals(transfee.getTransportId())){
							 transport.addFeeList(transfee);
						 }
					 }
				 }
				
			}
		});
	}

	public PageSupport query(SimpleSqlQuery query) {
		return transportDao.querySimplePage(query);
	}

	public List<Area> getCtyName(Long ctyId) {
		return transportDao.getCtyName(ctyId);
	}

	public Transport getDetailedTransport(Long shopId,Long id) {
		return transportDao.getDetailedTransport(shopId, id);
	}

	public void deleteTransport(Long id, Long shopId) {
		 transportDao.deleteTransport(id, shopId);
	}

	
	public DataListComparer<Long, TransCity> getTransCityComparer() {
		if (transCityComparer == null) {
			transCityComparer = new DataListComparer<Long, TransCity>(new TransCityComparer());
		}
		return transCityComparer;
	}
	
	public DataListComparer<Transfee,Transfee> getTransfeeComparer() {
		if (transfeeComparer == null) {
			transfeeComparer = new DataListComparer<Transfee,Transfee>(new TransfeeComparer());
		}
		return transfeeComparer;
	}

	/** 对比更新 运费模板 运费 **/
	private void updateTransfee(Transport transport,List<Transfee> dbTransfeeList, Transport dbTransport){
		List<Transfee> allTransfeeList = new ArrayList<Transfee> ();
		
		//对比 平邮 列表
		List<Transfee> mailList = transport.getMailList();
		if(!transport.isTransMail()){mailList = new ArrayList<Transfee>();}//没选中，则清空
		Iterator<Transfee> mailIt = mailList.iterator();
		while (mailIt.hasNext()) {
			Transfee mailTransfee =  mailIt.next();
			if(AppUtils.isBlank(mailTransfee.getTransWeight()) ||
					 AppUtils.isBlank(mailTransfee.getTransAddWeight()) ||
					   AppUtils.isBlank(mailTransfee.getTransFee()) ||
						 AppUtils.isBlank(mailTransfee.getTransAddFee())){
				mailIt.remove();
			}else{
				mailTransfee.setTransportId(transport.getId());
				mailTransfee.setFreightMode(Constants.TRANSPORT_MAIL);
			}
		}
		
		//对比 快递 列表
		List<Transfee> expressList = transport.getExpressList();
		if(!transport.isTransExpress()){expressList = new ArrayList<Transfee>();}//没选中，则清空
		Iterator<Transfee> expressIt = expressList.iterator();
		while (expressIt.hasNext()) {
			Transfee expressTransfee =  expressIt.next();
			if(AppUtils.isBlank(expressTransfee.getTransWeight()) ||
					 AppUtils.isBlank(expressTransfee.getTransAddWeight()) ||
					   AppUtils.isBlank(expressTransfee.getTransFee()) ||
						 AppUtils.isBlank(expressTransfee.getTransAddFee())){
				expressIt.remove();
			}else{
				expressTransfee.setTransportId(transport.getId());
				expressTransfee.setFreightMode(Constants.TRANSPORT_EXPRESS);
			}
		}
		
		// 对比 ems 列表
		List<Transfee> emsList = transport.getEmsList();
		if(!transport.isTransEms()){emsList = new ArrayList<Transfee>();}//没选中，则清空
		Iterator<Transfee> emsIt = emsList.iterator();
		while (emsIt.hasNext()) {
			Transfee emsTransfee =  emsIt.next();
			if(AppUtils.isBlank(emsTransfee.getTransWeight()) ||
					 AppUtils.isBlank(emsTransfee.getTransAddWeight()) ||
					   AppUtils.isBlank(emsTransfee.getTransFee()) ||
						 AppUtils.isBlank(emsTransfee.getTransAddFee())){
				 emsIt.remove();
			}else{
			emsTransfee.setTransportId(transport.getId());
			emsTransfee.setFreightMode(Constants.TRANSPORT_EMS);
			}
		}
			
		allTransfeeList.addAll(mailList);
		allTransfeeList.addAll(expressList);
		allTransfeeList.addAll(emsList);
		
		if(AppUtils.isNotBlank(allTransfeeList)){
			Map<Integer, List<Transfee>> transfeeMap = getTransfeeComparer().compare(allTransfeeList,dbTransfeeList, dbTransport);
			
			List<Transfee> addList = transfeeMap.get(1);
			List<Transfee> updateList = transfeeMap.get(0);
			List<Transfee> delList = transfeeMap.get(-1);
			
			if(AppUtils.isNotBlank(addList)){//添加运费模板的运费时，一并将运费模板引用的城市 添加
				List<Long> ids = transfeeDao.save(addList);
				
				List<TransCity> transCityList = new ArrayList<TransCity>();
				for (int i = 0; i < ids.size(); i++) {
					Transfee fee= addList.get(i);
					if(AppUtils.isNotBlank(fee) && AppUtils.isNotBlank(fee.getCityIdList())){
						
						
						/** 以防  fee.getCityIdList 里面的 CityId 有重复 ； 用 TreeSet 来去重 ；**/

						Set<Long> cityIdSet = new TreeSet<Long>(fee.getCityIdList());
						
						Iterator<Long> iterator = cityIdSet.iterator();
				          
				        while(iterator.hasNext()){
				            TransCity transCity = new TransCity();
							transCity.setCityId(iterator.next());
							transCity.setTransfeeId(ids.get(i));
							transCityList.add(transCity);
				        }  
						
					}
				}
				
				if(AppUtils.isNotBlank(transCityList)){
					transCityDao.saveTransCity(transCityList);
				}
			}
			if(AppUtils.isNotBlank(delList)){ //删除运费模板的运费时，一并删除 运费模板引用的城市
				for (Transfee transfee : delList) {
					List<TransCity> transCityList = new ArrayList<TransCity>();
					if(AppUtils.isNotBlank(transfee.getCityIdList())){
						for(Long cityId:transfee.getCityIdList()){
							TransCity transCity = new TransCity();
							transCity.setCityId(cityId);
							transCity.setTransfeeId(transfee.getId());
							transCityList.add(transCity);
						}
					}
					
					if(AppUtils.isNotBlank(transCityList)){
						transCityDao.deleteTransCity(transCityList);
					}
				}
				
				transfeeDao.delete(delList);
			}
			
			//对比更新 模板选中的城市
			updateTransCity(updateList);
		}
		
		
	}
	
	
	/** 对比更新 运费所选中城市  **/
	private void updateTransCity(List<Transfee> transfeeList){
		if(AppUtils.isNotBlank(transfeeList)){
			for (Transfee transfee : transfeeList) {
				
				List<Long> cityIdList = transfee.getCityIdList();
				HashSet<Long> hs = new HashSet<Long>(cityIdList);
				cityIdList =Arrays.asList( hs.toArray(new Long[]{}));
				
				if(AppUtils.isNotBlank(cityIdList)){
					
					List<TransCity> originCityList = transCityDao.getTransCitys(transfee.getId());
					Map<Integer, List<TransCity>> transCityMap = getTransCityComparer().compare(cityIdList,originCityList, transfee);
				
					List<TransCity> addTransCityList = transCityMap.get(1);
					List<TransCity> delTransCityList = transCityMap.get(-1);
					
					if(AppUtils.isNotBlank(delTransCityList)){ transCityDao.deleteTransCity(delTransCityList);}
					if(AppUtils.isNotBlank(addTransCityList)){ transCityDao.saveTransCity(addTransCityList);}
				}
				
				transfee.setStatus(Constants.ONLINE);
			}
			
			transfeeDao.update(transfeeList);
		}
	}

	@Override
	public boolean isAppTransport(Long id) {
		return transportDao.isAppTransport(id);
	}

	@Override
	public PageSupport<Transport> getTransportPage(String curPageNO, Transport transport, final Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Transport.class, curPageNO);
		cq.eq("transName", transport.getTransName());
		cq.eq("shopId", shopId);
		cq.addDescOrder("recDate");
		return transportDao.queryPage(cq, new PageSupportHandler<Transport>() {
			public void handle(List<Transport> transportList) {
				 List<Transfee> transfeeList = transfeeDao.getTranfeeByShopId(shopId);
				 for(Transport transport : transportList){
					 for(Transfee transfee:transfeeList){
						 if(transport.getId().equals(transfee.getTransportId())){
							 transport.addFeeList(transfee);
						 }
					 }
				 }
				
			}
		});
	}

	@Override
	public PageSupport<Transport> getFreightChargesTemp(String curPageNO, Long shopId, String transName) {
		return transportDao.getFreightChargesTemp(curPageNO,shopId, transName);
	}

	@Override
	public Integer checkByName(String name,Long shopId) {
		return transportDao.checkByName(name,shopId);
	}
}
