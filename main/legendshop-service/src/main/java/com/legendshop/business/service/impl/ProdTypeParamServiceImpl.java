/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdTypeParamDao;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.spi.service.ProdTypeParamService;
import com.legendshop.util.AppUtils;

/**
 * 商品类型跟参数的关系服务
 */
@Service("prodTypeParamService")
public class ProdTypeParamServiceImpl  implements ProdTypeParamService{
	
	@Autowired
    private ProdTypeParamDao prodTypeParamDao;

    public ProdTypeParam getProdTypeParam(Long id) {
        return prodTypeParamDao.getProdTypeParam(id);
    }

	@Override
	public ProdTypeParam getProdTypeParam(Long propId, Long prodTypeId) {
		return prodTypeParamDao.getProdTypeParam(propId, prodTypeId);
	}

	@Override
	public Long saveProdTypeParam(ProdTypeParam prodTypeParam) {
		if (!AppUtils.isBlank(prodTypeParam.getId())) {
			updateProdTypeParam(prodTypeParam);
			return prodTypeParam.getId();
		}
		return (Long) prodTypeParamDao.saveProdTypeParam(prodTypeParam);
	}

	@Override
	public void updateProdTypeParam(ProdTypeParam prodTypeParam) {
		prodTypeParamDao.updateProdTypeParam(prodTypeParam);
	}


}
