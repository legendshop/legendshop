/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProductCommentStat;

/**
 * 评论统计Dao.
 */

public interface ProductCommentStatDao extends Dao<ProductCommentStat, Long> {
     
	public abstract ProductCommentStat getProductCommentStat(Long id);
	
    public abstract int deleteProductCommentStat(ProductCommentStat productCommentStat);
	
	public abstract Long saveProductCommentStat(ProductCommentStat productCommentStat);
	
	public abstract int updateProductCommentStat(ProductCommentStat productCommentStat);
	
	public abstract ProductCommentStat getProductCommentStatByProdId(Long prodId);

	public abstract ProductCommentStat getProductCommentStatByShopId(Long shopId);

	public abstract ProductCommentStat getProductCommentStatByProdIdForUpdate(Long prodId);

	public abstract void updateCommentsAndScore(Integer comments,
			Integer score, Long prodId);
	
 }
