/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.StockLog;

/**
 *库存历史服务Dao
 */
public interface StockLogDao extends Dao<StockLog, Long> {
     
	public abstract StockLog getStockLog(Long id);
	
    public abstract int deleteStockLog(StockLog stockLog);
	
	public abstract Long saveStockLog(StockLog stockLog);
	
	public abstract int updateStockLog(StockLog stockLog);
	
	public abstract void saveStockLogs(List<StockLog> stockLogs);

	public abstract void deleteStockLogById(String skuIdList);

	public abstract PageSupport<StockLog> loadStockLog(Long prodId, Long skuId, String curPageNO);
	
 }
