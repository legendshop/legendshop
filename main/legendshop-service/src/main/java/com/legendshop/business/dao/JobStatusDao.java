/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.JobStatus;

/**
 * Job状态控制Dao.
 */
public interface JobStatusDao extends Dao<JobStatus, Long> {
     
	public abstract JobStatus getJobStatus(Long id);
	
    public abstract int deleteJobStatus(JobStatus jobStatus);
	
	public abstract Long saveJobStatus(JobStatus jobStatus);
	
	public abstract int updateJobStatus(JobStatus jobStatus);
	
	public abstract JobStatus getJobStatusByJobName(String jobName);
	
 }
