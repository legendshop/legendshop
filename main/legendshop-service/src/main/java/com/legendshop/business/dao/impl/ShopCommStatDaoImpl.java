/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopCommStatDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;

/**
 *店铺评分统计Dao
 */
@Repository
public class ShopCommStatDaoImpl extends GenericDaoImpl<ShopCommStat, Long> implements ShopCommStatDao  {

	public ShopCommStat getShopCommStat(Long id){
		return getById(id);
	}
	
    public int deleteShopCommStat(ShopCommStat shopCommStat){
    	return delete(shopCommStat);
    }
	
	public Long saveShopCommStat(ShopCommStat shopCommStat){
		return save(shopCommStat);
	}
	
	public int updateShopCommStat(ShopCommStat shopCommStat){
		return update(shopCommStat);
	}

	@Override
	public ShopCommStat getShopCommStatByShopIdForUpdate(Long shopId) {
		
		String sql = "SELECT * FROM ls_shop_comm_stat WHERE shop_id = ? FOR UPDATE";
		
		return this.get(sql, ShopCommStat.class, shopId);
	}
	
	@Override
	@Cacheable(value = "ShopCommStat", key = "#shopId")
	public ShopCommStat getShopCommStatByShopId(Long shopId) {
		String sql="SELECT  SUM(s.score) AS score,SUM(s.count) AS count FROM ls_shop_comm_stat s WHERE s.shop_id = ?";
		return this.get(sql, ShopCommStat.class, shopId);
	}
	
 }
