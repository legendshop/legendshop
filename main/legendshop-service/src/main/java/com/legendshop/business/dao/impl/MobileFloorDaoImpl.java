/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MobileFloorDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloor;
import com.legendshop.util.AppUtils;

/**
 * 手机端html5 首页楼层服务.
 */
@Repository
public class MobileFloorDaoImpl extends GenericDaoImpl<MobileFloor, Long> implements MobileFloorDao  {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	public MobileFloor getMobileFloor(Long id){
		return getById(id);
	}
	
    public int deleteMobileFloor(MobileFloor mobileFloor){
    	return delete(mobileFloor);
    }
	
	public Long saveMobileFloor(MobileFloor mobileFloor){
		return save(mobileFloor);
	}
	
	public int updateMobileFloor(MobileFloor mobileFloor){
		return update(mobileFloor);
	}
	
	@Override
	public List<MobileFloor> queryMobileFloor(Long shopId) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("status", 1).addAscOrder("seq"));
	}

	@Override
	public PageSupport<MobileFloor> getMobileFloorPage(String curPageNO, MobileFloor mobileFloor) {
		CriteriaQuery cq = new CriteriaQuery(MobileFloor.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        if(AppUtils.isNotBlank(mobileFloor.getName())){
        	cq.like("name", mobileFloor.getName()+ "%");
        }
        cq.addAscOrder("seq");
		return queryPage(cq);
	}
	
 }
