/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.model.dto.app.AppRechargeDto;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.spi.service.SubSettlementService;
import com.legendshop.util.AppUtils;

/**
 * 订单结算票据中心
 * 
 * @author tony
 * 
 */
@Service("subSettlementService")
public class SubSettlementServiceImpl implements SubSettlementService {

	@Autowired
	private SubSettlementDao subSettlementDao;

	public Long saveSubSettlement(SubSettlement subSettlement) {
		if (!AppUtils.isBlank(subSettlement.getSubSettlementId())) {
			updateSubSettlement(subSettlement);
			return subSettlement.getSubSettlementId();
		}
		return subSettlementDao.saveSubSettlement(subSettlement);
	}

	public int updateSubSettlement(SubSettlement subSettlement) {
		return subSettlementDao.updateSubSettlement(subSettlement);
	}


	@Override
	public SubSettlement getSubSettlement(String settlementNumbers, String userId) {
		return subSettlementDao.getSubSettlement(settlementNumbers, userId);
	}

	/**
	 * 查找结算单
	 */
	@Override
	public SubSettlement getSubSettlementBySn(String settlement_sn) {
		return subSettlementDao.getSubSettlementBySn(settlement_sn);
	}

	@Override
	public AppRechargeDto getSubSettlementToAppOrderDto(String subSettlementSn, String userId) {
		SubSettlement sub = subSettlementDao.getSubSettlement(subSettlementSn, userId);
		AppRechargeDto appOrderDto = new AppRechargeDto();
		appOrderDto.setActualTotal(sub.getCashAmount());
		appOrderDto.setPayTypeName(sub.getPayTypeName());
		return appOrderDto;
	}
	
	
}
