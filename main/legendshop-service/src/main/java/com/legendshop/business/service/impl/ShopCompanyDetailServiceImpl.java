/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopCompanyDetailDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.ShopCompanyDetail;
import com.legendshop.model.entity.User;
import com.legendshop.spi.service.ShopCompanyDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 公司详细信息服务
 */
@Service("shopCompanyDetailService")
public class ShopCompanyDetailServiceImpl implements ShopCompanyDetailService {

    @Autowired
    private ShopCompanyDetailDao shopCompanyDetailDao;

    @Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private UserDetailDao userDetailDao;


    public ShopCompanyDetail getShopCompanyDetail(Long id) {
        return shopCompanyDetailDao.getShopCompanyDetail(id);
    }

    public void deleteShopCompanyDetail(ShopCompanyDetail shopCompanyDetail) {
        shopCompanyDetailDao.deleteShopCompanyDetail(shopCompanyDetail);
    }

    public Long saveShopCompanyDetail(ShopCompanyDetail shopCompanyDetail) {
        if (!AppUtils.isBlank(shopCompanyDetail.getId())) {
            updateShopCompanyDetail(shopCompanyDetail);
            return shopCompanyDetail.getId();
        }
        return shopCompanyDetailDao.saveShopCompanyDetail(shopCompanyDetail);
    }

    public void updateShopCompanyDetail(ShopCompanyDetail shopCompanyDetail) {
        shopCompanyDetailDao.updateShopCompanyDetail(shopCompanyDetail);
    }

    @Override
    public ShopCompanyDetail getShopCompanyDetailByShopId(Long shopId) {
        return this.shopCompanyDetailDao.getShopCompanyDetailByShopId(shopId);
    }

    /**
     * 修改公司信息
     */
    @Override
    public boolean updataShopCompanyInfo(ShopCompanyDetail detail, ShopCompanyDetail newCompanyDetail) {
        detail.setLicenseNumber(newCompanyDetail.getLicenseNumber());
        detail.setLicenseProvinceid(newCompanyDetail.getLicenseProvinceid());
        detail.setLicenseCityid(newCompanyDetail.getLicenseCityid());
        detail.setLicenseAreaid(newCompanyDetail.getLicenseAreaid());
        detail.setLicenseAddr(newCompanyDetail.getLicenseAddr());
        detail.setLicenseEstablishDate(newCompanyDetail.getLicenseEstablishDate());
        detail.setLicenseStartDate(newCompanyDetail.getLicenseStartDate());
        detail.setLicenseEndDate(newCompanyDetail.getLicenseEndDate());
        detail.setLicenseRegCapital(newCompanyDetail.getLicenseRegCapital());
        detail.setLicenseBusinessScope(newCompanyDetail.getLicenseBusinessScope());
        detail.setCompanyProvinceid(newCompanyDetail.getCompanyProvinceid());
        detail.setCompanyCityid(newCompanyDetail.getCompanyCityid());
        detail.setCompanyAreaid(newCompanyDetail.getCompanyAreaid());
        detail.setCompanyAddress(newCompanyDetail.getCompanyAddress());
        detail.setCompanyName(newCompanyDetail.getCompanyName());
        detail.setCompanyNum(newCompanyDetail.getCompanyNum());
        detail.setCompanyIndustry(newCompanyDetail.getCompanyIndustry());
        detail.setCompanyProperty(newCompanyDetail.getCompanyProperty());
        detail.setCompanyTelephone(newCompanyDetail.getCompanyTelephone());
        detail.setTaxRegistrationNum(newCompanyDetail.getTaxRegistrationNum());
        detail.setTaxpayerId(newCompanyDetail.getTaxpayerId());
        detail.setSocialCreditCode(newCompanyDetail.getSocialCreditCode());
        detail.setBankAccountName(newCompanyDetail.getBankAccountName().trim());
        detail.setBankCard(newCompanyDetail.getBankCard().trim());
        detail.setBankName(newCompanyDetail.getBankName().trim());
        detail.setBankLineNum(newCompanyDetail.getBankLineNum().trim());
        detail.setBankProvinceid(newCompanyDetail.getBankProvinceid());
        detail.setBankCityid(newCompanyDetail.getBankCityid());
        detail.setBankAreaid(newCompanyDetail.getBankAreaid());
        detail.setCodeType(newCompanyDetail.getCodeType());

        //发人身份证电子版
        String legalIdCard = "";
        User user = userDetailDao.getUserByName(newCompanyDetail.getUserName());
        if (newCompanyDetail.getLegalIdCardPic().getSize() > 0) {

            legalIdCard = attachmentManager.upload(newCompanyDetail.getLegalIdCardPic());
            //保存附件表
            attachmentManager.saveImageAttachment(newCompanyDetail.getUserName(), user.getId(), user.getShopId(), legalIdCard, newCompanyDetail.getLegalIdCardPic(), AttachmentTypeEnum.OPENSHOP);
            //删除原引用
            if (AppUtils.isNotBlank(detail.getLegalIdCard())) {
                attachmentManager.delAttachmentByFilePath(detail.getLegalIdCard());
                attachmentManager.deleteTruely(detail.getLegalIdCard());
            }
            detail.setLegalIdCard(legalIdCard);

        }
        //营业执照副本电子版

        String licensePics = "";
        if (newCompanyDetail.getLicensePics().getSize() > 0) {
            licensePics = attachmentManager.upload(newCompanyDetail.getLicensePics());
            //保存附件表
            attachmentManager.saveImageAttachment(newCompanyDetail.getUserName(), user.getId(), user.getShopId(), licensePics, newCompanyDetail.getLicensePics(), AttachmentTypeEnum.OPENSHOP);
            //删除原引用
            if (AppUtils.isNotBlank(detail.getLicensePic())) {
                attachmentManager.delAttachmentByFilePath(detail.getLicensePic());
                attachmentManager.deleteTruely(detail.getLicensePic());
            }
            detail.setLicensePic(licensePics);
        }

        //银行开户许可证电子版
        String bankPermitImg = "";
        if (newCompanyDetail.getBankPermitPic().getSize() > 0) {
            bankPermitImg = attachmentManager.upload(newCompanyDetail.getBankPermitPic());
            //保存附件表
            attachmentManager.saveImageAttachment(newCompanyDetail.getUserName(), user.getId(), user.getShopId(), bankPermitImg, newCompanyDetail.getBankPermitPic(), AttachmentTypeEnum.OPENSHOP);
            //删除原引用
            if (AppUtils.isNotBlank(detail.getBankPermitImg())) {
                attachmentManager.delAttachmentByFilePath(detail.getBankPermitImg());
                attachmentManager.deleteTruely(detail.getBankPermitImg());
            }
            detail.setBankPermitImg(bankPermitImg);
        }
        if (detail.getCodeType() != null && detail.getCodeType() == 1) {
            //税务登记证电子版
            String taxRegistrationNumPic = "";
            if (newCompanyDetail.getTaxRegistrationNumEPic().getSize() > 0) {
                taxRegistrationNumPic = attachmentManager.upload(newCompanyDetail.getTaxRegistrationNumEPic());
                //保存附件表
                attachmentManager.saveImageAttachment(newCompanyDetail.getUserName(), user.getId(), user.getShopId(), taxRegistrationNumPic, newCompanyDetail.getTaxRegistrationNumEPic(), AttachmentTypeEnum.OPENSHOP);
                //删除原引用
                if (AppUtils.isNotBlank(detail.getTaxRegistrationNumE())) {
                    attachmentManager.delAttachmentByFilePath(detail.getTaxRegistrationNumE());
                    attachmentManager.deleteTruely(detail.getTaxRegistrationNumE());
                }
                detail.setTaxRegistrationNumE(taxRegistrationNumPic);
            }
        }

        Integer result = shopCompanyDetailDao.update(detail);

        return result > 0;
    }
}
