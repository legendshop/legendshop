/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdTypeDao;
import com.legendshop.business.dao.ProdTypeParamDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdTypeBrand;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.model.entity.ProdTypeProperty;
import com.legendshop.spi.service.ProdTypeService;
import com.legendshop.util.AppUtils;

/**
 * 商品类型Service.
 */
@Service("prodTypeService")
public class ProdTypeServiceImpl  implements ProdTypeService{

	@Autowired
    private ProdTypeDao prodTypeDao;

	@Autowired
    private ProdTypeParamDao prodTypeParamDao;

    public ProdType getProdType(Long id) {
        return prodTypeDao.getProdType(id);
    }
    
    @Override
    public ProdTypeParam getProdTypeParam(Long propId,Long prodTypeId) {
        return prodTypeParamDao.getProdTypeParam(propId,prodTypeId);
    }

    public void deleteProdType(ProdType prodType) {
        prodTypeDao.deleteProdType(prodType);
    }

    public Long saveProdType(ProdType prodType) {
		if(prodType.getAttrEditable() == null){
			prodType.setAttrEditable(1);
		}
		if(prodType.getParamEditable() == null){
			prodType.setParamEditable(1);
		}
        if (!AppUtils.isBlank(prodType.getId())) {
            updateProdType(prodType);
            return prodType.getId();
        }
        prodType.setRecDate(new Date());
        return prodTypeDao.saveProdType(prodType);
    }

    public void updateProdType(ProdType prodType) {
        prodTypeDao.updateProdType(prodType);
    }

	@Override
	public void saveTypeProp(List<ProdTypeProperty> propertyList, Long id) {
		prodTypeDao.saveTypeProp(propertyList, id);
	}

	@Override
	public void saveTypeBrand(List<ProdTypeBrand> brandList, Long id) {
		prodTypeDao.saveTypeBrand(brandList, id);
	}

	@Override
	public void deleteProdProp(Long propId, Long prodTypeId) {
		prodTypeDao.deleteProdProp(propId, prodTypeId);
	}

	@Override
	public void deleteBrand(Long brandId, Long prodTypeId) {
		prodTypeDao.deleteBrand(brandId, prodTypeId);
	}

	@Override
	public void saveParameterProperty(List<ProdTypeParam> propertyList, Long prodTypeId) {
		prodTypeDao.saveParameterProperty(propertyList, prodTypeId);
	}

	@Override
	public void deleteParameterProp(Long propId, Long prodTypeId) {
		prodTypeDao.deleteParameterProp(propId, prodTypeId);
	}

	@Override
	public List<KeyValueEntity> loadProdType() {
		return prodTypeDao.loadProdType();
	}

	@Override
	public List<ProdType> getProdTypeListWithId(Long tpyeId) {
		return prodTypeDao.getProdTypeListWithId(tpyeId);
	}

	@Override
	public PageSupport<ProdType> getProdTypePage(String typeName) {
		return prodTypeDao.getProdTypePage(typeName);
	}

	@Override
	public PageSupport<ProdType> queryProdTypePage(String curPageNO, ProdType prodType, Long categoryId) {
		return prodTypeDao.queryProdTypePage(curPageNO,prodType,categoryId);
	}

	@Override
	public PageSupport<ProdType> getProdTypePage(String curPageNO, String name) {
		return prodTypeDao.getProdTypePage(curPageNO,name);
	}

	@Override
	public PageSupport<ProdType> getProdTypeByName(String typeName) {
		return prodTypeDao.getProdTypeByName(typeName);
	}

	@Override
	public List<Long> ifDeleteProductProperty(Long proTypeId, Long propId) {
		return prodTypeDao.ifDeleteProductProperty(proTypeId, propId);
	}
}
