/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.NavigationDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Navigation;
import com.legendshop.spi.service.NavigationService;
import com.legendshop.util.AppUtils;

/**
 * 网站导航服务.
 */
@Service("navigationService")
public class NavigationServiceImpl implements NavigationService {

	@Autowired
	private NavigationDao navigationDao;

	public Navigation getNavigation(Long id) {
		return navigationDao.getNavigation(id);
	}

	public void deleteNavigation(Navigation navigation) {
		navigationDao.deleteNavigation(navigation);
	}

	public Long saveNavigation(Navigation navigation) {
		if (!AppUtils.isBlank(navigation.getNaviId())) {
			updateNavigation(navigation);
			return navigation.getNaviId();
		}
		return (Long) navigationDao.save(navigation);
	}

	public void updateNavigation(Navigation navigation) {
		navigationDao.updateNavigation(navigation);
	}

	@Override
	public List<Navigation> getNavigationList() {
		return navigationDao.getNavigationList();
	}

	@Override
	public PageSupport<Navigation> getNavigationPage(String curPageNO) {
		return navigationDao.getNavigationPage(curPageNO);
	}
}
