/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.auction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.util.AppUtils;

/**
 * 拍卖购物车.
 *
 */
@Service("orderAuctionsCartResolver")
public class OrderAuctionsCartResolver extends AbstractAddOrder implements IOrderCartResolver {

	/** The bidders win service. */
	@Autowired
	private BiddersWinService biddersWinService;

	/**
	 * 查询订单
	 */
	@Override
	@SuppressWarnings("unchecked")
	public UserShopCartList queryOrders(Map<String, Object> params) {
		List<Long> ids = (List<Long>) params.get("ids");
		Long biddId = ids.get(0);
		String userId = (String) params.get("userId");
		String userName = (String) params.get("userName");
		Long adderessId = (Long) params.get("adderessId");// 用户收货地址

		BiddersWin win = biddersWinService.getBiddersWin(userId, biddId);
		if (win == null) {
			throw new BusinessException("没有找到该拍卖");
		}
		if (BiddersWinStatusEnum.NOORDER.value().intValue() != win.getStatus().intValue()) {
			throw new BusinessException("请勿重复支付订单");
		}
		ShopCartItem shopCartItem = biddersWinService.findBiddersWinCart(win.getId(), win.getProdId(), win.getSkuId());
		if (shopCartItem == null) {
			throw new BusinessException("下单失败,请联系商城管理员,确认该拍物是否已经下架！");
		}
		shopCartItem.setCalculateMarketing(false);
		shopCartItem.setBasketCount(1);
		List<ShopCartItem> shopCartItems = new ArrayList<>(1);
		shopCartItems.add(shopCartItem);
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, shopCartItems, adderessId,CartTypeEnum.AUCTIONS.toCode());
		if (userShopCartList == null) {
			return null;
		}
		userShopCartList.setType(CartTypeEnum.AUCTIONS.toCode());
		return userShopCartList;
	}

	/**
	 * 下单操作.
	 *
	 * @param userShopCartList  用户购物车列表
	 * @return the list< long>
	 */
	@Override
	public List<String> addOrder(UserShopCartList userShopCartList) {
		List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
		List<String> subIds = super.submitOrder(userShopCartList);
		if (AppUtils.isBlank(subIds)) {
			return null;
		}
		for (Iterator<ShopCarts> shopIterator = shopCarts.iterator(); shopIterator.hasNext();) {
			ShopCarts shop = shopIterator.next();
			List<ShopCartItem> items = shop.getCartItems();
			List<Long> removeBaskets = new ArrayList<Long>();
			for (Iterator<ShopCartItem> iterator = items.iterator(); iterator.hasNext();) {
				ShopCartItem cartItem = (ShopCartItem) iterator.next();
				removeBaskets.add(cartItem.getBasketId()); // 删除购物车
				cartItem = null;
			}
			// 处理拍卖状态
			for (Iterator<Long> iterator = removeBaskets.iterator(); iterator.hasNext();) {
				Long id = (Long) iterator.next();
				biddersWinService.updateBiddersWin(id, shop.getSubNember(), shop.getSubId(), BiddersWinStatusEnum.WAITPAY.value());
			}
		}
		shopCarts = null;
		return subIds;
	}

}
