/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.User;


/**
 * 用户管理的Dao
 */
public interface UserManagerDao extends  Dao<User, String> {

	public abstract String saveUser(User user);

	public abstract User getUser(String userId);

	public abstract void updateUser(User user);

	public abstract void deleteUser(User user);
	
	public abstract void deleteUseById(String userId);
	
	public abstract boolean isUserExists(String userName);

	public abstract User getUserByName(String userName);
	/**
	 * Update user password.
	 *
	 * @param user the user
	 */
	public void updateUserPassword(User user);
	
	/**
	 * Query user.
	 *
	 * @param curPageNO the cur page no
	 * @param name the name
	 * @param enabled the enabled
	 * @param pageSize 
	 * @return the page support< user>
	 */
	public abstract PageSupport<User> queryUser(String curPageNO, String name, String enabled, Integer pageSize);
	
	public abstract PageSupport<User> queryUser(String curPageNO, String name, String enabled, String mobile, Integer pageSize);
	
}
