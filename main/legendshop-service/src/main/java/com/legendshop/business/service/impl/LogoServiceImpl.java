/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.cache.ShopDetailUpdate;
import com.legendshop.business.dao.LogoDao;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.LogoService;

/**
 * Logo服务.
 */
@Service("logoService")
public class LogoServiceImpl implements LogoService {

	@Autowired
	private LogoDao logoDao;

	@Override
	@ShopDetailUpdate
	public void deleteLogo(ShopDetail shopDetail) {
		logoDao.deleteLogo(shopDetail);
	}

	@Override
	@ShopDetailUpdate
	public void updateLogo(ShopDetail shopDetail) {
		logoDao.updateLogo(shopDetail);
	}
}
