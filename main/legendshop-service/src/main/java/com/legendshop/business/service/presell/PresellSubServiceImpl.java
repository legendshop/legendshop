/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.presell;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.spi.service.PresellSubService;
import com.legendshop.util.AppUtils;

import java.util.List;

/**
 * 预售订单服务实现类
 */
@Service("presellSubService")
public class PresellSubServiceImpl implements PresellSubService {

	@Autowired
	private PresellSubDao presellSubDao;
	
	/**
	 * 删除预售订单对象 
	 */
	public void deletePresellSub(PresellSub presellSub) {
		presellSubDao.deletePresellSub(presellSub);
	}

	/**
	 * 保存预售订单对象 
	 */
	public Long savePresellSub(PresellSub presellSub) {
		if (!AppUtils.isBlank(presellSub.getSubId())) {
			updatePresellSub(presellSub);
			return presellSub.getSubId();
		}
		return presellSubDao.savePresellSub(presellSub);
	}

	/**
	 * 更新预售订单对象 
	 */
	public void updatePresellSub(PresellSub presellSub) {
		presellSubDao.updatePresellSub(presellSub);
	}

	/**
	 * 
	 */
	@Override
	public PresellSub getPresellSub(String userId, String subNumber) {
		return presellSubDao.getPresellSub(userId,subNumber);
	}

	@Override
	public boolean findIfJoinPresell(Long productId) {
		return presellSubDao.findIfJoinPresell(productId);
	}

	@Override
	public PresellSub getPresellSubByPresell(String presellId) {
		return presellSubDao.getPresellSubByPresell(presellId);
	}

	@Override
	public PresellSub getPresellSub(String subNumber) {
		return presellSubDao.getPresellSub(subNumber);
	}

	/**
	 * 根据结算单号获取当期结算的预售订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<PresellSub> queryPresellSubList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {
		return presellSubDao.queryPresellSubList(curPageNO,shopId,subNumber,shopOrderBill);
	}
}
