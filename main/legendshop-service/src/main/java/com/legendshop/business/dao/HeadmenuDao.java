/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headmenu;

/**
 * Headmenu首页菜单Dao.
 */
public interface HeadmenuDao extends Dao<Headmenu, Long> {
     
	public abstract Headmenu getHeadmenu(Long id);
	
    public abstract int deleteHeadmenu(Headmenu headmenu);
	
	public abstract Long saveHeadmenu(Headmenu headmenu);
	
	public abstract int updateHeadmenu(Headmenu headmenu);
	
	public abstract List<Headmenu> getHeadmenuAll(String type);

	public abstract PageSupport<Headmenu> getHeadmenu(String curPageNO, Headmenu headmenu, String type);
	
 }
