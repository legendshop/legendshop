/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.business.dao.ActiveBannerDao;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 *  ActiveBanner服务实现类
 */
@Service("activeBannerService")
public class ActiveBannerServiceImpl  implements ActiveBannerService{

	@Autowired
	private ActiveBannerDao activeBannerDao;
	
	@Autowired
	private AttachmentManager attachmentManager;


	/**
	 *  根据Id获取
	 */
	public ActiveBanner getBanner(Long id) {
		return activeBannerDao.getBanner(id);
	}

	/**
	 *  删除
	 */ 
	public void deleteBanner(ActiveBanner activeBanner) {
		if (AppUtils.isNotBlank(activeBanner.getImageFile())) {
			// 删除
			attachmentManager.deleteTruely(activeBanner.getImageFile());
		}
		activeBannerDao.deleteBanner(activeBanner);
	}

	/**
	 *  保存
	 */	    
	public Long saveBanner(ActiveBanner banner, String userName, String userId, Long shopId, MultipartFile file) {
		
		String pic = null;
		if (file != null && file.getSize() > 0) {
			pic = attachmentManager.upload(file);
		}
		Long id = null;
		
		if (!AppUtils.isBlank(banner.getId())) {
			ActiveBanner oldBanner = activeBannerDao.getBanner(banner.getId());
			String image = oldBanner.getImageFile();
			oldBanner.setSeq(banner.getSeq());
			oldBanner.setUrl(banner.getUrl());
			if(AppUtils.isNotBlank(pic)){
				oldBanner.setImageFile(pic);
				attachmentManager.deleteTruely(image);
			}
			oldBanner.setBannerType(banner.getBannerType());
			updateBanner(oldBanner);
			return banner.getId();
		}else{
			banner.setImageFile(pic);
			id = activeBannerDao.saveBanner(banner);
			if(AppUtils.isNotBlank(pic)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, pic, file, AttachmentTypeEnum.MERGEGROUP);
			}
		}
		return id;
	}

	/**
	 *  更新
	 */	
	public void updateBanner(ActiveBanner activeBanner) {
		activeBannerDao.updateBanner(activeBanner);
	}

	public List<ActiveBanner> getBannerList(String type) {
		return activeBannerDao.getBannerList(type);
	}


}
