/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.entity.MergeGroupOperate;

/**
 * The Class MergeGroupOperateDao.
 * 每个团的运营信息Dao接口
 */
public interface MergeGroupOperateDao extends GenericDao<MergeGroupOperate, Long> {

   	/**
	 *  根据Id获取每个团的运营信息
	 */
	public abstract MergeGroupOperate getMergeGroupOperate(Long id);
	
   /**
	 *  删除每个团的运营信息
	 */
    public abstract int deleteMergeGroupOperate(MergeGroupOperate mergeGroupOperate);
    
   /**
	 *  保存每个团的运营信息
	 */	
	public abstract Long saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate);
	
	/**
	 * 保存每个团的运营信息
	 * @param mergeGroupOperate
	 * @param operateId
	 */
	public abstract void saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate, Long operateId);

   /**
	 *  更新每个团的运营信息
	 */		
	public abstract int updateMergeGroupOperate(MergeGroupOperate mergeGroupOperate);

	/**
	 *  获取本团运营信息
	 */
	public abstract MergeGroupOperate findMergeGroupOperate(Long shopId, String addNumber);
	

	//获取团运营信息，用于校检人数是否满团
	public abstract MergeGroupJoinDto getMergeGroupOperateByAddNumber(Long activeId, String addNumber);

	//校检用户是否参加了该团
	public abstract Long getMergeGroupAddByUserId(String userId, String addNumber);

	public abstract MergeGroupOperate getMergeGroupOperateByAddNumber(String addNumber);

	public abstract void updateMergeGroupOperateList(List<MergeGroupOperate> cancalOperates);

	public abstract List<MergeGroupOperate> findHaveInMergeGroupOperate();

	public abstract List<MergeGroupOperate> findHaveInMergeGroupOperate(Long mergeId);

 }
