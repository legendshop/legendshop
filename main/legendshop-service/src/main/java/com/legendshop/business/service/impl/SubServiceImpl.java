/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import com.legendshop.app.dto.IncomeTodayDto;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.exception.NotStocksException;
import com.legendshop.base.log.TimerLog;
import com.legendshop.business.dao.*;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.MergeGroupSubDto;
import com.legendshop.model.dto.SubCountsDto;
import com.legendshop.model.dto.SubDto;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.spi.service.*;
import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 订单服务实现.
 */
@Service("subService")
public class SubServiceImpl implements SubService {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SubServiceImpl.class);

	@Autowired
	private SubDao subDao;

	@Autowired
	private SubHistoryDao subHistoryDao;

	@Autowired
	private StockService stockService;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private OrderService orderService;

	@Autowired
	private CouponDao couponDao;

	@Autowired
	private UserCouponDao userCouponDao;

	@Autowired
	private SubSettlementDao subSettlementDao;

	@Autowired
	private SubSettlementItemDao subSettlementItemDao;

	@Autowired
	private ConstTableDao constTableDao;

	@Autowired
	private StoreOrderDao storeOrderDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private DeliveryCorpDao deliveryCorpDao;

	@Autowired
	private DeliveryTypeDao deliveryTypeDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private MergeGroupAddDao mergeGroupAddDao;

	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private SeckillService seckillService;

	@Autowired
	private ProductService productService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ConstTableService constTableService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 短信发送者
	 */
	@Resource(name = "sendSMSProcessor")
	private EventProcessor<ShortMessageInfo> sendSMSProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 超时时间，超过这个时间则不能修改物流单号
	 */
	private int timeOutHours = 1;

	@Override
	public MySubDto findOrderDetail(String subNumber) {
		if (AppUtils.isBlank(subNumber)) {
			log.warn("Can not found sub by null string");
			return null;
		}
		MySubDto subDto = subDao.findOrderDetail(subNumber);
		if (AppUtils.isBlank(subDto)) {
			log.warn("Can not found sub by {}", subNumber);
		}
		return subDto;
	}

	/**
	 * vue获取订单详情
	 */
	@Override
	public MySubDto findOrderDetail(String subNumber, String userId) {
		if (AppUtils.isBlank(subNumber)) {
			log.warn("Can not found sub by null string");
			return null;
		}
		MySubDto sub = subDao.findOrderDetail(subNumber, userId);
		if (AppUtils.isNotBlank(sub)&&AppUtils.isNotBlank(sub.getSubOrderItemDtos())&&sub.getSubOrderItemDtos().size()>0){
			for (SubOrderItemDto subOrderItemDto : sub.getSubOrderItemDtos()) {
				subOrderItemDto.setInReturnValidPeriod(true);
				//判断是否过了可退款时间
				if(OrderStatusEnum.SUCCESS.value().equals(sub.getStatus()) && !isInReturnValidPeriod(subOrderItemDto.getProdId(),sub)){
					subOrderItemDto.setInReturnValidPeriod(false);
				}
			}
		}
		if (AppUtils.isBlank(sub)) {
			log.warn("Can not found sub by {}", subNumber);
		}
		return sub;
	}

	@Override
	public MySubDto findOrderDetail(String subNumber, Long shopId) {
		MySubDto subDto = subDao.findOrderDetail(subNumber, shopId);
		if (subDto == null) {
			throw new NotFoundException("sub not found ");
		}
		return subDto;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.business.service.timer.SubService#getSubBySubNumber(java
	 * .lang.String)
	 */

	public Sub getSubBySubNumber(String subNumber) {
		return subDao.getSubBySubNumber(subNumber);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.business.service.timer.SubService#updateSub(com.legendshop
	 * .model.entity.Sub)
	 */

	public void updateSub(Sub sub) {
		subDao.updateSub(sub);
	}

	@Override
	public void updateSubPayType(String payTypeId, String payTypeName, Long subId) {
		subDao.update("UPDATE ls_sub s SET s.pay_type_id = ? , S.pay_type_name = ? WHERE s.sub_id = ?", payTypeId, payTypeName, subId);
	}

	public Long getTotalProcessingOrder(Long shopId) {
		return subDao.getTotalProcessingOrder(shopId);
	}

	public Sub getSubById(Long subId) {
		return subDao.getSubById(subId);
	}

	/**
	 * 调整订单 TODO
	 */
	@Override
	public boolean updateSubPrice(String subNumber, Double freight, Double orderAmount, String userName) {
		Sub sub = subDao.getSubBySubNumber(subNumber);
		if (sub != null && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
			SubHistory subHistory = null;
			Double totalchash = Arith.add(freight, orderAmount);
			if (!freight.equals(sub.getFreightAmount()) || !totalchash.equals(sub.getActualTotal())) {
				subHistory = new SubHistory();
				Date time = new Date();
				String timeStr = DateUtil.DateToString(time, "yyyy-MM-dd HH:mm:ss");
				subHistory.setRecDate(time);
				subHistory.setStatus(SubStatusEnum.PRICE_CHANGE.value());
				subHistory.setSubId(sub.getSubId());
				subHistory.setUserName(userName);
				String reason = userName + "于" + timeStr + "调整价格操作,原订单总金额:" + sub.getActualTotal() + ",原订单运费金额:" + sub.getFreightAmount() + "--> 调整订单总金额为:"
						+ totalchash + ",调整运费金额为:" + freight;
				subHistory.setReason(reason);

				//计算修改的总金额，冗余到sub表  修改金额 = 原订单总金额 - 调整后订单总金额
				Double oldChangedPrice = sub.getChangedPrice();
				Double newChangedPrice = Arith.sub(totalchash,sub.getActualTotal());
				// 原订单商品总金额 = 原订单总金额 - 原运费
				double oldProdAmount = Arith.sub(sub.getActualTotal(),sub.getFreightAmount());

				//多次调整订单，叠加修改金额
				oldChangedPrice = Arith.add(oldChangedPrice, newChangedPrice) ;

				//更新订单信息
				sub.setChangedPrice(oldChangedPrice);
				sub.setActualTotal(totalchash);
				sub.setFreightAmount(freight);

				//更新订单项实际实付金额（订单项实付金额 = （订单项总金额/订单总金额）* 不含运费的订单金额）
				List<SubItem> subItems = subItemDao.getSubItem(subNumber);
				BigDecimal difference = BigDecimal.valueOf(orderAmount);
				for (int i = 0; i < subItems.size(); i++) {
					////订单总金额-营销优惠总金额-使用优惠券金额-使用红包金额-改价金额 = 不含运费的订单金额 by:linzh 20191009
					//double actualTotal = Arith.mul(Arith.mul(Arith.mul(Arith.mul(sub.getTotal(), sub.getDiscountPrice()), sub.getCouponOffPrice()), sub.getRedpackOffPrice()), oldChangedPrice);
					//Double itemActualAmount = Arith.mul(Arith.div(subItem.getProductTotalAmout(), sub.getTotal()), actualTotal);
					//subItem.setActualAmount(itemActualAmount);
					//subItemDao.updateSubItem(subItem);

					// 改价属于订单提交后，不需要计算优惠价和营销活动
					// 订单项的金额应该按比例分配
					// 取旧有订单金额算出订单项金额占比
					// 取改价后的商品总价乘以占比（取整，舍弃小数点后两位的值），最后取总价 - （所有）订单项 = 差值，差值算在最后一项中
					// by xslong 2020-4-10
					SubItem subItem = subItems.get(i);
					BigDecimal proportion = BigDecimal.valueOf(subItem.getActualAmount()).divide(BigDecimal.valueOf(oldProdAmount), 2, RoundingMode.HALF_UP);
					BigDecimal itemActualAmount = BigDecimal.valueOf(orderAmount).multiply(proportion).setScale(2, RoundingMode.DOWN);
					subItem.setActualAmount(itemActualAmount.doubleValue());
					difference = difference.subtract(itemActualAmount);
					if (i == (subItems.size() - 1)) {
						subItem.setActualAmount(BigDecimal.valueOf(subItem.getActualAmount()).add(difference).doubleValue());
					}
				}
				subItemDao.updateSubItem(subItems);

				/**
				 * 记录订单日志和系统操作日记
				 */
				subHistoryService.saveSubHistory(subHistory);
				updateSub(sub);

				// 清除Order缓存
				orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());

			}
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		BigDecimal proportion = BigDecimal.valueOf(0.019);
		proportion = proportion.setScale(2, RoundingMode.DOWN);
		System.out.println(proportion.doubleValue());
	}


	/**
	 * 删除订单
	 */
	@Override
	public boolean deleteSub(Sub sub) {
		return subDao.deleteSub(sub);
	}

	@Override
	public Sub getSubBySubNumberByUserId(String subNumber, String userId) {
		return subDao.getSubBySubNumberByUserId(subNumber, userId);
	}

	@Override
	public Sub getSubBySubNumberByShopId(String subNumber, Long ShopId) {
		return subDao.getSubBySubNumberByShopId(subNumber, ShopId);
	}

	@Override
	public List<Sub> getSubByIds(String[] subIds, String userId, Integer status) {
		return subDao.getSubByIds(subIds, userId, status);
	}

	@Override
	public void updateSubForPay(Sub sub, Integer currentStatus) {
		subDao.updateSubForPay(sub, currentStatus);
	}

	@Override
	@Transactional
	public boolean cancleOrder(Sub sub) {
		long chainId = 0;
		if (CartTypeEnum.SHOP_STORE.toCode().equals(sub.getSubType())) {
			// 取消门店订单
			chainId = storeOrderDao.cancleOrder(sub.getSubNumber(), sub.getUserId());
			if (chainId <= 0) {
				return false;
			}
			// 更新订单状态
			subDao.update("update ls_sub set update_date =? ,status=?,cancel_reason=? where sub_id=? and user_id=? ",
					new Date(), sub.getStatus(), sub.getCancelReason(), sub.getSubId(), sub.getUserId());
		} else if (CartTypeEnum.AUCTIONS.toCode().equals(sub.getSubType())) {
			// 如果是拍卖订单
			int result = subDao.update("update ls_bidders_win set status=-2 where sub_number=? and user_id=?  ",
					sub.getSubNumber(), sub.getUserId());
			if (result == 0) {
				return false;
			}
		}else if(CartTypeEnum.SECKILL.toCode().equals(sub.getSubType())){
			// 秒杀订单
			int result = seckillService.cancleUnpaymentSeckillOrder(sub.getActiveId(), sub.getUserId());
			if (result == 0) {
				return false;
			}
			result = subDao.update("update ls_sub set update_date =? ,status=?,cancel_reason=? where sub_id=? and user_id=? and status=1 ",
					new Date(), sub.getStatus(), sub.getCancelReason(), sub.getSubId(), sub.getUserId());
			if (result == 0) {
				return false;
			}
		} else {
			// 普通订单
			int result = subDao.update("update ls_sub set update_date =? ,status=?,cancel_reason=? where sub_id=? and user_id=? and status=1 ",
					new Date(), sub.getStatus(), sub.getCancelReason(), sub.getSubId(), sub.getUserId());
			if (result == 0) {
				return false;
			}
		}
		// 更新订单项
		List<SubItem> subItems = subItemDao.getSubItem(sub.getSubNumber(), sub.getUserId());
		String subType=sub.getSubType();
		for (SubItem subItem : subItems) {
			Long skuId = subItem.getSkuId();
			if (CartTypeEnum.SHOP_STORE.toCode().equals(sub.getSubType())) {// 门店订单
				if (AppUtils.isNotBlank(skuId)) { // SKU订单
					subDao.update("update ls_store_prod set stock=stock+? where shop_id=? and prod_id=?  ",
							subItem.getBasketCount(), sub.getShopId(), subItem.getProdId());

					subDao.update("update ls_store_sku set stock=stock+? where store_id=? and prod_id=? and sku_id=? ",
							subItem.getBasketCount(), chainId, subItem.getProdId(), skuId);
				} else {
					subDao.update("update ls_store_prod set stock=stock+? where shop_id=? and prod_id=?  ",
							subItem.getBasketCount(), subItem.getProdId(), sub.getShopId());
				}
			} else {
				// 如果是拍下减库存
				if (StockCountingEnum.STOCK_BY_ORDER.value().equals(subItem.getStockCounting())) {
					stockService.releaseHold(subItem.getProdId(), skuId, subItem.getBasketCount(), subType);
				}
			}
		}


		Integer num=0;
		// 如果使用了优惠券，退回优惠券
		List<UserCoupon> userCoupons = userCouponDao.getUserCouponsByOrderNumber(sub.getSubNumber());
		if (AppUtils.isNotBlank(userCoupons)) {
			for (UserCoupon userCoupon : userCoupons) {
				String orderNums=userCouponDao.getOrderNumById(userCoupon.getUserCouponId());
				if(AppUtils.isNotBlank(orderNums)){
					for (String number : orderNums.split(",")) {
						num += subDao.getSubCountByOrderNum(number);
					}
					if(num == 0 ){//判断用户劵是否还存在对应的订单
						userCoupon.setUseStatus(UserCouponEnum.UNUSED.value());
						userCoupon.setUseTime(null);
						userCoupon.setOrderNumber(null);
						userCoupon.setOrderPrice(null);
						userCouponDao.updateUserCoupon(userCoupon);
						couponDao.reduceCouponUseNum(userCoupon.getCouponId(), sub.getUserId());
					}
				}
			}
		}
		return true;
	}

	@Override
	public boolean changeDeliverGoods(String subNumber, Long dvyTypeId, String dvyFlowId, String info, String userName) {

		Sub sub = subDao.getSubBySubNumber(subNumber);
		if (!dvyTypeId.equals(sub.getDvyTypeId()) || !dvyFlowId.equals(sub.getDvyFlowId())) {
			SubHistory subHistory = new SubHistory();
			Date date = new Date();
			String time = subHistory.DateToString(date);
			subHistory.setRecDate(date);
			subHistory.setSubId(sub.getSubId());
			subHistory.setStatus(SubHistoryEnum.CHANGE_INFO.value());
			StringBuilder sb = new StringBuilder();
			sb.append(userName).append("于").append(time).append("编辑订单物流");
			subHistory.setUserName(sub.getUserName());
			subHistory.setReason(sb.toString());
			sub.setDvyTypeId(dvyTypeId);
			sub.setDvyFlowId(dvyFlowId);
			subDao.update(sub);
			subHistoryService.saveSubHistory(subHistory);
		}
		return true;
	}

	/**
	 * 发货，减少实际库存actualStocks
	 */
	@Override
	public String fahuo(String subNumber, Long dvyTypeId, String dvyFlowId, String info, Long shopId) {
		Sub sub = subDao.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "找不到该订单";
		} else if (!OrderStatusEnum.PADYED.value().equals(sub.getStatus())) {
			return "该订单不能发货,不是已经支付状态";
		} else if (!sub.getShopId().equals(shopId)) {
			return "找不到店铺该订单";
		}

		//TODO
		if(CartTypeEnum.PRESELL.toCode().equals(sub.getSubType())) {//如果是预售订单

		}

		List<SubItem> subItem = subItemDao.getSubItem(subNumber);
		if (AppUtils.isBlank(subItem)) {
			return "订单数据有问题";
		}
		for (int i = 0; i < subItem.size(); i++) {
			// 减少实际库存
			boolean conf = stockService.addActualHold(subItem.get(i).getProdId(), subItem.get(i).getSkuId(), subItem.get(i).getBasketCount());
			if (!conf) {
				// 在支付后减库存，有可能导致超卖现象
				if (log.isWarnEnabled()) {
					log.warn("发货时调用失败 addActualHold for prodId: {}, skuId: {}， basketCount: {}，provinceId:{} ",
							new Object[] { subItem.get(i).getProdId(), subItem.get(i).getSkuId(), subItem.get(i).getBasketCount(), });
				}
			}

		}

		sub.setDvyTypeId(dvyTypeId);
		sub.setDvyFlowId(dvyFlowId);
		sub.setStatus(OrderStatusEnum.CONSIGNMENT.value());
		Date date = new Date();
		sub.setDvyDate(date);
		int result = subDao.update(sub);
		if (result > 0) {
			SubHistory subHistory = new SubHistory();

			String time = subHistory.DateToString(date);
			subHistory.setRecDate(date);
			subHistory.setSubId(sub.getSubId());
			subHistory.setStatus(SubHistoryEnum.DELIVER_GOODS.value());

			StringBuilder sb = new StringBuilder();
			sb.append("商家").append("于").append(time).append("进行订单发货处理");
			subHistory.setUserName("商家");
			subHistory.setReason(sb.toString());
			subHistoryService.saveSubHistory(subHistory);
			orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());

			// 发送站内信 通知
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(sub.getUserName(), "发货通知", "亲，您的订单[" + subNumber + "]我们已经发货咯").getSource());
			/* 是否发送发货通知短信 */
			Boolean deliverSMS = systemParameterUtil.isDeliverProdSms();
			if (deliverSMS) {
				// 获取物流公司信息
				DeliveryType deliveryType = deliveryTypeDao.getDeliveryType(dvyTypeId);
				DeliveryCorp deliveryCorp = deliveryCorpDao.getDeliveryCorp(deliveryType.getDvyId());
				String dvyCompany = deliveryCorp.getName();
				// 获取用户的手机号码
				UserDetail userDetail = userDetailDao.getUserDetailById(sub.getUserId());
				// 处理订单号
				String substring = "*" + subNumber.substring(4);
				if (AppUtils.isNotBlank(userDetail) && AppUtils.isNotBlank(userDetail.getUserMobile())) {
					// 发送短信
					String templateParam = String.format("{\"order\":\"%s\",\"dvycompany\":\"%s\"}", substring, dvyCompany);
					String text = String.format("亲，您好！您的订单 %s已经发货。请注意查收%s的信息，欢迎再次光临我们！", subNumber, dvyCompany);
					sendSMSProcessor.process(new SendSMSEvent(userDetail.getUserMobile(), userDetail.getUserName(), templateParam, text).getSource());
				}
			}

		}
		return Constants.SUCCESS;
	}

	/**
	 * 修改物流单号
	 */
	@Override
	public String changeDeliverNum(String subNumber, Long deliv, String dvyFlowId, String info, Long shopId) {
		Sub sub = subDao.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "找不到该订单";
		} else if (!sub.getShopId().equals(shopId)) {
			return "找不到店铺该订单";
		}
		Date date = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, -timeOutHours);
		Date nows = cal.getTime();
		if (nows.after(sub.getDvyDate())) {
			return "已经超过" + timeOutHours + "个小时，不能修改物流单号了";
		}

		sub.setDvyTypeId(deliv);
		sub.setDvyFlowId(dvyFlowId);

		int result = subDao.update(sub);
		if (result > 0) {
			orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
		}
		return Constants.SUCCESS;
	}

	@Override
	public String findSubhisInfo(Long subId) {
		List<SubHistory> histories = subHistoryDao.findSubHistoryBySubId(subId);
		String info = "";
		if (AppUtils.isNotBlank(histories)) {
			List<KeyValueEntity> entities = new ArrayList<KeyValueEntity>();
			for (int i = 0; i < histories.size(); i++) {
				KeyValueEntity keyValueListEntity = new KeyValueEntity(histories.get(i).getUserName(), histories.get(i).getReason());
				entities.add(keyValueListEntity);
			}
			return JSONUtil.getJson(entities);
		}
		return info;
	}

	@Override
	public List<OrderExcelWriterDto> getOutPutExcelOrder(Long[] subIds) {

		if (AppUtils.isBlank(subIds)) {
			return null;
		}
		List<OrderSub> subs = subDao.getOutPutExcelOrder(subIds);
		if (AppUtils.isBlank(subs)) {
			return null;
		}
		List<OrderExcelWriterDto> excelWriterVos = new ArrayList<OrderExcelWriterDto>();
		for (int i = 0; i < subs.size(); i++) {
			OrderSub sub = subs.get(i);
			OrderExcelWriterDto excelWriterDto = new OrderExcelWriterDto();
			excelWriterDto.setCreateDate(DateUtil.DateToString(sub.getSubDate(), "yyyy-MM-dd hh:mm:ss"));
			// excelWriterDto.setInvoiceInfo(sub.getInvoiceSubId());
			if (AppUtils.isBlank(sub.getInvoiceSubId()) || sub.getInvoiceSubId() == 0l) {
				excelWriterDto.setInvoiceType("不需要");
			} else {
				excelWriterDto.setInvoiceType("需要");
			}
			excelWriterDto.setSubNum(sub.getSubNumber());
			excelWriterDto.setFreeAmount(sub.getFreightAmount());
			excelWriterDto.setActualTotal(sub.getActualTotal());
			excelWriterDto.setReceiverAddr(sub.getProvince() + " " + sub.getCity() + " " + sub.getArea() + " " + sub.getSubAdds());
			excelWriterDto.setReceiverMobile(sub.getMobile());
			excelWriterDto.setReceiverName(sub.getReceiver());
			excelWriterDto.setReceiverTel(sub.getTelphone());
			excelWriterDto.setReceiverZipcode(sub.getSubPost());
			excelWriterDto.setRemark(sub.getOrderRemark());
			excelWriterDto.setUserName(sub.getUserName());
			excelWriterVos.add(excelWriterDto);
		}
		return excelWriterVos;
	}

	/**
	 *
	 * 确认收货
	 */
	@Override
	public boolean orderReceive(String subNumber, String userId) {
		Sub sub = subDao.getSubBySubNumberByUserId(subNumber, userId);
		if (AppUtils.isBlank(sub)) {
			log.warn("It can not found sub order by subMember {}, userId {}", subNumber, userId);
			return false;
		}
		if (!OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus())) {// 用户需要先发货才能收货
			log.warn("It can receive order by subMember {}, userId {}", subNumber, userId);
			return false;
		}
		Date date = new Date();
		sub.setStatus(OrderStatusEnum.SUCCESS.value());
		sub.setUpdateDate(date);
		sub.setFinallyDate(date);
		int result = subDao.update(sub);
		if (result > 0) {
			SubHistory subHistory = new SubHistory();
			String time = subHistory.DateToString(date);
			subHistory.setRecDate(date);
			subHistory.setSubId(sub.getSubId());
			subHistory.setStatus(SubHistoryEnum.TAKE_DELIVER.value());
			StringBuilder msg = new StringBuilder();
			msg.append(sub.getUserName()).append("于").append(time).append("确认收货");
			subHistory.setUserName(sub.getUserName());
			subHistory.setReason(msg.toString());
			subHistoryService.saveSubHistory(subHistory);
			orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
		}

		return true;
	}

	@Override
	public List<SubCountsDto> querySubCounts(String userId) {
		return subDao.querySubCounts(userId);
	}

	/**
	 * 永久删除订单
	 */
	@Override
	public boolean dropOrder(Sub sub) {

		List<SubItem> items = subItemDao.getSubItem(sub.getSubNumber(), sub.getUserId());

		//订单项
		if (AppUtils.isNotBlank(items)) {
			for (int i = 0; i < items.size(); i++) {
				subItemDao.deleteSubItem(items.get(i));
			}
		}

		// 订单地址
		if (AppUtils.isNotBlank(sub.getAddrOrderId())) {
			subDao.update("delete from ls_usr_addr_sub where addr_order_id=? and user_id=? ", new Object[] { sub.getAddrOrderId(), sub.getUserId() });
		}
		// 发票
		if (AppUtils.isNotBlank(sub.getInvoiceSubId())) {
			subDao.update("delete from ls_invoice_sub where id=? and user_id=? ", new Object[] { sub.getInvoiceSubId(), sub.getUserId() });
		}

		//删除支付记录
		List<String> settlementList = subDao.query("SELECT sub_settlement_sn FROM ls_sub_settlement_item WHERE sub_number = ? and user_id = ?",
				String.class, sub.getSubNumber(),  sub.getUserId() );
		if(AppUtils.isNotBlank(settlementList)) {
			List<Object[]> batchArgs = new ArrayList<>();
			for (String subSettlementSn : settlementList) {
				Object[] param = new Object[2];
				param[0] = subSettlementSn;
				param[1] = sub.getUserId();
				batchArgs.add(param);
			}
			subDao.batchUpdate("delete from ls_sub_settlement where sub_settlement_sn=? and user_id=? ", batchArgs);//删除支付记录

			//删除支付单据
			subDao.update("delete from ls_sub_settlement_item where sub_number=? and user_id=? ", sub.getSubNumber(), sub.getUserId());//删除支付记录
		}

		//订单历史
		subDao.update("delete from ls_sub_hist where sub_id=? ", new Object[] { sub.getSubId() });

		// 如果是预售订单,删除预售订单表ls_presell_sub
		if (SubTypeEnum.PRESELL.value().equals(sub.getSubType())) {
			subDao.update("delete from ls_presell_sub where sub_id = ?", sub.getSubId());
		}

		//删除订单本身
		subDao.deleteSub(sub);

		return true;
	}

	@Override
	public Sub getSubByShopId(Long subId, Long shopId) {
		return subDao.get("select  * from ls_sub where sub_id=? and shop_id=? ", Sub.class, subId, shopId);
	}

	@Override
	@Cacheable(value = "SubCounts")
	public List<KeyValueEntity> getSubCounts(Long shopId) {
		return subDao.getSubCounts(shopId);
	}

	@Override
	public List<Sub> getFinishUnPay(int commitInteval, Date date) {
		return subDao.getFinishUnPay(commitInteval, date);
	}

	@Override
	public List<Sub> getUnAcklodgeSub(int commitInteval, Date date) {
		return subDao.getUnAcklodgeSub(commitInteval, date);
	}

	@Override
	@CachePut(value = "UserShopCartList", key = "'UserShopCart_'+#type+'_'+#userId")
	public UserShopCartList putUserShopCartList(String type, String userId, UserShopCartList userShopCartList) {
		return userShopCartList;
	}

	@Override
	@Cacheable(value = "UserShopCartList", key = "'UserShopCart_'+#type+'_'+#userId")
	public UserShopCartList findUserShopCartList(String type, String userId) {
		return null;
	}

	@Override
	@CacheEvict(value = "UserShopCartList", key = "'UserShopCart_'+#type+'_'+#userId")
	public void evictUserShopCartCache(String type, String userId) {

	}

	/**
	 * 获取用户，对应的店铺可用的优惠券
	 */
	@Override
	public List<Coupon> queryCanUsedCoupons(String userId, Double orderTotalAmount, Long shopId) {
		return couponDao.queryCanUsedCoupons(userId, orderTotalAmount, shopId);
	}

	@Override
	public Long saveSub(Sub sub) {
		return subDao.save(sub);
	}

	@Deprecated
	public boolean updateSubPrice(Sub sub, String userName, Double totalPrice) {
		return subDao.updateSubPrice(sub, userName, totalPrice);
	}

	/**
	 * 查找用户曾经下过的团购单
	 */
	@Override
	public Integer findUserOrderGroup(String userId, Long productId, Long skuId, Long groupId) {
		return subDao.findUserOrderGroup(userId, productId, skuId, groupId);
	}

	@Override
	public PresellSubDto findPresellOrderDetail(String subNumber, String userId) {
		PresellSubDto presellSubDto = subDao.findPresellOrderDetail(subNumber, userId);
		if (presellSubDto == null) {
			throw new NotFoundException("sub not found ");
		}
		return presellSubDto;
	}

	@Override
	public PresellSubDto findPresellOrderDetail(String subNumber) {
		PresellSubDto presellSubDto = subDao.findPresellOrderDetail(subNumber);
		if (presellSubDto == null) {
			throw new NotFoundException("sub not found ");
		}
		return presellSubDto;
	}

	@Override
	public PresellSubEntity getPreSubBySubNoAndUserId(String subNumber, String userId) {
		return subDao.getPreSubBySubNoAndUserId(subNumber, userId);
	}

	@Override
	public PresellSubEntity getPreSubBySubNoAndShopId(String subNumber, Long shopId) {
		return subDao.getPreSubBySubNoAndShopId(subNumber, shopId);
	}

	@Override
	public boolean updateSubPrice(PresellSubEntity presellSub, String userName) {
		Sub sub = subDao.getSubBySubNumber(presellSub.getSubNumber());
		PresellSubEntity oldPresellsub = subDao.getPreSubBySubNo(presellSub.getSubNumber());
		// 支付定金前可以修改定金和尾款
		// 支付定金后也可以修改尾款 by 2020-4-25 xslong
		if (AppUtils.isNotBlank(sub) && AppUtils.isNotBlank(oldPresellsub)) {
			SubHistory subHistory = null;
			BigDecimal preDepositPrice = presellSub.getPreDepositPrice();
			BigDecimal finalPrice = presellSub.getFinalPrice();
			Double totalPrice = Arith.add(preDepositPrice, finalPrice);
			if (!preDepositPrice.equals(oldPresellsub.getPreDepositPrice())
					|| !finalPrice.equals(oldPresellsub.getFinalPrice())
					|| totalPrice.equals(oldPresellsub.getActualTotal())) {

				subHistory = new SubHistory();
				Date time = new Date();
				String timeStr = DateUtil.DateToString(time, "yyyy-MM-dd HH:mm:ss");
				subHistory.setRecDate(time);
				subHistory.setStatus(SubStatusEnum.PRICE_CHANGE.value());
				subHistory.setSubId(sub.getSubId());
				subHistory.setUserName(userName);
				String reason = userName + "于" + timeStr + "调整价格操作,原订单总金额:" + oldPresellsub.getActualTotal() + ",原订单订金金额:" + oldPresellsub.getPreDepositPrice()
						+ "原订单尾款金额:" + oldPresellsub.getFinalPrice() + "原订单运费金额:" + sub.getFreightAmount() + "--> 调整后订单总金额为:" + totalPrice + "调整后订单订金为:"
						+ presellSub.getPreDepositPrice() + "调整后订单尾款金额:" + presellSub.getFinalPrice() + ",调整后运费金额为:" + oldPresellsub.getFinalPrice();
				subHistory.setReason(reason);
			}
			sub.setActualTotal(totalPrice);
			sub.setTotal(Arith.add(preDepositPrice, finalPrice));
			// 需要同步更新订单项金额，防止其他地方计算订单金额错误
			// 因为是预售订单，所以订单项只有一个
			List<SubItem> subItemList = this.subItemDao.getSubItem(sub.getSubNumber());
			if (!CollectionUtils.isEmpty(subItemList)) {
				subItemList.forEach(e -> e.setActualAmount(totalPrice));
				this.subItemDao.updateSubItem(subItemList);
			}
			// 记录订单日志和系统操作日记
			subHistoryService.saveSubHistory(subHistory);
			updateSub(sub);
			// 更新预售订单
			subDao.update("update ls_presell_sub set pre_deposit_price = ?, final_price = ? where sub_id = ?", preDepositPrice, finalPrice, oldPresellsub.getSubId());
			// 清除Order缓存
			orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
			return true;
		}
		return false;
	}

	@Override
	public PresellSubEntity getPreSubBySubNo(String subNumber) {
		return subDao.getPreSubBySubNo(subNumber);
	}

	// 收到货款
	@Override
	public void receiveMoney(Sub oldSub, SubSettlement subSettlement) {

		SubSettlementItem settlementItem = new SubSettlementItem();
		settlementItem.setSubNumber(oldSub.getSubNumber());
		settlementItem.setSubSettlementSn(subSettlement.getSubSettlementSn());
		settlementItem.setUserId(subSettlement.getUserId());

		subSettlementItemDao.save(settlementItem);
		subSettlementDao.save(subSettlement);
		subDao.update(oldSub);

		List<SubItem> subItems = subItemDao.getSubItemBySubId(oldSub.getSubId());

		for (int i = 0, len = subItems.size(); i < len; i++) {
			SubItem subItem = subItems.get(i);

			// 更新商品订购数
			productDao.sumBuys(subItem.getBasketCount(), subItem.getProdId());
			/*
			 * 付款减库存 检查商品库存,并且更新商品的虚拟库存
			 */
			if (StockCountingEnum.STOCK_BY_PAY.value().equals(subItem.getStockCounting())) {
				boolean config = stockService.addHold(subItem.getProdId(), subItem.getSkuId(), subItem.getBasketCount());
				if (!config) {
					throw new NotStocksException(subItem.getProdName() + "库存不足,请补充库存或与用户协商解决!", ErrorCodes.BUSINESS_ERROR);
				}
			}
		}
	}

	@Override
	public Integer getTodaySubCount(Long shopId) {
		return subDao.getTodaySubCount(shopId);
	}

	/**
	 * //已完成的订单未评价的 订单个数
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public Integer getUnCommentOrderCount(String userId) {
		return subDao.get(
				"SELECT COUNT(1) FROM ( SELECT si.sub_item_id FROM ls_sub_item si, ls_sub s WHERE s.sub_number=si.sub_number AND s.status=4 AND s.delete_status=0 AND si.comm_sts=0 and s.user_id=? GROUP BY si.sub_number ) a",
				Integer.class, userId);
	}

	@Override
	public Double getIncomeToday(Long shopId) {
		return subDao.getIncomeToday(shopId);
	}

	@Override
	public int remindDelivery(Long subId, String userId) {
		return subDao.remindDelivery(subId, userId);
	}

	@Override
	public List<Sub> findCancelOrders() {
		return subDao.findCancelOrders();
	}

	/**
	 * 获取订单自动取消的时间
	 *
	 * @return
	 */
	@Override
	public int getOrderCancelExpireDate() {
		ConstTable constTable = constTableDao.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		int expire = 30;// 默认是30分钟
		if (AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())) {
			OrderSetMgDto orderSet = JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if (AppUtils.isNotBlank(orderSet) && AppUtils.isNotBlank(orderSet.getAuto_order_cancel())) {
				expire = Integer.valueOf(orderSet.getAuto_order_cancel());
			}
		}
		return expire;
	}


	@Override
	public int remindDeliveryBySN(String subNumber, String userId) {
		return subDao.remindDeliveryBySN(subNumber, userId);
	}

	@Override
	public PageSupport<Sub> getSubListPage(String curPageNO, ShopOrderBill shopOrderBill, String subNumber) {
		return subDao.getSubListPage(curPageNO, shopOrderBill, subNumber);
	}

	@Override
	public PageSupport getOrdersPage(OrderSearchParamDto paramDto, int pageSize) {
		return subDao.getOrdersPage(paramDto, pageSize);
	}

	@Override
	public PageSupport<Sub> getSubListPage(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {
		return subDao.getSubListPage(curPageNO, shopId, subNumber, shopOrderBill);
	}

	@Override
	public PageSupport<Sub> getOrderList(String curPageNO, String userName) {
		return subDao.getOrder(curPageNO, userName);
	}

	@Override
	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO, Long shopId) {
		return subDao.getInvoiceOrder(curPageNO, shopId);
	}

	@Override
	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO, Long shopId, InvoiceSubDto invoiceSubDto) {
		return subDao.getInvoiceOrder(curPageNO, shopId, invoiceSubDto);
	}

	@Override
	public List<Sub> getSubBySubNums(String[] subNums, String userId, Integer orderStatus) {
		return subDao.getSubBySubNums(subNums, userId, orderStatus);
	}

	@Override
	public List<Sub> findHaveInGroupSub(Long groupId, Integer groupStatus) {
		return subDao.findHaveInGroupSub(groupId, groupStatus);
	}

	@Override
	public MergeGroupSubDto getMergeGroupSubDtoBySettleSn(String subSettlementSn) {
		MergeGroupSubDto mergeGroupSubDto = subDao.getMergeGroupSubDtoBySettleSn(subSettlementSn);
		if (AppUtils.isNotBlank(mergeGroupSubDto) && AppUtils.isBlank(mergeGroupSubDto.getPic())) {// 即商品为默认sku
			// 没有设置图片,取商品主图
			mergeGroupSubDto.setPic(productDao.getProdPicByMergeId(mergeGroupSubDto.getMergeId()));
		}
		return mergeGroupSubDto;
	}

	@Override
	public List<String> queryUserPicByMergeGroupSub(Long mergeId, Long operateId) {
		return mergeGroupAddDao.queryUserPicByMergeGroupSub(mergeId, operateId);
	}

	@Override
	public List<SubDto> exportSubOrder(ShopOrderBill shopOrderBill, String subNumber) {
		StringBuilder sb = new StringBuilder();
		List<Object> obj = new ArrayList<Object>();
		sb.append("SELECT s.sub_number AS subNumber,s.pay_date AS payDate,s.finally_date AS finallyDate,s.actual_total AS actualTotal,");
		sb.append("s.freight_amount AS freightAmount,s.redpack_off_price AS redpackOffPrice ");
		sb.append("FROM ls_sub s ");
		sb.append("WHERE 1=1 ");
		sb.append(" AND s.shop_id=?");
		obj.add(shopOrderBill.getShopId());
		sb.append(" AND s.status=?");
		obj.add(OrderStatusEnum.SUCCESS.value());
		sb.append(" AND s.bill_sn=?");
		obj.add(shopOrderBill.getSn());
		if (AppUtils.isNotBlank(subNumber)) {
			sb.append(" AND s.sub_number=?");
			obj.add(subNumber);
		}
		List<SubDto> list = this.subDao.exportSubOrder(sb.toString(), obj.toArray());
		return list;
	}

	@Override
	public MySubDto getMySubDtoBySubNumber(String subNumber) {
		return subDao.getMySubDtoBySubNumber(subNumber);
	}

	/**
	 * 结束超时不付费的订单.
	 *
	 * @param list the list
	 */
	@Override
	public void finishUnPay(List<Sub> list) {

		TimerLog.info("<! ---- finishUnPay Order starting ");
		//当前时间
		Date now = new Date();
		StringBuilder idStr=new StringBuilder();
		for (Iterator<Sub> iterator = list.iterator(); iterator.hasNext();) {
			Sub sub = (Sub) iterator.next();
			if(AppUtils.isNotBlank(sub) ){

				String timeStr = DateUtils.format(now, DateUtils.PATTERN_CLASSICAL);
				String historyReason = "于" + timeStr + "系统自动取消订单";
				String subReason = "系统自动取消订单";

				sub.setStatus(OrderStatusEnum.CLOSE.value());
				sub.setCancelReason(subReason);
				boolean result=this.cancleOrder(sub);
				if(result){ //如果该订单取消成功
					/*
					 * 记录订单历史
					 */
					SubHistory subHistory=new SubHistory();
					subHistory.setRecDate(now);
					subHistory.setSubId(sub.getSubId());
					subHistory.setStatus(SubHistoryEnum.ORDER_OVER_TIME.value());
					subHistory.setUserName("系统自动");
					subHistory.setReason(historyReason);
					idStr.append(sub.getSubId()).append(",");
					subHistoryService.saveSubHistory(subHistory);
					//清除缓存
					orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
				}

			}
		}
		TimerLog.info("finishUnPay sub id by {} ",idStr.toString());
		TimerLog.info("finishUnPay Order success --->");

	}

	/*
	 * 查询退款的总红包金额
	 */
	@Override
	public Double getReturnRedpackOffPrice(Set<String> subNumbers) {
		return subDao.getReturnRedpackOffPrice(subNumbers);
	}

	@Override
	public List<Sub> getUnpaymentseckillOrder(Long seckillId) {
		return subDao.getUnpaymentseckillOrder(seckillId);
	}

	@Override
	public PageSupport<IncomeTodayDto> getOrders(Long shopId, String curPageNO, int pageSize) {
		return subDao.getOrders(shopId, curPageNO, pageSize);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogs(Long shopId, String curPageNO, int pageSize) {
		return subDao.getVisitLogs(shopId, curPageNO, pageSize);
	}

	/**
	 * 获取订单自动确认时间
	 */
	@Override
	public Integer getOrderConfirmExpireDate() {
		ConstTable constTable = constTableDao.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		int expire = 7;// 默认是7天
		if (AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())) {
			OrderSetMgDto orderSet = JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if (AppUtils.isNotBlank(orderSet) && AppUtils.isNotBlank(orderSet.getAuto_order_confirm())) {
				expire = Integer.valueOf(orderSet.getAuto_order_confirm());
			}
		}
		return expire;
	}

	@Override
	public MySubDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId) {
		if (AppUtils.isBlank(subSettlementSn)) {
			log.warn("Can not found sub by null string");
			return null;
		}
		MySubDto sub = subDao.findOrderDetailBySubSettlementSn(subSettlementSn, userId);
		if (AppUtils.isBlank(sub)) {
			log.warn("Can not found sub by {}", subSettlementSn);
		}
		return sub;
	}

	@Override
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn, String userId) {

		return subDao.getSubBySubSettlementSn(subSettlementSn, userId);
	}

	@Override
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn) {
		return subDao.getSubBySubSettlementSn(subSettlementSn);
	}

	@Override
	public Sub getLastOneSubmitAndUnPaySeckillSub(Long activeId) {

		return subDao.getLastOneSubmitAndUnPaySeckillSub(activeId);
	}

	/**
	 * 获取团购运营数据
	 */
	@Override
	public PageSupport<Sub> getGroupOperationList(String curPageNO, Integer pageSize, Long groupId, Long shopId) {

		return subDao.getGroupOperationList(curPageNO,pageSize,groupId,shopId);
	}

	@Override
	public PageSupport<Sub> getSeckillOperationList(String curPageNO, int pageSize, Long seckillId, Long shopId) {

		return subDao.getSeckillOperationList(curPageNO,pageSize,seckillId,shopId);
	}

	@Override
	public PageSupport<Sub> getPresellOperationList(String curPageNO, int size, Long id, Long shopId) {
		return subDao.getPresellOperationList(curPageNO,size,id,shopId);
	}


	private boolean isInReturnValidPeriod(Long prodId,MySubDto order){
		ProductDetail prod = productService.getProdDetail(prodId);
		Integer days = 0;
		if (AppUtils.isNotBlank(prod)){
			Long categoryId = prod.getCategoryId();
			Category category = categoryService.getCategory(categoryId);
			days = category.getReturnValidPeriod();
		}
		//当分类退换货时间设置为空，获取系统默认设置
		if(days == null || days == 0){
			ConstTable constTable =constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
			OrderSetMgDto orderSetting=null;
			if(AppUtils.isNotBlank(constTable)){
				String setting=constTable.getValue();
				orderSetting= JSONUtil.getObject(setting, OrderSetMgDto.class);
			}
			days = Integer.parseInt(orderSetting.getAuto_product_return());
		}
		Date finallyDate = order.getFinallyDate();
		if(null == finallyDate){//说明订单还未完成，默认有效
			return true;
		}
		return isInDates(finallyDate, days);
	}

	/**
	 * 代替 com.legendshop.util.DateUtils#isInDates(java.util.Date, int) 的方法，判断当前时间是否在范围之内
	 * @param date		时间
	 * @param days		偏移天数
	 * @return
	 */
	private static boolean isInDates(Date date, long days) {
		long ms = days * 24 * 3600 * 1000;
		long times = date.getTime() + ms;
		long now = (new Date()).getTime();
		return times > now;
	}
}
