/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinMenuDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.weixin.WeixinMenu;
import com.legendshop.util.AppUtils;

/**
 * 微信菜单Dao
 */
@Repository
public class WeixinMenuDaoImpl extends GenericDaoImpl<WeixinMenu, Long> implements WeixinMenuDao  {

	public WeixinMenu getWeixinMenu(Long id){
		return getById(id);
	}
	
    public int deleteWeixinMenu(WeixinMenu weixinMenu){
    	return delete(weixinMenu);
    }
	
	public Long saveWeixinMenu(WeixinMenu weixinMenu){
		return save(weixinMenu);
	}
	
	public int updateWeixinMenu(WeixinMenu weixinMenu){
		return update(weixinMenu);
	}

	@Override
	public boolean hasSubWeixinMenu(Long menuId) {
		return  getLongResult("select count(*) from ls_weixin_menu where parent_id = ?", menuId) > 0;
	}

	@Override
	public boolean isExitMenuKey(String menukey) {
		return  getLongResult("select count(*) from ls_weixin_menu where menu_key = ?", menukey) > 0;
	}

	@Override
	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, WeixinMenu weixinMenu, DataSortResult result) {
		 CriteriaQuery cq = new CriteriaQuery(WeixinMenu.class, curPageNO);
	        cq.setPageSize(30);
	       // cq.eq("grade", 1);//只是查询一级菜单
	        if(AppUtils.isNotBlank(weixinMenu.getName())){
	        	cq.like("name", "%"+weixinMenu.getName()+"%");
	        }
	        Long parentId=weixinMenu.getParentId();
	        if(AppUtils.isBlank(parentId)){
	        	 cq.isNull("parentId");
	        }else{
	        	 cq.eq("parentId", parentId);
	        }
	       
			if (!result.isSortExternal()) {// 非外部排序
				cq.addOrder("asc", "seq");
			}else {
				cq.addOrder(result.getOrderIndicator(), result.getSortName());
			}
			
		return queryPage(cq);
	}

	@Override
	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, Long parentId, int pageSize, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(WeixinMenu.class, curPageNO);
        cq.setPageSize(30);
        cq.eq("parentId", parentId);
		if (!result.isSortExternal()) {// 非外部排序
			cq.addOrder("asc", "seq");
		}
		return queryPage(cq);
	}
	
 }
