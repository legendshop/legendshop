/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DeliveryTypeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.util.AppUtils;

/**
 * 物流配送方式服务实现类
 */
@Service("deliveryTypeService")
public class DeliveryTypeServiceImpl implements DeliveryTypeService {
	
	@Autowired
	private DeliveryTypeDao deliveryTypeDao;

	/**
	 * 根据商城Id获取物流配送方式
	 */
	public List<DeliveryType>  getDeliveryTypeByShopId(Long shopId) {
		return deliveryTypeDao.getDeliveryTypeByShopId(shopId);
	}
	
	@Override
	public List<DeliveryType> getDeliveryTypeByShopId(){
		return deliveryTypeDao.getDeliveryTypeByShopId();
	}

	public DeliveryType getDeliveryType(Long id) {
		return deliveryTypeDao.getDeliveryType(id);
	}

	public void deleteDeliveryType(DeliveryType deliveryType) {
		deliveryTypeDao.deleteDeliveryType(deliveryType);
	}

	public Long saveDeliveryType(DeliveryType deliveryType) {
		if (!AppUtils.isBlank(deliveryType.getDvyTypeId())) {
			updateDeliveryType(deliveryType);
			return deliveryType.getDvyTypeId();
		}
		return (Long) deliveryTypeDao.save(deliveryType);
	}

	public void updateDeliveryType(DeliveryType deliveryType) {
		deliveryTypeDao.updateDeliveryType(deliveryType);
	}

	@Override
	public DeliveryType getDeliveryType(Long shopId, Long id) {
		return deliveryTypeDao.getDeliveryType( shopId,  id) ;
	}

	@Override
	public long checkMaxNumber(Long shopId) {
		return deliveryTypeDao.checkMaxNumber(shopId);
	}

	@Override
	public PageSupport<DeliveryType> getDeliveryTypePage(String curPageNO, DeliveryType deliveryType) {
		return deliveryTypeDao.getDeliveryTypePage(curPageNO,deliveryType);
	}

	@Override
	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId) {
		return deliveryTypeDao.getDeliveryTypeCq(shopId);
	}
	
	@Override
	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId, String curPageNO) {
		return deliveryTypeDao.getDeliveryTypeCq(shopId, curPageNO);
	}

	@Override
	public PageSupport<DeliveryTypeDto> getAppDeliveryTypePage(String curPageNO, Long shopId) {
		return deliveryTypeDao.getAppDeliveryTypePage(curPageNO,shopId);
	}

	@Override
	public Integer getDeliveryTypeByprintTempId(Long printTempId) {
		return deliveryTypeDao.getDeliveryTypeByprintTempId(printTempId);
	}

	
}
