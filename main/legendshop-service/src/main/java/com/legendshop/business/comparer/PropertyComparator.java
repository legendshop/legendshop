package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.util.AppUtils;

public class PropertyComparator implements DataComparer<ProductProperty,ProductProperty>{

	@Override
	public boolean needUpdate(ProductProperty newProperty, ProductProperty originProperty,Object obj) {
		
		return true;
	}

	@Override
	public boolean isExist(ProductProperty newProperty, ProductProperty originProperty) {
		if(AppUtils.isNotBlank(newProperty) && AppUtils.isNotBlank(originProperty)){
			if(originProperty.getPropName().equals(newProperty.getPropName()) && originProperty.getPropId().equals(newProperty.getPropId())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public ProductProperty copyProperties(ProductProperty newProperty, Object obj) {
		Product prod = (Product)obj;
		
		newProperty.setProdId(prod.getProdId());
		newProperty.setUserName(prod.getUserName());
		newProperty.setIsCustom(1);
	
		return newProperty;
	}

}
