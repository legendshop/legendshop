/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserFeedBackDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserFeedBack;
import com.legendshop.util.AppUtils;

/**
 * 用户反馈Dao.
 */
@Repository
public class UserFeedBackDaoImpl extends GenericDaoImpl<UserFeedBack, Long> implements UserFeedBackDao  {
     
	public UserFeedBack getUserFeedBack(Long id){
		return getById(id);
	}
	
    public int deleteUserFeedBack(UserFeedBack userFeedBack){
    	return delete(userFeedBack);
    }
	
	public Long saveUserFeedBack(UserFeedBack userFeedBack){
		return save(userFeedBack);
	}
	
	public int updateUserFeedBack(UserFeedBack userFeedBack){
		return update(userFeedBack);
	}

	@Override
	public PageSupport<UserFeedBack> getUserFeedBackPage(String curPageNO, UserFeedBack userFeedback) {
		CriteriaQuery cq = new CriteriaQuery(UserFeedBack.class, curPageNO);
		cq.setPageSize(10);
		if(AppUtils.isNotBlank(userFeedback.getContent())){
			cq.like("content", "%"+userFeedback.getContent().trim()+"%");
		}
		if(AppUtils.isNotBlank(userFeedback.getStatus())){
			cq.like("status",userFeedback.getStatus());
		}
		cq.addDescOrder("recDate");
		return queryPage(cq);
	}
	
 }
