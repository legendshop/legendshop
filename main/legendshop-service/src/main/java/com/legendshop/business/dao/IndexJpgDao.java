/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Indexjpg;

/**
 * 商城首页图片Dao.
 */
public interface IndexJpgDao extends GenericDao<Indexjpg, Long>{

	/**
	 * Gets the index jpeg by shop id.
	 *
	 * @return the index jpeg by shop id
	 */
	public List<Indexjpg> getIndexJpegByShopId();
	
	/**
	 * Query index jpg.
	 * 
	 * @param cq
	 *            the cq
	 * @return the page support
	 */
	public abstract PageSupport<Indexjpg> queryIndexJpg(CriteriaQuery cq);

	/**
	 * Query index jpg.
	 * 
	 * @param id
	 *            the id
	 * @return the indexjpg
	 */
	public abstract Indexjpg queryIndexJpg(Long id);

	/**
	 * Delete index jpg.
	 * 
	 * @param indexjpg
	 *            the indexjpg
	 */
	public abstract void deleteIndexJpg(Indexjpg indexjpg);

	/**
	 * Update indexjpg.
	 * 
	 * @param origin
	 *            the origin
	 */
	public abstract void updateIndexjpg(Indexjpg origin);

	/**
	 * Save indexjpg.
	 *
	 * @param indexjpg the indexjpg
	 * @return the long
	 */
	public abstract Long saveIndexjpg(Indexjpg indexjpg);
	
	/**
	 * Query mobile index jpeg.
	 *
	 * @param shopId the shop id
	 * @return the list< indexjpg>
	 */
	public List<Indexjpg> queryMobileIndexJpeg(Long shopId);

	/**
	 * Gets the index jpg page.
	 *
	 * @param curPageNO the cur page no
	 * @param indexjpg the indexjpg
	 * @param location the location
	 * @param pageSize the page size
	 * @param result the result
	 * @return the index jpg page
	 */
	public PageSupport<Indexjpg> getIndexJpgPage(String curPageNO, Indexjpg indexjpg, int location, Integer pageSize,
			DataSortResult result);

	/**
	 * Gets the index jpg page by data.
	 *
	 * @param curPageNO the cur page no
	 * @param indexjpg the indexjpg
	 * @param location the location
	 * @param pageSize the page size
	 * @param result the result
	 * @return the index jpg page by data
	 */
	public PageSupport<Indexjpg> getIndexJpgPageByData(String curPageNO, Indexjpg indexjpg, int location,
			Integer pageSize, DataSortResult result);

}