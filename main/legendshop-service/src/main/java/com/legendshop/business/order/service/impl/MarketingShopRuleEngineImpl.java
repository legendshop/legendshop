package com.legendshop.business.order.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.spi.service.MarketingShopRuleEngine;
import com.legendshop.spi.service.MarketingShopRuleExecutor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;


/**
 * 营销计算规则引擎实现,在xml中定义
 *
 */
public class MarketingShopRuleEngineImpl implements MarketingShopRuleEngine{

	private MarketingService marketingService;
	
	private Map<Integer, MarketingShopRuleExecutor> executors;
	
//	@Bean("marketingShopRuleEngineExecutors")
//	public Map<Integer, MarketingShopRuleExecutor> executors(
//			@Qualifier(value="manJianMarketingRuleExecutor") ManJianMarketingRuleExecutor  manJianMarketingRuleExecutor,
//			@Qualifier(value="manZheMarketingRuleExecutor") ManZheMarketingRuleExecutor  manZheMarketingRuleExecutor,
//			@Qualifier(value="xianshiMarketingRuleExecutor") XianshiMarketingRuleExecutor  xianshiMarketingRuleExecutor){
//		Map<Integer, MarketingShopRuleExecutor>  map = new HashMap<Integer, MarketingShopRuleExecutor>();
//		map.put(0, manJianMarketingRuleExecutor);
//		map.put(1, manZheMarketingRuleExecutor);
//		map.put(2, xianshiMarketingRuleExecutor);
//		return map;
//	}
	
	/**
	 * 按每个商家的订单计算规则
	 */
	@Override
	public void executorMarketing(ShopCarts shopCarts) {
		/*查询该商家所有可用的营销活动*/
	    List<MarketingDto> marketingDtoList = marketingService.findAvailableBusinessRuleList(shopCarts.getShopId(), shopCarts.getCartItems());
	    
	    List<ShopCartItem> cartItems = shopCarts.getCartItems();
	    List<ShopCartItem> allHitCartItems = new ArrayList<ShopCartItem>();
	    
	    double shopDiscountPrice = 0d;//优惠的总金额
	    double discountedPrice = 0d;//商品项优惠后的金额
	    
	    if(AppUtils.isNotBlank(marketingDtoList)){
			MarketRuleContext context = new MarketRuleContext(); //应用规则上下文,用于做排除
			for (MarketingDto marketingDto : marketingDtoList) {
				if(executors!=null && executors.containsKey(marketingDto.getType())){
					MarketingShopRuleExecutor executor =executors.get(marketingDto.getType());
					//把业务规则应用到店铺订单
					executor.execute(marketingDto,shopCarts, context);
				}
			}
			
			//找到命中的商品集合
		    for (MarketingDto marketingDto : marketingDtoList) {
		    	
		    	List<ShopCartItem> hitCartItems = marketingDto.getHitCartItems();
		    	if(AppUtils.isNotBlank(hitCartItems)){
		    		allHitCartItems.addAll(hitCartItems);
		    		
		    		//新增逻辑 计算订单项减去优惠的金额
		    		if(marketingDto.getTotalOffPrice()<=0d||marketingDto.getHitProdTotalPrice()<=0d){
		    			continue;
		    		}else {
		    			
						List<ShopCartItem> shopCartItemList=marketingDto.getHitCartItems();
						double usedMarketAmout=0.0;
						int shopCartItemSize=shopCartItemList.size();
						double reductionRate=Arith.div(marketingDto.getTotalOffPrice(), marketingDto.getHitProdTotalPrice());//每一块钱所占的比例
						for(int i=0;i<shopCartItemSize;i++){
							
							
							
							ShopCartItem cartItem=shopCartItemList.get(i);
							double marketOffPrice=0.0;
							if(i==shopCartItemSize-1){
								marketOffPrice=Arith.sub(marketingDto.getTotalOffPrice(), usedMarketAmout);
							}else{
								marketOffPrice=Arith.mul(reductionRate,cartItem.getCalculatedCouponAmount());//此购物车项限时优惠的金额
								usedMarketAmout=Arith.add(usedMarketAmout, marketOffPrice);
							}
							discountedPrice=Arith.sub(cartItem.getDiscountedPrice(),marketOffPrice);
							
							
							//TODO linzh 如果商品促销优惠完的金额出现负数，那商品的优惠后金额就是0
							//zhuang 取每个购物车项的优惠金额来统计marketOffPrice
							if (discountedPrice <= 0) {
								discountedPrice = 0;
								shopDiscountPrice = Arith.add(shopDiscountPrice,cartItem.getDiscountedPrice());//优惠金额就是商品的价格
							}else {
								shopDiscountPrice = Arith.add(shopDiscountPrice,marketOffPrice);
							}
							
							//减去一个优惠活动之后的价格
							cartItem.setDiscountedPrice(discountedPrice);
						}	
		    		}
		    	}
		    	
		    	
		    }
		    
	    }
	    
	    List<MarketingDto> marketingDtoList2 = new ArrayList<MarketingDto>();
	    //没有参与活动的商品,虚拟一个空的活动，把商品放进去
	    MarketingDto dto = new MarketingDto();
	    List<ShopCartItem> unHitCartItems = new ArrayList<ShopCartItem>(cartItems);
	    unHitCartItems.removeAll(allHitCartItems);
	    
	    dto.setHitCartItems(unHitCartItems);
	    marketingDtoList2.add(dto);
	    
	    marketingDtoList2.addAll(marketingDtoList); //已经参见活动的添加进 marketing 中;
	    
	    shopCarts.setDiscountPrice(shopDiscountPrice);
	    shopCarts.setMarketingDtoList(marketingDtoList2);
	    
	    //更改优惠逻辑 当优惠金额超过订单该支付的总金额 到0为止。不出现负数
	    Double shopActualTotalCash = Arith.sub(shopCarts.getShopTotalCash(), shopDiscountPrice);
	    shopActualTotalCash = shopActualTotalCash < 0 ? 0 : shopActualTotalCash;
	    shopCarts.setShopActualTotalCash(shopActualTotalCash);
	}

	public void setMarketingService(MarketingService marketingService) {
		this.marketingService = marketingService;
	}

	public void setExecutors(Map<Integer, MarketingShopRuleExecutor> executors) {
		this.executors = executors;
	}
	

}
