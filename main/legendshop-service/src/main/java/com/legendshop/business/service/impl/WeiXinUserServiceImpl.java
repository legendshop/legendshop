package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeiXinUserDao;
import com.legendshop.model.constant.ThirdPartyLoginType;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.WeiXinUserService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

@Service("weiXinUserService")
public class WeiXinUserServiceImpl implements WeiXinUserService {
	
	private final static Logger LOGGER=LoggerFactory.getLogger(WeiXinUserServiceImpl.class);
	
	@Autowired
	private WeiXinUserDao weiXinUserDao;
	
	@Autowired
	private PassportService passportService;
	
	@Override
	public WeixinGzuserInfo getGzuserInfo(String openId) {
		return weiXinUserDao.getGzuserInfo(openId);
	}
	
	@Override
	public String getUserIdByOpenId(String openId){
		Passport passport= passportService.getUserIdByOpenId(ThirdPartyLoginType.WEIXIN, openId);
		if(AppUtils.isNotBlank(passport)){
			return passport.getUserId();
		}
		return null;
	}
	
	@Override
	 public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
	       return weiXinUserDao.saveWeixinGzuserInfo(weixinGzuserInfo);
	 }
	 


	@Override
	public Passport autoRegBind(String ip, WeixinGzuserInfo gzuserInfo) {
		
		//存在商城用户
		Passport passport=passportService.getUserIdByOpenIdForUpdate(ThirdPartyLoginType.WEIXIN, gzuserInfo.getOpenid());
		if(AppUtils.isNotBlank(passport)){
			LOGGER.info("existing user by userId = {} , openid={} ",passport.getUserId(),gzuserInfo.getOpenid());
			return passport;
		}
		LOGGER.info("not Existing user by openid={}",gzuserInfo.getOpenid());
	    passport=new Passport();
		passport.setNickName(gzuserInfo.getNickname());
		passport.setOpenId(gzuserInfo.getOpenid());
		passport.setType(ThirdPartyLoginType.WEIXIN);
		String props=JSONUtil.getJson(gzuserInfo);
		if(props.length()>1000){
			passport.setProps(props.substring(0, 1000));
		}else{
			passport.setProps(props);
		}
		
		/*UserForm userForm =new UserForm();
		String userName="WX_"+userDetailService.generateUserName();
		String password=RandomStringUtils.randomAlphanumeric(6);
		userForm.setNickName(gzuserInfo.getNickname());
		userForm.setName(userName);
		userForm.setPassword(password);
		if("男性".equals(gzuserInfo.getSex())){
			userForm.setSex("M");	
		}else if("女性".equals(gzuserInfo.getSex())){
			userForm.setSex("F");
		}
		userForm.setNote(gzuserInfo.getBzname());
		
		//注册并保持通行证
		UserResultDto user = passportService.savePassportAndReg(ip, userForm, passport);
		if(user == null || user.getUser() == null){
			throw new BusinessException("auto register user error");
		}
		//绑定weixin_gzuserinfo
		passport.setUserId(user.getUser().getId());*/
		return passport;
	}
}
