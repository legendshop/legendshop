/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdPropImageDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.ProdPropImage;

/**
 * 属性图片Dao.
 */
@Repository
public class ProdPropImageDaoImpl extends GenericDaoImpl<ProdPropImage, Long> implements ProdPropImageDao  {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProdPropImageDaoImpl.class);
     
	public ProdPropImage getProdPropImage(Long id){
		return getById(id);
	}
	
    public int deleteProdPropImage(ProdPropImage prodPropImage){
    	return delete(prodPropImage);
    }
	
	public Long saveProdPropImage(ProdPropImage prodPropImage){
		return save(prodPropImage);
	}
	
	public int updateProdPropImage(ProdPropImage prodPropImage){
		return update(prodPropImage);
	}

	@Override
	public List<ProdPropImage> getProdPropImageByProdId(Long prodId){
		return this.queryByProperties(new EntityCriterion().eq("prodId", prodId).addAscOrder("seq"));
	}
	
	@CacheEvict(value = "ProdPropImageList", key = "#prodId")
	public void clearProdPropImageCache(Long prodId){
		log.debug("Clear ProdPropImage cache by prodId {}", prodId);
	}

	@Override
	public List<ProdPropImage> queryPropImageList(Long prodId, ProductPropertyDto productPropertyDto,
			List<ProductPropertyValueDto> valueDtoList) {
		
		 List<Long> ids = new ArrayList<Long>();
		 ids.add(prodId);
		 ids.add(productPropertyDto.getPropId());
		 StringBuffer sb =new StringBuffer("SELECT value_name,id,prop_id,prod_id,seq,value_id,create_date,url FROM ls_prop_image WHERE prod_id=? AND prop_id=? AND value_id in (");
		 for (ProductPropertyValueDto ppvd : valueDtoList) {
			 ids.add(ppvd.getValueId());
			 sb.append("?,");
		 }
		 sb.setLength(sb.length()-1);
		 sb.append(")");
		 
		 List<ProdPropImage> propImageList = query(sb.toString(), ProdPropImage.class,ids.toArray());
		
		 return propImageList;
	}

	/**
	 * 获取商品下的某个属性值的图片集合
	 */
	@Override
	public List<ProdPropImage> getProdPropImageByKeyVal(Long prodId, Long key,Long val) {
		return this.queryByProperties(new EntityCriterion().eq("prodId", prodId).eq("propId",key).eq("valueId", val).addAscOrder("seq"));
	}

	/**
	 * 获取属性值的所有图片
	 */
	@Override
	public List<String> getImageUrlList(Long prodId, Long propId, Long valueId) {
		List<ProdPropImage> prodPropImages = this.getProdPropImageByKeyVal(prodId,propId,valueId);
		List<String> urls = new ArrayList<String>();
		for(ProdPropImage prodPropImage : prodPropImages){
			urls.add(prodPropImage.getUrl());
		}
		return urls;
	}

	@Override
	public void deleteByProdId(Long id) {
		this.update("delete from ls_prop_image where prod_id = ?", id);
	}
 }
