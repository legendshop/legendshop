package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdWithdrawCashDto;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.spi.service.AppPdWithdrawCashService;
import com.legendshop.spi.service.PdWithdrawCashService;
import com.legendshop.util.AppUtils;

/**
 * app 预存款提现记录service实现
 */
@Service("appPdWithdrawCashService")
public class AppPdWithdrawCashServiceImpl implements AppPdWithdrawCashService{

	@Autowired
	private PdWithdrawCashService pdWithdrawCashService;

	/**
	 * 获取预存款提现记录列表
	 * @param curPageNO 当前页码
	 * @param userId 用户id
	 * @param state 提现状态
	 * @return
	 */
	@Override
	public AppPageSupport<AppPdWithdrawCashDto> getPdWithdrawCashPage(String curPageNO, String userId, String state) {
		
		PageSupport<PdWithdrawCash> ps = pdWithdrawCashService.getPdWithdrawCashPage(curPageNO, userId, state);
		AppPageSupport<AppPdWithdrawCashDto> pageSupportDto =  PageUtils.convertPageSupport(ps, 
				new PageUtils.ResultListConvertor<PdWithdrawCash, AppPdWithdrawCashDto>() {

			@Override
			public List<AppPdWithdrawCashDto> convert(List<PdWithdrawCash> form) {

				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppPdWithdrawCashDto> toList = new ArrayList<AppPdWithdrawCashDto>();
				for (PdWithdrawCash pdWithdrawCash : form) {
					AppPdWithdrawCashDto appPdWithdrawCashDto = convertToAppPdWithdrawCashDto(pdWithdrawCash);
					toList.add(appPdWithdrawCashDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}
	
	/**
	 * 获取预存款提现记录详情
	 */
	@Override
	public AppPdWithdrawCashDto getPdWithdrawCash(Long id, String userId) {
		
		PdWithdrawCash pdWithdrawCash =  pdWithdrawCashService.getPdWithdrawCash(id, userId);
		if (AppUtils.isBlank(pdWithdrawCash)) {
			return null;
		}
		return convertToAppPdWithdrawCashDto(pdWithdrawCash);
	}
	
	private AppPdWithdrawCashDto convertToAppPdWithdrawCashDto(PdWithdrawCash pdWithdrawCash){
		  AppPdWithdrawCashDto appPdWithdrawCashDto = new AppPdWithdrawCashDto();
		  appPdWithdrawCashDto.setAmount(pdWithdrawCash.getAmount());
		  appPdWithdrawCashDto.setAddTime(pdWithdrawCash.getAddTime());
		  appPdWithdrawCashDto.setUserNote(pdWithdrawCash.getUserNote());
		  appPdWithdrawCashDto.setBankName(pdWithdrawCash.getBankName());
		  appPdWithdrawCashDto.setAdminNote(pdWithdrawCash.getAdminNote());
		  appPdWithdrawCashDto.setUserName(pdWithdrawCash.getUserName());
		  appPdWithdrawCashDto.setUserId(pdWithdrawCash.getUserId());
		  appPdWithdrawCashDto.setPdcSn(pdWithdrawCash.getPdcSn());
		  appPdWithdrawCashDto.setUpdateAdminName(pdWithdrawCash.getUpdateAdminName());
		  appPdWithdrawCashDto.setBankUser(pdWithdrawCash.getBankUser());
		  appPdWithdrawCashDto.setBankNo(pdWithdrawCash.getBankNo());
		  appPdWithdrawCashDto.setId(pdWithdrawCash.getId());
		  appPdWithdrawCashDto.setPaymentTime(pdWithdrawCash.getPaymentTime());
		  appPdWithdrawCashDto.setPaymentState(pdWithdrawCash.getPaymentState());
		  return appPdWithdrawCashDto;
		}
	
}
