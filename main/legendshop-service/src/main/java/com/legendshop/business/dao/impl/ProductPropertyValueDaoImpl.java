/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.ProductPropertyValueDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.helper.FileProcessor;
import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 *
 * 商品属性值服务
 *
 */
@Repository
public class ProductPropertyValueDaoImpl extends GenericDaoImpl<ProductPropertyValue, Long> implements ProductPropertyValueDao {
    /**
     * The log.
     */
    private final Logger log = LoggerFactory.getLogger(ProductPropertyValueDaoImpl.class);

    @Autowired
    private AttachmentManager attachmentManager;
    
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

    /**
     * TODO 加上缓存
     */
    public ProductPropertyValue getProductPropertyValue(Long id) {
        return getById(id);
    }

    /**
     * TODO 加上缓存
     */
    @Override
    public ProductPropertyValue getProductPropertyValueInfo(Long valueId, Long prodId) {
        String sql = "select lpv.*,lpa.alias from ls_prod_prop_value lpv left join ls_prop_value_alias lpa  on lpv.value_id=lpa.value_id and lpa.prod_id=? where lpv.value_id =?";
        return this.get(sql, ProductPropertyValue.class, prodId, valueId);
    }

    @Override
    public ProductPropertyValue getProductPropertyValueInfo(String name, Long propId) {
        String sql = "select * from ls_prod_prop_value where prop_id=? and name =?";
        return this.get(sql, ProductPropertyValue.class, propId, name);
    }

    public void deleteProductPropertyValue(ProductPropertyValue productPropertyValue) {
        delete(productPropertyValue);
    }
    @Override
    public ProductPropertyValue getProductPropertyValue(Long propId, String propertiesValue) {
        return getByProperties(new EntityCriterion().eq("propId",propId).eq("name",propertiesValue));
    }
    /**
     * 删除属性图片
     *
     * @param pic
     */
    public void deleteProductPropertyPic(String pic) {
        // 删除原来的图片
        if (AppUtils.isNotBlank(pic)) {
            String url = propertiesUtil.getBigPicPath() + "/" + pic;
            FileProcessor.deleteFile(url);

            // 删除缩略图规格
            String smallPicUrl = propertiesUtil.getSmallPicPath() + "/3/" + pic;
            log.debug("delete small Image file {}", smallPicUrl);
            FileProcessor.deleteFile(smallPicUrl);
        }
    }

    /**
     * 保存商品属性
     *
     * @param userName
     * @param productPropertyValue
     */
    public void saveProductPropertyValue(String userName,String userId, Long shopId, ProductPropertyValue productPropertyValue) {
        MultipartFile formFile = productPropertyValue.getFile();// 取得上传的文件
        boolean uploadFile = fileUploaded(formFile);
        productPropertyValue.setModifyDate(new Date());
        if (AppUtils.isNotBlank(productPropertyValue.getName())) {// update when name is not null
            if (uploadFile) {// upload file
                String pic = attachmentManager.upload(formFile);
                //保存附件表
                attachmentManager.saveImageAttachment(userName,userId, shopId, pic, formFile, AttachmentTypeEnum.PRODPROPVAL);
                productPropertyValue.setPic(pic);
                saveProductPropertyValue(productPropertyValue);
            } else {
                saveProductPropertyValue(productPropertyValue);
            }

        }
    }

    /**
     * 更新商品属性 userName： 操作人 toBeUpdateValue： 将要更新的记录 productPropertyValue：
     * 页面传过来的记录
     */
    public void updateProductPropertyValue(String userName,String userId, Long shopId, ProductPropertyValue toBeUpdateValue, ProductPropertyValue productPropertyValue) {
        MultipartFile formFile = productPropertyValue.getFile();// 取得上传的文件
        boolean uploadFile = fileUploaded(formFile);
        String orginPic = null; // 记住当前路径用来删除
        String newPic = null;
        try {
            if (AppUtils.isNotBlank(productPropertyValue.getName())) {// update when name is not null
                if (uploadFile) {// upload file
                    orginPic = toBeUpdateValue.getPic();
                    newPic = attachmentManager.upload(formFile);
                    //保存附件表
                    Long attmntId = attachmentManager.saveImageAttachment(userName, userId, shopId, newPic, formFile, AttachmentTypeEnum.PRODPROPVAL);
                    toBeUpdateValue.setPic(newPic);
                }
                toBeUpdateValue.setModifyDate(new Date());
                toBeUpdateValue.setName(productPropertyValue.getName());
                toBeUpdateValue.setSequence(productPropertyValue.getSequence());
                toBeUpdateValue.setStatus(productPropertyValue.getStatus());
                updateProductPropertyValue(toBeUpdateValue);
            }
        } catch (Exception e) {
            throw new BusinessException(e, "Save Product Propertity error", ErrorCodes.BUSINESS_ERROR);
        }

    }

    public void updateProductPropertyValue(ProductPropertyValue productPropertyValue) {
        update(productPropertyValue);
    }

    /**
     * 属性值是否有改变，如果没有改变则不需要更改数据库
     *
     * @param originList
     * @param value
     * @return
     */
    public boolean ifValueChanged(List<ProductPropertyValue> originList, ProductPropertyValue value) {
        boolean result = false;
        if (AppUtils.isNotBlank(originList) && AppUtils.isNotBlank(value)) {
            for (ProductPropertyValue productPropertyValue : originList) {
                if (productPropertyValue.getValueId().equals(value.getValueId())) {
                    if (fileUploaded(value.getFile()) || !productPropertyValue.getName().equals(value.getName())
                            || !productPropertyValue.getSequence().equals(value.getSequence())
                            || !productPropertyValue.getStatus().equals(value.getStatus())) {
                        return true;
                    }
                }
            }
        }
        return result;
    }

    private Long saveProductPropertyValue(ProductPropertyValue productPropertyValue) {
        Date date = new Date();
        productPropertyValue.setModifyDate(date);
        productPropertyValue.setRecDate(date);
        return save(productPropertyValue);
    }

    public PageSupport getProductPropertyValue(CriteriaQuery cq) {
        return queryPage(cq);
    }

    /**
     * File uploaded. 是否上传了文件
     *
     * @param formFile the form file
     * @return true, if successful
     */
    private boolean fileUploaded(MultipartFile formFile) {
        return formFile != null && formFile.getSize() > 0;
    }

    @Override
    public List<ProductPropertyValue> getAllProductPropertyValue(List<ProductProperty> propertyList) {
        if (AppUtils.isBlank(propertyList)) {
            return null;
        }
        Object[] propIds = new Object[propertyList.size()];
        for (int i = 0; i < propIds.length; i++) {
            propIds[i] = propertyList.get(i).getPropId();
        }
        return this.queryByProperties(new EntityCriterion().in("propId", propIds).addAscOrder("sequence"));
    }

    @Override
    public void deleteProductPropertyValue(Long propId) {
        update("delete from ls_prod_prop_value where prop_id = ?", propId);
    }

    @Override
    public List<ProductPropertyValue> queryProductPropertyValue(Long propId) {
        return this.queryByProperties(new EntityCriterion().eq("propId", propId).addAscOrder("sequence"));
    }

    @Override
    public void deleteProductPropertyValue(List<ProductPropertyValue> originList) {
        delete(originList);
    }


    @Override
    public Long saveCustomPropValue(ProductPropertyValue customPropValue) {
        return createId();
    }

    @Override
    public List<ProductPropertyValue> getProductPropertyValue(Object[] ids, Long prodId, Long propId) {
//		StringBuffer sql = new StringBuffer("select lpv.*,(select lpi.url from ls_prop_image lpi where lpi.value_id=lpv.value_id ");
//		sql.append("and lpi.prod_id=? and lpi.prop_id=? limit 1) url from ls_prod_prop_value lpv where lpv.value_id in(");
        //去掉limit限制，实现按属性图片顺序排序
        StringBuffer sql = new StringBuffer("SELECT lpv.*,lpi.url FROM ls_prop_image lpi, ls_prod_prop_value lpv WHERE lpi.value_id=lpv.value_id AND lpi.prod_id=658 AND lpi.prop_id=208 AND lpv.value_id IN(");
        Object[] params = new Object[ids.length + 2];
        params[0] = prodId;
        params[1] = propId;
        for (int i = 0; i < ids.length; i++) {
            params[i + 2] = ids[i];
            sql.append("?,");
        }
        sql.setLength(sql.length() - 1);
        sql.append(") ORDER BY lpi.seq "); //去掉limit限制，实现按属性图片顺序排序
        return this.queryLimit(sql.toString(), ProductPropertyValue.class, 0, 1, params);
    }

    @Override
    public List<ProductPropertyValue> getProductPropertyValue(List<Long> allValIdList, Long prodId) {
        StringBuffer sql = new StringBuffer("select lpv.*,lpa.alias from ls_prod_prop_value lpv left join ls_prop_value_alias lpa  on lpv.value_id=lpa.value_id and lpa.prod_id=? where lpv.value_id in(");
        Object[] params = new Object[allValIdList.size() + 1];
        params[0] = prodId;
        for (int i = 0; i < allValIdList.size(); i++) {
            params[i + 1] = allValIdList.get(i);
            sql.append("?,");
        }
        sql.setLength(sql.length() - 1);
        sql.append(") order by lpv.sequence");
        return this.query(sql.toString(), ProductPropertyValue.class, params);
    }

    @Override
    public List<ProductPropertyValue> getProductPropertyValue(List<Long> ids) {
        return this.queryAllByIds(ids);
    }

    @Override
    public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String curPageNO) {
        CriteriaQuery cq = new CriteriaQuery(ProductPropertyValue.class, curPageNO);
        return queryPage(cq);
    }

    @Override
    public PageSupport<ProductPropertyValue> getProductPropertyValue(String valueName, Long propId) {
        CriteriaQuery cq = new CriteriaQuery(ProductPropertyValue.class, "1");
        cq.setPageSize(30);
        cq.like("name", valueName, MatchMode.START);
        cq.eq("propId", propId);
        return queryPage(cq);
    }

    @Override
    public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String valueName, Long propId) {
        CriteriaQuery cq = new CriteriaQuery(ProductPropertyValue.class, "1");
        cq.setPageSize(30);
        cq.like("name", valueName, MatchMode.START);
        cq.eq("propId", propId);
        return queryPage(cq);
    }

	@Override
	public List<Long> getValueIdsByPropId(Long propId)
	{
		String sql="SELECT `value_id` FROM `ls_prod_prop_value` WHERE `prop_id`=?";
		return query(sql,Long.class,propId);
	}

	@Override
	public List<ProductPropertyValueDto> getValueListByPropId(Long propId) {
		String sql="SELECT value_id AS valueId,prop_id AS propId,`name` AS NAME,pic AS pic FROM ls_prod_prop_value WHERE prop_id=?";
		return query(sql,ProductPropertyValueDto.class,propId);
	}

}
