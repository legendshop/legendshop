package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AppToken;

public interface AppTokenDao extends GenericDao<AppToken, Long> {

	public abstract boolean deleteAppToken(String userId);
}
