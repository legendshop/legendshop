/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.util.AppUtils;

/**
 * 会员积分日志明细Dao
 *
 */
@Repository
public class IntegraLogDaoImpl extends GenericDaoImpl<IntegraLog, Long> implements IntegraLogDao {

	@Override
	public IntegraLog getIntegraLog(Long id) {
		return getById(id);
	}

	@Override
	public int deleteIntegraLog(IntegraLog integraLog) {
		return delete(integraLog);
	}

	@Override
	public Long saveIntegraLog(IntegraLog integraLog) {
		return save(integraLog);
	}

	@Override
	public int updateIntegraLog(IntegraLog integraLog) {
		return update(integraLog);
	}

	@Override
	public PageSupport<IntegraLog> getIntegraLogPage(String userId, String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(IntegraLog.class, curPageNO);
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		cq.setPageSize(15);
		return queryPage(cq);
	}

	@Override
	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, Integer pageSize, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(IntegraLog.class, curPageNO);
		QueryMap paramMap = new QueryMap();
		if(AppUtils.isNotBlank(integraLog.getUserName())) {
			paramMap.like("userName", integraLog.getUserName().trim());
		}
		paramMap.put("logType", integraLog.getLogType());
		if (AppUtils.isNotBlank(pageSize)) {
			// 非导出情况
			query.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY l.add_time desc");
		}else{
			paramMap.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("integralLog.getIntegralLogCount", paramMap);
		String QueryNsort = ConfigCode.getInstance().getCode("integralLog.getIntegralLog", paramMap);
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, String userId, Integer pageSize, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(IntegraLog.class, curPageNO);
		QueryMap paramMap = new QueryMap();
		paramMap.put("userId", userId);
		if (AppUtils.isNotBlank(pageSize)) {
			// 非导出情况
			query.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY l.add_time desc");
		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("integralLog.getIntegralLogCount", paramMap);
		String QueryNsort = ConfigCode.getInstance().getCode("integralLog.getIntegralLog", paramMap);
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	/**
	 * 列表查询会员积分日志明细
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLog(String userId, String curPageNO, String logDesc, Long logType, Date startDate, Date endDate) {
		CriteriaQuery cq = new CriteriaQuery(IntegraLog.class, curPageNO);
		if (AppUtils.isNotBlank(logDesc)) {
			cq.like("logDesc", "%" + logDesc + "%");
		}
		if (AppUtils.isNotBlank(logType)) {
			cq.eq("logType", logType);
		}
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		if(AppUtils.isNotBlank(startDate)){
			cq.ge("addTime", startDate);
		}
		if(AppUtils.isNotBlank(endDate)){
			cq.le("addTime", endDate);
		}

		cq.setPageSize(5);
		return queryPage(cq);
	}
	
	/**
	 * 可通过获取或使用状况查询会员积分日志明细列表
	 */
	@Override
	public PageSupport<IntegraLog> getIntegraLogByIsUsed(String userId, String curPageNO,Integer type,Long logType) {
		CriteriaQuery cq = new CriteriaQuery(IntegraLog.class, curPageNO);
		if (AppUtils.isNotBlank(logType)) {
			cq.eq("logType", logType);
		}
		cq.eq("userId", userId);
		if (AppUtils.isNotBlank(type)) {
			if(type>0){
				cq.gt("integralNum", 0);
			}
			if(type<0){
				cq.lt("integralNum", 0);
			}
		}
		cq.addDescOrder("addTime");
		cq.setPageSize(10);
		return queryPage(cq);
	}

}
