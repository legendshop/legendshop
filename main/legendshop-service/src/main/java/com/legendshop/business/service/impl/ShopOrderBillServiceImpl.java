/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.TimerLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.ShopOrderBillDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.util.AppUtils;

/**
 * 商家结算服务
 */
@Service("shopOrderBillService")
public class ShopOrderBillServiceImpl implements ShopOrderBillService {

	@Autowired
	private ShopOrderBillDao shopOrderBillDao;

	@Autowired
	private SubDao subDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public ShopOrderBill getShopOrderBill(Long id) {
		ShopOrderBill shopOrderBill = shopOrderBillDao.getShopOrderBill(id);
		int shopBillDay = systemParameterUtil.getShopBillDay();
		shopOrderBill.setShopBillDay(shopBillDay);
		return shopOrderBill;
	}

	@Override
	public void deleteShopOrderBill(ShopOrderBill shopOrderBill) {
		shopOrderBillDao.deleteShopOrderBill(shopOrderBill);
	}

	@Override
	public Long saveShopOrderBill(ShopOrderBill shopOrderBill) {
		if (!AppUtils.isBlank(shopOrderBill.getId())) {
			updateShopOrderBill(shopOrderBill);
			return shopOrderBill.getId();
		}
		return shopOrderBillDao.saveShopOrderBill(shopOrderBill);
	}

	@Override
	public void updateShopOrderBill(ShopOrderBill shopOrderBill) {
		shopOrderBillDao.updateShopOrderBill(shopOrderBill);
	}

	/**
	 * 获取当前期结算的月份
	 */
	@Override
	public String getShopBillMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		return sdf.format(c.getTime());
	}

	/**
	 * 生成结算单号
	 */
	private synchronized String generateShopBillSN(Long shopId) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyMMddHHmmssSSS");
		String billSN = simpledateformat.format(new Date());
		Random r = new Random();
		String shopIds = shopId.toString();
		int length = shopIds.length();
		if(length>2){
			shopIds = shopIds.substring(length - 2, length);
		}
		billSN = billSN + shopIds + CommonServiceUtil.randomNumeric(r, 3);
		return billSN;
	}

	/**
	 * 订单开始时间
	 * @param endDate
	 * @return
	 */
	private Date parseStartDate(Date endDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(endDate);
		c.add(Calendar.MONTH, -1);
		return c.getTime();
	}

	/**
	 * 订单结束时间  这个月的开始日期  例如： 2015-12-01 00:00:00
	 * @return
	 */
	private Date parseEndDate() {
		Calendar c = Calendar.getInstance();
		Date nowDate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM");
		Date endDate = null;
		try {
			endDate = df.parse(df.format(nowDate));
			c.setTime(endDate);
			//c.add(Calendar.MONTH, 1);//测试
			endDate = c.getTime();
		} catch (ParseException e) {
			throw new BusinessException("date format exception");
		}
		return endDate;
	}

	/**
	 * 上个订单结束时间，本账单的开始时间
	 * @param endDate
	 * @return
	 */
	private Date parseBillStartDate(Date endDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(endDate);
		c.add(Calendar.SECOND, 1);
		return c.getTime();
	}

	@Override
	public ShopOrderBill getLastShopOrderBillByShopId(Long shopId) {
		return shopOrderBillDao.getLastShopOrderBillByShopId(shopId);
	}

	/**
	 * 保存结算单并更新订单状态和退货单为结束状态
	 * 
	 */
	@Override
	public void generateShopBill(ShopOrderBill shopOrderBill, List<Long> billSubIds, List<Long> billRefundId,List<Long> billPresellSubIds,List<Long> billAuctionDepositRecIds) {
		int billSubIdNumber = 0;
		if(billSubIds != null){
			billSubIdNumber = billSubIds.size();
		}
		
		int billRefundNum = 0;
		if(billRefundId != null){
			billRefundNum = billRefundId.size();
		}

		int billPresellSubIdNum = 0;
		if(billPresellSubIds != null){
			billPresellSubIdNum = billPresellSubIds.size();
		}
		int billAuctionDepositRecIdNum = 0;
		if(billAuctionDepositRecIds != null){
			billAuctionDepositRecIdNum = billAuctionDepositRecIds.size();
		}

		TimerLog.info("商家 {} 结算 订单Id {}， 退货单Id {}，预售订单 {}，拍卖保证金记录 {}", shopOrderBill.getShopId(), billSubIdNumber, billRefundNum,billPresellSubIdNum,billAuctionDepositRecIdNum);
		
		Long number=shopOrderBillDao.saveShopOrderBill(shopOrderBill);
		if(number>0){
			if(AppUtils.isNotBlank(billSubIds)){
				List<Object[]> batchArgs = new ArrayList<Object[]>();
				for (int i=0;i<billSubIds.size();i++) {
					batchArgs.add(new Object[]{1,shopOrderBill.getSn(),billSubIds.get(i)});
				}
				subDao.batchUpdate("update ls_sub set is_bill=?,bill_sn=? where sub_id =? ", batchArgs);
			}
			
			if(AppUtils.isNotBlank(billRefundId)){
				List<Object[]> batchArgs = new ArrayList<Object[]>();
				for (int i = 0; i < billRefundId.size(); i++) {
					batchArgs.add(new Object[]{1,shopOrderBill.getSn(),billRefundId.get(i)});
				}
				subDao.batchUpdate("update ls_sub_refund_return set is_bill=?,bill_sn=? where refund_id =? ",batchArgs);
			}
			if(AppUtils.isNotBlank(billPresellSubIds)){
				List<Object[]> batchArgs = new ArrayList<Object[]>();
				for (int i = 0; i < billPresellSubIds.size(); i++) {
					batchArgs.add(new Object[]{1,shopOrderBill.getSn(),billPresellSubIds.get(i)});
				}
				subDao.batchUpdate("update ls_presell_sub set is_bill=?,bill_sn=? where sub_id =? ",batchArgs);
			}

			if(AppUtils.isNotBlank(billAuctionDepositRecIds)){
				List<Object[]> batchArgs = new ArrayList<Object[]>();
				for (int i = 0; i < billAuctionDepositRecIds.size(); i++) {
					batchArgs.add(new Object[]{1,shopOrderBill.getSn(),billAuctionDepositRecIds.get(i)});
				}
				subDao.batchUpdate("update ls_auction_depositrec set is_bill=?,bill_sn=? where id =? ",batchArgs);
			}
		}
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBillPage(String curPageNO, ShopOrderBill shopOrderBill) {
		return shopOrderBillDao.getShopOrderBillPage(curPageNO,shopOrderBill);
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO, ShopOrderBill shopOrderBill) {
		return shopOrderBillDao.getShopOrderBillPage(shopId,curPageNO,shopOrderBill);
	}

	@Override
	public boolean getShopOrderBill(Long shopId, Date startDate, Date endDate) {
		return shopOrderBillDao.getShopOrderBill(shopId, startDate, endDate);
	}

	@Override
	public PageSupport<ShopOrderBill> getShopOrderBill(String curPageNO, Long shopId, Integer status, int pageSize) {
		return shopOrderBillDao.getShopOrderBill(curPageNO, shopId, status, pageSize);
	}

}
