/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.auction.BiddersWinDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.util.AppUtils;

/**
 * 中标Dao实现类.
 */
@Repository
public class BiddersWinDaoImpl extends GenericDaoImpl<BiddersWin, Long> implements BiddersWinDao {

	public List<BiddersWin> getBiddersWin(String userId) {
		return this.queryByProperties(new EntityCriterion().eq("userId", userId).addAscOrder("status").addAscOrder("bitTime"));
	}

	public BiddersWin getBiddersWin(Long id) {
		return getById(id);
	}

	public int deleteBiddersWin(BiddersWin biddersWin) {
		return delete(biddersWin);
	}

	public Long saveBiddersWin(BiddersWin biddersWin) {
		return save(biddersWin);
	}

	public int updateBiddersWin(BiddersWin biddersWin) {
		return update(biddersWin);
	}

	@Override
	public boolean isWin(String userId, Long prodId, Long skuId, Long paimaiId) {
		if (AppUtils.isNotBlank(skuId) && skuId != 0) {
			return getLongResult("select count(id) from ls_bidders_win where a_id=? and prod_id=? and sku_id=? and user_id=?",
					new Object[] { paimaiId, prodId, skuId, userId }) > 0;
		} else {
			return getLongResult("select count(id) from ls_bidders_win where a_id=? and prod_id=? and user_id=?",
					new Object[] { paimaiId, prodId, userId }) > 0;

		}
	}

	@Override
	public BiddersWin getBiddersWin(String userId, Long id) {
		return get(
				"select id as id,a_id as aId,prod_id as prodId,sku_id as skuId,price as price,status as status from ls_bidders_win where user_id=? and id=? ",
				BiddersWin.class, new Object[] { userId, id });
	}

	@Override
	public void updateBiddersWin(Long id, String subNember, Long subId, int value) {
		update("update ls_bidders_win set sub_number=? ,sub_id=?,status=?,order_time=? where id=? and status=-1 ",
				new Object[] { subNember, subId, value, new Date(), id });
	}

	@Override
	public List<BiddersWin> getBiddersWin(Date date, int commitInteval, Integer status) {
		String sql = "";
		if (status == -1) {
			sql = "SELECT a_id AS aId,user_id AS userId FROM ls_bidders_win WHERE ?	> bit_time AND STATUS=" + status;
		}
		if (status == 0) {
			sql = "SELECT a_id AS aId,user_id AS userId FROM ls_bidders_win WHERE ? > order_time AND STATUS=" + status;
		}
		return queryLimit(sql, BiddersWin.class, 0, commitInteval, date);
	}

	@Override
	public List<BiddersWin> getBiddersWinList(Long id) {
		return this.queryByProperties(new EntityCriterion().eq("id", id));
	}

	@Override
	public BiddersWin getBiddersWinByAid(Long aid) {
		return this.getByProperties(new EntityCriterion().eq("aId", aid));
	}

	@Override
	public PageSupport<BiddersWin> getBiddersWinPage(String curPageNO, BiddersWin biddersWin) {
		CriteriaQuery cq = new CriteriaQuery(BiddersWin.class, curPageNO);
		return queryPage(cq);
	}

	@Override
	public PageSupport<BiddersWin> queryBiddersWinListPage(String curPageNO, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(BiddersWin.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String querySQL = ConfigCode.getInstance().getCode("bidders.queryBiddersWin", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("bidders.queryBiddersWinCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<BiddersWin> queryUserPartAution(String curPageNO, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(BiddersWin.class,8, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String querySQL = ConfigCode.getInstance().getCode("bidders.queryUserPartAution", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("bidders.queryUserPartAutionCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
