package com.legendshop.business.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.SubDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.model.constant.OrderDeleteStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PayMannerEnum;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.RefundHandleStatusEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.dto.OrderExprotDto;
import com.legendshop.spi.service.OrderService;
import com.legendshop.util.AppUtils;


/**
 * 用于所有订单信息的查看
 *
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {
	
	private final static String GET_DELIVERYTYPE_SQL="SELECT ls_dvy_type.dvy_type_id AS dvyTypeId,ls_dvy_type.dvy_id AS dvyId,ls_dvy_type.printtemp_id AS printtempId,ls_dvy_type.name AS NAME,ls_dvy_type.notes AS notes,ls_dvy_type.is_system AS isSystem,ls_dvy_type.notes AS notes," +
					"ls_delivery.name AS company,ls_delivery.name AS printName FROM ls_dvy_type  LEFT JOIN ls_delivery ON ls_dvy_type.dvy_id=ls_delivery.dvy_id LEFT JOIN ls_print_tmpl ON ls_print_tmpl.prt_tmpl_id=ls_dvy_type.printtemp_id  WHERE ls_dvy_type.dvy_type_id=?  ";
	
	@Autowired
	private SubDao subDao;

	@Autowired
	private ProductService productService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ConstTableService constTableService;
	
	public PageSupport<MySubDto> getMyorderDtos(OrderSearchParamDto paramDto) {
		return constructPageSupport(paramDto);
	}
	
	public PageSupport<MySubDto> getShopOrderDtos(OrderSearchParamDto paramDto) {
		return constructPageSupport(paramDto);
	}

	public PageSupport<MySubDto> getAdminOrderDtos(OrderSearchParamDto paramDto) {
		return constructPageSupport(paramDto);
	}
	/**
	 * 订单列表
	 * @param paramDto
	 * @return
	 */
	public PageSupport<MySubDto> constructPageSupport(OrderSearchParamDto paramDto) {
		int curPageNO = 1;
		try {
			curPageNO=Integer.valueOf(paramDto.getCurPageNO());
		} catch (Exception e) {
			curPageNO = 1;
		}
		if(curPageNO <=0){
			curPageNO = 1;//防止溢出
		}
		int pageSize=paramDto.getPageSize();
		int offset=(curPageNO-1)*paramDto.getPageSize();
		QueryMap queryMap=new QueryMap();
		List<Object> args=new ArrayList<Object>();
		if(AppUtils.isNotBlank(paramDto.getUserId())){
			queryMap.put("userId", paramDto.getUserId());
			args.add(paramDto.getUserId());
		}
		if(AppUtils.isNotBlank(paramDto.getShopId())){
			queryMap.put("shopId", paramDto.getShopId());
			args.add(paramDto.getShopId());
		}
		if(AppUtils.isNotBlank(paramDto.getStatus())){
			queryMap.put("status", paramDto.getStatus());
			args.add(paramDto.getStatus());
		}
		//订单类型
		if(AppUtils.isNotBlank(paramDto.getSubType())){
			queryMap.put("subType", paramDto.getSubType());
			args.add(paramDto.getSubType());
		}
		//是否支付
		if(AppUtils.isNotBlank(paramDto.getIsPayed())){
			queryMap.put("isPayed", paramDto.getIsPayed());
			args.add(paramDto.getIsPayed());
		}
		//维权状态
		if (AppUtils.isNotBlank(paramDto.getRefundStatus())) {
			queryMap.put("refundState", paramDto.getRefundStatus());
			args.add(paramDto.getRefundStatus());
		}
		//备注状态
		if (AppUtils.isNotBlank(paramDto.getIsShopRemak())) {
			queryMap.put("isShopRemarked", paramDto.getIsShopRemak());
			args.add(paramDto.getIsShopRemak());
		}
		
		if(AppUtils.isNotBlank(paramDto.getDeleteStatus()) && !paramDto.isIncludeDeleted()){
			queryMap.put("deleteStatus", paramDto.getDeleteStatus());
			args.add(paramDto.getDeleteStatus());
		}
		
		if (AppUtils.isNotBlank(paramDto.getUserMobile())) {
			queryMap.put("userMobile", paramDto.getUserMobile());
			args.add(paramDto.getUserMobile());
		}
		try {
			if(AppUtils.isNotBlank(paramDto.getStartDate())){
				Date fromDate= DateUtils.getIntegralStartTime(paramDto.getStartDate());
				queryMap.put("startSubDate", fromDate);
				args.add(fromDate);
			}
			if(AppUtils.isNotBlank(paramDto.getEndDate())){
				Date toDate = DateUtils.getIntegralEndTime(paramDto.getEndDate());
				queryMap.put("endSubDate", toDate);
				args.add(toDate);
			}
			//付款日期
			if(AppUtils.isNotBlank(paramDto.getPayStartDate())){
				Date payStart= DateUtils.getIntegralStartTime(paramDto.getPayStartDate());
				queryMap.put("payStartDate", payStart);
				args.add(payStart);
			}
			if(AppUtils.isNotBlank(paramDto.getPayEndDate())){
				Date payEnd = DateUtils.getIntegralEndTime(paramDto.getPayEndDate());
				queryMap.put("payEndDate", payEnd); 
				args.add(payEnd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(AppUtils.isNotBlank(paramDto.getUserName())){
			queryMap.put("userName", paramDto.getUserName());
			args.add("%" + paramDto.getUserName().trim() + "%");
		}
		if(AppUtils.isNotBlank(paramDto.getShopName())){
			queryMap.put("shopName", paramDto.getShopName());
			args.add("%" + paramDto.getShopName().trim() + "%");
		}
		//订单编号
		if(AppUtils.isNotBlank(paramDto.getSubNumber())){
			queryMap.put("subNumber", paramDto.getSubNumber());
			args.add("%" + paramDto.getSubNumber().trim() + "%");
		}
		//下单时商家是否开启发票
		if(AppUtils.isNotBlank(paramDto.getSwitchInvoice())){
			queryMap.put("switchInvoice", paramDto.getSwitchInvoice());
			args.add(paramDto.getSwitchInvoice());
		}
		//买家电话（收货人电话）
		if (AppUtils.isNotBlank(paramDto.getReciverMobile())) {
			queryMap.put("reciverMobile", paramDto.getReciverMobile());
			args.add(paramDto.getReciverMobile());
		}
		//买家（收货人）
		if (AppUtils.isNotBlank(paramDto.getReciverName())) {
			queryMap.put("reciverName", paramDto.getReciverName());
			args.add("%" + paramDto.getReciverName().trim() + "%");
		}
		
// 删除了也要显示出来 所以要去掉		
//		if(!paramDto.isIncludeDeleted() && AppUtils.isNotBlank(paramDto.getDeleteStatus())){
//			//不包含已经删除的订单
//			queryMap.put("deleteStatus", paramDto.getDeleteStatus());
//			args.add(paramDto.getDeleteStatus());
//		}
		queryMap.put("offset", offset);
		queryMap.put("pageSize", pageSize);
		PageSupport<MySubDto> pageSupport=subDao.getOrders(queryMap,args,curPageNO,pageSize,paramDto);
		if (AppUtils.isNotBlank(pageSupport)&&AppUtils.isNotBlank(pageSupport.getResultList())&&pageSupport.getResultList().size()>0){
			for (MySubDto mySubDto : pageSupport.getResultList()) {
				for (SubOrderItemDto subOrderItemDto : mySubDto.getSubOrderItemDtos()) {
					subOrderItemDto.setInReturnValidPeriod(true);
					//判断是否过了可退款时间
					if(OrderStatusEnum.SUCCESS.value().equals(mySubDto.getStatus()) && !isInReturnValidPeriod(subOrderItemDto.getProdId(),mySubDto)){
						subOrderItemDto.setInReturnValidPeriod(false);
					}
				}
			}
		}
		return pageSupport;
	}



	/**
	 * 清除缓存
	 */
	@Override
	@Caching(evict={@CacheEvict(value="OrderDtoList",key="#shopId")})
	public void cleanOrderCache(String userId,Long shopId) {
		subDao.cleanOrderCache(userId,OrderDeleteStatusEnum.NORMAL.value());
		subDao.cleanOrderCache(userId,OrderDeleteStatusEnum.DELETED.value());
		subDao.cleanOrderCache(userId,OrderDeleteStatusEnum.PERMANENT_DELETED.value());
		subDao.cleanUnpayOrderCount(userId);
		subDao.cleanConsignmentOrderCount(userId);
	}

	@Override
	@Cacheable(value = "UnpayOrderCount",key = "#userId")
	public Integer calUnpayOrderCount(String userId) {
		return subDao.get("select count(s.sub_id) from ls_sub s where s.status=? and s.user_id=?", Integer.class, OrderStatusEnum.UNPAY.value(),userId);
	}

	@Override
	@Cacheable(value = "ConsignmentOrderCount",key = "#userId")
	public Integer calConsignmentOrderCount(String userId) {
		return subDao.get("select count(s.sub_id) from ls_sub s where s.status=? and s.user_id=?", Integer.class, OrderStatusEnum.CONSIGNMENT.value(),userId);
	}

	@Override
	/*@Cacheable(value="StoreProdCount",key="#poductId+#skuId")*/
	public boolean getStoreProdCount(Long poductId, Long skuId) {
		long number=0;
			if(skuId==null || skuId==0){
				number= subDao.getLongResult("select count(id) from ls_store_prod where prod_id=? and stock > 0", poductId);
			}else{
				number= subDao.getLongResult("select count(id) from ls_store_sku where prod_id=? and sku_id=? and stock > 0", poductId,skuId);
			}
		return number>0;
	}

	@Override
	public PageSupport<PresellSubDto> getPresellOrderList(OrderSearchParamDto paramDto) {
		int curPageNO = 1;
		try {
			curPageNO = Integer.valueOf(paramDto.getCurPageNO());
		} catch (Exception e) {
			curPageNO = 1;
		}

		if(curPageNO < 0){
			curPageNO = 1;
		}
		int pageSize = paramDto.getPageSize();
		int offset = (curPageNO - 1) * paramDto.getPageSize();

		QueryMap queryMap = new QueryMap();
		List<Object> args = new ArrayList<Object>();

		queryMap.put("offset", offset);
		queryMap.put("pageSize", pageSize);
		if(AppUtils.isNotBlank(paramDto.getUserId())){
			queryMap.put("userId", paramDto.getUserId());
			args.add(paramDto.getUserId());
		}
		if(AppUtils.isNotBlank(paramDto.getUserName())){
			queryMap.like("userName","%" + paramDto.getUserName().trim()+ "%");
			args.add("%"+paramDto.getUserName().trim()+ "%");
		}
		if(AppUtils.isNotBlank(paramDto.getShopId())){
			queryMap.put("shopId", paramDto.getShopId());
			args.add(paramDto.getShopId());
		}

		if(AppUtils.isNotBlank(paramDto.getSubNumber())){
			queryMap.like("subNumber","%" + paramDto.getSubNumber().trim() + "%");
			args.add("%" + paramDto.getSubNumber().trim() + "%");
		}
		if(AppUtils.isNotBlank(paramDto.getStatus())){
			queryMap.put("status", paramDto.getStatus());
			args.add(paramDto.getStatus());
		}
		if(AppUtils.isNotBlank(paramDto.getIsPayed())){
			queryMap.put("isPayed", paramDto.getIsPayed());
			args.add(paramDto.getIsPayed());
		}
		if(AppUtils.isNotBlank(paramDto.getShopName())){
			queryMap.like("shopName","%" + paramDto.getShopName().trim() + "%");
			args.add("%" + paramDto.getShopName().trim() + "%");
		}
		if(AppUtils.isNotBlank(paramDto.getStartDate())){
			queryMap.put("startDate", paramDto.getStartDate());
			args.add(paramDto.getStartDate());
		}
		if(AppUtils.isNotBlank(paramDto.getEndDate())){
			queryMap.put("endDate", paramDto.getEndDate());
			args.add(paramDto.getEndDate());
		}

		if(AppUtils.isNotBlank(paramDto.getDeleteStatus()) && !paramDto.isIncludeDeleted()){
			queryMap.put("deleteStatus", paramDto.getDeleteStatus());
			args.add(paramDto.getDeleteStatus());
		}

		return subDao.getPresellOrdersList(queryMap,args,curPageNO,pageSize,paramDto);
	}

	/**
	 * 导出预售订单
	 */
	@Override
	public List<OrderExprotDto> exportPresellOrders(OrderSearchParamDto orderSearchParamDto) {
		if(AppUtils.isNotBlank(orderSearchParamDto)){
			StringBuilder sb=new StringBuilder();
			List<Object> obj=new ArrayList<Object>();
			sb.append("SELECT o.sub_number number,")
			.append("o.status statue,o.user_name userName,o.sub_date createDate,")
			.append("o.total totalPrice,o.is_payed isPayed,o.pay_pct_type payPctType,")
			.append("o.pay_manner payManner,o.pay_type_name payTypeName,")
			.append("o.shop_name shopName,o.freight_amount freightAmount,")
			.append("o.deposit_trade_no depositTradeNo,o.pre_deposit_price depositPrice,")
			.append("o.final_price finalPrice,o.is_pay_final isPayFinal,")
			.append("item.prod_name prodName,item.attribute attr,item.price price ")
			.append("FROM (SELECT ps.sub_id, s.sub_number,s.shop_id, s.shop_name,s.user_id, s.user_name, s.prod_name, s.sub_date, s.total, s.actual_total, s.freight_amount,")
			.append("s.product_nums, s.update_date,ps.pre_deposit_price, ps.is_pay_deposit, ps.deposit_pay_name,ps.deposit_trade_no,ps.final_pay_name,ps.final_trade_no,")
			.append("ps.final_price, ps.pay_pct_type, ps.final_m_start, ps.final_m_end, ps.is_pay_final, s.status,s.pay_manner,s.pay_id, s.pay_date, s.pay_type_id,s.pay_type_name,")
			.append("s.other, s.score, s.dvy_type_id, s.dvy_type, s.dvy_flow_id, s.invoice_sub_id, s.order_remark, s.addr_order_id,s.dvy_date, s.is_need_invoice, s.weight,")
			.append("s.volume, s.finally_date,s.is_cod, s.is_payed, s.delete_status, s.dist_commis_amount, s.discount_price, s.coupon_off_price FROM ls_sub s, ls_presell_sub ps ")
			.append("WHERE s.sub_id = ps.sub_id AND sub_type = 'PRE_SELL'");
			if(AppUtils.isNotBlank(orderSearchParamDto.getStatus())){
				sb.append(" AND s.status=?");
				obj.add(orderSearchParamDto.getStatus());
			}
			if(AppUtils.isNotBlank(orderSearchParamDto.getShopId())){
				sb.append(" AND s.shop_id = ?");
				obj.add(orderSearchParamDto.getShopId());
			}
			if(AppUtils.isNotBlank(orderSearchParamDto.getSubNumber())){
				sb.append(" AND s.sub_number=?");
				obj.add(orderSearchParamDto.getSubNumber());
			}
			if(AppUtils.isNotBlank(orderSearchParamDto.getUserName())){
				sb.append(" AND s.user_name=?");
				obj.add(orderSearchParamDto.getUserName());
			}
			if(AppUtils.isNotBlank(orderSearchParamDto.getStartDate())){
				sb.append(" AND  s.sub_date >= ?");
				obj.add(orderSearchParamDto.getStartDate());
			}
			if(AppUtils.isNotBlank(orderSearchParamDto.getEndDate())){
				sb.append(" AND s.sub_date < ?");
				obj.add(orderSearchParamDto.getEndDate());
			}
			sb.append(" ORDER BY s.sub_date DESC,s.status ASC) o ")
			.append("INNER JOIN ls_sub_item item ON item.sub_number = o.sub_number ORDER BY o.sub_date DESC");
			List<OrderExprotDto> list= this.subDao.exportOrder(sb.toString(), obj.toArray());
			for (OrderExprotDto orderExprotDto : list) {
				if(orderExprotDto.getStatue().equals(OrderStatusEnum.UNPAY.value()+"")){
					orderExprotDto.setStatue("未付款");
				}
				if(orderExprotDto.getStatue().equals(OrderStatusEnum.PADYED.value()+"")){
					orderExprotDto.setStatue("待发货");
				}
				if(orderExprotDto.getStatue().equals(OrderStatusEnum.CONSIGNMENT.value()+"")){
					orderExprotDto.setStatue("已发货");
				}
				if(orderExprotDto.getStatue().equals(OrderStatusEnum.SUCCESS.value()+"")){
					orderExprotDto.setStatue("交易成功");
				}
				if(orderExprotDto.getStatue().equals(OrderStatusEnum.CLOSE.value()+"")){
					orderExprotDto.setStatue("交易关闭");
				}
				//付款状态
				if("1".equals(orderExprotDto.getIsPayed())){
					orderExprotDto.setIsPayed("已付款");
				}else{
					orderExprotDto.setIsPayed("未付款");
				}
				//付款方式
				if("1".equals(orderExprotDto.getPayManner())){
					orderExprotDto.setPayManner("货到付款");
				}else {
					orderExprotDto.setPayManner("在线支付");
				}
				//预售订单支付方式
				if("0".equals(orderExprotDto.getPayPctType())){
					orderExprotDto.setPayPctType("全额支付");
				}else {
					orderExprotDto.setPayPctType("订金支付");
				}
				//尾款支付状态
				if("0".equals(orderExprotDto.getIsPayFinal())){
					orderExprotDto.setIsPayFinal("已付款");
				}else {
					orderExprotDto.setIsPayFinal("未付款");
				}
				//属性，规格
				if(orderExprotDto.getAttr()!=null){
					orderExprotDto.setAttr(orderExprotDto.getAttr().replace(";", "\n"));
				}
			}
			return list;
		}
		return null;
	}


	@Override
	public DeliveryTypeDto getDeliveryTypeDto( Long id) {
		return subDao.get(GET_DELIVERYTYPE_SQL, new Object[]{id}, new RowMapper<DeliveryTypeDto>() {
			@Override
			public DeliveryTypeDto mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				DeliveryTypeDto deliveryTypeDto=new DeliveryTypeDto();
				deliveryTypeDto.setCompany(rs.getString("company"));
				deliveryTypeDto.setDvyId(rs.getLong("dvyId"));
				deliveryTypeDto.setDvyTypeId(rs.getLong("dvyTypeId"));
				deliveryTypeDto.setName(rs.getString("name"));
				deliveryTypeDto.setPrintName(rs.getString("printName"));
				deliveryTypeDto.setPrinttempId(rs.getLong("printtempId"));
				deliveryTypeDto.setIsSystem(rs.getInt("isSystem"));
				deliveryTypeDto.setNotes(rs.getString("notes"));
				return deliveryTypeDto;
			}
		});
	}

	/**
	 * 导出订单
	 */
	@Override
	public List<OrderExprotDto> exportOrder(OrderSearchParamDto orderSearchParamDto) throws ParseException {
		StringBuilder sb = new StringBuilder();
		List<Object> obj = new ArrayList<Object>();
		sb.append("SELECT t.sub_number number, t.status statue, t.actual_total totalPrice, t.freight_amount freightAmount,");
		sb.append("t.coupon_off_price couponPrice, t.redpack_off_price redpackPrice, t.pay_type_name payTypeName, ");
		sb.append("t.user_name userName, t.order_remark remark,t.prod_name prodName, t.basket_count prodCount, t.cn_properties attr, ");
		sb.append("t.sub_date createDate, t.shop_name shopName, t.price price, t.model_id modelId, t.party_code partyCode, ");
		sb.append("t.is_payed isPayed, t.refundSts1, t.refundSts2, t.receiver, t.mobile, t.detail_address addr, t.sub_type subType, ");
		sb.append("t.pay_date orderPayDate, t.dvy_date  deliveryDate, inv.company company, t.dvy_type dvyType, t.pay_manner payManner, ");
		sb.append("t.pay_pct_type payPctType, t.deposit_trade_no depositTradeNo, t.pre_deposit_price preDepositPrice, ");
		sb.append("t.deposit_pay_name depositPayName, t.final_trade_no finalTradeNo, t.final_price finalPrice, ");
		sb.append("t.final_pay_name finalPayName, t.is_pay_final isPayFinal ");
		sb.append("FROM ( ");
		sb.append("SELECT s.sub_number, s.status, s.actual_total, s.freight_amount, si.coupon_off_price, s.redpack_off_price,");
		sb.append("s.pay_type_name, s.addr_order_id, u.user_name, s.order_remark ,si.prod_name, si.basket_count, ");
//		sb.append("sk.cn_properties, s.sub_date, s.shop_name, sk.price,sk.model_id ,sk.party_code , s.is_payed, ");
		sb.append("sk.cn_properties, s.sub_date, s.shop_name, si.price,sk.model_id ,sk.party_code , s.is_payed, ");
		sb.append("r.is_handle_success refundSts1, rr.is_handle_success refundSts2, s.pay_date, s.dvy_date, s.invoice_sub_id, ");
		sb.append("a.receiver receiver, a.mobile mobile, a.detail_address, s.dvy_type, s.pay_manner, s.sub_type, ");
		sb.append("ps.pay_pct_type, ps.deposit_trade_no, ps.pre_deposit_price, ps.deposit_pay_name, ");
		sb.append("ps.final_trade_no, ps.final_price, ps.final_pay_name, ps.is_pay_final ");
		sb.append("FROM ls_sub s ");
		sb.append("LEFT JOIN ls_presell_sub ps ON ps.sub_number = s.sub_number ");
		sb.append("INNER JOIN ls_usr_detail u ON s.user_id = u.user_id ");
		sb.append("INNER JOIN ls_sub_item si ON s.sub_number = si.sub_number ");
		sb.append("INNER JOIN ls_usr_addr_sub a ON s.addr_order_id = a.addr_order_id ");
		sb.append("LEFT JOIN ls_sku sk ON si.sku_id=sk.sku_id ");
		//去掉 AND r.seller_state=2
		sb.append("LEFT JOIN (SELECT * FROM ls_sub_refund_return WHERE seller_state !=3 and apply_state != -1)  r ");
		sb.append("ON r.sub_item_id = si.sub_item_id ");
		//去掉  AND rr.seller_state=2
		sb.append("LEFT JOIN (SELECT * FROM ls_sub_refund_return WHERE seller_state !=3 and apply_state != -1)  rr ");
		sb.append("ON rr.sub_number = si.sub_number ");
		sb.append("AND rr.sub_item_id = 0 ");
		sb.append("WHERE s.sub_number=si.sub_number AND s.user_id=u.user_id ");

		if (AppUtils.isNotBlank(orderSearchParamDto.getShopId())) {
			sb.append(" AND s.shop_id=?");
			obj.add(orderSearchParamDto.getShopId());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getStatus())) {
			sb.append(" AND s.status=?");
			obj.add(orderSearchParamDto.getStatus());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getShopName())) {
			sb.append(" AND s.shop_name LIKE ?");
			obj.add("%" + orderSearchParamDto.getShopName().trim() + "%");
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getSubNumber())) {
			sb.append(" AND s.sub_number=?");
			obj.add(orderSearchParamDto.getSubNumber());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getUserName())) {
			sb.append(" AND s.user_name LIKE ?");
			obj.add("%"+ orderSearchParamDto.getUserName().trim() +"%");
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getStartDate())) {
			sb.append(" AND  s.sub_date >= ?");
			obj.add(orderSearchParamDto.getStartDate());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getEndDate())) {
			sb.append(" AND s.sub_date < ?");
			obj.add(DateUtils.getIntegralEndTime(orderSearchParamDto.getEndDate()));
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getSubType())) {
			sb.append(" AND s.sub_type=?");
			obj.add(orderSearchParamDto.getSubType());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getIsPayed())) {
			sb.append(" AND s.is_payed=?");
			if (orderSearchParamDto.getIsPayed() == 0) {//未支付
				obj.add(false);
			}else{//已支付
				obj.add(true);
			}
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getRefundStatus())) {
			sb.append(" AND s.refund_state=?");
			obj.add(orderSearchParamDto.getRefundStatus());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getPayStartDate())) {
			sb.append(" AND s.pay_date >= ?");
			obj.add(orderSearchParamDto.getPayStartDate());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getPayEndDate())) {
			sb.append(" AND s.pay_date < ?");
			obj.add(DateUtils.getIntegralEndTime(orderSearchParamDto.getPayEndDate()));
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getIsShopRemak())) {
			sb.append(" AND s.is_shop_remarked=?");
			obj.add(orderSearchParamDto.getIsShopRemak());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getReciverMobile())) {
			sb.append(" AND a.mobile=?");
			obj.add(orderSearchParamDto.getReciverMobile());
		}
		if (AppUtils.isNotBlank(orderSearchParamDto.getReciverName())) {
			sb.append(" AND a.receiver like ?");
			obj.add("%"+ orderSearchParamDto.getReciverName().trim() +"%");
		}
		sb.append("	ORDER BY s.sub_date DESC ) t ");
		sb.append(" LEFT JOIN ls_invoice_sub inv ON t.invoice_sub_id = inv.id ");
		sb.append(" ORDER BY t.sub_date DESC ");
		List<OrderExprotDto> list = this.subDao.exportOrder(sb.toString(), obj.toArray());

		for (OrderExprotDto orderExprotDto : list) {

			if (orderExprotDto.getStatue().equals(OrderStatusEnum.UNPAY.value() + "")) {
				orderExprotDto.setStatue("未付款");
			}
			if (orderExprotDto.getStatue().equals(OrderStatusEnum.PADYED.value() + "")) {
				orderExprotDto.setStatue("待发货");
			}
			if (orderExprotDto.getStatue().equals(OrderStatusEnum.CONSIGNMENT.value() + "")) {
				orderExprotDto.setStatue("已发货");
			}
			if (orderExprotDto.getStatue().equals(OrderStatusEnum.SUCCESS.value() + "")) {
				orderExprotDto.setStatue("交易成功");
			}
			if (orderExprotDto.getStatue().equals(OrderStatusEnum.CLOSE.value() + "")) {
				orderExprotDto.setStatue("交易失败");
			}

			if ("1".equals(orderExprotDto.getIsPayed())) {
				orderExprotDto.setIsPayed("已付款");
			} else {
				orderExprotDto.setIsPayed("未付款");
			}

			//处理退款状态: 0:退款处理中 1:退款成功 -1:退款失败
			if (RefundHandleStatusEnum.SUCCESS.value().equals(orderExprotDto.getRefundSts1()) || RefundHandleStatusEnum.SUCCESS.value().equals(orderExprotDto.getRefundSts2())) {
				orderExprotDto.setRefundSts1("退款成功");
				orderExprotDto.setRefundSts2(null);
			} else if (RefundHandleStatusEnum.PROCESSING.value().equals(orderExprotDto.getRefundSts1()) || RefundHandleStatusEnum.PROCESSING.value().equals(orderExprotDto.getRefundSts2())) {
				orderExprotDto.setRefundSts1("退款处理中");
				orderExprotDto.setRefundSts2(null);
			} else if (RefundHandleStatusEnum.FAILED.value().equals(orderExprotDto.getRefundSts1()) || RefundHandleStatusEnum.FAILED.value().equals(orderExprotDto.getRefundSts2())) {
				orderExprotDto.setRefundSts1("退款失败");
				orderExprotDto.setRefundSts2(null);
			} else if (orderExprotDto.getRefundSts1() == null){
				orderExprotDto.setRefundSts1("未退款");
				orderExprotDto.setRefundSts2(null);
			}

			if(orderExprotDto.getSubType().equals(SubTypeEnum.MERGE_GROUP.value())) {
				orderExprotDto.setSubType("拼团订单");
			}else if(orderExprotDto.getSubType().equals(SubTypeEnum.group.value())) {
				orderExprotDto.setSubType("团购订单");
			}else if(orderExprotDto.getSubType().equals(SubTypeEnum.auctions.value())) {
				orderExprotDto.setSubType("拍卖订单");
			}else if(orderExprotDto.getSubType().equals(SubTypeEnum.PRESELL.value())) {
				orderExprotDto.setSubType("预售订单");
			}else if(orderExprotDto.getSubType().equals(SubTypeEnum.SHOP_STORE.value())) {
				orderExprotDto.setSubType("门店订单");
			}else if(orderExprotDto.getSubType().equals(SubTypeEnum.SECKILL.value())) {
				orderExprotDto.setSubType("秒杀订单");
			}else {
				orderExprotDto.setSubType("普通订单");
			}

			if(String.valueOf(PayMannerEnum.DELIVERY_ON_PAY.value()).equals(orderExprotDto.getPayManner())) {
				orderExprotDto.setPayManner("货到付款");
			}else if(String.valueOf(PayMannerEnum.ONLINE_PAY.value()).equals(orderExprotDto.getPayManner())) {
				orderExprotDto.setPayManner("在线支付");
			}else if(String.valueOf(PayMannerEnum.SHOP_STORE_PAY.value()).equals(orderExprotDto.getPayManner())){
				orderExprotDto.setPayManner("门店支付");
			}

			if(String.valueOf(PayPctTypeEnum.FULL_PAY.value()).equals(orderExprotDto.getPayPctType())) {//全额支付
				orderExprotDto.setPayPctType("全额支付");
				orderExprotDto.setIsPayFinal("无需支付尾款");
			}else if(String.valueOf(PayPctTypeEnum.FRONT_MONEY.value()).equals(orderExprotDto.getPayPctType()) ||
					String.valueOf(PayPctTypeEnum.FINAL_MONEY.value()).equals(orderExprotDto.getPayPctType())){
				orderExprotDto.setPayPctType("订金支付");
			}

			if("1".equals(orderExprotDto.getIsPayFinal())) {
				orderExprotDto.setIsPayFinal("已付款");
			} else if("0".equals(orderExprotDto.getIsPayFinal())){
				orderExprotDto.setIsPayFinal("未付款");
			}

			if (orderExprotDto.getAttr() != null) {
				orderExprotDto.setAttr(orderExprotDto.getAttr().replace(";", "\n"));
			}
			if (orderExprotDto.getRemark() == null) {
				orderExprotDto.setRemark("无");
			}

			if (AppUtils.isNotBlank(orderExprotDto.getDvyType())) {

				if (orderExprotDto.getDvyType().equals(FreightModeEnum.TRANSPORT_EMS.value())) {
					orderExprotDto.setDvyType("EMS");
				}else if(orderExprotDto.getDvyType().equals(FreightModeEnum.TRANSPORT_EXPRESS.value())){
					orderExprotDto.setDvyType("快递");
				}else if(orderExprotDto.getDvyType().equals(FreightModeEnum.TRANSPORT_MAIL.value())){
					orderExprotDto.setDvyType("平邮");
				}
			}

		}
		return list;
	}

	private boolean isInReturnValidPeriod(Long prodId,MySubDto order){
		ProductDetail prod = productService.getProdDetail(prodId);
		Integer days = 0;
		if (AppUtils.isNotBlank(prod)){
			Long categoryId = prod.getCategoryId();
			Category category = categoryService.getCategory(categoryId);
			days = category.getReturnValidPeriod();
		}

		//当分类退换货时间设置为空，获取系统默认设置
		if(days == null || days == 0){
			ConstTable constTable =constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
			OrderSetMgDto orderSetting=null;
			if(AppUtils.isNotBlank(constTable)){
				String setting=constTable.getValue();
				orderSetting= JSONUtil.getObject(setting, OrderSetMgDto.class);
			}
			days = Integer.parseInt(orderSetting.getAuto_product_return());
		}
		Date finallyDate = order.getFinallyDate();
		if(null == finallyDate){//说明订单还未完成，默认有效
			return true;
		}
		return isInDates(finallyDate, days);
	}

	/**
	 * 代替 com.legendshop.util.DateUtils#isInDates(java.util.Date, int) 的方法，判断当前时间是否在范围之内
	 * @param date		时间
	 * @param days		偏移天数
	 * @return
	 */
	private static boolean isInDates(Date date, long days) {
		long ms = days * 24 * 3600 * 1000;
		long times = date.getTime() + ms;
		long now = (new Date()).getTime();
		return times > now;
	}
}
