package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.TagItemProd;

public class TagItemProdComparer implements DataComparer<TagItemProd, TagItemProd> {

	@Override
	public boolean needUpdate(TagItemProd dto, TagItemProd dbObj, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(TagItemProd dto, TagItemProd dbObj) {
		if(dto==null && dbObj==null){
		    return true;	
		}
		if(dto==null || dbObj==null){
			return false;
		}
		if(dto.getProdId().equals(dbObj.getProdId()) && dto.getTagItemId().equals(dbObj.getTagItemId())){
			return true;
		}
		return false;
	}

	@Override
	public TagItemProd copyProperties(TagItemProd dtoj, Object obj) {
		return dtoj;
	}


}
