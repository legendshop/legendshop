/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.VisitLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 
 * 访问历史Dao实现类
 */
@Repository
public class VisitLogDaoImpl extends GenericDaoImpl<VisitLog, Long> implements VisitLogDao {

	/** The log. */
	private static String SQL_FOR_VISIT_LOG = "SELECT v.visit_id AS visitId, v.ip, v.country, v.area, v.user_name AS userName, v.shop_name AS shopName, v.product_id AS productId, v.product_name AS productName, v.pic AS pic, v.price AS price, v.page, v.rec_date AS recDate, v.visit_num AS visitNum, v.shop_id AS shopId, v.source AS source, v.is_user_del AS isUserDel  FROM ls_vit_log v ";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.VisitLogDao#updateVisitLog(com.legendshop
	 * .model.entity.VisitLog)
	 */
	@Override
	public void updateVisitLog(VisitLog visitLog) {
		update(visitLog);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.VisitLogDao#deleteVisitLogById(java.lang.
	 * Long)
	 */
	@Override
	public void deleteVisitLogById(Long id) {
		deleteById(id);

	}

	/**
	 * 获取半个小时内的首页的访问历史
	 */
	@Override
	public VisitLog getVisitedIndexLog(VisitLog visitLog) {
		// 30分钟前
		Date date = DateUtil.add(new Date(), Calendar.MINUTE, -30);
		List<VisitLog> list = null;
		if (visitLog.getUserName() != null) {
			// 用户已经登录
			list = queryLimit(SQL_FOR_VISIT_LOG + " where v.ip = ? and v.shop_name = ? and v.user_name = ?  and v.rec_date > ?", VisitLog.class, 0, 1,
					visitLog.getIp(), visitLog.getShopName(), visitLog.getUserName(), date);
		} else {
			// 用户尚未登录
			list = queryLimit(SQL_FOR_VISIT_LOG + "where v.user_name is null and v.ip = ? and v.shop_name = ?  and v.rec_date > ?", VisitLog.class, 0, 1,
					visitLog.getIp(), visitLog.getShopName(), date);
		}

		if (AppUtils.isNotBlank(list)) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 获取半个小时内的商品的访问历史
	 */
	@Override
	public VisitLog getVisitedProdLog(VisitLog visitLog) {
		List<VisitLog> list = null;
		Date date = DateUtil.add(new Date(), Calendar.MINUTE, -30);
		if (visitLog.getUserName() != null) {
			// 用户已经登录
			list = queryLimit(SQL_FOR_VISIT_LOG + "where v.ip = ? and v.user_name = ? and v.product_id = ?  and v.rec_date > ?", VisitLog.class, 0, 1,
					visitLog.getIp(), visitLog.getUserName(), visitLog.getProductId(), date);
		} else {
			// 用户尚未登录
			list = queryLimit(SQL_FOR_VISIT_LOG + "where  v.user_name is null and v.ip = ? and v.product_id = ?  and v.rec_date > ?", VisitLog.class, 0, 1,
					visitLog.getIp(), visitLog.getProductId(), date);
		}

		if (AppUtils.isNotBlank(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public VisitLog getVisitLogById(Long id) {
		return getById(id);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogListPage(String curPageNO, String userName, VisitLog visitLog) throws ParseException {
		CriteriaQuery cq = new CriteriaQuery(VisitLog.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("userName", userName);
		cq.ge("date", visitLog.getStartTime());
		// 获取结束时间的年 月 日
		Date endDate = visitLog.getEndTime();
		if (AppUtils.isNotBlank(endDate)) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			endDate = dateFormat.parse(year + "-" + month + "-" + day + " 23:59:59");
		}
		cq.le("date", endDate);
		cq.eq("page", visitLog.getPage());

		if (AppUtils.isNotBlank(visitLog.getProductName())) {
			cq.like("productName", "%" + visitLog.getProductName() + "%");
		}

		cq.addOrder("desc", "date");
		return queryPage(cq);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogListPage(String curPageNO, VisitLog visitLog, DataSortResult result, Integer pageSize) {
		CriteriaQuery cq = new CriteriaQuery(VisitLog.class, curPageNO);

		if (!AppUtils.isBlank(visitLog.getShopId())) {
			cq.eq("shopId",visitLog.getShopId());
		}
		if (!AppUtils.isBlank(visitLog.getShopName())) {
			cq.like("shopName", "%" + StringUtils.trim(visitLog.getShopName()) + "%");
		}

		cq.ge("date", visitLog.getStartTime());
		if (AppUtils.isNotBlank(visitLog.getEndTime())) {
			Calendar c = Calendar.getInstance();
			c.setTime(visitLog.getEndTime());
			c.add(Calendar.DAY_OF_MONTH, 1);
			cq.le("date", c.getTime());
		}
		cq.eq("page", visitLog.getPage());
		if (AppUtils.isNotBlank(visitLog.getUserName())) {
			cq.like("userName","%" + visitLog.getUserName().trim() + "%");
		}

		if (AppUtils.isNotBlank(visitLog.getProductName())) {
			cq.like("productName", "%" + StringUtils.trim(visitLog.getProductName())  + "%");
		}
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			cq.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			cq.addOrder("desc", "date");
		} else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		return queryPage(cq);
	}

	/**
	 * 根据用户名获取浏览历史统计数量
	 */
	@Override
	public Long getVisitLogCountByUserName(String userName) {

		Date nowDate = new Date();
		Date date = DateUtils.rollDay(nowDate, -30);

		String sql = "SELECT COUNT(l.visit_id) FROM ls_vit_log l WHERE l.page=1 AND l.user_name = ? AND l.is_user_del = ? AND l.rec_date > ? " ;
		return get(sql, Long.class, userName, false,date);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogListPageForApp(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(VisitLog.class, curPageNO);
		query.setPageSize(10);
		QueryMap map = new QueryMap();
		map.put("page", 1);
		map.put("userName", userName);
		map.put("isUserDel", false);
		Date date = DateUtils.rollDay(new Date(), -30);
		map.put("date", date);

		String querySQL = ConfigCode.getInstance().getCode("uc.vit.getVisitLogListPage", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.vit.getVisitLogListPageCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryAllSQL);

		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public int updateIsUserDelBy(String userName, boolean isUserDel) {
		
		String sql = "UPDATE ls_vit_log SET is_user_del = ? WHERE user_name = ?";
		
		return this.update(sql, isUserDel, userName);
	}

}
