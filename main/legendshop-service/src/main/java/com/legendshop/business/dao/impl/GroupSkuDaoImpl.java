/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.GroupSku;
import com.legendshop.business.dao.GroupSkuDao;
import org.springframework.stereotype.Repository;

/**
 * The Class GroupSkuDaoImpl. 团购活动参加商品Dao实现类
 */
@Repository("groupSkuDao")
public class GroupSkuDaoImpl extends GenericDaoImpl<GroupSku, Long> implements GroupSkuDao{

	/**
	 * 根据Id获取团购活动参加商品
	 */
	public GroupSku getGroupSku(Long id){
		return getById(id);
	}

	/**
	 *  删除团购活动参加商品
	 *  @param groupSku 实体类
	 *  @return 删除结果
	 */	
    public int deleteGroupSku(GroupSku groupSku){
    	return delete(groupSku);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGroupSku(Long id){
		return deleteById(id);
	}

	/**
	 * 保存团购活动参加商品
	 */
	public Long saveGroupSku(GroupSku groupSku){
		return save(groupSku);
	}

	/**
	 *  更新团购活动参加商品
	 */		
	public int updateGroupSku(GroupSku groupSku){
		return update(groupSku);
	}

	/**
	 * 分页查询团购活动参加商品列表
	 */
	public PageSupport<GroupSku>queryGroupSku(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GroupSku.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public void deleteGroupSkuByGroupId(Long id) {
		String sql = "DELETE FROM ls_group_sku WHERE group_id=?";
		update(sql,id);
	}

	@Override
	public Double getGropPrice(Long id, Long prodId, Long skuId) {
		String sql = "SELECT group_price FROM ls_group_sku WHERE group_id=? AND prod_id=? AND sku_id=?";
		return get(sql,Double.class,id,prodId,skuId);
	}

}
