/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.SubFloor;

/**
 * The Class SubFloorDao.
 */

public interface SubFloorDao extends GenericDao<SubFloor, Long>{
     
    public abstract List<SubFloor> getSubFloorByShopId();

	public abstract SubFloor getSubFloor(Long id);
	
    public abstract void deleteSubFloor(SubFloor subFloor);
	
	public abstract Long saveSubFloor(SubFloor subFloor);
	
	public abstract void updateSubFloor(SubFloor subFloor);
	
	public abstract PageSupport getSubFloor(CriteriaQuery cq);
	
	public abstract List<SubFloor> getAllSubFloor(List<Floor> floorList);
	
	public abstract List<SubFloor> getSubFloorByfId(Long id);

	public abstract boolean checkPrivilege(String userName, Long floorId);
	
	public abstract List<SubFloor> getSubFloorAndProduct();
	
	public abstract List<SubFloor> getSubFloorAndProduct(Long floorId);
	
	public abstract void deleteSubFloorItems(Long subFloorId);

	public abstract PageSupport<SubFloor> getSubFloorPage(String curPageNO, SubFloor subFloor);
 }
