/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ScheduleListDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.ScheduleStatusEnum;
import com.legendshop.model.entity.ScheduleList;


/**
 *   动态的任务调度进程列表
 */
@Repository
public class ScheduleListDaoImpl extends GenericDaoImpl<ScheduleList, Long> implements ScheduleListDao {

	@Override
	public  List<ScheduleList> getNonexecutionScheduleList(Date triggerTime, Integer scheduleType) {
		return this.queryByProperties(new EntityCriterion().le("triggerTime", triggerTime).eq("scheduleType", scheduleType)
				.eq("status", ScheduleStatusEnum.NONEXECUTION.value()));
	}

	@Override
	public int deleteScheduleList(ScheduleList ScheduleList) {
		return delete(ScheduleList);
	}

	@Override
	public List<Long> saveScheduleList(List<ScheduleList> scheduleLists) {
		return save(scheduleLists);
	}

	@Override
	public List<ScheduleList> getScheduleList(Long shopId, String scheduleSn, Integer scheduleType) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("scheduleSn", scheduleSn).eq("scheduleType", scheduleType));
	}

	@Override
	public Long saveScheduleList(ScheduleList scheduleList) {
		return save(scheduleList);
	}

	@Override
	public int updateScheduleListStatus(Long shopId, int status, Long id) {
		String sql = "UPDATE ls_schedule_list SET status = ? WHERE id = ? AND shop_id=? ";
		return this.update(sql, status, id,shopId);
	}

	@Override
	public void cancelScheduleList(Long shopId, String schedule_sn, Integer schedule_type) {
		String sql = "UPDATE ls_schedule_list SET status = ? WHERE status= ? AND shop_id = ? AND schedule_sn=? AND schedule_type=? ";
		this.update(sql, ScheduleStatusEnum.LOSE_EFFICACY.value(),ScheduleStatusEnum.NONEXECUTION.value(), shopId,schedule_sn,schedule_type);
	}

}
