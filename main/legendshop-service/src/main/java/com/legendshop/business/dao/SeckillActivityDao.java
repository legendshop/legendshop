/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.AppSecKillActivityQueryDTO;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * 秒杀活动Dao.
 */
public interface SeckillActivityDao extends GenericDao<SeckillActivity, Long> {

	/** 获取秒杀活动数据 */
	public abstract SeckillActivity getSeckillActivity(Long id);

	/** 删除秒杀活动数据 */
	public abstract int deleteSeckillActivity(SeckillActivity seckillActivity);

	/** 保存秒杀活动商品 */
	public abstract Long saveSeckillActivity(SeckillActivity seckillActivity);

	/** 更新秒杀活动商品 */
	public abstract int updateSeckillActivity(SeckillActivity seckillActivity);

	/** 获取秒杀活动详情 */
	public abstract SeckillActivity getSeckillDetail(Long seckillId);

	/** 执行秒杀 */
	int executeSeckill(long seckillId, long prodId, long skuId, String userId, Double price, int sckNumber);

	/** 查询所有在线的秒杀活动 */
	public abstract List<Long> findOnActive();

	/** 获取秒杀成功记录 */
	public abstract SeckillSuccess getSeckillSucess(String userId, Long seckillId, long prodId, long skuId);

	/** 删除秒杀记录 */
	public abstract void deleteRecord(Long id, String userId);

	/** 查找拍卖列表 */
	public abstract PageSupport<SeckillActivity> queryAuctionList(String startTime);

	/** 查找拍卖列表 */
	PageSupport<SeckillActivity> queryAuctionList(AppSecKillActivityQueryDTO queryDTO);

	/** 查询秒杀成功的记录信息  */
	public abstract PageSupport<SeckillSuccessRecord> querySimplePage(String curPageNO, String userId);

	/** 查找拍卖列表 */
	public abstract PageSupport<SeckillActivity> queryAuctionList(String curPageNO, int pageSize, SeckillActivity seckillActivity);

	/** 获取秒杀活动数据  */
	public abstract PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, Long shopId, int pageSize, SeckillActivity seckillActivity);

	/** 查找拍卖列表 */
	public abstract PageSupport<SeckillActivity> queryAuctionList(String curPageNO, String startTime);

	/** 获取秒杀活动列表  */
	public abstract PageSupport<SeckillActivity> queryFrontSeckillList(String curPageNO, String startTime);

	/** 查询用户秒杀记录 */ 
	public abstract PageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId);

	public abstract long findSeckillSuccess(Long seckillId, long prodId, long skuId, String userId);

	public abstract boolean findIfJoinSeckill(Long productId);

	/**
	 * 获取秒杀活动
	 * @param shopId
	 * @param activeId
	 * @return
	 */
	public abstract SeckillActivity getSeckillActivityByShopIdAndActiveId(Long shopId, Integer activeId);

	/**
	 * 获取过期未完成的秒杀活动
	 * @param now
	 * @return
	 */
	public abstract List<SeckillActivity> getSeckillActivityByTime(Date now);
	
	/**
	 * 根据秒杀活动id查询秒杀成功的记录信息
	 * @param curPageNO
	 * @param id
	 * @return
	 */
	public abstract PageSupport<SeckillSuccess> querySeckillRecordListPage(String curPageNO, Long id);
	
	public abstract List<SeckillSuccess> getSeckillSucessBySeckillId(Long seckillId);

	/**
	 * 获取秒杀运营数据
	 * @param id
	 * @return
	 */
    OperateStatisticsDTO getOperateStatistics(Long id);

	/**
	 *获取参与人数
	 * @param id
	 * @return
	 */
	Integer getPersonCount(Long id);

	/**
	 * 通过商品获取秒杀
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	SeckillActivity getSeckillByProd(Long prodId, Long skuId);
}
