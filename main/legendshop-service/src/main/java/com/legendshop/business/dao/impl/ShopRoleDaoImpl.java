/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopRoleDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopRole;

/**
 *商家角色表Dao
 */
@Repository
public class ShopRoleDaoImpl extends GenericDaoImpl<ShopRole, Long> implements ShopRoleDao  {

	public ShopRole getShopRole(Long id){
		return getById(id);
	}
	
    public int deleteShopRole(ShopRole shopRole){
    	return delete(shopRole);
    }
	
	public Long saveShopRole(ShopRole shopRole){
		return save(shopRole);
	}
	
	public int updateShopRole(ShopRole shopRole){
		return update(shopRole);
	}

	@Override
	public ShopRole getShopRole(Long shopId, String name) {
		return this.getByProperties(new EntityCriterion().eq("shopId", shopId).eq("name", name));
	}

	@Override
	public List<ShopRole> getShopRolesByShopId(Long shopId) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
	}

	@Override
	public PageSupport<ShopRole> getShopRolePage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ShopRole.class, curPageNO);
        cq.setPageSize(20);
        cq.eq("shopId", shopId);
        cq.addDescOrder("recDate");
		return queryPage(cq);
	}
	
 }
