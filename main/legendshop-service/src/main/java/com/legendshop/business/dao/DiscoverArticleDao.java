/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.model.entity.ProdGroup;

/**
 * The Class DiscoverArticleDao. 发现文章表Dao接口
 */
public interface DiscoverArticleDao extends Dao<DiscoverArticle,Long>{

	/**
	 * 根据Id获取发现文章表
	 */
	public abstract DiscoverArticle getDiscoverArticle(Long id);

	/**
	 *  根据Id删除发现文章表
	 */
    public abstract int deleteDiscoverArticle(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteDiscoverArticle(DiscoverArticle discoverArticle);

	/**
	 * 保存发现文章表
	 */
	public abstract Long saveDiscoverArticle(DiscoverArticle discoverArticle);

	/**
	 *  更新发现文章表
	 */		
	public abstract int updateDiscoverArticle(DiscoverArticle discoverArticle);

	/**
	 * 分页查询发现文章表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<DiscoverArticle> queryDiscoverArticle(String curPageNO, Integer pageSize,String condition,String order);
	
	 /**
  	 *  分页查询通过标题列表
  	 */	
	public abstract PageSupport<DiscoverArticle> queryDiscoverArticlePage(String curPageNO, DiscoverArticle discoverArticle,Integer pageSize);
	
}
