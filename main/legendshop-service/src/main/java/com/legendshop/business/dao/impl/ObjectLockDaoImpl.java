/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ObjectLockDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.constant.ObjectLockEnum;
import com.legendshop.model.entity.ObjectLock;

/**
 * 对象锁实现类
 */
@Repository
public class ObjectLockDaoImpl extends GenericDaoImpl<ObjectLock, Long> implements ObjectLockDao  {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(ObjectLockDaoImpl.class);
	
	private final static Object lock = new Object();
	
	/**
	 * 加锁
	 */
	@Override
	public  Long loadDBLock(ObjectLockEnum lockEnum, Long prodId) {
		synchronized (lock) {
			Long id = this.get("select id from ls_object_lock where obj = ? and obj_id = ?", Long.class, lockEnum.value(),prodId);
			if(id == null){
				ObjectLock entity = new ObjectLock();
				entity.setObj(lockEnum.value());
				entity.setObjId(prodId);
				entity.setRecDate(new Date());
				this.save(entity);
			}
			//加行锁
			id = this.get("select id from ls_object_lock where id = ? for update", Long.class, prodId);
			log.info("Get lock {} by lockEnum {}, prodId {}", id, lockEnum.value(), prodId);
			return id;
		}
	}

	/**
	 * 删除锁
	 */
	@Override
	public void releaseDBLock(ObjectLockEnum lockEnum, Long prodId) {
		log.info("Release lock  by lockEnum {}, prodId {}", lockEnum.value(), prodId);
		this.update("delete from ls_object_lock where  obj = ? and obj_id = ?", Long.class, lockEnum.value(),prodId);
	}
	
 }
