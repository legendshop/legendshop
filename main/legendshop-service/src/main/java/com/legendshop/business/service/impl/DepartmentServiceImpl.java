/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DepartmentDao;
import com.legendshop.model.entity.Department;
import com.legendshop.spi.service.DepartmentService;
import com.legendshop.util.AppUtils;

/**
 * 部门服务.
 */
@Service("departmentService")
public class DepartmentServiceImpl  implements DepartmentService{
	
	@Autowired
    private DepartmentDao departmentDao;

    public Department getDepartment(Long id) {
        return departmentDao.getDepartment(id);
    }

    public void deleteDepartment(Department department) {
        departmentDao.deleteDepartment(department);
    }

    public Long saveDepartment(Department department) {
        if (!AppUtils.isBlank(department.getId())) {
            updateDepartment(department);
            return department.getId();
        }
        return departmentDao.saveDepartment(department);
    }

    public void updateDepartment(Department department) {
        departmentDao.updateDepartment(department);
    }

	@Override
	public List<Department> getAllDepartment() {
		return departmentDao.getAllDepartment();
	}

	@Override
	public String getDepartmentName(Long deptId) {
		return departmentDao.getDepartmentName(deptId);
	}
}
