/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailLog;

/**
 * 邮件发送历史Dao.
 */
public interface MailLogDao extends GenericDao<MailLog, Long>{
     
	public abstract MailLog getMailLog(Long id);
	
    public abstract void deleteMailLog(MailLog mailLog);
	
	public abstract Long saveMailLog(MailLog mailLog);
	
	public abstract void updateMailLog(MailLog mailLog);
	
	public abstract PageSupport<MailLog> getMailLogPage(String curPageNO, MailLog mailLog);
	
 }
