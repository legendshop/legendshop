/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShippingProdDao;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.spi.service.ShippingProdService;
import com.legendshop.util.AppUtils;

/**
 * 包邮活动商品Service
 */
@Service("shippingProdService")
public class ShippingProdServiceImpl  implements ShippingProdService{

	@Autowired
    private ShippingProdDao shippingProdDao;

    public ShippingProd getShippingProd(Long id) {
        return shippingProdDao.getShippingProd(id);
    }

    public void deleteShippingProd(ShippingProd shippingProd) {
        shippingProdDao.deleteShippingProd(shippingProd);
    }

    public Long saveShippingProd(ShippingProd shippingProd) {
        if (!AppUtils.isBlank(shippingProd.getId())) {
            updateShippingProd(shippingProd);
            return shippingProd.getId();
        }
        return shippingProdDao.saveShippingProd(shippingProd);
    }

    public void updateShippingProd(ShippingProd shippingProd) {
        shippingProdDao.updateShippingProd(shippingProd);
    }

	@Override
	public List<FullActiveProdDto> getActiveProdByActiveId(Long id, Long shopId) {
		return shippingProdDao.getFullActiveProdDtoByActiveId(shopId,id);
	}

	@Override
	public List<ShippingProd> getProdByActiveId(Long id, Long shopId) {
		return shippingProdDao.getActiveProdByActiveId(id,shopId);
	}
}
