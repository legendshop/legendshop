/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserManagerDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.User;
import com.legendshop.util.AppUtils;

/**
 * 针对User的Dao
 */
@Repository
public class UserManagerDaoImpl extends GenericDaoImpl<User, String> implements UserManagerDao {
	
	public  String saveUser(User user){
		return save(user);
	}

	@Override
	public User getUser(String userId) {
		return getById(userId);
	}

	@Override
	public void updateUser(User user) {
		update(user);
	}

	@Override
	public void deleteUser(User user) {
		delete(user);
	}

	@Override
	public void deleteUseById(String userId) {
		deleteById(userId);
	}

	/**
	 * 用户是否存在
	 */
	@Override
	public boolean isUserExists(String userName) {
		List<String> list = this.query("select 1 from ls_usr_detail where user_name = ?", String.class, userName);
		return AppUtils.isNotBlank(list);
	}

	@Override
	public User getUserByName(String userName) {
		return get("select id,name,password,enabled,note,dept_id,d.nick_name as nickName,d.shop_id as shopId from ls_user u,ls_usr_detail d where d.user_name = u.name and d.user_name = ?",User.class,userName);
	}
	
	/**
	 * 更改用户密码,密码需要Md5加盐加密
	 * 
	 */
	@Override
	public void updateUserPassword(User user) {
		update(user);
	}

	@Override
	public PageSupport<User> queryUser(String curPageNO, String name, String enabled,Integer pageSize) {
		CriteriaQuery cq = new CriteriaQuery(User.class, curPageNO);
		cq.setPageSize(pageSize);
		if (AppUtils.isNotBlank(name)) {
			cq.like("name", name + "%");// 1
		}
		if (AppUtils.isNotBlank(enabled)) {
			cq.eq("enabled", enabled);
		}
		return queryPage(cq);
	}

	@Override
	public PageSupport<User> queryUser(String curPageNO, String name, String enabled, String mobile, Integer pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(User.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(name)){
			map.like("name", name.trim());
		}
		if(AppUtils.isNotBlank(enabled)){
			map.put("enabled", enabled);
		}
		if(AppUtils.isNotBlank(mobile)){
			map.like("userMobile", mobile.trim());
		}
		String querySQL = ConfigCode.getInstance().getCode("mysql.queryUser", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("mysql.queryUserCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
}
