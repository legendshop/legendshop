/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.TagItemNews;

/**
 *标签里的文章Dao
 */
public interface TagItemNewsDao extends Dao<TagItemNews, Long> {
     
	public abstract TagItemNews getTagItemNews(Long id);
	
    public abstract int deleteTagItemNews(TagItemNews tagItemNews);
	
	public abstract Long saveTagItemNews(TagItemNews tagItemNews);
	
	public abstract int updateTagItemNews(TagItemNews tagItemNews);
	
	public abstract void saveItemList(List<TagItemNews> itemNewsList);

	public abstract List<TagItemNews> getTagItemNewsByNewsId(Long newsId);
	
	public abstract int deleteTagItemNews(List<TagItemNews> itemNewsList);
	
 }
