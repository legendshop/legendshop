/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.Date;
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.marketing.PurchaseProdDto;
import com.legendshop.model.entity.Marketing;

/**
 * 营销活动表服务类
 */
public interface MarketingDao extends GenericDao<Marketing, Long> {
     
	public abstract Marketing getMarketing(Long id);
	
    public abstract int deleteMarketing(Marketing marketing);
	
	public abstract Long saveMarketing(Marketing marketing);
	
	public abstract int updateMarketing(Marketing marketing);
	
	public abstract Marketing getMarketing(Long shopId, Long id);
	
	public abstract PageSupport simpleSqlQuery(SimpleSqlQuery query);

	public abstract PageSupport<Marketing> getMarketingPage(String curPageNO, Long shopId, Marketing marketing);

	public abstract PageSupport<Marketing> getMarketingPage(String curPageNO, Marketing marketing);

	public abstract PageSupport<Marketing> getAssemblefullmail(String curPageNO, Marketing marketing);

	public abstract PageSupport<Marketing> getShopMarketingMansong(String curPageNO, Marketing marketing,
			DataSortResult result,Integer pageSize);

	public abstract PageSupport<Marketing> getShopMatketingZhekou(String curPageNO, Marketing marketing,
			Integer pageSize, DataSortResult result);

	public abstract List<PurchaseProdDto> getMarketingList(Date startTime, Date endTime);

	/**
	 * 查找时间重叠的满折活动
	 * 三种情况：
	 * a、开始时间 在     startTime  和  endTime 之间
	 * b、开始时间少于  startTime 且结束时间大于 endTime
	 * c、结束时间在  startTime 和 endTime之间
	 */
	public abstract List<Marketing> findOngoingMarketing(Long shopId, Integer isAllProds, Date startTime, Date endTime);

	public abstract int batchDetele(String marketIds);

	public abstract void updateMarketStatus(List<Long> ids);
	
	public abstract int getAllProdsMarketing(Long shopId, Integer type, Date startTime, Date endTime);

	/**
	 * 获取已过期的促销活动
	 * @param commitInteval 获取条数
	 * @param endTime 过期时间
	 * @return List<Marketing>
	 */
	public abstract List<Marketing> getExpiredMarketing(Integer commitInteval, Date endTime);
}
