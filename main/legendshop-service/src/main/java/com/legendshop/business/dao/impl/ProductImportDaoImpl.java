/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProductImportDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductImport;

/**
 * 商品导入Dao
 */
@Repository
public class ProductImportDaoImpl extends GenericDaoImpl<ProductImport, Long> implements ProductImportDao  {

	public ProductImport getProductImport(Long id){
		return getById(id);
	}
	
    public int deleteProductImport(ProductImport productImport){
    	return delete(productImport);
    }
	
	public Long saveProductImport(ProductImport productImport){
		return save(productImport);
	}
	
	public int updateProductImport(ProductImport productImport){
		return update(productImport);
	}

	@Override
	public void saveProductImport(List<ProductImport> importList) {
		save(importList);
	}

	@Override
	public void deleyeProductImport(List<Long> ids) {
		deleteById(ids);
		
	}

	@Override
	public void deleteProductImportById(Long impId) {
		this.deleteById(impId);
	}

	@Override
	public PageSupport<ProductImport> getProductImportPage(String curPageNO, ProductImport productImport, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProductImport.class,curPageNO);
	 	cq.like("prodName",productImport.getProdName(),MatchMode.ANYWHERE );
	 	cq.eq("shopId", shopId);
	 	cq.setPageSize(12);
		return queryPage(cq);
	}

		
	}




 
