/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import com.legendshop.model.constant.SmsTemplateTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.SMSLogDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.entity.SMSLog;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.util.AppUtils;

/**
 * 短信记录服务
 */
@Service("smsLogService")
public class SMSLogServiceImpl  implements SMSLogService{

	@Autowired
    private SMSLogDao smsLogDao;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
    public SMSLog getSmsLog(Long id) {
        return smsLogDao.getSmsLog(id);
    }

    public void deleteSMSLog(SMSLog sMSLog) {
        smsLogDao.deleteSMSLog(sMSLog);
    }

    public Long saveSMSLog(SMSLog sMSLog) {
        if (!AppUtils.isBlank(sMSLog.getId())) {
            updateSMSLog(sMSLog);
            return sMSLog.getId();
        }
        return (Long) smsLogDao.save(sMSLog);
    }

    public void updateSMSLog(SMSLog sMSLog) {
        smsLogDao.updateSMSLog(sMSLog);
    }

	@Override
	public SMSLog getValidSMSCode(String mobile,  SMSTypeEnum type) {
		return smsLogDao.getValidSMSCode(mobile, type);
	}

	@Override
	public boolean allowToSend(String mobile) {
		return smsLogDao.allowToSend(mobile);
	}


//	/**
//	 * 判断验证码是否正确 ,验证后 验证码不会失效
//	 * @param mobile
//	 * @param verifyCode
//	 */
//	@Override
//	public boolean verifyCode(String mobile, String verifyCode) {
//		return this.verifyCodeAndClear(mobile, verifyCode, SMSTypeEnum.VAL);
//	}

//	/**
//	 * 判断验证码是否正确,验证后 验证码不会失效
//	 * @param mobile
//	 * @param verifyCode
//	 * @param typeEnum
//	 */
//	@Override
//	public boolean verifyCode(String mobile, String verifyCode ,SMSTypeEnum typeEnum) {
//		if(AppUtils.isBlank(verifyCode)) {
//			return false;
//		}
//
//		SMSLog log= smsLogDao.getValidSMSCode(mobile, typeEnum);
//		if(log != null && log.getMobileCode().equals(verifyCode))
//			return true;
//		else
//			return false;
//	}

	/**
	 * 判断验证码是否正确 ，验证成功后使验证码失效
	 * @param mobile
	 * @param verifyCode
	 */
	@Override
	public boolean verifyCodeAndClear(String mobile, String verifyCode) {
		return this.verifyCodeAndClear(mobile, verifyCode, SMSTypeEnum.VAL);
	}

	/**
	 * 判断验证码是否正确 ，验证成功后使验证码失效
	 * @param mobile
	 * @param verifyCode
	 * @param typeEnum
	 */
	@Override
	public boolean verifyCodeAndClear(String mobile, String verifyCode ,SMSTypeEnum typeEnum) {

		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}

		if(AppUtils.isBlank(verifyCode)) {
			return false;
		}

		SMSLog log= smsLogDao.getValidSMSCode(mobile, typeEnum);
		if(log != null && log.getMobileCode().equals(verifyCode)){
			smsLogDao.clearValidSMSCode(mobile, typeEnum);
			return true;
		}
		else
			return false;
	}




    /**
	 * 提交表单前判断验证码是否正确 ，验证成功后不使验证码失效
	 * @param mobile
	 * @param verifyCode
	 * @param typeEnum
	 */
	@Override
	public boolean verifyCodeBeforeSubmit(String mobile, String verifyCode, SMSTypeEnum typeEnum) {
		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}

		if(AppUtils.isBlank(verifyCode)) {
			return false;
		}

		SMSLog log= smsLogDao.getValidSMSCode(mobile, typeEnum);
		if(log != null && log.getMobileCode().equals(verifyCode)){
			return true;
		}
		else
			return false;
	}

	@Override
	public PageSupport<SMSLog> getSMSLogPage(String curPageNO, SMSLog sMSLog) {
		return smsLogDao.getSMSLogPage(curPageNO,sMSLog);
	}


	@Override
	public int getTodayLimit(String mobile) {
		return smsLogDao.getTodayLimit(mobile);
	}

	@Override
	public boolean allowToSend(String mobile, Date oneHourAgo, Integer maxSendNum) {
		return smsLogDao.allowToSend(mobile,oneHourAgo,maxSendNum );
	}

	@Override
	public int getUsefulMin() {
		return smsLogDao.getUsefulMin();
	}

    @Override
	public boolean verifyMobileCode(String validateCode, String mobile) {
		return smsLogDao.verifyMobileCode(validateCode,mobile);
	}

    @Override
    public boolean verifyCodeAndClear(String mobile, String verifyCode ,SmsTemplateTypeEnum typeEnum) {
        SMSLog log= smsLogDao.getValidSMSCode(mobile, typeEnum);
        if(log != null && verifyCode.equals(log.getMobileCode())){
            smsLogDao.clearValidSMSCode(mobile, typeEnum);
            return true;
        }
        else
            return false;
    }

}
