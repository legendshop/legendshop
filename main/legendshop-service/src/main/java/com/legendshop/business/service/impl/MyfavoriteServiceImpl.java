package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MyfavoriteDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Myfavorite;
import com.legendshop.spi.service.MyfavoriteService;
import com.legendshop.util.AppUtils;

/**
 * 我的商品收藏
 *
 */
@Service("myfavoriteService")
public class MyfavoriteServiceImpl  implements MyfavoriteService{
	
	@Autowired
	private MyfavoriteDao myfavoriteDao;
	
	/* (non-Javadoc)
	 * @see com.legendshop.usercenter.service.UserCenterService#deleteFavs(java.lang.String)
	 */
	public void deleteFavs(String userId, String selectedFavs) {
		if (AppUtils.isBlank(selectedFavs)) {
			return;
		}
		String[] favIds = selectedFavs.split(";");
		if(AppUtils.isNotBlank(favIds)){
			myfavoriteDao.deleteFavorite(userId, favIds);
		}
	}

	
	/* (non-Javadoc)
	 * @see com.legendshop.usercenter.service.UserCenterService#deleteAllFavs(java.lang.String)
	 */
	public void deleteAllFavs(String userId) {
		myfavoriteDao.deleteFavoriteByUserId(userId);
	}

	public void saveFavorite(Myfavorite myfavorite) {
		myfavoriteDao.saveFavorite(myfavorite);
	}
	
	public Boolean isExistsFavorite(Long prodId,String userName) {
		return myfavoriteDao.isExistsFavorite(prodId,userName);
	}

	public Long getfavoriteLength(String userId) {
		return myfavoriteDao.getfavoriteLength(userId);
	}

	/**
	 * @Description: TODO
	 * @param @param userName
	 * @param @return   
	 * @date 2016-6-21
	 */
	@Override
	public Long getCouponLength(String userName) {
		return myfavoriteDao.getCouponLength(userName);
	}

	/**
	 * @param @param userName
	 * @param @param paymentPassword   
	 * @date 2016-7-5
	 */
	@Override
	public void updatePaymentPassword(String userName, String paymentPassword) {
		myfavoriteDao.updatePaymentPassword(userName, paymentPassword);
	}
	
	@Override
	public PageSupport<Myfavorite> collect(String curPageNO,Integer pageSize, String userId) {
		return myfavoriteDao.collect(curPageNO,pageSize, userId);
	}

	@Override
	public void deleteFav(String userId, Long prodId) {
		myfavoriteDao.deleteFav(userId,prodId);
	}

	
}
