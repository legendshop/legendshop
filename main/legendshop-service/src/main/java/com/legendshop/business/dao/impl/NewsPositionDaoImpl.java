/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.NewsPositionDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.entity.NewsPosition;
/**
 * The Class NewsPositionDaoImpl.
 */
@Repository
public class NewsPositionDaoImpl extends GenericDaoImpl<NewsPosition, Long> implements NewsPositionDao {

	@Override
	@CacheEvict(value = "NewsList", allEntries=true)
	public Long saveNewsPosition(NewsPosition p) {
		return save(p);
	}

	@Override
	public NewsPosition getNewsPositionById(Long id) {
		return getById(id);
	}

	@Override
	public List<NewsPosition> getNewsPositionList() {
		return queryByProperties(new EntityCriterion());
	}

	@Override
	public void delete(Long id) {
		deleteById(id);
	}
	@Override
	public void updateNewsPosition(NewsPosition newsPosition) {
		update(newsPosition);
	}

	@Override
	@Cacheable(value = "NewsList")
	public List<NewsDto> getNewsList() {
		String sqlString="SELECT  p.news as newsId,n.news_title as newsTitle  FROM ls_news_position p LEFT JOIN ls_news n ON p.news=n.news_id WHERE p.position=-1 order by p.seq";
		return  this.query(sqlString, NewsDto.class);
	}

	@Override
	public void saveNewsPositionList(List<NewsPosition> newsPositionlist) {
		save(newsPositionlist);
	}

	@Override
	public List<NewsPosition> getNewsPositionByNewsId(Long newsId) {
		return queryByProperties(new EntityCriterion().eq("newsid", newsId));
	}

	@Override
	public void deleteNewsPositionList(List<NewsPosition> newsPositionlist) {
		this.delete(newsPositionlist);
	}
 }
