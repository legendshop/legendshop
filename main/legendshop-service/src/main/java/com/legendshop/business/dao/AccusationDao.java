/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.AccusationDto;
import com.legendshop.model.entity.Accusation;

/**
 * 举报表Dao.
 */
public interface AccusationDao extends GenericDao<Accusation, Long> {
     
    public abstract List<Accusation> getAccusation(String shopName);

	public abstract Accusation getAccusation(Long id);
	
    public abstract void deleteAccusation(Accusation accusation);
	
	public abstract Long saveAccusation(Accusation accusation);
	
	public abstract void updateAccusation(Accusation accusation);
	
	public abstract Accusation getDetailedAccusation(Long id);

	public abstract List<Accusation> queryUnHandler();
	
	/** 根据 shopId 查找 商家被举报的个数 **/
	public int getAccusationCounts(Long shopId);

	public abstract PageSupport<Accusation> queryReportForbit(String curPageNO, Long shopId);

	public abstract PageSupport<Accusation> queryOverlayLoad(Long id);

	public abstract PageSupport<AccusationDto> querySimplePage(String curPageNO, String userId);

	public abstract PageSupport<Accusation> querySimplePage(Long id, String userId);
 }
