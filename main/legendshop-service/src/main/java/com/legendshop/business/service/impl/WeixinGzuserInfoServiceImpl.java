/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.compare.DataListComparer;
import com.legendshop.business.comparer.WeiXinUsersComparator;
import com.legendshop.business.dao.WeixinGzuserInfoDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.weixin.WeiXinGzUserInfoDto;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.spi.service.WeixinGzuserInfoService;
import com.legendshop.util.AppUtils;

/**
 *微信关注用户服务
 */
@Service("weixinGzuserInfoService")
public class WeixinGzuserInfoServiceImpl  implements WeixinGzuserInfoService{

	@Autowired
    private WeixinGzuserInfoDao weixinGzuserInfoDao;
    
    private DataListComparer<WeiXinGzUserInfoDto,WeixinGzuserInfo> weixinUserListComparer = null;

    @Cacheable(value="WeixinGzuserInfo",key="#fromUserName")
    public WeixinGzuserInfo getWeixinGzuserInfo(String fromUserName) {
        return weixinGzuserInfoDao.getWeixinGzuserInfo(fromUserName);
    }

    public WeixinGzuserInfo getWeixinGzuserInfo(Long id) {
        return weixinGzuserInfoDao.getWeixinGzuserInfo(id);
    }

    public void deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
        weixinGzuserInfoDao.deleteWeixinGzuserInfo(weixinGzuserInfo);
    }

    public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
        if (!AppUtils.isBlank(weixinGzuserInfo.getId())) {
            updateWeixinGzuserInfo(weixinGzuserInfo);
            return weixinGzuserInfo.getId();
        }
        return weixinGzuserInfoDao.saveWeixinGzuserInfo(weixinGzuserInfo);
    }

    public void updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
        weixinGzuserInfoDao.updateWeixinGzuserInfo(weixinGzuserInfo);
    }

	@Override
	public List<WeixinGzuserInfo> getUserByGroupId(Long groupid) {
		return weixinGzuserInfoDao.getUserByGroupId(groupid);
	}

	
	@Override
	public void synchronousUser(List<WeiXinGzUserInfoDto> dtList,List<WeixinGzuserInfo> dbList, Object obj) {
		Map<Integer,List<WeixinGzuserInfo> > map=getWeixinUserListComparer().compare(dtList,dbList , null);
		 List<WeixinGzuserInfo> saveUsers=map.get(1);
		// List<WeixinGzuserInfo> delUsers=map.get(-1);
		 List<WeixinGzuserInfo> updateUsers=map.get(0);
		 if(saveUsers!=null){
			 for(int i =0;i<saveUsers.size();i++){
				 System.out.println(saveUsers.get(i).getNickname());
				 WeixinGzuserInfo info=saveUsers.get(i);
				 info.setAddtime(new Date());
				 info.setNickname(AppUtils.removeYanText(info.getNickname()));
				 weixinGzuserInfoDao.saveWeixinGzuserInfo(info);
			 }
		 }
//TODO 暂时不删除用户
//		 if(delUsers!=null){
//			 for(int i =0;i<delUsers.size();i++){
//				 System.out.println(delUsers.get(i).getNickname());
//				 weixinGzuserInfoDao.deleteWeixinGzuserInfo(delUsers.get(i));
//			 }
//		 }
		 if(updateUsers!=null){
			 for(int i =0;i<updateUsers.size();i++){
				 WeixinGzuserInfo info=updateUsers.get(i);
				 System.out.println(info.getNickname());
				 info.setNickname(AppUtils.removeYanText(info.getNickname()));
				 weixinGzuserInfoDao.updateWeixinGzuserInfo(info);
			 }
		 }
	}
    
	public DataListComparer<WeiXinGzUserInfoDto,WeixinGzuserInfo> getWeixinUserListComparer() {
		if (weixinUserListComparer == null) {
			weixinUserListComparer = new DataListComparer<WeiXinGzUserInfoDto,WeixinGzuserInfo>(new WeiXinUsersComparator());
		}
	    return weixinUserListComparer;
		}

	@Override
	public List<WeixinGzuserInfo> getUserInfo() {
		return weixinGzuserInfoDao.getUserInfo();
	}

	@Override
	public PageSupport<WeixinGzuserInfo> query(String curPageNO, String key) {
		return weixinGzuserInfoDao.querySimplePage(curPageNO,key);
	}

	public void setWeixinGzuserInfoDao(WeixinGzuserInfoDao weixinGzuserInfoDao) {
		this.weixinGzuserInfoDao = weixinGzuserInfoDao;
	}
}
