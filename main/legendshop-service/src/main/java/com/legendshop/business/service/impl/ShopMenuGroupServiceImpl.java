/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.legendshop.model.dto.ShopUrlPermissionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;

import com.legendshop.business.service.security.impl.AuthServiceImpl;
import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.plugins.PluginManager;
import com.legendshop.model.constant.ShopPermFunctionEnum;
import com.legendshop.model.entity.ShopMenuComparator;
import com.legendshop.model.entity.ShopMenuGroupComparator;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.spi.service.ShopMenuGroupService;
import com.legendshop.util.AppUtils;

/**
 * 计算商家菜单服务实现类,此类再xml中定义,无需用annotation,是属于security部分
 */
public class ShopMenuGroupServiceImpl implements ShopMenuGroupService, InitializingBean, BeanFactoryAware {

	/** The log. */
	private Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);

	private BeanFactory beanFactory;

	private List<ShopMenuGroup> shopMenuGroups;

	private List<String> allLinks;

	private PluginManager pluginManager = PluginRepository.getInstance();

	/*
	 * (non-Javadoc)
	 *
	 * @see com.legendshop.spi.service.ShopMenuGroupService#getShopMenuGroup()
	 */
	@Override
	public List<ShopMenuGroup> getShopMenuGroup() {
		return shopMenuGroups;
	}

	@Override
	public List<String> getAllLinks() {
		if(allLinks == null){
			allLinks = parseAllLinks(shopMenuGroups);
		}
		return allLinks;
	}

	/**
	 * 根据请求URL获取权限
	 */
	@Override
	public ShopUrlPermissionDto getShopMenuByURL(String path) {
		ShopUrlPermissionDto shopUrlPermissionDto = new ShopUrlPermissionDto();
		for (ShopMenuGroup group : shopMenuGroups) {
			for (ShopMenu menu : group.getShopMenuList()) {
				for (String checkUrl : menu.getUrlList()) {
					if ("/".equals(checkUrl.substring(checkUrl.length()-1))){
						if (path.contains(checkUrl)) {
							shopUrlPermissionDto.setName(menu.getLabel());
							shopUrlPermissionDto.setType(ShopPermFunctionEnum.CHECK_FUNC.value());
							return shopUrlPermissionDto;
						}
					}else{
						if (path.equals(checkUrl)) {
							shopUrlPermissionDto.setName(menu.getLabel());
							shopUrlPermissionDto.setType(ShopPermFunctionEnum.CHECK_FUNC.value());
							return shopUrlPermissionDto;
						}
					}

					if (AppUtils.isNotBlank(menu.getEditorUrlList())) {
						for (String editorUrl : menu.getEditorUrlList()) {
							if ("/".equals(editorUrl.substring(editorUrl.length()-1))){
								if (path.contains(editorUrl)) {
									shopUrlPermissionDto.setName(menu.getLabel());
									shopUrlPermissionDto.setType(ShopPermFunctionEnum.EDITOR_FUNC.value());
									return shopUrlPermissionDto;
								}
							}else{
								if (path.equals(editorUrl)) {
									shopUrlPermissionDto.setName(menu.getLabel());
									shopUrlPermissionDto.setType(ShopPermFunctionEnum.EDITOR_FUNC.value());
									return shopUrlPermissionDto;
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * 初始化bean的时候执行，可以针对某个具体的bean进行配置。
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {

		if (shopMenuGroups == null && beanFactory.containsBean("shopMenuGroups")) {
			List<ShopMenuGroup> result = (List<ShopMenuGroup>) beanFactory.getBean("shopMenuGroups");
			if (AppUtils.isNotBlank(result)) {
				for (Iterator<ShopMenuGroup> iterator = result.iterator(); iterator.hasNext();) {
					ShopMenuGroup shopMenuGroup = (ShopMenuGroup) iterator.next();
					List<ShopMenu> shopMenuList = shopMenuGroup.getShopMenuList();
					if (AppUtils.isNotBlank(shopMenuList)) {
						for (Iterator<ShopMenu> it = shopMenuList.iterator(); it.hasNext();) {
							ShopMenu shopMenu = (ShopMenu) it.next();
							String pluginId = shopMenu.getPluginId();
							if (pluginId != null && !pluginManager.isPluginRunning(pluginId)) { // 没有查询到该插件
								it.remove();
							}
						}
						if (shopMenuList.size() == 0) {
							iterator.remove();
						}
					}else{
						iterator.remove();//如果没有子菜单就删除
					}
				}
			}

			//子项重新排序
			if (AppUtils.isNotBlank(result)) {
				for (ShopMenuGroup shopMenuGroup : result) {
					Collections.sort(shopMenuGroup.getShopMenuList(), new ShopMenuComparator());
				}
			}

			//重新排序
			if (AppUtils.isNotBlank(result)) {
				Collections.sort(result, new ShopMenuGroupComparator());
			}

			shopMenuGroups = result;
			log.debug("*********init shopMenuGroups********");
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;

	}

	private List<String> parseAllLinks(List<ShopMenuGroup> shopMenuGroups){
		List<String> result = new ArrayList<String>();
		for (ShopMenuGroup group : shopMenuGroups) {
			for (ShopMenu menu : group.getShopMenuList()) {
				for (String url : menu.getUrlList()) {
					result.add(url);
				}
			}
		}
		return result;
	}
	
}
