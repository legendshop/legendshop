/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.DrawActivityDao;
import com.legendshop.business.dao.DrawRecordDao;
import com.legendshop.business.dao.DrawWinRecordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.CouponSourceEnum;
import com.legendshop.model.constant.IntegralLogTypeEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.dto.DrawAwardsDto;
import com.legendshop.model.dto.draw.AjaxJson;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.model.entity.draw.DrawWinRecord;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.vo.DrawRecharge;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.DrawActivityService;
import com.legendshop.spi.service.DrawWinRecordService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 抽奖活动实现类.
 */
@Service("drawActivityService")
public class DrawActivityServiceImpl implements DrawActivityService {
	
	@Autowired
	private DrawActivityDao drawActivityDao;
	
	@Autowired
	private DrawRecordDao drawRecordDao; // 抽奖记录
	
	@Autowired
	private DrawWinRecordDao drawWinRecordDao; // 中奖记录
	
	@Autowired
	private DrawWinRecordService drawWinRecordService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private UserCouponService userCouponService;
	
	@Resource(name="weiXinDrawSuccessProcessor")
	private EventProcessor<DrawRecharge> weiXinDrawSuccessProcessor;

	public List<DrawActivity> getDrawActivity() {
		return drawActivityDao.getDrawActivity();
	}

	public DrawActivity getDrawActivityById(Long id) {
		return drawActivityDao.getDrawActivity(id);
	}

	public void deleteDrawActivity(DrawActivity drawActivity) {
		drawActivityDao.deleteDrawActivity(drawActivity);
		// 删除中奖记录
		drawWinRecordDao.delDrawWinRecordsByActId(drawActivity.getId());
		// 删除抽奖记录
		drawRecordDao.delDrawRecordByActId(drawActivity.getId());
	}

	/**
	 * (non-Javadoc) 保存
	 */
	public Long saveDrawActivity(DrawActivity drawActivity) throws Exception {
		try {
			// 更新活动
			System.out.println(drawActivity.getAwardInfo());

			if (!AppUtils.isBlank(drawActivity.getId())) {
				drawActivityDao.updateDrawActivity(drawActivity);
			} else {
				drawActivity.setActStatus(1);
				Long id = drawActivityDao.saveDrawActivity(drawActivity);
				if (AppUtils.isNotBlank(id)) {
				}
			}

			return drawActivity.getId();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("保存失败！");
		}
	}

	/** 更新 */
	public void updateDrawActivity(DrawActivity drawActivity) {
		drawActivityDao.updateDrawActivity(drawActivity);
	}

	@Override
	public PageSupport<DrawActivity> getDrawActivityPage(String curPageNO, DrawActivity drawActivity) {
		return drawActivityDao.getDrawActivityPage(curPageNO, drawActivity);
	}

	/* 
	 * 领奖(预付款、积分充值)
	 */
	@Override
	public AjaxJson receiveCashOrIntegral(DrawWinRecord drawWinRecord, Long awardsRecordId, String userId, String userName, DrawActivity activityPo, AjaxJson json) {
		if (drawWinRecord.getAwardsType() == 2) {// 预付款充值操作
			String amount = getAmount(drawWinRecord);
			// 充值
			DrawRecharge recharge = new DrawRecharge();
			recharge.setUserId(userId);
			recharge.setUserName(userName);
			recharge.setAmount(Double.parseDouble(amount));
			recharge.setMessage("微信活动【" + activityPo.getActName() + "】充值预付款金额:" + amount);
			recharge.setSn(drawWinRecord.getActId().toString());
			weiXinDrawSuccessProcessor.process(recharge);
			String result = recharge.getResult();
			if (PreDepositErrorEnum.OK.value().equals(result)) {
				drawWinRecordService.saveDrawWinRecord(drawWinRecord, awardsRecordId); // 保存中奖记录
				json.setSuccess(true);
				json.setMsg("预付款充值成功，请查收");
			} else {
				json.setSuccess(false);
				json.setMsg(result);
			}
		} else if(drawWinRecord.getAwardsType() == 3) { // 积分充值操作
			String amount = getAmount(drawWinRecord);
			UserDetail userdetail = userDetailService.getUserDetailById(userId);
			if (AppUtils.isBlank(userdetail)) {
				json.setSuccess(false);
				json.setMsg("该用户详情不存在!");
				return json;
			}

			// 更新用户积分
			IntegraLog integraLog = new IntegraLog();
			integraLog.setUserId(userId);
			integraLog.setAddTime(new Date());
			integraLog.setLogType(IntegralLogTypeEnum.LOG_RECHARGE.value());
			integraLog.setIntegralNum(Integer.parseInt(amount));
			integraLog.setLogDesc("微信活动【" + activityPo.getActName() + "】充值积分:" + amount);
			String result = userDetailService.updateUserScore(userdetail, integraLog);
			if (result.equals("OK")) {
				drawWinRecordService.saveDrawWinRecord(drawWinRecord, awardsRecordId);
				json.setSuccess(true);
				json.setMsg("积分充值成功，请查收");
			} else {
				json.setSuccess(false);
				json.setMsg(result);
				return json;
			}
		}else {//赠送红包
			
			//赠送红包
			Long couponId = Long.parseLong(getAmount(drawWinRecord));
			Coupon coupon = couponService.getCoupon(couponId);
						
			String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setCouponId(couponId);
			userCoupon.setCouponName(coupon.getCouponName());
			userCoupon.setCouponSn(sn);
			userCoupon.setUseStatus(1);
			userCoupon.setGetSources(CouponSourceEnum.DRAW.value());
			userCoupon.setUserId(userId);
			userCoupon.setUserName(userName);
			userCoupon.setGetTime(new Date());
			Long result = userCouponService.saveUserCoupon(userCoupon);
			if(AppUtils.isNotBlank(result)) {
				//红包绑定 + 1
				coupon.setBindCouponNumber(coupon.getBindCouponNumber() + 1);
				couponService.updateCoupon(coupon);
				//保存中奖记录
				drawWinRecordService.saveDrawWinRecord(drawWinRecord, awardsRecordId);
				json.setSuccess(true);
				json.setMsg("红包领取成功，请查收");
			}else {
				json.setSuccess(false);
				json.setMsg("红包领取失败");
				return json;
			}	
		}
		return json;
	}
	/**
	 * @Description: 根据奖项类型返回相对应的预付款、积分
	 * @date 2016-4-27
	 */
	public String getAmount(DrawWinRecord drawWinRecord) {
		String amount = null;
		DrawActivity activityPo = getDrawActivityById(drawWinRecord.getActId());
		List<DrawAwardsDto> daDtos = JSONUtil.getArray(activityPo.getAwardInfo(), DrawAwardsDto.class);
		for (DrawAwardsDto dto : daDtos) {
			if (dto.getId() == drawWinRecord.getFlag()) {
				if (dto.getAwardsType() == drawWinRecord.getAwardsType()) {
					switch (drawWinRecord.getAwardsType()) {
					case 2: // 预付款
						amount = dto.getCash().toString();
						break;
					case 3: // 积分
						amount = dto.getIntegral().toString();
						break;
					case 4://红包
						amount = dto.getCoupon().toString();// 红包卷ID
					}
				}

			}
		}
		return amount;
	}
}
