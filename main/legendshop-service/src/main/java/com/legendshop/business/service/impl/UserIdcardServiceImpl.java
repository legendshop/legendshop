/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserIdcardDao;
import com.legendshop.model.entity.overseas.UserIdcard;
import com.legendshop.spi.service.UserIdcardService;
import com.legendshop.util.AppUtils;

/**
 *用户实名认证服务
 */
@Service("userIdcardService")
public class UserIdcardServiceImpl  implements UserIdcardService{
	
	@Autowired
    private UserIdcardDao userIdcardDao;


    @Override
	public List<UserIdcard> getUserIdcard(String userId) {
        return userIdcardDao.getUserIdcard(userId);
    }

    @Override
	public UserIdcard getUserIdcard(Long id) {
        return userIdcardDao.getUserIdcard(id);
    }

    @Override
	public void deleteUserIdcard(UserIdcard userIdcard) {
        userIdcardDao.deleteUserIdcard(userIdcard);
    }

    @Override
	public Long saveUserIdcard(UserIdcard userIdcard) {
        if (!AppUtils.isBlank(userIdcard.getId())) {
            updateUserIdcard(userIdcard);
            return userIdcard.getId();
        }
        return userIdcardDao.saveUserIdcard(userIdcard);
    }

    public void updateUserIdcard(UserIdcard userIdcard) {
        userIdcardDao.updateUserIdcard(userIdcard);
    }

	@Override
	public UserIdcard getDefaultUserIdcard(String userId) {
		return userIdcardDao.getDefaultUserIdcard(userId);
	}
}
