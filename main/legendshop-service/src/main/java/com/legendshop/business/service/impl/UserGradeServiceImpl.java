/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserGradeDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.util.AppUtils;

/**
 * 用户等级服务.
 */
@Service("userGradeService")
public class UserGradeServiceImpl  implements UserGradeService{
	
	@Autowired
    private UserGradeDao userGradeDao;

    public UserGrade getUserGrade(Integer id) {
        return userGradeDao.getUserGrade(id);
    }

    public void deleteUserGrade(UserGrade userGrade) {
        userGradeDao.deleteUserGrade(userGrade);
    }

    public Integer saveUserGrade(UserGrade userGrade) {
        if (!AppUtils.isBlank(userGrade.getGradeId())) {
            updateUserGrade(userGrade);
            return userGrade.getGradeId();
        }
        return (Integer) userGradeDao.save(userGrade);
    }

    public void updateUserGrade(UserGrade userGrade) {
        userGradeDao.updateUserGrade(userGrade);
    }

	@Override
	public List<StatusKeyValueEntity> retrieveUserGrade() {
		return userGradeDao.retrieveUserGrade();
	}

	@Override
	public List<UserGrade> queryUserGrades() {
		return userGradeDao.queryByProperties(new EntityCriterion().addAscOrder("score"));
	}

	@Override
	public PageSupport<UserGrade> getUserGradePage(String curPageNO, UserGrade userGrade) {
		return userGradeDao.getUserGradePage(curPageNO,userGrade);
	}
}
