/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ThemeModuleDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 *专题板块Dao
 */
@Repository
public class ThemeModuleDaoImpl extends GenericDaoImpl<ThemeModule, Long> implements ThemeModuleDao  {
	
	
    @Override
	public List<ThemeModule> getThemeModuleByTheme(Long themeId){
   		return this.queryByProperties(new EntityCriterion().eq("themeId", themeId).addAscOrder("seq"));
    }

	@Override
	public ThemeModule getThemeModule(Long id){
		return getById(id);
	}
	
    @Override
	public int deleteThemeModule(ThemeModule themeModule){
    	return delete(themeModule);
    }
	
	@Override
	public Long saveThemeModule(ThemeModule themeModule){
		return save(themeModule);
	}
	
	@Override
	public int updateThemeModule(ThemeModule themeModule){
		return update(themeModule);
	}

	@Override
	public void deleteThemeModule(List<ThemeModule> objModules) {
         this.delete(objModules);		
	}

	@Override
	public List<ThemeModuleProd> findModuleProdsByModuleId(Long moduleId){
		StringBuffer sql=new StringBuffer("select mp.*,p.cash ")
		.append("from ls_theme_module_prd mp ")
		.append("left join ls_prod p on p.prod_id =mp.prod_id ")
		.append("where mp.theme_module_id = ?");
		return super.query(sql.toString(), new Object[]{moduleId}, new RowMapper<ThemeModuleProd>() {
			@Override
			public ThemeModuleProd mapRow(ResultSet rs, int rowNum) throws SQLException {
				ThemeModuleProd themeModuleProd =new ThemeModuleProd();
				themeModuleProd.setAngleIcon(rs.getString("angle_icon"));
				double cash=rs.getDouble("cash");
				themeModuleProd.setCash(cash);
				themeModuleProd.setImg(rs.getString("img"));
				themeModuleProd.setIsSoldOut(rs.getLong("is_sold_out"));
				themeModuleProd.setProdId(rs.getLong("prod_id"));
				themeModuleProd.setProdName(rs.getString("prod_name"));
				themeModuleProd.setThemeModuleId(rs.getLong("theme_module_id"));
				return themeModuleProd;
			}
		});
		
	}
	
	
 }
