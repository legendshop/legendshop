package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.constant.PdCashLogEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdCashLogDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.spi.service.AppPdCashLogService;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.util.AppUtils;

/**
 * app 预存款明细记录service实现
 */
@Service("appPdCashLogService")
public class AppPdCashLogServiceImpl implements AppPdCashLogService{

	@Autowired
	private PdCashLogService pdCashLogService;

	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	@Override
	public AppPageSupport<AppPdCashLogDto> getPdCashLogPageByType(String userId, Integer state, String curPageNO) {

		PageSupport<PdCashLog> ps = pdCashLogService.getPdCashLogPageByType(userId,state,curPageNO);
		
		AppPageSupport<AppPdCashLogDto> pageSupportDto = PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<PdCashLog, AppPdCashLogDto>() {

			@Override
			public List<AppPdCashLogDto> convert(List<PdCashLog> form) {
				
				if (AppUtils.isBlank(form)) {
					return null;
				}
				
				List<AppPdCashLogDto> toList = new ArrayList<AppPdCashLogDto>();
				for (PdCashLog pdCashLog : form) {
					AppPdCashLogDto appPdCashLogDto = convertToAppPdCashLogDto(pdCashLog);
					toList.add(appPdCashLogDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}
	
	@Override
	public AppPdCashLogDto getPdCashLog(Long id) {
		
		PdCashLog pdCashLog = pdCashLogService.getPdCashLog(id);
		if (AppUtils.isNotBlank(pdCashLog)) {
			
			return convertToAppPdCashLogDto(pdCashLog);
		}
		return null;
	}
	
	/**
	 * 转dto方法
	 * @param pdCashLog
	 * @return
	 */
	private AppPdCashLogDto convertToAppPdCashLogDto(PdCashLog pdCashLog){
		  AppPdCashLogDto appPdCashLogDto = new AppPdCashLogDto();
		  appPdCashLogDto.setLogType(pdCashLog.getLogType());
		  appPdCashLogDto.setLogTypeName(PdCashLogEnum.getDesc(pdCashLog.getLogType()));
		  appPdCashLogDto.setAmount(pdCashLog.getAmount());
		  appPdCashLogDto.setLogDesc(pdCashLog.getLogDesc());
		  appPdCashLogDto.setAddTime(pdCashLog.getAddTime());
		  appPdCashLogDto.setId(pdCashLog.getId());
		  appPdCashLogDto.setSn(pdCashLog.getSn());
		  appPdCashLogDto.setUserName(pdCashLog.getUserName());
		  appPdCashLogDto.setUserId(pdCashLog.getUserId());
		  return appPdCashLogDto;
		}
}
