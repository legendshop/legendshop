/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CategoryCommDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.CategoryComm;
/**
 *常用分类Dao
 */
@Repository
public class CategoryCommDaoImpl extends GenericDaoImpl<CategoryComm, Long> implements CategoryCommDao  {
	
	@Override
	public List<CategoryComm> getCategoryCommListByShopId(Long shopId) {
		return this.query("SELECT co.id AS id,co.shop_id AS shopId,co.category_id AS categoryId,co.rec_date AS recDate FROM ls_category_comm co,ls_category ca WHERE co.category_id= ca.id AND co.shop_id = ? AND ca.status = 1 ORDER BY co.rec_date DESC;", CategoryComm.class, shopId);
	}

	@Override
	public void delCategoryComm(Long id) {
		this.deleteById(id);
	}

	@Override
	public Long saveCategoryComm(CategoryComm categoryComm) {
		return this.save(categoryComm);
	}

	@Override
	public CategoryComm getCategoryComm(Long categoryId, Long shopId) {
		return this.getByProperties(new EntityCriterion().eq("categoryId", categoryId).eq("shopId", shopId));
	}

	@Override
	public void delCategoryComm(Long id, Long shopId) {
		this.update("delete from ls_category_comm where id=? and shop_id=?",id,shopId);
	}


	
	
 }
