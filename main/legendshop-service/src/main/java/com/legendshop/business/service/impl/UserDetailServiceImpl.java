/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import cn.hutool.core.collection.CollUtil;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.*;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.*;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.AppTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.model.UserMessages;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.MailHelper;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.plugins.PluginManager;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.form.UserForm;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.model.form.UserNameTypeEnum;
import com.legendshop.model.securityCenter.UserInformation;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.IntegraLogService;
import com.legendshop.spi.service.IntegralService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.util.SafeHtml;

/**
 * 用户服务.
 */
@Service("userDetailService")
public class UserDetailServiceImpl  implements UserDetailService {
	
	private final static String getUserCouponCountSql = "select count(*) from ls_coupon bc ,ls_user_coupon buc where bc.coupon_id = buc.coupon_id " +
			"and buc.use_status =  1 and bc.status = 1 and buc.user_id = ? and bc.coupon_provider = ? and bc.start_date <=  ? and bc.end_date >  ?";

	@Autowired(required = false)
	private IntegralService integralService;
	
	@Autowired(required = false)
	private IntegraLogService integraLogService;
	
	private PluginManager pluginManager = PluginRepository.getInstance();
	
	@Autowired
	protected UserDetailDao userDetailDao;
	
	@Autowired
	protected UserSecurityDao userSecurityDao;
	
	@Autowired
	private MailManager mailManager;

	@Autowired
	private MailHelper mailHelper;
	
	@Autowired
	private ShopDetailDao shopDetailDao;
	
	@Autowired
	private CoinLogDao coinLogDao;
	
	@Resource(name="promotionRegSuccessProcessor")
	private EventProcessor<UserDetail> promotionRegSuccessProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private AppTokenService appTokenService;

	@Resource(name = "productIndexDelProcessor")
	private EventProcessor<ProductIdDto> productIndexDelProcessor;

	@Autowired
	private ProductDao productDao;



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.UserDetailService#queryScore(java.lang
	 * .String)
	 */
	@Override
	public Integer getScore(String userId) {
		return userDetailDao.getUserScore(userId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.legendshop.business.service.UserDetailService#findUserDetail(java
	 * .lang.String)
	 */
	@Override
	public UserDetail getUserDetail(String userName) {
		return userDetailDao.getUserDetailByName(userName);
	}
	
	@Override
	public UserDetail getUserDetailById(String userId) {
		return userDetailDao.getUserDetailById(userId);
	}
	
	/**
	 * 批量充值金币
	 * @param result
	 */
	@Override
	public CoinRechargeLog batchUpdateCoin(CoinImportResult result){
		List<UserDetail> userLists = result.getResult();
		CoinRechargeLog rechargeLog = new CoinRechargeLog();
		//充值成功个数
		Integer successNumber = 0;
		//充值失败个数
		Integer failNumber = 0;
		//充值成功总金额
		double sum = 0;
		//记录手机号码不存在的用户
		List<String> noMobileList = new ArrayList<String>();
		for(UserDetail user_new:userLists){
			String mobile = user_new.getUserMobile();
			//获得原先的用户
			UserDetail user_origin = userDetailDao.getUserDetailByMobile(mobile);
			//如果用户不存在,记录手机号码
			if(AppUtils.isBlank(user_origin)){
				noMobileList.add(mobile);
				rechargeLog.setIsSuccess("0");
				failNumber++;  //充值失败+1
				rechargeLog.setMessage("部分充值成功！");
				continue;
			}
			Double amount_add = user_new.getUserCoin();
			//如果充值为空，记录手机号码
			if(AppUtils.isBlank(amount_add)){
				noMobileList.add(mobile);
				rechargeLog.setIsSuccess("0");
				failNumber++;  //充值失败+1
				rechargeLog.setMessage("部分充值成功！");
				continue;
			}
			//获得原有的金币
			Double amount_origin = user_origin.getUserCoin();
			Double amount_new = Arith.add(amount_origin, amount_add);
			user_origin.setUserCoin(amount_new);
			userDetailDao.update(user_origin);
			//充值成功+1
			successNumber++;
			//统计充值成功总金额
			sum = Arith.add(sum, amount_add);
			//加入日志
			String desc = "批量充值，金币原有金额："+amount_origin+",现有金额："+amount_new;
			String sn = CommonServiceUtil.getRandomSn();
			CoinLog coinLog = new CoinLog(user_origin.getUserId(),user_origin.getUserName(),
					sn,"recharge",amount_add,desc,new Date());
			coinLogDao.save(coinLog);
		}
		if(AppUtils.isBlank(rechargeLog.getMessage())){
			rechargeLog.setIsSuccess("1");
			rechargeLog.setMessage("充值成功!");
		}
		rechargeLog.setFailNumber(failNumber);
		rechargeLog.setSuccessNumber(successNumber);
		rechargeLog.setSum(sum);
		return rechargeLog;
	}
	
	/**
	 * 注册用户,商家
	 */
	//@Override
	public UserResultDto saveUserReg(String ip, UserForm form, String encodedPassword) {
		UserResultDto userResultDto = new UserResultDto();
		User user = null;
		//检查form
		if (!isUserInfoValid(form, userResultDto)) {
			userResultDto.setStatusCode(0);//操作失败
			return userResultDto;
		}
		// 过滤特殊字符
		SafeHtml safeHtml = new SafeHtml();
		String userName = generateUserName();//获取随机数
		form.setNickName(safeHtml.makeSafe(form.getName()));
		form.setUserName(userName);
		form.setUserMemo(safeHtml.makeSafe(form.getUserMemo()));
		form.setUserMobile(safeHtml.makeSafe(form.getUserMobile()));
		form.setUserPostcode(safeHtml.makeSafe(form.getUserPostcode()));
		form.setUserTel(safeHtml.makeSafe(form.getUserTel()));
		form.setUserMail(safeHtml.makeSafe(form.getUserMail()));
		form.setUserAdds(safeHtml.makeSafe(form.getUserAdds()));
		form.setMsn(safeHtml.makeSafe(form.getMsn()));
		form.setNote(safeHtml.makeSafe(form.getNote()));
		form.setQq(safeHtml.makeSafe(form.getQq()));
		form.setName(userName);
		

		user = parseUser(form);
		UserDetail userDetail = parseUserDeatil(form);
		// 注册用户
		Date date = new Date();
		String plaintPassword = user.getPassword(); //Password不加密
		user.setPassword(encodedPassword);//密码已经在前面加密过
		userDetail.setUserRegtime(date);
		userDetail.setModifyTime(date);
		userDetail.setUserRegip(ip);
		userDetail.setTotalCash(0d);
		userDetail.setTotalConsume(0d);
		userDetail.setScore(0);
		
		user = userDetailDao.saveUser(user, userDetail);
		
		boolean validationFromMail = systemParameterUtil.isValidationFromMail();
		if(userDetail.getUserMail() != null && validationFromMail){	
			user.setEnabled(Constants.USER_STATUS_DISABLE);// 在用户验证邮箱前，无法进行登录
			userDetail.setRegisterCode(MD5Util.toMD5(user.getName() + user.getPassword())); // 将RegisterCode发送到邮件，点击开通用户
			
			// 发送通知注册成功邮件,注册后选择性验证邮箱
			user.setPasswordag(plaintPassword);//没加密密码
			mailManager.register(user,userDetail);
			user.setPasswordag("");//清空
		}
		
		userResultDto.setAttribute("userMail", form.getUserMail());
		//获取邮箱的类型
		if(AppUtils.isNotBlank(form.getUserMail()) && form.getUserMail().indexOf("@")!= -1){
			String officialEmail = mailHelper.getMailAddress(form.getUserMail().substring(form.getUserMail().indexOf("@")+1));
			userResultDto.setAttribute("officialEmail", officialEmail);
		}
		userResultDto.setAttribute("userName", form.getNickName());
		
		userResultDto.setUser(user);
		
		return userResultDto;

	}
	
	
	/**
	 * 商家注册
	 */
	@Override
	public boolean saveShopReg(String userName, ShopDetail shopDetail) {
		
		// 过滤特殊字符
		SafeHtml safeHtml = new SafeHtml();
		shopDetail.setRealPath(propertiesUtil.getBigPicPath());
		shopDetail.setSiteName(safeHtml.makeSafe(shopDetail.getSiteName().trim()));
		if(AppUtils.isNotBlank(shopDetail.getPostAddr())){
			shopDetail.setPostAddr(safeHtml.makeSafe(shopDetail.getPostAddr()));
		}
		shopDetail.setIdCardNum(safeHtml.makeSafe(shopDetail.getIdCardNum()));
		shopDetail.setGradeId(0);// 普通商家
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		shopDetailDao.saveShopDetail(userDetail, shopDetail);
		return true;
	}
	
	/**
	 * 手机注册用户
	 */
	@Override
	public User saveUserReg(String userRegip, UserMobileForm form, String encodedPassword) {
		User user = null;
		if(AppUtils.isBlank(form.getUserName())) {
			//获取随机数
			form.setUserName(generateUserName());
		}
		form.encoding(); //加密里面的字段		
		user = new User();
		user.setName(form.getUserName());
		user.setEnabled("1");//手机端，刚注册都有效
		user.setPassword(encodedPassword);
		user.setOpenId(form.getOpenId());
		
		UserDetail userDetail = parseUserDeatil(form);
		userDetail.setUserRegip(userRegip);
		
		user = userDetailDao.saveUser(user, userDetail);
		
		//推广员推广的用户,绑定上下级关系
		userDetail.setParentUserName(form.getParentUserName());
		
		//发送事件
		promotionRegSuccessProcessor.process(userDetail);
			
		//注册送积分
		integralService.addScore(user.getId(), SendIntegralRuleEnum.USER_REGISTER);

		return user;
	}
	
	private User parseUser(UserForm form){
		User user = new User();
		user.setName(form.getName());
		user.setNote(form.getNote());
		user.setEnabled(form.getEnabled());
		user.setPassword(form.getPassword());
		return user;
	}
	
	private UserDetail parseUserDeatil(UserForm form){
		UserDetail userDetail = new UserDetail();
		userDetail.setUserName(form.getUserName());
		userDetail.setUserMemo(form.getUserMemo());
		userDetail.setUserMobile(form.getUserMobile());
		userDetail.setUserPostcode(form.getUserPostcode());
		userDetail.setUserTel(form.getUserTel());
		userDetail.setUserMail(form.getUserMail());
		userDetail.setUserAdds(form.getUserAdds());
		userDetail.setNickName(form.getNickName());
		userDetail.setBirthDate(form.getBirthDate());
		userDetail.setEnabled(form.getEnabled());
		userDetail.setFax(form.getFax());
		userDetail.setQq(form.getQq());
		userDetail.setMsn(form.getMsn());
	    userDetail.setGradeId(1);// 注册用户
		return userDetail;
	}
	
	private UserDetail parseUserDeatil(UserMobileForm form){
		Date date = new Date();
		UserDetail userDetail = new UserDetail();
		userDetail.setUserMobile(form.getMobile());
		userDetail.setUserName(form.getUserName());
		userDetail.setNickName(form.getNickName());
		userDetail.setPortraitPic(form.getPortraitPic());
		userDetail.setSex(form.getSex());
		userDetail.setActivated(form.isActivated());
	    
		userDetail.setTotalCash(0d);
		userDetail.setIsRegIM(0);
		userDetail.setTotalConsume(0d);
		userDetail.setUserCoin(0d);
		userDetail.setScore(0);
		userDetail.setGradeId(1);// 注册用户
		userDetail.setUserRegtime(date);
		userDetail.setModifyTime(date);
		return userDetail;
	}

	/**
	 * 删除用户信息
	 */
	@Override
	@CacheEvict(value = "UserDetail", key = "#userId")
	public String deleteUserDetail(String userId, String userName) {
		String result = userDetailDao.deleteUserDetail(userId, userName);
		ShopDetail shopDetail = shopDetailDao.getShopDetailByUserId(userId);
		if (AppUtils.isNotBlank(shopDetail)) {
			/////////// 删除商城部分//////////////
			result = shopDetailDao.deleteShopDetail(shopDetail.getShopId(), userId, userName, false);
			if (result != null) {// 如果商城删除失败
				return result;
			}
		}
		
		return result;
	}


	@Override
	public boolean updatePassword(String userName, String mail,String password){
		return userDetailDao.updatePassword(userName, mail, password);

	}

	
	/**
	 * 根据用户输入自动生成用户名
	 * @param
	 * @return
	 */
	public String generateUserName(String name){
		int totalNum = 3;
		//String prefix = mail.substring(0,mail.indexOf('@'));
		String suffix = null;
		String userName;
		int count = 0;
		do {
			count++;
			totalNum +=count / 4; //每试3次不成功就加1
			suffix = RandomStringUtils.randomAlphanumeric(totalNum);
			userName = name + suffix;
		} while (isUserExist(userName));
		if(userName.length() > 50){//用户名称最长是50个字符
			userName = RandomStringUtils.random(50);
		}
		return userName;
	}
	
	/** 根据用户Id 生成 用户名 **/
	public String generateUserName(){
		Random random = new Random();
		String userName;
		do{
			long millis = System.currentTimeMillis();
			userName =  String.valueOf(AppUtils.getCRC32(String.valueOf(millis)))+random.nextInt(10)+random.nextInt(10);
		}while(isUserExist(userName));
		return userName;
	}
	
	@Override
	public boolean isUserExist(String userName) {
		return userDetailDao.isUserExist(userName);
	}

	@Override
	public boolean isEmailExist(String email) {
		return userDetailDao.isEmailExist(email);
	}

	@Override
	public boolean isShopExist(Long shopId) {
		return shopDetailDao.isShopExist(shopId);
	}

	@Override
	public User getUser(String userId) {
		return userDetailDao.getUser(userId);
	}

	@Override
	public void uppdateUser(User user) {
		userDetailDao.updateUser(user);

	}

	@Override
	public UserInformation getUserInfo( String userName) {
		return userDetailDao.getUserInfo(userName);
	}

	@Override
	public String getUserPassword(String UserId) {
		return userDetailDao.getUserPassword(UserId);
	}

	@Override
	public String getUserEmail(String userId) {
		return userDetailDao.getUserEmail(userId);
	}

	@Override
	public void updatePhoneVerifn(String userName) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setPhoneVerifn(1);
		Integer level = userSecurity.getSecLevel() == null ? 0 :userSecurity.getSecLevel();
		userSecurity.setSecLevel(level + 1);
		//原来的逻辑, 现在加入缓存处理 update("update ls_usr_security set phone_verifn = 1,sec_level =(sec_level+1) where user_name = ?",userName);
		userSecurityDao.updateUserSecurity(userSecurity);
	}

	@Override
	public void updatePaypassVerifn(String userName) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setPaypassVerifn(1);
		Integer level = userSecurity.getSecLevel() == null ? 0 :userSecurity.getSecLevel();
		userSecurity.setSecLevel(level + 1);
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
		
	}

	@Override
	public UserSecurity getUserSecurity(String userName) {
		return userSecurityDao.getUserSecurity(userName);
	}

	@Override
	public void updatEmailVerifn(String username) {

		UserSecurity userSecurity = userSecurityDao.getUserSecurity(username);
		if(userSecurity == null){
			return;
		}
		userSecurity.setMailVerifn(1);
		userSecurity.setTimes(1);
		Integer level = userSecurity.getSecLevel() == null ? 0 :userSecurity.getSecLevel();
		userSecurity.setSecLevel(level + 1);
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
	}

	@Override
	public void updateUser(User user) {
		userDetailDao.updateUser(user);
	}

	@Override
	public String getUserMobile(String userId) {
		return userDetailDao.getUserMobile(userId);
	}

	@Override
	public void updateUserDetail(UserDetail userDetail) {
		userDetailDao.updateUserDetail(userDetail);
	}
	
	/**
	 * 金币充值
	 */
	public String rechargeUserCoin(UserDetail userDetail,Double amount){
		//获取原有的金币
		Double amountOld = userDetail.getUserCoin();
		//判断原来的金币是否为null或0
		if(AppUtils.isBlank(amountOld)){
			amountOld = 0.0;
		}
		
		Double accountNew =Arith.add(amountOld, amount);
		userDetail.setUserCoin(accountNew);
		//更新金币
		userDetailDao.updateUserDetail(userDetail);
		//添加金币日志 
		String sn = CommonServiceUtil.getRandomSn();
		CoinLog coinLog = new CoinLog(userDetail.getUserId(),userDetail.getUserName(),sn,"recharge",amount,"金币原有"+amountOld+",现充值金额为"+amount,new Date());
		coinLogDao.saveCoinLog(coinLog);
		return "SUCCESS";
	}
	
	/**
	 * 金币退款
	 */
	@Override
	public String refundCoin(UserDetail userDetail,Double amount){
		//获取原有的金币
		Double amountOld = userDetail.getUserCoin();
		//判断原来的金币是否为null或0
		if(AppUtils.isBlank(amountOld)){
			amountOld = 0.0;
		}
		
		Double accountNew =Arith.add(amountOld, amount);
		userDetail.setUserCoin(accountNew);
		//更新金币
		userDetailDao.updateUserDetail(userDetail);
		//添加金币日志 
		String sn = CommonServiceUtil.getRandomSn();
		CoinLog coinLog = new CoinLog(userDetail.getUserId(),userDetail.getUserName(),sn,"refund",amount,"金币原有"+amountOld+",现退回金额为"+amount,new Date());
		coinLogDao.saveCoinLog(coinLog);
		return "SUCCESS";
	}


	/**
	 * 更新短信验证码
	 */
	@Override
	public void updateUserSecurity(String userName,String validateCode) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setValidateCode(validateCode);
		userSecurity.setSendSmsTime(new Date());
		userSecurityDao.updateUserSecurity(userSecurity);
	}
	

	@Override
	public void saveDefaultUserSecurity(String userName) {
		userSecurityDao.saveDefaultUserSecurity(userName);
		
	}

	@Override
	public boolean isPhoneExist(String Phone) {
		return userDetailDao.isPhoneExist(Phone);
	}

	@Override
	public UserInformation getUserInfoByPhone(String Phone) {
		return userDetailDao.getUserInfoByPhone(Phone);
	}

	@Override
	public UserInformation getUserInfoByMail(String Email) {
		return userDetailDao.getUserInfoByMail(Email);
	}
	
	@Override
	public String convertUserLoginName(String name) {
		return userDetailDao.convertUserLoginName(name);
	}

	@Override
	public String convertShopUserLoginName(String name) {
		return userDetailDao.convertShopUserLoginName(name);
	}
	

	@Override
	public boolean verifySMSCode(String userName, String code) {
		return userDetailDao.verifySMSCode(userName, code);
	}

	@Override
	public boolean isEmailOnly(String email,String userName) {
		return userDetailDao.isEmailOnly(email,userName);
	}

	@Override
	public boolean isPhoneOnly(String phone, String userName) {
		return userDetailDao.isPhoneOnly(phone, userName);
	}

	@Override
	public Integer getMailVerifn(String userName) {
		return userDetailDao.getMailVerifn(userName);
	}

	@Override
	public Integer getPhoneVerifn(String userName) {
		return userDetailDao.getPhoneVerifn(userName);
	}

	@Override
	public Integer getPaypassVerifn(String userName) {
		return userDetailDao.getPaypassVerifn(userName);
	}

	@Override
	public Boolean updateNewPassword(String userName, String newPassword) {
		return userDetailDao.updateNewPassword(userName, newPassword);
	}

	//将邮箱及邮箱验证码 更新到 ls_usr_security.
	@Override
	public void updateSecurityMail(String userName, String validateCode, String userEmail) {
		//update("update ls_usr_security set email_code = ? , update_mail = ? , send_mail_time = ?,times = ?  where user_name = ?",validateCode,userEmail,new Date(),0,userName);
		//userDetailDao.updateSecurityMail(userName, validateCode, userEmail);
		
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setEmailCode(validateCode);
		userSecurity.setUpdateMail(userEmail);
		userSecurity.setSendMailTime(new Date());
		userSecurity.setTimes(0);
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
	}

	@Override
	public void updateUserEmail(String userEmail, String userName) {
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		if(userDetail == null){
			return;
		}
		userDetail.setUserMail(userEmail);
		//update("update ls_usr_detail set user_mail = ? where user_name = ?",userEmail,userName);
		userDetailDao.updateUserDetail(userDetail);
		Integer emailVerifn = userDetailDao.getMailVerifn(userName);
		if(emailVerifn == 0){
			//该用户安全等级+1,邮箱验证变为1 且验证次数变为1
			this.updatEmailVerifn(userName);
		}
	}

	@Override
	public String getSecurityEmail(String userName) {
		//return getJdbcTemplate().queryForObject("select update_mail from ls_usr_security where user_name = ?", String.class,userName);
		//return userDetailDao.getSecurityEmail(userName);
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return null;
		}
		return userSecurity.getUpdateMail();
	}

	/**
	 * 更新次数
	 */
	@Override
	public void updatetimes(Integer number,String userName) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setTimes(number);
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
	}

	@Override
	public void updateUserMobile(String userMobile, String userName) {
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		if(userDetail == null){
			return;
		}
		userDetail.setUserMobile(userMobile);
		int updateUserDetail = userDetailDao.updateUserDetail(userDetail);
		System.out.println(updateUserDetail);
	}

	@Override
	public Integer getSecLevel(String userName) {
		return userDetailDao.getSecLevel(userName);
	}

	/**
	 * 获取用户头像
	 */
	@Override
	public String getPortraitPic(String userName) {
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		if(userDetail == null){
			return null;
		}
		return userDetail.getPortraitPic();
	}

	@Override
	public Date getLastLoginTime(String userName) {
		return userDetailDao.getLastLoginTime(userName);
	}

	@Override
	public boolean isNickNameExist(String nickName) {
		return userDetailDao.isNickNameExist(nickName);
	}

	@Override
	public void updateUserInfo(UserSecurity userSecurity,UserDetail userDetail){
		userSecurityDao.update(userSecurity);
		userDetailDao.updateUserDetailEmail(userDetail);
	}

	@Override
	public void updateNickName(String nickName, String userId) {
		UserDetail userDetail = userDetailDao.getUserDetailById(userId);
		if(userDetail == null){
			return;
		}
		userDetail.setNickName(nickName);
		userDetailDao.updateUserDetail(userDetail);
	}

	/** 根据用户ID 修改 性别 **/
	@Override
	public void updateSex(String sex, String userId) {
		UserDetail userDetail = userDetailDao.getUserDetailById(userId);
		if(userDetail == null){
			return;
		}
		userDetail.setSex(sex);
		userDetailDao.updateUserDetail(userDetail);
	}

	@Override
	public void updateBirthday(Date birthDate, String userId) {
		UserDetail userDetail = userDetailDao.getUserDetailById(userId);
		if(userDetail == null){
			return;
		}
		userDetail.setBirthDate(birthDate);
		userDetailDao.updateUserDetail(userDetail);
	}

	@Override
	public User getUserByMobile(String userMobile) {
		return userDetailDao.getUserByMobile(userMobile);
	}
	
	/**
	 * @Description:根据用户名查找用户
	 * @param  userName
	 * @date 2016-5-6
	 */
	@Override
	public User getUserByUserName(String userName) {
		return userDetailDao.getUserByName(userName);
	}

	@Override
	public UserInformation getUserInfoByNickName(String nickName) {
		return userDetailDao.getUserInfoByNickName(nickName);
	}

	@Override
	public User getUserByName(String userName) {
		return userDetailDao.getUserByName(userName);
	}

	@Override
	public User getUserByWxOpenId(String wxOpenId) {
		return userDetailDao.getUserByWxOpenId(wxOpenId);
	}

	@Override
	public boolean isMailCodeExist(String userName, String code) {
		return userSecurityDao.isMailCodeExist(userName,code);
	}

	@Override
	public void clearMailCode(String userName) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setEmailCode("");
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
	}
	
	@Override
	public void clearValidateCode(String userName) {
		UserSecurity userSecurity = userSecurityDao.getUserSecurity(userName);
		if(userSecurity == null){
			return;
		}
		userSecurity.setValidateCode("");
		//原来的逻辑, 现在加入缓存处理
		userSecurityDao.updateUserSecurity(userSecurity);
	}

	@Override
	public String getUserNameByNickName(String nickName) {
		return userDetailDao.getUserNameByNickName(nickName);
	}

	@Override
	public String getNickNameByUserName(String userName) {
		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		if(userDetail == null){
			return null;
		}
		return userDetail.getNickName();
	}

	@Override
	public boolean isSiteNameExist(String siteName) {
		return userDetailDao.isSiteNameExist(siteName);
	}

	@Override
	public UserDetail getUserDetailForScoreUser(String nickName) {
		return userDetailDao.getUserDetailForScoreUser(nickName);
	}

	@Override
	public UserDetail getUserDetailByAll(String nickName) {
		return userDetailDao.getUserDetailByAll(nickName);
	}

	@Override
	public String updateUserScore(UserDetail user, IntegraLog integraLog) {
		int resule=0;
		int score=integraLog.getIntegralNum();
		if(score<0){
			Integer newscore=Math.abs(score);
			resule=userDetailDao.updateScore(newscore, user.getScore(), user.getUserId());
			if(resule==0){
				return "对不起, 您的积分不足!";
			}
			saveIntegraLog(integraLog);
		}else{
			resule=userDetailDao.updateScore(score, user.getUserId());
			if(resule>0){			
				saveIntegraLog(integraLog);
			}
		}
		return Constants.SUCCESS;
	}
	
	private void saveIntegraLog(IntegraLog integraLog){
		//注册送积分
		integraLogService.saveIntegraLog(integraLog);
	}
	
	
	@Override
	public boolean checkBinding(String mobile) {
		return userDetailDao.checkBinding(mobile);
	}

	@Override
	public UserDetail getUserDetailByidForUpdate(String userId) {
		return userDetailDao.getUserDetailByidForUpdate(userId);
	}


	@Override
	public Integer calUnuseCouponCount(String userId,String proType) {
		return userDetailDao.get(getUserCouponCountSql, Integer.class, userId,proType,new Date(),new Date());
	}

	@Override
	public long getUserTotalCount() {
		return userDetailDao.getCount();
	}

	@Override
	public List<UserDetail> getUserDetailsByPage(int currPage,int pageSize) {
		return userDetailDao.queryByProperties(new EntityCriterion().addAscOrder("userRegtime"), (currPage-1)*pageSize, pageSize);
	}

	/**
	 * 根据消费记录  计算用户等级
	 * 
	 */
	@Override
	public void calculateUserGrade(String userId,Integer gradeId,List<UserGrade> userGrades) {
		
		//获得一年前的时间
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -1);
		Date startDate = c.getTime();
		
		//查询用户 一年内的有效的订单金额
		String sql = "SELECT SUM(actual_total) FROM ls_sub WHERE user_id = ? AND status=? AND finally_date >= ?";
		Double totalOrderPrice = userDetailDao.get(sql, Double.class, userId, OrderStatusEnum.SUCCESS.value(), startDate);
		if(totalOrderPrice==null){
			totalOrderPrice = 0d;
		}
		
		//查询用户 一年内的有效的退单金额 TODO 退款的申请状态  处理退款状态: 0:退款处理中 1:退款成功 -1:退款失败
		String returnSql = "SELECT SUM(refund_amount) FROM ls_sub_refund_return WHERE user_id = ? AND is_handle_success = ? AND admin_time >= ?";
		Double totalReturnPrice = userDetailDao.get(returnSql, Double.class, userId, 1, startDate);
		if(totalReturnPrice == null){
			totalReturnPrice = 0d;
		}
		
		//根据等级表的配置 计算用户的等级
		Double totalConsum = totalOrderPrice - totalReturnPrice;
		Integer userGradeId = userGrades.get(0).getGradeId();
		for (UserGrade userGrade : userGrades) {
			if(totalConsum.longValue()<userGrade.getScore().longValue()){
				break;
			}else{
				userGradeId = userGrade.getGradeId();
			}
		}
		
		//如果计算出来，用户的等级发生改变，则修改用户等级
		if(gradeId==null || userGradeId.intValue()!=gradeId.intValue()){
			userDetailDao.updateUserGrade(userId,userGradeId,totalConsum.intValue());
		}
		
	}

	@Override
	public UserDetail getUserDetailByIdNoCache(String userId) {
		return userDetailDao.getUserDetailByIdNoCache(userId);
	}

	@Override
	public Double getAvailablePredeposit(String userId) {
		return userDetailDao.getAvailablePredeposit(userId);
	}
	
	@CacheEvict(value = "UserDetail", key = "#user.id")
	public void changeUserStatus(User user, Integer status) {
		//判断用户是否有商城,且用户下线
		if (UserStatusEnum.DISABLED.value().equals(status)){
			appTokenService.deleteAppToken(user.getId());//下线app
			if (isHaveShop(user.getId())){
				shopDetailDao.updateShopDetailStatus(user.getId(), ShopStatusEnum.OFFLINE.value());//令其商城也下线
				Long shopId = shopDetailDao.getShopIdByUserId(user.getId());
				//查找其上线中和审核中的商品
				List<Long> prodIdList = productDao.getAllprodIdStr(shopId, ProductStatusEnum.PROD_ONLINE.value());
				prodIdList.addAll(productDao.getAllprodIdStr(shopId, ProductStatusEnum.PROD_AUDIT.value()));
				if (CollUtil.isNotEmpty(prodIdList)) {
					for (Long prodId : prodIdList) {
						//将上线状态或审核中的商品放入仓库中
						productDao.updateStatus(ProductStatusEnum.PROD_OFFLINE.value(),prodId,null);
					}
					//刷新商品索引
					productIndexDelProcessor.process(new ProductIdDto(prodIdList));
				}
			}
		}
		user.setEnabled(String.valueOf(status));
		userDetailDao.updateUser(user);
	}
	
	@Override
	public boolean isPromoter(String userId) {
		Integer promoterSts = userDetailDao.get("select promoter_sts from ls_user_commis where user_id=?", Integer.class, userId);
		if(promoterSts!=null && promoterSts.intValue()==1){
			return true;
		}else{
			return false;
		}
	}
	
	/* 
	 * 根据店铺ID获取用户信息
	 */
	@Override
	public UserDetail getUserDetailByShopId(Long shopId) {
		return userDetailDao.getUserDetailByShopId(shopId);
	}
	
	/**
	 * 判断用户是否有商城
	 * @param userId
	 * @return
	 */
	private boolean isHaveShop(String userId){
		UserDetail userDetail = userDetailDao.getById(userId);
		Long shopId = userDetail.getShopId();
		if(AppUtils.isBlank(shopId)){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public Double getUserCoin(String userId) {
		return userDetailDao.getUserCoin(userId);
	}

	@Override
	public List<UserDetailExportDto> findExportUserDetail(UserDetail user) {
		StringBuilder sb=new StringBuilder();
		List<Object> obj=new ArrayList<Object>();
		
		String sql = "SELECT * FROM";
		sb.append(sql).append(" (SELECT ud.user_id AS userId,ud.user_name userName,ud.nick_name nickName,ud.real_name realName,ud.sex sex,ud.birth_date birthday,ud.id_card idCard,")
		.append(" ud.user_coin userCoin,ud.total_cash totalCash,ud.user_mail email,ud.user_adds addr,ud.user_mobile mobile,ud.qq qq,")
		.append(" ud.score score,ud.income_level incomeLevel,ud.modify_time modifyTime,ud.user_regtime regTime,ud.user_lasttime lastTime,s.shop_id shopId")
		.append(" FROM ls_usr_detail ud LEFT JOIN ls_shop_detail s ON ud.user_id = s.user_id ")
		.append(" WHERE 1 = 1) u,ls_user s WHERE u.userId = s.id  ");
		
		if(AppUtils.isNotBlank(user)){
			if(AppUtils.isNotBlank(user.getUserName())){
				sb.append(" AND u.userName LIKE ?");
				obj.add("%"+user.getUserName().trim()+"%");
			}
			if(AppUtils.isNotBlank(user.getNickName())){
				sb.append(" AND u.nickName LIKE ?");
				obj.add("%"+user.getNickName().trim()+"%");
			}
			if(AppUtils.isNotBlank(user.getUserMail())){
				sb.append(" AND u.email LIKE ?");
				obj.add("%"+user.getUserMail().trim()+"%");
			}
			if(AppUtils.isNotBlank(user.getRealName())){
				sb.append(" AND u.realName LIKE ?");
				obj.add("%"+user.getRealName().trim()+"%");
			}
			if(AppUtils.isNotBlank(user.getUserMobile())){
				sb.append(" AND u.mobile LIKE ?");
				obj.add("%"+user.getUserMobile().trim()+"%");
			}
			if(AppUtils.isNotBlank(user.getEnabled())){
				sb.append(" AND s.enabled=?");
				obj.add(user.getEnabled());
			}
			if(AppUtils.isNotBlank(user.getHaveShop())){
				if("1".equals(user.getHaveShop())){
					sb.append(" AND u.shopId is not null");
				}else{
					sb.append(" AND u.shopId is null");
				}
			}
			sb.append(" ORDER BY u.modifyTime DESC");
			return this.shopDetailDao.findExportUserDetail(sb.toString(),obj.toArray());
		}
		return null;
	}

	@Override
	public boolean checkPromoter(String userId) {
		return userDetailDao.checkPromoter(userId);
	}

	@Override
	public String getNickNameByUserId(String userId) {
		return userDetailDao.getNickNameByUserId(userId);
	}
	
	@Override
	public Long getShopIdByUserId(String userId) {
		return userDetailDao.getShopIdByUserId(userId);
	}
	
	@Override
	public Integer upIsRegIM(String userId, Integer param) {
		return userDetailDao.upIsRegIM(userId, param);
	}

	@Override
	public boolean isExitMail(String mail) {
		return userDetailDao.isExitMail(mail);
	}
	
	/**
	 * 检查用户信息是否有效
	 * @param form
	 * @param
	 * @return
	 */
	private boolean isUserInfoValid(UserForm form, UserResultDto userResultDto) {
		boolean result = true;
		String name = form.getName();
		
		// 检查是否重名
		UserMessages messages = new UserMessages();
		if (AppUtils.isBlank(form.getName())) {
			messages.addCallBackList(ResourceBundleHelper.getString("username.required"));
			result = false;
		}
		if (result) {
			if(ValidationUtil.checkPhone(name)){//电话
				form.setUserNameType(UserNameTypeEnum.PHONE);
				if(userDetailDao.isPhoneExist(name)){
					messages.addCallBackList(ResourceBundleHelper.getString("errors.phone"));
				}
			}else if(ValidationUtil.checkEmail(name)){
				form.setUserNameType(UserNameTypeEnum.EMAIL);
				//邮件
				if (userDetailDao.isEmailExist(form.getUserMail())) {
					messages.addCallBackList("Email <b>" + form.getUserMail() + "</b> "
							+ ResourceBundleHelper.getString("user.email.exists"));
				}
			}else if(ValidationUtil.checkAlpha(name)){
				form.setUserNameType(UserNameTypeEnum.USER_NAME);
				//用户名
				if (form.getName().length() < 4) {
					messages.addCallBackList(ResourceBundleHelper.getString("username.minlength"));
				}else if (userDetailDao.isUserExist(form.getName())) {
					messages.addCallBackList(ResourceBundleHelper.getString("error.User.IsExist"));
				}
			}else{
				messages.addCallBackList(ResourceBundleHelper.getString("operation.error"));
			}
			
		}
		result = messages.correct();
		if (!result){
			userResultDto.setAttribute(UserMessages.MESSAGE_KEY, messages);
			userResultDto.setAttribute("userForm", form);
		}else{
			//替换用户名
			if(UserNameTypeEnum.EMAIL.equals(form.getUserNameType())){
				//生成随机名字
				form.setUserMail(name);
				form.setName(generateUserName(name.substring(0,name.indexOf('@'))));
			}else if(UserNameTypeEnum.PHONE.equals(form.getUserNameType())){
				//生成名字
				form.setUserMobile(name);
				form.setName(generateUserName(name));
			}
		}
		return result;
	}

	@Override
	public List<Select2Dto> getSelect2User(String q, Integer currPage,
			Integer pageSize, String userId) {
		int index = (currPage - 1) * pageSize;
		String sql="SELECT ud.user_id AS id, ud.user_mobile AS text FROM ls_usr_detail ud WHERE ud.user_mobile LIKE "+"'%"+q+"%'"+
		" AND ud.user_id <>'"+userId+"'";
		List<Select2Dto> list=userDetailDao.queryLimit(sql, Select2Dto.class, index, pageSize);
		return list;
	}
	
	@Override
	public int getSelect2UserTotal(String q,String userId) {
		String sql="SELECT COUNT(*) FROM ls_usr_detail ud WHERE ud.user_name LIKE "+"'%"+q+"%'"+
		" AND ud.user_id <>'"+userId+"'";
		return (int)userDetailDao.getLongResult(sql);
	}

	@Override
	public UserDetail updateUserReg(String userName, String registerCode) {
		UserDetail userDetail = null;
		RegisterEnum result = userDetailDao.getUserRegStatus(userName, registerCode);
		if (!RegisterEnum.REGISTER_SUCCESS.equals(result)) {
			throw new BusinessException(ResourceBundleHelper.getString(result.value()));
		}else{
			 userDetail = userDetailDao.getUserDetailByName(userName);
		}
		return userDetail;
	}

	@Override
	public String getUserNameByOpenId(String openId, String type) {
		return userDetailDao.getUserNameByOpenId(openId, type);
	}

	@Override
	public PageSupport<UserDetail> getUserDetailListPage(String haveShop, DataSortResult result, String curPageNO,
			UserDetail userDetail) {
		return userDetailDao.getUserDetailListPage(haveShop,result,curPageNO,userDetail);
	}

	@Override
	public void updateLastLoginTime(String userId) {
		userDetailDao.updateLastLoginTime(userId);
	}

	@Override
	public User getByOpenId(String openId) {
		return userDetailDao.getByOpenId(openId);
	}

	@Override
	public void bingUserOpenId(String userId, String openId) {
		 userDetailDao.bingUserOpenId(userId,openId);
	}

    @Override
    public void bingUserOpenId(String userId, String openId, String unionid) {
        userDetailDao.bingUserOpenId(userId, openId, unionid);
    }


    @Override
	public int batchOfflineUser(String userIds) {
		StringBuffer sqlShopOff=new StringBuffer();
		StringBuffer sqlUserOff=new StringBuffer();
		
		sqlUserOff.append("UPDATE ls_user SET enabled=0 WHERE id IN(");
		sqlShopOff.append("UPDATE ls_shop_detail SET status="+ShopStatusEnum.OFFLINE.value()+" WHERE user_id IN(");
		
		List<String> updateUserPara=new ArrayList<>();
		List<String> updateShopPara=new ArrayList<>();
		
		String[] userIdList=userIds.split(",");
		for(String userId:userIdList){
			if(isHaveShop(userId)){
				sqlShopOff.append("?,");
				updateShopPara.add(userId);
			}
			sqlUserOff.append("?,");
			updateUserPara.add(userId);
		}
		sqlUserOff.deleteCharAt(sqlUserOff.length()-1);
		sqlUserOff.append(")");
		
		sqlShopOff.deleteCharAt(sqlShopOff.length()-1);
		sqlShopOff.append(")");
		
		if(AppUtils.isNotBlank(updateShopPara)){
			shopDetailDao.update(sqlShopOff.toString(), updateShopPara.toArray());
		}
		return userDetailDao.update(sqlUserOff.toString(), updateUserPara.toArray());
	}

	@Override
	public boolean verifyMobileCode(String mobile, String mobileCode) {
		 return userDetailDao.verifyMobileCode(mobile,mobileCode);
	}

	@Override
	public String getUserNameByPhoneOrUsername(String userMobile, String userName) {
		return userDetailDao.getUserNameByPhoneOrUsername(userMobile,userName);
	}

	@Override
	public int updateAvailablePredeposit(Double updatePredeposit,Double originalPredeposit, String userId) {
		return userDetailDao.updateAvailablePredeposit(updatePredeposit, originalPredeposit, userId);
	}

	@Override
	public void expiredVerificationCode(String mobile, String mobileCode) {
		userDetailDao.expiredVerificationCode(mobile,mobileCode);
	}

	@Override
	public List<UserDetail> getUserDetailByPhone(String userPhone, String userId) {
		String sql="SELECT user_id as userId,user_mobile as userMobile from ls_usr_detail WHERE  user_mobile LIKE ? and user_id !=? ";
		List<UserDetail> userDetails = userDetailDao.queryLimit(sql, UserDetail.class, 0, 5,"%"+userPhone+"%",userId);

		return userDetails;
	}

}
