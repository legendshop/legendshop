/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MarketingRuleDao;
import com.legendshop.model.entity.MarketingRule;
import com.legendshop.spi.service.MarketingRuleService;
import com.legendshop.util.AppUtils;

/**
 * 营销活动规则服务.
 */
@Service("marketingRuleService")
public class MarketingRuleServiceImpl  implements MarketingRuleService{

	@Autowired
    private MarketingRuleDao marketingRuleDao;

    public MarketingRule getMarketingRule(Long id) {
        return marketingRuleDao.getMarketingRule(id);
    }

    public void deleteMarketingRule(MarketingRule marketingRule) {
        marketingRuleDao.deleteMarketingRule(marketingRule);
    }

    @Override
    public void deleteMarketingRuleByMarketing(Long marketingId) {
        marketingRuleDao.deleteMarketingRuleByMarketing(marketingId);
    }

    public Long saveMarketingRule(MarketingRule marketingRule) {
        if (!AppUtils.isBlank(marketingRule.getRuleId())) {
            updateMarketingRule(marketingRule);
            return marketingRule.getRuleId();
        }
        return marketingRuleDao.saveMarketingRule(marketingRule);
    }

    public void updateMarketingRule(MarketingRule marketingRule) {
        marketingRuleDao.updateMarketingRule(marketingRule);
    }

}
