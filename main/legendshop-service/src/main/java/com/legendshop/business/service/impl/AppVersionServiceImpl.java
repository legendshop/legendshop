package com.legendshop.business.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppVersion;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.AppVersionService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;

@Service("appVersionService")
public class AppVersionServiceImpl implements AppVersionService {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AppVersionServiceImpl.class);
	
	@Autowired
	private ConstTableDao constTableDao ;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public AppVersion getAppVersion() {
    	ConstTable constTable = constTableDao.getConstTablesBykey("APP_VERSION","APP_VERSION");
    	if(null == constTable){
    		return null;
    	}
    	String JSON = constTable.getValue();
    	AppVersion appVersion = JSONObject.parseObject(JSON, AppVersion.class);
		return appVersion;
	}

	@Override
	public String uploadApk(MultipartFile apkFile, String version, String type) throws IOException {
    	AppVersion appVersion = this.getAppVersion();
    	
    	if(null == appVersion){
    		return "对不起, 系统数据出错, 请联系相关技术人员!";
    	}
    	
		
    	String apkFilePath = this.uploadApkFile(apkFile, version, type);
    	if("user".equals(type)){
    		
    		if(version.equals(appVersion.getAndroidUserVersion())){
    			return "对不起, 版本号不能和上次一样!";
    		}
    		
    		String oldApkFile = appVersion.getAndroidUserApk();
    		if(AppUtils.isNotBlank(oldApkFile)){
    			this.deleteFile(oldApkFile);
    		}
    		appVersion.setAndroidUserVersion(version);
    		appVersion.setAndroidUserApk(apkFilePath);
    	}else if("shop".equals(type)){
    		
    		if(version.equals(appVersion.getAndroidShopVersion())){
    			return "对不起, 版本号不能和上次一样!";
    		}
    		
    		String oldApkFile = appVersion.getAndroidShopApk();
    		if(AppUtils.isNotBlank(oldApkFile)){
    			this.deleteFile(oldApkFile);
    		}
    		appVersion.setAndroidShopApk(apkFilePath);
    		appVersion.setAndroidShopVersion(version);
    	}else{
    		return "对不起, 非法的type参数!";
    	}
    	
    	String value = JSONObject.toJSONString(appVersion);
    	constTableDao.updateConstTableByTepe("APP_VERSION", "APP_VERSION", value);
    	
    	return Constants.SUCCESS;
	}
	
	@Override
	public String deleteApk(String type) {
    	AppVersion appVersion = this.getAppVersion();
    	
    	if(null == appVersion){
    		return "对不起, 系统数据出错, 请联系相关技术人员!";
    	}

    	if("user".equals(type)){
    		String oldApkFile = appVersion.getAndroidUserApk();
    		if(AppUtils.isNotBlank(oldApkFile)){
    			this.deleteFile(oldApkFile);
    		}
    		appVersion.setAndroidUserVersion(null);
    		appVersion.setAndroidUserApk(null);
    	}else if("shop".equals(type)){
    		String oldApkFile = appVersion.getAndroidShopApk();
    		if(AppUtils.isNotBlank(oldApkFile)){
    			this.deleteFile(oldApkFile);
    		}
    		appVersion.setAndroidShopVersion(null);
    		appVersion.setAndroidShopApk(null);
    	}else{
    		return "对不起, 非法的type参数!";
    	}
    	
    	String value = JSONObject.toJSONString(appVersion);
    	constTableDao.updateConstTableByTepe("APP_VERSION", "APP_VERSION", value);
    	
    	return Constants.SUCCESS;
	}
	
	/**
	 * 保存文件
	 * @throws IOException 
	 */
	public String uploadApkFile(MultipartFile file, String version, String type) throws IOException {
		File dirPath = new File(SystemUtil.getSystemRealPath() + propertiesUtil.getAppFileSubPath());
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		
		String fileName = type + "-" + version + "-release.apk";
		File f = new File(dirPath, fileName);
		InputStream is = file.getInputStream();
		OutputStream fos = null;
		try {
			fos =new BufferedOutputStream( new FileOutputStream(f));
			//3、文件拷贝   循环+读取+写出
			int len = 0;
            byte[] flush =new byte[1024];
            while ((len = is.read(flush)) > 0) {
				fos.write(flush, 0, len);
			}
            fos.flush(); //强制刷出
		} catch (Exception e) {
			
			LOGGER.error("upload apk file:", e);
			return null;
		} finally {
			try {
				if(fos!=null){
					fos.close();
				}
				
			} catch (Exception e) {
			}
			try {
				if(is!=null){
					is.close();
				}
			} catch (Exception e2) {
			}
		}
		
		return fileName;
	}

	/**
	 * 删除文件.
	 * 
	 * @param fileName
	 *            文件名称
	 * @param backup 是否备份大图片
	 * @return
	 *  0： 删除成功，
	 * -1：文件存在，但删除失败
	 * 1： 没有文件
	*	2： 未知原因失败
	 */
	private int deleteFile(String fileName) {
		try {
			String dir =  SystemUtil.getSystemRealPath() + propertiesUtil.getAppFileSubPath();
			File f = new File(dir, fileName);
			if (f.exists()) {// 检查是否存在
				boolean result = f.delete();// 删除文件
				if(result){
					return 0; // 删除成功
				}else{
					return -1; // 删除失败
				}
				
			} else {
				LOGGER.warn("没有该文件：{}", fileName);
				return 1;// 没有文件
			}
		} catch (Exception e) {
			LOGGER.warn("删除文件 {} 失败", fileName);
			return 2;// 失败
		}
	}

	@Override
	public String updateIOSAppstore(String url, String type) {
    	AppVersion appVersion = this.getAppVersion();
    	
    	if(null == appVersion){
    		return "对不起, 系统数据出错, 请联系相关技术人员!";
    	}
    	
    	if("user".equals(type)){
    		appVersion.setIosUserAppstore(url);
    	}else if("shop".equals(type)){
    		appVersion.setIosShopAppstore(url);
    	}else{
    		return "对不起, 非法的type参数!";
    	}
    	
    	String value = JSONObject.toJSONString(appVersion);
    	constTableDao.updateConstTableByTepe("APP_VERSION", "APP_VERSION", value);
    	
    	return Constants.SUCCESS;
	}
}
