/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PrintTmplDao;
import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PrintTmpl;
import com.legendshop.util.AppUtils;

/**
 *打印模板Dao
 */
@Repository
public class PrintTmplDaoImpl extends GenericDaoImpl<PrintTmpl, Long> implements PrintTmplDao  {

	private final static String PRINTTMPL_DELVID_SQL="select ls_print_tmpl.* from ls_print_tmpl left join ls_dvy_type on ls_dvy_type.printtemp_id=ls_print_tmpl.prt_tmpl_id where ls_dvy_type.dvy_type_id=?";
     
	public List<PrintTmpl> getPrintTmplByShop(Long shopId){
		EntityCriterion cq = new EntityCriterion();
		cq.add(cq.or(Restrictions.eq("shopId", shopId), Restrictions.eq("isSystem", 1)));
   		return this.queryByProperties(cq);
    }

	public PrintTmpl getPrintTmpl(Long id){
		return getById(id);
	}
	
    public int deletePrintTmpl(PrintTmpl printTmpl){
    	return delete(printTmpl);
    }
	
	public Long savePrintTmpl(PrintTmpl printTmpl){
		return save(printTmpl);
	}
	
	public int updatePrintTmpl(PrintTmpl printTmpl){
		return update(printTmpl);
	}
	
	@Override
	public PrintTmpl getPrintTmplByDelvId(Long delvId) {
		return get(PRINTTMPL_DELVID_SQL,PrintTmpl.class,delvId);
	}

	@Override
	public PageSupport<PrintTmpl> getPrintTmplPage(String curPageNO, PrintTmpl printTmpl) {
		CriteriaQuery cq = new CriteriaQuery(PrintTmpl.class, curPageNO);
		if (!AppUtils.isBlank(printTmpl.getPrtTmplTitle())) {
			cq.like("prtTmplTitle", "%" + printTmpl.getPrtTmplTitle().trim() + "%");
		}
		return queryPage(cq);
	}

	@Override
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(PrintTmpl.class, "1");
		Criterion cqs= cq.or(Restrictions.eq("shopId",shopId), Restrictions.eq("isSystem", "1"));
		cq.add(cqs);
		cq.addDescOrder("prtTmplId");
		cq.addAscOrder("isSystem");
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId, String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(PrintTmpl.class, curPageNO);
		Criterion cqs= cq.or(Restrictions.eq("shopId",shopId), Restrictions.eq("isSystem", "1"));
		cq.add(cqs);
		cq.addDescOrder("prtTmplId");
		cq.addAscOrder("isSystem");
		return queryPage(cq);
	}
	
 }
