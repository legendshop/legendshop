/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.mergeGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.util.AppUtils;

/**
 * 商品拼团下单.
 *
 */
@Service("orderMergeGroupCartResolver")
public class OrderMergeGroupCartResolver extends AbstractAddOrder  implements IOrderCartResolver {

	@Override
	public List<String> addOrder(UserShopCartList userShopCartList) {
		List<String> subIds=super.submitOrder(userShopCartList);//返回每个店铺的订单号的集合
		if(AppUtils.isBlank(subIds)){
			return null;
		}
		return subIds;
	}

	@Override
	public UserShopCartList queryOrders(Map<String, Object> params) {
		
		String userId=(String) params.get("userId");
		String userName=(String) params.get("userName");
		Long mergeGroupId=(Long) params.get("mergeGroupId");
		Long productId=(Long) params.get("productId");
		Long skuId=(Long) params.get("skuId");
		Integer number=(Integer) params.get("number");
		Long adderessId = (Long) params.get("adderessId");//用户收货地址
		
		ShopCartItem shopCartItem=basketService.findMergeGroupCart(mergeGroupId,productId,skuId);
		
		if(shopCartItem==null){
			throw new BusinessException("数据发送改变,请重新下单购物！");
		}
		shopCartItem.setBasketCount(number);
		List<ShopCartItem> cartItems=new ArrayList<ShopCartItem>(1);
		cartItems.add(shopCartItem);
		UserShopCartList userShopCartList=super.queryOrders(userId, userName, cartItems, adderessId,CartTypeEnum.MERGE_GROUP.toCode());
		if(userShopCartList==null){
			return null;
		}
		
		userShopCartList.setActiveId(mergeGroupId);
		userShopCartList.setType(CartTypeEnum.MERGE_GROUP.toCode());
		return userShopCartList;
	}
	
	
}
