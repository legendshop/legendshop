package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PriceRangeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.spi.service.PriceRangeService;
import com.legendshop.util.AppUtils;

/**
 * 统计报表 价格区间服务
 */
@Service("priceRangeService")
public class PriceRangeServiceImpl implements PriceRangeService{
	
	@Autowired
	private PriceRangeDao priceRangeDao;

	@Override
	public List<PriceRange> queryPriceRange(Long shopId,Integer type) {
		return priceRangeDao.queryPriceRange(shopId,type);
	}

	@Override
	public PriceRange getPriceRange(Long id) {
		return priceRangeDao.getPriceRange(id);
	}

	@Override
	public void deletePriceRange(PriceRange priceRange) {
		priceRangeDao.deletePriceRange(priceRange);
	}

	@Override
	public Long savePriceRange(PriceRange priceRange) {
		return priceRangeDao.savePriceRange(priceRange);
	}

	@Override
	public void updatePriceRange(PriceRange priceRange) {
		priceRangeDao.updatePriceRange(priceRange);
	}

	@Override
	public void savePriceRangeList(List<PriceRange> priceRangeList,Long shopId) {
		List<PriceRange> updateList = new ArrayList<PriceRange>();
		List<PriceRange> saveList = new ArrayList<PriceRange>();
		for (PriceRange priceRange : priceRangeList) {
			priceRange.setShopId(shopId);
			priceRange.setRecDate(new Date());
			if(AppUtils.isNotBlank(priceRange.getId())){
				updateList.add(priceRange);
			}else{
				saveList.add(priceRange);
			}
		}
		
		if(AppUtils.isNotBlank(updateList)){
			priceRangeDao.updatePriceRangeList(updateList);
		}
		
		if(AppUtils.isNotBlank(saveList)){
			priceRangeDao.savePriceRangeList(saveList);
		}
	}

	@Override
	public PageSupport<PriceRange> getPriceRange(String curPageNO, Long shopId, Integer type) {
		return priceRangeDao.getPriceRange(curPageNO,shopId,type);
	}
}