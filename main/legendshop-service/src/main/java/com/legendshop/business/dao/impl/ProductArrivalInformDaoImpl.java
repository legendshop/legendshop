package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProductArrivalInformDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.entity.ProdArrivalInform;
import com.legendshop.util.AppUtils;

/**
 * 到货通知
 * 
 */
@Repository
public class ProductArrivalInformDaoImpl extends GenericDaoImpl<ProdArrivalInform, Long> implements ProductArrivalInformDao{
	
	@Override
	public Long saveProdArriInfo(ProdArrivalInform prodArrivalInform) {
		return this.save(prodArrivalInform);
	}
	
	/**
	 * 获得用户已经添加到货通知的商品
	 * @param userId 用户id
	 * @param skuId 
	 * @param status 状态。查询尚未通知的
	 * @return
	 */
	@Override
	public ProdArrivalInform getAlreadySaveUser(String userId,long skuId,int status){
		ProdArrivalInform user = this.get("SELECT * FROM ls_prod_arrival_inform WHERE userId = ? AND skuId = ?  AND  STATUS = ?", ProdArrivalInform.class, userId,skuId,status);
		return user;
	}
	
	/**
	 * 根据商品skuid 仓库id查询设置到货通知得顾客
	 */
	@Override
	public List<ProdArrivalInform> getUserBySkuIdAndWhId(long skuId){
		List<ProdArrivalInform> list = this.query("SELECT * FROM ls_prod_arrival_inform a WHERE  a.skuId = ? AND a.status = 0", ProdArrivalInform.class, skuId);
		return list;
	}

	@Override
	public PageSupport<ProdArrivalInformDto> getProdArrival(Long shopId, String productName, String curPageNO) {
		SimpleSqlQuery hql = new SimpleSqlQuery(ProdArrivalInformDto.class);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if(AppUtils.isNotBlank(productName)){
			map.put("productName", "%"+productName+"%");
		}
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("shop.getCountAllProdArriInformByShopId", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("shop.getAllProdArriInformByShopId", map));
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<ProdArrivalInformDto> getSelectArrival(String curPageNO, String skuId,
			Long shopId) {
		SimpleSqlQuery hql = new SimpleSqlQuery(ProdArrivalInformDto.class);
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		QueryMap map = new QueryMap();
		map.put("skuId", skuId);
		map.put("shopId", shopId);
		hql.setPageSize(10);
		hql.setCurPage(curPageNO);
		hql.setParam(map.toArray());
		hql.setAllCountString(ConfigCode.getInstance().getCode("shop.getCountArriInformUserBySkuIdAndWhId", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("shop.getArriInformUserBySkuIdAndWhId", map));
		return querySimplePage(hql);
	}
}
