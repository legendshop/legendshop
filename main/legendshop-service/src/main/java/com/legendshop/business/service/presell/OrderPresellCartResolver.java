/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.presell;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.presell.PresellProdDao;
import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.presell.PreSellShopCartItem;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 预售订单
 *
 */
@Service("orderPresellCartResolver")
public class OrderPresellCartResolver extends AbstractAddOrder implements IOrderCartResolver {

	@Autowired
	private PresellProdDao presellProdDao;

	@Autowired
	private PresellSubDao presellSubDao;

	/**
	 * 查询预售订单
	 */
	@Override
	public UserShopCartList queryOrders(Map<String, Object> params) {
		String userId = (String) params.get("userId");
		String userName = (String) params.get("userName");
		Integer number = (Integer) params.get("number");
		Long presellId = (Long) params.get("presellId");
		Long adderessId = (Long) params.get("adderessId");// 用户收货地址
		Long skuId = (Long) params.get("skuId");// 预售skuId

		PreSellShopCartItem shopCartItem = (PreSellShopCartItem) presellProdDao.findPreSellCart(presellId,skuId);
		if (shopCartItem == null) {
			throw new BusinessException("下单失败,请联系商城管理员,确认该拍物是否已经下架！");
		}
		shopCartItem.setCalculateMarketing(false);
		shopCartItem.setCheckSts(1); // 检查状态
		shopCartItem.setBasketCount(number);
		if (shopCartItem.getPayPctType() != null && shopCartItem.getPayPctType().intValue() == 0) { // 支付方式,0:全额,1:订金
			shopCartItem.setPrice(shopCartItem.getPrePrice());// 设置预售价格
		}
		shopCartItem.setPromotionPrice(shopCartItem.getPrePrice());
		List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>(1);
		cartItems.add(shopCartItem);
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, cartItems, adderessId,CartTypeEnum.PRESELL.toCode());
		if (userShopCartList == null) {
			return null;
		}
		userShopCartList.setType(CartTypeEnum.PRESELL.toCode());
		return userShopCartList;

	}

	/**
	 * 下单
	 */
	@Override
	public List<String> addOrder(UserShopCartList userShopCartList) {
		List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
		List<String> subNembers = super.submitOrder(userShopCartList);
		if (AppUtils.isBlank(subNembers)) {
			return null;
		}
		for (Iterator<ShopCarts> shopIterator = shopCarts.iterator(); shopIterator.hasNext();) {
			ShopCarts shop = shopIterator.next();
			/** 组装SubItem */
			PreSellShopCartItem basket = (PreSellShopCartItem) shop.getCartItems().get(0); // 预售订单只有一个商品
			PresellSub presellSub = new PresellSub();
			presellSub.setSubId(shop.getSubId());
			presellSub.setSubNumber(shop.getSubNember());
			presellSub.setPayPctType(basket.getPayPctType());
			
			if (basket.getPayPctType() != null && basket.getPayPctType().intValue() == 1) {// 支付方式,0:全额,1:订金
				double percent =Double.parseDouble(basket.getPreDepositPrice().toString())/100;//定金百分比
				Double prsellPrice =basket.getPrePrice()*basket.getBasketCount();//预售价
				Double depositPrice = prsellPrice*percent;//定金金额
				Double finalPrice = prsellPrice-depositPrice;
				presellSub.setPreDepositPrice(new BigDecimal(depositPrice));
//				//计算尾款
//				Double finalPrice = Arith.mul(Arith.sub(basket.getPrePrice(), basket.getPreDepositPrice()), basket.getBasketCount());
//				Double totalFinalPrice = Arith.add(finalPrice, shop.getFreightAmount());
				
				presellSub.setFinalPrice(new BigDecimal(finalPrice));
			} else {// 全额就没有尾款
				presellSub.setPreDepositPrice(new BigDecimal(shop.getShopActualTotalCash()));
				presellSub.setFinalPrice(new BigDecimal(0));
			}
			
			presellSub.setPreSaleStart(basket.getPreSaleStart());
			presellSub.setPreSaleEnd(basket.getPreSaleEnd());
			presellSub.setFinalMStart(basket.getFinalMStart());
			presellSub.setFinalMEnd(basket.getFinalMEnd());
			presellSub.setIsPayDeposit(0);// 未支付
			presellSub.setIsPayFinal(0);
			presellSubDao.savePresellSub(presellSub);
		}
		return subNembers;
	}

}
