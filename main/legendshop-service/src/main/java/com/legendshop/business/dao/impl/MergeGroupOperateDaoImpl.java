/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MergeGroupOperateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.entity.MergeGroupOperate;

/**
 * The Class MergeGroupOperateDaoImpl.
 * 每个团的运营信息Dao实现类
 */
@Repository
public class MergeGroupOperateDaoImpl extends GenericDaoImpl<MergeGroupOperate, Long> implements MergeGroupOperateDao  {

	/**
	 *  根据Id获取每个团的运营信息
	 */
	public MergeGroupOperate getMergeGroupOperate(Long id){
		return getById(id);
	}

	/**
	 *  删除每个团的运营信息
	 */	
	public int deleteMergeGroupOperate(MergeGroupOperate mergeGroupOperate){
		return delete(mergeGroupOperate);
	}

	/**
	 *  保存每个团的运营信息
	 */		
	public Long saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate){
		return save(mergeGroupOperate);
	}
	
	@Override
	public void saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate, Long operateId) {
		save(mergeGroupOperate, operateId);
	}

	/**
	 *  更新每个团的运营信息
	 */		
	public int updateMergeGroupOperate(MergeGroupOperate mergeGroupOperate){
		return update(mergeGroupOperate);
	}

	/**
	 *  获取本团运营信息
	 */
	public MergeGroupOperate findMergeGroupOperate(Long shopId, String addNumber) {
		String sql ="SELECT o.id AS id,o.shop_id AS shopId,o.merge_id AS mergeId,o.add_number AS addNumber,o.number AS number,o.status AS STATUS,"+
					"o.create_time AS createTime,o.end_time AS endTime,o.is_head_free AS isHeadFree,g.people_number AS peopleNumber FROM ls_merge_group_operate o LEFT JOIN "+
					"ls_merge_group g ON o.merge_id=g.id WHERE o.shop_id=? AND o.add_number=?";
		return this.get(sql, MergeGroupOperate.class, shopId,addNumber);
	}

	//获取团运营信息，用于校检人数是否满团
	public MergeGroupJoinDto getMergeGroupOperateByAddNumber(Long activeId, String addNumber) {
		String sql ="SELECT g.id AS mergeId,g.status AS mergeStatus,g.end_time AS endTime,g.people_number AS peopleNumber,"+
					"o.id AS operateId,o.number AS number,o.status AS operateStatus "+
					"FROM ls_merge_group_operate o LEFT JOIN ls_merge_group g ON o.merge_id=g.id "+
					"WHERE o.merge_id=? AND o.add_number=?";
		return this.get(sql, MergeGroupJoinDto.class,activeId,addNumber);
	}

	//校检用户是否参加了该团
	public Long getMergeGroupAddByUserId(String userId, String addNumber) {
		String sql ="SELECT count(1) FROM ls_merge_group_operate o LEFT JOIN ls_merge_group_add a ON o.id=a.operate_id"+
					" WHERE o.add_number=? AND a.user_id=?";
		return this.getLongResult(sql, addNumber,userId);
	}

	public MergeGroupOperate getMergeGroupOperateByAddNumber(String addNumber) {
		return this.getByProperties(new EntityCriterion().eq("addNumber", addNumber));
	}

	public void updateMergeGroupOperateList(List<MergeGroupOperate> cancalOperates) {
		update(cancalOperates);
	}

	public List<MergeGroupOperate> findHaveInMergeGroupOperate() {
		return this.queryByProperties(new EntityCriterion().eq("status", MergeGroupAddStatusEnum.IN.value()));
	}

	public List<MergeGroupOperate> findHaveInMergeGroupOperate(Long mergeId) {
		return this.queryByProperties(new EntityCriterion().eq("status", MergeGroupAddStatusEnum.IN.value()).eq("mergeId", mergeId));
	}

 }
