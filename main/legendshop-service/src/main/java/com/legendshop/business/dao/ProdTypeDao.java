/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdTypeBrand;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.model.entity.ProdTypeProperty;

/**
 *商品类型Dao
 */
public interface ProdTypeDao extends Dao<ProdType, Long> {

	public abstract ProdType getProdType(Long id);
	
    public abstract int deleteProdType(ProdType prodType);
	
	public abstract Long saveProdType(ProdType prodType);
	
	public abstract int updateProdType(ProdType prodType);
	
	public void saveTypeProp(List<ProdTypeProperty> propertyList,Long id);
	
	public void saveTypeBrand(List<ProdTypeBrand> brandList,Long id);
	
	public void saveParameterProperty(List<ProdTypeParam> propertyList,Long prodTypeId);
	
	public void deleteProdProp(Long propId,Long prodTypeId);
	
	public void deleteBrand(Long brandId,Long prodTypeId);
	
	public void deleteParameterProp(Long propId,Long prodTypeId);
	
	public List<KeyValueEntity> loadProdType();

	public abstract List<ProdType> getProdTypeListWithId(Long tpyeId);

	public abstract PageSupport<ProdType> getProdTypePage(String typeName);

	public abstract PageSupport<ProdType> queryProdTypePage(String curPageNO, ProdType prodType, Long categoryId);

	public abstract PageSupport<ProdType> getProdTypePage(String curPageNO, String name);

	public abstract PageSupport<ProdType> getProdTypeByName(String typeName);

	/**
	 * 获取类型下的规格关联的sku
	 * @param proTypeId
	 * @param propId
	 * @return
	 */
	public abstract List<Long> ifDeleteProductProperty(Long proTypeId, Long propId);
 }
