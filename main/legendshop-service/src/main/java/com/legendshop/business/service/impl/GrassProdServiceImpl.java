/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassProd;
import com.legendshop.spi.service.GrassProdService;
import com.legendshop.util.AppUtils;

/**
 * The Class GrassProdServiceImpl.
 *  种草文章关联商品表服务实现类
 */
@Service("grassProdService")
public class GrassProdServiceImpl implements GrassProdService{

    /**
     *
     * 引用的种草文章关联商品表Dao接口
     */
	@Autowired
    private GrassProdDao grassProdDao;
   
   	/**
	 *  根据Id获取种草文章关联商品表
	 */
    public GrassProd getGrassProd(Long id) {
        return grassProdDao.getGrassProd(id);
    }
    
    /**
	 *  根据Id删除种草文章关联商品表
	 */
    public int deleteGrassProd(Long id){
    	return grassProdDao.deleteGrassProd(id);
    }

   /**
	 *  删除种草文章关联商品表
	 */ 
    public int deleteGrassProd(GrassProd grassProd) {
       return  grassProdDao.deleteGrassProd(grassProd);
    }

   /**
	 *  保存种草文章关联商品表
	 */	    
    public Long saveGrassProd(GrassProd grassProd) {
        if (!AppUtils.isBlank(grassProd.getId())) {
            updateGrassProd(grassProd);
            return grassProd.getId();
        }
        return grassProdDao.saveGrassProd(grassProd);
    }

   /**
	 *  更新种草文章关联商品表
	 */	
    public void updateGrassProd(GrassProd grassProd) {
        grassProdDao.updateGrassProd(grassProd);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassProd> queryGrassProd(String curPageNO, Integer pageSize){
     	return grassProdDao.queryGrassProd(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassProdDao(GrassProdDao grassProdDao) {
        this.grassProdDao = grassProdDao;
    }

	@Override
	public void deleteByGrassId(Long graid) {
        grassProdDao.deleteByGrassId(graid);
	}
    
}
