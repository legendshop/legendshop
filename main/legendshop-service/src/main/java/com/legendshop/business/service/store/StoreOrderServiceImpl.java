/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.store;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.StoreOrderDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.dto.StoreOrderSerachDto;
import com.legendshop.model.dto.order.StoreOrderDto;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.spi.service.StoreOrderService;
import com.legendshop.util.AppUtils;

/**
 * 门店订单服务接口实现类
 */
@Service("storeOrderService")
public class StoreOrderServiceImpl  implements StoreOrderService{
    
	@Autowired
	private StoreOrderDao storeOrderDao;
    
    /**
	 * 查看门店订单
	 */
    public StoreOrder getDeliveryOrder(Long id) {
        return storeOrderDao.getDeliveryOrder(id);
    }

    /** 
     * 删除门店订单 
     */
    public void deleteDeliveryOrder(StoreOrder deliveryOrder) {
    	storeOrderDao.deleteDeliveryOrder(deliveryOrder);
    }

    /**
     * 保存门店订单 
     */
    public Long saveDeliveryOrder(StoreOrder deliveryOrder) {
        if (!AppUtils.isBlank(deliveryOrder.getId())) {
            updateDeliveryOrder(deliveryOrder);
            return deliveryOrder.getId();
        }
        return storeOrderDao.saveDeliveryOrder(deliveryOrder);
    }

    /**
     * 更新门店订单 
     */
    public void updateDeliveryOrder(StoreOrder deliveryOrder) {
    	storeOrderDao.updateDeliveryOrder(deliveryOrder);
    }

    /**
     * 获取门店订单Dtos
     */
	@Override
	@Cacheable(value="StoreOrderDtoList",key="#storeId",
    condition="#paramdto.curPageNO < 3 " +
	" && #paramdto.paramName==null && #paramdto.orderStatus==null "+
    " && #paramdto.paramValue==null")
	public PageSupport<StoreOrderDto> getDeliveryOrderDtos(Long shopId,Long storeId,StoreOrderSerachDto paramdto) {
		return constructPageSupport(shopId,storeId,paramdto);
	}
	
	/**
	 * Page工具类
	 * @param shopId
	 * @param storeId
	 * @param paramdto
	 * @return
	 */
	public PageSupport<StoreOrderDto> constructPageSupport(Long shopId,Long storeId,StoreOrderSerachDto paramdto){
		int pageSize=10;
		int offset=(paramdto.getCurPageNO()-1)*pageSize;
		QueryMap queryMap=new QueryMap();
		List<Object> args=new ArrayList<Object>();
		args.add(shopId);
		queryMap.put("shopId", shopId);
		args.add(storeId);
		queryMap.put("storeId", storeId);
		if(AppUtils.isNotBlank(paramdto.getOrderStatus())){
			args.add(paramdto.getOrderStatus());
			queryMap.put("orderStatus", paramdto.getOrderStatus());
		}
		if(AppUtils.isNotBlank(paramdto.getParamName()) && AppUtils.isNotBlank(paramdto.getParamValue())){
			args.add(paramdto.getParamValue());
			if(paramdto.getParamName().equals("order_sn")){
				queryMap.put("orderSn", paramdto.getParamValue());
			}
			if(paramdto.getParamName().equals("chain_code")){
				queryMap.put("code", paramdto.getParamValue());
			}
			if(paramdto.getParamName().equals("buyer_phone")){
				queryMap.put("reciverMobile", paramdto.getParamValue());
			}
			if(paramdto.getParamName().equals("reciver_name")){
				queryMap.put("name", paramdto.getParamValue());
			}
		}
		queryMap.put("offset", offset);
		queryMap.put("pageSize", pageSize);
		PageSupport<StoreOrderDto> ps=storeOrderDao.getStorePageSupport(queryMap,args,paramdto.getCurPageNO(),pageSize);
		return ps;
	}

	/** 
	 * 根据订单对象编号获取门店订单 
	 */
	@Override
	public StoreOrder getDeliveryOrderBySubNumber(String subNumber) {
		return storeOrderDao.getDeliveryOrderBySubNumber(subNumber);
	}

	/**
	 * 更新门店订单状态
	 */
	@Override
	public int updateDeliveryOrderStatus(String subNumber,Integer status) {
		return storeOrderDao.updateDeliveryOrderStatus(subNumber,status);
	}
}
