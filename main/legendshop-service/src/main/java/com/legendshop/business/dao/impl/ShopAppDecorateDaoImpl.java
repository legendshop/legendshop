/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopAppDecorateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ShopAppDecorate;
import com.legendshop.util.AppUtils;

/**
 * The Class ShopAppDecorateDaoImpl. 移动端店铺主页装修Dao实现类
 */
@Repository
public class ShopAppDecorateDaoImpl extends GenericDaoImpl<ShopAppDecorate, Long> implements ShopAppDecorateDao{

	/**
	 * 根据Id获取移动端店铺主页装修
	 */
	public ShopAppDecorate getShopAppDecorate(Long id){
		return getById(id);
	}

	/**
	 *  删除移动端店铺主页装修
	 *  @param shopAppDecorate 实体类
	 *  @return 删除结果
	 */	
    public int deleteShopAppDecorate(ShopAppDecorate shopAppDecorate){
    	return delete(shopAppDecorate);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteShopAppDecorate(Long id){
		return deleteById(id);
	}

	/**
	 * 保存移动端店铺主页装修
	 */
	public Long saveShopAppDecorate(ShopAppDecorate shopAppDecorate){
		return save(shopAppDecorate);
	}

	/**
	 *  更新移动端店铺主页装修
	 */		
	public int updateShopAppDecorate(ShopAppDecorate shopAppDecorate){
		return update(shopAppDecorate);
	}

	
	/**
	 * 查询移动端店铺主页装修列表
	 */
	@Override
	public PageSupport<ShopAppDecorate> queryShopAppDecorate(String curPageNO, Integer pageSize,ShopAppDecorate shopAppDecorate){ 
		
		QueryMap map = new QueryMap();
		SimpleSqlQuery query = new SimpleSqlQuery(ShopAppDecorate.class);
		query.setPageSize(pageSize);
		query.setCurPage(curPageNO);
		
		if (AppUtils.isNotBlank(shopAppDecorate.getShopId())) {
			map.put("shopId", shopAppDecorate.getShopId());
		}

		query.setAllCountString(ConfigCode.getInstance().getCode("admin.queryShopAppDecorateCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("admin.queryShopAppDecorate", map));
		query.setParam(map.toArray());
		return querySimplePage(query);
		
	}

	/**
	 * 根据店铺id获取移动店铺首页装修数据
	 */
	@Override
	public ShopAppDecorate getShopAppDecorateByShopId(Long shopId) {
		
		EntityCriterion cq = new EntityCriterion();
		cq.eq("shopId", shopId);
		return getByProperties(cq);
	}

	/**
	 * 获取正常上线的店铺首页装修数据
	 */
	@Override
	public String getReleaseDecorateDataByShopId(Long shopId) {
		
		String sql = "SELECT release_data FROM ls_shop_app_decorate WHERE status = 1 AND shop_id = ?";
		return get(sql, String.class, shopId);
	}

}
