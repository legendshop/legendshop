/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.TagItemNewsDao;
import com.legendshop.model.entity.TagItemNews;
import com.legendshop.spi.service.TagItemNewsService;
import com.legendshop.util.AppUtils;

/**
 *标签里的文章
 */
@Service("tagItemNewsService")
public class TagItemNewsServiceImpl  implements TagItemNewsService{

	@Autowired
    private TagItemNewsDao tagItemNewsDao;

    public TagItemNews getTagItemNews(Long id) {
        return tagItemNewsDao.getTagItemNews(id);
    }

    public void deleteTagItemNews(TagItemNews tagItemNews) {
        tagItemNewsDao.deleteTagItemNews(tagItemNews);
    }

    public Long saveTagItemNews(TagItemNews tagItemNews) {
        if (!AppUtils.isBlank(tagItemNews.getId())) {
            updateTagItemNews(tagItemNews);
            return tagItemNews.getId();
        }
        return tagItemNewsDao.saveTagItemNews(tagItemNews);
    }

    public void updateTagItemNews(TagItemNews tagItemNews) {
        tagItemNewsDao.updateTagItemNews(tagItemNews);
    }

	@Override
	public List<TagItemNews> getTagItemNewsByNewsId(Long newsId) {
		return tagItemNewsDao. getTagItemNewsByNewsId(newsId);
	}
}
