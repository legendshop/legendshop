/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubFloorItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.model.entity.SubFloorItem;
/**
 * 首页楼层服务
 */
@Repository
public class SubFloorItemDaoImpl extends GenericDaoImpl<SubFloorItem, Long> implements SubFloorItemDao {
	
	private static String sqlForGetItemLimitBysfId = "select s.sfi_id,p.name as productName,s.sf_id,s.seq,p.pic as productPic,p.cash as productCash,p.price as productPrice,p.prod_id as productId from ls_sub_floor_item s,ls_prod p " +
			"  where s.refer_id = p.prod_id and s.sf_id = ? order by s.seq";
	
	private static String sqlForGetAllSubFloorItem = "SELECT s.sfi_id as sfiId,s.refer_id as referId,p.name as productName,s.sf_id as sfId,s.seq as seq ,p.pic as productPic from ls_sub_floor_item s,ls_prod p " +
	"where s.refer_id = p.prod_id  and s.sf_id = ?  order by s.seq";

	public SubFloorItem getSubFloorItem(Long id){
		return getById(id);
	}
	
    public void deleteSubFloorItem(SubFloorItem subFloorItem){
    	delete(subFloorItem);
    }
	
	public Long saveSubFloorItem(SubFloorItem subFloorItem){
		return (Long)save(subFloorItem);
	}
	
	public void updateSubFloorItem(SubFloorItem subFloorItem){
		 update(subFloorItem);
	}

	@Override
	public List<SubFloorItem> getAllSubFloorItem(Long sfId) {
		List<SubFloorItem> subFloorItemList = query(sqlForGetAllSubFloorItem, SubFloorItem.class, sfId);
		return subFloorItemList;
	}

	
	private List<SubFloorItem> getItemLimitBysfId(Long sfId,int start,int offset){
		List<SubFloorItem> subFloorItemList = queryLimit(sqlForGetItemLimitBysfId,SubFloorItem.class,start,offset,sfId);
		return subFloorItemList;
	}
	@Override
	public List<SubFloorItem> getItemLimit(Long sfId) {
		return getItemLimitBysfId(sfId,0,10);
	}

	@Override
	public void deleteSubFloorItemById(Long sfId) {
		update("delete from ls_sub_floor_item where sf_id = ?", sfId);
	}

	@Override
	public List<SubFloorItem> queryItem(List<SubFloor> subFloorList) {
		StringBuffer sb = new StringBuffer("select s.sfi_id,p.name as productName,s.sf_id,s.seq,p.pic as productPic,p.cash as productCash,p.price as productPrice,p.prod_id as productId " +
		"from ls_sub_floor_item s,ls_prod p where s.refer_id = p.prod_id and s.sf_id in ( ");
		List<Object> sfIds = new ArrayList<Object>();
		for (SubFloor sf : subFloorList) {
			sb.append("?,");
			sfIds.add(sf.getSfId());
		}
		sb.setLength(sb.length()-1);
		sb.append(") order by s.seq");
		List<SubFloorItem> subFloorItemList = query(sb.toString(),SubFloorItem.class,sfIds.toArray());
		return subFloorItemList;
	}

	@Override
	public void deleteSubFloorItemByFid(Long fId,String layout) {
		update("delete from ls_sub_floor_item where f_id = ? and layout=? ", new Object[]{fId,layout});
	}

 }
