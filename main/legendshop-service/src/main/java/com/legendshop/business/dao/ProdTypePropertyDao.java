/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProdTypeProperty;

/**
 * The Class ProductPropertyDao.
 */

public interface ProdTypePropertyDao extends Dao<ProdTypeProperty, Long>{
     
    public abstract void deleteProdTypeProperty(ProdTypeProperty prodTypeProperty);
	
	public abstract Long saveProdTypeProperty(ProdTypeProperty prodTypeProperty);
	
	public abstract List<Long> saveProdTypeProperty(List<ProdTypeProperty> prodTypePropertyList);
	
	ProdTypeProperty getProdTypeProperty(Long id);

	public abstract void updateProdTypeProperty(ProdTypeProperty prodTypeProperty);

	public abstract ProdTypeProperty getProdTypeProperty(Long propId, Long typeId);
	
 }
