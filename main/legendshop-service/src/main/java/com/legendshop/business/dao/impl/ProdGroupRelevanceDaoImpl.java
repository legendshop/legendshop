/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.ProdGroupRelevance;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdGroupRelevanceDao;

/**
 * The Class ProdGroupRelevanceDaoImpl. Dao实现类
 */
@Repository
public class ProdGroupRelevanceDaoImpl extends GenericDaoImpl<ProdGroupRelevance, Long> implements ProdGroupRelevanceDao{

	/**
	 * 根据Id获取
	 */
	public ProdGroupRelevance getProdGroupRelevance(Long id){
		return getById(id);
	}

	/**
	 *  删除
	 *  @param prodGroupRelevance 实体类
	 *  @return 删除结果
	 */	
    public int deleteProdGroupRelevance(ProdGroupRelevance prodGroupRelevance){
    	return delete(prodGroupRelevance);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteProdGroupRelevance(Long id){
		return deleteById(id);
	}
	
	@Override
	public int deleteProdGroupRelevanceByprod(Long id){
		
		String sql="delete from ls_prod_group_relevance where group_id=?";
		return update(sql,id);
	}
		
	
	/**
	 * 保存
	 */
	public Long saveProdGroupRelevance(ProdGroupRelevance prodGroupRelevance){
		return save(prodGroupRelevance);
	}

	/**
	 *  更新
	 */		
	public int updateProdGroupRelevance(ProdGroupRelevance prodGroupRelevance){
		return update(prodGroupRelevance);
	}

	/**
	 * 分页查询列表
	 */
	public PageSupport<ProdGroupRelevance>queryProdGroupRelevance(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(ProdGroupRelevance.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

    @Override
    public int getProductByGroupId(Long prod, Long groupId) {
		return get("SELECT COUNT(1) FROM ls_prod_group_relevance WHERE prod_id=? and group_id=?", Integer.class,prod,groupId);
    }

	/**
	 * 移除分组内商品
	 * @param prodId
	 * @param groupId
	 */
	@Override
	public void removeProd(Long prodId, Long groupId) {

		String sql="delete from ls_prod_group_relevance where prod_id=? and group_id=?";
		update(sql,prodId,groupId);
	}

}
