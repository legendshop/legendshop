/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.InvoiceSubDao;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.spi.service.InvoiceSubService;
import com.legendshop.util.AppUtils;

/**
 * 发票订单信息服务.
 */
@Service("invoiceSubService")
public class InvoiceSubServiceImpl  implements InvoiceSubService{

	@Autowired
    private InvoiceSubDao invoiceSubDao;

    public InvoiceSub getInvoiceSub(Long id) {
        return invoiceSubDao.getInvoiceSub(id);
    }

    public void deleteInvoiceSub(InvoiceSub invoiceSub) {
        invoiceSubDao.deleteInvoiceSub(invoiceSub);
    }

    public Long saveInvoiceSub(InvoiceSub invoiceSub) {
        if (!AppUtils.isBlank(invoiceSub.getId())) {
            updateInvoiceSub(invoiceSub);
            return invoiceSub.getId();
        }
        return invoiceSubDao.saveInvoiceSub(invoiceSub);
    }

    public void updateInvoiceSub(InvoiceSub invoiceSub) {
        invoiceSubDao.updateInvoiceSub(invoiceSub);
    }


}
