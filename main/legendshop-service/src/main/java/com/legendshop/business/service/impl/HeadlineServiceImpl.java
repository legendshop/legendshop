/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.HeadlineDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;
import com.legendshop.spi.service.HeadlineService;
import com.legendshop.util.AppUtils;

/**
 * WAP头条新闻表服务.
 */
@Service("headlineService")
public class HeadlineServiceImpl  implements HeadlineService{

	@Autowired
    private HeadlineDao headlineDao;

    public Headline getHeadline(Long id) {
        return headlineDao.getHeadline(id);
    }

    public void deleteHeadline(Headline headline) {
        headlineDao.deleteHeadline(headline);
    }

    public Long saveHeadline(Headline headline) {
        if (!AppUtils.isBlank(headline.getId())) {
            updateHeadline(headline);
            return headline.getId();
        }
        return headlineDao.saveHeadline(headline);
    }

    public void updateHeadline(Headline headline) {
        headlineDao.updateHeadline(headline);
    }

	@Override
	public List<Headline> queryByAll() {
		return headlineDao.queryByAll();
	}

	@Override
	public PageSupport<Headline> query(String curPageNO) {
		return headlineDao.query(curPageNO);
	}

	@Override
	public PageSupport<Headline> getHeadlinePage(String curPageNO) {
		return headlineDao.getHeadlinePage(curPageNO);
	}
}
