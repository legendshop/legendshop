/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopPermDao;
import com.legendshop.model.entity.ShopPerm;
import com.legendshop.model.entity.ShopPermId;
import com.legendshop.spi.service.ShopPermService;

/**
 *商家权限服务
 */
@Service("shopPermService")
public class ShopPermServiceImpl  implements ShopPermService{

	@Autowired
    private ShopPermDao shopPermDao;

    public ShopPerm getShopPerm(Long id) {
        return shopPermDao.getShopPerm(id);
    }

    public void deleteShopPerm(ShopPerm shopPerm) {
        shopPermDao.deleteShopPerm(shopPerm);
    }

    public Long saveShopPerm(ShopPerm shopPerm) {
        return shopPermDao.saveShopPerm(shopPerm);
    }

    public void updateShopPerm(ShopPerm shopPerm) {
        shopPermDao.updateShopPerm(shopPerm);
    }

	@Override
	public void saveShopPerm(List<ShopPerm> shopPermList) {
		shopPermDao.saveShopPerm(shopPermList);
	}

	@Override
	public List<String> getShopPermsByRoleId(Long shopRoleId) {
		return shopPermDao.getShopPermsByRoleId(shopRoleId);
	}

	@Override
	public void deleteUserRole(List<ShopPerm> shopPermList) {
		shopPermDao.deleteUserRole(shopPermList);
	}

	/**
	 * 根据职位Id和菜单标识获取权限
	 */
	@Override
	public String getFunction(Long shopRoleId, String label) {
		return shopPermDao.getFunction(shopRoleId,label);
	}

	/**
	 * 根据职位id获取所有权限
	 */
	@Override
	public List<ShopPermId> queryShopPermsByRoleId(Long shopRoleId) {
		
		return shopPermDao.queryShopPermsByRoleId(shopRoleId);
	}
}
