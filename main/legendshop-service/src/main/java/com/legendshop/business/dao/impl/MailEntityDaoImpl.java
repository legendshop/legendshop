/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MailEntityDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.MailEntity;
import com.legendshop.util.AppUtils;
/**
 * 邮件Dao.
 */
@Repository
public class MailEntityDaoImpl extends GenericDaoImpl<MailEntity, Long> implements MailEntityDao {

	public MailEntity getMailEntity(Long id){
		return getById(id);
	}
	
    public void deleteMailEntity(MailEntity mailEntity){
    	delete(mailEntity);
    }
	
	public Long saveMailEntity(MailEntity mailEntity){
		return (Long)save(mailEntity);
	}
	
	public void updateMailEntity(MailEntity mailEntity){
		 update(mailEntity);
	}

	/**
	 * 查询发送的邮件
	 * @param receiveMail
	 * @param type
	 * @return
	 */
	@Override
	public MailEntity getMailEntity(String receiveMail, MailCategoryEnum type) {
		List<MailEntity> result =  this.queryLimit("SELECT ls_mail.* FROM  ls_mail LEFT JOIN ls_mail_log ON ls_mail.id = ls_mail_log.mail_id " +
				"WHERE ls_mail.status=1 and ls_mail_log.receive_mail= ? AND ls_mail.category=? ORDER BY ls_mail.rec_time DESC  ", MailEntity.class, 0,1, receiveMail, type.value());
		if(AppUtils.isBlank(result)){
			return null;
		}else{
			return result.get(0);
		}
	}

	@Override
	public void clearMailCode(Long id) {
		this.update("update ls_mail set status=0 where id=?",id);
	}

	@Override
	public PageSupport<MailEntity> getMailEntityPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(MailEntity.class, curPageNO);
		return queryPage(cq);
	}

	
 }
