/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.util.AppUtils;

/**
 * 商品属性值服务.
 */
@Service("productPropertyValueService")
public class ProductPropertyValueServiceImpl  implements ProductPropertyValueService{

	@Autowired
    private ProductPropertyValueDao productPropertyValueDao;

    public ProductPropertyValue getProductPropertyValue(Long id) {
        return productPropertyValueDao.getProductPropertyValue(id);
    }

    public void deleteProductPropertyValue(ProductPropertyValue productPropertyValue) {
        productPropertyValueDao.deleteProductPropertyValue(productPropertyValue);
    }

    public Long saveProductPropertyValue(ProductPropertyValue productPropertyValue) {
        if (!AppUtils.isBlank(productPropertyValue.getValueId())) {
            updateProductPropertyValue(productPropertyValue);
            return productPropertyValue.getValueId();
        }
        return (Long) productPropertyValueDao.save(productPropertyValue);
    }

    public void updateProductPropertyValue(ProductPropertyValue productPropertyValue) {
        productPropertyValueDao.updateProductPropertyValue(productPropertyValue);
    }

    public PageSupport getProductPropertyValue(CriteriaQuery cq) {
        return productPropertyValueDao.queryPage(cq);
    }

	@Override
	public List<ProductPropertyValue> getAllProductPropertyValue(List<ProductProperty> propertyList) {
		return productPropertyValueDao.getAllProductPropertyValue(propertyList);
	}

	@Override
	public List<ProductPropertyValue> queryProductPropertyValue(Long propId) {
		return productPropertyValueDao.queryProductPropertyValue(propId);
	}

	@Override
	public List<ProductPropertyValue> saveCustomAttributeValues(Long propId, List<String> propValue) {
		List<ProductPropertyValue> propValueList = new ArrayList<ProductPropertyValue>();
		Date date = new Date();
		int sequence = 1;
		for(String valueName : propValue){
			ProductPropertyValue newPropertyValue = new ProductPropertyValue();
			newPropertyValue.setPropId(propId);
			newPropertyValue.setName(valueName);
			newPropertyValue.setPic("");//属性值图片
			newPropertyValue.setModifyDate(date);
			newPropertyValue.setRecDate(date);
			newPropertyValue.setStatus((short) 1);
			newPropertyValue.setSequence((long)sequence);
			sequence++;
			propValueList.add(newPropertyValue);
		}
		
		if(AppUtils.isNotBlank(propValueList)){
			for(ProductPropertyValue customPropValue : propValueList){
				Long valueId = productPropertyValueDao.saveCustomPropValue(customPropValue);
				customPropValue.setValueId(valueId);
			}
		}
		
		return propValueList;
	}
	
	@Override
	public ProductPropertyValue getProductPropertyValueInfo(Long valueId,Long propId) {
		return productPropertyValueDao.getProductPropertyValueInfo(valueId, propId);
	}

	@Override
	public ProductPropertyValue getProductPropertyValueInfo(String name, Long propId) {
		return productPropertyValueDao.getProductPropertyValueInfo(name, propId);
	}

	@Override
	public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String curPageNO) {
		return productPropertyValueDao.getProductPropertyValuePage(curPageNO);
	}

	@Override
	public PageSupport<ProductPropertyValue> getProductPropertyValue(String valueName, Long propId) {
		return productPropertyValueDao.getProductPropertyValue(valueName,propId);
	}

	@Override
	public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String valueName, Long propId) {
		return productPropertyValueDao.getProductPropertyValuePage(valueName,propId);
	}
}
