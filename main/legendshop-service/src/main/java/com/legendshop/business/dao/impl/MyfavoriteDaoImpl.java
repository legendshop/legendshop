package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MyfavoriteDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.Myfavorite;
import com.legendshop.util.AppUtils;

/**
 * 我的商品收藏Dao
 *
 */
@Repository
public class MyfavoriteDaoImpl extends GenericDaoImpl<Myfavorite, Long> implements MyfavoriteDao {

	public Myfavorite getFavorite(Long id) {
		return this.getById(id);
	}

	public int deleteFavorite(Myfavorite favorite) {
		return this.delete(favorite);
	}

	public Long saveFavorite(Myfavorite favorite) {
		return this.save(favorite);
	}

	public int updateFavorite(Myfavorite favorite) {
		return this.update(favorite);

	}

	public int deleteFavoriteById(Long id) {
		return deleteById(id);
	}

	public void deleteFavoriteByUserId(String userId) {
		if (AppUtils.isNotBlank(userId)) {
			update("delete from ls_favorite where user_id = ?", userId);
		}
	}

	public Boolean isExistsFavorite(Long prodId, String userName) {
		Long counts = getLongResult("select count(*) from ls_favorite where prod_id = ? and user_name = ?", prodId, userName);
		return counts > 0;
	}

	public void deleteFavorite(String userId, String[] favIds) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(favIds.length);
		for (String favId : favIds) {
			Object[] param = new Object[2];
			param[0] = userId;
			param[1] = favId;
			batchArgs.add(param);
		}
		this.batchUpdate("delete from ls_favorite where user_id = ? and id = ?", batchArgs);

	}

	public Long getfavoriteLength(String userId) {
		Long counts = getLongResult("select count(*) as count from ls_favorite f, ls_prod p where f.prod_id=p.prod_id and f.user_id = ?", userId);
		return counts;
	}

	@Override
	public PageSupport<Myfavorite> getFavoriteList(String curPageNO, Integer pageSize, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Myfavorite.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		query.setParam(map.toArray());
		String queryFavCount = ConfigCode.getInstance().getCode("uc.favourites.queryMyFavouriteCount", map);
		String queryFav = ConfigCode.getInstance().getCode("uc.favourites.queryMyFavourite", map);
		query.setAllCountString(queryFavCount);
		query.setQueryString(queryFav);
		return querySimplePage(query);
	}
	
	/**
	 * 修改支付密码
	 */
	@Override
	public void updatePaymentPassword(String userName, String paymentPassword) {
		update("update ls_usr_detail set pay_password = ? where user_name = ?", paymentPassword, userName);
	}

	@Override
	@CacheEvict(value = "UserDetail", key = "#userId")
	public void flushUserDetail(String userId) {
	}


	@Override
	public PageSupport<Myfavorite> collect(String curPageNO, Integer pageSize, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Myfavorite.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		query.setParam(map.toArray());

		String queryFavCount = ConfigCode.getInstance().getCode("biz.queryMyFavouriteCount", map);
		String queryFav = ConfigCode.getInstance().getCode("biz.queryMyFavourite", map);

		query.setAllCountString(queryFavCount);
		query.setQueryString(queryFav);
		return querySimplePage(query);
	}

	@Override
	public void deleteFav(String userId, Long prodId) {
		this.update("delete from ls_favorite where user_id = ? and prod_id = ?", userId, prodId);
	}

	@Override
	public Long getCouponLength(String userName) {
		String sql = "select count(1) from ls_coupon bc ,ls_user_coupon buc, ls_shop_detail sd where bc.coupon_id = buc.coupon_id and bc.shop_id=sd.shop_id  and buc.user_name = ?  and buc.use_status = ?  and bc.end_date >= ? and bc.status = ?";
		Long counts = getLongResult(sql, userName, 1, new Date(), 1);
		return counts;
	}

	@Override
	public int deleteFavoriteByProdIdAndUserId(Long prodId, String userId) {
		return update("DELETE FROM ls_favorite WHERE prod_id = ? AND user_id = ?", prodId, userId);
	}


}
