/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AppStartAdvDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppStartAdvDto;
import com.legendshop.model.entity.AppStartAdv;
import com.legendshop.util.AppUtils;

/**
 * The Class AppStartAdvDaoImpl.
 */
@Repository
public class AppStartAdvDaoImpl extends GenericDaoImpl<AppStartAdv, Long> implements AppStartAdvDao  {
    
	/** 根据ID更新状态 */
	public static final String SQL_UPDATE_STATUS_BY_ID = "UPDATE ls_app_start_adv SET status = ? WHERE id = ?";
	
	/** 根据name统计记录数 */
	public static final String SQL_GET_COUNT_BY_NAME = "SELECT COUNT(1) FROM ls_app_start_adv WHERE name = ?";
	
	/** 根据Id查询name */
	public static final String SQL_GET_NAME_BY_NAME = "SELECT name FROM ls_app_start_adv WHERE id = ?";
	
    public List<AppStartAdv> getAppStartAdv(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

	public AppStartAdv getAppStartAdv(Long id){
		return getById(id);
	}
	
    public int deleteAppStartAdv(AppStartAdv appStartAdv){
    	return delete(appStartAdv);
    }
	
	public Long saveAppStartAdv(AppStartAdv appStartAdv){
		return save(appStartAdv);
	}
	
	public int updateAppStartAdv(AppStartAdv appStartAdv){
		return update(appStartAdv);
	}
	
	public PageSupport<AppStartAdv> getAppStartAdv(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public int updateStatus(Long id, Integer status) {
		
		return update(SQL_UPDATE_STATUS_BY_ID, status, id);
	}

	@Override
	public int getCount(String name) {
		int result = get(SQL_GET_COUNT_BY_NAME, Integer.class, name);
		return result;
	}

	@Override
	public String getName(Long id) {
		return get(SQL_GET_NAME_BY_NAME, String.class, id);
	}

	@Override
	public PageSupport<AppStartAdv> getAppStartAdvPage(String curPageNO, AppStartAdv appStartAdv) {
		CriteriaQuery cq = new CriteriaQuery(AppStartAdv.class, curPageNO);
		if(AppUtils.isNotBlank(appStartAdv.getName())){
			 cq.like("name", "%" + appStartAdv.getName().trim() + "%");
		}
        cq.eq("status", appStartAdv.getStatus());
        cq.addDescOrder("status");
        cq.addDescOrder("createTime");
		return queryPage(cq);
	}
	
 }
