/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PrintTmpl;

/**
 *打印模板Dao
 */
public interface PrintTmplDao extends Dao<PrintTmpl, Long> {
     
	public List<PrintTmpl> getPrintTmplByShop(Long shopId);

	public abstract PrintTmpl getPrintTmpl(Long id);
	
    public abstract int deletePrintTmpl(PrintTmpl printTmpl);
	
	public abstract Long savePrintTmpl(PrintTmpl printTmpl);
	
	public abstract int updatePrintTmpl(PrintTmpl printTmpl);
	
	public abstract PrintTmpl getPrintTmplByDelvId(Long delvId);

	public PageSupport<PrintTmpl> getPrintTmplPage(String curPageNO, PrintTmpl printTmpl);

	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId);
	
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId, String curPageNO);
	
 }
