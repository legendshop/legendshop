/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MsgStatusDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.MsgStatus;
import com.legendshop.util.AppUtils;

/**
 * The Class MsgStatusDaoImpl.
 */
@Repository
public class MsgStatusDaoImpl extends GenericDaoImpl<MsgStatus, Long> implements MsgStatusDao {

	@Override
	public MsgStatus getSystemMsgStatus(String userName, Long msgId) {
		List<MsgStatus> list = queryByProperties(new EntityCriterion().eq("userName", userName).eq("msgId", msgId));
		if (AppUtils.isBlank(list)) {
			return null;
		} else {
			return list.iterator().next();
		}
	}

	@Override
	public void saveMsgStatus(MsgStatus msgStatus) {
		save(msgStatus);
	}

	@Override
	public void updateMsgStatus(MsgStatus msgStatus) {
		update(msgStatus);
	}

}
