/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ArticleDao;
import com.legendshop.business.dao.ArticleThumbDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleThumb;
import com.legendshop.spi.service.ArticleThumbService;
import com.legendshop.util.AppUtils;

/**
 * 文章点赞服务实现类
 */
@Service("articleThumbService")
public class ArticleThumbServiceImpl implements ArticleThumbService {

	@Autowired
	private ArticleThumbDao articleThumbDao;

	@Autowired
	private ArticleDao articleDao;

	/**
	 * 根据Id获取
	 */
	public ArticleThumb getArticleThumb(Long id) {
		return articleThumbDao.getArticleThumb(id);
	}

	/**
	 * 删除
	 */
	public void deleteArticleThumb(ArticleThumb articleThumb) {
		articleThumbDao.deleteArticleThumb(articleThumb);
	}

	/**
	 * 保存
	 */
	public Long saveArticleThumb(ArticleThumb articleThumb) {
		if (!AppUtils.isBlank(articleThumb.getId())) {
			updateArticleThumb(articleThumb);
			return articleThumb.getId();
		}
		// 保存点赞
		Long res = articleThumbDao.saveArticleThumb(articleThumb);
		// 修改点赞数量
		if (res > 0) {
			articleDao.updateThumbNum(articleThumb.getArtId());
		}
		return res;
	}

	/**
	 * 更新
	 */
	public void updateArticleThumb(ArticleThumb articleThumb) {
		articleThumbDao.updateArticleThumb(articleThumb);
	}

	@Override
	public String saveThumb(ArticleThumb articleThumb, Long artId, String userId) {
	PageSupport<ArticleThumb> item= articleThumbDao.saveThumb(artId,userId);
		
		long sum = item.getPageCount();
		if (sum >= 0) {
			// 添加文章点赞
			Long res = articleThumbDao.saveArticleThumb(articleThumb);
			// 修改点赞数量
			if (res > 0) {
				articleDao.updateThumbNum(articleThumb.getArtId());
			}
			return "ok";
		} else {
			return "false";
		}
	}
}
