/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdRecharge;

/**
 *预存款充值Dao
 */
public interface PdRechargeDao extends GenericDao<PdRecharge, Long> {
     
	public abstract PdRecharge getPdRecharge(Long id);
	
    public abstract int deletePdRecharge(PdRecharge pdRecharge);
	
	public abstract Long savePdRecharge(PdRecharge pdRecharge);
	
	public abstract int updatePdRecharge(PdRecharge pdRecharge);
	
	public abstract int deleteRechargeDetail(String userId, String pdrSn);

	public abstract PdRecharge findRechargePay(String userId, String paySn, Integer status);

	public abstract PdRecharge findRechargePay(String paySn) ;

	public abstract PageSupport<PdRecharge> getRechargeDetailedPage(String curPageNO, String userId, String state);

	public abstract PageSupport<PdRecharge> getPdRechargeListPage(String curPageNO, PdRecharge pdRecharge, Integer pageSize, DataSortResult result);

	public abstract Integer batchDelete(String ids);

	/**
	 * 充值明细列表
	 * @param userId 用户Id
	 * @param state 支付状态 0未支付1支付
	 * @param pdrSn 唯一的流水号
	 * @param curPageNO 当前页
	 * @param pageSize 每页大小
	 *
	 */
	public abstract PageSupport<PdRecharge> getPdRechargePage(String userId, String state, String pdrSn, String curPageNO, int pageSize);

}
