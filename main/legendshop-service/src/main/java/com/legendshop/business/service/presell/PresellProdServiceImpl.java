/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.presell;

import java.util.*;

import com.legendshop.business.dao.*;
import com.legendshop.business.dao.presell.PresellSkuDao;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.presell.*;
import com.legendshop.model.dto.presell.PresellSkuDto;
import com.legendshop.model.dto.seckill.SeckillProdDto;
import com.legendshop.model.dto.seckill.SeckillSkuDto;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.presell.PresellProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.util.AppUtils;

/**
 * 预售商品服务实现类
 */
@Service("presellProdService")
public class PresellProdServiceImpl implements PresellProdService {
	
	@Autowired
	private PresellProdDao presellProdDao;
	
	@Autowired
	private ProductPropertyDao productPropertyDao;
	
	@Autowired
	private ProductPropertyValueDao productPropertyValueDao;
	
	@Autowired
	private ProdPropImageDao prodPropImageDao;
	
	@Autowired
	private ImgFileDao imgFileDao;
	
	@Autowired
	private SkuDao skuDao;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private PresellSkuDao presellSkuDao;

	@Autowired
	private ImgFileService imgFileService;

	/**
	 * 获取预售商品 
	 */
	public PresellProd getPresellProd(Long id) {
		return presellProdDao.getPresellProd(id);
	}

	/**
	 * 删除预售商品 
	 */
	public void deletePresellProd(PresellProd presellProd) {
		presellProdDao.deletePresellProd(presellProd);
		List<Sku> skuByProd = skuDao.getSkuByProd(presellProd.getProdId());
		for (Sku sku : skuByProd) {
			//重置sku的营销状态
			if(AppUtils.isNotBlank(sku) && SkuActiveTypeEnum.PRESELL.value().equals(sku.getSkuType())) { //还是预售状态的，需要重置
				skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.PRESELL.value());
			}
		}
	}

	/**
	 * 保存预售商品 
	 */
	public Long savePresellProd(PresellProd presellProd) {
		if (!AppUtils.isBlank(presellProd.getId())) {
			updatePresellProd(presellProd);
			return presellProd.getId();
		}
		//新增预售活动，把sku 标记为 营销状态
		List<Sku> skuByProd = skuDao.getSkuByProd(presellProd.getProdId());
		for (Sku sku : skuByProd) {
			skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRESELL.value(),SkuActiveTypeEnum.PRODUCT.value());
		}
		return presellProdDao.savePresellProd(presellProd);
	}

	/**
	 * 更新预售商品 
	 */
	public void updatePresellProd(PresellProd presellProd) {
		
		if(PresellProdStatusEnum.NOT_PUT_AUDIT.value().equals(presellProd.getStatus()) || PresellProdStatusEnum.WAIT_AUDIT.value().equals(presellProd.getStatus())
				|| PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()) ) { //1、未提审 ，修改的时候会进来 ， 2、被拒绝，修改重新进来 , 3、已上线未开始
			
			PresellProd oldPresellProd = presellProdDao.getById(presellProd.getId());

			if(oldPresellProd.getProdId().equals(presellProd.getProdId())) {//修改了sku商品
				//重置原来的sku营销状态，重新选择新的sku
				List<Sku> skuByProd = skuDao.getSkuByProd(presellProd.getProdId());
				List<Sku> orderSkuByProd = skuDao.getSkuByProd(oldPresellProd.getProdId());
				for (Sku sku : orderSkuByProd) {
					skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.PRESELL.value());
				}
				for (Sku sku : skuByProd) {
					skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRESELL.value(), SkuActiveTypeEnum.PRODUCT.value());
				}
			}
		}
		presellProdDao.updatePresellProd(presellProd);
		//更新预售商品，重置sku状态
	    if (PresellProdStatusEnum.FINISH.value().equals(presellProd.getStatus()) 
	    		|| PresellProdStatusEnum.STOP.value().equals(presellProd.getStatus()) ) {//完成状态或终止状态
			List<Sku> skuByProd = skuDao.getSkuByProd(presellProd.getProdId());
			for (Sku sku : skuByProd) {
				if(AppUtils.isNotBlank(sku) && SkuActiveTypeEnum.PRESELL.value().equals(sku.getSkuType())) { //还是预售状态的，需要重置
					skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.PRESELL.value());
				}
			}
		}
	}

	/**
	 * 获取预售商品Dto 
	 */
	@Override
	public PresellProdDto getPresellProdDto(Long id) {
		PresellProdDto presellProdDto = presellProdDao.getPresellProdDto(id);
		
		if (AppUtils.isBlank(presellProdDto)) {
			return null;
		}
		List<Sku> skuByProd = skuDao.getSkuByProd(presellProdDto.getProdId());
//		presellProdDto
		// 活动已完成
		if (PresellProdStatusEnum.ONLINE.value().equals(presellProdDto.getStatus()) && presellProdDto.getPreSaleEnd().before(new Date())) {
			// 把标记改为已完成,更新数据库
			presellProdDao.updatePresellStatus(presellProdDto.getId(),presellProdDto.getProdId(), PresellProdStatusEnum.FINISH.value());
			presellProdDto.setStatus(PresellProdStatusEnum.FINISH.value());
			for (Sku sku : skuByProd) {
				//重置sku的营销状态
				skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.PRESELL.value());
			}

		}
		return presellProdDto;
	}

	/**
	 * 获取预售商品 
	 */
	@Override
	public PresellProd getPresellProd(String schemeName, Long shopId) {
		return presellProdDao.getPresellProd(schemeName, shopId);
	}

	/**
	 * 查询预售商品详情 
	 */
	@Override
	public PresellProdDetailDto getPresellProdDetailDto(Long presellId) {

		PresellProdDetailDto presellProdDetailDto = presellProdDao.getPresellProdDetailDto(presellId);

		if (AppUtils.isBlank(presellProdDetailDto)) {
			return null;
		}
		
		// 活动已完成， 活动已经结束
		if (PresellProdStatusEnum.ONLINE.value().equals(presellProdDetailDto.getStatus()) && presellProdDetailDto.getPreSaleEnd().before(new Date())) {
			// 把标记改为已完成,更新数据库
			presellProdDao.updatePresellStatus(presellProdDetailDto.getId(),presellProdDetailDto.getProdId(), PresellProdStatusEnum.FINISH.value());
			presellProdDetailDto.setStatus(PresellProdStatusEnum.FINISH.value());
		}

//		// 获取商品sku属性集合
//		List<Long> propIds = presellProdDetailDto.getPropIds();
//		if (AppUtils.isNotBlank(propIds)) {
//			List<Long> propValueIds = presellProdDetailDto.getPropValueIds();
//			List<ProductProperty> productPropertys = productPropertyDao.getProductProperty(propIds);
//			List<ProductPropertyValue> productPropertyValues = productPropertyValueDao.getProductPropertyValue(propValueIds);
//			List<ProdPropDto> prodPropDtos = new ArrayList<ProdPropDto>();
//
//			for (ProductProperty productProperty : productPropertys) {
//
//				ProdPropDto prodPropDto = new ProdPropDto();
//				prodPropDto.setPropId(productProperty.getPropId());
//				prodPropDto.setPropName(productProperty.getPropName());
//				prodPropDto.setMemo(productProperty.getMemo());
//				prodPropDto.setStatus(productProperty.getStatus().intValue());
//				prodPropDto.setType(productProperty.getType());
//				prodPropDto.setIsRuleAttributes(productProperty.getIsRuleAttributes());
//
//				List<PropValueDto> propValueDtos = new ArrayList<PropValueDto>();
//				for (ProductPropertyValue productPropertyValue : productPropertyValues) {
//					if (productPropertyValue.getPropId().equals(prodPropDto.getPropId())) {
//						PropValueDto propValueDto = new PropValueDto();
//						propValueDto.setValueId(productPropertyValue.getValueId());
//						propValueDto.setName(productPropertyValue.getName());
//						propValueDto.setStatus(productPropertyValue.getStatus().intValue());
//						propValueDto.setImgs(
//								prodPropImageDao.getImageUrlList(presellProdDetailDto.getProdId(), prodPropDto.getPropId(), propValueDto.getValueId()));
//						presellProdDetailDto.getPropValImageUrls().addAll(propValueDto.getImgs());
//						// 如果属性值的主图片为空,则使用该属性值的第一张图片作为属性值主图
//						if (AppUtils.isBlank(productPropertyValue.getPic()) && propValueDto.getImgs().size() > 0) {
//							propValueDto.setPic(propValueDto.getImgs().get(0));
//						}
//						propValueDtos.add(propValueDto);
//						break;
//					}
//				}
//				prodPropDto.setPropValueDtos(propValueDtos);
//				prodPropDtos.add(prodPropDto);
//			}
//			presellProdDetailDto.setProdPropDtos(prodPropDtos);
//		}
//
//		// 商品图片列表(没有属性图片时，展示商品图片)
//		if (presellProdDetailDto.getPropValImageUrls().size() == 0) {
//			List<String> prodPics = new ArrayList<String>();
//			List<ImgFile> imgFileList = imgFileDao.getAllProductPics(presellProdDetailDto.getProdId());
//			if (AppUtils.isNotBlank(imgFileList)) {
//				for (ImgFile imgFile : imgFileList) {
//					//ProdPicDto prodPicDto = new ProdPicDto();
//					//prodPicDto.setFilePath(imgFile.getFilePath());
//					prodPics.add(imgFile.getFilePath());
//				}
//			} else {
//				//ProdPicDto prodPicDto = new ProdPicDto();
//				//prodPicDto.setFilePath(presellProdDetailDto.getProdMainPic());
//				prodPics.add(presellProdDetailDto.getProdMainPic());
//			}
//			// 如果SKU的主图为空
//			if (AppUtils.isBlank(presellProdDetailDto.getSkuPic())) {
//				//presellProdDetailDto.setSkuPic(prodPics.get(0).getFilePath());
//				presellProdDetailDto.setSkuPic(prodPics.get(0));
//			}
//			presellProdDetailDto.setProdPics(prodPics);
//		} else {
//			// 如果SKU的主图为空
//			if (AppUtils.isBlank(presellProdDetailDto.getSkuPic())) {
//				presellProdDetailDto.setSkuPic(presellProdDetailDto.getPropValImageUrls().get(0));
//			}
//		}
		ProductDetail prod = productDao.getProdDetail(presellProdDetailDto.getProdId());
		if (prod == null || !ProductStatusEnum.PROD_ONLINE.value().equals(prod.getStatus())) {
			return null;
		}
		PresellProdNewDto prodDto = new PresellProdNewDto();
		copyProdField(prodDto, prod);

		// 获取商品sku信息
		List<PresellSkuDto> skuDtoList = new ArrayList<PresellSkuDto>();
		List<Sku> skuList = skuDao.getSkuByProd(presellProdDetailDto.getProdId());
		if (AppUtils.isNotBlank(skuList)) {
			for (Sku sku : skuList) {
				PresellSku presellSku = presellSkuDao.getPresellSkuByActive(sku.getSkuId(), presellProdDetailDto.getId());
				if (presellSku == null)
					continue;
				PresellSkuDto skuDto = new PresellSkuDto();
				skuDto.setSkuId(sku.getSkuId());
				skuDto.setName(processName(sku.getName()));
				skuDto.setPrice(sku.getPrice());
				skuDto.setPrePrice(presellSku.getPrsellPrice().doubleValue());
				skuDto.setProperties(sku.getProperties());
				skuDto.setStatus(sku.getStatus());
				skuDto.setSkuStock(sku.getStocks());
				skuDtoList.add(skuDto);
			}
			prodDto.setSkuDtoList(skuDtoList);
			prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));
		}

		// 获取属性图片
		List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
		Map<Long, List<String>> valueImagesMap = new HashMap<Long, List<String>>();
		if (AppUtils.isNotBlank(skuList)) {
			// 获取到商品 所有的属性图片
			List<ProdPropImage> prodPropImages = prodPropImageDao.getProdPropImageByProdId(presellProdDetailDto.getProdId());
			if (AppUtils.isNotBlank(prodPropImages)) {
				// 根据属性值 进行分组,封装成map
				for (ProdPropImage prodPropImage : prodPropImages) {
					Long valueId = prodPropImage.getValueId();
					List<String> imgs = valueImagesMap.get(valueId);
					if (AppUtils.isBlank(imgs)) {
						imgs = new ArrayList<String>();
					}
					imgs.add(prodPropImage.getUrl());
					valueImagesMap.put(valueId, imgs);
				}
				// 将map转换成 DTO LIST
				Iterator<Long> it = valueImagesMap.keySet().iterator();
				while (it.hasNext()) {
					Long valId = it.next();
					List<String> imgs = valueImagesMap.get(valId);
					PropValueImgDto propValueImgDto = new PropValueImgDto();
					propValueImgDto.setValueId(valId);
					propValueImgDto.setImgList(imgs);
					propValueImgList.add(propValueImgDto);
				}
			}
			prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));
		}
		// 获取商品属性和属性值
		List<ProductPropertyDto> prodPropDtoList = getPropValListByProd(prodDto, skuDtoList,valueImagesMap );
		prodDto.setProdPropDtoList(prodPropDtoList);
		prodDto.setPropValueImgList(propValueImgList);

		// 商品图片列表(没有属性图片时，展示商品图片)
		List<ProdPicDto> prodPics = new ArrayList<ProdPicDto>();
		if (AppUtils.isBlank(propValueImgList)) {
			List<ImgFile> imgFileList = imgFileService.getAllProductPics(presellProdDetailDto.getProdId());
			if (AppUtils.isNotBlank(imgFileList)) {
				for (ImgFile imgFile : imgFileList) {
					ProdPicDto prodPicDto = new ProdPicDto();
					prodPicDto.setFilePath(imgFile.getFilePath());
					prodPics.add(prodPicDto);
				}
			}
		} else {
			ProdPicDto prodPicDto = new ProdPicDto();
			prodPicDto.setFilePath(prodDto.getPic());
			prodPics.add(prodPicDto);
		}
		prodDto.setProdPics(prodPics);
		presellProdDetailDto.setProdDto(prodDto);

		return presellProdDetailDto;
	}

	/**
	 * 拷贝部分字段值
	 *
	 * @param prodDto
	 * @param prodDetail
	 */
	private void copyProdField(PresellProdNewDto prodDto, ProductDetail prodDetail) {
		prodDto.setProVideoUrl(prodDetail.getProVideoUrl());
		prodDto.setProdId(prodDetail.getProdId());
		prodDto.setName(processName(prodDetail.getName()));
		prodDto.setCash(prodDetail.getCash());
		prodDto.setPic(prodDetail.getPic());
		prodDto.setBrief(prodDetail.getBrief());
		prodDto.setModelId(prodDetail.getModelId());
		prodDto.setStatus(prodDetail.getStatus());
		prodDto.setContent(prodDetail.getContent());
		prodDto.setStocks(prodDetail.getStocks());
		prodDto.setCategoryId(prodDetail.getCategoryId());
	}


	/**
	 * 处理name
	 * @param name
	 * @return
	 */
	private String processName(String name) {
		if (name == null) {
			return null;
		}
		name = name.replace("\"", "〞");
		name = name.replace("'", "’");
		return name;
	}

	/**
	 * 判断指定sku是否已参与其他营销活动 
	 */
	@Override
	public Boolean isAttendActivity(Long activeId, Long prodId, Long skuId) {
		// 获得当前时间
		Date date = new Date();

		// 1 判断商品是否有参加拍卖活动, 状态,参见AuctionsStatusEnum, 在线和审核中排除
		String sql1 = "SELECT COUNT(1) FROM ls_auctions a WHERE a.prod_id = ? AND a.sku_id = ? AND a.status IN (-1,1) AND end_time > ?";
		Long count = presellProdDao.getLongResult(sql1, prodId, skuId,date);
		// 2 判断商品是否有参加团购活动 活动的状态有：审核中(-1)、上线（1）、下线(0)、已完成(2)、审核不通过。
		String sql2 = "SELECT COUNT(1) FROM ls_group WHERE product_id = ? and status IN (1,-1)  AND end_time > ?";
		count += presellProdDao.getLongResult(sql2, prodId, date);
		
		//3 判断是否已经参与了预售活动
		if(AppUtils.isNotBlank(activeId)) {//更新情况下，排除活动本身
			String sql3 = "SELECT COUNT(1) FROM ls_presell_prod WHERE prod_id = ? and sku_id = ? and status IN (-2,-1,0)  AND pre_sale_end > ? and id <> ?";
			count += presellProdDao.getLongResult(sql3,prodId,skuId,date, activeId);
		}else {
			String sql3 = "SELECT COUNT(1) FROM ls_presell_prod WHERE prod_id = ? and sku_id = ? and status IN (-2,-1,0)  AND pre_sale_end > ?";
			count += presellProdDao.getLongResult(sql3,prodId,skuId,date);
		}
		
		//4 秒杀
		String sql4 = "SELECT COUNT(1) FROM ls_seckill_activity sa inner JOIN ls_seckill_prod sp ON sa.id= sp.s_id WHERE sa.status IN (1,-1)  AND sa.end_time > ? AND sp.prod_id = ? AND sp.sku_id = ?";
		count += presellProdDao.getLongResult(sql4,date,prodId,skuId);
		
		//5 拼团
		String sql5 = "SELECT COUNT(1) FROM ls_merge_group mg inner JOIN ls_merge_product mp ON mg.id= mp.merge_id WHERE mg.status IN (0,1)  AND mg.end_time > ? AND mp.prod_id = ? AND mp.sku_id = ?";
		count += presellProdDao.getLongResult(sql5,date,prodId,skuId); 
		
		return count > 0;
	}

	/**
	 * 获取预售商品 
	 */
	@Override
	public PresellProd getPresellProd(Long prodId, Long skuId) {
		return presellProdDao.getPresellProd(prodId, skuId);
	}

	/**
	 * 查询预售商品页面 
	 */
	@Override
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, boolean isExpires, PresellProdDto presellProd) {
		return presellProdDao.queryPresellProdListPage(curPageNO, isExpires, presellProd);
	}

	/**
	 * 查询预售商品页面 
	 */
	@Override
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, Long shopId, PresellSearchParamDto paramDto) {
		return presellProdDao.queryPresellProdListPage(curPageNO, shopId, paramDto);
	}

	@Override
	public PageSupport<PresellProd> queryPresellProdListPage(String curPageNO, ProductSearchParms parms) {
		return presellProdDao.queryPresellProdListPage(curPageNO,parms);
	}

	@Override
	public Integer batchDelete(String ids) {
		String[] presellIds=ids.split(",");
		Integer result=0;
		for(String presellId: presellIds){
			PresellProd presellProd=presellProdDao.getById(Long.valueOf(presellId));
			if (presellProd.getStatus().intValue() > PresellProdStatusEnum.ONLINE.value().intValue()
					|| (PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()) && presellProd.getPreSaleStart().before(new Date()))
					|| (presellProd.getStatus().intValue() > PresellProdStatusEnum.ONLINE.value().intValue()
							&& !PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()))) {
				throw new RuntimeException("对不起,请不要非法操作!");
			}
			this.deletePresellProd(presellProd);
			result++;
		}
		return result;
	}

	@Override
	public int batchStop(String ids) {
		String[] presellIds=ids.split(",");
		Integer result=0;
		for(String presellId:presellIds){
			PresellProd presellProd = this.getPresellProd(Long.valueOf(presellId));
			// 如果当前活动不是正在进行状态
			if (!PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus())) {
				throw new RuntimeException("活动未在上线状态，不需要下线。");
			}
			presellProd.setStatus(PresellProdStatusEnum.STOP.value());
			this.updatePresellProd(presellProd);
			result++;
		}
		return result;
	}

	@Override
	public List<PresellProd> getPresellProdByTime(Date now) {
		return presellProdDao.getPresellProdByTime(now);
	}

	/**
	 * 获取审核过程中，未审核或审核拒绝后导致未能正常上线过期的预售活动
	 */
	@Override
	public List<PresellProd> getAuditExpiredPresellProd(Date now) {
		return presellProdDao.getAuditExpiredPresellProd(now);
	}

	/**
	 * 释放预售活动商品sku
	 */
	@Override
	public void releasePresellProd(PresellProd presellProd) {

		List<Sku> skuByProd = skuDao.getSkuByProd(presellProd.getProdId());
		for (Sku sku : skuByProd) {
			//重置sku的营销状态
			skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.PRESELL.value());
		}

		
		//标记为过期
		presellProd.setStatus(PresellProdStatusEnum.EXPIRED.value());
		this.updatePresellProd(presellProd);
	}

	@Override
	public OperateStatisticsDTO getOperateStatistics(Long id) {
		return presellProdDao.getOperateStatistics(id);
	}


	/**
	 * 根据商品id获取 属性和属性值.
	 *
	 * @param prodDto
	 * @param productSkus
	 * @param valueImagesMap
	 * @return the prop val list by prod
	 */
	private List<ProductPropertyDto> getPropValListByProd(PresellProdNewDto prodDto, List<PresellSkuDto> productSkus, Map<Long, List<String>> valueImagesMap) {
		List<Long> selectedVals = new ArrayList<Long>();
		if (AppUtils.isNotBlank(productSkus)) {
			// 取得价格最低的 sku 作为默认选中的sku
			PresellSkuDto defaultSku = getDefaultSku(productSkus);
			prodDto.setCash(defaultSku.getPrice());
			if (AppUtils.isNotBlank(defaultSku.getName())) {
				prodDto.setName(defaultSku.getName());
			}
			prodDto.setSkuId(defaultSku.getSkuId());
			prodDto.setPrePrice(defaultSku.getPrePrice());
			// 取得sku字符串(k1:v1;k2:v2;k3:v3)中的每对key:value,并对其进行相应处理
			Map<Long, List<Long>> propIdMap = new HashMap<Long, List<Long>>();
			List<Long> allValIdList = new ArrayList<Long>();
			for (PresellSkuDto sku : productSkus) {
				String properties = sku.getProperties();
				if (AppUtils.isNotBlank(properties)) {
					String skuStrs[] = properties.split(";");
					for (int i = 0; i < skuStrs.length; i++) {
						String skuItems[] = skuStrs[i].split(":");
						Long propId = Long.valueOf(skuItems[0]);
						Long valueId = Long.valueOf(skuItems[1]);
						if (sku.getSkuId().equals(defaultSku.getSkuId())) {
							selectedVals.add(valueId);
						}
						List<Long> valIdList = propIdMap.get(propId);
						if (AppUtils.isBlank(valIdList)) {
							valIdList = new ArrayList<Long>();
							valIdList.add(valueId);
						} else {
							if (!valIdList.contains(valueId)) {
								valIdList.add(valueId);
							}
						}
						propIdMap.put(propId, valIdList);
						allValIdList.add(valueId);
					}
				}
			}

			List<Long> propIds = new ArrayList<Long>(propIdMap.keySet());// set
			// 转
			// list
			if (AppUtils.isBlank(propIds)) {
				return null;
			}

			// 根据propId的集合 ，查出属性集合
			List<ProductProperty> pps = productPropertyDao.getProductProperty(propIds, prodDto.getCategoryId());
			List<ProductPropertyDto> ppds = new ArrayList<ProductPropertyDto>();
			// 根据valId的集合，查出属性值的集合
			List<ProductPropertyValue> allPropVals = productPropertyValueDao.getProductPropertyValue(allValIdList, prodDto.getProdId());
			for (ProductProperty prodProp : pps) {
				// 封装属性DTO
				ProductPropertyDto prodPropDto = new ProductPropertyDto();
				prodPropDto.setPropId(prodProp.getId());// 属性id
				prodPropDto.setPropName(prodProp.getMemo());// 属性名称

				// 根据propId拿到valueId的集合
				List<Long> valIds = propIdMap.get(prodProp.getId());

				// 根据valueId的集合,查出属性值的集合
				List<ProductPropertyValue> ppvs = getPropValsByIds(valIds, allPropVals);
				List<ProductPropertyValueDto> ppvds = new ArrayList<ProductPropertyValueDto>();
				for (ProductPropertyValue prodPropVal : ppvs) {
					// 封装属性值DTO
					ProductPropertyValueDto ppvd = new ProductPropertyValueDto();
					ppvd.setValueId(prodPropVal.getId());// 属性值id
					if (AppUtils.isNotBlank(prodPropVal.getAlias())) {
						ppvd.setName(prodPropVal.getAlias());// 属性值名称
					} else {
						ppvd.setName(prodPropVal.getName());// 属性值名称
					}
					ppvd.setPropId(prodProp.getId());// 属性id

					List<String> imgs = valueImagesMap.get(prodPropVal.getId());
					if (AppUtils.isNotBlank(imgs)) {
						prodPropVal.setUrl(imgs.get(0));
					}
					if (selectedVals.contains(prodPropVal.getId())) {
						ppvd.setIsSelected(true);// 是否默认选中
						if (AppUtils.isNotBlank(prodPropVal.getUrl())) {
							prodDto.setPic(prodPropVal.getUrl());
						}
					}
					if (AppUtils.isNotBlank(prodPropVal.getUrl())) {
						ppvd.setPic(prodPropVal.getUrl());
					} else {
						ppvd.setPic(prodPropVal.getPic());
					}
					ppvds.add(ppvd);
				}
				prodPropDto.setProductPropertyValueList(ppvds);
				ppds.add(prodPropDto);
			}
			return ppds;
		} else {
			return null;
		}
	}

	/**
	 * 查找最低价格的sku作为默认的sku.
	 *
	 * @param productSkus
	 * @return the default sku
	 */
	private PresellSkuDto getDefaultSku(List<PresellSkuDto> productSkus) {
		double price = 0;
		int minPos = 0; // 最小值位置
		int size = productSkus.size();
		for (int i = 0; i < size; i++) {
			double minPrice = productSkus.get(i).getPrice();
			if ((minPrice < price || price == 0) && productSkus.get(i).getStatus().equals(1)) {
				price = minPrice;
				minPos = i;
			}
		}
		return productSkus.get(minPos);
	}

	/**
	 * 根据valueId的集合,查出属性值的集合.
	 *
	 * @param valIds
	 * @param allPropVals
	 * @return the prop vals by ids
	 */
	private List<ProductPropertyValue> getPropValsByIds(List<Long> valIds, List<ProductPropertyValue> allPropVals) {
		List<ProductPropertyValue> ppvs = new ArrayList<ProductPropertyValue>();
		for (ProductPropertyValue productPropertyValue : allPropVals) {
			if (valIds.contains(productPropertyValue.getValueId())) {
				ppvs.add(productPropertyValue);
			}
		}
		return ppvs;
	}

}
