/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinImgFile;

/**
 *微信图片Dao
 */
public interface WeixinImgFileDao extends GenericDao<WeixinImgFile, Long> {
     
    public abstract List<WeixinImgFile> getWeixinImgFile();

	public abstract WeixinImgFile getWeixinImgFile(Long id);
	
    public abstract int deleteWeixinImgFile(WeixinImgFile weixinImgFile);
	
	public abstract Long saveWeixinImgFile(WeixinImgFile weixinImgFile);
	
	public abstract int updateWeixinImgFile(WeixinImgFile weixinImgFile);
	
	public abstract PageSupport<WeixinImgFile> getWeixinImgFile(String curPageNO, int pageSize, Long type);

	
 }
