/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.UserGrade;

/**
 * 用户等级Dao.
 */
public interface UserGradeDao extends GenericDao<UserGrade, Integer>{

	public abstract UserGrade getUserGrade(Integer id);

	public abstract void deleteUserGrade(UserGrade userGrade);

	public abstract Integer saveUserGrade(UserGrade userGrade);

	public abstract void updateUserGrade(UserGrade userGrade);

	public abstract List<StatusKeyValueEntity> retrieveUserGrade();

	public abstract PageSupport<UserGrade> getUserGradePage(String curPageNO, UserGrade userGrade);

}
