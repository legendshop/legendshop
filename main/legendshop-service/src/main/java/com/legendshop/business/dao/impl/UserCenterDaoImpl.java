package com.legendshop.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserCenterDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.entity.User;

/**
 * 用户中心
 * 
 */
@Repository
public class UserCenterDaoImpl  implements UserCenterDao {
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;
	
	public User getUser(String userName) {
		return genericJdbcDao.get("select * from ls_user where name = ?", User.class, userName);
	}


}
