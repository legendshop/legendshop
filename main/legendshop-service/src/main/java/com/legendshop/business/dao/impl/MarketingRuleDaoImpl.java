/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MarketingRuleDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.entity.MarketingRule;
import com.legendshop.util.AppUtils;

/**
 * 营销活动规则Dao.
 */
@Repository
public class MarketingRuleDaoImpl extends GenericDaoImpl<MarketingRule, Long> implements MarketingRuleDao  {

	public MarketingRule getMarketingRule(Long id){
		return getById(id);
	}
	
    public int deleteMarketingRule(MarketingRule marketingRule){
    	return delete(marketingRule);
    }
	
	public Long saveMarketingRule(MarketingRule marketingRule){
		return save(marketingRule);
	}
	
	public int updateMarketingRule(MarketingRule marketingRule){
		return update(marketingRule);
	}
	
	public PageSupport getMarketingRule(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public List<MarketingRuleDto> findMarketingRuleByMarketingId(Long shopId,Long marketId) {
		List<MarketingRuleDto> marketingRuleDtos= query("select rule_id as ruleId,off_amount as offAmount,full_price as fullPrice,off_discount,cut_amount as cutAmount,type as type,create_time from ls_marketing_rule where market_id=? and shop_id=? order by full_price asc ,create_time desc", MarketingRuleDto.class,
				new Object[]{marketId,shopId});
		if(AppUtils.isNotBlank(marketingRuleDtos)){
			for (int i = 0; i < marketingRuleDtos.size(); i++) {
				MarketingRuleDto dto=marketingRuleDtos.get(i);
				if(MarketingTypeEnum.MAN_JIAN.value().equals(dto.getType())){
					String marketRuleNm="满减 单笔满 ¥"+dto.getFullPrice()+"，立减¥"+dto.getOffAmount();
					dto.setMarketRuleNm(marketRuleNm);
				}else if(MarketingTypeEnum.MAN_ZE.value().equals(dto.getType())){
					String marketRuleNm="满折优惠 单笔满 ¥"+dto.getFullPrice()+"，打 "+"" +"折";
					dto.setMarketRuleNm(marketRuleNm);
				}
			}
		}
		return marketingRuleDtos;
	}

    @Override
    public void deleteMarketingRuleByMarketing(Long marketingId) {
        String sql = "delete from  ls_marketing_rule where market_id=?";
        update(sql,marketingId);
    }

}
