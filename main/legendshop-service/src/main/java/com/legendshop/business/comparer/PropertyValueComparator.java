package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.util.AppUtils;

public class PropertyValueComparator implements DataComparer<ProductPropertyValue,ProductPropertyValue>{

	@Override
	public boolean needUpdate(ProductPropertyValue dto,ProductPropertyValue dbObj, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(ProductPropertyValue newPropValue, ProductPropertyValue originPropValue) {
		if(AppUtils.isNotBlank(newPropValue) && AppUtils.isNotBlank(originPropValue)){
			if(originPropValue.getName().equals(newPropValue.getName()) && originPropValue.getValueId().equals(newPropValue.getValueId())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public ProductPropertyValue copyProperties(ProductPropertyValue newPropValue,Object obj) {
		return newPropValue;
	}

}
