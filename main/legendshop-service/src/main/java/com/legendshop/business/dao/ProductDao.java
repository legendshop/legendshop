/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.dto.stock.StockArmDto;
import com.legendshop.model.entity.AdminDashboard;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.prod.FullActiveProdDto;

/**
 * 商品Dao
 */
public interface ProductDao extends GenericDao<Product, Long>{

	/**
	 * 获得店铺推荐产品.
	 * 
	 */
	public abstract List<Product> getCommendProd(final Long shopId, final int total);
	

	/**
	 * 获取店铺最新的商品
	 */
	public abstract List<Product> getNewestProd(final Long shopId, final int total);
	

	/**
	 * 根据商品Id获取商品.
	 * 
	 * @param prodId
	 *            the prod id
	 * @return the prod detail
	 */
	public abstract ProductDetail getProdDetail(final Long prodId);

	/**
	 * 根据商品ID列表获取商品详情
	 * 
	 */
	public abstract List<ProductDetail> getProdDetail(final Long[] prodId);

	/**
	 * 获取热门商品
	 * 
	 */
	public abstract List<Product> getHotOn(Long shopId, Long categoryId);
	
	/**
	 * 获取全场商品
	 * 
	 */
	public abstract List<Product> getHotAll(Integer max);

	/**
	 * 查看某个商城的热门商品
	 * 
	 */
	public abstract List<Product> getHotViewProd(Long shopId, Integer number);

	/**
	 * 根据ID获取一个产品.
	 */
	public abstract Product getProduct(Long prodId);
	
	/**
	 * 根据ID获取一个产品.
	 * No cache
	 */
	public abstract Product getProductById(Long prodId);

	/**
	 * 更新商品查看数
	 */
	public abstract void updateProdViews(Long prodId);

	/**
	 * 更新产品和缓存.
	 * 
	 */
	public abstract void updateProduct(Product product);
	
	
	/**
	 * 只是更新产品，并不更新缓存.
	 * 
	 */
	public abstract void updateOnly(Product product);

	/**
	 * 保存商品
	 */
	public abstract Long saveProduct(Product product);

	/**
	 * 根据商品Id删除商品.
	 *
	 */
	public abstract String deleteProd(Long shopId, Long prodId,boolean isAdmin);
	
	/**
	 * 删除商品.
	 */
	public abstract void deleteProd(Product product);

	/**
	 * 获取商品
	 * 
	 */
	public abstract Product getProd(Long prodId,Long shopId);

	/**
	 * 获取商品参数
	 */
	public abstract String getProdParameter(Long prodId);

	/**
	 * 获取热门评论
	 * 
	 */
	public abstract List<Product> getHotComment(Long shopId, Long categoryId, int maxNum);

	/**
	 * 获取热门推荐商品
	 */
	public abstract List<Product> getHotRecommend(Long shopId, Long sortId, int maxNum);

	/**
	 * 获取商品SKU的库存数
	 */
	public abstract Integer getStocksByLockMode(Long prodId);


	/**
	 * 更新商品评论得分 和 评论数, 评论得分是累加, 评论数是直接更新
	 * @param scores
	 */
	public abstract void updateReviewScoresAndComments(Integer scores, Integer comments, Long prodId);

	/**
	 * 更改商品状态
	 * @param status
	 */
	public abstract void updateStatus(Integer status,Long productId,String auditOpin);

	/**
	 * Advanced Search 高级搜索
	 * @param sqlQuery
	 * @return
	 */
	PageSupport getAdvancedSearchProds(SimpleSqlQuery sqlQuery);

	public abstract List<ProdParamDto> getPropGroupByProdId(Long prodId);

	
	/**
	 * Advanced Search 高级搜索
	 * @return
	 */
	PageSupport<ProductDetail> getAdvancedSearchProds(Integer curPageNO,ProductSearchParms parms);
	
	/**
	 * 获得摸个产品分类下的热销产品.
	 *
	 */
	public abstract List<Product> getHotsaleProdByCategoryId(final Long categoryId, final int total);
	
	/** 获取后台首页的计数  **/
	public List<AdminDashboard> queryAdminDashboard();


	public abstract void updateProd(Long prodId, String string, Map<String, Object> params);


	public abstract void clearProductDto(Long prodId);


	public abstract void updateBuys(long buys, Long prodId);


	public abstract void updateProductActualStocks(Long prodId, int stocks);

	/** 商品销售排行 **/
	public abstract List<SalesRankDto> getSalesRank(Long shopId,Date startDate);
	
	/** 根据 shopId 获取 商品每个状态的数量  **/
	public abstract List<KeyValueEntity> getProdCounts(Long shopId);

	/** 商品的版本号加1**/
	public abstract void updateProdVersion(Long prodId);

	/** 更新商品的 分销佣金**/
	public abstract void updateProdDistCommis(Product product);


	public abstract AuctionsDto getAuctionProd(Long prodId,Long skuId);


	public abstract int checkStock(Long prodId, Long skuId);


	public void updateProdType(Long prodId, String type,Long seckillId);


	public Integer getStocksByMode(Long prodId);
	
	public abstract long getProdTotalCount(Long shopId);

	public abstract List<Long> getProdByPage(Long shopId, int currPage, Integer pageSize);

	/**
	 * 更新商品库存
	 * @param prodId
	 */
	public abstract void updateProdStocks(Long prodId);

	public abstract void updateProdActualStocks(Long prodId);
	
	public abstract boolean updataIsGroup(Long prodId,Integer status);

	public abstract Group getGroupPrice(Long prodId);
	
	public abstract Seckill getSeckillPrice(Integer seckillId,Long prodId,Long skuId);
	
	public abstract Auctions getAuctionsPrice(Long prodId,Long skuId);
	
	public abstract PresellProd getPresellProdPrice(Long prodId,Long skuId);
	
	
	

	/**
	 * 获取商品的库存数量
	 * @param productId
	 * @param skuId
	 * @return
	 */
	public abstract int getveryBuyNum(Long productId, Long skuId);

	/**
	 * 根据skuId分别获取库存
	 */
	public List<UpdSkuStockDto> getStockBySkuId(Long skuId);


	public abstract List<ProductDetail> getLikeProd(Long categoryId);


	public abstract List<ProductExportDto> findExport(String string, Object[] array);


	public abstract List<Product> getProdByCateId(Long id);

	public abstract int sumBuys(Long count, Long prodId);

	public List<Product> getZhiboAppProdList(Long shopId);

	public abstract PageSupport<Product> getProductList(String name, String curPageNO,Long shopId);

	public PageSupport<Product> storeProdctList(Long shopId, String curPageNO, Long fireCat, Long secondCat, Long thirdCat,String keyword, String order);

	public PageSupport<Product> newProd(Long shopId);

	public abstract PageSupport<Product> getProductListPage(String curPageNO, Product product);

	public abstract PageSupport<Product> getAuctionProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProductListPage(String curPageNO, Long shopId,Product product);

	public abstract PageSupport<Product> getProductListPage(String curPageNO, int pageSize, Long shopId, String prodName);

	public abstract PageSupport<Product> getloadProdListPage(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getDustbinProdListPage(String curPageNO, String name, String siteName);

	public abstract PageSupport<StockAdminDto> getStockcontrol(String curPageNO, String productName);

	public abstract PageSupport<Product> getloadSelectProd(Long shopId, String curPageNO, Product product);

	public abstract PageSupport<Product> getProductFloorLoad(String curPageNO, String name,Long shopId);

	public abstract PageSupport<Product> getGroupProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> groupProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getloadProdListPageByshopId(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getloadWantDistProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getSellingProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProdInStoreHouse(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getViolationProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getAuditingProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getAuditFailProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProdInDustbin(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getJoinDistProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getReportForbit(String curPageNO, String userName, Product product);

	public abstract PageSupport<StockArmDto> getStockwarning(Long shopId, String curPageNO, String productName);

	public abstract PageSupport<Product> getStoreProdcts(Long shopId, String curPageNO, Long fireCat, Long secondCat,
			Long thirdCat, String keyword, String order);

	public abstract PageSupport<Product> getAddProdSkuLayoutPage(String curPageNO, Product product, Long shopId);

	public abstract PageSupport<Product> getAddProdSkuLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProductListPage(Product product, String curPageNO, Integer pageSize,
			DataSortResult result);

	public abstract PageSupport<ProductPurchaseRateDto> getPurchaseRate(String curPageNO, Long shopId,
			String prodName, DataSortResult result,Date startDate,Date endDate);

	public abstract PageSupport<Product> getLoadSelectProd(String curPageNO, Product product);
	
	public abstract PageSupport<Product> queryPage(String curPageNO, int pageSize, Long shopId, Product product);

	public abstract PageSupport<Product> getSeckillLayer(String curPageNO, Long shopId, int pageSize, Product product);

	public abstract PageSupport<Product> getProductLists(String curPageNO, int pageSize, Long shopId, Product product);

	public abstract PageSupport<Product> getProductListMobile(String curPageNO, Long brandId);

	public abstract PageSupport<SalesRankDto> querySimplePage(String curPageNO, Long shopId, String prodName,
			Date startDate, Date endDate);

	public abstract void updateProductStocks(Long prodId, long stock);

	public abstract int upStocks(Long prodId, Long stocks);



	Integer batchOffline(List<Long> prodIdList);


	void batchOnlineLine(List<Long> prodIdList);



	public abstract PageSupport<FullActiveProdDto> getFullActiveProdDto(Long shopId, String curPageNo, String prodName);
	
	public abstract PageSupport<Product> getProdductByName(String curPageNo, String prodName);

	public abstract PageSupport<Product> getProdductByShopId(String curPageNo, String prodName,Long shopId);

	public abstract Group getGroup(Long activeId);


	public abstract void update(Group group);


	public abstract List<MergeGroupSelProdDto> queryMergeGroupProductToSku(Long shopId, Long[] prodIds);


	public abstract List<MergeGroupSelProdDto> queryMergeGroupProductByMergeId(Long id);


	public abstract PageSupport<Product> queryMergeGroupProductList(Long shopId, String curPageNO, String prodName);


	public abstract PageSupport<Product> queryMergeGroupProduct(Long shopId);


	public abstract String getProdPicByMergeId(Long mergeId);

	/**
	 * 获取除去shopId商家下的n个商品
	 * @param shopId
	 * @return
	 */
	public abstract List<Product> getProductsTop(Long shopId, Integer prodNums);


	public abstract boolean checkShopSubCategoryUse(Long subCatId);


	public abstract boolean checkShopCategoryUse(Long shopCatId);


	public abstract boolean checkShopNextCategoryUse(Long nextCatId);


	public abstract Boolean isAttendActivity(Long prodId);

	/**
	 * 获取优惠券商品
	 * @param prodIds
	 * @param curPageNO
	 * @return
	 */
	public abstract PageSupport<Product> getCouponProds(Long shopId, List<Long> prodIds, String curPageNO, String orders, String prodName);
	
	/**
	 * 根据shopId获取商家所有商品
	 * @param shopId
	 * @return
	 */
	public abstract List<Product> findAllProduct(Long shopId);
	
	public abstract int updateProdList(List<Product> list);

	PageSupport<StockArmDto> getStockList(Long shopId, String curPageNO, int pageSize);

	/**
	 * 获取用户购买过的商品
	 *
	 * @param currPage
	 * @param size
	 * @param prodIds
	 * @return
	 */
    PageSupport<Product> getProdByUser(String currPage, int size, List<Long> prodIds);

	/**
	 * 获取预售商品
	 * @param id
	 * @return
	 */
	List<PresellSkuDto> queryPresellProductByPresellId(Long id);

	/**
	 * 根据用户获取商品id集合
	 * @param shopId
	 * @return
	 */
	List<Long> getProdductIdByShopId(Long shopId);

	/**
	 * 根据属性ID获取prod
	 * @param propId
	 * @return
	 */
	List<Product> findProdByPropId(Long propId);

	/**
	 * 通过团购id获取商品
	 * @param id
	 * @return
	 */
	List<GroupSelProdDto> queryGroupProductByGroupId(Long id);

	/**
	 * 商家端获取商品列表
	 * @param curPageNO
	 * @param shopId
	 * @param status
	 * @param supportDist
	 * @return
	 */
	PageSupport<Product> queryProductList(String curPageNO, Long shopId, Integer status, Integer supportDist,String prodName);

	/**
	 * 根据IDS获取商品
	 * @param productIds
	 * @return
	 */
	List<Product> getProductListByIds(List<Long> productIds);

	/**
	 * 批量更新商品
	 * @param selProdictList
	 */
	void batchUpdateProduct(List<Product> selProdictList);

	PageSupport<Product> getProdductByShopId(String curPageNO, String name, Long shopId, Long shopCatId, Integer shopCatType);

	/**
	 * 清除商品缓存
	 * @param product
	 */
	void cleanProductCache(Product product);

	/**
	 * app商家端 获取包邮商品列表
	 * @param shopId
	 * @param curPageNO
	 * @return
	 */
	PageSupport<FullActiveProdDto> getAppFullActiveProdDto(Long shopId, String curPageNO,String name,Long shopCatId ,Integer shopCatType);

	void updateProdAllStocks(Long prodId);

	Boolean updateSearchWeight(Long prodId, Double searchWeight);

	List<Long> getAllprodIdStr(Long shopId, Integer status);

	/**
	 * 根据店铺Id获取商品
	 * @param shopIds
	 * @param curPageNo
	 * @param orders
	 * @return
	 */
	PageSupport<Product> queryProdByShopIds(List<Long> shopIds, String curPageNo, String orders);

	/**
	 * 获取包邮活动可选商品
	 * @param curPageNO
	 * @param prodName
	 * @param shopId
	 * @param shopCatId
	 * @param shopCatType
	 * @return
	 */
	PageSupport<Product> getFreeShippingProductByShopId(String curPageNO, String prodName, Long shopId, Long shopCatId, Integer shopCatType);

	/**
	 * 根据商品id获取商品服务说明
	 * @param prodId
	 */
	String getServerShowById(Long prodId);

}
