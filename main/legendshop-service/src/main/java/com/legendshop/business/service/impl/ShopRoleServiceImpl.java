/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.ShopPermDao;
import com.legendshop.business.dao.ShopRoleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopPerm;
import com.legendshop.model.entity.ShopPermId;
import com.legendshop.model.entity.ShopRole;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.spi.service.ShopRoleService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 *商家角色服务
 */
@Service("shopRoleService")
public class ShopRoleServiceImpl  implements ShopRoleService{

	@Autowired
    private ShopRoleDao shopRoleDao;
    
	@Autowired
    private ShopPermDao shopPermDao;

    public ShopRole getShopRole(Long id) {
        return shopRoleDao.getShopRole(id);
    }

    public void deleteShopRole(ShopRole shopRole) {
        shopRoleDao.deleteShopRole(shopRole);
        //关联删除职位权限表
        shopPermDao.deleteShopPermByRoleId(shopRole.getId());
    }

    public Long saveShopRole(ShopRole shopRole) {
        if (!AppUtils.isBlank(shopRole.getId())) {
            updateShopRole(shopRole);
            return shopRole.getId();
        }
        return shopRoleDao.saveShopRole(shopRole);
    }

    public void updateShopRole(ShopRole shopRole) {
        shopRoleDao.updateShopRole(shopRole);
    }

	@Override
	public ShopRole getShopRole(Long shopId, String name) {
		return shopRoleDao.getShopRole(shopId,name);
	}

	@Override
	public List<ShopRole> getShopRolesByShopId(Long shopId) {
		return shopRoleDao.getShopRolesByShopId(shopId);
	}

	@Override
	public PageSupport<ShopRole> getShopRolePage(String curPageNO, Long shopId) {
		return shopRoleDao.getShopRolePage(curPageNO,shopId);
	}

	/* 
	 * 保存设置职位权限
	 */
	@Override
	public void saveJobRole(ShopRole shopRole,List<ShopMenuGroup> shopMenuGroups, Long shopId) {
		// 更新
		if (shopRole.getId() != null) {
			ShopRole orinShopRole = this.getShopRole(shopRole.getId());
			if (!shopId.equals(orinShopRole.getShopId())) {
				throw new BusinessException("shopRole:" + shopRole.getId() + " is not ower to shop " + shopId);
			}
			orinShopRole.setName(shopRole.getName().trim());
			this.updateShopRole(orinShopRole);

			List<String> shopPermList = shopPermDao.getShopPermsByRoleId(shopRole.getId());
			List<String> menuLabels = shopRole.getMenuLabels();
			
			//获取选择的权限列表
			List<String> menuFunctions = shopRole.getMenuFunctions();

			List<String> addList = new ArrayList<String>();
			
			//初始化要更新删除掉的权限，下面通过比较，不需要删除的剔除掉
			List<String> delList = new ArrayList<String>(shopPermList);

			// 找出哪些需要删除，哪些需要新增
			if (AppUtils.isNotBlank(menuLabels) || AppUtils.isNotBlank(shopPermList)) {
				Iterator<String> dtoIt = menuLabels.iterator();
				while (dtoIt.hasNext()) {
					String menuLable = dtoIt.next();
					if (shopPermList.contains(menuLable)) {//如果权限菜单包含有选择的菜单，则不删除，更新子权限
						
						// 处理选择的子权限（查看、编辑）
						String function = getFunction(menuFunctions,menuLable);
						
						// 更新子权限
						shopPermDao.updateFunction(function,shopRole.getId(),menuLable);
						
						// 将菜单从删除列表移除
						delList.remove(menuLable);
					} else {//如果权限菜单不包含有选择的菜单，则为新增菜单
						addList.add(menuLable);
					}
				}
			}

			// 删除的权限
			if (AppUtils.isNotBlank(delList)) {
				List<ShopPerm> delShopPermList = new ArrayList<ShopPerm>();
				for (String label : delList) {
					ShopPerm shopPerm = new ShopPerm();
					ShopPermId shopPermId = new ShopPermId();
					shopPermId.setRoleId(shopRole.getId());
					shopPermId.setLabel(label);
					shopPerm.setId(shopPermId);
					delShopPermList.add(shopPerm);
				}
				shopPermDao.deleteUserRole(delShopPermList);
			}

			// 新增的权限
			if (AppUtils.isNotBlank(addList)) {
				List<ShopPerm> addShopPermList = new ArrayList<ShopPerm>();
				for (String label : addList) {
					
					// 处理选择的子权限（查看、编辑）
					String function = getFunction(menuFunctions,label);
					
					ShopPerm shopPerm = new ShopPerm();
					ShopPermId shopPermId = new ShopPermId();
					shopPermId.setRoleId(shopRole.getId());
					shopPermId.setLabel(label);
					shopPermId.setFunction(function);
					shopPerm.setId(shopPermId);
					addShopPermList.add(shopPerm);
				}
				shopPermDao.saveShopPerm(addShopPermList);
			}

		} else {// 保存
			shopRole.setName(shopRole.getName().trim());// 去空格
			shopRole.setShopId(shopId);
			shopRole.setRecDate(new Date());
			Long shopRoleId = this.saveShopRole(shopRole);

			//获取选择保存的菜单列表
			List<String> menuLabels = shopRole.getMenuLabels();
			
			//获取选择的权限列表
			List<String> menuFunctions = shopRole.getMenuFunctions();
			
			List<ShopMenu> shopMenuList = new ArrayList<ShopMenu>();
			for (ShopMenuGroup group : shopMenuGroups) {
				for (ShopMenu menu : group.getShopMenuList()) {
					if (menuLabels.contains(menu.getLabel())) {
						
						// 处理选择的子权限（查看、编辑）
						String function = getFunction(menuFunctions,menu.getLabel());
						
						//构建被选择的菜单列表
						menu.setFunctions(function);
						shopMenuList.add(menu);
					}
				}
			}

			List<ShopPerm> shopPermList = new ArrayList<ShopPerm>();
			for (ShopMenu shopMenu : shopMenuList) {
				
				ShopPerm shopPerm = new ShopPerm();
				ShopPermId shopPermId = new ShopPermId();
				shopPermId.setRoleId(shopRoleId);
				shopPermId.setLabel(shopMenu.getLabel());
				shopPerm.setId(shopPermId);
				
				//保存权限
				shopPermId.setFunction(shopMenu.getFunctions());
				shopPermList.add(shopPerm);
			}
			shopPermDao.saveShopPerm(shopPermList);
		}

	}

	/**
	 * 处理选择的子权限（查看、编辑）
	 * @param menuFunctions
	 * @param menuLable
	 * @return
	 */
	private String getFunction(List<String> menuFunctions, String menuLable) {
		
		List<String> funcs = new ArrayList<>();
		for (String function : menuFunctions) {
			
			if(!function.contains(menuLable)){
				continue;
			}
			//根据标识切割字符串
			String[] functionList = function.split("-");
			String func = functionList[1];
			funcs.add(func);
		}
		
		String function = StringUtil.join(funcs, ",");
		return function;
	}
	
	
}
