/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CategoryCommDao;
import com.legendshop.model.entity.CategoryComm;
import com.legendshop.spi.service.CategoryCommService;

@Service("categoryCommService")
public class CategoryCommServiceImpl implements CategoryCommService {

	@Autowired
	private CategoryCommDao categoryCommDao;
	
	@Override
	public List<CategoryComm> getCategoryCommListByShopId(Long shopId) {
		return categoryCommDao.getCategoryCommListByShopId(shopId);
	}

	@Override
	public void delCategoryComm(Long id) {
		categoryCommDao.delCategoryComm(id);
	}

	@Override
	public Long saveCategoryComm(CategoryComm categoryComm) {
		return categoryCommDao.saveCategoryComm(categoryComm);
	}

	public CategoryCommDao getCategoryCommDao() {
		return categoryCommDao;
	}

	@Override
	public CategoryComm getCategoryComm(Long categoryId, Long shopId) {
		return categoryCommDao.getCategoryComm(categoryId, shopId);
	}

	@Override
	public void delCategoryComm(Long id, Long shopId) {
		categoryCommDao.delCategoryComm(id,shopId);
	}
	
	
}
