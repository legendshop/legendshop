/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdUsefulDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProdUseful;

/**
 *产品是否有用的服务
 *
 */
@Repository
public class ProdUsefulDaoImpl extends GenericDaoImpl<ProdUseful, Long> implements ProdUsefulDao  {
     
	public ProdUseful getProdUseful(Long id){
		return getById(id);
	}
	
    public int deleteProdUseful(ProdUseful prodUseful){
    	return delete(prodUseful);
    }
	
	public Long saveProdUseful(ProdUseful prodUseful){
		return save(prodUseful);
	}
	
	public int updateProdUseful(ProdUseful prodUseful){
		return update(prodUseful);
	}

	@Override
	public Integer getUseful(String userId, Long prodComId) {
		return jdbcTemplate.queryForObject("select count(*) from ls_prod_useful where user_id = ? and prod_comm_id = ?", Integer.class,userId,prodComId);
	}
	
 }
