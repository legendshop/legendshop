/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.NewsCategoryDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.NewsCategory;
import com.legendshop.util.AppUtils;

/**
 * 文章分类Dao.
 */
@Repository
public class NewsCategoryDaoImpl extends GenericDaoImpl<NewsCategory, Long> implements NewsCategoryDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.NewsCategoryDao#deleteNewsCategoryById(java
	 * .lang.Long)
	 */
	@Override
	@CacheEvict(value="NewsList",allEntries=true)
	public void deleteNewsCategoryById(Long id) {
		deleteById(id);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.NewsCategoryDao#updateNewsCategory(com.legendshop
	 * .model.entity.NewsCategory)
	 */
	@Override
	@CacheEvict(value="NewsList",allEntries=true)
	public void updateNewsCategory(NewsCategory newsCategory) {
		update(newsCategory);

	}

	@Override
	public List<NewsCategory> getNewsCategoryList(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}

	@Override
	public NewsCategory getNewsCategoryById(Long id) {
		return getById(id);
	}

	@Override
	public List<NewsCategory> getNewsCategoryList() {
		return this.queryByProperties(new EntityCriterion());
	}

	@Override
	public PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO, NewsCategory newsCategory) {
		CriteriaQuery cq = new CriteriaQuery(NewsCategory.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		
		cq.eq("userName", newsCategory.getUserName());
		if (!AppUtils.isBlank(newsCategory.getNewsCategoryName())) {
			cq.like("newsCategoryName", "%" + newsCategory.getNewsCategoryName().trim() + "%");
		}
		cq.eq("status", newsCategory.getStatus());
		cq.addAscOrder("seq");
		return queryPage(cq);
		
	}

	@Override
	public PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO) {
		
		curPageNO = curPageNO == null? "1" : curPageNO;
		CriteriaQuery cq = new CriteriaQuery(NewsCategory.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("status", 1);
		cq.addAscOrder("seq");
		return queryPage(cq);
	}
}
