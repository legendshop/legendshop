/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeiXinKeywordResponseDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * 微信关键字回复Dao
 */
@Repository
public class WeiXinKeywordResponseDaoImpl extends GenericDaoImpl<WeiXinKeywordResponse, Long> implements WeiXinKeywordResponseDao  {

	public WeiXinKeywordResponse getWeiXinKeywordResponse(Long id){
		return getById(id);
	}
	
    public int deleteWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse){
    	return delete(weiXinKeywordResponse);
    }
	
	public Long saveWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse){
		return save(weiXinKeywordResponse);
	}
	
	public int updateWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse){
		return update(weiXinKeywordResponse);
	}

	@Override
	public PageSupport<WeiXinKeywordResponse> getWeiXinnKeywordResponse(String curPageNO) {
		 CriteriaQuery cq = new CriteriaQuery(WeiXinKeywordResponse.class, curPageNO);
		return queryPage(cq);
	}
	
 }
