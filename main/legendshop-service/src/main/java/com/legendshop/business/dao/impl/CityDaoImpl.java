/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CityDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.City;

/**
 * The Class CityDaoImpl.
 */
@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Integer> implements CityDao {

	@Override
	public List<KeyValueEntity> getCitiesList(Integer provinceid) {
		List<KeyValueEntity> result =  this.query("select id ,city  from ls_cities where provinceid=? ", new Object[]{provinceid}, new RowMapper<KeyValueEntity>() {
			@Override
			public KeyValueEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
				return  new KeyValueEntity(String.valueOf(rs.getLong("id")), rs.getString("city"));
			}
		});
		return result;
	}

	@Override
	@Cacheable(value = "CityList", key = "'Province_'+ #provinceid")
	public List<City> getCityByProvinceid(Integer provinceid) {
		return query("select id, cityid, city, provinceid from ls_cities where provinceid=?", City.class, provinceid);
	}

	@Override
	public void deleteCityByProvinceid(Integer id) {
		update("delete from ls_cities where provinceid = ?", id);
	}

	@Override
	@CacheEvict(value = "CityList", allEntries=true)
	public void deleteCity(Integer id) {
		deleteById(id);
	}

	@Override
	public void updateCity(City city) {
		update(city);
	}

	@Override
	@Cacheable(value = "CityList", key = "'cityName_'+ #cityName")
	public List<City> getCityByName(String cityName) {
		return this.query("select * from ls_cities where city = ?", City.class, cityName);
	}

}
