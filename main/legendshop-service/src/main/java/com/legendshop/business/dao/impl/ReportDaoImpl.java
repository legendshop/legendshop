/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ReportDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.dao.dialect.Dialect;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProdReportDto;
import com.legendshop.model.dto.ReportDto;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.model.report.PurchaseRankDto;
import com.legendshop.model.report.ReportRequest;
import com.legendshop.model.report.SaleAnalysisDto;
import com.legendshop.model.report.SaleAnalysisResponse;
import com.legendshop.model.report.SalerankDto;
import com.legendshop.model.report.SalesMonthDto;
import com.legendshop.model.report.SalesResponse;
import com.legendshop.model.report.SalesYearDto;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 报表实现类.
 */
@Repository
public class ReportDaoImpl implements ReportDao {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ReportDaoImpl.class);

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/** The generic jdbc dao. */
	@Autowired
	private GenericJdbcDao genericJdbcDao;
	
	/** The dialect. */
	@Autowired
	private Dialect dialect;
		
	/**
	 * 销售分析.
	 *
	 * @param saleAnalysisDto the sale analysis dto
	 * @return the sale analysis
	 */
	@Override
	public SaleAnalysisResponse getSaleAnalysis(SaleAnalysisDto saleAnalysisDto) {
		
		QueryMap paramMap = convertParamMap(saleAnalysisDto);
		
		String analysisSql = ConfigCode.getInstance().getCode("report.analysis", paramMap);
		SaleAnalysisResponse resopnse = genericJdbcDao.get(analysisSql, SaleAnalysisResponse.class, paramMap.toArray());
		// 总会员数
		long totalMember = genericJdbcDao.getLongResult("select count(*) from ls_user where enabled = 1");
		 
		// 总访问次数 
		String totalViewsSql = ConfigCode.getInstance().getCode("report.totalViews", paramMap);
		Long totalViews = genericJdbcDao.getLongResult(totalViewsSql,paramMap.toArray());
		
		resopnse.setTotalMember(totalMember);
		resopnse.setTotalViews(totalViews);
		
		return resopnse;
	}

	
	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.ReportDao#getSales(com.legendshop.model.report.ReportRequest)
	 */
	@Override
	public SalesResponse getSales(ReportRequest salesRequest) {
		QueryMap paramYearMap = new QueryMap();
		QueryMap paramMonthMap = new QueryMap();
		//paramYearMap.hasAllDataFunction("shopName",salesRequest.getUserName());
		//DataFunctionUtil.hasAllDataFunction(paramYearMap,"shopName" ,salesRequest.getUserName());
		//String selectedYear = salesRequest.getYear();
		String selectedYear = "";
		//String selectedMonth = salesRequest.getMonth();
		String selectedMonth = "";
		if (AppUtils.isBlank(selectedYear) || AppUtils.isBlank(selectedYear) ) {
			 selectedYear = DateUtil.getNowYear();
			 selectedMonth = DateUtil.getNowMonth();
		}

		Date monthStartDate = DateUtil.getDate(selectedYear, selectedMonth, "01");
		Date monthEndDate = DateUtil.getMonth(monthStartDate, 1);
		paramMonthMap.put("startDate",monthStartDate);
		paramMonthMap.put("endDate", monthEndDate);
		
		Date yearStartDate = DateUtil.getDate(selectedYear, "01", "01");
		Date yearEndDate = DateUtil.getYear(yearStartDate, 1);
		paramYearMap.put("startDate",yearStartDate);
		paramYearMap.put("endDate", yearEndDate);
		
		String getYearSalesSQL = ConfigCode.getInstance().getCode("report.getYearSales", paramYearMap);
		List<SalesYearDto> salesYearList = genericJdbcDao.query(getYearSalesSQL,SalesYearDto.class,paramYearMap.toArray());
		
		String getMonthSalesSQL = ConfigCode.getInstance().getCode("report.getMonthSales", paramMonthMap);
		List<SalesMonthDto> salesMonthList = genericJdbcDao.query(getMonthSalesSQL,SalesMonthDto.class,paramMonthMap.toArray());
		
		
		SalesResponse salesResponse = new SalesResponse();
		salesResponse.setUserName(salesRequest.getUserName());
		salesResponse.setSelectedYear(selectedYear);
		salesResponse.setSelectedMonth(selectedMonth);
		salesResponse.setYearTotalAmount(convertTotalAmountSalesYearList(salesYearList));
		salesResponse.setYearTotalOrders(convertTotalOrderSalesYearList(salesYearList));
		salesResponse.setMonthTotalAmount(convertTotalAmountSalesMonthList(salesMonthList,selectedYear,selectedMonth));
		salesResponse.setMonthTotalOrders(convertTotalOrderSalesMonthList(salesMonthList,selectedYear,selectedMonth));
		return salesResponse;
	}

	/**
	 * Convert param map.
	 *
	 * @param saleAnalysisDto the sale analysis dto
	 * @return the query map
	 */
	private QueryMap convertParamMap(SaleAnalysisDto saleAnalysisDto){
		QueryMap map = new QueryMap();
		if(!AppUtils.isBlank(saleAnalysisDto.getUserName())){
			map.put("userName", saleAnalysisDto.getUserName());
		}
		if (!AppUtils.isBlank(saleAnalysisDto.getStartDate())) {
			map.put("startDate", saleAnalysisDto.getStartDate());
		}
		if (!AppUtils.isBlank(saleAnalysisDto.getEndDate())) {
			Date endDate = DateUtils.getDay(saleAnalysisDto.getEndDate(), 1);
			map.put("endDate", endDate);
		}
		return map;
	}
	


	/**
	 * Sets the sales year list.
	 *
	 * @param yearList the year list
	 * @return the list
	 */
	private List<List<Object>> convertTotalAmountSalesYearList(List<SalesYearDto> yearList) {
		List<List<Object>> salesYearList =  new ArrayList<List<Object>>(12);
		for (int i = 1; i <= 12; i++) {
			List<Object> list = new ArrayList<Object>(2);
			SalesYearDto salesYearDto = parseSalesYearDto(yearList, i);
			if(salesYearDto == null){
				salesYearDto = constructSalesYearDto(i);
			}
			list.add(i);
			list.add(salesYearDto.getTotalAmount());
			salesYearList.add(list);
		}
		return salesYearList;
	}
	
	
	/**
	 * Convert total order sales year list.
	 *
	 * @param yearList the year list
	 * @return the list
	 */
	private List<List<Object>> convertTotalOrderSalesYearList(List<SalesYearDto> yearList) {
		List<List<Object>> salesYearList =  new ArrayList<List<Object>>(12);
		for (int i = 1; i <= 12; i++) {
			List<Object> list = new ArrayList<Object>(2);
			SalesYearDto salesYearDto = parseSalesYearDto(yearList, i);
			if(salesYearDto == null){
				salesYearDto = constructSalesYearDto(i);
			}
			list.add(i);
			list.add(salesYearDto.getTotalOrders());
			salesYearList.add(list);
		}
		return salesYearList;
	}

/**
 * Sets the sales month list.
 *
 * @param monthList the month list
 * @param selectedYear the selected year
 * @param selectedMonth the selected month
 * @return the list
 */
	private List<List<Object>> convertTotalAmountSalesMonthList(List<SalesMonthDto> monthList,String selectedYear, String selectedMonth) {
		int daysInMOnth = DateUtil.daysInMonth(selectedYear, selectedMonth);
		List<List<Object>> salesMonthList= new ArrayList<List<Object>>(daysInMOnth);
		
		for (int i = 1; i <= daysInMOnth; i++) {
			List<Object> list= new ArrayList<Object>();
			SalesMonthDto salesMonthDto = parseSalesMonthDto(monthList, i);
			if(salesMonthDto == null){
				salesMonthDto = constructSalesMonthDto(i);
			}
			list.add(i);
			list.add(salesMonthDto.getTotalAmount());
			salesMonthList.add(list);
		}
		return salesMonthList;
	}
	
	/**
	 * Convert total order sales month list.
	 *
	 * @param monthList the month list
	 * @param selectedYear the selected year
	 * @param selectedMonth the selected month
	 * @return the list
	 */
	private List<List<Object>> convertTotalOrderSalesMonthList(List<SalesMonthDto> monthList,String selectedYear, String selectedMonth) {
		int daysInMOnth = DateUtil.daysInMonth(selectedYear, selectedMonth);
		List<List<Object>> salesMonthList= new ArrayList<List<Object>>(daysInMOnth);
		
		for (int i = 1; i <= daysInMOnth; i++) {
			List<Object> list= new ArrayList<Object>();
			SalesMonthDto salesMonthDto = parseSalesMonthDto(monthList, i);
			if(salesMonthDto == null){
				salesMonthDto = constructSalesMonthDto(i);
			}
			list.add(i);
			list.add(salesMonthDto.getTotalOrders());
			salesMonthList.add(list);
		}
		return salesMonthList;
	}
	
	/**
	 * Parses the sales year dto.
	 *
	 * @param salesYearList the sales year list
	 * @param salesMonth the sales month
	 * @return the sales year dto
	 */
	private SalesYearDto parseSalesYearDto(List<SalesYearDto> salesYearList,int salesMonth){
		SalesYearDto result = null;
		if(salesYearList == null){
			return result;
		}
		for (SalesYearDto salesYearDto : salesYearList) {
			if(salesYearDto.getSalesMonth() == salesMonth){
				result = salesYearDto;
				break;
			}
		}
		return result;
	}

	/**
	 * 构造默认对象.
	 *
	 * @param salesMonth the sales month
	 * @return the sales year dto
	 */
	private SalesYearDto constructSalesYearDto(int salesMonth){
		SalesYearDto dto = new SalesYearDto();
		dto.setSalesMonth(salesMonth);
		dto.setTotalAmount(0d);
		dto.setTotalOrders(0l);
		return dto;
	}
	
	/**
	 * Parses the sales month dto.
	 *
	 * @param salesMonthList the sales month list
	 * @param salesDay the sales day
	 * @return the sales month dto
	 */
	private SalesMonthDto parseSalesMonthDto(List<SalesMonthDto> salesMonthList,int salesDay){
		SalesMonthDto result = null;
		if(salesMonthList == null){
			return result;
		}
		for (SalesMonthDto salesMonthDto : salesMonthList) {
			if(salesMonthDto.getSalesDay() == salesDay){
				result = salesMonthDto;
				break;
			}
		}
		return result;
	}

	/**
	 * 构造默认对象.
	 *
	 * @param salesDay the sales day
	 * @return the sales month dto
	 */
	private SalesMonthDto constructSalesMonthDto(int salesDay){
		SalesMonthDto dto = new SalesMonthDto();
		dto.setSalesDay(salesDay);
		dto.setTotalAmount(0d);
		dto.setTotalOrders(0l);
		return dto;
	}


	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.ReportDao#getOrderQuantity(com.legendshop.model.report.ReportRequest)
	 */
	@Override
	public List<ReportDto> getOrderQuantity(ReportRequest reportRequest) {
		if(Constants.CALCULATION_DAYS.equals(reportRequest.getQueryTerms())){
			return countByDay(reportRequest);
		}
		
		if(Constants.CALCULATION_MONTH.equals(reportRequest.getQueryTerms())){
			return countByMonth(reportRequest);
		}
		
		if(Constants.CALCULATION_YEAR.equals(reportRequest.getQueryTerms())){
			return countByYear(reportRequest);
		}
		
		if(Constants.CALCULATION_WEEK.equals(reportRequest.getQueryTerms())){
			return countByWeek(reportRequest);
		}
		return new ArrayList<ReportDto>();
	}
	
	/**
	 * 按照天数计算 *.
	 *
	 * @param reportRequest the report request
	 * @return the list
	 */
	private List<ReportDto> countByDay(ReportRequest reportRequest){
		
		if(AppUtils.isNotBlank(reportRequest.getSelectedDate())){
			
			Date startDate = reportRequest.getSelectedDate();
			Date endDate = DateUtils.getDay(reportRequest.getSelectedDate(), 1);
			
			
			QueryMap map = new QueryMap();	
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			if(AppUtils.isNotBlank(reportRequest.getShopId())){
				map.put("shopId",reportRequest.getShopId());
			}
			if(AppUtils.isNotBlank(reportRequest.getShopName())){
				map.put("shopName", "%"+  reportRequest.getShopName() + "%");
			}
			
			map.put("status", reportRequest.getStatus());
			String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportCountByDay", map);
			List<ReportDto> result  = genericJdbcDao.query(sql,ReportDto.class,map.toArray());
			return result;
		}
		return new ArrayList<ReportDto>();
	}
	
	/**
	 * 按照月份计算 *.
	 *
	 * @param reportRequest the report request
	 * @return the list
	 */
	private List<ReportDto> countByMonth(ReportRequest reportRequest){
		
		if(AppUtils.isNotBlank(reportRequest.getSelectedYear()) && AppUtils.isNotBlank(reportRequest.getSelectedMonth())){
			
			Date startDate = DateUtil.getDate(String.valueOf(reportRequest.getSelectedYear()), String.valueOf(reportRequest.getSelectedMonth()), "01");//current month
			Date endDate = DateUtil.getMonth(startDate, 1);//next month

			QueryMap map = new QueryMap();	
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			if(AppUtils.isNotBlank(reportRequest.getShopId())){
				map.put("shopId",reportRequest.getShopId());
			}
			if(AppUtils.isNotBlank(reportRequest.getShopName())){
				map.put("shopName", "%"+  reportRequest.getShopName() + "%");
			}
			
			map.put("status", reportRequest.getStatus());
			String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportCountByMonth", map);
			List<ReportDto> result  = genericJdbcDao.query(sql,ReportDto.class,map.toArray());
			return result;
		}
		return new ArrayList<ReportDto>();
	}
	
	/**
	 * 按照年份计算 *.
	 *
	 * @param reportRequest the report request
	 * @return the list
	 */
	private List<ReportDto> countByYear(ReportRequest reportRequest){
		
		if(AppUtils.isNotBlank(reportRequest.getSelectedYear())){
			Date startDate = DateUtil.getDate(String.valueOf(reportRequest.getSelectedYear()), "01", "01");
			Date endDate = DateUtil.getYear(startDate, 1);//next year
			
			QueryMap map = new QueryMap();	
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			if(AppUtils.isNotBlank(reportRequest.getShopId())){
				map.put("shopId",reportRequest.getShopId());
			}
			if(AppUtils.isNotBlank(reportRequest.getShopName())){
				map.put("shopName", "%"+  reportRequest.getShopName() + "%");
			}
			
			map.put("status", reportRequest.getStatus());
			String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportCountByYear", map);
			List<ReportDto> result  = genericJdbcDao.query(sql,ReportDto.class,map.toArray());
		
			return result;
		}		
		
		return new ArrayList<ReportDto>();
	}
	
	/**
	 * 按照周计算 *.
	 *
	 * @param reportRequest the report request
	 * @return the list
	 */
	private List<ReportDto> countByWeek(ReportRequest reportRequest){
		
		if(AppUtils.isNotBlank(reportRequest.getSelectedWeek())){
			String [] week = reportRequest.getSelectedWeek().split("~");		
			Date startDate = null;
			Date endDate = null;
			try {
				startDate = parseDateFormat(week[0]);
				endDate = DateUtils.getDay(parseDateFormat(week[1]), 1);
			} catch (Exception e) {
				log.debug("Parse week day error", e);
			}
			
			
			QueryMap map = new QueryMap();	
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			if(AppUtils.isNotBlank(reportRequest.getShopId())){
				map.put("shopId",reportRequest.getShopId());
			}
			if(AppUtils.isNotBlank(reportRequest.getShopName())){
				map.put("shopName", "%"+  reportRequest.getShopName() + "%");
			}
			
			map.put("status", reportRequest.getStatus());
			String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportCountByWeek", map);
			List<ReportDto> result  = genericJdbcDao.query(sql,ReportDto.class,map.toArray());
			
			return result;
		}
		
		return new ArrayList<ReportDto>();
	}
	
	/**
	 * 获取店铺有效的订单.
	 *
	 * @param shopId the shop id
	 * @return the list
	 */
	@Override
	public List<ReportDto> queryShopSales(Long shopId) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String nows = dateFormat.format(new Date());
		Date endDate = null;
		try {
			endDate = dateFormat.parse(nows);
		} catch (ParseException e) {
			log.debug("",e);
		}
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(endDate, Integer.parseInt(dashboardDate));
		
		//因为时间范围endDate的值是0000-00-00 00:00:00，导致当天下单，且endDate为最后一天，导致当天下单且已付款的订单无法统计，所以要获取endDate的最后一刻
		Date endMonment = DateUtils.getEndMonment(endDate);
		
		String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportQueryShopSales");
		List<ReportDto> salesList = genericJdbcDao.query(sql,ReportDto.class,shopId,startDate,endMonment);
		return salesList;
	}


	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.ReportDao#querySubCounts(java.lang.Long)
	 */
	@Override
	public List<Integer> querySubCounts(Long shopId) {
		return null;
	}
	

	/**
	 * 查看商品走势图.
	 *
	 * @param shopId the shop id
	 * @param prodId the prod id
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the prod trend
	 */
	@Override
	public List<ProdReportDto> getProdTrend(Long shopId, Long prodId, Date startDate, Date endDate) {
		String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportGetProdTrend");
		return genericJdbcDao.query(sql, ProdReportDto.class, shopId,prodId,startDate,endDate);
	}


	/**
	 * 根据时间查找各个城市的订单量.
	 *
	 * @param reportRequest the report request
	 * @return the city order quantity
	 */
	@Override
	public List<KeyValueEntity> getCityOrderQuantity(ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date  selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date  selectedEndDate = selectedDateMap.get("selectedEndDate");
		
		String sql = "SELECT c.city AS 'key', COUNT(1) AS VALUE FROM ls_sub s, ls_cities c " +
				"where s.city_id=c.id AND s.shop_id = ?  and s.status = ? and s.sub_date >= ? and s.sub_date < ?  group by s.city_id";
		return genericJdbcDao.query(sql,KeyValueEntity.class,reportRequest.getShopId(),reportRequest.getStatus(),selectedStartDate,selectedEndDate);
	}

	/**
	 * 根据时间查找各个城市的订单量.
	 *
	 * @param reportRequest the report request
	 * @return the city order quantity
	 */
	@Override
	public List<KeyValueEntity>  getCityOrderQuantitySpecial(ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date  selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date  selectedEndDate = selectedDateMap.get("selectedEndDate");

		String sql = "SELECT c.area AS 'key', COUNT(1) AS VALUE FROM ls_sub s, ls_areas c " +
				"where s.area_id=c.areaid AND s.shop_id = ?  and s.status = ? and s.sub_date >= ? and s.sub_date < ?  group by s.city_id";
		return genericJdbcDao.query(sql,KeyValueEntity.class,reportRequest.getShopId(),reportRequest.getStatus(),selectedStartDate,selectedEndDate);
	}


	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.ReportDao#getPurchaseAnalysis(java.util.List, com.legendshop.model.report.ReportRequest)
	 */
	@Override
	public List<ReportDto> getPurchaseAnalysis(List<PriceRange> priceRangeList, ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date	selectedEndDate = selectedDateMap.get("selectedEndDate");
		
		String sql = "select count(1) as numbers from ls_sub where shop_id = ? and status = ? and actual_total BETWEEN ? and ? and sub_date >= ? and sub_date < ?";
		List<ReportDto> result = new ArrayList<ReportDto>();
		for (PriceRange priceRange : priceRangeList) {
			
			ReportDto dto = genericJdbcDao.get(sql,ReportDto.class,reportRequest.getShopId(),reportRequest.getStatus(),priceRange.getStartPrice(),priceRange.getEndPrice(),selectedStartDate,selectedEndDate);
			dto.setTotalCash(priceRange.getEndPrice());
			result.add(dto);
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.ReportDao#getPurchaseTime(com.legendshop.model.report.ReportRequest)
	 */
	@Override
	public List<ReportDto> getPurchaseTime(ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date selectedEndDate = selectedDateMap.get("selectedEndDate");
		
		String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getPurchaseTime");
		
		List<ReportDto> result = genericJdbcDao.query(sql, ReportDto.class, reportRequest.getShopId(),reportRequest.getStatus(),selectedStartDate,selectedEndDate);
		return result;
	}
	
	/**
	 * 流量统计.
	 *
	 * @param reportRequest the report request
	 * @return the traffic statistics
	 */
	@Override
	public List<ReportDto> getTrafficStatistics(ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date selectedEndDate = selectedDateMap.get("selectedEndDate");
		
		String sql = null;
		
		if(Constants.CALCULATION_DAYS.equals(reportRequest.getQueryTerms())){
			 sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getTrafficStatistics-Day");
		}else if(Constants.CALCULATION_MONTH.equals(reportRequest.getQueryTerms())){
			sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getTrafficStatistics-Month");
		}else if(Constants.CALCULATION_WEEK.equals(reportRequest.getQueryTerms())){
			sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getTrafficStatistics-Week");
		}

		return genericJdbcDao.query(sql, ReportDto.class,reportRequest.getShopId(),selectedStartDate,selectedEndDate);
	}
	
	/**
	 * 运营数据.
	 *
	 * @param reportRequest the report request
	 * @return the operating report
	 */
	@Override
	public List<ReportDto> getOperatingReport(ReportRequest reportRequest) {
		Map<String,Date> selectedDateMap = getSelectedDateMap(reportRequest);
		Date selectedStartDate = selectedDateMap.get("selectedStartDate");
		Date selectedEndDate = selectedDateMap.get("selectedEndDate");
		
		String sql = null;
				
		if(Constants.CALCULATION_DAYS.equals(reportRequest.getQueryTerms())){
			 sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getOperatingReport-Day");
		}else if(Constants.CALCULATION_MONTH.equals(reportRequest.getQueryTerms())){
			sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getOperatingReport-Month");
		}else if(Constants.CALCULATION_WEEK.equals(reportRequest.getQueryTerms())){
			sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".getOperatingReport-Week");
		}
		
		
		List<ReportDto> result = genericJdbcDao.query(sql, ReportDto.class,selectedStartDate,selectedEndDate,reportRequest.getShopId(),reportRequest.getStatus());
		
		return result;
	}
	
	/**
	 * 根据 选项 获取 开始时间和结束时间 *.
	 *
	 * @param reportRequest the report request
	 * @return the selected date map
	 */
	private Map<String,Date> getSelectedDateMap(ReportRequest reportRequest){
		
		Map<String,Date> selectedDateMap = new HashMap<String,Date>();
		if(Constants.CALCULATION_DAYS.equals(reportRequest.getQueryTerms())){
			if(AppUtils.isNotBlank(reportRequest.getSelectedDate())){
				selectedDateMap.put("selectedStartDate", reportRequest.getSelectedDate());
				Date endDate = DateUtils.getDay(reportRequest.getSelectedDate(), 1);
				selectedDateMap.put("selectedEndDate", endDate);
			}
		}else if(Constants.CALCULATION_MONTH.equals(reportRequest.getQueryTerms())){
			if(AppUtils.isNotBlank(reportRequest.getSelectedYear()) && AppUtils.isNotBlank(reportRequest.getSelectedMonth())){
				Date startDate = DateUtil.getDate(String.valueOf(reportRequest.getSelectedYear()), String.valueOf(reportRequest.getSelectedMonth()), "01");//current month
				Date endDate = DateUtil.getMonth(startDate, 1);//next month
				selectedDateMap.put("selectedStartDate", startDate);
				selectedDateMap.put("selectedEndDate", endDate);//下个月的1号
			}
		}else if(Constants.CALCULATION_WEEK.equals(reportRequest.getQueryTerms())){
			if(AppUtils.isNotBlank(reportRequest.getSelectedWeek())){
				String [] week = reportRequest.getSelectedWeek().split("~");
				Date startDate = null;
				Date endDate = null;
				try {
					startDate = parseDateFormat(week[0]);
					endDate = parseDateFormat(week[1]);
				} catch (Exception e) {
					log.debug("Parse week day error", e);
				}
				selectedDateMap.put("selectedStartDate", startDate);
				selectedDateMap.put("selectedEndDate", DateUtils.getDay(endDate, 1)); //结束时间+1
			}
		}
		
		return selectedDateMap;
	}
	
	//日期格式自动补0
	/**
	 * Parses the date format.
	 *
	 * @param date the date
	 * @return the date
	 */
	private Date parseDateFormat(String date){
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-M-d");
		Date result = null;
		try {
			result = dateFormat.parse(date);
		} catch (ParseException e) {
			log.debug("", e);
		}
		return result;
	}

	@Override
	public PageSupport<SalerankDto> querySaleRank(SalerankDto salerankDto, String curPageNO, Integer pageSize,
			DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(SalerankDto.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap paramMap = new QueryMap();
		if(AppUtils.isNotBlank(salerankDto.getShopId())){
			paramMap.put("shopId",salerankDto.getShopId());
		}
		if(AppUtils.isNotBlank(salerankDto.getShopName())){
			paramMap.like("shopName",salerankDto.getShopName().trim());
		}
		if(AppUtils.isNotBlank(salerankDto.getStartDate()) ){
			paramMap.put("startTime",salerankDto.getStartDate());
		}
		if(AppUtils.isNotBlank(salerankDto.getEndDate()) ){
			Date endDate = DateUtils.getDay(salerankDto.getEndDate(), 1);
			paramMap.put("endTime", endDate);
		}
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			query.setPageSize(pageSize);
		}
		// 判断是否为外部排序
		if (result.isSortExternal()) {
			paramMap.put(AttributeKeys.ORDER_INDICATOR, result.parseSeq());
		}
		
		 String queryAllSQL = ConfigCode.getInstance().getCode("report.getSaleRankCounts", paramMap);
		 String querySQL = ConfigCode.getInstance().getCode("report.getSaleRanks", paramMap); 
		 paramMap.remove(AttributeKeys.ORDER_INDICATOR);
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(paramMap.toArray());
		return genericJdbcDao.querySimplePage(query);
	}


	/**
	 * 购买量查询
	 */
	@Override
	public PageSupport<PurchaseRankDto> queryPurchaseRankDto(String curPageNO, PurchaseRankDto purchaserankDto,
			Integer pageSize, DataSortResult result) {
		String querySQL;
		String queryAllSQL;
		SimpleSqlQuery query = new SimpleSqlQuery(PurchaseRankDto.class, systemParameterUtil.getPageSize(),curPageNO);
		QueryMap paramMap = new QueryMap();
		if(AppUtils.isNotBlank(purchaserankDto.getShopName())){
			paramMap.like("shopName",purchaserankDto.getShopName().trim());
		}
		if(AppUtils.isNotBlank(purchaserankDto.getUserName())) {			
			paramMap.like("userName",purchaserankDto.getUserName().trim());
		}

		if (AppUtils.isNotBlank(purchaserankDto.getUserMobile())) {
			paramMap.put("userMobile","%"+ purchaserankDto.getUserMobile() +"%");
		}

		paramMap.put("startTime",purchaserankDto.getStartDate());
		if(AppUtils.isNotBlank(purchaserankDto.getEndDate()) ){
			Date endDate = DateUtils.getDay(purchaserankDto.getEndDate(), 1);
			paramMap.put("endTime", endDate);
		}
		
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			query.setPageSize(pageSize);
		}
		// 判断是否为外部排序
		if (result.isSortExternal()) {
			paramMap.put(AttributeKeys.ORDER_INDICATOR, result.parseSeq());
 		}
		
		if(purchaserankDto.getStatus()==1){
			 querySQL = ConfigCode.getInstance().getCode("report.getPurchaseRankByShop", paramMap); 
			 queryAllSQL = ConfigCode.getInstance().getCode("report.getPurchaseRankByShopCount", paramMap);
		}else{	
			 querySQL = ConfigCode.getInstance().getCode("report.getPurchaseRankByUser", paramMap);
			 queryAllSQL = ConfigCode.getInstance().getCode("report.getPurchaseRankByUserCount", paramMap);
		}	 
		 paramMap.remove(AttributeKeys.ORDER_INDICATOR);
		 query.setQueryString(querySQL);
		 query.setAllCountString(queryAllSQL);
		 query.setParam(paramMap.toArray());
		return genericJdbcDao.querySimplePage(query);
	}
	
	
}
