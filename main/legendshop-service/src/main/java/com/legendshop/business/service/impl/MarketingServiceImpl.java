/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MarketingDao;
import com.legendshop.business.dao.MarketingProdsDao;
import com.legendshop.business.dao.MarketingRuleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingMjFulllRule;
import com.legendshop.model.dto.marketing.MarketingMjRule;
import com.legendshop.model.dto.marketing.MarketingMyRule;
import com.legendshop.model.dto.marketing.MarketingMzRule;
import com.legendshop.model.dto.marketing.PurchaseProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.model.entity.MarketingRule;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;

/**
 * 营销活动表服务类
 */
@Service("marketingService")
public class MarketingServiceImpl implements MarketingService {

	@Autowired
	private MarketingDao marketingDao;
	
	@Autowired
	private MarketingRuleDao marketingRuleDao;
	
	@Autowired
	private MarketingProdsDao marketingProdsDao;
	
	@Autowired
	private MarketingService marketingService;
	
	@Autowired
	private MarketingProdsService marketingProdsService;

	public Marketing getMarketing(Long id) {
		return marketingDao.getMarketing(id);
	}

	public void deleteMarketing(Marketing marketing) {
		marketingDao.deleteMarketing(marketing);

		/*
		 * 删除规则
		 */
		marketingRuleDao.update("delete from ls_marketing_rule where market_id=? and shop_id=? ", marketing.getId(), marketing.getShopId());

		/*
		 * 删除商品
		 */
		marketingDao.update("delete from ls_marketing_prods where market_id=? and shop_id=? ", marketing.getId(), marketing.getShopId());
	}

	public String saveMarketing(Marketing marketing) {

		if (MarketingTypeEnum.MAN_JIAN.value().equals(marketing.getType())) {
			if (AppUtils.isBlank(marketing.getManjian_rule())) {
				return "请添加满减规则";
			}
		} else if (MarketingTypeEnum.MAN_ZE.value().equals(marketing.getType())) {
			if (AppUtils.isBlank(marketing.getManze_rule())) {
				return "请添加满折规则";
			}
		}
		marketing.setCreateTime(new Date());
		marketing.setState(0);
		marketing.setIsExpired(0);
		Long id = marketingDao.saveMarketing(marketing);
		saveMarketingRule(marketing);
		return Constants.SUCCESS;
	}

	public void updateMarketing(Marketing marketing) {
		marketingDao.updateMarketing(marketing);
	}

	@Override
	public void saveMarketingRule(Marketing marketing) {
		List<MarketingRule> rules = new ArrayList<>();
		if (MarketingTypeEnum.MAN_JIAN.value().equals(marketing.getType())) {
			for (int i = 0; i < marketing.getManjian_rule().length; i++) {
				String market = marketing.getManjian_rule()[i];
				String[] values = market.split(",");
				MarketingRule marketingRule = new MarketingRule();
				marketingRule.setMarketId(marketing.getId());
				marketingRule.setShopId(marketing.getShopId());
				marketingRule.setFullPrice(Double.parseDouble(values[0]));
				marketingRule.setOffAmount(Double.parseDouble(values[1]));
				marketingRule.setCalType(Integer.valueOf(values[2]));
				marketingRule.setCreateTime(new Date());
				marketingRule.setType(MarketingTypeEnum.MAN_JIAN.value());
				rules.add(marketingRule);
			}
		} else if (MarketingTypeEnum.MAN_ZE.value().equals(marketing.getType())) {
			for (int i = 0; i < marketing.getManze_rule().length; i++) {
				String market = marketing.getManze_rule()[i];
				String[] values = market.split(",");
				MarketingRule marketingRule = new MarketingRule();
				marketingRule.setMarketId(marketing.getId());
				marketingRule.setShopId(marketing.getShopId());
				marketingRule.setFullPrice(Double.parseDouble(values[0]));
				marketingRule.setOffDiscount(Float.parseFloat(values[1]));
				marketingRule.setCalType(Integer.valueOf(values[2]));
				marketingRule.setCreateTime(new Date());
				marketingRule.setType(MarketingTypeEnum.MAN_ZE.value());
				rules.add(marketingRule);
			}
		} else if (MarketingTypeEnum.XIANSHI_ZE.value().equals(marketing.getType())) {
			MarketingRule marketingRule = new MarketingRule();
			marketingRule.setShopId(marketing.getShopId());
			marketingRule.setMarketId(marketing.getId());
			marketingRule.setOffDiscount(marketing.getDiscount());
			marketingRule.setType(MarketingTypeEnum.XIANSHI_ZE.value());
			marketingRule.setCreateTime(new Date());
			marketingRule.setFullPrice(0.00);
			rules.add(marketingRule);
		}
		marketingRuleDao.save(rules);
	}

    @Override
	public Marketing getMarketing(Long shopId, Long id) {
		Marketing marketing = marketingDao.getMarketing(shopId, id);
		return marketing;
	}

	@Override
	public Marketing getMarketingDetail(Long shopId, Long id) {

		Marketing marketing = marketingDao.getMarketing(shopId, id);
		if (AppUtils.isNotBlank(marketing)) {
			if (MarketingTypeEnum.MAN_JIAN.value().equals(marketing.getType())) {
				List<MarketingMjRule> marketingMjRules = marketingDao.query(
						"select off_amount as offAmount,full_price as fullAmount,cal_type as calType from ls_marketing_rule where market_id=? and shop_id=? and type=? order by full_price asc,off_amount asc ",
						MarketingMjRule.class, marketing.getId(), shopId, MarketingTypeEnum.MAN_JIAN.value());
				marketing.setMarketingMjRules(marketingMjRules);
				marketing.setMarketingProds(marketingProdsService.getMarketingProdsDtoByMarketId(marketing.getId()));
			} else if (MarketingTypeEnum.MAN_ZE.value().equals(marketing.getType())) {
				List<MarketingMzRule> marketingMzRules = marketingDao.query(
						"select full_price as fullAmount,off_discount as offDiscount,cal_type as calType  from ls_marketing_rule where market_id=? and shop_id=? and type=? order by full_price asc,off_amount asc ",
						MarketingMzRule.class, marketing.getId(), shopId, MarketingTypeEnum.MAN_ZE.value());
				marketing.setMarketingMzRules(marketingMzRules);
				marketing.setMarketingProds(marketingProdsService.getMarketingProdsDtoByMarketId(marketing.getId()));
			} else if (MarketingTypeEnum.XIANSHI_ZE.value().equals(marketing.getType())) {
				List<MarketingMzRule> marketingMzRules = marketingDao.query(
						"select full_price as fullAmount,off_discount as offDiscount  from ls_marketing_rule where market_id=? and shop_id=? and type=? order by full_price asc,off_amount asc ",
						MarketingMzRule.class, marketing.getId(), shopId, MarketingTypeEnum.XIANSHI_ZE.value());
				marketing.setMarketingMzRules(marketingMzRules);
				marketing.setMarketingProds(marketingProdsService.getMarketingProdsDtoByMarketId(marketing.getId()));
			} else if (MarketingTypeEnum.FULL_NUM_MAIL.value().equals(marketing.getType())) {
				String sql = "select full_price as fullAmount from ls_marketing_rule where market_id=? and shop_id=? and type=? order by full_price asc,off_amount asc";
				List<MarketingMjFulllRule> marketingMjFulllRule = marketingDao.query(sql, MarketingMjFulllRule.class, marketing.getId(), shopId,
						MarketingTypeEnum.FULL_NUM_MAIL.value());
				marketing.setMarketingMjFulllRule(marketingMjFulllRule);
			} else if (MarketingTypeEnum.FULL_AMOUNT_MAIL.value().equals(marketing.getType())) {
				String sql = "select full_price as fullAmount from ls_marketing_rule where market_id=? and shop_id=? and type=? order by full_price asc,off_amount asc";
				List<MarketingMyRule> marketingMyRule = marketingDao.query(sql, MarketingMyRule.class, marketing.getId(), shopId,
						MarketingTypeEnum.FULL_AMOUNT_MAIL.value());
				marketing.setMarketingMyRule(marketingMyRule);
			}
		}
		return marketing;
	}

	@Override
	public Long isExistProds(Long id, Long shopId) {
		return marketingProdsDao.getLongResult("select count(id) from ls_marketing_prods where market_id=? and shop_id=? ", id, shopId);
	}

	/**
	 * 找到该店铺所有的营销活动信息
	 */
	@Override
	public List<MarketingDto> findAvailableBusinessRuleList(Long shopId, List<ShopCartItem> cartItems) {

		Date time = new Date();
		List<MarketingDto> list = new ArrayList<MarketingDto>();

		// 按店铺订单的商品 来查找 营销活动列表
		List<MarketingDto> marketingList = marketingProdsDao.findProdMarketing(cartItems, shopId, time);
		if (AppUtils.isNotBlank(marketingList)) {
			for (MarketingDto marketingDto : marketingList) {
				// 判断是否过期
				// 开始时间在当前时间之后,当前时间在结束时间之前
				if (time.after(marketingDto.getStartTime()) && time.before(marketingDto.getEndTime())) {
					list.add(marketingDto);
				}
			}
		}

		/*
		 * 查询全局的营销活动计划
		 */
		List<MarketingDto> globalMarketList = marketingProdsDao.findGlobalMarketing(shopId, time);
		if (AppUtils.isNotBlank(globalMarketList)) {
			for (MarketingDto marketingDto : globalMarketList) {
				// 判断是否过期
				if (time.after(marketingDto.getStartTime()) && time.before(marketingDto.getEndTime())) {
					list.add(marketingDto);
				}
			}
		}
		marketingList = null;
		globalMarketList = null;

		// 根据 Type排序 0: 满减促销 ; 1:满折促销; 2：限时促销
		Collections.sort(list, new Comparator<MarketingDto>() {
			@Override
			public int compare(MarketingDto o1, MarketingDto o2) {
				if (o1 == null || o2 == null) {
					return -1;
				}
				if (Objects.equals(o1.getType(), o2.getType())) {
					return 0;
				}
				if (o1.getType() > o2.getType()) {
					return -1;
				}
				return 1;
			}
		});

		return list;
	}

	/**
	 * 找到该商品所有的营销活动信息
	 * 
	 * @param shopId 商城Id
	 * @param prodId 商品Id
	 * @return
	 */
	@Override
	public List<MarketingDto> findAvailableBusinessRuleList(Long shopId, Long prodId) {
		// 第一步，查询出所有已发布的，并且当前时间进行中产品营销活动[部分商品活动及全场活动] //默认第一个为优先命中活动
		Date time = new Date();
		List<MarketingDto> list = new ArrayList<MarketingDto>();
		List<MarketingDto> prodMarketList = marketingProdsDao.findProdMarketing(prodId, shopId, time);
		/*
		 * 查询全局的营销活动计划
		 */
		List<MarketingDto> globalMarketList = marketingProdsDao.findGlobalMarketing(shopId, time);
		if (AppUtils.isNotBlank(prodMarketList)) {
			for (MarketingDto marketingDto : prodMarketList) {
				// 判断是否过期
				// 开始时间在当前时间之后,当前时间在结束时间之前
				if (time.after(marketingDto.getStartTime()) && time.before(marketingDto.getEndTime())) {
					list.add(marketingDto);
				}
			}
		}
		if (AppUtils.isNotBlank(globalMarketList)) {
			for (MarketingDto marketingDto : globalMarketList) {
				// 判断是否过期
				if (time.after(marketingDto.getStartTime()) && time.before(marketingDto.getEndTime())) {
					list.add(marketingDto);
				}
			}
		}
		prodMarketList = null;
		globalMarketList = null;

		// TODO 根据 Type
		Collections.sort(list, new Comparator<MarketingDto>() {
			@Override
			public int compare(MarketingDto o1, MarketingDto o2) {
				if (o1 == null || o2 == null) {
					return -1;
				}
				if (o1.getType() == o2.getType()) {
					return 0;
				}
				if (o1.getType() > o2.getType()) {
					return -1;
				}
				return 1;
			}
		});

		return list;
	}

	@Override
	public String saveMarketingByFullmail(Marketing marketing) {
		marketing.setCreateTime(new Date());
		marketing.setState(0);
		Long id = marketingDao.saveMarketing(marketing);
		List<MarketingRule> rules = new ArrayList<>();
		if (MarketingTypeEnum.FULL_NUM_MAIL.value().equals(marketing.getType())) {
			for (int i = 0; i < marketing.getManjian_fullmail_rule().length; i++) {
				String value = marketing.getManjian_fullmail_rule()[i];
				MarketingRule marketingRule = new MarketingRule();
				marketingRule.setMarketId(id);
				marketingRule.setShopId(marketing.getShopId());
				marketingRule.setFullPrice(Double.parseDouble(value));
				marketingRule.setCreateTime(new Date());
				marketingRule.setType(MarketingTypeEnum.FULL_NUM_MAIL.value());
				rules.add(marketingRule);
			}

		} else if (MarketingTypeEnum.FULL_AMOUNT_MAIL.value().equals(marketing.getType())) {
			for (int i = 0; i < marketing.getManyuan_fullmail_rule().length; i++) {
				String value = marketing.getManyuan_fullmail_rule()[i];
				MarketingRule marketingRule = new MarketingRule();
				marketingRule.setMarketId(id);
				marketingRule.setShopId(marketing.getShopId());
				marketingRule.setFullPrice(Double.parseDouble(value));
				marketingRule.setCreateTime(new Date());
				marketingRule.setType(MarketingTypeEnum.FULL_AMOUNT_MAIL.value());
				rules.add(marketingRule);
			}
		}
		marketingRuleDao.save(rules);

		return Constants.SUCCESS;

	}

	@Override
	public PageSupport<Marketing> getMarketingPage(String curPageNO, Long shopId, Marketing marketing) {
		return marketingDao.getMarketingPage(curPageNO, shopId, marketing);
	}

	@Override
	public PageSupport<Marketing> getMarketingPage(String curPageNO, Marketing marketing) {
		return marketingDao.getMarketingPage(curPageNO, marketing);
	}

	@Override
	public PageSupport<Marketing> getAssemblefullmail(String curPageNO, Marketing marketing) {
		return marketingDao.getAssemblefullmail(curPageNO, marketing);
	}

	@Override
	public PageSupport<Marketing> getShopMarketingMansong(String curPageNO, Marketing marketing, DataSortResult result, Integer pageSize) {
		return marketingDao.getShopMarketingMansong(curPageNO, marketing, result, pageSize);
	}

	@Override
	public PageSupport<Marketing> getShopMatketingZhekou(String curPageNO, Marketing marketing, Integer pageSize, DataSortResult result) {
		return marketingDao.getShopMatketingZhekou(curPageNO, marketing, pageSize, result);
	}

	@Override
	public List<PurchaseProdDto> getMarketingList(Date startTime, Date endTime) {
		return marketingDao.getMarketingList(startTime, endTime);
	}

	@Override
	public List<Marketing> findOngoingMarketing(Long shopId, Integer isAllProds, Date startTime, Date endTime) {
		return marketingDao.findOngoingMarketing(shopId,isAllProds,startTime,endTime);
	}

	@Override
	public int batchDetele(String marketIds) {
		String[] marketIdList=marketIds.split(",");
		int i=0;
		for(String marketId:marketIdList){
			Marketing marketing = marketingService.getMarketing(Long.valueOf(marketId));
			if (AppUtils.isBlank(marketing)) {
				throw new RuntimeException("要删除的营销活动不存在。");
			}
			if (marketing.getState().intValue() == 1) {
				throw new RuntimeException("营销活动正在进行，不能删除。");
			}
			marketingService.deleteMarketing(marketing);
			if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
				marketingProdsService.clearGlobalMarketCache(marketing.getShopId());
			} else {
				marketingProdsService.clearProdMarketCache();
			}
			i++;
		}
		return i;
	}
	
	@Override
	public int getAllProdsMarketing(Long shopId, Integer type, Date startTime, Date endTime) {
		return marketingDao.getAllProdsMarketing(shopId, type, startTime, endTime);
	}
	
	@Override
	public List<Marketing> getExpiredMarketing(Integer commitInteval, Date endTime) {
		
		return marketingDao.getExpiredMarketing(commitInteval,endTime);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(value = "MarketingDtoList", allEntries=true),
			@CacheEvict(value = "GlobalMarketingDtoList", allEntries=true)})
	public void cleanMarketCache() {

	}
}
