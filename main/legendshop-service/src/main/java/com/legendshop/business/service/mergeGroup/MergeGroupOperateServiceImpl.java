/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.mergeGroup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MergeGroupOperateDao;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.util.AppUtils;

/**
 * The Class MergeGroupOperateServiceImpl.
 *  每个团的运营信息服务实现类
 */
@Service("mergeGroupOperateService")
public class MergeGroupOperateServiceImpl  implements MergeGroupOperateService{

	@Autowired
	private MergeGroupOperateDao mergeGroupOperateDao;

	/**	
	 *  根据Id获取每个团的运营信息
	 */
	public MergeGroupOperate getMergeGroupOperate(Long id) {
		return mergeGroupOperateDao.getMergeGroupOperate(id);
	}

	/**
	 *  删除每个团的运营信息
	 */ 
	public void deleteMergeGroupOperate(MergeGroupOperate mergeGroupOperate) {
		mergeGroupOperateDao.deleteMergeGroupOperate(mergeGroupOperate);
	}

	/**
	 *  保存每个团的运营信息
	 */	    
	public Long saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate) {
		if (!AppUtils.isBlank(mergeGroupOperate.getId())) {
			updateMergeGroupOperate(mergeGroupOperate);
			return mergeGroupOperate.getId();
		}
		return mergeGroupOperateDao.saveMergeGroupOperate(mergeGroupOperate);
	}

	/**
	 *  更新每个团的运营信息
	 */	
	public void updateMergeGroupOperate(MergeGroupOperate mergeGroupOperate) {
		mergeGroupOperateDao.updateMergeGroupOperate(mergeGroupOperate);
	}


	//获取团运营信息，用于校检人数是否满团
	public MergeGroupJoinDto getMergeGroupOperateByAddNumber(Long activeId, String addNumber) {
		return mergeGroupOperateDao.getMergeGroupOperateByAddNumber(activeId,addNumber);
	}

	//校检用户是否参加了该团
	public Long getMergeGroupAddByUserId(String userId, String addNumber) {
		return mergeGroupOperateDao.getMergeGroupAddByUserId(userId,addNumber);
	}

	public MergeGroupOperate getMergeGroupOperateByAddNumber(String addNumber) {
		return mergeGroupOperateDao.getMergeGroupOperateByAddNumber(addNumber);
	}

	public List<MergeGroupOperate> findHaveInMergeGroupOperate() {
		return mergeGroupOperateDao.findHaveInMergeGroupOperate();
	}

	public List<MergeGroupOperate> findHaveInMergeGroupOperate(Long mergeId) {
		return mergeGroupOperateDao.findHaveInMergeGroupOperate(mergeId);
	}

}
