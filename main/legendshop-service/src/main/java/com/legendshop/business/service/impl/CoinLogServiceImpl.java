package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CoinLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.coin.CoinConsumeDto;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.vo.CoinVo;
import com.legendshop.spi.service.CoinLogService;

/**
 * 金币服务
 *
 */
@Service("coinLogService")
public class CoinLogServiceImpl implements CoinLogService{

	@Autowired
	private CoinLogDao coinLogDao;

	@Override
	public CoinVo findUserCoin(String userId, String curPageNO) {
		return coinLogDao.findUserCoin(userId, curPageNO);
	}

	@Override
	public PageSupport<CoinConsumeDto> getRechargeDetailsList(String curPageNO, String userName, String mobile) {
		return coinLogDao.getRechargeDetailsList(curPageNO,userName,mobile);
	}

	@Override
	public PageSupport<CoinConsumeDto> getConsumeDetailsListPage(String curPageNO, String mobile, String userName) {
		return coinLogDao.getConsumeDetailsListPage(curPageNO,userName,mobile);
	}

	@Override
	public PageSupport<CoinLog> getUserCoinPage(String curPageNO, String userId) {
		return coinLogDao.getUserCoinPage(curPageNO,userId);
	}
	
	
}


