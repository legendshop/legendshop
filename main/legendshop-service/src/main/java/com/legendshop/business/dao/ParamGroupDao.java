/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ParamGroup;

/**
 * 参数组Dao.
 */
public interface ParamGroupDao extends Dao<ParamGroup, Long> {
     
	public abstract ParamGroup getParamGroup(Long id);
	
    public abstract int deleteParamGroup(ParamGroup paramGroup);
	
	public abstract Long saveParamGroup(ParamGroup paramGroup);
	
	public abstract int updateParamGroup(ParamGroup paramGroup);
	
	public abstract List<ParamGroup> getParamGroupList(Long groupId);

	public abstract PageSupport<ParamGroup> getParamGroupPage(String curPageNO, ParamGroup paramGroup);

	public abstract PageSupport<ParamGroup> getParamGroupPage(String groupName);
	
 }
