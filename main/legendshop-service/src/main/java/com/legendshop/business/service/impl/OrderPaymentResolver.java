package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.OrderCommis;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.MergeGroupAddDao;
import com.legendshop.business.dao.MergeGroupDao;
import com.legendshop.business.dao.MergeGroupOperateDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.StoreDao;
import com.legendshop.business.dao.StoreOrderDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.constant.MergeGroupOperateStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.constant.StockCountingEnum;
import com.legendshop.model.constant.SubGroupStatusEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.constant.SubMergeGroupStatusEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupAdd;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.spi.service.IPaymentResolver;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 
 * 普通订单和拼团订单都走这个流程
 *
 */
@Service("orderPaymentResolver")
public class OrderPaymentResolver implements IPaymentResolver {

	@Autowired
	private SubDao subDao;
	
	@Autowired
	private SubItemDao subItemDao;
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private SubSettlementDao subSettlementDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private ExpensesRecordDao expensesRecordDao;
	
	@Autowired
	private SubSettlementItemDao subSettlementItemDao;
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private StoreOrderDao storeOrderDao;
	
	@Autowired
	private StoreDao storeDao;
	
	@Autowired
    private MergeGroupAddDao mergeGroupAddDao;
	
	@Autowired
	private MergeGroupOperateDao mergeGroupOperateDao;
	
	@Autowired
	private MergeGroupDao mergeGroupDao;
	
	@Autowired
	private SubHistoryService subHistoryService;
	
	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;
	
	/** 订单三级分佣 **/
	@Autowired(required=false)
	@Qualifier(value="orderCommisProcessor")
	private EventProcessor<OrderCommis> orderCommisProcessor;
	
	@Resource(name="predepositPaySuccessProcessor")
	private EventProcessor<PredepositPaySuccess> predepositPaySuccessProcessor;
	
	@Override
	public Map<String, Object> queryPayto(Map<String, String> params) {
		String subNumber = params.get("subNumber");
		String userId = params.get("userId");
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("result", false);
		
		String [] subNumbers=subNumber.split(",");
		
		StringBuilder subject= new StringBuilder();
		double totalAmount = 0D;
				
		for(String _subNumber:subNumbers){
			Sub sub = subDao.getSubBySubNumberByUserId(_subNumber, userId);
			
			if (AppUtils.isBlank(sub)) {
				response.put("message", "支付不通过,订单数据缺失");
				return response;
			}
			
			if (!OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
				response.put("message", "支付失败,订单数据已发生改变");
				return response;
			}

			
			List<SubItem> subItems = subItemDao.getSubItemBySubId(sub.getSubId());
			for(SubItem subItem : subItems){
				if(StockCountingEnum.STOCK_BY_PAY.value().equals(subItem.getStockCounting())){
					boolean flag = stockService.checkStocks(subItem.getProdId(), subItem.getSkuId(), subItem.getBasketCount());
					if(!flag){
						response.put("message", "对不起, 名称为: " + subItem.getProdName() + "的商品库存不足, 暂不能付款, 请联系卖家!");
						return response;
					}
				 }
			}
			subject.append(sub.getProdName());
			totalAmount=Arith.add(sub.getActualTotal(), totalAmount);
		}
		
		response.put("result", true);
		response.put("subject", subject.toString());
		response.put("totalAmount",totalAmount);
		return response;
	}
	
	

	@Override
	public void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallback){
		PaymentLog.info("############################## 普通商品回调 ###########################");
		if (subSettlement.getIsClearing()) { // 没有清算
			return;
		}
		
		if(OrderStatusEnum.UNPAY.value().equals(bankCallback.getValidateStatus().value())){ 
			  //没有付款
			  PaymentLog.info("###################### doBankCallback subSettlementSn={} 开始处理 订单支付业务 ########################",subSettlement.getSubSettlementSn());
			  Date now=new Date();
			  subSettlement.setFlowTradeNo(bankCallback.getFlowTradeNo());
			  subSettlement.setClearingTime(now);
			  subSettlement.setIsClearing(true);
			  int settlementResult=subSettlementDao.updateSubSettlementForPay(subSettlement);
			  if(settlementResult==0){
				 PaymentLog.info("Settlement record has settled。 TradeNo is  {}", bankCallback.getFlowTradeNo());
				 return ;  
			  }
			  
			  List<SubSettlementItem> subSettlementItems=subSettlementItemDao.getSubSettlementItems(subSettlement.getSubSettlementSn());
			  
			  if (subSettlementItems == null) {
				   PaymentLog.error("支付异常,订单缺失, doBankCallback error subs is null by subSettlementSn={}", subSettlement.getSubSettlementSn());
				   throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			  }
				
			  List<String> subNumbers=new ArrayList<String>();
			  for(SubSettlementItem settlementItem:subSettlementItems){
				  String[] strings = settlementItem.getSubNumber().split(",");
				  for(String str : strings){
					  subNumbers.add(str);
				  }
			  }
			  
			  // 第二步更新Sub订单动作
			  PaymentLog.info("############################## 处理订单信息{} ###########################",subNumbers);
			  List<Sub> subs = subDao.getSubBySubNumberByUserId(subNumbers, subSettlement.getUserId());
			  if(AppUtils.isBlank(subs)){
				  PaymentLog.error("支付异常,订单缺失, doBankCallback error subs is null by subNumbers={}", subNumbers);
				  throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			  }
			  String userName = subs.get(0).getUserName(); //获取订单的用户名, 下面发短信要用
			  /*
				* 判断是否使用全款预付款支付
			  */
			  if(subSettlement.isFullPay()){
				    PaymentLog.info("############################## 使用全部的钱包支付 全部金额={}  ###########################",subSettlement.getCashAmount());
				    
				    //记得同步执行
					PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(), subSettlement.getPdAmount(),
							subSettlement.getUserId(),subSettlement.getPrePayType());
					
					predepositPaySuccessProcessor.process(paySuccess);
					
					String result = paySuccess.getResult();
					if (!PreDepositErrorEnum.OK.value().equals(result)) {
						PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
						throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
					}
			  }else if(subSettlement.getPdAmount()>0){ //部分使用钱包
				    PaymentLog.info("############################## 使用部分的钱包支付  部分金额={} ###########################",subSettlement.getPdAmount());
				    
					PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(), subSettlement.getPdAmount(),
							subSettlement.getUserId(),subSettlement.getPrePayType());
							
					predepositPaySuccessProcessor.process(paySuccess);
							
					String result = paySuccess.getResult();
					if (!PreDepositErrorEnum.OK.value().equals(result)) {
						PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
						throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
					}
			  }
			
			 for (Sub sub : subs) {
				PaymentLog.info("###################### 开始 处理 sub by subNumber={} ########################",sub.getSubNumber());
				sub.setPayed(true);// 已经支付成功
				sub.setPayDate(now);
				if(sub.getStatus().equals(OrderStatusEnum.CLOSE.value())){//增加这个逻辑：订单已经关闭 TODO
					 sub.setStatus(OrderStatusEnum.PADYED.value());
				 }
				sub.setPayTypeId(subSettlement.getPayTypeId());
				sub.setPayTypeName(subSettlement.getPayTypeName());
				sub.setStatus(bankCallback.getUpdateStatus().value());
				sub.setFlowTradeNo(bankCallback.getFlowTradeNo());
				sub.setSubSettlementSn(subSettlement.getSubSettlementSn());
				int result = subDao.updateSubForPay(sub, bankCallback.getValidateStatus().value()); // 更新为支付状态
				if(result==0){
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				PaymentLog.info("###################### sub by subNumber={} 处理支付成功 ########################",sub.getSubNumber());
				List<SubItem> items = subItemDao.getSubItem(sub.getSubNumber(), sub.getUserId());
				if(AppUtils.isBlank(items)){
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				
				String firstDistUserName = null; 
				Map<Long, Long> map=new HashMap<Long, Long>();
				for (Iterator<SubItem> iterator = items.iterator(); iterator.hasNext();) {
					SubItem subItem = (SubItem) iterator.next();
					if(subItem==null)
						iterator.remove();
					Product product=productDao.getById(subItem.getProdId());
					if(StockCountingEnum.STOCK_BY_PAY.value().equals(product.getStockCounting())){
						  PaymentLog.info("###################### 付款减库存 by subItemId={},productId={} ########################",subItem.getSubItemId(),subItem.getProdId());
						  boolean config=stockService.addHold(product.getProdId(), subItem.getSkuId(), subItem.getBasketCount());
						  if(!config){
							    PaymentLog.error("支付出现超卖,请人工处理 , 订单号是: {},支付方式是:{},外部流水:{}",sub.getSubNumber(),subSettlement.getPayTypeId(),bankCallback.getFlowTradeNo());
//								throw new BusinessException(subItem.getProdName()+"库存不足,造成订单失败,如若已经支付请联系客服进行退款操作！", ErrorCodes.BUSINESS_ERROR);
							  	//商品付款扣库存，第三方支付强行扣库存，超卖
							  Integer stocks = stockService.getStocksByLockMode(product.getProdId(), subItem.getSkuId());
							  stockService.updSkuStockDto(subItem.getSkuId(),(stocks - subItem.getBasketCount()));
						  }
					 }
					Long buys = product.getBuys();
					 if(map.containsKey(product.getProdId())){
						 Long target=  map.get(product.getProdId());
						 map.put(product.getProdId(), target+subItem.getBasketCount());
					  }else{
						  map.put(product.getProdId(), buys+subItem.getBasketCount());
					  }
					 if(firstDistUserName==null && AppUtils.isNotBlank(subItem.getDistUserName())){
						 firstDistUserName = subItem.getDistUserName();
					 }
				}
				
				//绑定分销上级    
			    if(AppUtils.isNotBlank(firstDistUserName)){
			    	PaymentLog.info("###################### 绑定分销上级该订单存在分销情况   by subNumber={},firstDistUserName={} ########################",sub.getSubNumber(),firstDistUserName);
					String userId = sub.getUserId();
					UserDetail userDetail = userDetailDao.getUserDetailById(userId);
					//如果之前没有被绑定过，则绑定现在的分销上级
					if(userDetail!=null && AppUtils.isBlank(userDetail.getParentUserName())){
						userDetailDao.update("update ls_usr_detail ud set ud.parent_user_name=?,ud.parent_binding_time=? where ud.user_id=?", firstDistUserName,new Date(),userId);
					}
			     }
			    
			    //如果是普通订单，则分佣
			    if(CartTypeEnum.NORMAL.toCode().equals(sub.getSubType())){
			    	List<String> subNum = new ArrayList<String>();
			    	subNum.add(sub.getSubNumber());
			    	orderCommisProcessor.process(new OrderCommis(subNum, sub.getUserId()));
			    }
			    
			     //if paimai order;
				 if(SubTypeEnum.auctions.value().equals(sub.getSubType())){
					 PaymentLog.info("###################### 该订单是拍卖订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
					 subSettlementDao.update("update ls_bidders_win set status=? where sub_number=? ", new Object[]{BiddersWinStatusEnum.PAYMENT.value(),sub.getSubNumber()});
				 }
				 
				//如果是拼团订单
			     if(SubTypeEnum.MERGE_GROUP.value().equals(sub.getSubType())){
			    	 PaymentLog.info("###################### 该订单是拼团订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
			    	 handleMergeGroup(sub);//组装拼团运营信息
			     }
				 
				//TODO 抽出独立插件?  团购订单
			     if(SubTypeEnum.group.value().equals(sub.getSubType())){
			    	 PaymentLog.info("###################### 该订单是团购订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
			    	 updateGroupSubStatus(sub);//组装拼团运营信息
			     }
				 
				 //如果是门店订单
			     if(SubTypeEnum.SHOP_STORE.value().equals(sub.getSubType())){
			    	 PaymentLog.info("###################### 该订单是门店订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
			    	 subSettlementDao.update("update ls_store_order set dlyo_satus=? where order_sn=? and user_id=? and pay_type=2 ", new Object[]{1,sub.getSubNumber(),sub.getUserId()});
			    	 //发送自提码短信
			    	 StoreOrder storeOrder = storeOrderDao.getDeliveryOrderBySubNumber(sub.getSubNumber());
			    	 String address = getAddress(storeOrder.getStoreId());
			    	 String orderSn = storeOrder.getOrderSn();
			    	 if(storeOrder.getOrderSn().length()>20){
			    		 orderSn = storeOrder.getOrderSn().substring(0, 19) + "…";
			    	 }
			    	 sendSMSProcessor.process(new SendSMSEvent(storeOrder.getReciverMobile(), storeOrder.getDlyoPickupCode(), userName, address, orderSn).getSource());
			     }
				
				 saveOrderHistory(sub);
				 
				 /**
				  * 可以异步处理 未免影响主流程
				  */
				 for (Entry<Long, Long> entry1: map.entrySet()) {
					 productDao.updateBuys(entry1.getValue(),entry1.getKey());
			     }
				 
				 //异步处理
				 ExpensesRecord expensesRecord=new ExpensesRecord();
				 expensesRecord.setRecordDate(now);
				 expensesRecord.setRecordMoney(-sub.getActualTotal());
				 expensesRecord.setRecordRemark("下单,账户支付金额为:"+sub.getActualTotal()+",订单流水号:"+sub.getSubNumber());
				 expensesRecord.setUserId(sub.getUserId());
				 expensesRecordDao.saveExpensesRecord(expensesRecord);
				 PaymentLog.info("###################### sub by subNumber={} 处理成功 ########################",sub.getSubNumber());
			}
			PaymentLog.info("###################### doBankCallback subSettlementSn={} 处理成功 ########################",subSettlement.getSubSettlementSn());
		}
	}
	
	/**
	 * 处理拼团订单
	 * @param sub 要处理的订单
	 */
	private void handleMergeGroup(Sub sub) {
		
		MergeGroup mergeGroup = mergeGroupDao.getMergeGroup(sub.getActiveId());
		
		if(AppUtils.isNotBlank(sub.getAddNumber())){//订单里有团编号, 说明是参团
			
			//获取本团运营信息
			MergeGroupOperate operate = mergeGroupOperateDao.findMergeGroupOperate(sub.getShopId(),sub.getAddNumber());
			
			if(null != operate //通过addNumber能找到相应的团
					&& MergeGroupOperateStatusEnum.ING.value().equals(operate.getStatus()) //团进行中状态
					&& operate.getEndTime().getTime() > System.currentTimeMillis() //团未过有效期
					&& mergeGroup.getPeopleNumber() > operate.getNumber()){//团人数未满, 可以正常参团
				
				//处理参团逻辑
				joinMergeGroup(mergeGroup, operate, sub);
				
			}else{//否则, 这应该属于异常情况, 不过不报错, 给他新开个团吧
				// TODO 要打印警告日志
				
				//处理开团逻辑
				openMergeGroup(mergeGroup, sub);
			}
			
		}else{//说明是开团
			//处理开团逻辑
			openMergeGroup(mergeGroup, sub);
		}
	}
	
	/**
	 * 处理参团逻辑
	 * @param mergeGroup 拼团活动
	 * @param operate 团信息
	 * @param sub 订单
	 */
	private void joinMergeGroup(MergeGroup mergeGroup, MergeGroupOperate operate, Sub sub){
		
		//获取用户信息
		 UserDetail userDetail = userDetailDao.getUserDetailById(sub.getUserId());
		 
		 //保存拼团运营信息
		 MergeGroupAdd mergeGroupAdd = new MergeGroupAdd();
		 
		 /**
		  * 查询該团已成的订单数量,来判断这个团是否已经成功
		  */
		 if(operate.getNumber() == (mergeGroup.getPeopleNumber() - 1)){//说明是最后一个参团的人, 拼团成功
			 
			 operate.setStatus(MergeGroupAddStatusEnum.SUCCESS.value());//设置成团状态成功
			 mergeGroupAdd.setStatus(MergeGroupAddStatusEnum.SUCCESS.value());//设置该参团人员状态成功
			 
			 mergeGroupAddDao.updateMergeGroupAddStatus(operate.getId());//批量修改该团所有参团记录状态为成功
			 subDao.updateMergeGroupAddStatusByAddNumber(operate.getAddNumber());//批量修改该团所有参团订单记录状态为成功
			 mergeGroupDao.updateMergeGroupSuccessCount(operate.getMergeId());//拼团成功更改成功订单数+1
			 
		 }else{//待成团
			 
			 mergeGroupAdd.setStatus(MergeGroupAddStatusEnum.IN.value());
			 sub.setMergeGroupStatus(SubMergeGroupStatusEnum.MERGE_GROUP_IN.value());
	    	 subDao.update(sub);//将订单改为拼团进行中  
		 }
		 
		 operate.setNumber(operate.getNumber()+1);//修改人数
		 mergeGroupOperateDao.updateMergeGroupOperate(operate);
		 
		//保存拼团运营信息
	   	 mergeGroupAdd.setMergeId(sub.getActiveId());
	   	 mergeGroupAdd.setShopId(sub.getShopId());
	   	 mergeGroupAdd.setSubId(sub.getSubId());
	   	 mergeGroupAdd.setSubNumber(sub.getSubNumber());
	   	 mergeGroupAdd.setUserId(sub.getUserId());
	   	 mergeGroupAdd.setUserName(sub.getUserName());
	   	 mergeGroupAdd.setPortraitPic(userDetail.getPortraitPic());
	   	 mergeGroupAdd.setNickName(userDetail.getNickName());
	   	 mergeGroupAdd.setIsHead(false);
	   	 mergeGroupAdd.setOperateId(operate.getId());
	   	 mergeGroupAdd.setCreateTime(new Date());
	   	 mergeGroupAddDao.saveMergeGroupAdd(mergeGroupAdd);
	}
	
	/**
	 * 处理新开团逻辑
	 * @param mergeGroup 拼团活动
	 * @param sub 订单
	 */
	private void openMergeGroup(MergeGroup mergeGroup, Sub sub){
		
		//生成团编号
		 String addNumber = CommonServiceUtil.getRandomSn();
		 sub.setAddNumber(addNumber);
		 
		 //新增活动运营信息
		 MergeGroupOperate mergeGroupOperate = new MergeGroupOperate();
		 mergeGroupOperate.setShopId(sub.getShopId());
		 mergeGroupOperate.setMergeId(sub.getActiveId());
		 mergeGroupOperate.setAddNumber(sub.getAddNumber());
		 mergeGroupOperate.setNumber(1);
		 mergeGroupOperate.setStatus(MergeGroupAddStatusEnum.IN.value());
		 mergeGroupOperate.setCreateTime(new Date());
		 mergeGroupOperate.setEndTime(DateUtils.rollDate(mergeGroupOperate.getCreateTime(), 0, 0, 0, mergeGroup.getCountdown(), 0, 0));
		 mergeGroupOperate.setIsHeadFree(mergeGroup.getIsHeadFree());
		 
		 Long operateId = mergeGroupOperateDao.createId();
		 mergeGroupOperate.setId(operateId);
		 
		 //保存拼团运营信息
		 UserDetail userDetail = userDetailDao.getUserDetailById(sub.getUserId());//获取用户信息
	   	 MergeGroupAdd mergeGroupAdd = new MergeGroupAdd();
	   	 mergeGroupAdd.setMergeId(sub.getActiveId());
	   	 mergeGroupAdd.setShopId(sub.getShopId());
	   	 mergeGroupAdd.setSubId(sub.getSubId());
	   	 mergeGroupAdd.setUserId(sub.getUserId());
	   	 mergeGroupAdd.setUserName(sub.getUserName());
	   	 mergeGroupAdd.setNickName(userDetail.getNickName());
	     mergeGroupAdd.setPortraitPic(userDetail.getPortraitPic());
	   	 mergeGroupAdd.setIsHead(true);
	   	 mergeGroupAdd.setStatus(MergeGroupAddStatusEnum.IN.value());
	   	 mergeGroupAdd.setOperateId(operateId);
	   	 mergeGroupAdd.setCreateTime(new Date());
	   	 mergeGroupAdd.setSubNumber(sub.getSubNumber());
	   	 
		 if(mergeGroup.getPeopleNumber() == 1){ //如果是一人团
			 mergeGroupOperate.setStatus(MergeGroupAddStatusEnum.SUCCESS.value());
			 mergeGroupAdd.setStatus(MergeGroupAddStatusEnum.SUCCESS.value());
			 sub.setMergeGroupStatus(SubMergeGroupStatusEnum.MERGE_GROUP_SUCCESS.value());//将订单改为拼团完成
		 }else{
			 mergeGroupOperate.setStatus(MergeGroupAddStatusEnum.IN.value());
			 mergeGroupAdd.setStatus(MergeGroupAddStatusEnum.IN.value());
			 sub.setMergeGroupStatus(SubMergeGroupStatusEnum.MERGE_GROUP_IN.value());//将订单改为拼团进行中
		 }
		 
		 mergeGroupOperateDao.saveMergeGroupOperate(mergeGroupOperate, operateId);
		 mergeGroupAddDao.saveMergeGroupAdd(mergeGroupAdd);
	   	 subDao.update(sub);
	} 
	
	private void updateGroupSubStatus(Sub sub) {
		
		//获取团购信息 TODO 考虑抽成独立插件?
		Group group = productDao.getGroup(sub.getActiveId());
		//判断前团购人数+1是否满足成团人数  若满足修改订单的团购状态为成功即直接为待发货状态  不满足为不成功即为等待成团状态
		Long count = group.getBuyerCount()+1;
		if(group.getPeopleCount() <= count){
			
			if(group.getPeopleCount()==count.intValue()){
				PaymentLog.info("###################### 团购人数满足 by count={}。处理之前拼团中订单 处理成功 ########################",count);
				subDao.updateSubGroupStatusByGroup(sub.getActiveId(),SubGroupStatusEnum.MERGE_GROUP_SUCCESS.value());
			}
			
			sub.setGroupStatus(SubGroupStatusEnum.MERGE_GROUP_SUCCESS.value());
		}else{
			sub.setGroupStatus(SubGroupStatusEnum.MERGE_GROUP_IN.value());
		}
		subDao.update(sub);//将订单改为拼团进行中
		//修改团购购买人数  TODO 考虑抽成独立插件?
		group.setBuyerCount(count);
		productDao.update(group);
		
	}

	private void saveOrderHistory(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_PAY.value());
		StringBuilder sb = new StringBuilder();
		sb.append(sub.getUserName()).append("于").append(time).append("完成订单支付 ");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

	//获取门店地址
	private String getAddress(Long storeId){
		Store store = storeDao.getById(storeId);
		String address = store.getAddress();
		if(address.length()>20){
			address = address.substring(0, 19) + "…";
		}
		return address;
	}
}
