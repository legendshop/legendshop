/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ExternalLinkDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.ExternalLink;
import com.legendshop.util.AppUtils;

/**
 * 友情链接DaoImpl
 */
@Repository
public class ExternalLinkDaoImpl  extends GenericDaoImpl<ExternalLink, Long>  implements ExternalLinkDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(ExternalLinkDaoImpl.class);

	@Override
	@Cacheable(value = "ExternalLinkList")
	public List<ExternalLink> getExternalLink() {
		log.debug("getExternalLink");
		List<ExternalLink> list = query("select * from ls_extl_link order by bs", ExternalLink.class);
		return list;
	}

	@Override
	@CacheEvict(value = "ExternalLinkList", allEntries=true)
	public void deleteExternalLinkById(Long id) {
		deleteById(id);
	}

	@Override
	public void updateExternalLink(ExternalLink externalLink) {
		update(externalLink);
	}

	@Override
	public PageSupport<ExternalLink> getFriendLink(String curPageNO, String userName) {
		CriteriaQuery cq = new CriteriaQuery(ExternalLink.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getFrontPageSize());
		cq.eq("userName", userName);
		cq.addAscOrder("bs");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ExternalLink> getDataByCriteriaQuery(String curPageNO, ExternalLink externalLink,
			Integer pageSize, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(ExternalLink.class, curPageNO);
		if(AppUtils.isNotBlank(pageSize)){
			cq.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {// 非外部排序
			cq.addOrder("desc", "bs");
		}else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		return queryPage(cq);
	}
	
}
