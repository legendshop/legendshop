/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.MergeGroupDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.constant.ActivitySearchTypeEnum;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.MergeGroupAddDto;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.util.AppUtils;

/**
 * The Class MergeGroupDaoImpl. 拼团活动Dao实现类
 */
@Repository
public class MergeGroupDaoImpl extends GenericDaoImpl<MergeGroup, Long> implements MergeGroupDao {

	
	private static final long ONLINE = 1;

	/** 获取有效的拼团活动（活动上线、已开始、未过期、商品是正常状态） */
	public PageSupport<MergeGroup> getAvailable(String curPageNO) {
		
		SimpleSqlQuery query = new SimpleSqlQuery(MergeGroup.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		
		String nowDate = DateUtils.format(new Date());
		map.put("timeQuery", "and m.start_time <'" + nowDate + "' and m.end_time >'" + nowDate + "'");
		map.put("status", ONLINE);
		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		String queryAllSQL = ConfigCode.getInstance().getCode("mergeGroup.getAvailableMergeGroupListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("mergeGroup.getAvailableMergeGroupList", map);
		
		map.remove("timeQuery");
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 根据Id获取拼团活动
	 */
	public MergeGroup getMergeGroup(Long id) {
		return getById(id);
	}

	/**
	 * 删除拼团活动
	 */
	public int deleteMergeGroup(MergeGroup mergeGroup) {
		return delete(mergeGroup);
	}

	/**
	 * 保存拼团活动
	 */
	public Long saveMergeGroup(MergeGroup mergeGroup) {
		return save(mergeGroup);
	}

	/**
	 * 更新拼团活动
	 */
	public int updateMergeGroup(MergeGroup mergeGroup) {
		return update(mergeGroup);
	}

	/**
	 * 获取拼团活动列表
	 */
	public PageSupport<MergeGroupDto> getMergeGroupList(String curPageNO, Integer pageSize, MergeGroupDto mergeGroup) {
		SimpleSqlQuery query = new SimpleSqlQuery(MergeGroupDto.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		
		if (AppUtils.isNotBlank(mergeGroup.getMergeName())) {
			map.like("mergeName", "%" + mergeGroup.getMergeName() + "%");
		}
		
		map.put("shopId", mergeGroup.getShopId());
		
		// 商家活动列表查询，只查询没被商家删除的活动
		if (AppUtils.isBlank(mergeGroup.getFlag())) {
			map.put("deleteStatus", "AND m.delete_status != 1");
		}

		//根据searchType,组装不同的筛选条件
		String tab = mergeGroup.getSearchType();
		if (AppUtils.isNotBlank(tab)) {
			
			Date currDate = new Date();
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			switch(ActivitySearchTypeEnum.matchType(tab)){
	         	case WAIT_AUDIT :
	         		map.put("status", MergeGroupStatusEnum.NEED_AUDITED.value());
	         		map.put("unFinished", "AND m.end_time > "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case NOT_PASS:
	         		map.put("status", MergeGroupStatusEnum.NO_AUDITED.value());
	         		map.put("unFinished", "AND m.end_time > "+ "'" +  sd.format(currDate) + "'");
		        	break;
	         	case NOT_STARTED :
	         		map.put("status", MergeGroupStatusEnum.ONLINE.value());
	         		map.put("unStarted", "AND m.start_time > "+"'" +  sd.format(currDate) + "'");
		            break;
	         	case ONLINE :
	         		map.put("status", MergeGroupStatusEnum.ONLINE.value());
	         		map.put("isStarted", "AND m.start_time < "+"'" +  sd.format(currDate) + "'");
	         		map.put("unFinished", "AND m.end_time > "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case FINISHED :
	         		map.put("isFinished", "AND m.end_time < "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case EXPIRED :
	         		map.put("status", MergeGroupStatusEnum.OFFLINE.value());
	         		map.put("unFinished", "AND m.end_time > "+ "'" +  sd.format(currDate) + "'");
	         		break;
	         	default :
			}
		}
		
		String queryAllSQL = ConfigCode.getInstance().getCode("mergeGroup.getMergeGroupListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("mergeGroup.getMergeGroupList", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		
		map.remove("isStarted");
		map.remove("unStarted");
		map.remove("isFinished");
		map.remove("unFinished");
		map.remove("deleteStatus");
		
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	public void updateMergeGroupStatus(Long id, Integer status) {
		update("update ls_merge_group set status=? where id=?", status, id);
	}

	public Integer updateStatusByShopId(Long id, Integer status, Long shopId) {
		return update("update ls_merge_group set status=? where id=? and shop_id=?", status, id, shopId);
	}

	public PageSupport<MergeGroupOperationDto> getMergeGroupOperationList(String curPageNO, Integer pageSize, Long mergeId, Long shopId) {
		// 组装sql
		QueryMap map = new QueryMap();
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		int offset = (Integer.valueOf(curPageNO) - 1) * pageSize;
		map.put("mergeId", mergeId);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Object> args = new ArrayList<Object>();
		args.add(mergeId);
		if (AppUtils.isNotBlank(shopId)) {
			map.put("shopId", shopId);
			args.add(shopId);
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("mergeGroup.getMergeGroupOperationListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("mergeGroup.getMergeGroupOperationList", map);

		// 查询总页数及分页数据
		Long total = getLongResult(queryAllSQL, args.toArray());
		List<MergeGroupDto> list = query(querySQL, MergeGroupDto.class, args.toArray());
		// 组装分页数据
		List<MergeGroupOperationDto> MergeGroupLists = getMergeGroupOperationList(list, mergeId);

		// 组装分页工具条
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<MergeGroupOperationDto> ps = new PageSupport<MergeGroupOperationDto>(MergeGroupLists, pageProvider);

		return ps;

	}

	private List<MergeGroupOperationDto> getMergeGroupOperationList(List<MergeGroupDto> list, Long mergeId) {
		if (AppUtils.isNotBlank(list)) {
			LinkedHashMap<String, MergeGroupOperationDto> map = new LinkedHashMap<String, MergeGroupOperationDto>();
			for (int i = 0; i < list.size(); i++) {
				MergeGroupDto merge = list.get(i);
				if (map.containsKey(merge.getAddNumber())) { // 存在
					MergeGroupOperationDto MergeGroupDto = map.get(merge.getAddNumber());

					MergeGroupDto.setAddPeopleNumber(MergeGroupDto.getAddPeopleNumber() + 1);
					List<MergeGroupAddDto> oldList = MergeGroupDto.getSubList();
					MergeGroupAddDto mergeGroupAddDto = new MergeGroupAddDto();
					mergeGroupAddDto.setActualTotal(merge.getActualTotal());
					mergeGroupAddDto.setProductNums(merge.getProductNums());
					mergeGroupAddDto.setSubNumber(merge.getSubNumber());
					mergeGroupAddDto.setIsHead(merge.getIsHead());
					mergeGroupAddDto.setUserName(merge.getUserName());
					mergeGroupAddDto.setAddNumber(merge.getAddNumber());
					mergeGroupAddDto.setSubId(merge.getSubId());
					mergeGroupAddDto.setMergeId(mergeId);
					mergeGroupAddDto.setSubDate(merge.getSubDate());

					oldList.add(mergeGroupAddDto);
					MergeGroupDto.setSubList(oldList);
				} else {
					MergeGroupOperationDto MergeGroupDto = new MergeGroupOperationDto();
					// 共有的
					MergeGroupDto.setAddNumber(merge.getAddNumber());
					MergeGroupDto.setPeopleNumber(merge.getPeopleNumber());
					MergeGroupDto.setEndTime(merge.getEndTime());
					MergeGroupDto.setStatus(merge.getStatus().longValue());
					// 第一次插入团订单
					MergeGroupDto.setAddPeopleNumber(1);

					MergeGroupAddDto mergeGroupAddDto = new MergeGroupAddDto();
					mergeGroupAddDto.setActualTotal(merge.getActualTotal());
					mergeGroupAddDto.setProductNums(merge.getProductNums());
					mergeGroupAddDto.setSubNumber(merge.getSubNumber());
					mergeGroupAddDto.setIsHead(merge.getIsHead());
					mergeGroupAddDto.setUserName(merge.getUserName());
					mergeGroupAddDto.setAddNumber(merge.getAddNumber());
					mergeGroupAddDto.setSubId(merge.getSubId());
					mergeGroupAddDto.setMergeId(mergeId);
					mergeGroupAddDto.setSubDate(merge.getSubDate());

					MergeGroupDto.addSubList(mergeGroupAddDto);
					map.put(merge.getAddNumber(), MergeGroupDto);
				}
			}

			List<MergeGroupOperationDto> lists = new ArrayList<MergeGroupOperationDto>();
			for (String key : map.keySet()) {
				MergeGroupOperationDto st = map.get(key);
				lists.add(st);
			}
			return lists;

		}

		return null;
	}

	public PageSupport<MergeGroup> queryMergeGroupList(String curPageNO, Long shopId, MergeGroupDto mergeGroup) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		CriteriaQuery cq = new CriteriaQuery(MergeGroup.class, curPageNO);
		cq.setPageSize(10);
		if (AppUtils.isNotBlank(mergeGroup) && AppUtils.isNotBlank(mergeGroup.getMergeName())) {
			cq.like("mergeName", "%" + mergeGroup.getMergeName() + "%");
		}
		if (AppUtils.isNotBlank(shopId)) {
			cq.eq("shopId", shopId);
		}
		cq.addDescOrder("createTime");
		return queryPage(cq);
	}

	public MergeGroup getMergeGroupByshopId(Long id, Long shopId) {
		return this.getByProperties(new EntityCriterion().eq("id", id).eq("shopId", shopId));
	}

	public MergeGroup getMergeGroupByshopId(Long id) {
		return this.getByProperties(new EntityCriterion().eq("id", id));
	}

	public void deleteMergeGroupByShopId(Long shopId, Long id) {
		update("DELETE m, a FROM ls_merge_group m LEFT JOIN ls_merge_group_add a ON m.id = a.merge_id WHERE m.id = ? AND m.shop_id = ? ", id, shopId);
	}

	public MergeGroupJoinDto getMergeGroupOperationDtoByAddNumber(String addNumber) {
		String sql = "SELECT g.id AS mergeId,g.status AS mergeStatus,g.end_time AS endTime,g.people_number AS peopleNumber,"+
					 "o.id AS operateId,o.number AS number,o.status AS operateStatus "+
					 "FROM ls_merge_group g RIGHT JOIN ls_merge_group_operate o ON g.id=o.merge_id"+
					 " WHERE o.add_number=?";
		return this.get(sql, MergeGroupJoinDto.class, addNumber);
	}

	public void updateMergeGroupSuccessCount(Long mergeId) {
		String sql ="update ls_merge_group set count=count+1 where id=?";
		this.update(sql, mergeId);
	}

	public boolean checkExitproduct(Long prodId) {
		String sql = "SELECT COUNT(1) FROM ls_merge_product p INNER JOIN ls_merge_group g ON g.id = p.merge_id "
				+ "WHERE prod_id = ? AND g.status IN (?,?) AND end_time > ?";
		return get(sql, Integer.class, prodId,MergeGroupStatusEnum.NEED_AUDITED.value(),MergeGroupStatusEnum.ONLINE.value(), new Date())>0;
	}

	public List<MergeGroup> getMergeGroupAll() {
		return queryAll();
	}

	public List<MergeGroup> queryMergeGroupListByStatus() {
		return queryByProperties(new EntityCriterion().eq("status", 1));
	}

	public void updateList(List<MergeGroup> cancalGroups) {
		update(cancalGroups);
	}

	String GroupExemptionSql = "SELECT COUNT(1) FROM ls_merge_group_operate INNER JOIN ls_merge_group_add ON ls_merge_group_operate.merge_id=ls_merge_group_add.merge_id "
			+ " WHERE ls_merge_group_operate.add_number=? AND ls_merge_group_add.user_id=? AND ls_merge_group_add.status=? AND ls_merge_group_add.is_head=1 " ;
	
	public boolean isGroupExemption(String addBumber,String userId) {
		return this.getLongResult(GroupExemptionSql, addBumber,userId,MergeGroupAddStatusEnum.SUCCESS.value())>0;
	}

	@Override
	public Long joinMergeGroup(Long activeId, String userId) {
		String sql = "select count(*) from ls_merge_group_add where merge_id = ? and user_id = ?";
		return this.getLongResult(sql, activeId,userId);
	}

	/**
	 * 更新拼团活动删除状态为商家删除
	 */
	@Override
	public void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus) {
		
		String sql = "UPDATE ls_merge_group SET delete_status = ? WHERE shop_id = ? AND id = ?";
		update(sql, deleteStatus,shopId,id);
	}

	/**
	 * 运营数据 -- 统计活动交易总金额跟参与人数
	 */
	@Override
	public OperateStatisticsDTO getTotalAmountAndParticipants(Long mergeId, Long shopId) {
		
		String sql = "SELECT COALESCE(SUM(s.actual_total),0) AS totalAmount,COALESCE(COUNT(s.sub_id),0) AS participants "
					+"FROM ls_merge_group_operate o LEFT JOIN ls_merge_group_add a ON o.id = a.operate_id "
					+"LEFT JOIN ls_sub s ON a.sub_id = s.sub_id WHERE 1 = 1 AND o.merge_id = ? AND o.shop_id = ?";
		
		return get(sql, OperateStatisticsDTO.class, mergeId,shopId);
	}

	/**
	 * 统计拼团活动成功交易金额以及成功拼团数
	 */
	@Override
	public OperateStatisticsDTO getSucceedAmountAndCount(Long mergeId, Long shopId) {
		
		String sql = "SELECT COALESCE(SUM(s.actual_total),0) AS succeedAmount,COUNT(DISTINCT  o.id) AS succeedCount "
					+"FROM ls_merge_group_operate o LEFT JOIN ls_merge_group_add a ON o.id = a.operate_id "
					+"LEFT JOIN ls_sub s ON a.sub_id = s.sub_id WHERE 1 = 1 AND o.merge_id = ? AND o.shop_id = ? AND o.status = 2";
	
		return get(sql, OperateStatisticsDTO.class, mergeId,shopId);
	}

	/**
	 *  待成团的数量
	 */
	@Override
	public Integer getWaitFightCount(Long mergeId, Long shopId) {
		String sql = "SELECT count(*) FROM ls_merge_group_operate o WHERE o.merge_id = ? and o.shop_id= ? AND o.status = 1";
//		String sql = "SELECT COUNT(*)"
//					+"FROM ls_merge_group_operate o LEFT JOIN ls_merge_group_add a ON o.id = a.operate_id "
//					+"LEFT JOIN ls_sub s ON a.sub_id = s.sub_id WHERE 1 = 1 AND o.merge_id = ? AND o.shop_id = ? AND o.status = 1";

		return get(sql, Integer.class, mergeId,shopId);
	}

	@Override
	public MergeGroup getMergeGroupPrice(Long prodId, Long skuId) {
		String sql="select g.id,g.min_price,status,g.start_time,g.end_time from ls_merge_group g inner join ls_merge_product p on g.id = p.merge_id where g.status=1 and p.prod_id=? and p.sku_id=? ";
		return get(sql,MergeGroup.class,prodId,skuId);
	}


}
