/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassComm;
import com.legendshop.model.entity.GrassProd;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassProdDao;

/**
 * The Class GrassProdDaoImpl. 种草文章关联商品表Dao实现类
 */
@Repository
public class GrassProdDaoImpl extends GenericDaoImpl<GrassProd, Long> implements GrassProdDao{

	/**
	 * 根据Id获取种草文章关联商品表
	 */
	public GrassProd getGrassProd(Long id){
		return getById(id);
	}

	/**
	 *  删除种草文章关联商品表
	 *  @param grassProd 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassProd(GrassProd grassProd){
    	return delete(grassProd);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassProd(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草文章关联商品表
	 */
	public Long saveGrassProd(GrassProd grassProd){
		return save(grassProd);
	}

	/**
	 *  更新种草文章关联商品表
	 */		
	public int updateGrassProd(GrassProd grassProd){
		return update(grassProd);
	}

	/**
	 * 分页查询种草文章关联商品表列表
	 */
	public PageSupport<GrassProd>queryGrassProd(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassProd.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public List<GrassProd> getGrassProdByGrassId(Long grassId) {
		String sql="SELECT id,grarticle_id,prod_id FROM ls_grass_prod WHERE grarticle_id=?";
		return query(sql, GrassProd.class,grassId);
	}

	@Override
	public void deleteByGrassId(Long id) {
		String sql="DELETE FROM ls_grass_prod WHERE grarticle_id=?";
		update(sql,id);
	}

}
