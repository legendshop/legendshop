package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

public interface WeiXinUserDao extends GenericDao<WeixinGzuserInfo, Long> {

	WeixinGzuserInfo getGzuserInfo(String openId);

	/**
	 * 保存用户关注
	 * @param weixinGzuserInfo
	 * @return
	 */
	Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);

}
