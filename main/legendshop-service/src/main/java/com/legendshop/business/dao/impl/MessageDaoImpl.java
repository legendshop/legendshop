/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MessageDao;
import com.legendshop.business.dao.MsgStatusDao;
import com.legendshop.business.dao.MsgTextDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.Msg;
import com.legendshop.model.entity.MsgStatus;
import com.legendshop.model.entity.MsgText;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.util.AppUtils;

/**
 * 站内信Dao.
 */
@Repository
public class MessageDaoImpl extends GenericDaoImpl<Msg, Long> implements MessageDao {

	@Autowired
	private MsgTextDao msgTextDao;

	@Autowired
	private MsgStatusDao msgStatusDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	private final String sqlForGetMsgText = "select lmt.title, lmt.text, lm.send_name as sendName,lm.receive_name as receiveName,lm.is_global as isGlobal, lmt.rec_date as recDate "
			+ "from ls_msg_text lmt , ls_msg lm where lmt.id = lm.text_id and lm.msg_id = ?";

	private final String sqlForCheckGlobalMsg = "select grade_id from ls_msg lm  join ls_msg_usrgrad lmu on lm.msg_id = lmu.msg_id and lm.is_global = 1 and lm.msg_id = ? "
			+ "and grade_id = (select grade_id from ls_usr_detail ud where ud.user_name = ?)";


	private final String sqlForCalUnreadMsgCount = "select count(m.msg_id) from (select ls_msg. *, ls_msg_status.user_name as username,ls_msg_status.status as realstatus  from ls_msg join ls_msg_status on ls_msg.msg_id =ls_msg_status.msg_id"
			+ " where ls_msg.is_global =1 union all select ls_msg.*,ls_msg.receive_name as username,ls_msg.status as realstatus from ls_msg where ls_msg.is_global =0) m where m.realstatus=0 and m.delete_status2=0 and m.username=?";


	private final String sqlForCalUnreadSystemMsgCount =
			"SELECT COUNT(*) FROM ls_msg lm JOIN ls_msg_usrgrad lmu ON lm.msg_id = lmu.msg_id " +
					"AND lmu.grade_id = ? " +
					"JOIN ls_msg_text lst ON lm.text_id = lst.id " +
					"LEFT JOIN ls_msg_status lms ON lm.msg_id = lms.msg_id AND lms.user_name = ?" +
					"WHERE lms.status IS NULL AND lst.rec_date > ? ";

	@Override
	public void deleteOutbox(Long msgId) {
		update("update ls_msg set delete_status = 1 where msg_id = ?", msgId);
	}

	@Override
	@CacheEvict(value = "UnreadMsgCount", key = "#userName")
	public void deleteInbox(Long msgId, String userName) {
		update("update ls_msg set delete_status2 = 1 where msg_id = ? and receive_name = ?", msgId, userName);
	}

	@Override
	public SiteInformation getMsgText(Long msgId) {
		return get(sqlForGetMsgText, SiteInformation.class, msgId);
	}

	@Override
	@CacheEvict(value = "UnreadMsgCount", key = "#userName")
	public void updateMsgRead(String userName, Long msgId) {
		update("update ls_msg lm set status = 1 where msg_id = ?", msgId);
	}

	@Override
	public SiteInformation getSystemMessages(Long msgId) {
		SiteInformation siteInformation = get(
				"select lm.msg_id as msgId, lm.user_level as userLevel,lst.title as title,lst.text as text from ls_msg lm,ls_msg_text lst "
						+ "where lm.text_id =lst.id and lm.is_global = 1 and lm.msg_id = ?",
				SiteInformation.class, msgId);
		return siteInformation;
	}

	@Override
	public void saveSystemMessages(SiteInformation siteInformation, Integer[] userGradeArray) {
		// 保存MsgText
		MsgText msgText = new MsgText();
		msgText.setText(siteInformation.getText());
		msgText.setTitle(siteInformation.getTitle());
		msgText.setRecDate(new Date());
		Long textId = msgTextDao.save(msgText);

		// 保存Msg
		Msg msg = new Msg();
		msg.setSendName(siteInformation.getSendName());
		msg.setIsGlobal(1);
		msg.setTextId(textId);
		msg.setStatus(0);
		msg.setDeleteStatus2(0);
		msg.setDeleteStatus(0);
		msg.setReceiveName("");

		// 保存用户等级到ls_msg
		if (userGradeArray != null) {
			StringBuffer stringBuffer = new StringBuffer();

			String sql = "select name from ls_usr_grad where grade_id in ( " + repeat("?", ",", userGradeArray.length)
					+ ")";

			List<String> nameList = getJdbcTemplate().queryForList(sql, String.class, (Object[]) userGradeArray);

			for (String name : nameList) {
				stringBuffer.append(name).append(",");
			}

			String userLevel = stringBuffer.substring(0, stringBuffer.length() - 1);
			msg.setUserLevel(userLevel);
		}

		Long msgId = this.save(msg);

		saveUserGrade(userGradeArray, msgId);

	}

	private String repeat(String s, String separator, int count) {
		StringBuilder string = new StringBuilder((s.length() + separator.length()) * count);
		while (--count > 0) {
			string.append(s).append(separator);
		}
		return string.append(s).toString();
	}

	@Override
	public void updateMsgText(String text, String title, Long textId) {
		update("update ls_msg_text set title = ? , text = ?  where  id = ?", title, text, textId);
	}

	@Override
	public void deleteUserGrade(Long msgId) {
		update("delete from ls_msg_usrgrad where msg_id = ?", msgId);
	}

	@Override
	public void saveUserGrade(Integer[] userGradeArray, Long msgId) {
		if (userGradeArray != null && userGradeArray.length > 0) {
			List<Object[]> batchArgs = new ArrayList<Object[]>(userGradeArray.length);
			for (Integer userGrade : userGradeArray) { // entity MsgUsrGrade
				Object[] obj = new Object[2];
				obj[0] = msgId;
				obj[1] = userGrade;
				batchArgs.add(obj);
			}
			this.getJdbcTemplate().batchUpdate("insert into ls_msg_usrgrad(msg_id,grade_id) values (?,?)", batchArgs);
		}
	}

	@Override
	public Long getTextId(Long msgId) {
		return getJdbcTemplate().queryForObject("select text_id from ls_msg where msg_id = ?", new Object[]{msgId}, Long.class);
	}

	@Override
	public void updateMsgLevel(Integer[] userGradeArray, Long msgId) {
		if (userGradeArray == null) {
			update("update ls_msg set user_level = ? where msg_id = ? ", null, msgId);
		} else {
			StringBuffer stringBuffer = new StringBuffer();
			String name;
			for (Integer selectUserGrade : userGradeArray) {
				name = getJdbcTemplate().queryForObject("select name from ls_usr_grad where grade_id = ?",
						String.class, selectUserGrade);
				stringBuffer.append(name + ", ");
			}
			String userLevel = stringBuffer.substring(0, stringBuffer.length() - 2);
			update("update ls_msg set user_level = ? where msg_id = ? ", userLevel, msgId);
		}

	}

	@Override
	public Integer getGradeId(String userName) {
		String sql = "select grade_id from ls_usr_detail where user_name = ?";
		return this.get(sql, Integer.class, userName);
	}

	public MsgStatus getSystemMsgStatus(String userName, Long msgId) {
		return msgStatusDao.getSystemMsgStatus(userName, msgId);
		// return
		// get("select id as id, user_name as userName, msg_id as msgId, status as status from ls_msg_status where user_name = ? and msg_id = ?",
		// MsgStatus.class, userName,msgId);
	}

	/**
	 * 进去status表要不是已读，或者是删除状态，未读状态无需记录
	 */
	@Override
	public void updateSystemMsgStatus(String userName, Long msgId, int status) {
		MsgStatus msgStatus = getSystemMsgStatus(userName, msgId);
		if (msgStatus == null) {
			msgStatus = new MsgStatus();
			msgStatus.setMsgId(msgId);
			msgStatus.setStatus(status);
			msgStatus.setUserName(userName);
			msgStatusDao.saveMsgStatus(msgStatus);
		} else {
			msgStatus.setMsgId(msgId);
			msgStatus.setStatus(status);
			msgStatus.setUserName(userName);
			msgStatusDao.updateMsgStatus(msgStatus);
		}

	}

	@Override
	public void deleteSystemMsgStatus(Long msgId) {
		update("update ls_msg set status = -1 where is_global = 1 and msg_id = ?", msgId);
	}

	/**
	 * 检查昵称是否为空
	 */
	@Override
	public boolean isReceiverExist(String receiveName) {
		if (receiveName.indexOf(",") > 0) {
			String[] str = receiveName.split(",");
			for (String name : str) {
				List<Integer> list = query("select 1 from ls_usr_detail where nick_name = ?", Integer.class, name);
				if (AppUtils.isBlank(list)) {
					return false;
				}
			}
			return true;
		} else {
			List<Integer> list = query("select 1 from ls_usr_detail where nick_name = ?", Integer.class, receiveName);
			return AppUtils.isNotBlank(list);
		}
	}

	@Override
	public boolean checkGlobalMsg(String userName, Long msgId) {
		Long result = getJdbcTemplate().queryForObject(sqlForCheckGlobalMsg, new Object[]{msgId, userName},
				Long.class);
		return result != null && result >= 1;
	}


	@Override
	@Cacheable(value = "UnreadMsgCount", key = "#userName")
	public Integer calUnreadMsgCount(String userName) {
		return this.get(sqlForCalUnreadMsgCount, Integer.class, userName);
	}

	@Override
	@Cacheable(value = "UnreadMsgCount", key = "#userName")
	public Integer calUnreadMsgCount(String userName, Integer gradeId, Date userRegtime) {
		Integer messageCount = this.get(sqlForCalUnreadMsgCount, Integer.class, userName);
		if (messageCount == null) {
			messageCount = 0;
		}
		Integer result = this.get(sqlForCalUnreadSystemMsgCount, Integer.class, gradeId, userName, userRegtime);
		if (result == null) {
			result = 0;
		}
		return messageCount + result;
	}

	@Override
	@CacheEvict(value = "UnreadMsgCount", key = "#msg.receiveName")
	public Msg saveMsg(Msg msg) {
		save(msg);
		return msg;
	}

	/**
	 * 删除收件箱
	 * @param msgIds 消息ID数组
	 * @param userName 当前用户名
	 * @return
	 */
	@Override
	public int deleteInboxByMsgIds(String[] msgIds, String userName) {
		if(null == msgIds || msgIds.length == 0){
			return update("UPDATE ls_msg set delete_status2 = 1 WHERE receive_name = ?", userName);
		}
		
		StringBuilder sql = new StringBuilder("UPDATE ls_msg set delete_status2 = 1 WHERE receive_name = ? AND msg_id in (");
		
		List<Object> args = new ArrayList<>();
		args.add(userName);
		for(int i = 0, len = msgIds.length; i < len; i++){
			
			if((len - 1) == i){//是否最后一个
				sql.append("?)");
			}else{
				sql.append("?, ");
			}
			args.add(msgIds[i]);
		}
		
		return update(sql.toString(), args.toArray());
	}

	@Override
	public void clearInboxMessage(String userName) {
		update("update ls_msg set delete_status2 = 1 where receive_name = ?", userName);
	}

	@Override
	public int getCalUnreadSystemMessagesCount(Integer gradeId, String userName, Date userRegtime) {

		Integer result = this.get(sqlForCalUnreadSystemMsgCount, Integer.class, gradeId, userName, userRegtime);

		return result == null ? 0 : result;
	}

	@Override
	public SiteInformation getNewSystemMessages(String userName, Integer gradeId, Date userRegtime) {

		String sql = "SELECT lm.msg_id AS msgId, lst.text AS TEXT, lst.title AS title ,lst.rec_date AS recDate, lm.send_name AS sendName, lms.status AS STATUS " +
				"FROM ls_msg lm JOIN ls_msg_usrgrad lmu ON lm.msg_id = lmu.msg_id AND lmu.grade_id = ? " +
				"JOIN ls_msg_text lst ON lm.text_id =lst.id " +
				"LEFT JOIN ls_msg_status lms ON lm.msg_id = lms.msg_id " +
				"AND lms.user_name = ? " +
				"WHERE  lms.status != -1 " +
				"OR (lms.status IS NULL AND lst.rec_date > ? ) " +
				"ORDER BY lst.rec_date DESC LIMIT 0,1";

		SiteInformation siteInformation = this.get(sql, SiteInformation.class, gradeId, userName, userRegtime);

		return siteInformation;
	}

	@Override
	public Long getCountById(String userName) {
		return this.getLongResult("select count(*) from ls_msg where receive_name = ? and status=0 and delete_status2=0", userName);
	}

	@Override
	public PageSupport<SiteInformation> inboxSiteInformation(String userName, String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getInBoxCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getInBox", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> outboxSiteInformation(String userName, String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getOutBoxCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getOutBox", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> systemInformation(String userName, Integer gradeId, String curPageNO, UserDetail userDetail) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("gradeId", gradeId);
		map.put("userName", userName);
		if (userDetail != null && userDetail.getUserRegtime() != null) {
			map.put("userRegtime", userDetail.getUserRegtime());
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.systemInformationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.systemInformation", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> querySimplePage(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.msg.getInBoxCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.msg.getInBox", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> querySiteInformation(String curPageNO, String title, Integer pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(title)) {
			map.put("title", "%" + title.trim() + "%");
		}
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			query.setPageSize(pageSize);
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("msg.getSystemMessagesCount", map);
		String querySQL = ConfigCode.getInstance().getCode("msg.getSystemMessages", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> queryMessage(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.msg.getOutBoxCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.msg.getOutBox", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SiteInformation> querySimplePage(String curPageNO, Integer gradeId, String userName, UserDetail userDetail) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, 20, curPageNO);
		
		QueryMap map = new QueryMap();
		map.put("gradeId", gradeId);
		map.put("userName", userName);
		
		if (userDetail != null && userDetail.getUserRegtime() != null) {
			map.put("userRegtime", userDetail.getUserRegtime());
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.msg.systemInformationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.msg.systemInformation", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public int getCalUnreadSystemMessagesCount(Integer gradeId, Date userRegtime) {
		Integer result = this.get(sqlForCalUnreadSystemMsgCount, Integer.class, gradeId, userRegtime);
		return result == null ? 0 : result;
	}

	@Override
	public Integer getAuditShop() {
		String sql = "SELECT COUNT(1) FROM ls_shop_detail WHERE STATUS = -1;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getProductConsultCount() {
		String sql = "select count(*) from ls_prod p,ls_prod_cons pc,ls_shop_detail sd, ls_usr_detail ud where p.prod_id = pc.prod_id and sd.shop_id=p.shop_id and pc.ask_user_name=ud.user_name and reply_sts = 0;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getAuditProductCount() {
		String sql = "SELECT COUNT(1) FROM ls_prod WHERE STATUS = 3;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getAuditBrandCount() {
		String sql = "SELECT COUNT(1) FROM ls_brand WHERE STATUS = -1;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getReturnGoodsCount() {
		String sql = "SELECT COUNT(1) FROM ls_sub_refund_return WHERE apply_state = 2 AND apply_type=2;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getReturnMoneyCount() {
		String sql = "SELECT COUNT(1) FROM ls_sub_refund_return WHERE apply_state = 2 AND apply_type=1;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getAccusationCount() {
		String sql = "SELECT COUNT(1) FROM ls_accusation la,ls_prod lp,ls_accu_subject las,ls_shop_detail lsd, ls_usr_detail lud WHERE la.prod_id = lp.prod_id AND la.subject_id = las.as_id AND lsd.shop_id=lp.shop_id AND lud.user_id=la.user_id     AND la.status = 0 ORDER BY la.rec_date DESC;";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getShopBillCount() {
		String sql = "SELECT COUNT(1) FROM ls_shop_order_bill WHERE STATUS = 1";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getShopBillConfirmCount() {
		String sql = "SELECT COUNT(1) FROM ls_shop_order_bill WHERE STATUS = 2";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getAuctionCount() {
		String sql = "SELECT COUNT(1) FROM ls_auctions WHERE STATUS = -1 and end_time > now()";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getProductcommentCount() {
		String sql = "SELECT COUNT(1) FROM ls_prod_comm c INNER JOIN ls_sub_item subItem ON subItem.sub_item_id = c.sub_item_id INNER JOIN ls_shop_detail sd ON sd.user_name = c.owner_name LEFT JOIN ls_prod_add_comm ac ON ac.prod_comm_id = c.id WHERE 1 = 1 AND (c.status = 0 OR (c.status = 1 AND ac.status = 0))";

		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getSeckillActivityCount() {
		String sql = "SELECT COUNT(1) FROM ls_seckill_activity WHERE STATUS = -1 AND NOW() < end_time";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getGroupCount() {
		String sql = "SELECT COUNT(1) FROM ls_group WHERE STATUS = -1 AND end_time > NOW()";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getPresellProdCount() {
		//String sql ="SELECT COUNT(1) FROM ls_presell_prod WHERE STATUS = -1 ;";
		String sql = "SELECT COUNT(1) FROM ls_presell_prod pp,ls_prod p WHERE pp.prod_id = p.prod_id AND pp. STATUS = -1 AND (pp.pre_sale_start >= ? AND pp.pre_sale_end > ?)";
		Date date = new Date();
		return this.get(sql, Integer.class, date, date);
	}

	@Override
	public Integer getUserCommisCount() {
		String sql = "SELECT COUNT(1) FROM ls_user_commis WHERE promoter_sts = -2";
		return this.get(sql, Integer.class);
	}

	@Override
	public Integer getUserFeedBackCount() {
		String sql = "SELECT COUNT(1) FROM ls_usr_feedback WHERE STATUS = 0";
		return this.get(sql, Integer.class);
	}

	@Override
	@CacheEvict(value = "UnreadMsgCount", key = "#userName")
	public void clearUnreadMsgCount(String userName) {

	}

	@Override
	public int batchDelete(String msgIds) {
		String[] msgIdList = msgIds.split(",");
		StringBuffer buffer = new StringBuffer();
		buffer.append("DELETE FROM ls_msg WHERE msg_id IN(");
		for (String msgId : msgIdList) {
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length() - 1);
		buffer.append(")");
		return update(buffer.toString(), msgIdList);
	}

	@Override
	public Integer getUnReadMsgNum(String userName) {
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String querySQL = ConfigCode.getInstance().getCode("uc.msg.getUnReadMsgNum", map);
		return get(querySQL, Integer.class, userName);
	}

	@Override
	public PageSupport<SiteInformation> queryShopMessage(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(SiteInformation.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getInBoxCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.getInBox", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
}
