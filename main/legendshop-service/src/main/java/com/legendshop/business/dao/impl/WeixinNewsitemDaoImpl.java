/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinNewsitemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * 微信新闻项目Dao
 */
@Repository
public class WeixinNewsitemDaoImpl extends GenericDaoImpl<WeixinNewsitem, Long> implements WeixinNewsitemDao  {

	public WeixinNewsitem getWeixinNewsitem(Long id){
		return getById(id);
	}
	
    public int deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem){
    	return delete(weixinNewsitem);
    }
	
	public Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem){
		return save(weixinNewsitem);
	}
	
	public int updateWeixinNewsitem(WeixinNewsitem weixinNewsitem){
		return update(weixinNewsitem);
	}

	@Override
	public List<WeixinNewsitem> getNewsItemsByTempId(Long tempId) {
		return this.queryByProperties(new EntityCriterion().eq("newsTemplateid", tempId).addAscOrder("orders"));
	}

	@Override
	public void saveWeixinNewsitemList(List<WeixinNewsitem> addItems) {
		this.save(addItems);
	}

	@Override
	public void updateWeixinNewsitemList(List<WeixinNewsitem> updateItems) {
		this.update(updateItems);
	}

	@Override
	public void deleteWeixinNewsitemByIds(List<Long> ids) {
		this.deleteById(ids);
	}

	@Override
	public PageSupport<WeixinNewsitem> getWeixinNewsitemPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(WeixinNewsitem.class, curPageNO);
		return queryPage(cq);
	}
	
 }
