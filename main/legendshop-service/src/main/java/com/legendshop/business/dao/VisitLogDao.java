/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.text.ParseException;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.VisitLog;

/**
 * 访问历史Dao
 */
public interface VisitLogDao extends GenericDao<VisitLog, Long>{

	/**
	 * Update visit log.
	 * 
	 * @param visitLog
	 *            the visit log
	 */
	void updateVisitLog(VisitLog visitLog);

	/**
	 * Delete visit log by id.
	 * 
	 * @param id
	 *            the id
	 */
	void deleteVisitLogById(Long id);

	/**
	 * Gets the visited index log.
	 * 
	 * @param visitLog
	 *            the visit log
	 * @return the visited index log
	 */
	VisitLog getVisitedIndexLog(VisitLog visitLog);

	/**
	 * Gets the visited prod log.
	 * 
	 * @param visitLog
	 *            the visit log
	 * @return the visited prod log
	 */
	VisitLog getVisitedProdLog(VisitLog visitLog);

	VisitLog getVisitLogById(Long id);

	/**
	 * 根据用户名获取浏览历史列表
	 * @param curPageNO
	 * @param userName
	 * @param visitLog
	 * @return
	 * @throws ParseException
	 */
	PageSupport<VisitLog> getVisitLogListPage(String curPageNO, String userName, VisitLog visitLog) throws ParseException;

	PageSupport<VisitLog> getVisitLogListPage(String curPageNO, VisitLog visitLog, DataSortResult result,
			Integer pageSize);

	/**
	 * 根据用户名获取浏览历史列表(给app我的足迹用的, 只查询商品浏览历史, 按时间早晚排序, 最近30天的)
	 * @param curPageNO
	 * @param userName 用户名
	 * @return
	 */
	PageSupport<VisitLog> getVisitLogListPageForApp(String curPageNO, String userName);
	
	/**
	 * 根据用户名获取浏览历史统计数量
	 * @param userName
	 * @return
	 */
	Long getVisitLogCountByUserName(String userName);

	/**
	 * 更新用户浏览历史的IsUserDel字段
	 * @param userName
	 * @param isUserDel
	 * @return
	 */
	int updateIsUserDelBy(String userName, boolean isUserDel);
}
