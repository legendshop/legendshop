/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AccusationSubjectDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.spi.service.AccusationSubjectService;
import com.legendshop.util.AppUtils;

/**
 * The Class AccusationSubjectServiceImpl.
 */
@Service("accusationSubjectService")
public class AccusationSubjectServiceImpl  implements AccusationSubjectService{
	
	@Autowired
    private AccusationSubjectDao accusationSubjectDao;

	public AccusationSubject getAccusationSubject(Long id) {
        return accusationSubjectDao.getAccusationSubject(id);
    }

    public void deleteAccusationSubject(AccusationSubject accusationSubject) {
        accusationSubjectDao.deleteAccusationSubject(accusationSubject);
    }

    public Long saveAccusationSubject(AccusationSubject accusationSubject) {
        if (!AppUtils.isBlank(accusationSubject.getAsId())) {
            updateAccusationSubject(accusationSubject);
            return accusationSubject.getAsId();
        }
        return (Long) accusationSubjectDao.save(accusationSubject);
    }

    public void updateAccusationSubject(AccusationSubject accusationSubject) {
        accusationSubjectDao.updateAccusationSubject(accusationSubject);
    }

    public PageSupport getAccusationSubject(CriteriaQuery cq) {
        return accusationSubjectDao.queryPage(cq);
    }
}
