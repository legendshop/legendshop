/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopLayoutHotDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutHot;
import com.legendshop.spi.service.ShopLayoutHotService;
import com.legendshop.util.AppUtils;

/**
 *商家装修热点模块服务
 */
@Service("shopLayoutHotService")
public class ShopLayoutHotServiceImpl  implements ShopLayoutHotService{

	@Autowired
    private ShopLayoutHotDao shopLayoutHotDao;

    public ShopLayoutHot getShopLayoutHot(Long id) {
        return shopLayoutHotDao.getShopLayoutHot(id);
    }

    public void deleteShopLayoutHot(ShopLayoutHot shopLayoutHot) {
        shopLayoutHotDao.deleteShopLayoutHot(shopLayoutHot);
    }

    public Long saveShopLayoutHot(ShopLayoutHot shopLayoutHot) {
        if (!AppUtils.isBlank(shopLayoutHot.getId())) {
            updateShopLayoutHot(shopLayoutHot);
            return shopLayoutHot.getId();
        }
        return shopLayoutHotDao.saveShopLayoutHot(shopLayoutHot);
    }

    public void updateShopLayoutHot(ShopLayoutHot shopLayoutHot) {
        shopLayoutHotDao.updateShopLayoutHot(shopLayoutHot);
    }


	@Override
	public void update(String string, Object[] objects) {
		shopLayoutHotDao.update(string, objects);
	}

	@Override
	public void deleteShopLayoutHot(Long shopId, Long layoutId) {
		shopLayoutHotDao.deleteShopLayoutHot(shopId, layoutId);
		
	}
}
