/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopBillPeriodDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopBillPeriod;
import com.legendshop.util.AppUtils;

/**
 * 结算档期Dao
 * 
 */
@Repository
public class ShopBillPeriodDaoImpl extends GenericDaoImpl<ShopBillPeriod, Long> implements ShopBillPeriodDao  {

	public ShopBillPeriod getShopBillPeriod(Long id){
		return getById(id);
	}
	
    public int deleteShopBillPeriod(ShopBillPeriod shopBillPeriod){
    	return delete(shopBillPeriod);
    }
	
	public Long saveShopBillPeriod(ShopBillPeriod shopBillPeriod){
		return save(shopBillPeriod);
	}
	
	public int updateShopBillPeriod(ShopBillPeriod shopBillPeriod){
		return update(shopBillPeriod);
	}

	@Override
	public void saveShopBillPeriodWithId(ShopBillPeriod shopBillPeriod, Long id) {
		this.save(shopBillPeriod, id);
	}

	/**
	 * 生成实体Id
	 */
	@Override
	public Long getShopBillPeriodId() {
		 return this.createId();
	}

	@Override
	public PageSupport<ShopBillPeriod> getShopBillPeriod(String curPageNO, ShopBillPeriod shopBillPeriod) {
		CriteriaQuery cq = new CriteriaQuery(ShopBillPeriod.class, curPageNO);
		if(AppUtils.isNotBlank(shopBillPeriod.getFlag())) {			
			cq.like("flag","%" + shopBillPeriod.getFlag().trim() + "%");
		}
        cq.addDescOrder("recDate");
		return queryPage(cq);
	}
	
 }
