/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.Auctions;

/**
 *  拍卖活动Dao接口.
 */
public interface AuctionsDao extends GenericDao<Auctions, Long> {
     
    public abstract List<Auctions> getAuctionsBystatus(Long firstId,Long lastId,Long status);

	public abstract Auctions getAuctions(Long id);
	
    public abstract int deleteAuctions(Auctions auctions);
	
	public abstract Long saveAuctions(Auctions auctions);
	
	public abstract int updateAuctions(Auctions auctions);

	public abstract int finishAuction(Long id);

	public abstract List<Auctions> getonlineList();

	public abstract Long getMinId();

	public abstract Long getMaxId();

	public abstract boolean excludePresell(Long prodId, Long skuId);

	public abstract PageSupport<Auctions> queryAuctionListPage(String curPageNO, Auctions auctions);

	public abstract PageSupport<Auctions> queryAuctionListPage(String curPageNO, Long shopId, Auctions auctions);

	public abstract PageSupport<Auctions> queryAuctionListPage(String curPageNO, ProductSearchParms parms);

	public abstract PageSupport<Auctions> queryPageAuctionList(String curPageNO, ProductSearchParms parms);

	public abstract int updateAuctionStatus(Long auctionStatus,Long auctionId);

	public abstract boolean findIfJoinAuctions(Long productId);

	/**
	 * sku是否参加拍卖
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public abstract Auctions isAuction(Long prodId, Long skuId);

	/**
	 * 更新拍卖活动处理状态
	 * @param id
	 * @param flagStatus
	 */
	void updateAuctionsFlagStatus(Long id, int flagStatus);
}
