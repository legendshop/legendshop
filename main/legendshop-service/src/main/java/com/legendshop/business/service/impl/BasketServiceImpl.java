/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.BasketDao;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.store.Store;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.MarketingShopRuleEngine;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.StoreService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 购物车服务.
 */
@Service("basketService")
public class BasketServiceImpl implements BasketService {

	@Autowired
	private BasketDao basketDao;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private OrderService orderService;

	/** 规则引擎 **/
	@Resource(name="marketingShopRuleEngine")
	private MarketingShopRuleEngine marketingShopRuleEngine;

	@Autowired
	private CouponDao couponDao;
	
	@Autowired
	private StoreService storeService;
	

	@Override
	public void deleteBasketByUserId(String userId) {
		basketDao.deleteBasketByUserId(userId);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.business.service.BasketService#deleteBasketById(java.lang
	 * .Long)
	 */
	@Override
	public void deleteBasketById(Long id) {
		basketDao.deleteBasketById(id);
	}

	@Override
	public List<ShopCartItem> getShopCartByUserId(String userId){
		List<ShopCartItem> cartItems=basketDao.loadBasketByIds(userId);
		for(int i = 0; i < cartItems.size(); i++){
			ShopCartItem shopCartItem = cartItems.get(i);
			// 判断是否支付门店
			if(AppUtils.isNotBlank(shopCartItem.getProdId())&&AppUtils.isNotBlank(shopCartItem.getSkuId())){
				boolean supportStore = orderService.getStoreProdCount(shopCartItem.getProdId(), shopCartItem.getSkuId());
				shopCartItem.setSupportStore(supportStore);
			}
		}
		return cartItems;
	}

	@Override
	public Long getTotalBasketByUserId(String userId){
		return basketDao.getTotalBasketByUserId(userId);
	}

	/**
	 * 加入购物车.在购物车中的产品并不会影响库存量，如果在下单时库存不足，则会造成无法下单。
	 * @return
	 */
	@Override
	public Long saveToCart(String userId, Long prodId, Integer count, Long skuId,Long shopId,String distUserName,Long storeId) {
		return basketDao.saveToCart(userId,prodId, count, skuId,shopId,distUserName,storeId);
	}

	/**
	 * 根据商品和SKUID和用户ID找出对应的购物车
	 */
	@Override
	public Basket getBasketByUserId(Long prodId, String userId, Long sku_id) {
		return basketDao.getBasketByUserId(prodId, userId, sku_id);
	}
	
	@Override
	public void updateBasket(Basket basket) {
		basketDao.updateBasket(basket);
	}

	/**
	 * 购物车根据来源分组
	 */
	@Override
	public UserShopCartList getShoppingCarts(List<ShopCartItem> baskets) {
		if(AppUtils.isBlank(baskets)){
			return null;
		}
		Set<Long> cartsSet=new HashSet<Long>();//购物车的shopId集合
		int size=baskets.size();
		for (int i = 0; i < size; i++) {
			cartsSet.add(baskets.get(i).getShopId());
		}
		List <ShopCarts> shoppingCarts = new ArrayList<ShopCarts>();//按shopId来分

	    int orderTotalQuanlity=0; //订单总个数
		Double orderTotalWeight=0d;//订单的总重量
		Double orderTotalVolume=0d;//订单的总大小
		Double orderTotalCash=0d; //商家的商品金额总价
		Double orderActualTotal=0d; //商家的商品实际金额总价
	    double allDiscount=0;//优惠的折扣
		int supportCod=0; //是否支持货到付款

		for (Long shopId : cartsSet) {//按商家来分单
			//查询该商家的信息
			ShopDetail shopDetail=shopDetailDao.getShopDetailByShopId(shopId);
			long shopCouponCount = couponDao.getCouponCountByShopId(CouponProviderEnum.SHOP.value(),shopId.toString());
			if(shopDetail==null){
				continue;
			}
			ShopCarts carts  = new ShopCarts();
			if(shopDetail.getStatus().intValue()!=1){//检查商城状态
				carts.setShopStatus(0);
			}
			if(AppUtils.isNotBlank(shopCouponCount)) {
				carts.setShopCouponCount(shopCouponCount);
			}
			int shopTotalQuanlity=0;//商家订单总个数
			Double shopTotalWeight=0d;//订单的总重量
			Double shopTotalVolume=0d;//订单的总大小
			Double shopTotalCash=0d;//商家的商品金额
			Double shopActualTotalCash=0d;//商家的商品金额
			Double shopDiscountPrice=0d;
			List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>();
			for (int i = 0; i < baskets.size(); i++) {
				if(baskets.get(i).getShopId().equals(shopId)){
					ShopCartItem item=baskets.get(i);
					// 如果有一件商品不支持货到付款,则认为整个订单不支持
					if(!item.isSupportCod()){
						supportCod++;
					}

					// 如果商品项失效，则不统计h进订单金额
					if (item.getIsFailure()){
						item.setCheckSts(0);
					}
					Store store = storeService.getStore(item.getStoreId());
					// 不支持门店或者门店不存在
					if (AppUtils.isBlank(store) && AppUtils.isNotBlank(item.getStoreId()) && item.getStoreId()!=0){

						item.setSupportStore(false);
					// 	支持门店，但门店未上线
					}else if(AppUtils.isNotBlank(store) && !store.getIsDisable()){
						item.setSupportStore(false);
					}
					//TODO 运费修改
					/*if(!SupportTransportFreeEnum.SELLER_SUPPORT.value().equals(item.getTransportFree())) //商家承担运费等于免运费
						isFreePostage++;*/

					Double weight = item.getWeight();
					if(null != weight){
						item.setTotalWeight(Arith.mul(item.getWeight(), item.getBasketCount()));
					}
					Double volume = item.getVolume();
					if(null != volume){
						item.setTotalWeight(Arith.mul(volume, item.getBasketCount()));
					}
					double total=Arith.mul(item.getPrice(),item.getBasketCount());
					item.setTotal(total);
					item.setDiscountedPrice(total);

					double totalMoeny =Arith.mul(item.getPromotionPrice(),item.getBasketCount());  //商品总价格 (商品单价[限时折扣]*数量)
					totalMoeny=Arith.sub(totalMoeny, item.getDiscountPrice());
					item.setTotalMoeny(totalMoeny); //商品真实价格
					item.setProperties(item.getProperties());
					item.setCnProperties(item.getCnProperties());
					item.setIsFailure(item.getIsFailure()); //是否失效
					//门店自提
					item.setSupportStore(item.isSupportStore());
					cartItems.add(item);

					//商品项  选中状态 才计算价格
					if(item.getCheckSts()==1){
						shopTotalCash=Arith.add(item.getTotal(),shopTotalCash);
						shopActualTotalCash=Arith.add(totalMoeny,shopActualTotalCash);
						shopTotalQuanlity= item.getBasketCount()+shopTotalQuanlity;
						shopTotalWeight=Arith.add(item.getTotalWeight(),shopTotalWeight);
						shopTotalVolume=Arith.add(item.getTotalVolume(),shopTotalVolume);
						shopDiscountPrice=Arith.add(item.getDiscountPrice(),shopDiscountPrice);
					}
				}
			 }
			//TODO 运费修改
			 //carts.setFreePostage(isFreePostage==0); //用于该商家下所有的商品是否承担运费
			 //carts.setFreePostageByShop(isFreePostage==0);  //记录是商家设置了商品包邮。
			 carts.setTotalcount(shopTotalQuanlity);
			 carts.setShopTotalWeight(shopTotalWeight);
			 carts.setShopTotalVolume(shopTotalVolume);
			 carts.setShopTotalCash(shopTotalCash);
			 carts.setShopActualTotalCash(shopActualTotalCash);
			 carts.setDiscountPrice(shopDiscountPrice);
			 carts.setShopId(shopId);
			 carts.setShopName(shopDetail.getSiteName());

			 //根据商品价格对购物车进行排序，方便应用
			 Collections.sort(cartItems, new ShopCartItemComparator());
			 carts.setCartItems(cartItems);
			 carts.setStatus(shopDetail.getStatus());


			 /**
			 * 查询该店铺订单商品的参与的营销规则-->计算价格
			 * 秒杀、团购、预售和拍卖不计算营销活动
			 */
			 marketingShopRuleEngine.executorMarketing(carts);
			 shoppingCarts.add(carts);

			 orderTotalQuanlity=orderTotalQuanlity+shopTotalQuanlity;
			 orderTotalWeight=Arith.add(shopTotalWeight,orderTotalWeight);
			 orderTotalVolume=Arith.add(shopTotalVolume,orderTotalVolume);
			 orderTotalCash=Arith.add(orderTotalCash,shopTotalCash);
			 orderActualTotal=Arith.add(orderActualTotal, carts.getShopActualTotalCash());
			 allDiscount=Arith.add(carts.getDiscountPrice(),allDiscount);

		}

		UserShopCartList userShopCartList=new UserShopCartList();
		userShopCartList.setOrderTotalQuanlity(orderTotalQuanlity);
		userShopCartList.setOrderTotalWeight(orderTotalWeight);
		userShopCartList.setOrderTotalVolume(orderTotalVolume);
		userShopCartList.setOrderTotalCash(orderTotalCash);
		userShopCartList.setOrderActualTotal(orderActualTotal);
		userShopCartList.setAllDiscount(allDiscount);
		userShopCartList.setCod(supportCod==0); //如果等于0 所有这个订单的所有商品都支持货到付款
		userShopCartList.setShopCarts(shoppingCarts);
		
		//判断是否支持门店
		List<ShopCarts> carts = userShopCartList.getShopCarts();
		for (int i = 0; i < carts.size(); i++) {
			ShopCarts shopCarts=carts.get(i);
			List<MarketingDto> marketingDtos = shopCarts.getMarketingDtoList();
			int num=0;
			for (MarketingDto marketingDto : marketingDtos) {
				List<ShopCartItem> shopCartItems = marketingDto.getHitCartItems();
				if(AppUtils.isNotBlank(shopCartItems)){
					for (int j = 0; j < shopCartItems.size(); j++) {
						ShopCartItem cartItem=shopCartItems.get(j);
						// 判断是否支付门店
						boolean supportStore = orderService.getStoreProdCount(cartItem.getProdId(), cartItem.getSkuId());
						if(supportStore)
							num++;
						
						//如果门店ID不为空，则查询门店信息 TODO
						if (AppUtils.isNotBlank(cartItem.getStoreId())) {
							
							Store store = storeService.getStore(cartItem.getStoreId());
							if (AppUtils.isNotBlank(store)) {
								cartItem.setStoreName(store.getName());
								cartItem.setStoreAddr(store.getShopAddr());
							}
						}
						
						cartItem.setSupportStore(supportStore);
					}
				}
			}
			shopCarts.setSupportStore(num>0);
		}

		return userShopCartList;
	}

	@Override
	public void deleteBasketById(Long id, String userId) {
		basketDao.deleteBasketById(id,userId);
	}


	/**
	 * 查询团购的购物车信息
	 * 团购没有购物车，直接查找团购商品
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @return
	 */
	@Override
	public ShopCartItem findGroupCart(Long groupId, Long productId, Long skuId) {
		ShopCartItem cartItems= basketDao.findGroupCart(groupId,productId,skuId);
		return cartItems;
	}


	@Override
	public List<ShopCartItem> loadBasketByIds(List<Long> basketIds,String userId) {
		List<ShopCartItem> cartItems= basketDao.loadBasketByIds(basketIds,userId);
		return cartItems;
	}

	@Override
	public List<ShopCartItem> loadBasketByIds(List<Long> ids, String userId,
			Long shopId) {
		List<ShopCartItem> cartItems= basketDao.loadBasketByIds(ids,userId,shopId);
		return cartItems;
	}

	@Override
	public Basket getBasketById(Long id) {
		return basketDao.getBasketById(id);
	}

	@Override
	public List<ShopCartItem> getShopCartItems(List<Basket> baskets) {
		List<ShopCartItem> cartItems = null;
		if(AppUtils.isNotBlank(baskets)){
			Long [] skuIds=new Long[baskets.size()];
			for (int i = 0; i < baskets.size(); i++) {
				skuIds[i]=baskets.get(i).getSkuId();
			}
			cartItems=basketDao.loadBasketBySkuIds(skuIds);
			if(AppUtils.isBlank(cartItems))
				return null;
			for (ShopCartItem shopCartItem : cartItems) {
				Basket basket=fileBasket(baskets,shopCartItem.getProdId(),shopCartItem.getSkuId());
				if(basket!=null){
					shopCartItem.setBasketCount(basket.getBasketCount());
					shopCartItem.setCheckSts(basket.getCheckSts());
				}
			}
		}
		return cartItems;
	}


	private Basket fileBasket(List<Basket> baskets,Long prodId,Long skuId){
		for (Iterator<Basket> iterator = baskets.iterator(); iterator.hasNext();) {
			Basket basket = (Basket) iterator.next();
			if(prodId.equals(basket.getProdId()) && skuId.equals(basket.getSkuId())){
				return basket;
			}
		}
		return null;
	}

	/**
	 * 更新购物车数量
	 */
	@Override
	public void updateBasket(Long productId, Long skuId, Integer basketCount, String userId) {
		basketDao.updateBasket(productId,skuId,basketCount,userId);
	}


	@Override
	public int getBasketCountByUserId(String userId) {
		return basketDao.getBasketCountByUserId(userId);
	}
	@Override
	public void deleteById(List<Long> ids) {
		basketDao.deleteById(ids);
	}

	@Override
	public void deleteBasketById(Long prodId, Long skuId, String userId) {
		basketDao.deleteBasketById(prodId,skuId,userId);
	}

	@Override
	public ShopCartItem findSeckillCart(Long seckillId, Long prodId, Long skuId) {
		return basketDao.findSeckillCart(seckillId, prodId, skuId);
	}

	/**
	 * 通过商品Id删除购物车中的商品
	 */
	@Override
	public void deleteBasketGoodsByProdId(Long prodId){
		if(AppUtils.isBlank(prodId)){
			return;
		}
		//通过prodId获取购物中的商品
		List<Basket> prods = basketDao.getBasketProdsByProdId(prodId);
		if(AppUtils.isBlank(prods)){
			return;
		}
		//删除购物中对应的商品
		basketDao.delete(prods);
	}

	/**
	 * 改变  购物车项 的选中状态
	 */
	@Override
	public void updateBasketChkSts(Long basketId, String userId, int checkSts) {
		Basket basket = basketDao.getBasketById(basketId);
		if(basket!=null && basket.getUserId().equals(userId)){
			basket.setCheckSts(checkSts);
			basketDao.update(basket);
		}
	}

	/**
	 * 批量改变  购物车项 的选中状态
	 */
	@Override
	public void batchUpdateBasketChkSts(String userId, List<Basket> basketList) {
		List<Long> ids = new ArrayList<Long>();
		if(AppUtils.isNotBlank(basketList)){
			for (Basket basket : basketList) {
				ids.add(basket.getBasketId());
			}
		}

		if(AppUtils.isNotBlank(ids)){
			List<Basket> dbBasketList = basketDao.queryAllByIds(ids);
			if(AppUtils.isNotBlank(dbBasketList)){
				for (Basket dbBasket : dbBasketList) {
					if(dbBasket.getUserId().equals(userId)){
						for (Basket basket : basketList) {
							if(dbBasket.getBasketId().equals(basket.getBasketId())){
								dbBasket.setCheckSts(basket.getCheckSts());
								break;
							}
						}
					}
				}
				basketDao.update(dbBasketList);
			}
		}
	}

	/**
	 * 更新商品是否失效. 0: 否, 1:是
	 */
	@Override
	public int updateIsFailureByProdId(boolean isFailure, Long prodId) {
		return basketDao.updateIsFailureByProdId(isFailure, prodId);
	}

	@Override
	public void deleteBasketByIds(String selectedSkuId, String userId) {
		String[] skuIds = selectedSkuId.split(";");
		if(AppUtils.isNotBlank(skuIds)){
			basketDao.deleteBasketByIds(skuIds,userId);
		}
	}

	@Override
	public ShopCartItem findMergeGroupCart(Long mergeGroupId, Long productId, Long skuId) {
		ShopCartItem cartItems= basketDao.findMergeGroupCart(mergeGroupId,productId,skuId);
		return cartItems;
	}

	@Override
	public List<ShopCartItem> differInvalidList(List<ShopCartItem> cartItems) {
		if(AppUtils.isNotBlank(cartItems)){
			List<ShopCartItem> validCartItems = new ArrayList<ShopCartItem>();
			List<ShopCartItem> invalidCartItems = new ArrayList<ShopCartItem>();
			for (ShopCartItem cartItemDto : cartItems) {
				if(!cartItemDto.getIsFailure()){
					validCartItems.add(cartItemDto);
				}else{
					invalidCartItems.add(cartItemDto);
				}
			}
			return validCartItems;
		}
		return null;
	}

	@Override
	public void clearInvalidBaskets(String userId) {
		if(AppUtils.isNotBlank(userId)){
			basketDao.clearInvalidBaskets(userId);
		}
	}

	/**
	 *  更改购物车已选门店
	 */
	@Override
	public void updateShopStore(Long productId, Long skuId, Long storeId, String userId) {
		
		basketDao.updateShopStore(productId,skuId,storeId,userId);
	}

/*	@Override
	@CacheEvict(value = "UserShopCartList", key = "'UserShopCart_'+#type+'_'+#userId")
	public void deleteBasketByIds(String type, List<Long> skuIds, String userId) {
		
		basketDao.deleteBasketByIds(skuIds, userId);	
	}
*/

	/**
	 * 购物车比较器，价格是从小到大排序
	 */
	public class ShopCartItemComparator implements Comparator<ShopCartItem> {

		@Override
		public int compare(ShopCartItem o1, ShopCartItem o2) {
			if(o1 != null && o2 != null && o1.getPrice() != null && o1.getPrice() != null){
				if(o1.getPrice().doubleValue() > o2.getPrice().doubleValue()){
					return 1;
				}else if(o1.getPrice().doubleValue() == o2.getPrice().doubleValue()){
					return 0;
				}else{
					return  -1;
				}
			}
			return  -1;
		}
	}


}
