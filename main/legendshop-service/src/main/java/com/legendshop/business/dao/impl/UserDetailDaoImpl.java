/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.UserDetailIdNameUpdate;
import com.legendshop.base.cache.UserDetailUpdate;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.helper.FileProcessor;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.business.dao.BasketDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.UserManagerDao;
import com.legendshop.business.dao.UserRoleDao;
import com.legendshop.business.dao.UserSecurityDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.RegisterEnum;
import com.legendshop.model.constant.RoleEnum;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserRole;
import com.legendshop.model.securityCenter.UserInformation;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.CommonUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * 
 * 用户详情Dao
 * 
 */
@Repository("userDetailDao")
public class UserDetailDaoImpl extends GenericDaoImpl<UserDetail, String> implements UserDetailDao {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(UserDetailDaoImpl.class);

	/** The common util. */
	@Autowired
	private CommonUtil commonUtil;

	@Autowired
	private SubDao subDao;

	// 购物车Dao
	@Autowired
	private BasketDao basketDao;


	@Autowired
	private UserSecurityDao userSecurityDao;

	@Autowired
	private UserManagerDao userManagerDao;

	@Autowired
	private UserRoleDao userRoleDao;

	// 缩略图列表
	@Resource(name="scaleList")
	private Map<String, List<Integer>> scaleList;

	@Autowired
	private MailManager mailManager;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
    /** 系统配置. */
    @Autowired
    private PropertiesUtil propertiesUtil;
	
	
	/** The REGISTE d_ tag. */
	private final String REGISTED_TAG = "REGISTED";

	private static String getUserInfoSQL = "select d.nick_name as nickName,d.user_name as userName,d.user_mail as userMail,d.pay_password as payPassword,d.user_mobile as userMobile,s.sec_level as secLevel,s.pay_strength as payStrength, "
			+ "s.mail_verifn as mailVerifn,s.phone_verifn as phoneVerifn,s.paypass_verifn as paypassVerifn from ls_usr_security s,ls_usr_detail d,ls_user u "
			+ "where u.enabled = '1' and s.user_name = d.user_name and d.user_name = u.name ";

	private static String getUserInfoByNickNameSQL = getUserInfoSQL + " and d.nick_name = ?";

	private static String getUserInfoByUserNameSQL = getUserInfoSQL + " and d.user_name = ?";

	private static String getUserInfoByPhoneSQL = "select d.nick_name as nickName,d.user_name as userName,d.user_mail as userMail,d.pay_password as payPassword,d.user_mobile as userMobile,s.sec_level as secLevel,s.pay_strength as payStrength, "
			+ "s.mail_verifn as mailVerifn,s.phone_verifn as phoneVerifn,s.paypass_verifn as paypassVerifn from ls_usr_security s,ls_usr_detail d,ls_user u "
			+ "where u.enabled = '1' and s.user_name = d.user_name and d.user_name = u.name and d.user_mobile = ?";

	private static String getUserInfoByEmailSQL = "select d.nick_name as nickName,d.user_name as userName,d.user_mail as userMail,d.pay_password as payPassword,d.user_mobile as userMobile,s.sec_level as secLevel,s.pay_strength as payStrength, "
			+ "s.mail_verifn as mailVerifn,s.phone_verifn as phoneVerifn,s.paypass_verifn as paypassVerifn from ls_usr_security s,ls_usr_detail d,ls_user u "
			+ "where u.enabled = '1' and s.user_name = d.user_name and d.user_name = u.name and d.user_mail = ?";

	private static String sqlForFindUserByMobileCode = "SELECT count(1) FROM ls_sms_log WHERE user_phone = ? and mobile_code = ?  AND TYPE = 2 AND status=1 AND rec_date > ? ORDER BY id DESC LIMIT 0,1";
	/**
	 * 普通用户注册
	 * 
	 */
	@Override
	public User saveUser(User user, UserDetail userDetail) {
		// 1. 如果需要邮箱验证，则使客户失效先
		if (systemParameterUtil.isValidationFromMail()) {
			user.setEnabled(Constants.USER_STATUS_DISABLE);// 不可用
			userDetail.setRegisterCode(MD5Util.toMD5(user.getPassword())); // 将RegisterCode发送到邮件，点击开通用户
		}
		String userId = userManagerDao.saveUser(user);
		user.setId(userId);
		userDetail.setUserId(userId);
		userDetail.setUserName(user.getName());
		// 2保存用户详细信息和商家信息
		saveUerDetail(userDetail);
		return user;
	}

	

	// 普通用户信息修改
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#updateUser(com.legendshop
	 * .model.entity.UserDetail)
	 */
	@Override
	@UserDetailUpdate
	public void updateUser(UserDetail userDetail) {
		this.update(userDetail);
	}

	/**
	 * 用户是否存在
	 */
	@Override
	public boolean isUserExist(String userName) {
		return userManagerDao.isUserExists(userName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#isEmailExist(java.lang
	 * .String)
	 */
	@Override
	public boolean isEmailExist(String email) {
		return AppUtils.isNotBlank(query("select 1  from ls_usr_detail where user_mail = ?", Integer.class, email));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#findUser(java.lang.String)
	 */
	@Override
	public User getUser(String userId) {
		return userManagerDao.getUser(userId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#findUserDetailByName(java
	 * .lang.String)
	 */
	@Override
	public UserDetail getUserDetailByName(String userName) {
		return this.getByProperties(new EntityCriterion().eq("userName", userName));
	}

	/**
	 * 用户最后登录时间
	 */
	@Override
	public Date getLastLoginTime(String userId) {
		return this.getJdbcTemplate().queryForObject("select user_lasttime,user_regtime from ls_usr_detail where user_id = ?", new Object[] { userId },
				new RowMapper<Date>() {
					@Override
					public Date mapRow(ResultSet rs, int rowNum) throws SQLException {
						Date lastLoginTime = rs.getDate("user_lasttime");
						if (lastLoginTime != null) {
							return lastLoginTime;
						} else {
							return rs.getDate("user_regtime"); // 用户注册时间
						}
					}
				});
	}

	@Override
	public void updateLastLoginTime(String userId) {
		super.update("update ls_usr_detail set user_lasttime=? where user_id = ? ", new Object[] { new Date(), userId });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#userRegSuccess(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public RegisterEnum getUserRegStatus(String userName, String registerCode) {
		UserDetail userDetail = getUserDetailByName(userName);
		if (userDetail == null) {
			return RegisterEnum.REGISTER_NO_USER_FOUND;
		}
		if (registerCode != null && registerCode.equals(userDetail.getRegisterCode())) {
			// update user
			User user = getUser(userDetail.getUserId());
			user.setEnabled("1");
			userManagerDao.updateUser(user);
			// update userDetail
			userDetail.setRegisterCode(REGISTED_TAG);
			update(userDetail);
			return RegisterEnum.REGISTER_SUCCESS;
		}
		if (REGISTED_TAG.equals(userDetail.getRegisterCode())) {
			return RegisterEnum.REGISTER_SUCCESS;
		}
		return RegisterEnum.REGISTER_FAIL;
	}

	@Override
	public String getUserNameByMail(String email) {
		return super.get("select user_name from ls_usr_detail where user_mail = ?", String.class, email);
	}

	@Override
	public String getShopUserNameByMail(String email) {
		return super.get(
				"SELECT u.user_name FROM ls_usr_detail u LEFT JOIN ls_shop_detail d ON u.shop_id=d.shop_id WHERE u.user_mail = ? AND u.shop_id IS NOT NULL AND d.status ='1'",
				String.class, email);
	}

	@Override
	public String getUserNameByPhone(String phone) {
		return super.get("select user_name from ls_usr_detail where user_mobile = ?", String.class, phone);
	}

	@Override
	public String getShopUserNameByPhone(String email) {
		return super.get(
				"SELECT u.user_name FROM ls_usr_detail u LEFT JOIN ls_shop_detail d ON u.shop_id=d.shop_id WHERE u.user_mobile = ? AND u.shop_id IS NOT NULL AND d.status ='1'",
				String.class, email);
	}

	@Override
	public String getUserNameByNickName(String nickName) {
		return super.get("select user_name from ls_usr_detail where nick_name = ?", String.class, nickName);
	}

	@Override
	public String getShopUserNameByNickName(String email) {
		return super.get(
				"SELECT u.user_name FROM ls_usr_detail u LEFT JOIN ls_shop_detail d ON u.shop_id=d.shop_id WHERE u.nick_name = ? AND u.shop_id IS NOT NULL AND d.status ='1'",
				String.class, email);
	}

	@Override
	public String getUserNameByUserId(String UserId) {
		return super.get("select user_name from ls_usr_detail where user_id = ?", String.class, UserId);
	}

	@Override
	public String getUserNameByOpenId(String openId, String type) {
		return super.get("select u.user_name from ls_usr_detail u, ls_passport p where u.user_id = p.user_id and p.type= ? and p.open_id  = ?", String.class,
				type, openId);
	}

	@Override
	public String getUserIdByOpenId(String openId, String type) {
		return super.get("select u.user_id from ls_usr_detail u, ls_passport p where u.user_id = p.user_id and p.type= ? and p.open_id  = ?", String.class,
				type, openId);
	}

	/**
	 * 根据用户输入，把可能的邮件或者手机号码找到对应的用户名称，昵称一定要有一个英文符号
	 */
	@Override
	public String convertUserLoginName(String name) {
		if (ValidationUtil.checkEmail(name)) {
			return this.getUserNameByMail(name);
		} else if (ValidationUtil.checkPhone(name)) {
			return this.getUserNameByPhone(name);
		} else {
			// TODO 为什么昵称可以登录用户 奇葩->Tony
			// 限制由用户昵称登录，只能由用户名登录，或由手机邮箱登录，2018-2-12 Jason Wu
//			return this.getUserNameByNickName(name);
			return name;
		}
	}

	/**
	 * 根据用户输入，把可能的邮件或者手机号码找到对应的用户名称，昵称一定要有一个英文符号
	 */
	@Override
	public String convertShopUserLoginName(String name) {
		if (ValidationUtil.checkEmail(name)) {
			return this.getShopUserNameByMail(name);
		} else if (ValidationUtil.checkPhone(name)) {
			return this.getShopUserNameByPhone(name);
		} else {
			//return this.getShopUserNameByNickName(name);
			return name;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.UserDetailDao#findUserScore(java.lang
	 * .String)
	 */
	@Override
	public Integer getUserScore(String userId) {
		return get("select score from ls_usr_detail where user_id=?", Integer.class, userId);
	}

	/**
	 * 通过手机号码查找用户详情
	 * 
	 * @param mobile
	 * @return
	 */
	@Override
	public UserDetail getUserDetailByMobile(String mobile) {
		return get("select * from ls_usr_detail where user_mobile=?", UserDetail.class, mobile);
	}

	/**
	 * 删除用户所有信息，如果该用户是一个商家也同时干掉商家部分的所有信息
	 */
	@Override
	public String deleteUserDetail(String userId, String userName) {

		User user = getUser(userId);

		if (user == null) {
			log.debug("删除用户{}不存在", userName);
			return "删除用户" + userName + "不存在";
		}

		if (!user.getName().equals(userName)) {
			log.debug("删除用户" + userName + "失败， ID错误");
			return "删除用户" + userName + "失败， ID错误";
		}

		// 检查是否超级管理员等不能删除的角色，出于对系统的保护，敏感数据删除不支持UI操作
		List<UserRole> userRoleList = userRoleDao.getUserRoleByUserId(userId);
		boolean isAdmin = false;
		for (UserRole role : userRoleList) {
			if (role.getId().getRoleId().equals(RoleEnum.ROLE_SUPERVISOR.value())) {
				isAdmin = true;
				break;
			}

		}
		if (isAdmin) {
			return "不能删除管理员用户，请先备份好数据和去掉该用户的权限再试！";
		}
		/////////// 删除用户部分//////////////
		// 如果已经有有效订单产生则不能删除
		if (subDao.isUserOrderProduct(userName)) {
			return "请先删除用户的有效订单！";
		}
		
		//如果用户还有预存款提现申请，则不能删除
		UserDetail userDetail = this.getUserDetailByIdNoCache(userId);
		if(userDetail.getFreezePredeposit()>0){
			
			return "请先处理该用户的预存款提现申请！";
		}

		//如果用户还有预存款，不能删除
		if(userDetail.getAvailablePredeposit()>0){
			
			return "该用户还有预存款，不能删除！";
		}

		// 用户的购物车
		basketDao.deleteUserBasket(userId);

		// 用户所有订单
		subDao.deleteSub(userId, userName);
		// 用户的商品收藏
		deleteUserItem("ls_favorite", userName);
		// 用户积分
		deleteUserItem("ls_score", userName);

		// 删除退换货记录
		update("DELETE FROM ls_sub_refund_return WHERE user_name = ?", userName);
		/*
		 * update(
		 * "delete from ls_prod_return_item where prod_return_id in (SELECT return_id FROM ls_prod_return WHERE user_name = ?)"
		 * , userName); //update(
		 * "delete from ls_prod_return_log where prod_return_id in (SELECT return_id FROM ls_prod_return WHERE user_name = ?)"
		 * , userName); update(
		 * "delete from ls_prod_return_operation_log where prod_return_id in (SELECT return_id FROM ls_prod_return WHERE user_name = ?)"
		 * , userName); deleteUserItem("ls_prod_return", userName);//方法已过期
		 */

		// 删除评论
		update("delete from ls_prod_comm where user_id = ?", userId);

		// 删除咨询
		update("delete from ls_prod_cons where ask_user_name = ?", userName);

		// 短消息
		deleteUserItem("ls_usr_comm", userName);

		// 删除消息
		update("delete from ls_msg_text where id in (select text_id from ls_msg where receive_name = ?)", userName);
		update("delete from ls_msg_usrgrad where msg_id in (select msg_id from ls_msg where receive_name = ?)", userName);
		update("delete from ls_msg_status where user_name = ?", userName);
		update("delete from ls_msg where receive_name = ?", userName);

		// 删除举报
		deleteUserItem("ls_accusation", userName);

		// 删除安全信息
		deleteUserItem("ls_usr_security", userName);

		// 删除第三方登录
		update("delete from ls_passport_sub where passport_id in (select pass_port_id from ls_passport where user_id = ?)", userId);
		update("delete from ls_passport where user_id = ?", userId);

		// 删除收货地址
		update("delete from ls_usr_addr where user_id = ?", userId);

		// 删除App登录历史
		update("delete from ls_app_token where user_name = ?", userName);

		// 删除用户角色
		userRoleDao.deleteUserRole(userRoleList);
		
		// 删除用户详细信息
		deleteById(userId);
		
		// 删除用户基本信息
		userManagerDao.deleteUser(user);
		log.debug("删除用户 {},  {} 成功", userId, userName);
		return null;
	}

	/**
	 * 根据用户删除用户信息
	 * @param tableName 表名
	 * @param userName 用户名
	 */
	private void deleteUserItem(String tableName, String userName) {
		update("delete from " + tableName + " where user_name = ?", userName);
	}

	

	/**
	 * 重置密码
	 */
	@Override
	public boolean updatePassword(String userName, String userMail, String password) {
		UserDetail userDetail = this.getByProperties(new EntityCriterion().eq("userName", userName).eq("userMail", userMail));
		if (userDetail == null) {
			return false;
		}
		// update password
		User user = userManagerDao.getUser(userDetail.getUserId());
		user.setPassword(password);
		userManagerDao.updateUser(user);

		// 将密码发送给邮箱
		mailManager.resetPassword(userDetail, userMail);

		return true;
	}

	@Override
	public Long getAllUserCount() {
		return getCount();
	}

	@Override
	public void updateUser(User user) {
		userManagerDao.updateUser(user);

	}

	@Override
	public UserInformation getUserInfo(String userName) {
		return get(getUserInfoByUserNameSQL, UserInformation.class, userName);
	}

	@Override
	public String getUserPassword(String UserId) {
		return getJdbcTemplate().queryForObject("select password from ls_user where enabled = 1 and id = ?", String.class, UserId);
	}

	@Override
	public String getUserEmail(String userId) {
		String userEmail = get("select user_mail from ls_usr_detail where user_id = ?", String.class, userId);
		return userEmail;
	}

	@Override
	public String getUserMobile(String userId) {
		return get("select user_mobile from ls_usr_detail where user_id = ?", String.class, userId);
	}

	@Override
	@UserDetailUpdate
	public int updateUserDetail(UserDetail userDetail) {
		log.info("udpate userDetail  by Id {}", userDetail.getUserId());
		return this.update(userDetail);
	}

	@Override
	@UserDetailUpdate
	public int updateUserDetailEmail(UserDetail userDetail) {
		log.info("udpate userDetail  by Id {}", userDetail.getUserId());
		return super.updateProperties(userDetail, "userMail");
	}

	@Override
	public boolean isPhoneExist(String phone) {
		List list = query("select 1  from ls_usr_detail where user_mobile = ?", Integer.class, phone);
		return AppUtils.isNotBlank(list);
	}

	@Override
	public UserInformation getUserInfoByPhone(String Phone) {
		return get(getUserInfoByPhoneSQL, UserInformation.class, Phone);
	}

	@Override
	public UserInformation getUserInfoByMail(String Email) {
		return get(getUserInfoByEmailSQL, UserInformation.class, Email);
	}

	@Override
	public boolean verifySMSCode(String userName, String code) {
		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}
		if (AppUtils.isBlank(code)) {
			return false;
		}
		String validatecode = getJdbcTemplate().queryForObject("select validate_code from ls_usr_security where user_name = ?", String.class, userName);
		return code.equals(validatecode);
	}

	@Override
	public boolean isEmailOnly(String email, String userName) {
		List<Integer> list = query("select 1  from ls_usr_detail where user_mail = ? and user_name != ?", Integer.class, email, userName);
		return AppUtils.isNotBlank(list);
	}

	@Override
	public boolean isPhoneOnly(String phone, String userName) {
		List<Integer> list = query("select 1  from ls_usr_detail where user_mobile = ? and user_name != ?", Integer.class, phone, userName);
		return AppUtils.isNotBlank(list);
	}

	@Override
	public Integer getMailVerifn(String userName) {
		Integer mailVerifn = getJdbcTemplate().queryForObject("select mail_verifn from ls_usr_security where user_name = ? ", new Object[] { userName },
				Integer.class);
		return mailVerifn;
	}

	@Override
	public Integer getPhoneVerifn(String userName) {
		Integer phoneVerifn = getJdbcTemplate().queryForObject("select phone_verifn from ls_usr_security where user_name = ? ", new Object[] { userName },
				Integer.class);
		return phoneVerifn;
	}

	@Override
	public Integer getPaypassVerifn(String userName) {
		Integer paypassVerifn = getJdbcTemplate().queryForObject("select paypass_verifn from ls_usr_security where user_name = ? ", new Object[] { userName },
				Integer.class);
		return paypassVerifn;
	}

	@Override
	public Boolean updateNewPassword(String userName, String newPassword) {
		Integer result = update("update ls_user set password = ? where name = ? ;", newPassword, userName);
		if (result == 1) {
			return true;
		} else {
			return false;
		}
	}

	// @Override
	// public void updateSecurityMail(String userName, String validateCode,
	// String userEmail) {
	// update("update ls_usr_security set email_code = ? , update_mail = ? ,
	// send_mail_time = ?,times = ? where user_name =
	// ?",validateCode,userEmail,new Date(),0,userName);
	// }

	// /**
	// * 更新已验证邮箱
	// * @param userEmail
	// * @param userName
	// */
	// @Override
	// public void updateUserEmail(String userEmail, String userName) {
	// update("update ls_usr_detail set user_mail = ? where user_name =
	// ?",userEmail,userName);
	// }

	@Override
	public String getSecurityEmail(String userName) {
		return getJdbcTemplate().queryForObject("select update_mail from ls_usr_security where user_name = ?", String.class, userName);
	}

	// @Override
	// public void updatetimes(Integer number,String userName) {
	// update("update ls_usr_security set times = ? where user_name =
	// ?",number,userName);
	// }

	// @Override
	// public void updateUserMobile(String userMobile, String userName) {
	// update("update ls_usr_detail set user_mobile = ? where user_name =
	// ?",userMobile, userName);
	// }

	@Override
	public Integer getSecLevel(String userName) {
		return getJdbcTemplate().queryForObject("select sec_level from ls_usr_security where user_name = ?", Integer.class, userName);
	}

	@Override
	public boolean isNickNameExist(String nickName) {
		return AppUtils.isNotBlank(query("select 1  from ls_usr_detail where user_name = ?", Integer.class, nickName));
	}

	@Override
	public String getNickNameByOpenId(String openId, String type) {
		return super.get("select u.nick_name from ls_usr_detail u, ls_passport p where u.user_id = p.user_id and p.type= ? and p.open_id  = ?", String.class,
				type, openId);
	}

	@Override
	public User getUserByMobile(String userMobile) {
		if(AppUtils.isBlank(userMobile)) {
			return null;
		}
		return get("select u.*,d.nick_name as nickName,d.shop_id as shopId from ls_user u,ls_usr_detail d where u.id = d.user_id and d.user_mobile = ?", User.class, userMobile);
	}

	@Override
	public User getUserByWxOpenId(String wxOpenId) {
		if(AppUtils.isBlank(wxOpenId)) {
			return null;
		}
		return get("select u.* from ls_user u,ls_usr_detail d where u.id = d.user_id and u.open_id = ?", User.class, wxOpenId);
	}

	@Override
	public UserInformation getUserInfoByNickName(String nickName) {
		if(AppUtils.isBlank(nickName)) {
			return null;
		}
		return get(getUserInfoByNickNameSQL, UserInformation.class, nickName);
	}

	private void saveUerDetail(UserDetail userDetail) {
		// 保存用户角色
		commonUtil.saveUserRight(userDetail.getUserId());
		userSecurityDao.saveDefaultUserSecurity(userDetail.getUserName());
		save(userDetail);
	}

	/**
	 * 删除各个等级的缩略图
	 * 
	 * @param userName
	 */
	private void deleteSmallImage(String userName) {
		for (String sacle : scaleList.keySet()) {
			StringBuilder sb = new StringBuilder(propertiesUtil.getSmallPicPath());
			sb.append("/").append(sacle).append("/").append(userName);
			FileProcessor.deleteDirectory(new File(sb.toString()));
		}

	}

	@Override
	@Cacheable(value = "UserDetail", key = "#userId")
	public UserDetail getUserDetailById(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}



	@Override
	public UserDetail getUserDetailByIdNoCache(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}

	@Override
	public User getUserByName(String userName) {
		return userManagerDao.getUserByName(userName);
	}

	@Override
	public boolean isSiteNameExist(String siteName) {
		return AppUtils.isBlank(query("SELECT 1  FROM ls_shop_detail d WHERE site_name=?", Integer.class, siteName));
	}

	/**
	 * for update predeposit
	 */
	@Override
	public UserDetail getUserDetailByidForUpdate(String userId) {
		return get(
				"select user_id as userId,user_name as userName,available_predeposit as availablePredeposit,freeze_predeposit as freezePredeposit,score as score,user_coin as userCoin from ls_usr_detail where user_id=?  for update ",
				UserDetail.class, userId);
	}

	@Override
	@UserDetailIdNameUpdate
	public int updatePredeposit(Double updatePredeposit, Double originalPredeposit, Double updatefreePredeposit, Double originalFreePredeposit, String userId) {
		int result = update(
				"update ls_usr_detail set available_predeposit=?,freeze_predeposit=? where  available_predeposit=? and freeze_predeposit=? and user_id=?  ",
				new Object[] { updatePredeposit, updatefreePredeposit, originalPredeposit, originalFreePredeposit, userId });
		return result;
	}

	/**
	 * 更新用户的预存款
	 */
	@Override
	@UserDetailIdNameUpdate
	public int updateAvailablePredeposit(Double updatePredeposit, Double originalPredeposit, String userId) {
		int result = update("update ls_usr_detail set available_predeposit=? where user_id=? and available_predeposit=? ",
				new Object[] { updatePredeposit, userId, originalPredeposit });
		return result;
	}

	/**
	 * 更新用户的金币
	 */
	@Override
	@UserDetailIdNameUpdate
	public int updateUserCoin(Double updateCoin, Double originalCoin, String userId) {
		int result = update("update ls_usr_detail set user_coin=? where user_id=? and user_coin=? ", new Object[] { updateCoin, userId, originalCoin });
		return result;
	}

	/**
	 * 更新用户的冻结的预存款
	 */
	@Override
	@UserDetailIdNameUpdate
	public int updateFreezePredeposit(Double updatePredeposit, Double originalPredeposit, String userId) {
		int result = update("update ls_usr_detail set freeze_predeposit=? where user_id=? and freeze_predeposit=? ",
				new Object[] { updatePredeposit, userId, originalPredeposit });
		return result;
	}

	/**
	 * 用户积分冲值
	 */
	@Override
	public UserDetail getUserDetailForScoreUser(String nickName) {
		UserDetail detail = this.getByProperties(new EntityCriterion().eq("nickName", nickName));
		if (AppUtils.isNotBlank(detail)) {
			return detail;
		}
		if (AppUtils.isBlank(detail)) {
			detail = this.getByProperties(new EntityCriterion().eq("userMobile", nickName));
		}
		return detail;
	}

	/**
	 * 检查微信是否已经绑定该手机号码
	 */
	@Override
	public boolean checkBinding(String mobile) {
		int result = get("SELECT COUNT(*) FROM ls_usr_detail ud, ls_passport p WHERE ud.user_id = p.binding_user_id AND ud.user_mobile = ?", Integer.class,
				mobile);
		return result > 0;
	}

	/**
	 * 减去用户积分
	 */
	@Override
	@UserDetailIdNameUpdate
	public int updateScore(Integer updateSore, Integer oldScore, String userId) {
		String updateSql = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".updateScore");
		int result = update(updateSql, new Object[] { updateSore, oldScore, updateSore, userId });
		return result;
	}

	/**
	 * 充值积分
	 * 
	 * @param updateSore
	 * @param userId
	 * @return
	 */
	@Override
	@UserDetailIdNameUpdate
	public int updateScore(Integer updateSore, String userId) {
		int result = update("update ls_usr_detail set score = score + ? where user_id=? ", new Object[] { updateSore, userId });
		return result;
	}

	@UserDetailIdNameUpdate
	public void updateShopId(Long shopId, String userId) {
		this.update("update ls_usr_detail set shop_id = ? where user_id = ?", shopId, userId);
	}

	@Override
	@UserDetailIdNameUpdate
	public void updateUserGrade(String userId, Integer userGradeId, Integer grouthValue) {
		this.update("update ls_usr_detail set grade_id =?, grouth_value = ?, upgrade_date=? where user_id=?", userGradeId, grouthValue, new Date(), userId);
	}

	@Override
	public UserDetail getUserDetailByAll(String nickName) {
		EntityCriterion criterion = new EntityCriterion();
		criterion.or(Restrictions.eq("nickName", nickName), Restrictions.eq("userMobile", nickName));
		return this.getByProperties(criterion);
	}

	@Override
	public String getuserIdByShopId(Long shopId) {
		return super.get("SELECT user_id FROM ls_usr_detail WHERE shop_id=?", String.class, shopId);
	}

	@Override
	public Double getAvailablePredeposit(String userId) {
		return super.get("SELECT available_predeposit FROM ls_usr_detail WHERE user_id=?", Double.class, userId);
	}

	/*
	 * 根据店铺ID获取UserDetail
	 */
	public UserDetail getUserDetailByShopId(Long shopId) {
		return get(
				"SELECT u.id AS userId,u.name AS userName,u.password AS PASSWORD,ud.* FROM ls_user u,ls_usr_detail ud,ls_shop_detail ls WHERE u.id=ud.user_id AND ud.user_id=ls.user_id AND ls.shop_id= ?",
				UserDetail.class, shopId);
	}

	@Override
	public Double getUserCoin(String userId) {
		return super.get("SELECT user_coin FROM ls_usr_detail WHERE user_id=?", Double.class, userId);
	}

	@Override
	public boolean checkPromoter(String userId) {
		return getLongResult("SELECT count(1) FROM ls_user_commis WHERE user_id=? and  promoter_sts =1", userId) > 0;
	}

	@Override
	public String getNickNameByUserName(String userName) {

		String sql = "SELECT nick_name FROM ls_usr_detail WHERE user_name = ?";

		return super.get(sql, String.class, userName);
	}

	@Override
	public String getNickNameByUserId(String userId) {
		String sql = "SELECT nick_name FROM ls_usr_detail WHERE user_id = ?";

		return super.get(sql, String.class, userId);
	}
	
	@Override
	public Long getShopIdByUserId(String userId) {
		String sql = "SELECT shop_id FROM ls_usr_detail WHERE user_id = ?";
		return super.get(sql, Long.class, userId);
	}

	@Override
	@UserDetailIdNameUpdate
	public Integer upIsRegIM(String userId, Integer param) {
		String sql = "UPDATE ls_usr_detail SET is_reg_im = ? WHERE user_id = ?";
		return super.update(sql, param, userId);
	}

	@Override
	public boolean isExitMail(String mail) {
		String sql = "select count(1) from ls_usr_detail where user_mail=?";
		Long falg = this.getLongResult(sql, mail);
		if (falg > 0) { // 存在，既不能再注册
			return false;
		} else {
			return true;
		}

	}

	@Override
	public PageSupport<UserDetail> getUserDetailListPage(String haveShop, DataSortResult result, String curPageNO, UserDetail userDetail) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hqlQuery = new SimpleSqlQuery(UserDetail.class, 30, curPageNO);

		if (AppUtils.isNotBlank(userDetail.getUserName())) {
			map.like("userName", userDetail.getUserName().trim());
		}

		if (AppUtils.isNotBlank(userDetail.getUserMail())) {
			map.like("userMail", userDetail.getUserMail().trim());
		}

		if (AppUtils.isNotBlank(userDetail.getRealName())) {
			map.like("realName", userDetail.getRealName().trim() );
		}

		if (AppUtils.isNotBlank(userDetail.getNickName())) {
			map.like("nickName", userDetail.getNickName().trim());
		}

		if (AppUtils.isNotBlank(userDetail.getUserMobile())) {
			map.like("userMobile", userDetail.getUserMobile().trim());
		}

		if (AppUtils.isNotBlank(userDetail.getEnabled())){
			map.put("enabled", userDetail.getEnabled());
		}

		if ("1".equals(haveShop)) {
			map.put("haveShop", "and u.shop_id is not null and u.status in (-3, 0, 1)");
		} else if ("0".equals(haveShop)) {
			map.put("haveShop", "and (u.shop_id is null or ( u.shop_id is not null and u.status in (-3, 0, -1))) ");
		}

		if (!result.isSortExternal()) {
			map.put(Constants.ORDER_INDICATOR, "order by u.user_regtime desc");
		}else{
			map.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		String totalUserDetail = ConfigCode.getInstance().getCode("biz.QueryUserDetailCount", map);
		String userDetailSQL = ConfigCode.getInstance().getCode("biz.QueryUserDetail", map);
		hqlQuery.setAllCountString(totalUserDetail);
		hqlQuery.setQueryString(userDetailSQL);
		map.remove(Constants.ORDER_INDICATOR);
		map.remove("haveShop");
		hqlQuery.setParam(map.toArray());
		return querySimplePage(hqlQuery);
	}

	@Override
	public User getByOpenId(String openId) {
		String sql = "select id as id,name as name,password as password,enabled as enabled,note as note,dept_id as deptId,open_id as openId from ls_user where open_id=? ";
		return get(sql,User.class,openId);
		
	}

	@Override
	public void bingUserOpenId(String userId, String openId) {
		String sql ="UPDATE ls_user SET open_id='' WHERE ls_user.open_id=?  ";		
		update(sql, openId);
	    sql ="UPDATE ls_user SET open_id=? WHERE ls_user.id=?  ";		
		update(sql, openId,userId);
	}

    @Override
    public  void bingUserOpenId(String userId, String openId,String unionid){
        String sql = null;
        if(AppUtils.isNotBlank(unionid)){
            sql ="UPDATE ls_user SET open_id='' WHERE ls_user.union_id=?  ";
            update(sql, unionid);
            sql ="UPDATE ls_user SET open_id='' WHERE ls_user.open_id=?  ";
            update(sql, openId);
        }
        else{
            sql ="UPDATE ls_user SET open_id='' WHERE ls_user.open_id=?  ";
            update(sql, openId);
        }
        sql ="UPDATE ls_user SET open_id=?,union_id=? WHERE ls_user.id=?  ";
        update(sql, openId,unionid,userId);
    }

	@Override
	public int batchOfflineUser(String userIds) {
		
		return 0;
	}

	@Override
	public boolean verifyMobileCode(String mobile, String mobileCode) {
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.MINUTE, -5);
		// 获取用户发送的短信，有效时间是5分钟
		Date date = cal.getTime();
		Integer mobileResult=this.get(sqlForFindUserByMobileCode, Integer.class, mobile, mobileCode,date);
		return mobileResult>0;
	}

	@Override
	public String getUserNameByPhoneOrUsername(String userMobile, String userName) {
		return super.get("select user_name from ls_usr_detail where user_mobile = ? or user_name = ?", String.class, userMobile, userName);
	}

	/**
	 * 记录用户的登录历史
	 */
	@Override
	public void updateUserLoginHistory(String ip, String userName) {
		String sql = ConfigCode.getInstance().getCode("login.updateUserDetail");
		if (log.isDebugEnabled()) {
			log.debug("Login Ip {}, userName {} ",ip,userName );
		}
		update(sql,  ip , userName);
	}

	@Override
	public void expiredVerificationCode(String mobile, String mobileCode) {
		String sql = "UPDATE ls_sms_log SET STATUS=0 WHERE user_phone = ? AND mobile_code = ?  AND STATUS=1";
		update(sql,mobile,mobileCode);
	}

}
