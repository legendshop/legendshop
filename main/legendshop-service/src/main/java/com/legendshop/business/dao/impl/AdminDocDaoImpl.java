/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AdminDocDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminDoc;
import com.legendshop.util.AppUtils;

/**
 * The Class AdminDocDaoImpl. Dao实现类
 */
@Repository
public class AdminDocDaoImpl extends GenericDaoImpl<AdminDoc, Long> implements AdminDocDao {

	/**
	 * 根据商城获取列表
	 */
	public List<AdminDoc> getAdminDoc(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}

	/**
	 * 根据Id获取
	 */
	public AdminDoc getAdminDoc(Long id) {
		return getById(id);
	}

	/**
	 * 删除
	 */
	public int deleteAdminDoc(AdminDoc adminDoc) {
		return delete(adminDoc);
	}

	/**
	 * 保存
	 */
	public Long saveAdminDoc(AdminDoc adminDoc) {
		return save(adminDoc, adminDoc.getId());
	}

	/**
	 * 更新
	 */
	public int updateAdminDoc(AdminDoc adminDoc) {
		return update(adminDoc);
	}

	/**
	 * 查询列表
	 */
	public PageSupport getAdminDoc(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public PageSupport<AdminDoc> queryAdminDocList(String curPageNO, AdminDoc adminDoc) {
		curPageNO = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
		CriteriaQuery cq = new CriteriaQuery(AdminDoc.class, curPageNO);

		if (AppUtils.isNotBlank(adminDoc.getTitle())) {
			cq.like("title", "%" + adminDoc.getTitle().trim() + "%");
		}
		cq.setPageSize(10);
		return queryPage(cq);
	}

}
