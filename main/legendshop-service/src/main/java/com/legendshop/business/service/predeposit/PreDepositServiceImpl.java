/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.predeposit;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.CashLog;
import com.legendshop.business.dao.CoinLogDao;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.PdRechargeDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.predeposit.PdCashHoldingDao;
import com.legendshop.business.dao.predeposit.PdWithdrawCashDao;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.constant.PrePayTypeEnum;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PdCashHolding;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.spi.service.PreDepositService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 预存款服务实现类
 *
 */
@Service("preDepositService")
public class PreDepositServiceImpl implements PreDepositService {

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private PdCashHoldingDao pdCashHoldingDao;

	@Autowired
	private PdWithdrawCashDao pdWithdrawCashDao;

	@Autowired
	private PdCashLogDao pdCashLogDao;

	@Autowired
	private PdRechargeDao pdRechargeDao;

	@Autowired
	private ExpensesRecordDao expensesRecordDao;

	@Autowired
	private CoinLogDao coinLogDao;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 用户提现申请操作.
	 *
	 * @param userId
	 * @param amount
	 * @param sn
	 * @param type
	 * @return the string
	 */
	@Override
	public String freezePredeposit(String userId, double amount, String sn, String type) {
		String result = null;
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}
		/*
		 * 判断是否余额金额
		 */
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		Double freezePredeposit = userDetail.getFreezePredeposit();
		if (userDetail.getAvailablePredeposit() <= 0 || availablePredeposit < amount) { // 可用金额为0
			result = PreDepositErrorEnum.INSUFFICIENT_AMOUNT.value();
			return result;
		}
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatePredeposit = Arith.sub(availablePredeposit, amount); // avaialbe_amout
																			// -金额
		Double updatefreePredeposit = Arith.add(freezePredeposit, amount); // hold_amount
																			// +
																			// 金额
		int count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		if (count == 0) {
			result = PreDepositErrorEnum.ERR.value();
			throw new BusinessException(" update user predeposit fail ");
		}
		PdCashHolding cashHolding = getPdCashHolding(sn, amount, userId, type);
		pdCashHoldingDao.save(cashHolding);
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 用户提现申请操作.
	 *
	 * @param withdrawCash
	 * @return the string
	 */
	@Override
	public String withdrawalApply(PdWithdrawCash withdrawCash) {
		String userId = withdrawCash.getUserId();
		String result = null;
		CashLog.log("<! ------ user_id = {} apply withdrawal flow.. ", userId);
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}
		/*
		 * 判断是否余额金额
		 */
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		Double freezePredeposit = userDetail.getFreezePredeposit();
		if (userDetail.getAvailablePredeposit() <= 0 || availablePredeposit < withdrawCash.getAmount()) { // 可用金额为0
			CashLog.log(" User cash insufficient amount by userId = {}  ------- !>  ", userId);
			result = PreDepositErrorEnum.INSUFFICIENT_AMOUNT.value();
			return result;
		}
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatePredeposit = Arith.sub(availablePredeposit, withdrawCash.getAmount()); // avaialbe_amout
																							// -
																							// 提现单金额
		Double updatefreePredeposit = Arith.add(freezePredeposit, withdrawCash.getAmount()); // hold_amount
																								// +
																								// 提现单金额
		int count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		if (count == 0) {
			CashLog.error(" update user predeposit fail, availablePredeposit:{} => {} , freezePredeposit:{} => {}   by userId = {} ------- !>  ",
					availablePredeposit, updatePredeposit, freezePredeposit, updatefreePredeposit, userId);
			result = PreDepositErrorEnum.ERR.value();
			throw new BusinessException(" update user predeposit fail ");
		}
		PdCashHolding cashHolding = getPdCashHolding(withdrawCash.getPdcSn(), withdrawCash.getAmount(), userId, PdCashLogEnum.WITHDRAW.value());
		pdCashHoldingDao.save(cashHolding);
		pdWithdrawCashDao.save(withdrawCash);
		result = PreDepositErrorEnum.OK.value();
		CashLog.log(" apply withdrawal  flow  success ------- !> ");
		return result;
	}

	/**
	 * 提现完成，财务审核结束.
	 *
	 * @param pdWithdrawCash
	 * @return the string
	 */
	@Override
	public String withdrawalApplyFinally(PdWithdrawCash pdWithdrawCash) {
		String result = null;
		CashLog.log("<! ------ apply withdrawCash finally flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(pdWithdrawCash.getUserId());
		if (AppUtils.isBlank(userDetail)) {
			CashLog.warn(" Can't find userDetai by userId= {}  ------- !> ", pdWithdrawCash.getUserId());
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}

		/*
		 * 先更新ls_pd_withdraw_cash 判断是否符合提现条件
		 */
		int count = pdCashHoldingDao.update(
				"update ls_pd_withdraw_cash set payment_time= ? ,payment_state=?,update_admin_name=?,admin_note=? " + "  where pdc_sn=? and payment_state=0 ",
				new Object[] { pdWithdrawCash.getPaymentTime(), pdWithdrawCash.getPaymentState(), pdWithdrawCash.getUpdateAdminName(),
						pdWithdrawCash.getAdminNote(), pdWithdrawCash.getPdcSn() }); // 更新
																						// ls_pd_withdraw_cash
																						// 如果更新成功才执行下面的动作
		if (count == 0) {
			CashLog.warn(
					" update ls_pd_withdraw_cash NON_COMPLIANCE  there is no data to meet the conditions for WithdrawCash pdc_sn={}  and payment_state=0 ------- !>  ",
					pdWithdrawCash.getPdcSn());
			result = PreDepositErrorEnum.NON_COMPLIANCE.value();
			return result;
		}

		/*
		 * update ls_pd_cash_holding 判断是否符合更新条件
		 */
		count = pdCashHoldingDao.update("update ls_pd_cash_holding set status= ?,release_time=?  where sn=?  and user_id=?  and status=0 ",
				new Object[] { 1, new Date(), pdWithdrawCash.getPdcSn(), pdWithdrawCash.getUserId() }); // 更新
																										// hoding
																										// 如果更新成功才执行下面的动作
		if (count == 0) {
			CashLog.error("withdraw alapply Finally, update ls_pd_cash_holding fail ------- !>  ");
			throw new BusinessException(" withdraw alapply Finally, update ls_pd_cash_holding fail ");
		}

		/*
		 * update userDetail freezePredeposit
		 */
		Double freezePredeposit = userDetail.getFreezePredeposit();
		Double updatefreePredeposit = Arith.sub(freezePredeposit, pdWithdrawCash.getAmount()); // hold_amount
																								// -
																								// 提现单金额
		count = userDetailDao.updateFreezePredeposit(updatefreePredeposit, freezePredeposit, pdWithdrawCash.getUserId()); // 更新用户冻结金额
		if (count == 0) {
			CashLog.error(" update user predeposit fail, freezePredeposit:{} => {}   by userId = {} ------- !>  ", freezePredeposit, updatefreePredeposit,
					pdWithdrawCash.getUserId());
			throw new BusinessException(" update user predeposit fail ");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(pdWithdrawCash.getUserId());
		pdCashLog.setUserName(userDetail.getUserName());
		pdCashLog.setSn(pdWithdrawCash.getPdcSn());
		pdCashLog.setLogType(PdCashLogEnum.WITHDRAW.value());
		pdCashLog.setAmount(-pdWithdrawCash.getAmount()); // 提现属于支出
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc("提现,提现金额为:" + pdWithdrawCash.getAmount() + " 提现流水号是：" + pdWithdrawCash.getPdcSn());
		pdCashLogDao.savePdCashLog(pdCashLog);
		CashLog.log(" apply withdrawCash finally flow  success ------- !> ");
		/**
		 * insert expensesRecord
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(-pdWithdrawCash.getAmount());
		expensesRecord.setRecordRemark("提现,提现金额为:" + pdWithdrawCash.getAmount() + " 提现流水号是：" + pdWithdrawCash.getPdcSn());
		expensesRecord.setUserId(pdWithdrawCash.getUserId());
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 用户取消提现申请.
	 *
	 * @param sn
	 *            用户提现申请的流水号
	 * @param amount
	 *            用户提现金额
	 * @param userId
	 *            用户ID
	 * @return the string
	 */
	@Override
	public String withdrawalApplyCancel(String sn, Double amount, String userId) {
		String result = null;
		CashLog.log("<! ------ user Cancel apply withdrawCash  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}

		/*
		 * 先更新ls_pd_withdraw_cash 判断是否符合提现条件
		 */
		int count = pdCashHoldingDao.update("delete from  ls_pd_withdraw_cash where pdc_sn=? and user_id=? and  payment_state=0 ", new Object[] { sn, userId }); // 更新
																																									// ls_pd_withdraw_cash
																																									// 如果更新成功才执行下面的动作
		if (count == 0) {
			CashLog.warn(
					" delete ls_pd_withdraw_cash NON_COMPLIANCE  there is no data to meet the conditions for WithdrawCash pdc_sn={}  and payment_state=0 ------- !>  ",
					sn);
			result = PreDepositErrorEnum.NON_COMPLIANCE.value();
			return result;
		}

		/*
		 * update ls_pd_cash_holding 判断是否符合更新条件
		 */
		count = pdCashHoldingDao.update("update ls_pd_cash_holding set status= ?,release_time=?  where sn=?  and user_id=?  and status=0 ",
				new Object[] { 1, new Date(), sn, userId }); // 更新 hoding
																// 如果更新成功才执行下面的动作
		if (count == 0) {
			CashLog.error("withdraw alapply Finally, update ls_pd_cash_holding fail ------- !>  ");
			throw new BusinessException(" withdraw alapply Finally, update ls_pd_cash_holding fail ");
		}

		/*
		 * update userDetail freezePredeposit
		 */
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		Double freezePredeposit = userDetail.getFreezePredeposit();
		Double updatePredeposit = Arith.add(availablePredeposit, amount); // avaialbe_amout
																			// +
																			// 提现单金额
		Double updatefreePredeposit = Arith.sub(freezePredeposit, amount); // hold_amount
																			// -
																			// 提现单金额
		count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		if (count == 0) {
			CashLog.error(" update user predeposit fail, availablePredeposit:{} => {} , freezePredeposit:{} => {}   by userId = {} ------- !>  ",
					availablePredeposit, updatePredeposit, freezePredeposit, updatefreePredeposit, userId);
			throw new BusinessException(" update user predeposit fail ");
		}

		CashLog.log(" user Cancel apply withdrawCash   success ------- !> ");
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 充值 -- 在线支付成功之后.
	 *
	 * @param pdRecharge
	 * @return the string
	 */
	@Override
	public String rechargeForPaySucceed(PdRecharge pdRecharge) {
		String result = null;
		CashLog.log("<! ------ recharge For PaySucceed  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		String userId = pdRecharge.getUserId();
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(pdRecharge.getUserId());
		if (AppUtils.isBlank(userDetail)) {
			CashLog.error(" Can't find userDetai by userId= {}  ------- !> ", userId);
			throw new BusinessException("支付失败，请联系管理员！ Can not find userDetail by userId " + userId);
		}

		/*
		 * 先更新 ls_pd_recharge 判断是否符合支付条件
		 */
		Date nowTimeDate = new Date();
		int count = pdRechargeDao.update(
				"update ls_pd_recharge set payment_state=?,payment_code=?,payment_name=?,trade_sn=?,payment_time=? where id=? and payment_state=0  ",
				new Object[] { 1, pdRecharge.getPaymentCode(), pdRecharge.getPaymentName(), pdRecharge.getTradeSn(), nowTimeDate, pdRecharge.getId() });
		if (count == 0) {
			CashLog.warn(
					" update ls_pd_recharge fail there is no data to meet the conditions for WithdrawCash ls_pd_recharge id={}  and payment_state=0 ------- !>  ",
					pdRecharge.getId());
			result = PreDepositErrorEnum.OK.value();
			return result; // 支付宝并发问题 ,不需要抛出异常 ,说明已经被处理过该充值单
		}

		/*
		 * update user predeposit
		 */
		Double available_predeposit = Arith.add(userDetail.getAvailablePredeposit(), pdRecharge.getAmount());
		count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			CashLog.error("预付款充值失败  by userId ={} , updatePredeposit={} ,originalPredeposit = {} , pdr_sn ={}  ", userId, available_predeposit,
					userDetail.getAvailablePredeposit(), pdRecharge.getSn());
			throw new BusinessException("预付款充值失败  ，请联系管理员！");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(pdRecharge.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.RECHARGE.value());
		pdCashLog.setAmount(pdRecharge.getAmount());
		pdCashLog.setAddTime(nowTimeDate);
		StringBuilder sb = new StringBuilder();
		sb.append("充值，充值单号: ").append(pdRecharge.getSn());
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(pdRecharge.getSn());
		pdCashLogDao.savePdCashLog(pdCashLog);
		CashLog.log("  recharge For PaySucceed  flow success -----！>  ");
		/*
		 * insert expensersRecored
		 */
		/*
		 * ExpensesRecord expensesRecord=new ExpensesRecord();
		 * expensesRecord.setRecordDate(nowTimeDate);
		 * expensesRecord.setRecordMoney(pdRecharge.getAmount());
		 * expensesRecord.setRecordRemark("预存款充值"+pdRecharge.getAmount()+
		 * "元,充值单号："+pdRecharge.getSn()); expensesRecord.setUserId(userId);
		 * expensesRecordDao.saveExpensesRecord(expensesRecord);
		 */
		return PreDepositErrorEnum.OK.value();
	}

	/**
	 * 分销佣金收入,充值到预存款.
	 *
	 * @param userId
	 *            用户ID
	 * @param amount
	 *            佣金收入金额
	 * @param subItemNumber
	 *            订单项流水号
	 * @return the string
	 */
	@Override
	public String rechargeForDistCommis(String userId, Double amount, String subItemNumber) {
		CashLog.log("<! ------ recharge For Distribute Commis  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.error(" Can't find userDetai by userId= {}  ------- !> ", userId);
			throw new BusinessException("佣金充值失败！ Can not find userDetail by userId " + userId);
		}

		/*
		 * update user predeposit
		 */
		Double available_predeposit = Arith.add(userDetail.getAvailablePredeposit(), amount);
		int count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			CashLog.error("佣金充值失败  by userId ={} , updatePredeposit={} ,originalPredeposit = {} , subItemNumber ={}  ", userId, available_predeposit,
					userDetail.getAvailablePredeposit(), subItemNumber);
			throw new BusinessException("佣金充值失败 ！");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userDetail.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.COMMISSION.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		StringBuilder sb = new StringBuilder();
		sb.append("分销佣金收入，分销流水号: ").append(subItemNumber);
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(subItemNumber);
		pdCashLogDao.savePdCashLog(pdCashLog);
		// 发送站内信通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(userDetail.getUserName(), "分销佣金收入通知", "分销的佣金" + amount + "已存入您的预存款，分销流水号:" + subItemNumber).getSource());
		CashLog.log("  recharge For Distribute Commis flow success -----！>  ");

		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark("分销佣金收入，分销流水号:" + subItemNumber);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.spi.service.PreDepositService#adminRechargeForPaySucceed(
	 * com.legendshop.model.entity.PdRecharge)
	 */
	@Override
	public String adminRechargeForPaySucceed(PdRecharge pdRecharge) {
		CashLog.log("<! ------ admin opereation  recharge For PaySucceed  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		String userId = pdRecharge.getUserId();
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(pdRecharge.getUserId());
		if (AppUtils.isBlank(userDetail)) {
			CashLog.error(" Can't find userDetai by userId= {}  ------- !> ", userId);
			return PreDepositErrorEnum.NOT_USER.value();
		}

		/*
		 * 先更新 ls_pd_recharge 判断是否符合支付条件
		 */
		int count = pdRechargeDao.update(
				"update ls_pd_recharge set payment_state=?,payment_code=?,payment_name=?,trade_sn=?,payment_time=?,admin_user_note=?,admin_user_name=? where id=? and payment_state=0  ",
				new Object[] { 1, pdRecharge.getPaymentCode(), pdRecharge.getPaymentName(), pdRecharge.getTradeSn(), pdRecharge.getPaymentTime(),
						pdRecharge.getAdminUserNote(), pdRecharge.getAdminUserName(), pdRecharge.getId() });
		if (count == 0) {
			CashLog.warn(
					" update ls_pd_recharge fail there is no data to meet the conditions for WithdrawCash ls_pd_recharge id={}  and payment_state=0 ------- !>  ",
					pdRecharge.getId());
			return PreDepositErrorEnum.NON_COMPLIANCE.value();
		}

		/*
		 * update user predeposit
		 */
		Double available_predeposit = Arith.add(userDetail.getAvailablePredeposit(), pdRecharge.getAmount());
		count = this.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			CashLog.error("预付款充值失败  by userId ={} , updatePredeposit={} ,originalPredeposit = {} , pdr_sn ={}  ", userId, available_predeposit,
					userDetail.getAvailablePredeposit(), pdRecharge.getSn());
			throw new BusinessException("预付款充值失败  ，请联系管理员！");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(pdRecharge.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.RECHARGE.value());
		pdCashLog.setAmount(pdRecharge.getAmount());
		pdCashLog.setAddTime(pdRecharge.getPaymentTime());
		StringBuilder sb = new StringBuilder();
		sb.append("充值，充值单号: ").append(pdRecharge.getSn());
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(pdRecharge.getSn());
		pdCashLogDao.savePdCashLog(pdCashLog);
		CashLog.log(" admin opereation  recharge For PaySucceed  success ..  -----!>  ");
		/*
		 * admin insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(pdRecharge.getAmount());
		expensesRecord.setRecordRemark("管理员充值，充值单号: " + pdRecharge.getSn());
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
	}

	/**
	 * 更新预存款
	 *
	 * @param available_predeposit
	 * @param availablePredeposit
	 * @param userId
	 * @return the int
	 */
	private int updateAvailablePredeposit(Double available_predeposit, Double availablePredeposit, String userId) {
		return userDetailDao.updateAvailablePredeposit(available_predeposit, availablePredeposit, userId);
	}

	/**
	 * 下单 支付宝成功返回前.
	 *
	 * @param sn
	 *            订单号
	 * @param amount
	 *            预付款支付金额
	 * @param userId
	 *            用户ID
	 * @param fullpay
	 *            是否通过预付款全额支付
	 * @return the string
	 */
	@Override
	public String beforeOrderPay(String sn, Double amount, String userId, boolean fullpay) {

		CashLog.log("<! ------ user_id = {} before OrderPay flow.. ", userId);
		/*
		 * 查询用户帐号 判断是否存在
		 */
		String result = null;
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}
		/*
		 * 判断是否余额金额
		 */
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		Double freezePredeposit = userDetail.getFreezePredeposit();
		if (userDetail.getAvailablePredeposit() <= 0 || availablePredeposit < amount) { // 可用金额为0
			CashLog.log(" User cash insufficient amount by userId = {}  ------- !>  ", userId);
			result = PreDepositErrorEnum.INSUFFICIENT_AMOUNT.value();
			return result;
		}
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatePredeposit = Arith.sub(availablePredeposit, amount); // avaialbe_amout
																			// -
																			// 下单预付款金额
		Double updatefreePredeposit = Arith.add(freezePredeposit, amount); // hold_amount
																			// +
																			// 下单预付款金额
		int count = 0;
		if (fullpay) { // 如果是全额支付
			count = userDetailDao.updateAvailablePredeposit(updatePredeposit, availablePredeposit, userId);
		} else {
			count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		}
		if (count == 0) {
			CashLog.error(" update user predeposit fail, availablePredeposit:{} => {} , freezePredeposit:{} => {}   by userId = {} ------- !>  ",
					availablePredeposit, updatePredeposit, freezePredeposit, updatefreePredeposit, userId);
			throw new BusinessException(" update user predeposit fail ");
		}
		if (fullpay) {
			/*
			 * insert PdCashLog
			 */
			PdCashLog pdCashLog = new PdCashLog();
			pdCashLog.setUserId(userId);
			pdCashLog.setUserName(userDetail.getUserName());
			pdCashLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
			pdCashLog.setAmount(amount);
			pdCashLog.setAddTime(new Date());
			StringBuilder sb = new StringBuilder();
			sb.append("下单,账户支付金额为:").append(amount).append(",订单流水号: ").append(sn);
			pdCashLog.setLogDesc(sb.toString());
			pdCashLog.setSn(sn);
			pdCashLogDao.savePdCashLog(pdCashLog);
			/*
			 * insert expensersRecored
			 */
			ExpensesRecord expensesRecord = new ExpensesRecord();
			expensesRecord.setRecordDate(new Date());
			expensesRecord.setRecordMoney(amount);
			expensesRecord.setRecordRemark("下单,账户支付金额为:" + amount + ",订单流水号:" + sn);
			expensesRecord.setUserId(userId);
			expensesRecordDao.saveExpensesRecord(expensesRecord);

		} else {
			PdCashHolding cashHolding = getPdCashHolding(sn, amount, userId, PdCashLogEnum.ORDER_PAY_FREEZE.value());
			pdCashHoldingDao.save(cashHolding);

		}
		CashLog.log(" before OrderPay success -------！> ");
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 下单后支付宝成功返回后，扣减预存款或者金币.
	 *
	 * @param sn
	 *            订单号
	 * @param amount
	 *            预付款支付的订单金额
	 * @param userId
	 *            订单的用户Id
	 * @param prePayType
	 *            the pre pay type
	 * @return the string
	 */
	@Override
	public String orderPaySuccess(String sn, Double amount, String userId, Integer prePayType) {
		CashLog.log("orderPaySuccess starting sn is {}, prePayType is {}, userId is {}, amount is {}", new Object[] { sn, prePayType, userId, amount });
		String result = null;

		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by sn= {},  userId= {}  ------- !> ", sn, userId);
			throw new BusinessException("支付失败，请联系管理员！  Can't find userDetai by userId= " + userId);
		}
		/*
		 * 判断是否余额金额
		 */
		Double availablePredeposit = 0d;

		if (PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)) {// 预存款
			availablePredeposit = userDetail.getAvailablePredeposit();
		} else if (PrePayTypeEnum.COIN.value().equals(prePayType)) {
			availablePredeposit = userDetail.getUserCoin();// 金币
		}

		CashLog.log("param availablePredeposit= {},pay amount={}------- !> ", availablePredeposit, amount);

		if (availablePredeposit <= 0 || availablePredeposit < amount) { // 可用金额为0
			CashLog.log(" User cash insufficient amount by sn= {}, userId = {}  ------- !>  ", sn, userId);
			throw new BusinessException("支付失败， 账户余额不足 ");
		}

		/*
		 * update userDetail freezePredeposit
		 */
		Double updatefreePredeposit = Arith.sub(availablePredeposit, amount); // hold_amount
																				// -
																				// 下单预付款金额
		int count = 0;
		if (PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)) {
			count = userDetailDao.updateAvailablePredeposit(updatefreePredeposit, availablePredeposit, userId); // 预付款
		} else if (PrePayTypeEnum.COIN.value().equals(prePayType)) {
			count = userDetailDao.updateUserCoin(updatefreePredeposit, availablePredeposit, userId); // 金币
		}

		if (count == 0) {
			CashLog.error(" update user predeposit fail,  availablePredeposit: {} => {}   by userId = {} for prePayType={}------- !>  ", availablePredeposit,
					updatefreePredeposit, userId, prePayType);
			throw new BusinessException("支付失败，请联系管理员！   update user predeposit fail");
		}

		/*
		 * insert PdCashLog 预存款使用历史
		 */
		if (PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)) {
			CashLog.log(" insert PdCashLog by sn= {}, userId = {}  ------- !>  ", sn, userId);
			PdCashLog pdCashLog = new PdCashLog();
			pdCashLog.setUserId(userId);
			pdCashLog.setUserName(userDetail.getUserName());
			pdCashLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
			pdCashLog.setAmount(-amount);
			pdCashLog.setAddTime(new Date());
			StringBuilder sb = new StringBuilder();
			sb.append("下单,账户支付金额为:").append(amount).append(",订单结算单据流水号: ").append(sn);
			pdCashLog.setLogDesc(sb.toString());
			pdCashLog.setSn(sn);
			pdCashLogDao.savePdCashLog(pdCashLog);
		} else if (PrePayTypeEnum.COIN.value().equals(prePayType)) {
			// 金币使用历史
			CashLog.log(" insert CoinLog by sn= {}, userId = {}  ------- !>  ", sn, userId);
			CoinLog coinLog = new CoinLog();
			coinLog.setUserId(userId);
			coinLog.setUserName(userDetail.getUserName());
			coinLog.setSn(sn);
			coinLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
			coinLog.setAmount(-amount);
			coinLog.setAddTime(new Date());
			StringBuilder sb = new StringBuilder();
			sb.append("下单,金币支付金额为:").append(amount).append(",订单结算单据流水号: ").append(sn);
			coinLog.setLogDesc(sb.toString());
			coinLogDao.saveCoinLog(coinLog);
		}

		CashLog.log(" after  OrderPay success ------- ！> ");
		result = PreDepositErrorEnum.OK.value();

		return result;
	}

	/**
	 * 订单支付成功
	 * 
	 * com.legendshop.spi.service.PreDepositService#orderPaySuccess1(java.lang.
	 * String, java.lang.Double, java.lang.String)
	 */
	@Override
	public String orderPaySuccess1(String sn, Double amount, String userId) {
		CashLog.log("<! ------ after orderPay  Success flow.. ");
		String result = null;
		/*
		 * update ls_pd_cash_holding 判断是否符合更新条件
		 */
		int count = pdCashHoldingDao.update("update ls_pd_cash_holding set status= ?, release_time= ?  where sn=?  and  user_id=?  and status=0 ",
				new Object[] { 1, new Date(), sn, userId }); // 更新 hoding
																// 如果更新成功才执行下面的动作
		if (count == 0) { // 说明您已经支付过了 不需要处理
			result = PreDepositErrorEnum.OK.value();
			return result;
		}

		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			throw new BusinessException("支付失败，请联系管理员！  Can't find userDetai by userId= {} " + userId);
		}

		Double freezePredeposit = userDetail.getFreezePredeposit();
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatefreePredeposit = Arith.sub(freezePredeposit, amount); // hold_amount
																			// -
																			// 下单预付款金额
		count = userDetailDao.updateFreezePredeposit(updatefreePredeposit, updatefreePredeposit, userId);
		if (count == 0) {
			CashLog.error(" update user predeposit fail,  freezePredeposit: {} => {}   by userId = {} ------- !>  ", freezePredeposit, updatefreePredeposit,
					userId);
			throw new BusinessException("支付失败，请联系管理员！   update user predeposit fail");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userDetail.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
		pdCashLog.setAmount(-amount);
		pdCashLog.setAddTime(new Date());
		StringBuilder sb = new StringBuilder();
		sb.append("下单,账户支付金额为:").append(amount).append(",订单流水号: ").append(sn);
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);

		CashLog.log(" after  OrderPay success ------- ！> ");

		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark("下单,账户支付金额为:" + amount + ",订单流水号:" + sn);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 取消订单.
	 *
	 * @param sn
	 *            订单号
	 * @param amount
	 *            预付款支付的订单金额
	 * @param userId
	 *            订单的用户Id
	 * @return the string
	 */
	@Override
	public String orderCancel(String sn, Double amount, String userId) {
		CashLog.log("<! ------ order Cancel flow.. ");
		String result = null;
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}

		int count = pdCashHoldingDao.update("update ls_pd_cash_holding set status= ?, release_time= ?  where sn=?  and  user_id=?  and status=0 ",
				new Object[] { 1, new Date(), sn, userId }); // 更新 hoding
																// 如果更新成功才执行下面的动作
		if (count == 0) {
			CashLog.log(" update ls_pd_cash_holding fail   ------- !> ");
			result = PreDepositErrorEnum.ERR.value(); // 不符合取消条件
			return result;
		}

		Double freezePredeposit = userDetail.getFreezePredeposit();
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatePredeposit = Arith.add(availablePredeposit, amount); // avaialbe_amout
																			// +
																			// 下单预付款金额
		Double updatefreePredeposit = Arith.sub(freezePredeposit, amount); // hold_amount
																			// -
																			// 下单预付款金额
		count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		if (count == 0) {
			CashLog.error(" update user predeposit fail,  freezePredeposit: {} => {}   by userId = {} ------- !>  ", freezePredeposit, updatefreePredeposit,
					userId);
			throw new BusinessException("取消订单失败，请联系管理员！   update user predeposit fail");
		}
		CashLog.log(" after  OrderPay success ------- ！> ");
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 退款订单.
	 *
	 * @param sn
	 *            退款流水号
	 * @param amount
	 *            退款金额
	 * @param userId
	 *            订单的用户Id
	 * @return the string
	 */
	@Override
	public String orderRefund(String sn, Double amount, String userId) {
		CashLog.log("<! ------ order  Refund flow.. ");
		String result = null;
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			CashLog.log(" Can't find userDetai by userId= {}  ------- !> ", userId);
			result = PreDepositErrorEnum.NOT_USER.value();
			return result;
		}
		Double availablePredeposit = userDetail.getAvailablePredeposit();
		/*
		 * update userDetail freezePredeposit
		 */
		Double updatePredeposit = Arith.sub(availablePredeposit, amount); // avaialbe_amout
																			// -
																			// 退款金额
		int count = userDetailDao.updateAvailablePredeposit(updatePredeposit, availablePredeposit, userId);
		if (count == 0) {
			CashLog.error(" update user predeposit fail,  availablePredeposit: {} => {}   by userId = {} ------- !>  ", availablePredeposit, updatePredeposit,
					userId);
			throw new BusinessException("取消订单失败，请联系管理员！   update user predeposit fail");
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userDetail.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		StringBuilder sb = new StringBuilder();
		sb.append("退款,退款金额为:").append(amount).append(",退款流水号: ").append(sn);
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);

		CashLog.log(" after  OrderPay success ------- ！> ");

		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark("退款,退款金额为:" + amount + ",退款流水号:" + sn);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		result = PreDepositErrorEnum.OK.value();
		return result;
	}

	/**
	 * 组装 PdCashHolding.
	 *
	 * @param sn
	 *            the sn
	 * @param amount
	 *            the amount
	 * @param userId
	 *            the user id
	 * @param type
	 *            the type
	 * @return the pd cash holding
	 */
	private PdCashHolding getPdCashHolding(String sn, Double amount, String userId, String type) {
		PdCashHolding cashHolding = new PdCashHolding();
		cashHolding.setSn(sn);
		cashHolding.setAmount(amount);
		cashHolding.setUserId(userId);
		cashHolding.setAddTime(new Date());
		cashHolding.setType(type);
		cashHolding.setStatus(Constants.NO);
		return cashHolding;
	}
	/**
	 * 充值
	 *
	 * @param userId
	 * @param userName
	 * @param amount
	 * @param sn
	 * @param message
	 * @return the string
	 */
	@Override
	public String recharge(String userId, String userName, Double amount, String sn, String message) {
		CashLog.log("<! ------ recharge For PaySucceed  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return " Can not find userDetail ";
		}
		/*
		 * update user predeposit
		 */
		Double originAmount = userDetail.getAvailablePredeposit();
		if (originAmount == null) {
			originAmount = 0d;
		}
		Double available_predeposit = Arith.add(originAmount, amount);
		int count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return " 充值失败 ，请联系管理员！";
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.RECHARGE.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc(message);
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);
		CashLog.log("  recharge For PaySucceed  flow success -----！>  ");
		/*
		 * 消费记录
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark(message);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
	}

	/**
	 * 预存款退款
	 */
	@Override
	public String refund(String userId, String userName, Double amount, String sn, String message) {
		CashLog.log("<! ------ refund For orderRefund  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return " Can not find userDetail ";
		}
		/*
		 * update user predeposit
		 */
		Double originAmount = userDetail.getAvailablePredeposit();
		if (originAmount == null) {
			originAmount = 0d;
		}
		Double available_predeposit = Arith.add(originAmount, amount);
		int count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return " 订单退款失败 ，请联系管理员！";
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc(message);
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);
		
		CashLog.log("  refund For orderRefund  flow  LogType = {} -----！>  ",PdCashLogEnum.REFUND.value());
		
		CashLog.log("  refund For orderRefund  flow success -----！>  ");
		
		/*
		 * 消费记录
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark(message);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
	}

}
