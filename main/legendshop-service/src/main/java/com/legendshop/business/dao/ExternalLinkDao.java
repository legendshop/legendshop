/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.ExternalLink;

/**
 * 友情链接Dao.
 */
public interface ExternalLinkDao extends GenericDao<ExternalLink, Long> {

	public abstract List<ExternalLink> getExternalLink();

	public abstract void deleteExternalLinkById(Long id);

	public abstract void updateExternalLink(ExternalLink externalLink);

	public abstract PageSupport<ExternalLink> getFriendLink(String curPageNO, String userName);

	public abstract PageSupport<ExternalLink> getDataByCriteriaQuery(String curPageNO, ExternalLink externalLink, Integer pageSize, DataSortResult result);

}