/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserAddressSubDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.UserAddressSub;

/**
 *用户配送地址Dao
 */
@Repository
public class UserAddressSubDaoImpl extends GenericDaoImpl<UserAddressSub, Long> implements UserAddressSubDao  {

	public UserAddressSub getUserAddressSub(Long id){
		return getById(id);
	}
	
    public int deleteUserAddressSub(UserAddressSub userAddressSub){
    	return delete(userAddressSub);
    }
	
	public Long saveUserAddressSub(UserAddressSub userAddressSub){
		return save(userAddressSub);
	}
	
	public int updateUserAddressSub(UserAddressSub userAddressSub){
		return update(userAddressSub);
	}

	@Override
	public UserAddressSub getUserAddressSub(Long addrId, Long version) {
		UserAddressSub  userAddressSub = getByProperties(new EntityCriterion().eq("addrId", addrId).eq("version", version));
		return userAddressSub;
	}

	@Override
	public UserAddressSub getUserAddressSubForOrderDetail(Long id) {
		return getById(id); 
	}
	
 }
