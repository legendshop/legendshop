/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.IndexJpgDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Indexjpg;
import com.legendshop.util.AppUtils;

/**
 * 首页广告Dao.
 */
@Repository
public class IndexJpgDaoImpl extends GenericDaoImpl<Indexjpg, Long> implements IndexJpgDao {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(IndexJpgDaoImpl.class);

	// 0：pc端；1：手机端
	private String sqlForgetIndexJpegByShopId = "SELECT id, img, des, title, link, STATUS, seq, upload_time AS uploadTime FROM ls_index_jpg WHERE STATUS = 1 AND location = 0  ORDER BY seq ASC";

	private String sqlForMobileGetIndexJpegByShopId = "SELECT id, img, des, title, link, STATUS, seq, upload_time AS uploadTime FROM ls_index_jpg WHERE STATUS = 1 AND location = 1 ORDER BY seq ASC";

	@Override

	/**
	 * pc端的广告
	 */
	@Cacheable(value = "IndexjpgList", key = "'0'")
	public List<Indexjpg> getIndexJpegByShopId() {
		log.debug("Load pc indexjpg");
		return queryLimit(sqlForgetIndexJpegByShopId, Indexjpg.class, 0, 7);
	}

	/**
	 * 手机端的广告
	 */
	@Override
	@Cacheable(value = "IndexjpgList", key = "'1'")
	public List<Indexjpg> queryMobileIndexJpeg(Long shopId) {
		return queryLimit(sqlForMobileGetIndexJpegByShopId, Indexjpg.class, 0, 7);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.IndexJpgDao#queryIndexJpg(com.legendshop
	 * .core.dao.support.CriteriaQuery)
	 */
	@Override
	public PageSupport<Indexjpg> queryIndexJpg(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.IndexJpgDao#queryIndexJpg(java.lang.
	 * Long)
	 */
	@Override
	public Indexjpg queryIndexJpg(Long id) {
		return getById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.IndexJpgDao#deleteIndexJpg(com.
	 * legendshop .model.entity.Indexjpg)
	 */
	@Override
	@CacheEvict(value = "IndexjpgList", allEntries = true)
	public void deleteIndexJpg(Indexjpg indexjpg) {
		delete(indexjpg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.IndexJpgDao#updateIndexjpg(com.
	 * legendshop .model.entity.Indexjpg)
	 */
	@Override
	@CacheEvict(value = "IndexjpgList", allEntries = true)
	public void updateIndexjpg(Indexjpg origin) {
		update(origin);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.IndexJpgDao#saveIndexjpg(com.legendshop
	 * .model.entity.Indexjpg)
	 */
	@Override
	@CacheEvict(value = "IndexjpgList", allEntries = true)
	public Long saveIndexjpg(Indexjpg indexjpg) {
		indexjpg.setUploadTime(new Date());
		return save(indexjpg);
	}

	@Override
	public PageSupport<Indexjpg> getIndexJpgPage(String curPageNO, Indexjpg indexjpg, int location, Integer pageSize, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(Indexjpg.class, curPageNO);

		if (!AppUtils.isBlank(indexjpg.getTitle())) {
			cq.like("title", "%" + indexjpg.getTitle().trim() + "%");
		}
		// 非导出情况
		if (AppUtils.isNotBlank(pageSize)) {
			cq.setPageSize(pageSize);
		}
		// 判断是否为外部排序
		if (!result.isSortExternal()) {
			cq.addOrder("asc", "seq");
		} else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		cq.eq("location", location);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Indexjpg> getIndexJpgPageByData(String curPageNO, Indexjpg indexjpg, int location, Integer pageSize, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(Indexjpg.class, curPageNO);

		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			cq.setPageSize(pageSize);
		}
		if (!result.isSortExternal()) {
			cq.addOrder("asc", "seq");
		}

		cq.eq("location", location);
		return queryPage(cq);
	}

}
