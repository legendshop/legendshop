/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.dao.security.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.security.AdminAuthDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.util.AppUtils;


/**
 * 管理员验证Dao
 */
@Repository
public class AdminAuthDaoImpl implements AdminAuthDao {

	/** The log. */
	Logger log = LoggerFactory.getLogger(AdminAuthDaoImpl.class);
    
	/** The generic jdbc dao. */
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/** The Constant sqlForFindAdminUserByName. */
	private static final String sqlForFindAdminUserByName =
			"SELECT u.id, u.name, u.note,u.enabled, u.password,u.active_time as activeTime FROM ls_admin_user u where  u.enabled = '1' and u.name =  ?";
	
	
	/** The Constant sqlForFindFunctionsByUser. */
	private static final  String sqlForFindFunctionsByUser = "select DISTINCT f.protect_function as name from ls_usr_role ur ,ls_role r,ls_perm p, ls_func f where r.enabled = '1'  and ur.role_id=r.id and r.id=p.role_id and p.function_id=f.id and r.app_no = ? and f.app_no = ? and ur.user_id= ?";


	/** The Constant sqlForFindRolesByUser. */
	private static final String sqlForFindRolesByUser = "SELECT DISTINCT r.role_type as name FROM ls_usr_role ur ,ls_role r WHERE r.enabled ='1'  AND ur.role_id=r.id AND r.app_no =? AND ur.user_id= ? ";

	
	/* (non-Javadoc)
	 * @see com.legendshop.business.dao.security.AdminAuthDao#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserEntity loadUserByUsername(String username) {
		// 获取用户信息
		UserEntity user =  findAdminUserByName(username);
		if (user == null) {
			log.debug("Can not find user by name {}", username);
		}
		return user;
	}

	/**
	 * 管理员登录，如果密码不正确则不返回信息
	 * 带上用户权限和角色.
	 *
	 * @param username the username
	 * @param presentedPassword the presented password
	 * @return the user entity
	 */
	@Override
	public UserEntity loadAdminUserByUsername(String username, String presentedPassword) {
		// 获取用户信息
		UserEntity user =  findAdminUserByName(username);
		if (user == null) {
			log.debug("Can not find user by name {}", username);
			return null;
		}
		List<String> roles = findRolesByUser(user.getId(), ApplicationEnum.BACK_END.value());
		if (AppUtils.isBlank(roles)) {
			log.debug("User {}  has no roles", user.getName());
			return null;
		}
		user.setRoles(roles);
		
		user.setFunctions(findFunctionsByUser(user.getId(), ApplicationEnum.BACK_END.value()));//查找权限)
		//返回用户
		return user;
	}
	
	/**
	 * 找到有效时间内的用户.
	 *
	 * @param name the name
	 * @return the user entity
	 */
	protected UserEntity findAdminUserByName(String name) {
		List<UserEntity> userList = genericJdbcDao.query(sqlForFindAdminUserByName, new Object[] {name}, new RowMapper<UserEntity>() {
			public UserEntity mapRow(ResultSet rs, int index) throws SQLException {
				Date activeTime = rs.getDate("activeTime");
				if(activeTime != null && activeTime.before(new Date())){
					return null;
				}else{
					UserEntity user = new UserEntity();
					user.setEnabled(rs.getString("enabled"));
					user.setId(rs.getString("id"));
					user.setName(rs.getString("name"));
					user.setNote(rs.getString("note"));
					user.setPassword(rs.getString("password"));
					user.setShopId(systemParameterUtil.getDefaultShopId()); //后台用户直接设置为默认商家的员工
					return user;
				}
			}
		});
		
		if (AppUtils.isNotBlank(userList)) {
			return userList.iterator().next();
		} else {
			return null;
		}
	}
	
	
	/**
	 * Find functions by user.
	 *
	 * @param userId the user id
	 * @param appNo the app no
	 * @return the list< granted function>
	 */
	private List<String> findFunctionsByUser(String userId, String appNo) {
		return genericJdbcDao.query(sqlForFindFunctionsByUser, new Object[] { appNo, appNo, userId }, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return rs.getString("name");
			}
		});
	}
	
	/**
	 * Find roles by user.
	 *
	 * @param userId the user id
	 * @param appNo the app no
	 * @return the list< granted authority>
	 */
	private List<String> findRolesByUser(String userId, String appNo) {
		return genericJdbcDao.query(sqlForFindRolesByUser, new Object[] { appNo, userId }, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return rs.getString("name");
			}
		});
	}

}
