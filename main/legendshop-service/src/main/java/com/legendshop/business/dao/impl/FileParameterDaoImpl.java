package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.FileParameterDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.FileParameter;

@Repository
public class FileParameterDaoImpl extends GenericDaoImpl<FileParameter, String> implements FileParameterDao {


    @Override
    public FileParameter findByName(String fileName) {
        return super.getById(fileName);
    }

    @Override
    public void deleteByName(String fileName) {
        super.deleteById(fileName);
    }

    @Override
    public String save(FileParameter fileParameter) {
        return super.save(fileParameter);
    }

    @Override
    public int update(FileParameter fileParameter) {

        return super.update(fileParameter);
    }
}
