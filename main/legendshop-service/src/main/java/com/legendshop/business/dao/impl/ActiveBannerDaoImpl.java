/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ActiveBannerDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ActiveBanner;

/**
 * The Class GroupBannerDaoImpl.
 * Dao实现类
 */
@Repository
public class ActiveBannerDaoImpl extends GenericDaoImpl<ActiveBanner, Long> implements ActiveBannerDao  {


	/**
	 *  根据Id获取
	 */
	public ActiveBanner getBanner(Long id){
		return getById(id);
	}

	/**	
	 *  删除
	 */	
	public int deleteBanner(ActiveBanner banner){
		return delete(banner);
	}

	/**
	 *  保存
	 */		
	public Long saveBanner(ActiveBanner banner){
		return save(banner);
	}

	/**
	 *  更新
	 */		
	public int updateBanner(ActiveBanner banner){
		return update(banner);
	}

	public List<ActiveBanner> getBannerList(String type) {	
		return this.queryByProperties(new EntityCriterion().eq("bannerType", type).addAscOrder("seq"));
	}

}
