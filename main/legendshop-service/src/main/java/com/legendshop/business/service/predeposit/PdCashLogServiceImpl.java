/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.predeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.util.AppUtils;

/**
 * 预存款日志服务实现类
 */
@Service("pdCashLogService")
public class PdCashLogServiceImpl implements PdCashLogService {

	@Autowired
	private PdCashLogDao pdCashLogDao;

	public PdCashLog getPdCashLog(Long id) {
		return pdCashLogDao.getPdCashLog(id);
	}

	/**
	 * 删除预存款日志
	 */
	public void deletePdCashLog(PdCashLog pdCashLog) {
		pdCashLogDao.deletePdCashLog(pdCashLog);
	}

	/**
	 * 保存预存款日志
	 */
	public Long savePdCashLog(PdCashLog pdCashLog) {
		if (!AppUtils.isBlank(pdCashLog.getId())) {
			updatePdCashLog(pdCashLog);
			return pdCashLog.getId();
		}
		return pdCashLogDao.savePdCashLog(pdCashLog);
	}

	/**
	 * 更新预存款日志
	 */
	public void updatePdCashLog(PdCashLog pdCashLog) {
		pdCashLogDao.updatePdCashLog(pdCashLog);
	}

	/**
	 * 获取预存款日志列表
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, String userId, String userName) {
		return pdCashLogDao.getPdCashLogListPage(curPageNO, userId, userName);
	}

	/**
	 * 获取预存款日志列表
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, PdCashLog pdCashLog, Integer pageSize, DataSortResult result) {
		return pdCashLogDao.getPdCashLogListPage(curPageNO, pdCashLog, pageSize, result);
	}

	/**
	 * 获取预存款日志
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLogPage(String userId, String curPageNO) {
		return pdCashLogDao.getPdCashLogPage(userId, curPageNO);
	}

	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLogPageByType(String userId, Integer state, String curPageNO) {


		return pdCashLogDao.getPdCashLogPageByType(userId,state,curPageNO);
	}
	
	
	//////////////

	/**
	 * 查询分销的历史转账.
	 *
	 * @param curPageNO
	 * @param pdCashLog
	 * @param pageSize
	 * @return the pd cash log
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, PdCashLog pdCashLog, int pageSize) {
		return pdCashLogDao.getPdCashLog(curPageNO, pdCashLog, pageSize);
	}

	/**
	 * 查询分销的历史转账.
	 *
	 * @param curPageNO
	 * @param uesrId
	 * @param logType
	 * @param pageSize
	 * @return the pd cash log
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, String uesrId, String logType, int pageSize) {
		return pdCashLogDao.getPdCashLog(curPageNO, uesrId, logType, pageSize);
	}

	/**
	 * 查询分销的历史转账.
	 *
	 * @param curPageNO
	 *            the cur page no
	 * @param pageSize
	 *            the page size
	 * @param userId
	 *            the user id
	 * @param logType
	 *            the log type
	 * @return the pd cash log
	 */
	@Override
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, int pageSize, String userId, String logType) {
		return pdCashLogDao.getPdCashLog(curPageNO, pageSize, userId, logType);
	}

	/**
	 * 查询个人提现记录.
	 *
	 * @param curPageNO
	 * @param pageSize
	 * @param userId
	 * @param logType
	 * @return the present record
	 */
	@Override
	public PageSupport<PdCashLog> getPresentRecord(String curPageNO, int pageSize, String userId, String logType) {
		return pdCashLogDao.getPresentRecord(curPageNO, pageSize, userId, logType);
	}
	
	
}
