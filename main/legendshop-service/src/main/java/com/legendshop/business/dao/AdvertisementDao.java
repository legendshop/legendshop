/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Advertisement;

/**
 * 广告Dao.
 */
public interface AdvertisementDao extends GenericDao<Advertisement, Long> {

	/**
	 *获取广告
	 */
	public abstract Map<String, List<Advertisement>> getAdvertisement(Long shopId, String page);

	/**
	 * 检查广告上限.
	 *
	 */
	public abstract boolean isMaxNum(Long shopId, String type);

	/**
	 * Delete adv by id.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void deleteAdvById(Long id);

	/**
	 * Update adv.
	 * 
	 * @param advertisement
	 *            the advertisement
	 */
	public abstract void updateAdv(Advertisement advertisement);

	public abstract Advertisement getAdvertisementById(Long id);

	public abstract PageSupport<Advertisement> queryAdvertisement(String curPageNO, Advertisement advertisement);

}