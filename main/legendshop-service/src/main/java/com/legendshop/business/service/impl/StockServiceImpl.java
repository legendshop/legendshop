/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.business.dao.*;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.processor.SendArrivalInformProcessor;
import com.legendshop.spi.service.StockLogService;
import com.legendshop.spi.service.StoreSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StockAdminDto;
import com.legendshop.model.dto.UpdSkuStockDto;
import com.legendshop.model.dto.stock.StockArmDto;
import com.legendshop.spi.service.StockService;
import com.legendshop.util.AppUtils;

/**
 * 库存管理服务
 */
@Service("stockService")
public class StockServiceImpl implements StockService {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private TransportDao transportDao;

	@Autowired
	private StockLogDao stockLogDao;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private SendArrivalInformProcessor sendArrivalInformProcessor;

	@Autowired
	private StoreSkuService storeSkuService;


	/**
	 * hold住库存，客户下单时调用，使得后面的人不能继续下单 如果库存不足， 则不能hold成功
	 *
	 */
	@Override
	public boolean addHold(Long prodId,Long skuId, Long basketCount) {
		Integer stocks = skuDao.getStocksByLockMode(skuId);
		// sku库存
		if (stocks == null) {
			stocks = 0;
		}
		if (stocks - basketCount < 0) {
			return false;
		} else {
			skuDao.updateSkuStocks(skuId, stocks - basketCount);
			//更新商品库存：等于所有SKU的库存
			productDao.updateProdStocks(prodId);
			Sku originSku = skuDao.getSku(skuId);
			StockLog stockLog = new StockLog();
			stockLog.setProdId(originSku.getProdId());
			stockLog.setSkuId(originSku.getSkuId());
			stockLog.setName(originSku.getName());
			stockLog.setBeforeStock(Long.valueOf(stocks));
			stockLog.setBeforeActualStock(originSku.getActualStocks());
			stockLog.setAfterStock(stocks - basketCount);
			stockLog.setAfterActualStock(originSku.getActualStocks());
			stockLog.setUpdateTime(new Date());
			stockLog.setUpdateRemark("用户下单'"+originSku.getName()+"'，商品销售库存为:'" + stockLog.getAfterStock() + "'，实际库存为:'" + stockLog.getAfterActualStock() + "'");
			stockLogDao.saveStockLog(stockLog);
			return true;
		}
	}
	/**
	 * 释放库存
	 */
	@Override
	public void releaseHold(Long prodId, Long skuId, Long basketCount,String subType) {
		Integer stocks = skuDao.getStocksByLockMode(skuId);
		// 产品库存
		if (stocks == null) {
			stocks = 0;
		}
		//销售库存回滚
		skuDao.updateSkuStocks(skuId, stocks + basketCount);

		//实际库存不回滚
//		if (!SubTypeEnum.SECKILL.value().equals(subType) && !SubTypeEnum.PRESELL.value().equals(subType)){
//			skuDao.updateSkuActualStocks(skuId, stocks + basketCount);
//		}
		//更新库存记录
		Sku sku = skuDao.getSkuById(skuId);

		if(AppUtils.isNotBlank(sku)){
			Long after = stocks + basketCount;
			StockLog stockLog = new StockLog();
			stockLog.setProdId(sku.getProdId());
			stockLog.setSkuId(sku.getSkuId());
			stockLog.setName(sku.getName());
			stockLog.setBeforeStock(Long.parseLong(stocks+""));
			stockLog.setAfterStock(after);
			stockLog.setBeforeActualStock(sku.getActualStocks());
			stockLog.setAfterActualStock(sku.getActualStocks());
			stockLog.setUpdateTime(new Date());
			stockLog.setUpdateRemark("取消订单，更新商品'"+sku.getName()+"'，商品销售库存为:'"+ after + "'");
			stockLogDao.saveStockLog(stockLog);
			//更新商品库存：等于所有SKU的库存
			productDao.updateProdStocks(prodId);
		}
	}

	/**
	 * 退款退货补库存
	 */
	@Override
	public void makeUpStocks(Long skuId, Long basketCount) {
		//更新库存记录
		Sku sku = skuDao.getSkuById(skuId);
		if (AppUtils.isNotBlank(sku)) {
			this.skuDao.updateSkuAllStocks(skuId, basketCount);
			Long stocks = sku.getStocks();
			Long actualStock = sku.getActualStocks();
			Long after = stocks + basketCount;
			Long afterActualStock = actualStock + basketCount;
			StockLog stockLog = new StockLog();
			stockLog.setProdId(sku.getProdId());
			stockLog.setSkuId(sku.getSkuId());
			stockLog.setName(sku.getName());
			stockLog.setBeforeStock(Long.parseLong(stocks + ""));
			stockLog.setAfterStock(after);
			stockLog.setBeforeActualStock(actualStock);
			stockLog.setAfterActualStock(afterActualStock);
			stockLog.setUpdateTime(new Date());
			stockLog.setUpdateRemark("用户退货，更新商品'" + sku.getName() + "'，商品销售库存为:'"+ after + "'，实际库存为:'" + afterActualStock + "'");
			stockLogDao.saveStockLog(stockLog);
			//更新商品库存：等于所有SKU的库存
			productDao.updateProdAllStocks(sku.getProdId());
		}
	}

	/**
	 * 检查库存，是否可以订购
	 *
	 * @param product
	 *            the product
	 * @return true, if successful
	 */
	@Override
	public boolean canOrder(Product product, Integer count) {
		// 数量检查
		return product != null && (product.getStocks() == null || product.getStocks() >= count);
	}

	@Override
	public boolean canOrder(Sku sku, Integer count) {
		// 数量检查
		return sku != null && (sku.getStocks() == null || sku.getStocks() >= count);
	}

	/**
	 * hold住库存，后台发货时调用  如果库存不足， 则不能hold成功
	 * @param prodId 商品Id
	 * @param skuId 单品Id
	 * @param basketCount 订购数
	 * @return 成功与否
	 */
	@Override
	public boolean addActualHold(Long prodId,Long skuId, Long basketCount) {
		Integer stocks = skuDao.getActualStocksByLockMode(skuId);
		Sku sku=skuDao.getSku(skuId);
		// sku库存
		if (stocks == null) {
			stocks = 0;
		}
		if (stocks - basketCount < 0) {
			return false;
		} else {
			skuDao.updateSkuActualStocks(skuId, stocks - basketCount);

			//更新商品库存：等于所有SKU的库存
			productDao.updateProdActualStocks(prodId);
			Long after = sku.getActualStocks()-basketCount;
			StockLog stockLog=new StockLog();
			stockLog.setProdId(sku.getProdId());
			stockLog.setSkuId(sku.getSkuId());
			stockLog.setName(sku.getName());
			stockLog.setBeforeActualStock(sku.getActualStocks());
			stockLog.setAfterActualStock(after);
			stockLog.setBeforeStock(sku.getStocks());
			stockLog.setAfterStock(sku.getStocks());
			Date date=new Date();
			stockLog.setUpdateTime(date);
			stockLog.setUpdateRemark("购买商品'"+sku.getName()+"'，商品实际库存为:'"+after+"'");
			stockLogDao.saveStockLog(stockLog);
			return true;
		}
	}


	/**
	 * 获取商品SKU的库存数
	 */
	@Override
	public Integer getStocksByLockMode(Long prodId,Long skuId) {
		if(AppUtils.isNotBlank(skuId) && skuId!=0 ){
			return skuDao.getStocksByLockMode(skuId);
		}else {
			return productDao.getStocksByLockMode(prodId);
		}
	}

	/**
	 * 获取商品或者SKU的库存
	 */
	@Override
	public Integer getStocksByMode(Long prodId, Long skuId) {
		if(AppUtils.isNotBlank(skuId) && skuId!=0 ){
			return skuDao.getStocksByMode(skuId);
		}else {
			return productDao.getStocksByMode(prodId);
		}
	}

	/**
	 * 根据 城市计算 区域限售
	 * @param prodId
	 * @param skuId
	 * @param cityId
	 * @param transportId
	 * @param shopId
	 * @return
	 */
	public Boolean isRegionalSales(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId) {
		if (AppUtils.isBlank(prodId) || AppUtils.isBlank(skuId) || AppUtils.isBlank(cityId)) {
			return false;
		}

		if (AppUtils.isNotBlank(transportId)) {
			Transport transport = transportDao.getDetailedTransport(shopId, transportId);
			if (AppUtils.isNotBlank(transport)) {
				//支持区域限售
				transport.getIsRegionalSales();
				if (transport.getIsRegionalSales() == 1) {
					if (!transport.geTransfeeIsCity(cityId.longValue()))
						return true;
				}
			}
		}

		return false;
	}

	/**
	 * 根据 城市计算 SKU的库存和仓库ID
	 */
	@Override
	public Map<String,String> calculateSkuStocksByCity(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId) {
		if(AppUtils.isBlank(prodId) || AppUtils.isBlank(skuId) || AppUtils.isBlank(cityId)){
			return null;
		}
		Map<String,String> map = new HashMap<String,String>();
		Sku sku = skuDao.getSku(skuId);
		map.put("skuType", sku.getSkuType());
		//如果 0代表没有自定义运费模板区域限售
		if(AppUtils.isNotBlank(transportId) && 0==transportId){
			//库存
			map.put("stocks", sku.getStocks().toString());
		}else{

			Transport transport=transportDao.getDetailedTransport(shopId,transportId);//shopId
			if(AppUtils.isNotBlank(transport)){
			 //支持区域限售
				if(transport.getIsRegionalSales() == 1){

			    	//List<Transfee> transfees=new ArrayList<Transfee>();

			    	if(transport.geTransfeeIsCity(cityId.longValue())){
			    		map.put("stocks", sku.getStocks().toString());
			    	}else{
			    		map.put("stocks","-1");
			    	}
			    /*	if(transport.isTransMail()){
			    		transfees=transport.getEmsList();
			    	}
			    	*/


			    	//判断这个城市是否在限售的区域里面，在就显示库存，不在就不显示库存
			    /*	List<Transfee> transfees=transport.getExpressList();

			    	if(AppUtils.isBlank(transfees)){
			    		transfees=transport.getExpressList(); //平邮
			    	}
			    	if(AppUtils.isBlank(transfees)){
			    		transfees=transport.getFeeList();//快递
			    	}
			    	*/

			    	/*for (Transfee transfee : transfees) {
			    		if (AppUtils.isBlank(transfee.getCityId())) {
			    			map.put("stocks","-1");
			    		}else{
			    			if(transfee.getCityIdList().contains(cityId.longValue())){
			    				map.put("stocks", sku.getStocks().toString());
			    				break;
			    			}else{
			    				map.put("stocks","-1");
			    			}
			    		}
					}*/
			    }else{
			     //不支持区域限售
			    	map.put("stocks", sku.getStocks().toString());
			    }
			}else{
				map.put("stocks", sku.getStocks().toString());
			}
		}
		return map;
	}

	/**
	 * 根据 城市计算 SKU的库存和仓库ID
	 */
	@Override
	public Map<String,String> calculateSkuStocksByCity(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId, Long storeId) {
		if(AppUtils.isBlank(prodId) || AppUtils.isBlank(skuId) || AppUtils.isBlank(cityId)){
			return null;
		}
		Map<String,String> map = new HashMap<String,String>();
		StoreSku sku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
		// 为空代表门店无货
		if (AppUtils.isBlank(sku)) {
			map.put("stocks", "0");
			return map;
		}
		//如果 0代表没有自定义运费模板区域限售
		if(AppUtils.isNotBlank(transportId) && 0==transportId){
			//库存
			map.put("stocks", sku.getStock().toString());
		}else{

			Transport transport=transportDao.getDetailedTransport(shopId,transportId);//shopId
			if(AppUtils.isNotBlank(transport)){
			 //支持区域限售
				if(transport.getIsRegionalSales() == 1){
			    	if(transport.geTransfeeIsCity(cityId.longValue())){
			    		map.put("stocks", sku.getStock().toString());
			    	}else{
			    		map.put("stocks","-1");
			    	}
			    }else{
			     //不支持区域限售
			    	map.put("stocks", sku.getStock().toString());
			    }
			}else{
				map.put("stocks", sku.getStock().toString());
			}
		}
		return map;
	}

	/**
	 * 根据skuid查出每个仓库中的库存
	 * @param skuId
	 * @return
	 */
	@Override
	public List<UpdSkuStockDto> getStockBySkuId(Long skuId){
		return productDao.getStockBySkuId(skuId);
	}

	/**
	 * 更新sku库存
	 */
	@Override
	public int updSkuStockDto(Long skuId,Long skocks){

		Sku sku=skuDao.getSku(skuId);

		if(AppUtils.isBlank(sku)){
			return 0;
		}
		sku.setStocks(skocks);
		sku.setActualStocks(skocks);
		skuDao.update(sku);
		return 1;
	}

	/**
	 * 检测虚拟库存, 看看是否可以减库存
	 */
	@Override
	public boolean checkStocks(Long prodId,Long skuId, long basketCount) {
		Integer stocks = skuDao.getStocksByLockMode(skuId);
		if (stocks == null) {
			stocks = 0;
		}
		if (stocks - basketCount < 0) {
			return false;
		} else {
			return true;
		}
	}



	@Override
	public PageSupport<StockAdminDto> getStockcontrol(String curPageNO, String productName) {
		return productDao.getStockcontrol(curPageNO,productName);
	}

	@Override
	public PageSupport<StockArmDto> getStockwarning(Long shopId, String curPageNO, String productName) {
		return productDao.getStockwarning(shopId,curPageNO,productName);
	}

	@Override
	public PageSupport<StockArmDto> getStockList(Long shopId, String curPageNO, int pageSize) {
		return productDao.getStockList(shopId,curPageNO,pageSize);
	}

	public void setStockLogDao(StockLogDao stockLogDao) {
		this.stockLogDao = stockLogDao;
	}
}
