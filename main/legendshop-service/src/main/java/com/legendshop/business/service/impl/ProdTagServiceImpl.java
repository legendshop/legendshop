package com.legendshop.business.service.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.ProdTagDao;
import com.legendshop.business.dao.ProdTagRelDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.ProdTag;
import com.legendshop.model.entity.ProdTagRel;
import com.legendshop.spi.service.ProdTagService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
/**
 * 商品标签实现类
 *
 */
@Service("prodTagService")
public class ProdTagServiceImpl implements ProdTagService{

	@Autowired
	private ProdTagDao prodTagDao;
	
	@Autowired
	private ProdTagRelDao prodTagRelDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Override
	@CacheEvict(value="ProdTag",key="#prodTag.id")
	public Long saveProdTag(ProdTag prodTag, String userName,String userId,Long shopId) {
		MultipartFile rightFile = prodTag.getRightFile();// 取得上传的文件
		MultipartFile bottomFile = prodTag.getBottomFile();// 取得上传的文件
		
        if (AppUtils.isNotBlank(prodTag.getId())) {
        	ProdTag orginProdTag = prodTagDao.getById(prodTag.getId());
        	
        	//判断shopId 是否一致
        	if(AppUtils.isBlank(shopId) && !orginProdTag.getShopId().equals(shopId)){
        		throw new BusinessException("shop id not correct");
        	}
        	if(!rightFile.isEmpty()){
        		//保存右上标签图片
            	orginProdTag.setRightTag(savePicture(rightFile,userName,userId, shopId, prodTag.getId()));
        	}
        	if(!bottomFile.isEmpty()){
        		//保存下横标签图片
            	orginProdTag.setBottomTag(savePicture(bottomFile,userName,userId, shopId, prodTag.getId()));
        	}
     		
    		orginProdTag.setName(prodTag.getName());    	
    		prodTagDao.update(orginProdTag);
    		this.updateProdTagCache(prodTag.getId());//更新缓存
            return prodTag.getId();
        }else{
        	String rFileName = null;
        	String bFileName = null;
        	
        	if ((rightFile != null) && (rightFile.getSize() > 0)) {
        		rFileName = attachmentManager.upload(rightFile);
				prodTag.setRightTag(rFileName);
			}
        	if ((bottomFile != null) && (bottomFile.getSize() > 0)) {
        		bFileName = attachmentManager.upload(bottomFile);
        		prodTag.setBottomTag(bFileName);
			}
        	
        	prodTag.setStatus(1L);        	
        	prodTag.setShopId(shopId);
        	Long prodTagId = prodTagDao.save(prodTag);
        	
        	if(AppUtils.isNotBlank(rFileName)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, rFileName, rightFile, AttachmentTypeEnum.SORT);
			}
        	if(AppUtils.isNotBlank(bFileName)){
				//保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId,bFileName, bottomFile, AttachmentTypeEnum.SORT);
			}
        	return prodTagId;
        }
	}
      
	/** 保存类目图片 **/
	private String savePicture(MultipartFile formFile,String userName,String userId, Long shopId, Long shopCatId){
		String filename= null;
		if ((formFile != null) && (formFile.getSize() > 0)) {
			filename = attachmentManager.upload(formFile);
			//保存附件表
			attachmentManager.saveImageAttachment(userName,userId, shopId,filename, formFile, AttachmentTypeEnum.SORT);
		}
		
		return filename;
	}

	@Override
	@Cacheable(value="ProdTag", key="#id")
	public ProdTag getById(Long id) {
		return prodTagDao.getById(id);
	}

	@Override
	@CacheEvict(value="ProdTag",key="#id")
	public boolean deleteProdTagById(Long id) {
		if(prodTagDao.deleteById(id) >= 1){
			this.updateProdTagCache(id);//更新缓存
			return true;
		}
		return false;
	}

	@Override
	@CacheEvict(value="ProdTag",key="#prodTag.id")
	public boolean updateByProdTag(ProdTag prodTag) {
		if(prodTagDao.update(prodTag) >= 1){
			this.updateProdTagCache(prodTag.getId());//更新缓存
			return true;
		}
		return false;
	}
	
	private void updateProdTagCache(Long tagId) {
		List<ProdTagRel> prodTagRelList = prodTagRelDao.queryProdTagRel(tagId);
		if(prodTagRelList !=null){
			for (ProdTagRel prodTagRel : prodTagRelList) {
				prodTagRelDao.updateProdTagCache(prodTagRel);
			}
		}
		
	}

	@Override
	@Cacheable(value="ProdTag", key="#tagId")
	public ProdTag getByIdAndStatus(Long tagId) {
		return prodTagDao.get("select * from ls_prod_tag where status = 1 and id = ?", ProdTag.class, tagId);	 
	}

	@Override
	public PageSupport<ProdTag> getProdTagManage(String curPageNO, ProdTag prodTag, Long shopId) {
		return prodTagDao.getProdTagManage(curPageNO,prodTag,shopId);
	}
	    
}
