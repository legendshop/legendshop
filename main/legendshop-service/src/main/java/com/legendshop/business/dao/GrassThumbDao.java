/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassThumb;

/**
 * The Class GrassThumbDao. 种草文章点赞表Dao接口
 */
public interface GrassThumbDao extends Dao<GrassThumb,Long>{

	/**
	 * 根据Id获取种草文章点赞表
	 */
	public abstract GrassThumb getGrassThumb(Long id);

	/**
	 *  根据Id删除种草文章点赞表
	 */
    public abstract int deleteGrassThumb(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassThumb(GrassThumb grassThumb);

	/**
	 * 保存种草文章点赞表
	 */
	public abstract Long saveGrassThumb(GrassThumb grassThumb);

	/**
	 *  更新种草文章点赞表
	 */		
	public abstract int updateGrassThumb(GrassThumb grassThumb);

	/**
	 * 分页查询种草文章点赞表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassThumb> queryGrassThumb(String curPageNO, Integer pageSize);
	
	public abstract Integer rsThumb(Long grassId, String uid);
	
	  /**
  	 *  根据grassId删除种草和发现文章点赞表
  	 */
     public void deleteGrassThumbByDiscoverId(Long grassId,String uid);

}
