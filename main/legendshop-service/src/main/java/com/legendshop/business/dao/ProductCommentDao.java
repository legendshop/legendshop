/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.biz.model.dto.AppQueryProductCommentsParams;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ProductCommentsQueryParams;
import com.legendshop.model.dto.ShopCommProdDto;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.model.form.ProductForm;
/**
 * 产品评论Dao.
 */
public interface ProductCommentDao extends GenericDao< ProductComment, Long> {

	public void deleteProductComment(Long prodId);

	public Long saveProductComment(ProductComment productComment);

	public void updateProductComment(ProductComment productComment);

	public void deleteProductCommentById(Long id);

	public ProductCommentCategory initProductCommentCategory(Long prodId);

	public abstract String validateComment(Long prodId, String userName);
	
	public abstract String getproductName(Long prodId);
	
	
	public abstract String getOwnerName(Long prodId);
	
	/**
	 * 收藏此商品的用户，还收藏其他什么商品
	 */
	public abstract List<ProductForm> getProductList(String userId,Long prodId);
	
	/**
	 * 根据prodId，查出ProductComment
	 */
	public abstract ProductComment  getProdCommentByprodId(Long subItemId);
	
	/**
	 * 更新有用计数
	 * @param prodComId
	 */
	public abstract int updateUsefulCounts(Long prodComId,String userId);

	public boolean canCommentThisProd(Long prodId, Long subItemId, String userId);
	
	public Long setGoodComments(Long prodId);
	
	public Long getComments(Long prodId);

	public int updateIsAddComm(boolean isAddComm, Long prodCommId);

	public ProductComment getProductComment(Long commId, String userId);

	public ProductCommentDto getProductCommentDetail(Long prodComId);

	PageSupport<ProductReply> replyCommentList(String curPageNO, Long parentReplyId);

	PageSupport<ProductComment> prodComment(String curPageNO, Long subId, String userName);

	PageSupport<ProductComment> prodCommentState(String curPageNO, String state, String userName);

	public PageSupport<ShopCommProdDto> queryProdComment(String curPageNO, Long shopId);

	public PageSupport<ProductCommentDto> queryProductCommentDto(ProductCommentsQueryParams params);

	public PageSupport<ProductCommentDto> getProductCommentList(String curPageNO, ProductComment productComment,
			Integer pageSize, String status, DataSortResult result);

	public PageSupport<ProductCommentDto> getProductCommentListPage(String curPageNO, ProductComment productComment,
			String userId, String userName, Integer pageSize, DataSortResult result);
	
	public PageSupport<ProductComment> querySimplePage(String curPageNO, String userName, Long subId);

	public PageSupport<ProductComment> querySimplePage(String curPageNO, String userName);

	/** 待回复评论 */
	public Integer getWaitReplyShopComm(Long prodId);

	PageSupport getProdCommentList(Long shopId, String curPageNO, int pageSize);

	PageSupport getProdComment(AppQueryProductCommentsParams params, int pageSize);

	PageSupport<UserCommentProdDto> queryComments(String userName, String curPageNO, int pageSize);

	/**
	 * 根据状态获取用户评论列表
	 */
	PageSupport<ProductCommentDto> queryProdCommentByState(String curPageNO, String state, String userName);

	/**
	 * 判断用户是否评论过商品
	 * @param prodId
	 * @param userId
	 * @param itemId
	 * @return
	 */
	Long findByProdIdandUserIdandItemId(Long prodId, String userId, Long itemId);
}