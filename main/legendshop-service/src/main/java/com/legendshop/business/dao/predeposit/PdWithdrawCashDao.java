/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.predeposit;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdWithdrawCash;

/**
 * 预存款提现Dao接口
 * 
 */
public interface PdWithdrawCashDao extends GenericDao<PdWithdrawCash, Long> {

	/** 获取预存款提现 */
	public abstract PdWithdrawCash getPdWithdrawCash(Long id);

	/** 删除预存款提现 */
	public abstract int deletePdWithdrawCash(PdWithdrawCash pdWithdrawCash);

	/** 保存预存款提现 */
	public abstract Long savePdWithdrawCash(PdWithdrawCash pdWithdrawCash);

	/** 更新预存款提现 */
	public abstract int updatePdWithdrawCash(PdWithdrawCash pdWithdrawCash);

	/** 获取预存款提现 */
	public abstract PdWithdrawCash getPdWithdrawCash(Long id, String userId);

	/** 获取预存款提现页面 */
	public abstract PageSupport<PdWithdrawCash> getPdWithdrawCashPage(String curPageNO, String userId, String state);

	/** 获取预存款提现列表页面 */
	public abstract PageSupport<PdWithdrawCash> getPdWithdrawCashListPage(String curPageNO, PdWithdrawCash pdWithdrawCash, DataSortResult result,
			Integer pageSize);

	/** 获取余额提现 */
	public abstract PageSupport<PdWithdrawCash> getFindBalanceWithdrawal(String curPageNO, String pdcSn, Integer status, String userId);

	PageSupport getPdWithdrawCashByState(String userId,String state, String curPageNO, Integer pageSize);
}
