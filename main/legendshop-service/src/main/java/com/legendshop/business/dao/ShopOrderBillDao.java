/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.Date;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;

/**
 * 商家订单结算Dao.
 */
public interface ShopOrderBillDao extends Dao<ShopOrderBill, Long> {


	/**
	 * Gets the shop order bill.
	 *
	 * @param id the id
	 * @return the shop order bill
	 */
	public abstract ShopOrderBill getShopOrderBill(Long id);
	
    /**
     * Delete shop order bill.
     *
     * @param shopOrderBill the shop order bill
     * @return the int
     */
    public abstract int deleteShopOrderBill(ShopOrderBill shopOrderBill);
	
	/**
	 * Save shop order bill.
	 *
	 * @param shopOrderBill the shop order bill
	 * @return the long
	 */
	public abstract Long saveShopOrderBill(ShopOrderBill shopOrderBill);
	
	/**
	 * Update shop order bill.
	 *
	 * @param shopOrderBill the shop order bill
	 * @return the int
	 */
	public abstract int updateShopOrderBill(ShopOrderBill shopOrderBill);

	/**
	 * Gets the last shop order bill by shop id.
	 *
	 * @param shopId the shop id
	 * @return the last shop order bill by shop id
	 */
	public abstract ShopOrderBill getLastShopOrderBillByShopId(Long shopId);

	/**
	 * Gets the shop order bill page.
	 *
	 * @param curPageNO the cur page no
	 * @param shopOrderBill the shop order bill
	 * @return the shop order bill page
	 */
	public abstract PageSupport<ShopOrderBill> getShopOrderBillPage(String curPageNO, ShopOrderBill shopOrderBill);

	/**
	 * Gets the shop order bill page.
	 *
	 * @param shopId the shop id
	 * @param curPageNO the cur page no
	 * @param shopOrderBill the shop order bill
	 * @return the shop order bill page
	 */
	public abstract PageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO,
			ShopOrderBill shopOrderBill);

	/**
	 * 查询该结算单是否已经存在
	 * @param shopId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public abstract boolean getShopOrderBill(Long shopId, Date startDate, Date endDate);

	PageSupport<ShopOrderBill> getShopOrderBill(String curPageNO, Long shopId, Integer status, int pageSize);
}
