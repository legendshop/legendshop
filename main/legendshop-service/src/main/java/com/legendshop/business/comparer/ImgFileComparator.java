package com.legendshop.business.comparer;

import java.util.Date;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.ImgFileDto;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.Product;
import com.legendshop.util.AppUtils;

public class ImgFileComparator implements DataComparer<ImgFileDto,ImgFile>{

	@Override
	public boolean needUpdate(ImgFileDto dto, ImgFile dbObj, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(ImgFileDto imgFileDto, ImgFile originImg) {
		
		if(AppUtils.isNotBlank(imgFileDto) && AppUtils.isNotBlank(originImg)){
			if(originImg.getFilePath().equals(imgFileDto.getImgUrl()) && originImg.getSeq().equals(imgFileDto.getSeq())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public ImgFile copyProperties(ImgFileDto imgFileDto, Object obj) {
		Product product = (Product)obj;
		
		ImgFile imgFile = new ImgFile();
		imgFile.setUserName(product.getUserName());
		imgFile.setFilePath(imgFileDto.getImgUrl());
		imgFile.setProductId(product.getProdId());
		imgFile.setFileSize(null);
		imgFile.setFileType(imgFileDto.getImgUrl().substring(imgFileDto.getImgUrl().lastIndexOf(".") + 1).toLowerCase());
		imgFile.setProductType((short) 1);
		imgFile.setStatus(Constants.ONLINE);
		imgFile.setUpoadTime(new Date());
		imgFile.setShopId(product.getShopId());
		imgFile.setSeq(imgFileDto.getSeq());
		return imgFile;
	}

}
