/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ThemeModuleProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 * 专题板块商品.
 */
@Repository
public class ThemeModuleProdDaoImpl extends GenericDaoImpl<ThemeModuleProd, Long> implements ThemeModuleProdDao  {
     
    @Override
	public List<ThemeModuleProd> getThemeModuleProdByModuleid(Long module_id){
   		return this.queryByProperties(new EntityCriterion().eq("themeModuleId", module_id));
    }

	@Override
	public ThemeModuleProd getThemeModuleProd(Long id){
		return getById(id);
	}
	
    @Override
	public int deleteThemeModuleProd(ThemeModuleProd themeModuleProd){
    	return delete(themeModuleProd);
    }
    
    @Override
	public int deleteThemeModuleProd(List<ThemeModuleProd> themeModuleProds) {
		return delete(themeModuleProds);
	}
    
    
	
	@Override
	public Long saveThemeModuleProd(ThemeModuleProd themeModuleProd){
		return save(themeModuleProd);
	}
	
	@Override
	public int updateThemeModuleProd(ThemeModuleProd themeModuleProd){
		return update(themeModuleProd);
	}

 }
