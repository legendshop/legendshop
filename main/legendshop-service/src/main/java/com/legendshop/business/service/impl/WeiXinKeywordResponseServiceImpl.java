/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeiXinKeywordResponseDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.WxMenuInfoypeEnum;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;
import com.legendshop.spi.service.WeiXinKeywordResponseService;
import com.legendshop.util.AppUtils;

/**
 * 微信关键字回复服务
 */
@Service("weiXinKeywordResponseService")
public class WeiXinKeywordResponseServiceImpl  implements WeiXinKeywordResponseService{

	@Autowired
    private WeiXinKeywordResponseDao weiXinKeywordResponseDao;

    public WeiXinKeywordResponse getWeiXinKeywordResponse(Long id) {
        return weiXinKeywordResponseDao.getWeiXinKeywordResponse(id);
    }

    public void deleteWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse) {
        weiXinKeywordResponseDao.deleteWeiXinKeywordResponse(weiXinKeywordResponse);
    }

    public Long saveWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse) {
    	 if(weiXinKeywordResponse.getMsgtype().equals(WxMenuInfoypeEnum.TEXT.value())){
    		 weiXinKeywordResponse.setTemplatename(null);
    		 weiXinKeywordResponse.setTemplateid(null);
 	      }else{
 	    	 weiXinKeywordResponse.setContent(null);
 	      }
    	if (!AppUtils.isBlank(weiXinKeywordResponse.getId())) {
        	WeiXinKeywordResponse response=weiXinKeywordResponseDao.getWeiXinKeywordResponse(weiXinKeywordResponse.getId());
        	response.setContent(weiXinKeywordResponse.getContent());
        	response.setKeyword(weiXinKeywordResponse.getKeyword());
        	response.setMsgtype(weiXinKeywordResponse.getMsgtype());
        	response.setPptype(weiXinKeywordResponse.getPptype());
        	response.setTemplateid(weiXinKeywordResponse.getTemplateid());
        	response.setTemplatename(weiXinKeywordResponse.getTemplatename());
            updateWeiXinKeywordResponse(weiXinKeywordResponse);
            return weiXinKeywordResponse.getId();
        }
        return weiXinKeywordResponseDao.saveWeiXinKeywordResponse(weiXinKeywordResponse);
    }

    public void updateWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse) {
        weiXinKeywordResponseDao.updateWeiXinKeywordResponse(weiXinKeywordResponse);
    }

	@Override
	public PageSupport<WeiXinKeywordResponse> getWeiXinnKeywordResponse(String curPageNO) {
		return weiXinKeywordResponseDao.getWeiXinnKeywordResponse(curPageNO);
	}
}
