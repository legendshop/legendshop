package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.NewsPosition;

public class NewsPositionComparer implements DataComparer<NewsPosition, NewsPosition> {

	@Override
	public boolean needUpdate(NewsPosition dto, NewsPosition dbObj, Object obj) {
		return false;
	}

	@Override
	public boolean isExist(NewsPosition dto, NewsPosition dbObj) {
		if(dto==null && dbObj==null){
		    return true;	
		}
		if(dto==null || dbObj==null){
			return false;
		}
		if(dto.getNewsid().equals(dbObj.getNewsid()) && dto.getPosition()==dbObj.getPosition()){
			return true;
		}
		return false;
	}

	@Override
	public NewsPosition copyProperties(NewsPosition dtoj, Object obj) {
		return dtoj;
	}


}
