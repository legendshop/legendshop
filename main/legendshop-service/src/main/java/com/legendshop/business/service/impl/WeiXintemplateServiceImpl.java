package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinImgFileDao;
import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.business.dao.WeixinNewstemplateDao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.WxMenuInfoypeEnum;
import com.legendshop.model.entity.weixin.WeixinImgFile;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;
import com.legendshop.spi.service.WeiXintemplateService;
import com.legendshop.util.AppUtils;

@Service("weiXintemplateService")
public class WeiXintemplateServiceImpl implements WeiXintemplateService {

	@Autowired
	private WeixinNewstemplateDao weixinNewstemplateDao;
	
	@Autowired
	private WeixinImgFileDao weixinImgFileDao;
	
	@Autowired
	private WeixinMenuExpandConfigDao weixinMenuExpandConfigDao;
	
	@Override
	public List<KeyValueEntity>  getTemplates(String msgType) {
		List<KeyValueEntity> entities=new ArrayList<KeyValueEntity>();
		if(WxMenuInfoypeEnum.TEXT.value().equals(msgType)){
			
		}else if(WxMenuInfoypeEnum.NEWS.value().equals(msgType)){
			 List<WeixinNewstemplate> list=weixinNewstemplateDao.getWeixinNewstemplate();
			 if(AppUtils.isNotBlank(list)){
				 for (int i = 0; i < list.size(); i++) {
					 WeixinNewstemplate newstemplate=list.get(i);
					 KeyValueEntity entity=new KeyValueEntity(String.valueOf(newstemplate.getId()), newstemplate.getTemplatename());
					 entities.add(entity);
				}
			 }
		}
		else if(WxMenuInfoypeEnum.IMAGE.value().equals(msgType)){
			 List<WeixinImgFile> list=weixinImgFileDao.getWeixinImgFile();
			 if(AppUtils.isNotBlank(list)){
				 for (int i = 0; i < list.size(); i++) {
					 WeixinImgFile file=list.get(i);
					 KeyValueEntity entity=new KeyValueEntity(String.valueOf(file.getId()), file.getName());
					 entities.add(entity);
				}
			 }
		}else if(WxMenuInfoypeEnum.EXPAND.value().equals(msgType)){
			List<WeixinMenuExpandConfig> configs=weixinMenuExpandConfigDao.getWeixinMenuExpandConfig();
			 if(AppUtils.isNotBlank(configs)){
				 for (int i = 0; i < configs.size(); i++) {
					 WeixinMenuExpandConfig config  =configs.get(i);
					 KeyValueEntity entity=new KeyValueEntity(String.valueOf(config.getId()), config.getName());
					 entities.add(entity);
				}
			 }
		}
		return entities;
	}
}
