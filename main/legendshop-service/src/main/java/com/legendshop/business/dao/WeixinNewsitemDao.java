/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * 微信新闻项目Dao
 */

public interface WeixinNewsitemDao extends Dao<WeixinNewsitem, Long> {
     
	public abstract WeixinNewsitem getWeixinNewsitem(Long id);
	
    public abstract int deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
	public abstract Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
	public abstract int updateWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
	public abstract List<WeixinNewsitem> getNewsItemsByTempId(Long tempId);

	public abstract void saveWeixinNewsitemList(List<WeixinNewsitem> addItems);

	public abstract void updateWeixinNewsitemList(List<WeixinNewsitem> updateItems);

	public abstract void deleteWeixinNewsitemByIds(List<Long> ids);

	public abstract PageSupport<WeixinNewsitem> getWeixinNewsitemPage(String curPageNO);
	
 }
