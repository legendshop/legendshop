/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import com.legendshop.base.config.SystemParameterUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendMailEvent;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.MailInfo;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponMsgTypeEnum;
import com.legendshop.model.constant.CouponSourceEnum;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.constant.SendCouponTypeEnum;
import com.legendshop.model.constant.UserCouponEnum;
import com.legendshop.model.dto.ProductCoupon;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.vo.UserCouponVo;
import com.legendshop.spi.service.FileParameterService;
import com.legendshop.spi.service.IntegraLogService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SystemUtil;
/**
 *用户优惠券服务
 */
@Service("userCouponService")
public class UserCouponServiceImpl  implements UserCouponService{

	private final static Logger LOGGER=LoggerFactory.getLogger(UserCouponServiceImpl.class);

	@Autowired
	private FileParameterService fileParameterService;
	
	@Autowired
	private UserCouponDao userCouponDao;

	@Autowired
	private CouponDao couponDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private IntegraLogService integraLogService;
	
	@Resource(name = "sendMailProcessor")
	private EventProcessor<MailInfo> sendMailProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PropertiesUtil propertiesUtil;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Override
	public List<UserCoupon> getUserCouponByCoupon(Long couponId) {
		return userCouponDao.getUserCouponByCoupon(couponId);
	}

	@Override
	public UserCoupon getUserCoupon(Long id) {
		return userCouponDao.getUserCoupon(id);
	}
	
	/**
     * 根据卡密查优惠券
     */
    @Override
    public UserCoupon getCouponByCouponPwd(String couponPwd){
    	UserCoupon coupon = userCouponDao.getCouponByCouponPwd(couponPwd);
    	return coupon;
    }
    
    /**
     * 获取用户已经获取同一种优惠券的数量
     * @param couponId
     * @return
     */
    @Override
    public int getUserCouponCount(String userId,long couponId){
    	Integer number = userCouponDao.getUserCouponCount(userId, couponId);
    	return number;
    }

	@Override
	public void deleteUserCoupon(UserCoupon userCoupon) {
		userCouponDao.deleteUserCoupon(userCoupon);
	}

	@Override
	public Long saveUserCoupon(UserCoupon userCoupon) {
		if (!AppUtils.isBlank(userCoupon.getUserCouponId())) {
			updateUserCoupon(userCoupon);
			return userCoupon.getUserCouponId();
		}
		return userCouponDao.saveUserCoupon(userCoupon);
	}

	@Override
	public void updateUserCoupon(UserCoupon userCoupon) {
		userCouponDao.updateUserCoupon(userCoupon);
	}

	@Override
	public String sendUserCnps(String userids, Long couponId) {
		if (couponId == null) {
			return "couponId不能为空";
		}
		if (AppUtils.isBlank(userids)) {
			return "赠送用户不能为空!";
		}
		Coupon coupon = couponDao.getCoupon(couponId);
		if (coupon == null) {
			return "找不到[" + couponId + "]礼券";
		}
		if (coupon.getCouponNumber() == 0) {
			return "[" + couponId + "]礼券初始化数量为0 !";
		}
		if (!coupon.getSendType().equals(SendCouponTypeEnum.ONLINE.value())) { // 不是该类型
			return "[" + couponId + "]礼券类型不正确!";
		}
		if (coupon.getStatus() == Constants.OFFLINE.intValue()) {
			return "[" + couponId + "]礼券已冻结!";
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date) {
			return "[" + couponId + "]礼券已过期!";
		}

		List<String> users = JSONUtil.getArray(userids, String.class);
		if (AppUtils.isNotBlank(users)) {
			 if (users.size() > coupon.getCouponNumber()) {
				return "赠送用户数量超出礼券初始化数量!";
			}
			List<UserCoupon> userCoupon = new ArrayList<UserCoupon>();
			List<UserDetail> userDetails=new ArrayList<UserDetail>();
			String prefix = coupon.getCouponNumPrefix();
			long cnps_number = 0;
			for (int i = 0; i < users.size(); i++) {
				String uid = users.get(i);
				UserDetail user = userDetailDao.getUserDetailById(uid);
				if(user==null){
					continue;
				}
				UserCoupon cou = new UserCoupon();
				cou.setUserId(uid);
				cou.setUserName(user.getUserName());
				cou.setCouponId(couponId);
				cou.setCouponName(coupon.getCouponName());
				cou.setUseStatus(1);
				// 生产劵号
				cou.setCouponSn(CommonServiceUtil.getRandomCouponNumberGenerator());
				cou.setGetTime(new Date());
				cou.setGetSources("平台赠送");
				userCoupon.add(cou);
				userDetails.add(user);
				cnps_number++;
			}

			if (AppUtils.isNotBlank(userCoupon) && cnps_number > 0) {
				// 保存券批次
				userCouponDao.batchSave(userCoupon);
				// 更新券动作
				coupon.setBindCouponNumber(cnps_number + coupon.getBindCouponNumber());
				coupon.setSendCouponNumber(cnps_number + coupon.getSendCouponNumber());
				coupon.setCouponNumber(coupon.getCouponNumber() - cnps_number);
				couponDao.updateCoupon(coupon);
				
				
				/**
				 * 发送通知信息
				 */
				StringBuilder str=new StringBuilder("");
				if(AppUtils.isNotBlank(coupon.getFullPrice()) && coupon.getFullPrice()!=0){
					str.append("满").append(coupon.getFullPrice());
				}
				if(str.toString()!=""){
					str.append("减").append(coupon.getOffPrice()).append("元");
				}else
				{
					str.append(coupon.getOffPrice()).append("元");
				}
				
				SendMessage(userDetails,coupon.getMessageType(),str.toString());
				
			}

		}
		return Constants.SUCCESS;
	}
	
	private void SendMessage(List<UserDetail> userDetails, String messageTyle, String test) {
		if (AppUtils.isBlank(userDetails) || AppUtils.isBlank(messageTyle)) {
			return;
		}
		LOGGER.info(" <! -- messageStyle [");
		String[] styles = messageTyle.split(",");
		if (AppUtils.isNotBlank(styles)) {
			for (int j = 0; j < styles.length; j++) {
				String str = styles[j];
				LOGGER.info(", "+ str);
				if (CouponMsgTypeEnum.INTERIOR.value().equals(str)) {
					LOGGER.info(" send systemmessage ");
					confirmationSystem(userDetails, test);
				} else if (CouponMsgTypeEnum.MAIL.value().equals(str)) {
					LOGGER.info(" send mail ");
					confirmationMail(userDetails, test);
				} else if (CouponMsgTypeEnum.SMS.value().equals(str)) {
					LOGGER.info(" send sms ");
					confirmationSMS(userDetails, test);
				}
			}
		}
		LOGGER.info(" -- >");
	}
	
	private void confirmationSystem(List<UserDetail> userDetails,String moneyStr){
		for (int i = 0; i < userDetails.size(); i++) {
			UserDetail detail = userDetails.get(i);
			if (detail == null) {
				continue;
			}
			String text = "尊敬的 " + detail.getNickName() + "恭喜您获得电子消费代金礼券[" + moneyStr + "]";
			try {
				sendSiteMessageProcessor.process(new SendSiteMsgEvent(detail.getUserName(), "恭喜您获得电子消费代金礼券", text).getSource());
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(" 发送礼券站内信通知失败，username {} ", detail.getUserName());
			}
		}
	}
	
	private void confirmationMail(List<UserDetail> userDetails,String moneyStr){
		for (int i = 0; i < userDetails.size(); i++) {
			UserDetail detail=userDetails.get(i);
			String text;
			String link =  propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/p/coupon";
			Map<String, String> values = new HashMap<String, String>();
			String name = null;
			if(AppUtils.isBlank(detail.getNickName())){
				name = detail.getUserName();
			}else{
				name =detail.getNickName();
			}
			values.put("#username#", name);
			values.put("#link#", link);
			values.put("#moneyStr#", moneyStr);
			String siteName  = systemParameterUtil.getSiteName();
			values.put("#siteName#", siteName);



			try {
				// TODO 修改邮箱页面到数据库，还未测试
//				String templateFilePath = SystemUtil.getSystemRealPath() + AttributeKeys.DOWNLOAD_PATH + "/mail/couponEmail.html";
//				text = AppUtils.convertTemplate(templateFilePath, values);
				text = fileParameterService.findByName("couponEmail.html").getValue();
				String sendName = systemParameterUtil.getMailName();
				if(AppUtils.isNotBlank(detail.getUserMail())){ //add 20160922
					sendMailProcessor.process(new SendMailEvent(detail.getUserMail(), "活动礼券", text,sendName, detail.getUserName(), MailCategoryEnum.COUPON.value()).getSource());
				}
			} catch (Exception e) {
				LOGGER.error("发送礼券邮件通知失败，请检查配置 , usermail {}", detail.getUserMail());
			}
		}
		
	}
	
	private void confirmationSMS(List<UserDetail> userDetails,String moneyStr){
		for (int i = 0; i < userDetails.size(); i++) {
			UserDetail detail=userDetails.get(i);
			try {
				String test="尊敬的 "+detail.getNickName()+"恭喜您获得电子消费代金礼券["+moneyStr+"]";
				//TODO TODO短信提醒恭喜您获得电子消费代金礼提醒  Jason 2018-2-26
//				EventHome.publishEvent(new SendSMSEvent(detail.getNickName(),detail.getUserMobile(), test, SMSTypeEnum.COUPON));
			} catch (Exception e) {
				LOGGER.error("发送礼券短信通知失败，请检查配置, usermobile {} ", detail.getUserMobile());
				e.printStackTrace();
			}
		}
	
	}

	/**
	 * 赠送用户
	 */
	@Override
	public boolean saveUserCoupon(List<UserCoupon> userCoupon, long cnps_number, Long couponId) {
		boolean config = false;
		if (AppUtils.isNotBlank(userCoupon) && cnps_number > 0) {
			Coupon coupon = couponDao.getCoupon(couponId);
			// 保存券批次
			userCouponDao.batchSave(userCoupon);
			// 更新券动作
			coupon.setBindCouponNumber(cnps_number + coupon.getBindCouponNumber());
			coupon.setSendCouponNumber(cnps_number + coupon.getSendCouponNumber());
			coupon.setCouponNumber(coupon.getCouponNumber() - cnps_number);
			couponDao.updateCoupon(coupon);
			config = true;
		}
		return config;
	}

	/**
	 * 线下发放
	 * 
	 * @param userCoupon
	 * @param cnps_number
	 * @param couponId
	 * @return
	 */
	@Override
	public boolean saveOffUserCoupon(List<UserCoupon> userCoupon, long cnps_number, Long couponId) {
		boolean config = false;
		if (AppUtils.isNotBlank(userCoupon) && cnps_number > 0) {
			Coupon coupon = couponDao.getCoupon(couponId);
			// 保存券批次
			userCouponDao.batchSave(userCoupon);
			// 更新券动作

			coupon.setSendCouponNumber(cnps_number + coupon.getSendCouponNumber());
			coupon.setCouponNumber(coupon.getCouponNumber() - cnps_number);
			couponDao.updateCoupon(coupon);
			config = true;
		}
		return config;
	}

	@Override
	public List<String> getSendUsers(String sql, Object[] praObjects) {
		return userCouponDao.getSendUsers(sql, praObjects);
	}

	@Override
	public List<UserCoupon> getOffExportExcelCoupon(Long coupon_id) {
		return userCouponDao.getOffExportExcelCoupon(coupon_id);
	}

	/**
	 * 
	 * @param title
	 *            表格标题名
	 * @param headers
	 *            表格属性列名数组
	 * @param dataset
	 *            需要显示的数据集合,集合中一定要放置符合javabean风格的类的对象。此方法支持的
	 *            javabean属性的数据类型有基本数据类型及String,Date,byte[](图片数据)
	 * @param out
	 *            与输出设备关联的流对象，可以将EXCEL文档导出到本地文件或者网络中
	 * @param pattern
	 *            如果有时间数据，设定输出格式。默认为"yyy-MM-dd"
	 */
	@Override
	public boolean exportExcel(String title, String[] headers, List<UserCoupon> dataset, OutputStream out, String pattern) {
		// 声明一个工作薄
		XSSFWorkbook workbook = new XSSFWorkbook();
	    XSSFSheet sheet = workbook.createSheet(title);
		// 生成一个表格
		// 设置表格默认列宽度为15个字节
		sheet.setDefaultColumnWidth(30);
		// 生成一个样式
		XSSFCellStyle style = workbook.createCellStyle();

		// 设置这些样式
		style.setFillForegroundColor(new XSSFColor(new Color(30, 130, 180)));   
		style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		

		// 生成一个字体
		
		XSSFFont font =createFont(workbook,(short)16);
		
		// 把字体应用到当前的样式
		style.setFont(font);
		// 生成并设置另一个样式
		XSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFillForegroundColor(new XSSFColor(new java.awt.Color(36, 142, 195)));
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style2.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style2.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style2.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

		// 生成另一个字体
		XSSFFont font2 = workbook.createFont();
		font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		// 把字体应用到当前的样式
		style2.setFont(font2);


		// 产生表格标题行
		XSSFRow row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			XSSFCell cell = row.createCell(i);
			cell.setCellStyle(style);
			XSSFRichTextString text = new XSSFRichTextString(headers[i]);
			cell.setCellValue(text);
		}

		List<String> cfields=new ArrayList<String>();
		cfields.add("couponId");
		cfields.add("couponName");
		cfields.add("couponSn");
		
		// 遍历集合数据，产生数据行
		Iterator<UserCoupon> it = dataset.iterator();
		int index = 0;
		while (it.hasNext()) {
			index++;
			row = sheet.createRow(index);
			UserCoupon t = it.next();
			// 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
			Field[] fields = t.getClass().getDeclaredFields();
			
			 int n=0;
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				String fieldName = field.getName();
				
				if(!cfields.contains(fieldName)){
					continue;
				}
				
				XSSFCell cell = row.createCell(n);
				cell.setCellStyle(style2);
				String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
				n++;
				try {
					Class tCls = t.getClass();
					Method getMethod = tCls.getMethod(getMethodName, new Class[] {});
					Object value = getMethod.invoke(t, new Object[] {});
					// 判断值的类型后进行强制类型转换
					String textValue = null;
					if (value instanceof Date) {
						Date date = (Date) value;
						SimpleDateFormat sdf = new SimpleDateFormat(pattern);
						textValue = sdf.format(date);
					}
					else {
                        // 其它数据类型都当作字符串简单处理
                        textValue = value.toString();
                    }
					
					if (textValue != null) {
						Pattern p = Pattern.compile("^//d+(//.//d+)?{1}quot;");
						Matcher matcher = p.matcher(textValue);
						if (matcher.matches()) {
							// 是数字当作double处理
							cell.setCellValue(Double.parseDouble(textValue));
						} else {
							XSSFRichTextString richString = new XSSFRichTextString(textValue);
							
							XSSFFont font3 = createFont(workbook,(short)14);
							
							richString.applyFont(font3);
							cell.setCellValue(richString);
						}
					}
				} catch (SecurityException e) {
					e.printStackTrace();
					return false;
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
					return false;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					return false;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					// TODO Auto-generated catch block
					return false;
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					return false;
				} finally {
					// 清理资源
				}
			}
		}
		try {
			workbook.write(out);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 创建字体
	 * @param workbook
	 * @param size 字体大小
	 * @return
	 */
	private static XSSFFont createFont(XSSFWorkbook workbook,short size){
	    XSSFFont font=workbook.createFont();
	    //字体样式
	    font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
	    //字体颜色
	    font.setColor(XSSFFont.COLOR_NORMAL);
	    //字体大小
	    if(0==size){
	        font.setFontHeightInPoints(XSSFFont.DEFAULT_FONT_SIZE);
	    }else{
	        font.setFontHeightInPoints(size);
	    }
	    font.setFontName("微软雅黑");
	    return font;
	}

	@Override
	public void betchUpdate(List<UserCoupon> userCoupon) {
		userCouponDao.betchUpdate(userCoupon);
	}

	/**
	 * 给用户绑定优惠券
	 * @param userCoupon
	 * @param userId
	 * @param userName
	 * @param cou
	 */
	@Override
	public void updateUsercouponProperty(UserCoupon userCoupon,String userId, String userName,Coupon cou) {
		 /**
		  * 1 修改usercoupon 绑定状态
		  * 2 修改coupon 绑定数量 
		  */
		if(userCoupon != null && userCoupon.getCouponId() >0){
			userCoupon.setUserName(userName);
			userCoupon.setUserId(userId);
			userCoupon.setGetTime(new Date());
			userCouponDao.updateUserCoupon(userCoupon);
			if(cou != null){
				cou.setBindCouponNumber(cou.getBindCouponNumber()+1);
				couponDao.updateCoupon(cou);
			}
		}
	}

	@Override
	public List<UserCoupon> getUnUseUserCouponsByCoupon(Long couponId) {
		return userCouponDao.queryByProperties(new EntityCriterion().eq("couponId", couponId).eq("useStatus", UserCouponEnum.UNUSED.value()));
	}

	@Override
	public void updateCouponBindNum(Coupon cou) {
		cou.setBindCouponNumber(cou.getBindCouponNumber() + 1);
		couponDao.updateCoupon(cou);
	}

	@Override
	public UserCoupon getUsercoupon(Long couponId, String userId) {
		return couponDao.getUserCoupon(couponId, userId);
	}

	@Override
	public UserCoupon getCouponByCouponSn(String couponSn) {
		UserCoupon coupon = userCouponDao.getCouponByCouponSn(couponSn);
    	return coupon;
	}

	@Override
	public List<ProductCoupon> getProdCouponList(String userId) {
		Map<Long,ProductCoupon> couponMap = new HashMap<Long,ProductCoupon>();
		List<ProductCoupon> dbProdCoupons = userCouponDao.getProdCouponList(userId);
		if(AppUtils.isNotBlank(dbProdCoupons)){
			for (ProductCoupon productCoupon : dbProdCoupons) {
				ProductCoupon prodCoupon = couponMap.get(productCoupon.getUserCouponId());
				if(prodCoupon==null){
					productCoupon.getProdIdList().add(productCoupon.getProdId());
					couponMap.put(productCoupon.getUserCouponId(), productCoupon);
				}else{
					prodCoupon.getProdIdList().add(productCoupon.getProdId());
				}
			}
		}
		return  new ArrayList<ProductCoupon>(couponMap.values());
	}

	@Override
	public List<UserCoupon> getAllActiveUserCoupons(String userId) {
		return userCouponDao.getAllActiveUserCoupons(userId);
	}

	@Override
	public List<UserCoupon> queryUnbindCoupon(Long couponId) {
		return this.userCouponDao.queryUnbindCoupon(couponId);
	}

	@Override
	public Integer BindCouponByUser(List<String> result, List<UserCoupon> userCoupons) {
		 Integer successCount=0;
		 List<UserDetail> users=this.findUserByUserMobile(result);//根据手机号码获取用户
		if(AppUtils.isNotBlank(users)){
			List<UserCoupon> saveUserCoupons = new ArrayList<UserCoupon>();
			for (int i = 0; i < users.size(); i++) {
				userCoupons.get(i).setUserId(users.get(i).getUserId());
				userCoupons.get(i).setUserName(users.get(i).getUserName());
				userCoupons.get(i).setGetTime(new Date());
				userCoupons.get(i).setGetSources("平台发放");
				saveUserCoupons.add(userCoupons.get(i));
			}
			successCount = userCouponDao.updateUserCoupon(saveUserCoupons);
		}
		return successCount;
	}
	
	public List<UserDetail> findUserByUserMobile(List<String> result){
		return this.userCouponDao.findUserByUserMobile(result);
	}

	@Override
	public void saveUserCoupons(List<UserCoupon> userCoupons) {
		this.userCouponDao.save(userCoupons);
	}

	@Override
	public PageSupport<UserCoupon> getUserCouponListPage(String curPageNO, Long id) {
		return userCouponDao.getUserCouponListPage(curPageNO,null,id);
	}

	@Override
	public PageSupport<UserCouponVo> getUserCoupon(String curPageNO, StringBuilder sql, StringBuilder sqlCount,
			List<Object> objects) {
		return userCouponDao.getUserCoupon(curPageNO,sql,sqlCount,objects);
	}

	@Override
	public List<String> getSendUsers(String curPageNO, StringBuilder sql, List<Object> objects) {
		return userCouponDao.getSendUsers(curPageNO,sql,objects);
	}

	@Override
	public PageSupport<UserCoupon> getUserCouponList(String curPageNO, Long id) {
		return userCouponDao.getUserCouponList(curPageNO,id);
	}

	@Override
	public PageSupport<UserCoupon> getWatchRedPacket(Long id, String curPageNO) {
		return userCouponDao.getWatchRedPacket(id,curPageNO);
	}


	/*
	 * 领取优惠卷
	 */
	@Override
	@CacheEvict(value="UnuseCouponCount",allEntries=true)
//	@CacheEvict(value = "UnuseCouponCount" ,key="#userId+'shop'")
	public String couponReceive(Long couponId, String userName, String userId) {
		if (AppUtils.isBlank(userName)) {
			return "还没登录";
		} else {
			Date date = new Date();
			Coupon c = couponDao.getCoupon(couponId);
			if (c == null || c.getStatus() == Constants.OFFLINE.intValue()) {
				return "当前券已失效，快寻找新目标吧~";
			} else if (date.getTime() > c.getEndDate().getTime()) {
				// resultMap.put("msg", "当前券已失效，快寻找新目标吧~");
				return "当前券已失效，快寻找新目标吧~";
			} else {
				if (c.getGetLimit() != null) {

					List<UserCoupon> userCouponList = userCouponDao.queryUserCouponList(userName, couponId);
					if (userCouponList != null && userCouponList.size() >= c.getGetLimit()) {
						return "领取失败，您该张券的领取量已超限额~";
					}
				}
				if (c.getBindCouponNumber() >= c.getCouponNumber()) {
					return "领取失败，当前券已领完，快寻找其他目标~";
				}
				UserCoupon userCoupon = null;
				UserDetail userDetail = this.userDetailDao.getUserDetailById(userId);
				//红包优惠券同步领取  红包需额外判断是否为指定商家，及shopid非空的问题
				if(AppUtils.isNotBlank(c.getShopId()) && AppUtils.isNotBlank(userDetail) && c.getShopId().equals(userDetail.getShopId())){
					return "您不能领取自己店铺的优惠券~";
				}
				if (AppUtils.isBlank(c.getNeedPoints())) {
					// 正常绑定 优惠券到用户
					String sn = null;
					sn = CommonServiceUtil.getRandomCouponNumberGenerator();
					if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
						userCoupon = new UserCoupon();
						userCoupon.setCouponId(c.getCouponId());
						userCoupon.setCouponName(c.getCouponName());
						userCoupon.setCouponSn(sn);
						userCoupon.setUseStatus(1);
						userCoupon.setGetTime(date);
						userCoupon.setUserName(userName);
						userCoupon.setUserId(userId);
						this.saveUserCoupon(userCoupon);//
						this.updateCouponBindNum(c);
					}
					return Constants.SUCCESS;
				} else {
					Integer score = userDetail.getScore();// 总积分
					if (score >= c.getNeedPoints()) {
						userDetail.setScore(score - c.getNeedPoints());
						this.userDetailDao.updateUserDetail(userDetail);
						String sn = null;
						sn = CommonServiceUtil.getRandomCouponNumberGenerator();
						if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
							userCoupon = new UserCoupon();
							userCoupon.setCouponId(c.getCouponId());
							userCoupon.setCouponName(c.getCouponName());
							userCoupon.setCouponSn(sn);
							userCoupon.setUseStatus(1);
							userCoupon.setGetTime(date);
							userCoupon.setUserName(userName);
							userCoupon.setUserId(userId);
							this.saveUserCoupon(userCoupon);
							this.updateCouponBindNum(c);

							IntegraLog integraLog = new IntegraLog();
							integraLog.setUserId(userId);
							integraLog.setLogDesc("用户兑换优惠券");
							integraLog.setUserMobile(userDetail.getUserMobile());
							integraLog.setNickName(userDetail.getNickName());
							integraLog.setAddTime(new Date());
							integraLog.setIntegralNum(-c.getNeedPoints());
							integraLog.setLogType(10);
							integraLogService.saveIntegraLog(integraLog);
						}
						return Constants.SUCCESS;
					} else {
						return "您的积分不够";
					}

				}
			}
		}
	}

	/* 领取优惠卷
	 */
	@Override
	public String mobileCouponReceive(Long couponId, String userName,String userId) {
		if (AppUtils.isBlank(userName)) {
			return "还没登录";
		} else {
			Date date = new Date();
			Coupon c = couponDao.getCoupon(couponId);
			if (c == null || c.getStatus() == Constants.OFFLINE.intValue()) {
				return "当前券已失效，快寻找新目标吧~";
			} else if (date.getTime() > c.getEndDate().getTime()) {
				// resultMap.put("msg", "当前券已失效，快寻找新目标吧~");
				return "当前券已失效，快寻找新目标吧~";
			} else {
				if (c.getGetLimit() != null) {
					List<UserCoupon> userCouponList = userCouponDao.queryUserCouponList(userName, couponId);
					if (userCouponList != null && userCouponList.size() >= c.getGetLimit()) {
						return "领取失败，您该张券的领取量已超限额~";
					}
				}
				if (c.getBindCouponNumber() >= c.getCouponNumber()) {
					return "领取失败，当前券已领完，快寻找其他目标~";
				}
				UserCoupon userCoupon = null;
				UserDetail userDetail = this.userDetailDao.getUserDetailById(userId);
				if (AppUtils.isNotBlank(c.getShopId()) && AppUtils.isNotBlank(userDetail)) {
					if(c.getShopId().equals(userDetail.getShopId())){
						return "您不能领取自己店铺的优惠券~";
					}
				}
				if (AppUtils.isBlank(c.getNeedPoints())) {
					// 正常绑定 优惠券到用户
					String sn = null;
					sn = CommonServiceUtil.getRandomCouponNumberGenerator();
					if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
						userCoupon = new UserCoupon();
						userCoupon.setCouponId(c.getCouponId());
						userCoupon.setCouponName(c.getCouponName());
						userCoupon.setCouponSn(sn);
						userCoupon.setUseStatus(1);
						userCoupon.setGetTime(date);
						userCoupon.setUserName(userName);
						userCoupon.setUserId(userId);
						this.saveUserCoupon(userCoupon);//
						this.updateCouponBindNum(c);
					}
					return "恭喜您领取成功";
				} else {
					Integer score = userDetail.getScore();// 总积分
					if (score >= c.getNeedPoints()) {
						userDetail.setScore(score - c.getNeedPoints());
						this.userDetailDao.updateUserDetail(userDetail);
						String sn = null;
						sn = CommonServiceUtil.getRandomCouponNumberGenerator();
						if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
							userCoupon = new UserCoupon();
							userCoupon.setCouponId(c.getCouponId());
							userCoupon.setCouponName(c.getCouponName());
							userCoupon.setCouponSn(sn);
							userCoupon.setUseStatus(1);
							userCoupon.setGetTime(date);
							userCoupon.setUserName(userName);
							userCoupon.setUserId(userId);
							this.saveUserCoupon(userCoupon);
							this.updateCouponBindNum(c);
						}
						return "恭喜您领取成功";
					} else {
						return "您的积分不够";
					}

				}
			}
		}
	}	
	
	/**
	 * 领取优惠卷
	 */
	@Override
	public Map<String, Object> newMobileCouponReceive(Coupon c, UserDetail userDetail) {
		Map<String, Object> map = new HashMap<>();
		if (AppUtils.isBlank(c.getNeedPoints())) {
			// 正常绑定 优惠券到用户
			String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
			if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
				UserCoupon userCoupon = new UserCoupon();
				userCoupon.setCouponId(c.getCouponId());
				userCoupon.setCouponName(c.getCouponName());
				userCoupon.setCouponSn(sn);
				userCoupon.setUseStatus(1);
				userCoupon.setGetTime(new Date());
				userCoupon.setGetSources(CouponSourceEnum.WEIXIN.value());
				userCoupon.setUserName(userDetail.getUserName());
				userCoupon.setUserId(userDetail.getUserId());
				userCouponDao.saveUserCoupon(userCoupon);//
				this.updateCouponBindNum(c);
			}
			map.put("status", Constants.SUCCESS);
			map.put("msg", "领取成功");
			map.put("couponSn", sn);
			return map;
		} else {
			Integer score = userDetail.getScore();// 总积分
			if (score > c.getNeedPoints()) {
				userDetail.setScore(score - c.getNeedPoints());
				userDetailDao.updateUserDetail(userDetail);
				String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
				if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
					UserCoupon userCoupon = new UserCoupon();
					userCoupon.setCouponId(c.getCouponId());
					userCoupon.setCouponName(c.getCouponName());
					userCoupon.setCouponSn(sn);
					userCoupon.setUseStatus(1);
					userCoupon.setGetTime(new Date());
					userCoupon.setUserName(userDetail.getUserName());
					userCoupon.setGetSources(CouponSourceEnum.WEIXIN.value());
					userCoupon.setUserId(userDetail.getUserId());
					userCouponDao.saveUserCoupon(userCoupon);
					this.updateCouponBindNum(c);
				}
				map.put("status", Constants.SUCCESS);
				map.put("msg", "领取成功");
				map.put("couponSn", sn);
				return map;
			} else {
				map.put("status", Constants.FAIL);
				map.put("msg", "对不起，领取失败, 您的积分不足！");
				return map;
			}

		}
	}
	
	/**
	 * app领取优惠卷
	 */
	@Override
	public Map<String, Object> newAppCouponReceive(Coupon c, UserDetail userDetail) {
		Map<String, Object> map = new HashMap<>();
		if(AppUtils.isNotBlank(userDetail.getShopId()) ){
			if(c.getShopId() == userDetail.getShopId()){
				map.put("status", Constants.FAIL);
				map.put("msg", "对不起，不能领取自己店铺的哦~");
				return map;
			}
		}
		if (AppUtils.isBlank(c.getNeedPoints())) {
			// 正常绑定 优惠券到用户
			String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
			if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
				UserCoupon userCoupon = new UserCoupon();
				userCoupon.setCouponId(c.getCouponId());
				userCoupon.setCouponName(c.getCouponName());
				userCoupon.setCouponSn(sn);
				userCoupon.setUseStatus(1);
				userCoupon.setGetTime(new Date());
				userCoupon.setGetSources(CouponSourceEnum.APP.value());
				userCoupon.setUserName(userDetail.getUserName());
				userCoupon.setUserId(userDetail.getUserId());
				userCouponDao.saveUserCoupon(userCoupon);//
				this.updateCouponBindNum(c);
			}
			map.put("status", Constants.SUCCESS);
			map.put("msg", "领取成功");
			map.put("couponSn", sn);
			return map;
		} else {
			Integer score = userDetail.getScore();// 总积分
			if (score > c.getNeedPoints()) {
				userDetail.setScore(score - c.getNeedPoints());
				userDetailDao.updateUserDetail(userDetail);
				String sn = CommonServiceUtil.getRandomCouponNumberGenerator();
				if (AppUtils.isBlank(this.getCouponByCouponSn(sn))) {
					UserCoupon userCoupon = new UserCoupon();
					userCoupon.setCouponId(c.getCouponId());
					userCoupon.setCouponName(c.getCouponName());
					userCoupon.setCouponSn(sn);
					userCoupon.setUseStatus(1);
					userCoupon.setGetTime(new Date());
					userCoupon.setUserName(userDetail.getUserName());
					userCoupon.setGetSources(CouponSourceEnum.APP.value());
					userCoupon.setUserId(userDetail.getUserId());
					userCouponDao.saveUserCoupon(userCoupon);
					this.updateCouponBindNum(c);
					
					//保存积分使用日志
					IntegraLog integraLog = new IntegraLog();
					integraLog.setUserId(userDetail.getUserId());
					integraLog.setLogDesc("用户兑换优惠券");
					integraLog.setAddTime(new Date());
					integraLog.setIntegralNum(-c.getNeedPoints());
					integraLog.setLogType(10);
					integraLogService.saveIntegraLog(integraLog);
				}
				map.put("status", Constants.SUCCESS);
				map.put("msg", "领取成功");
				map.put("couponSn", sn);
				return map;
			} else {
				map.put("status", Constants.FAIL);
				map.put("msg", "对不起，领取失败，您的积分不足！");
				return map;
			}

		}
	}

	@Override
	public UserCoupon getUserCouponBySn(String couponSn) {
		return userCouponDao.getUserCouponBySn(couponSn);
	}

	@Override
	public List<UserCoupon> queryUserCouponList(String userName, Long couponId) {
		return userCouponDao.queryUserCouponList(userName,couponId);
	}

	@Override
	public List<UserCoupon> getUserCoupons(Long couponId, String userId) {
		return userCouponDao.getUserCoupons(couponId, userId);
	}
}
