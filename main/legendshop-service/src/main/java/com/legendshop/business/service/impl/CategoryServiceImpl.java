/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CategoryDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SystemCacheEnum;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;

/**
 * 商品类目ServiceImpl.
 */
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CategoryManagerUtil categoryManagerUtil;
	
	@Autowired
	private  RedisTemplate<Object, Object> redisTemplate;
	
	@Autowired
	private PropertiesUtil propertiesUtil;

	public Category getCategory(Long id) {
		return categoryDao.getCategory(id);
	}

	@CacheEvict(value = "CategoryList", allEntries = true)
	public void deleteCategory(Category category) {
		categoryManagerService.delTreeNode(category);
	}

	/**
	 * 保存
	 */
	public Long saveCategory(Category category) {
		return categoryManagerService.addTreeNode(category);
	}

	@CacheEvict(value = "CategoryList", allEntries = true)
	public void updateCategory(Category category) {
		categoryManagerService.updateTreeNode(category);
	}

	/**
	 * 查看团购商品类目列表
	 */
	@Override
	public List<Category> getCategoryList(String type) {
		return categoryDao.getCategoryList(type);
	}

	/**
	 * 查看团购商品类目列表（未下线）
	 */
	@Override
	public List<Category> getCategoryListByStatus(String type) {
		return categoryDao.getCategoryListByStatus(type);
	}

	@Override
	public Set<CategoryDto> getAllCategoryByType(String type) {
		List<Category> categories = categoryDao.getCategory(type, null, null, 1, null);
		HashMap<String, CategoryDto> hashMap = getTreeIntoMap(categories);
		if (hashMap != null) {
			Set<CategoryDto> categoryDtos = new TreeSet<CategoryDto>(new Comparator<CategoryDto>() {
				public int compare(CategoryDto o1, CategoryDto o2) {
					if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
						return -1;
					} else if (o1.getSeq() <= o2.getSeq()) {
						return -1;
					} else {
						return 1;
					}
				}
			});
			Iterator<CategoryDto> it = hashMap.values().iterator();
			while (it.hasNext()) {
				CategoryDto categoryDto = (CategoryDto) it.next();
				long parentId = categoryDto.getParentId();
				String parentKeyId = String.valueOf(parentId);
				if (hashMap.containsKey(parentKeyId)) {
					CategoryDto parentCategory = (CategoryDto) hashMap.get(parentKeyId);
					parentCategory.addChildCategory(categoryDto);
					categoryDto.setParentCategory(parentCategory);
				} else if (categoryDto.getParentId() == 0) {
					categoryDtos.add(categoryDto);
				}
			}
			return categoryDtos;
		}
		return null;
	}

	// 组装树
	private HashMap<String, CategoryDto> getTreeIntoMap(List<Category> categories) {
		if (AppUtils.isNotBlank(categories)) {
			HashMap<String, CategoryDto> nodeMap = new HashMap<String, CategoryDto>();
			Iterator<Category> it = categories.iterator();
			while (it.hasNext()) {
				Category category = (Category) it.next();
				long id = category.getId();
				String keyId = String.valueOf(id);
				CategoryDto categoryDto = build(category);
				nodeMap.put(keyId, categoryDto);
			}
			return nodeMap;
		}
		return null;
	}

	/**
	 * 传入节点 得到该所有的子节点
	 * 
	 * @param parentId
	 * @param navigationMenu
	 * @param type
	 * @param status
	 * @return
	 */
	@Override
	public List<Category> getAllChildrenByFunction(Long parentId, Integer navigationMenu, String type, Integer status) {
		return categoryManagerUtil.getAllChildrenByFunction(parentId, navigationMenu, type, status);
	}

	/**
	 * 组装 CategoryDto对象
	 * 
	 * @param category
	 * @return
	 */
	private CategoryDto build(Category category) {
		return new CategoryDto(category.getId(), category.getParentId(), category.getName(), category.getTypeId(), category.getKeyword(), category.getTitle(),
				category.getCatDesc(), category.getSeq(), category.getGrade());
	}

	@Override
	public List<Category> findCategory(Long parentId) {
		return categoryDao.findCategory(parentId);
	}

	@Override
	public List<Category> findCategory(String cateName, Integer grade, Long parentId) {
		return categoryDao.findCategory(cateName, grade, parentId);
	}

	@Override
	public List<Category> getCategoryByIds(List<Long> catIdList) {

		return categoryDao.getCategoryByIds(catIdList);
	}

	@Override
	public void updateCategoryList(List<Category> categorys) {
		categoryDao.updateCategoryList(categorys);
	}

	@Override
	public List<Category> getCategorysByTypeId(Integer typeId) {
		return categoryDao.getCategorysByTypeId(typeId);
	}

	@Override
	public List<Category> queryCategoryList() {
		return categoryDao.queryCategoryList();
	}

	/**
	 * 更新内存树
	 */
	@Override
	@Caching(evict = { 
			@CacheEvict(value = "CategoryList", allEntries = true), 
			@CacheEvict(value = "NavigationCategoryList", allEntries = true), 
			@CacheEvict(value = "PAGE_CACHE", key = "'NAV'")})//更新导航页缓存
	public void cleanCategoryCache() {
		push(new CacheMessage(SystemCacheEnum.CATEGORY.value(), null));
	}

	/**
	 * 更新其他节点
	 * @param message
	 */
	private void push(CacheMessage message) {
		redisTemplate.convertAndSend(propertiesUtil.getSystemTopic(), message);
	}
	
	@Override
	public List<Category> getCategoryListByName(String categoryName) {
		return categoryDao.getCategoryListByName(categoryName);
	}

	@Override
	public List<Category> queryCategoryList(String type, Integer grade) {
		return categoryDao.getCategory(type, null, null, Constants.ONLINE, grade);
	}

	@Override
	public PageSupport<Category> getCategoryPage(String curPageNO) {
		return categoryDao.getCategoryPage(curPageNO);
	}

	@Override
	public List<Category> getCategory(String sortType, Integer headerMenu, Integer navigationMenu, Integer status, Integer level) {
		return categoryDao.getCategory(sortType, headerMenu, navigationMenu, status, level);
	}

	@Override
	public List<Category> findProdTypeId(Integer prodTypeId) {
		return categoryDao.findProdTypeId(prodTypeId);
	}

	@Override
	public List<Category> findCategoryByGradeAndParentId(Integer grade, Long parentId) {
		return categoryDao.findCategoryByGradeAndParentId(grade, parentId);
	}

}
