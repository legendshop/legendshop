package com.legendshop.business.dao;

import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.BankCallbackForm;

/**
 * 支付回调策略
 *
 */
public interface OrderCallbackStrategy {
	
	/**
	 * 支付回调
	 * @param subSettlement
	 * @param bankCallback
	 */
	public void callback(SubSettlement subSettlement,BankCallbackForm bankCallback);

	/**
	 * 预存款回调
	 * @param settlement
	 */
	public void predepositPayCallback(SubSettlement settlement);
	
}
