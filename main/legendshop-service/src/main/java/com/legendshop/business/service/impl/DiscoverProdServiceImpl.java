/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DiscoverProdDao;
import com.legendshop.model.entity.DiscoverProd;
import com.legendshop.spi.service.DiscoverProdService;

/**
 * The Class DiscoverProdServiceImpl.
 *  发现文章商品关联表服务实现类
 */
@Service("discoverProdService")
public class DiscoverProdServiceImpl implements DiscoverProdService{

    /**
     *
     * 引用的发现文章商品关联表Dao接口
     */
	@Autowired
    private DiscoverProdDao discoverProdDao;
   
   	/**
	 *  根据Id获取发现文章商品关联表
	 */
    public DiscoverProd getDiscoverProd(Long id) {
        return discoverProdDao.getDiscoverProd(id);
    }
    
    /**
	 *  根据Id删除发现文章商品关联表
	 */
    public int deleteDiscoverProd(Long id){
    	return discoverProdDao.deleteDiscoverProd(id);
    }

   /**
	 *  删除发现文章商品关联表
	 */ 
    public int deleteDiscoverProd(DiscoverProd discoverProd) {
       return  discoverProdDao.deleteDiscoverProd(discoverProd);
    }

   /**
	 *  保存发现文章商品关联表
	 */	    
    public Long saveDiscoverProd(DiscoverProd discoverProd) {
        if (!AppUtils.isBlank(discoverProd.getId())) {
            updateDiscoverProd(discoverProd);
            return discoverProd.getId();
        }
        return discoverProdDao.saveDiscoverProd(discoverProd);
    }

   /**
	 *  更新发现文章商品关联表
	 */	
    public void updateDiscoverProd(DiscoverProd discoverProd) {
        discoverProdDao.updateDiscoverProd(discoverProd);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverProd> queryDiscoverProd(String curPageNO, Integer pageSize){
     	return discoverProdDao.queryDiscoverProd(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setDiscoverProdDao(DiscoverProdDao discoverProdDao) {
        this.discoverProdDao = discoverProdDao;
    }

	@Override
	public List<DiscoverProd> getByDisId(Long id) {
		  return discoverProdDao.getByDisId(id);
	}


	@Override
	public int deleteByDisId(Long disId) {
		return discoverProdDao.deleteByDisId(disId);
	}
    
}
