/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProdReportDto;
import com.legendshop.model.dto.ReportDto;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.model.report.PurchaseRankDto;
import com.legendshop.model.report.ReportRequest;
import com.legendshop.model.report.SaleAnalysisDto;
import com.legendshop.model.report.SaleAnalysisResponse;
import com.legendshop.model.report.SalerankDto;
import com.legendshop.model.report.SalesResponse;

/**
 * 报表接口类.
 */
public interface ReportDao{

	/**
	 * 销售分析.
	 *
	 * @param saleAnalysisDto the sale analysis dto
	 * @return the sale analysis
	 */
	public abstract SaleAnalysisResponse getSaleAnalysis(SaleAnalysisDto saleAnalysisDto);
	
	/**
	 * Gets the sales.
	 *
	 * @param salesRequest the sales request
	 * @return the sales
	 */
	public abstract SalesResponse getSales(ReportRequest salesRequest);

	/** 根据 商城名称,开始时间和结束时间, 获取出订单个数  **/
	public List<ReportDto> getOrderQuantity(ReportRequest reportRequest);
	
	/** 根据商城Id 获取  最近30天的销售走势 **/
	public List<ReportDto> queryShopSales(Long shopId);
	
	/** 获取最近30天的订单数量 **/
	public List<Integer> querySubCounts(Long shopId);
	
	/** 根据商品ID 查看 最近30天的 商品销售走势,销售数量走势 **/
	public List<ProdReportDto> getProdTrend(Long shopId, Long prodId, Date startDate, Date endDate);
	
	/** 获取 每个城市的 订单数量 **/
	public List<KeyValueEntity> getCityOrderQuantity(ReportRequest reportRequest);

	/** 获取 每个城市（直辖市）的 订单数量 **/
	public List<KeyValueEntity> getCityOrderQuantitySpecial(ReportRequest reportRequest);
	
	/** 根据 用户自定义的价格区间，和 选择的日期 获取 订单数量 **/
	public List<ReportDto> getPurchaseAnalysis(List<PriceRange> priceRangeList,ReportRequest reportRequest);
	
	/** 获取 购买时段 **/
	public List<ReportDto> getPurchaseTime(ReportRequest reportRequest);
	
	/** 获取 流量统计 **/
	public List<ReportDto> getTrafficStatistics(ReportRequest reportRequest);
	
	/** 运营报表 销售统计 **/
	public List<ReportDto> getOperatingReport(ReportRequest reportRequest);

	public abstract PageSupport<SalerankDto> querySaleRank(SalerankDto salerankDto, String curPageNO, Integer pageSize,
			DataSortResult result);

	public abstract PageSupport<PurchaseRankDto> queryPurchaseRankDto(String curPageNO, PurchaseRankDto purchaserankDto,
			Integer pageSize, DataSortResult result);
}
