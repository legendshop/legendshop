/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SensitiveWord;

/**
 * 敏感字过滤Dao
 */
public interface SensitiveWordDao extends GenericDao<SensitiveWord, Long>{
     
	public abstract SensitiveWord getSensitiveWord(Long id);
	
    public abstract void deleteSensitiveWord(SensitiveWord sensitiveWord);
	
	public abstract Long saveSensitiveWord(SensitiveWord sensitiveWord);
	
	public abstract void updateSensitiveWord(SensitiveWord sensitiveWord);
	
	public abstract List<String> getWords();

	public abstract boolean checkSensitiveWordsNameExist(String nickName);

	public abstract PageSupport<SensitiveWord> getSensitiveWord(String curPageNO, SensitiveWord sensitiveWord);
	
 }
