/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopLayoutProdDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;
import com.legendshop.spi.service.ShopLayoutProdService;
import com.legendshop.util.AppUtils;

/**
 * 布局产品服务.
 */
@Service("shopLayoutProdService")
public class ShopLayoutProdServiceImpl  implements ShopLayoutProdService{
	
	@Autowired
    private ShopLayoutProdDao shopLayoutProdDao;

    public ShopLayoutProd getShopLayoutProd(Long id) {
        return shopLayoutProdDao.getShopLayoutProd(id);
    }

    public void deleteShopLayoutProd(ShopLayoutProd shopLayoutProd) {
        shopLayoutProdDao.deleteShopLayoutProd(shopLayoutProd);
    }

    public Long saveShopLayoutProd(ShopLayoutProd shopLayoutProd) {
        if (!AppUtils.isBlank(shopLayoutProd.getId())) {
            updateShopLayoutProd(shopLayoutProd);
            return shopLayoutProd.getId();
        }
        return shopLayoutProdDao.saveShopLayoutProd(shopLayoutProd);
    }

    public void updateShopLayoutProd(ShopLayoutProd shopLayoutProd) {
        shopLayoutProdDao.updateShopLayoutProd(shopLayoutProd);
    }


	@Override
	public void deleteShopLayoutProd(Long shopId, Long layoutId) {
		shopLayoutProdDao.deleteShopLayoutProd(shopId, layoutId);
	}
}
