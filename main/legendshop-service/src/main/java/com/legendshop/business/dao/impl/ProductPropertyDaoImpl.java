/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.ProdTypeDao;
import com.legendshop.business.dao.ProductPropertyDao;
import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.dao.PageSupportHandler;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 * 商品属性Dao.
 */
@Repository
public class ProductPropertyDaoImpl extends GenericDaoImpl<ProductProperty, Long> implements ProductPropertyDao {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductPropertyDaoImpl.class);
    
	@Autowired
    private ProductPropertyValueDao productPropertyValueDao;
    
	@Autowired
    private CategoryManagerUtil categoryManagerUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private ProdTypeDao prodTypeDao;
    
    private final String prodPropListquery ="select lpp.prop_id as propId, lpp.prop_name as propName, lpp.memo as memo, lpp.is_required as isRequired, lpp.is_multi as isMulti, " +
    		"lpp.sequence as sequence, lpp.status as status, lpp.type as type, lptp.seq as seq from ls_prod_prop lpp,ls_prod_type_prop lptp,ls_prod_type lpt " +
    		"where lpp.prop_id = lptp.prop_id and lptp.type_id = lpt.id and lpt.id = ? order by lptp.seq";
    
    private final String parameterPropertyListquery ="select lpp.prop_id as propId, lpp.prop_name as propName, lpp.memo as memo, lpp.is_required as isRequired, lpp.is_multi as isMulti, " +
    		"lpp.sequence as sequence, lpp.status as status, lpp.type as type, lptp.seq as seq, lptp.group_id as groupId, lpg.name as groupName from ls_prod_prop lpp," +
    		"ls_prod_type_param lptp left join ls_param_group lpg on lptp.group_id=lpg.id ,ls_prod_type lpt " +
    		"where lpp.prop_id = lptp.prop_id and lptp.type_id = lpt.id and lpt.id = ? order by lptp.seq";
    
    private final String querybrandListBySortId = "select b.brand_id as brandId, b.brand_name as brandName  from ls_sort s, ls_prod_type t, ls_prod_type_brand ptb, ls_brand b  " +
    		"where t.id = s.type_id and t.id = ptb.type_id and ptb.brand_id = b.brand_id  and s.sort_id = ? and b.status = 1";
    
    private final String querybrandListByTypeId = "SELECT b.brand_id AS brandId, b.brand_name AS brandName  FROM ls_brand b INNER JOIN ls_prod_type_brand ptb on ptb.brand_id=b.brand_id where b.status=1  AND  ptb.type_id=? ";
     
    private final String queryPropDtoList = "select prop_id as propId, prop_name as propName, is_required as isRequired, is_multi as isMulti, " +
			 "sequence as sequence, type as type, is_rule_attributes as isRuleAttributes, is_for_search as isForSearch," +
			 "is_input_prop as isInputProp, is_custom as isCustom from ls_prod_prop where prod_id = ?";
    
    private final String queryPropValueDtoList = "SELECT ppv.value_id AS valueId, ppv.prop_id AS propId, ppv.NAME AS NAME, ppv.pic AS pic FROM ls_prod_prop_value ppv,ls_prod_prop pp WHERE ppv.prop_id = pp.prop_id AND pp.prod_id = ? AND pp.is_custom = 1";
        
    private final String sqlForGetProductProperty1 = "select distinct pp.* from ls_prod_prop pp left join ls_prod_type_prop ptp on pp.prop_id = ptp.prop_id left join ls_prod_type pt on pt.id = ptp.type_id " +
			"left join ls_category c on c.type_id = ptp.type_id and c.id = ? where pp.prop_id in (";

    
    private final String sqlForGetProductProperty2 = ") order by ptp.seq";
    
    public List<ProductProperty> getProductProperty(String userName){
    	return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

    /**
     * TODO 要加上缓存
     */
	public ProductProperty getProductProperty(Long id){
		return getById(id);
	}
	
    public void deleteProductProperty(ProductProperty productProperty){
    	productPropertyValueDao.deleteProductPropertyValue(productProperty.getPropId());
    	delete(productProperty);
    }
	
    /**
     * 保存商品属性
     */
    @CacheEvict(value="ProductPropertyDtoList",allEntries=true)
	public Long saveProductProperty(ProductProperty productProperty,String userId, Long shopId){
		Long  propId = productProperty.getPropId();
        List<ProductPropertyValue> originList = productPropertyValueDao.queryProductPropertyValue(propId);
        if (AppUtils.isNotBlank(propId)) {//update
        	ProductProperty originProductProperty = this.getProductProperty(propId);
        	originProductProperty.setIsForSearch(productProperty.getIsForSearch());
        	originProductProperty.setIsInputProp(productProperty.getIsInputProp());
        	originProductProperty.setIsMulti(productProperty.getIsMulti());
        	originProductProperty.setIsRequired(productProperty.getIsRequired());
        	originProductProperty.setIsRuleAttributes(productProperty.getIsRuleAttributes());
        	originProductProperty.setPropName(productProperty.getPropName());
        	originProductProperty.setSequence(productProperty.getSequence());
        	originProductProperty.setType(productProperty.getType());
        	originProductProperty.setMemo(productProperty.getMemo());
            updateProductProperty(originProductProperty);
            //process  property 
           // productPropertyValueDao.deleteProductPropertyValue(propId);
            //save new property
            saveProductPropertyValue(productProperty, propId, originList, userId, shopId);
        }else{//save
        	productProperty.setRecDate(new Date());
        	productProperty.setModifyDate(new Date());
        	if(productProperty.getType() == null){
        		productProperty.setType(0); //文字
        	}
    	    propId =  (Long)save(productProperty);
    		saveProductPropertyValue(productProperty, propId, originList,userId, shopId);
        }
    	return propId;
	}

	private void saveProductPropertyValue(ProductProperty productProperty,Long propId, List<ProductPropertyValue> originList,String userId, Long shopId) {
		List<ProductPropertyValue> propValueList = productProperty.getValueList();
		List<ProductPropertyValue> toBeUpdateList = new ArrayList<ProductPropertyValue>(); //将要更新的的
		if(propValueList != null){
			for (ProductPropertyValue productPropertyValue : propValueList) {
				if(productPropertyValue.getValueId() != null){//原来的记录，将要update
					ProductPropertyValue toBeUpdateValue = queryToBeUpdatedPropValue(originList, productPropertyValue);
					if(toBeUpdateValue != null){
						toBeUpdateList.add(toBeUpdateValue);
						if(productPropertyValueDao.ifValueChanged(originList, productPropertyValue)){
							productPropertyValueDao.updateProductPropertyValue(productProperty.getUserName(),userId, shopId, toBeUpdateValue, productPropertyValue);
						}
					}

				}else{
					//保存新增记录
					productPropertyValue.setPropId(propId);
					productPropertyValueDao.saveProductPropertyValue(productProperty.getUserName(),userId, shopId, productPropertyValue);
				}

			}
		}
		//排除页面上的已有记录后，删除剩下的那些记录
		originList.removeAll(toBeUpdateList);
		if(AppUtils.isNotBlank(originList)){
			productPropertyValueDao.deleteProductPropertyValue(originList);
			for (ProductPropertyValue value : originList) {
				//删除原来的图片
				productPropertyValueDao.deleteProductPropertyPic(value.getPic());
			}

		}
		
	}
	
	/**
	 * 找出将要更新的记录
	 */
	private ProductPropertyValue queryToBeUpdatedPropValue(List<ProductPropertyValue> originList, ProductPropertyValue value){
		if(originList == null || value == null){
			return null;
		}
		for (int i = 0; i < originList.size(); i++) {
			ProductPropertyValue v = originList.get(i);
			if(v.getValueId().equals(value.getValueId())){
				return v;
			}
			
		}
		return null;
	}
	
	@CacheEvict(value = "ProductPropertyList", key = "#prodId")
	public void updateProductProperty(ProductProperty productProperty){
		productProperty.setModifyDate(new Date());
		if(productProperty.getType() == null){
			productProperty.setType(0);
		}
		 update(productProperty);
	}
	
	/**
	 * 加载属性和属性值
	 */
	public PageSupport getProductProperty(CriteriaQuery cq){
		return queryPage(cq, new PageSupportHandler<ProductProperty>() {
			@Override
			public void handle(List<ProductProperty> propertyList) {
	        	List<ProductPropertyValue> valueList = productPropertyValueDao.getAllProductPropertyValue(propertyList);
	        	for(ProductProperty property : propertyList){
	        		for(ProductPropertyValue  propertyValue : valueList){
	        			property.addProductPropertyValueList(propertyValue);
	        		}
	        	}
			}
		});
	}

	@Override
	public List<ProductProperty> getProductPropertyList(Long proTypeId) {
		return query(prodPropListquery,ProductProperty.class,proTypeId);
	}

	@Override
	public List<ProductProperty> getParameterPropertyList(Long proTypeId) {
		return query(parameterPropertyListquery,ProductProperty.class,proTypeId);
	}
	
	public List<ProductPropertyDto> getPublishProductDto(Long sortId, Long nsortId, Long subsortId){
		List<ProductPropertyDto> propDtoList=  query(ConfigCode.getInstance().getCode("prod.queryProdPropBySortId"),
				ProductPropertyDto.class,sortId,nsortId,subsortId);
		return propDtoList;
	}

    @Override
    public ProductPropertyValue getProductPropertyValue(Long propId, String propertiesValue) {
        return null;
    }

    /**
	 * 根据 categoryId 查找 分类下的类型id
	 * @param categoryId
	 * @return
	 */
	public PublishProductDto queryProductProperty(Long categoryId){
		Integer typeId = categoryManagerUtil.getRelationTypeId(categoryId);
		PublishProductDto publishProductDto = new PublishProductDto();
		if(AppUtils.isNotBlank(typeId)){
			ProdType prodType = prodTypeDao.getProdType(Long.parseLong(typeId.toString()));
			if(AppUtils.isNotBlank(prodType)){
				publishProductDto.setAttrEditable(prodType.getAttrEditable());
				publishProductDto.setParamEditable(prodType.getAttrEditable());
			}
		}
		if(AppUtils.isNotBlank(typeId)){
			//1. 装载规格属性  ProductPropertyTypeEnum.SALE_ATTR.value()) = 1
			List<ProductPropertyDto> propDtoList = query(ConfigCode.getInstance().getCode("prod.queryProdPropByTypeId"),ProductPropertyDto.class,typeId);
			Map<Long, ProductPropertyDto> specDtoMap= loadProdPropertiesValue(propDtoList);
			
			//2. 装载参数属性 // ProductPropertyTypeEnum.PARAM_ATTR.value()) = 2 
			List<ProductPropertyDto> paramDtoList=  query(ConfigCode.getInstance().getCode("prod.queryProdParamByTypeId"),ProductPropertyDto.class,typeId);
			Map<Long, ProductPropertyDto> productPropertyMap= loadProdPropertiesValue(paramDtoList);
			
			
			//3. 装载品牌
			List<BrandDto> brandList = query(querybrandListByTypeId,BrandDto.class,typeId);
			
			
			publishProductDto.setPropertyDtoList(new ArrayList<ProductPropertyDto>(productPropertyMap.values()));//商品参数
			publishProductDto.setSpecDtoList(new ArrayList<ProductPropertyDto>(specDtoMap.values()));//商品属性
			publishProductDto.setBrandList(brandList);
		}
		
		return publishProductDto;
	}
	
	/**
	 * 根据 categoryId 查找 分类下的类型id，从最低层找起，找到就返回第一个
	 * @param categoryId
	 * @return
	 */
	@Override
	@Cacheable(value="ProductPropertyDtoList",key="#categoryId")
	public List<ProductPropertyDto> findProductProper(Long categoryId){
		Integer typeId = categoryManagerUtil.getRelationTypeId(categoryId);
		if(AppUtils.isNotBlank(typeId)){
			//2. 装载参数属性 // ProductPropertyTypeEnum.PARAM_ATTR.value()) = 2 
			List<ProductPropertyDto> paramDtoList=  query(ConfigCode.getInstance().getCode("prod.queryProdParamByTypeId"),ProductPropertyDto.class,typeId);
			Map<Long, ProductPropertyDto> productPropertyMap= loadProdPropertiesValue(paramDtoList);
			return new ArrayList<>(productPropertyMap.values());
		}
		return null;
	}

	/**
	 * 装载分类下的属性和参数
	 * 
	 * TODO 需要修改分类，会影响这个功能
	 */
	@Override
	public PublishProductDto getProductProperty(Long sortId, Long nsortId, Long subsortId) {
		
		//1. 装载规格属性  ProductPropertyTypeEnum.SALE_ATTR.value()) = 1
		List<ProductPropertyDto> propDtoList=  query(ConfigCode.getInstance().getCode("prod.queryProdPropBySortId"),
				ProductPropertyDto.class,sortId,nsortId,subsortId);
		Map<Long, ProductPropertyDto> specDtoMap= loadProdPropertiesValue(propDtoList);
		
		
		//2. 装载参数属性 // ProductPropertyTypeEnum.PARAM_ATTR.value()) = 2
		List<ProductPropertyDto> paramDtoList=  query(ConfigCode.getInstance().getCode("prod.queryProdParamBySortId"),
				ProductPropertyDto.class,sortId, nsortId, subsortId);
		Map<Long, ProductPropertyDto> productPropertyMap= loadProdPropertiesValue(paramDtoList);
		
		//3. 装载品牌
		List<BrandDto> brandList = query(querybrandListBySortId,BrandDto.class,sortId);
		
		PublishProductDto publishProductDto = new PublishProductDto();
		publishProductDto.setPropertyDtoList(new ArrayList<ProductPropertyDto>(productPropertyMap.values()));//商品参数
		publishProductDto.setSpecDtoList(new ArrayList<ProductPropertyDto>(specDtoMap.values()));//商品属性
		publishProductDto.setBrandList(brandList);
		
		 return publishProductDto;
	}
	
	/**
	 * 加载属性下的值
	 * @param propertyDtoList
	 * @return
	 */
	private Map<Long, ProductPropertyDto> loadProdPropertiesValue(List<ProductPropertyDto> propertyDtoList){
		Map<Long, ProductPropertyDto> productPropertyMap= new LinkedHashMap<Long, ProductPropertyDto>();
		for (ProductPropertyDto productPropertyDto : propertyDtoList) {
			ProductPropertyDto ppd = productPropertyMap.get(productPropertyDto.getPropId());
			if(ppd == null){
				ppd = productPropertyDto;
				productPropertyMap.put(productPropertyDto.getPropId(), ppd);
			}
			ppd.addProductPropertyValueDto(productPropertyDto.getPropId(),productPropertyDto.getName(),productPropertyDto.getValueId(),productPropertyDto.getValuePic());
		}
		
		return productPropertyMap;
	}

	/**
	 * 只是获取ID值,并没有保存到数据库
	 */
	@Override
	public Long saveCustomProperty(ProductProperty newProperty) {
		return createId();
	}

	/**
	 * 获取用户自定义属性
	 */
	@Override
	public List<ProductPropertyDto> queryUserProp(Long prodId) {
		List<ProductPropertyDto> propDtoList = query(queryPropDtoList, ProductPropertyDto.class, prodId);
		if(AppUtils.isNotBlank(propDtoList)){
			List<ProductPropertyValueDto> propValueDtoList = query(queryPropValueDtoList,ProductPropertyValueDto.class,prodId);
			for(ProductPropertyDto propDto : propDtoList){
	    		for(ProductPropertyValueDto propValueDto : propValueDtoList){
	    			propDto.addProductPropertyValueList(propValueDto);
	    		}
	    	}
		}
		return propDtoList;
	}
	
	@Override
	@Cacheable(value = "ProductPropertyList", key = "#prodId")
	public List<ProductProperty> queryUserPropByProdId(Long prodId) {
		List<ProductProperty> propList = queryByProperties(new EntityCriterion().eq("prodId", prodId));
		List<ProductPropertyValue> valueList = productPropertyValueDao.getAllProductPropertyValue(propList);
		for(ProductProperty property : propList){
    		for(ProductPropertyValue  propertyValue : valueList){
    			property.addProductPropertyValueList(propertyValue);
    		}
    	}
		return propList;
	}

	@Override
	public List<ProductProperty> getProductProperty(List<Long> propIds, Long categoryId) {
		//return this.queryAllByIds(ids);
		StringBuffer sb = new StringBuffer(sqlForGetProductProperty1);
		List<Long> params = new ArrayList<Long>(propIds);
		params.add(0, categoryId);
		for (int i = 0; i < propIds.size(); i++) {
			sb.append("?,");
		}
		sb.setLength(sb.length() - 1);
		sb.append(sqlForGetProductProperty2);
		return query(sb.toString(), ProductProperty.class, params.toArray());
	}

	@Override
	public void deleteUserPropByProdId(Long prodId) {
		List<ProductProperty> propList = queryByProperties(new EntityCriterion().eq("prodId", prodId));
		
		for (ProductProperty productProperty : propList) {
			productPropertyValueDao.deleteProductPropertyValue(productProperty.getPropId());
		}
		
		delete(propList);
	}
	
	/**
	 * 更新缓存
	 */
	@CacheEvict(value = "ProductPropertyList", key = "#prodId")
	public void clearUserPropcache(Long prodId) {
		log.debug("Clear userProperties cache by prodId {}" , prodId);
	}

	@Override
	public ProductProperty getProductProperty(Long prodId, Long propId) {
		ProductProperty property = getByProperties(new EntityCriterion().eq("prodId", prodId).eq("propId", propId));
		return property;
	}

	@Override
	public List<ProductProperty> getProductProperty(List<Long> ids) {
		return this.queryAllByIds(ids);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductProperty.class, curPageNO);
    	QueryMap map = new QueryMap();
    	query.setPageSize(15);
    	map.put("isRuleAttributes", isRuleAttributes);
    	if(AppUtils.isNotBlank(propName)){
    		map.put("propName", "%"+propName+"%");
    	}
    	if(AppUtils.isNotBlank(memo)){
    		map.put("memo", "%"+memo+"%");
    	}
    	map.put("proTypeId", proTypeId);
    	query.setAllCountString(ConfigCode.getInstance().getCode("prod.queryProdPropertyCount", map));
    	query.setQueryString(ConfigCode.getInstance().getCode("prod.queryProdPropertyList",map));
    	query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyParamPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductProperty.class, curPageNO);
    	QueryMap map = new QueryMap();
    	query.setPageSize(15);
    	map.put("isRuleAttributes", isRuleAttributes);
    	if(AppUtils.isNotBlank(propName)){
    		map.put("propName", "%"+propName+"%");
    	}
    	if(AppUtils.isNotBlank(memo)){
    		map.put("memo", "%"+memo+"%");
    	}
    	map.put("proTypeId", proTypeId);
    	query.setAllCountString(ConfigCode.getInstance().getCode("prod.queryProdParamCount", map));
    	query.setQueryString(ConfigCode.getInstance().getCode("prod.queryProdParamList",map));
    	query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes) {
		CriteriaQuery cq = new CriteriaQuery(ProductProperty.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        cq.eq("isRuleAttributes", isRuleAttributes);
        cq.eq("isCustom", 0);  //不是用户自定义的属性和参数,是系统统一定义的属性
        cq.addDescOrder("modifyDate");
        cq.addAscOrder("sequence");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty) {
		 CriteriaQuery cq = new CriteriaQuery(ProductProperty.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        cq.eq("isRuleAttributes", isRuleAttributes);
        cq.eq("isCustom", 0);  //不是用户自定义的属性和参数,是系统统一定义的属性
        if(AppUtils.isNotBlank(productProperty.getPropName())) {      	
        	cq.like("propName", productProperty.getPropName().trim(), MatchMode.ANYWHERE);
        }
        if(AppUtils.isNotBlank(productProperty.getMemo())){        	
        	cq.like("memo", productProperty.getMemo().trim(), MatchMode.ANYWHERE);
        }
        cq.addDescOrder("modifyDate");
        cq.addAscOrder("sequence");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyByPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty) {
		CriteriaQuery cq = new CriteriaQuery(ProductProperty.class, curPageNO);
        cq.setPageSize(8);
		cq.eq("isCustom", 0);  //不是用户自定义的属性和参数,是系统统一定义的属性
        cq.eq("isRuleAttributes", isRuleAttributes);
        cq.like("propName", productProperty.getPropName(), MatchMode.START);
        cq.like("memo", productProperty.getMemo(), MatchMode.START);
        cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}

	@Override
	public Integer findProdId(Long propId) {
		Integer prodId = get("SELECT prod_id FROM ls_prod_prop WHERE prop_id = ?", Integer.class, propId);
		return prodId;
	}

    @Override
    public ProductProperty getProductProperty(Long prodId, String properties) {
        return getByProperties(new EntityCriterion().eq("propName",properties).eq("prodId",prodId));
    }
}
