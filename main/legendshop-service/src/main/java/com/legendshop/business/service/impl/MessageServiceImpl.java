/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.MessageDao;
import com.legendshop.business.dao.MsgTextDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.AdminMessageDto;
import com.legendshop.model.dto.AdminMessageDtoList;
import com.legendshop.model.dto.AdminMessageEnum;
import com.legendshop.model.entity.Msg;
import com.legendshop.model.entity.MsgText;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.spi.service.MessageService;
import com.legendshop.util.AppUtils;

/**
 * 站内信服务.
 */
@Service("messageService")
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageDao messageDao;
	
	@Autowired
	private MsgTextDao msgTextDao;


	@Override
	public void deleteOutbox(Long msgId) {
		messageDao.deleteOutbox(msgId);
	}
	
	@Override
	public void deleteInbox(Long msgId,String userName) {
		
		messageDao.deleteInbox(msgId,userName);
	}

	@Override
	public void saveSiteInformation(MsgText msgText,String userName,String receiveNames) {
		/**
		 * 先将文本内容保存到ls_msg_text表中
		 */
		Long terxtId = msgTextDao.save(msgText);

		/**
		 * 处理receiveName,然后再把发件信息存储到ls_msg
		 */

		String[] nameList = receiveNames.split(",");
		if (nameList.length == 1) {
			Msg msg = new Msg();
			msg.setSendName(userName);
			msg.setTextId(terxtId);
			msg.setStatus(0);
			msg.setIsGlobal(0);
			msg.setDeleteStatus(0);
			msg.setDeleteStatus2(0);
			msg.setReceiveName(receiveNames);
			messageDao.saveMsg(msg);
		} else {
			// 去重
			Set<String> nameSet = new HashSet<String>();
			for (int i = 0; i < nameList.length; i++) {
				String name = nameList[i];
				if (!nameSet.contains(name)) {
					nameSet.add(name);
					Msg msg = new Msg();
					msg.setSendName(userName);
					msg.setTextId(terxtId);
					msg.setStatus(0);
					msg.setIsGlobal(0);
					msg.setDeleteStatus(0);
					msg.setDeleteStatus2(0);
					msg.setReceiveName(name);
					messageDao.saveMsg(msg);
				}

			}
		}
	}

	@Override
	public SiteInformation getMsgText(String userName, Long msgId, boolean updateStatus) {
		SiteInformation siteInformation =  messageDao.getMsgText(msgId);
		
		//检查权限
		if(0 == siteInformation.getIsGlobal()){
			if(!siteInformation.getReceiveName().equals(userName) && !siteInformation.getSendName().equals(userName)){
				throw new BusinessException("Can not load  Msg, userName = " + userName + ", msgId = " + msgId);
			}
		}else{
			boolean result = messageDao.checkGlobalMsg(userName,msgId);
			if(!result){
				throw new BusinessException("Can not load System Msg, userName = " + userName + ", msgId = " + msgId);
			}
		}
		
		//更新状态
		if(updateStatus){
			messageDao.updateMsgRead(userName,msgId);
		}
		
		return siteInformation;
	}
	
	@Override
	public void updateMsgRead(String userName,Long msgId) {
		messageDao.updateMsgRead(userName,msgId);
	}

	@Override
	@Deprecated  //need to check
	public SiteInformation getSystemMessages(Long msgId) {
		return messageDao.getSystemMessages(msgId);
	}

	@Override
	public void saveSystemMessages(SiteInformation siteInformation, Integer[] userGradeArray) {
		if(AppUtils.isBlank(siteInformation.getMsgId())){
			messageDao.saveSystemMessages(siteInformation, userGradeArray);
		}else{
			//更新通知内容
			Long textId = messageDao.getTextId(siteInformation.getMsgId());
			messageDao.updateMsgText(siteInformation.getText(),siteInformation.getTitle(),textId);
			
			//更新用户等级选择
			messageDao.deleteUserGrade(siteInformation.getMsgId());
			messageDao.saveUserGrade(userGradeArray,siteInformation.getMsgId());
			
			//更新ls_msg的中文level
			messageDao.updateMsgLevel(userGradeArray,siteInformation.getMsgId());
		}
	}
	
	@Override
	public Integer getGradeId(String userName) {
		return messageDao.getGradeId(userName);
	}

	@Override
	@CacheEvict(value = "UnreadMsgCount", key = "#userName")
	public void updateSystemMsgStatus(String userName, Long msgId,int status) {
		messageDao.updateSystemMsgStatus(userName, msgId,status);
	}

	@Override
	public void deleteSystemMsgStatus(Long msgId) {
		messageDao.deleteSystemMsgStatus(msgId);
	}

	/**
	 * 检查昵称是否为空
	 */
	@Override
	public boolean isReceiverExist(String receiveName) {
		return messageDao.isReceiverExist(receiveName);
	}

	@Override
	public Integer calUnreadMsgCount(String userName) {
		return messageDao.calUnreadMsgCount(userName);
	}
	
	//
	@Override
	public Integer calUnreadMsgCount(String userName, Integer gradeId, Date userRegtime) {
		return messageDao.calUnreadMsgCount(userName, gradeId, userRegtime);
	}

	/**
	 * @Description: 删除
	 * @param @param msgId
	 * @param @param userName   
	 * @date 2016-8-11
	 */
	@Override
	public int deleteInboxByMsgIds(String[] msgIds, String userName) {
		
		return messageDao.deleteInboxByMsgIds(msgIds, userName);
	}

	@Override
	public void clearInboxMessage(String userName) {
		messageDao.clearInboxMessage(userName);
		messageDao.clearUnreadMsgCount(userName);
	}

	@Override
	public Long getCountById(String userName) {
		return messageDao.getCountById(userName);
	}

	@Override
	public int getCalUnreadSystemMessagesCount(String userName, Integer gradeId, Date userRegtime) {
		return messageDao.getCalUnreadSystemMessagesCount(gradeId, userName, userRegtime);
	}

	@Override
	public Msg getMessage(Long msgId) {
		return messageDao.getById(msgId);
	}
	
	@Override
	public PageSupport<SiteInformation> inboxSiteInformation(String userName, String curPageNO) {
		return messageDao.inboxSiteInformation(userName, curPageNO);
	}
	
	@Override
	public PageSupport<SiteInformation> outboxSiteInformation(String userName, String curPageNO) {
		return messageDao.outboxSiteInformation(userName, curPageNO);
	}
	
	@Override
	public PageSupport<SiteInformation> systemInformation(String userName,Integer gradeId,String curPageNO,UserDetail userDetail) {
		return messageDao.systemInformation(userName, gradeId, curPageNO,userDetail);
	}

	@Override
	public PageSupport<SiteInformation> query(String curPageNO, String userName) {
		return messageDao.querySimplePage(curPageNO,userName);
	}
	
	@Override
	public PageSupport<SiteInformation> querySiteInformation(String curPageNO, String title, Integer pageSize) {
		return messageDao.querySiteInformation(curPageNO,title,pageSize);
	}

	@Override
	public PageSupport<SiteInformation> queryMessage(String curPageNO, String userName) {
		return messageDao.queryMessage(curPageNO,userName);
	}

	@Override
	public PageSupport<SiteInformation> query(String curPageNO, Integer gradeId, String userName, UserDetail userDetail) {
		return messageDao.querySimplePage(curPageNO,gradeId,userName,userDetail);
	}
	
	/**
	 * 加到缓存里，20秒有效
	 */
	@Override
	@Cacheable(value = "AdminMessage")
	public AdminMessageDtoList getMessage() {
		
		Integer auditShopNumber = messageDao.getAuditShop();
		Integer productConsultNum = messageDao.getProductConsultCount();
		Integer auditProductNum = messageDao.getAuditProductCount();
		Integer auditBrandNum = messageDao.getAuditBrandCount();
		Integer returnGoodsNum = messageDao.getReturnGoodsCount();
		Integer returnMoneyNum = messageDao.getReturnMoneyCount();
		Integer accusationNum = messageDao.getAccusationCount();
		Integer shopBillNum = messageDao.getShopBillCount();
		Integer shopBillConfirmNum = messageDao.getShopBillConfirmCount();
		Integer auctionNum = messageDao.getAuctionCount();
		Integer productcommentNum = messageDao.getProductcommentCount();
		Integer seckillActivityNum = messageDao.getSeckillActivityCount();
		Integer groupNum = messageDao.getGroupCount();
		Integer presellProdNum = messageDao.getPresellProdCount();
		Integer userCommisNum = messageDao.getUserCommisCount();
		Integer userFeedBackNum = messageDao.getUserFeedBackCount();
		Integer totalNum = auditShopNumber+productConsultNum+auditProductNum+auditBrandNum+returnGoodsNum+returnMoneyNum
							+accusationNum+shopBillNum+shopBillConfirmNum+auctionNum+productcommentNum+seckillActivityNum
							+groupNum+presellProdNum+userCommisNum+userFeedBackNum;
		
		List<AdminMessageDto> adminMessageList = new ArrayList<AdminMessageDto>();
		AdminMessageDto adminMessageDto = new AdminMessageDto();
		adminMessageDto.setName(AdminMessageEnum.AUDITUING_SHOP.value());
		adminMessageDto.setUrl(AdminMessageEnum.AUDITUING_SHOP.url());
		adminMessageDto.setNum(auditShopNumber);
		adminMessageList.add(adminMessageDto);
		
		AdminMessageDto adminMessageDto2 = new AdminMessageDto();
		adminMessageDto2.setName(AdminMessageEnum.PROD_CONSULT.value());
		adminMessageDto2.setUrl(AdminMessageEnum.PROD_CONSULT.url());
		adminMessageDto2.setNum(productConsultNum);
		adminMessageList.add(adminMessageDto2);
		
		AdminMessageDto adminMessageDto3 = new AdminMessageDto();
		adminMessageDto3.setName(AdminMessageEnum.AUDITUING_PROD.value());
		adminMessageDto3.setUrl(AdminMessageEnum.AUDITUING_PROD.url());
		adminMessageDto3.setNum(auditProductNum);
		adminMessageList.add(adminMessageDto3);
		
		AdminMessageDto adminMessageDto4 = new AdminMessageDto();
		adminMessageDto4.setName(AdminMessageEnum.AUDITUING_BRAND.value());
		adminMessageDto4.setUrl(AdminMessageEnum.AUDITUING_BRAND.url());
		adminMessageDto4.setNum(auditBrandNum);
		adminMessageList.add(adminMessageDto4);
		
		AdminMessageDto adminMessageDto5 = new AdminMessageDto();
		adminMessageDto5.setName(AdminMessageEnum.RETURNGOODS.value());
		adminMessageDto5.setUrl(AdminMessageEnum.RETURNGOODS.url());
		adminMessageDto5.setNum(returnGoodsNum);
		adminMessageList.add(adminMessageDto5);
		
		AdminMessageDto adminMessageDto6 = new AdminMessageDto();
		adminMessageDto6.setName(AdminMessageEnum.RETURNMONEY.value());
		adminMessageDto6.setUrl(AdminMessageEnum.RETURNMONEY.url());
		adminMessageDto6.setNum(returnMoneyNum);
		adminMessageList.add(adminMessageDto6);
		
		AdminMessageDto adminMessageDto7 = new AdminMessageDto();
		adminMessageDto7.setName(AdminMessageEnum.ACCUSATION.value());
		adminMessageDto7.setUrl(AdminMessageEnum.ACCUSATION.url());
		adminMessageDto7.setNum(accusationNum);
		adminMessageList.add(adminMessageDto7);
		
		AdminMessageDto adminMessageDto8 = new AdminMessageDto();
		adminMessageDto8.setName(AdminMessageEnum.SHOP_BILL.value());
		adminMessageDto8.setUrl(AdminMessageEnum.SHOP_BILL.url());
		adminMessageDto8.setNum(shopBillNum);
		adminMessageList.add(adminMessageDto8);
		
		AdminMessageDto adminMessageDto9 = new AdminMessageDto();
		adminMessageDto9.setName(AdminMessageEnum.SHOP_BILL_CONFIRM.value());
		adminMessageDto9.setUrl(AdminMessageEnum.SHOP_BILL_CONFIRM.url());
		adminMessageDto9.setNum(shopBillConfirmNum);
		adminMessageList.add(adminMessageDto9);
		
		AdminMessageDto adminMessageDto10 = new AdminMessageDto();
		adminMessageDto10.setName(AdminMessageEnum.AUCTION.value());
		adminMessageDto10.setUrl(AdminMessageEnum.AUCTION.url());
		adminMessageDto10.setNum(auctionNum);
		adminMessageList.add(adminMessageDto10);
		
		AdminMessageDto adminMessageDto11 = new AdminMessageDto();
		adminMessageDto11.setName(AdminMessageEnum.SECKILL_ACTIVITY.value());
		adminMessageDto11.setUrl(AdminMessageEnum.SECKILL_ACTIVITY.url());
		adminMessageDto11.setNum(seckillActivityNum);
		adminMessageList.add(adminMessageDto11);
		
		AdminMessageDto adminMessageDto12 = new AdminMessageDto();
		adminMessageDto12.setName(AdminMessageEnum.GROUP.value());
		adminMessageDto12.setUrl(AdminMessageEnum.GROUP.url());
		adminMessageDto12.setNum(groupNum);
		adminMessageList.add(adminMessageDto12);
		
		AdminMessageDto adminMessageDto13 = new AdminMessageDto();
		adminMessageDto13.setName(AdminMessageEnum.PRESELL_PROD.value());
		adminMessageDto13.setUrl(AdminMessageEnum.PRESELL_PROD.url());
		adminMessageDto13.setNum(presellProdNum);
		adminMessageList.add(adminMessageDto13);
		
		AdminMessageDto adminMessageDto14 = new AdminMessageDto();
		adminMessageDto14.setName(AdminMessageEnum.USERCOMMIS.value());
		adminMessageDto14.setUrl(AdminMessageEnum.USERCOMMIS.url());
		adminMessageDto14.setNum(userCommisNum);
		adminMessageList.add(adminMessageDto14);
		
		AdminMessageDto adminMessageDto15 = new AdminMessageDto();
		adminMessageDto15.setName(AdminMessageEnum.USER_FEEDBACK.value());
		adminMessageDto15.setUrl(AdminMessageEnum.USER_FEEDBACK.url());
		adminMessageDto15.setNum(userFeedBackNum);
		adminMessageList.add(adminMessageDto15);
		
		AdminMessageDto adminMessageDto16 = new AdminMessageDto();
		adminMessageDto16.setName(AdminMessageEnum.PRODUCT_COMMENT.value());
		adminMessageDto16.setUrl(AdminMessageEnum.PRODUCT_COMMENT.url());
		adminMessageDto16.setNum(productcommentNum);
		adminMessageList.add(adminMessageDto16);
		
		AdminMessageDtoList adminMessageDtoList = new AdminMessageDtoList();
		adminMessageDtoList.setList(adminMessageList);
		adminMessageDtoList.setTotalMessage(totalNum);
		return adminMessageDtoList;
	}

	@Override
	public int batchDelete(String msgIds) {
		return messageDao.batchDelete(msgIds);
	}

	@Override
	public Integer getAuditShop() {
		return messageDao.getAuditShop();
	}

	@Override
	public Integer getProductConsultCount() {
		return messageDao.getProductConsultCount();
	}

	@Override
	public Integer getAuditProductCount() {
		return messageDao.getAuditProductCount();
	}

	@Override
	public Integer getAuditBrandCount() {
		return messageDao.getAuditBrandCount();
	}

	@Override
	public Integer getReturnGoodsCount() {
		return messageDao.getReturnGoodsCount();
	}

	@Override
	public Integer getReturnMoneyCount() {
		return messageDao.getReturnMoneyCount();
	}

	@Override
	public Integer getAccusationCount() {
		return messageDao.getAccusationCount();
	}

	@Override
	public Integer getShopBillCount() {
		return messageDao.getShopBillCount();
	}

	@Override
	public Integer getShopBillConfirmCount() {
		return messageDao.getShopBillConfirmCount();
	}

	@Override
	public Integer getUserCommisCount() {
		return messageDao.getUserCommisCount();
	}

	@Override
	public Integer getUserFeedBackCount() {
		return messageDao.getUserFeedBackCount();
	}

	@Override
	public Integer getAuctionCount() {
		return messageDao.getAuctionCount();
	}

	@Override
	public Integer getSeckillActivityCount() {
		return messageDao.getSeckillActivityCount();
	}

	@Override
	public Integer getGroupCount() {
		return messageDao.getGroupCount();
	}

	@Override
	public Integer getPresellProdCount() {
		return messageDao.getPresellProdCount();
	}

	@Override
	public Integer getProductcommentCount() {
		return messageDao.getProductcommentCount();
	}	
	@Override
	public Integer getUnReadMsgNum(String userName) {
		return messageDao.getUnReadMsgNum(userName);
	}

	@Override
	public PageSupport<SiteInformation> queryShopMessage(String curPageNO, String userName) {
		return messageDao.queryShopMessage(curPageNO, userName);
	}

}
