/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.DiscoverComme;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DiscoverCommeDao;

/**
 * The Class DiscoverCommeDaoImpl. 发现文章评论表Dao实现类
 */
@Repository
public class DiscoverCommeDaoImpl extends GenericDaoImpl<DiscoverComme, Long> implements DiscoverCommeDao{

	/**
	 * 根据Id获取发现文章评论表
	 */
	public DiscoverComme getDiscoverComme(Long id){
		return getById(id);
	}

	/**
	 *  删除发现文章评论表
	 *  @param discoverComme 实体类
	 *  @return 删除结果
	 */	
    public int deleteDiscoverComme(DiscoverComme discoverComme){
    	return delete(discoverComme);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteDiscoverComme(Long id){
		return deleteById(id);
	}

	/**
	 * 保存发现文章评论表
	 */
	public Long saveDiscoverComme(DiscoverComme discoverComme){
		return save(discoverComme);
	}

	/**
	 *  更新发现文章评论表
	 */		
	public int updateDiscoverComme(DiscoverComme discoverComme){
		return update(discoverComme);
	}

	/**
	 * 分页查询发现文章评论表列表
	 */
	public PageSupport<DiscoverComme>queryDiscoverComme(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(DiscoverComme.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<DiscoverComme> ps = queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<DiscoverComme> queryDiscoverCommeByGrassId(String currPage, int pageSize, Long disId) {
		CriteriaQuery query = new CriteriaQuery(DiscoverComme.class, currPage);
		query.setPageSize(pageSize);
		query.eq("disId",disId);
		query.addDescOrder("createTime");
		PageSupport<DiscoverComme> ps = queryPage(query);
		return ps;
	}

	@Override
	public List<DiscoverComme> getDiscoverCommeByDisId(Long disId) {
		String sql="SELECT  id,dis_id,user_id,content,create_time,`STATUS`,user_name,user_image FROM ls_discover_comm WHERE dis_id=?";
		return query(sql, DiscoverComme.class,disId);
	}

}
