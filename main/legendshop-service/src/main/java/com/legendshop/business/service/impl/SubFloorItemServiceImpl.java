/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.business.dao.SubFloorItemDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.model.floor.ProductFloor;
import com.legendshop.spi.service.SubFloorItemService;
import com.legendshop.util.AppUtils;

/**
 * 首页楼层服务
 */
@Service("subFloorItemService")
public class SubFloorItemServiceImpl  implements SubFloorItemService{

	@Autowired
    private SubFloorItemDao subFloorItemDao;
    
	@Autowired
    private FloorItemDao floorItemDao;
    
	@Autowired
    private FloorDao floorDao;

    public SubFloorItem getSubFloorItem(Long id) {
        return subFloorItemDao.getSubFloorItem(id);
    }

    public void deleteSubFloorItem(SubFloorItem subFloorItem) {
        subFloorItemDao.deleteSubFloorItem(subFloorItem);
    }

    public Long saveSubFloorItem(SubFloorItem subFloorItem) {
        if (!AppUtils.isBlank(subFloorItem.getSfiId())) {
            updateSubFloorItem(subFloorItem);
            return subFloorItem.getSfiId();
        }
        return (Long) subFloorItemDao.save(subFloorItem);
    }

    public void updateSubFloorItem(SubFloorItem subFloorItem) {
        subFloorItemDao.updateSubFloorItem(subFloorItem);
    }

    public PageSupport getSubFloorItem(CriteriaQuery cq) {
        return subFloorItemDao.queryPage(cq);
    }

	@Override
	public List<SubFloorItem> getAllSubFloorItem(Long sfId) {
		return subFloorItemDao.getAllSubFloorItem(sfId);
	}

	@Override
	public List<SubFloorItem> getItemLimit(Long sfId) {
		return subFloorItemDao.getItemLimit(sfId);
	}

	/**
	 * 保存子楼层产品
	 */
	@Override
	@CacheEvict(value = "FloorList", allEntries=true)
	public void saveSubFloorItem(List<ProductFloor> productFloorList, Long sfId,Long fId) {
		//删除原来的产品
		subFloorItemDao.deleteSubFloorItemById(sfId);
		if(productFloorList != null){//添加产品
			int seq = 1;
			List<SubFloorItem> subFloorItemList = new ArrayList<SubFloorItem>();
			for(ProductFloor PF: productFloorList){
				SubFloorItem subFloorItem =new SubFloorItem();
				subFloorItem.setRecDate(new Date());
				subFloorItem.setReferId(PF.getId());
				subFloorItem.setSfId(sfId);
				subFloorItem.setfId(fId);
				subFloorItem.setLayout(FloorLayoutEnum.WIDE_BLEND.value());
				subFloorItem.setType("PT");
				subFloorItem.setSeq(seq);
				subFloorItemList.add(subFloorItem);
				seq++;
			}
			subFloorItemDao.save(subFloorItemList);
		}
		
		//清除缓存
		Floor floor = floorDao.getById(fId);
		if (floor != null && floor.getStatus().intValue() == 1) {
			floorDao.updateFloorCache(floor);
		}
	}
	
	public FloorItemDao getFloorItemDao() {
		return floorItemDao;
	}

	@Override
	public void saveWideGoodsFloor(List<ProductFloor> productFloorList, Long fId,String layout) {
		 //删除原来的产品
		 subFloorItemDao.deleteSubFloorItemByFid(fId,layout);
		 if(AppUtils.isNotBlank(productFloorList)){//添加产品
				int seq = 1;
				List<SubFloorItem> subFloorItemList = new ArrayList<SubFloorItem>();
				for(ProductFloor PF: productFloorList){
					SubFloorItem subFloorItem =new SubFloorItem();
					subFloorItem.setRecDate(new Date());
					subFloorItem.setReferId(PF.getId());
					subFloorItem.setSfId(0l);
					subFloorItem.setfId(fId);
					subFloorItem.setLayout(layout);
					subFloorItem.setType("PT");
					subFloorItem.setSeq(seq);
					subFloorItemList.add(subFloorItem);
					seq++;
				}
			subFloorItemDao.save(subFloorItemList);
		}	
		// 清除缓存
		Floor floor = floorDao.getById(fId);
		if (floor != null && floor.getStatus().intValue() == 1) {
			floorDao.updateFloorCache(floor);
		}
	}

	public void setFloorDao(FloorDao floorDao) {
		this.floorDao = floorDao;
	}


	
}
