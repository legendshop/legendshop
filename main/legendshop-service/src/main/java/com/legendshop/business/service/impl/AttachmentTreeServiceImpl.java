/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AttachmentTreeDao;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.spi.service.AttachmentTreeService;
import com.legendshop.util.AppUtils;

/**
 * 附件树目录
 */
@Service("attachmentTreeService")
public class AttachmentTreeServiceImpl  implements AttachmentTreeService{
	
	@Autowired
    private AttachmentTreeDao attachmentTreeDao;

    public AttachmentTree getAttachmentTree(Long id) {
        return attachmentTreeDao.getAttachmentTree(id);
    }

    public void deleteAttachmentTree(AttachmentTree attachmentTree) {
        attachmentTreeDao.deleteAttachmentTree(attachmentTree);
    }

    public Long saveAttachmentTree(AttachmentTree attachmentTree) {
        if (!AppUtils.isBlank(attachmentTree.getId())) {
            updateAttachmentTree(attachmentTree);
            return attachmentTree.getId();
        }
        return attachmentTreeDao.saveAttachmentTree(attachmentTree);
    }

    public void updateAttachmentTree(AttachmentTree attachmentTree) {
        attachmentTreeDao.updateAttachmentTree(attachmentTree);
    }


	@Override
	public List<AttachmentTree> getAttachmentTreeByPid(Long pId, String userName) {
		return attachmentTreeDao.getAttachmentTreeByPid(pId, userName);
	}

	@Override
	public void updateAttmntTreeNameById(Integer id, String name) {
		attachmentTreeDao.updateAttmntTreeNameById(id, name);
	}

	@Override
	public AttachmentTree getAttachmentTree(Long treeId, Long shopId) {
		return attachmentTreeDao.getAttachmentTree(treeId,shopId);
	}

	@Override
	public List<AttachmentTree> getAllChildByTreeId(Long treeId) {
		return attachmentTreeDao.getAllChildByTreeId(treeId);
	}

	@Override
	public void deleteAttachmentTree(List<AttachmentTree> attmntTreeList) {
		attachmentTreeDao.delete(attmntTreeList);
	}

	/**
	 * 获取图片目录树结构
	 */
	@Override
	public List<AttachmentTree> getAttachmentTreeDtoByUserName( String userName) {
		
		List<AttachmentTree> attachmentTrees = attachmentTreeDao.getAttachmentTreeByUserName(userName);
		
		List<AttachmentTree> tree = getTree(0L,attachmentTrees);
		return tree;
	}
	
	
	//递归遍历树
	private List<AttachmentTree> getTree(Long pid ,List<AttachmentTree> list){
		
		List<AttachmentTree> tree = new ArrayList<AttachmentTree>();
		
		for (AttachmentTree attachment : list) {
			
			if (pid.equals(attachment.getParentId())) {
				attachment.setChildrenList(getTree(attachment.getId(), list));
                tree.add(attachment);
            }
		}
		return tree;
	}
}
