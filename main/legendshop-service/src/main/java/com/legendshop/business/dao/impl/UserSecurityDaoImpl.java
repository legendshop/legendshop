package com.legendshop.business.dao.impl;

import java.util.Date;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.UserSecurityUpdate;
import com.legendshop.business.dao.UserSecurityDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.securityCenter.UserSecurityEnum;
/**
 * 用户安全配置Dao.
 */
@Repository
public  class UserSecurityDaoImpl extends GenericDaoImpl<UserSecurity, Long> implements UserSecurityDao {

	@Override
	public void saveUserSecurity(UserSecurity userSecurity) {
		save(userSecurity);
	}

	/**
	 * 保存默认用户安全配置
	 */
	@Override
	public void saveDefaultUserSecurity(String userName, Integer secLevel) {
		UserSecurity userSecurity = new UserSecurity();
		userSecurity.setUserName(userName);
		userSecurity.setCreateTime(new Date());
		userSecurity.setSecLevel(secLevel);
		userSecurity.setMailVerifn(0);
		userSecurity.setPaypassVerifn(0);
		userSecurity.setPhoneVerifn(1);
		userSecurity.setTimes(0);
		saveUserSecurity(userSecurity);
	}

	public void saveDefaultUserSecurity(String userName) {
		saveDefaultUserSecurity(userName, UserSecurityEnum.SECURITY_DANGEROUS.value());//默认安全等级是1
	}

	@Override
	@UserSecurityUpdate
	public void deleteUserSecurity(UserSecurity userSecurity) {
		delete(userSecurity);
	}

	@Override
	@UserSecurityUpdate
	public void updateUserSecurity(UserSecurity userSecurity) {
		update(userSecurity);
	}

	/**
	 * 获取用户的安全配置
	 */
	@Override
	@Cacheable(value = "UserSecurity", key="#userName")
	public UserSecurity getUserSecurity(String userName) {
		return get("select * from ls_usr_security where user_name = ?", UserSecurity.class, userName);
	}

	@Override
	public boolean isMailCodeExist(String userName, String code) {
		UserSecurity userSecurity = this.getByProperties(new EntityCriterion().eq("userName", userName).eq("emailCode", code));
		if(userSecurity==null){
			return false;
		}
		return true;
	}

}
