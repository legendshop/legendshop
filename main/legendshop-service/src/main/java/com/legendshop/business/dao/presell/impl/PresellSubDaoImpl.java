/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell.impl;
 
import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.SubItem;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.PresellSub;

/**
 * The Class PresellSubDaoImpl.
 */
@Repository
public class PresellSubDaoImpl extends GenericDaoImpl<PresellSub, Long> implements PresellSubDao  {

	public PresellSub getPresellSub(Long id){
		return getById(id);
	}
	
	@Override
	public PresellSub getPresellSub(String userId, String subNumber) {
		String sql="SELECT ls_presell_sub.*,ls_sub.actual_total AS total FROM ls_presell_sub INNER JOIN ls_sub ON  ls_presell_sub.sub_number=ls_sub.sub_number WHERE ls_presell_sub.sub_number=? AND ls_sub.user_id=? ";
		return this.get(sql, PresellSub.class, subNumber,userId);
	}
	
	
    public int deletePresellSub(PresellSub presellSub){
    	return delete(presellSub);
    }
	
	public Long savePresellSub(PresellSub presellSub){
		return save(presellSub);
	}
	
	public int updatePresellSub(PresellSub presellSub){
		return update(presellSub);
	}
	
	@Override
	public boolean findIfJoinPresell(Long productId) {
		String sql=" SELECT COUNT(*) FROM ls_presell_prod WHERE ls_presell_prod.status=0 and prod_id=? and ls_presell_prod.pre_sale_end>=?";
		return this.get(sql, Integer.class, productId,new Date())>0;
	}

    @Override
    public PresellSub getPresellSubByPresell(String presellId)
	{
		String sql="SELECT is_pay_final,is_pay_deposit FROM ls_presell_sub\n" +
				"  WHERE sub_number=?";
        return get(sql,PresellSub.class,presellId);
    }

	@Override
	public PresellSub getPresellSub(String subNumber) {
		String sql="select *from ls_presell_sub where sub_number=?";
		return this.get(sql, PresellSub.class, subNumber);
	}

	/**
	 * 查询当前期 所有已支付定金未支付尾款但尾款支付时间已过且已关闭的订单
	 * @param shopId
	 * @param endDate
	 * @param orderStatus
	 * @return
	 */
	@Override
	public List<PresellSub> getBillUnPayFinalPresellOrder(Long shopId, Date endDate, Integer orderStatus) {

		String sql = "SELECT ps.sub_id AS subId,ps.pre_deposit_price AS preDepositPrice FROM ls_presell_sub ps LEFT JOIN ls_sub s ON ps.sub_number = s.sub_number " +
					 "WHERE  ps.is_bill = 0 AND ps.pay_pct_type = 1 AND ps.is_pay_deposit = 1 AND ps.is_pay_final = 0 AND s.shop_id = ? AND s.status = ? AND ps.final_m_end < ?";

		return this.query(sql,PresellSub.class,shopId,orderStatus,endDate);
	}

	/**
	 * 根据结算单号获取当期结算的预售订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<PresellSub> queryPresellSubList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {

		SimpleSqlQuery query = new SimpleSqlQuery(PresellSub.class,10, curPageNO);
		QueryMap map = new QueryMap();

		map.put("shopId", shopId);
		map.put("status", OrderStatusEnum.CLOSE.value());
		map.put("billSn",shopOrderBill.getSn());
		if (AppUtils.isNotBlank(subNumber)) {
			map.put("subNumber", "%" + subNumber.trim() + "%");
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("presellOrder.queryPresellSubListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("presellOrder.queryPresellSubList", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}


}
