/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Pub;

/**
 * 公告Dao.
 */
public interface PubDao extends GenericDao<Pub, Long>{

	/**
	 * Gets the pub.
	 *
	 * @return the pub
	 */
	public abstract List<Pub> getPubByShopId();


	/**
	 * Delete pub.
	 *
	 * @param pub the pub
	 */
	public abstract void deletePub(Pub pub);

	/**
	 * Update pub.
	 * 
	 * @param pub
	 *            the pub
	 */
	public abstract void updatePub(Pub pub);


	/**
	 * Save pub.
	 *
	 * @param pub the pub
	 * @return the long
	 */
	public abstract Long savePub(Pub pub);

	/**
	 * Gets the pub by id.
	 *
	 * @param id the id
	 * @return the pub by id
	 */
	public abstract Pub getPubById(Long id);

	/**
	 * Gets the pub.
	 *
	 * @param userName the user name
	 * @param id the id
	 * @return the pub
	 */
	public abstract List<Pub> getPub(String userName, Long id,boolean isUser);

	/**
	 * 查找 最新公告 *.
	 *
	 * @return the list< pub>
	 */
	public abstract List<Pub> queryPubByShopId();
	
	/**
	 * 获取 公告  *.
	 *
	 * @return the list< pub>
	 */
	public abstract List<Pub> queryShopPub();
	
	/**
	 * 获取 买家公告  *.
	 *
	 * @return the list< pub>
	 */
	public abstract List<Pub> queryUserPub();


	/**
	 * Gets the pub list page.
	 *
	 * @param curPageNO the cur page no
	 * @param pub the pub
	 * @param result the result
	 * @return the pub list page
	 */
	public abstract PageSupport<Pub> getPubListPage(String curPageNO, Pub pub, DataSortResult result);

}