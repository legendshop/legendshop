package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTagRel;
import com.legendshop.model.entity.Product;
/**
 * 商品标签关系Dao
 *
 */
public interface ProdTagRelDao extends GenericDao<ProdTagRel, Long>{

	/**
	 * 根据标签查找对应的关系
	 * @param tagId
	 * @return
	 */
	public abstract List<ProdTagRel> queryProdTagRel(Long tagId);
	
	/**
	 * 清除缓存
	 */
	public abstract boolean updateProdTagCache(ProdTagRel prodTagRel);

	public abstract boolean deleteProdTagRel(ProdTagRel prodTagRel);

	public abstract PageSupport<ProdTagRel> queryProdTagRel(String curPageNO, Product product, Long shopId);

	public abstract PageSupport<ProdTagRel> queryBindProdManage(String curPageNO, Long id, String prodName);
}
