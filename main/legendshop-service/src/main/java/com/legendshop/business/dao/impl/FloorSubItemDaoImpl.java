/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.util.AppUtils;

/**
 * 首页主楼层子内容Dao.
 */
@Repository
public class FloorSubItemDaoImpl extends GenericDaoImpl<FloorSubItem, Long> implements FloorSubItemDao {

	private static String getUserFloorSubItemSQL = "select s.fsi_id as fsiId,s.fi_id as fiId,s.seq, c.name as nsortName,s.sub_sort_id as subSortId "
			+ "from ls_floor f, ls_floor_item fi, ls_floor_sub_item s,ls_category c where f.fl_id = fi.floor_id and fi.fi_id = s.fi_id and s.sub_sort_id = c.id order by s.seq";

	private static String getAllFloorSubItemSQL = "select s.fsi_id as fsiId,s.fi_id as fiId,s.seq, c.name AS nsortName,s.sub_sort_id as subSortId FROM ls_floor_sub_item s,ls_category c"
			+ " where s.sub_sort_id = c.id  and s.fi_id = ? order by s.seq";
	
	private static String findSubFloorItemSQL ="select s.sfi_id as sfiId,s.refer_id as referId,p.name as productName,s.sf_id as sfId,s.f_id as fId,s.layout as layout,s.seq,p.pic as productPic,p.cash as productCash,p.price as productPrice,p.prod_id as productId from ls_sub_floor_item s,ls_prod p  where s.refer_id = p.prod_id and s.f_id = ? and s.layout=? order by s.seq" ;
	

	public FloorSubItem getFloorSubItem(Long id) {
		return getById(id);
	}

	public void deleteFloorSubItem(FloorSubItem floorSubItem) {
		delete(floorSubItem);
	}

	public Long saveFloorSubItem(FloorSubItem floorSubItem) {
		return (Long) save(floorSubItem);
	}

	public void updateFloorSubItem(FloorSubItem floorSubItem) {
		update(floorSubItem);
	}

	/**
	 * 加载楼层下的子项目
	 */
	@Override
	public List<FloorSubItem> getAllFloorSubItem(Long id) {
		List<FloorSubItem> floorSubItemList = query(getAllFloorSubItemSQL, FloorSubItem.class, id);
		return floorSubItemList;
	}

	/**
	 * 加载用户下所有楼层下的子项目
	 */
	@Override
	public List<FloorSubItem> getUserFloorSubItem() {
		List<FloorSubItem> floorSubItemList = query(getUserFloorSubItemSQL, FloorSubItem.class);
		return floorSubItemList;
	}

	/**
	 * 删除二级楼层item
	 */
	@Override
	public void deleteFloorSubItemById(Long floorId) {
		String sql = ConfigCode.getInstance().getCode("biz.selectFloorSubItemById");
		List<Long> idList = getJdbcTemplate().queryForList(sql, Long.class, floorId);
		if (AppUtils.isNotBlank(idList)) {
			StringBuilder sb = new StringBuilder("delete from ls_floor_sub_item where fsi_id in ( ");
			for (int i = 0; i < idList.size() - 1; i++) {
				sb.append("?, ");
			}
			sb.append("? )");
			update(sb.toString(), idList.toArray());
		}

	}

	@Override
	public List<SubFloorItem> findSubFloorItem(Long flId, String layout, int limit) {
		return queryLimit(findSubFloorItemSQL, SubFloorItem.class, 0, limit, new Object[]{flId,layout});
	}

	@Override
	public void deleteFloorSubItem(List<FloorSubItem> floorSubItemList) {
       delete(floorSubItemList);		
	}

	@Override
	public PageSupport<FloorSubItem> getFloorSubItemPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(FloorSubItem.class, curPageNO);
		return queryPage(cq);
	}

}
