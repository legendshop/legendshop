/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.ShopGrade;
/**
 * 用户等级Dao.
 */
public interface ShopGradeDao extends GenericDao<ShopGrade, Integer> {

	/**
	 * Save shop grade.
	 *
	 * @param shopGrade the shop grade
	 * @return the integer
	 */
	public abstract Integer saveShopGrade(ShopGrade shopGrade);
	
	/**
	 * Gets the shop grade.
	 *
	 * @param id the id
	 * @return the shop grade
	 */
	public abstract ShopGrade getShopGrade(Integer id);
	
	/**
	 * Delete shop grade.
	 *
	 * @param shopGrade the shop grade
	 */
	public abstract void deleteShopGrade(ShopGrade shopGrade);
	
	/**
	 * Update shop grade.
	 *
	 * @param shopGrade the shop grade
	 */
	public abstract void updateShopGrade(ShopGrade shopGrade);

	/**
	 * Retrieve shop grade.
	 *
	 * @return the list
	 */
	public abstract List<StatusKeyValueEntity> retrieveShopGrade();

	public abstract PageSupport<ShopGrade> getShopGradePage(String curPageNO, ShopGrade shopGrade);
}
