/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.spi.service.WeixinMenuExpandConfigService;
import com.legendshop.util.AppUtils;

/**
 *微信菜单扩展服务
 */
@Service("weixinMenuExpandConfigService")
public class WeixinMenuExpandConfigServiceImpl  implements WeixinMenuExpandConfigService{

	@Autowired
    private WeixinMenuExpandConfigDao weixinMenuExpandConfigDao;

    public WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id) {
        return weixinMenuExpandConfigDao.getWeixinMenuExpandConfig(id);
    }

    public void deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig) {
        weixinMenuExpandConfigDao.deleteWeixinMenuExpandConfig(weixinMenuExpandConfig);
    }

    public Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig) {
        if (!AppUtils.isBlank(weixinMenuExpandConfig.getId())) {
        	WeixinMenuExpandConfig expandConfig=weixinMenuExpandConfigDao.getById(weixinMenuExpandConfig.getId());
        	expandConfig.setClassService(weixinMenuExpandConfig.getClassService());
        	expandConfig.setCreateUserid(weixinMenuExpandConfig.getCreateUserid());
        	expandConfig.setKeyword(weixinMenuExpandConfig.getKeyword());
        	expandConfig.setName(weixinMenuExpandConfig.getName());
            updateWeixinMenuExpandConfig(expandConfig);
            return weixinMenuExpandConfig.getId();
        }
        weixinMenuExpandConfig.setCreatedate(new Date());
        return weixinMenuExpandConfigDao.saveWeixinMenuExpandConfig(weixinMenuExpandConfig);
    }

    public void updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig) {
        weixinMenuExpandConfigDao.updateWeixinMenuExpandConfig(weixinMenuExpandConfig);
    }


	@Override
	public PageSupport<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String curPageNO,
			WeixinMenuExpandConfig weixinMenuExpandConfig) {
		 return weixinMenuExpandConfigDao.getWeixinMenuExpandConfig(curPageNO,weixinMenuExpandConfig);
	}
}
