/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.base.exception.AuthorizationException;
import com.legendshop.business.dao.PayTypeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PayType;

/**
 * 付费类型Dao.
 */
@Repository
public class PayTypeDaoImpl extends GenericDaoImpl<PayType, Long> implements PayTypeDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	@Caching(
			evict={
				@CacheEvict(value="PayTypeList", allEntries=true),
				@CacheEvict(value="PayTypeDto",key="#payType.payTypeId"),
				@CacheEvict(value="PayTypeDto",key="#payType.payId")
	})
	public Long savePayType(PayType payType) {
		if(payType.getInterfaceType() == null){
			payType.setInterfaceType(0);//默认是0
		}
		return save(payType);
	}
	
	@Override
	@Caching(
			evict={
				@CacheEvict(value="PayTypeList", allEntries=true),
				@CacheEvict(value="PayTypeDto",key="#payType.payTypeId"),
				@CacheEvict(value="PayTypeDto",key="#payType.payId")
	})
	public void updatePayType(PayType payType) {
		update(payType);
	}

	@Override
	@Caching(
			evict={
				@CacheEvict(value="PayTypeList", allEntries=true),
				@CacheEvict(value="PayTypeDto",allEntries=true),
	})
	public void deletePayTypeById(Long id) {
		deleteById(id);
	}

	/**
	 * 根据Id查找支付类型
	 */
	@Override
	@Cacheable(value="PayTypeDto",key="#id")
	public PayType getPayTypeById(Long id) {
		return getById(id);
	}
	
	/**
	 * 根据Id查找支付类型
	 */
	@Override
	public PayType getPayTypeByIdFromDb(Long id) {
		return getById(id);
	}

	/**
	 * 支付接口类型
	 */
	@Override
	public Integer getInterfaceType(Long shopId, String payTypeId) {
		return get("select interface_type from ls_pay_type where shop_id = ? and pay_type_id = ? ", Integer.class, shopId, payTypeId);
	}
	
	/**
	 * 支付接口类型
	 */
	@Override
	public Integer getInterfaceType(String payTypeId) {
		return get("select interface_type from ls_pay_type where  pay_type_id = ? ", Integer.class, payTypeId);
	}
	

	/**
	 * payTypeId 分为ALP ALP_MOBILE WEIXIN_PAY_MOBILE，参见类PayTypeEnum
	 */
	@Override
	/*@Cacheable(value="PayTypeDto",key="#payTypeId")*/
	public PayType getPayTypeByPayTypeId(String payTypeId) {
		return getByProperties(new EntityCriterion().eq("payTypeId", payTypeId));
	}

	@Override
	@Cacheable(value="PayTypeList")
	public List<PayType> getPayTypeList() {
		return this.queryAll();
	}

	@Override
	public PageSupport<PayType> getPayTypeListPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(PayType.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		cq.addDescOrder("payTypeName");
		return queryPage(cq);
	}

	@Override
	public PageSupport<PayType> getPayTypeListPage(String curPageNO, String name) {
		CriteriaQuery cq = new CriteriaQuery(PayType.class, curPageNO);
		cq.setPageSize(5);
		if (name == null) {
			throw new AuthorizationException(name + " did not logon yet!");
		}
		cq.eq("userName", name);
		return queryPage(cq);
	}

	@Override
	public PayType getPayTypeById(String payTypeId) {
		return getByProperties(new EntityCriterion().eq("payTypeId", payTypeId));
	}


}
