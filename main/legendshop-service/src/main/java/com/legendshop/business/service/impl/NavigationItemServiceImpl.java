/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.NavigationItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.NavigationPositionEnum;
import com.legendshop.model.entity.NavigationItem;
import com.legendshop.spi.service.NavigationItemService;
import com.legendshop.util.AppUtils;

/**
 * 导航子项目服务.
 */
@Service("navigationItemService")
public class NavigationItemServiceImpl  implements NavigationItemService{
	
	@Autowired
    private NavigationItemDao navigationItemDao;

    public List<NavigationItem> getNavigationItem() {
        return navigationItemDao.getNavigationItem();
    }

    public NavigationItem getNavigationItem(Long id) {
        return navigationItemDao.getNavigationItem(id);
    }

    public void deleteNavigationItem(NavigationItem navigationItem) {
        navigationItemDao.deleteNavigationItem(navigationItem);
    }

    public Long saveNavigationItem(NavigationItem navigationItem) {
        if (!AppUtils.isBlank(navigationItem.getItemId())) {
            updateNavigationItem(navigationItem);
            return navigationItem.getItemId();
        }
        return (Long) navigationItemDao.save(navigationItem);
    }

    public void updateNavigationItem(NavigationItem navigationItem) {
        navigationItemDao.updateNavigationItem(navigationItem);
    }

	@Override
	public List<NavigationItem> getAllNavigationItem() {
		return navigationItemDao.getAllNavigationItem();
	}

	@Override
	public List<NavigationItem> getNavigationItem(NavigationPositionEnum position) {
		return navigationItemDao.getNavigationItem(position);
	}

	@Override
	public PageSupport<NavigationItem> getNavigationItemPage(String curPageNO) {
		return navigationItemDao.getNavigationItemPage(curPageNO);
	}
}
