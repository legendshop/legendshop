/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTypeBrandDao;
import com.legendshop.business.dao.ProdTypeDao;
import com.legendshop.business.dao.ProdTypeParamDao;
import com.legendshop.business.dao.ProdTypePropertyDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdTypeBrand;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.model.entity.ProdTypeProperty;
import com.legendshop.util.AppUtils;

/**
 *商品类型Dao
 */
@Repository
public class ProdTypeDaoImpl extends GenericDaoImpl<ProdType, Long> implements ProdTypeDao  {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Autowired
	private ProdTypePropertyDao prodTypePropertyDao;
	
	@Autowired
	private ProdTypeParamDao prodTypeParamDao;
	
	@Autowired
	private ProdTypeBrandDao prodTypeBrandDao;
     
	public ProdType getProdType(Long id){
		return getById(id);
	}
	
    public int deleteProdType(ProdType prodType){
    	return delete(prodType);
    }
	
	public Long saveProdType(ProdType prodType){
		return save(prodType);
	}
	
	public int updateProdType(ProdType prodType){
		return update(prodType);
	}
	
	public PageSupport getProdType(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	//保存关联规格
	public void saveTypeProp(List<ProdTypeProperty> propertyList, Long id) {
		if(AppUtils.isBlank(propertyList)){
			return;
		}
		for(ProdTypeProperty productProperty:propertyList){
			productProperty.setTypeId(id);
		}
		prodTypePropertyDao.saveProdTypeProperty(propertyList);
	}

	@Override
	//保存关联品牌
	public void saveTypeBrand(List<ProdTypeBrand> brandList, Long id) {
		if(AppUtils.isBlank(brandList)){
			return;
		}
		for(ProdTypeBrand brand:brandList){
			brand.setTypeId(id);
		}
		
		prodTypeBrandDao.saveProdTypeBrand(brandList);
	}

	@Override
	public void deleteProdProp(Long propId, Long prodTypeId) {
		update("delete from ls_prod_type_prop where type_id = ? and prop_id = ?",prodTypeId,propId);
	}

	@Override
	public void deleteBrand(Long brandId, Long prodTypeId) {
		update("delete from ls_prod_type_brand where type_id = ? and brand_id = ?",prodTypeId,brandId);
	}

	@Override
	public void saveParameterProperty(List<ProdTypeParam> propertyList,Long prodTypeId) {
		if(AppUtils.isBlank(propertyList)){
			return;
		}
		
		for(ProdTypeParam productProperty:propertyList){
			productProperty.setTypeId(prodTypeId);
		}
		
		prodTypeParamDao.saveProdTypeParam(propertyList);
	}

	@Override
	public void deleteParameterProp(Long propId, Long prodTypeId) {
		update("delete from ls_prod_type_param where type_id = ? and prop_id = ?",prodTypeId,propId);
	}

	@Override
	public List<KeyValueEntity> loadProdType() {
		List<KeyValueEntity> result =  this.query(" select id ,name from ls_prod_type ", new RowMapper<KeyValueEntity>() {
			@Override
			public KeyValueEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
				return  new KeyValueEntity(String.valueOf(rs.getLong("id")), rs.getString("name"));
			}
		});
		return result;
	}

	@Override
	public List<ProdType> getProdTypeListWithId(Long tpyeId) {
		List<ProdType> typeList= queryAll();
        
        //add selected option
        if(AppUtils.isNotBlank(typeList) && AppUtils.isNotBlank(tpyeId) && tpyeId != 0){
        	boolean found = false;
        	for (int i = 0; i < typeList.size(); i++) {
				if(tpyeId.equals(typeList.get(i).getId())){
					found = true;
				}
			}
        	
        	if(!found){ //add the option
        		typeList.add(0, this.getProdType(tpyeId));
        	}
        }
        return typeList;
	}

	@Override
	public PageSupport<ProdType> getProdTypePage(String typeName) {
		CriteriaQuery cq = new CriteriaQuery(ProdType.class, "1");
	    cq.setPageSize(30);
	    cq.like("name", typeName,MatchMode.START);
		return queryPage(cq);
	}

	@Override
	public PageSupport<ProdType> queryProdTypePage(String curPageNO, ProdType prodType, Long categoryId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProdType.class, systemParameterUtil.getPageSize(), curPageNO);
	   	 QueryMap map = new QueryMap();
	   	 map.put("categoryId",categoryId);
	   	 if(AppUtils.isNotBlank(prodType.getName())){
	   		 map.like("name",prodType.getName().trim());
	   	 }
	   	 query.setParam(map.toArray());
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getProdTypeListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.getProdTypeList", map);
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProdType> getProdTypePage(String curPageNO, String name) {
		CriteriaQuery cq = new CriteriaQuery(ProdType.class, curPageNO);
	    cq.setPageSize(systemParameterUtil.getPageSize());
	    cq.eq("name",name);
		return queryPage(cq);
	}

	@Override
	public PageSupport<ProdType> getProdTypeByName(String typeName) {
		CriteriaQuery cq = new CriteriaQuery(ProdType.class, "1");
        cq.setPageSize(30);
        cq.like("name", typeName,MatchMode.START);
		return queryPage(cq);
	}

	@Override
	public List<Long> ifDeleteProductProperty(Long proTypeId, Long propId) {
		QueryMap map = new QueryMap();
	   	map.put("proTypeId",proTypeId);
//	   	map.like("prefixPropId", ";" + propId + ":");
//	   	map.like("suffixPropId", propId + ":");
	   	String queryAll = ConfigCode.getInstance().getCode("prod.getTypeAboutSkuByPropId", map);
		return query(queryAll, Long.class, map.toArray());
	}
	
 }
