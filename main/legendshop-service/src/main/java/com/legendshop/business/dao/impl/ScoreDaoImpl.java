/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.business.dao.ScoreDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.Score;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserDetail;

/**
 * 
 * 积分Dao
 */
@Repository
public class ScoreDaoImpl  extends GenericDaoImpl<Score, Long> implements ScoreDao {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(ScoreDaoImpl.class);

	/** The user detail dao. */
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private SubDao subDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	// 1元 = ？ 积分
	/** The money per score. */
	@Value("1")
	private Long moneyPerScore = 1l;

	// 1积分 = ？元
	/** The score per money. */
	@Value("0.01")
	private Double scorePerMoney = 0.1;

	// 增加积分
	/** The CREDI t_ score. */
	private final String CREDIT_SCORE = "C";

	// 扣积分
	/** The DEBI t_ score. */
	private final String DEBIT_SCORE = "D";

	@Override
	public void saveScore(Sub sub) {
		log.debug("addScore UserName = {},Score ={} ", sub.getUserName(), sub.getScore());
		// 已经使用了积分的单子不再另外赠送积分
		if (sub == null || sub.getTotal() <= 0 || sub.getScoreId() != null || !systemParameterUtil.getUseScore()) {
			return;
		}
		UserDetail userDetail = userDetailDao.getUserDetailByName(sub.getUserName());
		Integer score = userDetail.getScore();
		if (score == null) {
			score = 0;
		}
		Double core = calScore(sub.getTotal(), CREDIT_SCORE);
		userDetail.setScore(score + core.intValue());
		userDetailDao.updateUserDetail(userDetail);
		save(makeScore(sub, core.intValue(), CREDIT_SCORE));
	}

	@Override
	public Map<String, Object> deleteScore(Sub sub, Integer avaibleScore) {
		if (sub == null || avaibleScore == null || avaibleScore <= 0 || !systemParameterUtil.getUseScore()) {
			return null;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		UserDetail userDetail = userDetailDao.getUserDetailByName(sub.getUserName());
		Integer orginScore = userDetail.getScore();
		if (orginScore == null) {
			orginScore = 0;
		}
		if (orginScore - avaibleScore < 0) {
			throw new BusinessException("Not enough score", ErrorCodes.NOT_ENOUGH_SCORE);
		}
		// 最多需要的积分
		Integer requiredScore = calRequiredScore(sub.getTotal()).intValue();
		Integer usedScore = null;
		// 积分不足以购买整个商品，所有积分都用掉
		if (requiredScore > avaibleScore) {
			userDetail.setScore(orginScore - avaibleScore); // expect 0
			usedScore = avaibleScore;
			//subHistoryDao.saveSubHistory(sub, SubStatusEnum.DEBIT_SCORE.value());
			sub.setActualTotal(sub.getTotal());
			sub.setTotal(sub.getTotal() - calMoney(avaibleScore));
		} else {
			//subHistoryDao.saveSubHistory(sub, SubStatusEnum.DEBIT_SCORE.value());
			// 积分可以购买整个商品，只是扣除部分积分，订单成交
			sub.setActualTotal(sub.getTotal());// 订单实际现金为0
			sub.setTotal(0d);
			sub.setStatus(OrderStatusEnum.PADYED.value());
			sub.setUpdateDate(new Date());
			userDetail.setScore(avaibleScore - requiredScore);
			usedScore = requiredScore;
		}

		map.put("userScore", userDetail.getScore());
		userDetailDao.updateUser(userDetail);
		map.put("subTotal", sub.getTotal());
		Long scoreId = (Long) save(makeScore(sub, usedScore, DEBIT_SCORE));
		sub.setScoreId(scoreId);
		sub.setScore(usedScore);
		subDao.updateSub(sub);
		return map;

	}

	/**
	 * 购买整个商品所需要的积分.
	 * 
	 * @param total
	 *            the total
	 * @return the long
	 */
	private Double calRequiredScore(Double total) {
		return (Math.ceil(total / scorePerMoney));
	}

	@Override
	public Double calScore(Double total, String scoreType) {
		if (CREDIT_SCORE.equals(scoreType)) {
			// 加积分取最小值
			return  (Math.floor(moneyPerScore * total));
		} else {
			// 使用积分取最大值
			return (Math.ceil(moneyPerScore * total));
		}

	}

	/**
	 * Make score.
	 *
	 */
	private Score makeScore(Sub sub, Integer score, String scoreType) {
		Score entity = new Score();
		entity.setRecDate(new Date());
		entity.setScore(score);
		entity.setScoreType(scoreType);
		entity.setSubId(sub.getSubId());
		entity.setUserName(sub.getUserName());
		return entity;
	}

	@Override
	public Double calMoney(Integer score) {
		return scorePerMoney * score;
	}


}
