package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.InvoiceDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Invoice;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.util.AppUtils;

/**
 * 发票服务
 */
@Service("invoiceService")
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceDao invoiceDao;
	
	@Override
	public List<Invoice> getInvoice(String userName) {
		return invoiceDao.getInvoice(userName);
	}

	@Override
	public Invoice getInvoice(Long id) {
		return invoiceDao.getById(id);
	}

	@Override
	public void deleteInvoice(Invoice invoice) {
		invoiceDao.deleteInvoice(invoice);
	}

	@Override
	public Long saveInvoice(Invoice invoice) {
		if(AppUtils.isNotBlank(invoice.getId())){
			invoiceDao.updateInvoice(invoice);
			return invoice.getId();
		}
		return invoiceDao.saveInvoice(invoice);
	}

	@Override
	public int updateInvoice(Invoice invoice) {
		return invoiceDao.updateInvoice(invoice);
	}

	@Override
	public void updateDefaultInvoice(Long invoiceId, String userName) {
		invoiceDao.updateDefaultInvoice(invoiceId, userName);
	}

	@Override
	public void delById(Long id) {
		invoiceDao.delById(id);
	}

	@Override
	public Invoice getDefaultInvoice(String userId) {
		return invoiceDao.getDefaultInvoice(userId);
	}

	@Override
	public Invoice getInvoice(Long id, String userId) {
		return invoiceDao.getInvoice(id, userId);
	}

	@Override
	public PageSupport<Invoice> getInvoicePage(String userName, String curPageNO, Integer pageSize) {
		return invoiceDao.getInvoicePage(userName,curPageNO, pageSize);
	}

	@Override
	public int updateInvoiceList(List<Invoice> list) {
		return invoiceDao.update(list);
	}

	@Override
	public void saveInvoice(Invoice item, String userName) {
		this.saveInvoice(item);
		this.updateDefaultInvoice(item.getId(), userName);
	}

	@Override
	public Invoice getInvoiceByTitleId(String userId, Integer titleId) {
		return invoiceDao.getInvoiceByTitleId(userId,titleId);
	}

	@Override
	public void removeDefaultInvoiceStatus(String userId) {
		invoiceDao.removeDefaultInvoiceStatus(userId);
	}

	/**
	 * 根据发票类型获取发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @param curPageNo 当前页码
	 * @return
	 */
	@Override
	public PageSupport<Invoice> queryInvoice(String userName, Integer invoiceType, String curPageNo) {

		return invoiceDao.queryInvoice(userName,invoiceType,curPageNo);
	}

	/**
	 * 根据类型获取用户发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @return
	 */
	@Override
	public List<Invoice> getInvoiceByType(String userName, Integer invoiceType) {
		return invoiceDao.getInvoiceByType(userName,invoiceType);
	}
}
