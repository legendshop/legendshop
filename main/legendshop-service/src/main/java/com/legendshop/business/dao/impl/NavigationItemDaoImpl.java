/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.NavigationItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.NavigationPositionEnum;
import com.legendshop.model.entity.NavigationItem;

/**
 * 导航管理Dao.
 */
@Repository
public class NavigationItemDaoImpl extends GenericDaoImpl<NavigationItem, Long> implements NavigationItemDao {

	public List<NavigationItem> getNavigationItem() {
		return this.queryByProperties(new EntityCriterion().eq("status", 1));
	}

	public List<NavigationItem> getAllNavigationItem() {
		return queryAll();
	}

	public NavigationItem getNavigationItem(Long id) {
		return getById(id);
	}

	@CacheEvict(value = "NavigationItemList", allEntries = true)
	public void deleteNavigationItem(NavigationItem navigationItem) {
		delete(navigationItem);
	}

	@CacheEvict(value = "NavigationItemList", allEntries = true)
	public Long saveNavigationItem(NavigationItem navigationItem) {
		return (Long) save(navigationItem);
	}

	@CacheEvict(value = "NavigationItemList", allEntries = true)
	public void updateNavigationItem(NavigationItem navigationItem) {
		update(navigationItem);
	}

	@Override
	public List<NavigationItem> getNavigationItemByNaviId(Long naviId) {
		return this.queryByProperties(new EntityCriterion().eq("naviId", naviId));
	}

	@Override
	@CacheEvict(value = "NavigationItemList", allEntries = true)
	public void deleteNavigationItems(Long naviId) {
		update("delete from ls_navi_item where navi_id = ?", naviId);
	}

	@Override
	@Cacheable(value = "NavigationItemList")
	public List<NavigationItem> getNavigationItem(NavigationPositionEnum position) {
		String sql = "select ni.item_id as itemId, ni.navi_id as naviId, ni.name, ni.link, ni.seq,ni.status from ls_navi n, ls_navi_item ni where n.navi_id = ni.navi_id and n.status = 1 and ni.status = 1 and n.position = ?";
		return query(sql, NavigationItem.class, position.value());
	}

	@Override
	public PageSupport<NavigationItem> getNavigationItemPage(String curPageNO) {
		 CriteriaQuery cq = new CriteriaQuery(NavigationItem.class, curPageNO);
		return queryPage(cq);
	}

}
