/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.dto.NewsTagDto;
import com.legendshop.model.dto.TagNewsDto;
import com.legendshop.model.entity.Tag;
import com.legendshop.model.entity.TagItems;

/**
 *挑选的产品和内容块的对应关系Dao
 */
public interface TagItemsDao extends Dao<TagItems, Long> {
     
    public abstract List<TagItems> getTagItems(String code);

	public abstract TagItems getTagItems(Long id);
	
    public abstract int deleteTagItems(TagItems tagItems);
	
	public abstract Long saveTagItems(TagItems tagItems);
	
	public abstract int updateTagItems(TagItems tagItems);
	
	public abstract TagItems getItemsByTagId(Long TagId);
	
	public List<TagItems> queryTagItemsList(List<Tag> tagList);
	 
	public abstract void deleteTagItems(Long itemId);

	public abstract List<NewsTagDto> getNewsTagDto(String type);

	public abstract List<TagNewsDto> getTagNewsDto(String type);

	public abstract Long getTagNewsDtoCount(String type);

	public abstract List<TagItems> queryTagItemsListByType(String type);

	public abstract void saveTagItemsNews(Long newsid, Long itemid);

	public abstract void delTagItemsNews(Long newsid, Long itemid);
	
 }
