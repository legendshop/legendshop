package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductDao;
import com.legendshop.model.dto.ProdParamDto;
import com.legendshop.model.dto.compare.IdNameDto;
import com.legendshop.model.dto.compare.IdNameImgDto;
import com.legendshop.model.dto.compare.ProductCompareDto;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.spi.service.ProductCompareService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
/**
 * 商品对比服务
 *
 */
@Service("productCompareService")
public class ProductCompareServiceImpl implements ProductCompareService {

	@Autowired
	private ProductDao productDao;
	
	@Override
	public ProductCompareDto getProductCompareDto(Long[] prods) {
		if(AppUtils.isBlank(prods)){
			  return null;
		}
		try {
			List<ProductDetail> products =new ArrayList<ProductDetail>();
			for (int i = 0; i < prods.length; i++) {
				ProductDetail detail=productDao.getProdDetail(prods[i]);
				if(AppUtils.isBlank(detail)){
					continue;
				}
				products.add(detail);
			}
			if (AppUtils.isBlank(products)) {
				return null;
			}
			ProductCompareDto compareDto = constructProductCompare(products);
			return compareDto;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	private ProductCompareDto constructProductCompare(List<ProductDetail> products) {
		ProductCompareDto compareDto = null;
		/* 组装默认的属性模版数据 
		 * 
		 * {"inputParam":"false","paramId":"211","paramName":"适用对象","paramValueId":"648","paramValueName":"青年"},
			{"inputParam":"false","paramId":"221","paramName":"箱包硬度","paramValueId":"658","paramValueName":"硬"},
			{"inputParam":"false","paramId":"214","paramName":"有无夹层","paramValueId":"651","paramValueName":"有"},
			{"inputParam":"false","paramId":"212","paramName":"闭合方式","paramValueId":"650","paramValueName":"扣合"},
			{"inputParam":"false","paramId":"210","paramName":"款式","paramValueId":"639","paramValueName":"手提包"}
		 * */
		String defaultParameter = parseParam(products);
		List<ProdParamDto> defaultParamDtoList = null;
		
		/*
		 * {{"id":221,"isdiffen":false,"name":"箱包硬度"}:[null,null,null,null],
         *  {"id":222,"isdiffen":false,"name":"适用场景"}:[null,null,null,null]}}
		 */
		Map<IdNameDto, String[]> defaultParamMap=null;
		if(AppUtils.isNotBlank(defaultParameter)){
			defaultParamMap = new HashMap<IdNameDto, String[]>();
			defaultParamDtoList = parseParam(defaultParameter);
			
			/*
			 * 组装成paramMap 
			 */
			for (ProdParamDto prodParamDto : defaultParamDtoList) {
				if(AppUtils.isBlank(prodParamDto.getParamValueId())){
					continue;
				}
				String[] labels = new String[4];
				defaultParamMap.put(new IdNameDto(prodParamDto.getParamId(),prodParamDto.getParamName()), labels);
			}
		}
		//System.out.println(JSONUtil.getJson(defaultParamMap));
		
		/*   组装 compareDto */
		if (AppUtils.isNotBlank(products)) {
			compareDto = new ProductCompareDto();
			int size = products.size();
			for (int i = 0; i < size; i++) {
				ProductDetail detail = products.get(i);
				if(detail==null){
				   continue;
				}
				Long prodId = detail.getProdId();
				double price=detail.getCash();
				String name = detail.getName();
				String pic = detail.getPic();
				String categoryName=detail.getCategoryName();
				String brandName=detail.getBrandName();
				IdNameImgDto nameImgDto = new IdNameImgDto();
				nameImgDto.setId(prodId);
				nameImgDto.setName(name);
				nameImgDto.setImage(pic);
				compareDto.getPrice()[i]=price;
				compareDto.getCategoryName()[i]=categoryName;
				compareDto.getBrandName()[i]=brandName;
				compareDto.getIdNameImg()[i]=nameImgDto;
				
				/* 判断商品的商品参数 是否能在默认模版找到相应的 数据信息  */
				if(AppUtils.isNotBlank(detail.getParameter()) && defaultParamMap!=null ){
					 /*
					  * 1、获取商品自身的参数属性
					  *  2、跟默认的defaultParamMap 比对 看看是否拥有该属性值
					  */
					 List<ProdParamDto> paramDtoList = parseParam(detail.getParameter());
					 for (int j = 0; j < paramDtoList.size(); j++) {
						 ProdParamDto prodParamDto  = paramDtoList.get(j);
						 setIdNameDto(defaultParamMap,prodParamDto.getParamId(),prodParamDto.getParamValueName(),i);
					}
				}
			}
			
			/*
			 * 处理不相同的属性
			 * */
			diffendParam(defaultParamMap,products.size());
			
			compareDto.setParamMap(defaultParamMap);
		}
		return compareDto;
	};
	
	
	private void setIdNameDto(Map<IdNameDto, String[]>  map,Long paramId,String valueName,int i){
		for (Entry<IdNameDto, String[]> element : map.entrySet()) {
			IdNameDto idNameDto=element.getKey();
			if(idNameDto.getId().equals(paramId)){
				 String[] lables=element.getValue();
				 if(AppUtils.isNotBlank(lables)){
					 lables[i] =valueName;
				}
			}
			
		}
	}
	
	/*
	 * 处理不相同的属性
	 */
	private void diffendParam(Map<IdNameDto, String[]> defaultParamMap,int number){
		if(AppUtils.isBlank(defaultParamMap)){
			return;
		}
		for (Entry<IdNameDto, String[]> entry : defaultParamMap.entrySet()) {
			String[] value= entry.getValue();
			IdNameDto nameDto=entry.getKey();
			int n=0;
			for (int i = 0; i < value.length; i++) {
				if(AppUtils.isNotBlank(value[i])){
					n++;
				}
			}
			if(number==n || n==0 ){
				nameDto.setIsdiffen(false);
			}else{
				nameDto.setIsdiffen(true);
			}
		}
	}
	
	/**
	 * 找到商品的参数，如果前面的没有则往下找
	 * @param products
	 * @return
	 */
	private String parseParam(List<ProductDetail> products) {
		if(AppUtils.isBlank(products)){
			return null;
		}
		for (int i = 0; i < products.size(); i++) {
			if(AppUtils.isBlank(products.get(i))){
				continue;
			}
			if(AppUtils.isNotBlank(products.get(i).getParameter()))
				return products.get(i).getParameter();
		}
		return null;
	}

	/**
	 * 把Json参数组装为Dto
	 * @param parameter
	 * @return
	 */
	private List<ProdParamDto> parseParam(String parameter){
		return JSONUtil.getArray(parameter, ProdParamDto.class);
	}
}
