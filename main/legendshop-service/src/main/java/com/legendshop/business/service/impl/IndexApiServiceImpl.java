/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.entity.FloorDto;
import com.legendshop.spi.service.FloorLayoutFactoryService;
import com.legendshop.spi.service.IndexApiService;

/**
 * 加载首页楼层
 *
 */
@Service("indexApiService")
public class IndexApiServiceImpl implements IndexApiService {
	
    
	@Autowired
    private FloorLayoutFactoryService floorLayoutFactoryService;

	/**
	 * 加载用户的楼层信息.
	 *
	 * @return the list< floor dto>
	 */
	@Override
	public List<FloorDto> loadFloor() {
		//1. 所有楼层
		return floorLayoutFactoryService.loadFloor();
	}

	
}
