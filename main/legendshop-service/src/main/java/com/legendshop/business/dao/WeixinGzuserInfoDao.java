/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * 微信用户信息Dao.
 */

public interface WeixinGzuserInfoDao extends GenericDao<WeixinGzuserInfo, Long> {
     
    /**
     * Gets the weixin gzuser info.
     *
     * @param shopName the shop name
     * @return the weixin gzuser info
     */
    public abstract WeixinGzuserInfo getWeixinGzuserInfo(String shopName);

	/**
	 * Gets the weixin gzuser info.
	 *
	 * @param id the id
	 * @return the weixin gzuser info
	 */
	public abstract WeixinGzuserInfo getWeixinGzuserInfo(Long id);
	
    /**
     * Delete weixin gzuser info.
     *
     * @param weixinGzuserInfo the weixin gzuser info
     * @return the int
     */
    public abstract int deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	/**
	 * Save weixin gzuser info.
	 *
	 * @param weixinGzuserInfo the weixin gzuser info
	 * @return the long
	 */
	public abstract Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	/**
	 * Update weixin gzuser info.
	 *
	 * @param weixinGzuserInfo the weixin gzuser info
	 * @return the int
	 */
	public abstract int updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	/**
	 * Gets the user by group id.
	 *
	 * @param groupId the group id
	 * @return the user by group id
	 */
	public abstract List<WeixinGzuserInfo> getUserByGroupId(Long groupId);
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public abstract List<WeixinGzuserInfo> getUserInfo();

	/**
	 * Query simple page.
	 *
	 * @param curPageNO the cur page no
	 * @param key the key
	 * @return the page support< weixin gzuser info>
	 */
	public abstract PageSupport<WeixinGzuserInfo> querySimplePage(String curPageNO, String key);

	
 }
