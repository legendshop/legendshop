/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminUser;
import com.legendshop.model.entity.Role;

/**
 * The Class AdminUserDao.
 */

public interface AdminUserDao extends Dao<AdminUser, String> {
     
	public abstract AdminUser getAdminUser(String id);
	
    public abstract int deleteAdminUser(AdminUser adminUser);
	
	public abstract String saveAdminUser(AdminUser adminUser);
	
	public abstract int updateAdminUser(AdminUser adminUser);
	
	public abstract PageSupport<AdminUser> getAdminUser(CriteriaQuery cq);

	public abstract Boolean isAdminUserExist(String userName);

	public abstract int updateAdminUserPassword(String id, String newPassword);

	public abstract List<Role> loadRole(String appNo);

	public abstract List<Role> loadRole(String userId, String appNo);
	
 }
