/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.predeposit;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PdCashHolding;

/**
 * 预存款拦截Dao接口
 * 
 */
public interface PdCashHoldingDao extends GenericDao<PdCashHolding, Long> {

	/** 获取预存款拦截 */
	public abstract PdCashHolding getPdCashHolding(Long id);

	/** 删除预存款拦截 */
	public abstract int deletePdCashHolding(PdCashHolding pdCashHolding);

	/** 保存预存款拦截 */
	public abstract Long savePdCashHolding(PdCashHolding pdCashHolding);

	/** 更新预存款拦截 */
	public abstract int updatePdCashHolding(PdCashHolding pdCashHolding);

	/** 获取预存款拦截页面 */
	public abstract PageSupport<PdCashHolding> getPdCashHoldingPage(String curPageNO, String userId, String sn, String type);

}
