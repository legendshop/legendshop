/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Hotsearch;

/**
 * 热门商品Dao.
 */
public interface HotsearchDao extends GenericDao<Hotsearch, Long>{

	public abstract List<Hotsearch> getHotsearch();

	public abstract void deleteHotsearchById(Long id);

	public abstract void updateHotsearch(Hotsearch hotsearch);

	public abstract PageSupport<Hotsearch> queryHotsearch(DataSortResult result, String curPageNO, Hotsearch hotsearch);

}