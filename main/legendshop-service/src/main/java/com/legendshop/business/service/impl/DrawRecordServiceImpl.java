/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DrawRecordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawRecord;
import com.legendshop.spi.service.DrawRecordService;
import com.legendshop.util.AppUtils;

/**
 * 活动抽奖记录服务实现类.
 */
@Service("drawRecordService")
public class DrawRecordServiceImpl  implements DrawRecordService{

	@Autowired
    private DrawRecordDao drawRecordDao;

    public DrawRecord getDrawRecord(Long id) {
        return drawRecordDao.getDrawRecord(id);
    }

    public void deleteDrawRecord(DrawRecord drawRecord) {
        drawRecordDao.deleteDrawRecord(drawRecord);
    }

    public Long saveDrawRecord(DrawRecord drawRecord) {
        if (!AppUtils.isBlank(drawRecord.getId())) {
            updateDrawRecord(drawRecord);
            return drawRecord.getId();
        }
        return drawRecordDao.saveDrawRecord(drawRecord);
    }

    public void updateDrawRecord(DrawRecord drawRecord) {
        drawRecordDao.updateDrawRecord(drawRecord);
    }

	/**
	 * @Description: 根据参数获取数据
	 * @param userId
	 * @param actId
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public Long getDrawRecordCount(String userId, Long actId) {
		return drawRecordDao.getDrawRecordCount(userId, actId);
	}

	/**
	 * @Description: 根据条件获取当天记录数
	 * @param userId
	 * @param actId
	 * @param date
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public Long getDRCountByCurrentDay(String userId, Long actId) {
		return drawRecordDao.getDRCountByCurrentDay(userId, actId);
	}

	@Override
	public PageSupport<DrawRecord> queryDrawRecordPage(String curPageNO, DrawRecord drawRecord) {
		return drawRecordDao.queryDrawRecordPage(curPageNO,drawRecord);
	}
}
