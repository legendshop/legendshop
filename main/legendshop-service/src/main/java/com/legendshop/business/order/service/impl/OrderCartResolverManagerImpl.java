package com.legendshop.business.order.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.spi.service.OrderCartResolverManager;

/**
 * 订单处理器封装器
 */
@Service("orderCartResolverManager")
public class OrderCartResolverManagerImpl implements OrderCartResolverManager {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(OrderCartResolverManagerImpl.class);
	
	@Resource(name="cartResolverExecutors")
	private Map<String, IOrderCartResolver> cartResolverExecutors; 
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.order.service.impl.OrderCartResolverManager#queryOrders(com.legendshop.model.constant.CartTypeEnum, java.util.Map)
	 */
	@Override
	public UserShopCartList queryOrders(CartTypeEnum typeEnum,Map<String, Object> params) {
		if(typeEnum == null){
			log.warn("OrderCartResolverManager typeEnum is null");
			return null;
		}
		IOrderCartResolver executor = cartResolverExecutors.get(typeEnum.toCode());
		if(executor!=null){
			//把业务规则应用到商品
			return executor.queryOrders(params);
		}
		log.warn("OrderCartResolverManager executor is null");
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.order.service.impl.OrderCartResolverManager#addOrder(com.legendshop.model.constant.CartTypeEnum, com.legendshop.model.dto.buy.UserShopCartList)
	 */
	@Override
	public  List<String> addOrder(CartTypeEnum typeEnum,UserShopCartList userShopCartList) {
		if(typeEnum == null){
			log.warn("OrderCartResolverManager typeEnum is null");
			return null;
		}
		IOrderCartResolver executor =cartResolverExecutors.get(typeEnum.toCode());
		if(executor!=null){
			return executor.addOrder(userShopCartList);
		}
		userShopCartList=null;
		log.warn("OrderCartResolverManager executor is null");
		return null;
	}

}
