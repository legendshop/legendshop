package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTag;
/**
 * 商品标签
 *
 */
public interface ProdTagDao extends GenericDao<ProdTag, Long>{

	public abstract PageSupport<ProdTag> getProdTagManage(String curPageNO, ProdTag prodTag, Long shopId);
	
}
