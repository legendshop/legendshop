/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.HeadlineAdminDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;
import com.legendshop.util.AppUtils;

/**
 * 头条管理Dao
 */
@Repository
public class HeadlineAdminDaoImpl extends GenericDaoImpl<Headline, Long> implements HeadlineAdminDao  {

	@Override
	public Headline getHeadlineAdmin(Long id) {
		
		return this.getById(id);
	}

	@Override
	public void deleteHeadlineAdmin(Long id) {
		this.deleteById(id);
	}

	@Override
	public Long saveHeadlineAdmin(Headline headline) {
		return this.save(headline);
	}

	@Override
	public void updateHeadlineAdmin(Headline headline) {
		this.update(headline);
	}

	@Override
	public PageSupport<Headline> getHeadlineAdmin(String curPageNO, Headline headline) {
		CriteriaQuery cq = new CriteriaQuery(Headline.class, curPageNO);
		cq.setPageSize(10);
		if(AppUtils.isNotBlank(headline.getTitle())){
			cq.like("title", "%" +headline.getTitle().trim()+"%");
		}
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}
    
   
	
 }
