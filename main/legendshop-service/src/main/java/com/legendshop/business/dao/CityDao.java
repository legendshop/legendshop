package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.City;

public interface CityDao extends GenericDao<City, Integer> {

	List<KeyValueEntity> getCitiesList(Integer provinceid);

	List<City> getCityByProvinceid(Integer provinceid);

	void deleteCityByProvinceid(Integer id);

	void deleteCity(Integer id);

	void updateCity(City city);

	List<City> getCityByName(String cityName); 

}
