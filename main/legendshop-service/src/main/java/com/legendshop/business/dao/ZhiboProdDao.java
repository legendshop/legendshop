/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.zhibo.ZhiboRoomMemberDto;
import com.legendshop.model.dto.zhibo.ZhiboRoomProdDto;
import com.legendshop.model.entity.ZhiboProd;

/**
 * The Class ZhiboProdDao.
 * Dao接口
 */
public interface ZhiboProdDao extends GenericDao<ZhiboProd, Long> {

   	/**
	 *  根据Id获取
	 */
	public abstract ZhiboProd getZhiboProd(Long id);
	
   /**
	 *  删除
	 */
    public abstract int deleteZhiboProd(ZhiboProd zhiboProd);
    
   /**
	 *  保存
	 */	
	public abstract Long saveZhiboProd(ZhiboProd zhiboProd);

   /**
	 *  更新
	 */		
	public abstract int updateZhiboProd(ZhiboProd zhiboProd);
	
	public List<Long> updateAvRoomProd(Long avRoomId,Long[] prodId);

	public abstract PageSupport<ZhiboRoomProdDto> querySimplePage(String curPageNO, String avRoomId, String prodName);

	public abstract PageSupport<ZhiboRoomMemberDto> queryZhiboRoomProdq(String curPageNO, String avRoomId, String userName);
	
 }
