/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.integral.IntegralOrderItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.integral.IntegralOrderItem;

/**
 * 积分订单项目Dao实现类.
 */
@Repository
public class IntegralOrderItemDaoImpl extends GenericDaoImpl<IntegralOrderItem, Long> implements IntegralOrderItemDao {

	/** 获取积分订单项目 */
	public IntegralOrderItem getIntegralOrderItem(Long id) {
		return getById(id);
	}

	/** 删除积分订单项目 */
	public int deleteIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		return delete(integralOrderItem);
	}

	/** 保存积分订单项目 */
	public Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		return save(integralOrderItem);
	}
	
	/** 更新积分订单项目 */
	public int updateIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		return update(integralOrderItem);
	}

	/** 根据用户id和商品id获取积分订单项目 */
	@Override
	public List<IntegralOrderItem> queryByUserIdAndProdId(String userId, Long prodId) {
		return this.query("select * from ls_integral_order_item where user_id=? and prod_id=?", IntegralOrderItem.class, userId, prodId);
	}

	@Override
	public List<IntegralOrderItem> queryByOrderId(Long orderId) {
		return this.query("select * from ls_integral_order_item where order_id=?", IntegralOrderItem.class, orderId);
	}

}
