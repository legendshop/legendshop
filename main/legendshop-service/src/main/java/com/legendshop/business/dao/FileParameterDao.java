package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.FileParameter;

public interface FileParameterDao extends Dao<FileParameter, String> {


    FileParameter findByName(String fileName);

    void deleteByName(String fileName);

    String save(FileParameter fileParameter);

    int update(FileParameter fileParameter);

}
