/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.mergeGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.log.TimerLog;
import com.legendshop.business.dao.MergeGroupAddDao;
import com.legendshop.business.dao.MergeGroupDao;
import com.legendshop.business.dao.MergeGroupOperateDao;
import com.legendshop.business.dao.MergeProductDao;
import com.legendshop.business.dao.MessageDao;
import com.legendshop.business.dao.MsgTextDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.constant.SubStatusEnum;
import com.legendshop.model.dto.BackRefundResponseDto;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupAdd;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.MergeProduct;
import com.legendshop.model.entity.Msg;
import com.legendshop.model.entity.MsgText;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.spi.service.BackRefundProcessService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 *  拼团活动服务实现类
 */
@Service("mergeGroupService")
public class MergeGroupServiceImpl  implements MergeGroupService{
	
	@Autowired
	private MergeGroupDao mergeGroupDao;

	@Autowired
	private MergeProductDao mergeProductDao;

	@Autowired
	private MergeGroupOperateDao mergeGroupOperateDao;

	@Autowired
	private MergeGroupAddDao mergeGroupAddDao;

	@Autowired
	private SubDao subDao;

	@Autowired
	private SubHistoryDao subHistoryDao;

	@Autowired
	private MsgTextDao msgTextDao;

	@Autowired
	private MessageDao messageDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private BackRefundProcessService backRefundProcessService;
	
	@Autowired
	private SkuDao skuDao;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	public PageSupport<MergeGroup> getAvailable(String curPageNO) {
		return mergeGroupDao.getAvailable(curPageNO);
	}

	/**
	 *  根据Id获取拼团活动
	 */
	public MergeGroup getMergeGroup(Long id) {
		return mergeGroupDao.getMergeGroup(id);
	}

	/**
	 *  删除拼团活动
	 */ 
	public void deleteMergeGroup(MergeGroup mergeGroup) {
		mergeGroupDao.deleteMergeGroup(mergeGroup);
	}

	/**
	 *  保存拼团活动
	 */	    
	public Long saveMergeGroup(MergeGroup mergeGroup) {
		if (!AppUtils.isBlank(mergeGroup.getId())) {
			updateMergeGroup(mergeGroup);
			return mergeGroup.getId();
		}
		return mergeGroupDao.saveMergeGroup(mergeGroup);
	}

	/**
	 *  更新拼团活动
	 */	
	public void updateMergeGroup(MergeGroup mergeGroup) {
		mergeGroupDao.updateMergeGroup(mergeGroup);
	}

	/**
	 * 获取拼团活动列表
	 */
	public PageSupport<MergeGroupDto> getMergeGroupList(String curPageNO, Integer pageSize, MergeGroupDto mergeGroup) {
		return mergeGroupDao.getMergeGroupList(curPageNO,pageSize,mergeGroup);
	}
	
	/**
	 * 查询所有 未成团成功的参与拼团信息;
	 * */
	public Integer findHaveInMergeGroupOperate(Long id) {
	
		List<MergeGroupOperate> mergeGroupOperates = mergeGroupOperateDao.findHaveInMergeGroupOperate(id);
		if (AppUtils.isNotBlank(mergeGroupOperates)){
			return 1;		
		}
		return 0;
	}
	/**
	 * 更新拼团状态
	 */
	public void updateMergeGroupStatus(Long id, Integer status) {

		if(AppUtils.isNotBlank(status)){

			if(MergeGroupStatusEnum.OFFLINE.value().equals(status)){ //如果是下线操作
				/**
				 * 查询所有 未成团成功的参与拼团信息;
				 */
				List<MergeGroupOperate> mergeGroupOperates= mergeGroupOperateDao.findHaveInMergeGroupOperate(id);
				int groupNumber=0;
				if (AppUtils.isNotBlank(mergeGroupOperates)){
					for(MergeGroupOperate groupOperate:mergeGroupOperates ){
						TimerLog.info("######### groupOperate={} ########",JSON.toJSONString(groupOperate));
						/**
						 * 处理拍卖保证金的原路退还操作
						 */
						
						backReturnMergeGroup(groupOperate);
						
						groupNumber++;
					}
				}

				if (groupNumber == mergeGroupOperates.size()) { // 全部退款处理成功,更新服务
					mergeGroupDao.updateMergeGroupStatus(id, status);
					TimerLog.info("######### 更新拼团的参团状态 ########");
					
					//查找拼团活动的sku
					List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(id);
					if(AppUtils.isNotBlank(mergeProducts)) {
						for (MergeProduct mergeProduct : mergeProducts) {
							//重置拼团商品sku的标记营销状态
							skuDao.updateSkuTypeById(mergeProduct.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.MERGE_GROUP.value());
						}
					}
					
				}
			}else if(MergeGroupStatusEnum.NO_AUDITED.value().equals(status)){ //审核不通过
				mergeGroupDao.updateMergeGroupStatus(id,status);
			}else {
				mergeGroupDao.updateMergeGroupStatus(id,status);
			}

		}
	}

	public void saveMergeGroupAndProd(MergeGroup mergeGroup,List<MergeProduct> mergeProductList, MultipartFile file, String userName, String userId, Long shopId) {
		String pic=savePrice(mergeGroup,mergeProductList,file);
		Long id = mergeGroupDao.saveMergeGroup(mergeGroup);
		if(AppUtils.isNotBlank(pic)){
			//保存附件表
			attachmentManager.saveImageAttachment(userName, userId,shopId, pic, file, AttachmentTypeEnum.MERGEGROUP);
		}
		batchSaveMergeProduct(mergeGroup,mergeProductList);
	}

	public void updateMergeGroupAndProd(MergeGroup mergeGroup,List<MergeProduct> mergeProductList, MultipartFile file, String userName, String userId, Long shopId) {
		String pic=savePrice(mergeGroup,mergeProductList,file);
		mergeGroupDao.updateMergeGroup(mergeGroup);

		if(AppUtils.isNotBlank(pic)){
			//保存附件表
			attachmentManager.saveImageAttachment(userName, userId,shopId, pic, file, AttachmentTypeEnum.MERGEGROUP);
		}

		List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(mergeGroup.getId());
		if(AppUtils.isNotBlank(mergeProducts)) {
			//拼团商品，每个productId都是一样的，所有取第一个
			Long productId = mergeProducts.get(0).getProdId();
			//重置sku的营销状态
			skuDao.updateSkuTypeByProdId(productId, SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.MERGE_GROUP.value());
		}

		mergeProductDao.deleteMergeProductBymergeId(mergeGroup.getId());
		batchSaveMergeProduct(mergeGroup,mergeProductList);
	}

	public void batchSaveMergeProduct(MergeGroup mergeGroup,List<MergeProduct> mergeProductList){

		for(MergeProduct mergeProduct:mergeProductList){
			mergeProduct.setShopId(mergeGroup.getShopId());
			mergeProduct.setMergeId(mergeGroup.getId());

			//标记sku为拼团活动
			Sku sku = skuDao.getSkuById(mergeProduct.getSkuId());
			sku.setSkuType(SkuActiveTypeEnum.MERGE_GROUP.value());
			skuDao.update(sku);
		}

		mergeProductDao.batchSaveMergeProduct(mergeProductList);
	}

	public String savePrice(MergeGroup mergeGroup,List<MergeProduct> mergeProductList,MultipartFile file){
		String pic = null;
		if (file != null && file.getSize() > 0) {
			pic = attachmentManager.upload(file);
			mergeGroup.setPic(pic);
		}


		Double minPrice = null;
		Double minProdPrice = null;
		for(MergeProduct mergeProduct:mergeProductList){
			if(AppUtils.isBlank(minPrice)){
				minPrice = mergeProduct.getMergePrice();
			}
			if(AppUtils.isBlank(minProdPrice)){
				minProdPrice = mergeProduct.getSkuPrice();
			}

			if(Double.doubleToLongBits(minPrice)>Double.doubleToLongBits(mergeProduct.getMergePrice())){
				minPrice = mergeProduct.getMergePrice();
			}
			if(Double.doubleToLongBits(minProdPrice)>Double.doubleToLongBits(mergeProduct.getSkuPrice())){
				minProdPrice = mergeProduct.getSkuPrice();
			}
		}

		mergeGroup.setMinPrice(minPrice);//最低拼团价格
		mergeGroup.setMinProdPrice(minProdPrice);//最低sku商品价格
		return pic;
	}



	public Integer updateStatusByShopId(Long id, Integer status, Long shopId) {
		
		List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(id);
		Long productId = null;
		if(AppUtils.isNotBlank(mergeProducts)) {
			//一个拼团的productId都一样，取第一个
			productId = mergeProducts.get(0).getProdId();
		}
		//重置sku营销状态
		skuDao.updateSkuTypeByProdId(productId, SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.MERGE_GROUP.value());
		
		
		
		
		
		return mergeGroupDao.updateStatusByShopId(id,status,shopId);
	}

	/**
	 * 运营详情列表
	 */
	public PageSupport<MergeGroupOperationDto> getMergeGroupOperationList(String curPageNO, Integer pageSize,Long mergeId,Long shopId) {
		return mergeGroupDao.getMergeGroupOperationList(curPageNO,pageSize,mergeId,shopId);
	}

	public PageSupport<MergeGroup> queryMergeGroupList(String curPageNO,Long shopId,MergeGroupDto mergeGroup) {
		return mergeGroupDao.queryMergeGroupList(curPageNO,shopId,mergeGroup);
	}

	public MergeGroup getMergeGroupByshopId(Long id, Long shopId) {
		return mergeGroupDao.getMergeGroupByshopId(id,shopId);
	}

	public MergeGroup getMergeGroupByshopId(Long id) {
		return mergeGroupDao.getMergeGroupByshopId(id);
	}

	/**
	 * 删除拼团活动
	 */
	public void deleteMergeGroupByShopId(Long shopId, Long id) {
		List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(id);
		if(AppUtils.isNotBlank(mergeProducts)) {
			//拼团商品，每个productId都是一样的，所有取第一个
			Long productId = mergeProducts.get(0).getProdId();
			//重置sku的营销状态
			skuDao.updateSkuTypeByProdId(productId, SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.MERGE_GROUP.value());
		}
		mergeGroupDao.deleteMergeGroupByShopId(shopId,id);
	}

	public boolean checkExitproduct(Long prodId) {

		return mergeGroupDao.checkExitproduct(prodId);
	}

	public List<MergeGroup> getMergeGroupAll() {
		return mergeGroupDao.getMergeGroupAll();
	}

	public List<MergeGroup> queryMergeGroupListByStatus() {
		return mergeGroupDao.queryMergeGroupListByStatus();
	}

	public void dealWithMergeGroup(List<MergeGroup> cancalGroups, List<MergeGroupOperate> cancalOperates) {
		List<Long> ids = new ArrayList<Long>();
		List<Long> subIds = new ArrayList<Long>();
		Map<Long,Boolean> map = new HashMap<Long,Boolean>();
		for (MergeGroup group : cancalGroups) {
			map.put(group.getId(), group.getIsHeadFree());
		}

		if(AppUtils.isNotBlank(cancalOperates)){
			for (MergeGroupOperate operate : cancalOperates) {
				operate.setStatus(MergeGroupAddStatusEnum.FAIL.value());
				ids.add(operate.getId());
			}
		}

		List<MergeGroupAdd> adds = mergeGroupAddDao.getMergeGroupAddByIds(ids);
		if(AppUtils.isNotBlank(adds)){
			for (MergeGroupAdd mergeGroupAdd : adds) {
				if(mergeGroupAdd.getIsHead()){
					Boolean flag = map.get(mergeGroupAdd.getMergeId());
					if(!flag){
						subIds.add(mergeGroupAdd.getSubId());
						continue;
					}
				}
				subIds.add(mergeGroupAdd.getSubId());
			}
		}

		dealWithSub(subIds);
		mergeGroupDao.updateList(cancalGroups);
		mergeGroupOperateDao.updateMergeGroupOperateList(cancalOperates);

	}

	private void dealWithSub(List<Long> subIds){
		if(AppUtils.isNotBlank(subIds)){
			List<Sub> subs = subDao.queryAllByIds(subIds);
			List<SubHistory> his = new ArrayList<SubHistory>();
			List<MsgText> texts = new ArrayList<MsgText>();
			List<Msg> msgs = new ArrayList<Msg>();
			for (Sub sub : subs) {
				sub.setMergeGroupStatus(-1);
				sub.setStatus(OrderStatusEnum.CLOSE.value());

				SubHistory subHistory = new SubHistory();
				Date time = new Date();
				String timeStr = DateUtil.DateToString(time,"yyyy-MM-dd HH:mm:ss");
				subHistory.setRecDate(time);
				subHistory.setStatus(SubStatusEnum.CHANGE_STATUS.value());
				subHistory.setSubId(sub.getSubId());
				subHistory.setUserName("系统");
				String reason = "系统于" + timeStr + "取消订单，原因：拼团未成功";
				subHistory.setReason(reason);
				his.add(subHistory);

				MsgText text = new MsgText();
				text.setTitle("拼团通知");
				text.setText("亲，您参与的拼团未能成团");
				text.setRecDate(new Date());
				texts.add(text);

				Msg msg = new Msg();
				msg.setSendName("系统");
				msg.setStatus(0);
				msg.setIsGlobal(0);
				msg.setDeleteStatus(0);
				msg.setDeleteStatus2(0);
				msg.setReceiveName(sub.getUserName());
				msgs.add(msg);
			}
			subDao.update(subs);
			List<Long> mids = msgTextDao.save(texts);
			for (int i = 0; i < mids.size(); i++) {
				mids.get(i);
				msgs.get(i).setTextId(mids.get(i));
			} 
			messageDao.save(msgs);
			subHistoryDao.saveSubHistoryList(his);

			//TODO 原路返回

		}
	}

	public boolean isGroupExemption(String addBumber, String userId){
		return mergeGroupDao.isGroupExemption(addBumber,userId);
	}

	@Override
	public void finishDepositJob(List<MergeGroupOperate> mergeGroupOperates,Integer groupSurvivalTime,Long currentTimeMillis) {
		
		for(MergeGroupOperate groupOperate:mergeGroupOperates ){
			/**
			 * 开团的时间往后延推
			 */
			Calendar createTime=Calendar.getInstance();
			createTime.setTime(groupOperate.getCreateTime());
			createTime.add(Calendar.HOUR,groupSurvivalTime);
			if(currentTimeMillis>=createTime.getTimeInMillis()){
				
				TimerLog.info("######### groupOperate={} ########",JSON.toJSONString(groupOperate));
				/**
				 * 处理拼团的原路退还操作
				 */
				backReturnMergeGroup(groupOperate);
				
			}
		}
		
		
	}

	private void backReturnMergeGroup(MergeGroupOperate groupOperate) {
		
		List<MergeGroupAdd> groupAdds= mergeGroupAddDao.findHaveInMergeGroupAdd(groupOperate.getId());
		
		if(AppUtils.isBlank(groupAdds)){
			groupOperate.setStatus(MergeGroupAddStatusEnum.FAIL.value());
			mergeGroupOperateDao.updateMergeGroupOperate(groupOperate);
			return;
		}
		
		int number=0;
		for(MergeGroupAdd groupAdd:groupAdds){
			try {
				
				BackRefundResponseDto refundResponseDto=backRefundProcessService.backRefund("MERGE_GROUP",String.valueOf(groupAdd.getId()));
				
				if(refundResponseDto==null){
					TimerLog.error("处理原路退还操作失败 = {}", JSON.toJSONString(groupAdd.getId()));
				}else{
					if(!refundResponseDto.isResult()){
						TimerLog.warn("######### 退款中心反馈退款结果={} ########",refundResponseDto.getMessage());
					}else{
						
						/**
						 * 处理订单
						 */
						Date time = new Date();
						String timeStr = DateUtil.DateToString(time,"yyyy-MM-dd HH:mm:ss");

						String historyReason = "系统于" + timeStr + "取消订单，原因：拼团未成功";
						String subReason = "系统取消订单，原因：拼团未成功";

						//更新订单状态
						int status = subDao.update("update ls_sub set status = ?,merge_group_status = ?,cancel_reason = ? where sub_id = ? and status != ?", 
								OrderStatusEnum.CLOSE.value(),-1,subReason,groupAdd.getSubId(),OrderStatusEnum.CLOSE.value());
						//更新成功保存到订单历史
						if (status > 0) {
							SubHistory subHistory = new SubHistory();	
							subHistory.setRecDate(time);
							subHistory.setStatus(SubStatusEnum.CHANGE_STATUS.value());
							subHistory.setSubId(groupAdd.getSubId());
							subHistory.setUserName("系统");	
							subHistory.setReason(historyReason);
							subHistoryDao.saveSubHistory(subHistory);
							//发送站内信通知
							sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", groupAdd.getUserName(),"您参加的拼团活动到期未成团" ,"您参加的拼团活动到期未成团,资金将原路返回." ).getSource());
							number++;
						}	
						
					}
				}	
			} catch (Exception e) {
				TimerLog.error("处理原路退还操作 异常", e);
			}
		}
		
		if(number==groupAdds.size()){ //全部退款处理成功,更新服务
			groupOperate.setStatus(MergeGroupAddStatusEnum.FAIL.value());
			mergeGroupOperateDao.updateMergeGroupOperate(groupOperate);
			TimerLog.info("######### 更新拼团的参团状态 ########");
		}
		
	}

	@Override
	public void mergeGroupExemption(List<Long> ids) {
		for (Long id : ids) {
			try {
				
				BackRefundResponseDto refundResponseDto=backRefundProcessService.backRefund("GROUP_EXEMPTION",String.valueOf(id));
				
				if(refundResponseDto==null){
					TimerLog.error("处理原路退还操作失败 = {}", JSON.toJSONString(id));
				}else{
					if(!refundResponseDto.isResult()){
						TimerLog.warn("######### 退款中心反馈退款结果={} ########",refundResponseDto.getMessage());
					}
				}
				
			} catch (Exception e) {
				TimerLog.error("处理团长免单原路退还操作 异常 ,该团的ls_merge_group_add id= "+id, e);
			}
		}
		
	}

	/**
	 * 更新拼团状态并且释放sku
	 */
	@Override
	public int updateOffineMergeGroup(List<MergeGroup> groups) {
		// 当前时间
		long now = new Date().getTime();
		int processOrder = 0;
		for (MergeGroup mergeGroup : groups) {
			long endTime = mergeGroup.getEndTime().getTime();
			if(now >= endTime){

				//设置为过期活动
				mergeGroup.setStatus(MergeGroupStatusEnum.OUT_OF_TIME.value());
				mergeGroupDao.updateMergeGroup(mergeGroup);
				
				//查找拼团活动的sku
				List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(mergeGroup.getId());
				if(AppUtils.isNotBlank(mergeProducts)) {
					//拼团商品，每个productId都是一样的，所有取第一个
					Long productId = mergeProducts.get(0).getProdId();
					skuDao.updateSkuTypeByProdId(productId, SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.MERGE_GROUP.value());
				}
				processOrder++;
			}
		}
		return processOrder;
	}

	/**
	 * 更新拼团活动删除状态为商家删除
	 */
	@Override
	public void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus) {
		
		List<MergeProduct> mergeProducts = mergeProductDao.getMergeProductList(id);
		if(AppUtils.isNotBlank(mergeProducts)) {
			//拼团商品，每个productId都是一样的，所有取第一个
			Long productId = mergeProducts.get(0).getProdId();
			//重置sku的营销状态
			skuDao.updateSkuTypeByProdId(productId, SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.MERGE_GROUP.value());
		}
		
		mergeGroupDao.updateDeleteStatus(shopId,id,deleteStatus);
	}

	@Override
	public MergeGroup getMergeGroupPrice(Long prodId, Long skuId) {
		return mergeGroupDao.getMergeGroupPrice(prodId,skuId);
	}

	/**
	 * 获取运营结果统计数据
	 */
	@Override
	public OperateStatisticsDTO getOperateStatistics(Long mergeId,Long shopId) {
		
		// 统计拼团活动交易总金额以及总参与人数
		OperateStatisticsDTO operate = mergeGroupDao.getTotalAmountAndParticipants(mergeId,shopId);
		
		// 统计拼团活动成功交易金额以及成功拼团数
		OperateStatisticsDTO succeed =  mergeGroupDao.getSucceedAmountAndCount(mergeId,shopId);
		
		// 待成团的数量
		Integer waitFightCount =  mergeGroupDao.getWaitFightCount(mergeId,shopId);
		
		//组装统计数据
		operate.setSucceedAmount(succeed.getSucceedAmount());
		operate.setSucceedCount(succeed.getSucceedCount());
		operate.setWaitFightCount(waitFightCount);
		return operate;
	}

}
