/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Date;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ArticleThumbDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleThumb;

/**
 * The Class ArticleThumbDaoImpl.
  * Dao实现类
 */
@Repository
public class ArticleThumbDaoImpl extends GenericDaoImpl<ArticleThumb, Long> implements ArticleThumbDao  {
     
   	/**
	 *  根据Id获取
	 */
	public ArticleThumb getArticleThumb(Long id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteArticleThumb(ArticleThumb articleThumb){
    	return delete(articleThumb);
    }

   /**
	 *  保存
	 */		
	public Long saveArticleThumb(ArticleThumb articleThumb){
		return save(articleThumb);
	}

   /**
	 *  更新
	 */		
	public int updateArticleThumb(ArticleThumb articleThumb){
		return update(articleThumb);
	}


	@Override
	public PageSupport<ArticleThumb> saveThumb(Long artId,String userId) {
		ArticleThumb articleThumb = new ArticleThumb();
		articleThumb.setArtId(artId);
		articleThumb.setUserId(userId);
		Date date = new Date();
		articleThumb.setCreateTime(date);

		CriteriaQuery cq = new CriteriaQuery(ArticleThumb.class);
		cq.eq("artId", articleThumb.getArtId());
		cq.eq("userId", articleThumb.getUserId());
		return queryPage(cq);
	}
 }
