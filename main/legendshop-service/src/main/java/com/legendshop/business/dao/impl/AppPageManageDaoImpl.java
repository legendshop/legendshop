/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AppPageManageDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.util.AppUtils;

/**
 * The Class AppPageManageDaoImpl. 移动端页面装修页面Dao实现类
 * @author legendshop
 */
@Repository
public class AppPageManageDaoImpl extends GenericDaoImpl<AppPageManage, Long> implements AppPageManageDao{

	/**
	 * 根据Id获取移动端页面装修页面
	 */
	@Override
	public AppPageManage getAppPageManage(Long id){
		return getById(id);
	}

	/**
	 *  删除移动端页面装修页面
	 *  @param appPageManage 实体类
	 *  @return 删除结果
	 */	
    @Override
	public int deleteAppPageManage(AppPageManage appPageManage){
    	return delete(appPageManage);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteAppPageManage(Long id){
		return deleteById(id);
	}

	/**
	 * 保存移动端页面装修页面
	 */
	@Override
	public Long saveAppPageManage(AppPageManage appPageManage){
		return save(appPageManage);
	}

	/**
	 *  更新移动端页面装修页面
	 */		
	@Override
	public int updateAppPageManage(AppPageManage appPageManage){
		return update(appPageManage);
	}

	/**
	 * 分页查询移动端页面装修页面列表
	 */
	@Override
	public PageSupport<AppPageManage>queryAppPageManage(String curPageNO, Integer pageSize, AppPageManage appPageManage){
		
		curPageNO = AppUtils.isBlank(curPageNO) ? "1" : curPageNO;
		CriteriaQuery query = new CriteriaQuery(AppPageManage.class, curPageNO);
		query.setPageSize(pageSize);
		
		if (AppUtils.isNotBlank(appPageManage)) {
			
			if (AppUtils.isNotBlank(appPageManage.getCategory())) {
				query.eq("category", appPageManage.getCategory());
			}
			if (AppUtils.isNotBlank(appPageManage.getName())) {

				// 模糊搜索
				query.like("name", appPageManage.getName(),MatchMode.ANYWHERE);
			}
			if (AppUtils.isNotBlank(appPageManage.getStatus())) {
				query.eq("status", appPageManage.getStatus());
			}
		}
		query.addDescOrder("isUse");
		query.addDescOrder("status");
		query.addDescOrder("recDate");
		PageSupport<AppPageManage> ps = queryPage(query);
		return ps;
	}

	/**
	 * 将使用中的页面更新为未使用
	 */
	@Override
	public void updatePageToUnUse(Integer use) {
		
		String sql = "UPDATE ls_app_page_manage SET is_use = ? WHERE is_use = 1";
		this.update(sql, use);
	}

	/**
	 * 根据使用状态获取页面
	 */
	@Override
	public AppPageManage getUseAppPageByUse(Integer isUse) {
		
		String sql = "SELECT * FROM  ls_app_page_manage WHERE is_use = ?";
		return get(sql, AppPageManage.class, isUse);
	}

}
