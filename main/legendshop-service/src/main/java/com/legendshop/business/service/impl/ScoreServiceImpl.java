/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ScoreDao;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.ScoreService;

/**
 * 积分服务
 */
@Service("scoreService")
public class ScoreServiceImpl implements ScoreService {

	@Autowired
	private ScoreDao scoreDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.ScoreService#addScore(com.legendshop.
	 * model.entity.Sub)
	 */
	@Override
	public void addScore(Sub sub) {
		scoreDao.saveScore(sub);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.ScoreService#useScore(com.legendshop.
	 * model.entity.Sub, java.lang.Long)
	 */
	@Override
	@Deprecated
	public Map<String, Object> useScore(Sub sub, Integer avaibleScore) {
		return scoreDao.deleteScore(sub, avaibleScore);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.ScoreService#calScore(java.lang.Double,
	 * java.lang.String)
	 */
	@Override
	public Double calScore(Double total, String scoreType) {
		return scoreDao.calScore(total, scoreType);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.ScoreService#calMoney(java.lang.Long)
	 */
	@Override
	public Double calMoney(Integer score) {
		return scoreDao.calMoney(score);
	}
}
