/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductConsultDto;
import com.legendshop.model.entity.ProductConsult;

/**
 * 商品咨询Dao
 */
public interface ProductConsultDao extends Dao<ProductConsult, Long>{

	/**
	 * Gets the product consult list.
	 * 
	 * @param prodId
	 *            the prod id
	 * @return the product consult list
	 */
	public abstract List<ProductConsult> getProductConsultList(Long prodId);

	/**
	 * Gets the product consult.
	 * 
	 * @param id
	 *            the id
	 * @return the product consult
	 */
	public abstract ProductConsult getProductConsult(Long id);

	/**
	 * Delete product consult.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void deleteProductConsult(Long id);

	/**
	 * Save product consult.
	 * 
	 * @param productConsult
	 *            the product consult
	 * @return the long
	 */
	public abstract Long saveProductConsult(ProductConsult productConsult);

	/**
	 * Update product consult.
	 * 
	 * @param productConsult
	 *            the product consult
	 */
	public abstract void updateProductConsult(ProductConsult productConsult);


	/**
	 * Delete product consult.
	 * 
	 * @param consult
	 *            the consult
	 */
	public abstract void deleteProductConsult(ProductConsult consult);

	/**
	 * Check frequency.
	 * 
	 * @param consult
	 *            the consult
	 * @return the integer
	 */
	public abstract long checkFrequency(ProductConsult consult);
	
	
	public abstract void deleteProductConsultById(Long consId);
	
	public abstract void deleteProductConsultByShopUser(Long consId);

	public abstract ProductConsultDto getConsult(String userId, Long consId);

	public abstract int updateConsult(String userId, ProductConsultDto consult);

	public abstract int deleteProductConsult(String userId, Long consId);

	public abstract List<ProductConsult> queryUnReply();

	/**
	 * 根据商品Id 查找 咨询个数（没回答的）
	 * @param shopId
	 * @return
	 */
	public abstract int getConsultCounts(Long shopId);

	public abstract PageSupport<ProductConsult> getProductConsultPage(String userName, ProductConsult productConsult,
			String curPageNO);

	public abstract PageSupport<ProductConsult> queryQarseConsult(String curPageNO, Long shopId, Integer replyed);

	public abstract PageSupport<ProductConsult> queryProdcons(String curPageNO, Long shopId,
			ProductConsult productConsult);

	public abstract PageSupport<ProductConsult> queryConsult(String curPageNO, String userName);

	public abstract int batchDeleteConsultByIds(String consIdStr);

	PageSupport<ProductConsult> getProductConsult(String curPageNO, ProductConsult productConsult);

	PageSupport<ProductConsult> getProductConsultForList(String curPageNO, Integer pointType, Long prodId);
}
