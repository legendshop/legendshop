/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AccusationDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.AccusationDto;
import com.legendshop.model.entity.Accusation;
/**
 * 举报表Dao.
 */
@Repository
public class AccusationDaoImpl extends GenericDaoImpl<Accusation, Long> implements AccusationDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
     
	String detailedAccusationSQL ="select la.id as id,lp.name as prodName,la.prod_id as prodId,las.title as title,la.rec_date as recDate,la.result as result,la.user_name as userName," +
			"la.status as status,la.handle_time as handleTime,la.handle_info as handleInfo,la.content as content,la.pic1 as pic1, la.pic2 as pic2, la.pic3 as pic3,la.user_del_status as userDelStatus,lud.nick_name as nickName, lat.name AS accuType," +
			"  la.illegal_off AS illegalOff," +
			"  lp.pic AS prodPic " +
			"from ls_accusation la,ls_prod lp,ls_accu_subject las,ls_usr_detail lud,ls_accu_type lat  where la.prod_id = lp.prod_id and la.subject_id = las.as_id and la.user_id=lud.user_id  AND las.type_id = lat.type_id  and  la.id = ?";
	
	String AccusationById = " select a.id as id,a.content as content,a.status as status,a.pic1 as pic1, a.pic2 as pic2,a.pic3 as pic3,a.rec_date as recDate, a.subject_id as subjectId, " +
			" a.user_id as userId, a.user_name as userName, a.prod_id as prodId, las.type_id as typeId,p.name as prodName " +
			" from ls_accusation a,ls_accu_subject las,ls_prod p  where a.subject_id = las.as_id  and a.prod_id = p.prod_id  and a.id = ?";
	
    public List<Accusation> getAccusation(String userName){
    	List<Accusation> list = query("select * from ls_accusation where user_name = ?", Accusation.class, userName);
    	return list;
    }

	public Accusation getAccusation(Long id){
		return get(AccusationById,Accusation.class,id);
	}
	
    public void deleteAccusation(Accusation accusation){
    	delete(accusation);
    }
	
	public Long saveAccusation(Accusation accusation){
		return (Long)save(accusation);
	}
	
	public void updateAccusation(Accusation accusation){
		 update(accusation);
	}

	@Override
	public Accusation getDetailedAccusation(Long id) {
		return get(detailedAccusationSQL, Accusation.class, id);
	}

	/**
	 * 查询尚未处理的投诉
	 */
	@Override
	public List<Accusation> queryUnHandler() {
		QueryMap map = new QueryMap();
		map.put("status",0);//尚未处理的投诉
    	String querySQL = ConfigCode.getInstance().getCode("biz.queryAccusation", map); 
		return queryLimit(querySQL, Accusation.class, 0, 5, map.toArray());
	}

	@Override
	public int getAccusationCounts(Long shopId) {
		return get("select count(a.id) from ls_accusation a,ls_prod p where a.prod_id = p.prod_id and p.shop_id = ? and a.status = 0",Integer.class,shopId);
	}

	@Override
	public PageSupport<Accusation> queryReportForbit(String curPageNO, Long shopId) {
		 SimpleSqlQuery query = new SimpleSqlQuery(Accusation.class, systemParameterUtil.getPageSize(), curPageNO);
		 QueryMap map = new QueryMap();
		 map.put("shopId", shopId);
		 String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryAccusationCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("shop.queryAccusation", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Accusation> queryOverlayLoad(Long id) {
		SimpleSqlQuery query = new SimpleSqlQuery(Accusation.class);
		QueryMap map = new QueryMap();
		map.put("accusationId", id);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryAccusationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.queryAccusation", map); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<AccusationDto> querySimplePage(String curPageNO, String userId) {
		
		SimpleSqlQuery query = new SimpleSqlQuery(AccusationDto.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.favourites.queryAccusationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.favourites.queryAccusation", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Accusation> querySimplePage(Long id, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Accusation.class);
		QueryMap map = new QueryMap();
		map.put("accusationId", id);
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.favourites.queryAccusationCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.favourites.queryAccusation", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
 }
