/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter;

import java.util.Date;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.promotor.SubLevelUserDto;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.promoter.model.UserCommis;

/**
 * 用户佣金Dao.
 */
public interface UserCommisDao extends Dao<UserCommis, String> {

	/** 加载下级用户列表 */
	public abstract UserCommis getUserCommis(String userId);

	/** 根据用户名查找下级用户 */
	public abstract UserCommis getUserCommisByName(String userName);

	/** 删除用户佣金 */
	public abstract int deleteUserCommis(UserCommis userCommis);

	/** 保存用户佣金 */
	public abstract String saveUserCommis(UserCommis userCommis);

	/** 更新用户佣金 */
	public abstract int updateUserCommis(UserCommis userCommis);

	/** 查询各级用户数量 */
	public abstract SubUserCountDto getSubUserCount(String userName);

	/** 查询本月发展会员数 */
	public abstract Long getMonthUser(Date startTime, Date endTime, String userName);

	/** 根据用户ID更新已结算佣金 */
	public abstract int updateSettledDistCommis(Double updateValue, Double originalValue, String userId);
 
	/** 保存预存款变更记录 */
	public abstract void savePdCashLog(PdCashLog pdCashLog);

	/** 增加 结算佣金 */
	public abstract void addSettleDistCommis(String userName, Double commisCash);

	/** 减少 未结算佣金 */
	public abstract void reduceUnSettleDistCommis(String userName, Double commisCash);
	
	/** 增加直接下级贡献总金额 */
	public abstract void addFirstDistCommis(String userName, Double firstDistCommis);

	/** 增加下两级贡献总金额 */
	public abstract void addSecondDistCommis(String userName, Double secondDistCommis);

	/** 增加下三级贡献总金额 */
	public abstract void addThirdDistCommis(String userName, Double thirdDistCommis);

	/** 增加 累计获得的佣金 */
	public abstract void addTotalDistCommis(String userName, Double totalDistCommis);

	/** 增加 未结算佣金 */
	public abstract void addUnsettleDistCommis(String userName, Double unsettleDistCommis);

	/** 获取 我推广的会员 */
	public abstract PageSupport<UserCommis> getUserCommisByName(String curPageNO, String userName, UserCommis userCommis, int pageSize);

	/** 获取推广员列表数据 */
	public abstract PageSupport<UserCommis> getUserCommis(String curPageNO, UserCommis userCommis);

	/** 加载下级用户列表 */
	public abstract PageSupport<SubLevelUserDto> getUserCommis(String curPageNO, String level, String userName, String nickName);

	public abstract Integer batchCloseCommis(String userIdStr);
	
	/** 减少 未结算佣金 */
	public abstract void reduceUnSettleDistCommisAndTotalCommis(String userName, Double commisCash);
	
	/** 减少 直接下级会员贡献佣金 */
	public abstract void reduceFirstDistCommis(String userName, Double commisCash);
	
	/** 减少 下两级会员贡献佣金 */
	public abstract void reduceSecondDistCommis(String userName, Double commisCash);
	
	/** 减少 下三级会员贡献佣金 */
	public abstract void reduceThirdDistCommis(String userName, Double commisCash);

	/**
	 * 获取用户佣金记录
	 * @param userId 用户ID
	 * @return
	 */
	public abstract UserCommis getUserCommisByUserId(String userId);

}
