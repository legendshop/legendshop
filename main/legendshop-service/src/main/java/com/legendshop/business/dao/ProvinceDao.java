/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.Region;

/**
 * 
 *
 */
public interface ProvinceDao extends GenericDao<Province, Integer> {

	List<KeyValueEntity> getProvincesList();

	void deleteProvince(Integer id);

	void updateProvince(Province province);

	/**
	  * 查询省份所在的区域
	  */
	abstract List<Region> getRegionList();
	/**
	 * 根据邮政编码获取信息
	 * @param provinceid
	 * @return
	 */
	abstract Province getProvinceByProvinceid(String provinceid);

	abstract List<Province> getProvinceByName(String provinceName);
}
