/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProdUseful;

/**
 * 产品是否有用表，每个用户只能点击一次.
 */

public interface ProdUsefulDao extends Dao<ProdUseful, Long> {
     
	public abstract ProdUseful getProdUseful(Long id);
	
    public abstract int deleteProdUseful(ProdUseful prodUseful);
	
	public abstract Long saveProdUseful(ProdUseful prodUseful);
	
	public abstract int updateProdUseful(ProdUseful prodUseful);
	
	public Integer getUseful(String userId,Long prodComId);
 }
