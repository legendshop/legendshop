package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.config.dto.CommonPropertiesDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.spi.service.FloorLayoutFactoryService;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.util.AppUtils;
import org.springframework.util.CollectionUtils;


/**
 * 楼层工厂方法
 *
 */
@Service("floorLayoutFactory")
public class FloorLayoutFactoryImpl implements  FloorLayoutFactoryService{

	@Resource(name="floorLayoutExecutors")
	private Map<String, FloorLayoutStrategy> executors;

	@Autowired
	private FloorDao floorDao;

	@Autowired
	private CommonPropertiesDto propertiesDto;

	@Bean("floorLayoutExecutors")
	public Map<String, FloorLayoutStrategy> executors(
			@Qualifier(value="floorLayoutAdvRectangleFourStrategy") FloorLayoutAdvRectangleFourStrategyImpl  floorLayoutAdvRectangleFourStrategy,
			@Qualifier(value="floorLayoutAdvBrandStrategy") FloorLayoutAdvBrandStrategyImpl  floorLayoutAdvBrandStrategy,
			@Qualifier(value="floorLayoutGoodsStrategy") FloorLayoutGoodsStrategyImpl  floorLayoutGoodsStrategy,
			@Qualifier(value="floorLayoutAdvFiveStrategy") FloorLayoutAdvFiveStrategyImpl  floorLayoutAdvFiveStrategy,
			@Qualifier(value="floorLayoutAdvSquareFourStrategy") FloorLayoutAdvSquareFourStrategyImpl  floorLayoutAdvSquareFourStrategy,
			@Qualifier(value="floorLayoutAdvEightStrategy") FloorLayoutAdvEightStrategyImpl floorLayoutAdvEightStrategy,
			@Qualifier(value="floorLayoutAdvRowStrategy") FloorLayoutAdvRowStrategyImpl floorLayoutAdvRowStrategy,
			@Qualifier(value="floorLayoutBlendStrategy") FloorLayoutBlendStrategyImpl floorLayoutBlendStrategy

			){
		Map<String, FloorLayoutStrategy>  map = new LinkedHashMap<String, FloorLayoutStrategy>();
		map.put("wide_adv_rectangle_four", floorLayoutAdvRectangleFourStrategy);
		map.put("wide_adv_brand", floorLayoutAdvBrandStrategy);
		map.put("wide_goods", floorLayoutGoodsStrategy);
		map.put("wide_adv_five", floorLayoutAdvFiveStrategy);
		map.put("wide_adv_square_four", floorLayoutAdvSquareFourStrategy);
		map.put("wide_adv_eight", floorLayoutAdvEightStrategy);
		map.put("wide_adv_row", floorLayoutAdvRowStrategy);
		map.put("wide_blend", floorLayoutBlendStrategy);
		return map;
	}



	@Override
	public  void deleteFloor(Floor floor) {
		if(executors!=null){
			FloorLayoutStrategy strategy=executors.get(floor.getLayout());
			if(strategy!=null){
				strategy.deleteFloor(floor);
			}
			//清除缓存
			if(floor!=null && floor.getStatus().intValue()==1){
				floorDao.updateFloorCache(floor);
			}

		}
	}

	private FloorDto findFloorDto(Floor floor) {
		if(executors!=null){
			FloorLayoutStrategy strategy=executors.get(floor.getLayout());
			if(strategy!=null){
				return strategy.findFloorDto(floor);
			}
		}
		return null;
	}

	@Override
	public FloorDto findAdminFloorDto(Floor floor) {
		if(executors!=null){
			FloorLayoutStrategy strategy=executors.get(floor.getLayout());
			if(strategy!=null){
				return strategy.findFloorDto(floor);
			}
		}
		return null;
	}

	@Override
	public FloorItem findFloorAdv(Floor floor, String position) {
		List<FloorItem> advList =null;
    	FloorDto floorDto=findAdminFloorDto(floor);
    	if(AppUtils.isNotBlank(floorDto)){
    		Map<String, List<FloorItem>> floorItems =floorDto.getFloorItems();
    		if(AppUtils.isNotBlank(floorItems) && floorItems.size()>0){
    			advList=floorItems.get(position);
                if(AppUtils.isNotBlank(advList)){
                	return advList.get(0);
                }
    		}
    	}
    	return null;
	}

	@Override
	public List<FloorItem> findFloorAdvs(Floor floor, String position) {
		List<FloorItem> advList = null;
		FloorDto floorDto = findAdminFloorDto(floor);
		if (AppUtils.isNotBlank(floorDto)) {
			Map<String, List<FloorItem>> floorItems = floorDto.getFloorItems();
			if (AppUtils.isNotBlank(floorItems) && floorItems.size() > 0) {
				advList = floorItems.get(position);
			}
		}
		if (CollectionUtils.isEmpty(advList)) {
			return new ArrayList<>();
		}
		String pcDomainName = propertiesDto.getPcDomainName();
		for (FloorItem floorItem : advList) {
			// 判断配置的URL是否包含全路径域名
			if (StringUtils.isNotBlank(floorItem.getLinkUrl()) && (!floorItem.getLinkUrl().toLowerCase().contains("http") && !floorItem.getLinkUrl().toLowerCase().contains("www"))) {
				if (floorItem.getLinkUrl().substring(0,1).equals("/")) {
					floorItem.setLinkUrl(pcDomainName + floorItem.getLinkUrl());
				} else {
					floorItem.setLinkUrl(pcDomainName + "/" + floorItem.getLinkUrl());
				}
			}
		}
		return advList;
	}


	@Override
    @Cacheable(value = "FloorList")
	public List<FloorDto> loadFloor() {
		//1. 所有楼层
		List<Floor> floorList = floorDao.getActiveFloor();
		List<FloorDto> floorDtos=null;
		if(AppUtils.isNotBlank(floorList)){
			floorDtos=new ArrayList<FloorDto>();
			for (Iterator<Floor> iterator = floorList.iterator(); iterator.hasNext();) {
				Floor floor = iterator.next();
				FloorDto floorDto=findFloorDto(floor);
				if(floorDto!=null){
					floorDtos.add(floorDto);
				}
			}

		}

		return floorDtos;
	}
}
