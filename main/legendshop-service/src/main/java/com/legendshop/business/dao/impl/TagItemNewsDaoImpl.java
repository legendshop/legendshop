/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TagItemNewsDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.TagItemNews;

/**
 *标签里的文章Dao
 */
@Repository
public class TagItemNewsDaoImpl extends GenericDaoImpl<TagItemNews, Long> implements TagItemNewsDao  {
     
	public TagItemNews getTagItemNews(Long id){
		return getById(id);
	}
	
    public int deleteTagItemNews(TagItemNews tagItemNews){
    	return delete(tagItemNews);
    }
	
	public Long saveTagItemNews(TagItemNews tagItemNews){
		return save(tagItemNews);
	}
	
	public int updateTagItemNews(TagItemNews tagItemNews){
		return update(tagItemNews);
	}

	@Override
	public void saveItemList(List<TagItemNews> itemNewsList) {
		save(itemNewsList);
	}

	@Override
	public List<TagItemNews> getTagItemNewsByNewsId(Long newsId) {
		return queryByProperties(new EntityCriterion().eq("newsId", newsId));
	}

	@Override
	public int deleteTagItemNews(List<TagItemNews> itemNewsList) {
		return this.delete(itemNewsList);
	}
	
 }
