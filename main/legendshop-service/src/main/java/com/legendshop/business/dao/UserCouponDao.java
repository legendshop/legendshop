/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductCoupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.vo.UserCouponVo;

/**
 * 用户优惠券Dao.
 */

public interface UserCouponDao extends GenericDao<UserCoupon, Long> {
     
	public abstract UserCoupon getUserCoupon(Long id);
	
	public abstract UserCoupon getCouponByCouponPwd(String couponPwd);
	
	public abstract UserCoupon getCouponByCouponSn(String couponSn);
	
	public abstract int getUserCouponCount(String userId,long couponId);

	/**
	 * 根据couponId 获取优惠卷领取人数
	 * @param couponId
	 * @return
	 */
	public abstract int getUserCouponCount(Long couponId);

	public abstract int deleteUserCoupon(UserCoupon userCoupon);

	public abstract Long saveUserCoupon(UserCoupon userCoupon);

	public abstract int updateUserCoupon(UserCoupon userCoupon);

	public abstract boolean hasCoupon(Long id);

	void batchSave(List<UserCoupon> userCoupon);

	public abstract List<UserCoupon> getUserCouponByCoupon(Long couponId);

	public abstract List<String> getSendUsers(String sql, Object[] praObjects);

	public abstract List<UserCoupon> getOffExportExcelCoupon(Long couponId);
	
	public void betchUpdate(List<UserCoupon> userCoupon);
	
	/**
	 * 更新用户优惠券
	 */
	public abstract void updateUserCouponUseSts(Long couponId, String userName, Double actualTotal, String subNumber);

	public abstract List<UserCoupon> getAllActiveUserCoupons(String userId);

	public abstract List<ProductCoupon> getProdCouponList(String userId);

	public abstract void deleteByCouponId(Long couponId);

	public abstract List<UserCoupon> queryUnbindCoupon(Long couponId);

	public abstract List<UserDetail> findUserByUserMobile(List<String> result);

	public abstract Integer updateUserCoupon(List<UserCoupon> userCoupons);

	public abstract PageSupport<UserCoupon> getUserCouponListPage(String curPageNO,Integer pageSize, Long id);

	public abstract PageSupport<UserCoupon> queryUserCoupon(String sqlString, StringBuilder sqlCount,
			List<Object> objects, String curPageNO);

	public abstract PageSupport<UserCouponVo> getUserCoupon(String curPageNO, StringBuilder sql, StringBuilder sqlCount,
			List<Object> objects);

	public abstract List<String> getSendUsers(String curPageNO, StringBuilder sql, List<Object> objects);

	public abstract PageSupport<UserCoupon> getUserCouponList(String curPageNO, Long id);

	public abstract PageSupport<UserCoupon> getWatchRedPacket(Long id, String curPageNO);

	/**
	 * 根据couponSn查询用户优惠券
	 * @param couponSn
	 * @return
	 */
	public abstract UserCoupon getUserCouponBySn(String couponSn);

	/**
	 * 获取用户优惠券
	 * @param userName
	 * @param couponId
	 * @return
	 */
	public abstract List<UserCoupon> queryUserCouponList(String userName, Long couponId);

	/**
	 * 根据订单号查询用户优惠券
	 * 
	 */
	public abstract List<UserCoupon> queryByOrderNumber(String subNumber);

	/**
	 * 获取用户优惠券
	 * @param couponId
	 * @param userId
	 * @return
	 */
	public abstract List<UserCoupon> getUserCoupons(Long couponId, String userId);
	
	/**
	 *根据用户名和卡卷号来获取用户优惠劵 
	 * */
	
	public abstract List<UserCoupon> getUserCouponsByUser (String userName, Long couponId);
	
	
	/**
	 *根据订单号来获取用户优惠劵 
	 * */
	
	public abstract List<UserCoupon> getUserCouponsByOrderNumber(String orderNumber);
	/**
	 * 根据id来获取订单号
	 */
	public abstract String getOrderNumById(Long userCouponId);
 }
