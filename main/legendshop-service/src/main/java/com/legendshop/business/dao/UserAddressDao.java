/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserAddress;

/**
 * 用户订单地址Dao
 */
public interface UserAddressDao extends GenericDao<UserAddress, Long>{

	public abstract List<UserAddress> getUserAddress(String shopName);

	public abstract UserAddress getUserAddress(Long id);

	public abstract int deleteUserAddress(UserAddress userAddress);

	public abstract Long saveUserAddress(UserAddress userAddress);

	public abstract void updateUserAddress(UserAddress userAddress);

	public abstract Long getMaxNumber(String userName);
	
	public abstract void updateDefaultUserAddress(Long addrId,String userId);

	public UserAddress  getDefaultAddress(String userId);

	public abstract long getUserAddressCount(String userId);

	public abstract UserAddress getFirstAddress(String userId);

	PageSupport<UserAddress> UserAddressPage(String curPageNO, String userId);

	PageSupport<UserAddress> queryUserAddress(String curPageNO, String userId);

	public abstract PageSupport<UserAddress> getUserAddressPage(String curPageNO, UserAddress userAddress);

	public abstract PageSupport<UserAddress> queryPage(String curPageNO, String userName);

	public abstract void updateOtherDefault(Long addrId, String userId, String commonAddr);

	PageSupport<UserAddress> queryUserAddress(String userId, String curPageNO, int pageSize);
}
