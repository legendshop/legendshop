/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ArticleCommentDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ArticleComment;
import com.legendshop.util.AppUtils;

/**
 * The Class ArticleCommentDaoImpl.
  * Dao实现类
 */
@Repository
public class ArticleCommentDaoImpl extends GenericDaoImpl<ArticleComment, Long> implements ArticleCommentDao  {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
   	/**
	 *  根据Id获取
	 */
	public ArticleComment getArticleComment(Long id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteArticleComment(ArticleComment articleComment){
    	return delete(articleComment);
    }

   /**
	 *  保存
	 */		
	public Long saveArticleComment(ArticleComment articleComment){
		return save(articleComment);
	}

   /**
	 *  更新
	 */		
	public int updateArticleComment(ArticleComment articleComment){
		return update(articleComment);
	}

	/**
	 *  根据id查询
	 */	
	@Override
	public ArticleComment getArticleCommentLinkArt(Long id) {
		String sql="SELECT ac.id,a.id AS artId,a.name AS artName,ac.create_time AS createTime,ac.content," +
				"ud.user_id AS userId,ud.nick_name AS nickName FROM ls_article_comment ac INNER JOIN ls_article" +
				" a ON a.id = ac.art_id INNER JOIN ls_usr_detail ud ON ud.user_id = ac.user_id WHERE ac.id=?";
		return get(sql, ArticleComment.class, id);
	}

	@Override
	public PageSupport<ArticleComment> getArticleCommentPage(String curPageNO, ArticleComment articleComment) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(ArticleComment.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);

		if (AppUtils.isNotBlank(articleComment.getArtName())) {
			map.like("artName", articleComment.getArtName().trim());
		}
		if (AppUtils.isNotBlank(articleComment.getContent())) {
			map.like("content", articleComment.getContent().trim());
		}

		hql.setAllCountString(ConfigCode.getInstance().getCode("article.comment.queryArticleCommentsCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("article.comment.queryArticletComments", map));

		hql.setParam(map.toArray());

		return querySimplePage(hql);
	}
	
	@Override
	public PageSupport<ArticleComment> queyArticleComment(String curPageNO,Long id) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(ArticleComment.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);

		map.put("id", id);
		// 审核状态(1:审核通过)
		map.put("status", 1);

		hql.setAllCountString(ConfigCode.getInstance().getCode("article.comment.queryArticleCommentsCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("article.comment.queryArticleComments", map));

		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}
	
	@Override
	public PageSupport<ArticleComment> queyArticleCommentScroll(String curPageNO,Long id) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(ArticleComment.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);

		map.put("id", id);
		// 审核状态(1:审核通过)
		map.put("status", 1);

		hql.setAllCountString(ConfigCode.getInstance().getCode("article.comment.queryArticleCommentsCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("article.comment.queryArticleComments", map));

		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}
 }
