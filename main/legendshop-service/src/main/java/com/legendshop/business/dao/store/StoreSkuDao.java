/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.store;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.entity.store.StoreSku;

/**
 * The Class StoreSkuDao.
 */

public interface StoreSkuDao extends Dao<StoreSku, Long> {
     
	public abstract StoreSku getStoreSku(Long id);
	
    public abstract int deleteStoreSku(StoreSku storeSku);
	
	public abstract Long saveStoreSku(StoreSku storeSku);
	
	public abstract int updateStoreSku(StoreSku storeSku);

	public abstract List<Long> saveStoreSkuBatch(List<StoreSku> storeSkus);

	public abstract int updateStoreSkuBatch(List<StoreSku> originStoreSkus);
	
	public abstract List<StoreSku> getStoreSkuByIds(List<Long> ids);

	public abstract List<StoreSku> getStoreSku(Long storeId, Long prodId);

	public abstract int deleteStoreSku(Long storeId, Long prodId);

	public abstract Long getStoreSkuCount(Long storeId, Long storeId2);
	
	public abstract StoreSku getStoreSkuBySkuId(Long storeId, Long skuId);
 }
