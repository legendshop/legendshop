/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.timer;

/**
 * 清理旧数据的服务
 */
public interface HouseKeepService {

	/**
	 * 将长期不登录的商城用户下线.
	 */
	public void turnOffShop();

	/**
	 * 将过期的购物车变为过时.
	 */
	public void turnOffBasket();
}
