/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MsgTextDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.MsgText;

/**
 * The Class MsgTextDaoImpl.
 */
@Repository
public class MsgTextDaoImpl extends GenericDaoImpl<MsgText, Long> implements MsgTextDao {

}
