/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SubRefundReturnDetailDao;
import com.legendshop.model.entity.SubRefundReturnDetail;
import com.legendshop.spi.service.SubRefundReturnDetailService;
import com.legendshop.util.AppUtils;

/**
 *退款明细服务
 */
@Service("subRefundReturnDetailService")
public class SubRefundReturnDetailServiceImpl implements SubRefundReturnDetailService {

	/**
	 *
	 * 引用的Dao接口
	 */
	@Autowired
	private SubRefundReturnDetailDao subRefundReturnDetailDao;

	/**
	 * 根据Id获取
	 */
	public SubRefundReturnDetail getSubRefundReturnDetail(Long id) {
		return subRefundReturnDetailDao.getSubRefundReturnDetail(id);
	}

	/**
	 * 删除
	 */
	public void deleteSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		subRefundReturnDetailDao.deleteSubRefundReturnDetail(subRefundReturnDetail);
	}

	/**
	 * 保存
	 */
	public Long saveSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		if (!AppUtils.isBlank(subRefundReturnDetail.getId())) {
			updateSubRefundReturnDetail(subRefundReturnDetail);
			return subRefundReturnDetail.getId();
		}
		return subRefundReturnDetailDao.saveSubRefundReturnDetail(subRefundReturnDetail);
	}

	/**
	 * 更新
	 */
	public void updateSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail) {
		subRefundReturnDetailDao.updateSubRefundReturnDetail(subRefundReturnDetail);
	}

	@Override
	public List<SubRefundReturnDetail> getSubRefundReturnDetailByRefundId(Long refundId) {
		return subRefundReturnDetailDao.getSubRefundReturnDetailByRefundId(refundId);
	}
}
