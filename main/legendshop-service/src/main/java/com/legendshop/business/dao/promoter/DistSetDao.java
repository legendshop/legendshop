/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.promoter.model.DistSet;

/**
 * 分销设置Dao
 */
public interface DistSetDao extends Dao<DistSet, Long> {

	/** 获取分销设置 */
	public abstract List<DistSet> getDistSet(String shopName);

	/** 获取分销设置 */
	public abstract DistSet getDistSet(Long id);

	/** 删除分销设置 */
	public abstract int deleteDistSet(DistSet distSet);

	/** 保存分销设置 */
	public abstract Long saveDistSet(DistSet distSet);

	/** 更新分销设置 */
	public abstract int updateDistSet(DistSet distSet);

}
