/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 * 专题板块商品.
 */
public interface ThemeModuleProdDao extends Dao<ThemeModuleProd, Long> {
     
    public abstract List<ThemeModuleProd> getThemeModuleProdByModuleid(Long module_id);

	public abstract ThemeModuleProd getThemeModuleProd(Long id);
	
    public abstract int deleteThemeModuleProd(ThemeModuleProd themeModuleProd);
	
    public abstract int deleteThemeModuleProd(List<ThemeModuleProd> themeModuleProds);
    
	public abstract Long saveThemeModuleProd(ThemeModuleProd themeModuleProd);
	
	public abstract int updateThemeModuleProd(ThemeModuleProd themeModuleProd);
	
 }
