package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.javia.arity.SyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShippingActiveDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.business.dao.TransportDao;
import com.legendshop.util.ArithmeticEngine;
import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.model.constant.ShippingTypeEnum;
import com.legendshop.model.constant.SupportTransportFreeEnum;
import com.legendshop.model.constant.TransTypeEnum;
import com.legendshop.model.constant.TransportTypeEnum;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.buy.ActiveRuleContext;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.spi.service.TransportManagerService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * @author tony 配送方式计算中心
 * 思路： 1. 计算商品有运费模版的运费信息
 * ①、得出每个商家的每个商品的运费模版
 * ②、归类：归类出对应商家下对应运费模版下的配送方式的商品总数 如：shopA --> {transportA[按件数]= *{EMS:p1,p2; mail:p1,p2 } 、
 * transportB[按体积]= {EMS:p3,p4; * mail:p3,p4;express:p3,p4 } } 说明该商家有两套运费模版 ,并且商品p1,p2 有EMS、mail配送方式
 * ，p3,p4商品有EMS、mail、express配送方式 ③、进行运费计算： 规则：前提（同一商家下 ）
 * 同一个模版下的商品属于续件，不同模版下的配送方式是运费金额进行各自现金 (EMS:transportA[EMS 10元]+transportB[EMS 12元],
 * MAIL:transportA[mail 5元]+transportB[mail 12元])，则该商家的运费总计是22元;
 * <p>
 * 2. 计算有固定运费模版的运费信息：固定运费: 平邮10元,快递20 元,EMS30元
 * <p>
 * 3. 统计 上述 步骤1 与步骤2 的运费！
 * <p>
 * 4、包邮活动
 */
@Service("transportManagerService")
public class TransportManagerServiceImpl implements TransportManagerService {
	
	private final static Logger logger = LoggerFactory.getLogger(TransportManagerServiceImpl.class);

	@Autowired
    private TransportDao transportDao;

	@Autowired
    private ShopDetailDao shopDetailDao;

	@Autowired
    private ShippingActiveDao shippingActiveDao;

    /**
     * 计算运费模板
     */
    @Override
    public void clacCartDeleivery(UserShopCartList userShopCartList, boolean isCouponBack) {
        Integer userCityId = userShopCartList.getCityId();
        if (AppUtils.isBlank(userCityId)) {
            return;
        }
        List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
        if (AppUtils.isBlank(shopCarts)) {
            return;
        }


        for (int k = 0; k < shopCarts.size(); k++) {

            ShopCarts carts = shopCarts.get(k);

            deleiveryActivity(carts.getShopId(), carts.getCartItems(), carts);
        }


        /**
         * 1.计算商品有运费模版的运费信息
         */
        Map<Long, List<TransfeeDto>> transportTransfeeMap = assembleShopTransport(shopCarts, userCityId, isCouponBack);

        /**
         * 2.计算有固定运费模版的运费信息
         */
        Map<Long, List<TransfeeDto>> fixedTransfeeMap = assembleShopFixedTransport(shopCarts, isCouponBack);


        /**
         * 3. 统计订单的运费
         */
        for (int i = 0; i < shopCarts.size(); i++) {
            ShopCarts carts = shopCarts.get(i);
            if (carts == null)
                continue;
            statisticalFreight(carts, transportTransfeeMap, fixedTransfeeMap);
        }

        transportTransfeeMap = null;
        fixedTransfeeMap = null;

        //重新计算商品价格，加上运费
//        Double orderActualTotal= Arith.add(userShopCartList.getOrderActualTotal(), userShopCartList.getOrderCurrentFreightAmount());
        Double orderActualTotal = Arith.sub(Arith.add(userShopCartList.getOrderActualTotal(), userShopCartList.getOrderCurrentFreightAmount()), userShopCartList.getOrderFreightAmount());
        userShopCartList.setOrderFreightAmount(userShopCartList.getOrderCurrentFreightAmount());
        userShopCartList.setOrderActualTotal(orderActualTotal);
    }


    private void deleiveryActivity(Long shopId, List<ShopCartItem> cartItems, ShopCarts carts) {

        //查询该商家所有的进行中包邮活动
        List<ShippingActive> activeList = shippingActiveDao.getShopRuleEngineShippingActive(shopId);
        if (AppUtils.isNotBlank(activeList)) {
            ActiveRuleContext context = new ActiveRuleContext(); //应用规则上下文,用于做排除
            //三种情况
            int shopCount = 0;
            int prodCount = 0;
            List<ShippingActive> prodActives = new ArrayList<ShippingActive>();
            List<ShippingActive> shopActives = new ArrayList<ShippingActive>();
            for (ShippingActive active : activeList) {
                if (active.getFullType() == ShippingTypeEnum.SHOP.value()) {
                    shopCount++;
                    shopActives.add(active);
                } else if (active.getFullType() == ShippingTypeEnum.PROD.value()) {
                    prodCount++;
                    prodActives.add(active);
                }
            }
            String mailfeeStr = null;
            ShippingActive shopActive = null;
            //1.店铺及商品包邮混合
            if (shopCount > 0 && prodCount > 0) {
                //( 先处理符合商品包邮的{若满足商品包邮的商品排除在外}  再处理店铺的
                for (ShippingActive prodActive : prodActives) {
                    executeProdShipping(context, prodActive, cartItems);
                }
                //同时间段仅会存在同个商品包邮活动
                shopActive = shopActives.get(0);
                executeShopShipping(context, shopActive, cartItems);
                if (shopActive.getReduceType() == ShippingTypeEnum.FULL_MONEY.value()) {//满金额包邮
                    mailfeeStr = shopActive.getName() + ",购物满" + Arith.round(shopActive.getFullValue(), 2) + "元包邮";
                } else if (shopActive.getReduceType() == ShippingTypeEnum.FULL_PIECE.value()) {
                    mailfeeStr = shopActive.getName() + ",购物满" + shopActive.getFullValue().intValue() + "件包邮";
                }

            } else if (prodCount == activeList.size()) { //2.仅指定商品包邮活动
                for (ShippingActive prodActive : activeList) {
                    //把业务规则应用到购物车商品中
                    executeProdShipping(context, prodActive, cartItems);
                }

            } else { //3. 仅店铺包邮活动
                //同时间段仅会存在同个商品包邮活动
                shopActive = shopActives.get(0);
                executeShopShipping(context, shopActive, cartItems);
                if (shopActive.getReduceType() == ShippingTypeEnum.FULL_MONEY.value()) {//满金额包邮
                    mailfeeStr = shopActive.getName() + ",购物满" + Arith.round(shopActive.getFullValue(), 2) + "元包邮";
                } else if (shopActive.getReduceType() == ShippingTypeEnum.FULL_PIECE.value()) {
                    mailfeeStr = shopActive.getName() + ",购物满" + shopActive.getFullValue().intValue() + "件包邮";
                }
            }
            carts.setMailfeeStr(mailfeeStr);
        }

        int isFreePostage = 0;// 是否免运费

        for (int i = 0; i < carts.getCartItems().size(); i++) {
            if (carts.getCartItems().get(i).getShopId().equals(shopId)) {
                ShopCartItem item = carts.getCartItems().get(i);
                if (!SupportTransportFreeEnum.SELLER_SUPPORT.value().equals(item.getTransportFree())) //商家承担运费等于免运费
                    isFreePostage++;
            }
        }

        boolean freePostage = isFreePostage == 0;
        carts.setFreePostage(freePostage); //用于该商家下所有的商品是否承担运费
        carts.setFreePostageByShop(freePostage);  //记录是商家设置了商品包邮。

    }

    /**
     * 计算商品包邮
     * context： 上下文
     * active： 包邮活动
     * cartItems: 商城下的商品集合
     */
    private void executeProdShipping(ActiveRuleContext context, ShippingActive prodActive, List<ShopCartItem> cartItems) {

        Double hitProdTotalPrice = 0d;//命中的商品总金额
        int hitProdCount = 0;//命中的商品总数
        List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();
        Long[] prodIds = prodActive.getProdIds();

        //判断是否执行过 满减、满折、限时折扣
        for (ShopCartItem cartItem_V2 : cartItems) {
            //参与其他活动
            if (context.isShippingExecuted(cartItem_V2.getProdId(), cartItem_V2.getSkuId())) {
                continue;
            }
            if (contains(prodIds, cartItem_V2.getProdId()) && cartItem_V2.getCheckSts() == 1) { //有商品命中活动
                hitProdTotalPrice = Arith.add(hitProdTotalPrice, cartItem_V2.getCalculatedCouponAmount()); //取计算营销活动及红包，优惠券之后的真实金额
                hitProdCount += cartItem_V2.getBasketCount();

                String shippingStr = null;
                if (prodActive.getReduceType() == ShippingTypeEnum.FULL_MONEY.value()) {//满金额包邮
                    shippingStr = prodActive.getName() + "活动,购物满" + Arith.round(prodActive.getFullValue(), 2) + "元包邮";
                } else if (prodActive.getReduceType() == ShippingTypeEnum.FULL_PIECE.value()) {
                    shippingStr = prodActive.getName() + "活动,购物满" + prodActive.getFullValue().intValue() + "件包邮";
                }
                cartItem_V2.setActivIntro(prodActive.getName());
                cartItem_V2.setShippingStr(shippingStr);
                hitCartItems.add(cartItem_V2);
                cartItem_V2.setActiv(true);
                context.executeShipping(cartItem_V2.getProdId(), cartItem_V2.getSkuId());//排除这个商品
            }
        }

        if (hitProdTotalPrice > 0 && hitProdCount > 0) { //有选中的商品且满足条件
            if ((ShippingTypeEnum.FULL_MONEY.value().equals(prodActive.getReduceType()) && hitProdTotalPrice >= prodActive.getFullValue()) || //满金额
                    (ShippingTypeEnum.FULL_PIECE.value().equals(prodActive.getReduceType()) && hitProdCount >= prodActive.getFullValue())) { //满件

                for (ShopCartItem cartItem_V2 : hitCartItems) {
                    cartItem_V2.setTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
                }

            } else {

                for (ShopCartItem cartItem_V2 : hitCartItems) {
                    cartItem_V2.setTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
                }

            }

        }

    }

    /**
     * 计算店铺包邮，要优先计算商品包邮
     * context： 上下文
     * active： 包邮活动
     * cartItems: 商城下的商品集合
     */
    private void executeShopShipping(ActiveRuleContext context, ShippingActive active, List<ShopCartItem> cartItems) {
        if (!context.isShopShippingExecuted()) { //是否已经参加过店铺的包邮活动

            Double hitProdTotalPrice = 0d;//命中的商品总金额
            int hitProdCount = 0;//命中的商品总数
            List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();

            //判断是否执行过 满包邮
            for (ShopCartItem cartItem_V2 : cartItems) {
                // 是否参加过针对于商品的优惠信息
                if (context.isShippingExecuted(cartItem_V2.getProdId(), cartItem_V2.getSkuId())||(cartItem_V2.getTransportFree()==1)&&!cartItem_V2.isActiv()) {
                    continue;
                }
                if (cartItem_V2.getCheckSts() == 1) {//选中状态才计算
                    hitProdTotalPrice = Arith.add(hitProdTotalPrice, cartItem_V2.getCalculatedCouponAmount());
                    hitProdCount += cartItem_V2.getBasketCount();
                }
                cartItem_V2.setActiv(true);
                hitCartItems.add(cartItem_V2);
                
            }


            if (hitProdTotalPrice >= 0 && hitProdCount > 0) { //有选中的商品且满足条件
                if ((ShippingTypeEnum.FULL_MONEY.value().equals(active.getReduceType()) && hitProdTotalPrice >= active.getFullValue()) || //满金额
                        (ShippingTypeEnum.FULL_PIECE.value().equals(active.getReduceType()) && hitProdCount >= active.getFullValue())) { //满件
                    for (ShopCartItem cartItem_V2 : hitCartItems) {
                        cartItem_V2.setTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
                    }

                    context.executeShopShipping();//设置参与了全店的包邮
                } else {
                    for (ShopCartItem cartItem_V2 : hitCartItems) {
                        cartItem_V2.setTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
                    }

                    context.executeShopShipping();//设置参与了全店的包邮

                }

            }
        }
    }

    /**
     * 对比商品ID是否在包邮活动里
     *
     * @param arr
     * @param targetValue
     * @return
     */
    private boolean contains(Long[] arr, Long targetValue) {
        for (Long s : arr) {
            if (s.equals(targetValue))
                return true;
        }
        return false;
    }

    /**
     * 统计运费
     * <p>
     * 商家ＩＤ
     *
     * @param transportTransfeeMap 　运费模版的运费
     * @param fixedTransfeeMap     　固定模版的运费
     * @return
     */
    private void statisticalFreight(ShopCarts carts, Map<Long, List<TransfeeDto>> transportTransfeeMap, Map<Long, List<TransfeeDto>> fixedTransfeeMap) {
        Long shopId = carts.getShopId();
		if(AppUtils.isBlank(shopId)) {
			logger.warn("shopId is null, it can not load shopDetail");
		}
        ShopDetail shopDetail = shopDetailDao.getShopDetailByShopId(shopId);
        Integer mailfeeSts = shopDetail.getMailfeeSts();// 包邮状态
        Integer mailfeeType = shopDetail.getMailfeeType();// 包邮类型
        Double mailfeeCon = shopDetail.getMailfeeCon();// 包邮条件的数量

        boolean isMailFee = false;// 是否满足包邮条件
        if (mailfeeSts != null && mailfeeSts.intValue() == 1) {
            if (mailfeeType.intValue() == 1) {// 满金额包邮
                double ShopActualTotalCash = 0d;
                for (ShopCartItem shopCarItem : carts.getCartItems()) {
                    ShopActualTotalCash += Arith.sub(Arith.sub(shopCarItem.getDiscountedPrice(), shopCarItem.getCouponOffPrice()), shopCarItem.getRedpackOffPrice());
                }
                if (ShopActualTotalCash >= mailfeeCon) {
                    isMailFee = true;
                    carts.setFreePostageByFullAmount(true);
                }
            } else if (mailfeeType.intValue() == 2) {// 满件包邮
                if (carts.getTotalcount() >= mailfeeCon) {
                    isMailFee = true;
                    carts.setFreePostageByFullPiece(true);
                }
            }
        }

        if (isMailFee) {
            carts.setFreePostage(true);
        } else {// 不满足包邮，计算运费
            List<TransfeeDto> transportTransDtos = transportTransfeeMap == null ? new ArrayList<TransfeeDto>() : transportTransfeeMap.get(shopId);
            List<TransfeeDto> fixedTransfeeDtos = fixedTransfeeMap == null ? new ArrayList<TransfeeDto>() : fixedTransfeeMap.get(shopId);

            List<TransfeeDto> orderTransfee = intersectionOrderTransfee(transportTransDtos, fixedTransfeeDtos);
            if (AppUtils.isNotBlank(orderTransfee)) {
                carts.setTransfeeDtos(orderTransfee);
                carts.setCurrentSelectTransfee(orderTransfee.get(0));
                carts.setFreightAmount(orderTransfee.get(0).getDeliveryAmount());
                double shopActualTotalCash = Arith.add(carts.getShopActualTotalCash(), orderTransfee.get(0).getDeliveryAmount());
                carts.setShopActualTotalCash(shopActualTotalCash);
                carts.setFreePostage(false);
                carts.setFreePostageByFullPiece(false);
                carts.setFreePostageByFullAmount(false);
            } else {
                //如果找不到运费模板，默认包邮
                carts.setFreePostage(true);
                carts.setFreightAmount(0d);
            }
        }
    }

    private List<TransfeeDto> intersectionOrderTransfee(List<TransfeeDto> transportTransDtos, List<TransfeeDto> fixedTransfeeDtos) {
        List<TransfeeDto> orderTransfeeDtos = new ArrayList<TransfeeDto>();
        TransfeeDto mail = null;
        TransfeeDto ems = null;
        TransfeeDto express = null;
        if (AppUtils.isNotBlank(transportTransDtos)) {
            for (TransfeeDto dto : transportTransDtos) {
                if (dto == null)
                    continue;
                if (FreightModeEnum.TRANSPORT_MAIL.value().equals(dto.getFreightMode())) {
                    mail = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_MAIL.value());
                } else if (FreightModeEnum.TRANSPORT_EXPRESS.value().equals(dto.getFreightMode())) {
                    express = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_EXPRESS.value());
                } else if (FreightModeEnum.TRANSPORT_EMS.value().equals(dto.getFreightMode())) {
                    ems = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_EMS.value());
                }
            }
        }

        if (AppUtils.isNotBlank(fixedTransfeeDtos)) {
            for (TransfeeDto dto : fixedTransfeeDtos) { // 固定运费
                if (dto == null)
                    continue;
                if (FreightModeEnum.TRANSPORT_MAIL.value().equals(dto.getFreightMode())) {
                    if (mail != null) {
                        double delivery = Arith.add(mail.getDeliveryAmount(), dto.getDeliveryAmount());
                        mail.setDeliveryAmount(delivery);
                    } else {
                        mail = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_MAIL.value());
                    }
                } else if (FreightModeEnum.TRANSPORT_EXPRESS.value().equals(dto.getFreightMode())) {
                    if (express != null) {
                        double delivery = Arith.add(express.getDeliveryAmount(), dto.getDeliveryAmount());
                        express.setDeliveryAmount(delivery);
                    } else {
                        express = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_EXPRESS.value());
                    }
                } else if (FreightModeEnum.TRANSPORT_EMS.value().equals(dto.getFreightMode())) {
                    if (ems != null) {
                        double delivery = Arith.add(ems.getDeliveryAmount(), dto.getDeliveryAmount());
                        ems.setDeliveryAmount(delivery);
                    } else {
                        ems = new TransfeeDto(dto.getDeliveryAmount(), FreightModeEnum.TRANSPORT_EMS.value());
                    }
                }
            }
        }
        if (ems != null) {
            orderTransfeeDtos.add(ems);
        }
        if (express != null) {
            orderTransfeeDtos.add(express);
        }
        if (mail != null) {
            orderTransfeeDtos.add(mail);
        }

        return orderTransfeeDtos;
    }

    private Map<Long, List<TransfeeDto>> assembleShopTransport(List<ShopCarts> shopCarts, Integer userCityId, boolean isCouponBack) {
        /**
         * {shopId={transportId={配送方式=[购物商品信息]},transportId2={配送方式=[购物商品信息]}},
         * shopId2={transportId={配送方式=[购物商品信息]}}}
         * {53={57={express=[Basket1,Basket2]}}, 45={59={mail=[Basket3,Basket4],
         * express=[Basket3,Basket4]}}}
         */
        Map<Long, Map<Long, Map<String, List<ShopCartItem>>>> shopMap = new HashMap<Long, Map<Long, Map<String, List<ShopCartItem>>>>();

        /** 区分商品所属的运费模版 每个商家的运费模版肯定是不一样的 **/
        for (int i = 0; i < shopCarts.size(); i++) {
            if (isCouponBack) {
                if (shopCarts.get(i).isFreePostageByFullPiece() || shopCarts.get(i).isFreePostageByShop()) {
                    continue;
                }
            } else {
                if (shopCarts.get(i).isFreePostage()) {
                    continue;
                }
            }

            Map<Long, Map<String, List<ShopCartItem>>> transMap = new HashMap<Long, Map<String, List<ShopCartItem>>>();
            List<ShopCartItem> baskets = shopCarts.get(i).getCartItems();
            for (int j = 0; j < baskets.size(); j++) {
                ShopCartItem basket = baskets.get(j);
                if (!isUseTransport(basket)) {// 不需要承担运费则跳过
                    continue;
                }
                // 查询商品是否参加了 包邮活动,并检查是否满足使用条件
                /*List<CartMarketRules> rules = basket.getCartMarketRules();
                if (AppUtils.isNotBlank(rules)) {
                    boolean isFullMail = false;
                    for (CartMarketRules rule : rules) {
                        // 满件包邮
                        if (MarketingTypeEnum.FULL_NUM_MAIL.value().equals(rule.getMarketType())) {
                            // 判断商品数量是否符合满件包邮
                            if (basket.getBasketCount() >= rule.getTrigger()) {
                                isFullMail = true;
                            }
                        } else if (MarketingTypeEnum.FULL_AMOUNT_MAIL.value().equals(rule.getMarketType())) {
                            Double price = Arith.mul(basket.getBasketCount(), basket.getCalculatedCouponAmount());
                            // 判断商品金额是否符合满金额包邮
                            if (price >= rule.getTrigger()) {
                                isFullMail = true;
                            }
                        }
                    }
                    if (isFullMail) {
                        basket.setTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
                        continue;
                    }
                }*/

                Long transportId = basket.getTransportId();
                // 运费模板
                Transport transport = basket.getTransport();
                if (transportId == null || transport == null) {
                    continue;
                }
                Map<String, List<ShopCartItem>> basketMap = transMap.get(transportId);
                if (AppUtils.isBlank(basketMap)) { // 用于初始化transMap
                    Map<String, List<ShopCartItem>> map2 = new HashMap<String, List<ShopCartItem>>();
                    if (transport.isTransEms()) { // EMS快递方式
                        List<ShopCartItem> list = new ArrayList<ShopCartItem>();
                        list.add(basket);
                        map2.put(FreightModeEnum.TRANSPORT_EMS.value(), list);
                    }
                    if (transport.isTransExpress()) {
                        List<ShopCartItem> list = new ArrayList<ShopCartItem>();
                        list.add(basket);
                        map2.put(FreightModeEnum.TRANSPORT_EXPRESS.value(), list);
                    }
                    if (transport.isTransMail()) {
                        List<ShopCartItem> list = new ArrayList<ShopCartItem>();
                        list.add(basket);
                        map2.put(FreightModeEnum.TRANSPORT_MAIL.value(), list);
                    }
                    transMap.put(transportId, map2);
                } else {
                    if (transport.isTransEms()) { // EMS快递方式
                        List<ShopCartItem> list = basketMap.get(FreightModeEnum.TRANSPORT_EMS.value());
                        if (AppUtils.isNotBlank(list)) {
                            list.add(basket);
                        } else {
                            list = new ArrayList<ShopCartItem>();
                            list.add(basket);
                            basketMap.put(FreightModeEnum.TRANSPORT_EMS.value(), list);
                        }
                    }
                    if (transport.isTransExpress()) {
                        List<ShopCartItem> list = basketMap.get(FreightModeEnum.TRANSPORT_EXPRESS.value());
                        if (AppUtils.isNotBlank(list)) {
                            list.add(basket);
                        } else {
                            list = new ArrayList<ShopCartItem>();
                            list.add(basket);
                            basketMap.put(FreightModeEnum.TRANSPORT_EXPRESS.value(), list);
                        }
                    }
                    if (transport.isTransMail()) {
                        List<ShopCartItem> list = basketMap.get(FreightModeEnum.TRANSPORT_MAIL.value());
                        if (AppUtils.isNotBlank(list)) {
                            list.add(basket);
                        } else {
                            list = new ArrayList<ShopCartItem>();
                            list.add(basket);
                            basketMap.put(FreightModeEnum.TRANSPORT_MAIL.value(), list);
                        }
                    }
                }
            }
            shopMap.put(shopCarts.get(i).getShopId(), transMap);
        }

        /*
         * for (Map.Entry<Long, Map<Long, Map<String, List<Basket>>>> entry :
         * shopMap.entrySet()) { // shop Long shopId = entry.getKey(); Map<Long,
         * Map<String, List<Basket>>> map=entry.getValue();
         * if(AppUtils.isNotBlank(map)){ if(map.size()>2){ //商家拥有多个运费模版
         * 判断模版直接是否拥有交集 for (Map.Entry<Long, Map<String, List<Basket>>> entry2 :
         * entry.getValue().entrySet()) { // transport 运费模版1
         *
         * }
         *
         * }else{
         *
         * } } }
         */
        Map<Long, List<TransfeeDto>> map = assembleShopTransportTransfee(shopMap, userCityId); // 处理商家的运费模版的运费规则;
        return map;
    }

    /**
     * 用于处理商品拥有运费模版
     *
     * @param shopMap
     */
    private Map<Long, List<TransfeeDto>> assembleShopTransportTransfee(
            Map<Long, Map<Long, Map<String, List<ShopCartItem>>>> shopMap, Integer userCityId) {
        // 组装配送方式
        Map<Long, List<TransfeeDto>> transportTransfeeMap = new HashMap<Long, List<TransfeeDto>>();

        /**
         * 对shopMap进行解析取值 {shopId={transportId={配送方式=[购物商品信息]}}}
         * {53={57={express=[Basket1,Basket2]}}, 45={59={mail=[Basket3,Basket4],
         * express=[Basket3,Basket4]}}}
         */
        for (Map.Entry<Long, Map<Long, Map<String, List<ShopCartItem>>>> entry : shopMap.entrySet()) { // shop
            Long shopId = entry.getKey();
            Map<String, TransfeeDto> transfeeDtoMap = new HashMap<String, TransfeeDto>(); // 每个商家的配送模版
            for (Map.Entry<Long, Map<String, List<ShopCartItem>>> entry2 : entry.getValue().entrySet()) { // transport
                // 运费模版1
                Map<String, List<ShopCartItem>> transportMap = entry2.getValue();
                Long transportId = entry2.getKey();
                Transport transport = transportDao.getDetailedTransport(shopId, transportId); // 运费模版
                if (AppUtils.isBlank(transportMap) || transport == null) {// 没有运费模版
                    continue;
                } else {
                    for (Map.Entry<String, List<ShopCartItem>> entry3 : transportMap.entrySet()) { // 运费类型    快递  EMS
                        boolean config = false;//是否为支持限售的，支持限售只能在该城市销售
                        String trans_type = entry3.getKey();
                        List<ShopCartItem> list = entry3.getValue(); // 商品信息
                        Transfee transfee = null;
                        TransfeeDto transfeeDto = transfeeDtoMap.get(trans_type);
                        if (FreightModeEnum.TRANSPORT_MAIL.value().equals(trans_type)) {
                            if (transport.getIsRegionalSales() == 1) {
                                for (Transfee transfee2 : transport.getMailList()) {
                                    if (transfee2.getCityIdList().contains(userCityId.longValue())) {
                                        transfee = getTransfeeDto(transport, userCityId, FreightModeEnum.TRANSPORT_MAIL.value());
                                        config = true;
                                        break;
                                    }
                                }
                            } else {
                                transfee = getTransfeeDto(transport, userCityId, trans_type);
                            }
                        } else if (FreightModeEnum.TRANSPORT_EXPRESS.value().equals(trans_type)) {
                            if (transport.getIsRegionalSales() == 1) {
                                for (Transfee transfee2 : transport.getExpressList()) {
                                    if (transfee2.getCityIdList().contains(userCityId.longValue())) {
                                        transfee = getTransfeeDto(transport, userCityId, FreightModeEnum.TRANSPORT_EXPRESS.value());
                                        config = true;
                                        break;
                                    }
                                }
                            } else {
                                transfee = getTransfeeDto(transport, userCityId, trans_type);
                            }
                        } else if (FreightModeEnum.TRANSPORT_EMS.value().equals(trans_type)) {
                            if (transport.getIsRegionalSales() == 1) {
                                for (Transfee transfee2 : transport.getEmsList()) {
                                    if (transfee2.getCityIdList().contains(userCityId.longValue())) {
                                        transfee = getTransfeeDto(transport, userCityId, FreightModeEnum.TRANSPORT_EMS.value());
                                        config = true;
                                        break;
                                    }
                                }
                            } else {
                                transfee = getTransfeeDto(transport, userCityId, trans_type);
                            }
                        }
                        double totalCount = 0d;
                        double totalWeight = 0d;
                        double totalvolume = 0d;
                        // 同属于 商家的运费模版下 的运送方式的 所有商品进行计算汇总;
                        for (int j = 0; j < list.size(); j++) {
                            ShopCartItem basket = list.get(j);
                            double basketCount = basket.getBasketCount() == null ? 1d : basket.getBasketCount();
                            totalCount = Arith.add(totalCount, basketCount);
                            totalWeight = Arith.add(totalWeight,
                                    Arith.mul(basket.getWeight() == null ? 0d : basket.getWeight(), basketCount));
                            totalvolume = Arith.add(totalvolume,
                                    Arith.mul(basket.getVolume() == null ? 0d : basket.getVolume(), basketCount));
                        }
                        double freeAmount = clacCartDeleivery(transport.getTransType(), transfee, totalWeight,
                                totalvolume, totalCount); // 该配送方式的总价格

                        if (transport.getIsRegionalSales() == 0 || (transport.getIsRegionalSales() == 1 && config)) {
                            if (transfeeDto != null) {
                                double deliveryAmount = transfeeDto.getDeliveryAmount();
                                double amout = Arith.add(deliveryAmount, freeAmount);
                                transfeeDto.setDeliveryAmount(amout);

                            } else {
                                transfeeDto = new TransfeeDto();
                                transfeeDto.setDeliveryAmount(freeAmount);
                                transfeeDto.setFreightMode(trans_type);
                                transfeeDtoMap.put(trans_type, transfeeDto);
                            }
                        }
                    }
                }
            }

            // 组装配送方式
            List<TransfeeDto> transfeeDtos = new ArrayList<TransfeeDto>();
            for (Map.Entry<String, TransfeeDto> trentry : transfeeDtoMap.entrySet()) {
                String key = trentry.getKey();
                TransfeeDto transfeeDto = trentry.getValue();
                if (AppUtils.isNotBlank(key) && AppUtils.isNotBlank(transfeeDto)) {
                    transfeeDtos.add(transfeeDto);
                }
            }
            transportTransfeeMap.put(shopId, transfeeDtos);
            transfeeDtoMap = null;
            /*
             * ShopCarts shopCart=userShopCartList.getShopCarts(shopId);
             * shopCart.setTransfeeDtos(transfeeDtos);
             * if(AppUtils.isNotBlank(transfeeDtos)){
             * shopCart.setCurrentSelectTransfee(transfeeDtos.get(0)); }
             */
        }
        return transportTransfeeMap;
    }

    private Map<Long, List<TransfeeDto>> assembleShopFixedTransport(List<ShopCarts> shopCarts, boolean isCouponBack) {
        // 组装配送方式
        Map<Long, List<TransfeeDto>> transportTransfeeMap = new HashMap<Long, List<TransfeeDto>>();
        int size = shopCarts.size();
        for (int i = 0; i < size; i++) {
            if (isCouponBack) {
                if (shopCarts.get(i).isFreePostageByFullPiece() || shopCarts.get(i).isFreePostageByShop()) {
                    continue;
                }
            } else {
                if (shopCarts.get(i).isFreePostage()) {
                    continue;
                }
            }
            List<ShopCartItem> cartItems = shopCarts.get(i).getCartItems();
            double totalEms = 0d;
            double totalExpress = 0d;
            double totalMail = 0d;
            double totalNumber = 0;
            Long tempProdId = null;
            for (ShopCartItem basket : cartItems) {
                if (basket == null) {
                    continue;
                }
                if (basket.getProdId().equals(tempProdId)) {
                    continue;
                }

                tempProdId = basket.getProdId();

                // 查询商品是否参加了 包邮活动,并检查是否满足使用条件
                /*List<CartMarketRules> rules = basket.getCartMarketRules();
                if (AppUtils.isNotBlank(rules)) {
                    boolean isFullMail = false;
                    for (CartMarketRules rule : rules) {
                        // 满件包邮
                        if (MarketingTypeEnum.FULL_NUM_MAIL.value().equals(rule.getMarketType())) {
                            // 判断商品数量是否符合满件包邮
                            if (basket.getBasketCount() >= rule.getTrigger()) {
                                isFullMail = true;
                            }

                        } else if (MarketingTypeEnum.FULL_AMOUNT_MAIL.value().equals(rule.getMarketType())) {
                            Double price = Arith.mul(basket.getBasketCount(), basket.getCalculatedCouponAmount());
                            // 判断商品金额是否符合满金额包邮
                            if (price >= rule.getTrigger()) {
                                isFullMail = true;
                            }
                        }
                    }
                    if (isFullMail) {
                        basket.setTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
                        continue;
                    }
                }*/

                if (SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(basket.getTransportFree())) {
                    if (TransportTypeEnum.FIXED_TEMPLE.value().equals(basket.getTransportType())) {
						/*totalNumber = Arith.add(basket.getBasketCount(), totalNumber);
						totalEms =Arith.round(Arith.add(Arith.mul(basket.getEmsTransFee(),basket.getBasketCount()), totalEms),2) ;
						totalExpress = Arith.round(Arith.add(Arith.mul(basket.getExpressTransFee(),basket.getBasketCount()), totalExpress),2);
						totalMail = Arith.round(Arith.add(Arith.mul(basket.getMailTransFee(),basket.getBasketCount()), totalMail),2);*/
                        totalNumber = Arith.add(basket.getBasketCount(), totalNumber);
                        totalEms = Arith.round(Arith.add(basket.getEmsTransFee(), totalEms), 2);
                        totalExpress = Arith.round(Arith.add(basket.getExpressTransFee(), totalExpress), 2);
                        totalMail = Arith.round(Arith.add(basket.getMailTransFee(), totalMail), 2);
                    }
                }
            }

            List<TransfeeDto> list = new ArrayList<TransfeeDto>();

            if (totalEms != 0d) {
                list.add(new TransfeeDto(totalEms, FreightModeEnum.TRANSPORT_EMS.value()));
            }
            if (totalExpress != 0) {
                list.add(new TransfeeDto(totalExpress, FreightModeEnum.TRANSPORT_EXPRESS.value()));
            }
            if (totalMail != 0) {
                list.add(new TransfeeDto(totalMail, FreightModeEnum.TRANSPORT_MAIL.value()));
            }
            transportTransfeeMap.put(shopCarts.get(i).getShopId(), list);
        }
        return transportTransfeeMap;
    }

    /**
     * 是否需要买家承担运费
     *
     * @param basket
     * @return
     */
    private boolean isUseTransport(ShopCartItem basket) {
        if (AppUtils.isBlank(basket)) {
            return false;
        }
        if (SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(basket.getTransportFree())) {
            if (TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(basket.getTransportType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 计算运费
     */
    @Override
    public List<TransfeeDto> getTransfeeDtos(Long shopId, Long transportId, Integer cityId, double totalCount,
                                             double totalWeight, double totalvolume) {
        // 加载运费模板
        List<TransfeeDto> transfeeDtos = new ArrayList<TransfeeDto>();

        /*
         * 获取运费模版
         */
        Transport transport = transportDao.getDetailedTransport(shopId, transportId);
        if (AppUtils.isBlank(transport)) {
            return null;
        }

        if (transport.isTransEms()) { // EMS快递方式
            Transfee defaultransfee = getTransfeeDto(transport, cityId, FreightModeEnum.TRANSPORT_EMS.value());
            TransfeeDto transfeeDto = new TransfeeDto();
            if (defaultransfee != null) {
                double freeAmount = clacCartDeleivery(transport.getTransType(), defaultransfee, totalWeight,
                        totalvolume, totalCount); // 该配送方式的总价格
                transfeeDto.setDeliveryAmount(freeAmount);
                transfeeDto.setDesc("EMS");
                transfeeDto.setFreightMode(FreightModeEnum.TRANSPORT_EMS.value());
                transfeeDtos.add(transfeeDto);
            }

        }

        if (transport.isTransExpress()) { // express快递方式
            Transfee defaultransfee = getTransfeeDto(transport, cityId, FreightModeEnum.TRANSPORT_EXPRESS.value());
            TransfeeDto transfeeDto = new TransfeeDto();
            if (defaultransfee != null) {
                double freeAmount = clacCartDeleivery(transport.getTransType(), defaultransfee, totalWeight,
                        totalvolume, totalCount); // 该配送方式的总价格
                transfeeDto.setDeliveryAmount(freeAmount);
                transfeeDto.setDesc("express");
                transfeeDto.setFreightMode(FreightModeEnum.TRANSPORT_EXPRESS.value());
                transfeeDtos.add(transfeeDto);
            }

        }

        if (transport.isTransMail()) { // mail快递方式

            Transfee defaultransfee = getTransfeeDto(transport, cityId, FreightModeEnum.TRANSPORT_MAIL.value());
            TransfeeDto transfeeDto = new TransfeeDto();
            if (defaultransfee != null) {
                double freeAmount = clacCartDeleivery(transport.getTransType(), defaultransfee, totalWeight,
                        totalvolume, totalCount); // 该配送方式的总价格
                transfeeDto.setDeliveryAmount(freeAmount);
                transfeeDto.setDesc("mail");
                transfeeDto.setFreightMode(FreightModeEnum.TRANSPORT_MAIL.value());
                transfeeDtos.add(transfeeDto);
            }
        }
        return transfeeDtos;
    }

    /**
     * 获取运费规则[1、符合城市地址的运费规则 2、默认的运费规则]
     *
     * @param transport
     * @param userCityId
     * @param trans_type
     * @return
     */
    private Transfee getTransfeeDto(Transport transport, Integer userCityId, String trans_type) {

        /* TransfeeDto transfeeDto=new TransfeeDto(); */
        Transfee defaultransfee = null;
        Transfee currentTransfee = null;

        if (FreightModeEnum.TRANSPORT_MAIL.value().equals(trans_type)) { // EMS快递方式

            /**
             * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元) 广州：首重 10(kg) 首费5(元) 续重1(kg)
             * 续费1(元) 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
             */
            List<Transfee> mailList = transport.getMailList();
            for (int k = 0; k < mailList.size(); k++) {
                Transfee transfee2 = mailList.get(k);
                if (AppUtils.isBlank(transfee2.getCityId()) && transport.getIsRegionalSales() == 0) {
                    // 默认方式
                    defaultransfee = transfee2;
                } else if (AppUtils.isNotBlank(transfee2.getCityIdList())
                        && transfee2.getCityIdList().contains(Long.valueOf(userCityId))) {
                    currentTransfee = transfee2;
                    break;
                }
            }

        } else if (FreightModeEnum.TRANSPORT_EXPRESS.value().equals(trans_type)) {

            /**
             * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元) 广州：首重 10(kg) 首费5(元) 续重1(kg)
             * 续费1(元) 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
             */
            List<Transfee> expressList = transport.getExpressList();
            for (int k = 0; k < expressList.size(); k++) {
                Transfee transfee2 = expressList.get(k);
                if (AppUtils.isBlank(transfee2.getCityId()) && transport.getIsRegionalSales() == 0) {
                    // 默认方式
                    defaultransfee = transfee2;
                } else if (AppUtils.isNotBlank(transfee2.getCityIdList())
                        && transfee2.getCityIdList().contains(Long.valueOf(userCityId))) {
                    currentTransfee = transfee2;
                    break;
                }
            }

        } else if (FreightModeEnum.TRANSPORT_EMS.value().equals(trans_type)) {
            /**
             * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元) 广州：首重 10(kg) 首费5(元) 续重1(kg)
             * 续费1(元) 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
             */
            List<Transfee> emsList = transport.getEmsList();
            for (int k = 0; k < emsList.size(); k++) {
                Transfee transfee2 = emsList.get(k);
                if (AppUtils.isBlank(transfee2.getCityId()) && transport.getIsRegionalSales() == 0) {
                    // 默认方式
                    defaultransfee = transfee2;
                } else if (AppUtils.isNotBlank(transfee2.getCityIdList())
                        && transfee2.getCityIdList().contains(Long.valueOf(userCityId))) {
                    currentTransfee = transfee2;
                    break;
                }
            }

        }
        if (currentTransfee == null)
            currentTransfee = defaultransfee;
        defaultransfee = null;
        return currentTransfee;
    }

    /**
     * 计算运费金额
     *
     * @param transType   配送方式[ems ,mail ....]
     * @param transfee    配送规则实体
     * @param totalWeight 总重量
     * @param totalVolume 总体积
     * @param totalCount  总数
     * @return
     */
    private double clacCartDeleivery(String transType, Transfee transfee, double totalWeight, double totalVolume,
                                     double totalCount) {
        if (AppUtils.isNotBlank(transfee)) {
            ArithmeticEngine arithmeticEngine = ArithmeticEngine.getInstance();
            double first = transfee.getTransWeight().doubleValue(); // 首件
            double firstPrice = transfee.getTransFee().doubleValue();// 首费;
            double xujian = transfee.getTransAddWeight().doubleValue();// 续件
            double xujian_price = transfee.getTransAddFee().doubleValue();// 续费
            double price = 0.0d;
            try {
                if (TransTypeEnum.NUMBER.value().equals(transType)) {
                    price = arithmeticEngine.clacStanderd(first, xujian, firstPrice, xujian_price, totalCount);
                } else if (TransTypeEnum.WEIGHT.value().equals(transType)) {

                    price = arithmeticEngine.clacStanderd(first, xujian, firstPrice, xujian_price, totalWeight);
                } else if (TransTypeEnum.VOLUME.value().equals(transType)) {
                    price = arithmeticEngine.clacStanderd(first, xujian, firstPrice, xujian_price, totalVolume);
                }
                return price;
            } catch (SyntaxException e) {
                e.printStackTrace();
                return 0.0d;
            }
        }
        return 0.0d;
    }


}
