/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinImgFileDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinImgFile;

/**
 *微信图片Dao
 */
@Repository
public class WeixinImgFileDaoImpl extends GenericDaoImpl<WeixinImgFile, Long> implements WeixinImgFileDao  {
     
    public List<WeixinImgFile> getWeixinImgFile(){
   		return this.queryByProperties(new EntityCriterion());
    }

	public WeixinImgFile getWeixinImgFile(Long id){
		return getById(id);
	}
	
    public int deleteWeixinImgFile(WeixinImgFile weixinImgFile){
    	return delete(weixinImgFile);
    }
	
	public Long saveWeixinImgFile(WeixinImgFile weixinImgFile){
		return save(weixinImgFile);
	}
	
	public int updateWeixinImgFile(WeixinImgFile weixinImgFile){
		return update(weixinImgFile);
	}
	
	@Override
	public PageSupport<WeixinImgFile> getWeixinImgFile(String curPageNO, int pageSize, Long type) {
		CriteriaQuery cq = new CriteriaQuery(WeixinImgFile.class, curPageNO);
    	cq.setPageSize(pageSize);
    	cq.eq("imgGoupId", type);
		return queryPage(cq);
	}

 }
