package com.legendshop.business.service.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.stereotype.Service;

import com.legendshop.model.dto.order.KuaiDiLogs;
import com.legendshop.spi.service.KuaiDiService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 快递100查询中心
 * 
 *
 */
@Service("kuaiDiService")
public class KuaiDiServiceImpl implements KuaiDiService {

	/**
	 * (
	 * "http://api.kuaidi100.com/api?id=XXXX&com=tiantian&nu=11111&show=2&muti=1&order=desc"
	 * ); {"message":"ok","status":"1","state":"3","data":
	 * [{"time":"2012-07-07 13:35:14","context":"客户已签收"},
	 * {"time":"2012-07-07 09:10:10","context":"离开 [北京石景山营业厅] 派送中，递送员[温]，电话[]"},
	 * {"time":"2012-07-06 19:46:38","context":"到达 [北京石景山营业厅]"},
	 * {"time":"2012-07-06 15:22:32","context":"离开 [北京石景山营业厅] 派送中，递送员[温]，电话[]"},
	 * {"time":"2012-07-06 15:05:00","context":"到达 [北京石景山营业厅]"},
	 * {"time":"2012-07-06 13:37:52","context":"离开 [北京_同城中转站] 发往 [北京石景山营业厅]"},
	 * {"time":"2012-07-06 12:54:41","context":"到达 [北京_同城中转站]"},
	 * {"time":"2012-07-06 11:11:03","context":"离开 [北京运转中心驻站班组] 发往 [北京_同城中转站]"},
	 * {"time":"2012-07-06 10:43:21","context":"到达 [北京运转中心驻站班组]"},
	 * {"time":"2012-07-05 21:18:53","context":"离开 [福建_厦门支公司] 发往 [北京运转中心_航空]"},
	 * {"time":"2012-07-05 20:07:27","context":"已取件，到达 [福建_厦门支公司]"} ]}
	 * 
	 * @param url
	 * @return
	 */
	@Override
	public KuaiDiLogs query(String url) {
		if(AppUtils.isBlank(url)){
			return null;
		}
		KuaiDiLogs logs = new KuaiDiLogs();
		HttpClient httpClient = new HttpClient();
		GetMethod method = new GetMethod(url);
		try {
			httpClient.executeMethod(method);
			byte[] result = method.getResponseBody();
			String str = new String(result, "UTF-8");
			str = unescapeJavaScript(str);
			try {
				kuaiDiInfo diInfo = JSONUtil.getObject(str, kuaiDiInfo.class);
				if (diInfo.getMessage().equals("ok")) {
					for (Entry entry : diInfo.getData()) {
						logs.addEntry(entry.getTime(), entry.getContext());
					}
					Collections.sort(logs.getEntries(),
							new Comparator<KuaiDiLogs.Status>() {
								@Override
								public int compare(KuaiDiLogs.Status o1,
										KuaiDiLogs.Status o2) {
									if (o1 == null || o2 == null) {
										return 1;
									}
									return o1.getDate().compareTo(o2.getDate());
								}
							});
				}
			} catch (StringIndexOutOfBoundsException e) {

			}
		} catch (IOException e) {
			throw new IllegalArgumentException("网络异常", e);
		}
		return logs;
	}

	private String unescapeJavaScript(String str) {// NOSONAR
		if (str == null) {
			return null;
		}

		StringBuffer writer = new StringBuffer(str.length());
		int sz = str.length();
		StringBuffer unicode = new StringBuffer(4);
		boolean hadSlash = false;
		boolean inUnicode = false;

		for (int i = 0; i < sz; i++) {
			char ch = str.charAt(i);
			if (inUnicode) {
				// if in unicode, then we're reading unicode
				// values in somehow
				unicode.append(ch);
				if (unicode.length() == 4) {
					// unicode now contains the four hex digits
					// which represents our unicode chacater
					try {
						int value = Integer.parseInt(unicode.toString(), 16);
						writer.append((char) value);
						unicode.setLength(0);
						inUnicode = false;
						hadSlash = false;
					} catch (NumberFormatException nfe) {
						throw new IllegalArgumentException(
								"Unable to parse unicode value: " + unicode
										+ " cause: " + nfe);
					}
				}
				continue;
			}

			if (hadSlash) {
				// handle an escaped value
				hadSlash = false;
				switch (ch) {
				case '\\':
					writer.append('\\');
					break;
				case '\'':
					writer.append('\'');
					break;
				case '\"':
					writer.append('"');
					break;
				case 'r':
					writer.append('\r');
					break;
				case 'f':
					writer.append('\f');
					break;
				case 't':
					writer.append('\t');
					break;
				case 'n':
					writer.append('\n');
					break;
				case 'b':
					writer.append('\b');
					break;
				case 'u':
					// uh-oh, we're in unicode country....
					inUnicode = true;
					break;
				default:
					writer.append(ch);
					break;
				}
				continue;
			} else if (ch == '\\') {
				hadSlash = true;
				continue;
			}
			writer.append(ch);
		}

		if (hadSlash) {
			// then we're in the weird case of a \ at the end of the
			// string, let's output it anyway.
			writer.append('\\');
		}

		return writer.toString();
	}

	public static class kuaiDiInfo {
		String message;
		List<Entry> data;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public List<Entry> getData() {
			return data;
		}

		public void setData(List<Entry> data) {
			this.data = data;
		}
	}

	public static class Entry {
		private String time;
		private String context;

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getContext() {
			return context;
		}

		public void setContext(String context) {
			this.context = context;
		}
	}

}
