/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.AdminUserDao;
import com.legendshop.business.dao.UserRoleDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.entity.AdminUser;
import com.legendshop.model.entity.Role;
import com.legendshop.model.entity.UserRole;
import com.legendshop.model.entity.UserRoleId;
import com.legendshop.util.AppUtils;

/**
 * 管理员管理
 */
@Repository
public class AdminUserDaoImpl extends GenericDaoImpl<AdminUser, String> implements AdminUserDao {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(AdminUserDaoImpl.class);

	@Autowired
	private UserRoleDao userRoleDao;

	public AdminUser getAdminUser(String id) {
		return getById(id);
	}

	public int deleteAdminUser(AdminUser adminUser) {
		// 删除用户相关角色
		userRoleDao.deleteUserRoleByUserId(adminUser.getId(), ApplicationEnum.BACK_END);
		return delete(adminUser);
	}

	public String saveAdminUser(AdminUser adminUser) {
		String[] roleIds = adminUser.getRoleId();
		if (AppUtils.isBlank(roleIds)) {
			throw new BusinessException("role id can not empty for user " + adminUser.getId());
		}
		String userId = save(adminUser);
		List<UserRole> userRoles = new ArrayList<UserRole>();
		for (String roleId : roleIds) {
			UserRoleId id = new UserRoleId(userId, roleId, ApplicationEnum.BACK_END.value());
			UserRole ur = new UserRole(id);
			userRoles.add(ur);
		}
		userRoleDao.saveUserRoleList(userRoles);
		return userId;
	}

	public int updateAdminUser(AdminUser adminUser) {
		return update(adminUser);
	}

	public PageSupport<AdminUser> getAdminUser(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public Boolean isAdminUserExist(String userName) {
		List<String> list = this.query("select 1 from ls_admin_user where name = ?", String.class, userName);
		return AppUtils.isNotBlank(list);
	}

	/**
	 * 更新管理员密码
	 */
	@Override
	public int updateAdminUserPassword(String id, String newPassword) {
		AdminUser user = getAdminUser(id);
		if (user == null) {
			log.warn("can not found user by id {}", id);
		}
		user.setPassword(newPassword);
		return update(user);
	}

	@Override
	public List<Role> loadRole(String appNo) {
		return query("select * from ls_role where app_no = ?", Role.class, appNo);
	}

	@Override
	public List<Role> loadRole(String userId, String appNo) {
		return query("SELECT *  FROM ls_role r, ls_usr_role ur WHERE ur.role_id = r.id AND ur.user_id = ? AND r.app_no = ?", Role.class, appNo);
	}

}
