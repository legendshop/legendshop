/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopBannerDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopBanner;
import com.legendshop.spi.service.ShopBannerService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 商家默认的轮换图服务
 *
 */
@Service("shopBannerService")
public class ShopBannerServiceImpl  implements ShopBannerService{

	@Autowired
    private ShopBannerDao shopBannerDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

	public ShopBanner getShopBanner(Long id) {
        return shopBannerDao.getShopBanner(id);
    }

    public void deleteShopBanner(ShopBanner shopBanner) {
    	if(AppUtils.isNotBlank(shopBanner.getImageFile())){
			//删除
			attachmentManager.delAttachmentByFilePath(shopBanner.getImageFile());
		}
        shopBannerDao.deleteShopBanner(shopBanner);
    }

    public Long saveShopBanner(ShopBanner shopBanner) {
        if (!AppUtils.isBlank(shopBanner.getId())) {
            updateShopBanner(shopBanner);
            return shopBanner.getId();
        }
        return shopBannerDao.saveShopBanner(shopBanner);
    }

    public void updateShopBanner(ShopBanner shopBanner) {
        shopBannerDao.updateShopBanner(shopBanner);
    }

	@Override
	public PageSupport<ShopBanner> getShopBanner(String curPageNO, Long shopId) {
		return shopBannerDao.getShopBanner(curPageNO,shopId);
	}
}
