/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.model.entity.ShopDetail;

/**
 * 系统logoDao.
 */
public interface LogoDao{

	public abstract void deleteLogo(ShopDetail shopDetail);

	public abstract void updateLogo(ShopDetail shopDetail);

}