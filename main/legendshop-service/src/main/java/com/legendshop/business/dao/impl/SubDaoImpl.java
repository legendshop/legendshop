/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.legendshop.model.dto.*;
import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.app.dto.IncomeTodayDto;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.DeliveryCorpDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderDeleteStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SubStatusEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.order.DeliveryDto;
import com.legendshop.model.dto.order.InvoiceSubDto;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderDetailDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.order.PresellSubDto;
import com.legendshop.model.dto.order.PresellSubItemDto;
import com.legendshop.model.dto.order.SubOrderItemDto;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.model.entity.OrderSub;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.model.entity.store.StoreProd;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 
 * 订单Dao
 */
@Repository("subDao")
public class SubDaoImpl extends GenericDaoImpl<Sub, Long> implements SubDao {

	/** 订单历史dao. */
	@Autowired
	private SubHistoryDao subHistoryDao;

	/** 物流方式Dao. */
	@Autowired
	private DeliveryCorpDao deliveryCorpDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.SubDao#saveSub(com.legendshop.model.
	 * entity.Sub)
	 */
	@Override
	public Long saveSub(Sub sub) {
		return save(sub);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.SubDao#deleteSub(com.legendshop.model
	 * .entity.Sub)
	 */
	@Override
	public boolean deleteSub(Sub sub) {
		if (sub != null) {
			subHistoryDao.saveSubHistory(sub, SubStatusEnum.ORDER_DEL.value());
			int result = delete(sub);
			return result > 0;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.SubDao#adminChangeSubPrice(com.
	 * legendshop .model.entity.Sub, java.lang.String, java.lang.Double)
	 */
	@Override
	public boolean updateSubPrice(Sub sub, String userName, Double totalPrice) {
		if (sub != null) {
			subHistoryDao.saveSubHistory(sub, SubStatusEnum.PRICE_CHANGE.value());
			sub.setActualTotal(totalPrice);
			updateSub(sub);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.SubDao#getSubById(java.lang.Long)
	 */
	@Override
	public Sub getSubById(Long subId) {
		return getById(subId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.SubDao#findSubBySubNumber(java.lang.
	 * String)
	 */
	@Override
	public Sub getSubBySubNumber(String subNumber) {
		if (AppUtils.isBlank(subNumber)) {
			return null;
		}
		return getByProperties(new EntityCriterion().eq("subNumber", subNumber));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.SubDao#finishUnPay(int,
	 * java.util.Date)
	 */
	@Override
	public List<Sub> getFinishUnPay(int maxNum, Date expireDate) {
		String sql = ConfigCode.getInstance().getCode("order.getFinishUnPay");
		return queryLimit(sql, Sub.class, 0, maxNum, expireDate, OrderStatusEnum.UNPAY.value(), CartTypeEnum.AUCTIONS.toCode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.impl.SubDao#findUnAcklodgeSub(int,
	 * java.util.Date)
	 */
	@Override
	public List<Sub> getUnAcklodgeSub(int maxNum, Date expireDate) {
		String sql = ConfigCode.getInstance().getCode("order.getUnAcklodgeSub");
		return queryLimit(sql, Sub.class, 0, maxNum, expireDate, OrderStatusEnum.CONSIGNMENT.value());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.SubDao#removeOverTimeBasket(java.util
	 * .Date)
	 */
	@Override
	public void deleteOverTimeBasket(Date date) {
		String sql = ConfigCode.getInstance().getCode("order.deleteOverTimeBasket");
		update(sql, Constants.TRUE_INDICATOR, date);
	}

	@Override
	public void updateSub(Sub sub) {
		update(sub);
	}

	/**
	 * 所有正在处理的订单
	 */
	@Override
	public Long getTotalProcessingOrder(Long shopId) {
		String sql = ConfigCode.getInstance().getCode("order.getTotalProcessingOrder");
		return getLongResult(sql, Constants.FALSE_INDICATOR, shopId);
	}

	/**
	 * 判断用户是否已经订购有效某个产品
	 */
	@Override
	public boolean isUserOrderProduct(Long prodId, String userName) {
		String sql = ConfigCode.getInstance().getCode("order.isUserOrderProduct");
		Long result = getLongResult(sql, Long.class, prodId, userName);
		return result > 0;
	}

	/**
	 * 判断用户是否有订购过有效的订单
	 * 
	 * @param userName
	 * @return
	 */
	public boolean isUserOrderProduct(String userName) {
		String sql = ConfigCode.getInstance().getCode("order.isUserOrderProduct2");
		Long result = getLongResult(sql, userName);
		return result > 0;
	}

	/**
	 * 删除订单
	 */
	@Override
	public void deleteSub(String userId, String userName) {
		update(ConfigCode.getInstance().getCode("order.deleteSubHis"), userId); // 删除用户订单项目
		update(ConfigCode.getInstance().getCode("order.deleteSubItem"), userId); // 删除用户订单项目
		update(ConfigCode.getInstance().getCode("order.deleteSub"), userId); // 删除用户订单项目
	}

	@Override
	public Long getSubId() {
		return createId();
	}

	@Override
	public Sub getSubBySubNumberByUserId(String subNumber, String userId) {
		if (AppUtils.isBlank(subNumber) && AppUtils.isBlank(userId)) {
			return null;
		}
		return getByProperties(new EntityCriterion().eq("subNumber", subNumber).eq("userId", userId));
	}
	
	
	@Override
	public List<Sub> getSubBySubNumberByUserId(List<String> subNumbers, String userId) {
		EntityCriterion entityCriterion=new EntityCriterion().eq("userId", userId);
		entityCriterion.in("subNumber", subNumbers.toArray());
		return queryByProperties(entityCriterion);
	}
	

	@Override
	public Sub getSubBySubNumberByShopId(String subNumber, Long ShopId) {
		if (AppUtils.isBlank(subNumber) && AppUtils.isBlank(ShopId)) {
			return null;
		}
		return getByProperties(new EntityCriterion().eq("subNumber", subNumber).eq("shopId", ShopId));
	}

	@Override
	public List<Sub> getSubByIds(String[] subIds, String userId, Integer status) {
		QueryMap map = new QueryMap();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < subIds.length; i++) {
			sql.append("?,");
			params.add(subIds[i]);
		}
		sql.setLength(sql.length() - 1);
		map.put("subByIds", sql.toString());
		params.add(userId);
		params.add(status);
		String query = ConfigCode.getInstance().getCode("order.getSubByIds", map);
		return query(query, Sub.class, params.toArray());
	}

	@Override
	public int updateSubForPay(Sub sub, Integer currentStatus) {
		String query = ConfigCode.getInstance().getCode("order.updateSubForPay");
		return update(query, sub.getPayId(), sub.getPayTypeId(), sub.getPayTypeName(), sub.getPayDate(), sub.getUpdateDate(), sub.getStatus(), sub.isPayed(),
				sub.getFlowTradeNo(), sub.getSubSettlementSn(), sub.getSubId(), currentStatus);
	}

	@Override
	public List<OrderSub> getOutPutExcelOrder(Long[] subIds) {
		QueryMap map = new QueryMap();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < subIds.length; i++) {
			sql.append("?,");
			params.add(subIds[i]);
		}
		sql.setLength(sql.length() - 1);
		map.put("subByIds", sql.toString());
		String query = ConfigCode.getInstance().getCode("order.getOutPutExcelOrder", map);
		return query(query, OrderSub.class, params.toArray());
	}

	@Override
	public List<SubCountsDto> querySubCounts(String userId) {
		String query = ConfigCode.getInstance().getCode("order.querySubCounts");
		return query(query, SubCountsDto.class, userId);
	}

	/**
	 * 统计一个月前到现在的订单
	 */
	@Override
	public List<KeyValueEntity> getSubCounts(Long shopId) {
		Date now = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(now, Integer.parseInt(dashboardDate));
		List<KeyValueEntity> result = null;
		List<Map<String, Object>> list = queryForList(ConfigCode.getInstance().getCode("order.getSubCountsBySubDate"), shopId, startDate);
		if (list != null) {
			result = new ArrayList<KeyValueEntity>();
			for (Map<String, Object> map : list) {
				KeyValueEntity kve = new KeyValueEntity(map.get("status").toString(), (String) map.get("value").toString());
				result.add(kve);
			}
		}
		return result;
	}

	/**
	 * 查询在endDate之前已经完成的订单
	 */
	@Override
	public List<Sub> getBillFinishOrders(Long shopId, Date endDate, Integer status) {
		String sql = ConfigCode.getInstance().getCode("order.getBillFinishOrders");
		return this.query(sql, Sub.class, shopId, status, endDate);
	}

	@Override
	@Caching(evict = { @CacheEvict(value = "OrderDtoList", key = "#userId + '.' + #deleteStatus") })
	public void cleanOrderCache(String userId, Integer deleteStatus) {

	}

	@Override
	@Caching(evict = { @CacheEvict(value = "UnpayOrderCount", key = "#userId") })
	public void cleanUnpayOrderCount(String userId) {

	}

	@Override
	@Caching(evict = { @CacheEvict(value = "ConsignmentOrderCount", key = "#userId") })
	public void cleanConsignmentOrderCount(String userId) {

	}

	class MySubDtoRowMapper implements RowMapper<OrderDetailDto> {

		private Map<String, MySubDto> map = null;

		private boolean config; // 是否查询其他订单信息

		public MySubDtoRowMapper(Map<String, MySubDto> map, boolean config) {
			this.map = map;
			this.config = config;
		}

		@Override
		public OrderDetailDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			String subNumber = rs.getString("subNumber");
			if (map.containsKey(subNumber)) { // 存在
				MySubDto mySubDto = map.get(subNumber);
				SubOrderItemDto mySubItemDto = buildSubItem(rs);
				mySubDto.addItem(mySubItemDto);
			} else {
				MySubDto dto = buildSub(rs);
				SubOrderItemDto mySubItemDto = buildSubItem(rs);
				dto.addItem(mySubItemDto);
				if (config) {
					// 载入用户发票信息
					if (AppUtils.isNotBlank(dto.getInvoiceSubId()) && dto.getInvoiceSubId() != 0) {
						InvoiceSub invoiceSub = new InvoiceSub();
						invoiceSub.setType(rs.getInt("invType"));
						invoiceSub.setTitle(rs.getInt("invTitle"));
						invoiceSub.setCompany(rs.getString("invCompany"));
						invoiceSub.setInvoiceHumNumber(rs.getString("invoiceHumNumber"));
						invoiceSub.setRegisterAddr(rs.getString("registerAddr"));
						invoiceSub.setRegisterPhone(rs.getString("registerPhone"));
						invoiceSub.setDepositBank(rs.getString("depositBank"));
						invoiceSub.setBankAccountNum(rs.getString("bankAccountNum"));

						dto.setInvoiceSub(invoiceSub);
					}
					// 载入用户地址信息
					if (AppUtils.isNotBlank(dto.getAddrOrderId())) {
						UserAddressSub userAddressSub = new UserAddressSub();
						userAddressSub.setReceiver(rs.getString("receiver"));
						userAddressSub.setTelphone(rs.getString("telphone"));
						userAddressSub.setSubPost(rs.getString("subPost"));
						userAddressSub.setMobile(rs.getString("mobile"));
						userAddressSub.setDetailAddress(rs.getString("detailAddress"));
						dto.setUserAddressSub(userAddressSub);
					}

				}

				// 载入订单物流公司信息
				if (AppUtils.isNotBlank(dto.getDvyTypeId())) {
					DeliveryDto delivery = new DeliveryDto();
					delivery.setDelName(rs.getString("companyName"));
					delivery.setDelUrl(rs.getString("companyHomeUrl"));
					delivery.setQueryUrl(rs.getString("queryUrl"));
					delivery.setDvyFlowId(dto.getDvyFlowId());
					delivery.setDvyTypeId(dto.getDvyTypeId());
					dto.setDelivery(delivery);
				}

				map.put(subNumber, dto);
			}
			return null;
		}

		public MySubDto buildSub(ResultSet rs) throws SQLException {
			MySubDto dto = new MySubDto();
			dto.setSubId(rs.getLong("subId"));
			dto.setSubNum(rs.getString("subNumber"));
			dto.setProdName(rs.getString("prodName"));
			dto.setUserId(rs.getString("userId"));
			dto.setUserName(rs.getString("userName"));
			dto.setSubType(rs.getString("subType"));
			dto.setTotal(rs.getDouble("total"));
			dto.setActualTotal(rs.getDouble("actualTotal"));
			dto.setPayManner(rs.getInt("payManner"));
			dto.setPayTypeId(rs.getString("payTypeId"));
			dto.setPayTypeName(rs.getString("payTypeName"));
			dto.setPayDate(rs.getTimestamp("payDate"));
			dto.setOther(rs.getString("other"));
			dto.setShopId(rs.getLong("shopId"));
			dto.setShopName(rs.getString("shopName"));
			dto.setStatus(rs.getInt("status"));
			dto.setScore(rs.getInt("score"));
			dto.setDvyType(rs.getString("dvyType"));
			dto.setDvyTypeId(rs.getLong("dvyTypeId"));
			dto.setDvyFlowId(rs.getString("dvyFlowId"));
			dto.setFreightAmount(rs.getDouble("freightAmount"));
			dto.setInvoiceSubId(rs.getLong("invoiceSubId"));
			dto.setOrderRemark(rs.getString("orderRemark"));
			dto.setAddrOrderId(rs.getLong("addrOrderId"));
			dto.setIsNeedInvoice(rs.getInt("isNeedInvoice") == 1 ? true :false);	
			dto.setWeight(rs.getDouble("weight"));
			dto.setVolume(rs.getDouble("volume"));
			dto.setProductNums(rs.getInt("productNums"));
			dto.setSubDate(rs.getTimestamp("subDate"));
			dto.setDvyDate(rs.getTimestamp("dvyDate"));
			dto.setFinallyDate(rs.getTimestamp("finallyDate"));
			dto.setUpdateDate(rs.getTimestamp("updateDate"));
			dto.setIspay(rs.getInt("status") == 1 ? false : true);
			dto.setIsCod(rs.getInt("isCod") == 1 ? false : true);
			dto.setDeleteStatus(rs.getInt("deleteStatus"));
			dto.setDiscountPrice(rs.getDouble("discountPrice"));
			dto.setDistCommisAmount(rs.getDouble("distCommisAmount"));
			dto.setCouponOffPrice(rs.getDouble("couponOffPrice"));         
			dto.setRefundState(rs.getLong("refundState"));
			dto.setPredepositAmount(rs.getDouble("predepositAmount"));
			dto.setPredPayType(rs.getString("predPayType"));
			dto.setRedpackOffPrice(rs.getDouble("redpackOffPrice"));
			dto.setRemindDelivery(rs.getBoolean("remindDelivery"));
			dto.setRemindDeliveryTime(rs.getDate("remindDeliveryTime"));
			dto.setSubSettlement(rs.getString("subSettlement"));
			dto.setFlowTradeNo(rs.getString("flowTradeNo"));
			dto.setCancelReason(rs.getString("cancelReason"));
			//备注
			dto.setIsShopRemarked(rs.getInt("isShopRemarked") == 1 ? true : false);
			dto.setShopRemark(rs.getString("shopRemark"));
			dto.setShopRemarkDate(rs.getTimestamp("shopRemarkDate"));
			dto.setActiveId(rs.getLong("activeId"));
			dto.setGroupStatus(rs.getInt("groupStatus"));
			dto.setMergeGroupStatus((Integer) rs.getObject("mergeGroupStatus"));
			dto.setAddNumber(rs.getString("addNumber"));
			
			//2018-12-18  新增 预售订单的信息
			dto.setPreDepositPrice(rs.getBigDecimal("preDepositPrice"));
			dto.setIsPayDeposit(rs.getInt("isPayDeposit"));
			dto.setDepositPayName(rs.getString("depositPayName"));
			dto.setDepositTradeNo(rs.getString("depositTradeNo"));
			dto.setFinalPayName(rs.getString("finalPayName"));
			dto.setFinalTradeNo(rs.getString("finalTradeNo"));
			dto.setFinalPrice(rs.getBigDecimal("finalPrice"));
			dto.setPayPctType(rs.getInt("payPctType"));
			dto.setFinalMStart(rs.getTimestamp("finalMStart"));
			dto.setFinalMEnd(rs.getTimestamp("finalMEnd"));
			dto.setIsPayFinal(rs.getInt("isPayFinal"));
			dto.setDepositPayTime(rs.getTimestamp("depositPayTime"));
			dto.setFinalPayTime(rs.getTimestamp("finalPayTime"));
			dto.setChangedPrice(rs.getDouble("changedPrice"));

			// 2020-08-17 这里漏了退款id
			dto.setRefundId(rs.getLong("refundId"));

			return dto;
		}

		private SubOrderItemDto buildSubItem(ResultSet rs) throws SQLException {
			SubOrderItemDto subOrderItemDto = new SubOrderItemDto();
			subOrderItemDto.setSubItemId(rs.getLong("subItemId"));
			subOrderItemDto.setSubItemNumber(rs.getString("subItemNumber"));
			subOrderItemDto.setProdId(rs.getLong("prodId"));
			subOrderItemDto.setSkuId(rs.getLong("skuId"));
			subOrderItemDto.setSnapshotId(rs.getLong("snapshotId"));
			subOrderItemDto.setBasketCount(rs.getInt("basketCount"));
			subOrderItemDto.setProdName(rs.getString("itemProdName"));
			subOrderItemDto.setAttribute(rs.getString("attribute"));
			subOrderItemDto.setPic(rs.getString("pic"));
			subOrderItemDto.setCash(rs.getDouble("cash"));
			subOrderItemDto.setPrice(rs.getDouble("price"));
			subOrderItemDto.setProductTotalAmout(rs.getDouble("productTotalAmout"));
			subOrderItemDto.setObtainIntegral(rs.getDouble("obtainIntegral"));
			subOrderItemDto.setWeight(rs.getDouble("itemWeight"));
			subOrderItemDto.setVolume(rs.getDouble("itemVolume"));
			subOrderItemDto.setDistUserName(rs.getString("distUserName"));
			subOrderItemDto.setDistCommisCash(rs.getDouble("distCommisCash"));
			subOrderItemDto.setPromotionInfo(rs.getString("promotionInfo"));
			subOrderItemDto.setCommSts(rs.getInt("commSts"));
			subOrderItemDto.setRefundId(rs.getLong("refundId"));
			subOrderItemDto.setRefundState(rs.getLong("itemRefundState"));
			subOrderItemDto.setRefundAmount(rs.getDouble("refundAmount"));
			subOrderItemDto.setRefundType(rs.getLong("refundType"));
			subOrderItemDto.setRefundCount(rs.getLong("refundCount"));
			return subOrderItemDto;
		}
	}

	private DeliveryDto buildDeliveryDto(MySubDto sub) {
		if ((OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus()) || OrderStatusEnum.SUCCESS.value().equals(sub.getStatus()))
				&& AppUtils.isNotBlank(sub.getDvyTypeId())) {
			DeliveryCorp corp = deliveryCorpDao.getDeliveryCorpByDvyId(sub.getDvyTypeId());
			if (AppUtils.isNotBlank(corp)) {
				DeliveryDto delivery = new DeliveryDto();
				delivery.setDvyTypeId(sub.getDvyTypeId());
				delivery.setDvyFlowId(sub.getDvyFlowId());
				delivery.setDelName(corp.getName());
				delivery.setDelUrl(corp.getCompanyHomeUrl());
				delivery.setQueryUrl(corp.getQueryUrl());
				return delivery;
			}
		}
		return null;
	}

	/**
	 * 根据订单号, 查找订单详情
	 * @param subNumber 订单号
	 * @return 订单详情
	 */
	@Override
	public MySubDto findOrderDetail(String subNumber) {
		QueryMap queryMap = new QueryMap();
		String sql = ConfigCode.getInstance().getCode("order.findOrderDetail", queryMap);
		Map<String, MySubDto> map = new HashMap<String, MySubDto>();
		super.get(sql, new Object[] { subNumber }, new MySubDtoRowMapper(map, true));
		if (AppUtils.isBlank(map))
			return null;
		MySubDto mySubDto = map.get(subNumber);
		return mySubDto;
	}

	@Override
	public MySubDto findOrderDetail(String subNumber, Long shopId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("shopId", shopId);
		String sql = ConfigCode.getInstance().getCode("order.findOrderDetail", queryMap);
		Map<String, MySubDto> map = new HashMap<String, MySubDto>();
		super.get(sql, new Object[] { subNumber, shopId }, new MySubDtoRowMapper(map, true));
		if (AppUtils.isBlank(map))
			return null;
		MySubDto mySubDto = map.get(subNumber);
		return mySubDto;

	}

	@Override
	public MySubDto findOrderDetail(String subNumber, String userId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("userId", userId);
		String sql = ConfigCode.getInstance().getCode("order.findOrderDetail", queryMap);
		Map<String, MySubDto> map = new HashMap<String, MySubDto>();
		super.get(sql, new Object[] { subNumber, userId }, new MySubDtoRowMapper(map, true));
		if (AppUtils.isBlank(map))
			return null;
		MySubDto mySubDto = map.get(subNumber);
		return mySubDto;
	}

	/**
	 * 查询订单列表信息
	 */
	@Override
	public PageSupport<MySubDto> getOrders(QueryMap queryMap, List<Object> args, int curPageNO, int pageSize, OrderSearchParamDto paramDto) {
		String dialectType = this.getDialect().getDialectType();
		String countSql = ConfigCode.getInstance().getCode(dialectType + ".getOrdersCount", queryMap);
		String subSQl = ConfigCode.getInstance().getCode(dialectType + ".getOrders", queryMap);
		Long total = getLongResult(countSql, args.toArray());
		List<MySubDto> mySubDtos = getMySubDtos(subSQl, args);
		PageSupport<MySubDto> pageSupport = initPageProvider(total, curPageNO, pageSize, mySubDtos);
		return pageSupport;
	}

	private PageSupport<MySubDto> initPageProvider(long total, Integer curPageNO, Integer pageSize, List<MySubDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<MySubDto> ps = new PageSupport<MySubDto>(resultList, pageProvider);
		return ps;
	}

	private List<MySubDto> getMySubDtos(String sql, List<Object> args) {
		Map<String, MySubDto> map = new HashMap<String, MySubDto>();
		super.query(sql, args.toArray(), new MySubDtoListRowMapper(map));
		if (AppUtils.isBlank(map))
			return null;
		List<MySubDto> subDtos = new ArrayList<MySubDto>();
		for (Entry<String, MySubDto> entry : map.entrySet()) {
			MySubDto mySubDto = entry.getValue();
			subDtos.add(mySubDto);
		}
		Collections.sort(subDtos, new Comparator<MySubDto>() {
			@Override
			public int compare(MySubDto o1, MySubDto o2) {
				int flag = o2.getSubDate().compareTo(o1.getSubDate());
				return flag;
			}
		});
		return subDtos;
	}
	
	
	class MySubDtoListRowMapper implements RowMapper<OrderDetailDto> {

		private Map<String, MySubDto> map = null;

		public MySubDtoListRowMapper(Map<String, MySubDto> map) {
			this.map = map;
		}

		@Override
		public OrderDetailDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			String subNumber = rs.getString("subNumber");
			if (map.containsKey(subNumber)) { // 存在
				MySubDto mySubDto = map.get(subNumber);
				SubOrderItemDto mySubItemDto = buildSubItem(rs);
				mySubDto.addItem(mySubItemDto);
			} else {
				MySubDto dto = buildSub(rs);
				SubOrderItemDto mySubItemDto = buildSubItem(rs);
				
				dto.addItem(mySubItemDto);

				map.put(subNumber, dto);
			}
			return null;
		}

		public MySubDto buildSub(ResultSet rs) throws SQLException {
			MySubDto dto = new MySubDto();
			dto.setSubId(rs.getLong("subId"));
//			dto.setSkuId(rs.getString("skuId"));
			dto.setSubNum(rs.getString("subNumber"));
			dto.setUserId(rs.getString("userId"));
			dto.setUserName(rs.getString("userName"));
			dto.setReciverName(rs.getString("receiver"));
			dto.setReciverMobile(rs.getString("mobile"));
			dto.setSubType(rs.getString("subType"));
			dto.setActualTotal(rs.getDouble("actualTotal"));
			dto.setPayManner(rs.getInt("payManner"));
			dto.setShopId(rs.getLong("shopId"));
			dto.setShopName(rs.getString("shopName"));
			dto.setStatus(rs.getInt("status"));
			dto.setFreightAmount(rs.getDouble("freightAmount"));
			dto.setSubDate(rs.getTimestamp("subDate"));
			dto.setRefundState(rs.getLong("refundState"));
			dto.setDvyTypeId(rs.getLong("dvyTypeId"));
			dto.setSubSettlement(rs.getString("subSettlement"));
			dto.setFlowTradeNo(rs.getString("flowTradeNo"));
			//备注
			dto.setIsShopRemarked(rs.getInt("isShopRemarked") == 1 ? true : false);
			dto.setShopRemark(rs.getString("shopRemark"));
			dto.setShopRemarkDate(rs.getTimestamp("shopRemarkDate"));
			dto.setActiveId(rs.getLong("activeId"));
			dto.setGroupStatus(rs.getInt("groupStatus"));
			dto.setMergeGroupStatus((Integer) rs.getObject("mergeGroupStatus"));
			dto.setAddNumber(rs.getString("addNumber"));
			dto.setHasInvoice(rs.getInt("hasInvoice"));
			dto.setIsNeedInvoice(rs.getInt("isNeedInvoice") == 1 ? true : false);
			dto.setPayTypeId(rs.getString("payTypeId"));
			dto.setPayTypeName(rs.getString("payTypeName"));
			dto.setIspay(rs.getBoolean("ispay"));
			dto.setUpdateDate(rs.getTimestamp("updateDate"));
			dto.setProductNums(rs.getInt("productNums"));
			dto.setPayDate(rs.getTimestamp("payDate"));
			
			//2018-12-14  新增 预售订单的信息
			dto.setPreDepositPrice(rs.getBigDecimal("preDepositPrice"));
			dto.setIsPayDeposit(rs.getInt("isPayDeposit"));
			dto.setDepositPayName(rs.getString("depositPayName"));
			dto.setDepositTradeNo(rs.getString("depositTradeNo"));
			dto.setFinalPayName(rs.getString("finalPayName"));
			dto.setFinalTradeNo(rs.getString("finalTradeNo"));
			dto.setFinalPrice(rs.getBigDecimal("finalPrice"));
			dto.setPayPctType(rs.getInt("payPctType"));
			dto.setFinalMStart(rs.getTimestamp("finalMStart"));
			dto.setFinalMEnd(rs.getTimestamp("finalMEnd"));
			dto.setIsPayFinal(rs.getInt("isPayFinal"));
			dto.setFinallyDate(rs.getTimestamp("finallyDate"));
			return dto;
		}

		private SubOrderItemDto buildSubItem(ResultSet rs) throws SQLException {
			SubOrderItemDto subOrderItemDto = new SubOrderItemDto();
			subOrderItemDto.setSubItemId(rs.getLong("subItemId"));
			subOrderItemDto.setProdId(rs.getLong("prodId"));
			subOrderItemDto.setSnapshotId(rs.getLong("snapshotId"));
			subOrderItemDto.setBasketCount(rs.getInt("basketCount"));
			subOrderItemDto.setProdName(rs.getString("itemProdName"));
			subOrderItemDto.setAttribute(rs.getString("attribute"));
			subOrderItemDto.setPic(rs.getString("pic"));
			subOrderItemDto.setCash(rs.getDouble("cash"));
			subOrderItemDto.setProductTotalAmout(rs.getDouble("productTotalAmout"));
			subOrderItemDto.setCommSts(rs.getInt("commSts"));
			subOrderItemDto.setRefundId(rs.getLong("refundId"));
			subOrderItemDto.setRefundType(rs.getLong("refundType"));
			subOrderItemDto.setRefundState(rs.getLong("itemRefundState"));
			subOrderItemDto.setRefundAmount(rs.getDouble("refundAmount"));
			return subOrderItemDto;
		}
	}
	

	/**
	 * 查找用户曾经下过的团购单
	 */
	@Override
	public Integer findUserOrderGroup(String userId, Long productId, Long skuId, Long groupId) {
		return get(
				/*"SELECT SUM(basket_count) FROM ls_sub LEFT JOIN ls_sub_item ON ls_sub.sub_number=ls_sub_item.sub_number WHERE ls_sub.user_id=? AND  ls_sub.sub_type='GROUP' AND  ls_sub_item.prod_id=?  AND ls_sub.active_id=?",*/
				"SELECT SUM(basket_count) FROM ls_sub LEFT JOIN ls_sub_item ON ls_sub.sub_number=ls_sub_item.sub_number WHERE ls_sub.user_id=? AND  ls_sub.sub_type='GROUP' AND  ls_sub_item.prod_id=? AND ls_sub.active_id=? AND ls_sub.status <>5",
				Integer.class, new Object[] { userId, productId, groupId });
	}

	@Override
	public PageSupport<PresellSubDto> getPresellOrdersList(QueryMap queryMap, List<Object> args, int curPageNO, int pageSize, OrderSearchParamDto paramDto) {
		String querySQL = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".getPresellOrders", queryMap);
		String queryCountSQL = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".getPresellOrdersCount", queryMap);
		Long total = getLongResult(queryCountSQL, args.toArray());
		List<PresellSubDto> presellSubDtos = getPresellSubDtos(querySQL, args);
		PageSupport<PresellSubDto> pageSupport = initPageProviderPresellOrders(total, curPageNO, pageSize, presellSubDtos);
		return pageSupport;
	}

	private PageSupport<PresellSubDto> initPageProviderPresellOrders(long total, Integer curPageNO, Integer pageSize, List<PresellSubDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<PresellSubDto> ps = new PageSupport<PresellSubDto>(resultList, pageProvider);
		return ps;
	}

	/**
	 * @param querySQL
	 * @param args
	 * @return
	 */
	private List<PresellSubDto> getPresellSubDtos(String querySQL, List<Object> args) {
		return this.query(querySQL, args.toArray(), new PresellSubDtoRowMapper(false));
	}

	@Override
	public PresellSubDto findPresellOrderDetail(String subNumber) {
		QueryMap queryMap = new QueryMap();
		String sql = ConfigCode.getInstance().getCode("order.findPresellOrderDetail", queryMap);
		return super.get(sql, new Object[] { subNumber }, new PresellSubDtoRowMapper(true));
	}

	@Override
	public PresellSubDto findPresellOrderDetail(String subNumber, String userId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("userId", userId);
		String sql = ConfigCode.getInstance().getCode("order.findPresellOrderDetail", queryMap);
		return super.get(sql, new Object[] { subNumber, userId }, new PresellSubDtoRowMapper(true));
	}

	@Override
	public PresellSubEntity getPreSubBySubNoAndUserId(String subNumber, String userId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("userId", userId);
		String sql = ConfigCode.getInstance().getCode("order.getPreSubBySubNoAndOther", queryMap);
		return this.get(sql, PresellSubEntity.class, subNumber, userId);
	}

	@Override
	public PresellSubEntity getPreSubBySubNoAndShopId(String subNumber, Long shopId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("shopId", shopId);
		String sql = ConfigCode.getInstance().getCode("order.getPreSubBySubNoAndOther", queryMap);
		return this.get(sql, PresellSubEntity.class, subNumber, shopId);
	}

	@Override
	public PresellSubEntity getPreSubBySubNo(String subNumber) {
		QueryMap queryMap = new QueryMap();
		String sql = ConfigCode.getInstance().getCode("order.getPreSubBySubNoAndOther", queryMap);
		return this.get(sql, PresellSubEntity.class, subNumber);
	}

	/**
	 * 预售订单Dto的结果集映射
	 * 
	 * @Description
	 * @author 关开发
	 */
	class PresellSubDtoRowMapper implements RowMapper<PresellSubDto> {
		private boolean isFindDetail = false;// 是否查询订单详情
		private boolean isTheFirst = true;// 是否第一次

		PresellSubDtoRowMapper(boolean isFindDetail) {
			this.isFindDetail = isFindDetail;
		}

		@Override
		public PresellSubDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			if (isTheFirst) {// 如果是第一次
				rs.beforeFirst();// 使指针回到表头
				if (!rs.next())
					return null;// 探测有没有记录
				isTheFirst = false;
			}

			Long subId = rs.getLong("subId");// 记住

			// 封装预售订单
			PresellSubDto presellSubDto = new PresellSubDto();
			presellSubDto.setSubId(subId);
			presellSubDto.setSubNo(rs.getString("subNo"));
			presellSubDto.setShopId(rs.getLong("shopId"));
			presellSubDto.setShopName(rs.getString("shopName"));
			presellSubDto.setUserId(rs.getString("userId"));
			presellSubDto.setUserName(rs.getString("userName"));
			presellSubDto.setSubCreateTime(rs.getTimestamp("subCreateTime"));
			presellSubDto.setTotalPrice(rs.getDouble("totalPrice"));
			presellSubDto.setActualTotalPrice(rs.getDouble("actualTotalPrice"));
			presellSubDto.setFreightAmount(rs.getDouble("freightAmount"));
			presellSubDto.setBasketCount(rs.getLong("basketCount"));
			presellSubDto.setDepositPrice(rs.getDouble("depositPrice"));
			presellSubDto.setDepositPayName(rs.getString("depositPayName"));
			presellSubDto.setDepositTradeNo(rs.getString("depositTradeNo"));
			presellSubDto.setFinalPayName(rs.getString("finalPayName"));
			presellSubDto.setFinalTradeNo(rs.getString("finalTradeNo"));
			presellSubDto.setIsPayDeposit(rs.getLong("isPayDeposit"));
			presellSubDto.setFinalPrice(rs.getDouble("finalPrice"));
			presellSubDto.setFinalMStart(rs.getTimestamp("finalMStart"));
			presellSubDto.setFinalMEnd(rs.getTimestamp("finalMEnd"));
			presellSubDto.setIsPayFinal(rs.getLong("isPayFinal"));
			presellSubDto.setStatus(rs.getLong("status"));
			presellSubDto.setIsPayed(rs.getLong("isPayed"));
			presellSubDto.setPayDate(rs.getTimestamp("payDate"));
			presellSubDto.setPayTypeName(rs.getString("payTypeName"));
			presellSubDto.setAddrOrderId(rs.getLong("addrOrderId"));
			presellSubDto.setInvoiceSubId(rs.getLong("invoiceSubId"));
			presellSubDto.setDvyTypeId(rs.getLong("dvyTypeId"));
			presellSubDto.setDvyFlowId(rs.getString("dvyFlowId"));
			presellSubDto.setDvyDate(rs.getTimestamp("dvyDate"));
			presellSubDto.setFinallyDate(rs.getTimestamp("finallyDate"));
			presellSubDto.setDvyType(rs.getString("dvyType"));
			presellSubDto.setOrderRemark(rs.getString("orderRemark"));
			presellSubDto.setPayPctType(rs.getInt("payPctType"));
			presellSubDto.setRefundState(rs.getLong("refundState"));

			presellSubDto.setActiveId(rs.getLong("activeId"));
			presellSubDto.setUpdateDate(rs.getTimestamp("updateDate"));
			presellSubDto.setCancelReason(rs.getString("cancelReason"));

			// 如果查询的是详情页面
			if (isFindDetail) {
				// 载入用户发票信息
				if (AppUtils.isNotBlank(presellSubDto.getInvoiceSubId()) && presellSubDto.getInvoiceSubId() != 0) {
					InvoiceSub invoiceSub = new InvoiceSub();
					invoiceSub.setType(rs.getInt("invType"));
					invoiceSub.setTitle(rs.getInt("invTitle"));
					invoiceSub.setCompany(rs.getString("invCompany"));
					invoiceSub.setInvoiceHumNumber(rs.getString("invoiceHumNumber"));
					invoiceSub.setRegisterAddr(rs.getString("registerAddr"));
					invoiceSub.setRegisterPhone(rs.getString("registerPhone"));
					invoiceSub.setDepositBank(rs.getString("depositBank"));
					invoiceSub.setBankAccountNum(rs.getString("bankAccountNum"));
					presellSubDto.setInvoiceSub(invoiceSub);
				}
				// 载入用户地址信息
				if (AppUtils.isNotBlank(presellSubDto.getAddrOrderId())) {
					UserAddressSub userAddressSub = new UserAddressSub();
					userAddressSub.setReceiver(rs.getString("receiver"));
					userAddressSub.setTelphone(rs.getString("telphone"));
					userAddressSub.setSubPost(rs.getString("subPost"));
					userAddressSub.setMobile(rs.getString("mobile"));
					userAddressSub.setDetailAddress(rs.getString("detailAddress"));
					presellSubDto.setUserAddressSub(userAddressSub);
				}
				// 载入订单物流公司信息
				if (AppUtils.isNotBlank(presellSubDto.getDvyTypeId())) {
					DeliveryDto delivery = new DeliveryDto();
					delivery.setDelName(rs.getString("companyName"));
					delivery.setDelUrl(rs.getString("companyHomeUrl"));
					delivery.setQueryUrl(rs.getString("queryUrl"));
					delivery.setDvyFlowId(presellSubDto.getDvyFlowId());
					delivery.setDvyTypeId(presellSubDto.getDvyTypeId());
					presellSubDto.setDelivery(delivery);
				}
			}

			// 封装预售订单项
			PresellSubItemDto presellSubItemDto = new PresellSubItemDto();
			presellSubItemDto.setProdId(rs.getLong("prodId"));
			presellSubItemDto.setSkuId(rs.getLong("skuId"));
			presellSubItemDto.setProdName(rs.getString("prodName"));
			presellSubItemDto.setProdPrice(rs.getDouble("prodPrice"));
			presellSubItemDto.setProdCash(rs.getDouble("prodCash"));
			presellSubItemDto.setProdPic(rs.getString("prodPic"));
			presellSubItemDto.setAttribute(rs.getString("attribute"));
			presellSubItemDto.setCommSts(rs.getLong("commsts"));
			presellSubItemDto.setBasketCount(rs.getInt("basketCount"));
			presellSubItemDto.setProductTotalAmout(rs.getDouble("productTotalAmout"));

			List<PresellSubItemDto> orderItems = new ArrayList<PresellSubItemDto>();
			orderItems.add(presellSubItemDto);

			// 继续往下走
			while (rs.next()) {
				if (!subId.equals(rs.getLong("subId"))) {
					rs.previous();
					break;
				}

				// 封装预售订单项
				presellSubItemDto = new PresellSubItemDto();
				presellSubItemDto.setProdId(rs.getLong("prodId"));
				presellSubItemDto.setSkuId(rs.getLong("skuId"));
				presellSubItemDto.setProdName(rs.getString("prodName"));
				presellSubItemDto.setProdPrice(rs.getDouble("prodPrice"));
				presellSubItemDto.setProdCash(rs.getDouble("prodCash"));
				presellSubItemDto.setProdPic(rs.getString("prodPic"));
				presellSubItemDto.setAttribute(rs.getString("attribute"));
				presellSubItemDto.setCommSts(rs.getLong("commsts"));
				presellSubItemDto.setBasketCount(rs.getInt("basketCount"));
				presellSubItemDto.setProductTotalAmout(rs.getDouble("productTotalAmout"));

				orderItems.add(presellSubItemDto);
			}

			presellSubDto.setOrderItems(orderItems);
			return presellSubDto;
		}
	}

	@Override
	public int updateRefundState(Long subId, Integer refundState) {
		String sql = "UPDATE ls_sub SET refund_state = ? WHERE sub_id = ?";
		return this.update(sql, refundState, subId);
	}

	@Override
	public int closeSub(Long subId) {
		String sql = "UPDATE ls_sub SET update_date=?,status = ? WHERE sub_id = ?";
		return this.update(sql, new Date(), OrderStatusEnum.CLOSE.value(), subId);
	}

	@Override
	public int finalySub(Long subId) {
		Date timeDate = new Date();
		String sql = "UPDATE ls_sub SET update_date=?,finally_date=?,status = ? WHERE sub_id = ?";
		return this.update(sql, timeDate, timeDate, OrderStatusEnum.SUCCESS.value(), subId);
	}

	@Override
	public Integer getTodaySubCount(Long shopId) {
		return this.get("SELECT COUNT(s.sub_id) FROM ls_sub s WHERE s.shop_id=? AND s.status=?", Integer.class, shopId, OrderStatusEnum.PADYED.value());
	}

	@Override
	public List<OrderExprotDto> exportOrder(String string, Object[] array) {
		return this.query(string, OrderExprotDto.class, array);
	}

	@Override
	public Double getIncomeToday(Long shopId) {
		Date now = new Date();
		Date startDate = DateUtils.getIntegralStartTime(now);
		Date endDate = DateUtils.getIntegralEndTime(now);

		String sql = ConfigCode.getInstance().getCode("app.shop.getIncomeToday");
		return get(sql, Double.class, shopId, startDate, endDate);
	}

	@Override
	public int remindDelivery(Long subId, String userId) {
		String sql = "UPDATE ls_sub SET remind_delivery = 1, remind_delivery_time = ? WHERE sub_id = ? and user_id = ? ";
		return this.update(sql, new Date(), subId, userId);
	}

	@Override
	public List<Sub> findCancelOrders() {
		return this.query(
				"select sub_id as subId,sub_number as subNumber,sub_date as subDate,user_id as userId,sub_type as subType from ls_sub where status=? order by sub_date asc ",
				Sub.class, OrderStatusEnum.UNPAY.value());
	}

	@Override
	public void updateBuys(long buys, Long prodId) {
		update("update ls_prod set buys = ? where prod_id = ?", buys, prodId);
	}

	@Override
	public Sub getSubBySubItemId(Long subItemId) {
		String sql = "SELECT sub.* FROM ls_sub sub, ls_sub_item item " + "WHERE sub.sub_number = item.sub_number " + "AND item.sub_item_id = ? ";
		return this.get(sql, Sub.class, subItemId);
	}

	@Override
	public int remindDeliveryBySN(String subNumber, String userId) {
		String sql = "UPDATE ls_sub SET remind_delivery = 1, remind_delivery_time = ? WHERE sub_number = ? and user_id = ? ";
		return this.update(sql, new Date(), subNumber, userId);
	}

	@Override
	public PageSupport<Sub> getSubListPage(String curPageNO, ShopOrderBill shopOrderBill, String subNumber) {
		CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("shopId", shopOrderBill.getShopId());
		cq.eq("status", OrderStatusEnum.SUCCESS.value());
		cq.eq("billSn", shopOrderBill.getSn());
		cq.eq("subNumber", subNumber);
		return queryPage(cq);
	}

	@Override
	public PageSupport getOrdersPage(OrderSearchParamDto paramDto, int pageSize) {
		QueryMap param = new QueryMap();
		param.put("userName", paramDto.getUserName());

		if (AppUtils.isNotBlank(paramDto.getSubNumber())) {
			param.put("subNumber", paramDto.getSubNumber());
		}
		if (AppUtils.isNotBlank(paramDto.getStatus())) {
			// 子订单状态
			param.put("status", paramDto.getStatus());
		}

		if (AppUtils.isNotBlank(paramDto.getEndDate())) {
			paramDto.setEndDate(getEndMonment(paramDto.getEndDate()));// 结束日期更新为最后一天
		}

		if (AppUtils.isNotBlank(paramDto.getStartDate())) {
			param.put("startDate", paramDto.getStartDate());
		}
		if (AppUtils.isNotBlank(paramDto.getEndDate())) {
			param.put("endDate", paramDto.getEndDate());
		}

		param.put("removeStatus", paramDto.getRemoveStatus());
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getAdminCountOrder", param);// 查询订单数量
		String querySQL = ConfigCode.getInstance().getCode("prod.getAdminOrder", param);// 查询订单对象List
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(OrderSub.class, pageSize, paramDto.getCurPageNO());
		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(param.toArray());
		return querySimplePage(simpleSqlQuery);
	}

	public Date getEndMonment(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	@Override
	public PageSupport<Sub> getSubListPage(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {
		CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("shopId", shopId);
		cq.eq("status", OrderStatusEnum.SUCCESS.value());
		if (AppUtils.isNotBlank(subNumber)){
			cq.like("subNumber","%" + subNumber + "%" );
		}
		cq.eq("billSn", shopOrderBill.getSn());
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sub> getOrder(String curPageNO, String userName) {
		CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
		cq.setPageSize(15);
		cq.eq("userName", userName);
		cq.eq("deleteStatus", OrderDeleteStatusEnum.NORMAL.value());
		cq.gt("subDate", DateUtil.getTimeMonthsAgo(1));
		cq.addOrder("desc", "subDate");
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO,Long shopId) {
		
		String sql = "SELECT ls.sub_number AS subNumber,li.title_id AS titleId,li.company AS company,li.create_time AS createTime,li.invoice_hum_number AS invoiceHumNumber,ls.user_name AS userName,ls.sub_date AS buyDate,ls.has_invoice AS hasInvoice FROM ls_sub ls LEFT JOIN ls_invoice li ON ls.invoice_sub_id = li.id WHERE ls.invoice_sub_id IS NOT NULL AND ls.is_need_invoice=1 AND ls.shop_id=? order by li.create_time desc";
		String countSql = "SELECT count(1) FROM ls_sub ls LEFT JOIN ls_invoice li ON ls.invoice_sub_id = li.id WHERE ls.invoice_sub_id IS NOT NULL AND ls.is_need_invoice=1 AND ls.shop_id=?";
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(InvoiceSubDto.class, 10, curPageNO);
		simpleSqlQuery.setAllCountString(countSql);
		simpleSqlQuery.setQueryString(sql);
		simpleSqlQuery.setParam(map.toArray());
		return querySimplePage(simpleSqlQuery);
	}
	
	@Override
	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO, Long shopId, InvoiceSubDto invoiceSubDto) {
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if(AppUtils.isNotBlank(invoiceSubDto.getSubNumber())){
			map.like("subNumber", invoiceSubDto.getSubNumber());
		}
		if(AppUtils.isNotBlank(invoiceSubDto.getHasInvoice())){
			map.put("hasInvoice", invoiceSubDto.getHasInvoice());
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.invoiceSub.queryInvoiceSubCount", map);// 查询订单数量
		String querySQL = ConfigCode.getInstance().getCode("uc.invoiceSub.queryInvoiceSub", map);// 查询订单对象List
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(InvoiceSubDto.class, 10, curPageNO);
		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(map.toArray());
		return querySimplePage(simpleSqlQuery);
	}

	@Override
	public List<Sub> getSubBySubNums(String[] subNums, String userId, Integer orderStatus) {
		QueryMap map = new QueryMap();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < subNums.length; i++) {
			sql.append("?,");
			params.add(subNums[i]);
		}
		sql.setLength(sql.length() - 1);
		map.put("subBySubNums", sql.toString());
		params.add(userId);
		params.add(orderStatus);
		String query = ConfigCode.getInstance().getCode("order.getSubBySubNums", map);
		return query(query, Sub.class, params.toArray());
	}
	
	
	@Override
	public StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId) {
		return get("SELECT * FROM ls_store_prod AS sp WHERE sp.shop_id = ? AND sp.prod_id = ?", StoreProd.class, shopId, prodId);
	}

	@Override
	public List<Sub> findHaveInGroupSub(Long groupId, Integer groupStatus) {
		return queryByProperties(new EntityCriterion().eq("activeId", groupId).eq("groupStatus", groupStatus).eq("subType", SubTypeEnum.group.value()));
	}

	@Override
	public void updateSubGroupStatusByGroup(Long activeId, Integer status) {
		String sql = "UPDATE ls_sub SET group_status = ? WHERE active_id = ? AND sub_type = ?";
		update(sql, status,activeId,SubTypeEnum.group.value());
	}

	@Override
	public void updateMergeGroupAddStatusByAddNumber(String addNumber) {
		String sql ="update ls_sub set merge_group_status=1 where add_number=?";
		this.update(sql, addNumber);
		
	}

	@Override
	public MergeGroupSubDto getMergeGroupSubDtoBySettleSn(String subSettlementSn) {
		String sql = "SELECT s.sub_id as subId,s.sub_number as subNumber,s.prod_name as prodName,s.sub_type as subType,o.id as operateId,s.add_number as addNumber,o.status,o.create_time as createTime,"+
				"g.is_head as isHead,m.count,p.merge_price as mergePrice,k.cn_properties as cnProperties,m.people_number as peopleNumber,m.id as mergeId,k.pic,o.number,m.end_time as endTime  "+
				"FROM ls_sub s LEFT JOIN ls_sub_item i ON s.sub_number=i.sub_number "+
				"LEFT JOIN ls_merge_group_operate o ON s.add_number=o.add_number "+
				"LEFT JOIN ls_merge_group_add g ON g.operate_id=o.id AND s.user_id=g.user_id "+
				"LEFT JOIN ls_merge_group m ON o.merge_id = m.id "+
				"LEFT JOIN ls_merge_product p ON i.sku_id=p.sku_id AND p.merge_id=g.merge_id "+
				"LEFT JOIN ls_sku k ON i.sku_id=k.sku_id WHERE s.sub_settlement_sn=?";
		MergeGroupSubDto mergeGroupSubDto = this.get(sql, MergeGroupSubDto.class, subSettlementSn);
		if(AppUtils.isBlank(mergeGroupSubDto)){
			return null;
		}
		return mergeGroupSubDto;
	}
	
	@Override
	public List<SubDto> exportSubOrder(String string, Object[] array) {
		return this.query(string, SubDto.class, array);
	}

	@Override
	public MySubDto getMySubDtoBySubNumber(String subNumber) {
		String sql = ConfigCode.getInstance().getCode("order.getMySubDtoBySubNumber");
		return get(sql, MySubDto.class, subNumber);
	}

	/* 
	 * 查询退款的总红包金额
	 */
	@Override
	public Double getReturnRedpackOffPrice(Set<String> subNumbers) {
		StringBuilder sql = new StringBuilder("SELECT SUM(ROUND(redpack_off_price*cash/product_total_amout,2)) FROM ls_sub_item WHERE refund_state = 2 AND sub_number in (");
		
		for (int i = 0; i < subNumbers.size() - 1; i++) {
			sql.append("?, ");
		}
		sql.append("? )");
		
		return get(sql.toString(), Double.class,subNumbers.toArray());
	}

	/**
	 * 获取已经支付成功的订单
	 */
	@Override
	public Integer getSeckillSubBuyCount(Long seckillId, Long skuId) {
		String sql = "SELECT SUM(basket_count) FROM ls_sub_item item LEFT JOIN ls_sub sub ON sub.sub_number = item.sub_number WHERE sub.sub_type = 'SECKILL' AND sub.active_id = ? AND item.sku_id = ? AND sub.STATUS <> 1 AND sub.STATUS <> 5";
		return this.get(sql, Integer.class, seckillId, skuId);
	}

	@Override
	public List<Sub> getUnpaymentseckillOrder(Long seckillId) {
		String sql = "SELECT shop_id as shopId,sub_id AS subId,user_name AS userName,user_id AS userId,sub_number AS subNumber,total AS total,actual_total AS actualTotal,province_id as provinceId,active_id AS activeId "
				+ "FROM ls_sub WHERE sub_type = 'SECKILL' AND active_id = ? AND STATUS = 1";
		return this.query(sql, Sub.class, seckillId);
	}

	@Override
	public PageSupport<IncomeTodayDto> getOrders(Long shopId, String curPageNO, int pageSize) {
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(IncomeTodayDto.class, curPageNO);
		simpleSqlQuery.setPageSize(10);

		Date now = new Date();
		Date startDate = DateUtils.getIntegralStartTime(now);
		Date endDate = DateUtils.getIntegralEndTime(now);

		Object[] params = new Object[3];
		params[0] = shopId;
		params[1] = startDate;
		params[2] = endDate;

		String queryAllSQL = ConfigCode.getInstance().getCode("app.shop.queryIncomeTodayCount" );
		String querySQL = ConfigCode.getInstance().getCode("app.shop.queryIncomeTodayList");

		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(params);
		return querySimplePage(simpleSqlQuery);
	}

	@Override
	public PageSupport<VisitLog> getVisitLogs(Long shopId, String curPageNO, int pageSize) {
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(VisitLog.class, curPageNO);
		simpleSqlQuery.setPageSize(10);

		Date now = new Date();
		Date startDate = DateUtils.getIntegralStartTime(now);
		Date endDate = DateUtils.getIntegralEndTime(now);

		Object[] params=new Object[3];
		params[0] = shopId;
		params[1] = startDate;
		params[2] = endDate;

		String queryAllSQL = ConfigCode.getInstance().getCode("app.shop.queryVitLogTodayCount" );
		String querySQL = ConfigCode.getInstance().getCode("app.shop.queryVitLogTodayList");
		simpleSqlQuery.setAllCountString(queryAllSQL);
		simpleSqlQuery.setQueryString(querySQL);
		simpleSqlQuery.setParam(params);
		return querySimplePage(simpleSqlQuery);
	}

	@Override
	public MySubDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("subSettlementSn", subSettlementSn);
		queryMap.put("userId", userId);
		String sql = ConfigCode.getInstance().getCode("order.findSuccessOrderDetail", queryMap);
		return get(sql,MySubDto.class, subSettlementSn, userId);
	}

	@Override
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn, String userId) {
		
		String sql = " SELECT sub.* "
				+ " FROM ls_sub sub INNER JOIN ls_sub_settlement settlement ON sub.sub_settlement_sn = settlement.sub_settlement_sn"
				+ " WHERE  settlement.sub_settlement_sn = ? AND settlement.user_id = ? ";
		
		return this.query(sql, Sub.class, subSettlementSn, userId);
	}

	@Override
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn) {
		String sql = " SELECT sub.* "
				+ " FROM ls_sub sub INNER JOIN ls_sub_settlement settlement ON sub.sub_settlement_sn = settlement.sub_settlement_sn"
				+ " WHERE  settlement.sub_settlement_sn = ? ";
		
		return this.query(sql, Sub.class, subSettlementSn);
	}

	@Override
	public Sub getLastOneSubmitAndUnPaySeckillSub(Long activeId) {
		
		String sql = "SELECT * FROM ls_sub sub WHERE sub.active_id = ? AND status = ? ORDER BY sub.sub_date DESC";
		
		List<Sub> subs = this.queryLimit(sql, Sub.class, 0, 1, activeId, OrderStatusEnum.UNPAY.value());
		if(AppUtils.isBlank(subs)){
			return null;
		}
		
		return subs.get(0);
	}

	@Override
	public Integer getSubCountByOrderNum(String  orderNums) {
		String sql="select count(1) from ls_sub  where status=1 and sub_number=?";
		return get(sql, Integer.class, orderNums);
	}

	/**
	 * 获取团购运营数据
	 */
	@Override
	public PageSupport<Sub> getGroupOperationList(String curPageNO, Integer pageSize, Long groupId, Long shopId) {
		
		CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
		cq.setPageSize(pageSize);
		
		if(AppUtils.isNotBlank(groupId)) {
			cq.eq("activeId", groupId);			
		}
		cq.eq("subType", CartTypeEnum.GROUP.toCode());
		if(AppUtils.isNotBlank(shopId)) {
			cq.like("shopId", shopId);			
		}
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sub> getSeckillOperationList(String curPageNO, int pageSize, Long seckillId, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Sub.class,curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("activeId",seckillId);
		cq.eq("subType", CartTypeEnum.SECKILL.toCode());
		cq.eq("shopId",shopId);
		return queryPage(cq);
	}

	@Override
	public PageSupport<Sub> getPresellOperationList(String curPageNO, int size, Long id, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Sub.class,curPageNO);
		cq.setPageSize(size);
		cq.eq("activeId",id);
		cq.eq("subType", CartTypeEnum.PRESELL.toCode());
		cq.eq("shopId",shopId);
		return queryPage(cq);
	}
}
