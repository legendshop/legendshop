/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdUserAttributeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdUserAttribute;

import java.util.List;

/**
 * The Class ProdUserAttributeDaoImpl.
 */
@Repository
public class ProdUserAttributeDaoImpl extends GenericDaoImpl<ProdUserAttribute, Long> implements ProdUserAttributeDao  {

	public ProdUserAttribute getProdUserAttribute(Long id){
		return getById(id);
	}
	
    public void deleteProdUserAttribute(Long id, Long shopId){
    	 update("delete from ls_prod_user_attr where id = ? and shop_id = ?",id,shopId);
    }
	
	public Long saveProdUserAttribute(ProdUserAttribute prodUserAttribute){
		return save(prodUserAttribute);
	}
	
	public int updateProdUserAttribute(ProdUserAttribute prodUserAttribute){
		return update(prodUserAttribute);
	}

	@Override
	public PageSupport<ProdUserAttribute> getProdUserAttributePage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProdUserAttribute.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId",shopId);
		return queryPage(cq);
	}

	@Override
	public PageSupport<ProdUserAttribute> getProdUserAttributeByPage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProdUserAttribute.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId",shopId);
		cq.addOrder("desc", "recDate");
		return queryPage(cq);
	}

	@Override
	public List<ProdUserAttribute> getShopProdAttribute(Long shopId) {
		return  query("select from ls_prod_user_attr where shop_id = ?",ProdUserAttribute.class,shopId);
	}
	
 }
