/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ThemeDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ThemeStatusEnum;
import com.legendshop.model.entity.Theme;
import com.legendshop.util.AppUtils;

/**
 * 专题Dao实现类.
 */
@Repository
public class ThemeDaoImpl extends GenericDaoImpl<Theme, Long> implements ThemeDao  {

	private final static String HAS_TOPICSSQL_STRING="SELECT COUNT(*) FROM ls_theme LEFT JOIN ls_theme_module ON ls_theme_module.theme_id=ls_theme.theme_id " +
			" LEFT JOIN ls_theme_related ON ls_theme_related.theme_id=ls_theme.theme_id" +
			" WHERE ls_theme.theme_id=?";

	@Override
	public Theme getTheme(Long id){
		return getById(id);
	}
	
    @Override
    //@CacheEvict(value="ThemePageList",allEntries=true)
	public int deleteTheme(Theme theme){
    	return delete(theme);
    }
	
	@Override
	public Long saveTheme(Theme theme){
		return save(theme);
	}
	
	@Override
	//@CacheEvict(value="ThemeVo",key="'ThemeVo'+#themeId")
	public void clearThemeDetailCache(Long themeId){
		System.out.println("clear clearThemeDetailCache success");
	}
	
	@Override
	//@CacheEvict(value="ThemePageList",allEntries=true)
	public int updateTheme(Theme theme){
		return update(theme);
	}

	@Override
	public boolean hasThemes(Long id) {
		int result=jdbcTemplate.queryForObject(HAS_TOPICSSQL_STRING, new Object[]{id}, Integer.class);
		return result > 0;
	}
	
	/**
	 * 前台专题列表
	 */
	@Override
	//@Cacheable(value = "ThemePageList",condition = "T(Integer).parseInt(#curPageNO) < 3")
	public PageSupport getThemePage(String curPageNO,Integer pageSize){
		String countSql="select count(1) from ls_theme theme where theme.status =1 ";
		String querysql = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".getThemeList");
		SimpleSqlQuery simpleSqlQuery = new SimpleSqlQuery(Theme.class, pageSize, curPageNO);
		simpleSqlQuery.setAllCountString(countSql);
		simpleSqlQuery.setQueryString(querysql);
		return super.querySimplePage(simpleSqlQuery);
	}

	@Override
	public List<Theme> getTheme() {
		return this.queryByProperties(new EntityCriterion().eq("status", 1).gt("endTime", new Date()).addAscOrder("endTime"));
	}

	@Override
	public PageSupport<Theme> getThemePage(String curPageNO, String name) {
		CriteriaQuery cq = new CriteriaQuery(Theme.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("status", ProductStatusEnum.PROD_ONLINE.value());
		cq.gt("endTime", new Date());
		if(AppUtils.isNotBlank(name)){
			cq.like("title",name,MatchMode.ANYWHERE);
		}
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<Theme> allThemes(String curPageNO) {
		CriteriaQuery cq=new CriteriaQuery(Theme.class,curPageNO);
		cq.setPageSize(10);
		cq.eq("status", ThemeStatusEnum.NORMAL.value());
		cq.gt("endTime", new Date());
		cq.addDescOrder("startTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Theme> getThemePage(String curPageNO) {
		
		curPageNO = curPageNO == null ? "1":curPageNO;
		CriteriaQuery cq=new CriteriaQuery(Theme.class,curPageNO);
		cq.setPageSize(8);
		cq.eq("status", ThemeStatusEnum.NORMAL.value());
		cq.gt("endTime", new Date());
		cq.addDescOrder("startTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Theme> getThemePage(String curPageNO, Theme entity, DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(Theme.class, curPageNO);
		cq.setPageSize(10);
		Date newDate = new Date();
		Integer status = entity.getStatus() == null ? 1 : entity.getStatus();
		if (status.intValue() == ThemeStatusEnum.OFFLINE.value().intValue()) { // 下线
			cq.eq("status", ThemeStatusEnum.OFFLINE.value());
			cq.gt("endTime", newDate);
		} else if (status.intValue() == ThemeStatusEnum.NORMAL.value().intValue()) { // 正常
			cq.eq("status", ThemeStatusEnum.NORMAL.value());
			cq.gt("endTime", newDate);
		} else if (status == ThemeStatusEnum.DEL.value().intValue()) {// 删除
			cq.eq("status", ThemeStatusEnum.DEL.value());
		} else { // 过期状态
			cq.lt("endTime", newDate);
			cq.notEq("status", ThemeStatusEnum.DEL.value());
		}
		if (!result.isSortExternal()) {
			cq.addOrder("desc", "createTime");
		}else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		return queryPage(cq);
	}

	@Override
	public PageSupport<Theme> getThemeQueryContent(String curPageNO,Integer status, DataSortResult result, String title) {
		CriteriaQuery cq = new CriteriaQuery(Theme.class, curPageNO);
		cq.setPageSize(10);
		Date newDate = new Date();
		if (status.intValue() == ThemeStatusEnum.OFFLINE.value().intValue()) { // 下线
			cq.eq("status", ThemeStatusEnum.OFFLINE.value());
			cq.gt("endTime", newDate);
		} else if (status.intValue() == ThemeStatusEnum.NORMAL.value().intValue()) { // 正常
			cq.eq("status", ThemeStatusEnum.NORMAL.value());
			cq.gt("endTime", newDate);
		} else if (status == ThemeStatusEnum.DEL.value().intValue()) {// 删除
			cq.eq("status", ThemeStatusEnum.DEL.value());
		} else { // 过期状态
			cq.lt("endTime", newDate);
			cq.notEq("status", ThemeStatusEnum.DEL.value());
		}
		if (!result.isSortExternal()) {
			cq.addOrder("desc", "createTime");
		}

		if (AppUtils.isNotBlank(title)) {
			cq.like("title", "%" + title.trim() + "%");
		}
		cq.addDescOrder("updateTime");
		cq.addDescOrder("endTime");
		cq.addDescOrder("createTime");
		return queryPage(cq);
	}

    @Override
    public PageSupport<Theme> getSelectTheme(String curPageNO, String title,Long themeId) {
		if (AppUtils.isNotBlank(title)){
			title = "%"+title+"%";
		}
		curPageNO = curPageNO == null ? "1":curPageNO;
		CriteriaQuery cq=new CriteriaQuery(Theme.class,curPageNO);
		cq.setPageSize(8);
		cq.notEq("themeId",themeId);
		cq.like("title",title);
		cq.eq("status", ThemeStatusEnum.NORMAL.value());
		cq.gt("endTime", new Date());
		cq.addDescOrder("startTime");
		return queryPage(cq);
    }

}
