/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopPermDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.entity.ShopPerm;
import com.legendshop.model.entity.ShopPermId;

/**
 *商家权限服务
 */
@Repository
public class ShopPermDaoImpl  implements ShopPermDao  {
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	public ShopPerm getShopPerm(Long id){
		return null;
	}
	
    public int deleteShopPerm(ShopPerm shopPerm){
    	return 0;
    }
	
	public Long saveShopPerm(ShopPerm shopPerm){
		return null;
	}
	
	public int updateShopPerm(ShopPerm shopPerm){
		return 0;
	}

	@Override
	public void saveShopPerm(List<ShopPerm> shopPermList) {
		if(shopPermList == null || shopPermList.size() == 0){
			return;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (ShopPerm sp : shopPermList) {
			Object[] param = new Object[3];
			param[0] = sp.getId().getRoleId();
			param[1] = sp.getId().getLabel();
			param[2] = sp.getId().getFunction();
			batchArgs.add(param);
		}
		 genericJdbcDao.batchUpdate("insert into ls_shop_perm(role_id,label,function)  values (?,?,?)", batchArgs);
	}
	
	@Override
	public void deleteUserRole(List<ShopPerm> shopPermList) {
		if(shopPermList == null || shopPermList.size() == 0){
			return;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (ShopPerm sp : shopPermList) {
			Object[] param = new Object[2];
			param[0] = sp.getId().getRoleId();
			param[1] = sp.getId().getLabel();
			batchArgs.add(param);
		}
		 genericJdbcDao.batchUpdate("delete  from ls_shop_perm  where role_id = ? and label = ? ", batchArgs);
	}
	
	@Override
	public List<String> getShopPermsByRoleId(Long shopRoleId) {
		return genericJdbcDao.query("select label from ls_shop_perm where role_id = ?", String.class, shopRoleId);
	}

	public void setGenericJdbcDao(GenericJdbcDao genericJdbcDao) {
		this.genericJdbcDao = genericJdbcDao;
	}

	/**
	 * 根据职位id删除相关联的职位权限 
	 */
	@Override
	public void deleteShopPermByRoleId(Long roleId) {

		String sql = "DELETE FROM ls_shop_perm WHERE role_id = ?";
		genericJdbcDao.update(sql, roleId);
		
	}

	/**
	 * 根据职位Id和菜单标识获取权限
	 */
	@Override
	public String getFunction(Long shopRoleId, String label) {
		
		String sql = "SELECT function FROM ls_shop_perm WHERE role_id = ? AND label = ?";
		return genericJdbcDao.get(sql, String.class, shopRoleId,label);
	}

	/**
	 * 根据职位id获取所有权限
	 */
	@Override
	public List<ShopPermId> queryShopPermsByRoleId(Long shopRoleId) {
		
		String sql = "SELECT * FROM ls_shop_perm WHERE role_id = ?";
		return genericJdbcDao.query(sql, ShopPermId.class, shopRoleId);
	}

	/**
	 * 根据职位ID与菜单名称更新子权限
	 */
	@Override
	public void updateFunction(String function, Long shopRoleId, String menuLable) {
		
		String sql = "UPDATE ls_shop_perm SET function = ? WHERE role_id = ? AND label= ? ";
		genericJdbcDao.update(sql, function,shopRoleId,menuLable);
	}
 }
