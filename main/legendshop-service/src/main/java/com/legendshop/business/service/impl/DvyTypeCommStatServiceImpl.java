/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DvyTypeCommStatDao;
import com.legendshop.business.dao.ShopCommStatDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ShopCommStat;
import com.legendshop.spi.service.DvyTypeCommStatService;
import com.legendshop.util.AppUtils;

/**
 * 物流评分统计表服务.
 */
@Service("dvyTypeCommStatService")
public class DvyTypeCommStatServiceImpl  implements DvyTypeCommStatService{

	@Autowired
    private DvyTypeCommStatDao dvyTypeCommStatDao;

    public DvyTypeCommStat getDvyTypeCommStat(Long id) {
        return dvyTypeCommStatDao.getDvyTypeCommStat(id);
    }

    @Override
   	public DvyTypeCommStat getDvyTypeCommStatByShopId(Long shopId) {
   		return dvyTypeCommStatDao.getDvyTypeCommStatByShopId(shopId);
   	}

    
    public void deleteDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat) {
        dvyTypeCommStatDao.deleteDvyTypeCommStat(dvyTypeCommStat);
    }

    public Long saveDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat) {
        if (!AppUtils.isBlank(dvyTypeCommStat.getId())) {
            updateDvyTypeCommStat(dvyTypeCommStat);
            return dvyTypeCommStat.getId();
        }
        return dvyTypeCommStatDao.saveDvyTypeCommStat(dvyTypeCommStat);
    }

    public void updateDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat) {
        dvyTypeCommStatDao.updateDvyTypeCommStat(dvyTypeCommStat);
    }

    public PageSupport getDvyTypeCommStat(CriteriaQuery cq) {
        return dvyTypeCommStatDao.getDvyTypeCommStat(cq);
    }
}
