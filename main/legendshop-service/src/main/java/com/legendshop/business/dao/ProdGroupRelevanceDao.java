/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdGroupRelevance;

/**
 * The Class ProdGroupRelevanceDao. Dao接口
 */
public interface ProdGroupRelevanceDao extends Dao<ProdGroupRelevance,Long>{

	/**
	 * 根据Id获取
	 */
	public abstract ProdGroupRelevance getProdGroupRelevance(Long id);

	/**
	 *  根据Id删除
	 */
    public abstract int deleteProdGroupRelevance(Long id);
    
	/**
	 *  根据prodId删除
	 */
    public abstract int deleteProdGroupRelevanceByprod(Long id);
	/**
	 *  根据对象删除
	 */
    public abstract int deleteProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);

	/**
	 * 保存
	 */
	public abstract Long saveProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);

	/**
	 *  更新
	 */		
	public abstract int updateProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);

	/**
	 * 分页查询列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<ProdGroupRelevance> queryProdGroupRelevance(String curPageNO, Integer pageSize);

    int getProductByGroupId(Long prod, Long groupId);

	/**
	 * 移除分组内商品
	 * @param prodId
	 * @param groupId
	 */
	void removeProd(Long prodId, Long groupId);
}
