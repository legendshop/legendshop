/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MessageDao;
import com.legendshop.model.dto.AdminMessageDto;
import com.legendshop.model.dto.AdminMessageDtoList;
import com.legendshop.model.dto.AdminMessageEnum;
import com.legendshop.spi.service.AdminMessageService;

/**
 * 后台消息获取实现类.
 */
@Service("adminMessageService")
public class AdminMessageServiceImpl implements AdminMessageService {

	@Autowired
	private MessageDao messageDao;
	
	/** 
	 * 获取后台待处理消息
	 */
	@Override
//	@Cacheable(value = "AdminMessage", key = "")
	public AdminMessageDtoList getMessage() {
		
		Integer auditShopNumber = messageDao.getAuditShop();
		List<AdminMessageDto> adminMessageList = new ArrayList<AdminMessageDto>();
		AdminMessageDto adminMessageDto = new AdminMessageDto();
		adminMessageDto.setName(AdminMessageEnum.AUDITUING_SHOP.name());
		adminMessageDto.setUrl(AdminMessageEnum.AUDITUING_SHOP.value());
		adminMessageDto.setNum(auditShopNumber);
		
		adminMessageList.add(adminMessageDto);
		AdminMessageDtoList adminMessageDtoList = new AdminMessageDtoList();
		adminMessageDtoList.setList(adminMessageList);
		adminMessageDtoList.setTotalMessage(auditShopNumber);
		return adminMessageDtoList;
	}

}
