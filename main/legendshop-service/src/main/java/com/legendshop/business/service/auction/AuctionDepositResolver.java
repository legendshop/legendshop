package com.legendshop.business.service.auction;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.CashLog;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.business.dao.auction.AuctionsDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.model.vo.FreezePredeposit;
import com.legendshop.spi.service.IPaymentResolver;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 拍卖保证金支付
 *
 */
@Service("auctionDepositResolver")
public class AuctionDepositResolver implements IPaymentResolver {
	
	@Autowired
	private AuctionDepositRecDao auctionDepositRecDao;
	
	@Autowired
	private AuctionsDao auctionsDao;
	
	@Autowired
	private SubSettlementDao subSettlementDao;
	
	@Autowired
	private SubSettlementItemDao subSettlementItemDao;
	
	@Autowired
	private ExpensesRecordDao expensesRecordDao;
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private PdCashLogDao pdCashLogDao;
	
	@Resource(name="predepositPaySuccessProcessor")
	private EventProcessor<PredepositPaySuccess> predepositPaySuccessProcessor;
	
	@Resource(name="freezePredepositProcessor")
	private EventProcessor<FreezePredeposit> freezePredepositProcessor;
	
	
	@Override
	public Map<String, Object> queryPayto(Map<String, String> params) {
		
		String userId = params.get("userId");
		String subNumber = params.get("subNumber").toString();
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("result", false);
		
		AuctionDepositRec depositRec=auctionDepositRecDao.getAuctionDepositRec(subNumber, userId);
		if(AppUtils.isBlank(depositRec)){
			response.put("message","找不到拍卖的缴纳保证金记录");
			return response;
		}
		
		if(depositRec.getOrderStatus()==1){
			response.put("message","您已支付活动保证金，请勿重复支付");
			return response;
		}
		
		Auctions auction = auctionsDao.getAuctions(depositRec.getAId());
		
		if (auction == null || auction.getStatus().intValue() == 0 || auction.getStatus().intValue() == -2) { // 下线或者审核中
			response.put("message","该活动已删除或者不存在");
			return response;
		}
		
		if (auction.getIsSecurity().intValue() == 0 && auction.getSecurityPrice().doubleValue() > 0) { 
			response.put("message","该活动不需要支付保证金");
			return response;
		}
		
		if (auctionDepositRecDao.isPaySecurity(userId, depositRec.getAId())) {
			response.put("message","您已支付活动保证金，请勿重复支付");
			return response;
		}
		
		double amount = depositRec.getPayMoney().doubleValue();
		String subject= "拍卖活动 "+auction.getAuctionsTitle()+" 订金支付;";
		response.put("result", true);
		response.put("subject",subject);
		response.put("totalAmount",amount);
		return response;
	}
	
	

	@Override
	public void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallback){
		if (subSettlement.getIsClearing()) { // 没有清算
			return;
		}

		if (OrderStatusEnum.UNPAY.value().equals(bankCallback.getValidateStatus().value())) {
			// 没有付款
			PaymentLog.info("###################### doBankCallback subSettlementSn={} 开始处理 拍卖订单支付业务 ########################",
					subSettlement.getSubSettlementSn());
			Date now = new Date();
			subSettlement.setFlowTradeNo(bankCallback.getFlowTradeNo());
			subSettlement.setClearingTime(now);
			subSettlement.setIsClearing(true);
			int settlementResult = subSettlementDao.updateSubSettlementForPay(subSettlement);

			if (settlementResult == 0) {
				PaymentLog.info("Settlement record has settled。 TradeNo is  {}", bankCallback.getFlowTradeNo());
				return;
			}

			List<SubSettlementItem> subSettlementItems = subSettlementItemDao.getSubSettlementItems(subSettlement.getSubSettlementSn());
			if (subSettlementItems == null) {
				PaymentLog.error("支付异常,订单单据缺失, doBankCallback error subSettlementItems is null by subSettlementSn={}",
						subSettlement.getSubSettlementSn());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}

			SubSettlementItem settlementItem = subSettlementItems.get(0);
			
			AuctionDepositRec depositRec=auctionDepositRecDao.getAuctionDepositRec(settlementItem.getSubNumber(), subSettlement.getUserId());
			if(AppUtils.isBlank(depositRec)){
				PaymentLog.error("支付异常,订单单据缺失, doBankCallback error AuctionDepositRec is null by subSettlementSn={}",
						subSettlement.getSubSettlementSn());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}
			
			/*
			 * 判断是否使用全款预付款支付
			 */
			if (subSettlement.isFullPay()) {
				PaymentLog.info("############################## 使用全部的钱包支付 全部金额={}  ###########################", subSettlement.getPdAmount());
				//PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(),subSettlement.getPdAmount(), subSettlement.getUserId(), subSettlement.getPrePayType());
				
				//EventHome.publishEvent(new PredepositPaySuccessEvent(paySuccess));
				
				String result = null;

				UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(subSettlement.getUserId());
				if (AppUtils.isBlank(userDetail)) {
					CashLog.log(" Can't find userDetai by sn= {},  userId= {}  ------- !> ", subSettlement.getSubSettlementSn(), subSettlement.getUserId());
					throw new BusinessException("支付失败，请联系管理员！  Can't find userDetai by userId= " + subSettlement.getUserId());
				}
				/*
				 * 判断是否余额金额
				 */
				Double availablePredeposit = userDetail.getAvailablePredeposit();

				PaymentLog.log("param availablePredeposit= {},pay amount={}------- !> ", availablePredeposit, subSettlement.getPdAmount());

				if (availablePredeposit <= 0 || availablePredeposit < subSettlement.getPdAmount()) { // 可用金额为0
					PaymentLog.log(" User cash insufficient amount by sn= {}, userId = {}  ------- !>  ", subSettlement.getSubSettlementSn(), subSettlement.getUserId());
					throw new BusinessException("支付失败， 账户余额不足 ");
				}
				
				//冻结金额
				FreezePredeposit predeposit = new FreezePredeposit();
				predeposit.setUserId(subSettlement.getUserId());
				predeposit.setAmount(subSettlement.getPdAmount());
				predeposit.setSn(depositRec.getSubNumber());
				freezePredepositProcessor.process(predeposit);
				result = PreDepositErrorEnum.OK.value();
				
				if (!PreDepositErrorEnum.OK.value().equals(result)) {
					PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}

			} else if (subSettlement.getPdAmount() > 0) { // 部分使用钱包
				PaymentLog.info("############################## 使用部分的钱包支付  部分金额={} ###########################",
						subSettlement.getPdAmount());

				PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(),
						subSettlement.getPdAmount(), subSettlement.getUserId(), subSettlement.getPrePayType());

				predepositPaySuccessProcessor.process(paySuccess);

				String result = paySuccess.getResult();
				if (!PreDepositErrorEnum.OK.value().equals(result)) {
					PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
			}
			
			
			depositRec.setPayName(subSettlement.getPayTypeName());
			depositRec.setPayType(subSettlement.getPayTypeId());
			depositRec.setPayTime(now);
			depositRec.setSubSettlementSn(subSettlement.getSubSettlementSn());
			depositRec.setOrderStatus(1);
			depositRec.setFlagStatus(0l);
			auctionDepositRecDao.update(depositRec);
			
			//获取用户信息
			UserDetail userDetail = userDetailDao.getUserDetailById(subSettlement.getUserId());
			if ("FULL_PAY".equals(subSettlement.getPayTypeId())){
				PdCashLog pdCashLog = new PdCashLog();
				pdCashLog.setUserId(userDetail.getUserId());
				pdCashLog.setUserName(userDetail.getUserName());
				pdCashLog.setLogType(PdCashLogEnum.AUCTION_DEDUCT.value());
				pdCashLog.setAmount(-subSettlement.getPdAmount());
				pdCashLog.setAddTime(new Date());
				StringBuilder sb = new StringBuilder();
				sb.append("拍卖活动订金支付:").append(subSettlement.getPdAmount());
				pdCashLog.setLogDesc(sb.toString());
				pdCashLog.setSn(depositRec.getSubNumber());
				pdCashLogDao.savePdCashLog(pdCashLog);
			}

			// 异步处理
			ExpensesRecord expensesRecord = new ExpensesRecord();
			expensesRecord.setRecordDate(now);
			expensesRecord.setRecordMoney(-Arith.add(subSettlement.getCashAmount(), subSettlement.getPdAmount()));
			expensesRecord.setRecordRemark("拍卖活动订金支付,流水号:"+depositRec.getSubNumber());
			expensesRecord.setUserId(depositRec.getUserId());
			expensesRecordDao.saveExpensesRecord(expensesRecord);

			
			PaymentLog.info(
					"###################### doBankCallback subSettlementSn={} 拍卖订单处理业务成功  ########################",
					subSettlement.getSubSettlementSn());
		}

	}
}
