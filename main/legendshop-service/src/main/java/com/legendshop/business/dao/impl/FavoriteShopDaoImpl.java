/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.FavoriteShopDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.FavoriteShop;

/**
 * 店铺收藏Dao.
 */
@Repository
public class FavoriteShopDaoImpl extends GenericDaoImpl<FavoriteShop, Long> implements FavoriteShopDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public FavoriteShop getfavoriteShop(Long id){
		return getById(id);
	}
	
    public void deletefavoriteShop(FavoriteShop favoriteShop){
    	delete(favoriteShop);
    }
	
	public Long savefavoriteShop(FavoriteShop favoriteShop){
		return (Long)save(favoriteShop);
	}
	
	public void updatefavoriteShop(FavoriteShop favoriteShop){
		 update(favoriteShop);
	}

	public boolean deletefavoriteShop(Long id, String userId) {
		return update("delete from ls_favorite_shop where user_id = ? and fs_id = ?  ",userId, id) > 0;
	}

	public boolean isExistsFavoriteShop(String userId,Long shopId) {
		Long counts = getLongResult("SELECT COUNT(lfs.fs_id) FROM ls_favorite_shop lfs WHERE lfs.user_id = ? AND lfs.shop_id = ?", userId,
				shopId);
		return counts > 0;
	}

	public Long getShopId(String shopName) {
		return get("select shop_id from ls_shop_detail where user_name = ?", Long.class,shopName);
	}

	public void deletefavoriteShop(Long id) {
		deleteById(id);
	}

	public void deletefavoriteShop(String userId, List<Long> idList) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(idList.size());
		for (Long id : idList) {
			Object[] param = new Object[2];
			param[0] = userId;
			param[1] = id;
			batchArgs.add(param);
		}
		this.batchUpdate("delete from ls_favorite_shop where user_id = ? and fs_id = ?", batchArgs);
		
	}

	@Override
	public void deleteAllfavoriteShop(String userId) {
		String sql = "delete from ls_favorite_shop where user_id = ?";
		this.update(sql, userId);
	}
	
	@Override
	public int deletefavoriteShopByShopIdAndUserId(Long shopId, String userId) {
		return this.update("DELETE FROM ls_favorite_shop WHERE shop_id = ? AND user_id = ?", shopId, userId);
	}
	
	@Override
	public Long getfavoriteShopLength(String userId) {
		return this.get("select count(*) from ls_favorite_shop lsf,ls_shop_detail lsd,ls_shop_grad lsg where lsf.shop_id = lsd.shop_id and lsd.grade_id = lsg.grade_id  and lsf.user_id = ? ", Long.class, userId);
	}
	
	@Override
	public PageSupport<FavoriteShop> collectShop(String curPageNO,String userId) {
	 	SimpleSqlQuery query = new SimpleSqlQuery(FavoriteShop.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		 String queryAllSQL = ConfigCode.getInstance().getCode("biz.queryFavouriteShopCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("biz.queryFavouriteShop", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<FavoriteShop> querySimplePage(String curPageNO, String userId) {
		SimpleSqlQuery query = new SimpleSqlQuery(FavoriteShop.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.favourites.queryFavouriteShopCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.favourites.queryFavouriteShop", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public void deletefavoriteShops(String[] fsIds, String userId) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(fsIds.length);
		for (String fsId : fsIds) {
			Object[] param = new Object[2];
			param[0] = fsId;
			param[1] = userId;
			batchArgs.add(param);
		}
		this.batchUpdate("DELETE FROM ls_favorite_shop WHERE fs_id = ? AND user_id = ?", batchArgs);
	}

	@Override
	public Long getfavoriteLength(String userId) {
		Long counts = getLongResult("select count(*) from ls_favorite_shop where user_id = ?", userId);
		return counts;
	}

	@Override
	public PageSupport<FavoriteShop> queryFavoriteShop(String curPageNO, Integer pageSize, String userId) {

		pageSize = pageSize == null? 10 : pageSize;
		curPageNO = curPageNO == null? "1" : curPageNO;
		
		SimpleSqlQuery query = new SimpleSqlQuery(FavoriteShop.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userId", userId);
		String queryAllSQL = ConfigCode.getInstance().getCode("app.queryFavouriteShopCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.queryFavouriteShop", map); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
 }
