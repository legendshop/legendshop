package com.legendshop.business.order.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.legendshop.model.constant.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotStocksException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.InvoiceDao;
import com.legendshop.business.dao.InvoiceSubDao;
import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.TransportDao;
import com.legendshop.business.dao.UserAddressDao;
import com.legendshop.business.dao.UserAddressSubDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.StoreCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.Transport;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.StringUtil;

/**
 *
 * 订单抽象
 *
 */
@Component
public abstract class AbstractAddOrder {

	@Autowired
	private UserCouponDao userCouponDao;

	@Autowired
	private CouponDao couponDao;

	@Autowired
	protected UserAddressSubDao userAddressSubDao;

	@Autowired
	protected GroupService groupService;

	@Autowired
	protected InvoiceDao invoiceDao;

	@Autowired
	protected InvoiceSubDao invoiceSubDao;

	@Autowired
	protected SubService subService;

	@Autowired
	protected SubItemDao subItemDao;

	@Autowired
	protected BasketService basketService;

	@Autowired
	private UserAddressDao userAddressDao;

	@Autowired
	private TransportDao transportDao;

	@Autowired
	private StockService stockService;

	@Autowired
	private TransportManagerService transportManagerService;

	@Autowired
	private SubHistoryDao subHistoryDao;

	@Autowired
	private ProductService productService;

	/**
	 * 基类查询下单列表
	 * @param userId 用户ID
	 * @param userName 用户名称
	 * @param shopCartItems 购物车商品信息
	 * @return
	 */
	public UserShopCartList queryOrders(String userId, String userName, List<ShopCartItem> shopCartItems, Long adderessId,String subType) {

		//取用户的默认地址
		UserAddress defaultAddress = null;
		if(AppUtils.isNotBlank(adderessId)){
			defaultAddress = userAddressDao.getUserAddress(adderessId);
		}else{
			defaultAddress = userAddressDao.getDefaultAddress(userId);
			if(null == defaultAddress){
				defaultAddress = userAddressDao.getFirstAddress(userId);
			}
		}

		Integer userCityId = null;
		if(AppUtils.isNotBlank(defaultAddress)){
			userCityId =defaultAddress.getCityId();
		}

		//需要改造 TODO 要一起拿到数据再统计
		for (int i = 0; i < shopCartItems.size(); i++) {

			ShopCartItem cartItem=shopCartItems.get(i);
			Long transportId = cartItem.getTransportId();
			Long shopId=cartItem.getShopId();

			/*  获取商品运费信息  */
			if (SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(cartItem.getTransportFree())//买家承担运费
					&& TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(cartItem.getTransportType())) {//采用运费模板
				if (transportId != null) {

					//加载运费模板
					Transport transport = transportDao.getDetailedTransport(cartItem.getShopId(),transportId);
					cartItem.setTransport(transport);
				} else {
					cartItem.setTransport(null);
				}
				cartItem.setTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value()); //设置买家承担运费
				cartItem.setTransportType(TransportTypeEnum.TRANSPORT_TEMPLE.value()); // 采用运费模版
			}

			/**
			 * 获取仓库库存
			 * stocks
			 **/
			Map<String,String> map = stockService.calculateSkuStocksByCity(cartItem.getProdId(),cartItem.getSkuId(), userCityId,transportId,shopId);
			if(map != null){
				Long stocks = Long.valueOf(map.get("stocks"));
				if(stocks!=null){
					String skuType = map.get("skuType");
					// 如果普通订单商品参与预售活动，就直接将仓库置为0，因为目前预售和普通商品是共用库存 update by 2020-07-30 jzh
					if (AppUtils.isNotBlank(skuType) && CartTypeEnum.NORMAL.toCode().equals(subType) && SkuActiveTypeEnum.PRESELL.value().equals(skuType)) {
						cartItem.setStocks(0);
					} else {
						cartItem.setStocks(stocks.intValue());
					}
				}
			}else{
				cartItem.setStocks(0);
			}
		}

		//购物车根据来源分组,按照商城来分组
		UserShopCartList userShopCartList= basketService.getShoppingCarts(shopCartItems);
		userShopCartList.setUserAddress(defaultAddress);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);

		//计算运费，门店订单无需计算运费
		if (!CartTypeEnum.SHOP_STORE.toCode().equals(subType)) {
			transportManagerService.clacCartDeleivery(userShopCartList, false);
		}

//		//重新计算商品价格，加上运费
//		Double orderActualTotal= Arith.add(userShopCartList.getOrderActualTotal(), userShopCartList.getOrderCurrentFreightAmount());
//		userShopCartList.setOrderFreightAmount(userShopCartList.getOrderCurrentFreightAmount());
//		userShopCartList.setOrderActualTotal(orderActualTotal);

		return userShopCartList;
	}

	/**
	 * 普通商品下单
	 * @param userShopCartList
	 * @return 订单号 subIds
	 */
	public List<String> submitOrder(UserShopCartList userShopCartList) {
		String userId=userShopCartList.getUserId();
		String userName=userShopCartList.getUserName();
		/**保存订单用户地址信息 **/
		UserAddressSub userAddressSub=saveUserAddressSub(userShopCartList.getUserAddress());
		 /**根据发票ID查找发票内容 **/
		Long invoiceSubId=0l;
		if (AppUtils.isNotBlank(userShopCartList.getInvoiceId())&& userShopCartList.getInvoiceId() > 0) {
			 invoiceSubId=saveUserInvoice(userShopCartList.getInvoiceId(), userName);
		}
		List<ShopCarts> shopCarts=userShopCartList.getShopCarts();
		List<String> subIds=new ArrayList<String>();
		Date date=new Date();
		String orderNumbers = "";
		Long conId=0l;
		double price=0;
		List<String> orderList = new ArrayList<String>();
		for (Iterator<ShopCarts> shopIterator = shopCarts.iterator(); shopIterator.hasNext();) {
			 // 是在同一个商城下，每一个商城生成一个订单
		     ShopCarts shopCart = (ShopCarts) shopIterator.next();

		     //判断商家 是否下线
		     int shopSts = shopCart.getStatus();
		     if(shopSts!=1){//1代表上线
		    	 throw new BusinessException("店铺["+shopCart.getShopName()+"]已下线", ErrorCodes.BUSINESS_ERROR);
		     }

	         List<ShopCartItem> items = shopCart.getCartItems();
		     List<SubItem> subItems = new ArrayList<SubItem>();
		     String subNember = CommonServiceUtil.getSubNember(userShopCartList.getUserName());
		     StringBuilder prodName=new StringBuilder(100);
		     int j=0;
		     double distCommisAmount = 0;//分销总金额
			 Double shopTotalCash = 0d; // 商家的商品金额
			 Double shopActualTotalCash = 0d; // 商家的商品金额
			 Double shopDiscountPrice = 0d;

		     for (Iterator<ShopCartItem> iterator = items.iterator(); iterator.hasNext();) {
		    	 ShopCartItem basket = (ShopCartItem) iterator.next();

		    	 //已下架的商品不能购买
		    	 ProductDetail prod = productService.getProdDetail(basket.getProdId());
		    	 if(prod.getStatus() != ProductStatusEnum.PROD_ONLINE.value()){
		    		 PaymentLog.error("下单商品已下架 , 商品Id是: {}, 单品Id是: {}", basket.getProdId(), basket.getSkuId());
		    		 throw new BusinessException(basket.getProdName()+"该商品已下架，请重新选择商品", ErrorCodes.BUSINESS_ERROR);
		    	 }

		    	 /** 组装SubItem */
				SubItem subItem = new SubItem();
				subItem.setSubNumber(subNember);
				subItem.setSubItemNumber(subNember + "-" + (++j));
				subItem.setProdId(basket.getProdId());
				subItem.setSkuId(basket.getSkuId());
				subItem.setSnapshotId(null);
				subItem.setBasketCount(basket.getBasketCount());
				subItem.setProdName(basket.getProdName());
				subItem.setAttribute(basket.getCnProperties());
				subItem.setUserId(userId);
				subItem.setPic(basket.getPic());
				subItem.setPrice(basket.getPrice());
				subItem.setCash(basket.getPromotionPrice()); //促销单价 如果没有参加过限时折扣 则为原价
				subItem.setProductTotalAmout(basket.getTotal());
				subItem.setObtainIntegral(0);
				subItem.setSubItemDate(date);
				subItem.setWeight(basket.getWeight());
				subItem.setVolume(basket.getVolume());
				if(AppUtils.isNotBlank(basket.getCartMarketRules())){
					StringBuilder builder=new StringBuilder(100);
					for (CartMarketRules rules:basket.getCartMarketRules()) {
						builder.append(rules.getPromotionInfo()).append(",");
					}
					subItem.setPromotionInfo(builder.toString());
				}
				subItem.setCommSts(0);//是否评论
				subItem.setCommisSettleSts(0); //是否结算，没有结算
				subItem.setHasDist(0);//是否有分佣金
				subItem.setRefundId(0l); //退换货ID
				subItem.setRefundState(0);//退换货状态
				subItem.setRefundAmount(null);//退钱的数目
				subItem.setStockCounting(basket.getStockCounting());

				subItem.setDiscountedPrice(basket.getDiscountedPrice());
				subItem.setCouponOffPrice(basket.getCouponOffPrice());
				subItem.setRedpackOffPrice(basket.getRedpackOffPrice());
				//订单项的实际金额
				subItem.setActualAmount(Arith.sub(basket.getDiscountedPrice(), Arith.add(basket.getCouponOffPrice(), basket.getRedpackOffPrice())));

				 //判断是否区域限售
				 Boolean regionalSales = stockService.isRegionalSales(subItem.getProdId(), subItem.getSkuId(), userShopCartList.getCityId(), prod.getTransportId(), prod.getShopId());
				 if(regionalSales)
				 {
					 PaymentLog.error("商品区域限售,请重新选择商品, 商品Id是: {}, 单品Id是: {}", basket.getProdId(), basket.getSkuId());
					 throw new NotStocksException("商品区域限售,请重新选择商品", ErrorCodes.BUSINESS_ERROR);
				 }

				/**
				 * 拍下减库存 检查商品库存,并且更新商品的虚拟库存
				 * 秒杀订单不扣减库存
				 */
				if(!CartTypeEnum.SECKILL.toCode().equals(userShopCartList.getType())){
					if(StockCountingEnum.STOCK_BY_ORDER.value().equals(basket.getStockCounting())){
						if(!CartTypeEnum.SHOP_STORE.toCode().equals(userShopCartList.getType())){  // 非自提订单
							boolean success = stockService.addHold(basket.getProdId(),basket.getSkuId(), basket.getBasketCount().longValue());
							if(!success){
								PaymentLog.error("下单出现库存不足 , 商品Id是: {}, 单品Id是: {}", basket.getProdId(), basket.getSkuId());
								throw new NotStocksException(basket.getProdName()+"库存不足,请重新选择商品件数", ErrorCodes.BUSINESS_ERROR);
							}
						}
					}
				}

				prodName.append(subItem.getProdName()).append(",");
				shopTotalCash=Arith.add(basket.getTotal(),shopTotalCash);
				shopActualTotalCash=Arith.add(basket.getTotalMoeny(),shopActualTotalCash);
				//shopActualTotalCash=Arith.add(basket.getCalculatedCouponAmount(),shopActualTotalCash);
				shopDiscountPrice=Arith.add(basket.getDiscountPrice(),shopDiscountPrice);
				subItems.add(subItem);
		     }

		     /** 组装ls_sub**/
			Sub sub = new Sub();
			sub.setSubNumber(subNember);
			sub.setUserId(userId);
			sub.setUserName(userName);
			sub.setSubDate(date);
			sub.setUpdateDate(date);
			sub.setDistCommisAmount(distCommisAmount);//分销总佣金
			sub.setActiveId(userShopCartList.getActiveId());

			//优惠券和红包的 金额
			sub.setCouponOffPrice(shopCart.getCouponOffPrice());
			sub.setRedpackOffPrice(shopCart.getRedpackOffPrice());

			sub.setProdName(parseProdName(prodName.toString()));
			sub.setShopName(shopCart.getShopName());
			sub.setShopId(shopCart.getShopId());
			sub.setStatus(OrderStatusEnum.UNPAY.value());
			sub.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());
			sub.setOrderRemark(shopCart.getRemark());
			sub.setAddrOrderId(userAddressSub.getAddrOrderId());
			if(AppUtils.isBlank(userShopCartList.getPayManner())){
				sub.setPayManner(2);
			}else{
				sub.setPayManner(userShopCartList.getPayManner());
			}
			Double freightAmount=shopCart.getFreightAmount();//运费价格
			sub.setFreightAmount(freightAmount);
			sub.setWeight(shopCart.getShopTotalWeight());
			sub.setVolume(shopCart.getShopTotalVolume());
			sub.setProductNums(shopCart.getTotalcount());
			sub.setDiscountPrice(shopCart.getDiscountPrice());
			sub.setSubType(userShopCartList.getType());
			sub.setTotal(shopTotalCash);
			shopActualTotalCash=Arith.add(shopActualTotalCash,freightAmount); //总的商品实际金额+商家运费
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getDiscountPrice());//减去 营销活动优惠金额
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getCouponOffPrice()); //减去 优惠券金额
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getRedpackOffPrice());//减去 红包金额

			if(shopActualTotalCash<=0){//如果优惠券和红包金额大于订单金额，则直接支付成功
				shopActualTotalCash=0d;
				sub.setStatus(OrderStatusEnum.PADYED.value());
				sub.setPayDate(new Date());
				sub.setPayTypeName("优惠抵价");
			}
			sub.setActualTotal(shopActualTotalCash); //订单实际支付金额

			TransfeeDto currentSelectTransfee=shopCart.getCurrentSelectTransfee();
			if(AppUtils.isNotBlank(currentSelectTransfee)){
				sub.setDvyType(currentSelectTransfee.getFreightMode());
			}
			//设置是否需要发票
			sub.setNeedInvoice(invoiceSubId>0);
			sub.setInvoiceSubId(invoiceSubId);
			sub.setHasInvoice(0);
			sub.setCod(userShopCartList.isCod());//是否货到付款
			sub.setPayed(false);//还没有支付
			sub.setScore(0);
			sub.setOrderRemark(shopCart.getRemark());
			sub.setAddrOrderId(userAddressSub.getAddrOrderId());//用户地址Id
			sub.setRefundState(0);//退换货状态  0:默认,1:在处理,2:处理完成
			sub.setProvinceId(userAddressSub.getProvinceId());//订单省份id
			sub.setCityId(userAddressSub.getCityId());//订单城市id

			sub.setSwitchInvoice(userShopCartList.getSwitchInvoice());//当前订单所属商家是否开启发票
			sub.setChangedPrice(0d);

			//拼团订单需增加拼团订单状态以及拼团活动编号
			if(CartTypeEnum.MERGE_GROUP.toCode().equals(userShopCartList.getType())){
				sub.setMergeGroupStatus(SubMergeGroupStatusEnum.UNPAY.value());//拼团待支付
				if(AppUtils.isNotBlank(userShopCartList.getAddNumber())){//不为空证明是参团，参团订单需记录参团编号
					sub.setAddNumber(userShopCartList.getAddNumber());
				}else{
					sub.setAddNumber(CommonServiceUtil.getRandomSn());//开团：拼团编号
				}
			}

			//团购订单增加订单状态位
			if(CartTypeEnum.GROUP.toCode().equals(userShopCartList.getType())){
				sub.setGroupStatus(SubGroupStatusEnum.UNPAY.value());//团购待支付
			}

			/**处理主订单 **/
			Long subId = saveSub(sub);

			/**处理订单项 **/
			saveSubItems(subItems);

			//订单操作历史
			saveOrderHistory(sub);

			shopCart.setSubNember(subNember);
			shopCart.setSubId(subId);
			subIds.add(subNember);

			//更新优惠券使用状态
			List<UserCoupon> selCoupons1 = userShopCartList.getSelCoupons();
			if(AppUtils.isNotBlank(selCoupons1)){
				for (UserCoupon userCoupon : selCoupons1) {
					if(shopCart.getShopId().equals(userCoupon.getShopId())&&CouponProviderEnum.SHOP.value().equals(userCoupon.getCouponProvider())){
						userCouponDao.updateUserCouponUseSts(userCoupon.getCouponId(),userShopCartList.getUserName(),shopActualTotalCash,subNember);
						//增加优惠券使用数量
						couponDao.addCouponUseNum(userCoupon.getCouponId(),userShopCartList.getUserId());
					}
				}
			}

			//更新红包使用状态
			List<UserCoupon> selCoupons = userShopCartList.getSelCoupons();
			if(AppUtils.isNotBlank(selCoupons)){
				for (UserCoupon userCoupon : selCoupons) {
					if(CouponProviderEnum.PLATFORM.value().equals(userCoupon.getCouponProvider())){
						orderList.add(subNember);
						orderNumbers = StringUtil.join(orderList,",");
						conId=userCoupon.getCouponId();
						price += shopActualTotalCash;
						//增加优惠券使用数量
						couponDao.addCouponUseNum(userCoupon.getCouponId(),userShopCartList.getUserId());
					}
				}
			}
		}
		userCouponDao.updateUserCouponUseSts(conId,userShopCartList.getUserName(),price,orderNumbers);
		return subIds;
	}


	/**
	 * 门店商品下单
	 * @param userShopCartList
	 * @return 订单号 subIds
	 */
	public List<String> submitStoreOrder(UserShopCartList userShopCartList) {
		String userId=userShopCartList.getUserId();
		String userName=userShopCartList.getUserName();

		ShopCarts shopCart = userShopCartList.getShopCarts().get(0);
		if (AppUtils.isBlank(shopCart)) {
			 throw new BusinessException("购物车信息错误！请重新下单", ErrorCodes.BUSINESS_ERROR);
		}

		/**保存订单用户地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		UserAddressSub userAddressSub = null;
		if(AppUtils.isNotBlank(userAddress)){
			 userAddressSub = saveUserAddressSub(userAddress);
		}

		 /**根据发票ID查找发票内容 **/
		Long invoiceSubId=0l;
		if (AppUtils.isNotBlank(userShopCartList.getInvoiceId())&& userShopCartList.getInvoiceId() > 0) {
			 invoiceSubId=saveUserInvoice(userShopCartList.getInvoiceId(), userName);
		}
		List<StoreCarts> storeCarts = shopCart.getStoreCarts();
		List<String> subIds=new ArrayList<String>();
		Date date=new Date();

		for (Iterator<StoreCarts> storeIterator = storeCarts.iterator(); storeIterator.hasNext();) {
			 // 是在同一个门店下，每一个门店生成一个订单
			StoreCarts storeCart = (StoreCarts) storeIterator.next();

		     //判断门店是否上线状态
		     if(!storeCart.getStoreStatus()){//boolean值
		    	 throw new BusinessException("门店["+storeCart.getStoreName()+"]已下线", ErrorCodes.BUSINESS_ERROR);
		     }

	         List<ShopCartItem> items = storeCart.getCartItems();
		     List<SubItem> subItems = new ArrayList<SubItem>();
		     String subNember = CommonServiceUtil.getSubNember(userShopCartList.getUserName());
		     StringBuilder prodName = new StringBuilder(100);
		     int j=0;
		     double distCommisAmount = 0;//分销总金额
			 Double shopTotalCash = 0d; // 商家的商品金额
			 Double shopActualTotalCash = 0d; // 商家的商品金额
			 Double shopDiscountPrice = 0d;

		     for (Iterator<ShopCartItem> iterator = items.iterator(); iterator.hasNext();) {
		    	 ShopCartItem basket = (ShopCartItem) iterator.next();

		    	 //已下架的商品不能购买
		    	 ProductDetail prod = productService.getProdDetail(basket.getProdId());
		    	 if(prod.getStatus() != ProductStatusEnum.PROD_ONLINE.value()){
		    		 PaymentLog.error("下单商品已下架 , 商品Id是: {}, 单品Id是: {}", basket.getProdId(), basket.getSkuId());
		    		 throw new BusinessException(basket.getProdName()+"该商品已下架，请重新选择商品", ErrorCodes.BUSINESS_ERROR);
		    	 }

		    	 /** 组装SubItem */
				SubItem subItem = new SubItem();
				subItem.setSubNumber(subNember);
				subItem.setSubItemNumber(subNember + "-" + (++j));
				subItem.setProdId(basket.getProdId());
				subItem.setSkuId(basket.getSkuId());
				subItem.setSnapshotId(null);
				subItem.setBasketCount(basket.getBasketCount());
				subItem.setProdName(basket.getProdName());
				subItem.setAttribute(basket.getCnProperties());
				subItem.setUserId(userId);
				subItem.setPic(basket.getPic());
				subItem.setPrice(basket.getPrice());
				subItem.setCash(basket.getPromotionPrice()); //促销单价 如果没有参加过限时折扣 则为原价
				subItem.setProductTotalAmout(basket.getTotal());
				subItem.setObtainIntegral(0);
				subItem.setSubItemDate(date);
				subItem.setWeight(basket.getWeight());
				subItem.setVolume(basket.getVolume());
				if(AppUtils.isNotBlank(basket.getCartMarketRules())){
					StringBuilder builder=new StringBuilder(100);
					for (CartMarketRules rules:basket.getCartMarketRules()) {
						builder.append(rules.getPromotionInfo()).append(",");
					}
					subItem.setPromotionInfo(builder.toString());
				}
				subItem.setCommSts(0);//是否评论
				subItem.setCommisSettleSts(0); //是否结算，没有结算
				subItem.setHasDist(0);//是否有分佣金
				subItem.setRefundId(0l); //退换货ID
				subItem.setRefundState(0);//退换货状态
				subItem.setRefundAmount(null);//退钱的数目
				subItem.setStockCounting(basket.getStockCounting());

				subItem.setDiscountedPrice(basket.getDiscountedPrice());
				subItem.setCouponOffPrice(basket.getCouponOffPrice());
				subItem.setRedpackOffPrice(basket.getRedpackOffPrice());
				//订单项的实际金额
				subItem.setActualAmount(Arith.sub(basket.getDiscountedPrice(), Arith.add(basket.getCouponOffPrice(), basket.getRedpackOffPrice())));

				prodName.append(subItem.getProdName()).append(",");
				shopTotalCash=Arith.add(basket.getTotal(),shopTotalCash);
				shopActualTotalCash=Arith.add(basket.getTotalMoeny(),shopActualTotalCash);
				//shopActualTotalCash=Arith.add(basket.getCalculatedCouponAmount(),shopActualTotalCash);
				shopDiscountPrice=Arith.add(basket.getDiscountPrice(),shopDiscountPrice);
				subItems.add(subItem);
		     }

		     /** 组装ls_sub**/
			Sub sub = new Sub();
			sub.setSubNumber(subNember);
			sub.setUserId(userId);
			sub.setUserName(userName);
			sub.setSubDate(date);
			sub.setUpdateDate(date);
			sub.setDistCommisAmount(distCommisAmount);//分销总佣金
			sub.setActiveId(userShopCartList.getActiveId());

			//优惠券和红包的 金额
			sub.setCouponOffPrice(shopCart.getCouponOffPrice());
			sub.setRedpackOffPrice(shopCart.getRedpackOffPrice());

			sub.setProdName(parseProdName(prodName.toString()));
			sub.setShopName(shopCart.getShopName());
			sub.setShopId(shopCart.getShopId());
			sub.setStatus(OrderStatusEnum.UNPAY.value());
			sub.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());
			sub.setOrderRemark(storeCart.getRemark());

			if(AppUtils.isNotBlank(userAddressSub)){
				sub.setAddrOrderId(userAddressSub.getAddrOrderId());
			}
			if(AppUtils.isBlank(userShopCartList.getPayManner())){
				sub.setPayManner(2);
			}else{
				sub.setPayManner(userShopCartList.getPayManner());
			}
			Double freightAmount=shopCart.getFreightAmount();//运费价格
			sub.setFreightAmount(freightAmount);
			sub.setWeight(shopCart.getShopTotalWeight());
			sub.setVolume(shopCart.getShopTotalVolume());
			sub.setProductNums(shopCart.getTotalcount());
			sub.setDiscountPrice(shopCart.getDiscountPrice());
			sub.setSubType(userShopCartList.getType());
			sub.setTotal(shopTotalCash);
			shopActualTotalCash=Arith.add(shopActualTotalCash,freightAmount); //总的商品实际金额+商家运费
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getDiscountPrice());//减去 营销活动优惠金额
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getCouponOffPrice()); //减去 优惠券金额
			shopActualTotalCash=Arith.sub(shopActualTotalCash, sub.getRedpackOffPrice());//减去 红包金额

			if(shopActualTotalCash<=0){//如果优惠券和红包金额大于订单金额，则直接支付成功
				shopActualTotalCash=0d;
				sub.setStatus(OrderStatusEnum.PADYED.value());
				sub.setPayDate(new Date());
				sub.setPayTypeName("优惠抵价");
			}
			sub.setActualTotal(shopActualTotalCash); //订单实际支付金额

			TransfeeDto currentSelectTransfee=shopCart.getCurrentSelectTransfee();
			if(AppUtils.isNotBlank(currentSelectTransfee)){
				sub.setDvyType(currentSelectTransfee.getFreightMode());
			}

			//设置是否需要发票
			sub.setNeedInvoice(invoiceSubId>0);
			sub.setInvoiceSubId(invoiceSubId);
			sub.setHasInvoice(0);
			sub.setCod(userShopCartList.isCod());//是否货到付款
			sub.setPayed(false);//还没有支付
			sub.setScore(0);

			sub.setRefundState(0);//退换货状态  0:默认,1:在处理,2:处理完成

			if(AppUtils.isNotBlank(userAddressSub)){
				sub.setAddrOrderId(userAddressSub.getAddrOrderId());//用户地址Id
				sub.setProvinceId(userAddressSub.getProvinceId());//订单省份id
				sub.setCityId(userAddressSub.getCityId());//订单城市id
			}
			sub.setSwitchInvoice(userShopCartList.getSwitchInvoice());//当前订单所属商家是否开启发票
			sub.setChangedPrice(0d);

			/**处理主订单 **/
			Long subId = saveSub(sub);

			/**处理订单项 **/
			saveSubItems(subItems);

			//订单操作历史
			saveOrderHistory(sub);

			shopCart.setSubNember(subNember);
			shopCart.setSubId(subId);

			//门店保存订单信息
			storeCart.setSubNumber(subNember);
			storeCart.setSubId(subId);
			subIds.add(subNember);

		}
		return subIds;
	}



	/**
	 * 保存订单用户地址信息
	 * @param userAddress
	 * @return
	 */
	private  UserAddressSub saveUserAddressSub(UserAddress userAddress) {
		UserAddressSub userAddressSub = new UserAddressSub();
		userAddressSub.setUserId(userAddress.getUserId());
		userAddressSub.setReceiver(userAddress.getReceiver());
		userAddressSub.setSubPost(userAddress.getSubPost());
		userAddressSub.setProvinceId(userAddress.getProvinceId());
		userAddressSub.setCityId(userAddress.getCityId());
		userAddressSub.setAreaId(userAddress.getAreaId());
		userAddressSub.setSubAdds(userAddress.getSubAdds());
		userAddressSub.setAliasAddr(userAddress.getAliasAddr());
		StringBuffer buffer=new StringBuffer();
		buffer.append(userAddress.getProvince()).append(" ").append(userAddress.getCity())
		.append(" ").append(userAddress.getArea()).append(" ").append(userAddress.getSubAdds());
		userAddressSub.setDetailAddress(buffer.toString());
		userAddressSub.setEmail(userAddress.getEmail());
		userAddressSub.setMobile(userAddress.getMobile());
		userAddressSub.setTelphone(userAddress.getTelphone());
		Long addrOrderId = userAddressSubDao.saveUserAddressSub(userAddressSub);
		userAddressSub.setAddrOrderId(addrOrderId);
		return userAddressSub;
	}


	private Long saveUserInvoice(Long invoiceId, String userName) {
		//获取用户
		if(AppUtils.isBlank(invoiceId) || invoiceId ==0)
			return 0l;
		Invoice invoice = invoiceDao.getInvoice(invoiceId);
		/** 保存发票信息 **/
		if (invoice != null) {

			InvoiceSub invoiceSub = new InvoiceSub();
			invoiceSub.setType(invoice.getType());
			invoiceSub.setUserId(invoice.getUserId());
			invoiceSub.setUserName(userName);
			invoiceSub.setTitle(invoice.getTitle());
			invoiceSub.setCompany(invoice.getCompany());
			invoiceSub.setContent(invoice.getContent());
			invoiceSub.setInvoiceHumNumber(invoice.getInvoiceHumNumber());

//			添加增值税发票信息
			invoiceSub.setRegisterAddr(invoice.getRegisterAddr());
			invoiceSub.setRegisterPhone(invoice.getRegisterPhone());
			invoiceSub.setDepositBank(invoice.getDepositBank());
			invoiceSub.setBankAccountNum(invoice.getBankAccountNum());

			invoiceSub.setCreateTime(new Date());
			return  invoiceSubDao.saveInvoiceSub(invoiceSub);
		}
	    return 0l;
	}

	public void saveOrderHistory(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_SUBMIT.value());
		StringBuilder sb=new StringBuilder();
		sb.append(sub.getUserName()).append("于").append(time).append("提交订单 ");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryDao.saveSubHistory(subHistory);
	}


	private long saveSub(Sub sub) {
		return subService.saveSub(sub);
	}

	/**
	 * 保存订单项 并 发布商品快照事件
	 * @param subItems
	 */
	private void saveSubItems(List<SubItem> subItems) {
		subItemDao.saveSubItem(subItems);
//		List<Long> idList = subItemDao.saveSubItem(subItems);
//底层已经设置了Id
//		for (int i=0;i<subItems.size();i++) {
//			SubItem subItem = subItems.get(i);
//			subItem.setSubItemId(idList.get(i));
//		}
	}

	/**
	 * 一个订单可能有多个商品，限制长度为1000
	 * @param prodName
	 * @return
	 */
	private String parseProdName(String prodName){
		if (AppUtils.isNotBlank(prodName)) {
			String result = prodName.trim();
			if(result.endsWith(",")){
				result = result.substring(0, result.length() - 1);//去掉最后一个逗号
			}
			if (result.length() > 1000) {
				result = result.substring(0, 996) + "...";
			}
			return result;
		}else{
			return null;
		}
	}

}
