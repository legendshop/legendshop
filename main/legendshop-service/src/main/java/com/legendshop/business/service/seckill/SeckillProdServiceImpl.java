/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.seckill;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.model.dto.seckill.SeckillActivityDto;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.SeckillProdService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动服务实现类
 */
@Service("seckillProdService")
public class SeckillProdServiceImpl implements SeckillProdService {
	
	@Autowired
	private SeckillProdDao seckillProdDao;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductDao productDao;


	/**
	 * 获取秒杀商品 
	 */
	public List<SeckillProd> getSeckillProds(long sId) {
		return seckillProdDao.getSeckillProdBySid(sId);
	}

	/**
	 * 获取秒杀商品 
	 */
	public SeckillProd getSeckillProd(Long id) {
		return seckillProdDao.getSeckillProd(id);
	}

	/**
	 * 删除秒杀商品 
	 */
	public void deleteSeckillProd(SeckillProd seckillProd) {
		seckillProdDao.deleteSeckillProd(seckillProd);
	}

	/**
	 * 保存秒杀商品 
	 */
	public Long saveSeckillProd(SeckillProd seckillProd) {
		if (!AppUtils.isBlank(seckillProd.getId())) {
			updateSeckillProd(seckillProd);
			return seckillProd.getId();
		}
		return seckillProdDao.saveSeckillProd(seckillProd);
	}

	/**
	 * 更新秒杀商品 
	 */
	public void updateSeckillProd(SeckillProd seckillProd) {
		seckillProdDao.updateSeckillProd(seckillProd);
	}

	/**
	 * 保存秒杀商品列表 
	 */
	@Override
	public List<Long> saveSeckillProdList(List<SeckillProd> seckillProdList) {
		return seckillProdDao.saveSeckillProdList(seckillProdList);
	}

	/**
	 * 查询商品sku 
	 */
	@Override
	public List<SeckillActivityDto> findSkuProd(List<SeckillProd> seckillProdList) {
		if (AppUtils.isBlank(seckillProdList)) {
			return null;
		}
		List<SeckillActivityDto> seckillDto = new ArrayList<SeckillActivityDto>();
		for (Iterator<SeckillProd> iterator = seckillProdList.iterator(); iterator.hasNext();) {
			SeckillProd seckillProd = iterator.next();
			SeckillActivityDto dto = new SeckillActivityDto();
			if (AppUtils.isNotBlank(seckillProd)) {
				if (seckillProd.getSkuId().intValue() == 0) {
					ProductDetail prod = productDao.getProdDetail(seckillProd.getProdId());
					dto.setProdId(prod.getProdId());
					dto.setSkuId(null);
					dto.setProdName(prod.getName());
					dto.setPrice(seckillProd.getSeckillPrice());
					dto.setStock(seckillProd.getSeckillStock());
					dto.setActivityStock(seckillProd.getActivityStock());
					seckillDto.add(dto);
				} else {
					Sku sku = skuService.getSkuByProd(seckillProd.getProdId(), seckillProd.getSkuId());
					String prop = skuService.getSkuByProd(sku);
					dto.setProp(prop);
					dto.setPrice(seckillProd.getSeckillPrice());
					dto.setStock(seckillProd.getSeckillStock());
					dto.setActivityStock(seckillProd.getActivityStock());
					if (AppUtils.isNotBlank(sku)) {
						dto.setProdId(sku.getProdId());
						dto.setSkuId(sku.getSkuId());
						dto.setProdName(sku.getName());
						dto.setCnProperties(sku.getCnProperties());
						dto.setPic(sku.getPic());
						dto.setSkuStock(sku.getStocks());
						dto.setSkuPrice(sku.getPrice());
					}
					seckillDto.add(dto);
				}
			}

		}
		return seckillDto;
	}

	/**
	 * 检查秒杀商品是否可以参加秒杀活动
	 * 
	 * @param prodId
	 * @return
	 */
	@Override
	public boolean checkSeckillProdSku(Long prodId) {
		return seckillProdDao.checkSeckillProdSku(prodId);
	}

	/**
	 * 查询秒杀商品列表 
	 */
	@Override
	public List<SeckillProd> querySeckillProdList(List<Long> ids) {
		return seckillProdDao.querySeckillProdList(ids);
	}

}
