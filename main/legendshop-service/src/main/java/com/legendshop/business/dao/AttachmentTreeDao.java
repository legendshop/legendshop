/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AttachmentTree;

/**
 * 附件类型Dao.
 */
public interface AttachmentTreeDao extends GenericDao<AttachmentTree, Long> {
     
	public abstract AttachmentTree getAttachmentTree(Long id);
	
    public abstract int deleteAttachmentTree(AttachmentTree attachmentTree);
	
	public abstract Long saveAttachmentTree(AttachmentTree attachmentTree);
	
	public abstract int updateAttachmentTree(AttachmentTree attachmentTree);
	
	public abstract List<AttachmentTree> getAttachmentTreeByPid(Long pId, String userName);

	public abstract void updateAttmntTreeNameById(Integer id, String name);

	public abstract AttachmentTree getAttachmentTree(Long treeId, Long shopId);

	public abstract List<AttachmentTree> getAllChildByTreeId(Long treeId);

	public abstract List<AttachmentTree> getAttachmentTreeByUserName(String userName);
	
 }
