/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.NavigationPositionEnum;
import com.legendshop.model.entity.NavigationItem;

/**
 * 导航管理Dao.
 */
public interface NavigationItemDao extends GenericDao<NavigationItem, Long>{

    public abstract List<NavigationItem> getNavigationItem();

    public abstract List<NavigationItem> getAllNavigationItem();

	public abstract NavigationItem getNavigationItem(Long id);

    public abstract void deleteNavigationItem(NavigationItem navigationItem);

	public abstract Long saveNavigationItem(NavigationItem navigationItem);

	public abstract void updateNavigationItem(NavigationItem navigationItem);

	public abstract List<NavigationItem> getNavigationItemByNaviId(Long naviId);

	public abstract void deleteNavigationItems(Long naviId);
	
	/**
	 * 查找对应位置的导航
	 * @param position
	 * @return
	 */
    public List<NavigationItem> getNavigationItem(NavigationPositionEnum position);

	public abstract PageSupport<NavigationItem> getNavigationItemPage(String curPageNO);
 }
