/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;

/**
 * 微信响应Dao
 */
public interface WeixinAutoresponseDao extends Dao<WeixinAutoresponse, Long> {
     
	public abstract WeixinAutoresponse getWeixinAutoresponse(Long id);
	
    public abstract int deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	public abstract Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	public abstract int updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	public abstract PageSupport<WeixinAutoresponse> getWeixinnAutoresponse(String curPageNO);
	
 }
