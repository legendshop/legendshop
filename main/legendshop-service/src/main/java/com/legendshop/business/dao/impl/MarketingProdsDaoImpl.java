/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.business.dao.MarketingProdsDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.entity.MarketingProds;
import com.legendshop.util.AppUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.Map.Entry;

/**
 * 参加营销活动的商品Dao.
 */
@Repository
public class MarketingProdsDaoImpl extends GenericDaoImpl<MarketingProds, Long> implements MarketingProdsDao  {

	@Override
	public List<MarketingProds> getMarketingProdsByMarketId(Long marketId) {
		return this.queryByProperties(new EntityCriterion().eq("marketId", marketId));
	}

	public MarketingProds getMarketingProds(Long id){
		return getById(id);
	}
	
    public int deleteMarketingProds(MarketingProds marketingProds){
    	return delete(marketingProds);
    }
	
	public Long saveMarketingProds(MarketingProds marketingProds){
		return save(marketingProds);
	}
	
	public int updateMarketingProds(MarketingProds marketingProds){
		return update(marketingProds);
	}

	@Override
	public PageSupport<MarketingProdDto> findMarketingRuleProds(String curPage, Long shopId, Long marketingId) {
		String queryString =ConfigCode.getInstance().getCode("promotions.findMarketingRuleProds");
		String allCountString=ConfigCode.getInstance().getCode("promotions.findMarketingRuleProdCount");
		Object[] param = new Object[]{shopId,marketingId};
		SimpleSqlQuery query = new SimpleSqlQuery(MarketingProdDto.class);
		query.setAllCountString(allCountString);
		query.setQueryString(queryString);
		query.setCurPage(curPage);
		query.setParam(param);
		query.setPageSize(10);
		return querySimplePage(query);
	}

	@Override
	public PageSupport<MarketingProdDto> findMarketingProds(String curPage, Long shopId,String name) {
		List<Object> objects=new ArrayList<Object>(2);
		objects.add(shopId);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(name)){
			map.put("productName", "%"+name+"%");
			objects.add("%"+name+"%");
		}
		String queryString=ConfigCode.getInstance().getCode("promotions.findMarketingProds",map);
		String allCountString=ConfigCode.getInstance().getCode("promotions.findMarketingProdCount",map);
		SimpleSqlQuery query = new SimpleSqlQuery(MarketingProdDto.class);
		query.setAllCountString(allCountString);
		query.setQueryString(queryString);
		query.setCurPage(curPage);
		query.setParam(objects.toArray());
		query.setPageSize(10);
		return querySimplePage(query);
	}

	@Override
	public List<MarketingDto> findMarketings(Long prodId,Long skuId, Long shopId) {
		Date time=new Date();
		List<Object> args=new ArrayList<Object>();
		String sqlString=ConfigCode.getInstance().getCode("promotions.findMarketings");
		args.add(skuId);
		args.add(shopId);
		args.add(time);
		args.add(time);
		List<MarketingDto> list=query(sqlString, MarketingDto.class, args.toArray());
		return list;
	}
	
	//全场的营销活动， 注意加入缓存
	@Cacheable(value="GlobalMarketingDtoList",key="#shopId")
	public List<MarketingDto> findGlobalMarketing(Long shopId, Date time){
		String sql=ConfigCode.getInstance().getCode("promotions.findGlobalMarketing");
		List<MarketingDto> list=query(sql, MarketingDto.class, new Object[]{shopId, time, time});
		if(AppUtils.isBlank(list)){
			return null;
		}
		
		Map<Long,  MarketingDto> map=new HashMap<Long, MarketingDto>();
		for (MarketingDto marketingDto : list) {
			map.put(marketingDto.getMarketId(), marketingDto);
		}
		
		//TODO 查找rule
		List<MarketingRuleDto> marketingRuleDtoList = findMarketinRule(shopId, list);
		if(AppUtils.isNotBlank(marketingRuleDtoList)){
			for (MarketingRuleDto ruleDto:marketingRuleDtoList) {
				if(map.containsKey(ruleDto.getMarketId())){
					MarketingDto marketingDto=map.get(ruleDto.getMarketId());
					marketingDto.addRuleList(ruleDto);
				}
			}
		}
		
		List<MarketingDto> list2=new ArrayList<MarketingDto>();
		for ( Entry<Long, MarketingDto> entrySet: map.entrySet()) {
			list2.add(entrySet.getValue());
		}
		list=null;
		map=null;
		return list2;
	}
	

	/**
	 * 按店铺订单 来查找 营销活动列表
	 *  注意加入缓存 TODO
	 */
	@Override
	public List<MarketingDto> findProdMarketing(List<ShopCartItem> cartItems,Long shopId, Date time) {
		
		//拼装sql语句
		StringBuilder builder=new StringBuilder();
		List<Object> params=new ArrayList<Object>();
		for (int i = 0; i < cartItems.size(); i++) {
			builder.append("?,");
			params.add(cartItems.get(i).getProdId());
		}
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids); //prodId集合
		params.add(shopId);
		params.add(time);
		params.add(time);
		
		String sqlString=ConfigCode.getInstance().getCode("promotions.findShopProdMarketing",queryMap);
		
		//找出店铺订单商品参与的所有营销活动  
		List<MarketingDto> list= query(sqlString, MarketingDto.class,params.toArray());
		
		if(AppUtils.isBlank(list)){
			return null;
		}
		
		// 查找这些营销活动的所有规则
		List<MarketingRuleDto> marketingRuleDtoList = findMarketinRule(shopId, list);
		
		//查找参加这些营销活动的所有商品
		List<MarketingProdDto> marketingProdsDtoList = findMarketinProd(shopId, list);
		
		//把营销活动放入map
		Map<Long,  MarketingDto> marketingMap=new HashMap<Long, MarketingDto>();
		for (MarketingDto marketingDto : list) {
			marketingMap.put(marketingDto.getMarketId(), marketingDto);
		}
		
		//找到营销活动的营销规则
		if(AppUtils.isNotBlank(marketingRuleDtoList)){
			for (MarketingRuleDto ruleDto:marketingRuleDtoList) {
				if(marketingMap.containsKey(ruleDto.getMarketId())){
					MarketingDto marketingDto=marketingMap.get(ruleDto.getMarketId());
					marketingDto.addRuleList(ruleDto);
				}
			}
		}
		
		//找到 营销活动参与的商品
		if(AppUtils.isNotBlank(marketingProdsDtoList)){
			for (MarketingProdDto prodDto:marketingProdsDtoList) {
				if(marketingMap.containsKey(prodDto.getMarketId())){
					for (ShopCartItem shopCartItem : cartItems) {
						if(shopCartItem.getProdId().equals(prodDto.getProdId()) && shopCartItem.getSkuId().equals(prodDto.getSkuId())){
							MarketingDto marketingDto=marketingMap.get(prodDto.getMarketId());
							marketingDto.addProdList(prodDto);
						}
					}
				}
			}
		}
		
		//把map 转成 list
		List<MarketingDto> list2=new ArrayList<MarketingDto>();
		for ( Entry<Long, MarketingDto> entrySet: marketingMap.entrySet()) {
			list2.add(entrySet.getValue());
		}
		list=null;
		marketingMap=null;
		return list2;
	}
	
	
	/**
	 * 商品的营销活动， 注意加入缓存
	 */
	@Cacheable(value="MarketingDtoList",key="#prodId+#shopId")
	public List<MarketingDto> findProdMarketing(Long prodId,Long shopId, Date time){
		String sqlString=ConfigCode.getInstance().getCode("promotions.findProdMarketing");
		List<MarketingDto> list=query(sqlString, MarketingDto.class, new Object[]{prodId, shopId, time, time});
		if(AppUtils.isBlank(list)){
			return null;
		}
		
		// 查找rule
		List<MarketingRuleDto> marketingRuleDtoList = findMarketinRule(shopId, list);
		
		//查找参与改活动的商品
		List<MarketingProdDto> marketingProdsDtoList = findMarketinProd(shopId, list);
		
		
		Map<Long,  MarketingDto> map=new HashMap<Long, MarketingDto>();
		for (MarketingDto marketingDto : list) {
			map.put(marketingDto.getMarketId(), marketingDto);
		}
		
		if(AppUtils.isNotBlank(marketingRuleDtoList)){
			for (MarketingRuleDto ruleDto:marketingRuleDtoList) {
				if(map.containsKey(ruleDto.getMarketId())){
					MarketingDto marketingDto=map.get(ruleDto.getMarketId());
					marketingDto.addRuleList(ruleDto);
				}
			}
		}
		
		if(AppUtils.isNotBlank(marketingProdsDtoList)){
			for (MarketingProdDto prodDto:marketingProdsDtoList) {
				if(map.containsKey(prodDto.getMarketId())){
					MarketingDto marketingDto=map.get(prodDto.getMarketId());
					marketingDto.addProdList(prodDto);
				}
			}
		}
		List<MarketingDto> list2=new ArrayList<MarketingDto>();
		for ( Entry<Long, MarketingDto> entrySet: map.entrySet()) {
			list2.add(entrySet.getValue());
		}
		list=null;
		map=null;
		return list2;
	}
	

	/**
	 * 根据活动ID获取相应的活动规则
	 */
	@Override
	public List<MarketingRuleDto> findMarketinRule(Long shopId,List<MarketingDto> resultRuleList) {
		if(AppUtils.isBlank(resultRuleList)){
			return null;
		}
		StringBuilder builder=new StringBuilder();
		List<Object> params=new ArrayList<Object>();
		for (int i = 0; i < resultRuleList.size(); i++) {
			builder.append("?,");
			params.add(resultRuleList.get(i).getMarketId());
		}
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids); //marketId集合
		params.add(shopId);
		String sqlString=ConfigCode.getInstance().getCode("promotions.findMarketinRule",queryMap);
		List<MarketingRuleDto> marketingRuleDtos= query(sqlString, MarketingRuleDto.class,params.toArray());
		return marketingRuleDtos;
	}
	

	/**
	 * 根据活动ID获取相应的参与商品
	 */
	@Override
	public List<MarketingProdDto> findMarketinProd(Long shopId,List<MarketingDto> resultRuleList) {
		if(AppUtils.isBlank(resultRuleList)){
			return null;
		}
		StringBuilder builder=new StringBuilder();
		List<Object> params=new ArrayList<Object>();
		for (int i = 0; i < resultRuleList.size(); i++) {
			builder.append("?,");
			params.add(resultRuleList.get(i).getMarketId());
		}
		String ids=builder.substring(0,builder.length()-1);
		QueryMap queryMap=new QueryMap();
		queryMap.put("ids", ids); //marketId的集合
		params.add(shopId);
		String sqlString=ConfigCode.getInstance().getCode("promotions.findMarketinProd",queryMap);
		List<MarketingProdDto> list= query(sqlString, MarketingProdDto.class,params.toArray());
		return list;
	}

	@Override
	public List<MarketingProds> getmarketingProdByProdId(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}

	@Override
	public MarketingProdDto iSHaveMarketing(Long prodId, Long skuId) {
		
		String sql = "SELECT lm.type AS type,lm.market_name AS marketName FROM ls_marketing_prods lmp LEFT JOIN ls_marketing lm ON lmp.market_id = lm.id WHERE lmp.prod_id = ? AND lmp.sku_id = ?";
		return get(sql, MarketingProdDto.class, prodId,skuId);
	}

}
