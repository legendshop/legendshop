package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.business.dao.HeadlineAdminDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.Headline;
import com.legendshop.spi.service.HeadlineAdminService;
import com.legendshop.uploader.AttachmentManager;

/**
 * 头条管理服务
 *
 */
@Service("headlineAdminService")
public class HeadlineAdminServiceImpl implements HeadlineAdminService{

	@Autowired
	private HeadlineAdminDao headlineAdminDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Override
	public Headline getHeadlineAdmin(Long id) {
		return headlineAdminDao.getHeadlineAdmin(id);
	}

	@Override
	public void deleteHeadlineAdmin(Long id) {
		headlineAdminDao.deleteHeadlineAdmin(id);
	}

	@Override
	public Long saveHeadlineAdmin(Headline headline) {
		return headlineAdminDao.saveHeadlineAdmin(headline);
	}

	@Override
	public void updateHeadlineAdmin(Headline headline) {
		headlineAdminDao.updateHeadlineAdmin(headline);
	}

	@Override
	public PageSupport<Headline> getHeadlineAdmin(String curPageNO, Headline headline) {
		return headlineAdminDao.getHeadlineAdmin(curPageNO,headline);
	}

	@Override
	public Long saveHeadlineAdmin(String userName,String userId, Long shopId, String pic, MultipartFile imageFile, Headline headline) {
		attachmentManager.saveImageAttachment(userName,userId,shopId, pic, imageFile, AttachmentTypeEnum.NEWS);
		headline.setPic(pic);
		headline.setAddTime(new Date());
		return headlineAdminDao.saveHeadlineAdmin(headline);
	}
}
