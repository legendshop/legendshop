/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.weixin;

import com.legendshop.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.dao.PayTypeDao;
import com.legendshop.business.dao.WeixinMpTokenDao;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.weixin.WeixinMpAccessToken;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.weixin.WeixinMpToken;
import com.legendshop.spi.service.WeixinMpTokenService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;

/**
 * 微信小程序token Service
 */
@Service
public class WeixinMpTokenServiceImpl  implements WeixinMpTokenService{
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(WeixinMpTokenServiceImpl.class);
	
	@Autowired
    private WeixinMpTokenDao weixinMpTokenDao;
    
	@Autowired
    private PayTypeDao payTypeDao;

    public void setWeixinMpTokenDao(WeixinMpTokenDao weixinMpTokenDao) {
        this.weixinMpTokenDao = weixinMpTokenDao;
    }

    public WeixinMpToken getWeixinMpToken(Long id) {
        return weixinMpTokenDao.getWeixinMpToken(id);
    }

    public void deleteWeixinMpToken(WeixinMpToken weixinMpToken) {
        weixinMpTokenDao.deleteWeixinMpToken(weixinMpToken);
    }

    public Long saveWeixinMpToken(WeixinMpToken weixinMpToken) {
        if (!AppUtils.isBlank(weixinMpToken.getId())) {
            updateWeixinMpToken(weixinMpToken);
            return weixinMpToken.getId();
        }
        return weixinMpTokenDao.saveWeixinMpToken(weixinMpToken);
    }

    public void updateWeixinMpToken(WeixinMpToken weixinMpToken) {
        weixinMpTokenDao.updateWeixinMpToken(weixinMpToken);
    }

	@Override
	public String getAccessToken() {
		
		WeixinMpToken weixinMpToken = weixinMpTokenDao.getWeixinMpToken();
		if (AppUtils.isNotBlank(weixinMpToken)) {
			// 判断有效时间 是否超过2小时
			java.util.Date end = new java.util.Date();
			java.util.Date start = new java.util.Date(weixinMpToken.getValidateTime().getTime());
			if ((end.getTime() - start.getTime()) / 1000 / 3600 >= 1) {//1个小时取一次，防止过期，一天最多200次调用
				return getToken(weixinMpToken);
			}
			
			return weixinMpToken.getAccessToken();
		} 
		
		return getToken(null);
	}
	
	private String getToken(WeixinMpToken weixinMpToken){
		
		PayType payType  = payTypeDao.getPayTypeById(PayTypeEnum.WX_MP_PAY.value());
		
		log.info("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			return null;
		} 
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		String appid = jsonObject.getString("APPID");
		String appSecret=jsonObject.getString("APPSECRET");
		
		WeixinMpAccessToken weixinAccessToken = WeixinMiniProgramApi.getAccessToken(appid, appSecret);
		if(!weixinAccessToken.isSuccess()){
			log.info("############## 获取微信公众号 accessToken 错误: {} #################", weixinAccessToken.getErrmsg());
			return null;
		}
		
		String accessToken = weixinAccessToken.getAccess_token();
		int  expiresIn = weixinAccessToken.getExpires_in();

		if(AppUtils.isNotBlank(weixinMpToken)){
			weixinMpToken.setValidateTime(DateUtil.getNowTime());
			weixinMpToken.setAccessToken(accessToken);
			weixinMpToken.setExpiresIn(expiresIn);
			weixinMpTokenDao.updateWeixinMpToken(weixinMpToken);
		}else{
			weixinMpToken =new WeixinMpToken();
			weixinMpToken.setExpiresIn(expiresIn);
			weixinMpToken.setValidateTime(DateUtil.getNowTime());
			weixinMpToken.setAccessToken(accessToken);
			weixinMpTokenDao.saveWeixinMpToken(weixinMpToken);
		}

		return accessToken;
	}
}
