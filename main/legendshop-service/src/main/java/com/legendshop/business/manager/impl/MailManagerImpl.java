package com.legendshop.business.manager.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.base.config.SystemParameterUtil;
import org.apache.oro.text.regex.MalformedPatternException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendMailEvent;
import com.legendshop.base.exception.ApplicationException;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.commond.ErrorCode;
import com.legendshop.model.MailInfo;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.FileParameterService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;
import com.legendshop.util.SystemUtil;

/**
 * 发送邮件的管理者
 * 
 */
@Component
public class MailManagerImpl implements MailManager {
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(MailManagerImpl.class);
	
	@Autowired
	private SystemConfigService systemConfigService;
	
	@Resource(name = "sendMailProcessor")
	private EventProcessor<MailInfo> sendMailProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private FileParameterService fileParameterService;
	
	
	@Override
	public void register(User user, UserDetail userDetail) {
		try {
			String text = getMailText(user, userDetail);
			sendMailProcessor.process(new SendMailEvent(userDetail.getUserMail(), "恭喜您，注册商城成功", text, user.getName(), MailCategoryEnum.REG.value()).getSource());
			log.info("{} 注册成功，发送通知邮件", userDetail.getUserMail());
			
		} catch (Exception e) {
			log.info("{}，发送通知邮件失败，请检查邮件配置", userDetail.getUserMail());
			throw new ApplicationException(e, "发送通知邮件失败，请检查邮件配置", ErrorCode.BUSINESS_ERROR);
		}

	}

	/**
	 * 前往修改密码界面
	 */
	@Override
	public void updatePassword(User user,String mail,String code) {
		String link =  propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/preUpdatePassWord/"+user.getName()+"/"+code;
		Map<String, String> values = new HashMap<String, String>();
		values.put("#userName#", user.getNickName() == null ? "":user.getNickName());
		values.put("#nowdate#", new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date()));
		values.put("#link#", link);
		
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		
		String template = systemConfig.getMailValidateTemplate();
		String text;
		try {
			text = StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
			sendMailProcessor.process(new SendMailEvent(mail, "密码修改验证", text,user.getName(), MailCategoryEnum.REG.value()).getSource());
		} catch (MalformedPatternException e) {
			log.error("send mail fail", e);
		}
	}

	/**
	 * 重置密码
	 */
	@Override
	public void resetPassword(UserDetail userDetail, String newPassword) {
		if (systemParameterUtil.sendMail()) {
			log.info("{} 修改密码，发送通知邮件到 {}", userDetail.getNickName(), userDetail.getUserMail());
			Map<String, String> values = new HashMap<String, String>();

			values.put("#domainName#", propertiesUtil.getPcDomainName());
			values.put("#nickName#", userDetail.getNickName());
			values.put("#userName#", userDetail.getUserName());
			values.put("#password#", newPassword);
			// TODO 将文件转入数据库，还未测试
//			String templateFilePath = propertiesUtil.getDownloadFilePath() + "/mail/resetpassmail.html";
			String text;
			try {
//				text = AppUtils.convertTemplate(templateFilePath, values);
				text = fileParameterService.findByName("resetpassmail.html").getValue();
				sendMailProcessor.process(new SendMailEvent(userDetail.getUserMail(), "恭喜您，修改密码成功！", text, userDetail.getUserName(), MailCategoryEnum.PWD.value()).getSource());
			} catch (Exception e) {
				log.error("send mail fail", e);
			}
		}
	}

	@Override
	public void updateMail(User user, String mail,String code) {
		try {
			//String filePath = propertiesUtil.getDownloadFilePath() + BusinessFileManager.MAIL_FILE_PATH + "/" + BusinessFileManager.VALIDATION_MAIL;
			SystemConfig systemConfig = systemConfigService.getSystemConfig();
			String template = systemConfig.getMailValidateTemplate();
			/** map 里的 value 不可为空  **/
			Map<String, String> values = new HashMap<String, String>();
			values.put("#userName#", user.getNickName() == null ? "":user.getNickName());
			values.put("#nowdate#", new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date()));
			
			StringBuffer buffer = new StringBuffer();
			buffer.append(propertiesUtil.getPcDomainName())
			.append(SystemUtil.getContextPath())
			.append("/validate/mail?userName=").append(user.getUserName()).append("&code=")
			.append(code);
			values.put("#link#", buffer.toString());
			String text = StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
			sendMailProcessor.process(new SendMailEvent(mail, "邮箱验证", text,user.getUserName(), MailCategoryEnum.VAL.value()).getSource());
		} catch (Exception e) {
			log.error("send mail fail", e);
		}
	}
	
	
	@Override
	public void confirmationMail(User user,String mail,String code){
		String link =  propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/preUpdateMail/"+user.getName()+"/"+code;
		Map<String, String> values = new HashMap<String, String>();
		values.put("#userName#", user.getNickName() == null ? "":user.getNickName());
		values.put("#nowdate#", new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date()));
		values.put("#link#", link);
		
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		
		String template = systemConfig.getMailValidateTemplate();
		
		String text;
		try {
			text = StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
			sendMailProcessor.process(new SendMailEvent(mail, "邮箱修改验证", text,user.getName(), MailCategoryEnum.REG.value()).getSource());
		} catch (MalformedPatternException e) {
			log.error("send mail fail", e);
		}
	}
	
	@Override
	public void withdrawApply(UserDetail user, String userEmail, String code) {
		try {
			String userName=user.getNickName() == null ? user.getUserName():user.getNickName();
		    String nowdate=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date());
			StringBuffer buffer = new StringBuffer();
			buffer.append("尊敬的 ").append(userName).append("您好:");
			buffer.append("您于").append(nowdate).append("进行提现操作,");
			 String siteName  = systemParameterUtil.getSiteName();
			 String text = "";
			 String valEmailTempateFormat = systemParameterUtil.getValEmailTempateFormat();
			 text = valEmailTempateFormat.replace("#SiteName", siteName);
			 text = text.replace("#EmailCode", code);
			 text = text.replace("#UsefulMin","3");
			 buffer.append(text);
			 sendMailProcessor.process(new SendMailEvent(userEmail, "提现验证", buffer.toString(),user.getUserName(), MailCategoryEnum.TIX.value(),code).getSource());
		} catch (Exception e) {
			log.error("send mail fail", e);
		}
	}
	

	@Override
	public void orderChanges() {
		// TODO Auto-generated method stub

	}

	@Override
	public void remindOrder() {
		// TODO Auto-generated method stub

	}

	private String getMailText(User user, UserDetail userDetail) throws MalformedPatternException {
		/** map 里的 value 不可为空  **/
		Map<String, String> values = new HashMap<String, String>();
		values.put("#nickName#", userDetail.getNickName() == null ? "":userDetail.getNickName());
//		values.put("#userName#", user.getUserName() == null ? "":user.getUserName());
		values.put("#userName#", userDetail.getNickName() == null ? "":userDetail.getNickName());
		values.put("#password#", user.getPasswordag() == null ? "":user.getPasswordag());
		if (AppUtils.isNotBlank(userDetail.getRegisterCode())) {
			StringBuffer buffer = new StringBuffer();
			// 在邮件里点击通过验证并开通帐号
			/*buffer.append("<p>你的帐号尚未开通，<a href=\"").append(propertiesUtil.getDomainName())
					.append("/userRegSuccess" + "?userName=").append(user.getName()).append("&registerCode=")
					.append(userDetail.getRegisterCode()).append("\">点击开通我的帐号</a></p><br>");*/
			
			buffer.append("<p>你的帐号尚未开通，点击此链接激活我的账号：<br>").append(propertiesUtil.getPcDomainName())
			.append(SystemUtil.getContextPath())
			.append("/userRegSuccess" + "?userName=").append(user.getName()).append("&code=")
			.append(userDetail.getRegisterCode()).append("</p>");
			
			values.put("#registerCode#", buffer.toString());
		} else {
			StringBuffer buffer = new StringBuffer();
			buffer.append("<p>你的帐号已经开通成功!</p><br>");
			values.put("#registerCode#", buffer.toString());
		}
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String template = systemConfig.getMailRegsuccessTemplate();
		return StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
	}

	/**
	 *找回密码时，进行邮箱的验证 
	 */
	@Override
	public void verifyEmail(User user, String mail, String code) {
		String link =  propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/retrievepassword?t=1&userName="+user.getName()+"&code="+code;
		Map<String, String> values = new HashMap<String, String>();
		values.put("#domainName#", systemParameterUtil.getDownloadFilePath());
		values.put("#link#", link);
		if(AppUtils.isBlank(user.getNickName())){
			user.setNickName(user.getUserName());
		}
		values.put("#userName#", user.getNickName() == null ? "":user.getNickName());
		values.put("#nowdate#", new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date()));
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String template = systemConfig.getMailValidateTemplate();
		String text;
		try {
			text = StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
			sendMailProcessor.process(new SendMailEvent(mail, "验证邮箱提醒", text,user.getName(), MailCategoryEnum.REG.value()).getSource());
		} catch (MalformedPatternException e) {
			log.error("send mail fail", e);
		}
	}

	@Override
	public void updatePaymentpassword(User user, String userEmail, String code) {
		String link =  propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/preUpdatePaymentpassword/"+user.getName()+"/"+code;
		Map<String, String> values = new HashMap<String, String>();
		values.put("#userName#", user.getUserName() == null ? "":user.getUserName());
		values.put("#nowdate#", new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss").format(new Date()));
		values.put("#link#", link);
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String template = systemConfig.getMailValidateTemplate();
		String text;
		try {
			text = StringUtil.convert(template, "\\#[a-zA-Z]+\\#", values);
			sendMailProcessor.process(new SendMailEvent(userEmail, "支付密码修改验证邮箱", text.toString(),user.getName(), MailCategoryEnum.VAL.value()).getSource());
		} catch (MalformedPatternException e) {
			log.error("send mail fail", e);
		}
	}
	

}
