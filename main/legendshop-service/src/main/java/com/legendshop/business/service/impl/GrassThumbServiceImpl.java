/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassThumbDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassThumb;
import com.legendshop.spi.service.GrassThumbService;
import com.legendshop.util.AppUtils;

/**
 * The Class GrassThumbServiceImpl.
 *  种草文章点赞表服务实现类
 */
@Service("grassThumbService")
public class GrassThumbServiceImpl implements GrassThumbService{

    /**
     *
     * 引用的种草文章点赞表Dao接口
     */
	@Autowired
    private GrassThumbDao grassThumbDao;
   
   	/**
	 *  根据Id获取种草文章点赞表
	 */
    public GrassThumb getGrassThumb(Long id) {
        return grassThumbDao.getGrassThumb(id);
    }
    
    /**
	 *  根据Id删除种草文章点赞表
	 */
    public int deleteGrassThumb(Long id){
    	return grassThumbDao.deleteGrassThumb(id);
    }

   /**
	 *  删除种草文章点赞表
	 */ 
    public int deleteGrassThumb(GrassThumb grassThumb) {
       return  grassThumbDao.deleteGrassThumb(grassThumb);
    }

   /**
	 *  保存种草文章点赞表
	 */	    
    public Long saveGrassThumb(GrassThumb grassThumb) {
        if (!AppUtils.isBlank(grassThumb.getId())) {
            updateGrassThumb(grassThumb);
            return grassThumb.getId();
        }
        return grassThumbDao.saveGrassThumb(grassThumb);
    }

   /**
	 *  更新种草文章点赞表
	 */	
    public void updateGrassThumb(GrassThumb grassThumb) {
        grassThumbDao.updateGrassThumb(grassThumb);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassThumb> queryGrassThumb(String curPageNO, Integer pageSize){
     	return grassThumbDao.queryGrassThumb(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassThumbDao(GrassThumbDao grassThumbDao) {
        this.grassThumbDao = grassThumbDao;
    }

	@Override
	public void deleteGrassThumbByDiscoverId(Long grassId, String uid) {
        grassThumbDao.deleteGrassThumbByDiscoverId(grassId, uid);
	}

    @Override
    public Integer rsThumb(Long grassId, String userId) {
        return grassThumbDao.rsThumb(grassId,userId);
    }

}
