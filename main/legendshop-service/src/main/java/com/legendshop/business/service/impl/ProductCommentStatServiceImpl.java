/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.business.dao.ProductCommentStatDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.spi.service.ProductCommentStatService;
import com.legendshop.util.AppUtils;

/**
 *  评论统计ServiceImpl.
 */
@Service("productCommentStatService")
public class ProductCommentStatServiceImpl  implements ProductCommentStatService{

	@Autowired
    private ProductCommentStatDao productCommentStatDao;
    
	@Autowired
    private ProductCommentDao productCommentDao;
    
	@Autowired
    private ProductDao productDao;

    public ProductCommentStat getProductCommentStat(Long id) {
        return productCommentStatDao.getProductCommentStat(id);
    }

    public void deleteProductCommentStat(ProductCommentStat productCommentStat) {
        productCommentStatDao.deleteProductCommentStat(productCommentStat);
    }

    public Long saveProductCommentStat(ProductCommentStat productCommentStat) {
        if (!AppUtils.isBlank(productCommentStat.getId())) {
            updateProductCommentStat(productCommentStat);
            return productCommentStat.getId();
        }
        return productCommentStatDao.saveProductCommentStat(productCommentStat);
    }

    public void updateProductCommentStat(ProductCommentStat productCommentStat) {
        productCommentStatDao.updateProductCommentStat(productCommentStat);
    }

	@Override
	public ProductCommentStat getProductCommentStatByProdId(Long prodId) {
		return productCommentStatDao.getProductCommentStatByProdId(prodId);
	}

	@Override
	public ProductCommentStat getProductCommentStatByShopId(Long shopId) {
		return productCommentStatDao.getProductCommentStatByShopId(shopId);
	}

	@Override
	public ProductCommentStat getProductCommentStatByProdIdForUpdate(Long prodId) {
		return productCommentStatDao.getProductCommentStatByProdIdForUpdate(prodId);
	}

	/**
	 * 重新计算 商品的评论数和分数
	 */
	@Override
	public void reCalculateProdComments(Long prodId) {
		Integer comments = productCommentDao.get("select count(1) from ls_prod_comm where prod_id=?", Integer.class, prodId);
		Integer score = 0;
		if(comments>0){
			score = productCommentDao.get("select sum(score) from ls_prod_comm where prod_id=?", Integer.class, prodId);
		}
		productCommentStatDao.updateCommentsAndScore(comments,score,prodId);
		productDao.update("update ls_prod set comments = ? where prod_id =?", comments, prodId);
	}
}
