/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.entity.Brand;

/**
 * 品牌Dao.
 */
public interface BrandDao extends GenericDao<Brand, Long> {

	/**
	 * Delete brand by id.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void deleteBrandById(Long id);

	/**
	 * Update brand.
	 * 
	 * @param brand
	 *            the brand
	 */
	public abstract void updateBrand(Brand brand);
	
	/**
	 * 品牌下是否带有产品
	 * @param userName
	 * @param brandId
	 * @return
	 */
	public abstract boolean hasChildProduct(Long brandId);
	
	/**
	 * 显示所有精品品牌
	 * @return
	 */
	public List<Brand> getAllCommentBrand();
	
	/**
	 * 根据一级商品的id 查找出对应的品牌
	 */
	public List<Brand> getBrandBySortId(Long sortId);
	
	/**
	 * 根据一级商品的id 查找出后30对应的品牌
	 * @param sortId
	 * @return
	 */
	public List<Brand> getMoreBrandList(Long sortId);

	public abstract List<Brand> getBrandByUserName(String userName);

	public abstract Brand getBrandById(Long id);
	
	/**
	 * 查出关联品牌
	 * @return
	 */
	public abstract List<Brand> getBrandList(Long prodTypeId);
	
	/**
	 * Save brand item.
	 * 
	 * @param idList
	 *            the id list
	 * @param nsortId
	 *            the nsort id
	 * @param userName
	 *            the user name
	 * @return the string
	 */
	public abstract String saveBrandItem(List<String> idList, Long nsortId, String userName);
	
	/** 获取 品牌列表 **/
	public abstract List<Brand> queryAllBrand();
	
	/**获取所有品牌*/
	public abstract List<Brand> getAvailableBrands();

	/**
	 * 显示所有精品品牌
	 * @return
	 */
	public abstract List<Brand> getAllBrand();
	
	public abstract PageSupport<Brand> queryBrands(String curPageNO,Integer status);

	PageSupport<Brand> brand(String curPageNO);
	
	public abstract PageSupport<Brand> getBrandsPage(String curPageNO, String brandName);

	public abstract PageSupport<Brand> getDataByCriteriaQueryPage(String curPageNO, String brandName, Long proTypeId);

	public abstract PageSupport<Brand> getBrandsPage();

	public abstract PageSupport<Brand> getDataByCriteriaQuery(String userName, String brandName);

	public abstract PageSupport<Brand> getDataByPage(String curPageNO, String userName);

	//
	public abstract PageSupport<Brand> getDataByPageByUserId(String curPageNO, String userId);
	
	public abstract PageSupport<Brand> getDataByBrandName(String brandName, String userName);

	public abstract PageSupport<Brand> queryBrandListPage(String curPageNO, Brand brand, DataSortResult result);

	public abstract PageSupport<Brand> getBrandsByNamePage(String brandName);

	public abstract boolean checkBrandByName(String brandName, Long brandId);

	public abstract int batchDel(String ids);

	public abstract int batchOffOrOnLine(String ids,Integer brandStatus);

	List<BrandDto> likeBrandName(String brandName, Integer categoryId);

	public abstract boolean checkBrandByNameByShopId(String brandName, Long brandId, Long shopId);

	/**
	 * 获取导出品牌列表
	 * @param brand
	 * @return
	 */
	public abstract List<Brand> getExportBrands(Brand brand);

	/**
	 * 根据名字获取品牌列表
	 * @param name
	 * @return
	 */
	public abstract List<Brand> getBrandsByName(String name);
}