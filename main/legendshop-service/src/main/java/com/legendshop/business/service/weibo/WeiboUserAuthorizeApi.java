package com.legendshop.business.service.weibo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.dto.weibo.WeiboAuthorizeUserInfo;
import com.legendshop.model.dto.weibo.WeiboUserToken;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

/**
 * 微博用户授权认证相关API
 * @author 开发很忙
 */
public class WeiboUserAuthorizeApi {
	
	private static Logger log = LoggerFactory.getLogger(WeiboUserAuthorizeApi.class);

	/** 通过 code 换取 accesstoken */
	private final static String OAUTH2_TOKEN= "https://api.weibo.com/oauth2/access_token?client_id=CLIENT_ID&client_secret=CLIENT_SECRET&grant_type=GRANT_TYPE&code=CODE&redirect_uri=REDIRECT_URI";
	
	/** 获取用户信息 */
	private final static String USER_SHOW_= "https://api.weibo.com/2/users/show.json?access_token=ACCESS_TOKEN&uid=UID";
	
	/**
	 * 获取accssToken
	 * @param grantType
	 * @param clientId
	 * @param clientSecret
	 * @param code
	 * @param redirectURI
	 * @return
	 */
	public static WeiboUserToken getAccessToken(String grantType, String clientId, String clientSecret, String code, String redirectURI) {
		
        // 通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = OAUTH2_TOKEN.replace("GRANT_TYPE", grantType)
        		.replace("CLIENT_ID", clientId)
        		.replace("CLIENT_SECRET", clientSecret)
        		.replace("CODE", code)
        		.replace("REDIRECT_URI", redirectURI);
        
        String[] urls = url.split("\\?");
        
        String text = HttpUtil.httpsRequest(urls[0], "POST", urls[1]);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取微博用户 accessToken 失败 ####################");
        	return null;
        }
        
        WeiboUserToken token = JSONObject.parseObject(text, WeiboUserToken.class);

		return token;
	}
	
	/**
	 * 获取用户信息
	 * @param accessToken
	 * @param uid
	 * @return
	 */
	public static WeiboAuthorizeUserInfo getUserInfo(String accessToken, String uid) {
		
        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String url = USER_SHOW_.replace("ACCESS_TOKEN", accessToken)
        		.replace("UID", uid);
        
        String text = HttpUtil.httpsRequest(url, "GET", null);
        
        if (AppUtils.isBlank(text)) {
        	log.info("############## 获取微博用户信息 失败 ####################");
        	return null;
        }
        
        WeiboAuthorizeUserInfo userInfo = JSONObject.parseObject(text, WeiboAuthorizeUserInfo.class);

		return userInfo;
	}
}
