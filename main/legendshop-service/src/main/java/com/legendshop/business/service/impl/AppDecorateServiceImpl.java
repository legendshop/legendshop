/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AppDecorateDao;
import com.legendshop.model.entity.AppDecorate;
import com.legendshop.spi.service.AppDecorateService;
import com.legendshop.util.AppUtils;

/**
 * App装修.
 */
@Service("appDecorateService")
public class AppDecorateServiceImpl  implements AppDecorateService{
	
	@Autowired
    private AppDecorateDao appDecorateDao;

    public AppDecorate getAppDecorate(Long shopId) {
        return appDecorateDao.getAppDecorate(shopId);
    }

    public void deleteAppDecorate(AppDecorate appDecorate) {
        appDecorateDao.deleteAppDecorate(appDecorate);
    }

    public Long saveAppDecorate(AppDecorate appDecorate) {
        if (!AppUtils.isBlank(appDecorate.getId())) {
            updateAppDecorate(appDecorate);
            return appDecorate.getId();
        }
        return appDecorateDao.saveAppDecorate(appDecorate);
    }

    public void updateAppDecorate(AppDecorate appDecorate) {
        appDecorateDao.updateAppDecorate(appDecorate);
    }
}
