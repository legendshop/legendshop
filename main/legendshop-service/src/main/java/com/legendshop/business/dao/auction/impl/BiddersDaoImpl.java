/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.auction.BiddersDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.Bidders;
import com.legendshop.util.AppUtils;

/**
 * 竞拍Dao实现类.
 */
@Repository
public class BiddersDaoImpl extends GenericDaoImpl<Bidders, Long> implements BiddersDao {

	public Bidders getBidders(Long id) {
		return getById(id);
	}

	public int deleteBidders(Bidders bidders) {
		return delete(bidders);
	}

	public Long saveBidders(Bidders bidders) {
		return save(bidders);
	}

	public int updateBidders(Bidders bidders) {
		return update(bidders);
	}

	@Override
	public List<Bidders> getBiddersByAuctionId(Long auctionsId, int offset, int limit) {
		return queryLimit(
				"select b.id as id,b.a_id as aId,b.prod_id as prodId,b.sku_id as skuId,b.price as price,b.user_id as userId,b.bit_time as bitTime,d.nick_name AS nickName,d.user_name AS userName from ls_bidders b INNER JOIN ls_usr_detail d ON b.user_id=d.user_id where a_id=?  order by price desc,bit_time desc ",
				Bidders.class, offset, limit, auctionsId);
	}

	@Override
	public long findBidsCount(Long paimaiId) {
		return getLongResult("select count(id) from ls_bidders where a_id=? ", paimaiId);
	}

	@Override
	public List<Bidders> getBidders(Long aId, Long prodId, Long skuId) {
		StringBuffer sql = new StringBuffer(
				"SELECT b.a_id AS id,b.a_id AS aId,b.user_id as userId,b.prod_id AS prodId,b.sku_id AS skuId,b.bit_time AS bitTime,b.price AS price,d.nick_name AS nickName,d.user_name AS userName  FROM ls_bidders b LEFT JOIN ls_usr_detail d ON b.user_id=d.user_id where b.a_id=? and b.prod_id=? ");
		Object[] obj;
		if (AppUtils.isNotBlank(skuId)) {
			sql.append("and b.sku_id=? ");
			obj = new Object[] { aId, prodId, skuId };
		} else {
			obj = new Object[] { aId, prodId };
		}
		sql.append("order by b.price desc");
		return this.query(sql.toString(), Bidders.class, obj);
	}

	@Override
	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long id) {
		SimpleSqlQuery query = new SimpleSqlQuery(Bidders.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("aId", id);
		String querySQL = ConfigCode.getInstance().getCode("bidders.queryBidders", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("bidders.queryBiddersCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Bidders> queryBiddListPage(String curPageNO, Long id) {
		SimpleSqlQuery query = new SimpleSqlQuery(Bidders.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("aId", id);
		String querySQL = ConfigCode.getInstance().getCode("bidders.queryBidders", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("bidders.queryBiddersCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long aId, Long prodId, Long skuId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Bidders.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("aId", aId);
		map.put("prodId", prodId);
		if (AppUtils.isNotBlank(skuId)) {
			map.put("skuId", skuId);
		}
		String querySQL = ConfigCode.getInstance().getCode("bidders.queryBidders", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("bidders.queryBiddersCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Bidders> getBiddersPage(String curPageNO, Bidders bidders) {
		CriteriaQuery cq = new CriteriaQuery(Bidders.class, curPageNO);
		// cq.setPageSize(PropertiesUtil.getObject(SysParameterEnum.PAGE_SIZE,
		// Integer.class));
		// DataFunctionUtil.hasAllDataFunction(cq, request,
		// StringUtils.trim(cash.getUserName()));
		/*
		 * //TODO add your condition
		 */
		return queryPage(cq);
	}
}
