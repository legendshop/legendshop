/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.model.entity.MarketingRule;

/**
 * 营销活动规则Dao.
 */
public interface MarketingRuleDao extends GenericDao<MarketingRule, Long> {
     
	public abstract MarketingRule getMarketingRule(Long id);
	
    public abstract int deleteMarketingRule(MarketingRule marketingRule);
	
	public abstract Long saveMarketingRule(MarketingRule marketingRule);
	
	public abstract int updateMarketingRule(MarketingRule marketingRule);
	
	/**
	 * 获取查询符合该商品营销活动的规则信息
	 * @param shopId
	 * @param marketId
	 * @return
	 */
	public abstract List<MarketingRuleDto> findMarketingRuleByMarketingId(Long shopId, Long marketId);

	/**
	 * 根据活动删除规则
	 * @param marketingId
	 */
    void deleteMarketingRuleByMarketing(Long marketingId);
}
