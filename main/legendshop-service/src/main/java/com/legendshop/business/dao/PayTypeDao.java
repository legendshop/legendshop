/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PayType;

/**
 * 支付类型Dao.
 */
public interface PayTypeDao extends Dao<PayType, Long>{

	void updatePayType(PayType payType);

	void deletePayTypeById(Long id);

	/**
	 * 从缓存中读取
	 * @param id
	 * @return
	 */
	PayType getPayTypeById(Long id);
	/**
	 * 从数据库中读取
	 */
	PayType getPayTypeByIdFromDb(Long id);

	Integer getInterfaceType(Long shopId, String payTypeId);

	Long savePayType(PayType payType);

	PayType getPayTypeByPayTypeId(String payTypeId);

	Integer getInterfaceType(String payTypeId);

	List<PayType> getPayTypeList();

	PageSupport<PayType> getPayTypeListPage(String curPageNO);

	PageSupport<PayType> getPayTypeListPage(String curPageNO, String name);

	PayType getPayTypeById(String payTypeId);

}