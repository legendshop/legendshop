/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DepartmentDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.Department;

/**
 * 部门DaoImpl.
 */
@Repository
public class DepartmentDaoImpl extends GenericDaoImpl<Department, Long> implements DepartmentDao  {

	public Department getDepartment(Long id){
		return getById(id);
	}
	
    public int deleteDepartment(Department department){
    	return delete(department);
    }
	
	public Long saveDepartment(Department department){
		return save(department);
	}
	
	public int updateDepartment(Department department){
		return update(department);
	}

	/**
	 * 获取所有的部门
	 */
	@Override
	public List<Department> getAllDepartment() {
		return queryAll();
	}

	/**
	 * 根据ID来查找名字
	 */
	@Override
	public String getDepartmentName(Long deptId) {
		return get("select name from ls_department where id = ?", String.class, deptId);
	}
	
 }
