/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.NewsCategory;

/**
 * 新闻分类Dao.
 */
public interface NewsCategoryDao extends GenericDao<NewsCategory, Long>{

	void deleteNewsCategoryById(Long id);

	void updateNewsCategory(NewsCategory newsCategory);

	List<NewsCategory> getNewsCategoryList(String userName);
	
	List<NewsCategory> getNewsCategoryList();
	
	NewsCategory getNewsCategoryById(Long id);

	PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO, NewsCategory newsCategory);

	/**
	 * 获取新闻栏目
	 * @param curPageNO
	 * @return
	 */
	public PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO);
	
}