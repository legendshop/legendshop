/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.model.entity.SubFloorItem;

/**
 * 首页楼层服务
 */
public interface SubFloorItemDao extends GenericDao<SubFloorItem, Long>{
     
	public abstract SubFloorItem getSubFloorItem(Long id);
	
    public abstract void deleteSubFloorItem(SubFloorItem subFloorItem);
	
	public abstract Long saveSubFloorItem(SubFloorItem subFloorItem);
	
	public abstract void updateSubFloorItem(SubFloorItem subFloorItem);
	
	public abstract List<SubFloorItem> getAllSubFloorItem(Long sfId);
	
	public abstract List<SubFloorItem> getItemLimit(Long sfId);
	 
	public abstract void deleteSubFloorItemById(Long sfId);

	public abstract List<SubFloorItem> queryItem(List<SubFloor> subFloorList);

	public abstract void deleteSubFloorItemByFid(Long fId,String layout);
	
 }
