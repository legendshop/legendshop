/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.promoter.CommisChangeLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.promoter.model.CommisChangeLog;

/**
 * 佣金变更历史DaoImpl
 */
@Repository
public class CommisChangeLogDaoImpl extends GenericDaoImpl<CommisChangeLog, Long> implements CommisChangeLogDao {

	/**
	 * 获取佣金变更历史
	 */
	public CommisChangeLog getCommisChangeLog(Long id) {
		return getById(id);
	}

	/**
	 * 删除佣金变更历史 
	 */
	public int deleteCommisChangeLog(CommisChangeLog commisChangeLog) {
		return delete(commisChangeLog);
	}

	/**
	 * 保存佣金变更历史 
	 */
	public Long saveCommisChangeLog(CommisChangeLog commisChangeLog) {
		return save(commisChangeLog);
	}

	/**
	 * 更新佣金变更历史 
	 */
	public int updateCommisChangeLog(CommisChangeLog commisChangeLog) {
		return update(commisChangeLog);
	}

	/**
	 * 获取佣金变更历史 
	 */
	public PageSupport<CommisChangeLog> getCommisChangeLogByCq(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(CommisChangeLog.class, curPageNO);
		return queryPage(cq);
	}

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public CommisChangeLog getCommisChangeLog(String userName, String sn) {
		return this.getByProperties(new EntityCriterion().eq("userName", userName).eq("sn", sn));
	}

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public PageSupport<CommisChangeLog> getCommisChangeLog(int pageSize, String userId) {
		QueryMap map=new QueryMap();
		map.put("userId", userId);
		SimpleSqlQuery query=new SimpleSqlQuery(CommisChangeLog.class);
		query.setPageSize(pageSize);
		query.setCurPage("0");
		String sqlCount=ConfigCode.getInstance().getCode("commisChangeLog.queryLogCount",map);
		String sql=ConfigCode.getInstance().getCode("commisChangeLog.queryLog",map);
		query.setAllCountString(sqlCount);
		query.setQueryString(sql);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO, int pageSize, String userId) {
		QueryMap map=new QueryMap();
		map.put("userId", userId);//put方法内部已经做了非空判断,此处无需再做一次,下同
		SimpleSqlQuery query=new SimpleSqlQuery(CommisChangeLog.class,curPageNO);
		query.setPageSize(pageSize);
		query.setAllCountString(ConfigCode.getInstance().getCode("commisChangeLog.queryLogCount",map));
		query.setQueryString(ConfigCode.getInstance().getCode("commisChangeLog.queryLog",map));
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO, int pageSize, String userId, AwardSearchParamDto paramDto) {
		QueryMap map=new QueryMap();
		map.put("userId", userId);//put方法内部已经做了非空判断,此处无需再做一次,下同
		map.put("commisType", paramDto.getCommisType());
		map.put("addTime", paramDto.getStartTime());
		map.put("endTime", paramDto.getEndTime());
		SimpleSqlQuery query=new SimpleSqlQuery(CommisChangeLog.class,curPageNO);
		query.setPageSize(pageSize);
		query.setAllCountString(ConfigCode.getInstance().getCode("commisChangeLog.queryLogCount",map));
		query.setQueryString(ConfigCode.getInstance().getCode("commisChangeLog.queryLog",map));
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
