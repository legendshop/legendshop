/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopLayoutDivDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;

/**
 * The Class ShopLayoutDivDaoImpl.
 */
@Repository
public class ShopLayoutDivDaoImpl extends GenericDaoImpl<ShopLayoutDiv, Long> implements ShopLayoutDivDao  {
     
	public ShopLayoutDiv getShopLayoutDiv(Long id){
		return getById(id);
	}
	
    public int deleteShopLayoutDiv(ShopLayoutDiv shopLayoutDiv){
    	return delete(shopLayoutDiv);
    }
	
	public Long saveShopLayoutDiv(ShopLayoutDiv shopLayoutDiv){
		return save(shopLayoutDiv);
	}
	
	public int updateShopLayoutDiv(ShopLayoutDiv shopLayoutDiv){
		return update(shopLayoutDiv);
	}
	
	@Override
	public List<ShopLayoutDiv> getShopLayoutDivs(Long layoutId, Long shopId) {
		List<ShopLayoutDiv> layoutDivs= queryByProperties(new EntityCriterion().eq("layoutId", layoutId).eq("shopId", shopId));
		return layoutDivs;
	}

	@Override
	public List<ShopLayoutDiv> getShopLayoutDivsByShop(Long shopId, Long decId) {
		List<ShopLayoutDiv> layoutDivs= queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("shopDecotateId", decId));
		return layoutDivs;
	}

	/**
	 * 批量删除记录
	 */
	@Override
	public int deleteShopLayoutDivs(List<ShopLayoutDiv> delDivs) {
		return delete(delDivs);
	}

	/**
	 * 批量保存记录
	 */
	@Override
	public void saveShopLayoutDivs(List<ShopLayoutDiv> addDivs) {
		 save(addDivs);
	}

	@Override
	public void updateShopLayoutDivs(List<ShopLayoutDiv> updateDivs,Long shopId) {
		List<Object[]> objects = new ArrayList<Object[]>();
		for (ShopLayoutDiv layoutDiv : updateDivs) {
			// update ls_shop_layout
			objects.add(new Object[] { layoutDiv.getLayoutModuleType(), layoutDiv.getLayoutContent(), layoutDiv.getLayoutDivId(), shopId });
		}
		batchUpdate(
				"update  ls_shop_layout_div set layout_module_type=?,layout_content=?  where layout_div_id =? and shop_id=?  ", objects);
		objects = null;
		
	}

	@Override
	public int deleteShopLayoutDiv(Long shopId, Long layoutId) {
		return update("delete from ls_shop_layout_div where shop_id=? and layout_id=?  ", shopId, layoutId);
		
	}

	@Override
	public int deleteShopLayoutDiv(Long layoutDivId) {
		return deleteById(layoutDivId);
	}
	
	
 }
