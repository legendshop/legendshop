package com.legendshop.business.service.weixin;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.legendshop.model.dto.weixin.WeiXinMsg;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.dto.weixin.Code2sessionResult;
import com.legendshop.model.dto.weixin.WXACodeResultDto;
import com.legendshop.model.dto.weixin.WeixinMpAccessToken;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

/**
 * 微信小程序服务端的一些接口方法
 * @author 开发很忙
 */
public class WeixinMiniProgramApi {
	
	private static Logger log = LoggerFactory.getLogger(WeixinMiniProgramApi.class);
	
	public static final String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

	private static final String JSCODE2SESSION_API = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";
	
	private static final String API_GET_WXACODE_UNLIMIT_URL = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=ACCESS_TOKEN";

	/**
	 * 微信过滤敏感内容API
 	 */
	private  static final String MSG_SEC_CHECK = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=ACCESS_TOKEN";
	
	/**
	 * 获取微信公众号accessToken
	 * @param appId
	 * @param appSecret
	 * @return
	 */
	public static WeixinMpAccessToken getAccessToken(String appId, String appSecret) {
		
		String requestUrl = GET_TOKEN_URL.replace("APPID", appId)
				.replace("APPSECRET", appSecret);
		
		String text=HttpUtil.httpsRequest(requestUrl, "GET",null);
		if(null == text){
			return null;
		}
		
		WeixinMpAccessToken token =JSONObject.parseObject(text, WeixinMpAccessToken.class);
		
		return token;
	}

	/**
	 * 通过wx.login()接口获取到的code换取openId和sessionKey
	 * @param appId 小程序的APPID
	 * @param appSecret 
	 * @param code 小程序端wx.login()接口获取到的code
	 * @return
	 */
	public static Code2sessionResult jscode2session(String appId, String appSecret, String code) {
		
		String requestUrl = JSCODE2SESSION_API.replace("APPID", appId).replace("SECRET", appSecret)
				.replace("JSCODE", code);
		String text = HttpUtil.httpsRequest(requestUrl, "GET", null);
		if(AppUtils.isBlank(text)){
			log.info("############## 微信登录,失败={} ####################");
			return null;
		}

		return JSONObject.parseObject(text, Code2sessionResult.class);
	}
	
	/**
	 * 获取微信小程序码
	 * @param accessToken 
	 * @param scene
	 * @param page
	 * @param width
	 * @param autoColor
	 * @param lineColor
	 * @param isHyaline
	 * @return
	 */
    public static WXACodeResultDto getWXACodeUnlimit(String accessToken, String scene, String page, Double width, Boolean autoColor, String lineColor, Boolean isHyaline) {
		
		WXACodeResultDto result = new WXACodeResultDto();
		
		String requestUrl = API_GET_WXACODE_UNLIMIT_URL.replace("ACCESS_TOKEN", accessToken);
		
		log.info("    >>>>进入getWXACodeUnlimit方法,  requestUrl: {}", requestUrl);
		
		JSONObject params = new JSONObject();
		params.put("scene", scene);
		params.put("page", page);
		params.put("width", width);
		params.put("auto_color", autoColor);
		params.put("line_color", JSONObject.parse(lineColor));
		params.put("is_hyaline", isHyaline);
		
		log.info("        >>>>请求参数, params : {}", params.toJSONString());
		
		CloseableHttpClient httpclient = null;
		InputStream instreams  = null;
		BufferedReader bf = null;
		try {
			StringEntity stringEntity = new StringEntity(params.toJSONString(), "utf-8");  
			
			httpclient = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(requestUrl);
			httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json;charset=utf-8");
			httpPost.setEntity(stringEntity);  
			
			CloseableHttpResponse response = httpclient.execute(httpPost);
	
			HttpEntity resEntity = response.getEntity();
			Header header = resEntity.getContentType();
			instreams = resEntity.getContent();
		
			log.info("        >>>> 返回结果的contentType: {}", header);
			if(header.getValue().contains("application/json")){ //说明是json
				
				log.info("        >>>> 获取小程序码失败, 返回的是json");
				
				bf = new BufferedReader(new InputStreamReader(instreams, "utf-8"));    
				StringBuilder sb=new StringBuilder();
				String s=null;
				while ((s=bf.readLine())!=null) {
					sb.append(s+"\r\n");
				}
				
				log.info("获取小程序码失败  response:" + sb.toString());
				
				result.setSuccess(false);//标记为失败
				WXACodeResultDto.Errormsg error = JSONObject.parseObject(sb.toString(), WXACodeResultDto.Errormsg.class);
				result.setErrormsg(error);
				
			}else{ //说明是流
				
				log.info("        >>>> 获取小程序码成功, 返回的是流");
				
				result.setSuccess(true);//标记为成功
				
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				
				int count = 0;
				byte buf[] = new byte[1024 * 1024]; //定义你需要的缓存大小
				while ((count = instreams.read(buf)) != -1) { 
					output.write(buf, 0, count);  
					output.flush();
				}
				
				result.setOutputStream(output);
			}
			
			return result;
		} catch (Exception e) {
			
			log.error("", e);
			result.setSuccess(false);//标记为失败
			return result;
		}  finally {
			
			if(null != bf){
				try {
					bf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(null != instreams){
				try {
					instreams.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(null != httpclient){
				try {
					httpclient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			log.info("    >>>>getWXACodeUnlimit方法结束");
		}
	}

	/**
	 * 微信过滤敏感内容
	 * @param accessToken 接口调用凭证
	 * @param content 要检测的文本内容，长度不超过 500KB
	 * @return WeiXinMsg
	 */
	public static WeiXinMsg checkMsg(String accessToken,String content){
		String requestUrl = MSG_SEC_CHECK.replace("ACCESS_TOKEN", accessToken);

		JSONObject params = new JSONObject();
		params.put("content", content);

		String text=HttpUtil.httpsRequest(requestUrl, "POST",params.toJSONString());
		if(AppUtils.isBlank(text)){
			log.info("############## 调用微信过滤敏感信息接口失败,失败 ####################");
			return null;
		}
		return JSONObject.parseObject(text, WeiXinMsg.class);
	}

}
