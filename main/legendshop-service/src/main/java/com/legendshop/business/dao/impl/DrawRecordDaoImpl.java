/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DrawRecordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.draw.DrawRecord;
import com.legendshop.util.AppUtils;

/**
 * 活动抽奖记录Dao实现类.
 */
@Repository
public class DrawRecordDaoImpl extends GenericDaoImpl<DrawRecord, Long> implements DrawRecordDao  {

	public DrawRecord getDrawRecord(Long id){
		return getById(id);
	}
	
    public int deleteDrawRecord(DrawRecord drawRecord){
    	return delete(drawRecord);
    }
	
	public Long saveDrawRecord(DrawRecord drawRecord){
		return save(drawRecord);
	}
	
	public int updateDrawRecord(DrawRecord drawRecord){
		return update(drawRecord);
	}

	/**
	 * @Description: 根据活动ID删除抽奖记录
	 * @param actId
	 * @date 2016-4-20
	 */
	@Override
	public int delDrawRecordByActId(Long actId) {
		return update("DELETE FROM ls_draw_record  WHERE act_id= ?", actId);
	}

	/**
	 * @Description: TODO
	 * @param userId
	 * @param actId
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public Long getDrawRecordCount(String userId, Long actId) {
		String sql="select count(id) from ls_draw_record where user_id = ? and act_id = ?";
		return this.getLongResult(sql, userId,actId);
	}

	/**
	 * @Description: 根据用户、活动、时间返回活动记录数
	 * @param userId
	 * @param actId
	 * @param date
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public Long getDRCountByCurrentDay(String userId, Long actId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date nowDate = new Date();
		Date startDate = null;
		try {
			startDate = sdf.parse(sdf.format(nowDate));
		} catch (ParseException e) {
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		Date endDate = cal.getTime();
		String sql="select count(id) from ls_draw_record where user_id = ? and act_id = ? and awards_time >=? and awards_time<?";
		return this.getLongResult(sql, userId,actId,startDate,endDate);    
	}

	@Override
	public PageSupport<DrawRecord> queryDrawRecordPage(String curPageNO, DrawRecord drawRecord) {
		
		SimpleSqlQuery query = new SimpleSqlQuery(DrawRecord.class,20,curPageNO);
    	QueryMap map = new QueryMap();
 		map.put("actId", drawRecord.getActId());
 		if(AppUtils.isNotBlank(drawRecord.getStartTime())){
 			map.put("startTime", drawRecord.getStartTime());
 		}
 		
 		if(AppUtils.isNotBlank(drawRecord.getEndTime())){
 			map.put("endTime", drawRecord.getEndTime());
 		}
 		
 		if(AppUtils.isNotBlank(drawRecord.getWeixinNickname())){
 			map.put("weixinNickname", drawRecord.getWeixinNickname());
 		}
 		
 		if(AppUtils.isNotBlank(drawRecord.getWinStatus())){
 			if(drawRecord.getWinStatus()==0){ //未中奖
 				map.put("winStatus", "and (ldr.win_id is null)");
 			}else if(drawRecord.getWinStatus()==1){
 				map.put("winStatus", "and (ldr.win_id is not null)");
 			}
 		}
 		String querySQL = ConfigCode.getInstance().getCode("adminDraw.getDrawRecord", map);
 		String queryAllSQL =  ConfigCode.getInstance().getCode("adminDraw.getDrawRecordCount", map);
 		query.setAllCountString(queryAllSQL);
 		query.setQueryString(querySQL);
 		map.remove("winStatus");
 		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	
 }
