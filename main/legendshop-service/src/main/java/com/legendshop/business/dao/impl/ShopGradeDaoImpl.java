/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopGradeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.ShopGrade;
import com.legendshop.util.AppUtils;

/**
 * 用户等级Dao.
 */
@Repository
public class ShopGradeDaoImpl extends GenericDaoImpl<ShopGrade, Integer> implements ShopGradeDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public Integer saveShopGrade(ShopGrade shopGrade) {
		return  save(shopGrade);
	}

	@Override
	public ShopGrade getShopGrade(Integer id) {
		return getById(id);
	}

	@Override
	public void deleteShopGrade(ShopGrade shopGrade) {
			delete(shopGrade);
	}

	@Override
	public void updateShopGrade(ShopGrade shopGrade) {
			update(shopGrade);
	}

	@Override
	public List<StatusKeyValueEntity> retrieveShopGrade() {
		List<ShopGrade> shopGradeList = queryAll();
		List<StatusKeyValueEntity> list = new ArrayList<StatusKeyValueEntity>(shopGradeList.size());
		for (ShopGrade grade : shopGradeList) {
			list.add(new StatusKeyValueEntity(grade.getGradeId(),grade.getName()));
		}
		return list;
	}

	@Override
	public PageSupport<ShopGrade> getShopGradePage(String curPageNO, ShopGrade shopGrade) {
		CriteriaQuery cq = new CriteriaQuery(ShopGrade.class, curPageNO);
		if(AppUtils.isNotBlank(shopGrade.getName())) {			
			cq.like("name","%" + shopGrade.getName().trim() + "%");
		}
        cq.setPageSize(systemParameterUtil.getPageSize());
        cq.addDescOrder("gradeId");
		return queryPage(cq);
	}

}
