/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.LocationDao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.Region;
import com.legendshop.spi.service.LocationService;
import com.legendshop.util.AppUtils;

/**
 * 位置服务实现类
 */
@Service("locationService")
public class LocationServiceImpl  implements LocationService {
	
	@Autowired
	private LocationDao locationDao;

	/* (non-Javadoc)
	 * @see com.legendshop.spi.service.LocationService#loadProvinces()
	 */
	@Override
	public List<KeyValueEntity> loadProvinces() {
		List<Province>  provinces=locationDao.getAllProvince();
		if(AppUtils.isNotBlank(provinces)){
			List<KeyValueEntity> result = new ArrayList<KeyValueEntity>();
			for (Province province : provinces) {
				KeyValueEntity entity = new KeyValueEntity(province.getId(),province.getProvince());
				result.add(entity);
			}
			return result;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.service.LocationService#loadCities(java.lang.String)
	 */
	@Override
	public List<KeyValueEntity> loadCities(Integer provinceid) {
		return locationDao.getCitiesList(provinceid);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.service.LocationService#loadAreas(java.lang.String)
	 */
	@Override
	public List<KeyValueEntity> loadAreas(Integer cityid) {
		return locationDao.getAreaList(cityid);
	}

	@Override
	public List<Province> getAllProvince() {
		return locationDao.getAllProvince();
	}

	@Override
	public List<City> getCity(Integer provinceid) {
		return locationDao.getCity(provinceid);
	}

	@Override
	public List<Area> getArea(Integer cityid) {
		return locationDao.getArea(cityid);
	}

	@Override
	public Province getProvinceById(Integer id) {
		return locationDao.getProvinceById(id);
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void deleteProvince(Integer provinceid) {
	locationDao.deleteProvince(provinceid);	
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void deleteCity(Integer cityid) {
	locationDao.deleteCity(cityid);	
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void deleteArea(Integer areaid) {
	locationDao.deleteArea(areaid);
	}
	
	@Override
	public void deleteAreaList(List<Integer> idList) {
		locationDao.deleteAreaList(idList);
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void saveProvince(Province province) {
	locationDao.saveProvince(province);
	}

	@Override
	public City getCityById(Integer id) {
		return locationDao.getCityById(id);
	}

	@Override
	public Area getAreaById(Integer id) {
		return locationDao.getAreaById(id);
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void updateProvince(Province province) {
		locationDao.updateProvince(province);
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void updateCity(City city) {
		locationDao.updateCity(city);
		
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void updateArea(Area area) {
		locationDao.updateArea(area);
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void saveCity(City city) {
		locationDao.saveCity(city);
		
	}

	@Override
	@CacheEvict(value = "Region", allEntries = true)
	public void saveArea(Area area) {
		locationDao.saveArea(area);
		
	}
	

	@Override
	@Cacheable(value = "Region")
	public List<Region> getRegionList() {
		Map<String, Region> provinceMap = new HashMap<String, Region>();
		List<Province> provinceList = locationDao.getAllProvince();
		List<City> cityList = locationDao.getAllCity();
		
		for (Province province : provinceList) {
			for (City city : cityList) {
				province.addCity(city);
			}
		}
		
		for (Province province : provinceList) {
			Region r = provinceMap.get(province.getGroupby());
			if(r == null){
				r = new Region();
				r.setGroupby(province.getGroupby());
				provinceMap.put(province.getGroupby(), r);
			}
			r.addProvinces(province);
		}
		
		return	new ArrayList<Region>(provinceMap.values());
	}

	@Override
	public List<City> getAllCity() {
		return locationDao.getAllCity();
	}

	/* 
	 * 根据邮编获取
	 */
	@Override
	public Province getProvince(String provinceId) {
		return locationDao.getProvinceByProId(provinceId);
	}

	@Override
	public List<Province> getProvinceByName(String provinceName) {
		return locationDao.getProvinceByName(provinceName);
	}

	@Override
	public List<City> getCityByName(String cityName) {
		return locationDao.getCityByName(cityName);
	}



}
