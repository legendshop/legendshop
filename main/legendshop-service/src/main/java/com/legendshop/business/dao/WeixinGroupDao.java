/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinGroup;

/**
 * 微信组Dao
 */
public interface WeixinGroupDao extends Dao<WeixinGroup, Long> {
     
    public abstract List<WeixinGroup> getWeixinGroup(String GroupName);

    public abstract List<WeixinGroup> getWeixinGroup();
    
	public abstract WeixinGroup getWeixinGroup(Long id);
	
    public abstract int deleteWeixinGroup(WeixinGroup weixinGroup);
	
	public abstract Long saveWeixinGroup(WeixinGroup weixinGroup);
	
	public abstract int updateWeixinGroup(WeixinGroup weixinGroup);
	
 }
