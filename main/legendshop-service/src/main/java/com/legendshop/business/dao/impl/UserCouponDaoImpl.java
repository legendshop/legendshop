/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.UserCouponEnum;
import com.legendshop.model.dto.ProductCoupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.vo.UserCouponVo;
import com.legendshop.util.AppUtils;

/**
 * 用户优惠券Dao实现类.
 */
@Repository
public class UserCouponDaoImpl extends GenericDaoImpl<UserCoupon, Long> implements UserCouponDao  {
     
	private final static String getAllActiveUserCouponsSql = "SELECT u.*,c.end_date as endDate,c.start_date as startDate,c.shop_id as shopId,c.full_price as fullPrice,c.off_price as offPrice,c.category_id as categoryId," +
			"c.coupon_pic as couponPic,c.coupon_type as couponType,c.coupon_provider as couponProvider,s.site_name siteName FROM ls_coupon c join ls_user_coupon u on c.coupon_id = u.coupon_id " +
			"left join ls_shop_detail s on c.shop_id = s.shop_id WHERE u.use_status = 1  AND c.end_date >= ?  AND c.start_date <= ? AND u.user_id = ? ORDER BY c.coupon_id";
	
	private final static String getProdCouponListSql = "SELECT uc.user_coupon_id AS userCouponId,c.end_date as endDate,c.coupon_id AS couponId,c.coupon_name AS couponName,c.shop_id AS shopId,c.full_price AS fullPrice," +
			"c.off_price AS offPrice,c.coupon_pic AS couponPic,cp.prod_id AS prodId,sd.site_name AS siteName  FROM ls_coupon_prod cp, ls_coupon c, ls_user_coupon uc, ls_shop_detail sd " +
			" WHERE cp.coupon_id = c.coupon_id AND c.coupon_id = uc.coupon_id AND c.shop_id=sd.shop_id AND uc.use_status=1 AND c.coupon_type='product' AND c.coupon_provider='shop'" +
			" AND c.end_date >= ?  AND c.start_date <= ? AND uc.user_id = ? ";

		@Override
		public UserCoupon getUserCoupon(Long id){
			return getById(id);
		}
		
		/**
		 * 根据卡密查询用户优惠券
		 */
		@Override
		public UserCoupon getCouponByCouponPwd(String couponPwd){
			return this.get("SELECT * FROM ls_user_coupon WHERE coupon_pwd = ?", UserCoupon.class, couponPwd);
		}
		
		/**
		 * 获取用户已得到某种优惠券的数量
		 */
		@Override
		public int getUserCouponCount(String userId,long couponId){
			return this.get("SELECT COUNT(*) FROM ls_user_coupon WHERE user_id = ? AND coupon_id = ?",Integer.class,userId,couponId);
		}

		@Override
		public int getUserCouponCount(Long couponId) {
			return this.get("SELECT COUNT(DISTINCT user_id) FROM ls_user_coupon  where coupon_id=?",Integer.class,couponId);
		}

	    @Override
		public int deleteUserCoupon(UserCoupon userCoupon){
	    	return delete(userCoupon);
	    }
		
		@Override
		public Long saveUserCoupon(UserCoupon userCoupon){
			return save(userCoupon);
		}
		
		@Override
		public int updateUserCoupon(UserCoupon userCoupon){
			return update(userCoupon);
		}
		

		@Override
		public boolean hasCoupon(Long id) {
			int result = this.get("select count(1) from ls_user_coupon uc where uc.coupon_id = ?", Integer.class, id);
			return result > 0;
		}

		@Override
		public void batchSave(List<UserCoupon> userCoupon){
			this.save(userCoupon);
		}

		@Override
		public List<UserCoupon> getUserCouponByCoupon(Long couponId) {
			return this.queryByProperties(new EntityCriterion().eq("couponId", couponId));
		}
		
		@Override
		public List<String> getSendUsers(String sql,Object [] praObjects){
			return this.query(sql, String.class, praObjects);
		}

		private final static String SQL_EXPORT_EXCEL="SELECT ls_user_coupon.coupon_id as couponId,ls_user_coupon.coupon_name as couponName,ls_user_coupon.coupon_sn as couponSn FROM ls_user_coupon "
	                                                  +" LEFT JOIN ls_coupon ON ls_user_coupon.coupon_id=ls_coupon.coupon_id "	
	                                                  + " WHERE ls_user_coupon.coupon_id=? AND ls_coupon.send_type=1 AND ls_user_coupon.get_time IS NULL AND ls_user_coupon.use_time IS NULL ";
		
		@Override
		public List<UserCoupon> getOffExportExcelCoupon(Long couponId) {
			return this.query(SQL_EXPORT_EXCEL,UserCoupon.class,couponId);
		}

		@Override
		public void betchUpdate(List<UserCoupon> userCoupon) {
			update(userCoupon);
		}
		

		/**
		 * 更新用户优惠券
		 */
		@Override
		public void updateUserCouponUseSts(Long couponId, String userName, Double actualTotal, String subNumber) {
			List<UserCoupon> userCoupons = getUserCouponsByUser(userName,couponId);
			if(AppUtils.isNotBlank(userCoupons)){
				UserCoupon userCoupon = userCoupons.get(0);
				userCoupon.setUseStatus(UserCouponEnum.USED.value());
				userCoupon.setUseTime(new Date());
				userCoupon.setOrderPrice(actualTotal);
				userCoupon.setOrderNumber(subNumber);
				this.update(userCoupon);
			}
		}

		@Override
		public UserCoupon getCouponByCouponSn(String couponSn) {
			return this.get("SELECT * FROM ls_user_coupon WHERE coupon_sn = ?", UserCoupon.class, couponSn);
		}

		@Override
		public List<UserCoupon> getAllActiveUserCoupons(String userId) {
			Date nowDate = new Date();
			return this.query(getAllActiveUserCouponsSql, UserCoupon.class, nowDate, nowDate, userId);
		}

		@Override
		public List<ProductCoupon> getProdCouponList(String userId) {
			Date nowDate = new Date();
			return this.query(getProdCouponListSql, ProductCoupon.class, nowDate, nowDate, userId);
		}

		@Override
		public void deleteByCouponId(Long couponId) {
			update("delete from ls_user_coupon where coupon_id = ?", couponId);			
		}

		@Override
		public List<UserCoupon> queryUnbindCoupon(Long couponId) {
			String sql="SELECT uc.* FROM ls_user_coupon uc,ls_coupon c WHERE uc.coupon_id=c.coupon_id AND c.end_date >= ? AND c.status=1 AND uc.coupon_id=?  AND c.get_type='pwd' AND uc.user_name IS NULL";
			return this.query(sql,UserCoupon.class,new Date(),couponId);
		}

		@Override
		public List<UserDetail> findUserByUserMobile(List<String> result) {
			StringBuilder sb=new StringBuilder();
			sb.append("(");
			for (int i = 0; i < result.size(); i++) {
				if(i==result.size()-1){
					sb.append("?)");
					break;
				}
				sb.append("?,");
			}
			String newSql="SELECT u.user_id,u.user_name FROM ls_usr_detail u WHERE u.user_mobile IN "+sb.toString();
			return this.query(newSql,UserDetail.class,result.toArray());
		}

		@Override
		public Integer updateUserCoupon(List<UserCoupon> userCoupons) {
			return this.update(userCoupons);
		}

		@Override
		public PageSupport<UserCoupon> getUserCouponListPage(String curPageNO,Integer pageSize, Long id) {
			QueryMap map = new QueryMap();
			map.put("couponId", id);
			SimpleSqlQuery hql = new SimpleSqlQuery(UserCoupon.class);
			if(AppUtils.isBlank(pageSize)){
				hql.setPageSize(5);
			}else {
				hql.setPageSize(pageSize);
			}

			hql.setCurPage(curPageNO);
			hql.setParam(map.toArray());
			hql.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUserCouponCount", map));
			hql.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUserCoupon", map));
			return querySimplePage(hql);
		}

		@Override
		public PageSupport<UserCoupon> queryUserCoupon(String sqlString, StringBuilder sqlCount, List<Object> objects,
				String curPageNO) {
			SimpleSqlQuery hqlQuery= new SimpleSqlQuery(UserCoupon.class, 20,curPageNO==null?"1":curPageNO);
			hqlQuery.setQueryString(sqlString);
			hqlQuery.setAllCountString(sqlCount.toString());
			hqlQuery.setParam(objects.toArray());
			return querySimplePage(hqlQuery);
		}

		@Override
		public PageSupport<UserCouponVo> getUserCoupon(String curPageNO, StringBuilder sql, StringBuilder sqlCount,
				List<Object> objects) {
			SimpleSqlQuery hqlQuery= new SimpleSqlQuery(UserCouponVo.class, 30, curPageNO);
			hqlQuery.setQueryString(sql.toString());
			hqlQuery.setAllCountString(sqlCount.toString());
			hqlQuery.setParam(objects.toArray());
			return querySimplePage(hqlQuery);
		}

		@Override
		public List<String> getSendUsers(String curPageNO, StringBuilder sql, List<Object> objects) {
			SimpleSqlQuery hqlQuery= new SimpleSqlQuery(UserCouponVo.class, 30, curPageNO);
			hqlQuery.setQueryString(sql.toString());
			hqlQuery.setParam(objects.toArray());
			
			String ssql=hqlQuery.getQueryString();
			Object[] sparam=hqlQuery.getParam();
			
			return this.query(ssql, String.class, sparam);
		}

		@Override
		public PageSupport<UserCoupon> getUserCouponList(String curPageNO, Long id) {
			QueryMap map = new QueryMap();
			map.put("couponId", id);
			SimpleSqlQuery hql = new SimpleSqlQuery(UserCoupon.class);
			hql.setPageSize(5);
			hql.setCurPage(curPageNO);
			hql.setParam(map.toArray());
			hql.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUserCouponCount", map));
			hql.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUserCoupon", map));
			return querySimplePage(hql);
		}

		@Override
		public PageSupport<UserCoupon> getWatchRedPacket(Long id, String curPageNO) {
			QueryMap map = new QueryMap();
			map.put("couponId", id);
			SimpleSqlQuery hql = new SimpleSqlQuery(UserCoupon.class);
			hql.setPageSize(5);
			hql.setCurPage(curPageNO);
			hql.setParam(map.toArray());
			hql.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUserCouponCount", map));
			hql.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUserCoupon", map));
			return querySimplePage(hql);
		}

		@Override
		public UserCoupon getUserCouponBySn(String couponSn) {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT uc.*, lc.full_price AS fullPrice, lc.off_price AS offPrice, lc.coupon_provider AS couponProvider, ")
			.append("lc.category_id  AS categoryId, lc.coupon_pic AS couponPic, lc.start_date AS startDate, lc.end_date AS endDate, ")
			.append("lc.coupon_type AS couponType, lc.description AS description, ld.site_name AS siteName  ")
			.append("FROM ls_user_coupon uc, ls_coupon lc, ls_shop_detail ld  ")
			.append("WHERE uc.coupon_id = lc.coupon_id AND lc.shop_id = ld.shop_id ")
			.append("AND lc.status = 1 AND lc.start_date < ? AND lc.end_date > ? ")
			.append("AND uc.coupon_sn = ? ");
			return get(sql.toString(), UserCoupon.class, new Date(), new Date(), couponSn);
		}

		@Override
		public List<UserCoupon> queryUserCouponList(String userName, Long couponId) {
			return queryByProperties(new EntityCriterion().eq("userName", userName).eq("couponId", couponId).addDescOrder("getTime"));
		}

		/**
		 * 根据订单号查询用户优惠券
		 * 
		 */
		@Override
		public List<UserCoupon> queryByOrderNumber(String subNumber) {
			List<UserCoupon> userCoupons = queryByProperties(new EntityCriterion().eq("orderNumber", subNumber));
			return userCoupons;
		}

		@Override
		public List<UserCoupon> getUserCoupons(Long couponId, String userId) {
			
			return this.queryByProperties(new EntityCriterion().eq("couponId", couponId).eq("userId", userId));
		}

		@Override
		public List<UserCoupon> getUserCouponsByUser(String userName, Long couponId) {
			String sql="select * from ls_user_coupon where user_name=? and coupon_id=? and use_status=1";
			return query(sql,UserCoupon.class,userName,couponId);
		}

		@Override
		public List<UserCoupon> getUserCouponsByOrderNumber(String orderNumber) {
			String sql = "select * from ls_user_coupon where order_number like ?";
			return query(sql,UserCoupon.class,"%"+orderNumber+"%");
		}

		@Override
		public String getOrderNumById(Long userCouponId) {
			String sql="select order_number from ls_user_coupon where user_coupon_id=?";
			return get(sql,String.class,userCouponId);
		}
	
 }
