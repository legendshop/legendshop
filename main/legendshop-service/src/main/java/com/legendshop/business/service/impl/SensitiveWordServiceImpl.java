/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SensitiveWordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SensitiveWord;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.util.AppUtils;

/**
 *
 * 敏感字服务
 *
 */
@Service("sensitiveWordService")
public class SensitiveWordServiceImpl  implements SensitiveWordService{

	@Autowired
    private SensitiveWordDao sensitiveWordDao;

    public SensitiveWord getSensitiveWord(Long id) {
        return sensitiveWordDao.getSensitiveWord(id);
    }

    public void deleteSensitiveWord(SensitiveWord sensitiveWord) {
        sensitiveWordDao.deleteSensitiveWord(sensitiveWord);
    }

    public Long saveSensitiveWord(SensitiveWord sensitiveWord) {
        if (!AppUtils.isBlank(sensitiveWord.getSensId())) {
            updateSensitiveWord(sensitiveWord);
            return sensitiveWord.getSensId();
        }
        return (Long) sensitiveWordDao.save(sensitiveWord);
    }

    public void updateSensitiveWord(SensitiveWord sensitiveWord) {
        sensitiveWordDao.updateSensitiveWord(sensitiveWord);
    }
    
    /**
     * 
     * @param src 要过滤的语句
     */
	public Set<String> checkSensitiveWords(String src) {
		if(AppUtils.isBlank(src)){
			return null;
		}
		 List<String> sensitiveWordList = sensitiveWordDao.getWords();
		 Map<Character,List<String>> wordMap=new HashMap<Character,List<String>>();
	        for (String s:sensitiveWordList){
	            char c=s.charAt(0);
	            List<String> strs=wordMap.get(c);
	            if (strs==null){
	                strs=new ArrayList<String>();
	                wordMap.put(c,strs);
	            }
	            strs.add(s);
	        }
	        String temp=null;
	        String find;
	        char c;
	        Set<String> findwords = new HashSet<String>();
	        for (int i=0;i<src.length();i++){
	        		c=src.charAt(i);
	             	find=null;
	            if (wordMap.containsKey(c)){
	                List<String> words=wordMap.get(c);
	                for (String s:words){
	                    temp=src.substring(i,(s.length()<=(src.length()-i))?i+s.length():i);
	                    if (s.equals(temp)){
	                        find=s;
	                        break;
	                    }
	                }
	            }
	            if (find!=null && findwords.size()<=10){           	
	            	findwords.add(find);
	                i+=(find.length()-1);  
	            }
	        }  
	        
		return findwords;
	}

	@Override
	public boolean checkSensitiveWordsNameExist(String nickName) {
		return sensitiveWordDao.checkSensitiveWordsNameExist(nickName);
	}

	@Override
	public PageSupport<SensitiveWord> getSensitiveWord(String curPageNO, SensitiveWord sensitiveWord) {
		return sensitiveWordDao.getSensitiveWord(curPageNO,sensitiveWord);
	}

	public static void main(String[] args) {
		String src = "共产党123";

		String[] sensitiveWordList = new String[]{"共产党"};
		Map<Character,List<String>> wordMap=new HashMap<Character,List<String>>();
		for (String s:sensitiveWordList){
			char c=s.charAt(0);
			List<String> strs     =wordMap.get(c);
			if (strs==null){
				strs=new ArrayList<String>();
				wordMap.put(c,strs);
			}
			strs.add(s);
		}
		String temp=null;
		String find;
		char c;
		Set<String> findwords = new HashSet<String>();
		for (int i=0;i<src.length();i++){
			c=src.charAt(i);
			find=null;
			if (wordMap.containsKey(c)){
				List<String> words=wordMap.get(c);
				for (String s:words){
					temp=src.substring(i,(s.length()<=(src.length()-i))?i+s.length():i);
					if (s.equals(temp)){
						find=s;
						break;
					}
				}
			}
			if (find!=null && findwords.size()<=10){
				findwords.add(find);
				i+=(find.length()-1);
			}
		}

		System.out.println(findwords);

	}
}
