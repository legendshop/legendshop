/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import com.legendshop.business.dao.AdminDashboardDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.constant.DashboardRecDateEnum;
import com.legendshop.model.constant.DashboardTableEnum;
import com.legendshop.model.constant.DashboardTypeEnum;
import com.legendshop.model.entity.AdminDashboard;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 计算后台的首页报表
 */
@Repository
public class AdminDashboardDaoImpl extends GenericDaoImpl<AdminDashboard, String> implements AdminDashboardDao  {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AdminDashboardDaoImpl.class);
	
	
	private void updateRecord(int count, String dashboardName,Date date){
		int isExist = get("select count(1) from ls_admin_dashboard where dashboard_name = ?",Integer.class,dashboardName);
		if(isExist == 0){
			update("insert into ls_admin_dashboard (dashboard_name,value,rec_date) value(?,?,?)",dashboardName,count,date);
		}else{
			update("update ls_admin_dashboard set value = ?,rec_date = ? where dashboard_name = ?",count,date,dashboardName);
		}
	}
     
    private int getCounts(String tableName,String recDateName,Date date){
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    	
    	Date startDate = null;
		try {
			startDate = sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			log.error("getCounts",e);
		}
    	
    	Date endDate = DateUtils.getDay(startDate, 1);
    	
    	StringBuffer sb = new StringBuffer("select count(1) as prodCounts from ");
    	sb.append(tableName);
    	if(AppUtils.isBlank(recDateName)){
    		return get(sb.toString(),Integer.class);
    	}
    	sb.append(" where "+ recDateName +" >= ? and "+ recDateName +" < ? ");
    	return get(sb.toString(),Integer.class,startDate,endDate);
    }

	@Override
	public void calAdminDashboard(DashboardTypeEnum typeEnum) {
		Date date = new Date();
		updateRecord(getNumbers(typeEnum,date),typeEnum.value(),date);
	}
	


	private int getNumbers(DashboardTypeEnum typeEnum,Date date){
		
		//用户数量
		if(typeEnum.value() == DashboardTypeEnum.USER_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.USER_TABLE.value(),DashboardRecDateEnum.USER_TABLE.value(),date);
			
		}
		
		//店铺数量
		if(typeEnum.value() == DashboardTypeEnum.SHOP_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.SHOP_TABLE.value(),DashboardRecDateEnum.SHOP_TABLE.value(),date);
			
		}
		
		//商品数量
		if(typeEnum.value() == DashboardTypeEnum.PROD_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.PROD_TABLE.value(),DashboardRecDateEnum.PROD_TABLE.value(),date);
			
		}
		
		//订单数量
		if(typeEnum.value() == DashboardTypeEnum.SUB_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.SUB_TABLE.value(),DashboardRecDateEnum.SUB_TABLE.value(),date);
			
		}
		
		//咨询数量
		if(typeEnum.value() == DashboardTypeEnum.CONSULT_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.CONSULT_TABLE.value(),DashboardRecDateEnum.CONSULT_TABLE.value(),date);
			
		}
		
		//举报数量
		if(typeEnum.value() == DashboardTypeEnum.ACCUSATION_COUNTS.value()){
			
			return getCounts(DashboardTableEnum.ACCUSATION_TABLE.value(),DashboardRecDateEnum.ACCUSATION_TABLE.value(),date);
			
		}
		
		//品牌数量  暂时不需要了
//		if(typeEnum.value() == DashboardTypeEnum.BRAND_COUNTS.value()){
//			
//			return getCounts(DashboardTableEnum.BRAND_TABLE.value(),DashboardRecDateEnum.BRAND_TABLE.value(),date);
//			
//		}
		
		return 0;
		
	}
	
	@Override
	public List<AdminDashboard> queryAdminDashboard() {
		return this.query("select dashboard_name as dashboardName , value as value from ls_admin_dashboard", AdminDashboard.class);
	}
	
	
 }
