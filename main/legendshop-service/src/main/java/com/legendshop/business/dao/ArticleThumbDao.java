/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleThumb;

/**
 * The Class ArticleThumbDao.
 * Dao接口
 */
public interface ArticleThumbDao extends Dao<ArticleThumb, Long> {

   	/**
	 *  根据Id获取
	 */
	abstract ArticleThumb getArticleThumb(Long id);
	
   /**
	 *  删除
	 */
    abstract int deleteArticleThumb(ArticleThumb articleThumb);
    
   /**
	 *  保存
	 */	
	abstract Long saveArticleThumb(ArticleThumb articleThumb);

   /**
	 *  更新
	 */		
	abstract int updateArticleThumb(ArticleThumb articleThumb);
	
    PageSupport<ArticleThumb> saveThumb(Long artId, String userId);
	
 }
