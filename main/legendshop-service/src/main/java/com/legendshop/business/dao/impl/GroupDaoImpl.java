/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.GroupDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.ActivitySearchTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Group;
import com.legendshop.util.AppUtils;

/**
 * 团购Dao实现类.
 */
@Repository
public class GroupDaoImpl extends GenericDaoImpl<Group, Long> implements GroupDao {
	
	@Autowired
	private SubDao subDao;

	public Group getGroup(Long id) {
		return getById(id);
	}

	public int deleteGroup(Group group) {
		return delete(group);
	}

	public Long saveGroup(Group group) {
		return save(group);
	}

	public int updateGroup(Group group) {
		return update(group);
	}

	public PageSupport<Group> getGroup(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public Group getGroup(Long shopId, Long id) {
		return get("select * from ls_group where shop_id=? and id=? ", Group.class, shopId, id);
	}

	/** The sql for get navigation nsort. */
	private static String sqlGetCategory = "select n.id as id, n.parent_id as parentId, n.name as name ,n.pic as pic ,n.status  as status, n.type_id as typeId,n.keyword as keyword,n.cat_desc as catDesc,n.title as title,n.grade as grade,n.seq as seq from ls_category n  where  n.status = 1 and  n.type= ? order by n.grade,n.seq";

	@Override
	public List<Category> findgroupCategory() {
		List<Category> categories = query(sqlGetCategory, Category.class, ProductTypeEnum.GROUP.value());
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		// 组装树
		List<Category> list = parseCategory(categories);
		return list;
	}

	/**
	 * 得到父节点
	 * 
	 * @param menuList
	 * @param menu
	 * @return
	 */
	private Category getParentMenu(Collection<Category> menuList, Category menu) {
		for (Category item : menuList) {
			if (item == null) {
				continue;
			}
			if (item.getId().equals(menu.getParentId())) {
				return item;
			}
		}
		return null;
	}

	/**
	 * 按照3级菜单来组装
	 * 
	 * @param menuList
	 * @return
	 */
	private List<Category> parseCategory(List<Category> menuList) {
		Map<Long, Category> menuMap = new LinkedHashMap<Long, Category>();
		for (Iterator<Category> iterator = menuList.iterator(); iterator.hasNext();) {
			Category menu = (Category) iterator.next();
			if (AppUtils.isBlank(menu)) {
				continue;
			}
			if (menu.getGrade() == 1) { // for 顶级菜单
				menu.setChildrenList(new ArrayList<Category>());
				menuMap.put(menu.getId(), menu);
			} else if (menu.getGrade() == 2) { // 二级菜单
				// 拿到一级菜单先
				Category menuLevel1 = menuMap.get(menu.getParentId());
				if (AppUtils.isBlank(menuLevel1)) {
					continue;
				}
				menu.setParentName(menuLevel1.getName());
				menu.setParentId(menuLevel1.getId());
				menuLevel1.addChildren(menu);
			} else if (menu.getGrade() == 3) { // 三级菜单
				// 拿到二级菜单
				Category secondMenu = getParentMenu(menuList, menu);
				if (secondMenu != null) {

					menu.setParentName(secondMenu.getName());
					menu.setParentId(secondMenu.getId());
					// 拿到一级菜单先
					Category menuLevel1 = menuMap.get(secondMenu.getParentId());
					if (menuLevel1 == null) {
						continue; // 可能是由于上下线的关系
					}
					List<Category> menuLevel2 = menuLevel1.getChildrenList();
					for (Category menu2 : menuLevel2) {
						if (menu2.getId().equals(menu.getParentId())) {
							// 找到对应的二级菜单
							menu2.addChildren(menu);
							break;
						}
					}
				}
			}
		}
		return new ArrayList<Category>(menuMap.values());
	}

	@Override
	public void updataStatus(Long id, Long status) {
		this.update("UPDATE ls_group SET STATUS=? WHERE id= ?", status, id);
	}

	@Override
	public PageSupport<Group> getGroupPage(String curPageNO, Group group) {
		
		SimpleSqlQuery query = new SimpleSqlQuery(Group.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		
		if (AppUtils.isNotBlank(group.getGroupName())) {
			map.like("groupName", "%" + group.getGroupName() + "%");
		}
		if (AppUtils.isNotBlank(group.getShopId())) {
			map.put("shopId", group.getShopId());
		}
		
		if (AppUtils.isNotBlank(group.getStartTime())) {
			
			map.put("startTime", group.getStartTime());
		}
		if (AppUtils.isNotBlank(group.getEndTime())) {
			
			map.put("endTime", group.getEndTime());
		}
		
		
		// 商家活动列表查询，只查询没被商家删除的活动
		if (AppUtils.isBlank(group.getFlag())) {
			map.put("deleteStatus", "AND g.delete_status != 1");
		}

		//根据searchType,组装不同的筛选条件
		String tab = group.getSearchType();
		if (AppUtils.isNotBlank(tab)) {
			Date currDate = new Date();
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			switch(ActivitySearchTypeEnum.matchType(tab)){
	         	case WAIT_AUDIT :
	         		map.put("status", GroupStatusEnum.VALIDATING.value());
	         		map.put("unFinished", "AND g.end_time > "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case NOT_PASS:
	         		map.put("status", GroupStatusEnum.FAILED.value());
	         		map.put("unFinished", "AND g.end_time > "+ "'" +  sd.format(currDate) + "'");
		        	break;
	         	case NOT_STARTED :
	         		map.put("status", GroupStatusEnum.ONLINE.value());
	         		map.put("unStarted", "AND g.start_time > "+"'" +  sd.format(currDate) + "'");
		            break;
	         	case ONLINE :
	         		map.put("status", GroupStatusEnum.ONLINE.value());
	         		map.put("isStarted", "AND g.start_time < "+"'" +  sd.format(currDate) + "'");
	         		map.put("unFinished", "AND g.end_time > "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case FINISHED :
	         		map.put("isFinished", "AND g.end_time < "+ "'" +  sd.format(currDate) + "'");
		            break;
	         	case EXPIRED :
	         		map.put("status", GroupStatusEnum.OFFLINE.value());
	         		map.put("unFinished", "AND g.end_time > "+ "'" +  sd.format(currDate) + "'");
	         		break;
	         	default :
			}
		}
		
		String queryAllSQL = ConfigCode.getInstance().getCode("group.queryGroupList", map);
		String querySQL = ConfigCode.getInstance().getCode("group.queryGroupListCount", map);
		query.setQueryString(queryAllSQL);
		query.setAllCountString(querySQL);
		
		map.remove("isStarted");
		map.remove("unStarted");
		map.remove("isFinished");
		map.remove("unFinished");
		map.remove("deleteStatus");
		
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Group> queryGouplist(String curPageNO, GroupProdSeachParam param) {
		SimpleSqlQuery query = new SimpleSqlQuery(Group.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		String nowDate = DateUtils.format(new Date());
		
		if(AppUtils.isNotBlank(param.getFirstCid())){
			map.put("firstCatId", param.getFirstCid());
		}
		
		map.put("timeQuery", "and g.start_time <'" + nowDate + "' and g.end_time >'" + nowDate + "'");
		map.put("groupStatus", GroupStatusEnum.ONLINE.value());
		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		
		if (AppUtils.isNotBlank(param.getOrders())) {
			String[] orders = param.getOrders().split(",");
			if (orders != null && orders.length == 2) {
				map.put("orderByAndDir", "order by g." + orders[0] + " " + orders[1]);
			}
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("group.getGroupProdList", map);
		String querySQL = ConfigCode.getInstance().getCode("group.getGroupProdCount", map);
		
		map.remove("timeQuery");
		map.remove("orderByAndDir");
		query.setQueryString(queryAllSQL);
		query.setAllCountString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Group> queryGroupPage(String curPageNO, Integer firstCid, Integer twoCid, Integer thirdCid) {
		SimpleSqlQuery query = new SimpleSqlQuery(Group.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(firstCid)){
			map.put("firstCatId", firstCid);
		}
		if(AppUtils.isNotBlank(twoCid)){
			map.put("secondCatId", twoCid);
		}
		if(AppUtils.isNotBlank(thirdCid)){
			map.put("thirdCatId", thirdCid);
		}
		String nowDate = DateUtils.format(new Date());
		map.put("timeQuery", "and g.start_time <'" + nowDate + "' and g.end_time >'" + nowDate + "'");
		map.put("groupStatus", GroupStatusEnum.ONLINE.value());
		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		String queryAllSQL = ConfigCode.getInstance().getCode("group.getGroupProdList", map);
		String querySQL = ConfigCode.getInstance().getCode("group.getGroupProdCount", map);
		map.remove("timeQuery");
		query.setQueryString(queryAllSQL);
		query.setAllCountString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Group> queryGroupList(String curPageNO, GroupProdSeachParam param) {
		SimpleSqlQuery query = new SimpleSqlQuery(Group.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		String nowDate = DateUtils.format(new Date());
		List<Object> params = new ArrayList<Object>();
		if (!AppUtils.isBlank(param.getFirstCid())) {
			map.put("firstCatId", param.getFirstCid());
			params.add(param.getFirstCid());
		}
		if (!AppUtils.isBlank(param.getTwoCid())) {
			map.put("secondCatId", param.getTwoCid());
			params.add(param.getTwoCid());
		}
		if (!AppUtils.isBlank(param.getThirdCid())) {
			map.put("thirdCatId", param.getThirdCid());
			params.add(param.getThirdCid());
		}
		if (param.getTimeTab() == 1) {// 正在进行
			map.put("timeQuery", "and g.start_time <= ? and  g.end_time >= ? ");
			params.add(nowDate.toString());
			params.add(nowDate.toString());
			map.put("groupStatus", GroupStatusEnum.ONLINE.value());
			params.add(GroupStatusEnum.ONLINE.value());
		} else if (param.getTimeTab() == 0) {// 即将开始
			map.put("timeQuery", "and g.start_time >= ? ");
			params.add(nowDate.toString());
			map.put("groupStatus", GroupStatusEnum.ONLINE.value());
			params.add(GroupStatusEnum.ONLINE.value());
		} else if (param.getTimeTab() == 2) {// 已经结束, 24小时内的才显示
			map.put("timeQuery", " and g.end_time <= ? AND TIMESTAMPDIFF(HOUR,g.end_time,NOW()) < 24  ");
			params.add(nowDate.toString());
		}

		map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
		params.add(ProductStatusEnum.PROD_ONLINE.value());
		if (AppUtils.isNotBlank(param.getOrders())) {
			String[] orders = param.getOrders().split(",");
			if (orders != null && orders.length == 2) {
				map.put("orderByAndDir", "order by g." + orders[0] + " " + orders[1]);
			}
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("group.getGroupProdList", map);
		String querySQL = ConfigCode.getInstance().getCode("group.getGroupProdCount", map);
		map.remove("orderByAndDir");
		query.setQueryString(queryAllSQL);
		query.setAllCountString(querySQL);
		query.setParam(params.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Group> getGroupPage(String curPageNO, DataSortResult result, Group group) {
		CriteriaQuery cq = new CriteriaQuery(Group.class, curPageNO);
		cq.setPageSize(10);
		if(AppUtils.isNotBlank(group.getStatus())){
			cq.eq("status", group.getStatus());
			cq.gt("endTime", new Date());
		}
		if(AppUtils.isNotBlank(group.getShopId())) {
			cq.eq("shopId", group.getShopId());			
		}
		if(AppUtils.isNotBlank(group.getShopName())) {
			cq.like("shopName", group.getShopName().trim(), MatchMode.ANYWHERE);			
		}
		if(AppUtils.isNotBlank(group.getGroupName())) {			
			cq.like("groupName", group.getGroupName().trim(), MatchMode.ANYWHERE);
		}
		if (AppUtils.isNotBlank(group.getOverdue())) {// 查询已过期
			cq.lt("endTime", group.getOverdue());
		}

		cq.gt("startTime", group.getStartTime());
		cq.lt("endTime", group.getEndTime());
		if (!result.isSortExternal()) {
			cq.addDescOrder("startTime");
			cq.addDescOrder("endTime");
		}else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		return queryPage(cq);
	}

	@Override
	public boolean findIfJoinGroup(Long productId) {
		String sql="SELECT COUNT(1) FROM ls_group WHERE ls_group.status=1 AND ls_group.product_id=? AND  ls_group.end_time>=? ";
		return get(sql, Integer.class, productId,new Date())>0;
	}

	/**
	 * 逻辑删除团购活动
	 */
	@Override
	public void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus) {
		
		String sql = "UPDATE ls_group SET delete_status = ? WHERE shop_id = ? AND id = ?";
		update(sql, deleteStatus,shopId,id);
	}

	/**
	 * 获取团购交易总金额
	 */
	@Override
	public OperateStatisticsDTO getTotalAmount(Long groupId) {
		
		String sql = "SELECT COALESCE(SUM(s.actual_total),0) AS totalAmount,g.buyer_count AS participants,g.people_count AS peopleCount "
					+"FROM ls_sub s INNER JOIN ls_group g ON s.active_id = g.id WHERE s.sub_type = 'GROUP' AND  s.active_id = ?";
		
		return get(sql, OperateStatisticsDTO.class, groupId);
	}

	/**
	 * 获取活动交易成功的金额
	 */
	@Override
	public Double getSucceedAmount(Long groupId) {
		
		String sql = "SELECT COALESCE(SUM(s.actual_total),0) AS succeedAmount "
				+"FROM ls_sub s LEFT JOIN ls_group g ON s.active_id = g.id WHERE s.group_status != -2 AND s.active_id = ?";
		return get(sql, Double.class, groupId);
	}

}
