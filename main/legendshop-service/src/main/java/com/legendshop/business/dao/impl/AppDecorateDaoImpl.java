/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AppDecorateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.AppDecorate;

/**
 * The Class AppDecorateDaoImpl.
 */
@Repository
public class AppDecorateDaoImpl extends GenericDaoImpl<AppDecorate, Long> implements AppDecorateDao  {
     
	public AppDecorate getAppDecorate(Long shopId){
		return this.getByProperties(new EntityCriterion().eq("shopId", shopId));
	}
	
    public int deleteAppDecorate(AppDecorate appDecorate){
    	return delete(appDecorate);
    }
	
	public Long saveAppDecorate(AppDecorate appDecorate){
		return save(appDecorate);
	}
	
	public int updateAppDecorate(AppDecorate appDecorate){
		return update(appDecorate);
	}
 }
