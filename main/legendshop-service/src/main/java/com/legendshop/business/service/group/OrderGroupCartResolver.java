/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.entity.Group;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.util.AppUtils;

/**
 * 商品团购下单.
 */
@Service("orderGroupCartResolver")
public class OrderGroupCartResolver extends AbstractAddOrder implements IOrderCartResolver {

	/**
	 * 添加购物车
	 * (non-Javadoc)
	 *
	 * @see com.legendshop.spi.resolver.order.IOrderCartResolver#queryOrders(java.util.Map)
	 */
	@Override
	public UserShopCartList queryOrders(Map<String, Object> params) {
		String userId = (String) params.get("userId");
		String userName = (String) params.get("userName");
		Long groupId = (Long) params.get("groupId");
		Long productId = (Long) params.get("productId");
		Long skuId = (Long) params.get("skuId");
		Integer number = (Integer) params.get("number");
		Long adderessId = (Long) params.get("adderessId");//用户收货地址

		/**
		 * 查询团购的购物车信息
		 * 团购没有购物车，直接查找团购商品
		 */
		ShopCartItem shopCartItem = basketService.findGroupCart(groupId, productId, skuId);
		if (shopCartItem == null) {
			throw new BusinessException("数据发送改变,请重新下单购物！");
		}
		shopCartItem.setBasketCount(number);
		List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>(1);
		cartItems.add(shopCartItem);
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, cartItems, adderessId, CartTypeEnum.GROUP.toCode());
		if (userShopCartList == null) {
			return null;
		}
		userShopCartList.setType(CartTypeEnum.GROUP.toCode());
		return userShopCartList;
	}


	/**
	 * 添加订单
	 * (non-Javadoc)
	 *
	 * @see com.legendshop.spi.resolver.order.IOrderCartResolver#addOrder(com.legendshop.model.dto.buy.UserShopCartList)
	 */
	@Override
	public List<String> addOrder(UserShopCartList userShopCartList) {
		List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
		List<String> subIds = super.submitOrder(userShopCartList);
		if (AppUtils.isBlank(subIds)) {
			return null;
		}
		// 校验团购是否被取消
		Long activeId = userShopCartList.getActiveId();
		Group group = groupService.getGroup(activeId);
		if (group == null || group.getStatus() != 1) {
			PaymentLog.error("团购活动状态失效，请重新提交订单 activeId：{}", activeId);
			throw new BusinessException("团购活动状态失效，请重新提交订单 ", ErrorCodes.BUSINESS_ERROR);
		}
		return subIds;
	}

}
