/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.GrassComm;

/**
 * The Class DiscoverCommeDao. 发现文章评论表Dao接口
 */
public interface DiscoverCommeDao extends Dao<DiscoverComme,Long>{

	/**
	 * 根据Id获取发现文章评论表
	 */
	public abstract DiscoverComme getDiscoverComme(Long id);
	
	/**
	 * 根据DisId获取发现文章评论表
	 */
	public abstract List<DiscoverComme> getDiscoverCommeByDisId(Long disId);

	/**
	 *  根据Id删除发现文章评论表
	 */
    public abstract int deleteDiscoverComme(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteDiscoverComme(DiscoverComme discoverComme);

	/**
	 * 保存发现文章评论表
	 */
	public abstract Long saveDiscoverComme(DiscoverComme discoverComme);

	/**
	 *  更新发现文章评论表
	 */		
	public abstract int updateDiscoverComme(DiscoverComme discoverComme);

	/**
	 * 分页查询发现文章评论表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<DiscoverComme> queryDiscoverComme(String curPageNO, Integer pageSize);

	/**
	 * 根据发现文章Id分页查询文章评论表
	 * @param currPage
	 * @param pageSize
	 * @param disId 发现文章id
	 * @return
	 */
	public abstract PageSupport<DiscoverComme> queryDiscoverCommeByGrassId(String currPage, int pageSize, Long disId);

}
