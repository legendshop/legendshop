package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTypePropertyDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProdTypeProperty;

@Repository
public class ProdTypePropertyDaoImpl extends GenericDaoImpl<ProdTypeProperty, Long> implements ProdTypePropertyDao {

	@Override
	public void deleteProdTypeProperty(ProdTypeProperty prodTypeProperty) {
		delete(prodTypeProperty);
	}

	@Override
	public Long saveProdTypeProperty(ProdTypeProperty prodTypeProperty) {
		return save(prodTypeProperty);
	}

	@Override
	public List<Long> saveProdTypeProperty(List<ProdTypeProperty> prodTypePropertyList) {
		return save(prodTypePropertyList);
	}

	@Override
	public ProdTypeProperty getProdTypeProperty(Long id) {
		return getById(id);
	}

	@Override
	public void updateProdTypeProperty(ProdTypeProperty prodTypeProperty) {
		this.update(prodTypeProperty);
	}

	@Override
	public ProdTypeProperty getProdTypeProperty(Long propId, Long typeId) {
		return get("select ptp.* from ls_prod_type_prop ptp, ls_prod_prop lpp where ptp.prop_id=lpp.prop_id and ptp.type_id = ? and ptp.prop_id = ?",ProdTypeProperty.class,typeId,propId);
	}

}
