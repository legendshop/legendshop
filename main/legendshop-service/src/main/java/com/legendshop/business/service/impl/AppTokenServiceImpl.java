package com.legendshop.business.service.impl;

import com.legendshop.business.dao.AppTokenDao;
import com.legendshop.spi.service.AppTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("AppTokenService")
public class AppTokenServiceImpl implements AppTokenService {

	@Autowired
	private AppTokenDao appTokenDao;

	@Override
	public boolean deleteAppToken(String userId) {

		return appTokenDao.deleteAppToken(userId);
	}
}