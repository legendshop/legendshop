package com.legendshop.business.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.ThirdLoginService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

@Service("thirdLoginService")
public class ThirdLoginServiceImpl implements ThirdLoginService {
	
	@Autowired
	private PassportService passportService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private CacheClient cacheClient;

	@Override
	public ThirdUserAuthorizeResult authThridUser(ThirdUserInfo thridUserInfo, String passportType,
			String passportSource) {
		
		ThirdUserAuthorizeResult result = new ThirdUserAuthorizeResult();
		
		PassportFull passportFull = passportService.getPassportByOpenId(thridUserInfo.getOpenId());
		if(null == passportFull){
			
			// 如果有Unionid, 则通过unionid机制再找
			if(AppUtils.isNotBlank(thridUserInfo.getUnionid())){
				Passport passport = passportService.getPassportByUnionid(thridUserInfo.getUnionid());
				if(null != passport){
					passportFull = passportService.generatePassport(thridUserInfo, passportType, passportSource, passport);
				}
			}
			
			// 如果到了这里 passportFull 还是为空, 则需要生成一个全新的通行证
			if(null == passportFull){
				passportFull = passportService.generatePassport(thridUserInfo, passportType, passportSource);
			}
		} else{
			//需要更新一下passport
			passportFull.setAccessToken(thridUserInfo.getAccessToken());
			passportFull.setNickName(thridUserInfo.getNickName());
			passportFull.setGender(thridUserInfo.getGender());
			passportFull.setCountry(thridUserInfo.getCountry());
			passportFull.setProvince(thridUserInfo.getProvince());
			passportFull.setCity(thridUserInfo.getCity());
			passportFull.setHeadPortraitUrl(thridUserInfo.getAvatarUrl());
			
			passportService.updatePassportFull(passportFull);
		}
		
		// 到了这里还是为空, 则说明异常, 直接返回错误结果
		if(null == passportFull){
			result.setStatus(ThirdUserAuthorizeResult.ERROR);
			return result;
		}
		
		// 之前生成的通信证是没有unionid的, 但现在拿到了unionid; 则要看这个unionid有没有已建立passport了, 如果有,则要进行合并
		if(AppUtils.isBlank(passportFull.getUnionid()) && AppUtils.isNotBlank(thridUserInfo.getUnionid())){
			passportFull = passportService.mergePassport(passportFull, thridUserInfo.getUnionid());
		}
		
		if(AppUtils.isBlank(passportFull.getUserId())){
			result.setStatus(ThirdUserAuthorizeResult.SUCCESS_NEED_BIND_ACCOUNT);
			result.setPassport(passportFull);
			return result;
		}
		
		User user = userDetailService.getUser(passportFull.getUserId());
		if(null == user){
			result.setStatus(ThirdUserAuthorizeResult.SUCCESS_NEED_BIND_ACCOUNT);
			result.setPassport(passportFull);
			return result;
		}
		
		result.setStatus(ThirdUserAuthorizeResult.SUCCESS);
		result.setUser(user);
		result.setPassport(passportFull);
		
		return result;
	}

	@Override
	public String bindAccount(User user, PassportFull passport) {
		
		//查询该用户是否已被第三方平台的其他用户绑定
		boolean isExistPassport = passportService.isExistPassport(user.getId(), passport.getType(), passport.getSource());
		if(isExistPassport){
			return "";
		}
		
		//利用通行证记录的第三方用户信息跟新用户.
		UserDetail userDetail = userDetailService.getUserDetailById(user.getId());
		if(AppUtils.isNotBlank(passport.getNickName()) && AppUtils.isBlank(userDetail.getNickName())){
			userDetail.setNickName(passport.getNickName());
		}
		
		if(AppUtils.isNotBlank(passport.getHeadPortraitUrl()) && AppUtils.isBlank(userDetail.getPortraitPic())){
			String portraitPic = attachmentManager.upload(passport.getHeadPortraitUrl(), "USER_HEADPORTRAIT_" + passport.getOpenId() + ".jpg");
			if(AppUtils.isNotBlank(portraitPic)){
				userDetail.setPortraitPic(portraitPic);
			}
		}
		
		userDetailService.updateUserDetail(userDetail);
		
		//通行证与用户进行绑定
		int result = passportService.bindUser(passport.getPassPortId(), user.getId(), user.getName());
		if(result <= 0){
			throw new BusinessException("绑定用户异常,更新ls_passport失败!");
		}
		
		return Constants.SUCCESS;
	}
	
	@Override
	public PassportFull checkPassportIdKey(String passportKey){
		
		Long passportId = getPassportIDFromCache(passportKey);
		if(null == passportId){
			return null;
		}
		
		//检查是否有通行证
		PassportFull passport = passportService.getPassportFullById(passportId);
		if(null == passport){
			return null;
		}
		
		return passport;
	}
	
	/**
	 * 缓存passportId, 返回缓存的key
	 * @param passportId
	 * @return
	 */
	public String cachePassportID(Long passportId) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timestamp = sdf.format(new Date());
		String nonce = RandomStringUtils.randomNumeric(7);
		String key = DigestUtils.sha1Hex(passportId + timestamp + nonce);
		
		cacheClient.put(key, 60 * 10, passportId);//时间为10分钟
		
		return key;
	}

	@Override
	public Long getPassportIDFromCache(String key) {
		
		Object value = cacheClient.get(key);
		if(null != value){
			return (Long) value;
		}

		return null;
	}

	@Override
	public void removePassportIDFromCache(String key) {
		cacheClient.delete(key);
	}
}
