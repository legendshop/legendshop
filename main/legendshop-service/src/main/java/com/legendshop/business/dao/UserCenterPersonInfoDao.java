/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.model.entity.MyPersonInfo;
import com.legendshop.model.entity.UserGrade;


/**
 * The Interface UserCenterPersonInfoDao.
 */
public interface UserCenterPersonInfoDao{
	
	/**
	 * Gets the person info.
	 *
	 * @param userId the user id
	 * @return the person info
	 */
	abstract MyPersonInfo getPersonInfo(String userId);
	
	/**
	 * Update person info.
	 *
	 * @param myPersonInfo the my person info
	 */
	abstract void updatePersonInfo(final MyPersonInfo myPersonInfo);

	/**
	 * Verify sms code.
	 *
	 * @param userName the user name
	 * @param validateCode the validate code
	 * @return true, if successful
	 */
	abstract boolean verifySMSCode(String userName, String validateCode);

	/**
	 * Update payment password.
	 *
	 * @param userName the user name
	 * @param paymentPassword the payment password
	 * @param payStrength the pay strength
	 */
	abstract void updatePaymentPassword(String userName, String paymentPassword, Integer payStrength);
	
	/**
	 * 查询用户等级
	 * @param userId
	 * @return
	 */
	abstract UserGrade getUserGrade(String userId);
	
	/**
	 * 查询用户的下一个等级
	 * @param gradeId
	 * @return
	 */
	String getNextGradeName(Integer gradeId);
}
