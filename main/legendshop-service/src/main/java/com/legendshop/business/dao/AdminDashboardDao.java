/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.constant.DashboardTypeEnum;
import com.legendshop.model.entity.AdminDashboard;

/**
 * The Class AdminDashboardDao.
 */

public interface AdminDashboardDao extends Dao<AdminDashboard, String> {
     

	public abstract void calAdminDashboard(DashboardTypeEnum typeEnum);

	List<AdminDashboard> queryAdminDashboard();
	
 }
