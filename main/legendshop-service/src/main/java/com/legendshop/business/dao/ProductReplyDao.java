package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductReply;

public interface ProductReplyDao extends GenericDao<ProductReply, Long>{

	public abstract List<ProductReply> getProductReply();

	public abstract ProductReply getProductReply(Long id);

	public abstract void deleteProductReply(ProductReply productReply);

	public abstract Long saveProductReply(ProductReply productReply);

	public abstract void updateProductReply(ProductReply productReply);

	public abstract PageSupport<ProductReply> getProductReplyPage(String curPageNO);
}
