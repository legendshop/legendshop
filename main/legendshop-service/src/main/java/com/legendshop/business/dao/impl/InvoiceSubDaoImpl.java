/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.InvoiceSubDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.InvoiceSub;

/**
 * 发票订单信息Dao.
 */
@Repository
public class InvoiceSubDaoImpl extends GenericDaoImpl<InvoiceSub, Long> implements InvoiceSubDao  {
     
	public InvoiceSub getInvoiceSub(Long id){
		return getById(id);
	}
	
    public int deleteInvoiceSub(InvoiceSub invoiceSub){
    	return delete(invoiceSub);
    }
	
	public Long saveInvoiceSub(InvoiceSub invoiceSub){
		return save(invoiceSub);
	}
	
	public int updateInvoiceSub(InvoiceSub invoiceSub){
		return update(invoiceSub);
	}

 }
