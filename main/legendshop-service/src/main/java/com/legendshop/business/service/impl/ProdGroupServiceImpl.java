/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdGroupDao;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.spi.service.ProdGroupService;

/**
 * The Class ProdGroupServiceImpl.
 *  服务实现类
 */
@Service("prodGroupService")
public class ProdGroupServiceImpl implements ProdGroupService{

    /**
     *
     * 引用的Dao接口
     */
	@Autowired
    private ProdGroupDao prodGroupDao;
   
   	/**
	 *  根据Id获取
	 */
    public ProdGroup getProdGroup(Long id) {
        return prodGroupDao.getProdGroup(id);
    }
    
    /**
	 *  根据Id删除
	 */
    public int deleteProdGroup(Long id){
    	return prodGroupDao.deleteProdGroup(id);
    }

   /**
	 *  删除
	 */ 
    public int deleteProdGroup(ProdGroup prodGroup) {
       return  prodGroupDao.deleteProdGroup(prodGroup);
    }

   /**
	 *  保存
	 */	    
    public Long saveProdGroup(ProdGroup prodGroup) {
        if (!AppUtils.isBlank(prodGroup.getId())) {
            updateProdGroup(prodGroup);
            return prodGroup.getId();
        }
        return prodGroupDao.saveProdGroup(prodGroup);
    }

   /**
	 *  更新
	 */	
    public void updateProdGroup(ProdGroup prodGroup) {
        prodGroupDao.updateProdGroup(prodGroup);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<ProdGroup> queryProdGroup(String curPageNO, Integer pageSize){
     	return prodGroupDao.queryProdGroup(curPageNO, pageSize);
    }
    
    public List<ProdGroup> getProdGroupByType(Integer type){
     	return prodGroupDao.getProdGroupByType(type);
    }
    
    @Override
	public PageSupport<ProdGroup> queryProdGroupListPage(String curPageNO, ProdGroup prodGroup,Integer pageSize) {
		return prodGroupDao.queryProdGroupPage(curPageNO,prodGroup,pageSize);
	}
    
    @Override
	public boolean checkNameIsExist(String name,Long id) {
		return prodGroupDao.checkNameIsExist(name,id);
	}
    
}
