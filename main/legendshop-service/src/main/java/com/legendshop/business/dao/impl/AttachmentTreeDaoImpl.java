/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AttachmentTreeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.util.AppUtils;

/**
 * 附件类型Dao.
 */
@Repository
public class AttachmentTreeDaoImpl extends GenericDaoImpl<AttachmentTree, Long> implements AttachmentTreeDao  {

	public AttachmentTree getAttachmentTree(Long id){
		return getById(id);
	}
	
    public int deleteAttachmentTree(AttachmentTree attachmentTree){
    	return delete(attachmentTree);
    }
	
	public Long saveAttachmentTree(AttachmentTree attachmentTree){
		return save(attachmentTree);
	}
	
	public int updateAttachmentTree(AttachmentTree attachmentTree){
		return update(attachmentTree);
	}

	@Override
	public List<AttachmentTree> getAttachmentTreeByPid(Long pId, String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName).eq("parentId", pId));
	}

	@Override
	public void updateAttmntTreeNameById(Integer id, String name) {
		this.update("update ls_attmnt_tree set name=? where id=?", name,id);
	}

	@Override
	public AttachmentTree getAttachmentTree(Long treeId, Long shopId) {
		List<AttachmentTree> attmntList =  this.queryByProperties(new EntityCriterion().eq("id", treeId).eq("shopId", shopId));
		if(AppUtils.isNotBlank(attmntList)){
			return attmntList.get(0);
		}else{
			return null;
		}
	}

	/**
	 * 查找所有子节点树
	 */
	@Override
	public List<AttachmentTree> getAllChildByTreeId(Long treeId) {
		String sql = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".getAllChildByTreeId"); //由于采用了递归算法，因此不同的数据库会有不同的sql实现, 见dialectSQL.dal.xml
		return this.query(sql, AttachmentTree.class, treeId);
	}

	@Override
	public List<AttachmentTree> getAttachmentTreeByUserName(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}
	
 }
