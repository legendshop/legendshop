/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AdminUserDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminUser;
import com.legendshop.model.entity.Role;
import com.legendshop.spi.service.AdminUserService;
import com.legendshop.util.AppUtils;

/**
 * The Class AdminUserServiceImpl.
 */
@Service("adminUserService")
public class AdminUserServiceImpl  implements AdminUserService{
	
	@Autowired
    private AdminUserDao adminUserDao;

    public AdminUser getAdminUser(String id) {
        return adminUserDao.getAdminUser(id);
    }

    public void deleteAdminUser(AdminUser adminUser) {
        adminUserDao.deleteAdminUser(adminUser);
    }

    public String saveAdminUser(AdminUser adminUser) {
        return adminUserDao.saveAdminUser(adminUser);
    }

    public void updateAdminUser(AdminUser adminUser) {
        adminUserDao.updateAdminUser(adminUser);
    }

	@Override
	public Boolean isAdminUserExist(String userName) {
		return adminUserDao.isAdminUserExist(userName);
	}

	@Override
	public int updateAdminUserPassword(String id, String newPassword) {
		return adminUserDao.updateAdminUserPassword(id, newPassword);
		
	}

	@Override
	public List<Role> loadRole(String appNo) {
		return adminUserDao.loadRole(appNo);
	}

	@Override
	public List<Role> loadRole(String userId, String appNo) {
		return adminUserDao.loadRole(userId, appNo);
	}

	@Override
	public PageSupport<AdminUser> getAdminUser(String curPageNO, AdminUser adminUser) {
        CriteriaQuery cq = new CriteriaQuery(AdminUser.class, curPageNO);
        if(AppUtils.isNotBlank(adminUser.getName())) {       	
        	cq.like("name","%" + adminUser.getName().trim() + "%", MatchMode.ANYWHERE);
        }
        cq.eq("enabled", adminUser.getEnabled());
        if(AppUtils.isNotBlank(adminUser.getRealName())) {        	
        	cq.like("realName","%" +  adminUser.getRealName().trim()  + "%");
        }
        return adminUserDao.getAdminUser(cq);
	}
}
