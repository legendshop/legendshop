/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopAppDecorateDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopAppDecorate;
import com.legendshop.spi.service.ShopAppDecorateService;
import com.legendshop.util.AppUtils;

/**
 * The Class ShopAppDecorateServiceImpl.
 *  移动端店铺主页装修服务实现类
 */
@Service("shopAppDecorateService")
public class ShopAppDecorateServiceImpl implements ShopAppDecorateService{

    @Autowired
    private ShopAppDecorateDao shopAppDecorateDao;
   
    
    
   	/**
	 *  根据Id获取移动端店铺主页装修
	 */
    public ShopAppDecorate getShopAppDecorate(Long id) {
        return shopAppDecorateDao.getShopAppDecorate(id);
    }
    
    /**
	 *  根据Id删除移动端店铺主页装修
	 */
    public int deleteShopAppDecorate(Long id){
    	return shopAppDecorateDao.deleteShopAppDecorate(id);
    }

   /**
	 *  删除移动端店铺主页装修
	 */ 
    public int deleteShopAppDecorate(ShopAppDecorate shopAppDecorate) {
       return  shopAppDecorateDao.deleteShopAppDecorate(shopAppDecorate);
    }

   /**
	 *  保存移动端店铺主页装修
	 */	    
    public Long saveShopAppDecorate(ShopAppDecorate shopAppDecorate) {
        if (!AppUtils.isBlank(shopAppDecorate.getId())) {
            updateShopAppDecorate(shopAppDecorate);
            return shopAppDecorate.getId();
        }
        return shopAppDecorateDao.saveShopAppDecorate(shopAppDecorate);
    }

   /**
	 *  更新移动端店铺主页装修
	 */	
    public void updateShopAppDecorate(ShopAppDecorate shopAppDecorate) {
        shopAppDecorateDao.updateShopAppDecorate(shopAppDecorate);
    }


    /**
     * 查询移动端店铺主页装修列表
     */
	@Override
	public PageSupport<ShopAppDecorate> queryShopAppDecorate(String curPageNO, Integer pageSize,
			ShopAppDecorate shopAppDecorate) {
		
		return shopAppDecorateDao.queryShopAppDecorate(curPageNO,pageSize,shopAppDecorate);
	}

	/**
	 * 根据店铺id获取移动店铺首页装修数据
	 */
	@Override
	public ShopAppDecorate getShopAppDecorateByShopId(Long shopId) {
		
		return shopAppDecorateDao.getShopAppDecorateByShopId(shopId);
	}

	/**
	 * 获取正常上线的店铺首页装修数据
	 */
	@Override
	public String getReleaseDecorateDataByShopId(Long shopId) {
		
		return shopAppDecorateDao.getReleaseDecorateDataByShopId(shopId);
	}

   
    
    
}
