/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.StoreDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.entity.store.Store;
import com.legendshop.spi.service.StoreService;
import com.legendshop.util.AppUtils;

/**
 * 门店服务接口实现类
 */
@Service("storeService")
public class StoreServiceImpl  implements StoreService{

	@Autowired
	private StoreDao storeDao;

    /**
     * 获取门店 
     */
	public Store getStore(Long id) {
        return storeDao.getStore(id);
    }

	/**
	 * 删除门店
	 */
    public void deleteStore(Store store) {
        storeDao.deleteStore(store);
    }

    /**
     * 保存门店
     */
    public Long saveStore(Store store) {
        if (!AppUtils.isBlank(store.getId())) {
            updateStore(store);
            return store.getId();
        }
        return storeDao.saveStore(store);
    }

    /**
     * 更新门店 
     */
    public void updateStore(Store store) {
        storeDao.updateStore(store);
    }

    /**
     * 查询门店列表
     */
	@Override
	public PageSupport<Store> getStore(String curPageNO, Long shopId) {
		 return storeDao.query(curPageNO,shopId);
	}

	/**
	 * 判断登录名是否存在 
	 */
	@Override
	public boolean checkStoreName(String username) {
		long number=storeDao.getLongResult("select count(id) from ls_store where user_name=?", new Object[]{username});
		return number==0;
	}

	 /**
     * 获取门店 
     */
	@Override
	public Store getStore(Long id, Long shopId) {
		return storeDao.getStore(id,shopId);
	}

	 /**
     * 获取门店 
     */
	@Override
	public PageSupport<StoreProductDto> getStore(String curPageNO, int pageSize, String storeId, Long shopId, Long poductId, String productname) {
		return storeDao.query(curPageNO,pageSize,storeId,shopId,poductId,productname);
	}
}
