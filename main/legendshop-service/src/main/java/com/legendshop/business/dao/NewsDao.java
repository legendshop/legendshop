/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.NewsPositionEnum;
import com.legendshop.model.entity.News;

/**
 * 文章Dao.
 */
public interface NewsDao extends GenericDao<News, Long>{

	/**
	 * Gets the news.
	 *
	 * @param newsPositionEnum the news category status enum
	 * @param num the num
	 * @return the news
	 */
	public abstract List<News> getNews(final NewsPositionEnum newsPositionEnum, final Integer num);

	/**
	 * Gets the news by id.
	 * 
	 * @param newsId
	 *            the news id
	 * @return the news by id
	 */
	public abstract News getNewsById(Long newsId);

	/**
	 * Gets the news.
	 *
	 * @param curPageNO the cur page no
	 * @param newsCategoryId the news category id
	 * @return the news
	 */
	public abstract PageSupport<News> getNews(String curPageNO, Long newsCategoryId);

	/**
	 * Update news.
	 * 
	 * @param news
	 *            the news
	 */
	public abstract void updateNews(News news);

	/**
	 * Delete news by id.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void deleteNewsById(Long id);

	/**
	 * 获取系统中所有的文章的条数.
	 *
	 * @return the all news
	 */
	public abstract Long getAllNewsCount();
	
	/**
	 *  获取系统中所有的文章.
	 *
	 * @return the all news
	 */
	public abstract List<News> getAllNews();

	/**
	 * Gets the news by category.
	 *
	 * @return the news by category
	 */
	Map<KeyValueEntity, List<News>> getNewsByCategory();

	/**
	 * 得到上下2个文章.
	 *
	 * @param id the id
	 * @return the news
	 */
	public abstract List<News> getNearNews(Long id);


	/**
	 * Gets the news bycat id.
	 *
	 * @param catid the catid
	 * @return the news bycat id
	 */
	public abstract List<News> getNewsBycatId(Long catid);
	
	/**
	 * Gets the news byposition.
	 *
	 * @param catid the catid
	 * @return the news byposition
	 */
	public abstract List<News> getNewsByposition(Long catid);

	/**
	 * Gets the serach news.
	 *
	 * @param keyword the keyword
	 * @param curPageNO the cur page no
	 * @return the serach news
	 */
	public abstract PageSupport<News> getSerachNews(String keyword,String curPageNO);

	/**
	 * Help.
	 *
	 * @param curPageNO the cur page no
	 * @return the page support< news>
	 */
	PageSupport<News> help(String curPageNO);

	/**
	 * Gets the news list page.
	 *
	 * @param curPageNO the cur page no
	 * @param news the news
	 * @param pageSize the page size
	 * @param result the result
	 * @return the news list page
	 */
	public abstract PageSupport<News> getNewsListPage(String curPageNO, News news, Integer pageSize, DataSortResult result);

	/**
	 * Gets the news list result dto.
	 *
	 * @param curPageNO the cur page no
	 * @param pageSize the page size
	 * @param result the result
	 * @return the news list result dto
	 */
	public abstract PageSupport<News> getNewsListResultDto(String curPageNO, Integer pageSize, DataSortResult result);

	/**
	 * 更新状态
	 * @param news
	 */
	public abstract void updateStatus(News news);

	/**
	 * 按分页查询商家的新闻
	 */
	PageSupport<News> getNewsList(String curPageNO, Long shopId);

    Map<KeyValueEntity, List<News>> findByCategoryId();
}