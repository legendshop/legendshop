/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DrawActivityDao;
import com.legendshop.business.dao.DrawRecordDao;
import com.legendshop.business.dao.DrawWinRecordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DrawAwardsDto;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.model.entity.draw.DrawRecord;
import com.legendshop.model.entity.draw.DrawWinRecord;
import com.legendshop.spi.service.DrawWinRecordService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 中奖纪录服务实现类.
 */
@Service("drawWinRecordService")
public class DrawWinRecordServiceImpl  implements DrawWinRecordService{
	@Autowired
    private DrawWinRecordDao drawWinRecordDao;
	
	@Autowired
    private DrawActivityDao drawActivityDao; 
	
	@Autowired
    private DrawRecordDao drawRecordDao;
    
    public DrawActivityDao getDrawActivityDao() {
		return drawActivityDao;
	}

    public DrawWinRecord getDrawWinRecord(Long id) {
        return drawWinRecordDao.getDrawWinRecord(id);
    }

    public void deleteDrawWinRecord(DrawWinRecord drawWinRecord) {
        drawWinRecordDao.deleteDrawWinRecord(drawWinRecord);
    }
    
    public void updateDrawWinRecord(DrawWinRecord drawWinRecord) {
        drawWinRecordDao.updateDrawWinRecord(drawWinRecord);
    }

    /**
     * @Description: 保存中奖信息
     * @param @param drawWinRecord
     * @param @return   
     * @date 2016-4-26
     */
    public boolean saveDrawWinRecord(DrawWinRecord drawWinRecord,Long awardsRecordId) {
		DrawActivity drawActivity = drawActivityDao.getDrawActivity(drawWinRecord.getActId());
		if (AppUtils.isBlank(drawActivity)) {
			return false;
		}
		DrawWinRecord winPo = new DrawWinRecord();
		List<DrawAwardsDto> daDtos = JSONUtil.getArray(drawActivity.getAwardInfo(), DrawAwardsDto.class);
		// 设置中奖记录对象
		winPo.setUserId(drawWinRecord.getUserId());
		winPo.setCusName(drawWinRecord.getCusName());
		winPo.setMobile(drawWinRecord.getMobile());
		winPo.setSendAddress(drawWinRecord.getSendAddress());
		winPo.setWeixinOpenid(drawWinRecord.getWeixinOpenid());
		winPo.setWeixinNickname(drawWinRecord.getWeixinNickname());
		winPo.setActId(drawWinRecord.getActId());
		if(drawWinRecord.getAwardsType()==1){
			winPo.setSendStatus(0); //实体、未发货
		}
		winPo.setAwardsTime(new Date());
		winPo.setFlag(drawWinRecord.getFlag());
		// 设置中奖描述和将类型
		DrawWinRecord po = appendWindes(daDtos, drawWinRecord.getFlag());
		winPo.setAwardsType(po.getAwardsType());
		winPo.setWinDes(po.getWinDes());
   			 
   		Long winId=drawWinRecordDao.saveDrawWinRecord(winPo);
   		//设置抽奖记录中的中奖id
   		DrawRecord dr=drawRecordDao.getDrawRecord(awardsRecordId);
   		dr.setWinId(winId);
   		drawRecordDao.updateDrawRecord(dr);
    		
   		return true;
    }


	/**
	 * @Description: 根据活动ID获取活动中奖记录集合
	 * @param actId
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public List<DrawWinRecord> getDrawWinRecordsByActId(Long actId) {
		return drawWinRecordDao.getDrawWinRecordsByActId(actId);
	}

	/**
	 * @Description: 根据当前用户ID和活动ID获取数据
	 * @param userId
	 * @param actId
	 * @return   
	 * @date 2016-4-26
	 */
	@Override
	public List<DrawWinRecord> getDrawWinRecords(String userId, Long actId) {
		return drawWinRecordDao.getDrawWinRecords(userId, actId);
	}

	
	/**
	 * @Description: 设置中奖描述、奖项类型
	 * @date 2016-4-26 
	 */
	private DrawWinRecord appendWindes(List<DrawAwardsDto> daDtos,Integer awards){
		 DrawWinRecord recordPo=new DrawWinRecord();
		 StringBuffer winDes=new StringBuffer();
		 for(DrawAwardsDto dto:daDtos){
			 if(dto.getId()==awards){
				recordPo.setAwardsType(dto.getAwardsType());//设置奖项类型
				switch (awards) {
					case 1:
						winDes.append("一等奖、");
						switch (dto.getAwardsType()) {
							case 1:
								winDes.append("实物").append(dto.getGiftName());
								break;
							case 2:
								winDes.append("现金").append(dto.getCash()).append("元");		
								break;
							case 3:
								winDes.append("积分").append(dto.getIntegral());		
								break;
						}
						break;
					case 2:
						winDes.append("二等奖、");	
						switch (dto.getAwardsType()) {
							case 1:
								winDes.append("实物").append(dto.getGiftName());
								break;
							case 2:
								winDes.append("现金").append(dto.getCash()).append("元");					
								break;
							case 3:
								winDes.append("积分").append(dto.getIntegral());	
								break;
						}
						break;
					case 3:
						winDes.append("三等奖、");
							switch (dto.getAwardsType()) {
							case 1:
								winDes.append("实物").append(dto.getGiftName());
								break;
							case 2:
								winDes.append("现金").append(dto.getCash()).append("元");				
								break;
							case 3:
								winDes.append("积分").append(dto.getIntegral());	
								break;
						  }
						break;
				}
				recordPo.setWinDes(winDes.toString());
			 }
		 }
		 return recordPo;
	}

	@Override
	public PageSupport<DrawWinRecord> queryDrawWinRecordPage(String curPageNO, DrawWinRecord drawWinRecord) {
		return drawWinRecordDao.queryDrawWinRecordPage(curPageNO,drawWinRecord);
	}
	
	
	
}
