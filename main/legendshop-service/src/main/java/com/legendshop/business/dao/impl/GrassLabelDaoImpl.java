/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.GrassLabel;
import com.legendshop.util.AppUtils;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassLabelDao;

/**
 * The Class GrassLabelDaoImpl. 种草社区标签表Dao实现类
 */
@Repository
public class GrassLabelDaoImpl extends GenericDaoImpl<GrassLabel, Long> implements GrassLabelDao{

	/**
	 * 根据Id获取种草社区标签表
	 */
	public GrassLabel getGrassLabel(Long id){
		return getById(id);
	}

	/**
	 *  删除种草社区标签表
	 *  @param grassLabel 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassLabel(GrassLabel grassLabel){
    	return delete(grassLabel);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassLabel(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草社区标签表
	 */
	public Long saveGrassLabel(GrassLabel grassLabel){
		return save(grassLabel);
	}

	/**
	 *  更新种草社区标签表
	 */		
	public int updateGrassLabel(GrassLabel grassLabel){
		return update(grassLabel);
	}

	/**
	 * 分页查询种草社区标签表列表
	 */
	public PageSupport<GrassLabel>queryGrassLabel(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassLabel.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<GrassLabel> ps = queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<GrassLabel> queryGrassLabelPage(String curPageNO, String name, Integer pageSize) {
		CriteriaQuery query=new CriteriaQuery(GrassLabel.class,curPageNO);
		query.setPageSize(pageSize);
		if (AppUtils.isNotBlank(name)) {
			query.like("name",name, MatchMode.ANYWHERE);
		}
		query.addDescOrder("createTime");
		PageSupport<GrassLabel> ps=queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<GrassLabel> getGrassLabelByLabel(String curPageNo,GrassLabel grassLabel,Integer PageSize) {
		CriteriaQuery query = new CriteriaQuery(GrassLabel.class,curPageNo);
		query.setPageSize(PageSize);
		if (AppUtils.isNotBlank(grassLabel)) {
			query.like("name",grassLabel.getName());
		}
		query.addDescOrder("isrecommend");
		query.addDescOrder("refcount");
		query.addDescOrder("createTime");
		return queryPage(query);
	}

    @Override
    public List<GrassLabel> getGrassLabelByGrassId(Long articleId) {
		String sql="select l.id,l.name,l.create_time,l.isrecommend,l.refcount from ls_grass_label l inner join ls_grass_article_lable a on l.id=a.grlabel_id where a.grarticle_id=?";
        return query(sql,GrassLabel.class,articleId);
    }

    @Override
    public void addRefNum(Long lid) {
        String sql="UPDATE ls_grass_label SET refcount=refcount+1 WHERE id=?";
        update(sql,lid);
    }


}
