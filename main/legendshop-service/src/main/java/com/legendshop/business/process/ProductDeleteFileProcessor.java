/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.process;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.helper.FileProcessor;
import com.legendshop.base.model.InvokeRetObj;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.event.processor.ThreadProcessor;
import com.legendshop.model.entity.Product;

/**
 * 删除产品图片，包括所有的缩略图
 */
@Component
public class ProductDeleteFileProcessor extends ThreadProcessor<Product> {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductDeleteFileProcessor.class);

	@Resource(name="scaleList")
	private Map<String, List<Integer>> scaleList;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.event.processor.AbstractProcessor#process(java.lang.Object)
	 */
	@Override
	public void process(Product product) {
		log.debug("ProductDeleteFileProcessor performed");
		String picUrl = propertiesUtil.getBigPicPath() + "/" + product.getPic();
		// 删除原图
		log.debug("delete Big Image file {}", picUrl);
		InvokeRetObj result = FileProcessor.deleteFile(picUrl);
		// 删除原图成功,跟着删除所有缩略图规格
		if (result.isFlag()) {
			for (String scaleValue : scaleList.keySet()) {
				String smallPicUrl = propertiesUtil.getSmallPicPath() + "/" + scaleValue + "/" + product.getPic();
				log.debug("delete small Image file {}", smallPicUrl);
				FileProcessor.deleteFile(smallPicUrl);
			}
		}

	}


}
