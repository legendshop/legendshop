/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinReceivetextDao;
import com.legendshop.model.entity.weixin.WeixinReceivetext;
import com.legendshop.spi.service.WeixinReceivetextService;
import com.legendshop.util.AppUtils;

/**
 * 微信接受的文字服务
 */
@Service("weixinReceivetextService")
public class WeixinReceivetextServiceImpl  implements WeixinReceivetextService{

	@Autowired
    private WeixinReceivetextDao weixinReceivetextDao;

    public WeixinReceivetext getWeixinReceivetext(Long id) {
        return weixinReceivetextDao.getWeixinReceivetext(id);
    }

    public void deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        weixinReceivetextDao.deleteWeixinReceivetext(weixinReceivetext);
    }

    public Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        if (!AppUtils.isBlank(weixinReceivetext.getId())) {
            updateWeixinReceivetext(weixinReceivetext);
            return weixinReceivetext.getId();
        }
        return weixinReceivetextDao.saveWeixinReceivetext(weixinReceivetext);
    }

    public void updateWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        weixinReceivetextDao.updateWeixinReceivetext(weixinReceivetext);
    }
}
