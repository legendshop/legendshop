/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.legendshop.model.constant.PromotionSearchTypeEnum;
import com.legendshop.model.constant.PromotionStatusEnum;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.ShippingActiveDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ShippingTypeEnum;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.util.AppUtils;

/**
 * 包邮活动Dao
 */
@Repository
public class ShippingActiveDaoImpl extends GenericDaoImpl<ShippingActive, Long> implements ShippingActiveDao  {
     
	@Override
	public ShippingActive getShippingActive(Long id){
		return getById(id);
	}
	
    @Override
	public int deleteShippingActive(ShippingActive shippingActive){
    	return delete(shippingActive);
    }
	
	@Override
	public Long saveShippingActive(ShippingActive shippingActive){
		String source = "PC";
		if (source.equals(shippingActive.getSource())) {
			Date endDate = DateUtils.getIntegralEndTime(shippingActive.getEndDate());
			shippingActive.setEndDate(endDate);
		}
		return save(shippingActive);
	}
	
	@Override
	public int updateShippingActive(ShippingActive shippingActive){
		return update(shippingActive);
	}

	@Override
	public PageSupport<ShippingActive> queryShippingActiveList(String curPageNO, Long shopId, ShippingActive shippingActive) {
		CriteriaQuery cq = new CriteriaQuery(ShippingActive.class, curPageNO);

		String searchType = shippingActive.getSearchType();
		if (AppUtils.isNotBlank(searchType) && !"ALL".equals(searchType)){
			switch (PromotionSearchTypeEnum.matchType(searchType)){

				case NOT_STARTED:
					cq.eq("status", PromotionStatusEnum.ONLINE.value());
					cq.gt("startDate", new Date());
					break;
				case ONLINE:
					cq.lt("startDate", new Date());
					cq.gt("endDate", new Date());
					cq.eq("status", PromotionStatusEnum.ONLINE.value());
					break;
				case FINISHED:
					cq.eq("status", PromotionStatusEnum.ONLINE.value());
					cq.lt("endDate", new Date());
					break;
				case EXPIRED:
					cq.eq("status", 0);
					break;

			}
		}
		cq.eq("shopId", shopId);
		if(AppUtils.isNotBlank(shippingActive.getName())) {
			cq.like("name", shippingActive.getName(), MatchMode.ANYWHERE);
		}
		cq.setPageSize(15);
		cq.addDescOrder("createDate");
		return queryPage(cq);
	}

	@Override
	public ShippingActive getShippingActiveById(Long activeId, Long shopId) {
		return getByProperties(new EntityCriterion().eq("shopId", shopId).eq("id", activeId));
	}

	@Override
	public List<ShippingActive> getShopRuleEngineShippingActive(Long shopId) {
		
		String now = getNowDate();
		
		String sql = "SELECT id,name,full_type AS fullType,reduce_type AS reduceType,full_value AS fullValue FROM ls_shipping_active WHERE shop_id = ? AND status = ? AND start_date <= ? AND end_date >= ?";
		List<ShippingActive> actives = query(sql,ShippingActive.class, shopId, ShippingTypeEnum.ONLINE.value(),now,now);
			
		if(AppUtils.isBlank(actives)){
			return null;
		}
		
		for(ShippingActive active:actives){
			
			if(active.getFullType() == ShippingTypeEnum.PROD.value() ){ //商品类型活动
            	//查询参与的所有商品
				String queryProdId = "SELECT prod_id FROM ls_shipping_prod WHERE shipping_id = ? AND shop_id = ?";
            	List<Long> prodIds =  query(queryProdId,Long.class, active.getId(),shopId);
				if (AppUtils.isNotBlank(prodIds)) {
					Long[] arrays = new Long[prodIds.size()];
					for (int i = 0, j = prodIds.size(); i < j; i++) {
						arrays[i] = prodIds.get(i);
					}
					active.setProdIds(arrays);
				}
            }
		}
		
		return actives;
	}

	@Override
	public List<ShippingActive> queryOnLineShippingActive(Long shopId, Long prodId) {
		
		String now = getNowDate();
		
		String sql = "SELECT id,name,full_type AS fullType,reduce_type AS reduceType,full_value AS fullValue FROM ls_shipping_active WHERE shop_id = ? AND status = ? AND start_date <= ? "
				+ "AND end_date >= ? AND full_type = ? UNION SELECT a.id,a.name,a.full_type AS fullType,a.reduce_type AS reduceType,a.full_value AS fullValue "
				+ "FROM ls_shipping_active AS a INNER JOIN ls_shipping_prod AS p ON a.id = p.shipping_id "
				+ "WHERE a.shop_id = ? AND a.status = ? AND a.start_date <= ? AND a.end_date >= ? AND a.full_type = ? AND p.prod_id = ?";
		
		Object[] params = new Object[]{shopId,ShippingTypeEnum.ONLINE.value(),now,now,ShippingTypeEnum.SHOP.value(),
				shopId,ShippingTypeEnum.ONLINE.value(),now,now,ShippingTypeEnum.PROD.value(),prodId};
		
		return query(sql, ShippingActive.class, params);
	}

	private String getNowDate() {
		Date nowDate = new Date();
		// 2020-08-17 update by jzh：这里如果只精确到日期的话，sql那边会查不到任何数据
//		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		return DateUtils.format(nowDate);
	}

	@CacheEvict(value = "ShopMailfeeStr", key = "#shopId + #prodId")
	public void clearShippingActiveProductCache(Long shopId, Long prodId) {

	}

	@CacheEvict(value = "ShopMailfeeStr", allEntries = true)
	public void clearAllShippingActiveProductCache() {

	}

	@Override
	public void clearShippingActiveProductCache(Long shopId, Long[] prodIds) {
		// 清除包邮活动查询缓存
		if (AppUtils.isNotBlank(prodIds)) {
			for (Long prodId : prodIds) {
				this.clearShippingActiveProductCache(shopId, prodId);
			}
		} else {
			this.clearAllShippingActiveProductCache();
		}
	}
}
