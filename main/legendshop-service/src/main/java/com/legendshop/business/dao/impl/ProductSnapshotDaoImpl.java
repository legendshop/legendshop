/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProductSnapshotDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ProductSnapshot;
import com.legendshop.util.AppUtils;

/**
 * The Class ProductSnapshotDaoImpl.
 */
@Repository
public class ProductSnapshotDaoImpl extends GenericDaoImpl<ProductSnapshot, Long> implements ProductSnapshotDao  {
     

	public ProductSnapshot getProductSnapshot(Long id){
		return getById(id);
	}
	
    public int deleteProductSnapshot(ProductSnapshot productSnapshot){
    	return delete(productSnapshot);
    }
	
	public Long saveProductSnapshot(ProductSnapshot productSnapshot){
		return save(productSnapshot);
	}
	
	public int updateProductSnapshot(ProductSnapshot productSnapshot){
		return update(productSnapshot);
	}
	
	@Override
	public ProductSnapshot getProductSnapshot(Long prodId,Long skuId, Integer version) {
		if(AppUtils.isNotBlank(skuId)){
			return this.getByProperties(new EntityCriterion().eq("prodId", prodId).eq("skuId", skuId).eq("version", version));
		}else{
			return this.getByProperties(new EntityCriterion().eq("prodId", prodId).eq("version", version));
		}
	}

	@Override
	public Long createProductSnapshotId() {
		return createId();
	}

	@Override
	public void saveProductSnapshotWithId(ProductSnapshot productSnapshot) {
		save(productSnapshot,productSnapshot.getSnapshotId());
	}
	
 }
