/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.seckill;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀.
 *
 */
@Service("orderSeckillCartResolver")
public class OrderSeckillCartResolver extends AbstractAddOrder implements IOrderCartResolver {

	@Autowired
	private SeckillService seckillService;

	/**
	 * 查询秒杀订单.
	 *
	 * @param params
	 * @return the user shop cart list
	 */
	@Override
	public UserShopCartList queryOrders(Map<String, Object> params) {
		String userId = (String) params.get("userId");
		String userName = (String) params.get("userName");
		Long seckillId = (Long) params.get("seckillId");
		Long skuId = (Long) params.get("skuId");
		Long prodId = (Long) params.get("prodId");
		Long adderessId = (Long) params.get("adderessId");// 用户收货地址

		ShopCartItem shopCartItem = basketService.findSeckillCart(seckillId, prodId, skuId);
		Integer stocks = shopCartItem.getStocks();
		if (shopCartItem == null) {
			throw new BusinessException("下单失败,没有找到您的秒杀记录");
		}
		List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>(1);
		cartItems.add(shopCartItem);
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, cartItems, adderessId,CartTypeEnum.SECKILL.toCode());
		ShopCartItem shopCartItem1 = userShopCartList.getShopCarts().get(0).getCartItems().get(0);
		if (shopCartItem1.getStocks()!=-1){
			shopCartItem1.setStocks(stocks);
		}
		if (userShopCartList == null) {
			return null;
		}
		userShopCartList.setType(CartTypeEnum.SECKILL.toCode());
		return userShopCartList;
	}

	/**
	 * 下单操作.
	 *
	 * @param userShopCartList
	 * @return the list< long>
	 */
	@Override
    public List<String> addOrder(UserShopCartList userShopCartList) {
		List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
		List<String> subNembers = super.submitOrder(userShopCartList);
		if (AppUtils.isBlank(subNembers)) {
			return null;
		}
		for (Iterator<ShopCarts> shopIterator = shopCarts.iterator(); shopIterator.hasNext();) {
			ShopCarts shop = shopIterator.next();
			List<ShopCartItem> shopCartItems = shop.getCartItems();
			for (ShopCartItem item : shopCartItems) {
				Long basketId = item.getBasketId();
				String userId = userShopCartList.getUserId();
				int result = seckillService.updateSeckillSuccess(item.getBasketId(), userShopCartList.getUserId());
				if (result == 0) {
					throw new BusinessException("下单失败,数据已发生变化,请刷新后重试");
				}
			}
			shopCartItems = null;
		}
		shopCarts = null;
		return subNembers;
	}

	public void setSeckillService(SeckillService seckillService) {
		this.seckillService = seckillService;
	}

}
