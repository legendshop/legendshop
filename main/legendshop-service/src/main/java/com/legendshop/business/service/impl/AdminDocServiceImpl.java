/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AdminDocDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminDoc;
import com.legendshop.spi.service.AdminDocService;
import com.legendshop.util.AppUtils;

/**
 * The Class AdminDocServiceImpl. 服务实现类
 */
@Service("adminDocService")
public class AdminDocServiceImpl implements AdminDocService {

	@Autowired
	private AdminDocDao adminDocDao;

	/**
	 * 根据Id获取
	 */
	public AdminDoc getAdminDoc(Long id) {
		return adminDocDao.getAdminDoc(id);
	}

	/**
	 * 删除
	 */
	public void deleteAdminDoc(AdminDoc adminDoc) {
		adminDocDao.deleteAdminDoc(adminDoc);
	}

	/**
	 * 保存
	 */
	public Long saveAdminDoc(AdminDoc adminDoc) {
		AdminDoc oldAdminDoc = adminDocDao.getAdminDoc(adminDoc.getId());
		if (!AppUtils.isBlank(oldAdminDoc)) {
			updateAdminDoc(adminDoc);
			return adminDoc.getId();
		}
		return adminDocDao.saveAdminDoc(adminDoc);
	}

	/**
	 * 更新
	 */
	public void updateAdminDoc(AdminDoc adminDoc) {
		adminDocDao.updateAdminDoc(adminDoc);
	}

	@Override
	public PageSupport<AdminDoc> queryAdminDocList(String curPageNO, AdminDoc adminDoc) {
		return adminDocDao.queryAdminDocList(curPageNO, adminDoc);
	}
}
