/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;
/**
 * 头条管理Dao
 */
public interface HeadlineAdminDao extends GenericDao<Headline, Long> {
     
	public abstract Headline getHeadlineAdmin(Long id);
    
    public abstract void deleteHeadlineAdmin(Long id);
    
    public abstract Long saveHeadlineAdmin(Headline headline);

    public abstract void updateHeadlineAdmin(Headline headline);

	public abstract PageSupport<Headline> getHeadlineAdmin(String curPageNO, Headline headline);
	
 }




