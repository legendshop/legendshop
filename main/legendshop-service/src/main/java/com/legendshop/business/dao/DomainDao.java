/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;

/**
 * DomainDao接口.
 */
public interface DomainDao {

	/**
	 * 查询该二级域名是否已经绑定
	 *
	 * @param domainName
	 * @return true, if successful
	 */
	boolean queryDomainNameBinded(String domainName);

	/**
	 * 更新二级域名
	 * 
	 * @param userId
	 * @param domainName
	 * @return
	 */
	int updateDomainName(String userId, String domainName, Date registDate);

}
