/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinAutoresponseDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.WxMenuInfoypeEnum;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;
import com.legendshop.spi.service.WeixinAutoresponseService;
import com.legendshop.util.AppUtils;

/**
 * 微信响应服务
 */
@Service("weixinAutoresponseService")
public class WeixinAutoresponseServiceImpl  implements WeixinAutoresponseService{
	
	@Autowired
    private WeixinAutoresponseDao weixinAutoresponseDao;

    public WeixinAutoresponse getWeixinAutoresponse(Long id) {
        return weixinAutoresponseDao.getWeixinAutoresponse(id);
    }

    public void deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
        weixinAutoresponseDao.deleteWeixinAutoresponse(weixinAutoresponse);
    }

    public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
    	 if(weixinAutoresponse.getMsgType().equals(WxMenuInfoypeEnum.TEXT.value())){
         	weixinAutoresponse.setTemplateName(null);
         	weixinAutoresponse.setTemplateId(null);
         }else{
         	weixinAutoresponse.setContent(null);
         }
        if (!AppUtils.isBlank(weixinAutoresponse.getId())) {
        	WeixinAutoresponse autoresponse=weixinAutoresponseDao.getWeixinAutoresponse(weixinAutoresponse.getId());
        	autoresponse.setContent(weixinAutoresponse.getContent());
        	autoresponse.setMsgTriggerType(weixinAutoresponse.getMsgTriggerType());
        	autoresponse.setMsgType(weixinAutoresponse.getMsgType());
        	autoresponse.setTemplateId(weixinAutoresponse.getTemplateId());
        	autoresponse.setTemplateName(weixinAutoresponse.getTemplateName());
            updateWeixinAutoresponse(autoresponse);
            return weixinAutoresponse.getId();
        }
        return weixinAutoresponseDao.saveWeixinAutoresponse(weixinAutoresponse);
    }

    public void updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
        weixinAutoresponseDao.updateWeixinAutoresponse(weixinAutoresponse);
    }

	@Override
	public PageSupport<WeixinAutoresponse> getWeixinnAutoresponse(String curPageNO) {
		 return weixinAutoresponseDao.getWeixinnAutoresponse(curPageNO);
	}
}
