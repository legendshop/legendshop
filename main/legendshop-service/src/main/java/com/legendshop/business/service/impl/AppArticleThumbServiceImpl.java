/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AppArticleThumbDao;
import com.legendshop.model.entity.AppArticleThumb;
import com.legendshop.spi.service.AppArticleThumbService;

/**
 * The Class AppArticleThumbServiceImpl.
 *  种草和发现文章点赞表服务实现类
 */
@Service("appArticleThumbService")
public class AppArticleThumbServiceImpl implements AppArticleThumbService{

    /**
     *
     * 引用的种草和发现文章点赞表Dao接口
     */
	@Autowired
    private AppArticleThumbDao appArticleThumbDao;
   
   	/**
	 *  根据Id获取种草和发现文章点赞表
	 */
    public AppArticleThumb getAppArticleThumb(Long id) {
        return appArticleThumbDao.getAppArticleThumb(id);
    }
    
    /**
	 *  根据Id删除种草和发现文章点赞表
	 */
    public int deleteAppArticleThumb(Long id){
    	return appArticleThumbDao.deleteAppArticleThumb(id);
    }

   /**
	 *  删除种草和发现文章点赞表
	 */ 
    public int deleteAppArticleThumb(AppArticleThumb appArticleThumb) {
       return  appArticleThumbDao.deleteAppArticleThumb(appArticleThumb);
    }

   /**
	 *  保存种草和发现文章点赞表
	 */	    
    public Long saveAppArticleThumb(AppArticleThumb appArticleThumb) {
        if (!AppUtils.isBlank(appArticleThumb.getId())) {
            updateAppArticleThumb(appArticleThumb);
            return appArticleThumb.getId();
        }
        return appArticleThumbDao.saveAppArticleThumb(appArticleThumb);
    }

   /**
	 *  更新种草和发现文章点赞表
	 */	
    public void updateAppArticleThumb(AppArticleThumb appArticleThumb) {
        appArticleThumbDao.updateAppArticleThumb(appArticleThumb);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<AppArticleThumb> queryAppArticleThumb(String curPageNO, Integer pageSize){
     	return appArticleThumbDao.queryAppArticleThumb(curPageNO, pageSize);
    }

    @Override
    public Integer rsThumb(Long discoverId, String userId) {
        return appArticleThumbDao.rsThumb(discoverId,userId);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setAppArticleThumbDao(AppArticleThumbDao appArticleThumbDao) {
        this.appArticleThumbDao = appArticleThumbDao;
    }

	@Override
	public int deleteAppArticleThumbByDiscoverId(Long disId, String uid) {
		return appArticleThumbDao.deleteAppArticleThumbByDiscoverId(disId,uid);
	}
	 
    
}
