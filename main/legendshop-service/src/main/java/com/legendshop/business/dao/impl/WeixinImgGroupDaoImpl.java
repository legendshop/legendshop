/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinImgGroupDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinImgGroup;

/**
 * 微信图片管理
 */
@Repository
public class WeixinImgGroupDaoImpl extends GenericDaoImpl<WeixinImgGroup, Long> implements WeixinImgGroupDao  {
	private String getAllGroupsInfoSQL = "select wf.img_goup_id as id,count(wf.id) as count,wg.name  from ls_weixin_img_file wf  " +
			"left join ls_weixin_img_group wg on wf.img_goup_id=wg.id group by id " +
			"union select wg.id as id,count(wf.id) as count,wg.name  from ls_weixin_img_file wf  " +
			"right join ls_weixin_img_group wg on wf.img_goup_id=wg.id group by id";

	public WeixinImgGroup getWeixinImgGroup(Long id){
		return getById(id);
	}
	
    public int deleteWeixinImgGroup(WeixinImgGroup weixinImgGroup){
    	return delete(weixinImgGroup);
    }
	
	public Long saveWeixinImgGroup(WeixinImgGroup weixinImgGroup){
		return save(weixinImgGroup);
	}
	
	public int updateWeixinImgGroup(WeixinImgGroup weixinImgGroup){
		return update(weixinImgGroup);
	}
	
	public PageSupport getWeixinImgGroup(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public List<WeixinImgGroup> getAllGroupsInfo() {
		return this.query(getAllGroupsInfoSQL, WeixinImgGroup.class);
	}

	@Override
	public WeixinImgGroup getWeixinImgGroupByName(String groupName) {
		return this.getByProperties(new EntityCriterion().eq("name", groupName));
	}
	
 }
