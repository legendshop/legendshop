/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopLayoutBannerDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.spi.service.ShopLayoutBannerService;
import com.legendshop.util.AppUtils;

/**
 * 布局Banner服务.
 */
@Service("shopLayoutBannerService")
public class ShopLayoutBannerServiceImpl  implements ShopLayoutBannerService{
	
	@Autowired
    private ShopLayoutBannerDao shopLayoutBannerDao;

    public ShopLayoutBanner getShopLayoutBanner(Long id) {
        return shopLayoutBannerDao.getShopLayoutBanner(id);
    }

    public void deleteShopLayoutBanner(ShopLayoutBanner shopLayoutBanner) {
        shopLayoutBannerDao.deleteShopLayoutBanner(shopLayoutBanner);
    }

    public Long saveShopLayoutBanner(ShopLayoutBanner shopLayoutBanner) {
        if (!AppUtils.isBlank(shopLayoutBanner.getId())) {
            updateShopLayoutBanner(shopLayoutBanner);
            return shopLayoutBanner.getId();
        }
        return shopLayoutBannerDao.saveShopLayoutBanner(shopLayoutBanner);
    }

    public void updateShopLayoutBanner(ShopLayoutBanner shopLayoutBanner) {
        shopLayoutBannerDao.updateShopLayoutBanner(shopLayoutBanner);
    }

	@Override
	//@CachePut(value="ShopLayoutBannerList",key="#decId+#layoutId")
	public List<ShopLayoutBanner> getShopLayoutBanner(Long decId, Long layoutId) {
		return shopLayoutBannerDao.getShopLayoutBanner(decId, layoutId);
	}

	@Override
	public void delete(List<ShopLayoutBanner> banners) {
		shopLayoutBannerDao.delete(banners);
	}
}
