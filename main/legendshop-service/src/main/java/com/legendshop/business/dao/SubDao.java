/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.legendshop.app.dto.IncomeTodayDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.order.InvoiceSubDto;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.order.PresellSubDto;
import com.legendshop.model.entity.OrderSub;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.model.entity.store.StoreProd;

/**
 * 订单Dao.
 */
public interface SubDao  extends GenericDao<Sub, Long> {

	/**
	 * Save sub.
	 *
	 * @param sub the sub
	 * @return the long
	 */
	public abstract Long saveSub(Sub sub);
	
	/**
	 * Gets the sub id.
	 *
	 * @return the sub id
	 */
	Long getSubId();
	

	/**
	 * Delete sub.
	 * 
	 * @param sub
	 *            the sub
	 * @return true, if successful
	 */
	public abstract boolean deleteSub(Sub sub);
	
	/**
	 * Delete sub.
	 *
	 * @param userId the user id
	 * @param userName the user name
	 */
	public abstract void deleteSub(String userId, String userName);

	/**
	 * Admin change sub price.
	 * 更新订单价格
	 * @param sub
	 *            the sub
	 * @param userName
	 *            the user name
	 * @param totalPrice
	 *            the total price
	 * @return true, if successful
	 */
	public abstract boolean updateSubPrice(Sub sub, String userName, Double totalPrice);

	/**
	 * Gets the sub by id.
	 * 
	 * @param subId
	 *            the sub id
	 * @return the sub by id
	 */
	public abstract Sub getSubById(Long subId);

	/**
	 * Find sub by sub number.
	 * 
	 * @param subNumber
	 *            the sub number
	 * @return the sub
	 */
	public abstract Sub getSubBySubNumber(String subNumber);

	/**
	 * 超时不付款的订单.
	 * 
	 * @param maxNum
	 *            the max num
	 * @param expireDate
	 *            the expire date
	 * @return the list
	 */
	public abstract List<Sub> getFinishUnPay(int maxNum, Date expireDate);

	/**
	 * 超时不确认收货的自动收货 此时卖家已经发货.
	 * 
	 * @param maxNum
	 *            the max num
	 * @param expireDate
	 *            the expire date
	 * @return the list
	 */
	public abstract List<Sub> getUnAcklodgeSub(int maxNum, Date expireDate);

	/**
	 * 移除已经过期的购物车，保留30天.
	 * 
	 * @param date
	 *            the date
	 */
	public abstract void deleteOverTimeBasket(Date date);

	/**
	 * Update sub.
	 * 
	 * @param sub
	 *            the sub
	 */
	public abstract void updateSub(Sub sub);

	/**
	 * Gets the total processing order.
	 *
	 * @param shopId the shop id
	 * @return the total processing order
	 */
	public abstract Long getTotalProcessingOrder(Long shopId);

	/**
	 * 判断用户是否已经订购有效某个产品.
	 *
	 * @param prodId the prod id
	 * @param userName the user name
	 * @return true, if checks if is user order product
	 */
	public abstract boolean isUserOrderProduct(Long prodId, String userName);
	
	/**
	 * 判断用户是否已经订购有效的产品.
	 *
	 * @param userName the user name
	 * @return true, if checks if is user order product
	 */
	public abstract boolean isUserOrderProduct(String userName);

	/**
	 * Gets the sub by sub number by user id.
	 *
	 * @param subNumber the sub number
	 * @param userId the user id
	 * @return the sub by sub number by user id
	 */
	public abstract Sub getSubBySubNumberByUserId(String subNumber,String userId);
	
	/**
	 * Gets the sub by sub number by shop id.
	 *
	 * @param subNumber the sub number
	 * @param ShopId the Shop id
	 * @return the sub by sub number by shop id
	 */
	public abstract Sub getSubBySubNumberByShopId(String subNumber,
			Long ShopId);

	/**
	 * Gets the sub by ids.
	 *
	 * @param subIds the sub ids
	 * @param userId the user id
	 * @param status the status
	 * @return the sub by ids
	 */
	public abstract List<Sub> getSubByIds(String [] subIds,String userId,Integer status);

	/**
	 * Update sub for pay.
	 *
	 * @param sub the sub
	 * @param currentStatus the current status
	 * @return the int
	 */
	public abstract int updateSubForPay(Sub sub,Integer currentStatus);


	/**
	 * Gets the out put excel order.
	 *
	 * @param subIds the sub ids
	 * @return the out put excel order
	 */
	public abstract List<OrderSub> getOutPutExcelOrder(Long[] subIds);

	/**
	 * Query sub counts.
	 *
	 * @param userId the user id
	 * @return the list< sub counts dto>
	 */
	public abstract List<SubCountsDto> querySubCounts(String userId);
	
	/**
	 * 根据 shopId 获取 订单每个状态的数量（最近一个月）.
	 *
	 * @param shopId the shop id
	 * @return the sub counts
	 */
	public List<KeyValueEntity> getSubCounts(Long shopId);

	/**
	 * 获取商家交易完成的订单信息-用于档期结算统计.
	 *
	 * @param shopId the shop id
	 * @param endDate the end date
	 * @param status the status
	 * @return the bill finish orders
	 */
	public abstract List<Sub> getBillFinishOrders(Long shopId,Date endDate, Integer status);


	/**
	 * Clean order cache.
	 *
	 * @param userId the user id
	 * @param deleteStatus the delete status
	 */
	public abstract void cleanOrderCache(String userId, Integer deleteStatus);

	/**
	 * Clean unpay order count.
	 *
	 * @param userId the user id
	 */
	void cleanUnpayOrderCount(String userId);

	/**
	 * Clean consignment order count.
	 *
	 * @param userId the user id
	 */
	void cleanConsignmentOrderCount(String userId);


	/**
	 * Gets the orders.
	 *
	 * @param queryMap the query map
	 * @param args the args
	 * @param curPageNO the cur page no
	 * @param pageSize the page size
	 * @param paramDto the param dto
	 * @return the orders
	 */
	public abstract PageSupport<MySubDto> getOrders(QueryMap queryMap, List<Object> args,
			int curPageNO, int pageSize, OrderSearchParamDto paramDto);

	/**
	 * Find order detail.
	 *
	 * @param subNumber the sub number
	 * @return the my sub dto
	 */
	public abstract MySubDto findOrderDetail(String subNumber);
	
	/**
	 * Find order detail.
	 *
	 * @param subNumber the sub number
	 * @param shopId the shop id
	 * @return the my sub dto
	 */
	public abstract MySubDto findOrderDetail(String subNumber, Long shopId);
	
	/**
	 * Find order detail.
	 *
	 * @param subNumber the sub number
	 * @param userId the user id
	 * @return the my sub dto
	 */
	public abstract MySubDto findOrderDetail(String subNumber, String userId);
	
	/**
	 * Find user order group.
	 *
	 * @param userId the user id
	 * @param productId the product id
	 * @param skuId the sku id
	 * @param groupId the group id
	 * @return the integer
	 */
	public abstract Integer findUserOrderGroup(String userId, Long productId,
			Long skuId, Long groupId);

	/**
	 * 查询预售订单列表.
	 *
	 * @param queryMap the query map
	 * @param args the args
	 * @param curPageNO 当前页码
	 * @param pageSize 每页显示条数
	 * @param paramDto 订单查询条件DTO
	 * @return the presell orders list
	 */
	public abstract PageSupport<PresellSubDto> getPresellOrdersList(QueryMap queryMap, List<Object> args, int curPageNO, int pageSize,OrderSearchParamDto paramDto);

	/**
	 * 查询预售订单详情.
	 *
	 * @param subNumber the sub number
	 * @return the presell sub dto
	 */
	public abstract PresellSubDto findPresellOrderDetail(String subNumber);

	/**
	 * 查询预售订单详情.
	 *
	 * @param subNumber 订单号
	 * @param userId 用户ID
	 * @return the presell sub dto
	 */
	public abstract PresellSubDto findPresellOrderDetail(String subNumber, String userId);

	/**
	 * Gets the pre sub by sub no and user id.
	 *
	 * @param subNumber the sub number
	 * @param userId the user id
	 * @return the pre sub by sub no and user id
	 */
	public abstract PresellSubEntity getPreSubBySubNoAndUserId(String subNumber, String userId);

	/**
	 * Gets the pre sub by sub no and shop id.
	 *
	 * @param subNumber the sub number
	 * @param shopId the shop id
	 * @return the pre sub by sub no and shop id
	 */
	public abstract PresellSubEntity getPreSubBySubNoAndShopId(String subNumber, Long shopId);

	/**
	 * Gets the pre sub by sub no.
	 *
	 * @param subNumber the sub number
	 * @return the pre sub by sub no
	 */
	public abstract PresellSubEntity getPreSubBySubNo(String subNumber);
	
	/**
	 * 更新订单的退款状态.
	 *
	 * @param subId the sub id
	 * @param refundState the refund state
	 * @return the int
	 */
	public int updateRefundState(Long subId, Integer refundState);
	
	/**
	 * 关闭订单.
	 *
	 * @param subId the sub id
	 * @return the int
	 */
	public int closeSub(Long subId);

	/**
	 * 完成订单.
	 *
	 * @param subId the sub id
	 * @return the int
	 */
	public abstract int finalySub(Long subId);
	
	/**
	 * 获取商家今天的订单数量.
	 *
	 * @param shopId the shop id
	 * @return the today sub count
	 */
	public abstract Integer getTodaySubCount(Long shopId);

	/**
	 * Export order.
	 *
	 * @param string the string
	 * @param array the array
	 * @return the list< order exprot dto>
	 */
	public abstract List<OrderExprotDto> exportOrder(String string, Object[] array);

	/**
	 * Gets the income today.
	 *
	 * @param shopId the shop id
	 * @return the income today
	 */
	public abstract Double getIncomeToday(Long shopId);

	/**
	 * Remind delivery.
	 *
	 * @param subId the sub id
	 * @param userId the user id
	 * @return the int
	 */
	public abstract int remindDelivery(Long subId, String userId);

	/**
	 * 获取所有未支付的订单信息.
	 *
	 * @return the list< sub>
	 */
	public List<Sub> findCancelOrders();

	/**
	 * Update buys.
	 *
	 * @param buys the buys
	 * @param prodId the prod id
	 */
	public abstract void updateBuys(long buys, Long prodId);

	/**
	 * Gets the sub by sub item id.
	 *
	 * @param subItemId the sub item id
	 * @return the sub by sub item id
	 */
	public abstract Sub getSubBySubItemId(Long subItemId);

	/**
	 * Remind delivery by sn.
	 *
	 * @param subNumber the sub number
	 * @param userId the user id
	 * @return the int
	 */
	public abstract int remindDeliveryBySN(String subNumber, String userId);

	/**
	 * Gets the sub list page.
	 *
	 * @param curPageNO the cur page no
	 * @param shopOrderBill the shop order bill
	 * @param subNumber the sub number
	 * @return the sub list page
	 */
	public abstract PageSupport<Sub> getSubListPage(String curPageNO, ShopOrderBill shopOrderBill, String subNumber);

	/**
	 * Gets the orders page.
	 *
	 * @param paramDto the param dto
	 * @param pageSize the page size
	 * @return the orders page
	 */
	public abstract PageSupport getOrdersPage(OrderSearchParamDto paramDto, int pageSize);

	/**
	 * Gets the sub list page.
	 *
	 * @param curPageNO the cur page no
	 * @param shopId the shop id
	 * @param subNumber the sub number
	 * @param shopOrderBill the shop order bill
	 * @return the sub list page
	 */
	public abstract PageSupport<Sub> getSubListPage(String curPageNO, Long shopId, String subNumber,
			ShopOrderBill shopOrderBill);

	/**
	 * Gets the order.
	 *
	 * @param curPageNO the cur page no
	 * @param userName the user name
	 * @return the order
	 */
	public abstract PageSupport<Sub> getOrder(String curPageNO, String userName);

	public abstract PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO,Long shopId);
	
	public abstract PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO,Long shopId, InvoiceSubDto invoiceSubDto);

	/**
	 * Gets the sub by sub number by user id.
	 *
	 * @param subNumbers the sub numbers
	 * @param userId the user id
	 * @return the sub by sub number by user id
	 */
	public abstract List<Sub> getSubBySubNumberByUserId(List<String> subNumbers, String userId);

	/**
	 * Gets the sub by sub nums.
	 *
	 * @param subNums the sub nums
	 * @param userId the user id
	 * @param orderStatus the order status
	 * @return the sub by sub nums
	 */
	public abstract List<Sub> getSubBySubNums(String[] subNums, String userId, Integer orderStatus);
	
	/** 获取门店商品 */
	public abstract StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId);

	public abstract List<Sub> findHaveInGroupSub(Long groupId, Integer groupStatus);

	/** 修改团购订单 */
	public abstract void updateSubGroupStatusByGroup(Long activeId, Integer value);

	public abstract void updateMergeGroupAddStatusByAddNumber(String addNumber);

	public abstract MergeGroupSubDto getMergeGroupSubDtoBySettleSn(String subSettlementSn);

	/**
	 * Export order.
	 *
	 * @param string the string
	 * @param array the array
	 * @return the list< order exprot dto>
	 */
	public abstract List<SubDto> exportSubOrder(String string, Object[] array);

	/**
	 * 根据subNumber获取 MySubDto
	 * @param subNumber
	 * @return
	 */
	public abstract MySubDto getMySubDtoBySubNumber(String subNumber);

	/**
	 * 查询退款的总红包金额
	 */
	public abstract Double getReturnRedpackOffPrice(Set<String> subNumbers);
	
	/**
	 * 根据秒杀活动id 获取秒杀订单购买数量
	 * @param seckillId
	 * @return
	 */
	public abstract Integer getSeckillSubBuyCount(Long seckillId, Long skuId);
	
	/**
	 * 根据秒杀id获取未支付的秒杀订单
	 * @param seckillId
	 * @return
	 */
	public abstract List<Sub> getUnpaymentseckillOrder(Long seckillId);

	PageSupport<IncomeTodayDto> getOrders(Long shopId, String curPageNO, int pageSize);

	PageSupport<VisitLog> getVisitLogs(Long shopId, String curPageNO, int pageSize);

	public abstract MySubDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId);

	/**
	 * 根据交易单号查询订单列表
	 * @param subSettlementSn
	 * @param userId
	 * @return
	 */
	public abstract List<Sub> getSubBySubSettlementSn(String subSettlementSn, String userId);

	/**
	 * 根据交易单号查询订单列表
	 * @param subSettlementSn
	 * @return
	 */
	public abstract List<Sub> getSubBySubSettlementSn(String subSettlementSn);

	/**
	 * 查询当前秒杀活动最后一个已提交但未支付的订单
	 * @param id 
	 * @return 
	 */
	public abstract Sub getLastOneSubmitAndUnPaySeckillSub(Long activeId);
	
	/**
	 * 根据优惠劵获取订单数量
	 * */
	
	public abstract Integer getSubCountByOrderNum(String  orderNums);

	/**
	 * 获取团购运营数据
	 * @param curPageNO 当前页码
	 * @param pageSize 页数
	 * @param groupId 团购ID
	 * @param shopId  店铺ID
	 * @return
	 */
	public abstract PageSupport<Sub> getGroupOperationList(String curPageNO, Integer pageSize, Long groupId,
			Long shopId);

	/**
	 * 获取秒杀订单
	 * @param curPageNO
	 * @param pageSize
	 * @param seckillId
	 * @param shopId
	 * @return
	 */
	PageSupport<Sub> getSeckillOperationList(String curPageNO, int pageSize, Long seckillId, Long shopId);

	/**
	 * 获取预售订单
	 * @param curPageNO
	 * @param size
	 * @param id
	 * @param shopId
	 * @return
	 */
    PageSupport<Sub> getPresellOperationList(String curPageNO, int size, Long id, Long shopId);
}