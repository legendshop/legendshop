/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MailLogDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailLog;
import com.legendshop.spi.service.MailLogService;
import com.legendshop.util.AppUtils;

/**
 * 邮件发送历史Service.
 */
@Service("mailLogService")
public class MailLogServiceImpl  implements MailLogService{
	
	@Autowired
    private MailLogDao mailLogDao;

    public MailLog getMailLog(Long id) {
        return mailLogDao.getMailLog(id);
    }

    public void deleteMailLog(MailLog mailLog) {
        mailLogDao.deleteMailLog(mailLog);
    }

    public Long saveMailLog(MailLog mailLog) {
        if (!AppUtils.isBlank(mailLog.getId())) {
            updateMailLog(mailLog);
            return mailLog.getId();
        }
        return (Long) mailLogDao.save(mailLog);
    }

    public void updateMailLog(MailLog mailLog) {
        mailLogDao.updateMailLog(mailLog);
    }

    public PageSupport getMailLog(CriteriaQuery cq) {
        return mailLogDao.queryPage(cq);
    }

	@Override
	public PageSupport<MailLog> getMailLogPage(String curPageNO, MailLog mailLog) {
		return mailLogDao.getMailLogPage(curPageNO,mailLog);
	}
}
