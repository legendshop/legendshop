/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.UserSecurity;

/**
 * 用户安全配置Dao.
 */
public interface UserSecurityDao extends GenericDao<UserSecurity, Long>{

	/**
	 * Save user security.
	 *
	 * @param userSecurity the user security
	 */
	public abstract void saveUserSecurity(UserSecurity userSecurity);
	

	/**
	 * Save default user security.
	 *
	 * @param userName the user name
	 * @param secLevel the sec level
	 */
	public abstract void saveDefaultUserSecurity(String userName, Integer secLevel);
	
	/**
	 * Save default user security.
	 *
	 * @param userName the user name
	 */
	public void saveDefaultUserSecurity(String userName) ;
	
	/**
	 * Delete user security.
	 *
	 * @param userSecurity the user security
	 * @return the string
	 */
	public abstract void deleteUserSecurity(UserSecurity userSecurity);
	
	/**
	 * Update user security.
	 *
	 * @param userSecurity the user security
	 */
	public abstract void updateUserSecurity(UserSecurity userSecurity);
	
	/**
	 * Gets the user security.
	 *
	 * @param userName the user name
	 * @return the user security
	 */
	public abstract UserSecurity getUserSecurity(String userName);
	
	/**
	 * 验证邮箱验证码是否正确
	 * @param userName
	 * @param code
	 * @return
	 */
	public abstract boolean isMailCodeExist(String userName, String code);

}
