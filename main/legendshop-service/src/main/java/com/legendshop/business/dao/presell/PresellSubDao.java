/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.model.entity.ShopOrderBill;

import java.util.Date;
import java.util.List;

/**
 * The Class PresellSubDao.
 */

public interface PresellSubDao extends Dao<PresellSub, Long> {
     
    public abstract int deletePresellSub(PresellSub presellSub);
	
	public abstract Long savePresellSub(PresellSub presellSub);
	
	public abstract int updatePresellSub(PresellSub presellSub);
	
	public abstract PresellSub getPresellSub(String userId, String subNumber);

	public abstract boolean findIfJoinPresell(Long productId);

	/**
	 * 获取预售订单
	 * @param presellId
	 * @return
	 */
	public PresellSub getPresellSubByPresell(String presellId);

	/**
	 * 根据订单号获取订单
	 * @param subNumber
	 * @return
	 */
	public PresellSub getPresellSub(String subNumber);

	/**
	 * 查询当前期 所有已支付定金未支付尾款但尾款支付时间已过且已关闭的订单
	 * @param shopId
	 * @param endDate
	 * @param orderStatus
	 * @return
	 */
	List<PresellSub> getBillUnPayFinalPresellOrder(Long shopId, Date endDate, Integer orderStatus);

	/**
	 * 根据结算单号获取当期结算的预售订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<PresellSub> queryPresellSubList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill);
}
