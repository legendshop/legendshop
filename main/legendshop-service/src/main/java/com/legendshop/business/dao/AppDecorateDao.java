/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AppDecorate;

/**
 * App装修.
 */
public interface AppDecorateDao extends GenericDao<AppDecorate, Long> {
     
	/**
	 * 获取商家的装修情况.
	 *
	 * @param shopId the shop id
	 * @return the app decorate
	 */
	public abstract AppDecorate getAppDecorate(Long shopId);
	
    /**
     * 删除商家的装修情况.
     *
     * @param appDecorate the app decorate
     * @return the int
     */
    public abstract int deleteAppDecorate(AppDecorate appDecorate);
	
	/**
	 * 保存App装修.
	 *
	 * @param appDecorate the app decorate
	 * @return the long
	 */
	public abstract Long saveAppDecorate(AppDecorate appDecorate);
	
	/**
	 * 更新App装修.
	 *
	 * @param appDecorate the app decorate
	 * @return the int
	 */
	public abstract int updateAppDecorate(AppDecorate appDecorate);
	
 }
