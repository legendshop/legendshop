package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.spi.service.SubSettlementItemService;

@Service("subSettlementItemService")
public class SubSettlementItemServiceImpl implements SubSettlementItemService {
	
	@Autowired
	private SubSettlementItemDao subSettlementItemDao;

	@Override
	public List<SubSettlementItem> getSettlementItem(String settlementSn) {
		return subSettlementItemDao.getSubSettlementItems(settlementSn);
	}

	public SubSettlementItemDao getSubSettlementItemDao() {
		return subSettlementItemDao;
	}
}
