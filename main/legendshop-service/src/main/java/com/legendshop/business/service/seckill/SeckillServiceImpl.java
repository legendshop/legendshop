/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.seckill;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.seckill.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.SeckillStringUtils;
import com.legendshop.business.dao.ProdPropImageDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ProductPropertyDao;
import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.ProdPropImage;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;

/**
 * 秒杀ServiceImpl.
 */
@Service("seckillService")
public class SeckillServiceImpl implements SeckillService {

	@Autowired
	private SeckillActivityDao seckillActivityDao;

	@Autowired
	private SeckillProdDao seckillProdDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProdPropImageDao prodPropImageDao;

	@Autowired
	private ProductPropertyDao productPropertyDao;

	@Autowired
	private ProductPropertyValueDao productPropertyValueDao;

	@Autowired
	private SeckillCachedhandle seckillCachedService;

	@Resource(name="orderSeckillCartResolver")
	private IOrderCartResolver orderCartResolver;

	@Autowired
	private ImgFileService imgFileService;

	/**
	 * 获取秒杀详情
	 */
	@Override
	public SeckillDto getSeckillDetail(Long seckillId) {
		
		// 1.访问redis
 		SeckillDto redisSeckillDto=seckillCachedService.getSeckillDto(seckillId);
		if (AppUtils.isNotBlank(redisSeckillDto)) {
			for (SeckillSkuDto seckillSkuDto : redisSeckillDto.getProdDto().getSkuDtoList()) {
				List<SeckillProd> seckillProdBySId = seckillProdDao.getSeckillProdBySId(redisSeckillDto.getSid());
				for (SeckillProd seckillProd : seckillProdBySId) {
					if (seckillProd.getSkuId().equals(seckillSkuDto.getSkuId())){
						seckillSkuDto.setSeckStock(Integer.parseInt(seckillProd.getSeckillStock().toString()));
					}
				}
			}
			System.out.println("----redis 缓存------ ");
			return redisSeckillDto;
		}
		
		// 2.访问数据库;
		SeckillActivity activity = seckillActivityDao.getSeckillDetail(seckillId);
		// || activity.getStatus().intValue() == 0 || activity.getStatus().intValue() == -2
		if (AppUtils.isBlank(activity)) {
			return null;
		}
		List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySId(seckillId);
		if (AppUtils.isBlank(seckillProds)) {
			return null;
		}
		SeckillProd firstProd = seckillProds.get(0);
		Long prodId = firstProd.getProdId();
		ProductDetail prod = productDao.getProdDetail(prodId);
		if (prod == null || ProductStatusEnum.PROD_DELETE.value().equals(prod.getStatus())) {
			return null;
		}
		SeckillDto seckillDto = new SeckillDto();
		seckillDto.setSid(activity.getId());
		seckillDto.setEndTime(activity.getEndTime());
		seckillDto.setSecDesc(activity.getSeckillDesc());
		seckillDto.setSecBrief(activity.getSeckillBrief());
		seckillDto.setSeckillTitle(activity.getSeckillTitle());
		seckillDto.setSecStatus(activity.getStatus());
		seckillDto.setShopId(activity.getShopId());
		seckillDto.setShopName(activity.getShopName());
		seckillDto.setStartTime(activity.getStartTime());
		seckillDto.setUserId(activity.getUserId());

		SeckillProdDto prodDto = new SeckillProdDto();
		// 拷贝字段
		copyProdField(prodDto, prod);
		
		if(AppUtils.isNotBlank(activity.getSeckillLowPrice())) {
			prodDto.setSeckPrice(activity.getSeckillLowPrice().doubleValue());
		}

		// 获取商品sku信息
		List<SeckillSkuDto> skuDtoList = new ArrayList<SeckillSkuDto>();
		Long[] skuIds = new Long[seckillProds.size()];
		for (int i = 0; i < seckillProds.size(); i++) {
			skuIds[i] = seckillProds.get(i).getSkuId();
		}
		List<Sku> skuList = skuService.getSku(skuIds);
		if (AppUtils.isNotBlank(skuList)) {
			for (Sku sku : skuList) {
				SeckillProd seckillProd = getSeckillProd(sku.getSkuId(), seckillProds);
				if (seckillProd == null)
					continue;
				SeckillSkuDto skuDto = new SeckillSkuDto();
				skuDto.setSkuId(sku.getSkuId());
				skuDto.setName(processName(sku.getName()));
				skuDto.setPrice(sku.getPrice());
				skuDto.setSeckPrice(seckillProd.getSeckillPrice().doubleValue());
				skuDto.setSeckStock(seckillProd.getSeckillStock().intValue());
				skuDto.setProperties(sku.getProperties());
				skuDto.setStatus(sku.getStatus());
				skuDtoList.add(skuDto);
			}
			prodDto.setSkuDtoList(skuDtoList);
			prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));
		}

		// 获取属性图片
		List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
		Map<Long, List<String>> valueImagesMap = new HashMap<Long, List<String>>();
		if (AppUtils.isNotBlank(skuList)) {
			// 获取到商品 所有的属性图片
			List<ProdPropImage> prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
			if (AppUtils.isNotBlank(prodPropImages)) {
				// 根据属性值 进行分组,封装成map
				for (ProdPropImage prodPropImage : prodPropImages) {
					Long valueId = prodPropImage.getValueId();
					List<String> imgs = valueImagesMap.get(valueId);
					if (AppUtils.isBlank(imgs)) {
						imgs = new ArrayList<String>();
					}
					imgs.add(prodPropImage.getUrl());
					valueImagesMap.put(valueId, imgs);
				}
				// 将map转换成 DTO LIST
				Iterator<Long> it = valueImagesMap.keySet().iterator();
				while (it.hasNext()) {
					Long valId = it.next();
					List<String> imgs = valueImagesMap.get(valId);
					PropValueImgDto propValueImgDto = new PropValueImgDto();
					propValueImgDto.setValueId(valId);
					propValueImgDto.setImgList(imgs);
					propValueImgList.add(propValueImgDto);
				}
			}
			prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));
		}

		// 获取商品属性和属性值
		List<ProductPropertyDto> prodPropDtoList = getPropValListByProd(prodDto, skuDtoList, valueImagesMap);
		prodDto.setProdPropDtoList(prodPropDtoList);
		prodDto.setPropValueImgList(propValueImgList);

		// 商品图片列表(没有属性图片时，展示商品图片)
		List<String> prodPics = new ArrayList<String>();
		if (AppUtils.isBlank(propValueImgList)) {
			List<ImgFile> imgFileList = imgFileService.getAllProductPics(prodId);
			if (AppUtils.isNotBlank(imgFileList)) {
				for (ImgFile imgFile : imgFileList) {
					prodPics.add(imgFile.getFilePath());
				}
			}
		} else {
			for(PropValueImgDto propValueImgDto : propValueImgList){
				List<String> imgs =  propValueImgDto.getImgList();
				for (String img : imgs) {
					prodPics.add(img);
				}
			}
		}
		prodDto.setProdPics(prodPics);
		seckillDto.setProdDto(prodDto);
		return seckillDto;
	}

	/**
	 * 执行秒杀
	 */
	@Override
	public int executeSeckill(long seckillId, long prodId, long skuId, String userId, Double price, int sckNumber) {
		return seckillActivityDao.executeSeckill(seckillId, prodId, skuId, userId, price, sckNumber);
	}

	/**
	 * 回滚.
	 *
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @param userId
	 */
	@Override
	public void rollbackSeckill(long seckillId, Long prodId, Long skuId, String userId) {
		seckillCachedService.rollbackSeckill(seckillId, prodId,skuId,userId);
	}

	/**
	 * 查询所有在线的秒杀活动 
	 */
	@Override
	public List<Long> findOnActive() {
		return seckillActivityDao.findOnActive();
	}

	/**
	 * 获取秒杀成功记录 
	 */
	@Override
	public SeckillSuccess getSeckillSucess(String userId, Long seckillId, long prodId, long skuId) {
		return seckillActivityDao.getSeckillSucess(userId, seckillId, prodId, skuId);
	}

	/**
	 * 获取秒杀成功商品Dto 
	 */
	@Override
	public SuccessProdDto findSuccessProdDto(SeckillSuccess seckillSuccess) {
		return seckillProdDao.findSuccessProdDto(seckillSuccess);
	}

	/**
	 * 添加秒杀订单 
	 */
	@Override
	public List<String> addSeckillOrder(UserShopCartList userShopCartList, SeckillSuccess seckillSuccess) {
		int result = seckillActivityDao.update("update ls_seckill_success set status=1 where id=? and user_id=? and  status=0 ",
				new Object[] { seckillSuccess.getId(), seckillSuccess.getUserId() });
		if (result > 0) {
			List<String> list = orderCartResolver.addOrder(userShopCartList);
			return list;
		}
		return null;
	}

	/**
	 * 更新秒杀成功数据 
	 */
	@Override
	public int updateSeckillSuccess(Long basketId, String userId) {
		return seckillActivityDao.update("update ls_seckill_success set status=1 where id=? and user_id=? and  status=0", new Object[] { basketId, userId });
	}

	/**
	 * 秒杀开关 
	 */
	@Override
	public SeckillJsonDto<SeckillExposer> exportSeckillUrl(long seckillId, long prodId, long skuId) {
		SeckillJsonDto<SeckillExposer> seckillJsonDto = new SeckillJsonDto<SeckillExposer>();
		
		SeckillDto seckillDto=getSeckillDetail(seckillId);
		
		if (AppUtils.isBlank(seckillDto)) {
			// 如果该秒杀商品不存在则关闭这个接口
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.NOT_FIND.getState());
			seckillJsonDto.setError("找不到秒杀商品对象");
			seckillJsonDto.setStock(0);
			return seckillJsonDto; // 返回一个关闭的接口
		}
		
		Date startTime = seckillDto.getStartTime();
		Date endTime = seckillDto.getEndTime();
		Date now = new Date();
		Integer number = seckillCachedService.getSeckillProdStock(seckillId, prodId, skuId);
		
		if (now.getTime() >= endTime.getTime()) { // 秒杀结束
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.TIME_OUT.getState());
			seckillJsonDto.setStock(number);
			return seckillJsonDto;
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.READY.getState());
			seckillJsonDto.setStock(number);
			return seckillJsonDto;
		}
		
		if (number == null || number <= 0) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.SELL_OUT.getState());
			seckillJsonDto.setStock(number);
			return seckillJsonDto;
		}

		String builderMd5 = SeckillStringUtils.joint(seckillId, prodId, skuId);
		String _md5 = MD5Util.toMD5(builderMd5 + Constants._MD5);

		seckillJsonDto.setIsSuccess(true);
		seckillJsonDto.setStatus(SeckillStateEnum.SUCCESS.getState());
		seckillJsonDto.setData(new SeckillExposer(seckillId, _md5, true));
		seckillJsonDto.setStock(number);
		return seckillJsonDto;
	}

	/**
	 * 获取秒杀商品信息
	 *
	 * @param skuId
	 * @param prods
	 * @return the seckill prod
	 */
	private SeckillProd getSeckillProd(Long skuId, List<SeckillProd> prods) {
		for (Iterator<SeckillProd> iterator = prods.iterator(); iterator.hasNext();) {
			SeckillProd seckillProd = (SeckillProd) iterator.next();
			if (seckillProd.getSkuId() != 0 && skuId.equals(seckillProd.getSkuId())) {
				return seckillProd;
			}
		}
		return null;
	}

	/**
	 * 拷贝部分字段值.
	 *
	 * @param prodDto
	 * @param prodDetail
	 */
	private void copyProdField(SeckillProdDto prodDto, ProductDetail prodDetail) {
		prodDto.setProdId(prodDetail.getProdId());
		prodDto.setName(processName(prodDetail.getName()));
		prodDto.setCash(prodDetail.getCash());
		prodDto.setPic(prodDetail.getPic());
		prodDto.setBrief(prodDetail.getBrief());
		prodDto.setModelId(prodDetail.getModelId());
		prodDto.setStatus(prodDetail.getStatus());
		prodDto.setContent(prodDetail.getContent());
		prodDto.setContentM(prodDetail.getContentM());
		prodDto.setTransportId(prodDetail.getTransportId());
		prodDto.setSupportTransportFree(prodDetail.getSupportTransportFree());
		prodDto.setTransportType(prodDetail.getTransportType());
		prodDto.setEmsTransFee(prodDetail.getEmsTransFee());
		prodDto.setExpressTransFee(prodDetail.getExpressTransFee());
		prodDto.setMailTransFee(prodDetail.getMailTransFee());
		prodDto.setCategoryId(prodDetail.getCategoryId());
		prodDto.setProVideoUrl(prodDetail.getProVideoUrl());
	}

	/**
	 * 处理名字
	 *
	 * @param name
	 * @return the string
	 */
	private String processName(String name) {
		if (name == null) {
			return null;
		}
		name = name.replace("\"", "〞");
		name = name.replace("'", "’");
		return name;
	}

	/**
	 * 根据商品id获取 属性和属性值.
	 *
	 * @param prodDto
	 * @param productSkus
	 * @param valueImagesMap
	 * @return the prop val list by prod
	 */
	private List<ProductPropertyDto> getPropValListByProd(SeckillProdDto prodDto, List<SeckillSkuDto> productSkus, Map<Long, List<String>> valueImagesMap) {
		List<Long> selectedVals = new ArrayList<Long>();
		if (AppUtils.isNotBlank(productSkus)) {
			// 取得价格最低的 sku 作为默认选中的sku
			SeckillSkuDto defaultSku = getDefaultSku(productSkus);
			prodDto.setCash(defaultSku.getPrice());
			if (AppUtils.isNotBlank(defaultSku.getName())) {
				prodDto.setName(defaultSku.getName());
			}
			// 取得sku字符串(k1:v1;k2:v2;k3:v3)中的每对key:value,并对其进行相应处理
			Map<Long, List<Long>> propIdMap = new HashMap<Long, List<Long>>();
			List<Long> allValIdList = new ArrayList<Long>();
			for (SeckillSkuDto sku : productSkus) {
				String properties = sku.getProperties();
				if (AppUtils.isNotBlank(properties)) {
					String skuStrs[] = properties.split(";");
					for (int i = 0; i < skuStrs.length; i++) {
						String skuItems[] = skuStrs[i].split(":");
						Long propId = Long.valueOf(skuItems[0]);
						Long valueId = Long.valueOf(skuItems[1]);
						if (sku.getSkuId().equals(defaultSku.getSkuId())) {
							selectedVals.add(valueId);
						}
						List<Long> valIdList = propIdMap.get(propId);
						if (AppUtils.isBlank(valIdList)) {
							valIdList = new ArrayList<Long>();
							valIdList.add(valueId);
						} else {
							if (!valIdList.contains(valueId)) {
								valIdList.add(valueId);
							}
						}
						propIdMap.put(propId, valIdList);
						allValIdList.add(valueId);
					}
				}
			}

			List<Long> propIds = new ArrayList<Long>(propIdMap.keySet());// set
																			// 转
																			// list
			if (AppUtils.isBlank(propIds)) {
				return null;
			}

			// 根据propId的集合 ，查出属性集合
			List<ProductProperty> pps = productPropertyDao.getProductProperty(propIds, prodDto.getCategoryId());
			List<ProductPropertyDto> ppds = new ArrayList<ProductPropertyDto>();
			// 根据valId的集合，查出属性值的集合
			List<ProductPropertyValue> allPropVals = productPropertyValueDao.getProductPropertyValue(allValIdList, prodDto.getProdId());
			for (ProductProperty prodProp : pps) {
				// 封装属性DTO
				ProductPropertyDto prodPropDto = new ProductPropertyDto();
				prodPropDto.setPropId(prodProp.getId());// 属性id
				prodPropDto.setPropName(prodProp.getMemo());// 属性名称

				// 根据propId拿到valueId的集合
				List<Long> valIds = propIdMap.get(prodProp.getId());

				// 根据valueId的集合,查出属性值的集合
				List<ProductPropertyValue> ppvs = getPropValsByIds(valIds, allPropVals);
				List<ProductPropertyValueDto> ppvds = new ArrayList<ProductPropertyValueDto>();
				for (ProductPropertyValue prodPropVal : ppvs) {
					// 封装属性值DTO
					ProductPropertyValueDto ppvd = new ProductPropertyValueDto();
					ppvd.setValueId(prodPropVal.getId());// 属性值id
					if (AppUtils.isNotBlank(prodPropVal.getAlias())) {
						ppvd.setName(prodPropVal.getAlias());// 属性值名称
					} else {
						ppvd.setName(prodPropVal.getName());// 属性值名称
					}
					ppvd.setPropId(prodProp.getId());// 属性id

					List<String> imgs = valueImagesMap.get(prodPropVal.getId());
					if (AppUtils.isNotBlank(imgs)) {
						prodPropVal.setUrl(imgs.get(0));
					}
					if (selectedVals.contains(prodPropVal.getId())) {
						ppvd.setIsSelected(true);// 是否默认选中
						if (AppUtils.isNotBlank(prodPropVal.getUrl())) {
							prodDto.setPic(prodPropVal.getUrl());
						}
					}
					if (AppUtils.isNotBlank(prodPropVal.getUrl())) {
						ppvd.setPic(prodPropVal.getUrl());
					} else {
						ppvd.setPic(prodPropVal.getPic());
					}
					ppvds.add(ppvd);
				}
				prodPropDto.setProductPropertyValueList(ppvds);
				ppds.add(prodPropDto);
			}
			return ppds;
		} else {
			return null;
		}
	}

	/**
	 * 查找最低价格的sku作为默认的sku.
	 *
	 * @param productSkus
	 * @return the default sku
	 */
	private SeckillSkuDto getDefaultSku(List<SeckillSkuDto> productSkus) {
		double price = 0;
		int minPos = 0; // 最小值位置
		int size = productSkus.size();
		for (int i = 0; i < size; i++) {
			double minPrice = productSkus.get(i).getPrice();
			if ((minPrice < price || price == 0) && productSkus.get(i).getStatus().equals(1)) {
				price = minPrice;
				minPos = i;
			}
		}
		return productSkus.get(minPos);
	}

	/**
	 * 根据valueId的集合,查出属性值的集合.
	 *
	 * @param valIds
	 * @param allPropVals
	 * @return the prop vals by ids
	 */
	private List<ProductPropertyValue> getPropValsByIds(List<Long> valIds, List<ProductPropertyValue> allPropVals) {
		List<ProductPropertyValue> ppvs = new ArrayList<ProductPropertyValue>();
		for (ProductPropertyValue productPropertyValue : allPropVals) {
			if (valIds.contains(productPropertyValue.getValueId())) {
				ppvs.add(productPropertyValue);
			}
		}
		return ppvs;
	}

	/**
	 * 删除秒杀记录
	 */
	@Override
	public void deleteRecord(Long id, String userId) {
		seckillActivityDao.deleteRecord(id, userId);
	}

	/**
	 * 查询秒杀成功的记录信息 
	 */
	@Override
	public PageSupport<SeckillSuccessRecord> querySeckillRecord(String curPageNO, String userId) {
		return seckillActivityDao.querySimplePage(curPageNO, userId);
	}

	/**
	 * 查询用户秒杀记录 
	 */
	@Override
	public PageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId) {
		return seckillActivityDao.queryUserSeckillRecord(curPageNO, userId);
	}

	/**
	 * 根据秒杀活动id查询秒杀成功的记录信息
	 */
	@Override
	public PageSupport<SeckillSuccess> querySeckillRecordListPage(String curPageNO, Long id) {
		return seckillActivityDao.querySeckillRecordListPage(curPageNO, id);
	}

	/**
	 * 取消超时未转订单资格
	 */
	@Override
	public void cancleSeckillOrder(Long seckillId,List<SeckillSuccess> seckillSuccessList) {
		
		if(AppUtils.isNotBlank(seckillSuccessList)){//如果秒杀记录不为空
			for (SeckillSuccess seckillSuccess : seckillSuccessList) {
				seckillActivityDao.update("update ls_seckill_success set status=-1 where id=? and user_id=? and status=0", new Object[] { seckillSuccess.getId(), seckillSuccess.getUserId()});
			}
		}
		//更新秒杀活动改状态为“转换订单结束”
		SeckillActivity seckillActivity =seckillActivityDao.getSeckillActivity(seckillId);
		seckillActivity.setStatus(SeckillStatusEnum.TRANSORDER.value());
		seckillActivityDao.updateSeckillActivity(seckillActivity);
	}

	@Override
	public int cancleUnpaymentSeckillOrder(Long seckillId, String userId) {
		return seckillActivityDao.update("update ls_seckill_success set status=-1 where seckill_id=? and user_id=? and status=1", seckillId, userId);
	}

	@Override
	public OperateStatisticsDTO getOperateStatistics(Long id) {
		return seckillActivityDao.getOperateStatistics(id);
	}

	@Override
	public Integer getPersonCount(Long id) {
		return seckillActivityDao.getPersonCount(id);
	}

	@Override
	public SeckillActivity getSeckillByProd(Long prodId, Long skuId) {
		return seckillActivityDao.getSeckillByProd(prodId, skuId);
	}

}
