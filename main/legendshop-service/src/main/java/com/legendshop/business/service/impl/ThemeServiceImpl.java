/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ThemeDao;
import com.legendshop.business.dao.ThemeModuleDao;
import com.legendshop.business.dao.ThemeModuleProdDao;
import com.legendshop.business.dao.ThemeRelatedDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ThemeStatusEnum;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Theme;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.model.entity.ThemeRelated;
import com.legendshop.model.vo.ThemeVo;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ThemeModuleService;
import com.legendshop.spi.service.ThemeService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 专题服务.
 */
@Service("themeService")
public class ThemeServiceImpl implements ThemeService {

	private final static Logger LOGGER = LoggerFactory.getLogger(ThemeServiceImpl.class);

	@Autowired
	private ThemeDao themeDao;

	@Autowired
	private ThemeRelatedDao themeRelatedDao;

	@Autowired
	private ThemeModuleDao themeModuleDao;

	@Autowired
	private ThemeModuleProdDao themeModuleProdDao;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ThemeModuleService  themeModuleService;

	@Override
	public Theme getTheme(Long id) {
		return themeDao.getTheme(id);
	}

	@Override
	public void deleteTheme(Theme theme) {
		themeDao.deleteTheme(theme);
	}

	@Override
	public Long saveTheme(Theme theme) {
		if (!AppUtils.isBlank(theme.getThemeId())) {
			Theme originTheme = themeDao.getTheme(theme.getThemeId());
			theme.setUpdateTime(new Date());
			theme.setCreateTime(originTheme.getCreateTime());
			theme.setStatus(originTheme.getStatus());
			updateTheme(theme);
			return theme.getThemeId();
		}
		theme.setStatus(ThemeStatusEnum.OFFLINE.value());
		theme.setCreateTime(new Date());
		return themeDao.saveTheme(theme);
	}

	@Override
	public void updateTheme(Theme theme) {
		themeDao.updateTheme(theme);
	}

	@Override
	public PageSupport getTheme(String curPageNo, Integer pageSize) {
		return themeDao.getThemePage(curPageNo, pageSize);
	}

	@Override
	public String deleteTheme(Long id) {
		try {
			if (id == null) {
				return "ID不能为空";
			}
			Theme theme = themeDao.getTheme(id);
			if (theme == null) {
				return "找不到该专题";
			} else if (!theme.getStatus().equals(ThemeStatusEnum.DEL.value())) {
				return "该专题在不在回收状态，请先下线";
			}

			/**
			 * TODO Cache 1.删除专题关联模块 2.删除专题管理商品模块及关联商品模块的商品 3.删除专题 4.清理专题包含的所有图片
			 * TODO 是否真实删除???
			 */

			// 1.删除专题关联模块
			List<ThemeRelated> themeRelateds = themeRelatedDao.getThemeRelatedByTheme(id);
			if (AppUtils.isNotBlank(themeRelateds)) {
				for (int i = 0; i < themeRelateds.size(); i++) {
					ThemeRelated themeRelated = themeRelateds.get(i);
					if (themeRelated != null) {
						themeRelatedDao.deleteThemeRelated(themeRelated);
						// 删除相关专题图片
						if (AppUtils.isNotBlank(themeRelated.getImg())) {
							attachmentManager.deleteTruely(themeRelated.getImg());
							attachmentManager.delAttachmentByFilePath(themeRelated.getImg());
						}
					}
				}

			}

			// 2.删除专题管理商品模块及关联商品模块的商品
			List<ThemeModule> themeModules = themeModuleDao.getThemeModuleByTheme(id); // 获取相关模块
			if (AppUtils.isNotBlank(themeModules)) {
				List<ThemeModule> objModules = new ArrayList<ThemeModule>();
				for (int i = 0; i < themeModules.size(); i++) {
					ThemeModule module = themeModules.get(i);
					if (AppUtils.isNotBlank(module)) {

						objModules.add(module);

						// 查询关联商品模块的商品
						List<ThemeModuleProd> themeModuleProds = themeModuleProdDao.getThemeModuleProdByModuleid(module.getThemeModuleId());
						List<ThemeModuleProd> moduleProds = new ArrayList<ThemeModuleProd>();
						if (AppUtils.isNotBlank(themeModuleProds)) {
							for (int j = 0; j < themeModuleProds.size(); j++) {
								ThemeModuleProd moduleProd = themeModuleProds.get(j);
								if (moduleProd != null) {
									Product prodInfo = productService.getProductById(moduleProd.getProdId());
									// 删除板块商品图片 如果是商品表的原有图片则不删
									if (AppUtils.isNotBlank(moduleProd.getImg()) && !moduleProd.getImg().equals(prodInfo.getPic())) {
										attachmentManager.deleteTruely(moduleProd.getImg());
										attachmentManager.delAttachmentByFilePath(moduleProd.getImg());
									}
									// 删除角标图片
									if (AppUtils.isNotBlank(moduleProd.getAngleIcon())) {
										attachmentManager.deleteTruely(moduleProd.getAngleIcon());
										attachmentManager.delAttachmentByFilePath(moduleProd.getAngleIcon());
									}
									moduleProds.add(moduleProd);
								}
							}
						}

						if (moduleProds.size() > 0) {
							themeModuleProdDao.deleteThemeModuleProd(moduleProds);
						}
						moduleProds = null;
						themeModuleProds = null;
						// 删除专题模块图片
						deltethemeModuleFile(module);

					}
				}

				if (objModules.size() > 0) {
					themeModuleDao.deleteThemeModule(objModules);
				}
			}
			// 删除专题图片
			delteThemeFile(theme);

			// 3.删除专题
			themeDao.deleteTheme(theme);

		} catch (Exception e) {
			LOGGER.error("deleteThemes fail with id " + id, e);
			return Constants.FAIL;
		}

		return Constants.SUCCESS;
	}

	private void delteThemeFile(Theme theme) {
		List<String> files = new ArrayList<String>();
		if (AppUtils.isNotBlank(theme.getBannerPcImg())) {
			files.add(theme.getBannerPcImg());
		}
		if (AppUtils.isNotBlank(theme.getBannerMobileImg())) {
			files.add(theme.getBannerMobileImg());
		}
		if (AppUtils.isNotBlank(theme.getBackgroundPcImg())) {
			files.add(theme.getBackgroundPcImg());
		}
		if (AppUtils.isNotBlank(theme.getBackgroundMobileImg())) {
			files.add(theme.getBackgroundMobileImg());
		}
		if (AppUtils.isNotBlank(theme.getThemePcImg())) {
			files.add(theme.getThemePcImg());
		}
		if (AppUtils.isNotBlank(theme.getThemeMobileImg())) {
			files.add(theme.getThemeMobileImg());
		}
		if (AppUtils.isNotBlank(files)) {
			for (String file : files) {
				attachmentManager.deleteTruely(file);
				attachmentManager.delAttachmentByFilePath(file);
			}
		}
	}

	private void deltethemeModuleFile(ThemeModule module) {
		List<String> files = new ArrayList<String>();
		if (AppUtils.isNotBlank(module.getTitleBgPcimg())) {
			files.add(module.getTitleBgPcimg());
		}
		if (AppUtils.isNotBlank(module.getTitleBgMobileimg())) {
			files.add(module.getTitleBgMobileimg());
		}
		if (AppUtils.isNotBlank(files)) {
			for (String file : files) {
				attachmentManager.deleteTruely(file);
				attachmentManager.delAttachmentByFilePath(file);
			}
		}
	}

	@Override
	public String updataStatus(Long id, Integer status) {
		try {
			if (id == null || status == null) {
				return "ID/status 不能为空";
			}
			Theme theme = themeDao.getTheme(id);
			if (theme == null) {
				return "找不到该专题";
			} else if (status == theme.getStatus()) {
				return Constants.SUCCESS;
			}
			theme.setStatus(status);
			themeDao.updateTheme(theme);
		} catch (Exception e) {
			LOGGER.error("deleteThemes fail with id " + id, e);
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}
	
	@Override
	//@Cacheable(value = "ThemeVo",key="'ThemeVo'+#themeId")
	public ThemeVo getThemeDetail(Long themeId) {
		ThemeVo themeVo=null;
		// 获取当前专题
		Theme theme = themeDao.getTheme(themeId);
		if (AppUtils.isNotBlank(theme)) {
			long ctime = new Date().getTime();
			Date end_date = theme.getEndTime();
			if ( (end_date!=null&& ctime > end_date.getTime()) || theme.getStatus().intValue() != ThemeStatusEnum.NORMAL.value()) {
				return themeVo;
			}
			themeVo=new ThemeVo();
			themeVo.setTheme(theme);
			
			// 获取相关专题
			List<ThemeRelated> themeRelateds =themeRelatedDao.getThemeRelatedByTheme(themeId);
			themeVo.setThemeRelateds(themeRelateds);
			// 获取专题相关板块的的商品
			// 获取板块以及商品
			Map<ThemeModule, List<ThemeModuleProd>> moduleProdMap = themeModuleService.getThemeModuleAndProdByThemeId(themeId);
			themeVo.setModuleProdMap(moduleProdMap);
			
			return themeVo;
		} else {
			return themeVo;
		}
	}

	@Override
	public void clearThemeDetailCache(Long themeId) {
        themeDao.clearThemeDetailCache(themeId);		
	}

	@Override
	public List<Theme> getTheme() {
		return themeDao.getTheme();
	}


	@Override
	public PageSupport<Theme> allThemes(String curPageNO) {
		return themeDao.allThemes(curPageNO);
	}
	
	@Override
	public PageSupport<Theme> getThemePage(String curPageNO, String name) {
		return themeDao.getThemePage(curPageNO,name);
	}

	@Override
	public PageSupport<Theme> getThemePage(String curPageNO) {
		return themeDao.getThemePage(curPageNO);
	}

	@Override
	public PageSupport<Theme> getThemePage(String curPageNO, Theme entity, DataSortResult result) {
		return themeDao.getThemePage(curPageNO,entity,result);
	}

	@Override
	public PageSupport<Theme> getThemeQueryContent(String curPageNO,Integer status, DataSortResult result, String title) {
		return themeDao.getThemeQueryContent(curPageNO,status,result,title);
	}

	@Override
	public void updateTheme(String type, Theme theme) {
		if (type.equals("themePcImg")) {
			if (AppUtils.isNotBlank(theme.getThemePcImg())) {
				attachmentManager.deleteTruely(theme.getThemePcImg());
				attachmentManager.delAttachmentByFilePath(theme.getThemePcImg());
				theme.setThemePcImg(null);
			}
		} else if (type.equals("themeMobileImg")) {
			if (AppUtils.isNotBlank(theme.getThemeMobileImg())) {
				attachmentManager.deleteTruely(theme.getThemeMobileImg());
				attachmentManager.delAttachmentByFilePath(theme.getThemeMobileImg());
				theme.setThemeMobileImg(null);
			}
		} else if (type.equals("bannerPcImg")) {
			if (AppUtils.isNotBlank(theme.getBannerPcImg())) {
				attachmentManager.deleteTruely(theme.getBannerPcImg());
				attachmentManager.delAttachmentByFilePath(theme.getBannerPcImg());
				theme.setBannerPcImg(null);
			}
		} else if (type.equals("bannerMobileImg")) {
			if (AppUtils.isNotBlank(theme.getBannerMobileImg())) {
				attachmentManager.deleteTruely(theme.getBannerMobileImg());
				attachmentManager.delAttachmentByFilePath(theme.getBannerMobileImg());
				theme.setBannerMobileImg(null);
			}
		} else if (type.equals("backgroundPcImg")) {
			if (AppUtils.isNotBlank(theme.getBackgroundPcImg())) {
				attachmentManager.deleteTruely(theme.getBackgroundPcImg());
				attachmentManager.delAttachmentByFilePath(theme.getBackgroundPcImg());
				theme.setBackgroundPcImg(null);
			}
		} else if (type.equals("backgroundMobileImg")) {
			if (AppUtils.isNotBlank(theme.getBackgroundMobileImg())) {
				attachmentManager.deleteTruely(theme.getBackgroundMobileImg());
				attachmentManager.delAttachmentByFilePath(theme.getBackgroundMobileImg());
				theme.setBackgroundMobileImg(null);
			}
		}
		themeDao.updateTheme(theme);
		
	}

    @Override
    public PageSupport<Theme> getSelectTheme(String curPageNO, String title,Long themeId) {
        return themeDao.getSelectTheme(curPageNO,title,themeId);
    }


}
