package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Area;

public interface AreaDao extends Dao<Area, Integer> {

	List<KeyValueEntity> getAreaList(Integer cityid);

	List<Area> getAreaByCityid(Integer cityid);

	void deleteAreaByProvinceid(Integer provinceid);

	void deleteAreaByCityid(Integer cityid);

	void deleteAreaById(Integer id);

	void updateArea(Area area);

	void deleteAreaList(List<Integer> idList);

	void saveArea(Area area);

	Area getAreaById(Integer id);

}
