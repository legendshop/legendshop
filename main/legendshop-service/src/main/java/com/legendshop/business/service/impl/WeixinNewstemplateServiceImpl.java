/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinNewstemplateDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;
import com.legendshop.spi.service.WeixinNewstemplateService;
import com.legendshop.util.AppUtils;

/**
 *微信素材服务
 */
@Service("weixinNewstemplateService")
public class WeixinNewstemplateServiceImpl  implements WeixinNewstemplateService{

	@Autowired
    private WeixinNewstemplateDao weixinNewstemplateDao;

    public WeixinNewstemplate getWeixinNewstemplate(Long id) {
        return weixinNewstemplateDao.getWeixinNewstemplate(id);
    }

    public void deleteWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate) {
        weixinNewstemplateDao.deleteWeixinNewstemplate(weixinNewstemplate);
    }

    public Long saveWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate) {
        if (!AppUtils.isBlank(weixinNewstemplate.getId())) {
            updateWeixinNewstemplate(weixinNewstemplate);
            return weixinNewstemplate.getId();
        }
        return weixinNewstemplateDao.saveWeixinNewstemplate(weixinNewstemplate);
    }

    public void updateWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate) {
        weixinNewstemplateDao.updateWeixinNewstemplate(weixinNewstemplate);
    }

	@Override
	public PageSupport<WeixinNewstemplate> getWeixinNewstemplate(String curPageNO, WeixinNewstemplate weixinNewstemplate, int pageSize) {
		return weixinNewstemplateDao.getWeixinNewstemplate(curPageNO,weixinNewstemplate,pageSize);
	}
}
