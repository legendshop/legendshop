/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.Sub;

/**
 * 单品Dao.
 */

public interface SkuDao extends GenericDao<Sku, Long>{

	public abstract Sku getSku(Long id);
	
	public  abstract void clearSkuByProd(Long prodId);
	
	public void cleanSkuCache(Long skuId);
	
    public abstract void deleteSku(Sku sku);
	
	public abstract Long saveSku(Sku sku);
	
	public abstract void updateSku(Sku sku);
	
	public abstract PageSupport<Sku> getSku(CriteriaQuery cq);

	public abstract void saveSku(List<Sku> skuList);

	//有缓存
	public abstract List<Sku> getSkuByProd(Long prodId);

	//没有缓存
	public abstract List<Sku> getSkuByProdId(Long prodId);
	
	public abstract Integer getStocksByLockMode(Long skuId);
	
	Integer getActualStocksByLockMode(Long skuId);

	/**
	 * 此方法为按最终结果更新，有可能存在并发更新问题
	 * @param skuId
	 * @param stocks
	 */
	public abstract void updateSkuStocks(Long skuId, long stocks);

	/**
	 * 只更新库存变化的数量，应不存在并发问题
	 * @param skuId
	 * @param stocks
	 */
	public abstract void updateSkuStocksByRealTime(Long skuId, long stocks);


	public abstract void updateSkuActualStocks(Long skuId, long stocks);

	public abstract Sku getSkuByProd(Long prodId, Long skuId);

	List<Sku> getSku(Long[] skuId);

	public abstract Integer getStocksByMode(Long skuId);

	public abstract BasketDto findBasketDtos(Long prodId,Long skuId);

	public abstract PageSupport<Sku> getSkuPage(String curPageNO, Sku sku);

	public abstract PageSupport<Sku> getAuctionProdSku(String curPageNO, Long prodId);

	public abstract PageSupport<Sku> getloadProdSkuPage(String curPageNO, Long prodId);

	public abstract PageSupport<Sku> getLoadProdSkuByProdId(String curPageNO, Long prodId);
	
	public abstract PageSupport<Sku> queryPage(String curPageNO, Long prodId, int status, int stocks);

	public abstract PageSupport<Sku> getseckillProdSku(String curPageNO, Long prodId);

	public abstract PageSupport<Sku> getSku(String curPageNO, Long prodId, int status);

	public abstract PageSupport<Sku> loadSkuListPage(String curPageNO, Long prodId, String name);

	/** 根据规格id查找对应的sku */
	public abstract List<Sku> findSkuByPropId(Long propId);

	public abstract  List<Sku> findSkuByProp(String prop);

	public abstract List<Sku> findSkuByPropIdAndProdTypeId(Long propId, Long prodTypeId);

	public abstract List<Sku> getSkuForMergeGroup(Long prodId, Long mergeGroupId);

	/**
	 * 根据skuId 获取 SKU
	 * @param skuId
	 * @return
	 */
	public abstract Sku getSkuById(Long skuId);

	/**
	 * 根据skuId 修改 skuType
	 * @param skuId
	 * @param skuType 修改为目标活动类型
	 * @param originSkuType 原来的活动类型
	 */
	public abstract int updateSkuTypeById(Long skuId, String skuType, String originSkuType);

	/**
	 * 根据prodId 修改 skuType
	 * @param productId
	 * @param value
	 */
	public abstract void updateSkuTypeByProdId(Long productId, String skuType, String originSkuType);

	/**
	 * 根据SkuId补充库存 [ 实际库存、销售库存 ]
	 * */
	void updateSkuAllStocks(Long skuId,Long basketCount);

}
