/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.dao.LoginHistoryDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.LoginHistorySum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.model.entity.LoginHistory;
import com.legendshop.spi.service.LoginHistoryService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.ip.IPSeeker;

/**
 * 登录历史服务.
 */
@Service("loginHistoryService")
public class LoginHistoryServiceImpl implements LoginHistoryService {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(LoginHistoryServiceImpl.class);

	@Autowired
	private LoginHistoryDao loginHistoryDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 *记录登录历史
	 */
	@Override
	public void saveLoginHistory(LoginSuccess loginSuccess) {
		
		if (systemParameterUtil.isLoginLogEnable()) {
//			if(!isboolIp(ip)){
//				log.error("invaild ip {} by userName {}", ip, userName);
//				return;
//			}
			String userName = loginSuccess.getUserName();
			String ip = loginSuccess.getIpAddress();
			String userId = loginSuccess.getUserId();
			log.debug("Login with userName {}, ipAddress {}", loginSuccess.getUserName(), loginSuccess.getIpAddress());
			try {
				LoginHistory loginHistory = new LoginHistory();
				if("127.0.0.1".equals(ip)||"0:0:0:0:0:0:0:1".equals(ip)){ //本地测试
					loginHistory.setArea("CZ88.NET");
					loginHistory.setCountry("本机地址");
				}else{
					  ExecutorService executors=Executors.newSingleThreadExecutor();
					  Future<String> future= executors.submit( new Callable<String>(){
								@Override
								public String call() throws Exception {
									String result=HttpUtil.httpGet("http://ip.taobao.com/service/getIpInfo.php?ip="+ip);
									return result;
								}
					  });
					  String dataString="";
					  try {
						   dataString= future.get(4, TimeUnit.SECONDS);
						   if(AppUtils.isNotBlank(dataString)){
							   JSONObject data=JSONObject.parseObject(dataString);
							   JSONObject result=JSONObject.parseObject(data.get("data").toString());
							   loginHistory.setCountry(result.getString("country")+
									   result.getString("region")+result.getString("city")+result.getString("area")+result.getString("county"));
						   }
					  } catch (Exception e) {
					  }finally{
						 future.cancel(true);
						  executors.shutdown();
					 }
					 if (AppUtils.isBlank(loginHistory.getCountry())) {
						loginHistory.setArea(IPSeeker.getInstance().getArea(ip));
						String Country = IPSeeker.getInstance().getCountry(ip);
						loginHistory.setCountry(Country);
						if (Country.equals("错误的IP数据库文件")
								|| Country.equals("未知国家")) {
							loginHistory.setCountry("IANA");
						}
					 }
				}
				
				String userName2 = userDetailDao.getUserNameByPhoneOrUsername(userName,userName);
				//获取为null，表示不为普通用户，进入管理员查询
				if(userName2 == null){
					userName2 = userName;
				}
				loginHistory.setIp(ip);
				loginHistory.setTime(new Date());
				loginHistory.setUserName(userName2);
				loginHistory.setUserId(userId);
				loginHistory.setLoginType(loginSuccess.getLoginUserType());
				loginHistory.setLoginType(loginSuccess.getLoginSource());
				
				//保存登录历史
				loginHistoryDao.saveLoginHistory(loginHistory);
				
				//更新用户的登录时间
				userDetailDao.updateUserLoginHistory(loginHistory.getIp(), loginHistory.getUserName());

			} catch (Exception e) {
				log.error("save userLoginHistory", e);
			}
		}
	}
	
	@Override
	@Cacheable(value = "LoginHistory")
	public LoginHistory getLastLoginTime(String userName) {
		return loginHistoryDao.getLastLoginTime(userName);
	}

	@Override
	public PageSupport<LoginHistory> getLoginHistoryPage(String curPageNO, LoginHistory login) {
		return loginHistoryDao.getLoginHistoryPage(curPageNO,login);
	}

	@Override
	public PageSupport<LoginHistorySum> getLoginHistoryBySQL(String curPageNO, LoginHistory login) {
		return loginHistoryDao.getLoginHistoryBySQL(curPageNO,login);
	}


}
