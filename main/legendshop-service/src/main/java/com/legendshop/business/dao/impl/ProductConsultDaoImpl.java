/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.util.DateUtil;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.ProductConsultUpdate;
import com.legendshop.business.dao.ProductConsultDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.ProductConsultDto;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 商品咨询
 */
@Repository
public class ProductConsultDaoImpl extends GenericDaoImpl<ProductConsult, Long> implements ProductConsultDao {

	private int interval = -5; // 单位为分钟
	
	private String sqlForGetConsult = " select c.cons_id as consId, c.content,c.answer  from ls_prod_cons c, ls_prod p where c.prod_id = p.prod_id and p.user_id =? and c.cons_id = ?";
	
	private String sqlForDeleteProductConsult = "delete from ls_prod_cons  where  exists (select 1 from ls_prod p where ls_prod_cons.prod_id = p.prod_id and p.user_id = ?) and cons_id = ?";

	private String sqlForGetById = "select pc.*,ud.nick_name as nickName from ls_prod_cons pc,ls_usr_detail ud where pc.ask_user_name=ud.user_name and pc.cons_id=?";

	@Override
	public List<ProductConsult> getProductConsultList(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}


	@Override
	public ProductConsult getProductConsult(Long id) {
		return this.get(sqlForGetById, ProductConsult.class, id);
	}

	@Override
	@CacheEvict(value = "ProductConsult", key = "#id")
	public void deleteProductConsult(Long id) {
		ProductConsult pc = getProductConsult(id);
		if (pc != null) {
			delete(pc);
		}
	}

	@Override
	@ProductConsultUpdate
	public Long saveProductConsult(ProductConsult productConsult) {
		return (Long) save(productConsult);
	}

	@Override
	@CacheEvict(value = "ProductConsult", key = "#productConsult.consId")
	@ProductConsultUpdate
	public void updateProductConsult(ProductConsult productConsult) {
		update(productConsult);
	}

	@Override
	@CacheEvict(value = "ProductConsult", key = "#consult.consId")
	public void deleteProductConsult(ProductConsult consult) {
		delete(consult);
	}

	@Override
	public long checkFrequency(ProductConsult consult) {
		// interval 个分钟前
		Date date = DateUtil.add(new Date(), Calendar.MINUTE, interval);
		String sql = "select count(*) from ls_prod_cons where point_type = ? and prod_id = ? and ask_user_name = ? and rec_date > ?";
		return getLongResult(sql, new Object[] { consult.getPointType(), consult.getProdId(), consult.getAskUserName(), date });
	}

	/**
	 * @param interval
	 *            the interval to set
	 */
	public void setInterval(int interval) {
		this.interval = interval;
	}

	@Override
	@CacheEvict(value = "ProductConsult", key = "#consId")
	public void deleteProductConsultById(Long consId) {
			deleteById(consId);
	}
	
	@Override
	@CacheEvict(value = "ProductConsult", key = "#consId")
	public void deleteProductConsultByShopUser(Long consId) {
		this.update("UPDATE ls_prod_cons SET del_sts=1 WHERE cons_id=?", consId);
	}

	/**
	 * load ProductConsult for update
	 */
	@Override
	public ProductConsultDto getConsult(String userId, Long consId) {
		return load(sqlForGetConsult, ProductConsultDto.class, userId,consId);
	}

	/**
	 * 回复咨询
	 */
	@Override
	@CacheEvict(value = "ProductConsult", key = "#consult.consId")
	public int updateConsult(String userId, ProductConsultDto consult) {
		return update("update ls_prod_cons c set c.answer = ?,c.answertime = ? where c.cons_id = ? and c.prod_id = (select prod_id from ls_prod p where c.prod_id = p.prod_id and p.user_id = ?)", consult.getAnswer(),new Date(), consult.getConsId(),userId);
	}

	@Override
	@CacheEvict(value = "ProductConsult", key = "#consId")
	public int deleteProductConsult(String userId, Long consId) {
		return update(sqlForDeleteProductConsult, userId,consId);
	}

	@Override
	public List<ProductConsult> queryUnReply() {
		String querySQL = ConfigCode.getInstance().getCode("prod.getUnReplyProductConsult"); 
		return queryLimit(querySQL, ProductConsult.class, 0, 3);
	}

	@Override
	public int getConsultCounts(Long shopId) {
		return get("select count(c.cons_id) from ls_prod_cons c,ls_prod p where c.prod_id = p.prod_id and p.shop_id = ? and c.answer is null",Integer.class,shopId);
	}

	@Override
	public PageSupport<ProductConsult> getProductConsultPage(String userName, ProductConsult productConsult,
			String curPageNO) {
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		map.put("pointType", productConsult.getPointType());
		// 用户已经回复
		if (productConsult.getReplySts() != null) {
			map.put("replySts", productConsult.getReplySts());
		}

		map.put("siteName", productConsult.getSiteName());
		map.like("prodName", productConsult.getProdName());
		map.put("startTime", productConsult.getStartTime());
		if(productConsult.getEndTime()!=null){
			map.put("endTime", getEndMonment((productConsult.getEndTime())));
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getProductConsultCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.getProductConsult", map);
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setCurPage(curPageNO);
		query.setParam(map.toArray());
		query.setPageSize(15);
		return querySimplePage(query);
	}
	
	public Date getEndMonment(Date date){
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	return cal.getTime();
    }

	@Override
	public PageSupport<ProductConsult> queryQarseConsult(String curPageNO, Long shopId, Integer replyed) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class, 6, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		// 用户已经回复
		if (replyed != null) {
			if (replyed == 0) {
				map.put("replyed", "and lps.answer is null");
			} else {
				map.put("replyed", "and lps.answer is not null");
			}
		} 
		 String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryProductConsultCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("shop.queryProductConsult", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 map.remove("replyed");
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductConsult> queryProdcons(String curPageNO, Long shopId, ProductConsult productConsult) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		map.put("pointType", productConsult.getPointType());
		// 用户已经回复
		if (productConsult.getReplySts() != null) {
			map.put("replySts", productConsult.getReplySts());
		}

		map.like("prodName", productConsult.getProdName());
		map.like("askUserName", productConsult.getAskUserName());
		//商家删除标记：0 保留 1 删除
		map.put("delSts", 0);
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getProductConsultCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.getProductConsult", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductConsult> queryConsult(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String querySQL = ConfigCode.getInstance().getCode("report.getProdcons", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("report.getProdconsCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public int batchDeleteConsultByIds(String consIdStr) {
		String[] consIds=consIdStr.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("DELETE FROM ls_prod_cons WHERE cons_id IN(");
		for(String consId:consIds){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(), consIds);
	}

	@Override
	public PageSupport<ProductConsult> getProductConsult(String curPageNO, ProductConsult productConsult) {
		SimpleSqlQuery query = resolveQuery(curPageNO,productConsult);
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductConsult> getProductConsultForList(String curPageNO, Integer pointType, Long prodId) {
		QueryMap map = new QueryMap();
		if (pointType != null && pointType != 0) {
			map.put("pointType", pointType);
		}
		map.put("prodId", prodId);
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getProductConsultListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.getProductConsultList", map);
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class );
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		query.setPageSize(8);
		query.setCurPage(curPageNO);
		return querySimplePage(query);
	}

	private SimpleSqlQuery resolveQuery(String curPageNO, ProductConsult productConsult) {
		QueryMap map = new QueryMap();
		map.put("pointType", productConsult.getPointType());
		// 用户已经回复
		if (productConsult.getReplySts() != null) {
			map.put("replySts", productConsult.getReplySts());
		}
		if(AppUtils.isNotBlank(productConsult.getShopId())) {
			map.put("shopId", productConsult.getShopId());
		}
		if(AppUtils.isNotBlank(productConsult.getSiteName())) {
			map.like("siteName", productConsult.getSiteName().trim());
		}
		if(AppUtils.isNotBlank(productConsult.getProdName())) {
			map.like("prodName", productConsult.getProdName().trim());
		}
		map.put("startTime", productConsult.getStartTime());
		if(productConsult.getEndTime()!=null){
			map.put("endTime", DateUtils.getDay(productConsult.getEndTime(), 1));
		}
		if(AppUtils.isNotBlank(productConsult.getNickName())) {
			map.put("nickName", productConsult.getNickName().trim());
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.getProductConsultCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.getProductConsult", map);
		SimpleSqlQuery query = new SimpleSqlQuery(ProductConsult.class);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setCurPage(curPageNO);
		query.setParam(map.toArray());
		return query;
	}

}
