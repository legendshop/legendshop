/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.integral;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CategoryDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.util.CategoryManagerUtil;

/**
 * 积分服务
 */
@Service("integralCategoryService")
public class IntegralCategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CategoryManagerUtil categoryManagerUtil;

	public Category getCategory(Long id) {
		return categoryDao.getCategory(id);
	}

	/**
	 * 删除节点
	 */
	@CacheEvict(value = "CategoryList", allEntries = true)
	public void deleteCategory(Category category) {
		categoryManagerService.delTreeNode(category);
	}

	/**
	 * 保存节点
	 */
	public Long saveCategory(Category category) {
		return categoryDao.saveCategory(category);
	}

	/**
	 * 更新节点
	 */
	@CacheEvict(value = "CategoryList", allEntries = true)
	public void updateCategory(Category category) {
		categoryDao.updateCategory(category);
	}

	/**
	 * 获取节点列表
	 */
	@Override
	public List<Category> getCategoryList(String type) {
		return categoryDao.getCategoryList(type);
	}

	/**空方法-未实现*/
	@Override
	public List<Category> getCategoryListByStatus(String type) {
		return null;
	}

	/**
	 * 根据类型获取所有节点
	 */
	@Override
	public Set<CategoryDto> getAllCategoryByType(String type) {
		return null;
	}

	/**
	 * 传入节点 得到该所有的子节点
	 * 
	 * @param parentId
	 * @param navigationMenu
	 * @param type
	 * @param status
	 * @return
	 */
	@Override
	public List<Category> getAllChildrenByFunction(Long parentId, Integer navigationMenu, String type, Integer status) {
		return categoryManagerUtil.getAllChildrenByFunction(parentId, navigationMenu, type, status);
	}

	/**
	 * 查找节点
	 */
	@Override
	public List<Category> findCategory(Long parentId) {
		return categoryDao.findCategory(parentId);
	}

	/**
	 * 查找节点
	 */
	@Override
	public List<Category> findCategory(String cateName, Integer level, Long parentId) {
		return categoryDao.findCategory(cateName, level, parentId);
	}

	/**
	 * 根据节点id获取节点
	 */
	@Override
	public List<Category> getCategoryByIds(List<Long> catIdList) {

		return categoryDao.getCategoryByIds(catIdList);
	}

	/**
	 * 更新节点
	 */
	@Override
	public void updateCategoryList(List<Category> categorys) {
		categoryDao.updateCategoryList(categorys);
	}

	/**
	 * 根据类型id获取节点
	 */
	@Override
	public List<Category> getCategorysByTypeId(Integer typeId) {
		return categoryDao.getCategorysByTypeId(typeId);
	}

	/**
	 * 查询节点列表
	 */
	@Override
	public List<Category> queryCategoryList() {
		return categoryDao.queryCategoryList();
	}

	/**
	 * 清除节点缓存
	 */
	@Override
	@Caching(evict = { @CacheEvict(value = "IntegralCategoryDtoList", allEntries = true),
			@CacheEvict(value = "AllIntegralCategoryDtoList", allEntries = true) })
	public void cleanCategoryCache() {
	}

	/**
	 * 根据名字获取节点列表
	 */
	@Override
	public List<Category> getCategoryListByName(String categoryName) {
		return categoryDao.getCategoryListByName(categoryName);
	}

	/**
	 * 查询节点列表
	 */
	@Override
	public List<Category> queryCategoryList(String type, Integer level) {
		return categoryDao.getCategory(type, null, null, Constants.ONLINE, level);
	}

	/**
	 * 获取节点页面
	 */
	@Override
	public PageSupport<Category> getCategoryPage(String curPageNO) {
		return categoryDao.getCategoryPage(curPageNO);
	}

	@Override
	public List<Category> getCategory(String sortType, Integer headerMenu, Integer navigationMenu, Integer status, Integer level) {
		return categoryDao.getCategory(sortType, headerMenu, navigationMenu, status, level);
	}

	@Override
	public List<Category> findProdTypeId(Integer prodTypeId) {
		return null;
	}

	@Override
	public List<Category> findCategoryByGradeAndParentId(Integer grade, Long parentId) {
		return categoryDao.findCategoryByGradeAndParentId(grade,parentId);
	}
}
