/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.NewsPositionDao;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.entity.NewsPosition;
import com.legendshop.spi.service.NewsPositionService;

/**
 * 文章位置中间表
 * 
 */
@Service("newsPositionService")
public class NewsPositionServiceImpl implements NewsPositionService {
	
	@Autowired
	private NewsPositionDao newsPositionDao;

	@Override
	public Long saveNewsPosition(NewsPosition p) {
		return newsPositionDao.saveNewsPosition(p);
	}

	@Override
	public NewsPosition getNewsPositionyById(Long id) {
		return newsPositionDao.getById(id);
	}

	@Override
	public List<NewsPosition> getNewsPositionList() {
		return newsPositionDao.getNewsPositionList();
	}

	@Override
	public void delete(Long id) {
		newsPositionDao.delete(id);
	}

	@Override
	public void updateNewsPosition(NewsPosition newsPosition) {
		newsPositionDao.updateNewsPosition(newsPosition);
	}

	@Override
	public List<NewsPosition> getNewsPositionByNewsId(Long newsId) {
		return newsPositionDao.getNewsPositionByNewsId(newsId);
	}

	@Override
	public List<NewsDto> getNewsList() {
		return newsPositionDao.getNewsList();
	}
}
