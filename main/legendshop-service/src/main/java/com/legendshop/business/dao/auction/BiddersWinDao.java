/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.entity.BiddersWin;

/**
 * 中标Dao接口.
 */

public interface BiddersWinDao extends Dao<BiddersWin, Long> {

	public abstract List<BiddersWin> getBiddersWin(String userId);

	public abstract BiddersWin getBiddersWin(Long id);

	public abstract int deleteBiddersWin(BiddersWin biddersWin);

	public abstract Long saveBiddersWin(BiddersWin biddersWin);

	public abstract int updateBiddersWin(BiddersWin biddersWin);

	public abstract boolean isWin(String userId, Long prodId, Long skuId, Long paimaiId);

	public abstract BiddersWin getBiddersWin(String userId, Long id);

	public abstract void updateBiddersWin(Long id, String subNember, Long subId, int value);

	public abstract List<BiddersWin> getBiddersWin(Date date, int commitInteval, Integer status);

	public abstract List<BiddersWin> getBiddersWinList(Long id);

	public abstract BiddersWin getBiddersWinByAid(Long aid);

	public abstract PageSupport<BiddersWin> getBiddersWinPage(String curPageNO, BiddersWin biddersWin);

	public abstract PageSupport<BiddersWin> queryBiddersWinListPage(String curPageNO, String userId);

	public abstract PageSupport<BiddersWin> queryUserPartAution(String curPageNO, String userId);

}
