/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;
import com.legendshop.util.AppUtils;

/**
 * 首页楼层.
 */
@Repository
public class FloorDaoImpl extends GenericDaoImpl<Floor, Long> implements FloorDao {

    @Autowired
    private SystemParameterUtil systemParameterUtil;

    public Floor getFloor(Long id) {
        return getById(id);
    }

    /**
     * 先要删除子记录
     * FloorItem, FloorSubItem, SubFloor, SubFloorItem
     */
    public void deleteFloor(Floor floor) {
        delete(floor);
    }

    public Long saveFloor(Floor floor) {
        return (Long) save(floor);
    }

    public void updateFloor(Floor floor) {
        update(floor);
    }

    /**
     * 检查楼层是否存在
     */
    @Override
    public boolean checkPrivilege(Long floorId) {
        Long size = jdbcTemplate.queryForObject("select count(1) from ls_floor where fl_id = ?", Long.class, floorId);
        if (size <=0) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public List<Floor> getActiveFloor() {
        return query("select fl_id as flId, name, css_style as cssStyle,layout,seq from ls_floor where status = 1 order by seq", Floor.class);
    }

    @Override
    public void deleteSubFloors(Long flId) {
        update("delete from ls_sub_floor where floor_id = ?", flId);
    }

    @Override
    public Integer getLeftSwitch(Long floorId) {
        return get("select left_switch from ls_floor where fl_id = ?", Integer.class, floorId);
    }

    /**
     * 清除楼层缓存，把FloorList缓存都清除掉
     */
    @Override
    @Caching(evict = {
    		@CacheEvict(value = "FloorDto", key = "#floor.flId+#floor.layout"),
            @CacheEvict(value = "FloorList", allEntries=true)})
    public void updateFloorCache(Floor floor) {
        System.out.println("------clear Floor Cache ---- " + floor.getLayout());
    }

    @Override
    public PageSupport<Floor> getFloorPage(String curPageNO, Floor floor) {
        CriteriaQuery cq = new CriteriaQuery(Floor.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        if(AppUtils.isNotBlank(floor.getName())){        	
        	cq.like("name", floor.getName().trim(), MatchMode.ANYWHERE);
        }
        cq.addOrder("asc", "seq");
        return queryPage(cq);
    }

	@Override
	public Integer batchOffline(String floorIdStr) {
		String[] floorIds=floorIdStr.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("UPDATE ls_floor SET status=0 WHERE fl_id IN(");
		
		for(String floorId:floorIds){
			buffer.append("?,");
		}
		
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		
		return update(buffer.toString(), floorIds);
	}

}
