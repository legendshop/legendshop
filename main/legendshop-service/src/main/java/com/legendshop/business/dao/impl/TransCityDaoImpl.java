/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.TransCityDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.entity.TransCity;

/**
 *每个城市的运费设置Dao
 */
@Repository
public class TransCityDaoImpl implements TransCityDao  {

	@Autowired
	private GenericJdbcDao genericJdbcDao;

	public int deleteTransCity(Long transfeeId) {
		return genericJdbcDao.update("delete from ls_transcity where transfee_id = ?", transfeeId);
	}
	
	public List<TransCity> getTransCitys(Long transfeeId){
		List<TransCity> transCityList = genericJdbcDao.query("select transfee_id as transfeeId,city_id as cityId from ls_transcity where transfee_id = ?", TransCity.class, transfeeId);
		return transCityList;
	}


	public void saveTransCity(List<TransCity> transCityList) {
		List<Object[]> batchArgs = new ArrayList<Object[]>(transCityList.size());
		for (TransCity transCity : transCityList) {
			Object[] param = new Object[2];
			param[0] = transCity.getTransfeeId();
			param[1] = transCity.getCityId();
			batchArgs.add(param);
		}
		genericJdbcDao.batchUpdate("insert into ls_transcity(transfee_id, city_id) values(?,?)", batchArgs);
		
	}

	@Override
	public void deleteTransCity(List<TransCity> transCityList) {
		for (TransCity transCity : transCityList) {
			genericJdbcDao.update("delete from ls_transcity where transfee_id = ? and city_id = ?", transCity.getTransfeeId(),transCity.getCityId());
		}
	}
     

 }
