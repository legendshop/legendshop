/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.entity.SubSettlement;

/**
 * 订单结算票据中心
 *
 */
@Repository
public class SubSettlementDaoImpl extends GenericDaoImpl<SubSettlement, Long> implements SubSettlementDao  {
	    
    /**
     * 根据商户系统内部的订单号[微信]查找结算单
     */
	@Override
	public SubSettlement getSubSettlementBySn(String subSettlementSn) {
		EntityCriterion criterion=new EntityCriterion().eq("subSettlementSn", subSettlementSn);
		SubSettlement settlements= super.getByProperties(criterion);
		return settlements;
	}
	
	public Long saveSubSettlement(SubSettlement subSettlement){
		return save(subSettlement);
	}
	
	public int updateSubSettlement(SubSettlement subSettlement){
		return update(subSettlement);
	}

	@Override
	public SubSettlement getSubSettlement(String subSettlementSn,String userId) {
		EntityCriterion criterion=new EntityCriterion().eq("subSettlementSn", subSettlementSn).eq("userId", userId);
		SubSettlement settlements= super.getByProperties(criterion);
		return settlements;
	}

	/**
	 * 
	 * 更新尚未清算的结算单
	 * 
	 */
	@Override
	public int updateSubSettlementForPay(SubSettlement subSettlement) {
		return update("update ls_sub_settlement set flow_trade_no = ?,is_clearing = ?, clearing_time= ?, pay_user_info = ? where sub_settlement_id= ? and is_clearing = 0 ", 
				subSettlement.getFlowTradeNo(),subSettlement.getIsClearing(), subSettlement.getClearingTime(), subSettlement.getPayUserInfo(), subSettlement.getSubSettlementId());		
	}


	/**
	 * 检查这份的订单流水是否含有支付成功的记录
	 */
	@Override
	public boolean checkRepeatPay(String [] subNumbers, String userId, String subSettlementType) {
		boolean result=false;
		if(SubSettlementTypeEnum.PRESELL_ORDER_PAY.value().equals(subSettlementType)){
			for(String subNumber:subNumbers){
				long number = getLongResult(" select count(1) from ls_sub_settlement LEFT JOIN ls_sub_settlement_item ON ls_sub_settlement.sub_settlement_sn=ls_sub_settlement_item.sub_settlement_sn"
						+ " WHERE is_clearing=1 AND ls_sub_settlement.type=? AND ls_sub_settlement_item.sub_number=? AND ls_sub_settlement_item.user_id=? AND ls_sub_settlement.is_presell=1 ", 
						subSettlementType,subNumber,userId);
				 long number2 = getLongResult(" select count(1) from ls_sub_settlement LEFT JOIN ls_sub_settlement_item ON ls_sub_settlement.sub_settlement_sn=ls_sub_settlement_item.sub_settlement_sn"
						+ " WHERE is_clearing=1 AND ls_sub_settlement.type=? AND ls_sub_settlement_item.sub_number=? AND ls_sub_settlement_item.user_id=? AND ls_sub_settlement.is_presell=2 ", 
						subSettlementType,subNumber,userId);
				 if((number+number2)>=2){ //说明订金和尾款已经支付完成
					 result=true;
					 break;
				 }
			}
		}else{
			for(String subNumber:subNumbers){
				result = getLongResult(" select count(1) from ls_sub_settlement LEFT JOIN ls_sub_settlement_item ON ls_sub_settlement.sub_settlement_sn=ls_sub_settlement_item.sub_settlement_sn"
						+ " WHERE is_clearing=1 AND ls_sub_settlement.type=? AND ls_sub_settlement_item.sub_number=? AND ls_sub_settlement_item.user_id=? ", 
						subSettlementType,subNumber,userId
						)>0;
				if(result) 
					break;
			}
		}
		return result;
	}



 }
