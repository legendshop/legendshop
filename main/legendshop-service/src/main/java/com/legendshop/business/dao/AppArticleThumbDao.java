/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AppArticleThumb;

/**
 * The Class AppArticleThumbDao. 种草和发现文章点赞表Dao接口
 */
public interface AppArticleThumbDao extends Dao<AppArticleThumb,Long>{

	/**
	 * 根据Id获取种草和发现文章点赞表
	 */
	public abstract AppArticleThumb getAppArticleThumb(Long id);

	/**
	 *  根据Id删除种草和发现文章点赞表
	 */
    public abstract int deleteAppArticleThumb(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteAppArticleThumb(AppArticleThumb appArticleThumb);

	/**
	 * 保存种草和发现文章点赞表
	 */
	public abstract Long saveAppArticleThumb(AppArticleThumb appArticleThumb);

	/**
	 *  更新种草和发现文章点赞表
	 */		
	public abstract int updateAppArticleThumb(AppArticleThumb appArticleThumb);

	/**
	 * 分页查询种草和发现文章点赞表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<AppArticleThumb> queryAppArticleThumb(String curPageNO, Integer pageSize);
	
	public abstract int deleteAppArticleThumbByDiscoverId(Long disId,String uid);

	public abstract Integer rsThumb(Long disId, String uid);
}
