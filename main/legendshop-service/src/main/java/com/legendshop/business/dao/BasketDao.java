/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.Basket;

/**
 * The Interface BasketDao.
 */
public interface BasketDao extends GenericDao<Basket, Long>{
	
	public abstract List<Basket> getBasketProdsByProdId(Long prodId);

	/**
	 * Delete basket by id.
	 * 
	 * @param basketId
	 *            the basket id
	 */
	public abstract void deleteBasketById(Long basketId);

	/**
	 * Gets the basket byuser name.
	 * 
	 * @param userName
	 *            the userId userId
	 * @return the basket byuser userId
	 */
	public abstract List<BasketDto> getBasketByUserId(String userId);

	// 得到有效订单总数
	/**
	 * Gets the total basket byuser name.
	 * 
	 * @param userName
	 *            the user name
	 * @return the total basket byuser name
	 */
	public abstract Long getTotalBasketByUserId(String userId);

	/**
	 * Gets the basket by id.
	 * 
	 * @param id
	 *            the id
	 * @return the basket by id
	 */
	public abstract Basket getBasketById(Long id);

	/**
	 * Save basket.
	 * 
	 * @param basket
	 *            the basket
	 * @return the long
	 */
	public abstract Long saveBasket(Basket basket);

	/**
	 * Update basket.
	 * 
	 * @param basket
	 *            the basket
	 */
	public abstract void updateBasket(Basket basket);

	/**
	 * Gets the basket.
	 * 
	 * @param prodId
	 *            the prod id
	 * @param userId
	 *            the user name
	 * @return the basket
	 */
	public abstract List<Basket> getBasket(String prodId, String userId);

	/**
	 * Delete basket by user name.
	 * 
	 * @param userName
	 *            the user name
	 */
	public abstract void deleteBasketByUserId(String userId);


	/**
	 * Save to cart.
	 * 
	 * @param userName
	 *            the user name
	 * @param prodId
	 *            the prod id
	 * @param count
	 *            the count
	 * @param attribute
	 *            the attribute
	 * @return true, if successful
	 */
	public Long saveToCart(String userId, Long prodId, Integer count, Long skuId,Long shopId,String distUserName,Long storeId);

	/**
	 * 删除商城的所有购物车
	 *
	 * @param shopId the user name
	 */
	public abstract void deleteBasket(Long shopId);
	
	
	/**
	 * 删除用户的购物车
	 *
	 * @param userName the user id
	 */
	public abstract void deleteUserBasket(String userId);

	public abstract void deleteBasketById(Long id, String userId);

	public abstract void updateShopCartNumber(Long basket_id, String userId,
			Integer num);

	public abstract int getBasketCountByUserId(String userId);

	/**
	 * 查询用户的购物车商品
	 * @param prodId
	 * @param userId
	 * @param sku_id
	 * @return
	 */
	Basket getBasketByUserId(Long prodId, String userId, Long sku_id);

	/**
	 * 根据SKUIds 查询购物车商品信息
	 * @param skuIds
	 * @return
	 */
	public abstract List<ShopCartItem> loadBasketBySkuIds(Long[] skuIds);

	/**
	 * 获取用户购物车信息
	 * @param userId
	 * @return
	 */
	public abstract List<ShopCartItem> loadBasketByIds(String userId);

	/**
	 * 更新用户的购物车数量
	 * @param productId
	 * @param skuId
	 * @param basketCount
	 * @param userId
	 */
	public abstract void updateBasket(Long productId, Long skuId,
			Integer basketCount, String userId);

	/**
	 * 删除购物车商品
	 * @param prodId
	 * @param skuId
	 * @param userId
	 */
	public abstract void deleteBasketById(Long prodId, Long skuId, String userId);

	/**
	 * 获取用户购物车信息
	 * @param basketIds
	 * @param userId
	 * @return
	 */
	public abstract List<ShopCartItem> loadBasketByIds(List<Long> basketIds,
			String userId);

	public abstract List<ShopCartItem> loadBasketByIds(List<Long> ids,
			String userId, Long shopId);

	/**
	 * 获取用户团购购物车信息
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @return
	 */
	ShopCartItem findGroupCart(Long groupId, Long productId, Long skuId);

	/**
	 * 获取用户的中标商品信息
	 * @param paimaiId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public abstract ShopCartItem findBiddersWinCart(Long paimaiId, Long prodId,
			Long skuId);

	/**
	 * 获取购物车订单信息
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	ShopCartItem findSeckillCart(Long seckillId, Long prodId, Long skuId);

	public abstract int updateIsFailureByProdId(Boolean isFailure, Long prodId);

	/**
	 * 批量删除购物车商品
	 * @param skuIds
	 * @param userId
	 * @return
	 */
	public abstract void deleteBasketByIds(String[] skuIds, String userId);

	public abstract ShopCartItem findMergeGroupCart(Long mergeGroupId, Long productId, Long skuId);

	/**
	 * 清空失效购物车商品
	 */
	public abstract void clearInvalidBaskets(String userId);

	/**
	 *  更改购物车已选门店
	 * @param productId
	 * @param skuId
	 * @param storeId
	 * @param userId
	 */
	public abstract void updateShopStore(Long productId, Long skuId, Long storeId, String userId);

	/**
	 * 批量失败用户购物车商品
	 * @param isFailure
	 * @param productIds
	 */
	void batchUpdateIsFailureByIds(Boolean isFailure, List<Long> productIds);


	/*public abstract void deleteBasketByIds(List<Long> skuIds, String userId);*/
}