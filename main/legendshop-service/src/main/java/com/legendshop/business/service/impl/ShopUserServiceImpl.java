/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopUserDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopUser;
import com.legendshop.spi.service.ShopUserService;

/**
 *商家用户服务
 */
@Service("shopUserService")
public class ShopUserServiceImpl  implements ShopUserService{

	@Autowired
    private ShopUserDao shopUserDao;

    public List<ShopUser> getShopUseByShopId(Long shopId) {
        return shopUserDao.getShopUseByShopId(shopId);
    }

    public ShopUser getShopUser(Long id) {
        return shopUserDao.getShopUser(id);
    }

    public void deleteShopUser(ShopUser shopUser) {
        shopUserDao.deleteShopUser(shopUser);
    }

    public Long saveShopUser(ShopUser shopUser) {
        return shopUserDao.saveShopUser(shopUser);
    }

    public void updateShopUser(ShopUser shopUser) {
        shopUserDao.updateShopUser(shopUser);
    }

	@Override
	public ShopUser getShopUserByNameAndShopId(String name, Long shopId) {
		return shopUserDao.getShopUserByNameAndShopId(name, shopId);
	}

	@Override
	public List<ShopUser> getShopUseRoleId(Long shopRoleId) {
		return shopUserDao.getShopUseRoleId(shopRoleId);
	}

	@Override
	public PageSupport<ShopUser> queryShopUser(String curPageNO, Long shopId) {
		return shopUserDao.queryShopUser(curPageNO,shopId);
	}
}
