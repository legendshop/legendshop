/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MobileFloorDao;
import com.legendshop.business.dao.MobileFloorItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloor;
import com.legendshop.model.entity.MobileFloorItem;
import com.legendshop.spi.service.MobileFloorService;
import com.legendshop.util.AppUtils;

/**
 * 手机端html5 首页楼层服务.
 */
@Service("mobileFloorService")
public class MobileFloorServiceImpl  implements MobileFloorService{
	
	@Autowired
    private MobileFloorDao mobileFloorDao;
    
	@Autowired
    private MobileFloorItemDao mobileFloorItemDao;

    public MobileFloor getMobileFloor(Long id) {
        return mobileFloorDao.getMobileFloor(id);
    }

    public void deleteMobileFloor(MobileFloor mobileFloor) {
        mobileFloorDao.deleteMobileFloor(mobileFloor);
    }

    public Long saveMobileFloor(MobileFloor mobileFloor) {
        if (!AppUtils.isBlank(mobileFloor.getId())) {
            updateMobileFloor(mobileFloor);
            return mobileFloor.getId();
        }
        return mobileFloorDao.saveMobileFloor(mobileFloor);
    }

    public void updateMobileFloor(MobileFloor mobileFloor) {
        mobileFloorDao.updateMobileFloor(mobileFloor);
    }

	@Override
	@Cacheable(value = "MobileFloorList", key = "#shopId")
	public List<MobileFloor> queryMobileFloor(Long shopId) {
		
		List<MobileFloor> mobileFloorList = mobileFloorDao.queryMobileFloor(shopId);
		if(AppUtils.isNotBlank(mobileFloorList)){
			List<Long> floorIds = new ArrayList<Long>();
			for (MobileFloor mobileFloor : mobileFloorList) {
				floorIds.add(mobileFloor.getId());
			}
			List<MobileFloorItem> floorItemList = mobileFloorItemDao.queryFloorItemByIds(floorIds);
			
			for (MobileFloorItem floorItem : floorItemList) {
				for (MobileFloor floor: mobileFloorList) {
					if(floor.getId().equals(floorItem.getParentId())){
						floor.addFloorItemList(floorItem);
					}
				}
			}
		}
		
		return mobileFloorList;
	}

	@Override
	public PageSupport<MobileFloor> getMobileFloorPage(String curPageNO, MobileFloor mobileFloor) {
		return mobileFloorDao.getMobileFloorPage(curPageNO,mobileFloor);
	}
}
