/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PassportSub;

import java.util.List;

/**
 * Dao接口
 */
public interface PassportSubDao extends Dao<PassportSub, Long> {

   	/**
	 *  根据Id获取
	 */
	public abstract PassportSub getPassportSub(Long id);
	
   /**
	 *  删除
	 */
    public abstract int deletePassportSub(PassportSub passportSub);
    
   /**
	 *  保存
	 */	
	public abstract Long savePassportSub(PassportSub passportSub);

   /**
	 *  更新
	 */		
	public abstract int updatePassportSub(PassportSub passportSub);
	
   /**
	 *  查询列表
	 */		
	public abstract PageSupport<PassportSub> getPassportSub(CriteriaQuery cq);

	/**
	 * 根据byPassPortId 更新 passPortId
	 * @param passPortId
	 * @param byPassPortId
	 */
	public abstract int updatePassportIdByPassportId(Long passPortId, Long byPassPortId);

	int delByPassportId(List<Long> passportIds);
}
