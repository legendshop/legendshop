/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AfterSaleDao;
import com.legendshop.business.dao.BrandDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ProductSnapshotDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.model.dto.KeyValueDto;
import com.legendshop.model.dto.ProdParamDto;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductSnapshot;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.ProductSnapshotService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品快照 service impl
 */
@Service("productSnapshotService")
public class ProductSnapshotServiceImpl  implements ProductSnapshotService{

	@Autowired
    private ProductSnapshotDao productSnapshotDao;
    
	@Autowired
    private ProductDao productDao;
    
	@Autowired
    private BrandDao brandDao;
    
	@Autowired
    private AfterSaleDao afterSaleDao;
    
	@Autowired
    private AttachmentManager attachmentManager;
    
    @Autowired
    private SubItemDao subItemDao;

    @Override
    public ProductSnapshot getProductSnapshot(Long id) {
        return productSnapshotDao.getProductSnapshot(id);
    }

    /**
     * 保存商品快照
     */
	@Override
	public void saveProdSnapshot(SubItem subItem) {
		
		Long prodId = subItem.getProdId();
		Long skuId = subItem.getSkuId();
		//查询商品详细信息
		Product product = productDao.getProduct(prodId);
		
		//根据prodId, skuid 和 version 先从快照表里找
		ProductSnapshot productSnapshot = productSnapshotDao.getProductSnapshot(subItem.getProdId(),skuId,product.getVersion());
		
		//快照表里没有找到时，保存到快照表
		if(AppUtils.isBlank(productSnapshot)){
			Long snapshotId = productSnapshotDao.createProductSnapshotId();
			productSnapshot = new ProductSnapshot();
			productSnapshot.setSnapshotId(snapshotId);
			productSnapshot.setProdId(product.getProdId());
			productSnapshot.setSkuId(skuId);
			productSnapshot.setVersion(product.getVersion());
			productSnapshot.setShopId(product.getShopId());
			productSnapshot.setName(product.getName());
			productSnapshot.setCarriage(subItem.getFreightAmount());
			productSnapshot.setBrief(product.getBrief());
			productSnapshot.setHasGuarantee(product.getHasGuarantee());
			productSnapshot.setRejectPromise(product.getRejectPromise());
			productSnapshot.setServiceGuarantee(product.getServiceGuarantee());
			productSnapshot.setProdType(product.getProdType());
			productSnapshot.setProdCount(subItem.getBasketCount());
			productSnapshot.setRecDate(new Date());
			//处理商品详情
			productSnapshot.setContent(handleContentPics(product.getContent(),snapshotId));
			//通知图片服务器 ，图片主图copy一份  TODO  改成异步 这里如果有问题会延长事务时间 还有容易报错。
			String snapShotImg =  attachmentManager.saveProdSnapshotImg(subItem.getPic(),snapshotId);
			productSnapshot.setPic(snapShotImg);
			//品牌
			productSnapshot.setBrandName(getBrandName(product.getBrandId()));
			//售后服务
			productSnapshot.setAfterSaleService(getAfterSale(product.getAfterSaleId()));
			//获取 属性 和 价格
			productSnapshot.setCash(subItem.getPrice());
			productSnapshot.setAttribute(subItem.getAttribute());
			//获取参数
			List<KeyValueDto> paramKeyValList = getParamKeyValList(product.getParameter(),product.getUserParameter());
			if(paramKeyValList.size()>0){
				productSnapshot.setParameter(JSONUtil.getJson(paramKeyValList));
			}
			//保存
			productSnapshotDao.saveProductSnapshotWithId(productSnapshot);
			productSnapshot.setSnapshotId(snapshotId);
		}
		
		//更新订单项的 快照id
		subItemDao.updateSnapshotIdById(productSnapshot.getSnapshotId(),subItem.getSubItemId());
	}
	

	/**
	 * 处理商品详情的图片, 不再支持拷贝详情的图片,因为太多了, 没有什么必要性
	 * @deprecated
	 * @param content
	 */
	private String handleContentPics(String content,Long snapshotId) {
		return null;
//		List<String> picPathList = HtmlUtil.getImgsSrc(content);//得到所有图片的路径
//		String newContent = new String(content);
//		String photoServer = ResourcePathUtil.getPhotoServer();
//		String pathPrefix = ResourcePathUtil.getPhotoPathPrefix();
//		String snapPathPrefix = ResourcePathUtil.getSnapPhotoPathPrefix();
//		for (String path : picPathList) {
//			String pic = null;
//			if(path.startsWith(pathPrefix)){
//				pic = path.replaceAll(pathPrefix, "");
//				newContent = newContent.replaceAll(path, snapPathPrefix+pic);
//			}else if(path.startsWith(photoServer +pathPrefix)){
//				pic = path.replaceAll(photoServer +pathPrefix, "");
//				newContent = newContent.replaceAll(path, photoServer+snapPathPrefix+pic);
//			}
//			if(AppUtils.isNotBlank(pic)){
//				//通知图片服务器 ，图片copy一份
//				attachmentManager.saveProdSnapshotImg(pic,snapshotId);
//			}
//		}
//		return newContent;
	}
	
	/**
	 * 把 参数 和 自定义参数 组合成 keyValueDto的列表
	 * @param param
	 * @param userParam
	 * @return
	 */
	private List<KeyValueDto> getParamKeyValList(String param, String userParam) {
		List<KeyValueDto> paramKeyValList = new ArrayList<KeyValueDto>();
		//获取参数
		if(AppUtils.isNotBlank(param)){
			List<ProdParamDto> params =  JSONUtil.getArray(param, ProdParamDto.class);
			for (ProdParamDto prodParamDto :params) {
				if(AppUtils.isNotBlank(prodParamDto.getParamValueName())){
					KeyValueDto keyValueDto = new KeyValueDto();
					keyValueDto.setKey(prodParamDto.getParamName());
					keyValueDto.setValue(prodParamDto.getParamValueName());
					paramKeyValList.add(keyValueDto);
				}
			}
		}
		//获取自定义参数
		if(AppUtils.isNotBlank(userParam)){
			List<KeyValueDto> kvs = JSONUtil.getArray(userParam, KeyValueDto.class);
			paramKeyValList.addAll(kvs);
		}
		return paramKeyValList;
	}

//	/**
//	 * 根据 sku 的属性id组合 获取 属性值的字符串
//	 * @param properties
//	 * @return
//	 */
//	private String getAttributeByProp(String properties) {
//		String attribute = "";
//		List<KeyValueEntity> keyValueEntities=splitJoinSku(properties,";");
//		for (KeyValueEntity entity:keyValueEntities) {
//			ProductProperty productProperty = productPropertyDao.getProductProperty(Long.valueOf(entity.getKey()));
//			ProductPropertyValue  productPropertyValue  = productPropertyValueDao.getProductPropertyValue(Long.valueOf(entity.getValue()));
//			if(productProperty != null && productPropertyValue != null){
//				attribute += productProperty.getMemo()+":"+productPropertyValue.getName()+";";
//			}
//		}
//		return attribute;
//	}

	/**
	 * 根据品牌id 获取品牌名称
	 * @param brandId
	 * @return
	 */
	private String getBrandName(Long brandId) {
		if(AppUtils.isNotBlank(brandId)){
			Brand brand = brandDao.getById(brandId);
			if(brand!=null){
				return brand.getBrandName();
			}
		}
		return null;
	}
	
	/**
	 * 根据id 获取售后服务说明
	 * @param afterSaleId
	 * @return
	 */
	private String getAfterSale(Long afterSaleId) {
		if(AppUtils.isNotBlank(afterSaleId)){
			AfterSale afterSale = afterSaleDao.getById(afterSaleId);
			if(afterSale!=null){
				return afterSale.getContent();
			}
		}
		return null;
	}
	
//	/**
//	 * 分解出SKU的属性和属性值ID
//	 * @param properties
//	 * @param expr
//	 * @return
//	 */
//	private List<KeyValueEntity> splitJoinSku(String properties, String expr) {
//		List<KeyValueEntity>  skuKVs=new ArrayList<KeyValueEntity>();
//		if (properties != null && properties != "") {
//			String[] proValue = properties.split(expr);
//			if (proValue != null && proValue.length > 0) {
//				int length = proValue.length;
//				for (int i = 0; i < length; i++) {
//					String[] valeus = proValue[i].split(":");
//					if (valeus != null && valeus.length > 1) {
//						KeyValueEntity kv = new KeyValueEntity();
//						kv.setKey(valeus[0]);
//						kv.setValue(valeus[1]);
//						skuKVs.add(kv);
//					}
//				}
//			}
//		}
//		return skuKVs;
//	}
}
