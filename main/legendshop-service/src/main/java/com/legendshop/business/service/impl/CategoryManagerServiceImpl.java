/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CategoryDao;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;

/**
 * 商品树处理业务类
 *
 */
@Service("categoryManagerService")
public class CategoryManagerServiceImpl implements  CategoryManagerService, InitializingBean {

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private CategoryManagerUtil categoryManagerUtil;
	
	/**
	 * 获取节点树
	 */
	@Override
	public TreeNode getTreeNodeById(long treeId) {
		//思路: 现在内存获取 , 如果没有择取数据库查询，如果数据库查询到，重新放入内存中
		TreeNode node =categoryManagerUtil.getTreeNodeById(treeId);
		return node;
	}

	public TreeNode getAllTreeNodeById(long treeId) {
		//思路: 现在内存获取 , 如果没有择取数据库查询，如果数据库查询到，重新放入内存中
		TreeNode node =categoryManagerUtil.getTreeNodeByIdForAll(treeId);
		return node;
	}

	/**
	 * 建立TreeNode节点树,会刷新时间戳,导致其他的节点会跟着时间戳变化
	 */
	@Override
	public void generateTree() {
		 //生成内存树
		 categoryManagerUtil.generateTree();
	}
	
	/**
	 * 刷新TreeNode节点树,不会刷新时间戳, 系统启动时就会调用这个方法
	 */
	@Override
	public void refreshTree() {
		 categoryManagerUtil.refreshTree();
	}	

	@Override
	public void delTreeNode(Category category) {
		int reult=categoryDao.deleteCategory(category);
		if(reult>0){
			//现操作数据库 ,操作成功后 再去操作树的内存
			if(category!=null && ProductTypeEnum.PRODUCT.value().equals(category.getType()) ){
				TreeNode node=categoryManagerUtil.getTreeNodeById(category.getId());
				if(AppUtils.isNotBlank(node)){
					categoryManagerUtil.delTreeNode(node);
					categoryManagerUtil.delTreeNodeForAll(node);
				}
			}
		}
		
	}

	@Override
	public void delChildNode(Category category, long childId) {
		
		//现操作数据库 ,操作成功后 再去操作树的内存
		if (category != null
				&& ProductTypeEnum.PRODUCT.value().equals(category.getType())) {
			TreeNode node = categoryManagerUtil.getTreeNodeById(category.getId());
			if (AppUtils.isNotBlank(node)) {
				categoryManagerUtil.delTreeNode(node,childId);
			}
		}
		
	}

	/**
	 * 添加类目
	 */
	@Override
	public Long addTreeNode(Category category) {
		//现操作数据库 ,操作成功后 再去操作树的内存
		Long cid=categoryDao.saveCategory(category);
		
		if(cid > 0  && ProductTypeEnum.PRODUCT.value().equals(category.getType()) && category.getNavigationMenu()){
			TreeNode treeNode=new TreeNode(category.getParentId()==null?0:category.getParentId(), cid, category.getName(),category.getSeq(), category.getGrade(),category);
			categoryManagerUtil.addTreeNode(treeNode);
			categoryManagerUtil.addTreeNodeForAll(treeNode);
		}
		return cid;
		
	}

	/**
	 * 更新商品分类树节点
	 * 
	 * TODO 暂时不支持 移动分类树结构
	 */
	@Override
	public void updateTreeNode(Category category) {
		
		int result=categoryDao.updateCategory(category);  //更新数据库
		
		if(result > 0 ){ //更新内存树
			
			 if(!ProductTypeEnum.PRODUCT.value().equals(category.getType()) ){
				return;
			 }
			 //现操作数据库 ,操作成功后 再去操作树的内存
			 if(category.getNavigationMenu() && Constants.ONLINE.equals(category.getStatus())){ //选择导航菜单中显示-是  Or 上线 
				
				TreeNode node = categoryManagerUtil.getTreeNodeById(category.getId());
				if(node!=null){
					categoryManagerUtil.updateTreeNode(node, category);
				}else{
					TreeNode treeNode=new TreeNode(category.getParentId()==null?0:category.getParentId(), category.getId(), category.getName(),category.getSeq(), category.getGrade(),category);
					if(AppUtils.isNotBlank(category.getParentId()) && category.getParentId()!=0){
						TreeNode parentNode=categoryManagerUtil.getTreeNodeById(category.getParentId());
						treeNode.setParentNode(parentNode);
					}
					List<Category>  categories=categoryManagerUtil.getAllChildrenByFunction(category.getId(),1,ProductTypeEnum.PRODUCT.value(),1);
					if(AppUtils.isNotBlank(categories)){
						Set<TreeNode> chrenNodes=getCategoryChildList(treeNode,categories);
						if(AppUtils.isNotBlank(chrenNodes)){
							treeNode.setChildList(new CopyOnWriteArrayList<TreeNode>(chrenNodes));
						}
					}
					categoryManagerUtil.addTreeNode(treeNode);
				}
			}else if(!category.getNavigationMenu() || Constants.OFFLINE.equals(category.getStatus())) { //选择导航菜单中显示-否  Or 下线 
				TreeNode node = categoryManagerUtil.getTreeNodeById(category.getId());
				if (AppUtils.isNotBlank(node)) {
					categoryManagerUtil.delTreeNode(node);
				}
			}
			 TreeNode node = categoryManagerUtil.getTreeNodeByIdForAll(category.getId());
			 categoryManagerUtil.updateTreeNodeForAll(node, category);
		}
	}
	
	/**
	 * 获取分类的子类
	 * @param parentNode
	 * @param categories
	 * @return
	 */
	private Set<TreeNode> getCategoryChildList(TreeNode parentNode,List<Category> categories){
		HashMap<String,TreeNode> hashMap=getTreeIntoMap(categories);
		String parentNodeId=String.valueOf(parentNode.getSelfId());
		if(hashMap!=null){
			 hashMap.put(String.valueOf(parentNode.getSelfId()), parentNode);
			 Set<TreeNode> nodes=new TreeSet<TreeNode>(new Comparator<TreeNode>() {
				public int compare(TreeNode o1, TreeNode o2) {
					if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
						return -1;
					} else if (o1.getSeq() < o2.getSeq()) {
						return -1;
					} else {
						return 1;
					}
				}
			 });
			 Iterator<TreeNode> it = hashMap.values().iterator();
			  while (it.hasNext()) {
				TreeNode node = (TreeNode) it.next();
				long id = node.getParentId();
				String parentKeyId = String.valueOf(id);
				if (hashMap.containsKey(parentKeyId)) {
					TreeNode parentTree = (TreeNode) hashMap.get(parentKeyId);
					parentTree.addChildNode(node);
					node.setParentNode(parentTree);
					if(parentKeyId.equals(parentNodeId)){
						nodes.add(node);
					}
			   }
			}
			return nodes;
		}
		return null;
    } 
	
	//组装树
	private	HashMap<String,TreeNode> getTreeIntoMap(List<Category> categories){
		if(AppUtils.isNotBlank(categories)){
			HashMap<String,TreeNode> nodeMap = new HashMap<String, TreeNode>();
			Iterator<Category> it = categories.iterator();
			while (it.hasNext()) {
				Category category = (Category) it.next();
				long id = category.getId();
				String keyId = String.valueOf(id);
				TreeNode node=build(category);
				nodeMap.put(keyId, node);
			}
			return nodeMap;
		}
		return null;
	}
	
	/**
     * 组装 TreeNode对象
     * @param category
     * @return
     */
    private TreeNode build(Category category)  {
    	return new TreeNode(category.getParentId()==null?0:category.getParentId(), category.getId(), category.getName(),category.getSeq(), category.getGrade(),category);
    }


	@Override
	public String getNodeObjectIdPath(Long fromNodeId, Long toNodeId, String separator) {
		StringBuffer buffer = new StringBuffer();
		buildNodePath(fromNodeId, toNodeId, separator, buffer);
		return buffer.toString();
	}
	
	@Override
	public String getNodeObjectIdPathOnline(Long fromNodeId, Long toNodeId, String separator) {
		StringBuffer buffer = new StringBuffer();
		buildNodePathOnline(fromNodeId, toNodeId, separator, buffer);
		return buffer.toString();
	}

	private void buildNodePath(Long fromNodeId, Long toNodeId,
			String separator, StringBuffer buffer) {
		if (toNodeId == null) {
			return;
		}
		TreeNode treeNode = this.getAllTreeNodeById(toNodeId);
		if (AppUtils.isNotBlank(treeNode)) {
			buffer.insert(0, treeNode.getSelfId());
			if (!toNodeId.equals(fromNodeId) && treeNode.getParentId()!=0 && treeNode.getParentId()!=-1) {
				if (separator != null) {
					buffer.insert(0, separator);
				}
				buildNodePath(fromNodeId, treeNode.getParentId(), separator,buffer);
			}
		}
	}
	
	private void buildNodePathOnline(Long fromNodeId, Long toNodeId,
			String separator, StringBuffer buffer) {
		if (toNodeId == null) {
			return;
		}
		TreeNode treeNode = this.getTreeNodeById(toNodeId);
		if (AppUtils.isNotBlank(treeNode)) {
			buffer.insert(0, treeNode.getSelfId());
			if (!toNodeId.equals(fromNodeId) && treeNode.getParentId()!=0 && treeNode.getParentId()!=-1) {
				if (separator != null) {
					buffer.insert(0, separator);
				}
				buildNodePath(fromNodeId, treeNode.getParentId(), separator,buffer);
			}
		}
	}

	private final static long FROM_NODEID = 0l; // 

	/**
	 * 获取类目树导航层级 ，应在 所有类目中查询，包括下线的，不在导航显示的。因为使用过的，应需要显示正常。 
	 * @param treeId
	 * @param separator
     * @return
     */
	@Override
	public List<TreeNode> getTreeNodeNavigation(Long treeId, String separator) {
		if(treeId<=0){
			return null;
		}
		String treeNodeStr = this.getNodeObjectIdPath(FROM_NODEID, treeId,separator);
		if(AppUtils.isBlank(treeNodeStr)){
			return null;
		}
		String categoryIds[] = treeNodeStr.split(",");
		int size = categoryIds.length;
		if (AppUtils.isNotBlank(categoryIds) && size > 0) {
			List<TreeNode> treeNodes = new ArrayList<TreeNode>();
			for (int i = 0; i < size; i++) {
				TreeNode node=this.getAllTreeNodeById(Long.valueOf(categoryIds[i]));
				if(AppUtils.isNotBlank(node)){
					treeNodes.add(node);
				}
			}
		  return treeNodes;
		}
		return null;
	}
	
	/**
	 * 获取类目树导航层级 ，应在 所有类目中查询，不包括下线的，不在导航显示的。因为使用过的，应需要显示正常。 
	 * @param treeId
	 * @param separator
     * @return
     */
	@Override
	public List<TreeNode> getTreeNodeOnline(Long treeId, String separator) {
		if(treeId<=0){
			return null;
		}
		String treeNodeStr = this.getNodeObjectIdPathOnline(FROM_NODEID, treeId,separator);
		if(AppUtils.isBlank(treeNodeStr)){
			return null;
		}
		String categoryIds[] = treeNodeStr.split(",");
		int size = categoryIds.length;
		if (AppUtils.isNotBlank(categoryIds) && size > 0) {
			List<TreeNode> treeNodes = new ArrayList<TreeNode>();
			for (int i = 0; i < size; i++) {
				TreeNode node=this.getTreeNodeById(Long.valueOf(categoryIds[i]));
				if(AppUtils.isNotBlank(node)){
					treeNodes.add(node);
				}
			}
		  return treeNodes;
		}
		return null;
	}
	

	/**
	 * use default separator
	 * @param treeId
	 * @return
	 */
	public List<TreeNode> getTreeNodeNavigation(Long treeId) {
		return getTreeNodeNavigation(treeId, ",");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		//刷新内存数
		refreshTree();
	}


}
