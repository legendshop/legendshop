/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.seckill;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.seckill.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.dao.ProdPropImageDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.ProdPropImage;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.StockLog;
import com.legendshop.processor.auction.scheduler.SchedulerConstant;
import com.legendshop.processor.auction.scheduler.SchedulerMamager;
import com.legendshop.processor.seckill.scheduler.SeckillCancleOrderJob;
import com.legendshop.processor.seckill.scheduler.SeckillReleaseProdJob;
import com.legendshop.processor.seckill.scheduler.SeckillStockRollbackJob;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.ImgFileService;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.StockLogService;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.DateUtil;
import com.legendshop.util.JSONUtil;
import org.springframework.transaction.annotation.Transactional;

/**
 * 秒杀活动ServiceImpl.
 */
@Slf4j
@Service("seckillActivityService")
public class SeckillActivityServiceImpl implements SeckillActivityService {

	@Autowired
    private SeckillActivityDao seckillActivityDao;

	@Autowired
    private SeckillProdDao seckillProdDao;

	@Autowired
    private ProductDao productDao;

	@Autowired
    private SkuService skuService;

	@Autowired
    private ProdPropImageDao prodPropImageDao;

	@Autowired
    private ImgFileService imgFileService;

	@Autowired
    private SeckillService seckillService;

	@Autowired
    private SeckillCachedhandle seckillCachedService;

	@Autowired
    private AttachmentManager attachmentManager;

	@Autowired
    private SkuDao skuDao;

	@Autowired
    private StockLogService stockLogService;

	@Autowired
    private SubDao subDao;

	@Autowired
    private ConstTableService constTableService;


    /**
     * 获取秒杀活动数据
     */
    public SeckillActivity getSeckillActivity(Long id) {
        return seckillActivityDao.getSeckillActivity(id);
    }

    /**
     * 删除秒杀活动数据
     */
    public void deleteSeckillActivity(SeckillActivity seckillActivity) {
        int result = seckillActivityDao.deleteSeckillActivity(seckillActivity);
        if (result > 0) {
            String images = seckillActivity.getSeckillPic();
            attachmentManager.deleteTruely(images);
        }
        String jobName = SchedulerConstant.SECKILL_SCHEDULER_ + seckillActivity.getId();
        SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);

        List<SeckillProd> seckillPro = seckillProdDao.getSeckillProdBySid(seckillActivity.getId());

        for (SeckillProd seckillProd : seckillPro) {
            Sku sku = skuDao.getSkuById(seckillProd.getSkuId());
            //重置sku营销状态
            if (SkuActiveTypeEnum.SECKILL.value().equals(sku.getSkuType())) {//如果是秒杀类型
                skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.SECKILL.value());
            }
        }
    }

    /**
     * 保存秒杀活动数据
     */
    public Long saveSeckillActivity(SeckillActivity seckillActivity) {
        if (!AppUtils.isBlank(seckillActivity.getId())) {
            updateSeckillActivity(seckillActivity);
            return seckillActivity.getId();
        }
        return seckillActivityDao.saveSeckillActivity(seckillActivity);
    }

    /**
     * 更新秒杀活动数据
     */
    public void updateSeckillActivity(SeckillActivity seckillActivity) {
        seckillActivityDao.updateSeckillActivity(seckillActivity);
        //更新秒杀活动，重置sku营销标志
        if (SeckillStatusEnum.FINISH.value().equals(seckillActivity.getStatus())) { // 已完成
            List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySId(seckillActivity.getId());
            for (SeckillProd seckillProd : seckillProds) {
                skuDao.updateSkuTypeById(seckillProd.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.SECKILL.value());
            }
        }
    }

    /**
     * 获取秒杀活动详情
     */
    @Override
    public SeckillDto getSeckillDetail(Long seckillId) {

        SeckillActivity activity = seckillActivityDao.getSeckillDetail(seckillId);
        if (AppUtils.isBlank(activity) || activity.getStatus().intValue() == 0 || activity.getStatus().intValue() == -2) {
            return null;
        }
        List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySId(seckillId);
        if (AppUtils.isBlank(seckillProds)) {
            return null;
        }
        Long prodId = seckillProds.get(0).getProdId();
        ProductDetail prod = productDao.getProdDetail(prodId);
        if (prod != null && !ProductStatusEnum.PROD_DELETE.value().equals(prod.getStatus())) {
            return null;
        }
        SeckillDto seckillDto = new SeckillDto();
        seckillDto.setSid(activity.getId());
        seckillDto.setEndTime(activity.getEndTime());
        seckillDto.setSecDesc(activity.getSeckillDesc());
        seckillDto.setSeckillTitle(activity.getSeckillTitle());
        seckillDto.setSecStatus(activity.getStatus());
        seckillDto.setShopId(activity.getShopId());
        seckillDto.setShopName(activity.getShopName());
        seckillDto.setStartTime(activity.getStartTime());
        seckillDto.setUserId(activity.getUserId());

        SeckillProdDto prodDto = new SeckillProdDto();
        // 拷贝字段
        copyProdField(prodDto, prod);

        if (seckillProds.size() > 1) { // 说明有SKU
            Long[] skuIds = new Long[seckillProds.size()];
            for (int i = 0; i < seckillProds.size(); i++) {
                skuIds[i] = seckillProds.get(0).getSkuId();
            }

            List<Sku> skuList = skuService.getSku(skuIds);
            List<SeckillSkuDto> skuDtoList = new ArrayList<SeckillSkuDto>();
            if (AppUtils.isNotBlank(skuList)) {
                for (Sku sku : skuList) {
                    SeckillSkuDto skuDto = new SeckillSkuDto();
                    skuDto.setSkuId(sku.getSkuId());
                    skuDto.setName(processName(sku.getName()));
                    skuDto.setPrice(sku.getPrice());
                    skuDto.setProperties(sku.getProperties());
                    skuDto.setStatus(sku.getStatus());
                    skuDtoList.add(skuDto);
                }
                prodDto.setSkuDtoList(skuDtoList);
                prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));

                // 获取属性图片
                List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
                Map<Long, List<String>> valueImagesMap = new HashMap<Long, List<String>>();

                // 获取到商品 所有的属性图片
                List<ProdPropImage> prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
                if (AppUtils.isNotBlank(prodPropImages)) {
                    // 根据属性值 进行分组,封装成map
                    for (ProdPropImage prodPropImage : prodPropImages) {
                        Long valueId = prodPropImage.getValueId();
                        List<String> imgs = valueImagesMap.get(valueId);
                        if (AppUtils.isBlank(imgs)) {
                            imgs = new ArrayList<String>();
                        }
                        imgs.add(prodPropImage.getUrl());
                        valueImagesMap.put(valueId, imgs);
                    }
                    // 将map转换成 DTO LIST
                    Iterator<Long> it = valueImagesMap.keySet().iterator();
                    while (it.hasNext()) {
                        Long valId = it.next();
                        List<String> imgs = valueImagesMap.get(valId);
                        PropValueImgDto propValueImgDto = new PropValueImgDto();
                        propValueImgDto.setValueId(valId);
                        propValueImgDto.setImgList(imgs);
                        propValueImgList.add(propValueImgDto);
                    }
                }
                prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));

                /*
                 * //获取商品属性和属性值 List<ProductPropertyDto> prodPropDtoList =
                 * skuService.getPropValListByProd(prodDto,skuList,
                 * valueImagesMap); prodDto.setProdPropDtoList(prodPropDtoList);
                 * prodDto.setPropValueImgList(propValueImgList);
                 */
            }
        }

        // 商品图片列表(没有属性图片时，展示商品图片)
        List<String> prodPics = new ArrayList<String>();
        if (AppUtils.isBlank(prodDto.getPropValueImgList())) {
            List<ImgFile> imgFileList = imgFileService.getAllProductPics(prodId);
            if (AppUtils.isNotBlank(imgFileList)) {
                for (ImgFile imgFile : imgFileList) {
                    prodPics.add(imgFile.getFilePath());
                }
            }
        } else {
        	for(PropValueImgDto propValueImgDto : prodDto.getPropValueImgList()){
				List<String> imgs =  propValueImgDto.getImgList();
				prodPics.addAll(imgs);
			}
        }
        prodDto.setProdPics(prodPics);
        seckillDto.setProdDto(prodDto);

        return seckillDto;
    }

    /**
     * 拷贝部分字段值
     *
     * @param prodDto
     * @param prodDetail
     */
    private void copyProdField(SeckillProdDto prodDto, ProductDetail prodDetail) {
        prodDto.setProdId(prodDetail.getProdId());
        prodDto.setName(processName(prodDetail.getName()));
        prodDto.setCash(prodDetail.getCash());
        prodDto.setPic(prodDetail.getPic());
        prodDto.setBrief(prodDetail.getBrief());
        prodDto.setModelId(prodDetail.getModelId());
        prodDto.setStatus(prodDetail.getStatus());
        prodDto.setContent(prodDetail.getContent());
        prodDto.setTransportId(prodDetail.getTransportId());
        prodDto.setSupportTransportFree(prodDetail.getSupportTransportFree());
        prodDto.setTransportType(prodDetail.getTransportType());
        prodDto.setEmsTransFee(prodDetail.getEmsTransFee());
        prodDto.setExpressTransFee(prodDetail.getExpressTransFee());
        prodDto.setMailTransFee(prodDetail.getMailTransFee());
    }

    /**
     * 处理名字
     *
     * @param name
     * @return
     */
    private String processName(String name) {
        if (name == null) {
            return null;
        }
        name = name.replace("\"", "〞");
        name = name.replace("'", "’");
        return name;
    }

    /**
     * 保存秒杀活动商品
     */
    @Override
    public boolean saveSeckillActivityAndProd(SeckillActivity seckillActivity, String userId, Long shopId, String shopName, String userNmae, Long status) {
        String skus = seckillActivity.getSkus();
        seckillActivity.setUserId(userId);
        seckillActivity.setShopName(shopName);
        seckillActivity.setShopId(shopId);
        seckillActivity.setStatus(status);
        String seckillPic = null;
        if (seckillActivity.getSeckillPicFile().getSize() > 0) {
            seckillPic = attachmentManager.upload(seckillActivity.getSeckillPicFile());
            seckillActivity.setSeckillPic(seckillPic);
        }
        Long id = seckillActivityDao.saveSeckillActivity(seckillActivity);
        List<SeckillActivityDto> skuDto = JSONUtil.getArray(skus, SeckillActivityDto.class);
        List<SeckillProd> seckillProdList = new ArrayList<SeckillProd>();
        for (Iterator<SeckillActivityDto> iterator = skuDto.iterator(); iterator.hasNext(); ) {
            SeckillActivityDto seckillSkuDto = iterator.next();
            if (!validateSeckillProd(seckillSkuDto)) {
                continue;
            }
            SeckillProd seckillProd = new SeckillProd();
            seckillProd.setSId(id);
            seckillProd.setProdId(seckillSkuDto.getProdId());
            seckillProd.setSkuId(seckillSkuDto.getSkuId());
            seckillProd.setSeckillPrice(seckillSkuDto.getPrice());
            seckillProd.setSeckillStock(seckillSkuDto.getStock());
            seckillProd.setActivityStock(seckillSkuDto.getStock());
            seckillProdList.add(seckillProd);

            //标记sku营销标记为秒杀
            skuDao.updateSkuTypeById(seckillSkuDto.getSkuId(), SkuActiveTypeEnum.SECKILL.value(), SkuActiveTypeEnum.PRODUCT.value());
        }
        seckillProdDao.saveSeckillProdList(seckillProdList);
        return true;
    }

    /**
     * 更新秒杀活动商品
     */
    @Override
    public boolean updateSeckillActivityAndProd(SeckillActivity seckillActivity, Long shopId, String userName) {
        SeckillActivity oldSeckillActivity = seckillActivityDao.getSeckillActivity(seckillActivity.getId());
		Long oldStatus = oldSeckillActivity.getStatus();

        if (oldSeckillActivity == null || !shopId.equals(oldSeckillActivity.getShopId())) {
            return false;
        }
        oldSeckillActivity.setSeckillTitle(seckillActivity.getSeckillTitle());
        oldSeckillActivity.setSeckillBrief(seckillActivity.getSeckillBrief());
        oldSeckillActivity.setSeckillLowPrice(seckillActivity.getSeckillLowPrice());
        oldSeckillActivity.setStartTime(seckillActivity.getStartTime());
        oldSeckillActivity.setEndTime(seckillActivity.getEndTime());
        oldSeckillActivity.setSeckillDesc(seckillActivity.getSeckillDesc());
        oldSeckillActivity.setStatus(seckillActivity.getStatus() == null ? oldSeckillActivity.getStatus() : seckillActivity.getStatus());
        String seckillPic = null;
        if (seckillActivity.getSeckillPicFile().getSize() > 0) {
            seckillPic = attachmentManager.upload(seckillActivity.getSeckillPicFile());
            // 删除原引用
            if (AppUtils.isNotBlank(oldSeckillActivity.getSeckillPic())) {
                attachmentManager.deleteTruely(oldSeckillActivity.getSeckillPic());
            }
            oldSeckillActivity.setSeckillPic(seckillPic);
        }
        // 更新活动信息
        Integer result = seckillActivityDao.update(oldSeckillActivity);

        // 如果更新活动信息成功，则处理该活动选择的商品
        if (result > 0) {

        	// 获取旧的sku
			List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySId(seckillActivity.getId());

			// 获取更新的sku信息
			String skus = seckillActivity.getSkus();
			List<SeckillActivityDto> skuDto = JSONUtil.getArray(skus, SeckillActivityDto.class);

			if(AppUtils.isNotBlank(seckillProds) &&  AppUtils.isNotBlank(skuDto)){

				// 循环判段是否有更新商品
				for (SeckillActivityDto seckillActivityDto : skuDto) {

					for (SeckillProd seckillProd : seckillProds) {

						// sku不同，则释放之前选择的sku
						if(AppUtils.isNotBlank(seckillActivityDto) && !seckillActivityDto.getSkuId().equals(seckillProd.getSkuId())){

							skuDao.updateSkuTypeById(seckillProd.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.SECKILL.value());
						}
						// 如果活动是已上线未开始状态下进行的编辑，则需要把之前锁定的库存恢复
						if (SeckillStatusEnum.ONLINE.value().equals(oldStatus)){
							//更新SKU销售库存
							skuDao.updateSkuStocksByRealTime(seckillProd.getSkuId(), seckillProd.getSeckillStock());
						}
					}
				}
			}

			seckillProdDao.deleteSeckillProdBySechillId(seckillActivity.getId());

            List<SeckillProd> seckillProdList = new ArrayList<SeckillProd>();
            for (Iterator<SeckillActivityDto> iterator = skuDto.iterator(); iterator.hasNext(); ) {
                SeckillActivityDto seckillSkuDto = iterator.next();
                if (!validateSeckillProd(seckillSkuDto)) {
                    continue;
                }
                SeckillProd seckillProd = new SeckillProd();
                seckillProd.setSId(seckillActivity.getId());
                seckillProd.setProdId(seckillSkuDto.getProdId());
                seckillProd.setSkuId(seckillSkuDto.getSkuId());
                seckillProd.setSeckillPrice(seckillSkuDto.getPrice());
                seckillProd.setSeckillStock(seckillSkuDto.getStock());
                seckillProd.setActivityStock(seckillSkuDto.getStock());
                seckillProdList.add(seckillProd);

                //标记sku营销标记为秒杀
                skuDao.updateSkuTypeById(seckillSkuDto.getSkuId(), SkuActiveTypeEnum.SECKILL.value(), SkuActiveTypeEnum.PRODUCT.value());
            }
            seckillProdDao.saveSeckillProdList(seckillProdList);
        }
        return true;
    }

    /**
     * 校验价格和库存
     *
     * @param seckillSkuDto
     * @return
     */
    private boolean validateSeckillProd(SeckillActivityDto seckillSkuDto) {
        if (AppUtils.isBlank(seckillSkuDto.getPrice())) {
            return false;
        }
        if (AppUtils.isBlank(seckillSkuDto.getStock())) {
            return false;
        }
        return true;
    }

    /**
     * 更新秒杀商品类型
     */
    @Override
    public void updateProdType(Long id, String value) {
        List<SeckillProd> seckillProd = seckillProdDao.getSeckillProdBySid(id);
        if (AppUtils.isNotBlank(seckillProd)) {
            SeckillProd prod = seckillProd.get(0);
            if (prod != null) {
                productDao.updateProdType(prod.getProdId(), value, id);
            }
        }
    }

    /**
     * 查找秒杀活动列表
     */
    @Override
    public PageSupport<SeckillActivity> querySeckillActivityList(String startTime) {
        return seckillActivityDao.queryAuctionList(startTime);
    }

    /**
     * 获取秒杀活动数据
     */
    @Override
    public PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, int pageSize, SeckillActivity seckillActivity) {
        return seckillActivityDao.queryAuctionList(curPageNO, pageSize, seckillActivity);
    }

    /**
     * 商家获取秒杀活动列表
     */
    @Override
    public PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, Long shopId, int pageSize, SeckillActivity seckillActivity) {
        return seckillActivityDao.getSeckillActivity(curPageNO, shopId, pageSize, seckillActivity);
    }

    /**
     * 获取秒杀活动列表
     */
    @Override
    public PageSupport<SeckillActivity> querySeckillActivityList(String curPageNO, String startTime) {
        return seckillActivityDao.queryAuctionList(curPageNO, startTime);
    }

    /**
     * 获取秒杀活动列表
     */
    @Override
    public PageSupport<SeckillActivity> queryFrontSeckillList(String curPageNO, String startTime) {
        return seckillActivityDao.queryFrontSeckillList(curPageNO, startTime);
    }

    @Override
    public boolean findIfJoinSeckill(Long productId) {
        return seckillActivityDao.findIfJoinSeckill(productId);
    }

    /*
     * 上下线
     */
    @Override
    public String updateSeckillActivity(Long id, Long status) {
        SeckillActivity seckillPo = this.getSeckillActivity(id);
        if (AppUtils.isBlank(seckillPo) || AppUtils.isBlank(status)) {
            return Constants.FAIL;
        }
        if (!status.equals(seckillPo.getStatus())) {
            seckillPo.setStatus(status);
            this.updateSeckillActivity(seckillPo);

            List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySid(seckillPo.getId());
            if (AppUtils.isBlank(seckillProds)) {
                return "操作失败,活动数据发送改变,请刷新后重试！";
            }
            SeckillProd seckillProd = seckillProds.get(0);

            if (status == 1) {
                SeckillDto seckill = seckillService.getSeckillDetail(seckillPo.getId());
                Long productId = 0l;
                if (seckill != null) {
                    productDao.updateProdType(seckillProd.getProdId(), ProductTypeEnum.PRODUCT.value(), id);
                    productId = seckillProd.getProdId();
                    for (SeckillProd prod : seckillProds) {
                        Sku sku = skuService.getSkuById(prod.getSkuId());
                        if (sku == null || ProductStatusEnum.PROD_DELETE.value().equals(sku.getStatus())) {
                            return "操作失败,上线失败,该活动的部分商品已经被删除！";
                        }

                        //判断sku库存是否满足秒杀要求
                        if (sku.getStocks() < prod.getSeckillStock()) {
                            return "操作失败,上线失败,该商品库存不足！";
                        }

                        //活动上线，更新sku库存
                        skuDao.updateSkuStocksByRealTime(sku.getSkuId(), - prod.getSeckillStock());
                        StockLog stockLog = new StockLog();
                        stockLog.setProdId(sku.getProdId());
                        stockLog.setSkuId(sku.getSkuId());
                        stockLog.setName(sku.getName());
                        stockLog.setBeforeStock(sku.getStocks());
                        stockLog.setAfterStock(sku.getStocks() - prod.getActivityStock());
                        stockLog.setBeforeActualStock(sku.getActualStocks());
                        stockLog.setAfterActualStock(sku.getActualStocks());
                        stockLog.setUpdateTime(new Date());
                        stockLog.setUpdateRemark("发布秒杀活动，更新商品'" + sku.getName() + "'，商品销售库存为:'" +(sku.getStocks()-prod.getSeckillStock()) + "'");
                        stockLogService.saveStockLog(stockLog);

                        seckillCachedService.initSeckillProdStock(seckillPo.getId(), prod.getProdId(), sku.getSkuId(), prod.getSeckillStock()); // 设置库存
                    }
                    //更新商品库存
                    productDao.updateProdStocks(seckillProd.getProdId());

                    seckillCachedService.addSeckill(seckill);

					String JobName1 = SchedulerConstant.SECKILL_SCHEDULER_ + seckill.getSid();
					String JobName2 = SchedulerConstant.CANCLE_SECKILL_SCHEDULER_ + seckill.getSid();
					String JobName3 = SchedulerConstant.SECKILL_STOCK_ROLLBACK_SCHEDULER_ + seckill.getSid();

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("seckillId", seckill.getSid());
					jsonObject.put("shopId", seckill.getShopId());
					jsonObject.put("productId", productId);

					//秒杀活动定时器,释放秒杀商品的状态.
					SchedulerMamager.getInstance().addJob(JobName1, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillReleaseProdJob.class, seckill.getEndTime(), jsonObject.toJSONString());
					//秒杀活动定时器,自动取消超时未转订单资格
					ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
					String setting = constTable.getValue();
					OrderSetMgDto orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
					Date cancleTime = DateUtil.add(seckill.getEndTime(), Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_seckill_cancel()));
					SchedulerMamager.getInstance().addJob(JobName2, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillCancleOrderJob.class, cancleTime, jsonObject.toJSONString());
					//秒杀活动定时器,剩余库存和未支付订单库存回滚
					Date rollbackTime = DateUtil.add(cancleTime, Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_order_cancel()));
					SchedulerMamager.getInstance().addJob(JobName3, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillStockRollbackJob.class, rollbackTime, jsonObject.toJSONString());
                }

            } else {

                SeckillDto seckill = seckillService.getSeckillDetail(seckillPo.getId());
                if (seckill != null) {
                    for (SeckillProd prod : seckillProds) {
                        Sku sku = skuService.getSku(prod.getSkuId());
                        if (sku == null || ProductStatusEnum.PROD_DELETE.value().equals(sku.getStatus())) {
                        	continue;
//                            return "操作失败,上线失败,该活动的部分商品已经被删除！";
                        }

						//重置sku营销状态
						if (SkuActiveTypeEnum.SECKILL.value().equals(sku.getSkuType())) {//如果是秒杀类型
							skuDao.updateSkuTypeById(sku.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.SECKILL.value());
						}

                        //活动下线，更新sku库存
                        skuDao.updateSkuStocksByRealTime(sku.getSkuId(),  prod.getSeckillStock());
                        StockLog stockLog = new StockLog();
                        stockLog.setProdId(sku.getProdId());
                        stockLog.setSkuId(sku.getSkuId());
                        stockLog.setName(sku.getName());
                        stockLog.setBeforeActualStock(sku.getActualStocks());
                        stockLog.setAfterActualStock(sku.getActualStocks());
                        stockLog.setBeforeStock(sku.getStocks());
                        stockLog.setAfterStock(sku.getStocks() + prod.getActivityStock());
                        stockLog.setUpdateTime(new Date());
                        stockLog.setUpdateRemark("终止秒杀活动，更新商品'" + sku.getName() + "'，商品销售库存为:'" + (sku.getStocks()+prod.getSeckillStock()) + "'");
                        stockLogService.saveStockLog(stockLog);
                    }

					//更新商品库存
					productDao.updateProdStocks(seckillProd.getProdId());

					seckillCachedService.delSeckill(seckillPo.getId());
					seckillPo.setStatus(SeckillStatusEnum.PAYMENT_FINISH.value());
					seckillActivityDao.updateSeckillActivity(seckillPo);
					String JobName1 = SchedulerConstant.SECKILL_SCHEDULER_ + seckill.getSid();
					String JobName2 = SchedulerConstant.CANCLE_SECKILL_SCHEDULER_ + seckill.getSid();
					String JobName3 = SchedulerConstant.SECKILL_STOCK_ROLLBACK_SCHEDULER_ + seckill.getSid();

					SchedulerMamager.getInstance().removeJob(JobName1, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
					SchedulerMamager.getInstance().removeJob(JobName2, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
					SchedulerMamager.getInstance().removeJob(JobName3, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
                }
            }
        }
        return Constants.SUCCESS;
    }

    /**
     * 商家上下线
     */
    @Override
    public String shopUpdateSeckillActivity(SeckillActivity seckillPo, Long id, Long status) {
        Date nowDate = new Date();
        if (!status.equals(seckillPo.getStatus())) {
            List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySid(seckillPo.getId());
            if (AppUtils.isBlank(seckillProds)) {
                return "操作失败,活动数据发送改变,请刷新后重试！";
            }
            if (status == 0) {
                /* 下线：未开始才能下线 */
                if (seckillPo.getStartTime().after(nowDate)) {
                    seckillPo.setStatus(status);
                    this.updateSeckillActivity(seckillPo);

                    for (SeckillProd prod : seckillProds) {
                        Sku sku = skuService.getSku(prod.getSkuId());
                        if (sku == null || ProductStatusEnum.PROD_DELETE.value().equals(sku.getStatus())) {
                        	continue;
//                            return "操作失败,上线失败,该活动的部分商品已经被删除！";
                        }
                        //活动下线，更新SKU销售库存
                        skuDao.updateSkuStocksByRealTime(sku.getSkuId(),  prod.getSeckillStock());
                        StockLog stockLog = new StockLog();
                        stockLog.setProdId(prod.getProdId());
                        stockLog.setSkuId(prod.getSkuId());
                        stockLog.setName(sku.getName());
                        stockLog.setBeforeActualStock(sku.getActualStocks());
                        stockLog.setAfterActualStock(sku.getActualStocks());
                        stockLog.setBeforeStock(sku.getStocks());
                        stockLog.setAfterStock(sku.getStocks() + prod.getActivityStock());
                        stockLog.setUpdateTime(new Date());
                        stockLog.setUpdateRemark("下线秒杀活动，更新商品'" + sku.getName() + "'，商品销售库存为:'" + (sku.getStocks()-prod.getSeckillStock()) + "'");
                        stockLogService.saveStockLog(stockLog);
                    }
                    //更新商品销售库存
                    productDao.updateProdStocks(seckillProds.get(0).getProdId());

                    seckillCachedService.delSeckill(id);

                    String jobName = SchedulerConstant.SECKILL_SCHEDULER_ + seckillPo.getId();
                    String jobName2 = SchedulerConstant.CANCLE_SECKILL_SCHEDULER_ + seckillPo.getId();
                    String jobName3 = SchedulerConstant.SECKILL_STOCK_ROLLBACK_SCHEDULER_ + seckillPo.getId();

                    SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
                    SchedulerMamager.getInstance().removeJob(jobName2, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
                    SchedulerMamager.getInstance().removeJob(jobName3, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
                }
            } else if (status == 1) {
                /* 上线：未过期才能上线(发布) */
                if (seckillPo.getEndTime().after(nowDate)) {
                    SeckillProd seckillProd = seckillProds.get(0);
                    Long productId = seckillProd.getProdId();
                    for (SeckillProd prod : seckillProds) {
                        Sku sku = skuService.getSku(prod.getSkuId());
                        if (sku == null || ProductStatusEnum.PROD_DELETE.value().equals(sku.getStatus())) {
                            return "操作失败,上线失败,该活动的部分商品已经被删除！";
                        }
                        //判断sku库存是否满足秒杀要求
                        if (sku.getStocks() < prod.getSeckillStock().intValue()) {
                            return "操作失败,上线失败,该商品库存不足！";
                        }

                        //活动上线，更新SKU销售库存
                        skuDao.updateSkuStocksByRealTime(sku.getSkuId(), - prod.getSeckillStock());
                        StockLog stockLog = new StockLog();
                        stockLog.setProdId(sku.getProdId());
                        stockLog.setSkuId(sku.getSkuId());
                        stockLog.setName(sku.getName());
						stockLog.setBeforeActualStock(sku.getActualStocks());
						stockLog.setAfterActualStock(sku.getActualStocks());
                        stockLog.setBeforeStock(sku.getStocks());
                        stockLog.setAfterStock(sku.getStocks() - prod.getActivityStock());
                        stockLog.setUpdateTime(new Date());
                        stockLog.setUpdateRemark("发布秒杀活动，更新商品'" + sku.getName() + "'，商品销售库存为:'" + (sku.getStocks()-prod.getSeckillStock()) + "'");
                        stockLogService.saveStockLog(stockLog);

						// 设置库存
                        seckillCachedService.initSeckillProdStock(seckillPo.getId(), prod.getProdId(), sku.getSkuId(), prod.getSeckillStock());
                    }
                    //更新商品销售库存
                    productDao.updateProdStocks(seckillProd.getProdId());

                    //更新秒杀活动状态
                    seckillPo.setStatus(status);
                    this.updateSeckillActivity(seckillPo);

                    // 初始化redis活动缓存
                    seckillService.getSeckillDetail(seckillPo.getId());

                    String jobName = SchedulerConstant.SECKILL_SCHEDULER_ + seckillPo.getId();
                    String jobName2 = SchedulerConstant.CANCLE_SECKILL_SCHEDULER_ + seckillPo.getId();
                    String jobName3 = SchedulerConstant.SECKILL_STOCK_ROLLBACK_SCHEDULER_ + seckillPo.getId();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("seckillId", seckillPo.getId());
                    jsonObject.put("shopId", seckillPo.getShopId());
                    jsonObject.put("productId", productId);
                    //秒杀活动定时器,释放秒杀商品的状态.
                    SchedulerMamager.getInstance().addJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillReleaseProdJob.class, seckillPo.getEndTime(), jsonObject.toJSONString());
                    //秒杀活动定时器,自动取消超时未转订单资格
                    ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
                    String setting = constTable.getValue();
                    OrderSetMgDto orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
                    Date cancleTime = DateUtil.add(seckillPo.getEndTime(), Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_seckill_cancel()));
                    SchedulerMamager.getInstance().addJob(jobName2, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillCancleOrderJob.class, cancleTime, jsonObject.toJSONString());
                    //秒杀活动定时器,剩余库存和未支付订单库存回滚
                    Date rollbackTime = DateUtil.add(cancleTime, Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_order_cancel()));
                    SchedulerMamager.getInstance().addJob(jobName3, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME, SeckillStockRollbackJob.class, rollbackTime, jsonObject.toJSONString());

                }
            }
        }
        return null;
    }

    /**
     * 获取过期未完成的秒杀活动
     */
    @Override
    public List<SeckillActivity> getSeckillActivityByTime(Date now) {
        return seckillActivityDao.getSeckillActivityByTime(now);
    }

    /**
     * 根据秒杀id，回滚剩余库存和下单后未付款订单库存
     */
    @Override
    public boolean stockRollback(Long seckillId) {
		//更新秒杀活动状态为“秒杀活动支付结束”
		SeckillActivity seckillActivity = seckillActivityDao.getSeckillActivity(seckillId);
		if(SeckillStatusEnum.PAYMENT_FINISH.value().equals(seckillActivity.getStatus())){
			return false;
		}
        List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySid(seckillId);
        if (AppUtils.isNotBlank(seckillProds)) {
            for (SeckillProd seckillProd : seckillProds) {

            	//循环获取每个活动的已支付订单数量
                Integer count = subDao.getSeckillSubBuyCount(seckillId, seckillProd.getSkuId());
                
                //计算秒杀活动剩余库存
                double restStock = 0D;
                if (seckillProd.getActivityStock()!=null){
                    restStock = Arith.sub(seckillProd.getActivityStock(), count);
                }
                if (restStock != 0D) {
                    Sku sku = skuService.getSku(seckillProd.getSkuId());
                    if (sku == null || ProductStatusEnum.PROD_DELETE.value().equals(sku.getStatus())) {
                    	log.warn("Sku {} 状态异常", seckillProd.getSkuId());
                        continue;
                    }
                    //更新SKU销售库存
                    skuDao.updateSkuStocksByRealTime(sku.getSkuId(), (long) restStock);

                    StockLog stockLog = new StockLog();
                    stockLog.setProdId(sku.getProdId());
                    stockLog.setSkuId(sku.getSkuId());
                    stockLog.setName(sku.getName());
					if (AppUtils.isBlank(sku.getActualStocks())) {
						log.warn("============================ sku实际库存为空， skuId: {} =============================", sku.getSkuId());
					}
					stockLog.setBeforeActualStock(sku.getActualStocks());
					stockLog.setAfterActualStock(sku.getActualStocks());
                    stockLog.setBeforeStock(sku.getStocks());
                    stockLog.setAfterStock((long) (sku.getStocks() + restStock));
                    stockLog.setUpdateTime(new Date());
                    stockLog.setUpdateRemark("关闭秒杀活动，更新商品'" + sku.getName() + "'，商品销售库存为:'" + sku.getActualStocks() + "'");
                    stockLogService.saveStockLog(stockLog);
                }
            }
            //更新商品销售库存
            productDao.updateProdStocks(seckillProds.get(0).getProdId());
        }

        seckillActivity.setStatus(SeckillStatusEnum.PAYMENT_FINISH.value());
        seckillActivityDao.updateSeckillActivity(seckillActivity);
        return true;
    }

    @Override
    public List<SeckillActivity> queryUnfinishSeckillActivity() {
        Date now = new Date();
        EntityCriterion entityCriterion = new EntityCriterion();
        
        //以下状态的秒杀活动过期后都释放商品
        List<Long> statusList = new ArrayList<Long>();
        statusList.add(SeckillStatusEnum.FAILED.value());
        statusList.add(SeckillStatusEnum.VALIDATING.value());
        statusList.add(SeckillStatusEnum.OFFLINE.value());
        statusList.add(SeckillStatusEnum.ONLINE.value());
//        statusList.add(SeckillStatusEnum.TRANSORDER.value());
        
        entityCriterion.in("status", statusList.toArray());
        entityCriterion.lt("endTime", now);
        
        return seckillActivityDao.queryByProperties(entityCriterion);
    }

    @Override
    public List<SeckillActivity> querySeckillActivityByStatus(Long status) {
        return seckillActivityDao.queryByProperties(new EntityCriterion().eq("status", status));
    }

	@Override
	public SeckillActivity getSeckillActivityByShopIdAndActiveId(Long shopId, Integer acitveId) {

		return seckillActivityDao.getSeckillActivityByShopIdAndActiveId(shopId, acitveId);
	}

	/**
	 * 根据id获取秒杀成功记录
	 */
	@Override
	public List<SeckillSuccess> getSeckillSucessBySeckillId(Long seckillId) {
		
		return seckillActivityDao.getSeckillSucessBySeckillId(seckillId);
	}

}
