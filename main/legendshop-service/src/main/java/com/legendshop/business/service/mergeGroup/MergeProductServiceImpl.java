/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.mergeGroup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MergeProductDao;
import com.legendshop.model.entity.MergeProduct;
import com.legendshop.spi.service.MergeProductService;
import com.legendshop.util.AppUtils;

/**
 * The Class MergeProductServiceImpl.
 *  拼团活动参加商品服务实现类
 */
@Service("mergeProductService")
public class MergeProductServiceImpl  implements MergeProductService{
   
   /**
     *
     * 引用的拼团活动参加商品Dao接口
     */
	@Autowired
    private MergeProductDao mergeProductDao;

   	/**
	 *  根据Id获取拼团活动参加商品
	 */
    public MergeProduct getMergeProduct(Long id) {
        return mergeProductDao.getMergeProduct(id);
    }

   /**
	 *  删除拼团活动参加商品
	 */ 
    public void deleteMergeProduct(MergeProduct mergeProduct) {
        mergeProductDao.deleteMergeProduct(mergeProduct);
    }

   /**
	 *  保存拼团活动参加商品
	 */	    
    public Long saveMergeProduct(MergeProduct mergeProduct) {
        if (!AppUtils.isBlank(mergeProduct.getId())) {
            updateMergeProduct(mergeProduct);
            return mergeProduct.getId();
        }
        return mergeProductDao.saveMergeProduct(mergeProduct);
    }

   /**
	 *  更新拼团活动参加商品
	 */	
    public void updateMergeProduct(MergeProduct mergeProduct) {
        mergeProductDao.updateMergeProduct(mergeProduct);
    }
}
