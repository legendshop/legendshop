/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;

/**
 * 保存订单历史.
 *
 */
@Repository
public class SubHistoryDaoImpl extends GenericDaoImpl<SubHistory, Long> implements SubHistoryDao {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SubHistoryDaoImpl.class);


	@Override
	public void saveSubHistory(Sub sub, String subAction) {
		if (log.isDebugEnabled()) {
			log.debug("Saving sub history, id is {}, status is{} ", sub.getId(), sub.getStatus());
		}

		SubHistory history = new SubHistory();
		history.setRecDate(new Date());
		history.setStatus(String.valueOf(sub.getStatus()));
		history.setSubId(sub.getId());
		save(history);
	}

	@Override
	public void saveSubHistory(SubHistory subHistory) {
		save(subHistory);
	}

	@Override
	public List<SubHistory> findSubHistoryBySubId(Long subId) {
		return queryByProperties(new EntityCriterion().eq("subId", subId).addAscOrder("recDate"));
	}

	@Override
	public void saveSubHistoryList(List<SubHistory> his) {
		save(his);
	}
	

}
