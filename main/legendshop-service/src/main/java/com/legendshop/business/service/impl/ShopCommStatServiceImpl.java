/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopCommStatDao;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;
import com.legendshop.spi.service.ShopCommStatService;
import com.legendshop.util.AppUtils;

/**
 *店铺评分统计服务
 */
@Service("shopCommStatService")
public class ShopCommStatServiceImpl  implements ShopCommStatService{

	@Autowired
    private ShopCommStatDao shopCommStatDao;

    public ShopCommStat getShopCommStat(Long id) {
        return shopCommStatDao.getShopCommStat(id);
    }
    
    @Override
	public ShopCommStat getShopCommStatByShopId(Long shopId) {
		return shopCommStatDao.getShopCommStatByShopId(shopId);
	}

    public void deleteShopCommStat(ShopCommStat shopCommStat) {
        shopCommStatDao.deleteShopCommStat(shopCommStat);
    }

    public Long saveShopCommStat(ShopCommStat shopCommStat) {
        if (!AppUtils.isBlank(shopCommStat.getId())) {
            updateShopCommStat(shopCommStat);
            return shopCommStat.getId();
        }
        return shopCommStatDao.saveShopCommStat(shopCommStat);
    }

    public void updateShopCommStat(ShopCommStat shopCommStat) {
        shopCommStatDao.updateShopCommStat(shopCommStat);
    }
}
