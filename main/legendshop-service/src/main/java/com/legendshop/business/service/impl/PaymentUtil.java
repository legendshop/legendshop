package com.legendshop.business.service.impl;

import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PaymentProcessorEnum;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.dto.payment.InitPayFromDto;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.spi.manager.PaymentResolverManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 支付工具
 */
@Service("paymentUtil")
public class PaymentUtil {
	
	@Autowired
	private PaymentResolverManager paymentResolverManager;
	
	@Autowired
	private SubSettlementDao subSettlementDao;
	
	@Autowired
	private SubSettlementItemDao subSettlementItemDao;
	
	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private AuctionDepositRecDao auctionDepositRecDao;

	
	
	public Map<String, Object>  initPay(InitPayFromDto initPayFrom) {
		
		PaymentLog.info("#############PaymentUtil initPay##########");
		
		Map<String, Object> resultMap=new HashMap<String, Object>();
		resultMap.put("result", "false");
		
		SubSettlementTypeEnum typeEnum = SubSettlementTypeEnum.fromCode(initPayFrom.getSubSettlementType());
		if (AppUtils.isBlank(typeEnum)) {
			resultMap.put("message", "单据类型错误,非法访问");
			return resultMap;
		}
		Map<String, String> params = new HashMap<String, String>();
		params.put("subNumber", initPayFrom.getSubNumberStr()); //多个订单 以,隔开
		params.put("userId", initPayFrom.getUserId());
		params.put("payPctType", initPayFrom.getPayPctType() + "");
		
		/**
		 * 查询支付单据信息
		 */
		Map<String, Object> result = paymentResolverManager.queryPayto(typeEnum, params);
		if (!Boolean.parseBoolean(result.get("result").toString())) {
			resultMap.put("message", result.get("message").toString());
			return resultMap;
		}
		
		Double totalAmount = Double.valueOf(result.get("totalAmount").toString()); //订单总金额

		String subject = String.valueOf(result.get("subject").toString()); // 支付单据信息
		if(subject.length()>21){
			subject = subject.substring(0,20);
		}
		
		Double predeposit=0d; //余额变量(金币或者预存款)
		Double accountAmount=0d; //采用账户余额支付的金额大小
		if(initPayFrom.isWalletPay()){ //是否钱包支付
			UserDetail detail=userDetailDao.getUserDetailByIdNoCache(initPayFrom.getUserId());
			if(initPayFrom.getPrePayTypeVal()==1){
				PaymentLog.info("平台支付方式：预存款支付");
				predeposit=detail.getAvailablePredeposit();//预付款
			}else{
				PaymentLog.info("平台支付方式：金币支付");
				predeposit=detail.getUserCoin(); //金币
			}
			if(predeposit >= totalAmount){ //如果预存款和金币足够支付，使用预存款或者金币支付
				initPayFrom.setPayTypeId(PaymentProcessorEnum.FULL_PAY.name()); //使用全额支付
				accountAmount = totalAmount; //全部采用余额支付
				initPayFrom.setFullPay(true); //是全额支付
			}else{  //如果预存款和金币不够支付
				PaymentLog.info("平台余额支付不足！总需要支付金额：{} ,平台余额：{}",totalAmount,predeposit);
				accountAmount=predeposit; //账户余额不足, 全部花完账户余额的钱先
				initPayFrom.setFullPay(false);
			}
		}
		
		PaymentProcessorEnum processorEnum = PaymentProcessorEnum.fromCode(initPayFrom.getPayTypeId());
		if (processorEnum == null) {
			resultMap.put("result", false);
			resultMap.put("message", "支付参数有误");
			return resultMap;
		}
		
		//构造showUrl
		String showUrl = returnShowURL(typeEnum, initPayFrom.getDomainURL(), initPayFrom.getWapDomainURL(), initPayFrom.getSource(),initPayFrom.getSubNumberStr());
		
		//构造quitUrl
		String quitUrl = returnQuitURL(typeEnum, initPayFrom.getDomainURL(), initPayFrom.getWapDomainURL(), initPayFrom.getSource());


		Date orderTime = new Date();// 订单时间
		SysPaymentForm paymentForm = new SysPaymentForm();
		paymentForm.setUserId(initPayFrom.getUserId());
		
		paymentForm.setAccountAmount(accountAmount); //订单采用预存款余额或者金币支付的大小
		paymentForm.setTotalAmount(Arith.sub(totalAmount, accountAmount)); //采用第三方支付的金额大小
		
		paymentForm.setSubNumbers(initPayFrom.getSubNumbers());
		paymentForm.setTypeEnum(typeEnum.value());
		paymentForm.setSubject(subject);
		paymentForm.setIp(initPayFrom.getIp());
		paymentForm.setOrderDatetime(orderTime);
		paymentForm.setDomainName(initPayFrom.getDomainURL()); //商城的域名
		paymentForm.setWapDomain(initPayFrom.getWapDomainURL());//手机端商城域名

		String redirectUrl = returnRedirectUrl(typeEnum, initPayFrom.getWapDomainURL(), initPayFrom.getSubNumberStr(), paymentForm.getTotalAmount());

		paymentForm.setRedirectUrl(redirectUrl);//仅微信H5支付使用
		paymentForm.setSource(initPayFrom.getSource());
		paymentForm.setOpenId(initPayFrom.getOpendId());
		paymentForm.setProcessorEnum(processorEnum);
		
		paymentForm.setPayTypeId(initPayFrom.getPayTypeId());
		paymentForm.setFullPay(initPayFrom.isFullPay());
		paymentForm.setPrePayTypeVal(initPayFrom.getPrePayTypeVal());
		paymentForm.setSubSettlementSn(initPayFrom.getSubSettlementSn());
		paymentForm.setShowUrl(showUrl);
		paymentForm.setQuitUrl(quitUrl);
		paymentForm.setPayPctType(initPayFrom.getPayPctType());
		paymentForm.setPresell(initPayFrom.isPresell());
		
		resultMap.put("result",true);
		resultMap.put("message", paymentForm);
		return resultMap;
	}
	
	private String returnRedirectUrl(SubSettlementTypeEnum typeEnum, String wapDomainURL, String subNumbers, Double totalAmount){
		String redirectUrl = "";
		switch(typeEnum){
		  case USER_ORDER_PAY:
			  redirectUrl = wapDomainURL +  "/payOrder?subNumber=" + subNumbers + "&totalAmount=" + totalAmount + "&isReturn=true";
			  break;
		  case USER_PREPAID_RECHARGE:
			  redirectUrl = wapDomainURL + "/preDeposit/recharge?totalAmount=" + totalAmount + "&isReturn=true";
			  break;  
		  case PRESELL_ORDER_PAY:
			  break;
		  case AUCTION_DEPOSIT:
		  	  break;
		default:
			break;
		}
		return redirectUrl;
	}
	
	private String returnShowURL(SubSettlementTypeEnum typeEnum,String domainURL, String wapDomainURL, String source,String subNumber){
		String showUrl = "";
		switch(typeEnum){
		  case USER_ORDER_PAY:
			  if(PaySourceEnum.isPC(source)){
				  showUrl = domainURL+"/p/myorder?uc=uc";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	 showUrl = wapDomainURL + "/orderModules/orderList/orderList";
			  }
              if(PaySourceEnum.isAPP(source)){
            	  showUrl = domainURL+"/p/myOrder"; 
              }
			  break;
		  case USER_PREPAID_RECHARGE:
			  if(PaySourceEnum.isPC(source)){
				  showUrl=domainURL+"/p/predeposit/account_balance";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	 showUrl = wapDomainURL + "/walletModules/preDeposit/preDeposit";
			  }
              if(PaySourceEnum.isAPP(source)){
            	  showUrl=domainURL+"/p/app/predeposit"; 
              }
			  break;  
		  case PRESELL_ORDER_PAY:
			  if(PaySourceEnum.isPC(source)){
				  showUrl=domainURL + "/p/presellOrder";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	 showUrl = wapDomainURL + "/orderModules/orderList/orderList";
			  }
              if(PaySourceEnum.isAPP(source)){
            	  showUrl=domainURL + "/p/app/presellOrder"; 
              }
			  break;
		case AUCTION_DEPOSIT:

			PaymentLog.info("#############PaymentUtil 拍卖定金支付回调链接########## 订单号={}",subNumber);

			AuctionDepositRec auctionDepositRec = auctionDepositRecDao.getAuctionDepositRecBySubNumber(subNumber);
			if (AppUtils.isNotBlank(auctionDepositRec)){
				showUrl = domainURL + "/auction/views/" + auctionDepositRec.getAId();
			}
			PaymentLog.info("showUrl={}",showUrl);

			break;
		default:
			break;
		}
		return showUrl;
	}
	
	private String returnQuitURL(SubSettlementTypeEnum typeEnum,String domainURL, String wapDomain, String source){
		String quitUrl = "";
		switch(typeEnum){
		  case USER_ORDER_PAY:
			  if(PaySourceEnum.isPC(source)){
				  quitUrl=domainURL+"/p/myorder?uc=uc";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	  quitUrl= wapDomain + "/orderModules/orderList/orderList";
			  }
			  break;
		  case USER_PREPAID_RECHARGE:
			  if(PaySourceEnum.isPC(source)){
				  quitUrl = domainURL + "/p/predeposit/account_balance";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	  quitUrl = wapDomain + "/walletModules/preDeposit/preDeposit";
			  }
			  break;  
		  case PRESELL_ORDER_PAY : 
			  if(PaySourceEnum.isPC(source)){
				  quitUrl = domainURL+"/p/presellOrder";
			  }
              if(PaySourceEnum.isWAP(source) || PaySourceEnum.isWEIXIN(source)){
            	  quitUrl = wapDomain+"/orderModules/orderList/orderList";
			  }
			  break;
		case AUCTION_DEPOSIT:
			break;
		default:
			break;
		}
		return quitUrl;
	}
	
	/**
	 * 保存支付单据
	 * @param paymentForm
	 */
	public void saveSubSettlement(SysPaymentForm paymentForm) {
		
		SubSettlement settlement = new SubSettlement();
		settlement.setCashAmount(paymentForm.getTotalAmount());
		settlement.setUserId(paymentForm.getUserId());
		settlement.setIsClearing(false);
		settlement.setCreateTime(paymentForm.getOrderDatetime());
		settlement.setSubSettlementSn(paymentForm.getSubSettlementSn());
		settlement.setPdAmount(paymentForm.getAccountAmount());
		settlement.setPayTypeId(paymentForm.getPayTypeId());
		settlement.setPayTypeName(paymentForm.getPayTypeName());
		settlement.setType(paymentForm.getTypeEnum());
		settlement.setSettlementFrom(paymentForm.getSource());
		settlement.setFullPay(paymentForm.isFullPay());
		settlement.setPresell(paymentForm.isPresell());
		settlement.setPayPctType(paymentForm.getPayPctType());
		settlement.setPrePayType(paymentForm.getPrePayTypeVal());
		//获取订单创建订单收据（可能有多个订单号）
		String [] subNumbers = paymentForm.getSubNumbers();
		//订单号处理【通过对数组用逗号拆分】
		String [] newSubNumbers = new String[0];
		for(String subNumber: subNumbers){
			String[] strings = subNumber.split(",");
			//数组扩容
			int newLength = newSubNumbers.length;
			newSubNumbers = Arrays.copyOf(newSubNumbers, newLength+strings.length);
			System.arraycopy(strings, 0, newSubNumbers, newLength, strings.length);
		}
		List<SubSettlementItem> items=new ArrayList<>(newSubNumbers.length);
		for(String subNumber: newSubNumbers){
			SubSettlementItem settlementItem=new SubSettlementItem();
			settlementItem.setSubNumber(subNumber);
			settlementItem.setSubSettlementSn(paymentForm.getSubSettlementSn());
			settlementItem.setUserId(paymentForm.getUserId());
			items.add(settlementItem);
		}
		subSettlementItemDao.saveSubSettlementItem(items);
		subSettlementDao.saveSubSettlement(settlement);
	}
	
	
	

	/**
	 * 检查支付单是否支付成功
	 * @param subNumber
	 * @param userId
	 * @param subSettlementType
	 * @return
	 */
	public boolean checkRepeatPay(String  [] subNumber, String userId, String subSettlementType) {
		return subSettlementDao.checkRepeatPay(subNumber,userId,subSettlementType);
	}
}
