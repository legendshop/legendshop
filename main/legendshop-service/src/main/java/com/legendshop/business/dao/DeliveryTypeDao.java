/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.entity.DeliveryType;

/**
 * 物流配送方式Dao
 */

public interface DeliveryTypeDao extends GenericDao<DeliveryType, Long>{

	/**
	 * Gets the delivery type.
	 *
	 * @param shopName the shop name
	 * @return the delivery type
	 */
	public abstract List<DeliveryType>  getDeliveryTypeByShopId(Long shopId);

	/**
	 * Gets the delivery type.
	 *
	 * @param id the id
	 * @return the delivery type
	 */
	public abstract DeliveryType getDeliveryType(Long id);

	/**
	 * Delete delivery type.
	 *
	 * @param deliveryType the delivery type
	 */
	public abstract void deleteDeliveryType(DeliveryType deliveryType);
	
	/**
	 * Delete delivery type.
	 *
	 * @param shopId
	 */
	public abstract void deleteDeliveryTypeByShopId(Long shopId);

	/**
	 * Save delivery type.
	 *
	 * @param deliveryType the delivery type
	 * @return the long
	 */
	public abstract Long saveDeliveryType(DeliveryType deliveryType);

	/**
	 * Update delivery type.
	 *
	 * @param deliveryType the delivery type
	 */
	public abstract void updateDeliveryType(DeliveryType deliveryType);

	List<DeliveryType> getDeliveryTypeByShopId();

	public abstract DeliveryType getDeliveryType(Long shopId, Long id);

	public abstract long checkMaxNumber(Long shopId);

	public abstract PageSupport<DeliveryType> getDeliveryTypePage(String curPageNO, DeliveryType deliveryType);

	public abstract PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId);
	
	public abstract PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId, String curPageNO);

	public abstract PageSupport<DeliveryTypeDto> getAppDeliveryTypePage(String curPageNO, Long shopId);
	
	public abstract Integer getDeliveryTypeByprintTempId(Long printTempId);

}
