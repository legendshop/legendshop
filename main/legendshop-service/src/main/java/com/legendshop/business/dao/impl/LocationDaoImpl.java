/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AreaDao;
import com.legendshop.business.dao.CityDao;
import com.legendshop.business.dao.LocationDao;
import com.legendshop.business.dao.ProvinceDao;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.Province;
import com.legendshop.util.AppUtils;

/**
 * 省份DaoImpl.
 */
@Repository
public class LocationDaoImpl implements LocationDao {

	@Autowired
	private ProvinceDao provinceDao;

	@Autowired
	private CityDao cityDao;

	@Autowired
	private AreaDao areaDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.dao.LocationDao#getProvincesList()
	 */
	public List<KeyValueEntity> getProvincesList() {
		List<Province>  provinces=getAllProvince();
		if(AppUtils.isNotBlank(provinces)){
			List<KeyValueEntity> result = new ArrayList<KeyValueEntity>();
			for (Province province : provinces) {
				KeyValueEntity entity = new KeyValueEntity(province.getId(),province.getProvince());
				result.add(entity);
			}
			return result;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.LocationDao#getCitiesList(java.lang.String)
	 */
	public List<KeyValueEntity> getCitiesList(Integer provinceid) {
		return cityDao.getCitiesList(provinceid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.LocationDao#getAreaList(java.lang.String)
	 */
	public List<KeyValueEntity> getAreaList(Integer cityid) {
		return areaDao.getAreaList(cityid);
	}

	/**
	 * 装载所有的省份
	 * 
	 * @return the all province
	 */
	@Cacheable(value = "ProvinceList")
	public List<Province> getAllProvince() {
		return provinceDao.queryAll();
	}
	
	@Cacheable(value = "CityList")
	public List<City> getAllCity(){
		return cityDao.queryAll();
	}

	/**
	 * 根据省份装载城市
	 * 
	 * @return the city
	 */
	@Cacheable(value = "CityList", key = "'Pro_' + #provinceid")
	public List<City> getCity(Integer provinceid) {
		return cityDao.getCityByProvinceid(provinceid);
	}

	/**
	 * 根据城市Id装载地区
	 * 
	 * @param cityid
	 * @return the Area
	 */
	@Cacheable(value = "AreaList", key = "'City_' + #cityid")
	public List<Area> getArea(Integer cityid) {
		return areaDao.getAreaByCityid(cityid);
	}

	/**
	 * 根据省份Id查找省份
	 * 
	 * @param provinceid
	 * @return the province
	 */
	@Cacheable(value = "ProvinceList", key="#id")
	public Province getProvinceById(Integer id) {
		return provinceDao.getById(id);
	}

	/**
	 * 删除省份
	 * 
	 * @param province
	 */
	@CacheEvict(value = "ProvinceList", allEntries=true)
	public void deleteProvince(Integer id) {
		// 1.删除省份下的地区
		areaDao.deleteAreaByProvinceid(id);
		// 2.删除省份下的城市
		cityDao.deleteCityByProvinceid(id);
		// 3.删除省份
		provinceDao.deleteProvince(id);
	}

	/**
	 * 删除城市
	 * 
	 * @param cityid
	 */
	public void deleteCity(Integer id) {
		// 1.删除城市下的地区
		areaDao.deleteAreaByCityid(id);
		// 2.删除城市
		cityDao.deleteCity(id);
	}

	/**
	 * 删除地区
	 * 
	 * @param areaid
	 */
	public void deleteArea(Integer id) {
		areaDao.deleteAreaById(id);
	}
	
	@Override
	public void deleteAreaList(List<Integer> idList) {
		areaDao.deleteAreaList(idList);
	}

	/**
	 * 添加省份
	 * 
	 * @param province
	 */
	@CacheEvict(value = "ProvinceList", allEntries=true)
	public void saveProvince(Province province) {
		provinceDao.save(province);
	}

	/**
	 * 添加城市
	 * 
	 * @param province
	 */
	@CacheEvict(value = "CityList", allEntries=true)
	public void saveCity(City city) {
		cityDao.save(city);
	}

	/**
	 * 添加地区
	 * 
	 * @param province
	 */
	@CacheEvict(value = "AreaList", allEntries=true)
	public void saveArea(Area area) {
		areaDao.saveArea(area);
	}

	/**
	 * 根据城市Id查找城市
	 * 
	 * @param cityid
	 * @return
	 */
	public City getCityById(Integer id) {
		return cityDao.getById(id);
	}

	/**
	 * 根据地区Id查找地区
	 * 
	 * @param areaid
	 * @return
	 */
	public Area getAreaById(Integer id) {
		return areaDao.getAreaById(id);
	}

	/**
	 * 更新省份
	 */
	@CacheEvict(value = "ProvinceList", allEntries=true)
	public void updateProvince(Province province) {
		provinceDao.updateProvince(province);
	}

	/**
	 * 更新城市
	 */
	@CacheEvict(value = "CityList", allEntries=true)
	public void updateCity(City city) {
		cityDao.updateCity(city);
	}

	/**
	 * 更新地区
	 */
	@CacheEvict(value = "AreaList", allEntries=true)
	public void updateArea(Area area) {
		areaDao.updateArea(area);
	}

	@Override
	public Province getProvinceByProId(String provinceid) {
		return provinceDao.getProvinceByProvinceid(provinceid);
	}

	@Override
	public List<Province> getProvinceByName(String provinceName) {
		return provinceDao.getProvinceByName(provinceName);
	}

	@Override
	public List<City> getCityByName(String cityName) {
		return cityDao.getCityByName(cityName);
	}


}
