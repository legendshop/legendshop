/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserFeedBackDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserFeedBack;
import com.legendshop.spi.service.UserFeedBackService;
import com.legendshop.util.AppUtils;

/**
 * 用户反馈服务.
 */
@Service("userFeedBackService")
public class UserFeedBackServiceImpl  implements UserFeedBackService{

	@Autowired
    private UserFeedBackDao userFeedBackDao;

    @Override
	public UserFeedBack getUserFeedBack(Long id) {
        return userFeedBackDao.getUserFeedBack(id);
    }

    @Override
	public void deleteUserFeedBack(UserFeedBack userFeedBack) {
        userFeedBackDao.deleteUserFeedBack(userFeedBack);
    }

    @Override
	public Long saveUserFeedBack(UserFeedBack userFeedBack) {
        if (!AppUtils.isBlank(userFeedBack.getId())) {
            updateUserFeedBack(userFeedBack);
            return userFeedBack.getId();
        }
        return userFeedBackDao.saveUserFeedBack(userFeedBack);
    }

    @Override
	public void updateUserFeedBack(UserFeedBack userFeedBack) {
        userFeedBackDao.updateUserFeedBack(userFeedBack);
    }

	@Override
	public PageSupport<UserFeedBack> getUserFeedBackPage(String curPageNO, UserFeedBack userFeedback) {
		return userFeedBackDao.getUserFeedBackPage(curPageNO,userFeedback);
	}
}
