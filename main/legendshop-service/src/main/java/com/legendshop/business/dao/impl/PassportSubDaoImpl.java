/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PassportSubDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PassportSub;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * The Class PassportSubDaoImpl.
 * Dao实现类
 */
@Repository("passportSubDao")
public class PassportSubDaoImpl extends GenericDaoImpl<PassportSub, Long> implements PassportSubDao {

	/**
	 * 根据Id获取
	 */
	public PassportSub getPassportSub(Long id) {
		return getById(id);
	}

	/**
	 * 删除
	 */
	public int deletePassportSub(PassportSub passportSub) {
		return delete(passportSub);
	}

	/**
	 * 保存
	 */
	public Long savePassportSub(PassportSub passportSub) {
		return save(passportSub);
	}

	/**
	 * 更新
	 */
	public int updatePassportSub(PassportSub passportSub) {
		return update(passportSub);
	}

	/**
	 * 查询列表
	 */
	public PageSupport<PassportSub> getPassportSub(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public int updatePassportIdByPassportId(Long passPortId, Long byPassPortId) {

		String sql = "UPDATE ls_passport_sub SET passport_id = ? WHERE passport_id = ? ";

		return this.update(sql, passPortId, byPassPortId);
	}

	@Override
	public int delByPassportId(List<Long> passportIds) {
		if (CollectionUtils.isEmpty(passportIds)) {
			return 0;
		}
		StringBuilder sql = new StringBuilder("DELETE FROM ls_passport_sub WHERE passport_id IN(");
		for (int i = 0; i < passportIds.size(); i++) {
			if (i != 0) {
				sql.append(",");
			}
			sql.append("?");
		}
		sql.append(")");
		return super.update(sql.toString(), passportIds.toArray());
	}
}
