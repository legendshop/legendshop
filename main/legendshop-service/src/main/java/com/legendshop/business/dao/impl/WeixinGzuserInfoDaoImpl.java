/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinGzuserInfoDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

/**
 *微信关注用户Dao
 */
@Repository
public class WeixinGzuserInfoDaoImpl extends GenericDaoImpl<WeixinGzuserInfo, Long> implements WeixinGzuserInfoDao  {
     
    public WeixinGzuserInfo getWeixinGzuserInfo(String userName){
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("openid", userName));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
    }
	
	public WeixinGzuserInfo getWeixinGzuserInfo(Long id){
		return getById(id);
	}
	
	@CacheEvict(value="WeixinGzuserInfo",key="#weixinGzuserInfo.id")
    public int deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
    	return delete(weixinGzuserInfo);
    }
	
	public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
		return save(weixinGzuserInfo);
	}
	
	public int updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
		return update(weixinGzuserInfo);
	}

	@Override
	public List<WeixinGzuserInfo> getUserByGroupId(Long groupid) {
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("groupid", groupid));
		return infos;
	}

	@Override
	public List<WeixinGzuserInfo> getUserInfo() {
		List<WeixinGzuserInfo> userInfoList=this.queryByProperties(new EntityCriterion()); 
		return userInfoList;
	}


	@Override
	public PageSupport<WeixinGzuserInfo> querySimplePage(String curPageNO, String key) {
		 SimpleSqlQuery query = new SimpleSqlQuery(WeixinGzuserInfo.class, 10, curPageNO);
		 QueryMap map = new QueryMap();
		 String queryAllSQL = "select count(id) from ls_weixin_gzuserinfo where 1=1 ";
		 String querySQL = "select wu.*,wg.name as groupName from ls_weixin_gzuserinfo wu left join ls_weixin_group wg on wu.groupid=wg.id ";
		 if(AppUtils.isNotBlank(key)){
			 map.put("key", "%"+key +"%");
			 queryAllSQL+= " and nickname like ?";
			 querySQL+=" where wu.nickname like ?";
		 }
		 querySQL+=" order by wu.subscribe_time desc";
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

 }
