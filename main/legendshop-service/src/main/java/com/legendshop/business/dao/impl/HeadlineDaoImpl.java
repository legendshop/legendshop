/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.HeadlineDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;

/**
 * WAP头条新闻表DaoImpl.
 */
@Repository
public class HeadlineDaoImpl extends GenericDaoImpl<Headline, Long> implements HeadlineDao  {
     
	public Headline getHeadline(Long id){
		return getById(id);
	}
	
    public int deleteHeadline(Headline headline){
    	return delete(headline);
    }
	
	public Long saveHeadline(Headline headline){
		return save(headline);
	}
	
	public int updateHeadline(Headline headline){
		return update(headline);
	}

	@Override
	public List<Headline> queryByAll() {
		String sql="SELECT * FROM ls_headline ORDER BY seq DESC";
		return this.query(sql, Headline.class);
	}

	@Override
	public PageSupport<Headline> getHeadlinePage(String curPageNO) {
		 CriteriaQuery cq = new CriteriaQuery(Headline.class, curPageNO);
		return queryPage(cq);
	}
	
	@Override
	public PageSupport<Headline> query(String curPageNO) {
    	CriteriaQuery cq = new CriteriaQuery(Headline.class, curPageNO);
    	cq.setPageSize(10);
    	cq.addAscOrder("seq");
		return queryPage(cq);
	}
 }
