/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.auction;

import java.util.List;

import com.legendshop.model.entity.ShopOrderBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.spi.service.AuctionDepositRecService;
import com.legendshop.util.AppUtils;

/**
 * The Class AuctionDepositRecServiceImpl.
 */
@Service("auctionDepositRecService")
public class AuctionDepositRecServiceImpl implements AuctionDepositRecService {
	
	@Autowired
	private AuctionDepositRecDao auctionDepositRecDao;

	public void setAuctionDepositRecDao(AuctionDepositRecDao auctionDepositRecDao) {
		this.auctionDepositRecDao = auctionDepositRecDao;
	}

	public AuctionDepositRec getAuctionDepositRec(Long id) {
		return auctionDepositRecDao.getAuctionDepositRec(id);
	}

	public void deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		auctionDepositRecDao.deleteAuctionDepositRec(auctionDepositRec);
	}

	public Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		if (!AppUtils.isBlank(auctionDepositRec.getId())) {
			updateAuctionDepositRec(auctionDepositRec);
			return auctionDepositRec.getId();
		}
		return auctionDepositRecDao.saveAuctionDepositRec(auctionDepositRec);
	}

	public void updateAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		auctionDepositRecDao.updateAuctionDepositRec(auctionDepositRec);
	}

	@Override
	public boolean isPaySecurity(String userId, Long paimaiId) {
		return auctionDepositRecDao.isPaySecurity(userId, paimaiId);
	}

	@Override
	public Long queryAccess(Long paimaiId) {
		return auctionDepositRecDao.queryAccess(paimaiId);
	}

	@Override
	public List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId) {
		return auctionDepositRecDao.getAuctionDepositRecByAid(aId);
	}

	@Override
	public AuctionDepositRec getAuctionDepositRecByUserIdAndaId(String userId, Long paimaiId) {
		return auctionDepositRecDao.getAuctionDepositRecByUserIdAndaId(userId, paimaiId);
	}

	@Override
	public PageSupport<AuctionDepositRec> queryDepositRecList(String curPageNO, Long id) {
		return auctionDepositRecDao.queryDepositRecList(curPageNO, id);
	}

	@Override
	public AuctionDepositRec getAuctionDepositRec(String userId, String paySn) {
		return auctionDepositRecDao.getAuctionDepositRec(paySn, userId);
	}

	/**
	 * 根据结算当期获取结算的拍卖保证金记录
	 * @param curPageNO
	 * @param shopId
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<AuctionDepositRec> queryAuctionDepositRecList(String curPageNO, Long shopId, ShopOrderBill shopOrderBill) {

		return auctionDepositRecDao.queryAuctionDepositRecList( curPageNO,shopId,shopOrderBill);
	}
}
