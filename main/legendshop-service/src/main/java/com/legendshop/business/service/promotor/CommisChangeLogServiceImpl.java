/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.promotor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.promoter.CommisChangeLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.promoter.model.CommisChangeLog;
import com.legendshop.spi.service.CommisChangeLogService;
import com.legendshop.util.AppUtils;

/**
 * 佣金变更历史Service实现类
 */
@Service("commisChangeLogService")
public class CommisChangeLogServiceImpl implements CommisChangeLogService {

	@Autowired
	private CommisChangeLogDao commisChangeLogDao;

	/**
	 * 获取佣金变更历史 
	 */
	public CommisChangeLog getCommisChangeLog(Long id) {
		return commisChangeLogDao.getCommisChangeLog(id);
	}

	/**
	 * 删除佣金变更历史 
	 */
	public void deleteCommisChangeLog(CommisChangeLog commisChangeLog) {
		commisChangeLogDao.deleteCommisChangeLog(commisChangeLog);
	}

	/**
	 * 保存佣金变更历史 
	 */
	public Long saveCommisChangeLog(CommisChangeLog commisChangeLog) {
		if (!AppUtils.isBlank(commisChangeLog.getId())) {
			updateCommisChangeLog(commisChangeLog);
			return commisChangeLog.getId();
		}
		return commisChangeLogDao.saveCommisChangeLog(commisChangeLog);
	}

	/**
	 * 更新佣金变更历史 
	 */
	public void updateCommisChangeLog(CommisChangeLog commisChangeLog) {
		commisChangeLogDao.updateCommisChangeLog(commisChangeLog);
	}

	/**
	 * 获取佣金变更历史 
	 */
	public PageSupport<CommisChangeLog> getCommisChangeLogByCq(String curPageNO) {
		return commisChangeLogDao.getCommisChangeLogByCq(curPageNO);
	}

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public PageSupport<CommisChangeLog> getCommisChangeLog(int pageSize, String userId) {
		return commisChangeLogDao.getCommisChangeLog(pageSize, userId);
	}

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO, int pageSize, String userId, AwardSearchParamDto paramDto) {
		return commisChangeLogDao.getCommisChangeLog(curPageNO, pageSize, userId, paramDto);
	}
	
}
