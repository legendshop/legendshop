/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.NavigationDao;
import com.legendshop.business.dao.NavigationItemDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Navigation;
import com.legendshop.model.entity.NavigationItem;
/**
 * The Class NavigationDaoImpl.
 */
@Repository
public class NavigationDaoImpl extends GenericDaoImpl<Navigation, Long> implements NavigationDao {
     
	@Autowired
    private NavigationItemDao navigationItemDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public Navigation getNavigation(Long id){
		return getById(id);
	}
	
	@CacheEvict(value = "NavigationList", allEntries = true)
    public void deleteNavigation(Navigation navigation){
		navigationItemDao.deleteNavigationItems(navigation.getNaviId());
    	delete(navigation);
    }
	
	@CacheEvict(value = "NavigationList", allEntries = true)
	public Long saveNavigation(Navigation navigation){
		return (Long)save(navigation);
	}
	
	@CacheEvict(value = "NavigationList", allEntries = true)
	public void updateNavigation(Navigation navigation){
		 update(navigation);
	}
	
	@Override
	@Cacheable(value = "NavigationList")
	public List<Navigation> getNavigationList() {
		//List<Navigation> list=findByHQL("from Navigation where status = 1 and position = 0");
		List<Navigation> list= queryByProperties(new EntityCriterion().eq("status", 1).eq("position", 0));
		//load 第二级导航
		List<NavigationItem> navigationItemList = navigationItemDao.getNavigationItem(); // 2级导航
		//将2级导航加入1级导航中
		for (Navigation navi : list) {
			for (NavigationItem navigationItem : navigationItemList) {
				navi.addSubItems(navigationItem);
			}
		}
		return list;
	}

	@Override
	public PageSupport<Navigation> getNavigationPage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(Navigation.class, curPageNO);
	    cq.setPageSize(systemParameterUtil.getPageSize());
		return queryPage(cq);
	}
 }
