/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticle;

import java.util.List;
import java.util.Set;

/**
 * The Class GrassArticleDao. 种草社区文章表Dao接口
 */
public interface GrassArticleDao extends Dao<GrassArticle,Long>{

	/**
	 * 根据Id获取种草社区文章表
	 */
	public abstract GrassArticle getGrassArticle(Long id);

	/**
	 *  根据Id删除种草社区文章表
	 */
    public abstract int deleteGrassArticle(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassArticle(GrassArticle grassArticle);

	/**
	 * 保存种草社区文章表
	 */
	public abstract Long saveGrassArticle(GrassArticle grassArticle);

	/**
	 *  更新种草社区文章表
	 */		
	public abstract int updateGrassArticle(GrassArticle grassArticle);

	/**
	 * 分页查询种草社区文章表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassArticle> queryGrassArticle(String curPageNO, Integer pageSize);
	

	public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo,String name,Integer pageSize);
	
	public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo,Integer pageSize,String condition,String order);
	
	public Long getThumbCountByUid(String uid);

	/**
	 * 获取标签关联文章
	 * @param pageNo
	 * @param size
	 * @param ids
	 * @return
	 */
    PageSupport<GrassArticle> queryGrassArticleByLabels(String pageNo, int size, Set<Long> ids);

	/**
	 * 根据作者获取文章
	 * @param pageNo
	 * @param writerId
	 * @param size
	 * @return
	 */
	PageSupport<GrassArticle> queryArticleBywriterId(String pageNo, String writerId, Integer size);
}
