/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.BrandDao;
import com.legendshop.model.constant.*;
import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MarketingDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.marketing.PurchaseProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.util.AppUtils;

/**
 * 营销活动表服务类
 */
@SuppressWarnings("all")
@Repository
public class MarketingDaoImpl extends GenericDaoImpl<Marketing, Long> implements MarketingDao {

    @Autowired
    private SystemParameterUtil systemParameterUtil;

    @Override
	public Marketing getMarketing(Long id) {
        return getById(id);
    }

    @Override
	public int deleteMarketing(Marketing marketing) {
        return delete(marketing);
    }

    @Override
	public Long saveMarketing(Marketing marketing) {
        return save(marketing);
    }

    @Override
	public int updateMarketing(Marketing marketing) {
        return update(marketing);
    }

    @Override
    public Marketing getMarketing(Long shopId, Long id) {
        return get("select * from ls_marketing where shop_id=? and id=? ", Marketing.class, shopId, id);
    }

    @Override
    public PageSupport simpleSqlQuery(SimpleSqlQuery query) {
        return this.querySimplePage(query);
    }

    @Override
    public PageSupport<Marketing> getMarketingPage(String curPageNO, Long shopId, Marketing marketing) {
        CriteriaQuery cq = new CriteriaQuery(Marketing.class, curPageNO);
        cq.setPageSize(20);
        String searchType = marketing.getSearchType();
        if (AppUtils.isNotBlank(searchType) && !"ALL".equals(searchType)){
            switch (PromotionSearchTypeEnum.matchType(searchType)){
                case NO_PUBLISH:
                    cq.eq("state", PromotionStatusEnum.NO_PUBLISH.value());
                    cq.gt("endTime", new Date());
                    break;
                case NOT_STARTED:
                    cq.eq("state", PromotionStatusEnum.ONLINE.value());
                    cq.gt("startTime", new Date());
                    break;
                case ONLINE:
                    cq.lt("startTime", new Date());
                    cq.gt("endTime", new Date());
                    cq.eq("state", PromotionStatusEnum.ONLINE.value());
                    break;
                case PAUSE:
                    cq.eq("state", PromotionStatusEnum.PAUSE.value());
                    cq.gt("endTime", new Date());
                    break;
                case FINISHED:
                    cq.lt("endTime", new Date());
                    break;
                case EXPIRED:
                    cq.eq("state", PromotionStatusEnum.OFFLINE.value());
					cq.lt("startTime", new Date());
					cq.gt("endTime", new Date());
                    break;

            }
        }
        if (AppUtils.isNotBlank(marketing.getMarketName())) {
            cq.like("marketName", "%" + marketing.getMarketName() + "%");
        }
        cq.eq("type", 2); // 限时折扣促销
        cq.eq("shopId", shopId);
        cq.addDescOrder("state");
        cq.addDescOrder("createTime");
        return queryPage(cq);
    }

    @Override
    public PageSupport<Marketing> getMarketingPage(String curPageNO, Marketing marketing) {
        CriteriaQuery cq = new CriteriaQuery(Marketing.class, curPageNO);
        cq.setPageSize(20);
        String searchType = marketing.getSearchType();
        if (AppUtils.isNotBlank(searchType) && !"ALL".equals(searchType)){
            switch (PromotionSearchTypeEnum.matchType(searchType)){
                case NO_PUBLISH:
                    cq.eq("state", PromotionStatusEnum.NO_PUBLISH.value());
                    cq.gt("endTime", new Date());
                    break;
                case NOT_STARTED:
                    cq.eq("state", PromotionStatusEnum.ONLINE.value());
                    cq.gt("startTime", new Date());
                    break;
                case ONLINE:
                    cq.lt("startTime", new Date());
                    cq.gt("endTime", new Date());
                    cq.eq("state", PromotionStatusEnum.ONLINE.value());
                    break;
                case PAUSE:
                    cq.eq("state", PromotionStatusEnum.PAUSE.value());
                    cq.gt("endTime", new Date());
                    break;
                case FINISHED:
                    cq.eq("state", PromotionStatusEnum.NO_PUBLISH.value());
                    cq.lt("endTime", new Date());
                    break;
                case EXPIRED:
                    cq.eq("state", PromotionStatusEnum.OFFLINE.value());
                    break;

            }
        }
        if (AppUtils.isNotBlank(marketing.getMarketName())) {
            cq.like("marketName", "%" + marketing.getMarketName() + "%");
        }
        // 满减 和 满折促销
        cq.add(cq.or(Restrictions.eq("type", 1), Restrictions.eq("type", 0)));
        cq.eq("shopId", marketing.getShopId());
        cq.addDescOrder("state");
        cq.addDescOrder("createTime");
        return queryPage(cq);
    }

    @Override
    public PageSupport<Marketing> getAssemblefullmail(String curPageNO, Marketing marketing) {
        CriteriaQuery cq = new CriteriaQuery(Marketing.class, curPageNO);
        cq.setPageSize(20);
        if (AppUtils.isNotBlank(marketing.getState()) && marketing.getState().intValue() != 2) {
            cq.eq("state", marketing.getState());
            if (marketing.getState().intValue() != 0) {
                cq.gt("endTime", new Date());
            }
        } else if (AppUtils.isNotBlank(marketing.getState()) && marketing.getState().intValue() == 2) {
            cq.notEq("state", 0);
            cq.lt("endTime", new Date());
        }
        if (AppUtils.isNotBlank(marketing.getMarketName())) {
            cq.like("marketName", "%" + marketing.getMarketName() + "%");
        }
        // 满件 和 满减 包邮
        cq.add(cq.or(Restrictions.eq("type", MarketingTypeEnum.FULL_NUM_MAIL.value()), Restrictions.eq("type", MarketingTypeEnum.FULL_AMOUNT_MAIL.value())));
        cq.eq("shopId", marketing.getShopId());
        cq.addDescOrder("state");
        cq.addDescOrder("createTime");
        return queryPage(cq);
    }

    @Override
    public PageSupport<Marketing> getShopMarketingMansong(String curPageNO, Marketing marketing, DataSortResult result, Integer pageSize) {
        SimpleSqlQuery query = new SimpleSqlQuery(Marketing.class, curPageNO);
        QueryMap paramMap = new QueryMap();
        Date date = new Date();
		String searchType = marketing.getSearchType();
		Timestamp nowTime = DateUtil.getNowTime();
		if (AppUtils.isNotBlank(searchType) && !"ALL".equals(searchType)){
			switch (PromotionSearchTypeEnum.matchType(searchType)){
				case NO_PUBLISH:
					paramMap.put("state", PromotionStatusEnum.NO_PUBLISH.value());
					paramMap.put("endTime", " and m.end_time > '" + nowTime + "' ");
					break;
				case NOT_STARTED:
					paramMap.put("state", PromotionStatusEnum.ONLINE.value());
					paramMap.put("startTime", " and m.start_time > '" + nowTime + "' ");
					break;
				case ONLINE:
					paramMap.put("state", PromotionStatusEnum.ONLINE.value());
					paramMap.put("startTime", " and m.start_time < '" + nowTime + "' ");
					paramMap.put("endTime", " and m.end_time > '" + nowTime + "' ");
					break;
				case PAUSE:
					paramMap.put("state", PromotionStatusEnum.PAUSE.value());
					paramMap.put("endTime", " and m.end_time > '" + nowTime + "' ");
					break;
				case FINISHED:
					paramMap.put("state", PromotionStatusEnum.NO_PUBLISH.value());
					paramMap.put("endTime", " and m.end_time < '" + nowTime + "' ");
					break;
				case EXPIRED:
					paramMap.put("state", PromotionStatusEnum.OFFLINE.value());
					break;

			}
		}
        if (AppUtils.isNotBlank(marketing.getShopId())) {
            paramMap.put("shopId", marketing.getShopId());
        }
        if (AppUtils.isNotBlank(marketing.getSiteName())) {
            paramMap.put("siteName", "%" + marketing.getSiteName().trim() + "%");
        }
        if (AppUtils.isNotBlank(marketing.getMarketName())) {
            paramMap.put("marketName", "%" + marketing.getMarketName().trim() + "%");
        }
        if (AppUtils.isNotBlank(pageSize)) {
            query.setPageSize(pageSize);
        }

        if (!result.isSortExternal()) {
            paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY m.state DESC, m.create_time DESC");
        } else {
            paramMap.put(Constants.ORDER_INDICATOR, result.parseSeq());
        }
        String QueryNsortCount = ConfigCode.getInstance().getCode("marketing.getMansongListCount", paramMap);
        String QueryNsort = ConfigCode.getInstance().getCode("marketing.getMansongList", paramMap);
        query.setAllCountString(QueryNsortCount);
        query.setQueryString(QueryNsort);
        query.setCurPage(curPageNO);
        paramMap.remove(Constants.ORDER_INDICATOR);
		paramMap.remove("startTime");
		paramMap.remove("endTime");
        query.setParam(paramMap.toArray());
        return querySimplePage(query);
    }

    @Override
    public PageSupport<Marketing> getShopMatketingZhekou(String curPageNO, Marketing marketing, Integer pageSize, DataSortResult result) {
        SimpleSqlQuery query = new SimpleSqlQuery(Marketing.class, curPageNO);
        QueryMap paramMap = new QueryMap();
        Date date = new Date();
        if (AppUtils.isNotBlank(marketing.getState()) && marketing.getState() != 2) {
            paramMap.put("startTime", date);
            paramMap.put("endTime", date);
        }
        if (AppUtils.isNotBlank(marketing.getShopId())) {
            paramMap.put("shopId", marketing.getShopId());
        }
        if (AppUtils.isNotBlank(marketing.getSiteName())) {
            paramMap.put("siteName", "%" + marketing.getSiteName().trim() + "%");
        }
        if (AppUtils.isNotBlank(marketing.getMarketName())) {
            paramMap.put("marketName", "%" + marketing.getMarketName().trim() + "%");
        }
        paramMap.put("state", marketing.getState());
        if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
            query.setPageSize(systemParameterUtil.getPageSize());
        }
        if (!result.isSortExternal()) {
            paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY  m.state DESC, m.create_time DESC");
        } else {
            paramMap.put(Constants.ORDER_INDICATOR, result.parseSeq());
        }
        String QueryNsortCount = ConfigCode.getInstance().getCode("marketing.getzhekouListCount", paramMap);
        String QueryNsort = ConfigCode.getInstance().getCode("marketing.getzhekouList", paramMap);
        query.setAllCountString(QueryNsortCount);
        query.setQueryString(QueryNsort);
        query.setCurPage(curPageNO);
        paramMap.remove(Constants.ORDER_INDICATOR);
        query.setParam(paramMap.toArray());
        return querySimplePage(query);
    }

    @Override
    public List<PurchaseProdDto> getMarketingList(Date startTime, Date endTime) {
        String sql = "SELECT p.prod_id AS prodId,p.name AS prodName,p.brief AS brirf, p.pic AS pic,"
                + " mp.cash AS cash,mp.discount_price AS discountPrice,m.start_time AS startTime, m.end_time AS endTime" + " FROM ls_prod p "
                + " LEFT JOIN ls_marketing_prods mp ON p.prod_id = mp.prod_id " + " LEFT JOIN ls_shop_detail d ON p.shop_id = d.shop_id "
                + " LEFT JOIN ls_marketing m ON mp.market_id=m.id " + " WHERE d.status=1 " + " AND p.status = 1 " + " AND mp.market_id IN (" + " SELECT id "
                + " FROM ls_marketing " + " WHERE TYPE = 2 "
                + "  AND DATE_FORMAT(start_time,'%Y:%m:%d') <= DATE_FORMAT(?,'%Y:%m:%d') AND  DATE_FORMAT(end_time,'%Y:%m:%d') >= DATE_FORMAT(?,'%Y:%m:%d') "
                + "AND DATE_FORMAT(start_time,'%H:%i:%s') >= DATE_FORMAT(?,'%H:%i:%s') AND  DATE_FORMAT(end_time,'%H:%i:%s') <= DATE_FORMAT(?,'%H:%i:%s') "
                + "AND state = 1" + "  ORDER BY start_time ASC)  limit 8";
        return this.query(sql, PurchaseProdDto.class, startTime, endTime, startTime, endTime);
    }

    @Override
    public List<Marketing> findOngoingMarketing(Long shopId, Integer isAllProds, Date startTime, Date endTime) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM ls_marketing WHERE state = 1 AND is_all_prods = ? ");
        sql.append("AND (type =1 OR type = 0 OR type = 2) AND shop_id = ? ");
        sql.append(" AND ((start_time <= ? AND end_time >= ?) ");
        sql.append(" OR (start_time <= ? AND end_time >= ?) ");
        sql.append(" OR (start_time <= ? AND end_time >= ?)) ");
        return query(sql.toString(), Marketing.class, isAllProds, shopId, startTime, startTime, endTime, endTime, startTime, endTime);
    }

    @Override
    public int batchDetele(String marketIds) {
        String[] marketIdList = marketIds.split(",");
        StringBuffer buff = new StringBuffer();
        buff.append("DELETE FROM ls_marketing WHERE id IN(");
        for (String marketId : marketIdList) {
            buff.append("?,");
        }
        buff.deleteCharAt(buff.length() - 1);
        buff.append(")");
        return update(buff.toString(), marketIdList);
    }

    @Override
    public void updateMarketStatus(List<Long> ids) {
        String sql = "update ls_marketing set state = 0 where id = ?";
        List<Object[]> objs = new ArrayList<Object[]>();
        for (Long id : ids) {
            Object[] obj = new Object[]{id};
            objs.add(obj);
        }
        batchUpdate(sql, objs);
    }

    @Override
    public int getAllProdsMarketing(Long shopId, Integer type, Date startTime, Date endTime) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT count(id) FROM ls_marketing WHERE state = 1 AND is_all_prods = 1 ");
        if (type.equals(MarketingTypeEnum.MAN_JIAN.value()) || type.equals(MarketingTypeEnum.MAN_ZE.value())) {
            sql.append("AND (TYPE = 1 OR TYPE = 0 OR TYPE = 2) ");
        } else {
            sql.append("AND type = " + type);
        }
        sql.append(" AND shop_id = ? ");
        sql.append(" AND ((start_time <= ? AND end_time >= ?) ");
        sql.append(" OR (start_time <= ? AND end_time >= ?) ");
        sql.append(" OR (start_time <= ? AND end_time >= ?)) ");
        return get(sql.toString(), Integer.class, shopId, startTime, startTime, endTime, endTime, startTime, endTime);
    }

    @Override
    public List<Marketing> getExpiredMarketing(Integer commitInteval, Date endTime) {

        String sql = "SELECT m.* FROM ls_marketing m WHERE m.is_expired = 0 and m.end_time < ?  LIMIT ?";

        return query(sql, Marketing.class, endTime, commitInteval);
    }
}
