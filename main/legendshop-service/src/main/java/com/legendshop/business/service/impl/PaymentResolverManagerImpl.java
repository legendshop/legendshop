package com.legendshop.business.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.spi.manager.PaymentResolverManager;
import com.legendshop.spi.service.IPaymentResolver;

/**
 * 
 * 支付处理者
 *
 */
@Component
public class PaymentResolverManagerImpl implements PaymentResolverManager {
	
	@Resource(name="paymentResolverExecutors")
	private Map<String, IPaymentResolver> executors; 

	
	/* (non-Javadoc)
	 * @see com.legendshop.spi.manager.PaymentResolverManager#queryPayto(com.legendshop.model.constant.SubSettlementTypeEnum, java.util.Map)
	 */
	@Override
	public Map<String, Object> queryPayto(SubSettlementTypeEnum typeEnum,Map<String,String> params){
		IPaymentResolver executor = executors.get(typeEnum.value());
		if(executor!=null){
			//把业务规则应用到商品
			return executor.queryPayto(params);
		}
		return null;
    }
	
	
	/** 
	 * 支付回调
	 */
	@Override
	public void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallbackForm){
		IPaymentResolver executor = executors.get(subSettlement.getType());
		if(executor!=null){
			//把业务规则应用到商品
			 executor.doBankCallback(subSettlement,bankCallbackForm);
		}
	}
	
}
