/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AppPageManageDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.spi.service.AppPageManageService;
import com.legendshop.util.AppUtils;


/**
 * The Class AppPageManageServiceImpl.
 *  移动端页面装修页面服务实现类
 * @author legendshop
 */
@Service("appPageManageService")
public class AppPageManageServiceImpl implements AppPageManageService{

	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(MenuManagerServiceImpl.class);
    /**
     *
     * 引用的移动端页面装修页面Dao接口
     */
	@Autowired
    private AppPageManageDao appPageManageDao;
   
   	/**
	 *  根据Id获取移动端页面装修页面
	 */
    @Override
	public AppPageManage getAppPageManage(Long id) {
        return appPageManageDao.getAppPageManage(id);
    }
    
    /**
	 *  根据Id删除移动端页面装修页面
	 */
    @Override
	public int deleteAppPageManage(Long id){
    	return appPageManageDao.deleteAppPageManage(id);
    }

   /**
	 *  删除移动端页面装修页面
	 */ 
    @Override
	public int deleteAppPageManage(AppPageManage appPageManage) {
       return  appPageManageDao.deleteAppPageManage(appPageManage);
    }

   /**
	 *  保存移动端页面装修页面
	 */	    
    @Override
	public Long saveAppPageManage(AppPageManage appPageManage) {
        if (!AppUtils.isBlank(appPageManage.getId())) {
            updateAppPageManage(appPageManage);
            return appPageManage.getId();
        }
        return appPageManageDao.saveAppPageManage(appPageManage);
    }

   /**
	 *  更新移动端页面装修页面
	 */	
    @Override
	public void updateAppPageManage(AppPageManage appPageManage) {
        appPageManageDao.updateAppPageManage(appPageManage);
    }  


    /**
	 *  分页查询列表
	 */	
    @Override
	public PageSupport<AppPageManage> queryAppPageManage(String curPageNO, Integer pageSize, AppPageManage appPageManage){
     	return appPageManageDao.queryAppPageManage(curPageNO, pageSize, appPageManage);
    }

    /**
     * 将使用中的页面更新为未使用
     */
	@Override
	public void updatePageToUnUse(Integer use) {
		appPageManageDao.updatePageToUnUse(use);
	}

	
	/**
	 * 根据使用状态获取页面 isUse 使用状态 1：使用中 0 ：未使用
	 */
	@Override
	public AppPageManage getUseAppPageByUse(Integer isUse) {
		return appPageManageDao.getUseAppPageByUse(isUse);
	}

}
