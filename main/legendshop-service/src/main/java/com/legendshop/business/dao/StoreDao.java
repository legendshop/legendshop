/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.entity.store.Store;

/**
 * 门店Dao接口
 */
public interface StoreDao extends GenericDao<Store, Long> {

	/** 获取门店 */
	public abstract Store getStore(Long id);

	/** 删除门店 */
	public abstract int deleteStore(Store store);

	/** 保存门店 */
	public abstract Long saveStore(Store store);

	/** 更新门店 */
	public abstract int updateStore(Store store);

	/** 获取门店 */
	public abstract Store getStore(Long id, Long shopId);

	/** 查询门店 */
	public abstract PageSupport<Store> query(String curPageNO, Long shopId);

	/** 查询门店 */
	public abstract PageSupport<StoreProductDto> query(String curPageNO, int pageSize, String storeId, Long shopId, Long poductId, String productname);

}
