/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.LoginHistoryDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.LoginHistorySum;
import com.legendshop.model.entity.LoginHistory;
import com.legendshop.util.AppUtils;

/**
 * 登录历史Dao
 * 
 */
@Repository
public class LoginHistoryDaoImpl extends GenericDaoImpl<LoginHistory, Long> implements LoginHistoryDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public LoginHistory getLastLoginTime(String userName) {
		List<LoginHistory> list = this.queryLimit("select id,area,country,user_name as userName,ip,time from ls_login_hist where user_name= ? order by time desc", LoginHistory.class, 1, 1, userName);
		if(AppUtils.isBlank(list)){
			return null;
		}else{
			return list.get(0);
		}
	}

	@Override
	public PageSupport<LoginHistory> getLoginHistoryPage(String curPageNO, LoginHistory login) {
		CriteriaQuery cq = new CriteriaQuery(LoginHistory.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		if(AppUtils.isNotBlank(login.getUserName())) {			
			cq.like("userName", "%" + login.getUserName().trim() + "%");
		}
		cq.ge("time", login.getStartTime());
		if(AppUtils.isNotBlank(login.getEndTime())){
			cq.le("time", DateUtils.getEndMonment(login.getEndTime()));
		}
		cq.addOrder("desc", "time");
		return queryPage(cq);
	}

	@Override
	public PageSupport<LoginHistorySum> getLoginHistoryBySQL(String curPageNO, LoginHistory login) {
		SimpleSqlQuery query = new SimpleSqlQuery(LoginHistorySum.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		if (!AppUtils.isBlank(login.getUserName())) {
			map.like("userName", login.getUserName().trim());
		}
		if (!AppUtils.isBlank(login.getStartTime())) {
			map.put("startTime", login.getStartTime());
		}
		if (!AppUtils.isBlank(login.getEndTime())) {
			map.put("endTime", DateUtils.getIntegralEndTime(login.getEndTime()));
		}
		String sql = ConfigCode.getInstance().getCode("biz.loginHistorySum", map);
		String countSql = ConfigCode.getInstance().getCode("biz.loginHistoryCount", map);
		query.setQueryString(sql);
		query.setAllCountString(countSql);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public Long saveLoginHistory(LoginHistory LoginHistory) {
		return save(LoginHistory);
	}

}
