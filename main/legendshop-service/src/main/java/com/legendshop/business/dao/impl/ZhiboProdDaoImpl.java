package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */


import com.legendshop.business.dao.ZhiboProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.zhibo.ZhiboRoomMemberDto;
import com.legendshop.model.dto.zhibo.ZhiboRoomProdDto;
import com.legendshop.model.entity.ZhiboProd;
import com.legendshop.util.AppUtils;

/**
 * The Class ZhiboProdDaoImpl.
  * Dao实现类
 */
@Repository
public class ZhiboProdDaoImpl extends GenericDaoImpl<ZhiboProd, Long> implements ZhiboProdDao  {

   	/**
	 *  根据Id获取
	 */
	public ZhiboProd getZhiboProd(Long id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteZhiboProd(ZhiboProd zhiboProd){
    	return delete(zhiboProd);
    }

   /**
	 *  保存
	 */		
	public Long saveZhiboProd(ZhiboProd zhiboProd){
		return save(zhiboProd);
	}

   /**
	 *  更新
	 */		
	public int updateZhiboProd(ZhiboProd zhiboProd){
		return update(zhiboProd);
	}

    public List<Long> updateAvRoomProd(Long avRoomId,Long[] prodId){
    	List<ZhiboProd> zhiboProdList = new ArrayList<ZhiboProd>();
    	
		for (int i = 0; i < prodId.length-1; i++) {
	    	ZhiboProd zhiboProd = new ZhiboProd();
	    	zhiboProd.setAvRoomId(avRoomId);
	    	zhiboProd.setProdId(prodId[i]);
			zhiboProdList.add(zhiboProd);

		}
    	return this.save(zhiboProdList);
    }

	@Override
	public PageSupport<ZhiboRoomProdDto> querySimplePage(String curPageNO, String avRoomId, String prodName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ZhiboRoomProdDto.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(avRoomId)){
			map.put("av_room_id", avRoomId);
		}
		if(AppUtils.isNotBlank(prodName)){
			map.put("name", prodName);
		}
		String querySQL = ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomProd", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomProdCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ZhiboRoomMemberDto> queryZhiboRoomProdq(String curPageNO, String avRoomId, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ZhiboRoomMemberDto.class,20,curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(avRoomId)){
			map.put("av_room_id", avRoomId);
		}
		if(AppUtils.isNotBlank(userName)){
			map.put("name", userName);
		}
		String querySQL = ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomMember", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("zhibo.queryZhiboRoomMemberCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
 }
