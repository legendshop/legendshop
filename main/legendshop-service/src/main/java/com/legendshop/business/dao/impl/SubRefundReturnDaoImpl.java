/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.legendshop.dao.support.*;
import org.springframework.stereotype.Repository;

import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.SubRefundReturnDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.dto.SubRefundRetuenDto;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.util.AppUtils;

/**
 *退款表Dao
 */
@Repository("subRefundReturnDao")
public class SubRefundReturnDaoImpl extends GenericDaoImpl<SubRefundReturn, Long> implements SubRefundReturnDao  {

    /** 获得已经退款成功的订单项 */
    public List<SubRefundReturn> getSubRefundsSuccessBySettleSn(String settleSn){
    	return this.query("SELECT * FROM ls_sub_refund_return r WHERE r.sub_settlement_sn = ? AND r.is_handle_success = 1",SubRefundReturn.class,settleSn);
    }

	public SubRefundReturn getSubRefundReturn(Long id){
		return getById(id);
	}
	
    public int deleteSubRefundReturn(SubRefundReturn subRefundReturn){
    	return delete(subRefundReturn);
    }
	
	public Long saveSubRefundReturn(SubRefundReturn subRefundReturn){
		if(AppUtils.isBlank(subRefundReturn.getRefundId())){
			return save(subRefundReturn);
		}else{
			update(subRefundReturn);
			return subRefundReturn.getRefundId();
		}
	}
	
	public int updateSubRefundReturn(SubRefundReturn subRefundReturn){
		return update(subRefundReturn);
	}
	
	/**
	 * 更新第三方退款金额
	 * @param refundAmount 第三方退款金额
	 */
	public int updateSubThirdRefundAmount(Long returnPayId, BigDecimal refundAmount){
		return this.update("UPDATE ls_sub_refund_return r SET r.third_party_refund = ? WHERE r.refund_id = ?",refundAmount,returnPayId);
	}
	
	/**
	 * 更新平台退款
	 */
	public int updateSubPlatRefund(Long refundId,BigDecimal refundAmount,Integer is_handle_success,String refundType){
    	return this.update("UPDATE ls_sub_refund_return r SET r.refund_amount = ? ,r.is_handle_success = ?,r.handle_type = ?,r.third_party_refund = 0,r.platform_refund = ? WHERE r.refund_id = ?",
    			refundAmount,is_handle_success,refundType,refundAmount,refundId);
    }

	@Override
	public ApplyRefundReturnDto getOrderInfo(Long orderId,String userId) {
		String sql = ConfigCode.getInstance().getCode("refund.getRefundOrder");
		return this.get(sql, ApplyRefundReturnDto.class, orderId,userId);
	}

	@Override
	public ApplyRefundReturnDto getOrderItemInfo(Long orderItemId,String userId) {
		String sql = ConfigCode.getInstance().getCode("refund.getRefundOrderItem");
		return this.get(sql, ApplyRefundReturnDto.class, orderItemId,userId);
	}

	@Override
	public void updateRefundSn(Long returnPayId, String batch_no) {
		this.update("UPDATE ls_sub_refund_return set refund_sn=? WHERE refund_id=? and is_handle_success=0 ", batch_no,returnPayId);
	}

	@Override
	public int orderReturnSucess(String refundSn, String outRefundNo,String handleType) {
		return this.update("UPDATE ls_sub_refund_return set out_refund_no=?,is_handle_success=1,handle_type=? where refund_sn=? and is_handle_success=0 ", outRefundNo,handleType,refundSn);
	}
	
	/**
	 * 当支付宝退款成功后，更新
	 */
	@Override
	public int orderReturnOnlineSettle(String refundSn, String outRefundNo,Double refundAmount,String handleType) {
		return this.update("UPDATE ls_sub_refund_return set out_refund_no=?,is_handle_success=1,handle_type=?,platform_refund=? where refund_sn=? and is_handle_success=0 ", outRefundNo,handleType,refundAmount,refundSn);
	}
	
	/**
	 * 退款失败
	 */
	@Override
	public int orderReturnFail(String refundSn) {
		return this.update("UPDATE ls_sub_refund_return set is_handle_success=-1 where refund_sn=? and is_handle_success=0 ",refundSn);
	}

	@Override
	public double getSubRefundSuccess(Long shopId, Date startDate, Date endDate) {
		return this.get("select sum(refund_amount) from ls_sub_refund_return where is_handle_success=1 and is_bill=0 and shop_id=? and status=? and admin_time <=? ",Double.class,shopId,RefundReturnStatusEnum.APPLY_FINISH.value(),endDate);
	}
	
	
	/**
	 * 退款完成并且 申请状态:3为已完成 的退款单
	 */
	@Override
	public List<SubRefundReturn> getSubRefundSuccess(Long shopId, Date endDate) {
		return this.query("select refund_id as refundId,sub_id as subId,sub_number as subNumber,sub_item_id as subItemId,seller_state as sellerState,apply_state as applyState,refund_amount as refundAmount from ls_sub_refund_return where is_handle_success=1 and is_bill=0 and shop_id=? and apply_state=? and admin_time < ? ",
				SubRefundReturn.class,
				shopId,RefundReturnStatusEnum.APPLY_FINISH.value(),endDate);
	}

	@Override
	public SubRefundReturn getSubRefund(String subNumber, Long subItemId) {
	    List<SubRefundReturn> refundReturns=query("select refund_id as refundId,sub_id as subId,sub_number as subNumber,sub_item_id as subItemId,seller_state as sellerState,apply_state as applyState from ls_sub_refund_return where is_bill=0 and seller_state!=3 and ((sub_number=? and sub_item_id=?) or (sub_number=? and sub_item_id=0)) ORDER BY ls_sub_refund_return.apply_time DESC ", SubRefundReturn.class,subNumber,subItemId);
	    if(AppUtils.isBlank(refundReturns)){
	    	return null;
	    }
	    return refundReturns.get(0);
	}

	@Override
	public SubRefundReturn getSubRefundReturnBySubNumber(String subNumber) {
		return this.getByProperties(new EntityCriterion().eq("subNumber", subNumber).notEq("applyState","-1"));
	}
	
	/**
	 * 根据订单项Id查看退货详情
	 */
	@Override
	public SubRefundReturn getSubRefundReturnBySubItemId(String subItemId) {
		return this.getByProperties(new EntityCriterion().eq("subItemId", subItemId));
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, String status,
			RefundSearchParamDto paramData) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		cq.eq("applyType", RefundReturnTypeEnum.REFUND_RETURN.value());
		if("pending".equalsIgnoreCase(status)){
			cq.eq("applyState", RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());
		}
		if(AppUtils.isNotBlank(paramData.getShopId())){
			cq.eq("shopId", paramData.getShopId());
		}
		if(AppUtils.isNotBlank(paramData.getSellerState())){
			cq.eq("sellerState", paramData.getSellerState());
		}
		if(AppUtils.isNotBlank(paramData.getStartDate())){
			cq.ge("applyTime", paramData.getStartDate());
		}
		Date endDate = paramData.getEndDate();
		if(AppUtils.isNotBlank(endDate)){
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				endDate = dateFormat.parse(year+"-"+month+"-"+day+" 23:59:59");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			cq.le("applyTime", endDate);
		}
		if(AppUtils.isNotBlank(paramData.getNumber())){
			if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//订单编号
				cq.like("subNumber", "%" + paramData.getNumber().trim() + "%");
			}else if(RefundSearchParamDto.REFUND_NO.equals(paramData.getNumType())){//退款编号
				cq.like("refundSn","%" +  paramData.getNumber().trim() + "%");
			}else if(RefundSearchParamDto.USER_NAME.equals(paramData.getNumType())){//会员名称
				cq.like("userName","%" +  paramData.getNumber().trim() + "%");
			} else if(RefundSearchParamDto.SHOP_NAME.equals(paramData.getNumType())){//商家名称
				cq.like("shopName","%" +  paramData.getNumber().trim() + "%");
			}
		}
		cq.addDescOrder("applyTime");
		cq.addAscOrder("applyState");
		return queryPage(cq);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturn(String curPageNO, String status,
			RefundSearchParamDto paramData) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		cq.eq("applyType", RefundReturnTypeEnum.REFUND.value());
		if("pending".equalsIgnoreCase(status)){
			cq.eq("applyState", RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());
		}
		if(AppUtils.isNotBlank(paramData.getShopId())){
			cq.eq("shopId", paramData.getShopId());
		}
		if(AppUtils.isNotBlank(paramData.getSellerState())){
			cq.eq("sellerState", paramData.getSellerState());
		}
		if(AppUtils.isNotBlank(paramData.getStartDate())){
			cq.ge("applyTime", paramData.getStartDate());
		}
		Date endDate = paramData.getEndDate();
		if(AppUtils.isNotBlank(endDate)){
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				endDate = dateFormat.parse(year+"-"+month+"-"+day+" 23:59:59");
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cq.le("applyTime", endDate);
		}
		if(AppUtils.isNotBlank(paramData.getNumber())){
			if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//订单编号
				cq.like("subNumber", paramData.getNumber().trim(),MatchMode.ANYWHERE);
			}else if(RefundSearchParamDto.REFUND_NO.equals(paramData.getNumType())){//退款编号
				cq.like("refundSn", paramData.getNumber().trim(),MatchMode.ANYWHERE);
			}else if(RefundSearchParamDto.USER_NAME.equals(paramData.getNumType())){//会员名称
				cq.like("userName", paramData.getNumber().trim(),MatchMode.ANYWHERE);
			}
		}
		cq.addDescOrder("applyTime");
		cq.addAscOrder("applyState");
		return queryPage(cq);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, ShopOrderBill shopOrderBill,
			String subNumber) {
		QueryMap map = new QueryMap();
		map.put("shopId", shopOrderBill.getShopId());
		map.put("applyState",RefundReturnStatusEnum.APPLY_FINISH.value());
		map.put("billSn",shopOrderBill.getSn());

		if (AppUtils.isNotBlank(subNumber)) {
			map.put("subNumber", "%" + subNumber.trim() + "%");
		}

		SimpleSqlQuery query = new SimpleSqlQuery(SubRefundReturn.class);
		query.setPageSize(10);
		query.setCurPage(curPageNO);
		query.setAllCountString(ConfigCode.getInstance().getCode("order.getRefundReturnOrderCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("order.getRefundReturnOrder", map));
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, Long shopId, String subNumber,
			ShopOrderBill shopOrderBill) {

		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		map.put("applyState",RefundReturnStatusEnum.APPLY_FINISH.value());
		map.put("billSn",shopOrderBill.getSn());

		if (AppUtils.isNotBlank(subNumber)) {
			map.put("subNumber", "%" + subNumber.trim() + "%");
		}

		SimpleSqlQuery query = new SimpleSqlQuery(SubRefundReturn.class);
		query.setPageSize(10);
		query.setCurPage(curPageNO);
		query.setAllCountString(ConfigCode.getInstance().getCode("order.getRefundReturnOrderCount", map));
		query.setQueryString(ConfigCode.getInstance().getCode("order.getRefundReturnOrder", map));
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, RefundSearchParamDto paramData,
			Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);

		Date endDate = paramData.getEndDate();
		if (ObjectUtil.isNotEmpty(endDate)) {
			//时间处理
			paramData.setEndDate(DateUtil.endOfDay(endDate));
		}
		//设置查询条件
		setSearchCondition(cq,shopId,RefundReturnTypeEnum.REFUND.value(),paramData);

		cq.addAscOrder("sellerState");
		cq.addDescOrder("applyTime");
		cq.setPageSize(20);
		PageSupport<SubRefundReturn> pageSupport = queryPage(cq);

		return pageSupport;
	}
	
	private void setSearchCondition(CriteriaQuery cq,Long shopId, Integer applyType,RefundSearchParamDto paramData){
		cq.eq("shopId", shopId);
		cq.eq("applyType", applyType);
		if(AppUtils.isNotBlank(paramData.getCustomStatus())) {
			switch (paramData.getCustomStatus()) {
			case 1://待审核
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
				cq.eq("applyState", RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
				break;
			case 2: //已撤销
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
				cq.eq("applyState", RefundReturnStatusEnum.UNDO_APPLY.value());
				break;
			case 3: //待买家退货
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value());
				break;
			case 4: //待收货	
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value());
				break;
			case 5: //未收到	
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value());
				break;
			case 6: //已收到
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_RECEIVED.value());
				break;
			case 7: //弃货
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NO_NEED_GOODS.value());
				break;
			case 8: //已同意
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				break;
			case 9: //不同意
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_DISAGREE.value());
				break;
			}
		}
		if(AppUtils.isNotBlank(paramData.getStartDate())){
			cq.ge("applyTime", paramData.getStartDate());
		}
		if(AppUtils.isNotBlank(paramData.getEndDate())){
			cq.le("applyTime", paramData.getEndDate());
		}
		
		if(AppUtils.isNotBlank(paramData.getNumber())){
			if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//订单编号
				cq.like("subNumber", paramData.getNumber());
			}else if(RefundSearchParamDto.REFUND_NO.equals(paramData.getNumType())){//退款编号
				cq.like("refundSn", paramData.getNumber());
			}
		}
	}

	@Override
	public PageSupport<SubRefundReturn> getSubReturnListPage(String curPageNO, RefundSearchParamDto paramData,
			Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		//设置查询条件
		cq.setPageSize(20);
		setSearchCondition(cq,shopId,RefundReturnTypeEnum.REFUND_RETURN.value(),paramData);
		cq.addDescOrder("applyTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<SubRefundReturn> getRefundListPage(String curPageNO, RefundSearchParamDto paramData,
			String userId) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
        cq.setPageSize(20);
        //设置查询条件
        setSearchMobileCondition(cq,userId,RefundReturnTypeEnum.REFUND.value(),paramData);
      	cq.addAscOrder("sellerState");
      	cq.addDescOrder("applyTime");
		return queryPage(cq);
	}
	
	
	private void setSearchMobileCondition(CriteriaQuery cq,String userId, Integer applyType,RefundSearchParamDto paramData){
		cq.eq("userId", userId);
		cq.eq("applyType", applyType);
		if(AppUtils.isNotBlank(paramData.getSellerState())){
			cq.eq("sellerState", paramData.getSellerState());
		}
		if(AppUtils.isNotBlank(paramData.getStartDate())){
			cq.ge("applyTime", paramData.getStartDate());
		}
		if(AppUtils.isNotBlank(paramData.getEndDate())){
			cq.le("applyTime", DateUtils.getEndMonment(paramData.getEndDate()));
		}
		if(AppUtils.isNotBlank(paramData.getNumber())){
			if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//订单编号
				cq.like("subNumber", paramData.getNumber());
			}else if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//退款编号
				cq.like("refundSn", paramData.getNumber());
			}
		}
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnMobile(String curPageNO, String userId,
			RefundSearchParamDto paramData) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		 cq.setPageSize(20);
		//设置查询条件
		 setSearchMobileCondition(cq,userId,RefundReturnTypeEnum.REFUND_RETURN.value(),paramData);
     	cq.addAscOrder("sellerState");
     	cq.addDescOrder("applyTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
        cq.setPageSize(10);
        //设置查询条件
      	setSearchConditionByCenter(cq,userId,RefundReturnTypeEnum.REFUND.value(),paramData);
      	cq.addDescOrder("applyTime");
        cq.addAscOrder("sellerState");
		return queryPage(cq);
	}
	
	private void setSearchConditionByCenter(CriteriaQuery cq,String userId, Integer applyType,RefundSearchParamDto paramData){
		cq.eq("userId", userId);
		cq.eq("applyType", applyType);		
		if(AppUtils.isNotBlank(paramData.getCustomStatus())) {
			switch (paramData.getCustomStatus()) {
			case 1://待审核
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
				cq.eq("applyState", RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
				break;
			case 2: //已撤销
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
				cq.eq("applyState", RefundReturnStatusEnum.UNDO_APPLY.value());
				break;
			case 3: //待买家退货
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value());
				break;
			case 4: //待收货	
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value());
				break;
			case 5: //未收到	
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value());
				break;
			case 6: //已收到
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NEED_GOODS.value());
				cq.eq("goodsState", RefundReturnStatusEnum.LOGISTICS_RECEIVED.value());
				break;
			case 7: //弃货
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				cq.eq("returnType", RefundReturnTypeEnum.NO_NEED_GOODS.value());
				break;
			case 8: //已同意
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_AGREE.value());
				break;
			case 9: //不同意
				cq.eq("sellerState", RefundReturnStatusEnum.SELLER_DISAGREE.value());
				break;
			}
		}
		if(AppUtils.isNotBlank(paramData.getStartDate())){
			cq.ge("applyTime", paramData.getStartDate());
		}
		if(AppUtils.isNotBlank(paramData.getEndDate())){
			cq.le("applyTime", DateUtils.getEndMonment(paramData.getEndDate()));
		}
		if(AppUtils.isNotBlank(paramData.getNumber())){
			if(RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())){//订单编号
				cq.like("subNumber", paramData.getNumber());
			}else if(RefundSearchParamDto.REFUND_NO.equals(paramData.getNumType())){//退款编号
				cq.like("refundSn", paramData.getNumber());
			}
		}
	}

	@Override
	public PageSupport<SubRefundReturn> getReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		 cq.setPageSize(20);
		//设置查询条件
		 setSearchConditionByCenter(cq,userId,RefundReturnTypeEnum.REFUND_RETURN.value(),paramData);
        cq.addDescOrder("applyTime");
        cq.addAscOrder("sellerState");
		return queryPage(cq);
	}

	@Override
	public SubRefundReturn getSubRefundReturnByRefundSn(String refundSn) {
		return this.getByProperties(new EntityCriterion().eq("refundSn", refundSn));
	}
	
	@Override
	public List<SubRefundRetuenDto> exportOrder(String string, Object[] array) {
		return this.query(string, SubRefundRetuenDto.class, array);
	}

	@Override
	public int updateNewSubPlatRefund(Long refundId, BigDecimal alpRefundAmount, BigDecimal platformAmount, Integer isDepositHandleSucces,
			Integer isHandleSuccess, String refundType, String depositHandleType, String outRefundNo, String depositOutRefundNo) {
		String sql = "UPDATE ls_sub_refund_return r SET r.platform_refund = ?, r.third_party_refund = ?, r.is_handle_success = ?, r.is_deposit_handle_succes = ?, r.handle_type = ?, r.deposit_handle_type = ?, r.out_refund_no = ?, r.deposit_out_refund_no = ?  WHERE r.refund_id = ?";
		return update(sql, platformAmount, alpRefundAmount, isHandleSuccess, isDepositHandleSucces, refundType, depositHandleType, outRefundNo, depositOutRefundNo, refundId);
	}

	@Override
	public PageSupport<SubRefundReturn> refundList(Long shopId, Long sellerState,Integer refundReturnType, String curPageNO, int pageSize) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		cq.setPageSize(pageSize);
		// 设置查询条件
		cq.eq("shopId", shopId);
		cq.eq("sellerState", sellerState);
		cq.eq("applyType", refundReturnType);
		cq.addAscOrder("sellerState");
		cq.addDescOrder("applyTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport loadShopOrderBill(Long shopId, Long type, String sn, String curPageNO, int pageSize) {
		PageSupport ps = null;
		if(type==null || type==1){
			CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
			cq.setPageSize(10);
			cq.eq("shopId", shopId);
			cq.eq("status", OrderStatusEnum.SUCCESS.value());
			cq.eq("billSn", sn);
			ps = queryPage(cq);
		}else{
			CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
			cq.setPageSize(10);
			cq.eq("shopId", shopId);
			cq.eq("applyState",RefundReturnStatusEnum.APPLY_FINISH.value());
			cq.eq("billSn", sn);
			ps = queryPage(cq);
		}
		return null;
	}

	@Override
	public PageSupport<SubRefundReturn> refundList(String userId, Integer refundReturnType, String curPageNO, int pageSize) {
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		// 设置查询条件
		cq.eq("userId", userId);
		cq.eq("applyType", refundReturnType);
		cq.addAscOrder("sellerState");
		cq.addDescOrder("applyTime");
		cq.setPageSize(pageSize);
		PageSupport<SubRefundReturn> ps = queryPage(cq);
		return ps;
	}

	@Override
	public PageSupport<SubRefundReturn> querySubRefundReturnList(String userId, String curPageNO, Integer status) {
		
		int pageSize = 10;
		CriteriaQuery cq = new CriteriaQuery(SubRefundReturn.class, curPageNO);
		
		// 设置查询条件
		cq.eq("userId", userId);
		
		if (AppUtils.isNotBlank(status)) {
			cq.add(cq.or(Restrictions.eq("applyState", 1), Restrictions.eq("applyState", 2)));//退款处理状态:商家处理中和后台处理中
		}
		cq.addDescOrder("applyTime");
		cq.setPageSize(pageSize);
		PageSupport<SubRefundReturn> ps = queryPage(cq);
		return ps;
	}

	@Override
	public List<SubRefundReturn> getSubRefundsSuccessBySubNumber(String subNumber) {
		return this.query("SELECT * FROM ls_sub_refund_return r WHERE r.sub_number = ? AND r.is_handle_success = 1",SubRefundReturn.class,subNumber);
	}

	@Override
	public SubRefundReturn getUnfinishedSubRefundReturn(Long subId, Long subItemId, String userId) {
		String sql = "select * from ls_sub_refund_return where sub_id = ? and sub_item_id = ? and user_id = ? and (apply_type <> '-1' and ((seller_state = '2' and apply_state <> '3') or seller_state = '3'))";
		return this.get(sql, SubRefundReturn.class, subId, subItemId, userId);
	}

	public static void main(String[] args) {
		Date endMonment = DateUtils.getEndMonment(new Date());
		System.out.println(endMonment);
	}

}
