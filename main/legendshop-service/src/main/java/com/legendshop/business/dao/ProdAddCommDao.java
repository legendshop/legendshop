/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ProdAddComm;

/**
 * 商品二次评论服务Dao
 */
public interface ProdAddCommDao extends GenericDao<ProdAddComm, Long> {
     
	public abstract ProdAddComm getProdAddComm(Long id);
	
    public abstract int deleteProdAddComm(ProdAddComm prodAddComm);
	
	public abstract Long saveProdAddComm(ProdAddComm prodAddComm);
	
	public abstract int updateProdAddComm(ProdAddComm prodAddComm);
	
	public abstract ProdAddComm getProdAddCommByCommId(Long commId);
	
 }
