package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;


/**
 * //(8x广告)
 * @author Tony
 *
 */
@Service("floorLayoutAdvEightStrategy")
public class FloorLayoutAdvEightStrategyImpl implements FloorLayoutStrategy {

	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private FloorItemDao floorItemDao;
	
	@Autowired
	private FloorDao floorDao;
	
	@Override
	public void deleteFloor(Floor floor) {
		List<FloorItem> adv = floorItemDao.queryAdvFloorItem(floor.getFlId(),FloorTemplateEnum.RIGHT_ADV.value(),FloorLayoutEnum.WIDE_ADV_EIGHT.value());
    	if(AppUtils.isNotBlank(adv)){
    		
    		for (FloorItem floorItem : adv) {
    			
    			if(AppUtils.isNotBlank(floorItem.getPicUrl())){
        			//delete  attachment
        			attachmentManager.delAttachmentByFilePath(floorItem.getPicUrl());
        			
        			//delete file image
        			attachmentManager.deleteTruely(floorItem.getPicUrl());
    			}
    			
			}
    	}
    	
    	floorItemDao.deleteFloorItem(floor.getFlId());
		
     	//最后删除自己本身
        floorDao.deleteFloor(floor);
	}

	@Override
	public FloorDto findFloorDto(Floor floor) {
		FloorDto floorDto=new FloorDto();
		floorDto.setCssStyle(floor.getCssStyle());
		floorDto.setFlId(floor.getFlId());
        floorDto.setLayout(floor.getLayout());
        floorDto.setName(floor.getName());
        floorDto.setSeq(floor.getSeq());
        List<FloorItem> advList = floorItemDao.queryAdvFloorItem(floor.getFlId(),FloorTemplateEnum.RIGHT_ADV.value(),FloorLayoutEnum.WIDE_ADV_EIGHT.value());
    	if(AppUtils.isNotBlank(advList)){
			 floorDto.addFloorItems(FloorTemplateEnum.RIGHT_ADV.value(), advList);
		}
		return floorDto;
	}
}
