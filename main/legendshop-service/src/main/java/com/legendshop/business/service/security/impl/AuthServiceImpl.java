/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.security.AuthDao;
import com.legendshop.model.security.StoreEntity;
import com.legendshop.model.security.UserEntity;
import com.legendshop.spi.service.security.AuthService;

/**
 * 用户权限管理服务类
 * 
 */
@Service("authService")
public class AuthServiceImpl implements AuthService{

	@Autowired
	private AuthDao authDao;

	@Override
	public UserEntity loadUserByUsername(String username) {
		return authDao.loadUserByUsername(username);
	}
	
	/**
	 *  根据用户名,手机号, 邮件查找用户
	 *  
	 */
	public UserEntity loadUserByUsernameOrMobileOrMail(String username) {
		return authDao.loadUserByUsernameOrMobileOrMail(username);
	}

	@Override
	public UserEntity loadUserByUsername(String username, String presentedPassword) {
		return authDao.loadUserByUsername(username,presentedPassword);
	}


	@Override
	public UserEntity loadThirdPartyUser(String username, String accessToken, String openId, String type, String source) {
		return authDao.loadThirdPartyUser(username, accessToken, openId, type, source);
	}


	@Override
	public UserEntity loadUserByMobile(String mobile) {
		return authDao.loadUserByMobile(mobile);
	}


	

	public void setAuthDao(AuthDao authDao) {
		this.authDao = authDao;
	}


	@Override
	public StoreEntity loadStoreByName(String username, String password) {
		return authDao.loadStoreByName(username, password);
	}


}
