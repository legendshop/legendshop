/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ZhiboAvRoom;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;

/**
 * The Class ZhiboInteractAvRoomDao.
 * Dao接口
 */
public interface ZhiboInteractAvRoomDao extends Dao<ZhiboInteractAvRoom, String> {


   	/**
	 *  根据Id获取
	 */
	public abstract ZhiboInteractAvRoom getZhiboInteractAvRoom(String id);
	
   /**
	 *  删除
	 */
    public abstract int deleteZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);
    
   /**
	 *  保存
	 */	
	public abstract String saveZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);

   /**
	 *  更新
	 */		
	public abstract int updateZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);

	public int createRoom(String uid);
	
	public ZhiboAvRoom getLastAvRoom(String uid);
	
	public int enterRoom(String uid,Long av_room_id,String status,Date modify_time,Long role);
	
	public int updateRoomInfoById(String uid,Long av_room_id,String title,String cover);
	
	public int saveNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord);
	
	/**
	 * 踢人
	 * @return
	 */
	public int kickingMemberByUid(String uid, Integer roomid);
 }
