/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.legendshop.model.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.base.cache.ProductIdUpdate;
import com.legendshop.base.compare.DataListComparer;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.comparer.ImgFileComparator;
import com.legendshop.business.comparer.ProdSkuComparator;
import com.legendshop.business.comparer.PropValueAliasComparer;
import com.legendshop.business.comparer.PropValueImageComparator;
import com.legendshop.business.comparer.PropertyComparator;
import com.legendshop.business.comparer.PropertyValueComparator;
import com.legendshop.business.dao.BrandDao;
import com.legendshop.business.dao.CategoryDao;
import com.legendshop.business.dao.ImgFileDao;
import com.legendshop.business.dao.MarketingDao;
import com.legendshop.business.dao.MarketingProdsDao;
import com.legendshop.business.dao.ProdPropImageDao;
import com.legendshop.business.dao.ProdUserAttributeDao;
import com.legendshop.business.dao.ProdUserParamDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ProductPropertyDao;
import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.business.dao.PropValueAliasDao;
import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.business.dao.ShopCategoryDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.business.dao.StockLogDao;
import com.legendshop.business.dao.TagItemProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.SendArrivalInform;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.constant.SupportTransportFreeEnum;
import com.legendshop.model.constant.TransportTypeEnum;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.MarketingProds;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.ProdPropImage;
import com.legendshop.model.entity.ProdUserAttribute;
import com.legendshop.model.entity.ProdUserParam;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.PropValueAlias;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.model.entity.ShopDetailView;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.StockLog;
import com.legendshop.model.entity.TagItemProd;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.processor.SendArrivalInformProcessor;
import com.legendshop.spi.service.AfterSaleService;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.TransportManagerService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;

/**
 * 产品服务.
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	private ProductDao productDao;

    @Resource(name = "productIndexSaveProcessor")
    private EventProcessor<ProductIdDto> productIndexSaveProcessor;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private ImgFileDao imgFileDao;

	@Autowired
	private ProdUserAttributeDao prodUserAttributeDao;

    @Autowired
    private CategoryDao categoryDao;

	@Autowired
	private ProdUserParamDao prodUserParamDao;

	@Autowired
	private ProductPropertyDao productPropertyDao;

	@Autowired
	private ProductPropertyValueDao productPropertyValueDao;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private SkuService skuService;

	@Autowired
	private BrandDao brandDao;

	@Autowired
	private StockLogDao stockLogDao;

	@Autowired
	private ProdPropImageDao prodPropImageDao;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private PropValueAliasDao propValueAliasDao;

	@Autowired
	private TransportManagerService transportManagerService;

	@Autowired
	private AfterSaleService afterSaleService;

	@Autowired
	private TagItemProdDao tagItemProdDao;

	@Autowired
	private BasketService basketService;

	@Autowired
	private MarketingDao marketingDao;

	@Autowired
	private MarketingProdsDao marketingProdsDao;

	@Autowired
	private SeckillActivityDao seckillActivityDao;

	@Autowired
	private SeckillProdDao seckillProdDao;

	@Resource(name="productIndexUpdateProcessor")
	private EventProcessor<ProductIdDto> productIndexUpdateProcessor;

	@Resource(name="sendArrivalInformProcessor")
	private SendArrivalInformProcessor sendArrivalInformProcessor;

    @Autowired
    private ShopCategoryDao shopCategoryDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	private DataListComparer<Sku, Sku> skuListComparer = null;

	private DataListComparer<ProductProperty, ProductProperty> propListComparer = null;

	private DataListComparer<ProductPropertyValue, ProductPropertyValue> propValueListComparer = null;

	private DataListComparer<ImgFileDto, ImgFile> imgListComparer = null;

	private DataListComparer<PropValueImgDto,ProdPropImage> valueImgListComparer = null;

	private DataListComparer<PropValueAlias,PropValueAlias> valueAliasListComparer = null;


	@Override
	public Product getProductById(Long prodId) {
		if (prodId == null) {
			return null;
		}
		return productDao.getProductById(prodId);
	}
    /**
     * 保存导入的商品
     *
     * @return
     */
    @Override
    public void saveImportProduct(String userId, String userName, Long shopId, ProdExportDTO prodExportDTO) {
        //平台分类id
        String categroyId = prodExportDTO.getCategroyId();
        Long categoryId = Long.parseLong(categroyId.trim());
        //判断是否存在这个三级平台分类id
        Boolean isExist = categoryDao.isExistThirdCategory(categoryId);
        if (!isExist) {
            log.warn("saveImportProduct() ---------平台三级分类不存在---------");
            throw new BusinessException("平台三级分类不存在");
        }
        //店铺分类
        Long shopFirstCategoryId = null;
        Long shopSecondCategoryId = null;
        Long shopThirdCategoryId = null;
        String shopCategroy = prodExportDTO.getShopCategroy();
        if (AppUtils.isNotBlank(shopCategroy)) {
            String[] split = shopCategroy.trim().split("\\|");
            if (split.length > 3) {
                log.warn("saveImportProduct() ---------店铺分类不能超过三级---------");
                throw new BusinessException("店铺分类不能超过三级");
            }
            //上一店铺分类级别;
            Long upLevel = null;
            //父类分类id
            Long upCategoryId = null;
            Date now = new Date();
            for (int i = 0; i < split.length; i++) {
                if (AppUtils.isBlank(split[i])) {
                    log.warn("saveImportProduct() ---------店铺分类格式有误---------");
                    throw new BusinessException("店铺分类格式有误");
                }
                Long shopCategoryId = null;
                String categoryName = split[i].trim();
                ShopCategory shopCategory = shopCategoryDao.getShopCategoryByName(shopId, upCategoryId, categoryName);
                if (shopCategory == null) {
                    //创建店铺分类
                    ShopCategory newShopCategory = new ShopCategory();
                    newShopCategory.setParentId(upCategoryId);
                    newShopCategory.setName(categoryName);
                    newShopCategory.setStatus(1);
                    newShopCategory.setShopId(shopId);
                    newShopCategory.setRecDate(now);
                    shopCategoryId = shopCategoryDao.save(newShopCategory);
                    upCategoryId = shopCategoryId;

                } else {
                    shopCategoryId = shopCategory.getId();
                    upCategoryId = shopCategoryId;
                }
                if (i == 0) {
                    shopFirstCategoryId = shopCategoryId;
                }
                if (i == 1) {
                    shopSecondCategoryId = shopCategoryId;
                }
                if (i == 2) {
                    shopThirdCategoryId = shopCategoryId;
                }
            }
        }
        //保存spu
        Long prodId = saveProd(prodExportDTO, shopId, userId, userName, shopFirstCategoryId, shopSecondCategoryId, shopThirdCategoryId);
        //创建商品后 ，循环建立索引数据
        productIndexSaveProcessor.process(new ProductIdDto(prodId));
        //保存sku
        saveSku(userName, prodId, prodExportDTO);
        //保存商品图片
       /* if(AppUtils.isNotBlank(prodExportDTO.getPicture())){
            String filePath = prodExportDTO.getPicture().trim();
            ImgFile imgFile = new ImgFile();
            imgFile.setFilePath(filePath);
            imgFile.setProductId(prodId);
            imgFile.setFileSize(null);
            imgFile.setFileType(filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase());
            imgFile.setProductType((short) 1);
            imgFile.setStatus(Constants.ONLINE);
            imgFile.setUpoadTime(new Date());
            imgFile.setUserName(userName);
            imgFile.setSeq(1);
            imgFile.setShopId(shopId);
            imgFileDao.save(imgFile);
        }*/
    }
    /**
     * 保存sku
     */
    private void saveSku(String userName, Long prodId, ProdExportDTO prodExportDTO) {
        Date now = new Date();
        List<SkuExportDTO> skuList = prodExportDTO.getSkuList();
        //TODO 先拿到所有sku的规格,交叉生成所有情况的sku规格情况
        Map<String, Set<String>> propMap = new LinkedHashMap<String, Set<String>>();
        //校验一个spu下面每个sku的规格个数是否相同,如果相同,则先创建所有的规格属性,属性值到数据库
        Boolean flag = checkSkusPorpCount(prodExportDTO, propMap);
        if (!flag) {
            throw new BusinessException("商品规格填写错误");
        }
        //交叉生成所有情况的sku规格情况到数据库
        List<String> propList = saveAllSkuProp(userName, prodId, prodExportDTO, propMap);
        for (SkuExportDTO skuExportDTO : skuList) {
            Sku sku = new Sku();
            sku.setProdId(prodId);
            //属性
            StringBuilder propString = new StringBuilder();
            //属性:id
            StringBuilder propIdString = new StringBuilder();
            if (AppUtils.isNotBlank(skuExportDTO.getFirstProperties()) && AppUtils.isNotBlank(skuExportDTO.getFirstPropertiesValue())) {
                String firstProperties = skuExportDTO.getFirstProperties().trim();
                String firstPropertiesValue = skuExportDTO.getFirstPropertiesValue().trim();
                //拼接属性
                concatProp(userName, prodId, propString, propIdString, firstProperties, firstPropertiesValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getSecondProperties()) && AppUtils.isNotBlank(skuExportDTO.getSecondPropertiesValue())) {
                String secondProperties = skuExportDTO.getSecondProperties().trim();
                String secondPropertiesValue = skuExportDTO.getSecondPropertiesValue().trim();
                concatProp(userName, prodId, propString, propIdString, secondProperties, secondPropertiesValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getThirdProperties()) && AppUtils.isNotBlank(skuExportDTO.getThirdPropertiesValue())) {
                String thirdProperties = skuExportDTO.getThirdProperties().trim();
                String thirdPropertiesValue = skuExportDTO.getThirdPropertiesValue().trim();
                concatProp(userName, prodId, propString, propIdString, thirdProperties, thirdPropertiesValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getFourthProperties()) && AppUtils.isNotBlank(skuExportDTO.getFourthPropertiesValue())) {
                String fourthProperties = skuExportDTO.getFourthProperties().trim();
                String fourthPropertiesValue = skuExportDTO.getFourthPropertiesValue().trim();
                concatProp(userName, prodId, propString, propIdString, fourthProperties, fourthPropertiesValue);
            }
            sku.setCnProperties(propString.toString());
            sku.setProperties(propIdString.toString());
            if (propList.contains(propIdString.toString())) {
                propList.remove(propIdString.toString());
            }
            sku.setPrice(Double.parseDouble(skuExportDTO.getSkuPrice().trim()));
            sku.setStocks(Long.parseLong(skuExportDTO.getStocks().trim()));
            sku.setActualStocks(Long.parseLong(skuExportDTO.getStocks().trim()));
            sku.setName(skuExportDTO.getSkuName());
            sku.setStatus(1);
            sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
            sku.setModifyDate(now);
            sku.setRecDate(now);
           /* if(AppUtils.isNotBlank(skuExportDTO.getWeight())){
                sku.setWeight(Double.parseDouble(skuExportDTO.getWeight().trim()));
            }*/
            sku.setPartyCode(skuExportDTO.getPartyCode());
            sku.setModelId(skuExportDTO.getModelId());
            //sku.setRemark(skuExportDTO.getRemark());
           // sku.setTenderSku(false);
            skuDao.saveSku(sku);
        }
        if (propList.size() > 0) {
            //自动补全其他sku
            for (int i = 0; i < propList.size(); i++) {
                String prop_value = propList.get(i);
                Sku sku = new Sku();
                sku.setProdId(prodId);
                sku.setProperties(prop_value);
                sku.setPrice(100D);
                sku.setStocks(0L);
                sku.setActualStocks(0L);
                int code = i + 1;
                sku.setName("自动补全单品" + code);
                sku.setStatus(0);
                sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
                sku.setModifyDate(now);
                sku.setRecDate(now);
              //  sku.setRemark("这是excel导入商品自动补全的无效的sku");
                //sku.setTenderSku(false);
                skuDao.saveSku(sku);
            }

        }
    }
    /**
     * 拼接属性字符串
     */
    private void concatProp(String userName, Long prodId, StringBuilder propString, StringBuilder propIdString
            , String prop, String propvalue) {
        Long propId = createProductProperty(userName, prodId, prop);
        Long propValueId = createProductPropertyValue(propId, propvalue);
        if (propString.length() > 0) {
            propString.append(";");
        }
        if (propIdString.length() > 0) {
            propIdString.append(";");
        }
        propString.append(prop).append(":").append(propvalue);
        propIdString.append(propId).append(":").append(propValueId);
    }

    /**
     * 创建属性
     */
    private Long createProductProperty(String userName, Long prodId, String propertiesName) {
        Date now = new Date();
        //是否存在属性名
        ProductProperty prop = productPropertyDao.getProductProperty(prodId, propertiesName);
        if (prop == null) {
            ProductProperty productProperty = new ProductProperty();
            productProperty.setPropName(propertiesName);
            productProperty.setMemo(propertiesName);
            productProperty.setIsRequired(false);
            productProperty.setIsMulti(false);
            productProperty.setStatus((short) 1);
            productProperty.setType(0);
            productProperty.setModifyDate(now);
            productProperty.setRecDate(now);
            productProperty.setIsRuleAttributes(1);
            productProperty.setIsForSearch(false);
            productProperty.setIsInputProp(true);
            productProperty.setUserName(userName);
            productProperty.setProdId(prodId);
            productProperty.setIsCustom(1);
            return productPropertyDao.save(productProperty);
        }
        return prop.getId();
    }

    /**
     * 保存spu
     */
    private Long saveProd(ProdExportDTO prodExportDTO, Long shopId, String userId, String userName, Long shopFirstCategoryId, Long shopSecondCategoryId, Long shopThirdCategoryId) {
        Product product = new Product();
        product.setVersion(1);
        product.setShopId(shopId);
        product.setName(prodExportDTO.getProdName());
        product.setUserId(userId);
        product.setUserName(userName);
        product.setCategoryId(Long.parseLong(prodExportDTO.getCategroyId().trim()));
        product.setShopFirstCatId(shopFirstCategoryId);
        product.setShopSecondCatId(shopSecondCategoryId);
        product.setShopThirdCatId(shopThirdCategoryId);
        product.setRecDate(new Date());
//        product.setPic(prodExportDTO.getPicture());
        product.setSupportDist(0);
        product.setViews(0L);
        product.setBuys(0L);
        product.setComments(0L);
        product.setProdType(ProductTypeEnum.PRODUCT.value());
        product.setModifyDate(new Date());
        List<Map<String, String>> userParameterList = new ArrayList<Map<String, String>>();
        //参数
        if (AppUtils.isNotBlank(prodExportDTO.getFirstParameter()) && AppUtils.isNotBlank(prodExportDTO.getFirstParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", prodExportDTO.getFirstParameter());
            map.put("value", prodExportDTO.getFirstParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(prodExportDTO.getSecondParameter()) && AppUtils.isNotBlank(prodExportDTO.getSecondParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", prodExportDTO.getSecondParameter());
            map.put("value", prodExportDTO.getSecondParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(prodExportDTO.getThirdParameter()) && AppUtils.isNotBlank(prodExportDTO.getThirdParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", prodExportDTO.getThirdParameter());
            map.put("value", prodExportDTO.getThirdParameterValue());
            userParameterList.add(map);
        }
        if (AppUtils.isNotBlank(prodExportDTO.getFourthParameter()) && AppUtils.isNotBlank(prodExportDTO.getFourthParameterValue())) {
            Map<String, String> map = new LinkedHashMap<>();
            map.put("key", prodExportDTO.getFourthParameter());
            map.put("value", prodExportDTO.getFourthParameterValue());
            userParameterList.add(map);
        }
        String userParameterJson = JSONUtil.getJson(userParameterList);
        product.setUserParameter(userParameterJson);
        product.setCash(Double.parseDouble(prodExportDTO.getSpuPrice()));
        product.setContent(prodExportDTO.getContent());
        product.setStatus(ProductStatusEnum.PROD_OFFLINE.value());
        product.setReviewScores(0);
        //默认商家承担运费
        product.setSupportTransportFree(1);
        product.setHasInvoice(0);
        product.setHasGuarantee(0);
        product.setStockCounting(0);
        product.setIsGroup(0);

        return productDao.saveProduct(product);
    }

	/**
	 * 更新商品详细信息
	 * 有变化才会更新数据库
	 */
	public int updateProduct(Product product, Product origin) {
		int result = 0;
		//用于组装SQL,格式是 数据库字段:值
		Map<String, Object> params = new HashMap<String, Object>();

		// 实际库存
		if (product.getStocks() != null && !product.getStocks().equals(origin.getStocks())) {
			params.put("stocks", product.getStocks());//页面上更新的是虚拟库存
			Integer originStocks = origin.getStocks() == null ? 0 : origin.getStocks();
			Integer originActualStocks = origin.getActualStocks() == null ? 0 : origin.getActualStocks();
			Integer diff = product.getStocks() - originStocks;
			Integer actualStock = originActualStocks + diff;
			if(actualStock < 0){
				actualStock = 0;
			}
			if(actualStock < product.getStocks()){
				actualStock = product.getStocks();
			}
			params.put("actual_stocks",  actualStock);//实际库存按照虚拟库存的差距来设置
		}
		theSame(product.getStocks(), origin.getStocks(),  params, "stocks");

		theSame(product.getName(), origin.getName(),  params, "name");

		theSame(product.getCategoryId(), origin.getCategoryId(),  params, "category_id");

		theSame(product.getShopFirstCatId(), origin.getShopFirstCatId(),  params, "shop_first_cat_id");

		theSame(product.getShopSecondCatId(), origin.getShopSecondCatId(),  params, "shop_second_cat_id");

		theSame(product.getShopThirdCatId(), origin.getShopThirdCatId(),  params, "shop_third_cat_id");

		theSame(product.getPrice(), origin.getPrice(),  params, "price");

		theSame(product.getCash(), origin.getCash(),  params, "cash");

		theSame(product.getCarriage(), origin.getCarriage(),  params, "carriage");

		theSame(product.getStocksArm(), origin.getStocksArm(),  params, "stocks_arm");

		theSame(product.getBrandId(), origin.getBrandId(),  params, "brand_id");

		theSame(product.getBrief(), origin.getBrief(),  params, "brief");

		theSame(product.getContent(), origin.getContent(),  params, "content");

		theSame(product.getContentM(), origin.getContentM(),  params, "content_m");

		theSame(product.getStartDate(), origin.getStartDate(),  params, "start_date");

		theSame(product.getEndDate(), origin.getEndDate(),  params, "end_date");

		theSame(product.getModelId(), origin.getModelId(),  params, "model_id");

		theSame(product.getPartyCode(), origin.getPartyCode(),  params, "party_code");

		theSame(product.getParameter(), origin.getParameter(),  params, "parameter");

		theSame(product.getUserParameter(), origin.getUserParameter(),  params, "user_parameter");

		theSame(product.getPic(), origin.getPic(),  params, "pic");

		theSame(product.getTransportId(), origin.getTransportId(),  params, "transport_id"); //TODO

		theSame(product.getHasInvoice(), origin.getHasInvoice(),  params, "has_invoice");

		theSame(product.getHasGuarantee(), origin.getHasGuarantee(),  params, "has_guarantee");

		theSame(product.getRejectPromise(), origin.getRejectPromise(),  params, "reject_promise");

		theSame(product.getServiceGuarantee(), origin.getServiceGuarantee(),  params, "service_guarantee");

		theSame(product.getAfterSaleId(), origin.getAfterSaleId(),  params, "after_sale_id");

		theSame(product.getStockCounting(), origin.getStockCounting(),  params, "stock_counting");

		theSame(product.getVolume(), origin.getVolume(),  params, "volume");

		theSame(product.getWeight(), origin.getWeight(),  params, "weight");

		theSame(product.getKeyWord(), origin.getKeyWord(),  params, "key_word");

		theSame(product.getMetaDesc(), origin.getMetaDesc(),  params, "meta_desc");

		theSame(product.getMetaTitle(), origin.getMetaTitle(),  params, "meta_title");

		theSame(product.getSupportTransportFree(), origin.getSupportTransportFree(),  params, "support_transport_free");

		theSame(product.getEmsTransFee(), origin.getEmsTransFee(),  params, "ems_trans_fee");

		theSame(product.getExpressTransFee(), origin.getExpressTransFee(),  params, "express_trans_fee");

		theSame(product.getMailTransFee(), origin.getMailTransFee(),  params, "mail_trans_fee");

		theSame(product.getTransportType(), origin.getTransportType(),  params, "transport_type");

		theSame(product.getPreStatus(), origin.getPreStatus(),  params, "pre_status");

		theSame(product.getSupportDist(), origin.getSupportDist(),  params, "support_dist");

		theSame(product.getDistCommisRate(), origin.getDistCommisRate(),  params, "dist_commis_rate");

		theSame(product.getFirstLevelRate(), origin.getFirstLevelRate(),  params, "first_level_rate");

		theSame(product.getSecondLevelRate(), origin.getSecondLevelRate(),  params, "second_level_rate");

		theSame(product.getThirdLevelRate(), origin.getThirdLevelRate(),  params, "third_level_rate");

        theSame(product.getProVideoUrl(), origin.getProVideoUrl(),  params, "pro_video");

        theSame(product.getServiceShow(),origin.getServiceShow(),params,"service_show");


        if(product.getStatus() != origin.getStatus()){//状态变更，重新统计数量
			origin.setStatus(product.getStatus());
			params.put("status", product.getStatus());
			shopDetailDao.updateShopDetailProdNum(origin.getShopId());
		}

		if(AppUtils.isNotBlank(params)){//有变化，更新商品
			// update
			Date date = new Date();
			params.put("modify_date", date);
			StringBuffer sql = new StringBuffer("update ls_prod set ");
			for (String key : params.keySet()) {
				sql.append(key).append(" = :").append(key).append(",");
			}
			sql.setLength(sql.length() -1);
			sql.append(" where prod_id = :prod_id");
			params.put("prod_id", origin.getProdId());
			//更新商品
			productDao.updateProd(origin.getProdId(), sql.toString(), params);
			result++; //更新成功
		}
		return result;

	}

	/**
	 *
	 * @param target
	 * @param source
	 * @return
	 */
	private boolean theSame(Object target, Object source, Map<String, Object> params, String name){
		if(target == null && source == null){
			return true;
		}
		if(target != null){
			if(target.equals(source)){
				return true;
			}else{
				params.put(name, target); //不相等就加入map，等待更新
				return false;
			}
		}else{
			params.put(name, target);//不相等就加入map，等待更新
			return false;
		}
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.business.service.ProductService#saveProduct(com.legendshop
	 * .model.entity.Product)
	 */
	@Override
	public Long saveProduct(Product product, String prodType) {
		Date date = new Date();
		product.setReviewScores(0);// 评分
		//product.setHasInvoice(product.getHasInvoice());// 发票, 发票迁移到店铺上
		product.setHasGuarantee(product.getHasGuarantee());//保修
		product.setStockCounting(product.getStockCounting());//库存计数方式，0：拍下减库存，1：付款减库存

		product.setBuys(0);// 已经销售数量
		product.setViews(0);// 观看人数
		product.setIsSupportCod(0); //货到付款
		product.setRecDate(date);// 记录时间
		product.setIsGroup(0); //是否团购
		product.setModifyDate(date);
		product.setComments(0); // 评论数
		product.setProdType(prodType);
		product.setSearchWeight(1D);
		if (product.getStocks() != null) {
			product.setActualStocks(product.getStocks());
		}

		if(AppUtils.isBlank(product.getStatus())){//如果商品没有为状态赋值，默认上线
			product.setStatus(ProductStatusEnum.PROD_ONLINE.value());
		}

		Long prodId = productDao.saveProduct(product);
		/*product.setProdId(prodId);*/
		shopDetailDao.updateShopDetailProdNum(product.getShopId()); //在线商品加1
		return prodId;
	}

	/**
	 * 商品图片库
	 */
	/*@Override
	public String getProductGallery(HttpServletRequest request, HttpServletResponse response, Long prodId) {
		ProductDetail prod = productDao.getProdDetail(prodId);
		if (prod != null) {
			request.setAttribute("prod", prod);
			List<String> prodPics = new ArrayList<String>();
			List<ProdPropImage>  prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
			if(AppUtils.isNotBlank(prodPropImages)){
				for (ProdPropImage prodPropImage : prodPropImages) {
					prodPics.add(prodPropImage.getUrl());
				}
			}else{
				// 查看商品的说明图片
				List<ImgFile> imgFiles = imgFileDao.getAllProductPics(prodId);
				for (ImgFile imgFile : imgFiles) {
					prodPics.add(imgFile.getFilePath());
				}
			}
			if (AppUtils.isNotBlank(prodPics)) {
				request.setAttribute("prodPics", prodPics);
			}
			return PathResolver.getPath(request, response, FrontPage.PROD_PIC_GALLERY);
		} else {
			UserMessages uem = new UserMessages();
			uem.setTitle(ResourceBundleHelper.getString("product.not.found"));
			uem.setDesc(ResourceBundleHelper.getString("product.status.check"));
			uem.setCode(ErrorCodes.ENTITY_NO_FOUND);
			request.setAttribute(UserMessages.MESSAGE_KEY, uem);
			return PathResolver.getPath(request, response, CommonPage.ERROR_FRAME_PAGE);
		}
	}*/

	/**
	 * 商品动态参数.
	 *
	 */
	@Override
	public String getProdParameter(Long prodId) {
		return productDao.getProdParameter(prodId);
	}


	/**
	 * 更新商品
	 */
	@Override
	public void updateProd(Product product) {
		boolean flag = product.getStatus().equals(2);
		if(flag){
			List<MarketingProds> prods = marketingProdsDao.getmarketingProdByProdId(product.getProdId());
			if(AppUtils.isNotBlank(prods)){
				List<Long> ids = new ArrayList<Long>();
				for (MarketingProds marketingProds : prods) {
					ids.add(marketingProds.getMarketId());
				}
				if(ids.size() > 0){
					marketingDao.updateMarketStatus(ids);
				}
			}
		};
		productDao.updateProduct(product);
		productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));

		basketService.updateIsFailureByProdId(true, product.getProdId());
	}

	/**
	 * 获取商品详情
	 */
	@Override
	public ProductDetail getProdDetail(Long prodId) {
		if (prodId == null) {
			return null;
		}
		return productDao.getProdDetail(prodId);
	}

	/**
	 * 获取多个商品详情
	 */
	@Override
	public List<ProductDetail> getProdDetail(Long [] prodId) {
		if (prodId == null) {
			return null;
		}
		return productDao.getProdDetail(prodId);
	}


	/**
	 * 更新商品查看数
	 */
	@Override
	public void updateProdViews(Long prodId) {
		productDao.updateProdViews(prodId);
	}

	/**
	 * 获取某个分类下的热门商品
	 */
	@Override
	public List<Product> getHotOn(Long shopId, Long categoryId) {
		return productDao.getHotOn(shopId, categoryId);
	}
	/**
	 * 获取全场的热门商品
	 */
	@Override
	public List<Product> getHotAll(Integer max) {
		return productDao.getHotAll(max);
	}

	/**
	 * 获取某个商家的热门商品
	 */
	@Override
	public List<Product> getHotViewProd(Long shopId, int maxNum) {
		return productDao.getHotViewProd(shopId, maxNum);
	}

	/**
	 * 获取热门评论
	 *
	 */
	@Override
	public List<Product> getHotComment(Long shopId, Long categoryId, int maxNum) {
		return productDao.getHotComment(shopId, categoryId, maxNum);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.spi.service.ProductService#getHotRecommend(java.lang.String
	 * , java.lang.Long, int)
	 */
	@Override
	public List<Product> getHotRecommend(Long shopId, Long sortId, int maxNum) {
		return productDao.getHotRecommend(shopId, sortId, maxNum);
	}

	/**
	 * 产品查看历史
	 */
	/*@Override
	public List<ProductDetail> getVisitedProd(HttpServletRequest request) {
		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ProductDetail> products = new ArrayList<ProductDetail>();
		for (Object prodId : prodIds) {
			ProductDetail productDetail = productDao.getProdDetail(Long.parseLong(prodId.toString()));

			if(productDetail!=null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
				products.add(productDetail);
			}
		}
		return products;
	}*/

	/**
	 *
	 * 得到推荐商品，跟以下因素相关 1. 商品访问历史，访问次数 2. 商品订购记录 3. 商品的收藏 暂时用商品浏览历史代替 TODO
	 */
	/*@Override
	public List<ProductDetail> getRecommendProd(Long prodId) {
		return getVisitedProd(RequestHolder.getRequest());
	}*/

	/**
	 * 删除商品
	 */
	@Override
	@ProductIdUpdate
	public String delete(Long shopId, Long prodId,boolean isAdmin) {
		try {
			String result= productDao.deleteProd(shopId, prodId, isAdmin);
			return result;
		} catch (Exception e) {
			log.error("delete product fail with id " + prodId, e);
			return Constants.FAIL;
		}
	}

	@Override
	public ProdUserAttribute getProdUserAttribute(Long id) {
		return prodUserAttributeDao.getProdUserAttribute(id);
	}

	@Override
	public void saveProdUserAttribute(ProdUserAttribute prodUserAttribute) {
		prodUserAttributeDao.saveProdUserAttribute(prodUserAttribute);
	}

	@Override
	public void deleteProdUserAttribute(Long id, Long shopId) {
		prodUserAttributeDao.deleteProdUserAttribute(id, shopId);
	}

	@Override
	public void deleteProdUserParam(Long id, Long shopId) {
		prodUserParamDao.deleteProdUserParam(id, shopId);
	}

	@Override
	public Long saveProdUserParam(ProdUserParam prodUserParam) {
		return prodUserParamDao.saveProdUserParam(prodUserParam);
	}

	@Override
	public ProdUserParam getProdUserParam(Long id) {
		return prodUserParamDao.getProdUserParam(id);
	}


	@Override
	public void updateStatus(Integer status, Long productId,String auditOpin) {
		productDao.updateStatus(status, productId, auditOpin);
		boolean flag = status.equals(2);//下线
		if(flag){
			List<MarketingProds> prods = marketingProdsDao.getmarketingProdByProdId(productId);
			if(AppUtils.isNotBlank(prods)){
				List<Long> ids = new ArrayList<Long>();
				for (MarketingProds marketingProds : prods) {
					ids.add(marketingProds.getMarketId());
				}
				if(ids.size() > 0){
					marketingDao.updateMarketStatus(ids);
				}
			}
		};
		Product product = productDao.getProduct(productId);
		if(product!=null && product.getShopId()!=null){
			shopDetailDao.updateShopDetailProdNum(product.getShopId());
		}
		//删除购物车中的商品, 现已改为失效, 而不是删除
		//basketService.deleteBasketGoodsByProdId(productId);

		//更改用户购物车的商品为失效
		basketService.updateIsFailureByProdId(true, product.getProdId());
	}

	/**
	 * 发布商品
	 */
	@Override
	public Long releaseOfProduct(Long shopId, String userId, String userName, Product product) {

		//判断分佣总比例不能超过50%
		if(product.getSupportDist() ==1){//支持分销
			Double firstLevelRate = product.getFirstLevelRate(); //第一级分销比例
			Double secondLevelRate = product.getSecondLevelRate();//第二级分销比例
			Double thirdLevelRate = product.getThirdLevelRate();//第三级分销比例
			if(firstLevelRate==null){ firstLevelRate = 0d; }
			if(secondLevelRate==null){ secondLevelRate = 0d; }
			if(thirdLevelRate==null){ thirdLevelRate = 0d; }
			Double totalRate = Arith.add(Arith.add(firstLevelRate, secondLevelRate), thirdLevelRate);
			if(totalRate > 50){//所有的分销的比例不能超过50%
				throw new BusinessException("Total percentage of commission can not be more than 50% by prodId:" + product.getProdId());
			}
			product.setDistCommisRate(totalRate);
		}

		//1. For update product， 如果商品ID不为空，更新商品
		if (AppUtils.isNotBlank(product.getProdId())) {
			Integer isChange = 0;
			Product origin = this.getProductById(product.getProdId());

			// 判断当前用户是否为商品发布者，如果不是，不能更改
			if (AppUtils.isBlank(origin)) {
				throw new BusinessException("origin product is not null by prodId " + product.getProdId());
			}

			//判断是否自己商城的商品
			if(!shopId.equals(origin.getShopId())){
				log.warn("You can not edit this product by shopId {}, product own to shopId {}", shopId, origin.getShopId());
				throw new BusinessException("Can not edit other shop product " + product.getProdId());
			}
			product.setShopId(shopId);

			// 给商品主图片赋值,并更新关联表
			if (AppUtils.isNotBlank(product.getImagesPaths())) {
				List<String> newImgUrls = JSONUtil.getArray(product.getImagesPaths(), String.class);
				product.setPic(newImgUrls.get(0));

			}
			// 验证 商品分类，商品主图片，商品详情, 放到第一位
			product.validate();
			product.setUserId(userId);
			product.setUserName(userName);

			//给商品的状态和开始时间 赋值
			setProdStatus(product);

			//违规下架 或 审核失败 的商品编辑后 状态应该是 审核中
			if(ProductStatusEnum.PROD_ILLEGAL_OFF.value().equals(origin.getStatus())
					|| ProductStatusEnum.PROD_AUDIT_FAIL.value().equals(origin.getStatus())){
				product.setPreStatus(product.getStatus());
				product.setStatus(ProductStatusEnum.PROD_AUDIT.value());
			}else{
				// 先判断商城的设置，如果商城没有设置 则判断平台的设置  看是否需要审核
				ShopDetailView shopDetailView = shopDetailService.getShopDetailView(shopId);
				if(shopDetailView.getProdRequireAudit()!=null && shopDetailView.getProdRequireAudit()==1){
					product.setPreStatus(product.getStatus());
					product.setStatus(ProductStatusEnum.PROD_AUDIT.value());
				}else if(shopDetailView.getProdRequireAudit()==null && systemParameterUtil.isProdRequireAudit()){
					product.setPreStatus(product.getStatus());
					product.setStatus(ProductStatusEnum.PROD_AUDIT.value());
				}
			}

			//处理运费信息
			handleTransport(product);

			// 更新商品
			isChange = isChange + this.updateProduct(product, origin);

			// 更新商品图片及其关联表
			isChange = isChange + updateProdImgFile(product, userName);

			// 更新用户自定义商品属性
			isChange = isChange + updateUserProperty(product, userName);

			// 更新商品sku和仓库库存
			isChange = isChange + updateProdSku(product);

			// 更新商品属性值图片
			isChange = isChange + updatePropValueImage(product);

			// 更新 用户自定义属性值名
			isChange = isChange + updatePropValueAlias(product);

			// 更新商品标签
			updateProdTags(product.getProdId(),product.getTags());

			// 删除用户应用的自定义属性
			deleteUserProperty(product.getProdId(),product.getDeleteUserProperties());

			/**
			 * 更新商品索引
			 */
			if(isChange > 0){
				//商品的版本号加1
				productDao.updateProdVersion(product.getProdId());
				//更改用户购物车的商品为失效
				basketService.updateIsFailureByProdId(true, product.getProdId());

				//更新缓存
				productDao.clearProductDto(product.getProdId());
			}

			return product.getProdId();
		} else {
			//2. For save product 保存商品
			product.setVersion(1);
			// 给商品主图片赋值
			if (AppUtils.isNotBlank(product.getImagesPaths())) {
				List<String> filePaths = JSONUtil.getArray(product.getImagesPaths(), String.class);
				product.setPic(AppUtils.isBlank(filePaths.get(0)) ? null : filePaths.get(0));
			}

			// 验证 商品分类，商品主图片
			product.validate();
			product.setShopId(shopId);
			product.setUserId(userId);
			product.setUserName(userName);

			//给商品的状态和开始时间 赋值
			setProdStatus(product);

			// 先判断商城的设置，如果商城没有设置 则判断平台的设置  看是否需要审核
			ShopDetailView shopDetailView = shopDetailService.getShopDetailView(shopId);
			if(shopDetailView.getProdRequireAudit()!=null && shopDetailView.getProdRequireAudit()==1){
				product.setPreStatus(product.getStatus());
				product.setStatus(ProductStatusEnum.PROD_AUDIT.value());
			}else if(shopDetailView.getProdRequireAudit()==null && systemParameterUtil.isProdRequireAudit()){
				product.setPreStatus(product.getStatus());
				product.setStatus(ProductStatusEnum.PROD_AUDIT.value());
			}

			//处理运费信息
			handleTransport(product);

			// 保存商品
			Long prodId = this.saveProduct(product, ProductTypeEnum.PRODUCT.value());

			// 根据商品ID，保存商品图片及其关联表
			saveProdImgFile(product.getImagesPaths(), product.getShopId(),prodId, userName);

			// 根据商品ID，保存用户自定义商品属性 userProperties
			saveUserProperties(product.getUserProperties(), prodId, userName);

			// 根据商品ID，保存商品sku
			saveProdSku(product.getSkus(), prodId);

			//保存属性值图片
			savePropValueImage(product.getValueImages(),prodId);

			//保存用户自定义属性值名
			savePropValueAlias(product.getValueAlias(),userId,prodId);

			//保存商品标签 目前界面上把商品标签给去掉,没有用 TODO
			//saveProductTags(product.getTags(),prodId);


			return prodId;
		}
	}

	/**
	 * 更新商品标签
	 * @param prodId
	 * @param tags
	 */
	private void updateProdTags(Long prodId, String tags) {
		if(AppUtils.isNotBlank(tags)){
			String [] items=tags.split(",");
			if(AppUtils.isBlank(items)){
				return;
			}
			tagItemProdDao.compareProdTags(prodId,items);
		}
	}

	/**
	 * 保存商品标签
	 * @param tags
	 * @param prodId
	 */
	private void saveProductTags(String tags, Long prodId) {
		if(AppUtils.isNotBlank(tags)){
			String [] items=tags.split(",");
			if(AppUtils.isBlank(items)){
				return;
			}
			List<TagItemProd> itemProds=new ArrayList<TagItemProd>();
			for(int i=0;i<items.length;i++){
				Long itemId=Long.valueOf(items[i]);
				TagItemProd itemProd=new TagItemProd();
				itemProd.setRecDate(new Date());
				itemProd.setTagItemId(itemId);
				itemProd.setProdId(prodId);
				itemProds.add(itemProd);
			}
			tagItemProdDao.save(itemProds);
		}
	}

	/**
	 * 处理运费信息
	 * @return
	 */
	private void handleTransport(Product product){
		if(SupportTransportFreeEnum.SELLER_SUPPORT.value().equals(product.getSupportTransportFree()) ){
			product.setSupportTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
			product.setTransportId(null);
			product.setTransportType(null);
			product.setEmsTransFee(0d);
			product.setExpressTransFee(0d);
			product.setMailTransFee(0d);
		}else if(SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(product.getSupportTransportFree())){
			if(TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(product.getTransportType())){
				product.setSupportTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
				product.setTransportType(TransportTypeEnum.TRANSPORT_TEMPLE.value());
				product.setTransportId(product.getTransportId());
				product.setEmsTransFee(0d);
				product.setExpressTransFee(0d);
				product.setMailTransFee(0d);
			}else{
				product.setSupportTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
				product.setTransportType(TransportTypeEnum.FIXED_TEMPLE.value());
				product.setTransportId(null);
				product.setEmsTransFee(product.getEmsTransFee());
				product.setExpressTransFee(product.getExpressTransFee());
				product.setMailTransFee(product.getMailTransFee());
			}
		}
	}

	/**
	 *
	 * @param target
	 * @param source
	 * @return
	 */
	private void handleTransport(Product target,Product source){
		if(SupportTransportFreeEnum.SELLER_SUPPORT.value().equals(source.getSupportTransportFree()) ){
			target.setSupportTransportFree(SupportTransportFreeEnum.SELLER_SUPPORT.value());
			target.setTransportId(null);
			target.setTransportType(null);
			target.setEmsTransFee(0d);
			target.setExpressTransFee(0d);
			target.setMailTransFee(0d);
		}else if(SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(source.getSupportTransportFree())){
			if(TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(source.getTransportType())){
				target.setSupportTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
				target.setTransportType(TransportTypeEnum.TRANSPORT_TEMPLE.value());
				target.setTransportId(source.getTransportId());
				target.setEmsTransFee(0d);
				target.setExpressTransFee(0d);
				target.setMailTransFee(0d);
			}else{
				target.setSupportTransportFree(SupportTransportFreeEnum.BUYERS_SUPPORT.value());
				target.setTransportType(TransportTypeEnum.FIXED_TEMPLE.value());
				target.setTransportId(null);
				target.setEmsTransFee(source.getEmsTransFee());
				target.setExpressTransFee(source.getExpressTransFee());
				target.setMailTransFee(source.getMailTransFee());
			}
		}
	}



	/**处理运费信息
	 * 商品编辑
	 */
	@Override
	public ProductDto getProductDto(PublishProductDto publishProductDto,Product product,Long prodId, boolean isSimilarImport) {

		ProductDto productDto = constructProductDto(product);

		if(!isSimilarImport){//如果不是商品导入则会做这个加载所有的包括自定义属性
			//取自定义的商品属性
			List<ProductPropertyDto> userPropList = productPropertyDao.queryUserProp(prodId);

			//组装出用户的自定义属性
			List<ResultCustomPropertyDto> rcpdList = constructUserPropList(userPropList);

			//变成JSON
			productDto.setUserPropList(JSONUtil.getJson(rcpdList));

			//商品Sku
			List<Sku> skuList = skuDao.getSkuByProd(prodId);

			//查取用户自定义属性值的别名
			List<PropValueAlias> valueAliasList = propValueAliasDao.queryByProdId(prodId);

			productDto.setValueAliasList(JSONUtil.getJson(valueAliasList));

			if(AppUtils.isNotBlank(publishProductDto)){
				//将自定义的商品属性 放入  全部商品属性  里面
				if(AppUtils.isNotBlank(publishProductDto.getSpecDtoList())){
					publishProductDto.getSpecDtoList().addAll(userPropList);
				}else{
					List<ProductPropertyDto> specDtoList = new ArrayList<ProductPropertyDto>();
					specDtoList.addAll(userPropList);
					publishProductDto.setSpecDtoList(specDtoList);
				}
				//找出那些属性是作为特征值，是全部属性的子集
				PropertiesAndValueIdList propIdList = constructPropertyAndValueIdList(skuList,publishProductDto.getSpecDtoList());

				//找出商品属性中 哪些属性值被选中且哪些属性值名有改变
				productDto.setProdPropDtoList(constructProdPropDtoList(propIdList, publishProductDto.getSpecDtoList(),valueAliasList));
				Map<Long, List<Long>> propValueIdMap = null;
				if(AppUtils.isNotBlank(propIdList)){
					propValueIdMap = propIdList.getPropValueIdMap();
					//sku表的属性值
					productDto.setPropNameList(constructPropNameList(publishProductDto.getSpecDtoList(),propIdList.getPropIdMap(),skuList));
				}
				//组装SkuDto
				productDto.setSkuDtoList(constructSkuDto(publishProductDto.getSpecDtoList(), propValueIdMap, skuList));

				//属性值图片处理
				productDto.setPropImageDtoList(ObtainPropImageDto(publishProductDto,prodId));
			 }
		}else{
			//如果是类似商品导入，则需要保留系统默认的商品属性
			//将自定义的商品属性 放入  全部商品属性  里面
			//找出那些属性是作为特征值，是全部属性的子集
			//找出商品属性中 哪些属性值被选中且哪些属性值名有改变
			productDto.setProdPropDtoList(constructProdPropDtoList(null, publishProductDto.getSpecDtoList(),null));
		}



		//加载商品品牌
		if(AppUtils.isNotBlank(productDto.getBrandId())){
			Brand brand  = brandDao.getBrandById(productDto.getBrandId());
			if(AppUtils.isNotBlank(brand)){
				productDto.setBrandName(brand.getBrandName());
			}
		}

		//售后说明
		if(AppUtils.isNotBlank(productDto.getAfterSaleId())){
			AfterSale afterSale =  afterSaleService.getAfterSale(productDto.getAfterSaleId());
			if(afterSale!=null && AppUtils.isNotBlank(afterSale.getTitle())){
				productDto.setAfterSaleName(afterSale.getTitle());
			}
		}


		//商品图片
		List<ImgFile> imgFileList = imgFileDao.getAllProductPics(prodId);
		productDto.setImgFileList(imgFileList);

		/*
		 * 获取商品Tag, TODO 去掉,做法变更, 已经不再使用标签

		List<TagItemProd> itemProds=tagItemProdDao.getTagItemProdsByPid(prodId);
		if(AppUtils.isNotBlank(itemProds)){
			String tag="";
			for(TagItemProd itemProd :itemProds){
				tag+=itemProd.getTagItemId()+",";
			}
			if(tag!=""){
				tag=tag.substring(0,tag.length()-1);
				productDto.setTags(tag);
			}
		}
		*/

		return productDto;
	}




	/** 更新用户的自定义属性 **/
	public int updateUserProperty(Product product, String userName) {
		int result = 0;
		if (AppUtils.isNotBlank(product.getUserProperties())) {
			List<ResultCustomPropertyDto> customPropertyDtoList = JSONUtil.getArray(product.getUserProperties(),ResultCustomPropertyDto.class);//新的自定义属性

			Map<Long, ProductProperty> originPropMap = new HashMap<Long, ProductProperty>(); //原有的自定义属性

			List<ProductProperty> originPropList = productPropertyDao.queryUserPropByProdId(product.getProdId());
			for (ProductProperty productProperty : originPropList) {
				originPropMap.put(productProperty.getPropId(), productProperty);
			}

			List<ProductProperty> newPropList = new ArrayList<ProductProperty>();
			if (AppUtils.isNotBlank(customPropertyDtoList)) {
				for (ResultCustomPropertyDto customPropertyDto : customPropertyDtoList) {
					ProductProperty property = customPropertyDto.getProductProperty();
					List<ProductPropertyValue> propertyValueList = customPropertyDto.getPropertyValueList();
					property.setValueList(propertyValueList);
					newPropList.add(property);
				}

				Map<Integer, List<ProductProperty>> propCompareResult = getPropListComparer().compare(newPropList,
						originPropList, product);

				List<ProductProperty> updatePropList = propCompareResult.get(0);
				List<ProductProperty> delPropList = propCompareResult.get(-1);
				List<ProductProperty> addPropList = propCompareResult.get(1);

				if (AppUtils.isNotBlank(delPropList)) {// 删除属性和属性值

					for (ProductProperty productProperty : delPropList) {//删除属性值
						if (AppUtils.isNotBlank(productProperty.getValueList())) {
							productPropertyValueDao.delete(productProperty.getValueList());
						}
					}
					productPropertyDao.delete(delPropList);//删除属性
					result++;
				}

				if (AppUtils.isNotBlank(addPropList)) {// 增加属性和属性值
					productPropertyDao.saveWithId(addPropList);//保存属性
					for (ProductProperty productProperty : addPropList) {
						if (AppUtils.isNotBlank(productProperty.getValueList())) {// 保存属性值
				    	   productPropertyValueDao.saveWithId(productProperty.getValueList());
						}
					}
					result++;
				}

				if (AppUtils.isNotBlank(updatePropList)) {// 对比 属性值

					for (ProductProperty newProp : updatePropList) {
						ProductProperty originProp = originPropMap.get(newProp.getPropId());

						Map<Integer, List<ProductPropertyValue>> propValueCompareResult = getPropValueListComparer()
								.compare(newProp.getValueList(), originProp.getValueList(), null);

						List<ProductPropertyValue> delPropValueList = propValueCompareResult.get(-1);
						List<ProductPropertyValue> addPropValueList = propValueCompareResult.get(1);

						if (AppUtils.isNotBlank(addPropValueList)) {// 保存属性值
							productPropertyValueDao.saveWithId(addPropValueList);
						}

						if (AppUtils.isNotBlank(delPropValueList)) {// 删除属性值
							productPropertyValueDao.delete(delPropValueList);
						}
					}

					result++;
				}

			}

		}else{
			//根据商品Id和用户名， 删除用户自定义属性 删除属性时，确保和sku的属性一致
			productPropertyDao.deleteUserPropByProdId(product.getProdId());
			result++;
		}
		if(result > 0){
			//更新缓存
			productPropertyDao.clearUserPropcache(product.getProdId());
		}

		return result;
	}

	/** 更新商品图片及其商品主图片 **/
	public int updateProdImgFile(Product product, String userName) {
		int result = 0;
		if (AppUtils.isNotBlank(product.getImagesPaths())) {
			List<String> newImgUrls = JSONUtil.getArray(product.getImagesPaths(), String.class);
			List<ImgFile> originImgsList = imgFileDao.getAllProductPics(product.getProdId());
			List<ImgFileDto> imgFileDtoList = new ArrayList<ImgFileDto>();

			int seq = 1;
			for (String imgUrl : newImgUrls) {
				ImgFileDto imgFileDto = new ImgFileDto();
				imgFileDto.setImgUrl(imgUrl);
				imgFileDto.setSeq(seq);
				imgFileDtoList.add(imgFileDto);
				seq++;
			}
			Map<Integer, List<ImgFile>> imgFileCompareResult = getImgListComparer().compare(imgFileDtoList,
					originImgsList, product);

			List<ImgFile> addImgFileList = imgFileCompareResult.get(1);
			List<ImgFile> delImgFileList = imgFileCompareResult.get(-1);

			if (AppUtils.isNotBlank(delImgFileList)) {// 删除旧图片
				imgFileDao.delete(delImgFileList);
				result++;
			}

			if (AppUtils.isNotBlank(addImgFileList)) {// 保存图片
				imgFileDao.save(addImgFileList);
				result++;
			}

			if(result > 0){
				imgFileDao.clearProductPicsCache(product.getProdId());//更新缓存
			}

		}
		return result;
	}

	/** 构建 商品属性图片 列表  **/
	public List<ProdPropImageDto> constructPropImage(List<ProductPropertyValueDto> valueDtoList,ProductPropertyDto productPropertyDto,Long prodId){

			List<ProdPropImage> propImageList = prodPropImageDao.queryPropImageList(prodId,productPropertyDto,valueDtoList);
			Map<Long,ProdPropImageDto> propImageMap = new HashMap<Long,ProdPropImageDto>();

			//if(AppUtils.isNotBlank(propImageList)){
				for (ProductPropertyValueDto productPropertyValueDto : valueDtoList) {
					if(productPropertyValueDto.getIsSelected()){
						ProdPropImageDto ppi = new ProdPropImageDto();
						ppi.setProductPropertyValueDto(productPropertyValueDto);
						propImageMap.put(productPropertyValueDto.getValueId(),ppi);
					}

					for(ProdPropImage propImage : propImageList){
						if(propImage.getValueId().equals(productPropertyValueDto.getValueId())){
							ProdPropImageDto prodPropImageDto = propImageMap.get(productPropertyValueDto.getValueId());
							if(prodPropImageDto != null){
								//prodPropImageDto = new ProdPropImageDto();
								//prodPropImageDto.setProductPropertyValueDto(productPropertyValueDto);
								prodPropImageDto.addProdPropImage(propImage);
								propImageMap.put(productPropertyValueDto.getValueId(), prodPropImageDto);
							}
						}
					}
				}
			//}

			return new ArrayList<ProdPropImageDto>(propImageMap.values());


	}

	/** 获取商品属性图片 **/
	public List<ProdPropImageDto> ObtainPropImageDto(PublishProductDto publishProductDto,Long prodId){
		List<ProductPropertyDto> specDtoList = publishProductDto.getSpecDtoList();
        List<ProdPropImageDto> propImageDtoList = new ArrayList<>();
        for (ProductPropertyDto productPropertyDto : specDtoList) {
			int type = productPropertyDto.getType();
			List<ProductPropertyValueDto> valueDtoList =  productPropertyDto.getProductPropertyValueList();
			if(type == 1 && AppUtils.isNotBlank(valueDtoList)){
				//构建商品属性图片 列表
                propImageDtoList.addAll(constructPropImage(valueDtoList,productPropertyDto,prodId));
			}
		}
        return propImageDtoList;
    }

	/** 保存商品属性值图片 **/
	public void savePropValueImage(String valueImages,Long prodId){
		if(AppUtils.isNotBlank(valueImages)){

			List<PropValueImgDto> propValueImgDtoList = JSONUtil.getArray(valueImages,PropValueImgDto.class);

			List<ProdPropImage> prodPropImageList = new ArrayList<ProdPropImage>();
			int seq;
			for (PropValueImgDto propValueImgDto : propValueImgDtoList) {
				seq = 1;
				for(String filePath : propValueImgDto.getImgList()){
					ProdPropImage prodPropImage = new ProdPropImage();
					prodPropImage.setProdId(prodId);
					prodPropImage.setCreateDate(new Date());
					prodPropImage.setPropId(propValueImgDto.getPropId());
					prodPropImage.setSeq((long)seq);
					prodPropImage.setUrl(filePath);
					prodPropImage.setValueId(propValueImgDto.getValueId());
					prodPropImage.setValueName(propValueImgDto.getValueName());

					prodPropImageList.add(prodPropImage);
					seq ++;
				}
			}

			if(AppUtils.isNotBlank(prodPropImageList)){
				prodPropImageDao.save(prodPropImageList);
			}
		}
	}

	/** 更新商品商品属性值图片  **/
	public int updatePropValueImage(Product product){
		int result = 0;
		if(AppUtils.isNotBlank(product.getValueImages())){
			List<ProdPropImage> propImageList = prodPropImageDao.getProdPropImageByProdId(product.getProdId());
			List<PropValueImgDto> propValueImgDtoList = JSONUtil.getArray(product.getValueImages(),PropValueImgDto.class);
			List<PropValueImgDto> imgDtoList = new ArrayList<PropValueImgDto>();

			for (PropValueImgDto propValueImgDto : propValueImgDtoList) {//将 propValueImgDtoList 拆分开
				int seq = 1;
				for(String filePath : propValueImgDto.getImgList()){
					PropValueImgDto imgDto = new PropValueImgDto();
					imgDto.setPropId(propValueImgDto.getPropId());
					imgDto.setSeq((long)seq);
					imgDto.setValueId(propValueImgDto.getValueId());
					imgDto.setValueImage(filePath);
					imgDto.setValueName(propValueImgDto.getValueName());
					imgDtoList.add(imgDto);
					seq++;
				}
			}

			Map<Integer, List<ProdPropImage>> valueImgCompareResult = getValueImgListComparer().compare(imgDtoList,
					propImageList, product);

			List<ProdPropImage> addValueImgList = valueImgCompareResult.get(1);
			List<ProdPropImage> delValueImgList = valueImgCompareResult.get(-1);

			if(AppUtils.isNotBlank(addValueImgList)){
				prodPropImageDao.save(addValueImgList);
				result++;
			}

			if(AppUtils.isNotBlank(delValueImgList)){
				prodPropImageDao.delete(delValueImgList);
				result++;
			}

			if(result > 0){
				prodPropImageDao.clearProdPropImageCache(product.getProdId()); //更新缓存
			}
		}else{
			prodPropImageDao.deleteByProdId(product.getId());
		}
		return result;
	}

	/****
	 * 将 用户自定义属性 dtoList 转成 ResultCustomPropertyDto
	 * @param userPropList
	 * @return
	 */
	public List<ResultCustomPropertyDto> constructUserPropList(List<ProductPropertyDto> userPropList){
		if(AppUtils.isNotBlank(userPropList)){
			List<ResultCustomPropertyDto> rcpdList = new ArrayList<ResultCustomPropertyDto>();
			for (ProductPropertyDto ppd : userPropList) {
				ResultCustomPropertyDto rcpd = new ResultCustomPropertyDto();
				ProductProperty prop = new ProductProperty();

				prop.setPropName(ppd.getPropName());
				prop.setIsRequired(ppd.isRequired());
				prop.setIsMulti(ppd.isMulti());
				prop.setSequence(ppd.getSequence());
				prop.setType((int)ppd.getType());
				prop.setIsRuleAttributes(ppd.getIsRuleAttributes());
				prop.setIsForSearch(ppd.isForSearch());
				prop.setIsInputProp(ppd.isInputProp());

				prop.setModifyDate(new Date());
				prop.setRecDate(new Date());
				prop.setStatus((short) 1);

				Long propId =ppd.getPropId();
				prop.setPropId(propId);

				rcpd.setProductProperty(prop);
				if(AppUtils.isNotBlank(ppd.getProductPropertyValueList())){
					List<ProductPropertyValue> propertyValueList = new ArrayList<ProductPropertyValue>();
					for (ProductPropertyValueDto ppvd : ppd.getProductPropertyValueList()) {
						ProductPropertyValue propValue = new ProductPropertyValue();

						propValue.setPropId(propId);
						propValue.setName(ppvd.getName());
						propValue.setPic(ppvd.getPic());

						propValue.setStatus((short) 1);
						propValue.setModifyDate(new Date());
						propValue.setRecDate(new Date());

//						Long propValueId = productPropertyValueDao.saveCustomPropValue(propValue);
//						propValue.setValueId(propValueId);
						propValue.setValueId(ppvd.getValueId());

						propertyValueList.add(propValue);
					}
					rcpd.setPropertyValueList(propertyValueList);

				}



				rcpdList.add(rcpd);
			}
			return rcpdList;
		}
		return null;
	}

	public DataListComparer<Sku, Sku> getDataListComparer() {
		if (skuListComparer == null) {
			skuListComparer = new DataListComparer<Sku, Sku>(new ProdSkuComparator());
		}
		return skuListComparer;
	}

	public DataListComparer<ProductProperty, ProductProperty> getPropListComparer() {
		if (propListComparer == null) {
			propListComparer = new DataListComparer<ProductProperty, ProductProperty>(new PropertyComparator());
		}
		return propListComparer;
	}

	public DataListComparer<ProductPropertyValue, ProductPropertyValue> getPropValueListComparer() {
		if (propValueListComparer == null) {
			propValueListComparer = new DataListComparer<ProductPropertyValue, ProductPropertyValue>(
					new PropertyValueComparator());
		}
		return propValueListComparer;
	}

	public DataListComparer<ImgFileDto, ImgFile> getImgListComparer() {
		if (imgListComparer == null) {
			imgListComparer = new DataListComparer<ImgFileDto, ImgFile>(new ImgFileComparator());
		}
		return imgListComparer;
	}

	public DataListComparer<PropValueImgDto,ProdPropImage> getValueImgListComparer(){
		if(valueImgListComparer == null){
			valueImgListComparer = new DataListComparer<PropValueImgDto,ProdPropImage>(new PropValueImageComparator());
		}
		return valueImgListComparer;
	}

	public DataListComparer<PropValueAlias,PropValueAlias> getValueAliasListComparer(){
		if(valueAliasListComparer == null){
			valueAliasListComparer = new DataListComparer<PropValueAlias,PropValueAlias>(new PropValueAliasComparer());
		}
		return valueAliasListComparer;
	}

	@Override
    @Cacheable(value = "ProductDto", key = "#prodId")
    public ProductDto getProductDto(Long prodId){
		ProductDto prodDto = new ProductDto();
		//获取商品基本信息
		ProductDetail prodDetail = getProdDetail(prodId);

        if(prodDetail!=null){
			//拷贝字段
			copyProdField(prodDto,prodDetail);
		}else{
			return null;
		}
        //prodDto.setProVideoUrl(prodDetail);
		//获取商品sku信息
		List<SkuDto> skuDtoList = new ArrayList<SkuDto>();
		List<Sku> skuList = skuService.getSkuByProd(prodId);
		if(AppUtils.isNotBlank(skuList)){
			for (Sku sku : skuList) {
				SkuDto skuDto = new SkuDto();
				skuDto.setSkuId(sku.getSkuId());
				skuDto.setName(processName(sku.getName()));
				skuDto.setPrice(sku.getPrice());
				skuDto.setCash(sku.getPrice());
				skuDto.setStocks(sku.getStocks());  //add sku数量
				skuDto.setProperties(sku.getProperties());
				skuDto.setStatus(sku.getStatus());
				skuDto.setWeight(sku.getWeight());
				skuDto.setVolume(sku.getVolume());
				skuDto.setSkuType(sku.getSkuType());
				skuDtoList.add(skuDto);
			}
			prodDto.setSkuDtoList(skuDtoList);
			prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));
		}

		//获取属性图片
		List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
		Map<Long,List<String>> valueImagesMap = new HashMap<Long,List<String>>();
		if(AppUtils.isNotBlank(skuList)){
			//获取到商品  所有的属性图片
			List<ProdPropImage>  prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
			if(AppUtils.isNotBlank(prodPropImages)){
				//根据属性值 进行分组,封装成map
				for (ProdPropImage prodPropImage : prodPropImages) {
					Long valueId = prodPropImage.getValueId();
					List<String> imgs = valueImagesMap.get(valueId);
					if(AppUtils.isBlank(imgs)){
						imgs = new ArrayList<String>();
					}
					imgs.add(prodPropImage.getUrl());
					valueImagesMap.put(valueId, imgs);
				}
				//将map转换成 DTO LIST
				Iterator<Long> it = valueImagesMap.keySet().iterator();
				while(it.hasNext()){
					Long valId = it.next();
					List<String> imgs = valueImagesMap.get(valId);
					PropValueImgDto propValueImgDto = new PropValueImgDto();
					propValueImgDto.setValueId(valId);
					propValueImgDto.setImgList(imgs);
					propValueImgList.add(propValueImgDto);
				}
			}
			prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));
		}

		//获取商品属性和属性值
		List<ProductPropertyDto> prodPropDtoList = skuService.getPropValListByProd(prodDto,skuList,valueImagesMap);
		prodDto.setProdPropDtoList(prodPropDtoList);
		prodDto.setPropValueImgList(propValueImgList);


		//商品图片列表(没有属性图片时，展示商品图片)
		List<ProdPicDto> prodPics = new ArrayList<ProdPicDto>();
		if(AppUtils.isBlank(propValueImgList)){
			List<ImgFile> imgFileList = imgFileDao.getAllProductPics(prodId);
			if(AppUtils.isNotBlank(imgFileList)){
				for (ImgFile imgFile : imgFileList) {
					ProdPicDto prodPicDto = new ProdPicDto();
					prodPicDto.setFilePath(imgFile.getFilePath());
					prodPics.add(prodPicDto);
				}
			}
		}else{
			ProdPicDto prodPicDto = new ProdPicDto();
			prodPicDto.setFilePath(prodDto.getPic());
			prodPics.add(prodPicDto);
		}
		prodDto.setProdPics(prodPics);
		return prodDto;
	}

	@Override
	public List<ProdParamDto> getPropGroupByProdId(Long prodId){
		return productDao.getPropGroupByProdId(prodId);
	}

	@Override
	public List<ParamGroupDto> getParamGroups(Long prodId) {
		ProductDetail prod = getProdDetail(prodId);
		if(prod == null){
			return null;
		}
		String parameter = prod.getParameter();//参数属性
		String userParameter = prod.getUserParameter();//自定义 参数

		List<ParamGroupDto> paramGroups = this.buildParamGroups(prodId, parameter, userParameter);
		return paramGroups;
	}

	@Override
	public List<ParamGroupDto> buildParamGroups(Long prodId, String parameter, String userParameter) {
		List<ParamGroupDto> paramGroups = new ArrayList<ParamGroupDto>();
		if (AppUtils.isNotBlank(parameter)) {

			//解析参数 json字符串,得到 参数id,参数名,参数值
			List<ProdParamDto> params =  JSONUtil.getArray(parameter, ProdParamDto.class);
			Map<Long,ProdParamDto> paramMap = new HashMap<Long,ProdParamDto>();
			for (int i = 0; i < params.size(); i++) {
				ProdParamDto paramDto = params.get(i);
				paramMap.put(paramDto.getParamId(), paramDto);
			}

			//查出  参数id 对应的 组名 (有序)
			List<ProdParamDto> paramList = getPropGroupByProdId(prodId);

			//对参数进行分组,放进 map
			Map<String,List<ProdParamDto>> groupMap = new HashMap<String,List<ProdParamDto>>();
			List<String> groupNames = new ArrayList<String>();
			for (int i = 0; i < paramList.size(); i++) {
				ProdParamDto paramDto = paramList.get(i);
				//如果没有分组，则分到基本参数
				if(AppUtils.isBlank(paramDto.getGroupName())){
					paramDto.setGroupName("基本参数");
				}
				ProdParamDto oriParamDto = paramMap.get(paramDto.getPropId());
				if(AppUtils.isNotBlank(oriParamDto)&& AppUtils.isNotBlank(oriParamDto.getParamValueName())){
					paramDto.setParamName(oriParamDto.getParamName());
					paramDto.setParamValueName(oriParamDto.getParamValueName());


					List<ProdParamDto> groupParams = groupMap.get(paramDto.getGroupName());
					if(AppUtils.isBlank(groupParams)){
						groupNames.add(paramDto.getGroupName());
						groupParams = new ArrayList<ProdParamDto>();
					}
					groupParams.add(paramDto);
					groupMap.put(paramDto.getGroupName(), groupParams);
				}
			}

			//将map转成list
			for(int i=0;i<groupNames.size();i++){
				ParamGroupDto paramGroupDto = new ParamGroupDto();
				paramGroupDto.setGroupName(groupNames.get(i));
				paramGroupDto.setParams(groupMap.get(groupNames.get(i)));
				paramGroups.add(paramGroupDto);
			}


		}
		//解析自定义参数 json字符串, 加入到其他分组
		if (AppUtils.isNotBlank(userParameter)) {
			List<KeyValueDto> kvs = JSONUtil.getArray(userParameter, KeyValueDto.class);
			if(AppUtils.isNotBlank(kvs)){
				List<ProdParamDto> userParams = new ArrayList<ProdParamDto>();
				for (KeyValueDto keyValueDto : kvs) {
					ProdParamDto prodParamDto = new ProdParamDto();
					prodParamDto.setParamName(keyValueDto.getKey());
					prodParamDto.setParamValueName(keyValueDto.getValue());
					userParams.add(prodParamDto);
				}
				ParamGroupDto paramGroupDto = new ParamGroupDto();
				paramGroupDto.setGroupName("其他参数");
				paramGroupDto.setParams(userParams);
				paramGroups.add(paramGroupDto);
			}
		}
		return paramGroups;
	}

	@Override
	public List<Long> getProdductIdByShopId(Long shopId) {
		return productDao.getProdductIdByShopId(shopId);
	}

	@Override
	public List<Product> findProdByPropId(Long propId) {
		List<Product> productListOrd = productDao.findProdByPropId(propId);
		List<Product> productListNew = new ArrayList<>();
		if (AppUtils.isNotBlank(productListOrd)) {
			for (Product product : productListOrd) {
				String params = product.getParameter();
				if (params!=""&&params!=null){
					List<ProdParamDto> prodParamDtoList = JSON.parseArray(params,ProdParamDto.class);
					for (ProdParamDto param : prodParamDtoList) {
						if (propId.equals(param.getParamId())) {
							productListNew.add(product);
							break;
						}
					}
				}
			}
		}
		return productListNew;
	}

	@Override
	public List<GroupSelProdDto> queryGroupProductByGroupId(Long id) {
		return productDao.queryGroupProductByGroupId(id);
	}

	/** 给商品的状态和开始时间 赋值  **/
	public void setProdStatus(Product product){
		//设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库
		if(AppUtils.isNotBlank(product.getPublishStatus())){
			int status = product.getPublishStatus();
			if(status == 2){ //有设定开始时间，设置为上线状态
				status = ProductStatusEnum.PROD_ONLINE.value();
				try {//开始时间设置
					if(AppUtils.isNotBlank(product.getSetUpTime())){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						product.setStartDate(sdf.parse(product.getSetUpTime()));
					}else{
						product.setStartDate(null);
					}
				} catch (ParseException e) {
					product.setStartDate(null);
					log.error("product startDate format exception:{} with prodId:{}",e,product.getProdId());
				}
			}else{
				product.setStartDate(null);
			}
			product.setStatus(status);
		}else{//默认商品上线
			product.setStatus(ProductStatusEnum.PROD_ONLINE.value());
		}
	}

	@Override
	public List<Product> getNewProds(int mount) {
		return productDao.queryLimit("select * from ls_prod where status=1 order by prod_id desc", Product.class, 0, mount);
	}

	@Override
	public List<Product> getRencFavorProd(Integer amount,String userId) {
		return productDao.queryLimit("select lp.* from ls_prod lp, ls_favorite lf where lp.prod_id=lf.prod_id and lp.status=1 and lf.user_id=? order by lf.addtime desc", Product.class, 0, amount,userId);
	}

	/**
	 * 计算运费
	 */
	@Override
	public List<TransfeeDto> getProdFreight(Long shopId,Long transportId, Integer cityId ,double totalCount,
	 double totalWeight,
	 double totalvolume) {
		List<TransfeeDto> transfeeDtos=transportManagerService.getTransfeeDtos(shopId, transportId, cityId, totalCount, totalWeight, totalvolume);
		return transfeeDtos;
		
	}

	@Override
	public List<Product> getHotsaleProdByCategoryId(Long categoryId, int i) {
		return productDao.getHotsaleProdByCategoryId(categoryId,i);
	}

	@Override
	@Cacheable(value = "SalesRankDtotList")
	public List<SalesRankDto> getSalesRank(Long shopId) {
		Date date = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(date, Integer.parseInt(dashboardDate));
		
		SimpleDateFormat myFormat=new SimpleDateFormat("yyyy-MM-dd"); 
		String temp = myFormat.format(startDate);
		
		Date result = null;
		try {
			result = myFormat.parse(temp);
		} catch (ParseException e) {
			result = startDate;
		}

		return productDao.getSalesRank(shopId,result);
	}

	@Override
	@Cacheable(value = "SellerDashboard")
	public List<KeyValueEntity> getProdCounts(Long shopId) {
		return productDao.getProdCounts(shopId);
	}

	@Override
	public void updateProdDistCommis(Product product) {
		productDao.updateProdDistCommis(product);
	}

	@Override
	public AuctionsDto getAuctionProd(Long prodId, Long skuId) {
		return productDao.getAuctionProd(prodId,skuId);
		
	}

	@Override
	public int checkStock(Long prodId, Long skuId) {
		return productDao.checkStock(prodId,skuId);
	}

	/**
	 * 拷贝部分字段值
	 * @param prodDto
	 * @param prodDetail
	 */
	private void copyProdField(ProductDto prodDto, ProductDetail prodDetail) {
		prodDto.setProdId(prodDetail.getProdId());
		prodDto.setName(processName(prodDetail.getName()));
		prodDto.setPrice(prodDetail.getPrice());
		prodDto.setCash(prodDetail.getCash());
		prodDto.setPic(prodDetail.getPic());
		prodDto.setKeyWord(prodDetail.getKeyWord());
		prodDto.setMetaTitle(prodDetail.getMetaTitle());
		prodDto.setMetaDesc(prodDetail.getMetaDesc());
		prodDto.setBrief(prodDetail.getBrief());
		prodDto.setModelId(prodDetail.getModelId());
		prodDto.setStatus(prodDetail.getStatus());
		prodDto.setContent(prodDetail.getContent());
		prodDto.setContentM(prodDetail.getContentM());
		prodDto.setUserName(prodDetail.getUserName());
		prodDto.setShopId(prodDetail.getShopId());
		prodDto.setWeight(prodDetail.getWeight());
		prodDto.setShopId(prodDetail.getShopId());
		prodDto.setVolume(prodDetail.getVolume());
		prodDto.setCategoryId(prodDetail.getCategoryId());
		prodDto.setShopFirstCatId(prodDetail.getShopFirstCatId());
		prodDto.setShopSecondCatId(prodDetail.getShopSecondCatId());
		prodDto.setShopThirdCatId(prodDetail.getShopThirdCatId());
		prodDto.setTransportId(prodDetail.getTransportId());
		prodDto.setSupportTransportFree(prodDetail.getSupportTransportFree());
		prodDto.setTransportType(prodDetail.getTransportType());
		prodDto.setEmsTransFee(prodDetail.getEmsTransFee());
		prodDto.setExpressTransFee(prodDetail.getExpressTransFee());
		prodDto.setMailTransFee(prodDetail.getMailTransFee());
		prodDto.setShopId(prodDetail.getShopId());
		prodDto.setBrandName(prodDetail.getBrandName());
		prodDto.setBrandId(prodDetail.getBrandId());
		prodDto.setAfterSaleId(prodDetail.getAfterSaleId());
		prodDto.setStartDate(prodDetail.getStartDate());
		prodDto.setEndDate(prodDetail.getEndDate());
		prodDto.setSupportDist(prodDetail.getSupportDist());
		prodDto.setDistCommisRate(prodDetail.getDistCommisRate());
		prodDto.setProdType(prodDetail.getProdType());
		prodDto.setActiveId(prodDetail.getAcitveId());
		prodDto.setIsGroup(prodDetail.getIsGroup());
        prodDto.setProVideoUrl(prodDetail.getProVideoUrl());
	}
	
	private String processName(String name){
		if(name==null){
			return null;
		}
		name = name.replace("\"", "〞");
		name = name.replace("'", "’");
		return name;
	}
	

	private List<Long> constructSpecDtoValIds(List<ProductPropertyDto> specDtoList) {
		List<Long> valueIds = new ArrayList<Long>();
		for (ProductPropertyDto productPropertyDto : specDtoList) {
			List<ProductPropertyValueDto> ppvds = productPropertyDto.getProductPropertyValueList();
			for (ProductPropertyValueDto ppvd : ppvds) {
				valueIds.add(ppvd.getValueId());
			}
		}
		return valueIds;
	}

	private List<Long> constructSpecDtoPropIds(List<ProductPropertyDto> specDtoList) {
		List<Long> propIds = new ArrayList<Long>();
		for (ProductPropertyDto productPropertyDto : specDtoList) {
			propIds.add(productPropertyDto.getPropId());
		}
		return propIds;
	}
	

	/** 删除用户应用的自定义属性  **/
	private void deleteUserProperty(Long prodId,String deleteUserProperties){
		if(AppUtils.isNotBlank(deleteUserProperties) && AppUtils.isNotBlank(prodId)){
			List<Long> propIds = JSONUtil.getArray(deleteUserProperties, Long.class);
			if(AppUtils.isNotBlank(propIds)){
				
				for (Long propId : propIds) {
					ProductProperty property = productPropertyDao.getProductProperty(prodId,propId);
					if(AppUtils.isNotBlank(property)){
						List<ProductPropertyValue> propValueList = productPropertyValueDao.queryProductPropertyValue(property.getPropId());
						if(AppUtils.isNotBlank(propValueList)){
							productPropertyValueDao.deleteProductPropertyValue(propValueList);
						}
						productPropertyDao.deleteProductProperty(property);
					}
				}
				
			}
		}
	}
	

	/** 根据商品ID，保存商品图片及其关联表 **/
	private void saveProdImgFile(String imagesPaths,Long shopId, Long prodId, String userName) {

		if (AppUtils.isNotBlank(imagesPaths)) {
			List<String> imgUrls = JSONUtil.getArray(imagesPaths, String.class);

			for (int i = 0; i < imgUrls.size(); i++) {
				String filePath = imgUrls.get(i);
				ImgFile imgFile = new ImgFile();
				imgFile.setFilePath(filePath);
				imgFile.setProductId(prodId);
				imgFile.setFileSize(null);
				imgFile.setFileType(filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase());
				imgFile.setProductType((short) 1);
				imgFile.setStatus(Constants.ONLINE);
				imgFile.setUpoadTime(new Date());
				imgFile.setUserName(userName);
				imgFile.setSeq(i + 1);
				imgFile.setShopId(shopId);
				imgFileDao.save(imgFile);
			}

		} else {
			throw new BusinessException("imgUrls is not null");
		}
	}
	

	/**
	 * 根据sku valueId 转化为属性值名
	 * @param propertyDtoList
	 * @param propList
	 * @return
	 */
	private List<KeyValueEntity> constructPropertiesNameList(List<ProductPropertyDto> propertyDtoList, List<Long> propList) {
		if(AppUtils.isBlank(propList)){
			return null;
		}
		
		List<KeyValueEntity> result = new ArrayList<KeyValueEntity>();
		
		String propValueName = "";
		for (Long propValueId : propList) {
			propValueName = parsePropValueName(propertyDtoList, propValueId);
			if(AppUtils.isNotBlank(propValueName)){
				KeyValueEntity entity = new KeyValueEntity();
				entity.setKey(String.valueOf(propValueId));
				entity.setValue(propValueName);
				result.add(entity);
			}
		}
		
		return result;
	}
	
	/**
	 * 构造sku表（sku 表 内容）
	 * @param propertyDtoList
	 * @param propValueIdMap
	 * @param skuList
	 * @return
	 */
	private List<SkuDto> constructSkuDto(List<ProductPropertyDto> propertyDtoList, Map<Long, List<Long>> propValueIdMap, List<Sku> skuList) {
		if (AppUtils.isBlank(skuList)) {
			return null;
		}
		List<SkuDto> skuDtoList = new ArrayList<SkuDto>();
		for (Sku sku : skuList) {
			SkuDto dto = new SkuDto();
			dto.setName(sku.getName());
			dto.setPrice(sku.getPrice());
			dto.setSkuId(sku.getSkuId());
			dto.setProperties(sku.getProperties());
			dto.setStatus(sku.getStatus());
			dto.setStocks(sku.getStocks());
			dto.setModelId(sku.getModelId());
			dto.setVolume(sku.getVolume());
			dto.setWeight(sku.getWeight());
			dto.setPartyCode(sku.getPartyCode());
			
			if(AppUtils.isNotBlank(sku.getProperties())){
				if(propValueIdMap!=null){
					List<Long> propList = propValueIdMap.get(sku.getSkuId()); //sku的属性值ID列表
					if(AppUtils.isNotBlank(propList)){
						String PropValueIds = "";
						for(Long valueId : propList){
							PropValueIds += valueId+ "_";
						}
						
						PropValueIds = PropValueIds.substring(0,PropValueIds.lastIndexOf("_"));
						dto.setPropValueIds(PropValueIds);//保存List<Long>
					}
					dto.setPropertiesNameList(constructPropertiesNameList(propertyDtoList, propList)); //转化为属性值名字
				}
			}
			skuDtoList.add(dto);
		}
		return skuDtoList;
	}
	
	/**
	 * 选中那些属性是用来构造sku
	 * 
	 * @param propIdList
	 * @param propertyDtoList
	 * @param valueAliasList 值的别名
	 * @return
	 */
	private List<ProductPropertyDto> constructProdPropDtoList(PropertiesAndValueIdList propIdList,List<ProductPropertyDto> propertyDtoList,List<PropValueAlias> valueAliasList) {
		if (AppUtils.isBlank(propertyDtoList)) {
			return propertyDtoList;
		}
		
		Map<Long,String> aliasMap = new HashMap<Long,String>();
		if(valueAliasList != null){
			for(PropValueAlias alias : valueAliasList){
				aliasMap.put(alias.getValueId(), alias.getAlias());
			}
		}
		for (ProductPropertyDto productPropertyDto : propertyDtoList) {
			
			List<ProductPropertyValueDto> valueDtoList = productPropertyDto.getProductPropertyValueList();
			
			if (AppUtils.isNotBlank(valueDtoList)) {
				
				for (ProductPropertyValueDto productPropertyValueDto : valueDtoList) {
					
					//属性值 是否被选中
					if (AppUtils.isNotBlank(propIdList) && AppUtils.isNotBlank(propIdList.getPropValueIdList())) {
						Set<Long> idList = propIdList.getPropValueIdList();
						if (idList.contains(productPropertyValueDto.getValueId())) {
							productPropertyValueDto.setIsSelected(true);
						}
					}
					
					//属性值别名
					String valueAlias = aliasMap.get(productPropertyValueDto.getValueId());
					String propValue =AppUtils.isNotBlank(valueAlias)?valueAlias:productPropertyValueDto.getName();
					productPropertyValueDto.setName(propValue);
				}
			}
		}
		return propertyDtoList;
	}

	/**
	 * 组装属性ID和属性值ID
	 * 
	 * @param skuList     商家
	 * @param specDtoList 商品规格
	 * @return
	 */
	private PropertiesAndValueIdList constructPropertyAndValueIdList(List<Sku> skuList,List<ProductPropertyDto> specDtoList) {
		if (AppUtils.isBlank(skuList)||AppUtils.isBlank(specDtoList)) {
			return null;
		}
		PropertiesAndValueIdList result = new PropertiesAndValueIdList();
		for (Sku sku : skuList) {
			result.addPropIdList(sku.getSkuId(),constructPropId(sku));
			result.addPropValueIdList(sku.getSkuId(), constructValuePropId(sku));
		}
		//3054,208,3094
		
		//检查类型下 的属性和属性值  是否包含SKU的属性和属性值，否则返回NULL
		List<Long> propIds = constructSpecDtoPropIds(specDtoList);
		if(!propIds.containsAll(result.getPropIdList())){
			return null;
		}else{
			List<Long> valueIds = constructSpecDtoValIds(specDtoList);
			if(!valueIds.containsAll(result.getPropValueIdList())){
				return null;
			}
		}
		return result;
	}

	/**
	 * 切割sku Properties 收集 属性值ID
	 * @param sku
	 * @return
	 */
	private List<Long> constructValuePropId(Sku sku) {
		List<Long> propValueIdList = new ArrayList<Long>();
		
		String skuProperties = sku.getProperties();
		if(AppUtils.isNotBlank(skuProperties)){
			String[] properties = skuProperties.split(";");
			for (int i = 0; i < properties.length; i++) {
				String propValueId = properties[i].split(":")[1];
				if(AppUtils.isNotBlank(propValueId)){
					propValueIdList.add(Long.parseLong(propValueId));
				}
			}
		}

		return propValueIdList;
	}

	/**
	 * 切割sku Properties 收集 属性ID
	 * @param sku
	 * @return
	 */
	private List<Long> constructPropId(Sku sku) {
		List<Long> propIdList = new ArrayList<Long>();
		if(AppUtils.isNotBlank(sku.getProperties())){
			String[] properties = sku.getProperties().split(";");
			for (int i = 0; i < properties.length; i++) {
				String propId = properties[i].split(":")[0];
				if(AppUtils.isNotBlank(propId)){
					propIdList.add(Long.parseLong(propId));
				}
			}
		}
		return propIdList;
	}
	
	/**
	 * 构建属性名列表（sku 表头使用）
	 * @param propertyDtoList
	 * @param propIdMap
	 * @param skuList
	 * @return
	 */
	private List<String> constructPropNameList(List<ProductPropertyDto> propertyDtoList,Map<Long, List<Long>> propIdMap, List<Sku> skuList){
		if (AppUtils.isBlank(skuList)) {
			return null;
		}
		
		List<String> propNameList = new ArrayList<String>();
		List<Long> propIdList =	propIdMap.get(skuList.get(0).getSkuId());
		for (int i = 0; i < propIdList.size(); i++) {
			
			for(ProductPropertyDto prodProp : propertyDtoList){
				if(propIdList.get(i).equals(prodProp.getPropId())){
					propNameList.add(prodProp.getPropName());
				}
			}
		}
		return propNameList;
	}

	


	
	/**
	 * 根据sku valueId 从propertyDtoList 找出对应的 productPropertyValueDto，取得属性值名
	 * @param propertyDtoList
	 * @param propValueId
	 * @return
	 */
	private String parsePropValueName(List<ProductPropertyDto> propertyDtoList, Long propValueId){
		for (ProductPropertyDto productPropertyDto : propertyDtoList) {
			List<ProductPropertyValueDto> valueDtoList = productPropertyDto.getProductPropertyValueList();
			if(AppUtils.isNotBlank(valueDtoList)){
				for (ProductPropertyValueDto productPropertyValueDto : valueDtoList) {
					if(propValueId.equals(productPropertyValueDto.getValueId())){
						return productPropertyValueDto.getName();
					}
				}
			}
		}
		return null;
	}
	
	private ProductDto constructProductDto(Product product) {
		ProductDto productDto = new ProductDto();
        productDto.setProVideoUrl(product.getProVideoUrl());
		productDto.setProdId(product.getProdId());
		productDto.setName(product.getName());
		productDto.setCategoryId(product.getCategoryId());
		productDto.setKeyWord(product.getKeyWord());
		productDto.setPrice(product.getPrice());
		productDto.setCash(product.getCash());
		productDto.setStocks(product.getStocks());
		productDto.setBrandId(product.getBrandId());
		productDto.setBrief(product.getBrief());
		productDto.setContent(product.getContent());
		productDto.setContentM(product.getContentM());
		productDto.setModelId(product.getModelId());
		productDto.setPartyCode(product.getPartyCode());
		productDto.setVolume(product.getVolume());
		productDto.setWeight(product.getWeight());
		productDto.setParameter(product.getParameter());
		productDto.setUserParameter(product.getUserParameter());
		productDto.setStatus(product.getStatus());
		
		productDto.setSupportDist(product.getSupportDist());
		productDto.setFirstLevelRate(product.getFirstLevelRate());
		productDto.setSecondLevelRate(product.getSecondLevelRate());
		productDto.setThirdLevelRate(product.getThirdLevelRate());
		
		productDto.setShopFirstCatId(product.getShopFirstCatId());
		productDto.setShopSecondCatId(product.getShopSecondCatId());
		productDto.setShopThirdCatId(product.getShopThirdCatId());
		productDto.setHasInvoice(product.getHasInvoice());
		productDto.setHasGuarantee(product.getHasGuarantee());
		productDto.setRejectPromise(product.getRejectPromise());
		productDto.setServiceGuarantee(product.getServiceGuarantee());
		productDto.setAfterSaleId(product.getAfterSaleId());
		productDto.setStockCounting(product.getStockCounting());
		productDto.setKeyWord(product.getKeyWord());
		productDto.setMetaDesc(product.getMetaDesc());
		productDto.setMetaTitle(product.getMetaTitle());
		
		productDto.setTransportId(product.getTransportId());
		productDto.setTransportType(product.getTransportType());
		productDto.setSupportTransportFree(product.getSupportTransportFree());
		productDto.setEmsTransFee(product.getEmsTransFee());
		productDto.setMailTransFee(product.getMailTransFee());
		productDto.setExpressTransFee(product.getExpressTransFee());
		productDto.setServiceShow(product.getServiceShow());

		productDto.setStocksArm(product.getStocksArm());
		
		Integer publishStatus = product.getStatus();
		if(AppUtils.isNotBlank(product.getStartDate())){
			productDto.setSelectDays(new SimpleDateFormat("yyyy-M-d").format(product.getStartDate()));
			productDto.setSelectHours(new SimpleDateFormat("HH").format(product.getStartDate()));
			productDto.setSelectMinutes(new SimpleDateFormat("mm").format(product.getStartDate()));
			productDto.setSetUpTime(new SimpleDateFormat("yyyy-M-d HH:mm:ss").format(product.getStartDate()));
			//如果是上线状态 并且还没到生效时间， 发布状态 改为 2 ：设定
			if(product.getStatus().equals(ProductStatusEnum.PROD_ONLINE.value())){
				if(product.getStartDate().after(new Date())){
					publishStatus = 2;
				}
			}
		}
		productDto.setPublishStatus(publishStatus);
		
		return productDto;
	}

	

	/** 
	 * 
	 * 根据商品ID，保存用户自定义商品属性 userProperties
	 * 
	 *  isSimilarImport 是否根据类似商品发布商品
	 * 
	 **/
	private void saveUserProperties(String userProperties, Long prodId, String userName) {
		// 直接发布商品
		if (AppUtils.isNotBlank(userProperties)) {
			List<ResultCustomPropertyDto> customPropertyDtoList = JSONUtil.getArray(userProperties,ResultCustomPropertyDto.class);
			if (AppUtils.isNotBlank(customPropertyDtoList)) {
				for (ResultCustomPropertyDto customPropertyDto : customPropertyDtoList) {
					ProductProperty property = customPropertyDto.getProductProperty();
					property.setProdId(prodId);
					property.setUserName(userName);
					productPropertyDao.save(property, property.getId());
					List<ProductPropertyValue> propertyValueList = customPropertyDto.getPropertyValueList();
					productPropertyValueDao.saveWithId(propertyValueList);
				}
			}
		}
	}

	/** 
	 * 根据商品ID，保存商品sku 
	 * 保存库存历史记录
	 * 
	 **/
	private void saveProdSku(String skus, Long prodId) {
		if (AppUtils.isNotBlank(skus)) {
			Date date = new Date();
			List<Sku> skuList = JSONUtil.getArray(skus, Sku.class);
			for (Sku sku : skuList) {
				//判断 sku 属性是否正常
				boolean reslut = sku.checkProperties();
				
				if(!reslut){
					throw new BusinessException("Properties incorrect for prod " + prodId + ", skuId:" + sku.getId());
				}
				
				sku.setStocks(AppUtils.isBlank(sku.getStocks()) ? (long) 0 : sku.getStocks());
				sku.setActualStocks(AppUtils.isBlank(sku.getStocks()) ? (long) 0 : sku.getStocks());
				sku.setBeforeStock(-1L);
				sku.setBeforeActualStock(-1L);
				sku.setStatus(sku.getStatus());
				sku.setProdId(prodId);
				sku.setModifyDate(date);
				sku.setRecDate(date);
				sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
			}
			skuDao.save(skuList);			
			
			List<StockLog> stockLogs = new ArrayList<StockLog>();  //库存记录
			stockLogs.addAll(parseStockLogs(skuList,"创建"));
			stockLogDao.saveStockLogs(stockLogs);
		}
	}
	
		
		
		//后台传过来的仓库列表
		private List<StockLog> parseStockLogs(List<Sku> skuList,String type){
			List<StockLog> result=new ArrayList<StockLog>();
			Date date=new Date();
			if(skuList==null || skuList.size()==0){
				return null;
			}
			
			for (Sku sku : skuList) {
				StockLog stockLog=new StockLog();
				stockLog.setProdId(sku.getProdId());
				stockLog.setSkuId(sku.getSkuId());
				stockLog.setName(sku.getName());
				stockLog.setBeforeActualStock(sku.getBeforeActualStock());
				stockLog.setAfterActualStock(sku.getActualStocks());
				stockLog.setBeforeStock(sku.getBeforeStock());
				stockLog.setAfterStock(sku.getStocks());
				stockLog.setUpdateTime(date);
				stockLog.setUpdateRemark(type+"商品'"+sku.getName()+"'，商品销售库存为:'" + sku.getStocks() + "'， 实际库存为:'" + sku.getActualStocks() + "'");
				result.add(stockLog);
			}
			return result;
		}
	
	/** 保存用户自定义属性值名 **/
	private void savePropValueAlias(String valueAlias,String userId,Long prodId){
		if (AppUtils.isNotBlank(valueAlias)) {
			List<PropValueAlias> valueAliasList = JSONUtil.getArray(valueAlias, PropValueAlias.class);
			for (PropValueAlias propValueAlias : valueAliasList) {
				propValueAlias.setUserId(userId);
				propValueAlias.setProdId(prodId);
			}
			propValueAliasDao.save(valueAliasList);
		}
	}
	
	/** 对比更新 用户自定义属性值名 **/
	private int updatePropValueAlias(Product product){
		int result = 0;
		if(AppUtils.isNotBlank(product.getValueAlias())){
			List<PropValueAlias> aliasList = JSONUtil.getArray(product.getValueAlias(), PropValueAlias.class);
			List<PropValueAlias> originAliasList = propValueAliasDao.queryByProdIdNoCache(product.getProdId());
			
			Map<Integer,List<PropValueAlias>> compareResult = getValueAliasListComparer().compare(aliasList, originAliasList, product);
		
			List<PropValueAlias> delValueAliasList = compareResult.get(-1);
			List<PropValueAlias> addValueAliasList = compareResult.get(1);
			
			if(AppUtils.isNotBlank(delValueAliasList)){
				propValueAliasDao.delete(delValueAliasList);
				result++;
			}
			
			if(AppUtils.isNotBlank(addValueAliasList)){
				propValueAliasDao.save(addValueAliasList);
				result++;
			}
			
			if(result > 0){
				propValueAliasDao.clearPropValueAliasCache(product.getProdId()); //chear cache
			}
		}
		return result;
	}

	/** 对比更新商品sku **/
	private int updateProdSku(Product product) {
		int result = 0;
		if (AppUtils.isNotBlank(product.getSkus())) {
			List<Sku> originSkuList = skuDao.getSkuByProdId(product.getProdId());// 数据库的SKU数据
			
			List<Sku> skuList = JSONUtil.getArray(product.getSkus(), Sku.class); //页面传过来的SKU
			
			Map<Integer, List<Sku>> compareResult = getDataListComparer().compare(skuList, originSkuList, product);

			List<Sku> updateSkuList = compareResult.get(0);
			List<Sku> delSkuList = compareResult.get(-1);
			List<Sku> addSkuList = compareResult.get(1);
			
			if (AppUtils.isNotBlank(delSkuList)) {
				skuDao.delete(delSkuList);
				result++;
			}

			if (AppUtils.isNotBlank(addSkuList)) {
				List<Sku> toBeSaveSkuList = new ArrayList<Sku>();
				for (Sku sku : addSkuList) {//检查一下页面传递过来的参数是否正确
					if (AppUtils.isBlank(sku.getSkuType())) {
						sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
					}
					if(sku.checkProperties()){
						toBeSaveSkuList.add(sku);
					}
				}
				if(AppUtils.isNotBlank(toBeSaveSkuList)){
					skuDao.save(toBeSaveSkuList);
					result++;
				}
			}

			if (AppUtils.isNotBlank(updateSkuList)) {
				for (Sku sku : updateSkuList) {//检查一下页面传递过来的参数是否正确
					if (AppUtils.isBlank(sku.getSkuType())) {
						sku.setSkuType(SkuActiveTypeEnum.PRODUCT.value());
					}
					sku.checkProperties();
				}
				product.setUpdateSkuList(updateSkuList);
				skuDao.update(updateSkuList);
				result++;
			}
			
			
			List<StockLog> stockLogs = new ArrayList<StockLog>();  
			
			if(AppUtils.isNotBlank(addSkuList)){
				stockLogs.addAll(parseStockLogs(addSkuList,"创建"));
			}
			
			if(AppUtils.isNotBlank(updateSkuList)){
				stockLogs.addAll(parseStockLogs(updateSkuList,"更新"));
			}
			
			//删除的仓库记录
			if(AppUtils.isNotBlank(delSkuList)){
				String skuIdList=parseStockLogSkuId(delSkuList);
				stockLogDao.deleteStockLogById(skuIdList);
			}
			
			stockLogDao.saveStockLogs(stockLogs);
			
			if(result > 0){
				//clear cache
				skuDao.clearSkuByProd(product.getProdId());
				if(AppUtils.isNotBlank(updateSkuList)) {
					for(Sku sku :updateSkuList){
						skuDao.cleanSkuCache(sku.getId());
					}
					
				}
				if(AppUtils.isNotBlank(delSkuList)) {
					for(Sku sku :delSkuList){
						skuDao.cleanSkuCache(sku.getId());
					}
				}

				
			}
		}
		return result;
	}

	private String parseStockLogSkuId(List<Sku> delSkuList){
		List<Long> skuIdList=new ArrayList<Long>();
		StringBuffer sb=new StringBuffer();
		for (Sku sku : delSkuList) {
			Long skuId=sku.getSkuId();
			if(!skuIdList.contains(skuId)){
				sb.append(skuId+",");
			}
		}
		return sb.substring(0,sb.length()-1);
	}
	
	@Override
	public boolean updataIsGroup(Long prodId,Integer status) {
		return productDao.updataIsGroup(prodId,status);
	}

	@Override
	public Group getGroupPrice(Long prodId) {
		return productDao.getGroupPrice(prodId);
	}
	
	@Override
	public Seckill getSeckillPrice(Integer seckillId,Long prodId,Long skuId){
		return productDao.getSeckillPrice(seckillId,prodId,skuId);
	}
	
	@Override
	public Auctions getAuctionsPrice(Long prodId,Long skuId){
		return productDao.getAuctionsPrice(prodId,skuId);
	}
	@Override
	public  PresellProd getPresellProdPrice(Long prodId,Long skuId){
		return productDao.getPresellProdPrice(prodId,skuId);
	}
	@Override
	public void addProdComments(Long prodId) {
		productDao.update("update ls_prod set comments=comments+1 where prod_id=?", prodId);
	}

	@Override
	public int getveryBuyNum(Long productId, Long skuId) {
		return productDao.getveryBuyNum(productId,skuId);
	}

	@Override
	public List<ProductDetail> getLikeProd(Long categoryId) {
		return productDao.getLikeProd(categoryId);
	}

	@Override
	public List<ProductExportDto> findExportProd(Product prod) {
		if(AppUtils.isNotBlank(prod)){
			StringBuilder sb=new StringBuilder();
			List<Object> obj=new ArrayList<Object>();
			sb.append("SELECT p.name prodName,p.brief brief,s.cn_properties attr,")
			.append("p.price price,s.price cash,s.stocks stocks,")
			.append("s.actual_stocks totalStocks,p.stocks_arm arm,s.model_id modelCode,")
			.append("s.party_code partycode FROM ls_sku s,ls_prod p,ls_shop_detail sd ")
			.append("WHERE p.prod_id=s.prod_id AND sd.shop_id=p.shop_id ");

			if(AppUtils.isNotBlank(prod.getStatus())){
				if(prod.getStatus() == 4){
					//查询所有商品不做处理
				}else{
					sb.append(" AND p.status=?");
					obj.add(prod.getStatus());
				}
			}
			if(AppUtils.isNotBlank(prod.getShopId())){
				sb.append(" AND sd.shop_id = ?");
				obj.add(prod.getShopId());
			}
			if(AppUtils.isNotBlank(prod.getCategoryId())){
				sb.append(" AND p.category_id=?");
				obj.add(prod.getCategoryId());
			}
			if(AppUtils.isNotBlank(prod.getBrandId())){
				sb.append(" AND p.brand_id=?");
				obj.add(prod.getBrandId());
			}
			if(AppUtils.isNotBlank(prod.getName())){
				sb.append(" AND p.name LIKE ?");
				obj.add("%"+prod.getName().trim()+"%");
			}
			if(AppUtils.isNotBlank(prod.getProdId())){
				sb.append(" AND p.prod_id=?");
				obj.add(prod.getProdId());
			}

			return this.productDao.findExport(sb.toString(),obj.toArray());
		}
		return null;
	}

	@Override
	public List<Product> getProdByCateId(Long id) {
		return this.productDao.getProdByCateId(id);
	}

	/**
	 * 释放秒杀商品
	 */
	@Override
	public void releaseSeckill(SeckillActivity seckillActivity) {

		// 重置sku营销状态
		List<SeckillProd> seckillProds = seckillProdDao.getSeckillProdBySId(seckillActivity.getId());
		for (SeckillProd seckillProd : seckillProds) {
			skuDao.updateSkuTypeById(seckillProd.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.SECKILL.value());
		}
		
		// 更改秒杀活动状态为“已完成，释放sku”
		seckillActivity.setStatus(SeckillStatusEnum.FINISH.value());
		seckillActivityDao.updateSeckillActivity(seckillActivity);
	}
	
	@Override
	public PageSupport<Product> getProductList(String name, String curPageNO,Long shopId) {
		return productDao.getProductList(name, curPageNO, shopId);
	}
	
	@Override
	public PageSupport<Product> storeProdctList(Long shopId,String curPageNO,Long fireCat,Long secondCat,Long thirdCat,String keyword,String order) {
		return productDao.storeProdctList(shopId,curPageNO,fireCat,secondCat,thirdCat,keyword,order);
	}
	
	@Override
	public PageSupport<Product> newProd(Long shopId) {
		return productDao.newProd(shopId);
	}

	@Override
	public PageSupport<Product> getProductListPage(String curPageNO, Product product) {
		return productDao.getProductListPage(curPageNO,product);
	}

	@Override
	public PageSupport<ProdUserAttribute> getProdUserAttributePage(String curPageNO, Long shopId) {
		return prodUserAttributeDao.getProdUserAttributePage(curPageNO,shopId);
	}

	@Override
	public PageSupport<ProdUserParam> getProdUserParamPage(String curPageNO, Long shopId) {
		return prodUserParamDao.getProdUserParamPage(curPageNO,shopId);
	}

	@Override
	public PageSupport<Product> getAuctionProdLayout(String curPageNO, Long shopId, Product product) {
		return productDao.getAuctionProdLayout(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getProductListPage(String curPageNO, Long shopId,Product product) {
		return productDao.getProductListPage(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getloadProdListPage(String curPageNO, Long shopId, Product product) {
		return productDao.getloadProdListPage(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getDustbinProdListPage(String curPageNO, String name, String siteName) {
		return productDao.getDustbinProdListPage(curPageNO,name,siteName);
	}

	@Override
	public PageSupport<Product> getloadSelectProd(Long shopId, String curPageNO, Product product) {
		return productDao.getloadSelectProd(shopId,curPageNO,product);
	}

	@Override
	public PageSupport<ProductDetail> getAdvancedSearchProds(Integer curPageNO, ProductSearchParms parms) {
		return productDao.getAdvancedSearchProds(curPageNO, parms);
	}

	@Override
	public PageSupport<Product> getProductFloorLoad(String curPageNO, String name,Long shopId) {
		return productDao.getProductFloorLoad(curPageNO,name,shopId);
	}

	@Override
	public PageSupport<Product> getGroupProdLayout(String curPageNO, Long shopId, Product product) {
		return productDao.getGroupProdLayout(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> groupProdLayout(String curPageNO, Long shopId, Product product) {
		return productDao.groupProdLayout(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getloadProdListPageByshopId(String curPageNO, Long shopId, Product product) {
		return productDao.getloadProdListPageByshopId(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getloadWantDistProd(String curPageNO, Long shopId, Product product) {
		return productDao.getloadWantDistProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<ProdUserAttribute> getProdUserAttributeByPage(String curPageNO, Long shopId) {
		return prodUserAttributeDao.getProdUserAttributeByPage(curPageNO,shopId);
	}

	@Override
	public PageSupport<ProdUserParam> getUserParamOverlay(String curPageNO, Long shopId) {
		return prodUserParamDao.getUserParamOverlay(curPageNO,shopId);
	}

	@Override
	public PageSupport<Product> getSellingProd(String curPageNO, Long shopId, Product product) {
		return productDao.getSellingProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getProdInStoreHouse(String curPageNO, Long shopId, Product product) {
		return productDao.getProdInStoreHouse(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getViolationProd(String curPageNO, Long shopId, Product product) {
		return productDao.getViolationProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getAuditingProd(String curPageNO, Long shopId, Product product) {
		return productDao.getAuditingProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getAuditFailProd(String curPageNO, Long shopId, Product product) {
		return productDao.getAuditFailProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getProdInDustbin(String curPageNO, Long shopId, Product product) {
		return productDao.getProdInDustbin(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getJoinDistProd(String curPageNO, Long shopId, Product product) {
		return productDao.getJoinDistProd(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getReportForbit(String curPageNO, String userName, Product product) {
		return productDao.getReportForbit(curPageNO,userName,product);
	}

	@Override
	public PageSupport<Product> getProductList(String curPageNO, int pageSize, Long shopId, Product product) {
		return productDao.queryPage(curPageNO,pageSize,shopId,product);
	}

	@Override
	public PageSupport<Product> getSeckillLayer(String curPageNO, Long shopId, int pageSize, Product product) {
		return productDao.getSeckillLayer(curPageNO,shopId,pageSize,product);
	}

	/**
	 * 加载商品列表
	 */
	@Override
	public PageSupport<Product> getProductLists(String curPageNO, int pageSize, Long shopId, Product product) {
		return productDao.getProductLists(curPageNO,pageSize,shopId,product);
	}

	@Override
	public PageSupport<Product> getStoreProdcts(Long shopId, String curPageNO, Long fireCat, Long secondCat,
			Long thirdCat, String keyword, String order) {
		return productDao.getStoreProdcts(shopId,curPageNO,fireCat,secondCat,thirdCat,keyword,order);
	}

	@Override
	public PageSupport<Product> getAddProdSkuLayoutPage(String curPageNO, Product product, Long shopId) {
		return productDao.getAddProdSkuLayoutPage(curPageNO,product,shopId);
	}

	@Override
	public PageSupport<Product> getAddProdSkuLayout(String curPageNO, Long shopId, Product product) {
		return productDao.getAddProdSkuLayout(curPageNO,shopId,product);
	}

	@Override
	public PageSupport<Product> getProductListPage(Product product, String curPageNO, Integer pageSize,
			DataSortResult result) {
		return productDao.getProductListPage(product,curPageNO,pageSize,result);
	}
	

	@Override
	public PageSupport<ProductPurchaseRateDto> getPurchaseRate(String curPageNO, Long shopId, String prodName,
			DataSortResult result,Date startDate,Date endDate) {
		return productDao.getPurchaseRate(curPageNO,shopId,prodName,result,startDate,endDate);
	}

	@Override
	public PageSupport<Product> getLoadSelectProd(String curPageNO, Product product) {
		return productDao.getLoadSelectProd(curPageNO,product);
	}

	@Override
	public PageSupport<Product> getProductListMobile(String curPageNO, Long brandId) {
		return productDao.getProductListMobile(curPageNO,brandId);
	}
	
	@Override
	public PageSupport<SalesRankDto> getProductList(String curPageNO, Long shopId, String prodName, Date startDate,
			Date endDate) {
		return productDao.querySimplePage(curPageNO,shopId,prodName,startDate,endDate);
	}

	@Override
	public int upStocks(Long prodId, Long stocks) {
		return productDao.upStocks(prodId, stocks);
	}

	@Override
	public Integer batchOffline(List<Long> prodIdList) {
		return productDao.batchOffline(prodIdList);
	}

	@Override
	public void batchOnlineLine(List<Long> prodIdList) {
		productDao.batchOnlineLine(prodIdList);
	}

	@Override
	public void updateProdType(Long prodId, String type, Long seckillId) {
		productDao.updateProdType(prodId, type, seckillId);
	}
	@Override
	public PageSupport<FullActiveProdDto> getFullActiveProdDto(Long shopId, String curPageNo, String prodName) {
		return productDao.getFullActiveProdDto(shopId,curPageNo,prodName);
	}
	
	@Override
	public List<MergeGroupSelProdDto> queryMergeGroupProductToSku(Long shopId, Long[] prodIds) {
		return productDao.queryMergeGroupProductToSku(shopId,prodIds);
	}
	@Override
	public PageSupport<Product> queryMergeGroupProduct(Long shopId) {
		return productDao.queryMergeGroupProduct(shopId);
	}
	@Override
	public PageSupport<Product> queryMergeGroupProductList(Long shopId, String curPageNO, String prodName) {
		return productDao.queryMergeGroupProductList(shopId,curPageNO,prodName);
	}
	@Override
	public List<MergeGroupSelProdDto> queryMergeGroupProductByMergeId(Long id) {
		return productDao.queryMergeGroupProductByMergeId(id);
	}

    @Override
    public List<PresellSkuDto> queryPresellProductByPresellId(Long id) {
        return productDao.queryPresellProductByPresellId(id);
    }

    @Override
	public ProductDto getProductDtoForMergrGroup(Long prodId, Long mergeGroupId) {
		
		ProductDto prodDto = new ProductDto();
		//获取商品基本信息
		ProductDetail prodDetail = getProdDetail(prodId);
		if(prodDetail!=null){
			//拷贝字段
			copyProdField(prodDto,prodDetail);
		}else{
			return null;
		}
		
		//获取商品sku信息
		List<SkuDto> skuDtoList = new ArrayList<SkuDto>();
		List<Sku> skuList = skuService.getSkuForMergeGroup(prodId,mergeGroupId);
		if(AppUtils.isNotBlank(skuList)){
			for (Sku sku : skuList) {
				SkuDto skuDto = new SkuDto();
				skuDto.setSkuId(sku.getSkuId());
				skuDto.setImages(sku.getPic());
				skuDto.setName(processName(sku.getName()));
				skuDto.setPrice(sku.getPrice());
				skuDto.setCash(sku.getPrice());
				skuDto.setMergePrice(sku.getMergePrice());
				skuDto.setStocks(sku.getStocks());  //add sku数量
				skuDto.setProperties(sku.getProperties());
				skuDto.setStatus(sku.getStatus());
				skuDto.setCnProperties(sku.getCnProperties());
				skuDto.setVolume(sku.getVolume());
				skuDto.setWeight(sku.getWeight());
				if(AppUtils.isBlank(sku.getProperties())){
					skuDto.setPrice(prodDto.getCash());
				}
				skuDtoList.add(skuDto);
			}
			prodDto.setSkuDtoList(skuDtoList);
			prodDto.setSkuDtoListJson(JSONUtil.getJson(skuDtoList));
		}
		
		//获取属性图片
		List<PropValueImgDto> propValueImgList = new ArrayList<PropValueImgDto>();
		Map<Long,List<String>> valueImagesMap = new HashMap<Long,List<String>>();
		if(AppUtils.isNotBlank(skuList)){
			//获取到商品  所有的属性图片
			List<ProdPropImage>  prodPropImages = prodPropImageDao.getProdPropImageByProdId(prodId);
			if(AppUtils.isNotBlank(prodPropImages)){
				//根据属性值 进行分组,封装成map
				for (ProdPropImage prodPropImage : prodPropImages) {
					Long valueId = prodPropImage.getValueId();
					List<String> imgs = valueImagesMap.get(valueId);
					if(AppUtils.isBlank(imgs)){
						imgs = new ArrayList<String>();
					}
					imgs.add(prodPropImage.getUrl());
					valueImagesMap.put(valueId, imgs);
				}
				//将map转换成 DTO LIST
				Iterator<Long> it = valueImagesMap.keySet().iterator();
				while(it.hasNext()){
					Long valId = it.next();
					List<String> imgs = valueImagesMap.get(valId);
					PropValueImgDto propValueImgDto = new PropValueImgDto();
					propValueImgDto.setValueId(valId);
					propValueImgDto.setImgList(imgs);
					propValueImgList.add(propValueImgDto);
				}
			}
			prodDto.setPropValueImgListJson(JSONUtil.getJson(propValueImgList));
		}
		
		//获取商品属性和属性值
		List<ProductPropertyDto> prodPropDtoList = getPropValListByProd(prodDto,skuList,valueImagesMap);
		prodDto.setProdPropDtoList(prodPropDtoList);
		prodDto.setPropValueImgList(propValueImgList);
		
		
		//商品图片列表(没有属性图片时，展示商品图片)
		List<ProdPicDto> prodPics = new ArrayList<ProdPicDto>();
		if(AppUtils.isBlank(propValueImgList)){
			List<ImgFile> imgFileList = imgFileDao.getAllProductPics(prodId);
			if(AppUtils.isNotBlank(imgFileList)){
				for (ImgFile imgFile : imgFileList) {
					ProdPicDto prodPicDto = new ProdPicDto();
					prodPicDto.setFilePath(imgFile.getFilePath());
					prodPics.add(prodPicDto);
				}
			}
		}else{
			ProdPicDto prodPicDto = new ProdPicDto();
			prodPicDto.setFilePath(prodDto.getPic());
			prodPics.add(prodPicDto);
		}
		prodDto.setProdPics(prodPics);
		
		return prodDto;
	}
	
	private List<ProductPropertyDto> getPropValListByProd(ProductDto prodDto, List<Sku> productSkus,
			Map<Long, List<String>> valueImagesMap) {
		List<Long> selectedVals = new ArrayList<Long>();
		if(AppUtils.isNotBlank(productSkus)){
			//取得价格最低的 sku 作为默认选中的sku
			Sku defaultSku = getDefaultSku(productSkus);
			prodDto.setCash(defaultSku.getPrice());
			prodDto.setSkuId(defaultSku.getSkuId());
			prodDto.setStocks(Integer.parseInt(defaultSku.getStocks().toString()));
			if(AppUtils.isNotBlank(defaultSku.getName())){
				prodDto.setName(defaultSku.getName());
			}
			//取得sku字符串(k1:v1;k2:v2;k3:v3)中的每对key:value,并对其进行相应处理  
			Map<Long,List<Long>> propIdMap = new LinkedHashMap<Long,List<Long>>();
			List<Long> allValIdList = new ArrayList<Long>();
			for (Sku sku : productSkus) {
				String properties = sku.getProperties();
				if(AppUtils.isNotBlank(properties)){
					String skuStrs[] = properties.split(";");
					for (int i = 0; i < skuStrs.length; i++) {
						String skuItems[] = skuStrs[i].split(":");
						Long propId = Long.valueOf(skuItems[0]);
						Long valueId = Long.valueOf(skuItems[1]);
						if(sku.getSkuId().equals(defaultSku.getSkuId())){
							selectedVals.add(valueId);
						}
						List<Long> valIdList = propIdMap.get(propId);
						if(AppUtils.isBlank(valIdList)){
							valIdList = new ArrayList<Long>();
							valIdList.add(valueId);
						}else{
							if(!valIdList.contains(valueId)){
								valIdList.add(valueId);
							}
						}
						propIdMap.put(propId, valIdList);
						allValIdList.add(valueId);
					}
				}else{
					log.warn("sku id {} does not have correct properties setting", sku.getId());
				}
			}
			
			List<Long> propIds = new ArrayList<Long>(propIdMap.keySet());//set 转 list
			if(AppUtils.isBlank(propIds)){
				return null;
			}
			
			//根据propId的集合 ，查出属性集合
			List<ProductProperty> pps = productPropertyDao.getProductProperty(propIds, prodDto.getCategoryId());
			List<ProductPropertyDto> ppds = new ArrayList<ProductPropertyDto>();
			//根据valId的集合，查出属性值的集合
			List<ProductPropertyValue> allPropVals = productPropertyValueDao.getProductPropertyValue(allValIdList,prodDto.getProdId());
			for (ProductProperty prodProp : pps) {
				//封装属性DTO
				ProductPropertyDto prodPropDto = new ProductPropertyDto();
				prodPropDto.setPropId(prodProp.getId());//属性id
				prodPropDto.setPropName(prodProp.getMemo());//属性名称
				
				//根据propId拿到valueId的集合
				List<Long> valIds = propIdMap.get(prodProp.getId());
				
				//根据valueId的集合,查出属性值的集合
				List<ProductPropertyValue> ppvs = getPropValsByIds(valIds,allPropVals);
				List<ProductPropertyValueDto> ppvds = new ArrayList<ProductPropertyValueDto>();
				for (ProductPropertyValue prodPropVal : ppvs) {
					//封装属性值DTO
					ProductPropertyValueDto ppvd = new ProductPropertyValueDto();
					ppvd.setValueId(prodPropVal.getId());//属性值id
					if(AppUtils.isNotBlank(prodPropVal.getAlias())){
						ppvd.setName(prodPropVal.getAlias());//属性值名称
					}else{
						ppvd.setName(prodPropVal.getName());//属性值名称
					}
					ppvd.setPropId(prodProp.getId());//属性id
					
					List<String> imgs = valueImagesMap.get(prodPropVal.getId());
					if(AppUtils.isNotBlank(imgs)){
						prodPropVal.setUrl(imgs.get(0));
					}
					if(selectedVals.contains(prodPropVal.getId())){
						ppvd.setIsSelected(true);//是否默认选中
						if(AppUtils.isNotBlank(prodPropVal.getUrl())){
							prodDto.setPic(prodPropVal.getUrl());
						}
					}
					if(AppUtils.isNotBlank(prodPropVal.getUrl())){
						ppvd.setPic(prodPropVal.getUrl());
					}else{
						ppvd.setPic(prodPropVal.getPic());
					}
					ppvds.add(ppvd);
				}
				
				prodPropDto.setProductPropertyValueList(ppvds);
				ppds.add(prodPropDto);
			}
			return ppds;
		}else{
			return null;
		}
	}
	/**
	 * //根据valueId的集合,查出属性值的集合
	 * @param valIds
	 * @param allPropVals
	 * @return
	 */
	private List<ProductPropertyValue> getPropValsByIds(List<Long> valIds, List<ProductPropertyValue> allPropVals) {
		List<ProductPropertyValue> ppvs = new ArrayList<ProductPropertyValue>();
		for (ProductPropertyValue productPropertyValue : allPropVals) {
			if(valIds.contains(productPropertyValue.getValueId())){
				ppvs.add(productPropertyValue);
			}
		}
		return ppvs;
	}
	/**
	 * 查找最低价格的sku作为默认的sku
	 * @return
	 */
	private Sku getDefaultSku(List<Sku> productSkus){
		double price = 0;
		int minPos = 0; //最小值位置
		for (int i = 0; i < productSkus.size(); i++) {
			double minPrice = productSkus.get(i).getPrice();
			if((minPrice < price || price==0) && productSkus.get(i).getStatus().equals(1)){
				price = minPrice;
				minPos = i;
			}
			
		}
		return productSkus.get(minPos);
	}
	
	
	@Override
	public boolean checkShopSubCategoryUse(Long subCatId) {
		
		return productDao.checkShopSubCategoryUse(subCatId);
	}
	@Override
	public boolean checkShopCategoryUse(Long shopCatId) {
		return productDao.checkShopCategoryUse(shopCatId);
	}
	@Override
	public boolean checkShopNextCategoryUse(Long nextCatId) {
		return productDao.checkShopNextCategoryUse(nextCatId);
	}
	/* 
	 * 更新库存
	 */
	@Override
	public String updateStock(String shopName,Long skuId, Long stocks, Long prodId) {
		//更新sku库存
		Sku sku=skuService.getSku(skuId);
		Long beforeStore = sku.getStocks();  //获取更新前的库存
		Long beforeActualStocks = sku.getActualStocks();  //获取更新前的库存
		if(AppUtils.isBlank(sku)){
			return Constants.FAIL;
		}
		sku.setStocks(stocks);
		sku.setActualStocks(stocks);
		skuService.saveSku(sku);
		//更新prod库存
		int result2=this.upStocks(prodId,stocks);
		if(result2 == 0){
			return Constants.FAIL;
		}
		//保存库存记录表
		StockLog stockLog = new StockLog();
		stockLog.setProdId(prodId);
		stockLog.setSkuId(skuId);
		stockLog.setName(sku.getName());
		stockLog.setBeforeStock(beforeStore);
		stockLog.setAfterStock(stocks);
		stockLog.setBeforeActualStock(beforeActualStocks);
		stockLog.setAfterActualStock(stocks);
		stockLog.setUpdateTime(new Date());
		stockLog.setUpdateRemark("更新商品'"+sku.getName()+"'，商品销售库存为:'"+ stocks + "'，实际库存为:'" + stocks + "'");
		Long id= null;
		if (!AppUtils.isBlank(stockLog.getId())) {
			 stockLogDao.updateStockLog(stockLog);
	         id =  stockLog.getId();
	        }else{
	        	id = stockLogDao.saveStockLog(stockLog);
	        }
		
		if(id==null){
			return Constants.FAIL;
		}
		sendArrivalInformProcessor.process(new SendArrivalInform(skuId,stocks,shopName));
		return Constants.SUCCESS;
	}
	@Override
	public Boolean isAttendActivity(Long prodId) {
		return productDao.isAttendActivity(prodId);
	}
	
	/** 
	 * 获取简单的商品信息数据
	 */
	@Override
	public List<Select2Dto> getProductSelect2(String siteName, Integer currPage, Integer pageSize) {
		int index = (currPage - 1) * pageSize;
//		String sql="SELECT s.shop_Id AS id, s.site_Name AS text  FROM ls_shop_detail s WHERE s.status = 1";
		String sql = "SELECT ld.shop_Id AS id, ld.site_Name AS text FROM ls_shop_detail ld LEFT JOIN " +
				"ls_shop_app_decorate ls ON ld.shop_id = ls.shop_id WHERE ld.STATUS = 1";
		if(AppUtils.isNotBlank(siteName)){
			sql = sql +  " AND s.site_Name LIKE ?";
			return productDao.queryLimit(sql, Select2Dto.class, index, pageSize, "%" + siteName.trim() + "%");
		}else{
			return productDao.queryLimit(sql, Select2Dto.class, index, pageSize);
		}

	}
	
	/**
	 * 获取简单的商品信息数据
	 */
	@Override
	public int getProductSelect2Count(String siteName) {
//		String sql="SELECT COUNT(*) FROM ls_shop_detail s WHERE s.status=1";
//		String sql = "SELECT COUNT(*) FROM ls_shop_app_decorate ls LEFT JOIN " +
//				"ls_shop_detail ld ON ld.shop_id = ls.shop_id WHERE ld.STATUS = 1";
		String sql = "SELECT COUNT(*) FROM ls_shop_detail ld LEFT JOIN " +
				"ls_shop_app_decorate ls ON ld.shop_id = ls.shop_id WHERE ld.STATUS = 1";
		
		if(AppUtils.isNotBlank(siteName)){
			sql = sql +  " AND s.site_Name LIKE ?";
			return (int)productDao.getLongResult(sql, "%" + siteName.trim() + "%");
		}else{
			return (int)productDao.getLongResult(sql);
		}
	}
	
	@Override
	public PageSupport<Product> getCouponProds(Long shopId, List<Long> prodIds, String curPageNO, String orders, String prodName) {
		return productDao.getCouponProds(shopId, prodIds, curPageNO, orders, prodName);
	}
	
	@Override
	public PageSupport<Product> queryRedpackProd(List<Long> shopIds, String curPageNo, String orders) {
		return productDao.queryProdByShopIds(shopIds, curPageNo, orders);
	}

	@Override
	public List<Product> findAllProduct(Long shopId) {
		return productDao.findAllProduct(shopId);
	}
	
	@Override
	public int updateProdList(List<Product> list) {
		return productDao.update(list);
	}

    /**
     * 校验一个spu下面的规格数据是否正常
     * 1.每个sku的规格个数是否相同,不同返回false
     * 2.相同规格字段规格名不同,返回false
     * 3.是否有相同的规格,相同返回false
     * ,如果规格证据正常,则先创建所有的规格属性,属性值到数据库
     */
    private Boolean checkSkusPorpCount(ProdExportDTO prodExportDTO, Map<String, Set<String>> propMap) {
        //第一个sku的规格数量
        int sku1PropCount = 0;
        //第一个sku的规格名
        String sku1firstProperties = "";
        String sku1secondProperties = "";
        String sku1thirdProperties = "";
        String sku1fourthProperties = "";
        //存储excel过来的拼接后的prop_provalue
        List<String> propAndPropValueList = new ArrayList<>();
        //sku的规格属性拼接字符串
        //List<String> propList = new ArrayList<String>();
        List<SkuExportDTO> skuList = prodExportDTO.getSkuList();
        for (int i = 0; i < skuList.size(); i++) {
            StringBuilder propAndvalueSb = new StringBuilder();
            SkuExportDTO skuExportDTO = skuList.get(i);
            int propCount = 0;
            if (AppUtils.isNotBlank(skuExportDTO.getFirstProperties()) && AppUtils.isNotBlank(skuExportDTO.getFirstPropertiesValue())) {
                propCount++;
                String firstProperties = skuExportDTO.getFirstProperties().trim();
                if (i == 0) {
                    sku1firstProperties = firstProperties;
                } else {
                    if (!sku1firstProperties.equals(firstProperties)) {
                        return false;
                    }
                }
                String firstPropertiesValue = skuExportDTO.getFirstPropertiesValue().trim();
                copyPropAndValueToMap(firstProperties, firstPropertiesValue, propMap);
                String propAndPropValue = firstProperties + firstPropertiesValue;
                propAndvalueSb.append(propAndPropValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getSecondProperties()) && AppUtils.isNotBlank(skuExportDTO.getSecondPropertiesValue())) {
                propCount++;
                String secondProperties = skuExportDTO.getSecondProperties().trim();
                if (i == 0) {
                    sku1secondProperties = secondProperties;
                } else {
                    if (!sku1secondProperties.equals(secondProperties)) {
                        return false;
                    }
                }
                String secondPropertiesValue = skuExportDTO.getSecondPropertiesValue().trim();
                copyPropAndValueToMap(secondProperties, secondPropertiesValue, propMap);
                String propAndPropValue = secondProperties + secondPropertiesValue;
                propAndvalueSb.append(propAndPropValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getThirdProperties()) && AppUtils.isNotBlank(skuExportDTO.getThirdPropertiesValue())) {
                propCount++;
                String thirdProperties = skuExportDTO.getThirdProperties().trim();
                if (i == 0) {
                    sku1thirdProperties = thirdProperties;
                } else {
                    if (!sku1thirdProperties.equals(thirdProperties)) {
                        return false;
                    }
                }
                String thirdPropertiesValue = skuExportDTO.getThirdPropertiesValue().trim();
                copyPropAndValueToMap(thirdProperties, thirdPropertiesValue, propMap);
                String propAndPropValue = thirdProperties + thirdPropertiesValue;
                propAndvalueSb.append(propAndPropValue);
            }
            if (AppUtils.isNotBlank(skuExportDTO.getFourthProperties()) && AppUtils.isNotBlank(skuExportDTO.getFourthPropertiesValue())) {
                propCount++;
                String fourthProperties = skuExportDTO.getFourthProperties().trim();
                if (i == 0) {
                    sku1fourthProperties = fourthProperties;
                } else {
                    if (!sku1fourthProperties.equals(fourthProperties)) {
                        return false;
                    }
                }
                String fourthPropertiesValue = skuExportDTO.getFourthPropertiesValue().trim();
                copyPropAndValueToMap(fourthProperties, fourthPropertiesValue, propMap);
                String propAndPropValue = fourthProperties + fourthPropertiesValue;
                propAndvalueSb.append(propAndPropValue);
            }
            if (propAndPropValueList.contains(propAndvalueSb)) {
                return false;
            }
            propAndPropValueList.add(propAndvalueSb.toString());
            if (i == 0) {
                sku1PropCount = propCount;
            }
            //如果规格数量不同或规格为空
            if (sku1PropCount != propCount || propCount == 0) {
                return false;
            }
        }
        return true;
    }
    /**
     * 交叉生成所有情况的sku规格情况到数据库 返回所有的规格交叉情况
     */
    private List<String> saveAllSkuProp(String userName, Long prodId, ProdExportDTO prodExportDTO, Map<String, Set<String>> propMap) {
        //存储单个规格情况
        List<String> properties = new ArrayList<String>();
        //把所有规格,规格值保存到数据库
        for (String key : propMap.keySet()) {
            Set<String> stringSet = propMap.get(key);
            //保存规格
            StringBuilder sb = new StringBuilder();
            Long propId = createProductProperty(userName, prodId, key);
            for (String propValue : stringSet) {
                //保存规格值
                Long propValueId = createProductPropertyValue(propId, propValue);
                if (sb.length() > 0) {
                    sb.append(";").append(propId).append(":").append(propValueId);
                } else {
                    sb.append(propId).append(":").append(propValueId);
                }
            }
            properties.add(sb.toString());
        }
        //所有的sku规格情况{propId:propValue}
        List<List<String>> allList = new ArrayList<>();
        for (String props : properties) {
            String[] split = props.split(";");
            List<String> strList = Arrays.asList(split);
            allList.add(strList);
        }
        List<String> propList = calculateCombination(allList);
        return propList;
    }
    /**
     * 组合规格属性到map
     */
    private void copyPropAndValueToMap(String prop, String propValue, Map<String, Set<String>> propMap) {
        if (propMap.keySet().contains(prop)) {
            propMap.get(prop).add(propValue);
        } else {
            Set<String> set = new HashSet<String>();
            set.add(propValue);
            propMap.put(prop, set);
        }
    }

    /**
     * 算法，非递归计算所有组合
     *
     * @param inputList 所有数组的列表
     */
    private List<String> calculateCombination(List<List<String>> inputList) {
        List<String> result = new ArrayList<String>();
        List<Integer> combination = new ArrayList<Integer>();
        int n = inputList.size();
        for (int i = 0; i < n; i++) {
            combination.add(0);
        }
        int i = 0;
        boolean isContinue = false;
        do {
            //打印一次循环生成的组合
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < n; j++) {
                if (j == n - 1) {
                    sb.append(inputList.get(j).get(combination.get(j)));
                } else {
                    sb.append(inputList.get(j).get(combination.get(j)) + ";");
                }
            }
            System.out.print(sb);
            result.add(sb.toString());
            System.out.println();

            i++;
            combination.set(n - 1, i);
            for (int j = n - 1; j >= 0; j--) {
                if (combination.get(j) >= inputList.get(j).size()) {
                    combination.set(j, 0);
                    i = 0;
                    if (j - 1 >= 0) {
                        combination.set(j - 1, combination.get(j - 1) + 1);
                    }
                }
            }
            isContinue = false;
            for (Integer integer : combination) {
                if (integer != 0) {
                    isContinue = true;
                }
            }
        } while (isContinue);
        return result;
    }
    /**
     * 创建属性值
     */
    private Long createProductPropertyValue(Long propId, String propertiesValueName) {
        Date now = new Date();
        //是否存在属性值
        ProductPropertyValue propvalue = productPropertyValueDao.getProductPropertyValue(propId, propertiesValueName);
        if (propvalue == null) {
            ProductPropertyValue productPropertyValue = new ProductPropertyValue();
            productPropertyValue.setPropId(propId);
            productPropertyValue.setName(propertiesValueName);
            productPropertyValue.setStatus((short) 1);
            productPropertyValue.setModifyDate(now);
            productPropertyValue.setRecDate(now);
            return productPropertyValueDao.save(productPropertyValue);
        }
        return propvalue.getId();
    }
	@Override
	public PageSupport<Product> getProdductByName(String curPageNo, String prodName) {
		return productDao.getProdductByName(curPageNo,prodName);
	}

	@Override
	public PageSupport<Product> getProdductByShopId(String curPageNo, String prodName,Long shopId) {
		return productDao.getProdductByShopId(curPageNo,prodName,shopId);
	}


	@Override
	public void updateProdUserParam(ProdUserParam prodUserParam) {
		prodUserParamDao.updateProdUserParam(prodUserParam);
	}

	@Override
	public Boolean updateSearchWeight(Long prodId, Double searchWeight) {
		return productDao.updateSearchWeight(prodId,searchWeight);
	}

	@Override
	public List<Long> getAllprodIdStr(Long shopId, Integer status) {
		return productDao.getAllprodIdStr(shopId,status);
	}

	@Override
	public List<Product> queryProductByProdIds(List<Long> prodIds) {
		return productDao.getProductListByIds(prodIds);
	}

	@Override
	public String getServerShowById(Long prodId) {

		return productDao.getServerShowById(prodId);
	}

}
