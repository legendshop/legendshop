/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PartnerDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Partner;

/**
 * 合作伙伴Dao.
 */
@Repository
public class PartnerDaoImpl extends GenericDaoImpl<Partner, Long> implements PartnerDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public Partner getPartner(Long id) {
		return getById(id);
	}

	public void deletePartner(Partner partner) {
		delete(partner);
	}

	public Long savePartner(Partner partner) {
		return (Long) save(partner);
	}

	public void updatePartner(Partner partner) {
		update(partner);
	}

	@Override
	public PageSupport<Partner> getPartnerPage(String curPageNO, Partner partner) {
		CriteriaQuery cq = new CriteriaQuery(Partner.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		cq.eq("userName", partner.getUserName());
		return queryPage(cq);
	}

}
