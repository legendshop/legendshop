/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AreaDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Area;

/**
 * The Class AreaDaoImpl.
 */
@Repository
public class AreaDaoImpl extends GenericDaoImpl<Area, Integer> implements AreaDao {

	@Override
	public List<KeyValueEntity> getAreaList(Integer cityid) {
		List<KeyValueEntity> result =  this.query("select id ,area from ls_areas where cityid=?", new Object[]{cityid}, new RowMapper<KeyValueEntity>() {
			@Override
			public KeyValueEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
				return  new KeyValueEntity(String.valueOf(rs.getLong("id")), rs.getString("area"));
			}
		});
		
		return result;
	}

	@Override
	public List<Area> getAreaByCityid(Integer cityid) {
		return query("select id,areaid, area, cityid,code from ls_areas where cityid=?", Area.class, cityid);
	}

	@Override
	@CacheEvict(value = "AreaList", allEntries=true)
	public void deleteAreaByProvinceid(Integer provinceid) {
		update("delete from ls_areas where cityid in (select id from ls_cities where provinceid = ?)", provinceid);
	}

	@Override
	@CacheEvict(value = "AreaList", allEntries=true)
	public void deleteAreaByCityid(Integer cityid) {
		update("delete from ls_areas where cityid  = ?", cityid);
	}

	@Override
	@CacheEvict(value = "AreaList", allEntries=true)
	public void deleteAreaById(Integer id) {
		deleteById(id);
	}
	
	@Override
	@CacheEvict(value = "AreaList", allEntries=true)
	public void deleteAreaList(List<Integer> idList) {
			deleteById(idList);
	}

	@Override
	@CacheEvict(value = "AreaList", allEntries=true)
	public void updateArea(Area area) {
		update(area);
	}


	@Override
	public void saveArea(Area area) {
		save(area);
	}

	@Override
	public Area getAreaById(Integer id) {
		return getById(id);
	}




}
