/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralOrder;

/**
 * 积分订单dao接口
 */
public interface IntegralOrderDao extends GenericDao<IntegralOrder, Long> {

	/**
	 * 获取积分订单
	 * @param id
	 * @return
	 */
	public abstract IntegralOrder getIntegralOrder(Long id);
	
	/**
	 * 根据订单号和用户ID查询积分订单
	 * @param orderSn 订单号
	 * @param userId 用户ID
	 * @return
	 */
	public abstract IntegralOrder getIntegralOrder(String orderSn, String userId);

	/** 
	 * 删除积分订单
	 * @param integralOrder
	 * @return
	 */
	public abstract int deleteIntegralOrder(IntegralOrder integralOrder);

	/** 
	 * 保存积分订单
	 * @param integralOrder
	 * @return
	 */
	public abstract Long saveIntegralOrder(IntegralOrder integralOrder);

	/** 
	 * 更新积分订单
	 * @param integralOrder
	 * @return
	 */
	public abstract int updateIntegralOrder(IntegralOrder integralOrder);

	/** 
	 * 获取积分订单Dto 
	 * @param subSQl
	 * @param args
	 * @return
	 */
	public abstract List<IntegralOrderDto> getIntegralOrderDtos(String subSQl, List<Object> args);

	/** 
	 * 取消订单
	 * @param id
	 * @return
	 */
	public abstract int orderCancel(String id);

	/** 
	 * 根据订单流水号获取积分订单
	 * @param orderSn
	 * @return
	 */
	public abstract IntegralOrder getIntegralOrderByOrderSn(String orderSn);

	/**  
	 * 发货
	 * @param orderSn
	 * @param deliv
	 * @param dvyFlowId
	 */
	public abstract void fahuo(String orderSn, Long deliv, String dvyFlowId);

	/** 
	 * 查找积分订单明细
	 * @param ordeSn
	 * @return
	 */
	public abstract IntegralOrderDto findIntegralOrderDetail(String ordeSn);

	/** 
	 * 删除订单
	 * @param integralOrder
	 * @return
	 */
	public abstract String orderDel(IntegralOrder integralOrder);

	/** 
	 * 获取优惠卷列表页面
	 * @param curPageNO
	 * @param type
	 * @return
	 */
	public abstract PageSupport<Coupon> getCouponListPage(String curPageNO, String type);

}
