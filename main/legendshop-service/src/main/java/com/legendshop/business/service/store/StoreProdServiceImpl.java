/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.store;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.store.StoreProdDao;
import com.legendshop.business.dao.store.StoreSkuDao;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.dto.StoreSkuDto;
import com.legendshop.model.dto.store.StoreCitys;
import com.legendshop.model.entity.store.StoreProd;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.util.AppUtils;

/**
 * 门店商品服务实现类
 */
@Service("storeProdService")
public class StoreProdServiceImpl implements StoreProdService {

	@Autowired
	private StoreProdDao storeProdDao;

	@Autowired
	private StoreSkuDao storeSkuDao;


	/**
	 * 获取门店商品 
	 */
	public StoreProd getStoreProd(Long id) {
		return storeProdDao.getStoreProd(id);
	}

	/**
	 * 删除门店商品 
	 */
	public void deleteStoreProd(StoreProd storeProd) {
		storeProdDao.deleteStoreProd(storeProd);
	}

	/**
	 * 保存门店商品 
	 */
	public Long saveStoreProd(StoreProd storeProd) {
		if (!AppUtils.isBlank(storeProd.getId())) {
			updateStoreProd(storeProd);
			return storeProd.getId();
		}
		return storeProdDao.saveStoreProd(storeProd);
	}

	/**
	 * 更新门店商品
	 */
	public void updateStoreProd(StoreProd storeProd) {
		storeProdDao.updateStoreProd(storeProd);
	}

	/**
	 * 获取门店商品数量 
	 */
	@Override
	public long getStoreProdCount(Long poductId, Long skuId) {
		if(poductId == null || skuId == null){//找不到商品
			return 0;
		}
		return storeProdDao.getStoreProdCount(poductId, skuId);
	}

	/**
	 * 按商品查找每个城市的门店
	 */
	@Override
	public List<StoreCitys> findStoreCitys(Long poductId, String province, String city, String area) {
		return storeProdDao.findStoreCitys(poductId, province, city, area);
	}

	/**
	 * 按商城查找每个城市的门店
	 */
	@Override
	public List<StoreCitys> findStoreShopCitys(Long shopId, String province, String city, String area) {
		return storeProdDao.findStoreShopCitys(shopId, province, city, area);
	}

	/**
	 * 获取库存 
	 */
	@Override
	public Integer getStocks(Long storeId, Long poductId, Long skuId) {
		return storeProdDao.getStocks(storeId, poductId, skuId);
	}

	/**
	 * 弹出设置库存窗口
	 */
	@Override
	public StoreProductDto getStoreProductDto(Long storeId, Long prodId) {
		return storeProdDao.getStoreProductDto(storeId, prodId);
	}

	/**
	 * 门店添加商品和单品.
	 */
	@Override
	public String addProd(StoreProductDto prod) {
		if (AppUtils.isBlank(prod.getId())) { // 说明是第一次添加商品

			StoreProd storeProd = new StoreProd();
			storeProd.setProdId(prod.getProductId());
			storeProd.setShopId(prod.getShopId());
			storeProd.setStoreId(prod.getStoreId());
			storeProd.setStock(prod.getStock());
			storeProd.setAddtime(new Date());
			Long spId = storeProdDao.createId();
			storeProd.setId(spId);

			if (AppUtils.isNotBlank(prod.getStoreSkuDto())) {// 如果商品有sku
				List<StoreSkuDto> skuDtos = prod.getStoreSkuDto();
				long stock = 0;
				for (StoreSkuDto storeSkuDto : skuDtos) {
					StoreSku storeSku = new StoreSku();
					storeSku.setSkuId(storeSkuDto.getSkuId());
					storeSku.setProdId(prod.getProductId());
					storeSku.setStoreId(prod.getStoreId());
					storeSku.setStock(storeSkuDto.getStock());
					storeSku.setStoreProdId(spId);
					Long result = storeSkuDao.saveStoreSku(storeSku);
					if (result > 0) {
						stock += storeSkuDto.getStock();
					}
				}
				storeProd.setStock(stock);
			}

			storeProdDao.saveStoreProd(storeProd, spId);
		} else { // 说明该商品还有有SKU未添加 可以再次添加
			StoreProd storeProd = storeProdDao.getStoreProd(prod.getId());
			if (storeProd == null) {
				return "门店商品不存在,添加失败";
			} else if (!storeProd.getStoreId().equals(prod.getStoreId())) {
				return "没有操作权限";
			}
			if (AppUtils.isNotBlank(prod.getStoreSkuDto())) {// 如果商品有sku
				List<StoreSkuDto> skuDtos = prod.getStoreSkuDto();
				long stock = storeProd.getStock();
				for (StoreSkuDto storeSkuDto : skuDtos) {
					StoreSku storeSku = new StoreSku();
					storeSku.setSkuId(storeSkuDto.getSkuId());
					storeSku.setProdId(prod.getProductId());
					storeSku.setStoreId(prod.getStoreId());
					storeSku.setStock(storeSkuDto.getStock());
					storeSku.setStoreProdId(prod.getId());
					Long result = storeSkuDao.saveStoreSku(storeSku);
					if (result > 0) {
						stock += storeSkuDto.getStock();
					}
				}
				storeProd.setStock(stock);
			}
			storeProdDao.updateStoreProd(storeProd);
		}
		return "OK";
	}

	/**
	 * 设置门店商品库存
	 */
	@Override
	public String setStock(StoreProductDto prod) {
		StoreProd origin = storeProdDao.getStoreProd(prod.getId());
		if (origin == null) {
			return "找不到ID为" + prod.getId() + "商品!";
		} else if (!origin.getStoreId().equals(prod.getStoreId())) {
			return "没有操作权限";
		}

		List<StoreSkuDto> skuDtos = prod.getStoreSkuDto();
		if (AppUtils.isNotBlank(skuDtos)) {// 如果有sku
			long stock = 0;
			for (StoreSkuDto storeSkuDto : skuDtos) {
				StoreSku sku = storeSkuDao.getStoreSku(storeSkuDto.getId());
				if (sku != null) {
					long newStock = storeSkuDto.getStock(); // 页面传过来的库存数
					sku.setStock(storeSkuDto.getStock()); //
					int result = storeSkuDao.updateStoreSku(sku);
					if (result > 0) {
						stock += storeSkuDto.getStock();
						continue;
					}
					stock += newStock;
				}
			}
			origin.setStock(stock);
		} else {
			origin.setStock(prod.getStock());
		}
		storeProdDao.updateStoreProd(origin);
		return "OK";
	}

	/**
	 * 删除商品
	 */
	@Override
	public String deleteProd(StoreProd storeProd) {
		// 删除商品关联的sku
		storeSkuDao.deleteStoreSku(storeProd.getStoreId(), storeProd.getProdId());
		storeProdDao.deleteStoreProd(storeProd);

		return Constants.SUCCESS;
	}

	/**
	 * 删除商品sku
	 */
	@Override
	public String deleteSku(StoreSku storeSku) {
		Long storeId = storeSku.getStoreId();
		Long prodId = storeSku.getProdId();

		StoreProd origin = storeProdDao.getStoreProd(storeId, prodId);

		if (null == origin) {
			return "该商品不存在或已被删除!";
		}

		if (storeSkuDao.getStoreSkuCount(storeId, prodId) == 1) {// 如果只剩最后一个SKU
			storeSkuDao.deleteStoreSku(storeSku);
			storeProdDao.deleteStoreProd(origin);
		} else {
			Long originStock = origin.getStock();
			Long skuStock = storeSku.getStock();
			origin.setStock(originStock - skuStock);
			storeProdDao.updateStoreProd(origin);

			storeSkuDao.deleteStoreSku(storeSku);
		}
		return Constants.SUCCESS;
	}

	/**
	 * 获取门店商品 
	 */
	@Override
	public StoreProd getStoreProd(Long storeId, Long prodId) {
		return storeProdDao.getStoreProd(storeId, prodId);
	}

	@Override
	public StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId) {
		return storeProdDao.getStoreProdByShopIdAndProdId(shopId, prodId);
	}

}
