/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ArticleCommentDao;
import com.legendshop.business.dao.ArticleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleComment;
import com.legendshop.spi.service.ArticleCommentService;
import com.legendshop.util.AppUtils;

/**
 * 文章服务实现类
 */
@Service("articleCommentService")
public class ArticleCommentServiceImpl implements ArticleCommentService {

	@Autowired
	private ArticleCommentDao articleCommentDao;

	@Autowired
	private ArticleDao articleDao;

	/**
	 * 根据Id获取
	 */
	public ArticleComment getArticleComment(Long id) {
		return articleCommentDao.getArticleComment(id);
	}

	/**
	 * 删除
	 */
	public void deleteArticleComment(ArticleComment articleComment) {
		// 删除文章评论
		articleCommentDao.deleteArticleComment(articleComment);
		// 修改评论数量
		articleDao.updateCommentNum(articleComment.getArtId());
	}

	/**
	 * 保存
	 */
	public Long saveArticleComment(ArticleComment articleComment) {
		if (!AppUtils.isBlank(articleComment.getId())) {
			updateArticleComment(articleComment);
			return articleComment.getId();
		}
		// 保存评论
		return articleCommentDao.saveArticleComment(articleComment);
	}

	/**
	 * 更新
	 */
	public void updateArticleComment(ArticleComment articleComment) {
		articleCommentDao.updateArticleComment(articleComment);
	}

	/**
	 * 根据评论id查询评论（评论、用户、文章连接）
	 */
	@Override
	public ArticleComment getArticleCommentLinkArt(Long id) {
		return articleCommentDao.getArticleCommentLinkArt(id);
	}

	/**
	 * @Description: TODO 修改文章评论状态
	 * @param articleComment 文章实体
	 * @param status 审核状态
	 */
	@Override
	public int updateStatus(ArticleComment articleComment, int status) {
		articleComment.setStatus(status);
		
		int res = articleCommentDao.update(articleComment);
		if (res  > 0) {
			// 修改评论数量
			articleDao.updateCommentNum(articleComment.getArtId());
		}
		return res;
	}

	@Override
	public PageSupport<ArticleComment> getArticleCommentPage(String curPageNO, ArticleComment articleComment) {
		return articleCommentDao.getArticleCommentPage(curPageNO,articleComment);
	}
	
	@Override
	public PageSupport<ArticleComment> queyArticleComment(String curPageNO,Long id) {
		return articleCommentDao.queyArticleComment(curPageNO,id);
	}
	
	@Override
	public PageSupport<ArticleComment> queyArticleCommentScroll(String curPageNO,Long id) {
		return articleCommentDao.queyArticleCommentScroll(curPageNO,id);
	}
}
