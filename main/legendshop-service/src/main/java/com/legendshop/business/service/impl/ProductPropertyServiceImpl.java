/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductPropertyDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.CustomPropertyDto;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.spi.service.ProductPropertyService;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 规格属性ServiceImpl.
 */
@Service("productPropertyService")
public class ProductPropertyServiceImpl  implements ProductPropertyService{

	@Autowired
    private ProductPropertyDao productPropertyDao;
    
	@Autowired
    private ProductPropertyValueService productPropertyValueService;


	public List<ProductProperty> getProductProperty(String userName) {
        return productPropertyDao.getProductProperty(userName);
    }

    public ProductProperty getProductProperty(Long id) {
        return productPropertyDao.getProductProperty(id);
    }

    public void deleteProductProperty(ProductProperty productProperty) {
        productPropertyDao.deleteProductProperty(productProperty);
    }

    public Long saveProductProperty(ProductProperty productProperty, String userId, Long shopId) {
        return  productPropertyDao.saveProductProperty(productProperty, userId, shopId);
    }

    public void updateProductProperty(ProductProperty productProperty) {
        productPropertyDao.updateProductProperty(productProperty);
    }

	@Override
	public List<ProductProperty> getProductPropertyList(Long proTypeId) {
		return productPropertyDao.getProductPropertyList(proTypeId);
	}

	@Override
	public List<ProductProperty> getParameterPropertyList(Long proTypeId) {
		return productPropertyDao.getParameterPropertyList(proTypeId);
	}

	@Override
	public PublishProductDto getProductProperty(Long sortId, Long nsortId, Long subsortId) {
		return productPropertyDao.getProductProperty(sortId, nsortId, subsortId);
	}

	@Override
	public ProductProperty saveCustomAttribute(String propertyName, String userName, int sequence) {

			ProductProperty newProperty = new ProductProperty();
			newProperty.setPropName(propertyName);//属性名
			newProperty.setMemo(propertyName);//别名
			newProperty.setIsMulti(false);//是否多选
			newProperty.setIsRequired(false);//是否必须
			newProperty.setSequence((long) sequence);//顺序
			newProperty.setStatus((short) 1);//状态
			newProperty.setType(0);//类型：文字；
			Date date = new Date();
			newProperty.setRecDate(date);
			newProperty.setModifyDate(date);
			newProperty.setIsRuleAttributes(1);//销售属性
			newProperty.setIsForSearch(false);//是否可以搜索
			newProperty.setIsInputProp(true);//是否输入属性
			newProperty.setUserName(userName);
			newProperty.setProdId(null);//商品ID
			newProperty.setIsCustom(1);//用户自定义
			Long propId = productPropertyDao.saveCustomProperty(newProperty);
			newProperty.setId(propId);
			return newProperty;
	}

	@Override
	public List<ProductProperty> queryUserProp(Long prodId, String userName) {
		List<ProductProperty> propList = productPropertyDao.queryUserPropByProdId(prodId);
		return propList;
	}

	/**
	 * 获取商品分类下的信息
	 */
	@Override
	public PublishProductDto queryProductProperty(Long categoryId) {
		return productPropertyDao.queryProductProperty(categoryId);
	}

	/**
	 *  根据 分类Id 查找对应的属性
	 *  用于商品列表动态属性
	 */
	@Override
	public List<ProductPropertyDto> findProductProper(Long categoryId) {
		return productPropertyDao.findProductProper(categoryId);
	}

	@Override
	public List<ProductProperty> queryUserPropByProdId(Long prodId) {
		return productPropertyDao.queryUserPropByProdId(prodId);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes, String propName, String memo, Long proTypeId) {
		return productPropertyDao.getProductPropertyPage(curPageNO,isRuleAttributes,propName,memo,proTypeId);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyParamPage(String curPageNO, Integer isRuleAttributes, String propName, String memo, Long proTypeId) {
		return productPropertyDao.getProductPropertyParamPage(curPageNO,isRuleAttributes,propName,memo,proTypeId);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes) {
		return productPropertyDao.getProductPropertyPage(curPageNO,isRuleAttributes);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes, ProductProperty productProperty) {
		return productPropertyDao.getProductPropertyPage(curPageNO,isRuleAttributes,productProperty);
	}

	@Override
	public PageSupport<ProductProperty> getProductPropertyByPage(String curPageNO, Integer isRuleAttributes, ProductProperty productProperty) {
		return productPropertyDao.getProductPropertyByPage(curPageNO,isRuleAttributes,productProperty);
	}

	@Override
	public Integer findProdId(Long propId) {
		return productPropertyDao.findProdId(propId);
	}

	@Override
	public List<ResultCustomPropertyDto> applyProperty(String originUserProperties, String userName, String customAttribute) {
		//1. 要返回的属性和属性值结果
		List<ResultCustomPropertyDto> resultCustomPropertyDtoList = new ArrayList<ResultCustomPropertyDto>();

		//1.1. 如果该商品已有 用户自定义属性了，也需取出并加入
		if (AppUtils.isNotBlank(originUserProperties)) {
			List<ResultCustomPropertyDto> originPropList = JSONUtil.getArray(originUserProperties, ResultCustomPropertyDto.class);
			if (AppUtils.isNotBlank(originPropList)) {
				resultCustomPropertyDtoList.addAll(originPropList);
			}
		}

		//2. 客户新加的自定义属性
		List<CustomPropertyDto> customPropertyDtoList = JSONUtil.getArray(customAttribute, CustomPropertyDto.class);
		if (AppUtils.isNotBlank(customPropertyDtoList)) {
			for (int i = 0; i < customPropertyDtoList.size(); i++) {
				String propertyName = customPropertyDtoList.get(i).getKey();
				List<String> propValue = customPropertyDtoList.get(i).getValue();

				if (AppUtils.isNotBlank(propertyName) && AppUtils.isNotBlank(propValue)) {

					//2.2 获取新的属性的ID值并返回
					ProductProperty customProperty = this.saveCustomAttribute(propertyName, userName, i);
					List<ProductPropertyValue> newValueList = productPropertyValueService.saveCustomAttributeValues(customProperty.getId(), propValue);

					ResultCustomPropertyDto resultCustomPropertyDto = new ResultCustomPropertyDto();
					resultCustomPropertyDto.setProductProperty(customProperty);
					resultCustomPropertyDto.setPropertyValueList(newValueList);
					resultCustomPropertyDtoList.add(resultCustomPropertyDto);
				}
			}

		}
		return resultCustomPropertyDtoList;
	}
}
