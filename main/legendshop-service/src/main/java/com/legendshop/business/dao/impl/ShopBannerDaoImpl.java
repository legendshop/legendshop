/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopBannerDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopBanner;

/**
 * 商家默认的轮换图Dao
 *
 */
@Repository
public class ShopBannerDaoImpl extends GenericDaoImpl<ShopBanner, Long> implements ShopBannerDao  {

	public ShopBanner getShopBanner(Long id){
		return getById(id);
	}
	
	public List<ShopBanner>  getShopBannerByShopId(Long shopId){
		List<ShopBanner> banners= queryByProperties(new EntityCriterion().eq("shopId", shopId).addAscOrder("seq"));
		return banners;
	}
	
	@Caching(evict={@CacheEvict(value="ShopBannerList",key="#shopBanner.shopId")
	,@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopBanner.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopBanner.shopId")})
    public int deleteShopBanner(ShopBanner shopBanner){
    	return delete(shopBanner);
    }
	
	@Caching(evict={@CacheEvict(value="ShopBannerList",key="#shopBanner.shopId")
	,@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopBanner.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopBanner.shopId")})
	public Long saveShopBanner(ShopBanner shopBanner){
		return save(shopBanner);
	}
	
	@Caching(evict={@CacheEvict(value="ShopBannerList",key="#shopBanner.shopId")
	,@CacheEvict(value="ShopDecotateDto",key="'ShopDecotate_'+#shopBanner.shopId")
	,@CacheEvict(value="OnlineShopDecotateDto",key="'OnlineShopDecotate_'+#shopBanner.shopId")})
	public int updateShopBanner(ShopBanner shopBanner){
		return update(shopBanner);
	}
	
	@Override
	public PageSupport<ShopBanner> getShopBanner(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ShopBanner.class, curPageNO);
    	cq.eq("shopId", shopId);
        cq.addAscOrder("seq");
		return queryPage(cq);
	}
	
 }
