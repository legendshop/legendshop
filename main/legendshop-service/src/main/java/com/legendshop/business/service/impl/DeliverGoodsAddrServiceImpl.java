/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.DeliverGoodsAddrDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliverGoodsAddr;
import com.legendshop.spi.service.DeliverGoodsAddrService;
import com.legendshop.util.AppUtils;

/**
 * 发货地址服务.
 */
@Service("deliverGoodsAddrService")
public class DeliverGoodsAddrServiceImpl implements DeliverGoodsAddrService {

	@Autowired
	private DeliverGoodsAddrDao deliverGoodsAddrDao;

	public List<DeliverGoodsAddr> getDeliverGoodsAddrList(Long shopId) {
		return deliverGoodsAddrDao.getDeliverGoodsAddrList(shopId);
	}

	public DeliverGoodsAddr getDeliverGoodsAddr(Long id) {
		return deliverGoodsAddrDao.getDeliverGoodsAddr(id);
	}

	public void deleteDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr) {
		deliverGoodsAddrDao.deleteDeliverGoodsAddr(deliverGoodsAddr);
	}

	public Long saveDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr, Long shopId) {
		if (!AppUtils.isBlank(deliverGoodsAddr.getAddrId())) {
			updateUserAddress(deliverGoodsAddr, shopId);
			return deliverGoodsAddr.getAddrId();
		}
		return deliverGoodsAddrDao.saveDeliverGoodsAddr(deliverGoodsAddr);
	}

	public void updateUserAddress(DeliverGoodsAddr userAddress, Long shopId) {
		DeliverGoodsAddr origin = deliverGoodsAddrDao.getById(userAddress.getAddrId());
		if (origin.getShopId().equals(shopId)) {
			origin.setAreaId(userAddress.getAreaId());
			origin.setCityId(userAddress.getCityId());
			origin.setMobile(userAddress.getMobile());
			origin.setProvinceId(userAddress.getProvinceId());
			origin.setContactName(userAddress.getContactName());
			origin.setAdds(userAddress.getAdds());
			deliverGoodsAddrDao.update(origin);
		}
	}

	public void updateDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr) {
		deliverGoodsAddrDao.updateDeliverGoodsAddr(deliverGoodsAddr);
	}

	@Override
	public Long getMaxNumber(Long shopId) {
		return deliverGoodsAddrDao.getLongResult("select count(addr_id) from ls_deliver_goods_addr where shop_id=? ", shopId);
	}

	@Override
	public DeliverGoodsAddr getDefaultDeliverGoodsAddr(Long shopId) {
		String sql = "SELECT ls_deliver_goods_addr.* " + "FROM ls_deliver_goods_addr " + "WHERE shop_id = ? AND is_default = 1 "
				+ "ORDER BY create_time DESC LIMIT 0,1";
		return deliverGoodsAddrDao.get(sql, DeliverGoodsAddr.class, shopId);
	}

	@Override
	public void updateDefaultDeliverAddress(Long addrId, Long shopId) {
		// 1.更新原来的收货地址
		deliverGoodsAddrDao.update("update ls_deliver_goods_addr set is_default = 0 where is_default = 1  and shop_id=?", shopId);
		// 2.更新新的收货地址
		deliverGoodsAddrDao.update("update ls_deliver_goods_addr set is_default = 1 where addr_id = ?  and shop_id = ? ", addrId, shopId);
	}

	@Override
	public void turnonDefault(Long id, Long shopId) {
		deliverGoodsAddrDao.turnonDefault(id, shopId);
	}

	@Override
	public void closeDefault(Long id, Long shopId) {
		deliverGoodsAddrDao.closeDefault(id, shopId);
	}

	@Override
	public void deleteDeliverGoodsAddr(Long addrId, Long shopId) {
		deliverGoodsAddrDao.deleteDeliverGoodsAddr(addrId, shopId);
	}

	@Override
	public DeliverGoodsAddr getDeliverGoodsAddr(Long shopId, int i) {

		return deliverGoodsAddrDao.getDefultDeliverGoodsAddr(shopId, i);
	}

	@Override
	public PageSupport<DeliverGoodsAddr> getDeliverGoodsAddrPage(String curPageNO, Long shopId) {
		return deliverGoodsAddrDao.getDeliverGoodsAddrPage(curPageNO, shopId);
	}
}
