/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinMpTokenDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMpToken;

/**
 * The Class WeixinMpTokenDaoImpl.
 */
@Repository
public class WeixinMpTokenDaoImpl extends GenericDaoImpl<WeixinMpToken, Long> implements WeixinMpTokenDao  {
     
	public WeixinMpToken getWeixinMpToken(Long id){
		return getById(id);
	}
	
    public int deleteWeixinMpToken(WeixinMpToken weixinMpToken){
    	return delete(weixinMpToken);
    }
	
	public Long saveWeixinMpToken(WeixinMpToken weixinMpToken){
		return save(weixinMpToken);
	}
	
	public int updateWeixinMpToken(WeixinMpToken weixinMpToken){
		return update(weixinMpToken);
	}
	
	public PageSupport<WeixinMpToken> getWeixinMpToken(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public WeixinMpToken getWeixinMpToken() {
		
		return null;
	}
	
 }
