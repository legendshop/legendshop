/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloor;

/**
 * 手机端html5 首页楼层服务.
 */
public interface MobileFloorDao extends Dao<MobileFloor, Long> {
     
	public abstract MobileFloor getMobileFloor(Long id);
	
    public abstract int deleteMobileFloor(MobileFloor mobileFloor);
	
	public abstract Long saveMobileFloor(MobileFloor mobileFloor);
	
	public abstract int updateMobileFloor(MobileFloor mobileFloor);
	
	public List<MobileFloor> queryMobileFloor(Long shopId);

	public abstract PageSupport<MobileFloor> getMobileFloorPage(String curPageNO, MobileFloor mobileFloor);
 }
