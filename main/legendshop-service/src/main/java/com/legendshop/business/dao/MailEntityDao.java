/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.MailEntity;

/**
 * 邮件Dao.
 */

public interface MailEntityDao extends GenericDao<MailEntity, Long>{

	public abstract MailEntity getMailEntity(Long id);
	
    public abstract void deleteMailEntity(MailEntity mailEntity);
	
	public abstract Long saveMailEntity(MailEntity mailEntity);
	
	public abstract void updateMailEntity(MailEntity mailEntity);
	
	MailEntity getMailEntity(String receiveMail, MailCategoryEnum type);

	public abstract void clearMailCode(Long id);

	public abstract PageSupport<MailEntity> getMailEntityPage(String curPageNO);
	
 }
