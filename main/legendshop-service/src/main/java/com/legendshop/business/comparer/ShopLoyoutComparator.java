/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.comparer;

import java.io.Serializable;
import java.util.Comparator;

import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;


/**
 * The Class MenuComparator.
 */
public 	class ShopLoyoutComparator implements Comparator<ShopLayoutDto>, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7171985873166798645L;

	
	
	public int compare(ShopLayoutDto o1, ShopLayoutDto o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		}else if(o1.getSeq().equals(o2.getSeq())){
			return 0;
		} else if (o1.getSeq() < o2.getSeq()) {
			return -1;
		} else {
			return 1;
		}
	}
	
	
}