/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinTexttemplate;

/**
 * 微信文字模板服务
 */
public interface WeixinTexttemplateDao extends Dao<WeixinTexttemplate, Long> {

	public abstract WeixinTexttemplate getWeixinTexttemplate(Long id);
	
    public abstract int deleteWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);
	
	public abstract Long saveWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);
	
	public abstract int updateWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);
	
 }
