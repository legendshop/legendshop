/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.Product;
import com.legendshop.model.prod.AppProdDto;
import com.legendshop.model.prod.AppProdListDto;
import com.legendshop.model.prod.AppProdSearchParms;
import com.legendshop.spi.service.AppShopProdManageService;
import com.legendshop.util.AppUtils;

/**
 * 直播查询商品接口.
 */
@Service("appShopProdManageService")
public class AppShopProdManageServiceImpl  implements AppShopProdManageService{

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private ProductDao productDao;

	@Override
	public AppProdListDto getAppProdList(AppProdSearchParms searchParms,Long shopId) {  
		CriteriaQuery cq = new CriteriaQuery(Product.class, String.valueOf(searchParms.getCurPageNO()));
		cq.eq("shopId", shopId);
		cq.setPageSize(10);
		cq.eq("categoryId", searchParms.getCategoryId());
		cq.like("name", searchParms.getKeyword(),MatchMode.ANYWHERE);
		cq.eq("status", searchParms.getStatus());
		cq.addDescOrder("modifyDate");
		PageSupport ps=productDao.queryPage(cq);
		return convertPageSupportDto(ps);
	}
	
	@Override
	public AppProdListDto getAppProdListByRoomnum(AppProdSearchParms searchParms, Integer roomnum, String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, systemParameterUtil.getPageSize(),
				curPageNO);
		QueryMap map = new QueryMap();
		map.put("roomnum", roomnum);
		String queryAllSQL = ConfigCode.getInstance().getCode("app.getProdByRoomidCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.getProdByRoomid", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		PageSupport ps = productDao.querySimplePage(query);
		return convertPageSupportDto(ps);
	}


	
	@SuppressWarnings("unchecked")
	private AppProdListDto convertPageSupportDto(PageSupport ps){
		if(AppUtils.isNotBlank(ps)){
			AppProdListDto appProdListDto = new AppProdListDto();
			appProdListDto.setPageCount(ps.getPageCount());
			appProdListDto.setCurrPage(ps.getCurPageNO());
			appProdListDto.setTotalCount(ps.getTotal());
			if(AppUtils.isNotBlank(ps.getResultList())){
				List<AppProdDto> prodList = new ArrayList<AppProdDto>();
				List<Product> productList = (List<Product>) ps.getResultList();
				for (Product productDetail : productList) {
					AppProdDto dto = convertProductListDto(productDetail);
					prodList.add(dto);
				}
				
				appProdListDto.setResultList(prodList);
			}
			return appProdListDto;
		}
		return null;
	}
	
	
	private AppProdDto convertProductListDto(Product product){
		if(AppUtils.isNotBlank(product)){
			AppProdDto appProdDto = new AppProdDto();
			appProdDto.setPrice(product.getCash());
			appProdDto.setProdId(product.getProdId());
			appProdDto.setName(product.getName());
			appProdDto.setPic(product.getPic());
			appProdDto.setBuys(product.getBuys());
			appProdDto.setStocks(product.getStocks());
			appProdDto.setStatus(product.getStatus());
			StringBuffer url = new StringBuffer();
			url.append(propertiesUtil.getMobileDomainName()).append("/views/").append(product.getProdId());
			appProdDto.setShareUrl(url.toString());
			return appProdDto;
		}
		return null;
	}


	
}
