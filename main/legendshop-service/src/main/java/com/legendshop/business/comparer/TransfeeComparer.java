package com.legendshop.business.comparer;

import java.util.List;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.util.AppUtils;

public class TransfeeComparer implements DataComparer<Transfee,Transfee>{

	@Override
	public boolean needUpdate(Transfee newTransfee, Transfee originTransfee, Object obj) {
		int number = 0;
		
		originTransfee.setTransCityList(newTransfee.getTransCityList());
		originTransfee.setCityNameList(newTransfee.getCityNameList());
		originTransfee.setCity(newTransfee.getCity());
		originTransfee.setCityId(newTransfee.getCityId());
		
		//这里会有问题
		/*if(isChage(newTransfee.getCityIdList(),originTransfee.getCityIdList())){
			originTransfee.setCityIdList(newTransfee.getCityIdList());
			number ++; 
		}*/
		/** 已改：判断 两个list 是否有变化 **/
		if(AppUtils.isNotBlank(newTransfee.getCityIdList()) && AppUtils.isNotBlank(originTransfee.getCityIdList())){
			if(!compare(newTransfee.getCityIdList(),originTransfee.getCityIdList())){
				originTransfee.setCityIdList(newTransfee.getCityIdList());
				number ++;
			}
		}else if(AppUtils.isBlank(newTransfee.getCityIdList()) && AppUtils.isNotBlank(originTransfee.getCityIdList())){
			originTransfee.setCityIdList(newTransfee.getCityIdList());
			number ++;
		}else if(AppUtils.isNotBlank(newTransfee.getCityIdList()) && AppUtils.isBlank(originTransfee.getCityIdList())){
			originTransfee.setCityIdList(newTransfee.getCityIdList());
			number ++;
		}
		
		
		if(isChage(newTransfee.getFreightMode(), originTransfee.getFreightMode())){
			originTransfee.setFreightMode(newTransfee.getFreightMode());
			number ++; 
		}
		
		if(isChage(newTransfee.getTransWeight(), originTransfee.getTransWeight())){
			originTransfee.setTransWeight(newTransfee.getTransWeight());
			number ++; 
		}
		
		if(isChage(newTransfee.getTransAddWeight(), originTransfee.getTransAddWeight())){
			originTransfee.setTransAddWeight(newTransfee.getTransAddWeight());
			number ++; 
		}
		
		if(isChage(newTransfee.getTransFee(), originTransfee.getTransFee())){
			originTransfee.setTransFee(newTransfee.getTransFee());
			number ++;
		}
		
		if(isChage(newTransfee.getTransAddFee(), originTransfee.getTransAddFee())){
			originTransfee.setTransAddFee(newTransfee.getTransAddFee());
			number ++;
		}
		
		return number>0;
	}

	@Override
	public boolean isExist(Transfee newTransfee, Transfee originTransfee) {
		
		if(AppUtils.isNotBlank(newTransfee) && AppUtils.isNotBlank(originTransfee)){
			
			if(originTransfee.getTransportId().equals(newTransfee.getTransportId()) &&
					originTransfee.getId().equals(newTransfee.getId())){
				return true;
			}
			
		}
		return false;
	}

	@Override
	public Transfee copyProperties(Transfee newTransfee, Object obj) {
		Transport tranport = (Transport)obj;
		Transfee tf = new Transfee();
		
		tf.setTransportId(tranport.getId());
		tf.setFreightMode(newTransfee.getFreightMode());
		tf.setTransFee(newTransfee.getTransFee());
		tf.setTransAddFee(newTransfee.getTransAddFee());
		tf.setTransWeight(newTransfee.getTransWeight());
		tf.setTransAddWeight(newTransfee.getTransAddWeight());
		tf.setStatus(Constants.ONLINE);
		
		tf.setTransCityList(newTransfee.getTransCityList());
		tf.setCityIdList(newTransfee.getCityIdList());
		tf.setCityNameList(newTransfee.getCityNameList());
		tf.setCity(newTransfee.getCity());
		tf.setCityId(newTransfee.getCityId());
		
		return tf;
	}

	/** 是否有改变 **/
	private  boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 队列比较
	 * @param <T>
	 * @param a
	 * @param b
	 * @return
	 */
	public static <T extends Comparable<T>> boolean compare(List<T> a, List<T> b) {
	    if(a.size() != b.size())
	        return false;
	    
	   return a.containsAll(b);
	}
}
