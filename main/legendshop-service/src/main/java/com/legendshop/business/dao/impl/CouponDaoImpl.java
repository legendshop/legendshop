/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CouponDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.UserCouponEnum;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.util.AppUtils;

/**
 * 优惠券Dao.
 */
@Repository
public class CouponDaoImpl extends GenericDaoImpl<Coupon, Long> implements CouponDao {

	private static final String queryCanUsedCoupons = "select c.*,uc.coupon_sn couponSn,uc.user_coupon_id couponUsrId from ls_coupon c, ls_user_coupon uc where c.coupon_id=uc.coupon_id "
			+ " and uc.use_status = 1 and c.status=1 and uc.user_id=? and c.shop_id=? "
			+ " and c.start_date<? and c.end_date>? and c.full_price<=? order by c.off_price desc";

	@Override
	public List<Coupon> getCouponByShopId(Long shopId) {
		return super.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("status", "1"));
	}

	@Override
	public Coupon getCoupon(Long id) {
		return getById(id);
	}

	@Override
	public int deleteCoupon(Coupon coupon) {
		return delete(coupon);
	}

	@Override
	public Long saveCoupon(Coupon coupon) {
		return save(coupon);
	}

	@Override
	public int updateCoupon(Coupon coupon) {
		return update(coupon);
	}

	@Override
	public PageSupport getCoupon(CriteriaQuery cq) {
		return queryPage(cq);
	}

	@Override
	public PageSupport<PersonalCouponDto> query(SimpleSqlQuery query) {
		return this.querySimplePage(query);
	}
	
	@Override
	public PageSupport<Coupon> queryCoupon(SimpleSqlQuery query) {
		return this.querySimplePage(query);
	}

	@Override
	public List<Coupon> queryByProperties(EntityCriterion entityCriterion) {
		return queryByProperties(entityCriterion);
	}

	@Override
	public void betchUpdate(List<Coupon> list) {
		update(list);
	}

	@Override
	public List<Coupon> queryCanUsedCoupons(String userId, Double orderTotalAmount, Long shopId) {
		return this.query(queryCanUsedCoupons, Coupon.class, userId, shopId, new Date(), new Date(), orderTotalAmount);
	}

	/**
	 * 增加优惠券的使用次数
	 */
	@Override
	@Caching(evict = { @CacheEvict(value = "UnuseCouponCount", key = "#userId + 'platform'"), @CacheEvict(value = "UnuseCouponCount", key = "#userId + 'shop'") })
	public void addCouponUseNum(Long couponId, String userId) {
		this.update("update ls_coupon set use_coupon_number=use_coupon_number+1 where coupon_id=?", couponId);
	}

	@Override
	@Caching(evict = { @CacheEvict(value = "UnuseCouponCount", key = "#userId + 'platform'"), @CacheEvict(value = "UnuseCouponCount", key = "#userId + 'shop'") })
	public void reduceCouponUseNum(Long couponId, String userId) {
		this.update("update ls_coupon set use_coupon_number=use_coupon_number-1 where coupon_id=?", couponId);
	}

	@Override
	public UserCoupon getUserCoupon(Long couponId, String userId) {
		String sql = "select * from ls_user_coupon where coupon_id = ? and user_id = ?";
		return get(sql, UserCoupon.class, couponId, userId);
	}

	@Override
	public List<Coupon> getCouponByProvider(String getType, String provider, Date date) {
		String sql;
		if (provider.equals(CouponProviderEnum.SHOP.value())) {
			sql = "SELECT c.*,s.site_name as shopName FROM ls_coupon c,ls_shop_detail s WHERE c.status=1 AND c.is_dsignated_user = 0 AND c.shop_id=s.shop_id AND c.get_type=? AND c.coupon_provider=? AND c.end_date>? ORDER BY c.bind_coupon_number desc";
		} else {
			sql = "SELECT c.* FROM ls_coupon c left join ls_category cp on c.category_id=cp.id where  c.status=1 AND c.get_type=? AND c.coupon_provider=? AND c.end_date>? ORDER BY c.bind_coupon_number desc";
		}
		return this.queryLimit(sql, Coupon.class, 0, 6, getType, provider, date);
	}

	@Override
	public PageSupport<Coupon> getCouponsPage(String curPageNO, String type) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class,12 , curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		String querySQL;
		String queryAllSQL;
		map.put("getType", CouponGetTypeEnum.FREE.value());
		map.put("endDate", nowDate);
		if (type.equals(CouponProviderEnum.PLATFORM.value())) {
			map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByCategory", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByCategory", map);
		} else {
			map.put("couponProvider", CouponProviderEnum.SHOP.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByShop", map);
		}
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		return querySimplePage(q);
	}

	@Override
	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(String userId, int sizePage, String curPageNO,
			Integer useStatus, String couPro) {
		SimpleSqlQuery query = new SimpleSqlQuery(PersonalCouponDto.class, sizePage, curPageNO);
		QueryMap map = new QueryMap();
		// 默认为打开可使用状态
		if (useStatus == null) {
			useStatus = 1;
		}

		map.put("userId", userId);
		map.put("couponProvider", couPro);
		query.setParam(map.toArray());

		if ("shop".equals(couPro)) { // 查看优惠券
			if (useStatus == 1) {
				// 查询未使用未过期的优惠券
				query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUnuseCouponCount", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseCoupon", map));
			} else if (useStatus == 2) {
				// 查询已使用的优惠券
				query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseCouponCount", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseCoupon", map));
			} else {
				// 查询未使用已过期的优惠券
				query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeCouponCount", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeCoupon", map));
			}
		} else { // 查看红包
			// 查询未使用未过期的红包
			if (useStatus == 1) {
				query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryCountUnuseRedPacket", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseRedPacket", map));
			} else if (useStatus == 2) {
				// 查询已使用的红包
				query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseRedPacketCount", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseRedPacket", map));
			} else {
				// 查询未使用已过期的红包
				query.setAllCountString(
						ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeRedPacketCount", map));
				query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeRedPacket", map));
			}
		}
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Coupon> queryCouponPage(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(Coupon.class, 6, curPageNO);
		QueryMap map = new QueryMap();
		/** 可以使用优惠券：标记为1，活动结束时间大于等于当前时间 **/
		map.put("userName", userName);
		map.put("useStatus", UserCouponEnum.UNUSED.value());
		map.put("enabletime", new Date());
		map.put("status", Constants.ONLINE.intValue());

		String queryAllSQL = ConfigCode.getInstance().getCode("uc.coupon.queryCouponCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.coupon.queryCoupon", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Coupon> queryCoupon(StringBuffer sql, String curPageNO, StringBuffer sqlCount,
			List<Object> objects) {
		SimpleSqlQuery hqlQuery = new SimpleSqlQuery(Coupon.class, 10, curPageNO == null ? "1" : curPageNO);
		hqlQuery.setQueryString(sql.toString());
		hqlQuery.setAllCountString(sqlCount.toString());
		hqlQuery.setParam(objects.toArray());
		return querySimplePage(hqlQuery);
	}

	@Override
	public PageSupport<Coupon> getIntegralCenterList(String curPageNO, String type) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		String querySQL;
		String queryAllSQL;
		map.put("getType", CouponGetTypeEnum.FREE.value());
		map.put("endDate", nowDate);
		if (type.equals(CouponProviderEnum.PLATFORM.value())) {
			map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByCategory", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByCategory", map);
		} else {
			map.put("couponProvider", CouponProviderEnum.SHOP.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByShop", map);
		}
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		return querySimplePage(q);
	}

	@Override
	public PageSupport<Coupon> getIntegralList(String curPageNO, String type, String shopId) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		String querySQL;
		String queryAllSQL;
		map.put("getType", CouponGetTypeEnum.FREE.value());
		if (AppUtils.isNotBlank(shopId)) {
			map.put("shopId", shopId);
		}
		map.put("endDate", nowDate);
		if (type.equals(CouponProviderEnum.PLATFORM.value())) {
			map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByCategory", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByCategory", map);
		} else {
			map.put("couponProvider", CouponProviderEnum.SHOP.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByShop", map);
		}
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		return querySimplePage(q);
	}

	@Override
	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(int sizePage, String curPageNO, String userId,
			String couPro) {
		SimpleSqlQuery query = new SimpleSqlQuery(PersonalCouponDto.class, sizePage, curPageNO);
		QueryMap map = new QueryMap();

		// 查询未使用未过期的优惠券
		map.put("userId", userId);
		map.put("couponProvider", couPro);
		query.setParam(map.toArray());

		if ("shop".equals(couPro)) { // 查看优惠券
			query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUnuseCouponCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseCoupon", map));
		} else { // 查看红包
			query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryCountUnuseRedPacket", map));
			query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseRedPacket", map));
		}
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Coupon> queryCoupon(String curPageNO, Coupon coupon, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(Coupon.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("couponProvider", CouponProviderEnum.SHOP.value());
		map.put("couponType", CouponTypeEnum.CATEGORY.value());
		if (AppUtils.isNotBlank(coupon.getShopId())) {
			map.put("shopId", coupon.getShopId());
		}
		if (AppUtils.isNotBlank(coupon.getCouponName())) {
			map.like("couponName", "%" + coupon.getCouponName().trim() + "%");
		}
		// 判断支持外部排序
		if (result.isSortExternal()) {
			map.put(AttributeKeys.ORDER_INDICATOR, result.parseSeq());
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryCouponList", map);
		String countSQL = ConfigCode.getInstance().getCode("coupon.countCouponList", map);
		query.setAllCountString(countSQL);
		query.setQueryString(queryAllSQL);
		map.remove(AttributeKeys.ORDER_INDICATOR);// 去掉排序的条件
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<UserCoupon> queryUsecouponView(String curPageNO, String sqlString, StringBuilder sqlCount,
			List<Object> objects) {
		SimpleSqlQuery hqlQuery = new SimpleSqlQuery(UserCoupon.class, 30, curPageNO == null ? "1" : curPageNO);
		hqlQuery.setQueryString(sqlString);
		hqlQuery.setAllCountString(sqlCount.toString());
		hqlQuery.setParam(objects.toArray());
		return querySimplePage(hqlQuery);
	}

	@Override
	public PageSupport<Coupon> getPlatform(String curPageNO, Coupon coupon) {
		CriteriaQuery cq = new CriteriaQuery(Coupon.class, curPageNO);
		if (AppUtils.isNotBlank(coupon.getCouponName())) {
			cq.like("couponName", "%" + coupon.getCouponName().trim() + "%");
		}
		cq.addDescOrder("createDate");
		cq.eq("couponProvider", CouponProviderEnum.PLATFORM.value());
		return queryPage(cq);
	}

	@Override
	public int shopDeleteCoupon(Long couponId) {
		String sql = "update ls_coupon set shop_del =1 where coupon_id=?";
		return update(sql, couponId);
	}

	@Override
	public long getCouponCountByShopId(String type, String shopId) {
		Date date = new Date();
		String getType = CouponGetTypeEnum.FREE.value();
		if(CouponProviderEnum.PLATFORM.value().equals(type)) {
			return this.getLongResult("SELECT COUNT(1) FROM ls_coupon WHERE shop_del=0 AND status=1 AND coupon_provider=? AND shop_id=? AND end_date> ? AND get_Type=?",
					 type, shopId, date, getType);
		} else if(CouponProviderEnum.SHOP.value().equals(type)) {
			return this.getLongResult("SELECT COUNT(1) FROM ls_coupon WHERE shop_del=0 AND status=1 AND coupon_provider=? AND shop_id=? AND end_date> ? AND get_Type=?",
					 type, shopId, date, getType);
		} else {
			return this.getLongResult("SELECT COUNT(1) FROM ls_coupon WHERE shop_del=0 AND status=1 AND shop_id=? AND end_date> ? AND get_Type=?",
					 shopId, date, getType);
		}
	}

	@Override
	public Integer batchDelete(String platfromIds) {
		String[] platfromIdList=platfromIds.split(",");
		StringBuffer buff=new StringBuffer();
		buff.append("DELETE FROM ls_coupon WHERE coupon_id IN(");
		for(String platfromId:platfromIdList){
			buff.append("?,");
		}
		buff.deleteCharAt(buff.length()-1);
		buff.append(")");
		return update(buff.toString(), platfromIdList);
	}

	@Override
	public int batchOffline(String platfromIds) {
		String[] platfromIdList=platfromIds.split(",");
		StringBuffer buff=new StringBuffer();
		buff.append("UPDATE ls_coupon SET status=0 WHERE coupon_id IN(");
		for(String platfromId:platfromIdList){
			buff.append("?,");
		}
		buff.deleteCharAt(buff.length()-1);
		buff.append(")");
		return update(buff.toString(), platfromIdList);
	}

	@Override
	public PageSupport<Coupon> loadSelectCouponByDraw(String curPageNO, String couponName, Date endDate, List<Long> couponIds) {
		SimpleSqlQuery query = new SimpleSqlQuery(Coupon.class, 5, curPageNO);
		QueryMap map = new QueryMap();
		map.like("couponName", couponName);
		map.put("endDate", endDate);
		
		if(AppUtils.isNotBlank(couponIds) && couponIds.size() > 0) {
			StringBuffer idStr = new StringBuffer();
			idStr.append("(");
			for(int i = 0; i < couponIds.size(); i++) {
				if(i == couponIds.size() -1) {
					idStr.append(couponIds + ")");
				}else {
					idStr.append(couponIds + ",");
				}
			} 
			map.put("idStr", idStr);
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("coupon.getSelectCouponByDraw", map);
		String countSQL = ConfigCode.getInstance().getCode("coupon.getSelectCouponByDrawCount", map);
		query.setAllCountString(countSQL);
		query.setQueryString(queryAllSQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public List<Coupon> getProdsCoupons(Long shopId, Long prodId) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  c.*,s.site_name AS shopName ")
		.append("FROM ls_coupon c ")
		.append("INNER JOIN ls_shop_detail s ON c.shop_id = s.shop_id  ")
		.append("LEFT JOIN ls_coupon_prod cp ON c.coupon_id = cp.coupon_id ")
		.append("WHERE c.status = 1 AND c.is_dsignated_user = 0 AND c.get_type = ? ")
		.append("AND c.shop_id = ? AND c.end_date > ? AND c.coupon_provider = ? ")
		.append("AND (cp.prod_id = ? OR cp.prod_id IS NULL) ")
		.append("ORDER BY c.off_price DESC ");
		return query(sql.toString(), Coupon.class, CouponGetTypeEnum.FREE.value(), shopId, new Date(), CouponProviderEnum.SHOP.value(), prodId);
	}

	@Override
	public Coupon getCouponByCouponId(Long couponId) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT lc.*, ld.site_name AS siteName ")
		.append("FROM ls_coupon lc, ls_shop_detail ld ")
		.append("WHERE lc.shop_id = ld.shop_id AND lc.status = 1 ")
		.append("AND lc.start_date < ? AND lc.end_date > ? ")
		.append("AND lc.coupon_id = ? ");
		return get(sql.toString(), Coupon.class, new Date(), new Date(), couponId);
	}

	/** 
	 * 获取平台发放跟店铺相关的优惠券
	 */
	@Override
	public List<Coupon> getPlatformCoupons(Long shopId) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT  c.*,s.site_name AS shopName ")
		.append("FROM ls_coupon c ")
		.append("LEFT JOIN ls_shop_detail s ON c.shop_id = s.shop_id  ")
		.append("WHERE c.status = 1 AND c.is_dsignated_user = 0 AND c.get_type = ? ")
		.append("AND (c.shop_id = ? OR c.shop_id IS NULL) AND c.end_date > ? AND c.coupon_provider = ? ")
		.append("ORDER BY c.off_price DESC ");
		return query(sql.toString(), Coupon.class, CouponGetTypeEnum.FREE.value(), shopId, new Date(), CouponProviderEnum.PLATFORM.value());
	}

}
