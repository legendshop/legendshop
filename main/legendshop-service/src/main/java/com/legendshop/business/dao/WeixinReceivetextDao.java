/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinReceivetext;

/**
 * 微信接受的文字Dao
 */
public interface WeixinReceivetextDao extends Dao<WeixinReceivetext, Long> {
     
	public abstract WeixinReceivetext getWeixinReceivetext(Long id);
	
    public abstract int deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext);
	
	public abstract Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext);
	
	public abstract int updateWeixinReceivetext(WeixinReceivetext weixinReceivetext);
	
 }
