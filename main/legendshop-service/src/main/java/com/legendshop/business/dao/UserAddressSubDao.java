/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.UserAddressSub;

/**
 *用户配送地址
 */
public interface UserAddressSubDao extends Dao<UserAddressSub, Long> {
     
	public abstract UserAddressSub getUserAddressSub(Long id);
	
    public abstract int deleteUserAddressSub(UserAddressSub userAddressSub);
	
	public abstract Long saveUserAddressSub(UserAddressSub userAddressSub);
	
	public abstract int updateUserAddressSub(UserAddressSub userAddressSub);
	
	public abstract UserAddressSub getUserAddressSub(Long addrId, Long version);

	public abstract UserAddressSub getUserAddressSubForOrderDetail(Long id);
	
 }
