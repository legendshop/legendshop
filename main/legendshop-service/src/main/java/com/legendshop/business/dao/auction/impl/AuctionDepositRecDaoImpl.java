/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.auction.impl;

import java.util.List;

import com.legendshop.dao.support.*;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.Sub;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.AuctionDepositRec;

/**
 *拍卖保证金Dao
 */
@Repository
public class AuctionDepositRecDaoImpl extends GenericDaoImpl<AuctionDepositRec, Long> implements AuctionDepositRecDao {

	public AuctionDepositRec getAuctionDepositRec(Long id) {
		return getById(id);
	}

	public int deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		return delete(auctionDepositRec);
	}

	public Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		return save(auctionDepositRec);
	}

	public int updateAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
		return update(auctionDepositRec);
	}

	@Override
	public boolean isPaySecurity(String userId, Long paimaiId) {
		long number = getLongResult("select count(id) from ls_auction_depositrec where order_status=1 AND a_id=? AND user_id=? ", paimaiId, userId);
		return number > 0;
	}

	@Override
	public Long queryAccess(Long paimaiId) {
		return getLongResult("select count(id) from ls_auction_depositrec where a_id=? and order_status=1 ", paimaiId);
	}

	@Override
	public List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId) {
		return this.queryByProperties(new EntityCriterion().eq("aId", aId).eq("flagStatus", 0l).eq("orderStatus", 1));
	}

	@Override
	public AuctionDepositRec getAucitonRec(Long aId, String userId) {
		return this.getByProperties(new EntityCriterion().eq("aId", aId).eq("userId", userId));
	}

	@Override
	public AuctionDepositRec getAuctionDepositRecByUserIdAndaId(String userId, Long paimaiId) {
		return this.get("select * from ls_auction_depositrec where a_id=? and user_id = ? and order_status = 1", AuctionDepositRec.class, paimaiId, userId);
	}

	@Override
	public PageSupport<AuctionDepositRec> queryDepositRecList(String curPageNO, Long id) {
		SimpleSqlQuery query = new SimpleSqlQuery(AuctionDepositRec.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("aId", id);
		String querySQL = ConfigCode.getInstance().getCode("depositrec.queryDepositrec", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("depositrec.queryDepositrecCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public AuctionDepositRec getAuctionDepositRec(String subNumber, String userId) {
		return this.getByProperties(new EntityCriterion().eq("subNumber", subNumber).eq("userId", userId));
	}

	/**
	 * 查询有扣除保证金标识并且未结算的拍卖定金列表
	 * @param shopId
	 * @return
	 */
	@Override
	public List<AuctionDepositRec> getBillAuctionDepositRecList(Long shopId) {

		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("detainFlag", 1).eq("isBill", 0));

	}

	/**
	 * 根据结算当期获取结算的拍卖保证金记录
	 * @param curPageNO
	 * @param shopId
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<AuctionDepositRec> queryAuctionDepositRecList(String curPageNO, Long shopId, ShopOrderBill shopOrderBill) {

		CriteriaQuery cq = new CriteriaQuery(Sub.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("shopId", shopId);
		cq.eq("billSn", shopOrderBill.getSn());
		return queryPage(cq);
	}

	/**
	 * 根据订单号获取定金记录
	 * @param subNumber
	 * @return
	 */
	@Override
	public AuctionDepositRec getAuctionDepositRecBySubNumber(String subNumber) {

		return getByProperties(new EntityCriterion().eq("subNumber", subNumber));

	}
}
