/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.TagDao;
import com.legendshop.business.dao.TagItemsDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.NewsTagDto;
import com.legendshop.model.dto.TagNewsDto;
import com.legendshop.model.entity.Tag;
import com.legendshop.model.entity.TagItems;
import com.legendshop.spi.service.TagService;
import com.legendshop.util.AppUtils;

/**
 * 标签服务
 */
@Service("tagService")
public class TagServiceImpl implements TagService {

	@Autowired
	private TagDao tagDao;
	
	@Autowired
	private TagItemsDao tagItemsDao;
	
	public List<Tag> getPageTag(Long shopId, String page) {
		return tagDao.getPageTag(shopId, page);
	}

	public Tag getTag(Long id) {
		return tagDao.getTag(id);
	}

	public void deleteTag(Tag tag) {
		tagDao.deleteTag(tag);
	}

	public void saveTag(Tag tag) {
		if(AppUtils.isNotBlank(tag.getTagId())){
			tagDao.updateTag(tag);
		}else{
			tag.setCreateTime(new Date());
		 tagDao.saveTag(tag);
		}
	}

	public void updateTag(Tag tag) {
		tagDao.updateTag(tag);
	}

	@Override
	public Tag getTag(String name, String userName) {
		return tagDao.getTag(name, userName);
	}

	@Override
	public List<TagItems> queryTagItemsList(List<Tag> list) {
		return tagItemsDao.queryTagItemsList(list);
	}

	@Override
	public TagItems getItemsByTagId(Long id) {
		return tagItemsDao.getItemsByTagId(id);
	}

	@Override
	public void deleteTagItems(TagItems tagItems) {
		tagItemsDao.deleteTagItems(tagItems);
	}

	@Override
	public TagItems getTagItems(Long itemId) {
		return tagItemsDao.getTagItems(itemId);
	}

	@Override
	public void saveTagItems(TagItems tagItems) {
		if(AppUtils.isNotBlank(tagItems.getItemId())){
			tagItemsDao.updateTagItems(tagItems);
		}else{
			tagItems.setCreateTime(new Date());
			tagItemsDao.saveTagItems(tagItems);
		}
	}

	@Override
	public void deleteTagItems(Long itemId) {
		tagItemsDao.deleteTagItems(itemId);
	}

	@Override
	public List<TagItems> getTagItems(String code) {
		return tagItemsDao.getTagItems(code);
	}

	@Override
	public List<NewsTagDto> getNewsTagDto(String type) {
		return tagItemsDao.getNewsTagDto(type);
	}
	
	@Override
	public List<TagNewsDto> getTagNewsDto(String type) {
		return tagItemsDao.getTagNewsDto(type);
	}

	@Override
	public Long getTagNewsDtoCount(String type) {
		return tagItemsDao.getTagNewsDtoCount(type);
	}

	@Override
	public List<TagItems> queryTagItemsListByType(String type) {
		return tagItemsDao.queryTagItemsListByType(type);
	}

	@Override
	public void saveTagItemsNews(Long newsid, Long itemid) {
		tagItemsDao.saveTagItemsNews(newsid,itemid);
	}

	@Override
	public void delTagItemsNews(Long newsid, Long itemid) {
		tagItemsDao.delTagItemsNews(newsid,itemid);
		
	}

	@Override
	public PageSupport<Tag> getTagPage(String curPageNO, Tag tag) {
		return tagDao.getTagPage(curPageNO,tag);
	}

	@Override
	public void deleteTag(Tag tag, TagItems tagItems) {
		if (AppUtils.isNotBlank(tagItems)) {
			tagItemsDao.deleteTagItems(tagItems);
		}
		tagDao.deleteTag(tag);
	}

}
