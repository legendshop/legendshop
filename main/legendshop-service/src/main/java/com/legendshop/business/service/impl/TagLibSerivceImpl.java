/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.TagLibDao;
import com.legendshop.spi.service.TagLibSerivce;

@Service("tagLibSerivce")
public class TagLibSerivceImpl implements TagLibSerivce {
	
	@Autowired
	private TagLibDao tagLibDao;

	@Override
	public List<Object[]> query(String sql, Object param) {
		return tagLibDao.query(sql, param);
	}

	@Override
	public List<Object[]> query(String sql) {
		return tagLibDao.query(sql);
	}
}
