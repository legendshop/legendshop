/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ThemeModuleProdDao;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.spi.service.ThemeModuleProdService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 专题板块商品.
 */
@Service("themeModuleProdService")
public class ThemeModuleProdServiceImpl  implements ThemeModuleProdService{
	
	@Autowired
    private ThemeModuleProdDao themeModuleProdDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

	@Override
	public List<ThemeModuleProd> getThemeModuleProdByModuleid(Long id) {
        return themeModuleProdDao.getThemeModuleProdByModuleid(id);
    }

    @Override
	public ThemeModuleProd getThemeModuleProd(Long id) {
        return themeModuleProdDao.getThemeModuleProd(id);
    }

    @Override
	public void deleteThemeModuleProd(ThemeModuleProd themeModuleProd) {
        themeModuleProdDao.deleteThemeModuleProd(themeModuleProd);
    }

    @Override
	public void updateThemeModuleProd(ThemeModuleProd themeModuleProd) {
        themeModuleProdDao.updateThemeModuleProd(themeModuleProd);
    }

	@Override
	public Long saveThemeModuleProd(ThemeModuleProd themeModuleProd) {
		if (!AppUtils.isBlank(themeModuleProd.getModulePrdId())) {
            updateThemeModuleProd(themeModuleProd);
            return themeModuleProd.getModulePrdId();
        }
        return themeModuleProdDao.saveThemeModuleProd(themeModuleProd);
	}

	@Override
	public void updateThemeModuleProdt(ThemeModuleProd themeModuleProd) {
		if (AppUtils.isNotBlank(themeModuleProd.getAngleIcon())) {
			attachmentManager.deleteTruely(themeModuleProd.getAngleIcon());
			attachmentManager.delAttachmentByFilePath(themeModuleProd.getAngleIcon());
			themeModuleProd.setAngleIcon(null);
		}
		themeModuleProdDao.updateThemeModuleProd(themeModuleProd);
	}

	@Override
	public void deleteThemeModuleProd(ProductDetail prodInfo, ThemeModuleProd themeModuleProd) {
		// 删除板块商品图片 如果是商品表的原有图片则不删
		if (AppUtils.isNotBlank(themeModuleProd.getImg()) && !themeModuleProd.getImg().equals(prodInfo.getPic())) {
			attachmentManager.deleteTruely(themeModuleProd.getImg());
			attachmentManager.delAttachmentByFilePath(themeModuleProd.getImg());
		}
		// 删除角标图片
		if (AppUtils.isNotBlank(themeModuleProd.getAngleIcon())) {
			attachmentManager.deleteTruely(themeModuleProd.getAngleIcon());
			attachmentManager.delAttachmentByFilePath(themeModuleProd.getAngleIcon());
		}
		themeModuleProdDao.deleteThemeModuleProd(themeModuleProd);
	}
}
