package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdRechargeDto;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.spi.service.AppPdRechargeService;
import com.legendshop.spi.service.PdRechargeService;
import com.legendshop.util.AppUtils;

/**
 * app 预存款充值记录service实现
 * 
 */
@Service("appPdRechargeService")
public class AppPdRechargeServiceImpl implements AppPdRechargeService {

	@Autowired
	private PdRechargeService pdRechargeService;
	

	/**
	 * 获取预存款充值记录列表
	 */
	@Override
	public AppPageSupport<AppPdRechargeDto> getRechargeDetailedPage(String curPageNO, String userId, String state) {


		PageSupport<PdRecharge> ps = pdRechargeService.getRechargeDetailedPage(curPageNO, userId, state);
		
		AppPageSupport<AppPdRechargeDto> pageSupportDto = PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<PdRecharge, AppPdRechargeDto>() {

			@Override
			public List<AppPdRechargeDto> convert(List<PdRecharge> form) {

				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppPdRechargeDto> toList = new ArrayList<AppPdRechargeDto>();
				for (PdRecharge pdRecharge : form) {
					AppPdRechargeDto appPdRechargeDto = convertToAppPdRechargeDto(pdRecharge);
					toList.add(appPdRechargeDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}

	@Override
	public AppPdRechargeDto getPdRecharge(Long id, String userId) {

		PdRecharge pdRecharge = pdRechargeService.getPdRecharge(id, userId);
		if (AppUtils.isNotBlank(pdRecharge)) {
			return convertToAppPdRechargeDto(pdRecharge);
		}
		return null;
	}
	
	private AppPdRechargeDto convertToAppPdRechargeDto(PdRecharge pdRecharge){
		  AppPdRechargeDto appPdRechargeDto = new AppPdRechargeDto();
		  appPdRechargeDto.setAmount(pdRecharge.getAmount());
		  appPdRechargeDto.setAdminUserName(pdRecharge.getAdminUserName());
		  appPdRechargeDto.setAddTime(pdRecharge.getAddTime());
		  appPdRechargeDto.setNickName(pdRecharge.getNickName());
		  appPdRechargeDto.setAdminUserNote(pdRecharge.getAdminUserNote());
		  appPdRechargeDto.setUserName(pdRecharge.getUserName());
		  appPdRechargeDto.setUserId(pdRecharge.getUserId());
		  appPdRechargeDto.setPaymentCode(pdRecharge.getPaymentCode());
		  appPdRechargeDto.setId(pdRecharge.getId());
		  appPdRechargeDto.setSn(pdRecharge.getSn());
		  appPdRechargeDto.setPaymentState(pdRecharge.getPaymentState());
		  appPdRechargeDto.setPaymentTime(pdRecharge.getPaymentTime());
		  appPdRechargeDto.setPaymentName(pdRecharge.getPaymentName());
		  appPdRechargeDto.setTradeSn(pdRecharge.getTradeSn());
		  return appPdRechargeDto;
		}

}
