/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.SubFloorDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.SubFloor;
import com.legendshop.spi.service.SubFloorService;
import com.legendshop.util.AppUtils;

/**
 * 首页子楼层服务
 */
@Service("subFloorService")
public class SubFloorServiceImpl  implements SubFloorService{

	@Autowired
    private SubFloorDao subFloorDao;

	@Autowired
    private FloorDao floorDao;

    public List<SubFloor> getSubFloorByShopId() {
        return subFloorDao.getSubFloorByShopId();
    }

    public SubFloor getSubFloor(Long id) {
        return subFloorDao.getSubFloor(id);
    }

    public void deleteSubFloor(SubFloor subFloor) {
    	subFloorDao.deleteSubFloorItems(subFloor.getId());
        subFloorDao.deleteSubFloor(subFloor);
        
        Floor floor=new Floor();
        floor.setFlId(subFloor.getFloorId());
        floor.setLayout(FloorLayoutEnum.WIDE_BLEND.value());
        floorDao.updateFloorCache(floor);
    }

    public Long saveSubFloor(SubFloor subFloor) {
        if (!AppUtils.isBlank(subFloor.getSfId())) {
            updateSubFloor(subFloor);
            return subFloor.getSfId();
        }
        Floor floor=new Floor();
        floor.setFlId(subFloor.getFloorId());
        floor.setLayout(FloorLayoutEnum.WIDE_BLEND.value());
        floorDao.updateFloorCache(floor);
        return (Long) subFloorDao.save(subFloor);
    }

    public void updateSubFloor(SubFloor subFloor) {
        subFloorDao.updateSubFloor(subFloor);
        Floor floor=new Floor();
        floor.setFlId(subFloor.getFloorId());
        floor.setLayout(FloorLayoutEnum.WIDE_BLEND.value());
        floorDao.updateFloorCache(floor);
    }

	public List<SubFloor> getAllSubFloor(List<Floor> floorList) {
		return subFloorDao.getAllSubFloor( floorList);
	}

	public List<SubFloor> getSubFloorByfId(Long id) {
		return subFloorDao.getSubFloorByfId(id);
	}

	@Override
	public boolean checkPrivilege(String userName, Long floorId) {
		return subFloorDao.checkPrivilege(userName, floorId);
	}

	@Override
	public PageSupport<SubFloor> getSubFloorPage(String curPageNO, SubFloor subFloor) {
		return subFloorDao.getSubFloorPage(curPageNO,subFloor);
	}
}
