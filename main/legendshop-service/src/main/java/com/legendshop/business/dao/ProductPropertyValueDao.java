/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;

/**
 *
 * 商品属性值服务
 *
 */
public interface ProductPropertyValueDao extends GenericDao<ProductPropertyValue, Long>{
     
	public abstract ProductPropertyValue getProductPropertyValue(Long id);

    /** 根据prodid和属性值名称查询 */
    ProductPropertyValue getProductPropertyValue(Long propId, String propertiesValue);
	
    public abstract void deleteProductPropertyValue(ProductPropertyValue productPropertyValue);
    
    public abstract void deleteProductPropertyPic(String pic);
	
	public abstract void saveProductPropertyValue(String userName, String userId, Long shopId, ProductPropertyValue productPropertyValue);
	
	public abstract void updateProductPropertyValue(String userName, String userId, Long shopId, ProductPropertyValue toBeUpdateValue,ProductPropertyValue productPropertyValue);
	
	public abstract void updateProductPropertyValue(ProductPropertyValue productPropertyValue);
	
	public List<ProductPropertyValue> getAllProductPropertyValue(List<ProductProperty> propertyList);
	
	public abstract void deleteProductPropertyValue(Long propId);
	
	 public List<ProductPropertyValue> queryProductPropertyValue(Long propId);
	 
	 public boolean ifValueChanged(List<ProductPropertyValue> originList, ProductPropertyValue value);

	public abstract void deleteProductPropertyValue(List<ProductPropertyValue> originList);

	/** 直接获取属性值 id，但并没有做保存 **/
	public abstract Long saveCustomPropValue(ProductPropertyValue customPropValue);

	public abstract List<ProductPropertyValue> getProductPropertyValue(Object[] array, Long prodId, Long propId);

	public abstract List<ProductPropertyValue> getProductPropertyValue(List<Long> allValIdList, Long prodId);

	ProductPropertyValue getProductPropertyValueInfo(Long valueId, Long prodId);

	ProductPropertyValue getProductPropertyValueInfo(String name, Long propId);
	
	public abstract List<ProductPropertyValue> getProductPropertyValue(List<Long> ids);

	public abstract PageSupport<ProductPropertyValue> getProductPropertyValuePage(String curPageNO);

	public abstract PageSupport<ProductPropertyValue> getProductPropertyValue(String valueName, Long propId);

	public abstract PageSupport<ProductPropertyValue> getProductPropertyValuePage(String valueName, Long propId);

	/**
	 * 根据propId获取属性
	 * @param propId
	 * @return
	 */
	List<Long> getValueIdsByPropId(Long propId);


	/**
	 * 根据propId获取属性
	 * @param propId
	 * @return
	 */
	List<ProductPropertyValueDto> getValueListByPropId(Long propId);
}
