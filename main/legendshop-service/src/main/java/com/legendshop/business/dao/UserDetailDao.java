/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.RegisterEnum;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.securityCenter.UserInformation;

/**
 * 用户详情Dao.
 */
public interface UserDetailDao  extends GenericDao<UserDetail, String>,UserLoginDao{

	
	/**
	 * 手机用户注册
	 * @param user
	 * @param userDetail
	 * @return
	 */
	abstract User saveUser(User user, UserDetail userDetail);

	/**
	 * 根据手机号码获取用户详情
	 * 
	 */
	abstract UserDetail getUserDetailByMobile(String mobile);


	/**
	 * 普通用户信息修改
	 * 
	 * @param userDetail
	 *            the user detail
	 */
	abstract void updateUser(UserDetail userDetail);


	/**
	 * 客户是否存在 true: 客户已经存在 false: 客户不存在.
	 * 
	 * @param userName
	 *            the user name
	 * @return true, if is user exist
	 */
	abstract boolean isUserExist(String userName);
	
	abstract UserDetail getUserDetailById(String userId);
	
	/**
	 * 根据店铺ID获取用户信息
	 * @param shopId
	 * @return
	 */
	abstract UserDetail getUserDetailByShopId(Long shopId);

	/**
	 * Email是否存在 true: Email已经存在 false: Email不存在.
	 * 
	 * @param email
	 *            the email
	 * @return true, if is email exist
	 */
	abstract boolean isEmailExist(String email);
	
	/**
	 * Phone是否存在true: Phone已经存在false: Phone不存在.
	 *
	 * @param Phone the phone
	 * @return true, if is phone exist
	 */
	abstract boolean isPhoneExist(String Phone);
	
	/**
	 * Email 是否唯一.
	 *
	 * @param email the email
	 * @param userName the user name
	 * @return true, if is email only
	 */
	abstract boolean isEmailOnly(String email,String userName);
	
	/**
	 * 根据昵称获取用户登录名称
	 * @param nickName
	 * @return
	 */
	abstract String getUserNameByNickName(String nickName);
	/**
	 * 根据昵称获取商家用户登录名称
	 * @param nickName
	 * @return
	 */
	abstract String getShopUserNameByNickName(String nickName);
	/**
	 * 根据昵称获取用户登录名称
	 * @param nickName
	 * @return
	 */
	abstract String getUserNameByUserId(String userId);
	
	/**
	 * 根据邮件获取用户登陆名称
	 * @param email
	 * @return
	 */
	abstract String getUserNameByMail(String email);
	
	
	
	/**
	 * 根据邮件获取商家用户登陆名称
	 * @param email
	 * @return
	 */
	abstract String getShopUserNameByMail(String email);
	
	/**
	 * 根据电话获取用户登陆名称
	 * @param phone
	 * @return
	 */
	
	abstract String getUserNameByPhone(String phone);
	
	/**
	 * 根据电话获取商家登陆名称
	 * @param phone
	 * @return
	 */
	
	abstract String getShopUserNameByPhone(String phone);

	/**
	 * phone 是否唯一.
	 *
	 * @param phone the phone
	 * @param userName the user name
	 * @return true, if is phone only
	 */
	abstract boolean isPhoneOnly(String phone,String userName);
	
	/**
	 * Find user.
	 * 
	 * @param userId
	 *            the user id
	 * @return the user
	 */
	abstract User getUser(String userId);

	/**
	 * Find user detail by name.
	 * 
	 * @param userName
	 *            the user name
	 * @return the user detail
	 */
	abstract UserDetail getUserDetailByName(String userName);

	/**
	 * User reg success.
	 * 
	 * @param userName
	 *            the user name
	 * @param registerCode
	 *            the register code
	 * @return the register enum
	 */
	abstract RegisterEnum getUserRegStatus(String userName, String registerCode);

	/**
	 * Find user score.
	 * 
	 * @param userName
	 *            the user name
	 * @return the long
	 */
	abstract Integer getUserScore(String userName);


	/**
	 * Delete user detail.
	 *
	 * @param userId the user id
	 * @param userName the user name
	 * @return the string
	 */
	abstract String deleteUserDetail(String userId, String userName);
	

	/**
	 * 重置密码.
	 *
	 */
	abstract boolean updatePassword(String userName, String mail, String password);

	/**
	 * Gets the all shop count.
	 * 
	 * @return the all shop count
	 */
	abstract Long getAllUserCount();

	/**
	 * Update user.
	 * 
	 * @param user
	 *            the user
	 */
	abstract void updateUser(User user);
	
	/**
	 * 通过userName来获取用户的信息，联合UserSecurity.
	 *
	 * @param userName the user name
	 * @return the user info
	 */
	abstract UserInformation getUserInfo(String userName);
	
	/**
	 * 通过Phone来获取用户的信息，联合UserSecurity.
	 *
	 * @param Phone the phone
	 * @return the user info by phone
	 */
	abstract UserInformation getUserInfoByPhone(String Phone);
	
	/**
	 * 通过Email来获取用户的信息，联合UserSecurity.
	 *
	 * @param Email the email
	 * @return the user info by mail
	 */
	abstract UserInformation getUserInfoByMail(String Email);
	
	/**
	 * 根据ID查找password.
	 *
	 * @param UserId the user id
	 * @return the user password
	 */
	abstract String getUserPassword(String UserId);
	
	/**
	 * 根据ID查找邮箱.
	 *
	 * @param userId the user id
	 * @return the user email
	 */
	abstract String getUserEmail(String userId);
	
	/**
	 * 根据ID查找手机.
	 *
	 * @param userId the user id
	 * @return the user mobile
	 */
	abstract String getUserMobile(String userId);
	
	/**
	 * 更新userDetail用户信息.
	 *
	 * @param userDetail the user detail
	 */
	abstract int updateUserDetail(UserDetail userDetail);
	
	/**
	 * 对比验证码是否一致.
	 *
	 * @param userName the user name
	 * @param code the code
	 * @return true, if successful
	 */
	abstract boolean verifySMSCode(String userName,String code);
	
	/**
	 * 是否已验证邮箱.
	 *
	 * @param userName the user name
	 * @return the mail verifn
	 */
	abstract Integer getMailVerifn(String userName);
	
	/**
	 * 是否已验证手机.
	 *
	 * @param userName the user name
	 * @return the phone verifn
	 */
	abstract Integer getPhoneVerifn(String userName);
	
	/**
	 * 是否已验证支付密码.
	 *
	 * @param userName the user name
	 * @return the paypass verifn
	 */
	abstract Integer getPaypassVerifn(String userName);
	
	/**
	 * 更新账户的密码.
	 *
	 * @param userName the user name
	 * @param newPassword the new password
	 * @return the boolean
	 */
	abstract Boolean updateNewPassword(String userName,String newPassword);
	
//	/**
//	 * 更新已验证邮箱.
//	 *
//	 * @param userEmail the user email
//	 * @param userName the user name
//	 */
//	public abstract void updateUserEmail(String userEmail,String userName);
	
	/**
	 * 获得userSecurity表中的update_mail.
	 *
	 * @param userName the user name
	 * @return the security email
	 */
	abstract String getSecurityEmail(String userName);
	

	/**
	 * 查找出安全等级.
	 *
	 * @param userName the user name
	 * @return the sec level
	 */
	abstract Integer getSecLevel(String userName);

	/**  第三方登陆 查找用户名 **/
	abstract String getUserNameByOpenId(String openId, String type);
	
	/**  第三方登陆 查找用户名 **/
	abstract String getUserIdByOpenId(String openId, String type);
	
	/**  第三方登陆 查找昵称 **/
	abstract String getNickNameByOpenId(String openId, String type);

	/**  获取用户最后的登陆时间 **/
	abstract Date getLastLoginTime(String userName);

	/**
	 * 用户昵称是否存在
	 * @param nickName 用户昵称
	 * @return
	 */
	abstract boolean isNickNameExist(String nickName);
	
	/**
	 * 根据店铺名字判断是否已经存在一个店铺
	 * 
	 */
	abstract boolean isSiteNameExist(String siteName);
	
	
	/** 根据 用户的手机号码 查找 user **/
	abstract User getUserByMobile(String userMobile);


	/**
	 * 根据微信公众号OpenId获取用户
	 * @param wxOpenId
	 * @return
	 */
	abstract User getUserByWxOpenId(String wxOpenId);

	abstract UserInformation getUserInfoByNickName(String nickName);

	abstract User getUserByName(String userName);

	abstract UserDetail getUserDetailByidForUpdate(String userId);

	/**
	 * 更新账户可用预付款
	 * @param updatePredeposit 要更新的预付款金额
	 * @param originalPredeposit 原来旧的预付款金额
	 * @param userId 用户ＩＤ
	 * @return
	 */
	abstract int updateAvailablePredeposit(Double updatePredeposit,Double originalPredeposit, String userId);
	
	

	/**
	 * 更新账户冻结预付款
	 * @param updatePredeposit 要更新的预付款金额
	 * @param originalPredeposit 原来旧的预付款金额
	 * @param userId 用户ＩＤ
	 * @return
	 */
	public abstract int updateFreezePredeposit(Double updatePredeposit,Double originalPredeposit, String userId);

	/**
	 * 更新预账户预付款
	 * @param updatePredeposit 要更新的可用预付款金额
	 * @param originalPredeposit 原来旧的可用预付款金额
	 * @param updatefreePredeposit  要更新的冻结预付款金额
	 * @param originalFreePredeposit 原来旧的冻结预付款金额
	 * @param userId
	 * @return
	 */
	int updatePredeposit(Double updatePredeposit, Double originalPredeposit,Double updatefreePredeposit, Double originalFreePredeposit,
			String userId);
	
	/**
	 * 减去用户积分
	 */
	abstract int updateScore(Integer updateSore,Integer originalSore, String userId);

	abstract UserDetail getUserDetailForScoreUser(String nickName);

	abstract boolean checkBinding(String mobile);

	
	/**
	 * 充值积分
	 * @param updateSore
	 * @param userId
	 * @return
	 */
	int updateScore(Integer updateSore, String userId);

	public abstract void updateUserGrade(String userId, Integer userGradeId, Integer grouthValue);

	
	public void updateShopId(Long shopId,String userId);
	
	public abstract UserDetail getUserDetailByAll(String nickName);

	public abstract String getuserIdByShopId(Long userId);

	/**
	 * 更新最后登录时间
	 * @param userId
	 */
	void updateLastLoginTime(String userId);

	UserDetail getUserDetailByIdNoCache(String userId);

	abstract Double getAvailablePredeposit(String userId);
	
	/**
	 * 获取剩余金币
	 * @param userId
	 * @return
	 */
	abstract Double getUserCoin(String userId);

	/**
	 * 更改金币
	 * @param updateCoin  更改数
	 * @param originalCoin 原来数
	 * @param userId
	 * @return
	 */
	int updateUserCoin(Double updateCoin, Double originalCoin, String userId);

	/**
	 * 检查该用户是否为推广员
	 * @param userId
	 * @return
	 */
	abstract boolean checkPromoter(String userId);

	
	abstract String getNickNameByUserName(String userName);

	abstract String getNickNameByUserId(String userId);
	
	abstract Long getShopIdByUserId(String userId);
	
	abstract Integer upIsRegIM(String userId, Integer param);
	
	//更新用户邮件
	abstract int updateUserDetailEmail(UserDetail userDetail);

	abstract boolean isExitMail(String mail);

	abstract PageSupport<UserDetail> getUserDetailListPage(String haveShop, DataSortResult result, String curPageNO, UserDetail userDetail);

	abstract User getByOpenId(String openId);

	abstract void bingUserOpenId(String userId, String openId);

        public abstract void bingUserOpenId(String userId, String openId,String unionid);


    abstract int batchOfflineUser(String userIds);

	abstract boolean verifyMobileCode(String mobile, String mobileCode);

	/**
	 * 通过手机号或用户名来获取用户名
	 * 
	 */
	abstract String getUserNameByPhoneOrUsername(String userMobile, String userName);

	/**
	 * 记录用户的登录历史
	 */
	abstract void updateUserLoginHistory(String ip, String userName);

	/**
	 * 手动过期验证码
	 * @param mobile
	 * @param mobileCode
	 */
	void expiredVerificationCode(String mobile, String mobileCode);
}