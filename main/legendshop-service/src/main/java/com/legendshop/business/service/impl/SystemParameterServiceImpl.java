/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.business.dao.SystemParameterDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.spi.service.SystemParameterService;

/**
 * 系统参数服务.
 */
@Service("systemParameterService")
public class SystemParameterServiceImpl implements SystemParameterService, InitializingBean {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SystemParameterServiceImpl.class);

	@Autowired
	private SystemParameterDao systemParameterDao;
	
	@Autowired
	private SystemParameterProvider systemParameterProvider;

	/**
	 * 加载所有的系统参数.
	 * 
	 * @return the list
	 */
	private List<SystemParameter> list() {
		return systemParameterDao.loadAll();
	}

	@Override
	public SystemParameter getSystemParameter(String id) {
		return systemParameterDao.getSystemParameterById(id);
	}

	@Override
	public void delete(String id) {
		systemParameterDao.deleteSystemParameterById(id);
	}

	@Override
	public void update(SystemParameter systemParameter) {
		systemParameterDao.updateSystemParameter(systemParameter);
	}

	/**
	 * 初始化System parameter
	 *
	 */
	@Override
	public List<SystemParameter> initSystemParameter() {
		List<SystemParameter> list = list();
		systemParameterProvider.setParameter(list);
		log.info("System Parameter size = {}", list.size());
		return list;
	}


	@Override
	public List<SystemParameter> getSystemParameterByGroupId(String groupId) {
		return this.systemParameterDao.getSystemParameterByGroupId(groupId);
	}

	@Override
	public PageSupport<SystemParameter> getSystemParameterListPage(String curPageNO, String name, String groupId) {
		return systemParameterDao.getSystemParameterListPage(curPageNO,name,groupId);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("Start o init system paramter");
		initSystemParameter();
	}
}
