/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassCommDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassComm;
import com.legendshop.spi.service.GrassCommService;
import com.legendshop.util.AppUtils;

/**
 * The Class GrassCommServiceImpl.
 *  种草文章评论表服务实现类
 */
@Service("grassCommService")
public class GrassCommServiceImpl implements GrassCommService{

    /**
     *
     * 引用的种草文章评论表Dao接口
     */
	@Autowired
    private GrassCommDao grassCommDao;
   
   	/**
	 *  根据Id获取种草文章评论表
	 */
    public GrassComm getGrassComm(Long id) {
        return grassCommDao.getGrassComm(id);
    }
    
    /**
	 *  根据Id删除种草文章评论表
	 */
    public int deleteGrassComm(Long id){
    	return grassCommDao.deleteGrassComm(id);
    }

   /**
	 *  删除种草文章评论表
	 */ 
    public int deleteGrassComm(GrassComm grassComm) {
       return  grassCommDao.deleteGrassComm(grassComm);
    }

   /**
	 *  保存种草文章评论表
	 */	    
    public Long saveGrassComm(GrassComm grassComm) {
        if (!AppUtils.isBlank(grassComm.getId())) {
            updateGrassComm(grassComm);
            return grassComm.getId();
        }
        return grassCommDao.saveGrassComm(grassComm);
    }

   /**
	 *  更新种草文章评论表
	 */	
    public void updateGrassComm(GrassComm grassComm) {
        grassCommDao.updateGrassComm(grassComm);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassComm> queryGrassComm(String curPageNO, Integer pageSize){
     	return grassCommDao.queryGrassComm(curPageNO, pageSize);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassCommDao(GrassCommDao grassCommDao) {
        this.grassCommDao = grassCommDao;
    }
    
}
