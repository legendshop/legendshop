/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DeliverGoodsAddrDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliverGoodsAddr;

/**
 * The Class DeliverGoodsAddrDaoImpl.
 */
@Repository
public class DeliverGoodsAddrDaoImpl extends GenericDaoImpl<DeliverGoodsAddr, Long> implements DeliverGoodsAddrDao  {
     
	private final String getDeliverGoodsAddrList="SELECT d.addr_id AS addrId,d.contact_name AS contactName,d.mobile AS mobile,d.province_id as provinceId,d.city_id as cityId,d.area_id as areaId,d.adds AS adds,d.is_default AS detault,p.province AS province,c.city AS city,a.area AS AREA FROM "+
								"ls_deliver_goods_addr d LEFT JOIN ls_provinces p ON d.province_id = p.id "+
								"LEFT JOIN ls_cities c ON d.city_id =c.id "+
								"LEFT JOIN ls_areas a ON d.area_id =a.id "+
								"WHERE d.shop_id=? ORDER BY d.is_default DESC,d.create_time DESC";
	
	private final String getDeliverGoodsAddr="SELECT d.addr_id AS addrId,d.shop_id AS shopId,d.contact_name AS contactName,d.mobile AS mobile,d.province_id as provinceId,d.city_id as cityId,d.area_id as areaId,d.adds AS adds,d.is_default AS detault,p.province AS province,c.city AS city,a.area AS AREA FROM "+
								"ls_deliver_goods_addr d LEFT JOIN ls_provinces p ON d.province_id = p.id "+
								"LEFT JOIN ls_cities c ON d.city_id =c.id "+
								"LEFT JOIN ls_areas a ON d.area_id =a.id "+
								"WHERE d.addr_id=? ORDER BY d.is_default DESC,d.create_time DESC";
	
    public List<DeliverGoodsAddr> getDeliverGoodsAddrList(Long shopId){
    	return this.query(getDeliverGoodsAddrList, DeliverGoodsAddr.class, shopId);
    }

	public DeliverGoodsAddr getDeliverGoodsAddr(Long id){
		return this.get(getDeliverGoodsAddr, DeliverGoodsAddr.class, id);
	}
	
    public int deleteDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr){
    	return delete(deliverGoodsAddr);
    }
	
	public Long saveDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr){
		return save(deliverGoodsAddr);
	}
	
	public int updateDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr){
		return update(deliverGoodsAddr);
	}

	@Override
	public void turnonDefault(Long addrId, Long shopId) {
		//1.更新原来的收货地址
		this.update("update ls_deliver_goods_addr set is_default = 0 where is_default = 1  and shop_id=?",shopId);
		//2.更新新的收货地址
		this.update("update ls_deliver_goods_addr set is_default = 1 where addr_id = ?  and shop_id = ? ", addrId, shopId);
	}

	@Override
	public void closeDefault(Long addrId, Long shopId) {
		this.update("update ls_deliver_goods_addr set is_default = 0 where addr_id = ?  and shop_id = ? ", addrId, shopId);
	}

	@Override
	public void deleteDeliverGoodsAddr(Long addrId, Long shopId) {
		this.update("delete from ls_deliver_goods_addr where addr_id = ?  and shop_id = ? ", addrId, shopId);
	}

	@Override
	public DeliverGoodsAddr getDefultDeliverGoodsAddr(Long shopId, int i) {
		String sql = "SELECT d.province_id AS provinceId,d.city_id AS cityId,d.area_id AS areaId,d.adds AS adds,d.is_default AS detault,p.province AS province,c.city AS city,a.area AS AREA " +
				"FROM ls_deliver_goods_addr d LEFT JOIN ls_provinces p ON d.province_id = p.id LEFT JOIN ls_cities c ON d.city_id = c.id LEFT JOIN ls_areas a ON d.area_id = a.id WHERE d.shop_id = ? AND d.is_default = ?";
		return this.get(sql, DeliverGoodsAddr.class, shopId,i);
	}

	@Override
	public PageSupport<DeliverGoodsAddr> getDeliverGoodsAddrPage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(DeliverGoodsAddr.class, curPageNO);
        cq.setPageSize(5);
        cq.eq("shopId", shopId);
        cq.addOrder("desc", "detault");
        cq.addOrder("desc", "createTime");
		return queryPage(cq);
	}
	
 }
