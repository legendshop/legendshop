/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ExpensesRecord;

/**
 * 消费记录Dao.
 */
public interface ExpensesRecordDao extends Dao<ExpensesRecord, Long> {
     
	public abstract ExpensesRecord getExpensesRecord(Long id);
	
    public abstract int deleteExpensesRecord(ExpensesRecord expensesRecord);
	
	public abstract Long saveExpensesRecord(ExpensesRecord expensesRecord);
	
	public abstract int updateExpensesRecord(ExpensesRecord expensesRecord);
	
	public abstract PageSupport<ExpensesRecord> getExpensesRecord(CriteriaQuery cq);

	public abstract boolean deleteExpensesRecord(String userId, Long id);

	public abstract void deleteExpensesRecord(String userId, List<Long> idList);

	public abstract PageSupport<ExpensesRecord> getExpensesRecordPage(String curPageNO, String userId);

	public abstract PageSupport<ExpensesRecord> getExpensesRecord(String curPageNO, String userId, int pageSize);
	
 }
