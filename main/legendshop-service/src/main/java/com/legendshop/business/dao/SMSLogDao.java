/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.Date;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.constant.SmsTemplateTypeEnum;
import com.legendshop.model.entity.SMSLog;

/**
 * 短信记录服务Dao
 */
public interface SMSLogDao extends GenericDao<SMSLog, Long>{

    public abstract SMSLog getSmsLog(Long id);
	
    public abstract void deleteSMSLog(SMSLog sMSLog);
	
	public abstract Long saveSMSLog(SMSLog sMSLog);
	
	public abstract void updateSMSLog(SMSLog sMSLog);
	
    /** 根据手机号码   **/
	public SMSLog getValidSMSCode(String mobile, SMSTypeEnum type);

    public SMSLog getValidSMSCode(String mobile, SmsTemplateTypeEnum type);


    /** 返回手机号码1个小时内的发送条数   **/
	public boolean allowToSend(String mobile);
	
	/** 短信验证码超时时间 **/
	public int getUsefulMin();

	/**
	 * 更改短信验证码 的状态，使之 失效
	 * @param mobile
	 * @param type
	 */
	public abstract void clearValidSMSCode(String mobile, SMSTypeEnum type);

    public abstract void clearValidSMSCode(String mobile, SmsTemplateTypeEnum type);


    public abstract int getTodayLimit(String mobile);

	/**
	 * 限制时间内可以发送条数
	 * @param mobile
	 * @param oneHourAgo
	 * @param maxSendNum
	 * @return
	 */
	boolean allowToSend(String mobile, Date oneHourAgo, Integer maxSendNum);

	public abstract PageSupport<SMSLog> getSMSLogPage(String curPageNO, SMSLog sMSLog);

	/**
	 * 判断手机验证码是否正确且有效
	 * @param validateCode
	 * @param mobile
	 * @return
	 */
	public abstract boolean verifyMobileCode(String validateCode, String mobile);
 }
