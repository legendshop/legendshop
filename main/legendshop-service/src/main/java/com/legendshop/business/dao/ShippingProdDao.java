/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.FullActiveProdDto;

/**
 * 包邮活动商品Dao
 */
public interface ShippingProdDao extends Dao<ShippingProd, Long> {
     
	public abstract ShippingProd getShippingProd(Long id);
	
    public abstract int deleteShippingProd(ShippingProd shippingProd);
	
	public abstract Long saveShippingProd(ShippingProd shippingProd);
	
	public abstract int updateShippingProd(ShippingProd shippingProd);

	public abstract void batchShippingProd(List<ShippingProd> lists);

	public abstract List<FullActiveProdDto> getFullActiveProdDtoByActiveId(Long shopId, Long id);

	public abstract List<ShippingProd> getActiveProdByActiveId(Long id, Long shopId);

	/**
	 * 根据ShippingId删除包邮商品
	 */
	public abstract void deleteShippingProdByShippingId(Long shippingId, Long shopId);
	
 }
