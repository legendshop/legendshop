/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ParamGroupDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ParamGroup;
import com.legendshop.util.AppUtils;

/**
 * 参数组Dao.
 */
@Repository
public class ParamGroupDaoImpl extends GenericDaoImpl<ParamGroup, Long> implements ParamGroupDao  {
     
	public ParamGroup getParamGroup(Long id){
		return getById(id);
	}
	
    public int deleteParamGroup(ParamGroup paramGroup){
    	return delete(paramGroup);
    }
	
	public Long saveParamGroup(ParamGroup paramGroup){
		return save(paramGroup);
	}
	
	public int updateParamGroup(ParamGroup paramGroup){
		return update(paramGroup);
	}

	@Override
	public List<ParamGroup> getParamGroupList(Long groupId) {
		CriteriaQuery cq = new CriteriaQuery(ParamGroup.class, "1");
        cq.setPageSize(30);
        PageSupport ps = this.queryPage(cq);
        List<ParamGroup> paramGroupList =  (List<ParamGroup>)ps.getResultList();
        
        //add selected option
        if(AppUtils.isNotBlank(paramGroupList) && AppUtils.isNotBlank(groupId) && groupId != 0){
        	boolean found = false;
        	for (int i = 0; i < paramGroupList.size(); i++) {
				if(groupId.equals(paramGroupList.get(i).getId())){
					found = true;
				}
			}
        	
        	if(!found){ //add the option
        		paramGroupList.add(0, this.getParamGroup(groupId));
        	}
        }
        return paramGroupList;
	}

	@Override
	public PageSupport<ParamGroup> getParamGroupPage(String curPageNO, ParamGroup paramGroup) {
		CriteriaQuery cq = new CriteriaQuery(ParamGroup.class, curPageNO);
        if(AppUtils.isNotBlank(paramGroup.getName())){
        	cq.like("name", "%"+paramGroup.getName().trim()+"%");
        }
        cq.setPageSize(10);
        cq.addAscOrder("seq");
		return queryPage(cq);
	}

	@Override
	public PageSupport<ParamGroup> getParamGroupPage(String groupName) {
		 CriteriaQuery cq = new CriteriaQuery(ParamGroup.class, "1");
	        cq.setPageSize(30);
	        cq.like("name", groupName,MatchMode.START);
		return queryPage(cq);
	}
	
 }
