/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.EventDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserEvent;
import com.legendshop.spi.service.EventService;
import com.legendshop.util.AppUtils;

/**
 * 保存后台操作历史.
 */
@Service("eventService")
public class EventServiceImpl implements EventService {

	@Autowired
	private EventDao eventDao;

	public UserEvent getEvent(Long id) {
		return eventDao.getEvent(id);
	}

	public void deleteEvent(UserEvent userEvent) {
		eventDao.deleteEvent(userEvent);
	}

	public Long saveEvent(UserEvent userEvent) {
		if (!AppUtils.isBlank(userEvent.getEventId())) {
			updateEvent(userEvent);
			return userEvent.getEventId();
		}
		return (Long) eventDao.save(userEvent);
	}

	public void updateEvent(UserEvent userEvent) {
		eventDao.updateEvent(userEvent);
	}

	@Override
	public PageSupport<UserEvent> getEventPage(String curPageNO, UserEvent userEvent) {
		return eventDao.getEventPage(curPageNO,userEvent);
	}
}
