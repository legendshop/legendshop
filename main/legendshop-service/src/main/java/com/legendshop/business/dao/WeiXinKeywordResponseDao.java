/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * 微信关键字回复服务
 */
public interface WeiXinKeywordResponseDao extends Dao<WeiXinKeywordResponse, Long> {
     
	public abstract WeiXinKeywordResponse getWeiXinKeywordResponse(Long id);
	
    public abstract int deleteWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);
	
	public abstract Long saveWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);
	
	public abstract int updateWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);
	
	public abstract PageSupport<WeiXinKeywordResponse> getWeiXinnKeywordResponse(String curPageNO);
	
 }
