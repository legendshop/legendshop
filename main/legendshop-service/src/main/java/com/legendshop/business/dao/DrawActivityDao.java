/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawActivity;

/**
 * 抽奖活动Dao.
 */
public interface DrawActivityDao extends Dao<DrawActivity, Long> {

	/** 获取正在进行中的抽奖活动列表 */
	public abstract List<DrawActivity> getDrawActivity();

	public abstract DrawActivity getDrawActivity(Long id);

	/** 删除 */
	public abstract int deleteDrawActivity(DrawActivity drawActivity);

	/** 保存 */
	public abstract Long saveDrawActivity(DrawActivity drawActivity);

	/** 更新 */
	public abstract int updateDrawActivity(DrawActivity drawActivity);

	/**
	 * @Description: 根据奖品或奖项获取活动条数
	 * @param @param
	 *            sql
	 * @param @return
	 * @date 2016-4-18
	 */
	public abstract boolean getActivityCount(Long giftId, Long awardsId);

	/** 获取抽奖活动页面 */
	public abstract PageSupport<DrawActivity> getDrawActivityPage(String curPageNO, DrawActivity drawActivity);

	public abstract List<DrawActivity> getCouponByDate(Date startDate, Date endDate);
}
