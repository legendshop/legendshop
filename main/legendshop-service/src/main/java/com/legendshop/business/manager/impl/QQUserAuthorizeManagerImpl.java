/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.manager.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.service.qq.QQUserAuthorizeApi;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.ThirdUserToken;
import com.legendshop.model.dto.qq.QQAuthorizeUserInfo;
import com.legendshop.model.dto.qq.QQMeResponse;
import com.legendshop.model.dto.qq.QQUserToken;

/**
 * QQ用户授权认证管理器实现
 * @author 开发很忙
 */
@Service
public class QQUserAuthorizeManagerImpl extends TemplateThirdUserAuthorizeManagerImpl {
	
	private static Logger log = LoggerFactory.getLogger(QQUserAuthorizeManagerImpl.class);
	
	/** 授权类型 */
	private static final String RESPONSE_TYPE = "code";
	
	/** 授权范围 */
	private static final String SCOPE = "get_user_info";
	
	/** 回调地址路径 */
	private static final String REDIRECT_URL_PATH= "/thirdlogin/qq/{source}/callback";
	
	/** 授权地址模板 */
	private static final String AUTHORIZE_URL = "https://graph.qq.com/oauth2.0/authorize?response_type=RESPONSE_TYPE&client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&state=STATE&scope=SCOPE&display=DISPLAY";
	
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Override
	protected String authorize(String source) {
		
		String clientId = systemParameterUtil.getQqAppId();
		String state = gennerateState();
		
		String redirectURI = "";
		String display = "";
		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5
			redirectURI = propertiesUtil.getUserAppDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
			display = "mobile";
		}else{//否则就是PC了
			redirectURI = propertiesUtil.getPcDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}
	
		String authorizeURL = AUTHORIZE_URL.replace("RESPONSE_TYPE", RESPONSE_TYPE)
				.replace("CLIENT_ID", clientId)
				.replace("REDIRECT_URI", redirectURI)
				.replace("STATE", state)
				.replace("SCOPE", SCOPE)
				.replace("DISPLAY", display);
		
		return authorizeURL;
	}

	@Override
	protected ThirdUserToken getAccessToken(String code, String source) {
		
		String clientId = null;
		String clientSecret = null;
		String redirectURI = null;
		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5
			clientId = systemParameterUtil.getQqH5AppId();
			clientSecret = systemParameterUtil.getQqH5Secret();
			redirectURI = propertiesUtil.getUserAppDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}else{//否则就是PC了
			clientId = systemParameterUtil.getQqAppId();
			clientSecret = systemParameterUtil.getQqSecret();
			redirectURI = propertiesUtil.getPcDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}
		
		QQUserToken token = QQUserAuthorizeApi.getAccessToken("authorization_code", clientId, clientSecret, code, redirectURI);
		if(null == token){
			log.info("############## 获取token失败 = {} ####################");
			return null;
		}
		
//		if(!token.isSuccess()){
//			log.info("############## 获取token失败 = {} ####################", token.getMsg());
//			return null;
//		}
		
		return new ThirdUserToken(token);
	}

	@Override
	protected ThirdUserInfo getUserInfo(ThirdUserToken token) {
		
		QQMeResponse  me = QQUserAuthorizeApi.getOpenid(token.getAccessToken());
		if(null == me){
			log.info("############## 获取openId失败 ####################");
			return null;
		}
		
//		if(!me.isSuccess()){
//			log.info("############## 获取openId失败 = {} ####################", me.getMsg());
//			return null;
//		}
		
		token.setOpenId(me.getOpenid());
		token.setUnionid(me.getUnionid());
		
//		QQAuthorizeUserInfo userInfo = QQUserAuthorizeApi.getUserInfo(token.getAccessToken(), me.getClient_id(), me.getOpenid());
//		if(null != userInfo){
//			if(!userInfo.isSuccess()){
//				log.info("############## 获取openId失败 = {} ####################", me.getMsg());
//			}
//		}
//
//		return new ThirdUserInfo(token, userInfo);

		QQAuthorizeUserInfo qqAuthorizeUserInfo = null;
		return new ThirdUserInfo(token, new QQAuthorizeUserInfo());
	}

	@Override
	protected ThirdUserInfo getMpUserInfo(String code, String encryptedData, String iv) {
		throw new BusinessException("暂不支持QQ小程序");
	}
	
	@Override
	protected ThirdUserAuthorizeResult authThirdUser(ThirdUserInfo thirdUserInfo, String source) {
		
		return thirdLoginService.authThridUser(thirdUserInfo, PassportTypeEnum.QQ.value(), source);
	}

}
