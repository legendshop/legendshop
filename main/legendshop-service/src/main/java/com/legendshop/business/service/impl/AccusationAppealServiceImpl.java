/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.AccusationAppealDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationAppeal;
import com.legendshop.spi.service.AccusationAppealService;
import com.legendshop.util.AppUtils;

/**
 * 商家上诉服务实现类
 */
@Service("accusationAppealService")
public class AccusationAppealServiceImpl implements AccusationAppealService {
	
	@Autowired
	private AccusationAppealDao accusationAppealDao;

	/** 删除商家上诉 */
	public AccusationAppeal getAccusationAppeal(Long id) {
		return accusationAppealDao.getAccusationAppeal(id);
	}

	/** 保存商家上诉 */
	public void deleteAccusationAppeal(AccusationAppeal accusationAppeal) {
		accusationAppealDao.deleteAccusationAppeal(accusationAppeal);
	}

	/** 更新商家上诉 */
	public Long saveAccusationAppeal(AccusationAppeal accusationAppeal) {
		if (!AppUtils.isBlank(accusationAppeal.getAaId())) {
			updateAccusationAppeal(accusationAppeal);
			return accusationAppeal.getAaId();
		}
		return (Long) accusationAppealDao.save(accusationAppeal);
	}

	public void updateAccusationAppeal(AccusationAppeal accusationAppeal) {
		accusationAppealDao.updateAccusationAppeal(accusationAppeal);
	}

	/** 获取商家上诉列表 */
	@Override
	public PageSupport<AccusationAppeal> getAccusationAppealPage(String curPageNO) {
		return accusationAppealDao.getAccusationAppealPage(curPageNO);
	}
}
