package com.legendshop.business.order.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.legendshop.model.constant.MarketingRuleCalTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.dto.marketing.MarketingRuleDto;
import com.legendshop.spi.service.MarketingShopRuleExecutor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;


/**
 * 满减运算规则
 *
 */
@Component("manJianMarketingRuleExecutor")
public class ManJianMarketingRuleExecutor  implements MarketingShopRuleExecutor{

	@Override
	public void execute(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		if(marketingDto.getIsAllProds() == 1){
			//全场满减
			globalRuleResolver(marketingDto, shopCarts, context);
		}else{
			//部分商品满减
			prodRuleResolver(marketingDto, shopCarts, context);
		}
	}
	
	/*
	 * 部分商品
	 */
	private void prodRuleResolver(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		
		List<ShopCartItem> cartItems = shopCarts.getCartItems();
		List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();
		Double hitProdTotalPrice = 0d;//命中的商品总金额
		int hitProdCount = 0;//命中的商品总数
		List<MarketingProdDto> marketingProds = marketingDto.getMarketingProdDtos();
		if(AppUtils.isNotBlank(marketingProds)){
			for (MarketingProdDto marketingProdDto : marketingProds) {
				//判断是否执行过 满减、满折、限时折扣
				if(!context.isManJianExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId()) 
						&& !context.isManZeExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId())
						&& !context.isXianShiExecuted(marketingProdDto.getProdId(), marketingProdDto.getSkuId())){
					for (ShopCartItem item : cartItems) {
						//找到命中的商品 ，并累计商品价格
						if(marketingProdDto.getProdId().equals(item.getProdId()) && marketingProdDto.getSkuId().equals(item.getSkuId())){
							if(item.isCalculateMarketing()){//秒杀、团购、预售和拍卖不计算营销活动
								hitCartItems.add(item);
								context.executeManJian(item.getProdId(), item.getSkuId());
								if(item.getCheckSts()==1){//选中状态才计算价格
									hitProdTotalPrice = Arith.add(hitProdTotalPrice, item.getTotal());
									hitProdCount+=item.getBasketCount();
								}
							}
							break;
						}
					}
				}
			}
			//找到命中的商品
			marketingDto.setHitCartItems(hitCartItems);
			marketingDto.setHitProdCount(hitProdCount);
			marketingDto.setHitProdTotalPrice(hitProdTotalPrice);
			
			//计算满减规则 
			calMarketingRule(marketingDto);
		}
	}

	
	/*
	 * 全部商品
	 */
	private void globalRuleResolver(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context) {
		List<ShopCartItem> cartItems = shopCarts.getCartItems();
		List<ShopCartItem> hitCartItems = new ArrayList<ShopCartItem>();
		Double hitProdTotalPrice = 0d;//命中的商品总金额
		int hitProdCount = 0;//命中的商品总数
		//判断是否执行过 满减、满折、限时折扣
		for (ShopCartItem item : cartItems) {
			if(!context.isManJianExecuted(item.getProdId(), item.getSkuId()) 
					&& !context.isManZeExecuted(item.getProdId(), item.getSkuId())
					&& !context.isXianShiExecuted(item.getProdId(), item.getSkuId())){
				if(item.isCalculateMarketing()){//秒杀、团购、预售和拍卖不计算营销活动
					hitCartItems.add(item);
					context.executeManJian(item.getProdId(), item.getSkuId());
					if(item.getCheckSts()==1){//选中状态才计算价格
						//修改之前
						//hitProdTotalPrice = Arith.add(hitProdTotalPrice, item.getTotal());
						
						//修改之后
						hitProdTotalPrice = Arith.add(hitProdTotalPrice, item.getDiscountedPrice());
						hitProdCount+=item.getBasketCount();
					}
				}
			}
		}
		
		//找到命中的商品
		marketingDto.setHitCartItems(hitCartItems);
		marketingDto.setHitProdCount(hitProdCount);
		marketingDto.setHitProdTotalPrice(hitProdTotalPrice);
		
		//计算满减规则 
		calMarketingRule(marketingDto);
	}
	
	/**
	 * 计算满减规则 
	 */
	private void calMarketingRule(MarketingDto marketingDto) {
		//从高到低排序 规则
		List<MarketingRuleDto> arrays=sortRule(marketingDto.getMarketingRuleDtos());
		//根据金额或者件数，获得命中的 满减规则
		MarketingRuleDto rule=existRule(arrays, marketingDto.getHitProdTotalPrice(), marketingDto.getHitProdCount());
		
		if(rule!=null){//命中规则
			marketingDto.setTotalOffPrice(rule.getOffAmount());
			
//			//新增逻辑
//			List<ShopCartItem> shopCartItemList=marketingDto.getHitCartItems();
//			double usedXianShiAmout=0.0;
//			int shopCartItemSize=shopCartItemList.size();
//			for(int i=0;i<shopCartItemSize;i++){
//				ShopCartItem cartItem=shopCartItemList.get(i);
//				double xianShiOffPeice=0.0;
//				if(i==shopCartItemSize-1){
//					xianShiOffPeice=Arith.sub(marketingDto.getTotalOffPrice(), usedXianShiAmout);
//				}else{
//					double reductionRate=Arith.div(marketingDto.getTotalOffPrice(), marketingDto.getHitProdTotalPrice());//每一块钱所占的比例
//					xianShiOffPeice=Arith.mul(reductionRate,cartItem.getCalculatedCouponAmount());//此购物车项限时优惠的金额
//					usedXianShiAmout=Arith.add(usedXianShiAmout, xianShiOffPeice);
//				}
//				
//				double discountedPrice=Arith.sub(cartItem.getDiscountedPrice(),xianShiOffPeice);
//				//减去一个优惠活动之后的价格
//				cartItem.setDiscountedPrice(discountedPrice);
//			}
//			
			if(MarketingRuleCalTypeEnum.BY_MONEY.value().equals(rule.getCalType())){
				//满金额减
				marketingDto.setPromotionInfo("活动商品已购满¥"+rule.getFullPrice()+" （已减¥"+rule.getOffAmount()+"） ");
				
//				for(ShopCartItem item :marketingDto.getHitCartItems()){
//					double discountedPrice=Arith.sub(item.getDiscountedPrice(),Arith.mul(Arith.div(item.getDiscountedPrice(), rule.getFullPrice()),rule.getOffAmount()));
//					//减去一个优惠活动之后的价格
//					item.setDiscountedPrice(discountedPrice);
//				}
			}else if(MarketingRuleCalTypeEnum.BY_NUMBER.value().equals(rule.getCalType())){
				//满件减
				marketingDto.setPromotionInfo("活动商品已购满"+rule.getFullPrice().intValue()+"件 （已减¥"+rule.getOffAmount()+"） ");
				
//				//新增逻辑
//				for(ShopCartItem item :marketingDto.getHitCartItems()){
//					double discountedPrice=Arith.sub(item.getDiscountedPrice(),Arith.mul(Arith.div(item.getBasketCount(), rule.getFullPrice()),rule.getOffAmount()));
//					//减去一个优惠活动之后的价格
//					item.setDiscountedPrice(discountedPrice);
//				}
			}
		}else{//未达到条件，显示促销信息
			MarketingRuleDto lastRule = arrays.get(arrays.size()-1);
			marketingDto.setTotalOffPrice(0d);
			if(MarketingRuleCalTypeEnum.BY_MONEY.value().equals(lastRule.getCalType())){
				//满金额减
				marketingDto.setPromotionInfo("活动商品购满¥"+lastRule.getFullPrice()+"，即可享受满减");
			}else if(MarketingRuleCalTypeEnum.BY_NUMBER.value().equals(lastRule.getCalType())){
				//满件减
				marketingDto.setPromotionInfo("活动商品购满"+lastRule.getFullPrice().intValue()+"件，即可享受满减");
			}
		}
	}

	/**
     * 根据金额或者件数，获得命中的 满减规则
     */
	private MarketingRuleDto existRule(List<MarketingRuleDto> rules,Double price, int hitProdCount){
		if(AppUtils.isBlank(rules)){
			return null;
		}
		for (MarketingRuleDto marketingRuleDto : rules) {
			
			if(MarketingRuleCalTypeEnum.BY_MONEY.value().equals(marketingRuleDto.getCalType())){
				//满金额减
				if(price-marketingRuleDto.getFullPrice()>=0){
					return marketingRuleDto;
				}
			}else if(MarketingRuleCalTypeEnum.BY_NUMBER.value().equals(marketingRuleDto.getCalType())){
				//满件减
				if(hitProdCount-marketingRuleDto.getFullPrice().intValue()>=0){
					return marketingRuleDto;
				}
			}
		}
		return null;
	}
	

	/**
	 * 从高到低排序
	 * @return
	 */
	private List<MarketingRuleDto> sortRule(List<MarketingRuleDto> rules ){
		if(AppUtils.isBlank(rules)){
			return null;
		}
		Object[] arrays=rules.toArray();
		for (int i = 0; i < arrays.length; i++) {
			for (int j = i+1; j < arrays.length; j++) {
				MarketingRuleDto dtoi=(MarketingRuleDto)arrays[i];
				MarketingRuleDto dtoj=(MarketingRuleDto)arrays[j];
				if(dtoi.getOffAmount()<dtoj.getOffAmount()){
					MarketingRuleDto temp=dtoi;
					arrays[i]=arrays[j];
					arrays[j]=temp;
				}
			}
		}
		List<MarketingRuleDto> list=new ArrayList<MarketingRuleDto>();
		for (Object object : arrays) {
			list.add((MarketingRuleDto)object);
		}
		arrays=null;
		return list;
	}
	
}
