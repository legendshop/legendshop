package com.legendshop.business.service.timer.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.CashLog;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.framework.event.EventHome;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.PreDepositTimerService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;


/**
 * 用于定时器结算的分佣金服务
 * @author Tony
 *
 */
@Service("preDepositTimerService")
public class PreDepositTimerServiceImpl implements PreDepositTimerService  {

	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private ExpensesRecordDao expensesRecordDao;
	
	@Autowired
	private PdCashLogDao pdCashLogDao;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;
	
	
	/**
	 * 分销佣金收入,充值到预存款
	 * @param userId  用户ID
	 * @param amount  佣金收入金额
	 * @param subItemNumber 订单项流水号
	 * @return
	 */
	@Override
	public String rechargeForDistCommis(String userId,Double amount,String subItemNumber){
		CashLog.log("<! ------ recharge For Distribute Commis  flow ..  ");
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail=userDetailDao.getUserDetailByidForUpdate(userId);
		if(AppUtils.isBlank(userDetail)){
			CashLog.error(" Can't find userDetai by userId= {}  ------- !> ", userId);
			throw new BusinessException("佣金充值失败！ Can not find userDetail by userId "+ userId);
		}
		
		/*
		 * update user predeposit 
		 */
		Double available_predeposit=Arith.add(userDetail.getAvailablePredeposit(), amount);
		int count=userDetailDao.updateAvailablePredeposit(available_predeposit,userDetail.getAvailablePredeposit(),userId);
		if(count==0){
			CashLog.error("佣金充值失败  by userId ={} , updatePredeposit={} ,originalPredeposit = {} , subItemNumber ={}  ", userId,available_predeposit,userDetail.getAvailablePredeposit(),subItemNumber);
			throw new BusinessException("佣金充值失败 ！");
		}
		
		/*
		 * insert PdCashLog 
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userDetail.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.COMMISSION.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		StringBuilder sb = new StringBuilder();
		sb.append("分销佣金收入，分销流水号: ").append(subItemNumber);
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(subItemNumber);
		pdCashLogDao.savePdCashLog(pdCashLog);
		//发送站内信通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(userDetail.getUserName(),"分销佣金收入通知","分销的佣金"+amount+"已存入您的预存款，分销流水号:"+subItemNumber).getSource());
		CashLog.log("Recharge For Distribute Commis flow success -----！>  ");
		
		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord=new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark("分销佣金收入，分销流水号:"+subItemNumber);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
	}
}
