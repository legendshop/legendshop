/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PartnerDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Partner;
import com.legendshop.spi.service.PartnerService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 合作伙伴服务.
 */
@Service("partnerService")
public class PartnerServiceImpl implements PartnerService {

	@Autowired
	private PartnerDao partnerDao;
	
	@Autowired
	private AttachmentManager attachmentManager;

	public Partner getPartner(Long id) {
		return partnerDao.getPartner(id);
	}

	public void deletePartner(Partner partner) {
		if (AppUtils.isNotBlank(partner.getImage())) {
			// 删除原图片
			attachmentManager.deleteTruely(partner.getImage());
			// 删除附件表记录
			attachmentManager.delAttachmentByFilePath(partner.getImage());
		}
		if (AppUtils.isNotBlank(partner.getImage1())) {
			// 删除原图片
			attachmentManager.deleteTruely(partner.getImage1());
			// 删除附件表记录
			attachmentManager.delAttachmentByFilePath(partner.getImage1());
		}
		if (AppUtils.isNotBlank(partner.getImage2())) {
			// 删除原图片
			attachmentManager.deleteTruely(partner.getImage2());
			// 删除附件表记录
			attachmentManager.delAttachmentByFilePath(partner.getImage2());
		}
		partnerDao.deletePartner(partner);
	}

	public Long savePartner(Partner partner) {
		if (!AppUtils.isBlank(partner.getPartnerId())) {
			updatePartner(partner);
			return partner.getPartnerId();
		}
		return (Long) partnerDao.save(partner);
	}

	public void updatePartner(Partner partner) {
		partnerDao.updatePartner(partner);
	}

	public PageSupport getPartner(CriteriaQuery cq) {
		return partnerDao.queryPage(cq);
	}

	@Override
	public PageSupport<Partner> getPartnerPage(String curPageNO, Partner partner) {
		return partnerDao.getPartnerPage(curPageNO,partner);
	}
}
