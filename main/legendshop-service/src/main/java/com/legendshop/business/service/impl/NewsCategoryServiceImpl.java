/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.NewsCategoryDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.NewsCategory;
import com.legendshop.spi.service.NewsCategoryService;
import com.legendshop.util.AppUtils;

/**
 * 
 * 新闻分类服务
 */
@Service("newsCategoryService")
public class NewsCategoryServiceImpl implements NewsCategoryService {

	@Autowired
	private NewsCategoryDao newsCategoryDao;

	@Override
	public NewsCategory getNewsCategoryById(Long id) {
		return newsCategoryDao.getNewsCategoryById(id);
	}

	@Override
	public void delete(Long id) {
		newsCategoryDao.deleteNewsCategoryById(id);
	}


	@Override
	@CacheEvict(value="NewsList",allEntries=true)
	public Long save(NewsCategory newsCategory) {
		if (!AppUtils.isBlank(newsCategory.getNewsCategoryId())) {
			update(newsCategory);
			return newsCategory.getNewsCategoryId();
		}
		return (Long) newsCategoryDao.save(newsCategory);
	}

	@Override
	public void update(NewsCategory newsCategory) {
		newsCategoryDao.updateNewsCategory(newsCategory);
	}

	@Override
	public List<NewsCategory> getNewsCategoryList() {
		return this.newsCategoryDao.getNewsCategoryList();
	}

	@Override
	public PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO, NewsCategory newsCategory) {
		return newsCategoryDao.getNewsCategoryListPage(curPageNO,newsCategory);
	}

	@Override
	public PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO) {
		
		return newsCategoryDao.getNewsCategoryListPage(curPageNO);
	}
}
