/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.security.AdminAuthDao;
import com.legendshop.model.security.UserEntity;
import com.legendshop.spi.service.security.AdminAuthService;

/**
 * 后台权限过滤权限管理服务类
 * @author Tony
 *
 */
@Service("adminAuthService")
public class AdminAuthServiceImpl implements AdminAuthService {
	
	/** The jdbc template. */
	@Autowired
	private AdminAuthDao adminAuthDao;
	
	/**
	 * 后台用户登录
	 * 
	 */
	public UserEntity loadUserByUsername(String username, String presentPassword){
		return adminAuthDao.loadAdminUserByUsername(username, presentPassword);
		
	}
	
	/**
	 * 目前还没有调用，默认是前端登录
	 * remember me会调用
	 */
	@Override
	public UserEntity loadUserByUsername(String username) {
		return adminAuthDao.loadUserByUsername(username);
	}


}
