/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.dto.seckill.SuccessProdDto;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * 秒杀商品Dao.
 */
public interface SeckillProdDao extends Dao<SeckillProd, Long> {

	/** 根据sId获取秒杀商品 */
	public abstract List<SeckillProd> getSeckillProdBySid(Long sId);

	/** 获取秒杀商品 */
	public abstract SeckillProd getSeckillProd(Long id);

	/** 删除秒杀商品 */
	public abstract int deleteSeckillProd(SeckillProd seckillProd);

	/** 根据sId删除秒杀商品 */
	public abstract int deleteSeckillProdBySechillId(Long seckillId);

	/** 删除秒杀商品列表 */
	public abstract int deleteSeckillProds(List<SeckillProd> seckillProd);

	/** 保存秒杀商品 */
	public abstract Long saveSeckillProd(SeckillProd seckillProd);

	/** 更新秒杀商品 */
	public abstract int updateSeckillProd(SeckillProd seckillProd);

	/** 保存秒杀商品列表 */
	public abstract List<Long> saveSeckillProdList(List<SeckillProd> seckillProdList);

	/** 根据seckillId获取秒杀商品 */
	public abstract List<SeckillProd> getSeckillProdBySId(Long seckillId);

	/** 查找秒杀成功商品Dto */
	public abstract SuccessProdDto findSuccessProdDto(SeckillSuccess seckillSuccess);

	/** 检查秒杀商品是否可以参加秒杀活动 */
	public abstract boolean checkSeckillProdSku(Long prodId);

	/** 查询秒杀商品列表 */
	public abstract List<SeckillProd> querySeckillProdList(List<Long> ids);
	
	/** 获取用户秒杀成功信息 */
	public abstract SuccessProdDto getSeckillSuccessInfo(String userId, Long seckillId, Long skuId);

}
