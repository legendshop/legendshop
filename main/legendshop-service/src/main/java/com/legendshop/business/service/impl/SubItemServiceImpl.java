/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.ShopOrderBill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SubItemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.util.AppUtils;

/**
 * 订单项服务.
 */
@Service("subItemService")
public class SubItemServiceImpl  implements SubItemService{
	
	@Autowired
    private SubItemDao subItemDao;

    public List<SubItem> getSubItem(String subNumber,String userId) {
        return subItemDao.getSubItem(subNumber,userId);
    }

    public SubItem getSubItem(Long id) {
        return subItemDao.getSubItem(id);
    }

    public void deleteSubItem(SubItem subItem) {
        subItemDao.deleteSubItem(subItem);
    }

    public Long saveSubItem(SubItem subItem) {
        if (!AppUtils.isBlank(subItem.getSubItemId())) {
            updateSubItem(subItem);
            return subItem.getSubItemId();
        }
        return subItemDao.saveSubItem(subItem);
    }

    public void updateSubItem(SubItem subItem) {
        subItemDao.updateSubItem(subItem);
    }

	@Override
	public List<SubItem> getSubItem(String subNumber) {
		return subItemDao.getSubItem(subNumber);
	}

	@Override
	public void updateSnapshotIdById(Long snapshotId, Long subItemId) {
		subItemDao.updateSnapshotIdById(snapshotId,subItemId);
	}

	@Override
	@Caching(evict={@CacheEvict(value="UnCommProdCount",key="#userId")})
	public void updateSubItemCommSts(Long subItemId,String userId) {
		subItemDao.updateSubItemCommSts(subItemId);
	}

	@Override
	@Cacheable(value = "UnCommProdCount",key = "#userId")
	public Integer getUnCommProdCount(String userId) {
		return subItemDao.getUnCommProdCount(userId);
	}

	@Override
	public List<Long> saveSubItem(List<SubItem> subItems) {
		return subItemDao.saveSubItem(subItems);
	}

	/**
	 * 根据订单号查找订单项
	 */
	@Override
	public List<SubItem> getSubItemBySubId(Long subId) {
		return subItemDao.getSubItemBySubId(subId);
	}


	@Override
	public PageSupport<SubItem> querySubItems(String curPageNO, Long shopId, String distUserName, Integer status,
			String subItemNum, Date fromDate, Date toDate) {
		return subItemDao.querySubItems(curPageNO,shopId,distUserName,status,subItemNum,fromDate,toDate);
	}

	@Override
	public PageSupport<SubItem> querySubItems(String curPageNO, String userName, Integer status, String subItemNum,
			Date fromDate, Date toDate) {
		return subItemDao.querySubItems(curPageNO,userName,status,subItemNum,fromDate,toDate);
	}

	@Override
	public List<SubItem> getDistSubItemBySubNumber(String subNumber) {
		return subItemDao.getDistSubItemBySubNumber(subNumber);
	}

	@Override
	public List<SubItem> getNoCommonSubItemList(Date time){
		return subItemDao.getNoCommonSubItemList(time);
	}

	/**
	 * 查询结算分销订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<SubItem> queryDistSubItemList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {
		return subItemDao.queryDistSubItemList(curPageNO,shopId,subNumber,shopOrderBill);
	}

	@Override
	public List<SubItem> getSubItemByReturnId(Long refundId) {
		return subItemDao.getSubItemByReturnId(refundId);
	}
}
