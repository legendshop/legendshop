package com.legendshop.business.comparer;

import java.util.Date;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.util.AppUtils;

/**
 * 商品SKU对比器
 *
 */
public class ProdSkuComparator implements DataComparer<Sku,Sku>{

	@Override
	public boolean needUpdate(Sku newSku, Sku originSku, Object obj) {
		
		int number = 0;
		
		if (isChange(newSku.getStatus(), originSku.getStatus())) {
			originSku.setStatus(newSku.getStatus());
			number ++; 
		}
		
		if(isChange(newSku.getPrice(),originSku.getPrice())){
			originSku.setPrice(newSku.getPrice());
			number ++; 
		}

		if(isChange(newSku.getStocks(),originSku.getStocks())){
			Long diff = newSku.getStocks() - originSku.getStocks();
			originSku.setBeforeStock(originSku.getStocks());
			originSku.setBeforeActualStock(originSku.getActualStocks());
			originSku.setStocks(newSku.getStocks());
			if(originSku.getActualStocks()==null){
				originSku.setActualStocks(0L);
			}
			originSku.setActualStocks(originSku.getActualStocks() + diff);//实际库存按照虚拟库存的差距来设置
			number ++; 
		}	

		if(isChange(newSku.getName(),originSku.getName())){
			originSku.setName(newSku.getName());
			number ++; 
		}
		
		if(isChange(newSku.getCnProperties(),originSku.getCnProperties())){
			originSku.setCnProperties(newSku.getCnProperties());
			number ++; 
		}
		
		if(isChange(newSku.getPic(),originSku.getPic())){
			originSku.setPic(newSku.getPic());
			number ++; 
		}
		
		if(isChange(newSku.getPartyCode(),originSku.getPartyCode())){
			originSku.setPartyCode(newSku.getPartyCode());
			number ++; 
		}
		
		if(isChange(newSku.getModelId(),originSku.getModelId())){
			originSku.setModelId(newSku.getModelId());
			number ++; 
		}
		
		if(isChange(newSku.getVolume(),originSku.getVolume())){
			originSku.setVolume(newSku.getVolume());
			number ++; 
		}
		
		if(isChange(newSku.getWeight(), originSku.getWeight())){
			originSku.setWeight(newSku.getWeight());
			number ++; 
		}
		
		return number>0;
	}

	@Override
	public boolean isExist(Sku newSku, Sku originSku) {
		boolean result = false;
		String originProperties = originSku.getProperties();
//		if(AppUtils.isNotBlank(originProperties)){
//			if(originProperties.endsWith(";")){
//				originProperties = originProperties.substring(0, originProperties.length()-1);
//			}
//		}
		
//		String newProperties = null;
//		
//		if(AppUtils.isNotBlank(newSku.getProperties())){
//			if(newSku.getProperties().endsWith(";")){
//				newProperties = newSku.getProperties().substring(0,newSku.getProperties().length()-1);
//			}else{
//				newProperties = newSku.getProperties();
//			}
//		}
		
		String newProperties = newSku.getProperties();
		
		if(originProperties == null && newProperties == null){//两个为空则相等
			result = true;
		}else{
			result = newProperties != null && newProperties.equals(originProperties);
		}
		
		if(result){
			newSku.setSkuId(originSku.getSkuId());
		}
		
		
		return result;
	}

	@Override
	public Sku copyProperties(Sku newSku, Object obj) {
		Product prod = (Product)obj;
//		
//		Sku sku = new Sku();
//		Date date = new Date();
//		sku.setName(newSku.getName());
//		sku.setProperties(newSku.getProperties());
//		sku.setPrice(newSku.getPrice());
//		sku.setModelId(newSku.getModelId());
//		sku.setPartyCode(newSku.getPartyCode());
//		sku.setUserProperties(null);
//		sku.setStocks(AppUtils.isBlank(newSku.getStocks())?(long)0:newSku.getStocks());
//		sku.setActualStocks(AppUtils.isBlank(newSku.getStocks())?(long)0:newSku.getStocks());
//		sku.setStatus(AppUtils.isBlank(newSku.getStatus())?0:newSku.getStatus());
//		sku.setProdId(prod.getProdId());
//		sku.setModifyDate(date);
//		sku.setRecDate(date);
//		sku.setWareProdList(newSku.getWareProdList());
		
		
		
		Date date = new Date();
		newSku.setUserProperties(null);
		newSku.setStocks(AppUtils.isBlank(newSku.getStocks())?(long)0:newSku.getStocks());
		newSku.setActualStocks(AppUtils.isBlank(newSku.getStocks())?(long)0:newSku.getStocks());
		newSku.setStatus(AppUtils.isBlank(newSku.getStatus())?0:newSku.getStatus());
		newSku.setProdId(prod.getProdId());
		newSku.setModifyDate(date);
		newSku.setRecDate(date);
		return newSku;
	}

	
	/** 是否有改变 **/
	private  boolean isChange(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

}
