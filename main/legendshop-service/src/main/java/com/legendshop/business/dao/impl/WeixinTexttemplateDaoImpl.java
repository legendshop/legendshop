/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinTexttemplateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.weixin.WeixinTexttemplate;

/**
 * 微信文字模板服务Dao
 * 
 */
@Repository
public class WeixinTexttemplateDaoImpl extends GenericDaoImpl<WeixinTexttemplate, Long> implements WeixinTexttemplateDao  {

	public WeixinTexttemplate getWeixinTexttemplate(Long id){
		return getById(id);
	}
	
    public int deleteWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
    	return delete(weixinTexttemplate);
    }
	
	public Long saveWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
		return save(weixinTexttemplate);
	}
	
	public int updateWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
		return update(weixinTexttemplate);
	}
	
 }
