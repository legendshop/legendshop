package com.legendshop.business.dao.impl;

import com.legendshop.business.dao.AppTokenDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.AppToken;
import org.springframework.stereotype.Repository;

@Repository("AppTokenDao")
public class AppTokenDaoImpl extends GenericDaoImpl<AppToken, Long> implements AppTokenDao {


	@Override
	public boolean deleteAppToken(String userId) {
		return update("delete from ls_app_token where user_id = ?", userId) > 0;
	}
}
