/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinSubscribeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinSubscribe;

/**
 * 微信订阅的消息
 */
@Repository
public class WeixinSubscribeDaoImpl extends GenericDaoImpl<WeixinSubscribe, Long> implements WeixinSubscribeDao  {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public WeixinSubscribe getWeixinSubscribe(Long id){
		return getById(id);
	}
	
    public int deleteWeixinSubscribe(WeixinSubscribe weixinSubscribe){
    	return delete(weixinSubscribe);
    }
	
	public Long saveWeixinSubscribe(WeixinSubscribe weixinSubscribe){
		return save(weixinSubscribe);
	}
	
	public int updateWeixinSubscribe(WeixinSubscribe weixinSubscribe){
		return update(weixinSubscribe);
	}

	@Override
	public PageSupport<WeixinSubscribe> getWeixinSubscribePage(String curPageNO) {
		CriteriaQuery cq = new CriteriaQuery(WeixinSubscribe.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        cq.addDescOrder("addtime");
		return queryPage(cq);
	}
	
 }
