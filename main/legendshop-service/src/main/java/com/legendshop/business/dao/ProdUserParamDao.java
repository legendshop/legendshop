/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdUserParam;

import java.util.List;

/**
 * The Class ProdUserParamDao.
 */

public interface ProdUserParamDao extends Dao<ProdUserParam, Long> {
     
	public abstract ProdUserParam getProdUserParam(Long id);
	
    public abstract void deleteProdUserParam(Long id,Long shopId);
	
	public abstract Long saveProdUserParam(ProdUserParam prodUserParam);
	
	public abstract int updateProdUserParam(ProdUserParam prodUserParam);
	
	public abstract PageSupport<ProdUserParam> getProdUserParamPage(String curPageNO, Long shopId);

	public abstract PageSupport<ProdUserParam> getUserParamOverlay(String curPageNO, Long shopId);

	/**
	 * 获取商家自定义参数
	 * @param shopId
	 * @return
	 */
	List<ProdUserParam> getShopProdUserParam(Long shopId);
	
 }
