/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.store.impl;

import com.legendshop.business.dao.store.StoreProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.dto.StoreSkuDto;
import com.legendshop.model.dto.store.StoreCitys;
import com.legendshop.model.entity.store.StoreProd;

import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 门店商品管理
 */
@Repository
public class StoreProdDaoImpl extends GenericDaoImpl<StoreProd, Long> implements StoreProdDao {

	private final static Logger log = LoggerFactory.getLogger(StoreProdDaoImpl.class);

	/**
	 * 获取门店商品
	 */
	public StoreProd getStoreProd(Long id) {
		return getById(id);
	}

	/**
	 * 删除门店商品 
	 */
	public int deleteStoreProd(StoreProd storeProd) {
		return delete(storeProd);
	}

	/**
	 * 保存门店商品 
	 */
	public Long saveStoreProd(StoreProd storeProd) {
		return save(storeProd);
	}

	/**
	 * 更新门店商品 
	 */
	public int updateStoreProd(StoreProd storeProd) {
		return update(storeProd);
	}

	/**
	 * 主动建立主键，用于保存 
	 */
	public Long createId() {
		return super.createId();
	}

	/**
	 * 获取门店商品数量 
	 */
	@Override
	@Cacheable(value = "StoreProdCount", key = "#poductId+#skuId")
	public long getStoreProdCount(Long poductId, Long skuId) {
		if (skuId == null || skuId == 0) {
			return super.getLongResult("select count(id) from ls_store_prod where prod_id=? and stock > 0", poductId);
		} else {
			return super.getLongResult("select count(id) from ls_store_sku where prod_id=? and sku_id=? and stock > 0", poductId, skuId);
		}
	}

	/**
	 * 按商品查找每个城市的门店
	 */
	@Override
	public List<StoreCitys> findStoreCitys(Long poductId, String province, String city, String area) {
		if (poductId == null) {
			log.warn("findStoreCitys: poductId is null");
			return null;
		}

		QueryMap map = new QueryMap();
		
		if (!province.equals("-- 请选择 --")) {
			map.put("s_province", province);
		}
		if (!city.equals("-- 请选择 --")) {
			map.put("s_city", city);
		}
		if (!area.equals("-- 请选择 --")) {
			map.put("s_area", area);
		}
		map.put("prod_id", poductId);
		String sql = ConfigCode.getInstance().getCode("store.findStoreCitys", map);
		List<StoreCitys> citys = query(sql, map.toArray(), new RowMapper<StoreCitys>() {
			@Override
			public StoreCitys mapRow(ResultSet rs, int rowNum) throws SQLException {
				Long storeId = rs.getLong("id");
				String sName = rs.getString("name");
				String sAddress = rs.getString("address");
				String province = rs.getString("province");
				String city = rs.getString("city");
				String area = rs.getString("area");
				StoreCitys citys = new StoreCitys();
				citys.setStoreId(storeId);
				citys.setStoreName(sName);

				if (AppUtils.isNotBlank(province)){

					StringBuffer buffer = new StringBuffer(province);
					buffer.append(" ").append(city).append(" ").append(area).append(" ").append(sAddress);
					citys.setAddress(buffer.toString());
				}
				return citys;
			}
		});
		return citys;
	}

	/**
	 * 按商品查找每个城市的门店
	 */
	@Override
	public List<StoreCitys> findStoreShopCitys(Long shopId, String province, String city, String area) {
		if (shopId == null) {
			log.warn("findStoreShopCitys: shopId is null");
			return null;
		}

		QueryMap map = new QueryMap();
		map.put("s_province", province);
		map.put("s_city", city);
		map.put("s_area", area);
		map.put("shopId", shopId);
		String sql = ConfigCode.getInstance().getCode("store.findStoreShopCitys", map);
		List<StoreCitys> citys = query(sql, new Object[] { province, city, area, shopId }, new RowMapper<StoreCitys>() {
			@Override
			public StoreCitys mapRow(ResultSet rs, int rowNum) throws SQLException {
				Long storeId = rs.getLong("id");
				String sName = rs.getString("name");
				String sAddress = rs.getString("address");
				String province = rs.getString("province");
				String city = rs.getString("city");
				String area = rs.getString("area");
				StoreCitys citys = new StoreCitys();
				citys.setStoreId(storeId);
				citys.setStoreName(sName);
				StringBuffer buffer = new StringBuffer(province);
				buffer.append(" ").append(city).append(" ").append(area).append(" ").append(sAddress);
				citys.setAddress(buffer.toString());
				return citys;
			}
		});
		return citys;
	}

	/**
	 * 获取门店商品 
	 */
	@Override
	public Integer getStocks(Long storeId, Long poductId, Long skuId) {
		if (skuId == null || skuId == 0) {
			return super.get("select stock from ls_store_prod where prod_id=? and store_id=? ", Integer.class, poductId, storeId);
		} else {
			return super.get("select stock from ls_store_sku where prod_id=? and sku_id=? and store_id=? ", Integer.class, poductId, skuId, storeId);
		}
	}

	/**
	 * 获取门店商品Dto的SQL语句
	 */
	private String getStoreProductDtoSQL = "SELECT ss.sp_id AS spId, ss.store_id AS storeId,ss.prod_id AS prodId,p.name AS prodName, p.pic AS prodPic,ss.id AS ssId,s.sku_id AS skuId,"
			+ " s.name AS skuName, s.cn_properties AS properties, s.price AS skuPrice, ss.stock AS ssStock "
			+ " FROM ls_store_sku ss, ls_sku s, ls_prod  p WHERE s.sku_id = ss.sku_id AND p.prod_id = s.prod_id AND ss.prod_id = p.prod_id AND ss.store_id = ? AND ss.prod_id = ?";

	
	/**
	 * 找到门店的商品SKU
	 */
	@Override
	public StoreProductDto getStoreProductDto(Long storeId, Long prodId) {
		List<StoreProductDto> list = this.query(getStoreProductDtoSQL, new Object[]{storeId, prodId}, new RowMapper<StoreProductDto>() {
			@Override
			public StoreProductDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				rs.beforeFirst();

				if (!rs.next()) {
					return null;
				}
				StoreProductDto storeProductDto = null;

				storeProductDto = new StoreProductDto();
				storeProductDto.setId(rs.getLong("spId"));
				storeProductDto.setProductId(rs.getLong("prodId"));
				storeProductDto.setProductName(rs.getString("prodName"));
				// storeProductDto.setCash(rs.getDouble("prodCash"));
				storeProductDto.setStoreId(rs.getLong("storeId"));
				storeProductDto.setPic(rs.getString("prodPic"));
				// storeProductDto.setStock(rs.getLong("spStock"));

				if (rs.getLong("ssId") == 0) {
					return storeProductDto;
				}

				StoreSkuDto storeSkuDto = new StoreSkuDto();
				storeSkuDto.setId(rs.getLong("ssId"));
				storeSkuDto.setSkuId(rs.getLong("skuId"));
				storeSkuDto.setSkuName(rs.getString("skuName"));
				storeSkuDto.setProperties(rs.getString("properties"));
				storeSkuDto.setCash(rs.getDouble("skuPrice"));
				storeSkuDto.setStock(rs.getLong("ssStock"));

				List<StoreSkuDto> storeSkuDtos = new ArrayList<StoreSkuDto>();

				storeSkuDtos.add(storeSkuDto);

				while (rs.next()) {
					storeSkuDto = new StoreSkuDto();
					storeSkuDto.setId(rs.getLong("ssId"));
					storeSkuDto.setSkuId(rs.getLong("skuId"));
					storeSkuDto.setSkuName(rs.getString("skuName"));
					storeSkuDto.setProperties(rs.getString("properties"));
					storeSkuDto.setCash(rs.getDouble("skuPrice"));
					storeSkuDto.setStock(rs.getLong("ssStock"));

					storeSkuDtos.add(storeSkuDto);
				}

				storeProductDto.setStoreSkuDto(storeSkuDtos);

				return storeProductDto;
			}
		});
		if (AppUtils.isNotBlank(list)){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 获取门店商品 
	 */
	@Override
	public StoreProd getStoreProd(Long storeId, Long prodId) {
		return get("SELECT * FROM ls_store_prod AS sp WHERE sp.store_id = ? AND sp.prod_id = ?", StoreProd.class, storeId, prodId);
	}

	/**
	 * 用指定Id来建立 
	 */
	@Override
	public void saveStoreProd(StoreProd storeProd, Long spId) {
		this.save(storeProd, spId);
	}

	@Override
	public StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId) {
		return get("SELECT * FROM ls_store_prod AS sp WHERE sp.shop_id = ? AND sp.prod_id = ?", StoreProd.class, shopId, prodId);
	}
	
}
