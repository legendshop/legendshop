/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopNavigationDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopNavigation;
import com.legendshop.spi.service.ShopNavigationService;
import com.legendshop.util.AppUtils;

/**
 *
 * 商城导航服务
 *
 */
@Service("shopNavigationService")
public class ShopNavigationServiceImpl  implements ShopNavigationService{

	@Autowired
    private ShopNavigationDao shopNavigationDao;

    public List<ShopNavigation> getShopNavigationByShopId(Long shopId) {
        return shopNavigationDao.getShopNavigationByShopId(shopId);
    }

    public ShopNavigation getShopNavigation(Long id) {
        return shopNavigationDao.getShopNavigation(id);
    }

    public void deleteShopNavigation(ShopNavigation shopNavigation) {
        shopNavigationDao.deleteShopNavigation(shopNavigation);
    }

    public Long saveShopNavigation(ShopNavigation shopNavigation) {
        if (!AppUtils.isBlank(shopNavigation.getId())) {
            updateShopNavigation(shopNavigation);
            return shopNavigation.getId();
        }
        return (Long) shopNavigationDao.save(shopNavigation);
    }

    public void updateShopNavigation(ShopNavigation shopNavigation) {
        shopNavigationDao.updateShopNavigation(shopNavigation);
    }

	@Override
	public PageSupport<ShopNavigation> getShopNavigationPage(String curPageNO, ShopNavigation shopNavigation) {
		return shopNavigationDao.getShopNavigationPage(curPageNO,shopNavigation);
	}
}
