/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Partner;

/**
 * 合作伙伴Dao.
 */
public interface PartnerDao extends GenericDao<Partner, Long>{

	public abstract Partner getPartner(Long id);

	public abstract void deletePartner(Partner partner);

	public abstract Long savePartner(Partner partner);

	public abstract void updatePartner(Partner partner);

	public abstract PageSupport<Partner> getPartnerPage(String curPageNO, Partner partner);

}
