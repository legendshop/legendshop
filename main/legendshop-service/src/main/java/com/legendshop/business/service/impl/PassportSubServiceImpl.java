/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PassportSubDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PassportSub;
import com.legendshop.spi.service.PassportSubService;
import com.legendshop.util.AppUtils;

/**
 * The Class PassportSubServiceImpl.
 *  服务实现类
 */
@Service("passportSubService")
public class PassportSubServiceImpl  implements PassportSubService{
   
   /**
     * 引用的Dao接口
     */
    private PassportSubDao passportSubDao;

    public void setPassportSubDao(PassportSubDao passportSubDao) {
        this.passportSubDao = passportSubDao;
    }

   	/**
	 *  根据Id获取
	 */
    public PassportSub getPassportSub(Long id) {
        return passportSubDao.getPassportSub(id);
    }

   /**
	 *  删除
	 */ 
    public void deletePassportSub(PassportSub passportSub) {
        passportSubDao.deletePassportSub(passportSub);
    }

   /**
	 *  保存
	 */	    
    public Long savePassportSub(PassportSub passportSub) {
        if (!AppUtils.isBlank(passportSub.getId())) {
            updatePassportSub(passportSub);
            return passportSub.getId();
        }
        return passportSubDao.savePassportSub(passportSub);
    }

   /**
	 *  更新
	 */	
    public void updatePassportSub(PassportSub passportSub) {
        passportSubDao.updatePassportSub(passportSub);
    }

   /**
	 *  查询列表
	 */	
    public PageSupport getPassportSub(CriteriaQuery cq) {
        return passportSubDao.getPassportSub(cq);
    }
}
