/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopAudit;

/**
 * 店铺审核Dao
 */
public interface ShopAuditDao extends Dao<ShopAudit, Long> {
     
	public abstract ShopAudit getShopAudit(Long id);
	
	public abstract List<ShopAudit> getShopAuditInfo(Long shopId);
	
    public abstract int deleteShopAudit(ShopAudit shopAudit);
	
	public abstract Long saveShopAudit(ShopAudit shopAudit);
	
	public abstract int updateShopAudit(ShopAudit shopAudit);
	
	public abstract PageSupport<ShopAudit> getShopAuditPage(String curPageNO, Long shopId);
	
 }
