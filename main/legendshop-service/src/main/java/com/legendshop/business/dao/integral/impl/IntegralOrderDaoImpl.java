/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.integral.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.integral.IntegralOrderDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.IntegralOrderStatusEnum;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.integral.IntegralOrderItemDto;
import com.legendshop.model.dto.order.UsrAddrSubDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 积分订单dao实现类.
 */
@Repository
public class IntegralOrderDaoImpl extends GenericDaoImpl<IntegralOrder, Long> implements IntegralOrderDao {

	/** 获取积分订单 */
	public IntegralOrder getIntegralOrder(Long id) {
		return getById(id);
	}

	/** 删除积分订单 */
	public int deleteIntegralOrder(IntegralOrder integralOrder) {
		return delete(integralOrder);
	}

	/** 保存积分订单 */
	public Long saveIntegralOrder(IntegralOrder integralOrder) {
		return save(integralOrder);
	}

	/** 更新积分订单 */
	public int updateIntegralOrder(IntegralOrder integralOrder) {
		return update(integralOrder);
	}

	/** 获取积分订单Dto */
	@Override
	public List<IntegralOrderDto> getIntegralOrderDtos(String sql, List<Object> args) {
		List<IntegralOrderDto> dtos = super.query(sql, args.toArray(), new IntegralOrderDtoRowMapper());
		if (AppUtils.isBlank(dtos)) {
			return null;
		}
		Map<Long, IntegralOrderDto> map = new HashMap<Long, IntegralOrderDto>();
		for (Iterator<IntegralOrderDto> iterator = dtos.iterator(); iterator.hasNext();) {
			IntegralOrderDto integralOrderDto = (IntegralOrderDto) iterator.next();
			IntegralOrderItemDto copy = null;
			try {
				copy = (IntegralOrderItemDto) integralOrderDto.getOrderItemDto().clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}

			if (map.containsKey(integralOrderDto.getOrderId())) {
				IntegralOrderDto dto = map.get(integralOrderDto.getOrderId());
				dto.addOrderItemDtos(copy);
				integralOrderDto.setOrderItemDto(null);
				integralOrderDto.setUsrAddrSubDto(null);
			} else {
				integralOrderDto.addOrderItemDtos(copy);
				integralOrderDto.setOrderItemDto(null);
				map.put(integralOrderDto.getOrderId(), integralOrderDto);
			}
		}
		List<IntegralOrderDto> subDtos = new ArrayList<IntegralOrderDto>();
		for (Entry<Long, IntegralOrderDto> entry : map.entrySet()) {
			IntegralOrderDto mySubDto = entry.getValue();
			subDtos.add(mySubDto);
		}
		Collections.sort(subDtos, new Comparator<IntegralOrderDto>() {

			@Override
			public int compare(IntegralOrderDto o1, IntegralOrderDto o2) {
				int flag = o2.getAddTime().compareTo(o1.getAddTime());
				return flag;
			}
		});
		map = null;
		return subDtos;
	}

	class IntegralOrderDtoRowMapper implements RowMapper<IntegralOrderDto> {

		@Override
		public IntegralOrderDto mapRow(ResultSet rs, int rowNum) throws SQLException {

			// 查询积分订单
			IntegralOrderDto orderDto = new IntegralOrderDto();
			orderDto.setOrderId(rs.getLong("orderId"));
			orderDto.setOrderSn(rs.getString("orderSn"));
			orderDto.setUserId(rs.getString("userId"));
			orderDto.setUserName(rs.getString("userName"));
			orderDto.setProductNums(rs.getInt("productNums"));
			orderDto.setIntegralTotal(rs.getInt("integralTotal"));
			orderDto.setDvyTypeId(rs.getLong("dvyTypeId"));
			orderDto.setDvyFlowId(rs.getString("dvyFlowId"));
			orderDto.setOrderStatus(rs.getInt("orderStatus"));
			orderDto.setAddrOrderId(rs.getLong("addrOrderId"));

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				if (AppUtils.isNotBlank(rs.getString("addTime"))) {
					Date fromDate = dateFormat.parse(rs.getString("addTime"));
					orderDto.setAddTime(fromDate);
				}
				if (AppUtils.isNotBlank(rs.getDate("finallyTime"))) {
					Date date = dateFormat.parse(rs.getString("finallyTime"));
					orderDto.setFinallyTime(date);
				}
				if (AppUtils.isNotBlank(rs.getDate("dvyTime"))) {
					Date date = dateFormat.parse(rs.getString("dvyTime"));
					orderDto.setDvyTime(date);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			orderDto.setOrderDesc(rs.getString("orderDesc"));
			// 查询积分订单项;
			IntegralOrderItemDto orderItemDto = new IntegralOrderItemDto();
			orderItemDto.setOrderItemId(rs.getLong("orderItemId"));
			orderItemDto.setProdId(rs.getLong("prodId"));
			orderItemDto.setProdName(rs.getString("prodName"));
			orderItemDto.setProdPic(rs.getString("prodPic"));
			orderItemDto.setBasketCount(rs.getInt("basketCount"));
			orderItemDto.setExchangeIntegral(rs.getInt("exchangeIntegral"));
			orderItemDto.setPrice(rs.getDouble("price"));
			orderItemDto.setTotalIntegral(Arith.mul(orderItemDto.getBasketCount(), orderItemDto.getExchangeIntegral()));

			// 查询订单收货人地址
			UsrAddrSubDto usrAddrSubDto = new UsrAddrSubDto();
			usrAddrSubDto.setReceiver(rs.getString("receiver"));
			usrAddrSubDto.setSubAdds(rs.getString("subAdds"));
			usrAddrSubDto.setArea(rs.getString("area"));
			usrAddrSubDto.setCity(rs.getString("city"));
			usrAddrSubDto.setProvince(rs.getString("province"));
			usrAddrSubDto.setMobile(rs.getString("mobile"));
			usrAddrSubDto.setTelphone(rs.getString("telphone"));
			usrAddrSubDto.setSubPost(rs.getString("subPost"));
			usrAddrSubDto.setEmail(rs.getString("email"));
			orderDto.setOrderItemDto(orderItemDto);
			orderDto.setUsrAddrSubDto(usrAddrSubDto);
			return orderDto;

		}
	}

	/** 取消订单 */
	@Override
	public int orderCancel(String orderSn) {
		return this.update("update ls_integral_order set order_status=3 where order_status=0 and order_sn=? ", orderSn);
	}

	/** 根据订单流水号获取积分订单 */
	@Override
	public IntegralOrder getIntegralOrderByOrderSn(String orderSn) {
		List<IntegralOrder> list = this.queryByProperties(new EntityCriterion().eq("orderSn", orderSn));
		if (AppUtils.isNotBlank(list)) {
			return list.get(0);
		}
		return null;
	}

	/** 发货 */
	@Override
	public void fahuo(String orderSn, Long deliv, String dvyFlowId) {
		this.update("update ls_integral_order set dvy_type_id=?,dvy_flow_id=?,dvy_time=?,order_status=? where order_status=0 and order_sn=?",
				new Object[] { deliv, dvyFlowId, new Date(), IntegralOrderStatusEnum.CONSIGNMENT.value(), orderSn });
	}

	private static String ORDER_DETAIL_SQL = "SELECT o.id AS orderId,o.order_sn AS orderSn,o.user_id AS userId,o.user_name AS userName ,o.product_nums AS productNums,o.integral_total AS integralTotal,o.dvy_type_id AS dvyTypeId,o.dvy_flow_id AS dvyFlowId,o.order_status AS orderStatus,o.addr_order_id AS addrOrderId,o.add_time AS addTime,"
			+ "o.dvy_time AS dvyTime,o.finally_time AS finallyTime,o.order_desc AS orderDesc"
			+ ",item.id AS orderItemId,item.prod_id AS prodId,item.prod_name AS prodName,item.prod_pic AS prodPic,item.basket_count AS basketCount,"
			+ " item.exchange_integral AS exchangeIntegral,item.price AS price,addr.receiver  AS receiver,addr.detail_address AS subAdds,addr.sub_post AS subPost,addr.province_id AS province,addr.city_id AS city,"
			+ " addr.area_id AS AREA,addr.mobile AS mobile, addr.telphone AS telphone,addr.email AS email ,addr.sub_post AS subPost from  (FROM_SUB_SQL)  o  "
			+ " INNER JOIN ls_usr_addr_sub addr ON addr.addr_order_id=o.addr_order_id INNER JOIN ls_integral_order_item item ON item.order_id=o.id ";

	/** 查找积分订单明细 */
	@Override
	public IntegralOrderDto findIntegralOrderDetail(String ordeSn) {
		StringBuilder limitSql = new StringBuilder();
		this.getDialect().getLimitString(limitSql, "SELECT o.* FROM ls_integral_order o WHERE 1=1 AND o.order_sn=?", 0, 1);

		String executeSQL = ORDER_DETAIL_SQL.replace("FROM_SUB_SQL", limitSql.toString());

		List<IntegralOrderDto> dtos = super.query(executeSQL, new Object[] { ordeSn }, new IntegralOrderDtoRowMapper());
		if (AppUtils.isBlank(dtos)) {
			return null;
		}
		Map<Long, IntegralOrderDto> map = new HashMap<Long, IntegralOrderDto>();
		for (Iterator<IntegralOrderDto> iterator = dtos.iterator(); iterator.hasNext();) {
			IntegralOrderDto integralOrderDto = (IntegralOrderDto) iterator.next();
			IntegralOrderItemDto copy = null;
			try {
				copy = (IntegralOrderItemDto) integralOrderDto.getOrderItemDto().clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			if (map.containsKey(integralOrderDto.getOrderId())) {
				IntegralOrderDto dto = map.get(integralOrderDto.getOrderId());
				dto.addOrderItemDtos(copy);
				integralOrderDto.setOrderItemDto(null);
				integralOrderDto.setUsrAddrSubDto(null);
			} else {
				integralOrderDto.addOrderItemDtos(copy);
				integralOrderDto.setOrderItemDto(null);
				map.put(integralOrderDto.getOrderId(), integralOrderDto);
			}
		}
		IntegralOrderDto integralOrderDto = null;
		for (Entry<Long, IntegralOrderDto> entry : map.entrySet()) {
			IntegralOrderDto mySubDto = entry.getValue();
			integralOrderDto = mySubDto;
			break;
		}
		map = null;
		return integralOrderDto;
	}

	/** 删除订单 */
	@Override
	public String orderDel(IntegralOrder integralOrder) {
		int result = this.delete(integralOrder);
		if (result > 0) {
			this.update("delete from ls_integral_order_item where order_id=? ", integralOrder.getId());
		}
		return Constants.SUCCESS;
	}

	/** 获取优惠卷列表页面 */
	@Override
	public PageSupport<Coupon> getCouponListPage(String curPageNO, String type) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class, 6, curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		String querySQL;
		String queryAllSQL;
		map.put("getType", CouponGetTypeEnum.POINTS.value());
		map.put("endDate", nowDate);
		if (type.equals(CouponProviderEnum.PLATFORM.value())) {
			map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByCategory", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByCategory", map);
		} else {
			map.put("couponProvider", CouponProviderEnum.SHOP.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCenterCountByShop", map);
		}
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		return querySimplePage(q);
	}

	@Override
	public IntegralOrder getIntegralOrder(String orderSn, String userId) {
		
		return this.getByProperties(new EntityCriterion().eq("orderSn", orderSn).eq("userId", userId));
	}

}
