/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductImport;

/**
 * 商品导入Dao.
 */

public interface ProductImportDao extends Dao<ProductImport, Long> {
     
	public abstract ProductImport getProductImport(Long id);
	
    public abstract int deleteProductImport(ProductImport productImport);
	
	public abstract Long saveProductImport(ProductImport productImport);
	
	public abstract int updateProductImport(ProductImport productImport);
	
	/**
	 * 批量保存到数据库
	 * @param importList
	 */
	public abstract void saveProductImport(List<ProductImport> importList);
	
	/**
	 * 批量删除上传的数据
	 */
	public abstract void deleyeProductImport(List<Long> ids);

	public abstract void deleteProductImportById(Long impId);

	public abstract PageSupport<ProductImport> getProductImportPage(String curPageNO, ProductImport productImport,
			Long shopId);
 }
