package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.TransfeeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Transfee;
import com.legendshop.spi.service.TransfeeService;
/**
 * 运输费用服务
 */
@Service("transfeeService")
public class TransfeeServiceImpl implements TransfeeService {

	@Autowired
	private TransfeeDao transfeeDao;

	public List<Transfee> getTransfee() {
		return transfeeDao.getTransfee();
	}

	public Transfee getTransfee(Long id) {
		return transfeeDao.getTransfee(id);
	}

	public void deleteTransfee(Transfee transfee) {
		deleteTransfee(transfee);
	}

	public Long saveTransfee(Transfee transfee) {
		return transfeeDao.saveTransfee(transfee);
	}

	public void updateTransfee(Transfee transfee) {
		transfeeDao.updateTransfee(transfee);
	}

	public List<Transfee> getTranfee(Long transportId) {
		return transfeeDao.getTranfee(transportId);
	}

	public List<Transfee> getTranfeeByShopId(Long shopId) {
		return transfeeDao.getTranfeeByShopId(shopId);
	}

	@Override
	public PageSupport<Transfee> getTransfeePage(String curPageNO, Transfee transfee) {
		return transfeeDao.getTransfeePage(curPageNO,transfee);
	}
}
