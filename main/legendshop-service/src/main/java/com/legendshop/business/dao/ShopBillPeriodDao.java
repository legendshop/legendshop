/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopBillPeriod;

/**
 * 结算档期Dao
 */
public interface ShopBillPeriodDao extends Dao<ShopBillPeriod, Long> {
     
	public abstract ShopBillPeriod getShopBillPeriod(Long id);
	
    public abstract int deleteShopBillPeriod(ShopBillPeriod shopBillPeriod);
	
	public abstract Long saveShopBillPeriod(ShopBillPeriod shopBillPeriod);
	
	public abstract int updateShopBillPeriod(ShopBillPeriod shopBillPeriod);
	
	public abstract void saveShopBillPeriodWithId(ShopBillPeriod shopBillPeriod, Long id);

	public abstract Long getShopBillPeriodId();

	public abstract PageSupport<ShopBillPeriod> getShopBillPeriod(String curPageNO, ShopBillPeriod shopBillPeriod);
	
 }
