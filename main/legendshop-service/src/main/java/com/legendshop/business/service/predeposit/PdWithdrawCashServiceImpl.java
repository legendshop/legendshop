/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.predeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.predeposit.PdWithdrawCashDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.spi.service.PdWithdrawCashService;
import com.legendshop.util.AppUtils;

/**
 * 预存款提现服务实现类
 * 
 */
@Service("pdWithdrawCashService")
public class PdWithdrawCashServiceImpl implements PdWithdrawCashService {
	
	@Autowired
	private PdWithdrawCashDao pdWithdrawCashDao;

	public PdWithdrawCash getPdWithdrawCash(Long id) {
		return pdWithdrawCashDao.getPdWithdrawCash(id);
	}

	/**
	 * 删除预存款提现
	 */
	public void deletePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		pdWithdrawCashDao.deletePdWithdrawCash(pdWithdrawCash);
	}

	/**
	 * 保存预存款提现
	 */
	public Long savePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		if (!AppUtils.isBlank(pdWithdrawCash.getId())) {
			updatePdWithdrawCash(pdWithdrawCash);
			return pdWithdrawCash.getId();
		}
		return pdWithdrawCashDao.savePdWithdrawCash(pdWithdrawCash);
	}

	/**
	 * 更新预存款提现
	 */
	public void updatePdWithdrawCash(PdWithdrawCash pdWithdrawCash) {
		pdWithdrawCashDao.updatePdWithdrawCash(pdWithdrawCash);
	}

	/**
	 * 获取预存款提现
	 */
	@Override
	public PdWithdrawCash getPdWithdrawCash(Long id, String userId) {
		return pdWithdrawCashDao.getPdWithdrawCash(id, userId);
	}
	
	/**
	 * 获取预存款提现
	 */
	@Override
	public PageSupport<PdWithdrawCash> getPdWithdrawCashPage(String curPageNO, String userId, String state) {
		return pdWithdrawCashDao.getPdWithdrawCashPage(curPageNO, userId, state);
	}

	/**
	 * 获取预存款提现列表
	 */
	@Override
	public PageSupport<PdWithdrawCash> getPdWithdrawCashListPage(String curPageNO, PdWithdrawCash pdWithdrawCash, DataSortResult result, Integer pageSize) {
		return pdWithdrawCashDao.getPdWithdrawCashListPage(curPageNO, pdWithdrawCash, result, pageSize);
	}

	/**
	 * 获取预存款余额提现
	 */
	@Override
	public PageSupport<PdWithdrawCash> getFindBalanceWithdrawal(String curPageNO, String pdcSn, Integer status, String userId) {
		return pdWithdrawCashDao.getFindBalanceWithdrawal(curPageNO, pdcSn, status, userId);
	}

	@Override
	public PageSupport getPdWithdrawCashByState(String userId,String state, String curPageNO, Integer pageSize) {
		return pdWithdrawCashDao.getPdWithdrawCashByState(userId, state, curPageNO, pageSize);
	}
}
