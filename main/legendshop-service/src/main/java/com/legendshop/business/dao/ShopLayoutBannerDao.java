/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;

/**
 * The Class ShopLayoutBannerDao.
 */

public interface ShopLayoutBannerDao extends GenericDao<ShopLayoutBanner, Long> {
     
    public abstract List<ShopLayoutBanner> getShopLayoutBannerByShopId(Long  shopId);

	public abstract ShopLayoutBanner getShopLayoutBanner(Long id);
	
    public abstract int deleteShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);
	
	public abstract Long saveShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);
	
	public abstract int updateShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);
	
	public abstract List<ShopLayoutBanner> getShopLayoutBanner(Long decId,
			Long layoutId);

	public abstract void saveShopLayoutBanner(List<ShopLayoutBanner> addPermList);

	
 }
