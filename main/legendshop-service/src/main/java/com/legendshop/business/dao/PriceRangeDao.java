/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PriceRange;

/**
 * 统计报表的价格范围.
 */
public interface PriceRangeDao extends Dao<PriceRange, Long>{

	/**
	 * Query price range.
	 *
	 * @param shopId the shop id
	 * @param type the type
	 * @return the list
	 */
	public abstract List<PriceRange> queryPriceRange(Long shopId,Integer type);

	/**
	 * Gets the price range.
	 *
	 * @param id the id
	 * @return the price range
	 */
	public abstract PriceRange getPriceRange(Long id);
	
    /**
     * Delete price range.
     *
     * @param priceRange the price range
     * @return the int
     */
    public abstract int deletePriceRange(PriceRange priceRange);
	
	/**
	 * Save price range.
	 *
	 * @param priceRange the price range
	 * @return the long
	 */
	public abstract Long savePriceRange(PriceRange priceRange);
	
	/**
	 * Update price range.
	 *
	 * @param priceRange the price range
	 * @return the int
	 */
	public abstract int updatePriceRange(PriceRange priceRange);
	
	/**
	 * Update price range list.
	 *
	 * @param priceRangeList the price range list
	 */
	public abstract void updatePriceRangeList(List<PriceRange> priceRangeList);

	/**
	 * Save price range list.
	 *
	 * @param priceRangeList the price range list
	 */
	public abstract void savePriceRangeList(List<PriceRange> priceRangeList);

	public abstract PageSupport<PriceRange> getPriceRange(String curPageNO, Long shopId, Integer type);
	
}
