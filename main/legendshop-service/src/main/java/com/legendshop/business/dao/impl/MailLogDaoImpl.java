/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MailLogDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailLog;
import com.legendshop.util.AppUtils;
/**
 * 邮件发送历史Dao.
 */
@Repository
public class MailLogDaoImpl extends GenericDaoImpl<MailLog, Long> implements MailLogDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public MailLog getMailLog(Long id){
		return getById(id);
	}
	
    public void deleteMailLog(MailLog mailLog){
    	delete(mailLog);
    }
	
	public Long saveMailLog(MailLog mailLog){
		return (Long)save(mailLog);
	}
	
	public void updateMailLog(MailLog mailLog){
		 update(mailLog);
	}

	@Override
	public PageSupport<MailLog> getMailLogPage(String curPageNO, MailLog mailLog) {
		CriteriaQuery cq = new CriteriaQuery(MailLog.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        if(AppUtils.isNotBlank(mailLog.getReceiveMail())) {       	
        	cq.like("receiveMail", "%" + mailLog.getReceiveMail().trim() + "%");
        }
        cq.addDescOrder("sendTime");
		return queryPage(cq);
	}
	
 }
