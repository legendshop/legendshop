/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ZhiboNewLiveRecordDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.spi.service.ZhiboNewLiveRecordService;
import com.legendshop.util.AppUtils;


/**
 * The Class ZhiboNewLiveRecordServiceImpl. 新版直播记录表服务实现类
 */
@Service("zhiboNewLiveRecordService")
public class ZhiboNewLiveRecordServiceImpl implements ZhiboNewLiveRecordService {

	@Autowired
	private ZhiboNewLiveRecordDao zhiboNewLiveRecordDao;

	/**
	 * 根据Id获取新版直播记录表
	 */
	public ZhiboNewLiveRecord getZhiboNewLiveRecord(Long id) {
		return zhiboNewLiveRecordDao.getZhiboNewLiveRecord(id);
	}

	/**
	 * 删除新版直播记录表
	 */
	public void deleteZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		zhiboNewLiveRecordDao.deleteZhiboNewLiveRecord(zhiboNewLiveRecord);
	}

	/**
	 * 保存新版直播记录表
	 */
	public Long saveZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		if (!AppUtils.isBlank(zhiboNewLiveRecord.getId())) {
			updateZhiboNewLiveRecord(zhiboNewLiveRecord);
			return zhiboNewLiveRecord.getId();
		}
		return zhiboNewLiveRecordDao.saveZhiboNewLiveRecord(zhiboNewLiveRecord);
	}

	/**
	 * 更新新版直播记录表
	 */
	public void updateZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord) {
		zhiboNewLiveRecordDao.updateZhiboNewLiveRecord(zhiboNewLiveRecord);
	}

	@Override
	public List<ZhiboNewLiveRecord> getRoomList(Integer index, Integer size) {
		return zhiboNewLiveRecordDao.getRoomList(index, size);
	}

	@Override
	public int deleteUid(String id, Integer roomid) {
		return zhiboNewLiveRecordDao.deleteUid(id, roomid);
	}

	@Override
	public int clearRoom(Integer id) {
		return zhiboNewLiveRecordDao.clearRoom(id);
	}

	/**
	 * 检查房间ID是否存在
	 */
	@Override
	public ZhiboNewLiveRecord getRoomById(Integer roomnum) {
		return zhiboNewLiveRecordDao.getRoomById(roomnum);
	}
	
	/**
	 * 获取房间成员,成功返回房间成员信息，失败返回空
	 * @return
	 */
	@Override
	public List<ZhiboInteractAvRoom> getRoomidList(Integer index, Integer size, Integer roomid) {
		return zhiboNewLiveRecordDao.getRoomidList(index, size, roomid);
	}
	
	/**
	 * 检查成员是否已经在房间内
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getInfoByUid(String uid) {
		return zhiboNewLiveRecordDao.getInfoByUid(uid);
	}
	
	/**
	 * 成员进入房间
	 */
	@Override
	public int enterRoom(String uid, Integer roomid, String status,
			String modifyTime, Integer role) {
		return zhiboNewLiveRecordDao.enterRoom(uid, roomid, status, modifyTime, role);
	}
	
	/**
	 * 成员退出房间
	 */
	@Override
	public int exitRoom(String uid) {
		return zhiboNewLiveRecordDao.exitRoom(uid);
	}

	@Override
	public PageSupport<ZhiboNewLiveRecord> getRoom(String curPageNO, String avRoomId) {
		return zhiboNewLiveRecordDao.getRoom(curPageNO,avRoomId);
	}
}
