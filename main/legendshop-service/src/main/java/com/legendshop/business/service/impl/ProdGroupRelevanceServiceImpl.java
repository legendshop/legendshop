/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.util.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdGroupRelevanceDao;
import com.legendshop.model.entity.ProdGroupRelevance;
import com.legendshop.spi.service.ProdGroupRelevanceService;

/**
 * The Class ProdGroupRelevanceServiceImpl.
 *  服务实现类
 */
@Service("prodGroupRelevance")
public class ProdGroupRelevanceServiceImpl implements ProdGroupRelevanceService{

    /**
     *
     * 引用的Dao接口
     */
	@Autowired
    private ProdGroupRelevanceDao prodGroupRelevanceDao;
   
   	/**
	 *  根据Id获取
	 */
    public ProdGroupRelevance getProdGroupRelevance(Long id) {
        return prodGroupRelevanceDao.getProdGroupRelevance(id);
    }
    
    /**
	 *  根据Id删除
	 */
    public int deleteProdGroupRelevance(Long id){
    	return prodGroupRelevanceDao.deleteProdGroupRelevance(id);
    }

    public int getProductByGroupId(Long prod, Long groupId){
    	return prodGroupRelevanceDao.getProductByGroupId(prod, groupId);
    }
    
    /**
   	 *  根据prodId删除
   	 */
   public int deleteProdGroupRelevanceByprod(Long id){
   	return prodGroupRelevanceDao.deleteProdGroupRelevanceByprod(id);
   }

   /**
	 *  删除
	 */ 
    public int deleteProdGroupRelevance(ProdGroupRelevance prodGroupRelevance) {
       return  prodGroupRelevanceDao.deleteProdGroupRelevance(prodGroupRelevance);
    }

   /**
	 *  保存
	 */	    
    public Long saveProdGroupRelevance(ProdGroupRelevance prodGroupRelevance) {
        if (!AppUtils.isBlank(prodGroupRelevance.getId())) {
            updateProdGroupRelevance(prodGroupRelevance);
            return prodGroupRelevance.getId();
        }
        return prodGroupRelevanceDao.saveProdGroupRelevance(prodGroupRelevance);
    }

   /**
	 *  更新
	 */	
    public void updateProdGroupRelevance(ProdGroupRelevance prodGroupRelevance) {
        prodGroupRelevanceDao.updateProdGroupRelevance(prodGroupRelevance);
    }

	/**
	 * 移除分组内商品
	 * @param prodId
	 * @param groupId
	 */
	@Override
	public void removeProd(Long prodId, Long groupId) {

		prodGroupRelevanceDao.removeProd(prodId,groupId);
	}


    /**
	 *  分页查询列表
	 */	
    public PageSupport<ProdGroupRelevance> queryProdGroupRelevance(String curPageNO, Integer pageSize){
     	return prodGroupRelevanceDao.queryProdGroupRelevance(curPageNO, pageSize);
    }


	/**
	 *  设置Dao实现类
	 */	    
    public void setProdGroupRelevanceDao(ProdGroupRelevanceDao prodGroupRelevanceDao) {
        this.prodGroupRelevanceDao = prodGroupRelevanceDao;
    }
    
}
