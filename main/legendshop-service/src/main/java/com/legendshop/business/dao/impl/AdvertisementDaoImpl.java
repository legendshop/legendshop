/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AdvertisementDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Advertisement;

/**
 * 广告Dao.
 */
@Repository
public class AdvertisementDaoImpl  extends GenericDaoImpl<Advertisement, Long>  implements AdvertisementDao {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AdvertisementDaoImpl.class);

	@Value("3")
	private Integer maxNumPerType;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 得到某个页面的广告
	 */
	@Override
	@Cacheable(value = "Advertisement", key = "#shopId + #page")
	public Map<String, List<Advertisement>> getAdvertisement(final Long shopId, final String page) {
		log.debug("getAdvertisement shopName = {}, page = {}", shopId, page);
		Map<String, List<Advertisement>> advertisementMap = new LinkedHashMap<String, List<Advertisement>>();
		EntityCriterion criterion = new EntityCriterion();
		criterion.eq("enabled", 1);
		criterion.eq("shopId", shopId);
		criterion.like("type", page, MatchMode.START);
		List<Advertisement> list =queryByProperties(criterion);
		for (Advertisement advertisement : list) {
			List<Advertisement> ads = advertisementMap.get(advertisement.getType());
			if (ads == null) {
				ads = new ArrayList<Advertisement>();
			}
			ads.add(advertisement);
			advertisementMap.put(advertisement.getType(), ads);
		}
		return advertisementMap;
	}

	/**
	 * 获取广告上限
	 */
	@Override
	public boolean isMaxNum(Long shopId, String type) {
		boolean result = false;
		Long num = getLongResult("select count(*) from ls_adv where shop_id = ? and type = ?", shopId, type);
		if (num != null) {
			result = num <= maxNumPerType;
		}
		if (log.isDebugEnabled()) {
			log.debug("shopId = {},type = {},result = {}", new Object[] { shopId, type, result });
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.impl.AdvertisementDao#setMaxNumPerType(java
	 * .lang.Integer)
	 */
	public void setMaxNumPerType(Integer maxNumPerType) {
		this.maxNumPerType = maxNumPerType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.dao.AdvertisementDao#deleteAdvById(java.lang.
	 * Long)
	 */
	@Override
	public void deleteAdvById(Long id) {
		deleteById(id);
	}

	@Override
	@CacheEvict(value = "Advertisement", key = "#advertisement.id")
	public void updateAdv(Advertisement advertisement) {
		update(advertisement);

	}

	@Override
	public Advertisement getAdvertisementById(Long id) {
		return getById(id);
	}

	@Override
	public PageSupport<Advertisement> queryAdvertisement(String curPageNO, Advertisement advertisement) {
		CriteriaQuery cq = new CriteriaQuery(Advertisement.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		cq.addOrder("asc", "type");
		return queryPage(cq);
	}
}
