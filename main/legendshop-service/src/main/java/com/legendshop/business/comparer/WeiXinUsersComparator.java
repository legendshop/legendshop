package com.legendshop.business.comparer;

import java.util.Date;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.weixin.WeiXinGzUserInfoDto;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

public class WeiXinUsersComparator implements DataComparer<WeiXinGzUserInfoDto,WeixinGzuserInfo> {
/**
 * 微信本地用户和服务器用户对比类
 * @author liyuan
 */
	@Override
	public boolean needUpdate(WeiXinGzUserInfoDto dto, WeixinGzuserInfo dbObj, Object obj) {
		if(isChage(dto,dbObj)){
			dbObj.setNickname((dto.getNickname()));
			dbObj.setSex(dto.getSex());
			dbObj.setCity((dto.getCity()));
			dbObj.setProvince(dto.getProvince());
			dbObj.setCountry(dto.getCountry());
			dbObj.setHeadimgurl((dto.getHeadimgurl()));
			dbObj.setBzname(dto.getRemark());
			dbObj.setGroupid(dto.getGroupid());
			return true;
		}
		return false;
	}

	@Override
	public boolean isExist(WeiXinGzUserInfoDto dto, WeixinGzuserInfo dbObj) {
		if(AppUtils.isNotBlank(dto) && AppUtils.isNotBlank(dbObj) && AppUtils.isTheSame(dto.getOpenid(), dbObj.getOpenid())){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public WeixinGzuserInfo copyProperties(WeiXinGzUserInfoDto dtoj, Object obj) {
		WeixinGzuserInfo gzuser=new WeixinGzuserInfo();
		if(dtoj.getSubscribe()==1){
			gzuser.setSubscribe("Y");
		}else{
			gzuser.setSubscribe("N");
		}
		gzuser.setOpenid(dtoj.getOpenid());
		gzuser.setNickname(dtoj.getNickname());
		gzuser.setSex(dtoj.getSex());
		gzuser.setCity(dtoj.getCity());
		gzuser.setProvince(dtoj.getProvince());
		gzuser.setCountry(dtoj.getCountry());
		gzuser.setGroupid(dtoj.getGroupid());
		gzuser.setHeadimgurl(dtoj.getHeadimgurl());
		gzuser.setBzname(dtoj.getRemark());
		gzuser.setSubscribeTime(new Date(Long.valueOf(dtoj.getSubscribe_time())));
		return gzuser;
	}
	/** 是否有改变 **/
	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private  boolean isChage(WeiXinGzUserInfoDto a, WeixinGzuserInfo b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if(!AppUtils.isTheSame(a.getNickname(), b.getNickname()) || 
				!AppUtils.isTheSame(a.getSex(),b.getSex()) ||
				!AppUtils.isTheSame(a.getCity(),b.getCity()) || 
				!AppUtils.isTheSame(a.getProvince(), b.getProvince())||
				!AppUtils.isTheSame(a.getCountry(), b.getCountry()) ||
				!AppUtils.isTheSame(a.getHeadimgurl(), b.getHeadimgurl()) ||
				!AppUtils.isTheSame(a.getRemark(), b.getBzname())  ||  
				!AppUtils.isTheSame(a.getGroupid(), b.getGroupid())){
				return true;
			}else{
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}
}
