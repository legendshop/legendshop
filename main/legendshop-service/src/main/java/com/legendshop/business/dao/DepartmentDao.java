/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.Department;

/**
 * 部门Dao.
 */
public interface DepartmentDao extends Dao<Department, Long> {
     
	public abstract Department getDepartment(Long id);
	
    public abstract int deleteDepartment(Department department);
	
	public abstract Long saveDepartment(Department department);
	
	public abstract int updateDepartment(Department department);
	
	public abstract List<Department> getAllDepartment();

	public abstract String getDepartmentName(Long deptId);
	
 }
