/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.CategoryComm;

public interface CategoryCommDao extends Dao<CategoryComm, Long> {
	
	List<CategoryComm> getCategoryCommListByShopId(Long shopId);
	    
	void delCategoryComm(Long id);
	    
	Long saveCategoryComm(CategoryComm categoryComm);

	CategoryComm getCategoryComm(Long categoryId, Long shopId);

	void delCategoryComm(Long id, Long shopId);
 }
