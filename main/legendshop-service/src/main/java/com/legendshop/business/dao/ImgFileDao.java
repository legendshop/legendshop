/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ImgFile;

/**
 * 产品图片Dao.
 */
public interface ImgFileDao extends GenericDao<ImgFile, Long>{

	/**
	 *  得到所以的产品的描述图片
	 *
	 */
	public abstract List<ImgFile> getAllProductPics(final Long prodId);
	
	/**
	 * 根据商品ID清除缓存
	 */
	public abstract void clearProductPicsCache(final Long prodId);

	public abstract void deleteImgFileById(Long id);

	public abstract void updateImgFile(ImgFile imgFile);

	public abstract void deleteImgFile(ImgFile imgFile);

	public abstract List<String> queryProductPics(Long prodId);

	public abstract PageSupport<ImgFile> getImgFileList(String curPageNO, Long productId, ImgFile imgFile);


}