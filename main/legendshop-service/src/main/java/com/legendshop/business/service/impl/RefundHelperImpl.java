package com.legendshop.business.service.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.log.RefundLog;
import com.legendshop.business.dao.SubRefundReturnDao;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.constant.PrePayTypeEnum;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.model.vo.DrawRecharge;
import com.legendshop.spi.service.PreDepositService;
import com.legendshop.spi.service.RefundHelper;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

@Service("refundHelper")
public class RefundHelperImpl implements RefundHelper {

	@Autowired
	private SubRefundReturnDao subRefundReturnDao;
	
	@Autowired
	private PreDepositService preDepositService;
	
	@Resource(name="weiXinDrawSuccessProcessor")
	private EventProcessor<DrawRecharge> weiXinDrawSuccessProcessor;
	
	@Resource(name="coinDrawSuccessProcessor")
	private  EventProcessor<DrawRecharge> coinDrawSuccessProcessor;

	@Override
	public  String checkSubRefundReturn(SubRefundReturn refundReturn) {
		if (AppUtils.isBlank(refundReturn)) {
 			return "找不到该退款申请单";
		}
		
		if (refundReturn.getIsHandleSuccess() != 0) {
			return "退款处理失败,该退款单已被处理过";
		}
		
		if(refundReturn.getIsPresell() && refundReturn.getIsRefundDeposit() && AppUtils.isBlank(refundReturn.getPayTypeId())) {//校验仅退订金的情况下
			if (AppUtils.isBlank(refundReturn.getDepositPayTypeId())) {
				return "不符合退款操作,缺少支付方式";
			}
		}else {
			if (AppUtils.isBlank(refundReturn.getPayTypeId())) {
				return "不符合退款操作,缺少支付方式";
			}
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 第三方平台允许退款数
	 */
	@Override
	public Double allowReturnAmount(SubSettlement subSettlement){
		//根据settlementSn获得所有已经退款成功的订单项
		List<SubRefundReturn> sunRefundlist = subRefundReturnDao.getSubRefundsSuccessBySettleSn(subSettlement.getSubSettlementSn());
		/*Double cashAmount = subSettlement.getCashAmount();  //订单的总金额
		Double pdAmount = subSettlement.getPdAmount();  //使用预存款或者康云币支付的总金额
		Double amount = Arith.sub(cashAmount,pdAmount);  //使用微信的总金额*/
		/**混合支付情况下，CashAmount记录的就是第三方支付的金额*/
		Double amount = subSettlement.getCashAmount();
		if (!PayTypeEnum.FULL_PAY.value().equals(subSettlement.getPayTypeId()) && !PayTypeEnum.SIMULATE_PAY.value().equals(subSettlement.getPayTypeId())) {
			if(AppUtils.isBlank(sunRefundlist)){
				return amount;
			}
		}
		
		Double alreadyAmount = 0d;  //已经成功退款的总金额
		for(SubRefundReturn refund:sunRefundlist){
			Double successRefund = refund.getRefundAmount().doubleValue();  //已经成功退款的金额
			alreadyAmount = Arith.add(alreadyAmount,successRefund);  //累加订单中已经成功退款的总金额
		}
		
		if(alreadyAmount.equals(0)){  //如果退款总金额为0，那么还没成功退款的，直接返回使用微信的总金额
			return amount;
		}
		Double remainAmount = Arith.sub(amount, alreadyAmount); //微信剩下允许退款余额
		if(remainAmount <= 0){
			return 0d;
		}
		return remainAmount;
	}
	
	
	/* (non-Javadoc)
	 * @see com.legendshop.payment.refund.RefundHelper#plateformCoinReturn(com.legendshop.model.entity.orderreturn.SubRefundReturn)
	 */
	@Override
	public boolean plateformCoinReturn(SubRefundReturn refundReturn) {
		Long returnPayId = refundReturn.getId();
		DrawRecharge recharge = makeDrawRecharge(refundReturn);
		coinDrawSuccessProcessor.process(recharge);
		String result = recharge.getResult();
		if ("SUCCESS".equals(result)) {
			// 充值成功，更新sub_refund_return的平台退款金额 IsHandleSuccess HandleType
			subRefundReturnDao.updateSubPlatRefund(returnPayId, refundReturn.getRefundAmount(), 1, "金币退款");
			PaymentLog.log("退款成功, 金币退款金额: {}", refundReturn.getRefundAmount().doubleValue());
			return true;
		} else {
			RefundLog.error(" ############### 全款平台支付方式  = 金币退款失败  ##############   ");
			throw new BusinessException(" 金币退款失败！ ");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.payment.refund.RefundHelper#plateformPrePayReturn(com.legendshop.model.entity.orderreturn.SubRefundReturn)
	 */
	@Override
	public boolean plateformPrePayReturn(SubRefundReturn refundReturn){
		Long returnPayId = refundReturn.getId();
		DrawRecharge recharge= makeDrawRecharge(refundReturn);	
		weiXinDrawSuccessProcessor.process(recharge);
		String result=recharge.getResult();
		if(PreDepositErrorEnum.OK.value().equals(result)){
			//充值成功，更新sub_refund_return的平台退款金额   IsHandleSuccess HandleType
			subRefundReturnDao.updateSubPlatRefund(returnPayId,refundReturn.getRefundAmount(),1,"预存款退款");	
			PaymentLog.log("退款成功, 预存款退款金额: {}",refundReturn.getRefundAmount().doubleValue());
			return true;
		}else{
			RefundLog.error(" ############### 全款平台支付方式  = 预存款退款失败！ ##############   ");
			throw new BusinessException("预存款退款失败！");
		}
	}
	
	/**
	 * 预存款退款 充值
	 */
	@Override
	public boolean plateformPrePayReturn2(String userId, String userName, double amount, String sn, String message){
		String result = preDepositService.refund(userId,  userName, amount, sn, message);
		if(PreDepositErrorEnum.OK.value().equals(result)){
			PaymentLog.log("退款成功, 预存款退款金额: {}", amount);
			return true;
		}else{
			RefundLog.error(" ############### 全款平台支付方式  = 预存款退款失败！ ##############   ");
			throw new BusinessException("预存款退款失败！");
		}
	}
	
	
	/**
	 * 准备参数
	 * @param refundReturn
	 * @return
	 */
	private DrawRecharge makeDrawRecharge(SubRefundReturn refundReturn){
		DrawRecharge recharge=new DrawRecharge();
		recharge.setUserId(refundReturn.getUserId());
		recharge.setUserName(refundReturn.getUserName());
		recharge.setAmount(refundReturn.getRefundAmount().doubleValue());
		recharge.setMessage("订单退款,订单号【"+refundReturn.getSubNumber()+"】退款申请单号:"+refundReturn.getRefundSn());
		recharge.setSn(refundReturn.getRefundSn());
		RefundLog.info("订单退款,订单号【"+refundReturn.getSubNumber()+"】退款申请单号:"+refundReturn.getRefundSn());
		RefundLog.info(" 退款金额={} ",refundReturn.getRefundAmount().doubleValue());
		return recharge;
	}
	
	
	/**
	 * 
	 */
	@Override
	public void orderReturnOnlineSettle(Long refundId, String outRefundNo, Double thirdPartyRefund, Integer prePayType) {
		SubRefundReturn subRefundReturn = subRefundReturnDao.getSubRefundReturn(refundId);
		if(subRefundReturn.getIsHandleSuccess()==1){ //说明是出来成功过的
			return ;
		}
		Double refundAmount=subRefundReturn.getRefundAmount().doubleValue(); //退款金额
		if( refundAmount.compareTo(thirdPartyRefund) == 1){ //需要退的总金额大于第三方已退金额，那么还需要平台退款
			
			Double platformRefund=Arith.sub(refundAmount, thirdPartyRefund);
			
			DrawRecharge recharge=new DrawRecharge();
			recharge.setUserId(subRefundReturn.getUserId());
			recharge.setUserName(subRefundReturn.getUserName());
			recharge.setAmount(platformRefund);
			recharge.setMessage("订单退款,订单号【"+subRefundReturn.getSubNumber()+"】退款申请单号:"+subRefundReturn.getRefundSn());
			recharge.setSn(subRefundReturn.getRefundSn());
			RefundLog.info("订单退款,订单号【"+subRefundReturn.getSubNumber()+"】退款申请单号:"+subRefundReturn.getRefundSn());
			RefundLog.info(" 退款金额={} ",platformRefund);
			
			subRefundReturn.setPlatformRefund(new BigDecimal(platformRefund));
			if(PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)){ //预付款支付
				subRefundReturn.setHandleType(subRefundReturn.getPayTypeName() + "预付款混合退款");
				weiXinDrawSuccessProcessor.process(recharge);
				String result=recharge.getResult();
				if (!"SUCCESS".equals(result)) {
					subRefundReturn.setIsHandleSuccess(-1);
					RefundLog.error(" ####### 第三方支付退款回调业务处理失败  {},{} #######",result,subRefundReturn.getPayTypeName() + "预付款混合退款");
				}
        	}else if(PrePayTypeEnum.COIN.value().equals(prePayType)){
        		subRefundReturn.setHandleType(subRefundReturn.getPayTypeName() + "金币混合退款");
        		coinDrawSuccessProcessor.process(recharge);
				String result = recharge.getResult();
				if (!"SUCCESS".equals(result)) {
					RefundLog.error(" ####### 第三方支付退款回调业务处理失败  {},{} #######", result,subRefundReturn.getPayTypeName() + "金币混合退款");
					subRefundReturn.setIsHandleSuccess(-1);
				}
        	}
		}else{
			subRefundReturn.setHandleType(subRefundReturn.getPayTypeName());	
		}
		subRefundReturn.setOutRefundNo(outRefundNo);
		subRefundReturn.setThirdPartyRefund(new BigDecimal(thirdPartyRefund));
		subRefundReturn.setIsHandleSuccess(1);
		int result=subRefundReturnDao.updateSubRefundReturn(subRefundReturn);
		if(result==0){
			throw new BusinessException("退款失败！");
		}
	}
}
