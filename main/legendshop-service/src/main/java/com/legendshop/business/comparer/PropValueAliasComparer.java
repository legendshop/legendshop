package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.PropValueAlias;
import com.legendshop.util.AppUtils;

public class PropValueAliasComparer implements DataComparer<PropValueAlias,PropValueAlias>{

	@Override
	public boolean needUpdate(PropValueAlias dto, PropValueAlias dbObj, Object obj) {
		return true;
	}

	@Override
	public boolean isExist(PropValueAlias newValueAlias, PropValueAlias originValueAlias) {
		if(AppUtils.isNotBlank(newValueAlias) && AppUtils.isNotBlank(originValueAlias)){
			String alias = AppUtils.isNotBlank(newValueAlias.getAlias())? newValueAlias.getAlias():"";
			Long valueId = AppUtils.isNotBlank(newValueAlias.getValueId())? newValueAlias.getValueId():(long)0;
			if(alias.equals(originValueAlias.getAlias()) && valueId.equals(originValueAlias.getValueId())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public PropValueAlias copyProperties(PropValueAlias newValueAlias, Object obj) {
		Product prod = (Product)obj;
		PropValueAlias valueAlias = new PropValueAlias();
		valueAlias.setAlias(newValueAlias.getAlias());
		valueAlias.setValueId(newValueAlias.getValueId());
		valueAlias.setUserId(prod.getUserId());
		valueAlias.setProdId(prod.getProdId());
		return valueAlias;
	}

}
