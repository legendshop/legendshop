package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.entity.ProdArrivalInform;

/**
 * 到货通知
 * @author wei
 */
public interface ProductArrivalInformDao extends GenericDao<ProdArrivalInform, Long>{
	
	public abstract Long saveProdArriInfo(ProdArrivalInform prodArrivalInform);
	
	public abstract List<ProdArrivalInform> getUserBySkuIdAndWhId(long skuId);
	
	/**
	 * 获得用户已经添加到货通知的商品
	 * @param userId 用户id
	 * @param skuId 
	 * @param status 状态。查询尚未通知的
	 */
	public abstract ProdArrivalInform getAlreadySaveUser(String userId,long skuId,int status);

	public abstract PageSupport<ProdArrivalInformDto> getProdArrival(Long shopId, String productName, String curPageNO);

	public abstract PageSupport<ProdArrivalInformDto> getSelectArrival(String curPageNO, String skuId,
			 Long shopId);
}
