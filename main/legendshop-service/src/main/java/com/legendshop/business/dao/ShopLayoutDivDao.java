/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;

/**
 * 布局Div Dao.
 */

public interface ShopLayoutDivDao  {
     
	public abstract ShopLayoutDiv getShopLayoutDiv(Long id);
	
    public abstract int deleteShopLayoutDiv(ShopLayoutDiv shopLayoutDiv);
    
    public abstract int deleteShopLayoutDiv(Long layoutDivId);
	
	public abstract Long saveShopLayoutDiv(ShopLayoutDiv shopLayoutDiv);
	
	public abstract int updateShopLayoutDiv(ShopLayoutDiv shopLayoutDiv);
	
	public abstract List<ShopLayoutDiv> getShopLayoutDivs(Long layoutId, Long shopId);

	public abstract List<ShopLayoutDiv> getShopLayoutDivsByShop(Long shopId, Long decId);

	public abstract int deleteShopLayoutDivs(List<ShopLayoutDiv> delDivs);

	public abstract void saveShopLayoutDivs(List<ShopLayoutDiv> addDivs);

	public abstract void updateShopLayoutDivs(List<ShopLayoutDiv> updateDivs, Long shopId);

	public abstract int deleteShopLayoutDiv(Long shopId, Long layoutId);
	
 }
