/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.StoreDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.entity.store.Store;
import com.legendshop.util.AppUtils;

/**
 * 门店Dao接口实现类
 */
@Repository
public class StoreDaoImpl extends GenericDaoImpl<Store, Long> implements StoreDao {

	/**
	 * 获取门店 
	 */
	public Store getStore(Long id) {
		return getById(id);
	}

	/**
	 * 删除门店 
	 */
	public int deleteStore(Store store) {
		return delete(store);
	}

	/**
	 * 保存门店 
	 */
	public Long saveStore(Store store) {
		return save(store);
	}

	/**
	 * 更新门店 
	 */
	public int updateStore(Store store) {
		return update(store);
	}

	/**
	 * 获取门店 
	 */
	@Override
	public Store getStore(Long id, Long shopId) {
		String sql = ConfigCode.getInstance().getCode("store.getShopStoreByIdAndShopId");
		return get(sql, Store.class, id, shopId);
	}

	/**
	 * 查询门店 
	 */
	@Override
	public PageSupport<Store> query(String curPageNO, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(Store.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shop_id", shopId);
		String queryAllSQL = ConfigCode.getInstance().getCode("store.getShopStoresCount", map);
		String querySQL = ConfigCode.getInstance().getCode("store.getShopStores", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	/**
	 * 查询门店 
	 */
	@Override
	public PageSupport<StoreProductDto> query(String curPageNO, int pageSize, String storeId, Long shopId, Long poductId, String productname) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(StoreProductDto.class);
		hql.setPageSize(pageSize);
		hql.setCurPage(curPageNO);
		map.put("storeId", storeId);
		map.put("shopId", shopId);
		if (AppUtils.isNotBlank(poductId)&&!"null".equals(poductId)) {
			map.put("productId", poductId);
		}
		if (AppUtils.isNotBlank(productname)&&!"null".equals(productname)) {
			map.like("productName", productname);
		}
		hql.setQueryString(ConfigCode.getInstance().getCode("store.getStoreProds", map));
		hql.setAllCountString(ConfigCode.getInstance().getCode("store.getStoreProdsCount", map));
		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}
}
