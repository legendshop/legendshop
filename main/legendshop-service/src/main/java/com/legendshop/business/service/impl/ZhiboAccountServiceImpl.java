/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ZhiboAccountDao;
import com.legendshop.model.entity.ZhiboAccount;
import com.legendshop.spi.service.ZhiboAccountService;



/**
 * 直播账号服务实现类
 */
@Service("zhiboAccountService")
public class ZhiboAccountServiceImpl  implements ZhiboAccountService{
   
	@Autowired
    private ZhiboAccountDao zhiboAccountDao;

   	/**
	 *  根据Id获取
	 */
    public ZhiboAccount getZhiboAccountById(String id) {
        return zhiboAccountDao.getZhiboAccountById(id);
    }

   /**
	 *  删除
	 */ 
    public void deleteZhiboAccount(ZhiboAccount zhiboAccount) {
        zhiboAccountDao.deleteZhiboAccount(zhiboAccount);
    }

   /**
	 *  保存
	 */	    
    public String saveZhiboAccount(ZhiboAccount zhiboAccount) {
        return zhiboAccountDao.saveZhiboAccount(zhiboAccount);
    }

    public int insertZhiboAccount(ZhiboAccount zhiboAccount) {
        return zhiboAccountDao.insertZhiboAccount(zhiboAccount);
    }
    
   /**
	 *  更新
	 */	
    public void updateZhiboAccount(ZhiboAccount zhiboAccount) {
        zhiboAccountDao.updateZhiboAccount(zhiboAccount);
    }

    public int loginUpdate(ZhiboAccount zhiboAccount){
    	return zhiboAccountDao.loginUpdate(zhiboAccount);
    }
    
    public ZhiboAccount getAccountByToken(String token,String type){
    	return zhiboAccountDao.getAccountByToken(token,type);
    }

	@Override
	public ZhiboAccount getZhiboAccountByType(String userName, String type) {
		return zhiboAccountDao.getZhiboAccountByType(userName,type);
	}
}
