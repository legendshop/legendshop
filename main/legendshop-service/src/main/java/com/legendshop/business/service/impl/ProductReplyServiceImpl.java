package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProdUsefulDao;
import com.legendshop.business.dao.ProductReplyDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.ProductCommentReplyDto;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.spi.service.ProductReplyService;

/**
 * 商品评论回复服务
 */
@Service("productReplyService")
public class ProductReplyServiceImpl implements ProductReplyService{

	@Autowired
	private ProductReplyDao productReplyDao;
	
	@Autowired
	private ProdUsefulDao prodUsefulDao;

	@Override
	public List<ProductReply> getProductReply() {
		return productReplyDao.getProductReply();
	}

	@Override
	public ProductReply getProductReply(Long id) {
		return productReplyDao.getProductReply(id);
	}

	@Override
	public void deleteProductReply(ProductReply productReply) {
		productReplyDao.deleteProductReply(productReply);
	}

	@Override
	public Long saveProductReply(ProductReply productReply) {
		return productReplyDao.saveProductReply(productReply);
	}

	@Override
	public void updateProductReply(ProductReply productReply) {
		productReplyDao.updateProductReply(productReply);
	}

	@Override
	public Integer getUseful(String userId,Long prodComId) {
		return prodUsefulDao.getUseful(userId,prodComId);
	}

	@Override
	public PageSupport<ProductCommentReplyDto> queryProductReplyList(Long prodComId, String curPageNO) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductCommentReplyDto.class, 20, String.valueOf(curPageNO));
		QueryMap map = new QueryMap();
		
		map.put("prodCommId", prodComId);
		
		String querySQL = ConfigCode.getInstance().getCode("prod.queryCommentReplys", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("prod.queryCommentReplysCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		query.setParam(map.toArray());
		
		PageSupport<ProductCommentReplyDto> ps = productReplyDao.querySimplePage(query);
		return ps;
	}

	@Override
	public PageSupport<ProductReply> getProductReplyPage(String curPageNO) {
		return productReplyDao.getProductReplyPage(curPageNO);
	}
}
