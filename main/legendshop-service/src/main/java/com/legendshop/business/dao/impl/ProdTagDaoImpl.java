package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTagDao;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTag;


/**
 * 商品标签Dao
 */
@Repository
public class ProdTagDaoImpl extends GenericDaoImpl<ProdTag, Long> implements ProdTagDao{

	@Override
	public PageSupport<ProdTag> getProdTagManage(String curPageNO, ProdTag prodTag, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProdTag.class, curPageNO);
        cq.setPageSize(10);
        cq.eq("shopId", shopId);        
        cq.like("name", prodTag.getName(), MatchMode.ANYWHERE);
		return queryPage(cq);
	}

}
