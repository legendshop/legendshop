/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DiscoverArticleDao;
import com.legendshop.dao.criterion.Criterion;
import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.util.AppUtils;

/**
 * The Class DiscoverArticleDaoImpl. 发现文章表Dao实现类
 */
@Repository
public class DiscoverArticleDaoImpl extends GenericDaoImpl<DiscoverArticle, Long> implements DiscoverArticleDao{

	/**
	 * 根据Id获取发现文章表
	 */
	public DiscoverArticle getDiscoverArticle(Long id){
		return getById(id);
	}

	/**
	 *  删除发现文章表
	 *  @param discoverArticle 实体类
	 *  @return 删除结果
	 */	
    public int deleteDiscoverArticle(DiscoverArticle discoverArticle){
    	return delete(discoverArticle);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteDiscoverArticle(Long id){
		return deleteById(id);
	}

	/**
	 * 保存发现文章表
	 */
	public Long saveDiscoverArticle(DiscoverArticle discoverArticle){
		return save(discoverArticle);
	}

	/**
	 *  更新发现文章表
	 */		
	public int updateDiscoverArticle(DiscoverArticle discoverArticle){
		return update(discoverArticle);
	}

	/**
	 * 分页查询发现文章表列表
	 */
	public PageSupport<DiscoverArticle>queryDiscoverArticle(String curPageNO, Integer pageSize,String condition,String order){
		CriteriaQuery query = new CriteriaQuery(DiscoverArticle.class, curPageNO);
		query.setPageSize(pageSize);
		if ("synthesize".equals(order)) {
			query.addDescOrder("pageView");
			query.addDescOrder("publishTime");
			query.addDescOrder("thumbNum");
		}else if ("hot".equals(order)) {
			query.addDescOrder("pageView");
		}else if ("new".equals(order)) {
			query.addDescOrder("publishTime");
		}
		query.eq("status", true);
		if (AppUtils.isNotBlank(condition)) {
		Criterion cqs= query.or(Restrictions.like("name","%"+condition+"%"), Restrictions.like("writerName","%"+condition+"%"));
		query.add(cqs);
		}
		PageSupport<DiscoverArticle> ps = queryPage(query);
		return ps;
	}

	@Override
	public PageSupport<DiscoverArticle> queryDiscoverArticlePage(String curPageNO, DiscoverArticle discoverArticle,
			Integer pageSize) {
		CriteriaQuery query = new CriteriaQuery(DiscoverArticle.class, curPageNO);
		query.setPageSize(pageSize);
		if (AppUtils.isNotBlank(discoverArticle) && AppUtils.isNotBlank(discoverArticle.getName())) {
			query.like("name", discoverArticle.getName().trim(),MatchMode.ANYWHERE);
		}
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<DiscoverArticle> ps = queryPage(query);
		return ps;
	}

}
