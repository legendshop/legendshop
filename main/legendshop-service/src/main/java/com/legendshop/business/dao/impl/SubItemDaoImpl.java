/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.entity.ShopOrderBill;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.SubItem;
import com.legendshop.util.AppUtils;

/**
 *
 * 订单项服务
 *
 */
@Repository("subItemDao")
public class SubItemDaoImpl extends GenericDaoImpl<SubItem, Long> implements SubItemDao  {

	//获取分销的订单详情，用于分佣金结算
	private static String getDistSubItemListSql = "SELECT si.sub_item_id AS subItemId,si.sub_item_number as sub_item_number,si.sub_number AS subNumber,si.prod_id AS prodId,si.sku_id AS skuId,si.refund_id as refundId,si.refund_state as refundState,si.refund_amount as refundAmount,si.refund_type as refundType,si.refund_count as refundCount," +
			" si.dist_user_name AS distUserName,si.dist_commis_cash AS distCommisCash,si.dist_second_name AS distSecondName, "+
			" si.dist_second_commis AS distSecondCommis,si.dist_third_name AS distThirdName,si.dist_third_commis AS distThirdCommis FROM ls_sub_item si,ls_sub s WHERE si.sub_number =s.sub_number AND si.has_dist = 1  AND si.commis_settle_sts = 0 AND s.refund_state != 2 AND s.refund_state!=1  " +
			" AND s.status=? AND si.sub_item_id>=? AND si.sub_item_id<? AND s.finally_date< ? " ;

	//获取分销的订单详情，用于分佣金结算（过了退货时间才能结算）
	private static String getDistSubItemListAfterReturnSql = "SELECT si.sub_item_id AS subItemId,si.sub_item_number as sub_item_number,si.sub_number AS subNumber,si.prod_id AS prodId,si.sku_id AS skuId,si.refund_id as refundId,si.refund_state as refundState,si.refund_amount as refundAmount,si.refund_type as refundType,si.refund_count as refundCount,"+
			" si.dist_user_name AS distUserName,si.dist_commis_cash AS distCommisCash,si.dist_user_commis AS distUserCommis,si.dist_second_name AS distSecondName,si.dist_second_commis AS distSecondCommis,si.dist_third_name AS distThirdName,si.dist_third_commis AS distThirdCommis,c.return_valid_period AS returnValidPeriod "+
			" FROM ls_sub_item si,ls_sub s,ls_prod p,ls_category c WHERE si.sub_number =s.sub_number AND si.prod_id = p.prod_id AND c.id = p.category_id AND si.has_dist = 1  AND si.commis_settle_sts = 0 AND s.refund_state != 2 AND s.refund_state!=1 " +
			" AND s.status=? AND si.sub_item_id>=? AND si.sub_item_id<=? AND NOW() > DATE_ADD(s.finally_date, INTERVAL IF((c.return_valid_period IS NOT NULL AND c.return_valid_period > 0),c.return_valid_period,?) DAY) ";

	//获取第一个分销的订单号，尚未结算的，用于分佣金结算
	private static String getFirstDistSubItemId = "select min(si.sub_item_id) from ls_sub_item si, ls_sub s where si.sub_number =s.sub_number AND si.has_dist = 1  AND si.commis_settle_sts = 0 AND s.refund_state != 2 AND s.refund_state!=1 " +
			" and s.status=? AND s.finally_date< ? ";

	//获取最后一个分销的订单号，尚未结算的，用于分佣金结算
	private static String getLastDistSubItemId = "select max(si.sub_item_id) from ls_sub_item si, ls_sub s where si.sub_number =s.sub_number AND si.has_dist = 1  AND si.commis_settle_sts = 0 AND s.refund_state != 2 AND s.refund_state!=1 " +
			" and s.status=? AND s.finally_date< ? ";

	//查询当前期 所有的分销单，用于商家结算.    refund_state: 退换货状态  0:默认,1:在处理,2:处理完成
	private static String getShopDistSubItemListSql = "SELECT si.sub_item_id AS subItemId,si.sub_item_number as sub_item_number,si.sub_number AS subNumber,si.prod_id AS prodId,si.sku_id AS skuId,si.refund_id as refundId,si.refund_state as refundState,si.refund_amount as refundAmount,si.refund_type as refundType,si.refund_count as refundCount," +
			"si.dist_user_name AS distUserName,si.dist_user_commis AS distUserCommis,si.dist_commis_cash AS distCommisCash,si.dist_second_name AS distSecondName," +
			"si.dist_second_commis AS distSecondCommis,si.dist_third_name AS distThirdName,si.dist_third_commis AS distThirdCommis FROM ls_sub_item si,ls_sub s WHERE si.sub_number =s.sub_number AND s.is_bill = 0 AND si.has_dist = 1 AND s.refund_state!=2 AND s.refund_state!=1  " +
			" AND s.status=? AND s.shop_id=? AND s.finally_date<? " ;

	//查询当前已完成，未评论的订单项
	private static String getNoCommonSubItemListSql = "select si.* from ls_sub s left join ls_sub_item si on s.sub_number = si.sub_number where s.status = 4 and si.comm_sts = 0 and finally_date <= ?";

	// 更新当前订单项退款状态
	private static String updateRefundStateBySubItemIdSql = "UPDATE ls_sub_item SET refund_state = ? WHERE sub_item_id = ?";

	@Override
	public List<SubItem> getSubItem(String subNumber, String userId){
		return this.queryByProperties(new EntityCriterion().eq("subNumber", subNumber).eq("userId", userId));
	}

	@Override
	public SubItem getSubItem(Long id){
		return getById(id);
	}

	@Override
	public int deleteSubItem(SubItem subItem){
		return delete(subItem);
	}

	@Override
	public Long saveSubItem(SubItem subItem){
		return save(subItem);
	}

	@Override
	public int updateSubItem(SubItem subItem){
		return update(subItem);
	}

	@Override
	public int updateSubItem(List<SubItem> subItemList){
		return update(subItemList);
	}

	@Override
	public List<Long> saveSubItem(List<SubItem> subItems) {
		return this.save(subItems);
	}

	@Override
	public List<SubItem> getSubItem(String subNumber) {
		return this.queryByProperties(new EntityCriterion().eq("subNumber", subNumber));
	}

	@Override
	public void updateSnapshotIdById(Long snapshotId, Long subItemId) {
		this.update("update ls_sub_item set snapshot_id = ? where sub_item_id = ?", snapshotId,subItemId);
	}

	@Override
	public Long getFirstDistSubItemId(Date date) {
		return this.get(getFirstDistSubItemId, Long.class, OrderStatusEnum.SUCCESS.value(),date);
	}

	@Override
	public Long getLastDistSubItemId(Date date) {
		return this.get(getLastDistSubItemId, Long.class,OrderStatusEnum.SUCCESS.value(), date);
	}

	/**
	 * 获取分销的订单详情
	 */
	@Override
	public List<SubItem> getDistSubItemList(Long firstSubItemId, Long toSubItemId,Date date) {
		return this.query(getDistSubItemListSql, SubItem.class, OrderStatusEnum.SUCCESS.value(), firstSubItemId, toSubItemId, date);
	}


	/**
	 * 查询当前期 所有的分销单
	 */
	@Override
	public List<SubItem> getShopDistSubItemList(Long shopId, Date endDate) {
		return this.query(getShopDistSubItemListSql, SubItem.class,OrderStatusEnum.SUCCESS.value(), shopId,endDate);
	}

	@Override
	public void updateSubItemCommSts(Long subItemId) {
		this.update("update ls_sub_item set comm_sts=1 where sub_item_id=?", subItemId);
	}

	@Override
	public void updateSubItemCommSts(Long subItemId, String userId) {
		this.update("update ls_sub_item set comm_sts = 1 where sub_item_id = ? AND user_id = ?", subItemId, userId);
	}

	@Override
	public Integer getUnCommProdCount(String userId) {
		return this.get("select count(i.sub_item_id) from ls_sub_item i, ls_sub s where i.sub_number=s.sub_number and s.status=? and  i.comm_sts=0 and i.user_id=?", Integer.class, OrderStatusEnum.SUCCESS.value(),userId);
	}

	@Override
	public void updateRefundState(Long subItemId, Integer refundState) {
		this.update("UPDATE ls_sub_item SET refund_state = ? WHERE sub_item_id = ?", refundState,subItemId);
	}

	@Override
	public Long queryRefundItemCount(String subNumber, Integer refundState) {
		String sql = "SELECT COUNT(sub_item_id) FROM ls_sub_item WHERE sub_number = ? AND  refund_state = ? ";
		return this.getLongResult(sql, subNumber, refundState);
	}

	@Override
	public boolean existNoRefunditem(String subNumber) {
		String sql = "SELECT COUNT(sub_item_id) FROM ls_sub_item WHERE sub_number = ? AND  ( refund_state = 0 or refund_state=-1)";
		return this.getLongResult(sql, subNumber)>0;
	}

	@Override
	public void updateRefundInfoBySubId(String subNumber, Long refundId) {
		//退款金额原封不动的set到订单项的退款金额
		String sql = "UPDATE ls_sub_item SET refund_id = ?, refund_state = 1, refund_type = 1, refund_amount = actual_amount WHERE sub_number = ?";
		this.update(sql, refundId,subNumber);
	}

	@Override
	public void updateRefundStateBySubNumber(String subNumber, Integer refundState) {
		String sql = "UPDATE ls_sub_item SET refund_state = ? WHERE sub_number = ?";
		this.update(sql, refundState, subNumber);
	}


	private static String getSubItemBySubIdSql = "SELECT si.*,s.freight_amount as freightAmount FROM ls_sub_item si, ls_sub s WHERE si.sub_number =s.sub_number AND s.sub_id=? ";

	@Override
	public List<SubItem> getSubItemBySubId(Long subId) {
		return this.query(getSubItemBySubIdSql, SubItem.class,subId);
	}

	@Override
	public void updateCommisSettleSts(Long subItemId, int status) {
		this.update("update ls_sub_item set commis_settle_sts = ? where sub_item_id = ?", status,subItemId);
	}

	@Override
	public PageSupport<SubItem> querySubItems(String curPageNO, Long shopId, String distUserName, Integer status,
											  String subItemNum, Date fromDate, Date toDate) {
		SimpleSqlQuery query = new SimpleSqlQuery(SubItem.class,10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("shopId", shopId);
		if(AppUtils.isNotBlank(distUserName)){
			map.like("distUserName", distUserName.trim());
		}
		map.put("status", status);
		if(AppUtils.isNotBlank(subItemNum)){
			map.like("subItemNum", subItemNum.trim());
		}
		map.put("startDate", fromDate);
		map.put("endDate", toDate);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.getDistSubItemsCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.getDistSubItems", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<SubItem> querySubItems(String curPageNO, String userName, Integer status, String subItemNum, Date fromDate, Date toDate) {
		SimpleSqlQuery query = new SimpleSqlQuery(SubItem.class,10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("distUserName", userName);
		map.put("status", status);
		map.put("subItemNum", subItemNum);
		map.put("startDate", fromDate);
		map.put("endDate", toDate);
		String queryAllSQL = ConfigCode.getInstance().getCode("uc.order.getDistSubItemsCount", map);
		String querySQL = ConfigCode.getInstance().getCode("uc.order.getDistSubItems", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public List<SubItem> getDistSubAfterReturnItemList(Long firstSubItemId, Long toSubItemId, String autoProductReturn) {
		return this.query(getDistSubItemListAfterReturnSql, SubItem.class, OrderStatusEnum.SUCCESS.value(), firstSubItemId, toSubItemId, autoProductReturn);
	}

	@Override
	public List<SubItem> getDistSubItemBySubNumber(String subNumber) {
		return this.queryByProperties(new EntityCriterion().eq("subNumber", subNumber).eq("hasDist", 1));
	}

	@Override
	public List<SubItem> getNoCommonSubItemList(Date time){
		return this.query(getNoCommonSubItemListSql, SubItem.class, time);
	}

	@Override
	public List<Long> getProdIdByUser(String userId) {
		String sql="SELECT DISTINCT prod_id FROM ls_sub_item WHERE user_id=?";
		return query(sql,Long.class,userId);
	}

	/**
	 * 查询结算分销订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	@Override
	public PageSupport<SubItem> queryDistSubItemList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill) {

		SimpleSqlQuery query = new SimpleSqlQuery(SubItem.class,10, curPageNO);
		QueryMap map = new QueryMap();

		map.put("shopId", shopId);
		map.put("status", OrderStatusEnum.SUCCESS.value());
		map.put("billSn",shopOrderBill.getSn());
		if (AppUtils.isNotBlank(subNumber)) {
			map.put("subNumber", "%" + subNumber.trim() + "%");
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("shop.queryDistSubItemListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("shop.queryDistSubItemList", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public List<SubItem> getSubItemByReturnId(Long refundId) {
		String sql = "SELECT * FROM ls_sub_item WHERE refund_id=?";
		return query(sql,SubItem.class,refundId);
	}

	@Override
	public void updateRefundStateBySubItemId(Long subItemId, Integer refundState) {
		this.update(updateRefundStateBySubItemIdSql, refundState, subItemId);
	}
}
