/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ShopCompanyDetail;

/**
 *公司详细信息Dao
 */
public interface ShopCompanyDetailDao extends GenericDao<ShopCompanyDetail, Long> {
     
	public abstract ShopCompanyDetail getShopCompanyDetail(Long id);
	
	public abstract ShopCompanyDetail getShopCompanyDetailByShopId(Long shopId);
	
    public abstract int deleteShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);
	
	public abstract Long saveShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);
	
	public abstract int updateShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);
	
 }
