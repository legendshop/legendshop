/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawRecord;

/**
 * 活动抽奖记录Dao.
 */

public interface DrawRecordDao extends Dao<DrawRecord, Long> {
     
	public abstract DrawRecord getDrawRecord(Long id);
	
    public abstract int deleteDrawRecord(DrawRecord drawRecord);
	
	public abstract Long saveDrawRecord(DrawRecord drawRecord);
	
	public abstract int updateDrawRecord(DrawRecord drawRecord);
	
	public abstract int delDrawRecordByActId(Long actId);
	
	public abstract Long getDrawRecordCount(String userId,Long actId);
	 
	/**
	 * @Description: 根据用户、活动、返回当天抽奖记录数
	 * @date 2016-4-26 
	 */
	public abstract Long getDRCountByCurrentDay(String userId,Long actId);

	public abstract PageSupport<DrawRecord> queryDrawRecordPage(String curPageNO, DrawRecord drawRecord); 
 }
