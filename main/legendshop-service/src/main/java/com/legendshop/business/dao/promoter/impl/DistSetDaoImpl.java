/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.promoter.impl;

import com.legendshop.business.dao.promoter.DistSetDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.promoter.model.DistSet;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * 分销设置DaoImpl
 */
@Repository
public class DistSetDaoImpl extends GenericDaoImpl<DistSet, Long> implements DistSetDao {

	/**
	 * 获取分销设置
	 */
	public List<DistSet> getDistSet(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}

	/**
	 * 获取分销设置
	 */
	public DistSet getDistSet(Long id) {
		return getById(id);
	}

	/**
	 * 删除分销设置
	 */
	public int deleteDistSet(DistSet distSet) {
		return delete(distSet);
	}

	/**
	 * 保存分销设置
	 */
	public Long saveDistSet(DistSet distSet) {
		return save(distSet);
	}

	/**
	 * 更新分销设置
	 */
	public int updateDistSet(DistSet distSet) {
		return update(distSet);
	}

}
