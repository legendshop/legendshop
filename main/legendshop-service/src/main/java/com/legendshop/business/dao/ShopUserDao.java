/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopUser;

/**
 * 商城用户Dao.
 */
public interface ShopUserDao extends Dao<ShopUser, Long> {
     
    public abstract List<ShopUser> getShopUseByShopId(Long shopId);

	public abstract ShopUser getShopUser(Long id);
	
	//根据商城来找到唯一的员工
	public abstract ShopUser getShopUser(Long shopId,String name);
	
	//查询用户的权限
	public abstract List<String> queryPerm(Long shopUserId);
	
    public abstract int deleteShopUser(ShopUser shopUser);
	
	public abstract Long saveShopUser(ShopUser shopUser);
	
	public abstract int updateShopUser(ShopUser shopUser);
	
	public abstract ShopUser getShopUserByNameAndShopId(String name, Long shopId);

	public abstract List<ShopUser> getShopUseRoleId(Long shopRoleId);

	public abstract PageSupport<ShopUser> queryShopUser(String curPageNO, Long shopId);
	
 }
