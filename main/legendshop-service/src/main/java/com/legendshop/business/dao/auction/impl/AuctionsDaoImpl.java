/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.auction.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.legendshop.model.constant.ActivitySearchTypeEnum;
import com.legendshop.model.constant.GroupStatusEnum;
import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.auction.AuctionsDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.Auctions;
import com.legendshop.util.AppUtils;

/**
 * 拍卖活动Dao实现类.
 *
 */
@Repository
public class AuctionsDaoImpl extends GenericDaoImpl<Auctions, Long> implements AuctionsDao {

	@Override
	public List<Auctions> getAuctionsBystatus(Long firstId, Long lastId, Long status) {
		return queryByProperties(new EntityCriterion().ge("id", firstId).lt("lastId", lastId).eq("status", status));
	}

	@Override
	public Auctions getAuctions(Long id) {
		return getById(id);
	}

	@Override
	@Caching(evict = { @CacheEvict(value = "AuctionsDetailDto", key = "#auctions.id"), @CacheEvict(value = "AuctionsDto", key = "#auctions.id") })
	public int deleteAuctions(Auctions auctions) {
		return delete(auctions);
	}

	@Override
	public Long saveAuctions(Auctions auctions) {
		return save(auctions);
	}

	@Override
	@Caching(evict = { @CacheEvict(value = "AuctionsDetailDto", key = "#auctions.id"), @CacheEvict(value = "AuctionsDto", key = "#auctions.id") })
	public int updateAuctions(Auctions auctions) {
		return update(auctions);
	}

	/**
	 * 更改拍卖活动状态为完成
	 */
	@Override
	@Caching(evict = { @CacheEvict(value = "AuctionsDetailDto", key = "#id"), @CacheEvict(value = "AuctionsDto", key = "#id") })
	public int finishAuction(Long id) {
		return update("update ls_auctions set status=2 where id=? and status=1 ", id);
	}

	/**
	 * 查询所有在线并且不过期的活动
	 */
	@Override
	public List<Auctions> getonlineList() {
		return this.queryByProperties(new EntityCriterion().eq("status", AuctionsStatusEnum.ONLINE.value()).gt("endTime", new Date()));
	}

	@Override
	public Long getMinId() {
		return this.get("select min(id) from ls_auctions where status=" + AuctionsStatusEnum.FINISH.value(), Long.class);
	}

	@Override
	public Long getMaxId() {
		return this.get("select max(id) from ls_auctions where status=" + AuctionsStatusEnum.FINISH.value(), Long.class);
	}

	/**
	 * 排除预错活动
	 */
	@Override
	public boolean excludePresell(Long prodId, Long skuId) {
		return this.getLongResult("SELECT COUNT(id)  FROM ls_auctions WHERE prod_id=? AND sku_id=? AND STATUS <> 2", prodId, skuId) > 0;
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Auctions auctions) {
		SimpleSqlQuery query = new SimpleSqlQuery(Auctions.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		Date date = new Date();
		if(AppUtils.isNotBlank(auctions.getStatus())){
			if (-3 == auctions.getStatus()) {
				// 过期数据，未审核且已经过了活动开始时间
				map.put("status", -1);
				map.put("nowDate2", date);
			} else {
				map.put("status", auctions.getStatus());
				// 查询未审核
				if (-1 == auctions.getStatus()) {
					map.put("nowDate3", date);
				}
			}
		}
		if(AppUtils.isNotBlank(auctions.getShopId())) {
			map.put("shopId", auctions.getShopId());
		}
		if(AppUtils.isNotBlank(auctions.getShopName())) {
			map.like("shopName", auctions.getShopName().trim());
		}
		if(AppUtils.isNotBlank(auctions.getAuctionsTitle())) {
			map.like("auctionsTitle", auctions.getAuctionsTitle().trim());
		}
		if (AppUtils.isNotBlank(auctions.getEndTime())) {
			map.put("nowDate", date);// 用于查询过期
		}
		map.put("orderByAndDir", "ORDER BY T.id desc");
		String querySQL = ConfigCode.getInstance().getCode("aucitons.queryAution", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("aucitons.queryAuctionCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("orderByAndDir");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Long shopId, Auctions auctions) {
		SimpleSqlQuery query = new SimpleSqlQuery(Auctions.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		//根据searchType,组装不同的筛选条件
		String tab = auctions.getSearchType();
		if (AppUtils.isNotBlank(tab)) {

			Date currDate = new Date();
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			switch (Objects.requireNonNull(ActivitySearchTypeEnum.matchType(tab))) {
				case WAIT_AUDIT:
					map.put("status", AuctionsStatusEnum.VALIDATING.value());
					map.put("isStarted", "AND a.start_time > " + "'" + sd.format(currDate) + "'");
					break;
				case NOT_PASS:
					map.put("status", AuctionsStatusEnum.FAILED.value());
//					map.put("unFinished", "AND a.end_time > " + "'" + sd.format(currDate) + "'");
					break;
				case NOT_STARTED:
					map.put("status", AuctionsStatusEnum.ONLINE.value());
					map.put("unStarted", "AND a.start_time > " + "'" + sd.format(currDate) + "'");
					break;
				case ONLINE:
					map.put("status", AuctionsStatusEnum.ONLINE.value());
					map.put("isStarted", "AND a.start_time < " + "'" + sd.format(currDate) + "'");
					map.put("unFinished", "AND a.end_time > " + "'" + sd.format(currDate) + "'");
					break;
				case FINISHED:
					map.put("status", AuctionsStatusEnum.FINISH.value());
					break;
				case EXPIRED:
					map.put("status", AuctionsStatusEnum.OFFLINE.value());
//					map.put("unFinished", "AND a.end_time > " + "'" + sd.format(currDate) + "'");
					break;
				default:
			}
		}
		map.like("auctionsTitle", auctions.getAuctionsTitle());
		map.put("shopId", shopId);
		map.put("shopName", auctions.getShopName());
		map.put("orderByAndDir", "ORDER BY a.status ASC");
		String querySQL = ConfigCode.getInstance().getCode("aucitons.queryAuctionList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("aucitons.queryAuctionListCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);

		map.remove("orderByAndDir");
		map.remove("isStarted");
		map.remove("unStarted");
		map.remove("isFinished");
		map.remove("unFinished");

		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, ProductSearchParms parms) {
		SimpleSqlQuery query = new SimpleSqlQuery(Auctions.class, 12, curPageNO);
		QueryMap map = new QueryMap();
		Date date = new Date();
		map.put("status", 1);
		map.put("startDate", date);
		map.put("endDate", date);
		if (AppUtils.isBlank(parms.getOrders())) {
			map.put("orderByAndDir", "order by T.status asc");
		}
		String querySQL = ConfigCode.getInstance().getCode("aucitons.queryFrontAution", map);
		map.remove("orderByAndDir");
		String queryAllSQL = ConfigCode.getInstance().getCode("aucitons.queryFrontAuctionCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Auctions> queryPageAuctionList(String curPageNO, ProductSearchParms parms) {
		SimpleSqlQuery query = new SimpleSqlQuery(Auctions.class, 12, curPageNO);
		QueryMap map = new QueryMap();
		Date date = new Date();

		if ("0".equals(parms.getLoadType())) {//即将开拍

			map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
			map.put("skuStatus", ProductStatusEnum.PROD_ONLINE.value());
			map.put("status", 1);
			map.put("preDate", date);
		} else if ("2".equals(parms.getLoadType())) {//竞拍完成
			map.put("status", 2);
		} else {//正在拍卖
			map.put("prodStatus", ProductStatusEnum.PROD_ONLINE.value());
			map.put("skuStatus", ProductStatusEnum.PROD_ONLINE.value());
			map.put("status", 1);
			map.put("startDate", date);
			map.put("endDate", date);
		}

		if (AppUtils.isNotBlank(parms.getOrders())) {
			String[] order = StringUtils.split(parms.getOrders(), ",");
			if (order != null && order.length == 2) {
				map.put("orderByAndDir", "order by " + order[0] + " " + order[1]);
			}
		}
		String querySQL = ConfigCode.getInstance().getCode("aucitons.queryFrontAution", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("aucitons.queryFrontAuctionCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("orderByAndDir");
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public int updateAuctionStatus(Long auctionStatus,Long auctionId) {
		String sql="UPDATE ls_auctions SET status=? WHERE id=?";
		return update(sql,auctionStatus,auctionId);
	}

	@Override
	public boolean findIfJoinAuctions(Long productId) {
		String sql="SELECT COUNT(1) FROM ls_auctions  WHERE ls_auctions.status=1 AND ls_auctions.prod_id=?  AND ls_auctions.end_time>=? ";
		return get(sql, Integer.class, productId,new Date())>0;
	}

	@Override
	public Auctions isAuction(Long prodId, Long skuId) {
		String sql = "SELECT la.* FROM ls_auctions la WHERE la.prod_id = ? AND la.sku_id = ? AND la.status = ? AND start_time < ? AND end_time > ?";
		return get(sql, Auctions.class, prodId, skuId, AuctionsStatusEnum.ONLINE.value(), new Date(), new Date());
	}

	/**
	 * 更新拍卖活动处理状态
	 * @param id
	 * @param flagStatus
	 */
	@Override
	public void updateAuctionsFlagStatus(Long id, int flagStatus) {

		update("update ls_auctions set flag_status=?  where id=?",flagStatus,id);
	}

}
