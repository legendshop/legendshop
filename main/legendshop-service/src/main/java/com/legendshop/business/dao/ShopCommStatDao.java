/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;

/**
 *店铺评分统计Dao
 */
public interface ShopCommStatDao extends Dao<ShopCommStat, Long> {
     
	public abstract ShopCommStat getShopCommStat(Long id);
	
	public abstract ShopCommStat getShopCommStatByShopId(Long shopId);
	
    public abstract int deleteShopCommStat(ShopCommStat shopCommStat);
	
	public abstract Long saveShopCommStat(ShopCommStat shopCommStat);
	
	public abstract int updateShopCommStat(ShopCommStat shopCommStat);
	
	public abstract ShopCommStat getShopCommStatByShopIdForUpdate(Long shopId);
	
 }
