package com.legendshop.business.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ReportDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProdReportDto;
import com.legendshop.model.dto.ReportDto;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.model.report.PurchaseRankDto;
import com.legendshop.model.report.ReportRequest;
import com.legendshop.model.report.SaleAnalysisDto;
import com.legendshop.model.report.SaleAnalysisResponse;
import com.legendshop.model.report.SalerankDto;
import com.legendshop.model.report.SalesResponse;
import com.legendshop.spi.service.ReportService;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;
/**
 * 报表服务类
 *
 */
@Service("reportService")
public class ReportServiceImpl implements ReportService {
	
	@Autowired
	private ReportDao reportDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Override
	public SaleAnalysisResponse getSaleAnalysis(SaleAnalysisDto saleAnalysisDto) {
		return reportDao.getSaleAnalysis(saleAnalysisDto);
	}


	@Override
	public SalesResponse getSales(ReportRequest salesRequest) {
		return reportDao.getSales(salesRequest);
	}


	@Override
	public List<ReportDto> getOrderQuantity(ReportRequest reportRequest) {
		return reportDao.getOrderQuantity(reportRequest);
	}


	@Override
	public List<ReportDto> queryShopSales(Long shopId) {
		return reportDao.queryShopSales(shopId);
	}


	@Override
	public List<String> getXAxis() {
		Date now = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(now, Integer.parseInt(dashboardDate));
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");
		
		List<String> result = new ArrayList<String>();
		Calendar calendar = Calendar.getInstance();
		String dateFormat = "";
		while(startDate.compareTo(now)<=0){
			 calendar.setTime(startDate);
		     dateFormat = sdf.format(startDate);
		    
		     result.add(dateFormat);
		     
		     calendar.add(Calendar.DATE, 1);
		     startDate = calendar.getTime();
		}
		return result;
	}


	@Override
	public List<Integer> querySubCounts(Long shopId) {
		return reportDao.querySubCounts(shopId);
	}


	@Override
	public List<ProdReportDto> getProdTrend(Long shopId, Long prodId, Date startDate, Date endDate) {
		return reportDao.getProdTrend(shopId, prodId, startDate, endDate);
	}


	@Override
	public List<KeyValueEntity> getCityOrderQuantity(ReportRequest reportRequest) {
		return reportDao.getCityOrderQuantity(reportRequest);
	}

	@Override
	public List<KeyValueEntity> getCityOrderQuantitySpecial(ReportRequest reportRequest) {
		return reportDao.getCityOrderQuantitySpecial(reportRequest);
	}


	@Override
	public List<ReportDto> getPurchaseAnalysis(List<PriceRange> priceRangeList, ReportRequest reportRequest) {
		return reportDao.getPurchaseAnalysis(priceRangeList, reportRequest);
	}


	@Override
	public List<ReportDto> getPurchaseTime(ReportRequest reportRequest) {
		return reportDao.getPurchaseTime(reportRequest);
	}


	@Override
	public List<ReportDto> getTrafficStatistics(ReportRequest reportRequest) {
		return reportDao.getTrafficStatistics(reportRequest);
	}


	@Override
	public List<ReportDto> getOperatingReport(ReportRequest reportRequest) {
		return reportDao.getOperatingReport(reportRequest);
	}


	@Override
	public String getWeek(int year, int month) throws Exception {

		List<String> weekList = new ArrayList<String>();
		
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        String date = year+"-"+month;
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(dateFormat.parse(year+"-"+(month)));
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 1; i <= days; i++) {
        	String week = "";
        	
            DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
            Date date2 = dateFormat1.parse(date + "-" + i);
            calendar.clear();
            calendar.setTime(date2);
            int k = new Integer(calendar.get(Calendar.DAY_OF_WEEK));
            if (k == 1) {// 若当天是周日
                if (i - 6 <= 1) {
                    week = date + "-" + 1;
                } else {
                    week = date + "-" + (i - 6);
                }
                week +="~"+date + "-" + i;
                weekList.add(week);
            }
            if (k != 1 && i == days) {// 若是本月最后一天，且不是周日
                week = date + "-" + (i - k + 2) + "~" + date + "-" + i;
                weekList.add(week);
            }
        }
        
		return JSONUtil.getJson(weekList);
	}


	@Override
	public PageSupport<SalerankDto> querySaleRank(SalerankDto salerankDto, String curPageNO, Integer pageSize,DataSortResult result) {
		return reportDao.querySaleRank(salerankDto,curPageNO,pageSize,result);
	}


	/**
	 * 购买量查询
	 */
	@Override
	public PageSupport<PurchaseRankDto> queryPurchaseRankDto(String curPageNO, PurchaseRankDto purchaserankDto,
			Integer pageSize, DataSortResult result) {
		return reportDao.queryPurchaseRankDto(curPageNO,purchaserankDto,pageSize,result);
	}
}
