/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopNav;

/**
 *商家导航Dao
 */
public interface ShopNavDao extends GenericDao<ShopNav, Long> {

	public abstract ShopNav getShopNav(Long id);
	
    public abstract int deleteShopNav(ShopNav shopNav);
	
	public abstract Long saveShopNav(ShopNav shopNav);
	
	public abstract int updateShopNav(ShopNav shopNav);
	
	public abstract List<ShopNav> getLayoutNav(Long shopId);

	public abstract PageSupport<ShopNav> getShopNavPage(String curPageNO, Long shopId);
	
 }
