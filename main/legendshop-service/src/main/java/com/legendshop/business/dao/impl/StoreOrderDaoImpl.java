/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.StoreOrderDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.order.StoreOrderDto;
import com.legendshop.model.dto.order.StoreOrderItemDto;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.util.AppUtils;

/**
 * The Class DeliveryOrderDaoImpl.
 */
@Repository
public class StoreOrderDaoImpl extends GenericDaoImpl<StoreOrder, Long> implements StoreOrderDao {

	public StoreOrder getDeliveryOrder(Long id) {
		return getById(id);
	}

	public int deleteDeliveryOrder(StoreOrder deliveryOrder) {
		return delete(deliveryOrder);
	}

	public Long saveDeliveryOrder(StoreOrder deliveryOrder) {
		return save(deliveryOrder);
	}

	public int updateDeliveryOrder(StoreOrder deliveryOrder) {
		return update(deliveryOrder);
	}

	private final static String STORE_PRODUCT_STRING = "SELECT  ls.id AS basketId, lp.pic AS pic,lp.cash AS cash,lp.price AS price,lk.volume AS volume,lk.weight AS weight,lp.transport_id AS transportId,lp.is_support_cod AS isSupportCod,"
			+ "lp.support_dist AS supportDist,lp.dist_commis_rate AS distCommisRate, lp.stock_counting AS stockCounting,ls.prod_id AS prodId ,lp.model_id AS modelId,lp.shop_id AS shopId,lp.support_transport_free AS support_transport_free,lp.transport_type AS transport_type,"
			+ "lp.ems_trans_fee AS ems_trans_fee,lp.express_trans_fee AS express_trans_fee,lp.mail_trans_fee AS mail_trans_fee,ls.stock AS actualStocks FROM ls_store_prod  ls LEFT JOIN ls_prod lp ON ls.prod_id=lp.prod_id  WHERE lp.status=1 AND ls.prod_id=? AND ls.store_id=? ";

	private final static String STORE_SKU_STRING = "SELECT  ls.id AS basketId, lp.pic AS pic,lp.cash AS cash,lp.price AS price,lk.volume AS volume,lk.weight AS weight,lp.transport_id AS transportId,ls.sku_id AS skuId,lk.price AS skuPrice,lp.is_support_cod AS isSupportCod,"
			+ "lp.support_dist AS supportDist,lp.dist_commis_rate AS distCommisRate, lp.stock_counting AS stockCounting,ls.prod_id AS prodId , lp.model_id AS modelId,lp.shop_id AS shopId,lp.support_transport_free AS support_transport_free,lp.transport_type AS transport_type, lp.ems_trans_fee AS ems_trans_fee,lp.express_trans_fee AS express_trans_fee,lp.mail_trans_fee AS mail_trans_fee,ls.stock AS actualStocks"
			+ "FROM ls_store_sku  ls LEFT JOIN ls_prod lp ON ls.prod_id=lp.prod_id LEFT JOIN ls_sku lk ON ls.sku_id=lk.sku_id WHERE lk.status=1 AND ls.prod_id=? AND ls.sku_id=? AND ls.store_id=?  ";

	@Override
	public ShopCartItem findStoreOrder(long storeId, long prodId, long skuId) {
		ShopCartItem cartItem = null;
		if (skuId == 0) {
			cartItem = get(STORE_PRODUCT_STRING, new Object[] { prodId, storeId }, new ShopCartMapper());
		} else {
			cartItem = get(STORE_SKU_STRING, new Object[] { prodId, skuId, storeId }, new ShopCartMapper());
		}
		return cartItem;
	}

	private class ShopCartMapper implements RowMapper<ShopCartItem> {
		@Override
		public ShopCartItem mapRow(ResultSet rs, int rowNum) throws SQLException {
			ShopCartItem b = new ShopCartItem();
			b.setBasketId(rs.getLong("basketId") == 0 ? null : rs.getLong("basketId"));
			b.setPic(rs.getString("pic"));
			b.setProdName(rs.getString("prodName"));
			Double cash = rs.getDouble("cash");
			Double price = rs.getDouble("price");
			Long skuId = rs.getLong("skuId");
			if (AppUtils.isNotBlank(skuId) && skuId != 0) { // 说明是SKU商品
				b.setSkuId(skuId);
				cash = rs.getDouble("skuPrice");
			} else {
				b.setSkuId(null); // 不是SKU 商品
			}
			b.setPrice(price);
			// b.setCash(price);
			b.setPromotionPrice(price);
			b.setVolume(rs.getDouble("volume"));
			b.setWeight(rs.getDouble("weight"));
			b.setTransportId(rs.getLong("transportId") == 0 ? null : rs.getLong("transportId"));
			// b.setIsSupportCod(rs.getInt("isSupportCod"));
			b.setStockCounting(rs.getInt("stockCounting"));
			b.setProdId(rs.getLong("prodId") == 0 ? null : rs.getLong("prodId"));
			b.setBasketCount(rs.getInt("basketCount"));
			b.setShopId(rs.getLong("shopId") == 0 ? null : rs.getLong("shopId"));
			b.setEmsTransFee(rs.getFloat("ems_trans_fee"));
			b.setExpressTransFee(rs.getFloat("express_trans_fee"));
			b.setMailTransFee(rs.getFloat("mail_trans_fee"));
			b.setTransportFree(rs.getInt("support_transport_free"));
			b.setTransportType(rs.getInt("transport_type"));
			b.setDistUserName(rs.getString("distUserName"));
			b.setSupportDist(rs.getInt("supportDist"));
			b.setDistCommisRate(rs.getDouble("distCommisRate"));
			return b;
		}
	}

	@Override
	public int addHold(Long storeId, Long prodId, Long skuId, int count) {
		super.update("UPDATE ls_store_prod SET stock=stock-? WHERE store_id=? AND prod_id=?",
				new Object[] { count, storeId, prodId });
		return super.update(
				"UPDATE ls_store_sku SET stock=stock-? WHERE store_id=? AND prod_id=? AND sku_id=? AND stock-? >=0  ",
				new Object[] { count, storeId, prodId, skuId, count });
	}

	@Override
	public PageSupport<StoreOrderDto> getStorePageSupport(QueryMap queryMap, List<Object> args, Integer curPageNO,
			Integer pageSize) {
		String storecountSql = ConfigCode.getInstance()
				.getCode(this.getDialect().getDialectType() + ".getStoreOrdersCount", queryMap);
		String storeSQl = ConfigCode.getInstance().getCode(this.getDialect().getDialectType() + ".getStoreOrders",
				queryMap);
		Long total = getLongResult(storecountSql, args.toArray());
		List<StoreOrderDto> storeOrderDtoDtos = getStoreOrderDtoList(storeSQl, args);
		PageSupport<StoreOrderDto> pageSupport = initPageProvider(total, curPageNO, pageSize, storeOrderDtoDtos);
		return pageSupport;

	}

	public List<StoreOrderDto> getStoreOrderDtoList(String storeSQl, List<Object> args) {
		List<StoreOrderDto> dtos = super.query(storeSQl, args.toArray(), new MyStoreOrderDtoRowMapper());
		if (AppUtils.isBlank(dtos)) {
			return null;
		}
		Map<Long, StoreOrderDto> map = new HashMap<Long, StoreOrderDto>();
		for (StoreOrderDto dto : dtos) {
			if (map.containsKey(dto.getId())) {
				StoreOrderDto storeDto = map.get(dto.getId());
				List<StoreOrderItemDto> subOrderItemDtos = storeDto.getStoreOrderItemDtoList();
				subOrderItemDtos.add(dto.getStoreOrderItemDto());
			} else {
				StoreOrderItemDto storeDto = dto.getStoreOrderItemDto();
				dto.getStoreOrderItemDtoList().add(storeDto);
				map.put(dto.getId(), dto);
			}
		}

		List<StoreOrderDto> storeOrderDto = new ArrayList<StoreOrderDto>();
		for (Entry<Long, StoreOrderDto> entry : map.entrySet()) {
			StoreOrderDto storeOrderItemDto = entry.getValue();
			storeOrderDto.add(storeOrderItemDto);
		}

		Collections.sort(storeOrderDto, new Comparator<StoreOrderDto>() {
			@Override
			public int compare(StoreOrderDto o1, StoreOrderDto o2) {
				int flag = o1.getDlyoSatus().compareTo(o2.getDlyoSatus());
				return flag;
			}
		});
		map = null;

		return storeOrderDto;
	}

	class MyStoreOrderDtoRowMapper implements RowMapper<StoreOrderDto> {

		@Override
		public StoreOrderDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			StoreOrderDto dto = new StoreOrderDto();
			dto.setId(rs.getLong("id"));
			dto.setShopId(rs.getLong("shopId"));
			dto.setUserId(rs.getString("userId"));
			dto.setOrderSn(rs.getString("orderSn"));
			dto.setActualTotal(rs.getDouble("actualTotal"));
			dto.setReciverName(rs.getString("reciverName"));
			dto.setReciverMobile(rs.getString("reciverMobile"));
			dto.setDlyoPickupCode(rs.getString("dlyoPickupCode"));
			dto.setPayType(rs.getInt("payType"));
			dto.setDlyoSatus(rs.getInt("dlyoSatus"));
			dto.setDlyoAddtime(rs.getTimestamp("dlyoAddtime"));
			dto.setRefundType(rs.getInt("refundType"));
			StoreOrderItemDto itemDto = new StoreOrderItemDto();
			itemDto.setProdId(rs.getLong("prodId"));
			itemDto.setProdName(rs.getString("prodName"));
			itemDto.setPic(rs.getString("pic"));
			itemDto.setBasketCount(rs.getInt("basketCount"));
			itemDto.setAttribute(rs.getString("attribute"));
			itemDto.setCash(rs.getDouble("cash"));
			dto.setStoreOrderItemDto(itemDto);
			return dto;
		}
	}

	public PageSupport<StoreOrderDto> initPageProvider(long total, Integer curPageNO, Integer pageSize,
			List<StoreOrderDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<StoreOrderDto> ps = new PageSupport<StoreOrderDto>(resultList, pageProvider);
		return ps;
	}

	@Override
	public StoreOrder getDeliveryOrderBySubNumber(String subNumber) {
		return this.getByProperties(new EntityCriterion().eq("orderSn", subNumber));
	}

	@Override
	public int updateDeliveryOrderStatus(String subNumber, Integer status) {
		int result = this.update("UPDATE ls_store_order SET dlyo_satus=?  WHERE  order_sn=?",
				new Object[] { status, subNumber });
		if (result > 0) {
			result = this.update("UPDATE ls_sub SET finally_date=?,status=?  WHERE  sub_number=?",
					new Object[] { new Date(), OrderStatusEnum.SUCCESS.value(), subNumber });
		}
		return result;
	}

	@Override
	public long cancleOrder(String subNumber, String userId) {
		Long chainId = this.get("select store_id from ls_store_order where order_sn=? and user_id=? ", Long.class,
				new Object[] { subNumber, userId });
		if (AppUtils.isBlank(chainId) || chainId == 0)
			return 0l;

		/*
		 * update success
		 */
		int result = this.update(
				"DELETE FROM ls_store_order WHERE order_sn=? AND user_id=? and ((pay_type=2 AND dlyo_satus=0) OR (pay_type=3 AND dlyo_satus=1))",
				new Object[] { subNumber, userId });
		if (result == 0) {
			return 01;
		}
		return chainId;
	}
}
