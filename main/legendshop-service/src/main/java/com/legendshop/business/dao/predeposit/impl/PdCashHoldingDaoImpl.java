/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.predeposit.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.predeposit.PdCashHoldingDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PdCashHolding;

/**
 * 预存款拦截Dao实现类
 * 
 */
@Repository
public class PdCashHoldingDaoImpl extends GenericDaoImpl<PdCashHolding, Long> implements PdCashHoldingDao {

	/**
	 * 获取预存款拦截
	 */
	public PdCashHolding getPdCashHolding(Long id) {
		return getById(id);
	}

	/**
	 * 删除预存款拦截
	 */
	public int deletePdCashHolding(PdCashHolding pdCashHolding) {
		return delete(pdCashHolding);
	}

	/**
	 * 保存预存款拦截
	 */
	public Long savePdCashHolding(PdCashHolding pdCashHolding) {
		return save(pdCashHolding);
	}

	/**
	 * 更新预存款拦截
	 */
	public int updatePdCashHolding(PdCashHolding pdCashHolding) {
		return update(pdCashHolding);
	}

	/**
	 * 获取预存款拦截页面
	 */
	@Override
	public PageSupport<PdCashHolding> getPdCashHoldingPage(String curPageNO, String userId, String sn, String type) {
		CriteriaQuery cq = new CriteriaQuery(PdCashHolding.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("sn", sn);
		cq.eq("type", type);
		cq.eq("status", "0");
		cq.eq("userId", userId);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

}
