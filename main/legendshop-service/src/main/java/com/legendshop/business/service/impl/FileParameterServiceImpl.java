package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FileParameterDao;
import com.legendshop.model.entity.FileParameter;
import com.legendshop.spi.service.FileParameterService;

@Service("fileParameterService")
public class FileParameterServiceImpl implements FileParameterService {

    @Autowired
    private FileParameterDao fileParameterDao;

    /**
     * The log.
     */
    private static Logger log = LoggerFactory.getLogger(SystemParameterServiceImpl.class);


    @Override
    public FileParameter findByName(String fileName) {
        return fileParameterDao.findByName(fileName);
    }

    @Override
    public void deleteByName(String fileName) {
        fileParameterDao.deleteByName(fileName);
    }

    @Override
    public void save(FileParameter fileParameter) {
        fileParameterDao.save(fileParameter);
    }

    @Override
    public void update(FileParameter fileParameter) {
        fileParameterDao.update(fileParameter);
    }
}
