/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.GrassConcernDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassConcern;
import com.legendshop.spi.service.GrassConcernService;
import com.legendshop.util.AppUtils;

/**
 * The Class GrassConcernServiceImpl.
 *  种草社区关注表服务实现类
 */
@Service("grassConcernService")
public class GrassConcernServiceImpl implements GrassConcernService{

    /**
     *
     * 引用的种草社区关注表Dao接口
     */
	@Autowired
    private GrassConcernDao grassConcernDao;
   
   	/**
	 *  根据Id获取种草社区关注表
	 */
    public GrassConcern getGrassConcern(Long id) {
        return grassConcernDao.getGrassConcern(id);
    }
    
    /**
	 *  根据Id删除种草社区关注表
	 */
    public int deleteGrassConcern(Long id){
    	return grassConcernDao.deleteGrassConcern(id);
    }

   /**
	 *  删除种草社区关注表
	 */ 
    public int deleteGrassConcern(GrassConcern grassConcern) {
       return  grassConcernDao.deleteGrassConcern(grassConcern);
    }

   /**
	 *  保存种草社区关注表
	 */	    
    public Long saveGrassConcern(GrassConcern grassConcern) {
        if (!AppUtils.isBlank(grassConcern.getId())) {
            updateGrassConcern(grassConcern);
            return grassConcern.getId();
        }
        return grassConcernDao.saveGrassConcern(grassConcern);
    }

   /**
	 *  更新种草社区关注表
	 */	
    public void updateGrassConcern(GrassConcern grassConcern) {
        grassConcernDao.updateGrassConcern(grassConcern);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassConcern> queryGrassConcern(String curPageNO, Integer pageSize){
     	return grassConcernDao.queryGrassConcern(curPageNO, pageSize);
    }

    @Override
    public Integer rsConcern(String grassUserId, String userId) {
        return grassConcernDao.rsConcern(grassUserId,userId);
    }

    /**
	 *  设置Dao实现类
	 */	    
    public void setGrassConcernDao(GrassConcernDao grassConcernDao) {
        this.grassConcernDao = grassConcernDao;
    }

	@Override
	public void deleteGrassConcernByDiscoverId(String grassUserId, String uid) {
        grassConcernDao.deleteGrassConcernByDiscoverId(grassUserId, uid);
	}
    
}
