/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.HotsearchDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Hotsearch;
import com.legendshop.spi.service.HotsearchService;

/**
 * 
 * 热门搜索服务
 */
@Service("hotsearchService")
public class HotsearchServiceImpl implements HotsearchService {

	/** The log. */
	Logger log = LoggerFactory.getLogger(HotsearchServiceImpl.class);

	@Autowired
	private HotsearchDao hotsearchDao;

	@Override
	public Hotsearch getHotsearchById(Long id) {
		return hotsearchDao.getById(id);
	}

	@Override
	public void delete(Long id) {
		hotsearchDao.deleteHotsearchById(id);
	}


	@Override
	@CacheEvict(value="HotsearchList",allEntries=true)
	public Long saveHotsearch(Hotsearch hotsearch) {
		return (Long) hotsearchDao.save(hotsearch);
	}

	@Override
	public void updateHotsearch(Hotsearch hotsearch) {
		hotsearchDao.updateHotsearch(hotsearch);
	}

	@Override
	public List<Hotsearch> getHotsearch() {
		return hotsearchDao.getHotsearch();
	}

	@Override
	public PageSupport<Hotsearch> queryHotsearch(DataSortResult result, String curPageNO, Hotsearch hotsearch) {
		return hotsearchDao.queryHotsearch(result,curPageNO,hotsearch);
	}
}
