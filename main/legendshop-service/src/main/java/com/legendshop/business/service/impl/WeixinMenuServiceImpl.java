/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinMenuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.WxMenuTypeEnum;
import com.legendshop.model.entity.weixin.WeixinMenu;
import com.legendshop.spi.service.WeixinMenuService;
import com.legendshop.util.AppUtils;

/**
 * 微信菜单服务
 */
@Service("weixinMenuService")
public class WeixinMenuServiceImpl  implements WeixinMenuService{

	@Autowired
    private WeixinMenuDao weixinMenuDao;

    public WeixinMenu getWeixinMenu(Long id) {
        return weixinMenuDao.getWeixinMenu(id);
    }

    public void deleteWeixinMenu(WeixinMenu weixinMenu) {
        weixinMenuDao.deleteWeixinMenu(weixinMenu);
    }

    public Long saveWeixinMenu(WeixinMenu weixinMenu) {
        if (!AppUtils.isBlank(weixinMenu.getId())) {
        	WeixinMenu menu = weixinMenuDao.getWeixinMenu(weixinMenu.getId());
        	
        	if(WxMenuTypeEnum.CLICK.value().equals(weixinMenu.getMenuType())){
        		weixinMenu.setMenuUrl("");
        	}else if(WxMenuTypeEnum.VIEW.value().equals(weixinMenu.getMenuType())){
        		weixinMenu.setInfoType(null);
        		weixinMenu.setTemplateId(null);
        		weixinMenu.setContent(null);
        		weixinMenu.setMenuKey(null);
        	}
        	menu.setName(weixinMenu.getName());
        	menu.setMenuType(weixinMenu.getMenuType());
        	menu.setMenuUrl(weixinMenu.getMenuUrl());
        	menu.setSeq(weixinMenu.getSeq());
        	menu.setInfoType(weixinMenu.getInfoType());
        	menu.setTemplateId(weixinMenu.getTemplateId());
        	menu.setContent(weixinMenu.getContent());
        	menu.setMenuKey(weixinMenu.getMenuKey());
            updateWeixinMenu(menu);
            
            weixinMenu.setGrade(menu.getGrade());
            
            return weixinMenu.getId();
        }
        Long parentId = weixinMenu.getParentId();
        Integer level = 1;
        if(parentId != null){
        	WeixinMenu parent = weixinMenuDao.getWeixinMenu(parentId);
        	level = parent.getGrade() + 1;
        }
        weixinMenu.setGrade(level);
        weixinMenu.setCreatedate(new Date());
        return weixinMenuDao.saveWeixinMenu(weixinMenu);
    }

    public void updateWeixinMenu(WeixinMenu weixinMenu) {
        weixinMenuDao.updateWeixinMenu(weixinMenu);
    }

	@Override
	public WeixinMenu getMenu(Long id) {
		if(id != null && id > 0){
    		return weixinMenuDao.getWeixinMenu(id);
    	}else{
    		return null;
    	}
	}

	@Override
	public List<KeyValueEntity> getParentMenu(Long parentId) {
		List<KeyValueEntity> result = null;
		WeixinMenu parentMenu = weixinMenuDao.getWeixinMenu(parentId) ;
        int i = 0;
        if(parentMenu != null){
        	result = new ArrayList<KeyValueEntity>();
            result.add(new KeyValueEntity(parentMenu.getId().toString(),parentMenu.getName()));
            while(parentMenu.getParentId() != null && parentMenu.getParentId() > 0){
            	i ++;
            	if(i > 10){//防止数据错误引起系统无限循环
            		break;
            	}
            	parentMenu= weixinMenuDao.getWeixinMenu(parentMenu.getParentId()) ;
            	if(parentMenu != null){
            		result.add(new KeyValueEntity(parentMenu.getId().toString(),parentMenu.getName()));
            	}
            }
        }
        if(AppUtils.isNotBlank(result)){
            int length = result.size();
            if(length > 1){
            	//反转 reverse the data
            	List<KeyValueEntity> reveseResult =  new ArrayList<KeyValueEntity>();
            	for (int j = length -1; j >= 0; j--) {
            		reveseResult.add(result.get(j));
				}
            	return reveseResult;
            }else{//只有一个结果不需要反转
            	  return result;
            }
        }else{
        	return null;
        }

	}

	@Override
	public boolean hasSubWeixinMenu(Long menuId) {
		return weixinMenuDao.hasSubWeixinMenu(menuId);
	}

	@Override
	public boolean isExitMenuKey(String menukey) {
		return weixinMenuDao.isExitMenuKey(menukey);
	}

	@Override
	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, WeixinMenu weixinMenu, DataSortResult result) {
		return weixinMenuDao.getWeixinMenu(curPageNO,weixinMenu,result);
	}

	@Override
	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, Long parentId, int pageSize, DataSortResult result) {
		 return weixinMenuDao.getWeixinMenu(curPageNO,parentId,pageSize,result);
	}
}
