/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ThemeRelatedDao;
import com.legendshop.model.dto.ThemeRelatedDto;
import com.legendshop.model.entity.ThemeRelated;
import com.legendshop.spi.service.ThemeRelatedService;
import com.legendshop.util.AppUtils;

/**
 * 相关专题.
 */
@Service("themeRelatedService")
public class ThemeRelatedServiceImpl  implements ThemeRelatedService{
	
	@Autowired
    private ThemeRelatedDao themeRelatedDao;

	@Override
	public List<ThemeRelated> getThemeRelatedByTheme(Long themeId) {
        return themeRelatedDao.getThemeRelatedByTheme(themeId);
    }

    @Override
	public ThemeRelated getThemeRelated(Long id) {
        return themeRelatedDao.getThemeRelated(id);
    }

    @Override
	public void deleteThemeRelated(ThemeRelated themeRelated) {
        themeRelatedDao.deleteThemeRelated(themeRelated);
    }

    @Override
	public Long saveThemeRelated(ThemeRelated themeRelated) {
        if (!AppUtils.isBlank(themeRelated.getRelatedId())) {
            updateThemeRelated(themeRelated);
            return themeRelated.getRelatedId();
        }
        return themeRelatedDao.saveThemeRelated(themeRelated);
    }

    @Override
	public void updateThemeRelated(ThemeRelated themeRelated) {
        themeRelatedDao.updateThemeRelated(themeRelated);
    }

    @Override
    public List<ThemeRelatedDto> getThemeRelatedDtoByTheme(Long themeId) {
        return themeRelatedDao.getThemeRelatedDtoByTheme(themeId);
    }

}
