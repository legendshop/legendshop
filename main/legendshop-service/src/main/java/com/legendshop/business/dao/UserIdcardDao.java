/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.overseas.UserIdcard;

/**
 *用户实名认证Dao
 */
public interface UserIdcardDao extends Dao<UserIdcard, Long> {
     
    public abstract List<UserIdcard> getUserIdcard(String userId);

	public abstract UserIdcard getUserIdcard(Long id);
	
    public abstract int deleteUserIdcard(UserIdcard userIdcard);
	
	public abstract Long saveUserIdcard(UserIdcard userIdcard);
	
	public abstract int updateUserIdcard(UserIdcard userIdcard);
	
	public abstract UserIdcard getDefaultUserIdcard(String userId);
	
 }
