/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinNewsitemDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.weixin.WeixinNewsItemsDto;
import com.legendshop.model.entity.weixin.WeixinNewsitem;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;
import com.legendshop.spi.service.WeixinNewsitemService;
import com.legendshop.spi.service.WeixinNewstemplateService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 微信新闻项目服务
 */
@Service("weixinNewsitemService")
public class WeixinNewsitemServiceImpl  implements WeixinNewsitemService{

	@Autowired
    private WeixinNewsitemDao weixinNewsitemDao;
    
	@Autowired
    private WeixinNewstemplateService weixinNewstemplateService;

    public WeixinNewsitem getWeixinNewsitem(Long id) {
        return weixinNewsitemDao.getWeixinNewsitem(id);
    }

    public void deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem) {
        weixinNewsitemDao.deleteWeixinNewsitem(weixinNewsitem);
    }

    public Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem) {
        if (!AppUtils.isBlank(weixinNewsitem.getId())) {
            updateWeixinNewsitem(weixinNewsitem);
            return weixinNewsitem.getId();
        }
        return weixinNewsitemDao.saveWeixinNewsitem(weixinNewsitem);
    }

    public void updateWeixinNewsitem(WeixinNewsitem weixinNewsitem) {
        weixinNewsitemDao.updateWeixinNewsitem(weixinNewsitem);
    }

	@Override
	public List<WeixinNewsitem> getNewsItemsByTempId(Long tempId) {
		return weixinNewsitemDao.getNewsItemsByTempId(tempId);
	}

	@Override
	public void saveWeixinNewsitemList(List<WeixinNewsitem> addItems) {
		weixinNewsitemDao.saveWeixinNewsitemList(addItems);
	}

	@Override
	public void updateWeixinNewsitemList(List<WeixinNewsitem> updateItems) {
		weixinNewsitemDao.updateWeixinNewsitemList(updateItems);
	}

	@Override
	public void deleteWeixinNewsitemByIds(List<Long> ids) {
		weixinNewsitemDao.deleteWeixinNewsitemByIds(ids);
	}

	@Override
	public PageSupport<WeixinNewsitem> getWeixinNewsitemPage(String curPageNO) {
		return weixinNewsitemDao.getWeixinNewsitemPage(curPageNO);
	}

	@Override
	public void saveWeixinNewsitemList(WeixinNewsItemsDto newsItemsDto,String userId) {
		List<WeixinNewsitem> weixinNewsItems = JSONUtil.getArray(newsItemsDto.getNewsItemsJson(), WeixinNewsitem.class);
		
		//保存模板
		Long newsTemplateid = weixinNewsItems.get(0).getNewsTemplateid();
		WeixinNewstemplate weixinNewstemplate;
		if(newsTemplateid!=null){
			weixinNewstemplate = weixinNewstemplateService.getWeixinNewstemplate(newsTemplateid);
		}else{
			weixinNewstemplate = new WeixinNewstemplate();
			weixinNewstemplate.setCreateUserId(userId);
			weixinNewstemplate.setCreateDate(new Date());
		}
		weixinNewstemplate.setTemplatename(weixinNewsItems.get(0).getTitle());
		newsTemplateid = weixinNewstemplateService.saveWeixinNewstemplate(weixinNewstemplate);
		
		//对 新增 和 更新的 进行分组
		List<WeixinNewsitem> updateItems = new ArrayList<WeixinNewsitem>();
		List<WeixinNewsitem> addItems = new ArrayList<WeixinNewsitem>();
		for (int i=0;i<weixinNewsItems.size();i++) {
			WeixinNewsitem item = weixinNewsItems.get(i);
			item.setOrders(Long.valueOf(i+1));
			if(item.getId()==null){
				item.setNewsTemplateid(newsTemplateid);
				item.setCreateUserId(userId);
				item.setCreateDate(new Date());
				addItems.add(item);
			}else{
				updateItems.add(item);
			}
		}
		//保存图文消息
		weixinNewsitemDao.saveWeixinNewsitemList(addItems);
		
		//更新图文消息
		updateWeixinNewsitemList(updateItems);
		
		//删除图文消息
		String delIds = newsItemsDto.getDelIds();
		if(delIds!=""){
			String[] ids = delIds.split(",");
			List<Long> idList = new ArrayList<Long>();
			for (String idStr : ids) {
				idList.add(Long.valueOf(idStr));
			}
			deleteWeixinNewsitemByIds(idList);
		}
	}
}
