/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ActiveBanner;

/**
 * The Class GroupBannerDao.
 * Dao接口
 */
public interface ActiveBannerDao extends Dao<ActiveBanner, Long> {


	/**
	 *  根据Id获取
	 */
	public abstract ActiveBanner getBanner(Long id);

	/**
	 *  删除
	 */
	public abstract int deleteBanner(ActiveBanner banner);

	/**
	 *  保存
	 */	
	public abstract Long saveBanner(ActiveBanner banner);

	/**
	 *  更新
	 */		
	public abstract int updateBanner(ActiveBanner banner);

	public abstract List<ActiveBanner> getBannerList(String type);


}
