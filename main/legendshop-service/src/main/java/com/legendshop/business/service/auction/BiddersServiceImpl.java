/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.auction;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SkuDao;
import com.legendshop.business.dao.auction.BiddersDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.processor.auction.scheduler.SchedulerConstant;
import com.legendshop.processor.auction.scheduler.SchedulerMamager;
import com.legendshop.spi.service.AuctionsService;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.util.AppUtils;

/**
 * 竞拍服务实现类.
 */
@Service("biddersService")
public class BiddersServiceImpl implements BiddersService {

	@Autowired
	private BiddersDao biddersDao;

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private BiddersWinService biddersWinService;

	@Autowired
	private SkuDao skuDao;

	public Bidders getBidders(Long id) {
		return biddersDao.getBidders(id);
	}

	public void deleteBidders(Bidders bidders) {
		biddersDao.deleteBidders(bidders);
	}

	public Long saveBidders(Bidders bidders) {
		if (!AppUtils.isBlank(bidders.getId())) {
			updateBidders(bidders);
			return bidders.getId();
		}
		return biddersDao.saveBidders(bidders);
	}

	public void updateBidders(Bidders bidders) {
		biddersDao.updateBidders(bidders);
	}

	@Override
	public List<Bidders> getBiddersByAuctionId(Long auctionsId, int offset, int limit) {
		return biddersDao.getBiddersByAuctionId(auctionsId, offset, limit);
	}

	@Override
	public long findBidsCount(Long paimaiId) {
		return biddersDao.findBidsCount(paimaiId);
	}

	@Override
	@CacheEvict(value = "AuctionsDto", key = "#bid.aId")
	public Long bidAuctions(Bidders bid) {
		// 设置当前价
		biddersDao.update("update ls_auctions set cur_price =? where id=? ", new Object[] { bid.getPrice(), bid.getAId() });
		return biddersDao.saveBidders(bid);
	}

	@Override
	public List<Bidders> getBidders(Long aId, Long prodId, Long skuId) {
		return biddersDao.getBidders(aId, prodId, skuId);
	}

	@Override
	@CachePut(value = "DisposeBidsLogo", key = "#paimaiId")
	public int disposeBids(Long paimaiId, int status) {
		return status;
	}

	@Override
	@Cacheable(value = "DisposeBidsLogo", key = "#paimaiId")
	public int getDisposeBids(Long paimaiId) {
		return 0;
	}

	@Override
	@CacheEvict(value = "DisposeBidsLogo", key = "#paimaiId")
	public void clearDisposeBids(Long paimaiId) {

	}

	@Override
	@CachePut(value = "CrowdWatchAuctions", key = "#paimaiId")
	public Integer crowdWatchAuctions(Long paimaiId, int count) {
		return count;
	}

	@Override
	@Cacheable(value = "CrowdWatchAuctions", key = "#paimaiId")
	public Integer getCrowdWatchAuctions(Long paimaiId) {
		return 0;
	}

	@Override
	@CachePut(value = "CrowdWatchAuctions", key = "#paimaiId")
	public void clearCrowdWatchAuctions(Long paimaiId) {

	}

	@Override
	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long id) {
		return biddersDao.queryBiddersListPage(curPageNO, id);
	}

	@Override
	public PageSupport<Bidders> queryBiddListPage(String curPageNO, Long id) {
		return biddersDao.queryBiddListPage(curPageNO, id);
	}

	@Override
	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long aId, Long prodId, Long skuId) {
		return biddersDao.queryBiddersListPage(curPageNO, aId, prodId, skuId);
	}

	@Override
	public PageSupport<Bidders> getBiddersPage(String curPageNO, Bidders bidders) {
		return biddersDao.getBiddersPage(curPageNO, bidders);
	}

	/** 出价，并更新出价记录 */
	@Override
	public String bidAuctions(Auctions auction, Bidders bid, Long id, Long prodId, Long skuId) {
		String result = null;
		Long bidId = bidAuctions(bid);
		// 清空缓存
		biddersWinService.cleanAuctions(id);
		if (auction.getIsCeiling().equals(1L) && auction.getFixedPrice().compareTo(bid.getPrice()) == 0) {
			auctionsService.updateAuctionStatus(AuctionsStatusEnum.FINISH.value(), auction.getId());
			// 添加中标记录
			BiddersWin biddersWin = new BiddersWin();
			biddersWin.setAId(id);
			biddersWin.setBidId(bid.getId());
			biddersWin.setProdId(prodId);
			biddersWin.setSkuId(skuId);
			biddersWin.setUserId(bid.getUserId());
			biddersWin.setBitTime(new Date());
			biddersWin.setPrice(bid.getPrice());
			biddersWin.setStatus(BiddersWinStatusEnum.NOORDER.value());
			biddersWinService.saveBiddersWin(biddersWin);

			String jobName = SchedulerConstant.AUCTIONS_SCHEDULER_ + auction.getId();
			// 一口价成功，移除定时任务
			SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
			// 重置sku营销状态
			skuDao.updateSkuTypeById(skuId, SkuActiveTypeEnum.PRODUCT.value(), SkuActiveTypeEnum.AUCTION.value());

			auctionsService.updateAuctionsBidNumber(auction.getId()); // 更新出价记录
			auctionsService.updateAuctionsEnrolNumber(auction.getId(),auction.getEnrolNumber()); // 更新参加人数

			return "201";
		}
		if (bidId > 0) {
			auctionsService.updateAuctionsBidNumber(auction.getId()); // 更新出价记录
			auctionsService.updateAuctionsEnrolNumber(auction.getId(),auction.getEnrolNumber()); // 更新参加人数
			return "200";
		}
		return result;
	}
}
