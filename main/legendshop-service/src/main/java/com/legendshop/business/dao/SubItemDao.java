/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.SubItem;

import java.util.Date;
import java.util.List;

/**
 * 订单项Dao.
 */
public interface SubItemDao extends Dao<SubItem, Long> {

	/**
	 * Gets the sub item.
	 *
	 * @param subNumber the sub number
	 * @param userId the user id
	 * @return the sub item
	 */
	public abstract List<SubItem> getSubItem(String subNumber,String userId);

	/**
	 * Gets the sub item.
	 *
	 * @param id the id
	 * @return the sub item
	 */
	public abstract SubItem getSubItem(Long id);

	/**
	 * Delete sub item.
	 *
	 * @param subItem the sub item
	 * @return the int
	 */
	public abstract int deleteSubItem(SubItem subItem);

	/**
	 * Save sub item.
	 *
	 * @param subItem the sub item
	 * @return the long
	 */
	public abstract Long saveSubItem(SubItem subItem);

	/**
	 * Update sub item.
	 *
	 * @param subItem the sub item
	 * @return the int
	 */
	public abstract int updateSubItem(SubItem subItem);

	int updateSubItem(List<SubItem> subItemList);

	/**
	 * Save sub item.
	 *
	 * @param subItems the sub items
	 * @return the list< long>
	 */
	public abstract List<Long> saveSubItem(List<SubItem> subItems);

	/**
	 * Gets the sub item.
	 *
	 * @param subNumber the sub number
	 * @return the sub item
	 */
	public abstract List<SubItem> getSubItem(String subNumber);

	/**
	 * Update snapshot id by id.
	 *
	 * @param snapshotId the snapshot id
	 * @param subItemId the sub item id
	 */
	public abstract void updateSnapshotIdById(Long snapshotId, Long subItemId);

	/**
	 * 获取上个月 分销 订单项的最小的ID.
	 *
	 * @param date the date
	 * @return the first dist sub item id
	 */
	public Long getFirstDistSubItemId(Date date);

	/**
	 * 获取上个月 分销 订单项的最大的ID.
	 *
	 * @param date the date
	 * @return the last dist sub item id
	 */
	public Long getLastDistSubItemId(Date date);

	/**
	 * 查询当前所有的分销订单---结算统计.
	 *
	 * @param shopId the shop id
	 * @param endDate the end date
	 * @return the shop dist sub item list
	 */
	public abstract List<SubItem> getShopDistSubItemList(Long shopId, Date endDate);

	/**
	 * 获取 分销订单项 列表.
	 *
	 * @param firstSubItemId  开始id
	 * @param toSubItemId  结束id
	 * @param date the date
	 * @return the dist sub item list
	 */
	public List<SubItem> getDistSubItemList(Long firstSubItemId, Long toSubItemId,Date date);

	/**
	 * 获取 分销订单项 列表.
	 * @param firstSubItemId
	 * @param toSubItemId
	 * @param autoProductReturn
	 * @return
	 */
	public List<SubItem> getDistSubAfterReturnItemList(Long firstSubItemId, Long toSubItemId, String autoProductReturn);

	/**
	 * Update sub item comm sts.
	 *
	 * @param subItemId the sub item id
	 */
	public abstract void updateSubItemCommSts(Long subItemId);

	/**
	 * Update sub item comm sts.
	 *
	 * @param subItemId the sub item id
	 * @param userId the user id
	 */
	public abstract void updateSubItemCommSts(Long subItemId, String userId);

	/**
	 * Gets the un comm prod count.
	 *
	 * @param userId the user id
	 * @return the un comm prod count
	 */
	public abstract Integer getUnCommProdCount(String userId);

	/**
	 * 更新订单项的退款,退货状态.
	 *
	 * @param subItemId the sub item id
	 * @param refundState the refund state
	 */
	public abstract void updateRefundState(Long subItemId, Integer refundState);

	/**
	 * 根据订单编号和退款状态查询订单的订单项数量.
	 *
	 * @param subNumber 订单编号
	 * @param refundState 退款状态
	 * @return the long
	 */
	public abstract Long queryRefundItemCount(String subNumber,Integer refundState);

	/**
	 * 检测该订单 没有发起过退款或者发起过不同意的 订单项.
	 *
	 * @param subNumber the sub number
	 * @return true, if exist no refunditem
	 */
	public abstract boolean existNoRefunditem(String subNumber);

	/**
	 * 根据订单流水号更新 订单下的所有订单项的退款的退款信息
	 * </br>用于订单退款申请时 , 更新订单项的信息,包括退款id,退款类型为"仅退款",退款金额等于订单项金额,退款状态为处理中.
	 *
	 * @param subNumber 订单流水号
	 * @param refundId 退款ID
	 */
	public abstract void updateRefundInfoBySubId(String subNumber, Long refundId);

	/**
	 * 根据订单流水号更新订单项的退款状态.
	 *
	 * @param subNumber the sub number
	 * @param value the value
	 */
	public abstract void updateRefundStateBySubNumber(String subNumber, Integer value);

	/**
	 * Gets the sub item by sub id.
	 *
	 * @param subId the sub id
	 * @return the sub item by sub id
	 */
	public abstract List<SubItem> getSubItemBySubId(Long subId);

	/**
	 * 更新订单项的分用结算状态.
	 *
	 * @param subItemId the sub item id
	 * @param status the status
	 */
	public abstract void updateCommisSettleSts(Long subItemId, int status);

	/**
	 * Query sub items.
	 *
	 * @param curPageNO the cur page no
	 * @param shopId the shop id
	 * @param distUserName the dist user name
	 * @param status the status
	 * @param subItemNum the sub item num
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the page support< sub item>
	 */
	public abstract PageSupport<SubItem> querySubItems(String curPageNO, Long shopId, String distUserName,
													   Integer status, String subItemNum, Date fromDate, Date toDate);

	/**
	 * Query sub items.
	 *
	 * @param curPageNO the cur page no
	 * @param userName the user name
	 * @param status the status
	 * @param subItemNum the sub item num
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the page support< sub item>
	 */
	public abstract PageSupport<SubItem> querySubItems(String curPageNO, String userName, Integer status,
													   String subItemNum, Date fromDate, Date toDate);

	/**
	 * Query sub items.
	 * @param subNumber
	 * @return
	 */
	public abstract List<SubItem> getDistSubItemBySubNumber(String subNumber);

	/**
	 * 查询已完成，未评论，且满足时间要求的订单
	 * */
	List<SubItem> getNoCommonSubItemList(Date time);

	/**
	 * 根据用户id查询商品id
	 * @param userId
	 * @return
	 */
	List<Long> getProdIdByUser(String userId);

	/**
	 * 查询结算分销订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<SubItem> queryDistSubItemList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill);

	List<SubItem> getSubItemByReturnId(Long refundId);

	/**
	 * 根据订单项id更新订单项状态
	 * @param subItemId
	 * @param refundState
	 */
	void updateRefundStateBySubItemId(Long subItemId, Integer refundState);
}
