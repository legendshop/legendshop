/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

/**
 * 用户名称转换
 */
public interface UserLoginDao {
	
	/**
	 * 根据用户输入，把可能的邮件或者手机号码找到对应的用户名称.
	 *
	 * @param name the name
	 * @return the string
	 */
	public abstract String convertUserLoginName(String name);
	

	/**
	 * 根据商家输入，把可能的邮件或者手机号码找到对应的用户名称.
	 *
	 * @param name the name
	 * @return the string
	 */
	public abstract String convertShopUserLoginName(String name);
}
