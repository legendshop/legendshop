/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.SubSettlementItem;

/**
 * 订单结算票据中心
 *
 */
@Repository
public class SubSettlementItemDaoImpl extends GenericDaoImpl<SubSettlementItem, Long> implements SubSettlementItemDao  {
	    
	@Override
	public List<SubSettlementItem> getSubSettlementItems(String subSettlementSn) {
		return queryByProperties(new EntityCriterion().eq("subSettlementSn", subSettlementSn));
	}

	/**
	 * 批量保存订单支付票据
	 */
	@Override
	public void saveSubSettlementItem(List<SubSettlementItem> items) {
		save(items);
		
	}

	
 }
