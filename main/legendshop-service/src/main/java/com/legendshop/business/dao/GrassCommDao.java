/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.GrassComm;

/**
 * The Class GrassCommDao. 种草文章评论表Dao接口
 */
public interface GrassCommDao extends Dao<GrassComm,Long>{

	/**
	 * 根据Id获取种草文章评论表
	 */
	public abstract GrassComm getGrassComm(Long id);

	/**
	 *  根据Id删除种草文章评论表
	 */
    public abstract int deleteGrassComm(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassComm(GrassComm grassComm);

	/**
	 * 保存种草文章评论表
	 */
	public abstract Long saveGrassComm(GrassComm grassComm);

	/**
	 *  更新种草文章评论表
	 */		
	public abstract int updateGrassComm(GrassComm grassComm);

	/**
	 * 分页查询种草文章评论表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassComm> queryGrassComm(String curPageNO, Integer pageSize);
	
	public abstract List<GrassComm> getGrassCommByGrassId(Long grassId);

	/**
	 * 根据种草文章Id分页查询文章评论表
	 * @param currPage
	 * @param pageSize
	 * @param grassId 种草文章id
	 * @return
	 */
	public abstract PageSupport<GrassComm> queryGrassCommByGrassId(String currPage, int pageSize, Long grassId);
}
