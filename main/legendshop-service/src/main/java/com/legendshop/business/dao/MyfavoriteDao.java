/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Myfavorite;

/**
 * 我的商品收藏Dao
 *
 */
public interface MyfavoriteDao extends GenericDao<Myfavorite, Long> {
     
    public void deleteFavoriteByUserId(String userId);
	
	public abstract Long saveFavorite(Myfavorite favorite);
	
	public abstract Boolean isExistsFavorite(Long prodId,String userName);

	public abstract void deleteFavorite(String userId, String[] favIds);
	
	public abstract Long getfavoriteLength(String userId);

	public abstract PageSupport<Myfavorite> getFavoriteList(String curPageNO,Integer pageSize, String userId);
	
	/**
	 * 获取用户的礼券
	 */
	public abstract Long getCouponLength(String userName);
	
	/**
	 * 修改支付密码
	 */
	abstract void updatePaymentPassword(String userName, String paymentPassword);
	void flushUserDetail(String userId);

	/**
	 * 收集我的商品收藏
	 * 
	 */
	public abstract PageSupport<Myfavorite> collect(String curPageNO,Integer pageSize, String userId);

	public abstract void deleteFav(String userId, Long prodId);

	public int deleteFavoriteByProdIdAndUserId(Long prodId, String userId);

 }
