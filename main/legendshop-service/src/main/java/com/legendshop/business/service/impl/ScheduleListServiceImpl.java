package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ScheduleListDao;
import com.legendshop.model.entity.ScheduleList;
import com.legendshop.spi.service.ScheduleListService;

/**
 * 任务调度服务
 */
@Service("scheduleListService")
public class ScheduleListServiceImpl implements ScheduleListService {
	
	@Autowired
	private ScheduleListDao scheduleListDao;
	
	@Override
	public  List<ScheduleList> getNonexecutionScheduleList(Date triggerTime, Integer value) {
		return scheduleListDao.getNonexecutionScheduleList(triggerTime,value);
	}

	@Override
	public int deleteScheduleList(ScheduleList ScheduleList) {
		return scheduleListDao.deleteScheduleList(ScheduleList);
	}

	@Override
	public List<Long> saveScheduleList(List<ScheduleList> scheduleLists) {
		return scheduleListDao.saveScheduleList(scheduleLists);
	}

	@Override
	public List<ScheduleList> getScheduleList(Long shopId, String scheduleSn, Integer scheduleType) {
		return scheduleListDao.getScheduleList(shopId,scheduleSn,scheduleType);
	}

	@Override
	public Long saveScheduleList(ScheduleList scheduleList) {
		return scheduleListDao.saveScheduleList(scheduleList);
	}

	@Override
	public int updateScheduleListStatus(Long shopId, int status, Long id) {
		return scheduleListDao.updateScheduleListStatus(shopId,status,id);
	}
}
