/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ZhiboAccountDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ZhiboAccount;
import com.legendshop.util.AppUtils;


/**
 * 直播账号Dao实现类
 */
@Repository
public class ZhiboAccountDaoImpl extends GenericDaoImpl<ZhiboAccount, String> implements ZhiboAccountDao  {

   	/**
	 *  根据Id获取
	 */
	public ZhiboAccount getZhiboAccountById(String id){
		return getById(id);
	}

   /**
	 *  删除
	 */	
    public int deleteZhiboAccount(ZhiboAccount zhiboAccount){
    	return delete(zhiboAccount);
    }

   /**
	 *  保存
	 */		
	public String saveZhiboAccount(ZhiboAccount zhiboAccount){
		return save(zhiboAccount);
	}

   /**
	 *  更新
	 */		
	public int updateZhiboAccount(ZhiboAccount zhiboAccount){
		return update(zhiboAccount);
	}

    public int insertZhiboAccount(ZhiboAccount zhiboAccount) {
        return this.update("INSERT INTO ls_zhibo_account(uid,pwd,state,register_time,type) VALUES(?,?,?,?,?)", 
     		   zhiboAccount.getUid(),zhiboAccount.getPwd(),zhiboAccount.getState(),zhiboAccount.getRegisterTime(),zhiboAccount.getType());}
    
    public int loginUpdate(ZhiboAccount zhiboAccount) {
        return this.update("UPDATE ls_zhibo_account SET login_time = ?,token = ?,user_sig=? where uid = ?", zhiboAccount.getLoginTime(),zhiboAccount.getToken(),zhiboAccount.getUserSig(),zhiboAccount.getUid());
    }
    
    public ZhiboAccount getAccountByToken(String token,String type) {
    	if(AppUtils.isBlank(token)){
    		System.out.println("missing token, type = " + type);
    	}
        return this.get("select * from ls_zhibo_account where token = ? and type = ?",ZhiboAccount.class, token,type);
    }

	@Override
	public ZhiboAccount getZhiboAccountByType(String userName, String type) {
		return this.get("select * from ls_zhibo_account where uid = ? and type = ?",ZhiboAccount.class, userName,type);
	}
 }
