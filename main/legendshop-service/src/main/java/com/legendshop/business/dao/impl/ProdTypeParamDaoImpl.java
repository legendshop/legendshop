/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdTypeParamDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ProdTypeParam;

/**
 * 商品类型跟参数的关系服务
 */
@Repository
public class ProdTypeParamDaoImpl extends GenericDaoImpl<ProdTypeParam, Long> implements ProdTypeParamDao  {

	@Override
	public ProdTypeParam getProdTypeParam(Long id) {
		return getById(id);
	}
     
	@Override
	public int deleteProdTypeParam(ProdTypeParam prodTypeParam) {
		return delete(prodTypeParam);
	}

	@Override
	public Long saveProdTypeParam(ProdTypeParam prodTypeParam) {
		return save(prodTypeParam);
	}

	@Override
	public List<Long> saveProdTypeParam(List<ProdTypeParam> prodTypeParamList) {
		return save(prodTypeParamList);
	}

	@Override
	public ProdTypeParam getProdTypeParam(Long propId, Long prodTypeId) {
		return get("select ptp.*,lpp.prop_name as propName from ls_prod_type_param ptp, ls_prod_prop lpp where ptp.prop_id=lpp.prop_id and ptp.type_id = ? and ptp.prop_id = ?",ProdTypeParam.class,prodTypeId,propId);
	}

	@Override
	public void updateProdTypeParam(ProdTypeParam prodTypeParam) {
		update(prodTypeParam);
	}
	
 }
