/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.store;

import com.legendshop.dao.Dao;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.dto.store.StoreCitys;
import com.legendshop.model.entity.store.StoreProd;

import java.util.List;

/**
 * 门店商品Dao接口.
 */
public interface StoreProdDao extends Dao<StoreProd, Long> {

    /** 获取门店商品 */
	public abstract StoreProd getStoreProd(Long id);
	
	/** 删除门店商品 */
    public abstract int deleteStoreProd(StoreProd storeProd);
	
    /** 保存门店商品 */
	public abstract Long saveStoreProd(StoreProd storeProd);
	
	/** 更新门店商品 */
	public abstract int updateStoreProd(StoreProd storeProd);

	/** 获取门店商品数量 */
	public abstract long getStoreProdCount(Long poductId, Long skuId);

	/** 获取门店所在城市 */
	public abstract List<StoreCitys> findStoreCitys(Long poductId, String province, String city, String area);

	/** 查找商城在城市里的门店 */
	public abstract List<StoreCitys> findStoreShopCitys(Long shopId, String province, String Integer, String area);

	/** 获取门店商品 */
	public abstract Integer getStocks(Long storeId, Long poductId, Long skuId);
	
	/** 获取门店商品Dto */
	public abstract StoreProductDto getStoreProductDto(Long storeId,Long prodId);
	
	/** 获取门店商品 */
	public abstract StoreProd getStoreProd(Long storeId,Long prodId);
	
	/** 主动建立主键，用于保存 */
	public  abstract Long createId();

	/** 用指定Id来建立 */
	public abstract void saveStoreProd(StoreProd storeProd, Long spId);
	
	/** 获取门店商品 */
	public abstract StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId);
	
 }
