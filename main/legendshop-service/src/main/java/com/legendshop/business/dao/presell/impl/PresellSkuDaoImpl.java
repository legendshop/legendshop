/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.presell.impl;

import com.legendshop.business.dao.presell.PresellSkuDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSku;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Class PresellSkuDaoImpl. 预售活动skuDao实现类
 */
@Repository
public class PresellSkuDaoImpl extends GenericDaoImpl<PresellSku, Long> implements PresellSkuDao {

	/**
	 * 根据Id获取预售活动sku
	 */
	public PresellSku getPresellSku(Long id){
		return getById(id);
	}

	/**
	 *  删除预售活动sku
	 *  @param presellSku 实体类
	 *  @return 删除结果
	 */	
    public int deletePresellSku(PresellSku presellSku){
    	return delete(presellSku);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deletePresellSku(Long id){
		return deleteById(id);
	}

	/**
	 * 保存预售活动sku
	 */
	public Long savePresellSku(PresellSku presellSku){
		return save(presellSku);
	}

	/**
	 *  更新预售活动sku
	 */		
	public int updatePresellSku(PresellSku presellSku){
		return update(presellSku);
	}

	/**
	 * 分页查询预售活动sku列表
	 */
	public PageSupport<PresellSku>queryPresellSku(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(PresellSku.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

    @Override
    public void deletePresellSkuByPid(Long presellId) {
        String sql="delete from ls_presell_sku where p_id=?";
        update(sql,presellId);
    }

    @Override
    public PresellSku getPresellSkuByActive(Long skuId, Long id) {
        String sql = "SELECT id,p_id,prod_id,sku_id,prsell_price FROM ls_presell_sku WHERE sku_id=? AND p_id=?";
		return get(sql,PresellSku.class,skuId,id);
    }

	@Override
	public PresellSku getPresellSkuBySkuId(Long presellId,Long skuId) {
		String sql="SELECT id,p_id,prod_id,sku_id,prsell_price FROM ls_presell_sku WHERE p_id=? and sku_id=?";
		return get(sql,PresellSku.class,presellId,skuId);
	}

	@Override
	public List<Long> getSkuIdByPresellId(Long id)
	{
		String sql = "SELECT `sku_id` FROM `ls_presell_sku` WHERE `p_id`=?";
		return query(sql,Long.class,id);
	}

}
