/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdUserParamDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdUserParam;

import java.util.List;

/**
 * The Class ProdUserParamDaoImpl.
 */
@Repository
public class ProdUserParamDaoImpl extends GenericDaoImpl<ProdUserParam, Long> implements ProdUserParamDao  {
     

	public ProdUserParam getProdUserParam(Long id){
		return getById(id);
	}
	
    public void deleteProdUserParam(Long id, Long shopId){
    	 update("delete from ls_prod_user_param where id = ? and shop_id = ?",id,shopId);
    }
	
	public Long saveProdUserParam(ProdUserParam prodUserParam){
		return save(prodUserParam);
	}
	
	public int updateProdUserParam(ProdUserParam prodUserParam){
		return update(prodUserParam);
	}

	@Override
	public PageSupport<ProdUserParam> getProdUserParamPage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProdUserParam.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId",shopId);
		return queryPage(cq);
	}

	/**
	 * 用户自定义的参数
	 */
	@Override
	public PageSupport<ProdUserParam> getUserParamOverlay(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ProdUserParam.class, curPageNO);
		cq.setPageSize(5);
		cq.eq("shopId",shopId);
		cq.addOrder("desc", "recDate");
		return queryPage(cq);
	}


	@Override
	public List<ProdUserParam> getShopProdUserParam(Long shopId) {
		return  query("select * from ls_prod_user_param where shop_id = ?",ProdUserParam.class,shopId);
	}
 }
