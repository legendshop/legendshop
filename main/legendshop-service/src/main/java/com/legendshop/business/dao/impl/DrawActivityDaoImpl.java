/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.DrawActivityDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.util.AppUtils;

/**
 * 抽奖活动Dao实现类
 * The Class DrawActivityDaoImpl.
 * 
 */
@Repository
public class DrawActivityDaoImpl extends GenericDaoImpl<DrawActivity, Long> implements DrawActivityDao {

	/** 获取正在进行中的抽奖活动列表 */
	public List<DrawActivity> getDrawActivity() {
		String sql = "select id,act_name,act_type,act_des,act_status,win_pencent,start_time,end_time,times_perday,times_person,win_join_status,"
				+ "act_rule,awards_info from ls_draw_activity where act_status=1 and start_time <= ? and end_time >= ? order by end_time";
		Date nowDate = new Date();
		return query(sql, DrawActivity.class, nowDate, nowDate);
	}

	public DrawActivity getDrawActivity(Long id) {
		return getById(id);
	}

	/** 删除 */
	public int deleteDrawActivity(DrawActivity drawActivity) {
		return delete(drawActivity);
	}

	/** 保存 */
	public Long saveDrawActivity(DrawActivity drawActivity) {
		return save(drawActivity);
	}

	/** 更新 */
	public int updateDrawActivity(DrawActivity drawActivity) {
		return update(drawActivity);
	}

	/**
	 * @Description: 根据奖品或奖项获取活动条数
	 * @param @param
	 *            sql
	 * @param @return
	 * @date 2016-4-18
	 */
	@Override
	public boolean getActivityCount(Long giftId, Long awardsId) {
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StringBuffer sql = new StringBuffer(
				"SELECT count(act.id) FROM ls_draw_awards_rel rel left join ls_draw_activity act on rel.act_id=act.id where 1=1");
		sql.append(" and ('" + sdformat.format(new Date()) + "') between act.start_time and act.end_time and ");
		Object[] par = new Object[1];
		if (AppUtils.isNotBlank(giftId)) {
			sql.append("rel.gift_id = ?");
			par[0] = giftId;
		} else {
			sql.append("rel.awards_id =?");
			par[0] = awardsId;
		}
		return this.getLongResult(sql.toString(), par) > 0;
	}

	/** 
	 * 获取抽奖活动页面
	 */
	@Override
	public PageSupport<DrawActivity> getDrawActivityPage(String curPageNO, DrawActivity drawActivity) {
		CriteriaQuery cq = new CriteriaQuery(DrawActivity.class, curPageNO);
		if (AppUtils.isNotBlank(drawActivity.getActType())) {
			cq.eq("actType", drawActivity.getActType());
		}
		if (AppUtils.isNotBlank(drawActivity.getActName())) {
			cq.like("actName", "%" + drawActivity.getActName().trim() + "%");
		}
		if (AppUtils.isNotBlank(drawActivity.getEndTime())) {
			cq.lt("endTime", drawActivity.getEndTime());
		}
		if (AppUtils.isNotBlank(drawActivity.getStartTime())) {
			cq.gt("startTime", drawActivity.getStartTime());
		}
		cq.addDescOrder("id");
		return queryPage(cq);
	}

	@Override
	public List<DrawActivity> getCouponByDate(Date startDate, Date endDate) {
		String sql = "select * from ls_draw_activity where end_time > ?";
		return query(sql, DrawActivity.class, startDate);
	}

}
