package com.legendshop.business.resolver;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.spi.resolver.order.IActiveRuleResolver;
import com.legendshop.spi.service.ActiveRuleResolverManager;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;


/*
 * 促销规则管理器--用于商品详情展示,不参与商品价格计算
 */
@Service("activeRuleResolverManager")
public class ActiveRuleResolverManagerImpl implements ActiveRuleResolverManager {
	
	@Autowired
	private MarketingService marketingService;
	
	@Resource(name="activeRuleResolver")
	private Map<Integer, IActiveRuleResolver> executors;
	
	@Override
	public void executorMarketing(RuleResolver resolver) {
		/*查询该商品所有可用的营销活动*/
	    List<MarketingDto> marketingDtoList = marketingService.findAvailableBusinessRuleList(resolver.getShopId(), resolver.getProdId());
	    if(AppUtils.isNotBlank(marketingDtoList)){
	    	//2. apply all rule to the prod
          	MarketRuleContext context = new MarketRuleContext(); //应用规则上下文,用于做排除
			for (MarketingDto marketingDto : marketingDtoList) {
				if(executors!=null && executors.containsKey(marketingDto.getType())){
					IActiveRuleResolver executor =executors.get(marketingDto.getType());
					//把业务规则应用到商品
					executor.execute(marketingDto,resolver, context);
				}
			}
	    }
	}
	
	@Bean("activeRuleResolver")
	public Map<Integer, IActiveRuleResolver> initIActiveRuleResolver(
			@Qualifier(value="manjianRuleResolver") ManjianRuleResolver  manjianRuleResolver,
			@Qualifier(value="manZeRuleResolver") ManZeRuleResolver  manZeRuleResolver,
			@Qualifier(value="xianShiRuleResolver") XianShiRuleResolver xianShiRuleResolver,
			@Qualifier(value="manJianFullRuleResolver") ManJianFullRuleResolver  manJianFullRuleResolver,
			@Qualifier(value="manYuanFullRuleResolver") ManYuanFullRuleResolver  manYuanFullRuleResolver
			){
		Map<Integer, IActiveRuleResolver> map = new LinkedHashMap<Integer, IActiveRuleResolver>();
		map.put(0, new ManjianRuleResolver());
		map.put(1, new ManZeRuleResolver());
		map.put(2, new XianShiRuleResolver());
		map.put(4, new ManJianFullRuleResolver());
		map.put(5, new ManYuanFullRuleResolver());
		return map; 
	}
}
