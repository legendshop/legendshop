/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AccusationType;

/**
 * 举报类型Dao
 */
public interface AccusationTypeDao extends GenericDao<AccusationType, Long> {
     
    public abstract List<AccusationType> getAccusationType(String shopName);

	public abstract AccusationType getAccusationType(Long id);
	
    public abstract void deleteAccusationType(AccusationType accusationType);
	
	public abstract Long saveAccusationType(AccusationType accusationType);
	
	public abstract void updateAccusationType(AccusationType accusationType);
	
	public List<AccusationType> queryAccusationType();
	
 }
