/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ArticleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.entity.Article;
import com.legendshop.spi.service.ArticleService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * The Class ArticleServiceImpl.
 *  服务实现类
 */
@Service("articleService")
public class ArticleServiceImpl  implements ArticleService{
   
	@Autowired
    private ArticleDao articleDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

   	/**
	 *  根据Id获取
	 */
    public Article getArticle(Long id) {
        return articleDao.getArticle(id);
    }

   /**
	 *  删除
	 */ 
    public void deleteArticle(Article article) {
        articleDao.deleteArticle(article);
    }

   /**
	 *  保存
	 */	    
    public Long saveArticle(Article article) {
        if (!AppUtils.isBlank(article.getId())) {
            updateArticle(article);
            return article.getId();
        }
        return articleDao.saveArticle(article);
    }

   /**
	 *  更新
	 */	
    public void updateArticle(Article article) {
        articleDao.updateArticle(article);
    }

	@Override
	public PageSupport<Article> getArticlePage(String curPageNO, Article article) {
		return articleDao.getArticlePage(curPageNO,article);
	}
	
	@Override
	public PageSupport<Article> query(String curPageNO) {
		return articleDao.query(curPageNO);
	}
	
	@Override
	public PageSupport<Article> getArticleList(String curPageNO) {
		return articleDao.getArticleList(curPageNO);
	}

	@Override
	public void saveArticle(String userName,String userId, Long shopId, Article article,String pic) {
		if (article.getPicFile() != null && article.getPicFile().getSize() > 0) {
			attachmentManager.saveImageAttachment(userName, userId,shopId, pic, article.getPicFile(), AttachmentTypeEnum.CATEGORY_RECOMM_ADV);
			if (AppUtils.isNotBlank(article.getPic())) {
				// 删除原图片
				attachmentManager.delAttachmentByFilePath(article.getPic());
				attachmentManager.deleteTruely(article.getPic());
			}
			article.setPic(pic);
		}
		saveArticle(article);
	}
}
