/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.CateGoryExportDTO;
import com.legendshop.model.entity.Category;

import java.util.List;

/**
 * 分类树Dao
 *
 */
public interface CategoryDao extends Dao<Category, Long> {
     
	public abstract Category getCategory(Long id);

    /**
     * 是否存在这个三级分类
     * @param categoryId
     * @return
     */
    Boolean isExistThirdCategory(Long categoryId);
	
    public abstract int deleteCategory(Category category);
	
	public abstract Long saveCategory(Category category);
	
	public abstract int updateCategory(Category category);
	
	public abstract PageSupport<Category> getCategory(CriteriaQuery cq);
    /**
     *获取平台类目导出list(只获取有三级类目的分类)
     * @return
     */
    List<CateGoryExportDTO> getCateGoryExportDTOList();


    /** 获取店铺类目导出list */
    List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId);
	
	/**
     * 获取所有分类信息 ，用于后台分类树组建
     * @return
     */
	public abstract List<Category> getCategoryList(String type);

	/**
	 * 获取所有分类信息 ，用于后台分类树组建
	 * @return
	 */
	public abstract List<Category> getCategoryListByStatus(String type);

	/**
	 * 获取子分类
	 * @param parentId
	 * @return
	 */
	public abstract List<Category> getChildren(Long parentId);

	/**
	 * 获取商品列表及商品分类导航数据 用于内存树的数据组建
	 * @return
	 */
	public abstract List<Category> getProductCategory();
	
	

	List<Category> getCategory(String sortType, Integer headerMenu,Integer navigationMenu, Integer status,Integer level);

	
	
	/** 根据 parentId 查找 商品分类 **/
	public List<Category> findCategory(Long parentId);
	
	/** 根据 分类名称  查找 商品分类 **/
	public List<Category> findCategory(String cateName,Integer level,Long parentId);

	public abstract List<Category> getCategoryByIds(List<Long> catIdList);

	public abstract void updateCategoryList(List<Category> categorys);

	public abstract List<Category> getCategorysByTypeId(Integer typeId);
	
	/** 查找 首页导航栏上显示的分类 **/
	public abstract List<Category> queryCategoryList();

	public abstract List<Category> getCategoryListByName(String categoryName);

	public abstract List<Category> getAllProductCategory();

	public abstract PageSupport<Category> getCategoryPage(String curPageNO);

	/** 根据类型id获取商品类目 */
	public abstract List<Category> findProdTypeId(Integer prodTypeId);

	/**
	 * 根据分类等级获取分类
	 * @param grade
	 * @return
	 */
	List<Category> findCategoryByGradeAndParentId(Integer grade, Long parentId);
}
