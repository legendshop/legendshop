/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.CouponShop;

public interface CouponShopDao extends GenericDao<CouponShop, Long> {
     
	public abstract List<Long> saveCouponShops(List<CouponShop> couponShops);

	public abstract List<CouponShop> getCouponShopByCouponIds(List<Long> couponIds);
	
 }
