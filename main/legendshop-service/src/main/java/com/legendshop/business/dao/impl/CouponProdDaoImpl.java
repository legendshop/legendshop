/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CouponProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.CouponProd;

/**
 * 优惠券商品服务
 */
@Repository
public class CouponProdDaoImpl extends GenericDaoImpl<CouponProd, Long> implements CouponProdDao  {
     
	public CouponProd getCouponProd(Long id){
		return getById(id);
	}
	
    public int deleteCouponProd(CouponProd couponProd){
    	return delete(couponProd);
    }
	
	public Long saveCouponProd(CouponProd couponProd){
		return save(couponProd);
	}
	
	public int updateCouponProd(CouponProd couponProd){
		return update(couponProd);
	}
	
	public PageSupport getCouponProd(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public List<CouponProd> getCouponProdByCouponId(Long couponId) {
		return this.queryByProperties(new EntityCriterion().eq("couponId", couponId));
	}

	public void deleteByCouponId(Long couponId) {
		update("delete from ls_coupon_prod where coupon_id = ?", couponId);
		
	}

	@Override
	public List<CouponProd> getCouponProdByCouponIds(List<Long> couponIds) {
		StringBuffer sql = new StringBuffer("select * from ls_coupon_prod where coupon_id in (");
		for (int i = 0; i < couponIds.size(); i++) {
			sql.append("?,");
		}
		sql.setLength(sql.length()-1);
		sql.append(")");
		return this.query(sql.toString(), CouponProd.class, couponIds.toArray());
	}

	@Override
	public List<CouponProd> getCouponProdByProdId(Long prodId) {
		return this.queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}
	
	@Override
	public List<Long> getCouponProdIds(Long couponId) {
		String sql="SELECT prod_id FROM ls_coupon_prod  WHERE coupon_id=? ";
		return query(sql,Long.class, couponId);
	}
	
 }
