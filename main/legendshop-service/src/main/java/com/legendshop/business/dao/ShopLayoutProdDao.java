/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;

/**
 * 布局商品Dao.
 */

public interface ShopLayoutProdDao extends GenericDao<ShopLayoutProd, Long> {
     
    public abstract List<ShopLayoutProd> getShopLayoutProdByShopId(Long  shopId);
    
    public List<ShopLayoutProd> getShopLayoutProd(Long shopId,Long layoutId);

	public abstract ShopLayoutProd getShopLayoutProd(Long id);
	
    public abstract int deleteShopLayoutProd(ShopLayoutProd shopLayoutProd);
	
	public abstract Long saveShopLayoutProd(ShopLayoutProd shopLayoutProd);
	
	public abstract int updateShopLayoutProd(ShopLayoutProd shopLayoutProd);

	public abstract void deleteShopLayoutProd(Long shopId, Long layoutId);
	
 }
