package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserRoleDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.entity.UserRole;
import com.legendshop.model.entity.UserRoleId;

@Repository
public class UserRoleDaoImpl implements UserRoleDao {
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	@Override
	public List<UserRole> getUserRoleByUserId(String userId) {
		return genericJdbcDao.query("select user_id,role_id,app_no  from ls_usr_role where user_id = ?", new Object[] { userId }, new UserRoleRowMapper());
	}
	
	@Override
	public List<UserRole> getUserRoleByRoleId(String roleId) {
		return genericJdbcDao.query("select user_id,role_id,app_no  from ls_usr_role where role_id = ?", new Object[] { roleId }, new UserRoleRowMapper());
	}

	@Override
	public UserRole getUserRoleById(UserRoleId id) {
		return genericJdbcDao.get("select * from ls_usr_role  where user_id ? and role_id = ? and app_no = ?",new Object[]{ id.getUserId(),id.getRoleId(), id.getAppNo()}, new UserRoleRowMapper());
	}

	@Override
	public int deleteUserRoleById(UserRoleId id) {
		return genericJdbcDao.update("delete  from ls_usr_role  where user_id ? and role_id = ? and app_no = ?", new Object[]{ id.getUserId(),id.getRoleId(),id.getAppNo()});
	}

	@Override
	public int deleteUserRole(UserRole userRole) {
		return deleteUserRoleById(userRole.getId());
	}

	@Override
	public int saveUserRole(UserRole userRole) {
		return genericJdbcDao.update("insert into ls_usr_role(user_id,role_id,app_no)  values (?,?,?)", userRole.getId().getUserId(), userRole.getId().getRoleId(), userRole.getId().getAppNo());
	}
	
	@Override
	public void saveUserRoleList(List<UserRole> userRoles) {
		if(userRoles == null || userRoles.size() == 0){
			return;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (UserRole ur : userRoles) {
			Object[] param = new Object[3];
			param[0] = ur.getId().getUserId();
			param[1] = ur.getId().getRoleId();
			param[2] = ur.getId().getAppNo();
			batchArgs.add(param);
		}
		 genericJdbcDao.batchUpdate("insert into ls_usr_role(user_id,role_id,app_no)  values (?,?,?)", batchArgs);
		
	}

	@Override
	public int deleteUserRole(List<UserRole> userRoleList) {
		if(userRoleList == null || userRoleList.size() == 0){
			return 0;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (UserRole ur : userRoleList) {
			Object[] param = new Object[3];
			param[0] = ur.getId().getUserId();
			param[1] = ur.getId().getRoleId();
			param[2] = ur.getId().getAppNo();
			batchArgs.add(param);
		}
		 genericJdbcDao.batchUpdate("delete  from ls_usr_role  where user_id = ? and role_id = ? and app_no = ?", batchArgs);
		 return 1;
	}

	/**
	 * 删除用户的权限
	 */
	@Override
	public void deleteUserRoleByUserId(String userId, ApplicationEnum appEnum) {
		genericJdbcDao.update("delete  from ls_usr_role  where user_id = ?  and app_no = ?", new Object[]{userId, appEnum.value()});
	}
	
	private class UserRoleRowMapper implements RowMapper<UserRole>{

		@Override
		public UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {
			UserRole ur = new UserRole();
			UserRoleId id = new UserRoleId();
			id.setUserId(rs.getString("user_id"));
			id.setRoleId(rs.getString("role_id"));
			id.setAppNo(rs.getString("app_no"));
			ur.setId(id);
			return ur;
		}
	}

	@Override
	public void deleteByUserIdAndRoleId(String id, String roldId) {
		this.genericJdbcDao.update("delete  from ls_usr_role  where user_id = ?  and role_id = ?", new Object[]{id, roldId});
	}

	@Override
	public Integer isExistUserRole(String userId, String roldId) {
		return this.genericJdbcDao.get("select count(*) from ls_usr_role where user_id = ?  and role_id = ?", Integer.class, userId, roldId);
	}
	
}
