/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopLayoutProdDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;

/**
 * The Class ShopLayoutProdDaoImpl.
 */
@Repository
public class ShopLayoutProdDaoImpl extends GenericDaoImpl<ShopLayoutProd, Long> implements ShopLayoutProdDao  {
     
    public List<ShopLayoutProd> getShopLayoutProdByShopId(Long  shopId){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
    }
    
    public List<ShopLayoutProd> getShopLayoutProd(Long shopId,Long layoutId){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("layoutId", layoutId));
    }
    
    

	public ShopLayoutProd getShopLayoutProd(Long id){
		return getById(id);
	}
	
    public int deleteShopLayoutProd(ShopLayoutProd shopLayoutProd){
    	return delete(shopLayoutProd);
    }
	
	public Long saveShopLayoutProd(ShopLayoutProd shopLayoutProd){
		return save(shopLayoutProd);
	}
	
	public int updateShopLayoutProd(ShopLayoutProd shopLayoutProd){
		return update(shopLayoutProd);
	}

	@Override
	public void deleteShopLayoutProd(Long shopId, Long layoutId) {
		update("delete from ls_shop_layout_prod where shop_id=? and layout_id=? ", shopId, layoutId);
	}

 }
