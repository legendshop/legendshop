/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MergeGroupAddDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.entity.MergeGroupAdd;

/**
 * The Class MergeGroupAddDaoImpl.
 * 拼团参加表Dao实现类
 */
@Repository
public class MergeGroupAddDaoImpl extends GenericDaoImpl<MergeGroupAdd, Long> implements MergeGroupAddDao  {

	/**
	 *  根据Id获取拼团参加表
	 */
	public MergeGroupAdd getMergeGroupAdd(Long id){
		return getById(id);
	}

	/**
	 *  删除拼团参加表
	 */	
	public int deleteMergeGroupAdd(MergeGroupAdd mergeGroupAdd){
		return delete(mergeGroupAdd);
	}

	/**
	 *  保存拼团参加表
	 */		
	public Long saveMergeGroupAdd(MergeGroupAdd mergeGroupAdd){
		return save(mergeGroupAdd);
	}

	/**
	 *  更新拼团参加表
	 */		
	public int updateMergeGroupAdd(MergeGroupAdd mergeGroupAdd){
		return update(mergeGroupAdd);
	}

	@Override
	public List<MergeGroupingDto> getMergeGroupingByMergeId(Long mergeId, Integer count) {
		String sql = "SELECT o.add_number AS addNumber,o.number AS propleCount, "
				+ "a.portrait_pic AS portraitPic,o.end_time as endTime, o.create_time AS createTime, "
				+ "o.end_time AS endTime "
				+ "FROM ls_merge_group_operate o "
				+ "LEFT JOIN ls_merge_group_add a ON a.operate_id = o.id AND a.is_head = ? "
				+ "WHERE o.status=? "
				+ "AND o.merge_id = ? "
				+ "AND o.end_time > ? "
				+ "GROUP BY o.id";
		
		if(null == count || count == 0){
			return query(sql, MergeGroupingDto.class, 1, 1, mergeId, new Date());
		}
		
		return queryLimit(sql, MergeGroupingDto.class, 0, count, new Object[]{1, 1, mergeId, new Date()});
	}
	
	public void updateMergeGroupAddStatus(Long operateId) {
		String sql ="update ls_merge_group_add set status=2 where operate_id=?";
		this.update(sql, operateId);
	}

	public List<String> queryUserPicByMergeGroupSub(Long mergeId, Long operateId) {
		String sql = "SELECT u.portrait_pic FROM ls_merge_group_add a "+
					 "LEFT JOIN ls_usr_detail u ON u.user_id=a.user_id WHERE a.merge_id=? AND a.operate_id =?";
		return this.query(sql, String.class, mergeId,operateId);
	}

	public List<MergeGroupAdd> getMergeGroupAddByIds(List<Long> ids) {
		if(ids == null || ids.size() == 0){
			return null;
		}
		return queryByProperties(new EntityCriterion().in("operateId", ids.toArray()));
	}


	public List<MergeGroupAdd> findHaveInMergeGroupAdd(Long operateId) {
		return queryByProperties(new EntityCriterion().eq("status", 1).in("operateId",operateId));
	}

	public List<Long> findGroupExemption() {
		String sql=" SELECT ls_merge_group_add.id FROM  ls_merge_group_add LEFT JOIN ls_merge_group_operate ON ls_merge_group_operate.id=ls_merge_group_add.operate_id "
				+ " LEFT JOIN ls_merge_group ON  ls_merge_group.id=ls_merge_group_operate.merge_id WHERE ls_merge_group.is_head_free=1 AND ls_merge_group_add.is_head=1 AND ls_merge_group_add.status=? AND ls_merge_group_add.is_exemption=0 ";
		return query(sql, Long.class,MergeGroupAddStatusEnum.SUCCESS.value());
	}

	@Override
	public List<MergeGroupAdd> queryMergeGroupAddList(Long operateId, Integer count) {
		EntityCriterion ec = new EntityCriterion();
		ec.eq("operateId", operateId);
		ec.addDescOrder("isHead");
		ec.addDescOrder("createTime");
		
		if(null == count || count == 0){
			return this.queryByProperties(ec);
		}
		
		return this.queryByProperties(ec, 0, count);
	}

	@Override
	public MergeGroupAdd getMergeGroupAddByOperateIdAndUserId(Long operateId, String userId) {
		return this.getByProperties(new EntityCriterion().eq("operateId", operateId).eq("userId", userId));
	}

	@Override
	public MergeGroupAdd isMergeGroupHeadFreeSub(Long subId) {

		String sql=" SELECT ls_merge_group_add.* FROM  ls_merge_group_add LEFT JOIN ls_merge_group_operate ON ls_merge_group_operate.id=ls_merge_group_add.operate_id "
				+ " LEFT JOIN ls_merge_group ON  ls_merge_group.id=ls_merge_group_operate.merge_id WHERE ls_merge_group.is_head_free=1 AND ls_merge_group_add.is_head=1 AND ls_merge_group_add.status=2 AND ls_merge_group_add.sub_id = ? ";
		return get(sql, MergeGroupAdd.class,subId);
	}

}
