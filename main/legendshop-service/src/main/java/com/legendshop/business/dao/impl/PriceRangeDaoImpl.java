package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PriceRangeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PriceRange;
/**
 * 统计报表的价格范围.
 */
@Repository
public class PriceRangeDaoImpl extends GenericDaoImpl<PriceRange, Long> implements PriceRangeDao{

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public List<PriceRange> queryPriceRange(Long shopId,Integer type) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).eq("type", type));
	}

	@Override
	public PriceRange getPriceRange(Long id) {
		return getById(id);
	}

	@Override
	public int deletePriceRange(PriceRange priceRange) {
		return delete(priceRange);
	}

	@Override
	public Long savePriceRange(PriceRange priceRange) {
		return save(priceRange);
	}

	@Override
	public int updatePriceRange(PriceRange priceRange) {
		return update(priceRange);
	}

	@Override
	public void updatePriceRangeList(List<PriceRange> priceRangeList) {
		update(priceRangeList);
	}

	@Override
	public void savePriceRangeList(List<PriceRange> priceRangeList) {
		save(priceRangeList);
	}

	@Override
	public PageSupport<PriceRange> getPriceRange(String curPageNO, Long shopId, Integer type) {
		CriteriaQuery cq = new CriteriaQuery(PriceRange.class,curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		cq.eq("shopId", shopId);
		cq.eq("type", type);
		return queryPage(cq);
	}

}
