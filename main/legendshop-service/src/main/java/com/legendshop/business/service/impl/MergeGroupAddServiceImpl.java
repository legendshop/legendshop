/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MergeGroupAddDao;
import com.legendshop.business.dao.MergeGroupDao;
import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupAdd;
import com.legendshop.spi.service.MergeGroupAddService;
import com.legendshop.util.AppUtils;

/**
 * The Class MergeGroupAddServiceImpl.
 *  拼团参加表服务实现类
 */
@Service("mergeGroupAddService")
public class MergeGroupAddServiceImpl  implements MergeGroupAddService{

	@Autowired
	private MergeGroupAddDao mergeGroupAddDao;
	
	@Autowired
	private MergeGroupDao mergeGroupDao;

	/**
	 *  根据Id获取拼团参加表
	 */
	public MergeGroupAdd getMergeGroupAdd(Long id) {
		return mergeGroupAddDao.getMergeGroupAdd(id);
	}

	/**
	 *  删除拼团参加表
	 */ 
	public void deleteMergeGroupAdd(MergeGroupAdd mergeGroupAdd) {
		mergeGroupAddDao.deleteMergeGroupAdd(mergeGroupAdd);
	}

	/**
	 *  保存拼团参加表
	 */	    
	public Long saveMergeGroupAdd(MergeGroupAdd mergeGroupAdd) {
		if (!AppUtils.isBlank(mergeGroupAdd.getId())) {
			updateMergeGroupAdd(mergeGroupAdd);
			return mergeGroupAdd.getId();
		}
		return mergeGroupAddDao.saveMergeGroupAdd(mergeGroupAdd);
	}

	/**
	 *  更新拼团参加表
	 */	
	public void updateMergeGroupAdd(MergeGroupAdd mergeGroupAdd) {
		mergeGroupAddDao.updateMergeGroupAdd(mergeGroupAdd);
	}

	public List<MergeGroupingDto> getMergeGroupingByMergeId(Long mergeId,Integer activeTime) {
		
		return mergeGroupAddDao.getMergeGroupingByMergeId(mergeId,activeTime);
	}

	public List<MergeGroupAdd> findHaveInMergeGroupAdd(Long operateId) {
		return mergeGroupAddDao.findHaveInMergeGroupAdd(operateId);
	}

	/**
	 * 查询已经拼团成功并且是团长免单的拼团记录; 
	 */
	public List<Long> findGroupExemption() {
		return mergeGroupAddDao.findGroupExemption();
	}

	@Override
	public boolean joinMergeGroup(Long activeId, String userId) {
		//查询改活动是否限购
		MergeGroup mergeGroup = mergeGroupDao.getMergeGroup(activeId);
		if (AppUtils.isBlank(mergeGroup)) {
			return false;
		}
		//不限购，返回true
		if (!mergeGroup.getIsLimit()) {
			return true;
		}
		//获取该活动该用户开启的拼团数，只获取为团长的数量、
		Long count = mergeGroupDao.joinMergeGroup(activeId,userId);
		if (count >= mergeGroup.getLimitNumber()) {
			return false;
		}
		return true;
	}

	@Override
	public List<MergeGroupAdd> queryMergeGroupAddList(Long operateId, Integer count) {
		return mergeGroupAddDao.queryMergeGroupAddList(operateId, count);
	}

	@Override
	public MergeGroupAdd getMergeGroupAddByOperateIdAndUserId(Long operateId, String userId) {
		return mergeGroupAddDao.getMergeGroupAddByOperateIdAndUserId(operateId, userId);
	}

	@Override
	public Boolean isMergeGroupHeadFreeSub(Long subId) {

		MergeGroupAdd mergeGroupAdd = mergeGroupAddDao.isMergeGroupHeadFreeSub(subId);
		if (AppUtils.isBlank(mergeGroupAdd)){
			return false;
		}
		return true;
	}

}
