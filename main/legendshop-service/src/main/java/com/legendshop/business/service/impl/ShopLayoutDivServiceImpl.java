/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShopLayoutDivDao;
import com.legendshop.spi.service.ShopLayoutDivService;

/**
 * The Class ShopLayoutHotServiceImpl.
 */
@Service("shopLayoutDivService")
public class ShopLayoutDivServiceImpl  implements ShopLayoutDivService{
	
	@Autowired
    private ShopLayoutDivDao shopLayoutDivDao;

	@Override
	public void deleteShopLayoutDiv(Long shopId, Long layoutId) {
		shopLayoutDivDao.deleteShopLayoutDiv(shopId, layoutId);
	}
}
