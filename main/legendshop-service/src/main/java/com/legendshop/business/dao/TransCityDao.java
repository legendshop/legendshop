/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.model.entity.TransCity;

/**
 *每个城市的运费设置Dao
 */
public interface TransCityDao{
	
    public abstract int deleteTransCity(Long transfeeId);
	
	public abstract void saveTransCity(List<TransCity> transCityList);
	
	public abstract List<TransCity> getTransCitys(Long transfeeId);
	
	public abstract void deleteTransCity(List<TransCity> transCityList);
	
 }
