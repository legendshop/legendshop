/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.PropValueAlias;

/**
 *商品属性值别名Dao
 */
public interface PropValueAliasDao extends GenericDao<PropValueAlias, Long> {
     
    public abstract void clearPropValueAliasCache(Long prodId);

	public abstract PropValueAlias getPropValueAlias(Long id);
	
    public abstract int deletePropValueAlias(PropValueAlias propValueAlias);
	
	public abstract Long savePropValueAlias(PropValueAlias propValueAlias);
	
	public abstract int updatePropValueAlias(PropValueAlias propValueAlias);
	
	public abstract List<PropValueAlias> queryByProdId(Long prodId);
	
	//没有缓存
	public abstract List<PropValueAlias> queryByProdIdNoCache(Long prodId);
	
 }
