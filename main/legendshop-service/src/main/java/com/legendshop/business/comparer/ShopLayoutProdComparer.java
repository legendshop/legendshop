package com.legendshop.business.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;
import com.legendshop.util.AppUtils;


/**
 * 
 * @author tony
 *
 */
public class ShopLayoutProdComparer implements DataComparer<ShopLayoutProdDto, ShopLayoutProd> {

	@Override
	public boolean needUpdate(ShopLayoutProdDto dto, ShopLayoutProd dbObj,
			Object obj) {
		if(isChage(dto.getSeq(),dbObj.getSeq())){
			dbObj.setSeq(dto.getSeq());
			return true;
		}
		return false;
	}
	
	/** 是否有改变 **/
	private  boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isExist(ShopLayoutProdDto dto, ShopLayoutProd dbObj) {
		if(AppUtils.isBlank(dto.getLayoutProdId())){
			return false;
		}
		if(dto.getLayoutProdId().equals(dbObj.getId())){
			return true;
		}
		return false;
	}

	@Override
	public ShopLayoutProd copyProperties(ShopLayoutProdDto dtoj, Object obj) {
		ShopLayoutProd layoutProd=new ShopLayoutProd();
		layoutProd.setId(dtoj.getLayoutProdId());
		layoutProd.setLayoutId(dtoj.getLayoutId());
		layoutProd.setShopId(dtoj.getShopId());
		layoutProd.setShopDecotateId(dtoj.getShopDecotateId());
		layoutProd.setSeq(dtoj.getSeq()==null?0:dtoj.getSeq());
		layoutProd.setProdId(dtoj.getProdId());
		return layoutProd;
	}
	

}
