/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MergeProductDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.MergeProduct;

/**
 * The Class MergeProductDaoImpl.
  * 拼团活动参加商品Dao实现类
 */
@Repository
public class MergeProductDaoImpl extends GenericDaoImpl<MergeProduct, Long> implements MergeProductDao  {
   	/**
	 *  根据Id获取拼团活动参加商品
	 */
	public MergeProduct getMergeProduct(Long id){
		return getById(id);
	}

   /**
	 *  删除拼团活动参加商品
	 */	
    public int deleteMergeProduct(MergeProduct mergeProduct){
    	return delete(mergeProduct);
    }

   /**
	 *  保存拼团活动参加商品
	 */		
	public Long saveMergeProduct(MergeProduct mergeProduct){
		return save(mergeProduct);
	}

   /**
	 *  更新拼团活动参加商品
	 */		
	public int updateMergeProduct(MergeProduct mergeProduct){
		return update(mergeProduct);
	}

	
	public void batchSaveMergeProduct(List<MergeProduct> mergeProductList) {
		this.save(mergeProductList);
	}

	@Override
	public List<MergeProduct> getMergeProductList(Long mergeId) {
		return queryByProperties(new EntityCriterion().eq("mergeId", mergeId));
	}

    @Override
    public void deleteMergeProductBymergeId(Long id) {
        String sql="DELETE FROM ls_merge_product WHERE merge_id=?";
        update(sql,id);
    }

}
