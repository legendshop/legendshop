/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.StoreCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.OrderUtil;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.spi.service.StoreService;
import com.legendshop.spi.service.StoreSkuService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;

/**
 * 订单常用处理工具类，
 * 公共的订单处理逻辑。
 */
@Service("orderUtil")
public class OrderUtilImpl implements OrderUtil {
	
	@Autowired
	private  StockService stockService;
	
	@Autowired
	private SkuService skuService;

	@Autowired
	private BasketService basketService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private StoreSkuService storeSkuService;
	

	/* (non-Javadoc)
	 * @see com.legendshop.business.service.order.OrderUtil#buyNow(java.lang.Boolean, java.lang.Long, java.lang.Long, java.lang.Integer)
	 */
	@Override
	public JSONObject buyNow(Boolean buyNow, Long prodId, Long skuId, Integer count){
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("result", false);
		
		Product product = productService.getProductById(prodId);
		ResultDto resultDto = null;
		if(product == null){
			resultDto= ResultDtoManager.fail(-1, "对不起,您要购买的商品不存在或已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}
		
		if (count == null || count == 0) {//默认要为1
			count = 1;
		}
		
		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			resultDto=  ResultDtoManager.fail(-1, "对不起, 您购买的商品信息异常, 不能购买!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		} 
		
		if(Constants.OFFLINE.equals(product.getStatus())){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的商品已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}
		
		if(!stockService.canOrder(product, count)){
			//数量检查
			resultDto = ResultDtoManager.fail(-1, "对不起,您购买的商品库存不足!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}
		
		//SKU数量检查
		Sku sku = skuService.getSku(skuId);
		if(!stockService.canOrder(sku, count)){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的数量已超出商品的库存量!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}
		
		ShopCartItem shopCartItem=new ShopCartItem();
		shopCartItem.setProdId(prodId);
		shopCartItem.setShopId(product.getShopId());
		shopCartItem.setSkuId(skuId);
		shopCartItem.setPrice(sku.getPrice());
		if(AppUtils.isNotBlank(sku.getPic())){
			shopCartItem.setPic(sku.getPic());
		}else{
			shopCartItem.setPic(product.getPic());
		}
		if(AppUtils.isNotBlank(sku.getName())){
			shopCartItem.setProdName(sku.getName());
		}else{
			shopCartItem.setProdName(product.getName());
		}
		shopCartItem.setPromotionPrice(sku.getPrice());
		shopCartItem.setBasketCount(count);
		shopCartItem.setStatus(1);
		shopCartItem.setStocks(sku.getStocks().intValue());
		shopCartItem.setActualStocks(sku.getActualStocks().intValue());
		shopCartItem.setStockCounting(product.getStockCounting());
		shopCartItem.setWeight(sku.getWeight());
		shopCartItem.setVolume(sku.getVolume());
		shopCartItem.setTransportId(product.getTransportId());
		shopCartItem.setTransportFree(product.getSupportTransportFree());
		shopCartItem.setTransportType(product.getTransportType());
		//shopCartItem.setEmsTransFee(product.getEmsTransFee().floatValue());
		shopCartItem.setEmsTransFee(0F);
		shopCartItem.setExpressTransFee(product.getExpressTransFee().floatValue());
		//shopCartItem.setMailTransFee(product.getMailTransFee().floatValue());
		shopCartItem.setMailTransFee(0F);
		shopCartItem.setSupportCod(product.getIsSupportCod()==0?false:true);
		shopCartItem.setIsGroup(product.getIsGroup());
		shopCartItem.setProperties(sku.getProperties());
		shopCartItem.setCnProperties(sku.getCnProperties());
		shopCartItem.setSupportStore(false);
		shopCartItem.setCheckSts(1);//立即购买默认是选中
		jsonObject.put("result", true);
		jsonObject.put("data", shopCartItem);
		
		return jsonObject;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.service.order.OrderUtil#checkSubmitParam(java.lang.String, java.lang.Integer, com.legendshop.model.constant.CartTypeEnum, com.legendshop.model.dto.OrderAddParamDto)
	 */
	@Override
	public  AddOrderMessage checkSubmitParam(String userId, Integer sessionToken, CartTypeEnum typeEnum, OrderAddParamDto paramDto){
		AddOrderMessage message=new AddOrderMessage();
		message.setResult(false);
		if(AppUtils.isBlank(userId)){
			message.setCode(SubmitOrderStatusEnum.NOT_LOGIN.value());
			return message;
		}
		//根据您的下单
		if(typeEnum==null){
			message.setCode(SubmitOrderStatusEnum.PARAM_ERR.value());
			message.setMessage("参数有误");
			return message;
		}
		Integer userToken = Integer.parseInt(paramDto.getToken()); // 取用户提交过来的token进行对比
		if(AppUtils.isBlank(sessionToken)){
			message.setCode(SubmitOrderStatusEnum.NULL_TOKEN.value());
			message.setMessage("购物信息已失效, 请刷新页面重试!");
			return message;
		}
		if (!userToken.equals(sessionToken)){
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			message.setMessage("购物信息已失效, 请刷新页面重试!");
			return message;
		}
		return  message;
	}
	
	
	
	/**
	 * remarkText：买家留言
	 * 处理买家留言
	 */
	@Override
	public  void handlerRemarkText(String remarkText, UserShopCartList userShopCartList){
		if(AppUtils.isNotBlank(remarkText)){ //商家留言
			Map<Long, String > remarkMap=new HashMap<Long, String>();
			String [] str=remarkText.split(";");// format  is shopId: remark
			for (int i = 0; i < str.length; i++) {
				if(AppUtils.isNotBlank(str[i])){
					String [] text=str[i].split(":");//格式为 shopId:留言
					if(text.length == 2 && AppUtils.isNotBlank(text[0]) && AppUtils.isNotBlank(text[1])){
						remarkMap.put(Long.valueOf(text[0]), text[1]);
					}
				}
			}
			//处理remarkMap
			if(AppUtils.isNotBlank(remarkMap)){
				List<ShopCarts> shopCarts =userShopCartList.getShopCarts();
				for (int i = 0; i < shopCarts.size(); i++) {
					if(AppUtils.isNotBlank(remarkMap.get(shopCarts.get(i).getShopId()))){
						String remark=remarkMap.get(shopCarts.get(i).getShopId());
						shopCarts.get(i).setRemark(remark);//设置买家留言
					}
				}
			}
			
		}
	}

	public static void main(String[] args) {
		String str = "4:";
		String[] split = str.split(":");
		if(AppUtils.isNotBlank(split[0]) && AppUtils.isNotBlank(split[1])){
		}
	}
	
	/**
	 * 处理物流信息
	 */
	@Override
	public  String handlerDelivery(String delivery,UserShopCartList userShopCartList){
		if(AppUtils.isNotBlank(delivery)){ //shop1:ems;shop2:mail
			String [] str=delivery.split(";");
			for (int i = 0; i < str.length; i++) {
				if(AppUtils.isNotBlank(str[i])){
					String [] text=str[i].split(":");
					if(AppUtils.isNotBlank(text[0]) && AppUtils.isNotBlank(text[1])){
						if(!FreightModeEnum.TRANSPORT_MAIL.value().equals(text[1])  && !FreightModeEnum.TRANSPORT_EXPRESS.value().equals(text[1]) && !FreightModeEnum.TRANSPORT_EMS.value().equals(text[1]))
						{
							return SubmitOrderStatusEnum.PARAM_ERR.value();
						}
						ShopCarts shopCarts=userShopCartList.getShopCarts(Long.valueOf(text[0]));
						if(shopCarts==null){
							return SubmitOrderStatusEnum.PARAM_ERR.value();
						}
						List<TransfeeDto> transfeeDtos=shopCarts.getTransfeeDtos();
						if(transfeeDtos!=null){
							for (int j = 0; j < transfeeDtos.size(); j++) {
								TransfeeDto transfeeDto=transfeeDtos.get(j);
								if(transfeeDto.getFreightMode().equals(text[1])){
									shopCarts.setCurrentSelectTransfee(transfeeDto);
									shopCarts.setFreightAmount(transfeeDto.getDeliveryAmount());
									break;
								}
							}
						}
					}
					
				}
			}
		}
		return Constants.SUCCESS;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.service.order.OrderUtil#handleCouponStr(com.legendshop.model.dto.buy.UserShopCartList)
	 */
	@Override
	public  void handleCouponStr(UserShopCartList userShopCartList){
		List<UserCoupon> userCouponList = userShopCartList.getSelCoupons(); //界面选中的优惠券和红包
		List<ShopCarts> shopCartsList = userShopCartList.getShopCarts();  //商家订单列表，一个商家一个ShopCarts
		
		if(AppUtils.isNotBlank(shopCartsList)){
			//1. 红包和优惠券按比例分
			double totalRedpackOffPrice = 0d;//分配出去的红包总额
			for (ShopCarts shopCarts : shopCartsList) {
				double couponOffPrice = 0d;
				double redpackOffPrice = 0d;
				if(AppUtils.isNotBlank(userCouponList)){
					for (UserCoupon userCoupon : userCouponList) {
						//优惠券
						if(CouponProviderEnum.SHOP.value().equals(userCoupon.getCouponProvider())){
							if(shopCarts.getShopId().equals(userCoupon.getShopId())){
								couponOffPrice = Arith.add(couponOffPrice, userCoupon.getOffPrice());
							}
						}
						//红包
						else if(CouponProviderEnum.PLATFORM.value().equals(userCoupon.getCouponProvider())){
							redpackOffPrice=Arith.round(Arith.add(shopCarts.getTotalRedpackOffPrice(),redpackOffPrice),2);
//							if(CouponTypeEnum.COMMON.value().equals(userCoupon.getCouponType())){
//								//全平台通用红包，，按订单金额比例  分配红包的金额,修改前
//								double offPrice = Arith.round(Arith.mul(userCoupon.getOffPrice(), Arith.div(shopCarts.getShopTotalCash(),userShopCartList.getOrderTotalCash())),2);
//								
//								//修改后逻辑
//								//double offPrice = Arith.round(Arith.mul(userCoupon.getOffPrice(), Arith.div(shopCarts.getCalculatedCouponAmount(),userShopCartList.getOrderTotalCash())),2);
//								redpackOffPrice = Arith.add(redpackOffPrice, offPrice);
//							}else if(CouponTypeEnum.SHOP.value().equals(userCoupon.getCouponType())){
//								//店铺红包，，按参与红包的 所有店铺订单 金额比例  分配红包的金额
//								if(userCoupon.getShopIds().contains(shopCarts.getShopId())){
//									//修改前
//									double offPrice = Arith.round(Arith.mul(userCoupon.getOffPrice(), Arith.div(shopCarts.getShopTotalCash(),userCoupon.getHitTotalPrice())),2);
//									
//									//修改后逻辑
//									//double offPrice = Arith.round(Arith.mul(userCoupon.getOffPrice(), Arith.div(shopCarts.getCalculatedCouponAmount(),userCoupon.getHitTotalPrice())),2);
//									redpackOffPrice = Arith.add(redpackOffPrice, offPrice);
//								}
//							}
						}
					}
				}
				shopCarts.setCouponOffPrice(couponOffPrice);
				shopCarts.setRedpackOffPrice(redpackOffPrice);
				totalRedpackOffPrice = Arith.add(totalRedpackOffPrice, redpackOffPrice);
			}
			
			//2. 补差额: 针对红包分配后，总额差 会出现0.01差别问题
			double atualTotalRedpackPrice = 0d;//实际红包总额
			if(AppUtils.isNotBlank(userCouponList)){
				//获得实际红包总额
				for (UserCoupon userCoupon : userCouponList) {
					if(CouponProviderEnum.PLATFORM.value().equals(userCoupon.getCouponProvider())){
						atualTotalRedpackPrice = Arith.add(atualTotalRedpackPrice, userCoupon.getOffPrice());
					}
				}
				
				//判断实际总红包金额 和 分配出去的红包总额 是否相等，如果不相等，需要 补差额
				if(Double.compare(totalRedpackOffPrice, atualTotalRedpackPrice)!=0){
					//得到差额
					double diffPrice = Arith.sub(atualTotalRedpackPrice,totalRedpackOffPrice);
					//找到一个红包金额不为0的订单，补差额
					for (ShopCarts shopCarts : shopCartsList) {
						if(shopCarts.getRedpackOffPrice()>0){
							shopCarts.setRedpackOffPrice(Arith.add(shopCarts.getRedpackOffPrice(), diffPrice));
							break;
						}
					}
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.service.order.OrderUtil#verifySafetyStock(com.legendshop.model.dto.buy.UserShopCartList)
	 */
	@Override
	public  String verifySafetyStock(UserShopCartList userShopCartList) {
		String result=Constants.SUCCESS;
		List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
		for (int i = 0; i < shopCarts.size(); i++) {
			ShopCarts carts = shopCarts.get(i);
			int count=carts.getCartItems().size();
			for (int j = 0; j < count; j++) {
				ShopCartItem basket = carts.getCartItems().get(j);
				if (basket != null) {
					Integer stocks = stockService.getStocksByMode(basket.getProdId(),basket.getSkuId());
					// 产品库存
					if (stocks == null) {
						stocks = 0;
					}
					if (stocks - basket.getBasketCount() < 0) {
						result="";
						result= basket.getProdName()+" 库存不足，只剩 "+stocks+" 件,请重新选择商品件数";
						i=count;
						break;
					}
				}
			}
		}
		return  result;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.business.service.order.OrderUtil#handelOrderStocks(com.legendshop.model.dto.buy.UserShopCartList)
	 */
	@Override
	public String handelOrderStocks(UserShopCartList userShopCartList) {
		Integer cityId=userShopCartList.getCityId();
		if(AppUtils.isBlank(cityId)){
			return "参数错误";
		}
		List<ShopCarts> shopCarts=userShopCartList.getShopCarts();
		if(AppUtils.isBlank(shopCarts)){
			return "参数错误";
    	}
		String result = Constants.SUCCESS;
		for (int i = 0; i < shopCarts.size(); i++) {
			List<ShopCartItem> shopCartItems = shopCarts.get(i).getCartItems();
			for (int j = 0; j < shopCartItems.size(); j++) {
				ShopCartItem shopCartItem = shopCartItems.get(j);
				Map<String,String> map = stockService.calculateSkuStocksByCity(shopCartItem.getProdId(), shopCartItem.getSkuId(), cityId, shopCartItem.getTransportId(), shopCartItem.getShopId());
				if(map != null){
					Long stocks = Long.valueOf(map.get("stocks"));
					shopCartItem.setStocks(stocks.intValue());
					if(stocks<shopCartItem.getBasketCount()){
						result = shopCartItem.getProdName()+" 库存不足，只剩 "+stocks+" 件,请重新选择商品件数";
					}
				}else{
					result = shopCartItem.getProdName()+" 库存不足，只剩 "+0+" 件,请重新选择商品件数";
				}
			}
		}
		return result;
	}

	@Override
	public String checkCartParams(Product product, Long skuId,Integer count, String userId){
		
		if(product == null){
			return "亲, 该商品库存不足哦, 看看其他的吧!";
		}
		
		//检查商品是否有所属店铺
		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			return "亲, 该商品不存在或已被删除了哦, 看看其他的吧！";
		} 
		
		//先检查商品的总库存
		if(product.getStocks()- count< 0 ){
			return "亲, 该商品库存不足哦, 看看其他的吧!";
		}
		
		//检查商品是否已经下线
		if(!ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())){
			return "亲, 该商品已经下架了哦, 看看其他的吧！";
		}
		Sku sku = skuService.getSku(skuId);
		if(AppUtils.isBlank(sku)){
			return "亲, 该商品不存在或已被删除了哦, 看看其他的吧！";
		}
		
		//检查单品是否已经下线
		if(!Constants.ONLINE.equals(sku.getStatus())){
			return "亲, 该商品已经下架了哦, 看看其他的吧！";
		}
	
		//先检查单品商品的总库存
		if(sku.getStocks()- count< 0 ){
			return "亲, 该商品库存不足哦, 看看其他的吧!";
		}
		if(AppUtils.isNotBlank(userId)){
			Basket basket = basketService.getBasketByUserId(product.getProdId(), userId, skuId);
			if(AppUtils.isNotBlank(basket)){
				if(sku.getStocks()-(basket.getBasketCount()+count)<0){
					return "亲,目前商品库存不足, 购买数不能超过" + sku.getStocks() + "件哦!";
				}
			}
		}
		
		//TODO 检查商品的商城状态
		
		return Constants.SUCCESS;
	}
	
	/**
	 * 检查门店商品是否可以购买
	 */
	@Override
	public String checkStoreCartParams(Product product, Long skuId, Integer count, String userId, Long storeId) {
		
		
		//TODO 检查商品所属门店状态
		Store store  = storeService.getStore(storeId);
		if (AppUtils.isBlank(store) || !store.getIsDisable()) {
			return "该门店不存在或刚被下线!";
		}
		
		
		if(product == null){
			return "亲, 该商品不存在或刚被删除, 看看其他的吧!";
		}
		
		//检查商品是否有所属店铺
		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			return "亲, 该商品不存在或已被删除了哦, 看看其他的吧！";
		} 
		
		//检查商品是否已经下线
		if(!ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())){
			return "亲, 该商品已经下架了哦, 看看其他的吧！";
		}
		Sku sku = skuService.getSku(skuId);
		if(AppUtils.isBlank(sku)){
			return "亲, 该商品不存在或已被删除了哦, 看看其他的吧！";
		}
		
		//检查单品是否已经下线
		if(!Constants.ONLINE.equals(sku.getStatus())){
			return "亲, 该商品已经下架了哦, 看看其他的吧！";
		}
		
		StoreSku storeSku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
		//检查门店的sku是否存在
		if (AppUtils.isBlank(storeSku)) {
			return "亲, 该门店商品不存在或已被删除了哦, 看看其他的吧！";
		}
		//检查门店的sku库存
		if(storeSku.getStock() - count < 0 ){
			return "亲, 该门店库存不足哦, 选择其他门店看看吧!";
		}
		if(AppUtils.isNotBlank(userId)){
			Basket basket = basketService.getBasketByUserId(product.getProdId(), userId, skuId);
			if(AppUtils.isNotBlank(basket)){
				if(storeSku.getStock()-(basket.getBasketCount()+count)<0){
					return "亲,目前门店商品库存不足, 购买数不能超过" + storeSku.getStock() + "件哦!";
				}
			}
		}
		
		return Constants.SUCCESS;
	}
	
	
	
	/**
	 * 处理门店提货人名称
	 */
	@Override
	public void handlerStoreBuyerName(String buyerNameText, UserShopCartList userShopCartList) {
		
		if(AppUtils.isNotBlank(buyerNameText)){ //提货人名称
			Map<Long, String > buyerNameMap = new HashMap<Long, String>();
			String [] str = buyerNameText.split(";");// format  is shopId: remark
			for (int i = 0; i < str.length; i++) {
				if(AppUtils.isNotBlank(str[i])){
					String [] text = str[i].split(":");//格式为 storeId:提货人名称
					if(AppUtils.isNotBlank(text[0]) && AppUtils.isNotBlank(text[1])){
						buyerNameMap.put(Long.valueOf(text[0]), text[1]);
					}
				}
			}
			//处理buyerNameMap
			if(AppUtils.isNotBlank(buyerNameMap)){
				ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
				
				List<StoreCarts> storeCartList = shopCarts.getStoreCarts();
				
				
				for (int i = 0; i < storeCartList.size(); i++) {
					if(AppUtils.isNotBlank(buyerNameMap.get(storeCartList.get(i).getStoreId()))){
						String buyerName = buyerNameMap.get(storeCartList.get(i).getStoreId());
						storeCartList.get(i).setBuyerName(buyerName);//设置提货人
					}
				}
			}
			
		}
		
	}

	
	/**
	 * 处理门店提货人手机号码
	 */
	@Override
	public void handlerStoreTelPhone(String telPhoneText, UserShopCartList userShopCartList) {
		
		if(AppUtils.isNotBlank(telPhoneText)){ //提货人名称
			Map<Long, String > telPhoneMap = new HashMap<Long, String>();
			String [] str = telPhoneText.split(";");// format  is shopId: remark
			for (int i = 0; i < str.length; i++) {
				if(AppUtils.isNotBlank(str[i])){
					String [] text = str[i].split(":");//格式为 storeId:提货人名称
					if(AppUtils.isNotBlank(text[0]) && AppUtils.isNotBlank(text[1])){
						telPhoneMap.put(Long.valueOf(text[0]), text[1]);
					}
				}
			}
			//处理buyerNameMap
			if(AppUtils.isNotBlank(telPhoneMap)){
				ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
				
				List<StoreCarts> storeCartList = shopCarts.getStoreCarts();
				
				
				for (int i = 0; i < storeCartList.size(); i++) {
					if(AppUtils.isNotBlank(telPhoneMap.get(storeCartList.get(i).getStoreId()))){
						String telPhone = telPhoneMap.get(storeCartList.get(i).getStoreId());
						storeCartList.get(i).setTelphone(telPhone);;//设置提货电话
					}
				}
			}
			
		}
		
	}

	/**
	 * 处理门店买家留言
	 */
	@Override
	public void handlerStoreRemarkText(String remarkText, UserShopCartList userShopCartList) {
		
		if(AppUtils.isNotBlank(remarkText)){ //提货人名称
			Map<Long, String > remarkMap = new HashMap<Long, String>();
			String [] str = remarkText.split(";");// format  is shopId: remark
			for (int i = 0; i < str.length; i++) {
				if(AppUtils.isNotBlank(str[i])){
					String [] text = str[i].split(":");//格式为 storeId:提货人名称
					if(AppUtils.isNotBlank(text[0]) && AppUtils.isNotBlank(text[1])){
						remarkMap.put(Long.valueOf(text[0]), text[1]);
					}
				}
			}
			//处理buyerNameMap
			if(AppUtils.isNotBlank(remarkMap)){
				ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
				
				List<StoreCarts> storeCartList = shopCarts.getStoreCarts();
				
				
				for (int i = 0; i < storeCartList.size(); i++) {
					if(AppUtils.isNotBlank(remarkMap.get(storeCartList.get(i).getStoreId()))){
						String remark = remarkMap.get(storeCartList.get(i).getStoreId());
						storeCartList.get(i).setRemark(remark);//设置买家留言
					}
				}
			}
			
		}
		
	}

	/**
	 * 门店商品立即购买
	 */
	@Override
	public JSONObject storeBuyNow(Boolean isBuyNow, Long prodId, Long skuId, Integer count, Long storeId) {
		
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("result", false);
		
		Product product = productService.getProductById(prodId);
		ResultDto resultDto = null;
		if(product == null){
			resultDto= ResultDtoManager.fail(-1, "对不起,您要购买的商品不存在或已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","对不起,您要购买的商品不存在或已下架!");
			return jsonObject;
		}
		
		Store store  = storeService.getStore(storeId);
		if (AppUtils.isBlank(store) || !store.getIsDisable()) {
			
			resultDto= ResultDtoManager.fail(-1, "该门店不存在或刚被下线!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","该门店不存在或刚被下线");
			return jsonObject;
		}
		
		if (count == null || count == 0) {//默认要为1
			count = 1;
		}
		
		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			resultDto=  ResultDtoManager.fail(-1, "对不起, 您购买的商品信息异常, 不能购买!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","对不起, 您购买的商品信息异常, 不能购买!");
			return jsonObject;
		} 
		
		if(Constants.OFFLINE.equals(product.getStatus())){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的商品已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","对不起, 您要购买的商品已下架!");
			return jsonObject;
		}
		
		if(!stockService.canOrder(product, count)){
			//数量检查
			resultDto = ResultDtoManager.fail(-1, "对不起,您购买的商品库存不足!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","对不起,您购买的商品库存不足!");
			return jsonObject;
		}
		
		//SKU数量检查
		Sku sku = skuService.getSku(skuId);
		if(!stockService.canOrder(sku, count)){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的数量已超出商品的库存量!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","对不起, 您要购买的数量已超出商品的库存量!");
			return jsonObject;
		}
		
		StoreSku storeSku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
		//检查门店的sku是否存在
		if (AppUtils.isBlank(storeSku)) {
			resultDto = ResultDtoManager.fail(-1, "亲, 该门店商品不存在或已被删除了哦, 看看其他的吧！");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","亲, 该门店商品不存在或已被删除了哦, 看看其他的吧！");
			return jsonObject;
		}
		//检查门店的sku库存
		if(storeSku.getStock() - count < 0 ){
			resultDto = ResultDtoManager.fail(-1, "亲, 该门店库存不足哦, 选择其他门店看看吧!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			jsonObject.put("msg","亲, 该门店库存不足哦, 选择其他门店看看吧!");
			return jsonObject;
		}
		
		ShopCartItem shopCartItem=new ShopCartItem();
		shopCartItem.setProdId(prodId);
		shopCartItem.setShopId(product.getShopId());
		shopCartItem.setSkuId(skuId);
		shopCartItem.setPrice(sku.getPrice());
		if(AppUtils.isNotBlank(sku.getPic())){
			shopCartItem.setPic(sku.getPic());
		}else{
			shopCartItem.setPic(product.getPic());
		}
		if(AppUtils.isNotBlank(sku.getName())){
			shopCartItem.setProdName(sku.getName());
		}else{
			shopCartItem.setProdName(product.getName());
		}
		shopCartItem.setPromotionPrice(sku.getPrice());
		shopCartItem.setBasketCount(count);
		shopCartItem.setStatus(1);
		shopCartItem.setStocks(sku.getStocks().intValue());
		shopCartItem.setActualStocks(sku.getActualStocks().intValue());
		shopCartItem.setStockCounting(product.getStockCounting());
		shopCartItem.setWeight(sku.getWeight());
		shopCartItem.setVolume(sku.getVolume());
		shopCartItem.setTransportId(product.getTransportId());
		shopCartItem.setTransportFree(product.getSupportTransportFree());
		shopCartItem.setTransportType(product.getTransportType());
		shopCartItem.setEmsTransFee(0F);
		shopCartItem.setExpressTransFee(product.getExpressTransFee().floatValue());
		shopCartItem.setMailTransFee(0F);
		shopCartItem.setSupportCod(product.getIsSupportCod()==0?false:true);
		shopCartItem.setIsGroup(product.getIsGroup());
		shopCartItem.setProperties(sku.getProperties());
		shopCartItem.setCnProperties(sku.getCnProperties());
		shopCartItem.setSupportStore(true);
		shopCartItem.setCheckSts(1);//立即购买默认是选中
		shopCartItem.setStoreId(storeId);
		shopCartItem.setStoreName(store.getName());
		shopCartItem.setStoreAddr(store.getShopAddr());
		
		jsonObject.put("result", true);
		jsonObject.put("data", shopCartItem);
		
		return jsonObject;
		
	}
}
