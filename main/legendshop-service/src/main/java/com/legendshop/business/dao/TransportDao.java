package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.Transport;

public interface TransportDao extends GenericDao<Transport, Long> {

	public abstract List<Transport> getTransports(Long shopId);

	public abstract Transport getTransport(Long id);
	
    public abstract void deleteTransport(Transport transport);
	
	public abstract Long saveTransport(Transport transport);
	
	public abstract void updateTransport(Transport transport);
	
	public abstract PageSupport getTransport(CriteriaQuery cq);
	
	 public abstract List<Area> getCtyName(Long ctyId);
	 
	 public Transport getDetailedTransport(Long shopId,Long id);
	 
	 public abstract void deleteTransport(Long id,Long shopId);
 
	 public boolean isAppTransport(Long id);

	public abstract PageSupport<Transport> getFreightChargesTemp(String curPageNO, Long shopId, String transName);

	public abstract Integer checkByName(String name,Long shopId);
}
