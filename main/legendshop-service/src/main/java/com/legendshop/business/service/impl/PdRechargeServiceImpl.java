/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.PdRechargeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.spi.service.PdRechargeService;
import com.legendshop.util.AppUtils;

/**
 * 预存款充值表服务.
 */
@Service("pdRechargeService")
public class PdRechargeServiceImpl implements PdRechargeService {
	
	@Autowired
	private PdRechargeDao pdRechargeDao;
	
	@Autowired
	private PdCashLogDao pdCashLogDao;

	public PdRecharge getPdRecharge(Long id) {
		return pdRechargeDao.getPdRecharge(id);
	}

	public void deletePdRecharge(PdRecharge pdRecharge) {
		pdRechargeDao.deletePdRecharge(pdRecharge);
	}

	public Long savePdRecharge(PdRecharge pdRecharge) {
		if (!AppUtils.isBlank(pdRecharge.getId())) {
			updatePdRecharge(pdRecharge);
			return pdRecharge.getId();
		}
		return pdRechargeDao.savePdRecharge(pdRecharge);
	}

	public void updatePdRecharge(PdRecharge pdRecharge) {
		pdRechargeDao.updatePdRecharge(pdRecharge);
	}

	@Override
	public int deleteRechargeDetail(String userId, String pdrSn) {
		return pdRechargeDao.deleteRechargeDetail(userId, pdrSn);
	}

	@Override
	public PdRecharge findRechargePay(String userId, String paySn, Integer status) {
		return pdRechargeDao.findRechargePay(userId, paySn, status);
	}

	@Override
	public PdRecharge findRechargePay(String paySn) {
		return pdRechargeDao.findRechargePay(paySn);
	}

	@Override
	public PdRecharge getPdRecharge(Long id, String userId) {
		return pdCashLogDao.getPdRecharge(id, userId);
	}

	@Override
	public PageSupport<PdRecharge> getRechargeDetailedPage(String curPageNO, String userId, String state) {
		return pdRechargeDao.getRechargeDetailedPage(curPageNO, userId, state);
	}

	@Override
	public PageSupport<PdRecharge> getPdRechargeListPage(String curPageNO, PdRecharge pdRecharge, Integer pageSize, DataSortResult result) {
		return pdRechargeDao.getPdRechargeListPage(curPageNO, pdRecharge, pageSize, result);
	}

	@Override
	public Integer batchDelete(String ids) {
		return pdRechargeDao.batchDelete(ids);
	}

	/**
	 * 充值明细列表
	 * @param userId 用户Id
	 * @param state 支付状态 0未支付1支付
	 * @param pdrSn 唯一的流水号
	 * @param curPageNO 当前页
	 * @param pageSize 每页大小
	 *
	 */
	@Override
	public PageSupport<PdRecharge> getPdRechargePage(String userId, String state, String pdrSn,String curPageNO, int pageSize) {
		return pdRechargeDao.getPdRechargePage(userId, state, pdrSn, curPageNO, pageSize);
	}

}
