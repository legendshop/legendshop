/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.store;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.StoreOrderDao;
import com.legendshop.business.order.service.impl.AbstractAddOrder;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.PayMannerEnum;
import com.legendshop.model.constant.StockCountingEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.StoreCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.spi.resolver.order.IOrderCartResolver;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.util.AppUtils;

/**
 * 门店订单.
 */
@Service("orderShopStoreCartResolver")
public class OrderShopStoreCartResolver extends AbstractAddOrder implements IOrderCartResolver {

	@Autowired
	private StoreOrderDao storeOrderDao;
	
	@Autowired
	private StoreProdService storeProdService;

	/**
	 * 查找订单
	 */
	@Override
	public UserShopCartList queryOrders(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Long> ids = (List<Long>) params.get("ids");
		String userId = (String) params.get("userId");
		String userName = (String) params.get("userName");
		Long shopId = (Long) params.get("shopId");
		
		//拿到购物车对应的商品信息
		List<ShopCartItem> shopCartItems = null;
		if(AppUtils.isNotBlank(params.get("buyNow")) && (boolean)params.get("buyNow")){
			shopCartItems = new ArrayList<ShopCartItem>(1);
			ShopCartItem shopCartItem=  (ShopCartItem) params.get("shopCartItem");
			shopCartItems.add(shopCartItem);
		}else{
			
			shopCartItems = basketService.loadBasketByIds(ids, userId, shopId);
			//处理不支持门店自提的商品项 添加标记
			for (ShopCartItem items : shopCartItems) {
				
				Long prodId = items.getProdId();
				Long skuId = items.getSkuId();
				Integer count = items.getBasketCount();
				Long storeId = items.getStoreId();
				
				Integer stocks = storeProdService.getStocks(storeId, prodId, skuId);
				if (stocks == null || stocks - count < 0) {//stocks为空，表示该门店没有这个商品，小于0表示该商品没有库存
					items.setStoreIsExist(false);//所选门店没有这个商品
				}
			}
		}
		
		if(AppUtils.isBlank(shopCartItems)){
			return null;
		}
		
		// 门店下单可能不需要
		UserShopCartList userShopCartList = super.queryOrders(userId, userName, shopCartItems, null,CartTypeEnum.SHOP_STORE.toCode());
		if (userShopCartList == null) {
			return null;
		}
		userShopCartList.setType(CartTypeEnum.SHOP_STORE.toCode());

		return userShopCartList;
	}

	/**
	 * 下单操作
	 * 
	 * @param
	 * @return
	 */
	@Override
    public List<String> addOrder(UserShopCartList userShopCartList) {
		
		ShopCarts shopCart = userShopCartList.getShopCarts().get(0);
		if (AppUtils.isBlank(shopCart)) {
			 throw new BusinessException("购物车信息错误！请重新下单", ErrorCodes.BUSINESS_ERROR);
		}
		
		List<String> subNembers = super.submitStoreOrder(userShopCartList);
		if (AppUtils.isBlank(subNembers)) {
			return null;
		}
		
		List<StoreCarts> storeCats = shopCart.getStoreCarts();//门店购物车分组
		
		PayMannerEnum payMannerEnum = PayMannerEnum.fromCode(userShopCartList.getPayManner());
		List<Long> removeBaskets = new ArrayList<Long>();
		for (Iterator<StoreCarts> storeIterator = storeCats.iterator(); storeIterator.hasNext();) {
			StoreCarts store = storeIterator.next();
		
			List<ShopCartItem> shopCartItems = store.getCartItems();
			for (ShopCartItem shopCartItem : shopCartItems) {
				if(StockCountingEnum.STOCK_BY_ORDER.value().equals(shopCartItem.getStockCounting())){
					int result = storeOrderDao.addHold(shopCartItem.getStoreId(), shopCartItem.getProdId(), shopCartItem.getSkuId(), shopCartItem.getBasketCount());
					if (result == 0) {
						throw new BusinessException(shopCartItem.getProdName() + "库存不足,请重新选择商品件数", ErrorCodes.BUSINESS_ERROR);
					}
					removeBaskets.add(shopCartItem.getBasketId()); // 删除购物车
				}
			}
			StoreOrder order = new StoreOrder();
			// 组装门店订单提交信息
			order.setOrderSn(store.getSubNumber());
			order.setShopId(shopCart.getShopId());
			order.setStoreId(store.getStoreId());
			order.setUserId(userShopCartList.getUserId());
			order.setReciverName(store.getBuyerName());
			order.setReciverMobile(store.getTelphone());
			String dlyoPickupCode = CommonServiceUtil.getRandomString(8);
			order.setDlyoPickupCode(dlyoPickupCode);
			if (PayMannerEnum.ONLINE_PAY.value().equals(payMannerEnum.value())) {
				order.setPayType(PayMannerEnum.ONLINE_PAY.value());
				order.setDlyoSatus(0); // 未支付
			} else if (PayMannerEnum.SHOP_STORE_PAY.value().equals(payMannerEnum.value())) {
				order.setPayType(PayMannerEnum.SHOP_STORE_PAY.value());
				order.setDlyoSatus(1); // 等待自提支付
			}
			order.setDlyoAddtime(new Date());
			// 保存门店订单
			storeOrderDao.saveDeliveryOrder(order);

		}
		/** 清除购物车商品 **/
		basketService.deleteById(removeBaskets);
		userShopCartList = null;
		return subNembers;
	}

	public void setStoreOrderDao(StoreOrderDao storeOrderDao) {
		this.storeOrderDao = storeOrderDao;
	}
}






