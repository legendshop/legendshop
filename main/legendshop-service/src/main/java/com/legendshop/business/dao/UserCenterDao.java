package com.legendshop.business.dao;

import com.legendshop.model.entity.User;

/**
 * @author george
 * 
 */
public interface UserCenterDao{

	
	User getUser(String userName);
}
