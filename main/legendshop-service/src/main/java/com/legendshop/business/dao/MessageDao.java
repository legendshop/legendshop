/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Msg;
import com.legendshop.model.entity.MsgStatus;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;

/**
 * 站内信Dao.
 */
public interface MessageDao extends GenericDao<Msg, Long>{

	/**
	 * 删除 发件箱  站内信
	 * @param msgId
	 */
	public abstract void deleteOutbox(Long msgId);
	
	/**
	 * 删除 收件箱  站内信 
	 * @param msgId
	 */
	public void deleteInbox(Long msgId,String userName);
	
	/**
	 * 删除 收件箱  站内信 
	 * @param msgIds
	 * @return TODO
	 */
	public int deleteInboxByMsgIds(String[] msgIds,String userName);
	
	/**
	 * 查找消息状态
	 * @param userName
	 * @param msgId
	 * @return
	 */
	public MsgStatus getSystemMsgStatus(String userName, Long msgId);
	
	/**
	 * 查找出邮件的内容
	 * @param msgId
	 * @return
	 */
	public SiteInformation getMsgText(Long msgId);
	
	/**
	 * 将邮件设置为已读
	 * @param userName
	 */
	public void updateMsgRead(String userName,Long msgId);
	
	/**
	 * 通过id获取系统通知
	 * @param msgId
	 * @return
	 */
	@Deprecated  //need to check
	public SiteInformation getSystemMessages(Long msgId);

	/**
	 * 保存系统通知
	 * @param siteInformation
	 * @param userGradeArray
	 */
	public void  saveSystemMessages(SiteInformation siteInformation,Integer[] userGradeArray);
	
	/**
	 * 查取textId
	 * @param msgId
	 * @return
	 */
	public Long getTextId(Long msgId);
	
	/**
	 * 更新内容
	 * @param textId
	 */
	public void updateMsgText(String text,String title,Long textId);
	
	/**
	 *删除之前选择的用户等级 
	 * @param msgId
	 */
	public void deleteUserGrade(Long msgId);
	
	/**
	 * 重新载入新选择的用户等级
	 * @param userGradeArray
	 * @param msgId
	 */
	public void saveUserGrade(Integer[] userGradeArray,Long msgId);
	
	/**
	 * 更新ls_msg 的用户等级
	 * @param userGradeArray
	 * @param msgId
	 */
	public void updateMsgLevel(Integer[] userGradeArray,Long msgId);
	
	/**
	 * 通过用户名查找出用户等级
	 * @param userName
	 * @return
	 */
	public Integer getGradeId(String userName);
	
	/**
	 * 将ls_msg_status中的状态更新为1
	 * @param userName
	 * @param msgId
	 */
	public void updateSystemMsgStatus(String userName,Long msgId, int status);

	/**
	 * 将系统通知 lm_msg 中的status 改为-1
	 * @param msgId
	 */
	public void deleteSystemMsgStatus(Long msgId);
	
	/**
	 * 验证接受人是否存在
	 */
	public boolean isReceiverExist(String receiveName);

	public abstract boolean checkGlobalMsg(String userName, Long msgId);

	public abstract Integer calUnreadMsgCount(String userName);

	public abstract Integer calUnreadMsgCount(String userName, Integer gradeId, Date userRegtime);
	
	public abstract Msg saveMsg(Msg msg);

	public abstract void clearInboxMessage(String userName);

	public abstract int getCalUnreadSystemMessagesCount(Integer gradeId, String userName, Date userRegtime);

	public abstract SiteInformation getNewSystemMessages(String userName, Integer gradeId, Date userRegtime);

	public abstract Long getCountById(String userName);

	PageSupport<SiteInformation> inboxSiteInformation(String userName, String curPageNO);

	PageSupport<SiteInformation> outboxSiteInformation(String userName, String curPageNO);

	PageSupport<SiteInformation> systemInformation(String userName, Integer gradeId, String curPageNO, UserDetail userDetail);

	public abstract PageSupport<SiteInformation> querySiteInformation(String curPageNO, String title, Integer pageSize);
	
	public abstract PageSupport<SiteInformation> querySimplePage(String curPageNO, String userName);

	public abstract PageSupport<SiteInformation> queryMessage(String curPageNO, String userName);

	public abstract PageSupport<SiteInformation> querySimplePage(String curPageNO, Integer gradeId, String userName, UserDetail userDetail);

	public abstract int getCalUnreadSystemMessagesCount(Integer gradeId, Date userRegtime);

	Integer getAuditShop();

	Integer getProductConsultCount();

	Integer getAuditProductCount();

	Integer getAuditBrandCount();

	Integer getReturnGoodsCount();

	Integer getReturnMoneyCount();

	Integer getAccusationCount();

	Integer getShopBillCount();

	Integer getShopBillConfirmCount();

	Integer getAuctionCount();

	Integer getProductcommentCount();

	Integer getSeckillActivityCount();

	Integer getGroupCount();

	Integer getPresellProdCount();

	Integer getUserCommisCount();

	Integer getUserFeedBackCount();

	public abstract void clearUnreadMsgCount(String userName);

	public abstract int batchDelete(String msgIds); 
	
	/**
	 * 获取消息箱未读消息数
	 */
	public abstract Integer getUnReadMsgNum(String userName);

	PageSupport<SiteInformation> queryShopMessage(String curPageNO, String userName);
}
