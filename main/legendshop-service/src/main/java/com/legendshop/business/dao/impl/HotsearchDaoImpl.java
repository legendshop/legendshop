/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.HotsearchDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Hotsearch;
import com.legendshop.util.AppUtils;

/**
 * 热门搜索Dao
 */
@Repository
public class HotsearchDaoImpl extends GenericDaoImpl<Hotsearch, Long> implements HotsearchDao {

    @Autowired
    private SystemParameterUtil systemParameterUtil;
	
    /**
     * The log.
     */
    private final Logger log = LoggerFactory.getLogger(HotsearchDaoImpl.class);

    @Override
    @Caching(evict = {@CacheEvict(value = "Hotsearch", key = "#id"), @CacheEvict(value = "HotsearchList", allEntries = true)})
    public void deleteHotsearchById(Long id) {
        deleteById(id);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "Hotsearch", key = "#hotsearch.id"), @CacheEvict(value = "HotsearchList", allEntries = true)})
    public void updateHotsearch(Hotsearch hotsearch) {
        update(hotsearch);
    }

    @Override
    @Cacheable(value = "HotsearchList")
    public List<Hotsearch> getHotsearch() {
        return this.queryByProperties(new EntityCriterion().eq("status", 1).addAscOrder("seq"));
    }

    @Override
    public PageSupport<Hotsearch> queryHotsearch(DataSortResult result, String curPageNO, Hotsearch hotsearch) {
        QueryMap map = new QueryMap();
        if(AppUtils.isNotBlank(hotsearch.getTitle())){
        	map.like("title", hotsearch.getTitle().trim());
        }
        SimpleSqlQuery query = new SimpleSqlQuery(Hotsearch.class,systemParameterUtil.getPageSize(),curPageNO);
        if (!result.isSortExternal()) {//支持外部排序
        }
        String queryAllSQL = ConfigCode.getInstance().getCode("prod.queryHotSearchCount", map);
        String querySQL = ConfigCode.getInstance().getCode("prod.queryHotSearch", map);
        query.setAllCountString(queryAllSQL);
        query.setQueryString(querySQL);
        query.setParam(map.toArray());
        map.remove(AttributeKeys.ORDER_INDICATOR);//去掉排序的条件
        return querySimplePage(query);
    }

}
