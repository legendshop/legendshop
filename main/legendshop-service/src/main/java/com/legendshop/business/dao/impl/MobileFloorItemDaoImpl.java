/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.MobileFloorItemDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloorItem;
import com.legendshop.util.AppUtils;

/**
 * 手机端html5 楼层内容关联服务Dao
 */
@Repository
public class MobileFloorItemDaoImpl extends GenericDaoImpl<MobileFloorItem, Long> implements MobileFloorItemDao  {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public MobileFloorItem getMobileFloorItem(Long id){
		return getById(id);
	}
	
    public int deleteMobileFloorItem(MobileFloorItem mobileFloorItem){
    	return delete(mobileFloorItem);
    }
	
	public Long saveMobileFloorItem(MobileFloorItem mobileFloorItem){
		return save(mobileFloorItem);
	}
	
	public int updateMobileFloorItem(MobileFloorItem mobileFloorItem){
		return update(mobileFloorItem);
	}

	@Override
	public List<MobileFloorItem> queryMobileFloorItem(Long parentId) {
		return queryByProperties(new EntityCriterion().eq("parentId", parentId).addAscOrder("seq"));
	}

	@Override
	public List<MobileFloorItem> queryFloorItemByIds(List<Long> floorIds) {
		
		StringBuffer sb = new StringBuffer("select mf.id as id, mf.parent_id as parentId, mf.seq as seq,mf.status as status,mf.title as title,mf.link_path as linkPath,mf.pic as pic from ls_m_floor_item mf where mf.parent_id in(");
		for(int i=0;i<floorIds.size();i++){
			sb.append("?,");
		}
		sb.setLength(sb.length()-1);
		sb.append(") order by mf.seq asc");
		
		return query(sb.toString(), MobileFloorItem.class, floorIds.toArray());
	}

	@Override
	public Long getCountMobileFloorItem(Long parentId) {
		String sql = "select count(1) from ls_m_floor_item where parent_id=?";
		return getLongResult(sql, parentId);
	}

	@Override
	public PageSupport<MobileFloorItem> getMobileFloorItem(String curPageNO, String title, Long id) {
		CriteriaQuery cq = new CriteriaQuery(MobileFloorItem.class, curPageNO);
        cq.setPageSize(systemParameterUtil.getPageSize());
        if(AppUtils.isNotBlank(title)){
        	cq.like("title", "%"+title+"%");
        }
        cq.eq("parentId", id);
        cq.addAscOrder("seq");
		return queryPage(cq);
	}
	
 }
