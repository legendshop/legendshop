/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.manager.impl;

import java.io.File;
import java.io.IOException;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.helper.FileProcessor;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.spi.manager.BusinessFileManager;
import com.legendshop.spi.service.FileParameterService;

/**
 * 文件读写器
 */
@Component
public class BusinessFileManagerImpl implements BusinessFileManager {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private FileParameterService fileParameterService;


	@Override
	public String readFile(String parentFilePath, String fileName) throws IOException {
		File file = new File(systemParameterUtil.getDownloadFilePath() + parentFilePath + "/" + fileName);
		String content = FileProcessor.readFile(file, true);
		return content;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.manager.BusinessFileManager#readRegItem()
	 */
	@Override
	public String readRegItem() throws IOException{
		// TODO 将文件转入数据库，还未测试
//		File file = new File(propertiesUtil.getDownloadFilePath() + REGISTER_FILE_PATH + "/" + REG_ITEM);
//		String content = FileProcessor.readFile(file, true);
		String text = fileParameterService.findByName(REG_ITEM).getValue();
		return text;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.manager.BusinessFileManager#readResetPassMail()
	 */
	@Override
	public String readResetPassMail() throws IOException{
		// TODO 将文件转入数据库，还未测试
//		File file = new File(propertiesUtil.getDownloadFilePath() + MAIL_FILE_PATH + "/" + VALIDATION_MAIL);
//		String content = FileProcessor.readFile(file, true);
		String text = fileParameterService.findByName(VALIDATION_MAIL).getValue();
		return text;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.manager.BusinessFileManager#readRegistersuccess()
	 */
	@Override
	public String readRegistersuccess() throws IOException{

		// TODO 将文件转入数据库，还未测试
//		File file = new File(propertiesUtil.getDownloadFilePath() + MAIL_FILE_PATH + "/" + REGISTER_SUCCESS);
//		String content = FileProcessor.readFile(file, true);
		String text = fileParameterService.findByName(REGISTER_SUCCESS).getValue();

		return text;
	}



}
