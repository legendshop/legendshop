/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.PassportUpdate;
import com.legendshop.business.dao.PassportDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.dto.IdPassord;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.entity.Passport;

/**
 * The Class PassportDaoImpl.
 */
@Repository
public class PassportDaoImpl extends GenericDaoImpl<Passport, Long> implements PassportDao  {

	private static final String SELECT_PASSPORT_FULL_SQL = "SELECT sub.id, sub.passport_id AS passportId, sub.open_id AS openId, sub.source, sub.create_time AS createTime,sub.access_token as accessToken, "
			+ "passport.user_id AS userId, passport.user_name AS userName, passport.unionid, "
			+ "passport.nick_name AS nickName,"
			+ "passport.head_portrait_url AS headPortraitUrl, passport.gender, "
			+ "passport.country, passport.province, passport.city, passport.props, passport.type "
			+ "FROM ls_passport_sub sub, ls_passport passport "
			+ "WHERE sub.passport_id = passport.pass_port_id ";

    public List<Passport> getPassport(String userId){
   		return this.queryByProperties(new EntityCriterion().eq("userId", userId));
    }

	public Passport getPassport(Long id){
		return getById(id);
	}

	@PassportUpdate
    public int deletePassport(Passport passport){
    	return delete(passport);
    }

    /**
     * 保存第三方凭证
     */
	public Long savePassport(Passport passport){
		//加密Passport
		encryptPassport(passport);
		return save(passport);
	}

	@PassportUpdate
	public int updatePassport(Passport passport){
		return update(passport);
	}

	@Override
	@PassportUpdate
	public int updateLastLoginTime(Passport passport) {
		return update("update ls_passport set access_token = ?,props = ?, last_login_time = ? where open_id = ? and type = ?",
				passport.getAccessToken(), passport.getProps(), new Date(),
				passport.getOpenId(), passport.getType());
	}

	//绑定用户
	@Override
	@PassportUpdate
	public int bindingUser(Passport passport) {
		return update("update ls_passport set last_login_time = ?, binding_user_id = ?, current_user_id = ? where open_id = ? and type = ?",
				 new Date(),
				 passport.getBindingUserId(),
				 passport.getCurrentUserId(),
				passport.getOpenId(),
				passport.getType());
	}


	/**
	 * #passport.type + '.' + #passport.openId
	 */
	@Override
	@Cacheable(value = "Passport", key = "#type + '.' + #openId")
	public Passport getUserIdByOpenId(String type, String openId) {
		String encryptPassport=encryptPassport(openId);
		return getByProperties(new EntityCriterion().eq("type", type).eq("openId", encryptPassport));
	}

	@Override
	public Passport getUserIdByOpenIdForUpdate(String type, String openId) {
		String encryptPassport=encryptPassport(openId);
		return get(" SELECT * from ls_passport p where p.type=? and p.open_id=?  FOR UPDATE ", Passport.class, type, encryptPassport) ;
	}

	/**
	 * 取消openId的加密
	 * @param openId
	 * @return
	 */
	private String encryptPassport(String openId){
		return openId;
	}

	/**
	 * 取消openId的加密
	 */
	private void encryptPassport(Passport passport){

	}

	/**
	 * 检查账号是否已经绑定
	 */
	@Override
	public boolean isAccountBind(String userName, String type) {
		return get(" select count(*) from ls_passport p, ls_user u where p.user_id = u.id and p.type = ? and u.name = ?", Long.class, type, userName) > 0;
	}



	@Override
	public List<Passport> getPassport(String userId, String type) {
		return query("select * from ls_passport where user_id = ? and type = ?", Passport.class, userId, type);
	}


	/**
	 *  返回用户,用于校验 检查用户名密码是否正确
	 *
	 */
	@Override
	public IdPassord validateMobilePassword(String mobile, String password) {
		IdPassord idPassord = get("SELECT u.id, u.name, u.password FROM ls_user u, ls_usr_detail ud WHERE u.id = ud.user_id AND ud.user_mobile = ?", IdPassord.class, mobile);
		return idPassord;
	}


	@Override
	public Passport getPassportByBindingUserId(String userId) {
		return this.getByProperties(new EntityCriterion().eq("bindingUserId", userId));
	}

	@Override
	public PassportFull getPassportByOpenId(String openid) {
		String sql = SELECT_PASSPORT_FULL_SQL + " AND sub.open_id = ? ";
		return this.get(sql, PassportFull.class, openid);
	}

	@Override
	public Passport getPassportByUnionid(String unionid) {

		return this.getByProperties(new EntityCriterion().eq("unionid", unionid));
	}

	@Override
	public PassportFull getPassportFullById(Long passportId) {
		String sql = SELECT_PASSPORT_FULL_SQL + " AND sub.id = ? ";
		return this.get(sql, PassportFull.class, passportId);
	}

	@Override
	public int bindUser(Long passPortId, String userId, String userName) {
		String sql = "UPDATE ls_passport SET user_id = ? , user_name = ? WHERE pass_port_id = ?";
		return this.update(sql, userId, userName, passPortId);
	}

	@Override
	public PassportFull getPassportFullByUserIdAndTypeAndSource(String userId, String type, String source) {
		String sql = SELECT_PASSPORT_FULL_SQL + " AND passport.user_id = ? AND passport.type = ? AND sub.source = ? ";
		return this.get(sql, PassportFull.class, userId, type, source);
	}
}
