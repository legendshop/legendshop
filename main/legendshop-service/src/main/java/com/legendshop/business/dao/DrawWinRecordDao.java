/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawWinRecord;

/**
 * 中奖纪录Dao.
 */

public interface DrawWinRecordDao extends Dao<DrawWinRecord, Long> {
     
    public abstract List<DrawWinRecord> getDrawWinRecordsByActId(Long  actId);
    
    public abstract List<DrawWinRecord> getDrawWinRecords(String userID, Long actId);

	public abstract DrawWinRecord getDrawWinRecord(Long id);
	
    public abstract int deleteDrawWinRecord(DrawWinRecord drawWinRecord);
	
	public abstract Long saveDrawWinRecord(DrawWinRecord drawWinRecord);
	
	public abstract int updateDrawWinRecord(DrawWinRecord drawWinRecord);

	public abstract int delDrawWinRecordsByActId(Long actId);

	public abstract PageSupport<DrawWinRecord> queryDrawWinRecordPage(String curPageNO, DrawWinRecord drawWinRecord);
	
 }
