/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.business.dao.FloorSubItemDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.FloorSubItem;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Product;
import com.legendshop.util.AppUtils;

/**
 * 楼层元素Dao
 *
 */
@Repository
public class FloorItemDaoImpl extends GenericDaoImpl<FloorItem, Long> implements FloorItemDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private FloorSubItemDao floorSubItemDao;
	
	private static String getUserSortFloorItemSQL3 = "select f.fl_id as floorId, fi.fi_id as fiId, c.name as categoryName, fi.seq as seq, fi.refer_id as referId, fi.sort_level as sortLevel from ls_floor f, ls_floor_item fi,ls_category c " +
			"where fi.refer_id = c.id and f.fl_id = fi.floor_id  and fi.sort_level = 1 and f.fl_id=? and f.layout=? order by fi.seq ";
	
	private static String getUserSortFloorItemSQL4 = "select  f.fl_id as floorId, fi.fi_id as fiId,c.name as categoryName,fi.seq as seq ,fi.refer_id as referId ,fi.sort_level as sortLevel from  ls_floor f,ls_floor_item fi,ls_category c " +
			"where fi.refer_id = c.id and  f.fl_id = fi.floor_id and fi.sort_level = 2 and f.fl_id=? and f.layout=? order by fi.seq ";
	
	

	private static String sqlForGetFloorItem = "select  f.fi_id as fiId,c.name as categoryName,f.seq as seq from ls_floor_item f,ls_category c "
			+ "where f.refer_id = c.id and  f.type='ST' and f.sort_level = 1 and f.refer_id = ?";

	private static String sqlForGetAllSortFloorItem1 = "select  f.fi_id as fiId,s.name as sortName,f.seq as seq ,f.refer_id as referId ,f.sort_level as sortLevel from ls_floor_item f,ls_category s "
			+ "where f.refer_id = s.id and f.type = 'ST' and f.sort_level = 1 and f.floor_id = ?";
	
    private static String sqlForGetAllSortFloorItem2 = "select  f.fi_id as fiId,s.name as sortName,f.seq as seq ,f.refer_id as referId ,f.sort_level as sortLevel from ls_floor_item f,ls_category s "
			+ "where f.refer_id = s.id and f.type = 'ST' and f.sort_level = 2 and f.floor_id = ?";
   
    private static String sqlForGetBrandList = "select f.fi_id as fiId, f.refer_id as referId,b.brand_name as brandName,b.brand_pic as pic "
			+ "from ls_floor_item f, ls_brand b where  f.refer_id = b.brand_id and f.layout = ? and  f.floor_id = ? order by f.fi_id ";
    		
	public List<FloorItem> getFloorItem(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}

	public FloorItem getFloorItem(Long id) {
		FloorItem floorItem = get(sqlForGetFloorItem, FloorItem.class, id);
		return floorItem;
	}

	/**
	 * 删除楼层下的所有一级item
	 */
	@CacheEvict(value = "FloorList", allEntries=true)
	public void deleteFloorItem(String type, Long floorId) {
		update("delete from ls_floor_item where type=? and floor_id = ?", type, floorId);
	}
	
	@CacheEvict(value = "FloorList", allEntries=true)
	public Long saveFloorItem(FloorItem floorItem) {
		return (Long) save(floorItem);
	}

	@CacheEvict(value = "FloorList", allEntries=true)
	public void updateFloorItem(FloorItem floorItem) {
		update(floorItem);
	}

	public PageSupport getFloorItem(CriteriaQuery cq) {
		return queryPage(cq);
	}

	/**
	 * 装载楼层下的商品分类子楼层
	 */
	@Override
	public List<FloorItem> getAllSortFloorItem(Long flId) {
		List<FloorItem> floorItemList = new ArrayList<FloorItem>();
		List<FloorItem> floorItemList1 =query(sqlForGetAllSortFloorItem1, FloorItem.class, flId);
		List<FloorItem> floorItemList2 = query(sqlForGetAllSortFloorItem2, FloorItem.class, flId);
		floorItemList.addAll(floorItemList1);
		floorItemList.addAll(floorItemList2);
		return floorItemList;
	}

	
	/**
	 * 装载用户的商品分类子楼层
	 */
	@Override
	public List<FloorItem> getUserSortFloorItem(Long FloorId,String layout) {
		List<FloorItem> floorItemList = new ArrayList<FloorItem>();
		List<FloorItem> floorItemList1 = query(getUserSortFloorItemSQL3, FloorItem.class, new Object[]{FloorId,layout});
		List<FloorItem> floorItemList2 = query(getUserSortFloorItemSQL4, FloorItem.class, new Object[]{FloorId,layout});
		floorItemList.addAll(floorItemList1);
		floorItemList.addAll(floorItemList2);
		if (AppUtils.isNotBlank(floorItemList)) {
			// 4. 装载商品子楼层item
			List<FloorSubItem> floorSubItemList = floorSubItemDao.getUserFloorSubItem();
			// 装配好楼层项目和子项目

			for (FloorItem floorItem : floorItemList) {
				for (FloorSubItem fSI : floorSubItemList) {
					floorItem.addFloorSubItem(fSI);
				}
			}
		}
		return floorItemList;
	}

	@Override
	public void deleteFloorSubItems(Long fiId) {
		update("delete from ls_floor_sub_item where fi_id = ?", fiId);
	}

	@Override
	public List<FloorItem> getBrandList(Long floorId,String layout,int limit) {
		return queryLimit(sqlForGetBrandList, FloorItem.class, 0, limit, new Object[]{layout,floorId});
	}

	@Override
	public List<FloorItem> getRankList(Long floorId) {
		List<FloorItem> rankList =  query("select f.refer_id as referId,p.name as sortName,p.pic as pic,p.price as price from ls_floor_item f, ls_prod p where f.type = 'PT' and f.refer_id = p.prod_id and f.floor_id = ?",
						FloorItem.class, floorId);
		return rankList;
	}

	@Override
	public FloorItem getFloorGroup(Long floorId) {
		FloorItem group = get("select f.refer_id as referId,p.name as sortName,p.pic as pic,p.brief as brief,p.cash as cash,p.price as price from ls_floor_item f, ls_prod p where f.type = 'GP' and f.refer_id = p.prod_id and f.floor_id = ?",
						FloorItem.class, floorId);
		return group;
	}

	@Override
	public List<FloorItem> getNewsList(Long floorId) {
		List<FloorItem> newsList =  query("select f.refer_id as referId,p.news_title as sortName from ls_floor_item f, ls_news p where f.type = 'NS' and f.refer_id = p.news_id and f.floor_id = ? ",
						FloorItem.class, floorId);
		return newsList;
	}

	public void setFloorSubItemDao(FloorSubItemDao floorSubItemDao) {
		this.floorSubItemDao = floorSubItemDao;
	}

	@Override
	public List<FloorItem> getFloorItemST(Long flId) {
		//return findByHQL("from FloorItem where floor_id = ? and type = 'ST' ", flId);
		return queryByProperties(new EntityCriterion().eq("floorId", flId).eq("type", "ST"));
	}

	@Override
	public void deleteFloorItem(Long FlId) {
		update("delete from ls_floor_item where floor_id = ? ", FlId);
	}

	@Override
	public FloorItem getAdvFloorItem(Long flId,Long fiId,String type) {
		return get("select adv.pic_url as picUrl , adv.link_url as linkUrl ,adv.title as title ,adv.source_input as sourceInput ,f.fi_id as fiId ,f.floor_id as floorId ,f.refer_id as referId ,f.type as type from ls_floor_item f ,ls_adv adv where f.refer_id = adv.id and f.floor_id = ? and f.fi_id = ? and f.type = ?",FloorItem.class,flId,fiId,type);
	}

	@Override
	public List<FloorItem> queryAdvFloorItem(Long flId, String type,String layout) {
		return query("select adv.pic_url as picUrl , adv.link_url as linkUrl ,adv.title as title ,adv.source_input as sourceInput ,f.fi_id as fiId ,f.floor_id as floorId ,f.refer_id as referId ,f.type as type from ls_floor_item f ,ls_adv adv where f.refer_id = adv.id and f.floor_id = ? and f.type = ? and f.layout=? ",FloorItem.class,flId,type,layout);
	}

	@Override
	public FloorItem getAdvFloorItem(Long flId, String type,String layout) {
		List<FloorItem> list= queryLimit("select adv.pic_url as picUrl , adv.link_url as linkUrl ,adv.title as title ,adv.source_input as sourceInput ,f.fi_id as fiId ,f.floor_id as floorId ,f.refer_id as referId ,f.type as type from ls_floor_item f ,ls_adv adv where f.refer_id = adv.id and f.floor_id = ? and f.type = ? and f.layout=? ", FloorItem.class, 0, 1, flId,type,layout);
	    if(AppUtils.isNotBlank(list)){
	    	return list.get(0);
	    }
		return null;
	}

	@Override
	@CacheEvict(value = "FloorList", allEntries=true)
	public void updateFloorItemForAdv(FloorItem floorAdv) {
		update("update ls_floor_item set attachment_id=?,pic=?,refer_id=? where fi_id=? ", new Object[]{floorAdv.getAttachmentId(),floorAdv.getPic(),floorAdv.getReferId(),floorAdv.getId()});
	}

	@Override
	public PageSupport<Brand> getBrandListPage(String curPageNO, String brandName) {
		SimpleSqlQuery query = new SimpleSqlQuery(Brand.class, 10, curPageNO);
   		QueryMap map = new QueryMap();
   		if(AppUtils.isNotBlank(brandName)){
   			map.like("brandName", brandName);
   		}
   		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getFloorBrandCount", map);
   		 String querySQL = ConfigCode.getInstance().getCode("biz.getFloorBrand", map); 
   		 query.setAllCountString(queryAllSQL);
   		 query.setQueryString(querySQL);
   		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<Product> getGroupListPage(String curPageNO, String groupName) {
		SimpleSqlQuery query = new SimpleSqlQuery(Product.class, systemParameterUtil.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		if(AppUtils.isNotBlank(groupName)){
			map.like("groupName", groupName);
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getFloorGroupCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("biz.getFloorGroup", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<FloorItem> queryAdvFloorItemPage(String curPageNO, Long flId, String layout, String title) {
		SimpleSqlQuery query = new SimpleSqlQuery(FloorItem.class, systemParameterUtil.getPageSize(), curPageNO);
		 QueryMap map = new QueryMap();
		 map.put("floorId",flId);
		 map.put("type", FloorTemplateEnum.RIGHT_ADV.value());
		 map.put("layout", layout);
		 if(AppUtils.isNotBlank(title)){
			map.like("title", title);
		 }
		
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.queryAdvListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("biz.queryAdvList", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<News> getNewsListPage(String curPageNO, Long newsCategoryId, String newsTitle,String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(News.class, 6, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName",userName );
		map.put("newsCategoryId", newsCategoryId);
		if(AppUtils.isNotBlank(newsTitle)){
			map.like("newsTitle", newsTitle);
		}
		String queryAllSQL = ConfigCode.getInstance().getCode("biz.getNewsFloorCount", map);
		 String querySQL = ConfigCode.getInstance().getCode("biz.getNewsFloor", map); 
		 query.setAllCountString(queryAllSQL);
		 query.setQueryString(querySQL);
		 query.setCurPage(curPageNO);
		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
