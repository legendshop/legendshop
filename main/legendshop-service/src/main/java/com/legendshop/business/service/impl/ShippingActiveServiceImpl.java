/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ShippingActiveDao;
import com.legendshop.business.dao.ShippingProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ShippingTypeEnum;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.spi.service.ShippingActiveService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 包邮活动Service
 */
@Service("shippingActiveService")
public class ShippingActiveServiceImpl  implements ShippingActiveService{

	@Autowired
	private ShippingActiveDao shippingActiveDao;

	@Autowired
	private ShippingProdDao shippingProdDao;

	@Override
	public ShippingActive getShippingActive(Long id) {
		return shippingActiveDao.getShippingActive(id);
	}


	@Override
	public void deleteShippingActive(ShippingActive shippingActive) {
		shippingActiveDao.deleteShippingActive(shippingActive);
		//如果是商品包邮活动
		if(shippingActive.getFullType().equals(ShippingTypeEnum.PROD.value())) {
			shippingProdDao.deleteShippingProdByShippingId(shippingActive.getId(), shippingActive.getShopId());
		}
	}

	@Override
	public Long saveShippingActive(ShippingActive shippingActive) {
		if (!AppUtils.isBlank(shippingActive.getId())) {
			updateShippingActive(shippingActive);
			return shippingActive.getId();
		}
		return shippingActiveDao.saveShippingActive(shippingActive);
	}

	@Override
	public void updateShippingActive(ShippingActive shippingActive) {
		shippingActiveDao.updateShippingActive(shippingActive);
	}

	@Override
	public PageSupport<ShippingActive> queryShippingActiveList(String curPageNO, Long shopId, ShippingActive shippingActive) {
		return shippingActiveDao.queryShippingActiveList(curPageNO, shopId, shippingActive);
	}

	@Override
	public ShippingActive getShippingActiveById(Long activeId, Long shopId) {
		return shippingActiveDao.getShippingActiveById(activeId, shopId);
	}

	//保存商品满减活动和活动商品
	@Override
	public void saveActiveAndProd(ShippingActive active, Long[] prodIds) {
		Long id = shippingActiveDao.saveShippingActive(active);
		if(AppUtils.isNotBlank(id)){
			List<ShippingProd> lists = new ArrayList<ShippingProd>();
			for(int i=0;i<prodIds.length;i++){
				ShippingProd activeProd = new ShippingProd();
				activeProd.setShopId(active.getShopId());
				activeProd.setProdId(prodIds[i]);
				activeProd.setShippingId(id);
				lists.add(activeProd);
			}
			shippingProdDao.batchShippingProd(lists);
		}

	}

	@Override
	public void updateShippingActiveByProd(ShippingActive active, String[] prodIds) {
		shippingActiveDao.updateShippingActive(active);
		if(AppUtils.isNotBlank(prodIds) && prodIds.length>0){
			List<ShippingProd> lists = new ArrayList<ShippingProd>();

			for(int i=0;i<prodIds.length;i++){
				ShippingProd activeProd = new ShippingProd();
				activeProd.setShopId(active.getShopId());
				activeProd.setProdId(Long.valueOf(prodIds[i]));
				activeProd.setShippingId(active.getId());
				lists.add(activeProd);
			}
			shippingProdDao.batchShippingProd(lists);
		}

	}

	@Override
//	@Cacheable(value = "ShopMailfeeStr", key = "#shopId + #prodId ")
	public String queryShopMailfeeStr(Long shopId, Long prodId) {

		List<ShippingActive> activeList =  shippingActiveDao.queryOnLineShippingActive(shopId,prodId);

		if (AppUtils.isNotBlank(activeList)) { //当前活动最多仅会存在针对商品的两个活动 一个是商品一个是店铺
			List<ShippingActive> prodlist = activeList.stream().filter((e) -> e.getFullType() == ShippingTypeEnum.PROD.value()).collect(Collectors.toList());
			List<ShippingActive> shoplist = activeList.stream().filter((e) -> e.getFullType() == ShippingTypeEnum.SHOP.value()).collect(Collectors.toList());

			ShippingActive shopActive = null;
			String mailfeeStr = null;
			if(AppUtils.isNotBlank(prodlist)){
				
				shopActive = prodlist.get(0);
				if(shopActive.getReduceType() == ShippingTypeEnum.FULL_MONEY.value()){//满金额包邮
					mailfeeStr = shopActive.getName()+", 满" + Arith.round(shopActive.getFullValue(), 2) + "元包邮";
				}else if(shopActive.getReduceType() == ShippingTypeEnum.FULL_PIECE.value()){
					mailfeeStr = shopActive.getName()+", 满" + shopActive.getFullValue().intValue() + "件包邮";
				}

			}else if(AppUtils.isNotBlank(shoplist)){
				shopActive = shoplist.get(0);
				if(shopActive.getReduceType() == ShippingTypeEnum.FULL_MONEY.value()){//满金额包邮
					mailfeeStr = shopActive.getName()+", 满" + Arith.round(shopActive.getFullValue(), 2) + "元包邮";
				}else if(shopActive.getReduceType() == ShippingTypeEnum.FULL_PIECE.value()){
					mailfeeStr = shopActive.getName()+", 满" + shopActive.getFullValue().intValue() + "件包邮";
				}
			}
			return mailfeeStr;
		}
		return null;
	}

	/**
	 * app商家端 修改包邮活动
	 * @param id
	 * @param name
	 * @param prodId
	 */
	@Override
	public int appUpdateShipping(Long id, String name , Long [] prodId) {
		ShippingActive shippingActive = shippingActiveDao.getShippingActive(id);
		if (AppUtils.isBlank(shippingActive)){
			return -1;
		}
		shippingActive.setName(name);

		if(shippingActive.getFullType().equals(1)){
			if(AppUtils.isBlank(prodId) || prodId.length <= 0 ){
				return -1;
			}
			// 部分商品 2020-08-19 update by jzh: 与PC端一致
//			shippingProdDao.deleteShippingProdByShippingId(shippingActive.getId(),shippingActive.getShopId());
			List<ShippingProd> lists = new ArrayList<>();
			for(int i=0;i<prodId.length;i++){
				ShippingProd activeProd = new ShippingProd();
				activeProd.setShopId(shippingActive.getShopId());
				activeProd.setProdId(prodId[i]);
				activeProd.setShippingId(shippingActive.getId());
				lists.add(activeProd);
			}
			shippingProdDao.batchShippingProd(lists);
		}
		// 全部商品
		shippingActiveDao.updateShippingActive(shippingActive);
		return 1;
	}
}
