/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopRole;

/**
 *商家角色表Dao
 */
public interface ShopRoleDao extends Dao<ShopRole, Long> {
     
	public abstract ShopRole getShopRole(Long id);
	
    public abstract int deleteShopRole(ShopRole shopRole);
	
	public abstract Long saveShopRole(ShopRole shopRole);
	
	public abstract int updateShopRole(ShopRole shopRole);
	
	public abstract ShopRole getShopRole(Long shopId, String name);

	public abstract List<ShopRole> getShopRolesByShopId(Long shopId);

	public abstract PageSupport<ShopRole> getShopRolePage(String curPageNO, Long shopId);
	
 }
