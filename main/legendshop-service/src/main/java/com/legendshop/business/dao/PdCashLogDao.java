/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;

/**
 * 预存款变更日志表Dao.
 */
public interface PdCashLogDao extends Dao<PdCashLog, Long> {
     
	public abstract PdCashLog getPdCashLog(Long id);
	
    public abstract int deletePdCashLog(PdCashLog pdCashLog);
	
	public abstract Long savePdCashLog(PdCashLog pdCashLog);
	
	public abstract int updatePdCashLog(PdCashLog pdCashLog);
	
	public abstract PdRecharge getPdRecharge(Long id, String userId);

	public abstract PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, String userId, String userName);

	public abstract PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, PdCashLog pdCashLog, Integer pageSize, DataSortResult result);

	public abstract PageSupport<PdCashLog> getPdCashLogPage(String userId, String curPageNO);

	public abstract PageSupport<PdCashLog> getPdCashLog(String curPageNO, String uesrId, String logType, int pageSize);

	public abstract PageSupport<PdCashLog> getPdCashLog(String curPageNO,PdCashLog pdCashLog, int pageSize);

	public abstract PageSupport<PdCashLog> getPdCashLog(String curPageNO, int pageSize, String userId, String logType);

	public abstract PageSupport<PdCashLog> getPresentRecord(String curPageNO, int pageSize, String userId, String logType);

	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	public abstract PageSupport<PdCashLog> getPdCashLogPageByType(String userId, Integer state, String curPageNO);

 }
