/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserCenterPersonInfoDao;
import com.legendshop.model.entity.MyPersonInfo;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.UserCenterPersonInfoService;


/**
 * The Class UserCenterServiceImpl.
 */
@Service("userCenterPersonInfoService")
public class UserCenterPersonInfoServiceImpl implements UserCenterPersonInfoService {
	
	@Autowired
	private UserCenterPersonInfoDao userCenterPersonInfoDao;

	public MyPersonInfo getUserCenterPersonInfo(String userId) {
		return userCenterPersonInfoDao.getPersonInfo(userId);
	}
	
	public void updateUserInfo(MyPersonInfo myPersonInfo) {
		userCenterPersonInfoDao.updatePersonInfo(myPersonInfo);
	}

	public boolean verifySMSCode(String userName, String validateCode) {
		return userCenterPersonInfoDao.verifySMSCode(userName, validateCode);
		
	}
	
	/**
	 *  更新支付密码和支付强度
	 */
	public void updatePaymentPassword(String userName, String paymentPassword, Integer payStrength) {
		userCenterPersonInfoDao.updatePaymentPassword(userName, paymentPassword, payStrength);
	}

	/**
	 * 查询用户等级
	 */
	public UserGrade getUserGrade(String userId) {
		return userCenterPersonInfoDao.getUserGrade(userId);
	}
	
	/**
	 * 查询用户的下一个等级
	 */
	public String getNextGradeName(Integer gradeId) {
		return userCenterPersonInfoDao.getNextGradeName(gradeId);
	}

}
