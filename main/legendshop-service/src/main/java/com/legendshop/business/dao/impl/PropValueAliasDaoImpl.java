/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PropValueAliasDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.PropValueAlias;

/**
 *商品属性值别名Dao
 */
@Repository
public class PropValueAliasDaoImpl extends GenericDaoImpl<PropValueAlias, Long> implements PropValueAliasDao  {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(PropValueAliasDaoImpl.class);

	public PropValueAlias getPropValueAlias(Long id){
		return getById(id);
	}
	
    public int deletePropValueAlias(PropValueAlias propValueAlias){
    	return delete(propValueAlias);
    }
	
	public Long savePropValueAlias(PropValueAlias propValueAlias){
		return save(propValueAlias);
	}
	
	public int updatePropValueAlias(PropValueAlias propValueAlias){
		return update(propValueAlias);
	}

	@Override
	public List<PropValueAlias> queryByProdId(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}
	
	@CacheEvict(value = "PropValueAliasList", key = "#prodId")
	public void clearPropValueAliasCache(Long prodId){
		log.debug("clear PropValueAlias cache by prodId {}", prodId);
	}

	@Override
	public List<PropValueAlias> queryByProdIdNoCache(Long prodId) {
		return queryByProperties(new EntityCriterion().eq("prodId", prodId));
	}
	
 }
