/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.BrandDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.dto.Select2Dto;
import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 
 *品牌服务
 */
@Service("brandService")
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandDao brandDao;
	
	@Autowired
	private CategoryManagerUtil categoryManagerUtil;


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.BrandService#list(java.lang.String)
	 */
	@Override
	public List<Brand> getBrand(String userName) {
		return brandDao.getBrandByUserName(userName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.BrandService#load(java.lang.Long)
	 */
	@Override
	@Cacheable(value = "Brand", key = "#id")
	public Brand getBrand(Long id) {
		return brandDao.getBrandById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.business.service.BrandService#delete(java.lang.Long)
	 */
	@Override
	@CacheEvict(value = "Brand", key = "#brandId")
	public void delete(Long brandId) {
		brandDao.deleteBrandById(brandId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.BrandService#save(com.legendshop.model
	 * .entity.Brand)
	 */
	@Override
	public Long save(Brand brand) {
		if (!AppUtils.isBlank(brand.getBrandId())) {
			update(brand);
			return brand.getBrandId();
		}
		return (Long) brandDao.save(brand);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.business.service.BrandService#update(com.legendshop.model
	 * .entity.Brand)
	 */
	@Override
	@CacheEvict(value = "Brand", key = "#brand.brandId")
	public void update(Brand brand) {
		brandDao.updateBrand(brand);
	}
	
	
	@Override
	public String saveBrandItem(List<String> idList, Long nsortId, String userName) {

		return brandDao.saveBrandItem(idList, nsortId, userName);

	}

	@Override
	public String saveBrandItem(String idJson, String nameJson, Long nsortId, String userName) {
		List<String> idList = JSONUtil.getArray(idJson, String.class);
		return brandDao.saveBrandItem(idList, nsortId, userName);
	}


	@Override
	public boolean hasChildProduct(Long brandId) {
		return brandDao.hasChildProduct(brandId);
	}

	@Override
	public List<Brand> getAllCommentBrand() {
		return brandDao.getAllCommentBrand();
	}

	@Override
	public List<Brand> getBrandBySortId(Long sortId) {
		return brandDao.getBrandBySortId(sortId);
	}

	@Override
	public List<Brand> getMoreBrandList(Long sortId) {
		return brandDao.getMoreBrandList(sortId);
	}

	@Override
	public List<Brand> getBrandList(Long prodTypeId) {
		return brandDao.getBrandList(prodTypeId);
	}

	@Override
	public List<Brand> queryAllBrand() {
		return brandDao.queryAllBrand();
	}

	@Override
	public List<Brand> getAvailableBrands() {
		return brandDao.getAvailableBrands();
	}

	@Override
	public List<Brand> getBrandsByName(String name) {
		return  brandDao.getBrandsByName(name);
	}

	@Override
	public List<Brand> getBrandByIds(List<Long> brandIds) {
		return brandDao.queryAllByIds(brandIds);
	}

	@Override
	public List<Brand> getAllBrand() {
		return brandDao.getAllBrand();
	}

	@Override
	public PageSupport<Brand> queryBrands(String curPageNO, Integer ststus) {
		return brandDao.queryBrands(curPageNO, ststus);
	}
	
	@Override
	public PageSupport<Brand> brand(String curPageNO) {
		return brandDao.brand(curPageNO);
	}

	@Override
	public PageSupport<Brand> getBrandsPage(String curPageNO, String brandName) {
		return brandDao.getBrandsPage(curPageNO,brandName);
	}

	@Override
	public PageSupport<Brand> getBrand(String curPageNO, String brandName, Long proTypeId) {
		return brandDao.getDataByCriteriaQueryPage(curPageNO,brandName,proTypeId);
	}

	@Override
	public PageSupport<Brand> getBrandsPage() {
		return brandDao.getBrandsPage();
	}

	@Override
	public PageSupport<Brand> getBrand(String userName, String brandName) {
		return brandDao.getDataByCriteriaQuery(userName,brandName);
	}

	@Override
	public PageSupport<Brand> getDataByPage(String curPageNO, String userName) {
		return brandDao.getDataByPage(curPageNO,userName);
	}
	
	@Override
	public PageSupport<Brand> getDataByPageByUserId(String curPageNO, String userId) {
		return brandDao.getDataByPageByUserId(curPageNO,userId);
	}

	@Override
	public PageSupport<Brand> getDataByBrandName(String brandName,String userName) {
		return brandDao.getDataByBrandName(brandName,userName);
	}

	@Override
	public PageSupport<Brand> queryBrandListPage(String curPageNO, Brand brand, DataSortResult result) {
		return brandDao.queryBrandListPage(curPageNO,brand,result);
	}

	@Override
	public PageSupport<Brand> getBrandsByNamePage(String brandName) {
		return brandDao.getBrandsByNamePage(brandName);
	}

	@Override
	public boolean checkBrandByName(String brandName,Long brandId) {
		return brandDao.checkBrandByName(brandName,brandId);
	}

	@Override
	public int batchDel(String ids) {
		return brandDao.batchDel(ids);
	}

	@Override
	public int batchOffOrOnLine(String ids,int brandStatus) {
		return brandDao.batchOffOrOnLine(ids,brandStatus);
	}

	@Override
	public List<BrandDto> likeBrandName(String brandName,Long categoryId) {
		Integer typeId = categoryManagerUtil.getRelationTypeId(categoryId);
		return brandDao.likeBrandName(brandName,typeId);
	}

	@Override
	public List<Select2Dto> getBrandSelect2(String brandName, Integer currPage, Integer pageSize) {
		int index = (currPage - 1) * pageSize;
		String sql="SELECT b.brand_id AS id, b.brand_name AS text  FROM ls_brand b WHERE b.status=1";
		if(AppUtils.isNotBlank(brandName)){
			sql = sql + "AND b.brand_name LIKE ?";
			return brandDao.queryLimit(sql, Select2Dto.class, index, pageSize,"%" + brandName + "%");
		}else{
			return brandDao.queryLimit(sql, Select2Dto.class, index, pageSize);
		}
	}

	@Override
	public int getBrandSelect2Count(String brandName) {
		String sql="SELECT COUNT(*) FROM ls_brand b WHERE b.status=1";
		if(AppUtils.isNotBlank(brandName)){
			sql = sql + "AND b.brand_name LIKE ?";
			return (int)brandDao.getLongResult(sql,"%" + brandName + "%");
		}else{
			return (int)brandDao.getLongResult(sql);
		}
	}

	@Override
	public boolean checkBrandByNameByShopId(String brandName, Long brandId, Long shopId) {
		return brandDao.checkBrandByNameByShopId(brandName, brandId,shopId);
	}

	@Override
	public List<Brand> getExportBrands(Brand brand) {
		return brandDao.getExportBrands(brand);
	}
}
