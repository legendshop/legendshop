/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ShopDomain;

/**
 * 店铺域名Dao
 */
public interface ShopDomainDao extends Dao<ShopDomain, Long> {
     
	public abstract ShopDomain getShopDomain(Long id);
	
    public abstract int deleteShopDomain(ShopDomain shopDomain);
	
	public abstract Long saveShopDomain(ShopDomain shopDomain);
	
	public abstract int updateShopDomain(ShopDomain shopDomain);
	
 }
