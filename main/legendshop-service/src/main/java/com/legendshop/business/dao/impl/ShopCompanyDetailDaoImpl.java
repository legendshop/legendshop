/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopCompanyDetailDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ShopCompanyDetail;

/**
 *公司详细信息Dao
 */
@Repository
public class ShopCompanyDetailDaoImpl extends GenericDaoImpl<ShopCompanyDetail, Long> implements ShopCompanyDetailDao { 

	public ShopCompanyDetail getShopCompanyDetail(Long id){
		return getById(id);
	}
	
    public int deleteShopCompanyDetail(ShopCompanyDetail shopCompanyDetail){
    	return delete(shopCompanyDetail);
    }
	
	public Long saveShopCompanyDetail(ShopCompanyDetail shopCompanyDetail){
		return save(shopCompanyDetail);
	}
	
	public int updateShopCompanyDetail(ShopCompanyDetail shopCompanyDetail){
		return update(shopCompanyDetail);
	}

	@Override
	public ShopCompanyDetail getShopCompanyDetailByShopId(Long shopId) {
		return  this.getByProperties(new EntityCriterion().eq("shopId", shopId));
	}
	
 }
