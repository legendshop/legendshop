/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.predeposit;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.predeposit.PdCashHoldingDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.UserPredepositDto;
import com.legendshop.model.entity.PdCashHolding;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.vo.CoinVo;
import com.legendshop.spi.service.CoinLogService;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.spi.service.PdRechargeService;
import com.legendshop.spi.service.PdWithdrawCashService;
import com.legendshop.spi.service.UserCenterPredepositService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 用户中心预存款服务实现类
 *
 */
@Service("userCenterPredepositService")
public class UserCenterPredepositServiceImpl implements UserCenterPredepositService {

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PdCashLogService pdCashLogService;

	@Autowired
	private PdWithdrawCashService pdWithdrawCashService;

	@Autowired
	private PdRechargeService pdRechargeService;

	@Autowired
	private PdCashHoldingDao pdCashHoldingDao;

	@Autowired
	private CoinLogService coinLogService;

	/**
	 * 账户余额及充值记录信息
	 * 
	 * @param userId
	 * @param curPageNO
	 * @return
	 */
	@Override
	public UserPredepositDto findUserPredeposit(String userId, String curPageNO) {
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdCashLog> pdCashLogs = pdCashLogService.getPdCashLogPage(userId, curPageNO);
		predeposit.setPdCashLogs(pdCashLogs);
		return predeposit;
	}

	/**
	 * 查看充值明细
	 */
	@Override
	public UserPredepositDto findRechargeDetail(String userId, String curPageNO, String pdrSn) {
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdRecharge> pageSupport = pdRechargeService.getPdRechargePage(userId, null, pdrSn, curPageNO, 10);
		predeposit.setRechargeDetails(pageSupport);
		return predeposit;
	}

	/**
	 * 查看预存款提现申请
	 */
	@Override
	public UserPredepositDto findBalanceWithdrawal(String userId, String curPageNO, String pdcSn, Integer status) {
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdWithdrawCash> pageSupport = pdWithdrawCashService.getFindBalanceWithdrawal(curPageNO, pdcSn, status, userId);
		predeposit.setPdCashs(pageSupport);
		return predeposit;
	}

	/**
	 * 查看用户冻结余额
	 */
	@Override
	public UserPredepositDto findBalanceHoding(String userId, String curPageNO, String sn, String type) {
		UserPredepositDto predeposit = new UserPredepositDto();
		PageSupport<PdCashHolding> pageSupport = pdCashHoldingDao.getPdCashHoldingPage(curPageNO, userId, sn, type);
		predeposit.setCashHodings(pageSupport);
		return predeposit;
	}
	
	/**
	 * 查看用户冻结余额
	 */
	@Override
	public PageSupport<PdCashHolding> getBalanceHoding(String userId, String curPageNO, String sn, String type) {
		PageSupport<PdCashHolding> pageSupport = pdCashHoldingDao.getPdCashHoldingPage(curPageNO, userId, sn, type);
		return pageSupport;
	}
	

	@Override
	public String deleteRechargeDetail(String userId, String pdrSn) {
		int count = pdRechargeService.deleteRechargeDetail(userId, pdrSn);
		if (count > 0) {
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 充值支付
	 */
	@Override
	public PdRecharge rechargePay(String userId, String userName, Double pdrAmount) {
		String pdrSn = CommonServiceUtil.getSubNember(userName);
		PdRecharge pdRecharge = new PdRecharge();
		pdRecharge.setSn(pdrSn);
		pdRecharge.setUserId(userId);
		pdRecharge.setUserName(userName);
		pdRecharge.setAmount(pdrAmount);
		pdRecharge.setAddTime(new Date());
		pdRecharge.setPaymentState(0);
		Long pdrId = pdRechargeService.savePdRecharge(pdRecharge);
		pdRecharge.setId(pdrId);
		return pdRecharge;
	}

	/**
	 * 金币
	 */
	@Override
	public CoinVo findUserCoin(String userId, String curPageNO) {
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		CoinVo coinVo = new CoinVo();
		if (AppUtils.isNotBlank(userDetail)) {
			coinVo.setCoin(userDetail.getUserCoin());
		}
		PageSupport<CoinLog> coinLog = coinLogService.getUserCoinPage(curPageNO, userId);
		coinVo.setCoinLog(coinLog);
		return coinVo;
	}

	public CoinLogService getCoinLogService() {
		return coinLogService;
	}
}
