package com.legendshop.business.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.spi.service.ShopDecotateCacheService;

/**
 * 
 * 布局服务缓存服务
 *
 */
@Component("shopDecotateCacheService")
public class ShopDecotateCacheServiceImpl implements ShopDecotateCacheService {

	@Override
	@CachePut(value = "ShopDecotateDto", key = "'ShopDecotate_'+#decotateDto.shopId")
	public ShopDecotateDto cachePutShopDecotate(ShopDecotateDto decotateDto) {
		return decotateDto;
	}

	@Override
	@CachePut(value = "OnlineShopDecotateDto", key = "'OnlineShopDecotate_'+#decotateDto.shopId")
	public ShopDecotateDto cachePutOnlineShopDecotate(ShopDecotateDto decotateDto) {
		return decotateDto;
	}

	@Override
	@CachePut(value = "StoreListDto", key = "'StoreList_'+#storeListDto.shopId")
	public StoreListDto cachePutShopStoreListDto(StoreListDto storeListDto) {
		return storeListDto;
	}

	@Override
	@CacheEvict(value = "ShopDecotateDto", key = "'ShopDecotate_'+#shopId")
	public void cachePutShopDecotate(Long shopId) {
	}

	@Override
	@Caching(evict = { @CacheEvict(value = "ShopLayoutHotProdDtoList", allEntries = true), @CacheEvict(value = "ShopLayoutBannerList", allEntries = true) })
	public void clearAllDecCache() {

	}

}
