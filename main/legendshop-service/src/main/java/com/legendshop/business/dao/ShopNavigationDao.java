/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopNavigation;

/**
 * 商家导航Dao.
 */
public interface ShopNavigationDao extends GenericDao<ShopNavigation, Long> {

	/**
	 * Gets the shop navigation by shop id.
	 *
	 * @param shopId
	 *            the shop id
	 * @return the shop navigation by shop id
	 */
	public abstract List<ShopNavigation> getShopNavigationByShopId(Long shopId);

	/**
	 * Gets the shop navigation.
	 *
	 * @param id
	 *            the id
	 * @return the shop navigation
	 */
	public abstract ShopNavigation getShopNavigation(Long id);

	/**
	 * Delete shop navigation.
	 *
	 * @param shopNavigation
	 *            the shop navigation
	 */
	public abstract void deleteShopNavigation(ShopNavigation shopNavigation);

	/**
	 * Save shop navigation.
	 *
	 * @param shopNavigation
	 *            the shop navigation
	 * @return the long
	 */
	public abstract Long saveShopNavigation(ShopNavigation shopNavigation);

	/**
	 * Update shop navigation.
	 *
	 * @param shopNavigation
	 *            the shop navigation
	 */
	public abstract void updateShopNavigation(ShopNavigation shopNavigation);

	/**
	 * Gets the shop navigation.
	 *
	 * @param cq
	 *            the cq
	 * @return the shop navigation
	 */
	public abstract PageSupport<ShopNavigation> getShopNavigation(CriteriaQuery cq);

	/**
	 * Gets the shop navigation page.
	 *
	 * @param curPageNO
	 *            the cur page no
	 * @param shopNavigation
	 *            the shop navigation
	 * @return the shop navigation page
	 */
	public abstract PageSupport<ShopNavigation> getShopNavigationPage(String curPageNO, ShopNavigation shopNavigation);

}
