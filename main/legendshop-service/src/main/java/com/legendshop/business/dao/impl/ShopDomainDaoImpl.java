/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopDomainDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ShopDomain;

/**
 * 店铺域名Dao
 */
@Repository
public class ShopDomainDaoImpl extends GenericDaoImpl<ShopDomain, Long> implements ShopDomainDao  {

	public ShopDomain getShopDomain(Long id){
		return getById(id);
	}
	
    public int deleteShopDomain(ShopDomain shopDomain){
    	return delete(shopDomain);
    }
	
	public Long saveShopDomain(ShopDomain shopDomain){
		return save(shopDomain);
	}
	
	public int updateShopDomain(ShopDomain shopDomain){
		return update(shopDomain);
	}

 }
