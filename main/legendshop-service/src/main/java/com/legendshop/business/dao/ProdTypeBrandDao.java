/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProdTypeBrand;

/**
 * The Class ProdTypeBrandDao.
 */

public interface ProdTypeBrandDao extends Dao<ProdTypeBrand, Long> {
	
	public ProdTypeBrand getProdTypeBrand(Long id);
	
    public abstract int deleteProdTypeBrand(ProdTypeBrand prodTypeBrand);
	
	public abstract Long saveProdTypeBrand(ProdTypeBrand prodTypeBrand);
	
	public abstract List<Long> saveProdTypeBrand(List<ProdTypeBrand> prodTypeBrandList);
 }
