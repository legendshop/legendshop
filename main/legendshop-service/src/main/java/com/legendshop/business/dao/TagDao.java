/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Tag;

/**
 * 标签Dao.
 */
public interface TagDao extends Dao<Tag, Long>{

	public abstract List<Tag> getPageTag(Long shopId, String page);

	/**
	 * Gets the tag.
	 * 
	 * @param id
	 *            the id
	 * @return the tag
	 */
	public abstract Tag getTag(Long id);

	/**
	 * Delete tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public abstract void deleteTag(Tag tag);

	/**
	 * Save tag.
	 * 
	 * @param tag
	 *            the tag
	 * @return the long
	 */
	public abstract Long saveTag(Tag tag);

	/**
	 * Update tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public abstract void updateTag(Tag tag);

	/**
	 * Gets the tag.
	 * 
	 * @param name
	 *            the name
	 * @param userName
	 *            the user name
	 * @return the tag
	 */
	public abstract Tag getTag(String name, String userName);

	public abstract PageSupport<Tag> getTagPage(String curPageNO, Tag tag);

}
