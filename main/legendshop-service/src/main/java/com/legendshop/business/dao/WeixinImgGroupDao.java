/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinImgGroup;

/**
 * 微信图片组Dao
 */
public interface WeixinImgGroupDao extends Dao<WeixinImgGroup, Long> {
     
	public abstract WeixinImgGroup getWeixinImgGroup(Long id);
	
    public abstract int deleteWeixinImgGroup(WeixinImgGroup weixinImgGroup);
	
	public abstract Long saveWeixinImgGroup(WeixinImgGroup weixinImgGroup);
	
	public abstract int updateWeixinImgGroup(WeixinImgGroup weixinImgGroup);
	
	public abstract List<WeixinImgGroup> getAllGroupsInfo();

	public abstract WeixinImgGroup getWeixinImgGroupByName(String groupName);
	
 }
