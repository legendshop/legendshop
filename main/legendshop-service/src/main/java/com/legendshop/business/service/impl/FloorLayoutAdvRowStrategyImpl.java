package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.FloorDao;
import com.legendshop.business.dao.FloorItemDao;
import com.legendshop.model.constant.FloorLayoutEnum;
import com.legendshop.model.constant.FloorTemplateEnum;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.spi.service.FloorLayoutStrategy;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;


/**
 *横排广告
 * @author Tony
 *
 */
@Service("floorLayoutAdvRowStrategy")
public class FloorLayoutAdvRowStrategyImpl implements FloorLayoutStrategy {
	
	@Autowired
	private FloorDao floorDao;
	
	@Autowired
	private FloorItemDao floorItemDao;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Override
	public void deleteFloor(Floor floor) {
        
        //删除装载的广告信息;
        FloorItem leftAdv = floorItemDao.getAdvFloorItem(floor.getFlId(), FloorTemplateEnum.LEFT_ADV.value(),FloorLayoutEnum.WIDE_BLEND.value());
        
        deleteAdvItem(leftAdv);
        
        //删除FloorItem中floorId = floor.getFlId() 但类型不为'ST'的floorItem
    	floorItemDao.deleteFloorItem(floor.getFlId());
    	//最后删除自己本身
        floorDao.deleteFloor(floor);
        
	}
	
	 private void deleteAdvItem(FloorItem leftAdv ){
	    	if(leftAdv==null){
	    		return;
	    	}
	    	if(AppUtils.isNotBlank(leftAdv.getPicUrl())){
	    		
	    		if(AppUtils.isNotBlank(leftAdv.getPicUrl())){
					//delete  attachment
					attachmentManager.delAttachmentByFilePath(leftAdv.getPicUrl());
					
					//delete file image
					attachmentManager.deleteTruely(leftAdv.getPicUrl());
	    		}
				
			}
	}

	@Override
	public FloorDto findFloorDto(Floor floor) {
		FloorDto floorDto=new FloorDto();
		floorDto.setCssStyle(floor.getCssStyle());
		floorDto.setFlId(floor.getFlId());
        floorDto.setLayout(floor.getLayout());
        floorDto.setName(floor.getName());
        floorDto.setSeq(floor.getSeq());
        List<FloorItem> advList = floorItemDao.queryAdvFloorItem(floor.getFlId(),FloorTemplateEnum.RIGHT_ADV.value(),FloorLayoutEnum.WIDE_ADV_ROW.value());
    	if(AppUtils.isNotBlank(advList)){
			 floorDto.addFloorItems(FloorTemplateEnum.RIGHT_ADV.value(), advList);
		}
		return floorDto;
	}
}
