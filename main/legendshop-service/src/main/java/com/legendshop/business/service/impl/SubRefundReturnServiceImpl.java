/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import cn.hutool.core.util.NumberUtil;
import com.legendshop.model.constant.*;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.util.Arith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.ConstTableDao;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.ScheduleListDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.SubRefundReturnDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.dto.SubRefundRetuenDto;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.model.entity.store.StoreProd;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import org.springframework.transaction.annotation.Transactional;

/**
 * 退款服务
 */
@Service("subRefundReturnService")
public class SubRefundReturnServiceImpl implements SubRefundReturnService {

	@Autowired
	private SubRefundReturnDao subRefundReturnDao;

	@Autowired
	private SubDao subDao;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private StockService stockService;

	@Autowired
	private CouponDao couponDao;

	@Autowired
	private UserCouponDao userCouponDao;

	@Autowired
	private ScheduleListDao scheduleListDao;

	@Autowired
	private ConstTableDao constTableDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private SubHistoryService subHistoryService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;


	/**
	 * 根据settlementSn获得已经退款成功的退款订单项
	 */
	@Override
	public List<SubRefundReturn> getSubRefundsSuccessBySettleSn(String settleSn) {
		return subRefundReturnDao.getSubRefundsSuccessBySettleSn(settleSn);
	}

	@Override
	public SubRefundReturn getSubRefundReturn(Long id) {
		return subRefundReturnDao.getSubRefundReturn(id);
	}

	@Override
	@Transactional
	public void receiveGoods(Integer goodsState, SubRefundReturn subRefundReturn) {
		if (RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value().equals(goodsState)) {
			subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value());
			this.closeSubRefundReturnByReturn(subRefundReturn, true);
		} else {
			subRefundReturn.setReceiveTime(new Date());
			subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_RECEIVED.value());
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());// 待管理员审核
		}
		this.confirmSubRefundReturn(subRefundReturn);
		Sub sub = new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		sub.setUserName(subRefundReturn.getUserName());
		SubItem subItem = subItemService.getSubItem(subRefundReturn.getSubItemId());
		// 商家确认收货，库存恢复 update by 2020-08-12 只有平台同意并且该订单未发货才会自动恢复库存，商家收到货后可以自行添加
//		stockService.makeUpStocks(subItem.getSkuId(), subItem.getRefundCount());
		saveOrderHistoryProduct(sub);
	}

	@Override
	public void deleteSubRefundReturn(SubRefundReturn subRefundReturn) {
		subRefundReturnDao.deleteSubRefundReturn(subRefundReturn);
	}

	@Override
	public Long saveSubRefundReturn(SubRefundReturn subRefundReturn) {
		if (!AppUtils.isBlank(subRefundReturn.getRefundId())) {
			updateSubRefundReturn(subRefundReturn);
			return subRefundReturn.getRefundId();
		}
		return subRefundReturnDao.saveSubRefundReturn(subRefundReturn);
	}

	@Override
	public void updateSubRefundReturn(SubRefundReturn subRefundReturn) {
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);
	}

	/**
	 * 保存订单退款申请
	 **/
	@Override
	public void saveApplyOrderRefund(SubRefundReturn subRefundReturn) {
		Long refundId = subRefundReturnDao.saveSubRefundReturn(subRefundReturn);
		//更新订单的退款状态
		subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_PROCESSING.value());
		//更新订单项的退款信息
		subItemDao.updateRefundInfoBySubId(subRefundReturn.getSubNumber(), refundId);

		//如果是用户申请退款
		if (RefundSouceEnum.USER.value() == subRefundReturn.getRefundSouce()) {
			// 插入自动同意退款的定时任务列表
			insertScheduleList(subRefundReturn, ScheduleTypeEnum.AUTO_7_REFUND.value());
		}
	}

	/**
	 * 保存订单项退款申请
	 **/
	@Override
	public void saveApplyItemRefund(SubRefundReturn subRefundReturn) {
		Long refundId = subRefundReturnDao.saveSubRefundReturn(subRefundReturn);
		//更新订单的状态
		subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_PROCESSING.value());

		SubItem subItem = subItemDao.getSubItem(subRefundReturn.getSubItemId());
		subItem.setRefundId(refundId);
		subItem.setRefundState(RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
		subItem.setRefundType(RefundReturnTypeEnum.REFUND.value());
		if (subRefundReturn.getIsPresell() && subRefundReturn.getIsRefundDeposit()) {//预售是否退订金
			subItem.setRefundAmount(subRefundReturn.getRefundAmount().add(subRefundReturn.getDepositRefund()));
		} else {
			subItem.setRefundAmount(subRefundReturn.getRefundAmount());
		}
		subItemDao.updateSubItem(subItem);//更新订单项

		// 插入自动同意退款的定时任务列表
		insertScheduleList(subRefundReturn, ScheduleTypeEnum.AUTO_7_REFUND.value());

		// 发送站内信 通知
		UserDetail shopUser = userDetailDao.getUserDetailByShopId(subRefundReturn.getShopId());

		sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopUser.getUserName(), "退货通知", "亲，您的订单["
				+ subRefundReturn.getSubNumber() + "],买家申请退款！").getSource());
	}

	/**
	 * 保存订单项退货申请
	 **/
	@Override
	public void saveApplyItemReturn(SubRefundReturn subRefundReturn) {
		Long refundId = subRefundReturnDao.saveSubRefundReturn(subRefundReturn);
		//更新订单的状态
		subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_PROCESSING.value());

		SubItem subItem = subItemDao.getSubItem(subRefundReturn.getSubItemId());
		subItem.setRefundId(refundId);
		subItem.setRefundState(RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
		subItem.setRefundType(RefundReturnTypeEnum.REFUND_RETURN.value());
		subItem.setRefundCount(subRefundReturn.getGoodsNum());
		if (subRefundReturn.getIsPresell() && subRefundReturn.getIsRefundDeposit()) {//预售是否退订金
			subItem.setRefundAmount(subRefundReturn.getRefundAmount().add(subRefundReturn.getDepositRefund()));
		} else {
			subItem.setRefundAmount(subRefundReturn.getRefundAmount());
		}
		subItemDao.updateSubItem(subItem);//更新订单项

		// 插入自动同意退款的定时任务列表
		insertScheduleList(subRefundReturn, ScheduleTypeEnum.AUTO_7_REFUND.value());

		// 发送站内信 通知
		UserDetail shopUser = userDetailDao.getUserDetailByShopId(subRefundReturn.getShopId());

		sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopUser.getUserName(), "退货通知", "亲，您的订单["
				+ subRefundReturn.getSubNumber() + "],买家申请退款！").getSource());
	}

	@Override
	public ApplyRefundReturnDto getApplyRefundReturnDto(Long id, RefundReturnTypeEnum opType, String userId) {
		ApplyRefundReturnDto applyRefundReturnDto = null;
		switch (opType) {
			case OP_ORDER: //订单退款
				applyRefundReturnDto = subRefundReturnDao.getOrderInfo(id, userId);
				break;
			case OP_ORDER_ITEM: //订单项退款/退货
				applyRefundReturnDto = subRefundReturnDao.getOrderItemInfo(id, userId);
				break;
		}
		if (applyRefundReturnDto.getSubType().equals(SubTypeEnum.MERGE_GROUP.value())){
			List<SubRefundReturn> sunRefundlist = subRefundReturnDao.getSubRefundsSuccessBySubNumber(applyRefundReturnDto.getSubNumber());
			Double alreadyAmount = 0d;  //已经成功退款的总金额
			//Double alreadyItemAmount = 0d;
			Integer goodsCount = applyRefundReturnDto.getBasketCount().intValue();
			for (SubRefundReturn refund : sunRefundlist) {
				if (refund.getGoodsNum() == null){
					refund.setGoodsNum(1L);
				}
				goodsCount = goodsCount - refund.getGoodsNum().intValue();
				Double successRefund = refund.getRefundAmount().doubleValue();  //已经成功退款的金额
				//累加订单中已经成功退款的总金额
				alreadyAmount = Arith.add(alreadyAmount, successRefund);

//				if (refund.getSubItemId() != null && subRefundReturn.getSubItemId() != null && refund.getSubItemId().equals(subRefundReturn.getSubItemId())) {
//					alreadyItemAmount = Arith.add(alreadyItemAmount, successRefund);
//				}
			}
			Double refundAmount = applyRefundReturnDto.getRefundAmount();
			double sub = NumberUtil.sub(refundAmount, alreadyAmount);
			applyRefundReturnDto.setRefundAmount(sub);
			applyRefundReturnDto.setGoodsCount(goodsCount);
			applyRefundReturnDto.setBasketCount(goodsCount.longValue());
		}
		return applyRefundReturnDto;
	}

	@Override
	public void shopAuditRefund(SubRefundReturn subRefundReturn, boolean isAgree, String sellerMessage, boolean falg) {
		Date currDate = new Date();
		String tips = null;
		if (isAgree) {//同意
			tips = "]商家已经同意退款";
			subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_AGREE.value());
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());

			//商品库存恢复
			Sub sub = subDao.getSubById(subRefundReturn.getSubId());
			if (subRefundReturn.getSubItemId() == 0) {      //全部订单退款
				if (CartTypeEnum.SHOP_STORE.toCode().equals(sub.getSubType())) {  //自提订单
					List<SubItem> subItems = subItemDao.getSubItem(subRefundReturn.getSubNumber());
					for (SubItem item : subItems) {
						StoreProd storeProd = subDao.getStoreProdByShopIdAndProdId(sub.getShopId(), item.getProdId());
						subDao.update("update ls_store_prod set stock=stock+? where shop_id=? and prod_id=?  ",
								item.getBasketCount(), sub.getShopId(), item.getProdId());
						subDao.update("update ls_store_sku set stock=stock+? where store_id=? and prod_id=? and sku_id=? ",
								item.getBasketCount(), storeProd.getStoreId(), item.getProdId(), item.getSkuId());
					}
				}
			}

		} else {//不同意
			tips = "]退款申请审核失败";
			if (subRefundReturn.getSubItemId() == 0) {//全部订单退款
				//更新订单退款状态为初始状态
				subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_NO_REFUND.value());

				//还原sub item state
				subItemDao.updateRefundStateBySubNumber(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_NO_REFUND.value());
			} else {//订单项退款
				Long count = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
				if (count == 1) {//最后一个在处理的订单项
					subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_NO_REFUND.value());
				}
				//更新订单项退款状态为不同意
				subItemDao.updateRefundState(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_NO_REFUND.value());
			}
			subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_DISAGREE.value());//不同意
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_FINISH.value());
		}
		subRefundReturn.setSellerTime(currDate);
		subRefundReturn.setSellerMessage(sellerMessage);

		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);


		// 由于商家在规定时间内做下一步动作 关闭自动同意退款的定时任务
		if (falg) {
			scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_REFUND.value());
		}

		// 发送站内信 通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(subRefundReturn.getUserName(), "退款通知", "亲，您的订单["
				+ subRefundReturn.getSubNumber() + tips).getSource());
	}

	/**
	 * 1、同意：
	 * 卖家处理状态:2
	 * 如果是弃货 直接变成待平台审核通过,否则需要用户进行补货流程.
	 * <p>
	 * 2、不同意退货申请:
	 * 1判断这条退款申请是否是最后一天处理记录  是：
	 */
	@Override
	public void shopAuditReturn(SubRefundReturn subRefundReturn, Boolean isAgree, Boolean isJettison, String sellerMessage, boolean falg) {
		Date currDate = new Date();
		String tips;
		if (isAgree) {//同意
			tips = "]商家已经同意退货";
			subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_AGREE.value());
			if (isJettison) {//弃货
				subRefundReturn.setReturnType(RefundReturnTypeEnum.NO_NEED_GOODS.value());// return_type 不需要退货
				subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());// apply_state 待管理员审核
			} else {
				subRefundReturn.setReturnType(RefundReturnTypeEnum.NEED_GOODS.value());// return_type 需要退货
				subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value());// apply_state 设为待发货

				// 商家同意退货,新增用户发货的定时器
				insertScheduleList(subRefundReturn, ScheduleTypeEnum.AUTO_7_NOT_WRITE_CLOSE_REFUND.value());
			}
		} else {//不同意
			tips = "]退货申请审核失败";
			Long count = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
			if (count == 1) {//最后一个在处理的订单项
				subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_NO_REFUND.value());
			}
			//更新订单项状态为初始化状态 : 没有发起退款,退货,这样用户就可以重新发起
			subItemDao.updateRefundState(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_NO_REFUND.value());
			subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_DISAGREE.value());
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_FINISH.value());
		}
		subRefundReturn.setSellerTime(currDate);
		subRefundReturn.setSellerMessage(sellerMessage);
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);

		// 由于商家在规定时间内做下一步动作 关闭自动同意退款的定时任务
		if (falg) {
			scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_REFUND.value());
		}

		// 发送站内信 通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(subRefundReturn.getUserName(), "退货通知", "亲，您的订单["
				+ subRefundReturn.getSubNumber() + tips).getSource());
	}


	@Override
	public void adminAuditRefund(SubRefundReturn subRefundReturn, String adminMessage) {
		/*
		 * 分两种情况:
		 * 	1.如果是订单退款: 订单状态更新为已取消, 订单退款状态更新为已完成
		 * 	2.如果是订单项退款: 订单项退款状态更新为已完成,检查是否最后一个在退款中的订单项,是则订单状态更新为已完成
		 */
		if (subRefundReturn.getSubItemId() == 0) { //说明是整个订单的退款操作
			// 如果是整个订单退款操作，此时订单应该是已付款未发货状态，可以直接回退库存
			Sub sub = subDao.getSubById(subRefundReturn.getSubId());
			List<SubItem> subItems = subItemDao.getSubItem(subRefundReturn.getSubNumber());
			for (SubItem item : subItems) {
				stockService.releaseHold(item.getProdId(), item.getSkuId(), item.getBasketCount(), sub.getSubType());
			}
			subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_FINISH.value());
			subDao.closeSub(subRefundReturn.getSubId());
			//更新订单下的所有订单项为退款完成
			subItemDao.updateRefundStateBySubNumber(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_FINISH.value());

			// 如果使用了优惠券，退回优惠券
			//List<UserCoupon> userCoupons = userCouponDao.queryByProperties(new EntityCriterion().eq("orderNumber", subRefundReturn.getSubNumber()));
			List<UserCoupon> userCoupons = userCouponDao.queryByOrderNumber(subRefundReturn.getSubNumber());
			if (AppUtils.isNotBlank(userCoupons)) {
				for (UserCoupon userCoupon : userCoupons) {
					userCoupon.setUseStatus(UserCouponEnum.UNUSED.value());
					userCoupon.setUseTime(null);
					userCoupon.setOrderNumber(null);
					userCoupon.setOrderPrice(null);
					userCouponDao.updateUserCoupon(userCoupon);
					couponDao.reduceCouponUseNum(userCoupon.getCouponId(), subRefundReturn.getUserId());
				}
			}

		} else {
			Long count = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
			if (count == 1) {// 最后一个在处理的订单项
				/*
				 * 检测是否有未发起过退款/退货的订单项
				 */
				boolean config = subItemDao.existNoRefunditem(subRefundReturn.getSubNumber());
				if (!config) {//没有
					subDao.finalySub(subRefundReturn.getSubId());
				}
				//更新订单退款状态
				subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_FINISH.value());
			}
			//更新订单项为同意
			subItemDao.updateRefundState(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_REFUND_FINISH.value());
		}
		/*
		 * 处理该退款单流程
		 */
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_FINISH.value());//退款完成
		subRefundReturn.setAdminTime(new Date());
		subRefundReturn.setAdminMessage(adminMessage);
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);//更新退款记录
	}

	@Override
	public void adminAuditReturn(SubRefundReturn subRefundReturn, String adminMessage) {
		Long count = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
		if (count == 1) {// 最后一个在处理的订单项
			//更新订单退款状态
			subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_REFUND_FINISH.value());
			/*
			 * 检测是否有未发起
			 */
			boolean config = subItemDao.existNoRefunditem(subRefundReturn.getSubNumber());
			if (!config) {
				subDao.finalySub(subRefundReturn.getSubId());
			}
		}
		//更新订单项为同意
		subItemDao.updateRefundState(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_REFUND_FINISH.value());
		/*
		 * 处理该退款单流程
		 */
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_FINISH.value());//退款完成
		subRefundReturn.setAdminTime(new Date()); //最后管理员处理时间
		subRefundReturn.setAdminMessage(adminMessage);
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);//更新退款记录
	}

	/**
	 * 重新绑定 申请编号(商户退款单号)
	 *
	 * @param returnPayId
	 * @param batch_no
	 */
	@Override
	public void updateRefundSn(Long returnPayId, String batch_no) {
		subRefundReturnDao.updateRefundSn(returnPayId, batch_no);
	}

	@Override
	public int orderReturnSucess(String refundSn, String outRefundNo, String handleType) {
		return subRefundReturnDao.orderReturnSucess(refundSn, outRefundNo, handleType);
	}

	@Override
	public int orderReturnFail(String refundSn) {
		return subRefundReturnDao.orderReturnFail(refundSn);
	}

	public static void main(String[] args) {
		Long aLong = 130l;
		Long bLong = 130l;
		System.out.println(aLong == bLong);
	}

	@Override
	public void deleteRefundReturn(SubRefundReturn subRefundReturn) {
		subRefundReturnDao.deleteSubRefundReturn(subRefundReturn);
		//删除凭证图片
		if (AppUtils.isNotBlank(subRefundReturn.getPhotoFile1())) {
			attachmentManager.deleteTruely(subRefundReturn.getPhotoFile1());
		}
		if (AppUtils.isNotBlank(subRefundReturn.getPhotoFile2())) {
			attachmentManager.deleteTruely(subRefundReturn.getPhotoFile2());
		}
		if (AppUtils.isNotBlank(subRefundReturn.getPhotoFile3())) {
			attachmentManager.deleteTruely(subRefundReturn.getPhotoFile3());
		}
	}

	@Override
	public SubRefundReturn getSubRefundReturnBySubNumber(String subNumber) {
		return subRefundReturnDao.getSubRefundReturnBySubNumber(subNumber);
	}

	/**
	 * 根据订单项id查看退货详情
	 */
	@Override
	public SubRefundReturn getSubRefundReturnBySubItemId(String subItemId) {
		return subRefundReturnDao.getSubRefundReturnBySubItemId(subItemId);
	}

	/**
	 * 返回batch_no
	 */
	@Override
	public String updateRefundSn(SubRefundReturn refundReturn) {
		String batch_no = null;
		Date date = new Date();
		if (AppUtils.isBlank(refundReturn.getRefundSn())) {
			batch_no = CommonServiceUtil.getRandomSn();
			subRefundReturnDao.updateRefundSn(refundReturn.getId(), batch_no); // 重新绑定-申请编号(商户退款单号)
		} else {
			// 将其按照以下格式转换成字符串
			SimpleDateFormat sdfLogin = new SimpleDateFormat("yyyyMMdd");
			// 按照格式转换两个数据
			String nowTime = sdfLogin.format(date);
			String batchTime = refundReturn.getRefundSn().substring(0, 8);
			// 判断不是同一天
			if (!nowTime.equals(batchTime)) {
				batch_no = CommonServiceUtil.getRandomSn();
				subRefundReturnDao.updateRefundSn(refundReturn.getId(), batch_no); // 重新绑定-申请编号(商户退款单号)
			} else {
				batch_no = refundReturn.getRefundSn();
			}
		}
		return batch_no;
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, String status,
															   RefundSearchParamDto paramData) {
		return subRefundReturnDao.getSubRefundReturnPage(curPageNO, status, paramData);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturn(String curPageNO, String status,
														   RefundSearchParamDto paramData) {
		return subRefundReturnDao.getSubRefundReturn(curPageNO, status, paramData);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, ShopOrderBill shopOrderBill,
															   String subNumber) {
		return subRefundReturnDao.getSubRefundReturnPage(curPageNO, shopOrderBill, subNumber);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, Long shopId, String subNumber,
															   ShopOrderBill shopOrderBill) {
		return subRefundReturnDao.getSubRefundReturnPage(curPageNO, shopId, subNumber, shopOrderBill);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, RefundSearchParamDto paramData,
															   Long shopId) {
		return subRefundReturnDao.getSubRefundReturnPage(curPageNO, paramData, shopId);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubReturnListPage(String curPageNO, RefundSearchParamDto paramData,
															 Long shopId) {
		return subRefundReturnDao.getSubReturnListPage(curPageNO, paramData, shopId);
	}

	@Override
	public PageSupport<SubRefundReturn> getRefundListPage(String curPageNO, RefundSearchParamDto paramData,
														  String userId) {
		return subRefundReturnDao.getRefundListPage(curPageNO, paramData, userId);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnMobile(String curPageNO, String userId,
																 RefundSearchParamDto paramData) {
		return subRefundReturnDao.getSubRefundReturnMobile(curPageNO, userId, paramData);
	}

	@Override
	public PageSupport<SubRefundReturn> getSubRefundReturnByCenter(String curPageNO, String userId,
																   RefundSearchParamDto paramData) {
		return subRefundReturnDao.getSubRefundReturnByCenter(curPageNO, userId, paramData);
	}

	@Override
	public PageSupport<SubRefundReturn> getReturnByCenter(String curPageNO, String userId,
														  RefundSearchParamDto paramData) {
		return subRefundReturnDao.getReturnByCenter(curPageNO, userId, paramData);
	}

	@Override
	public List<SubRefundRetuenDto> exportOrder(ShopOrderBill shopOrderBill, String subNumber) {
		StringBuilder sb = new StringBuilder();
		List<Object> obj = new ArrayList<Object>();
		sb.append("SELECT s.sub_number AS subNumber,s.apply_type AS applyType,s.refund_amount AS refundAmount,s.apply_time AS applyTime,");
		sb.append("IFNULL (ROUND(i.redpack_off_price*i.cash/i.product_total_amout,2),'0.00') AS returnRedpackOffPrice, ");
		sb.append("s.admin_time AS adminTime ");
		sb.append("FROM ls_sub_refund_return s ");
		sb.append("LEFT JOIN ls_sub_item i ON s.sub_item_id = i.sub_item_id ");
		sb.append("WHERE 1=1 ");
		sb.append(" AND s.shop_id=?");
		obj.add(shopOrderBill.getShopId());
		sb.append(" AND s.apply_state=?");
		obj.add(RefundReturnStatusEnum.APPLY_FINISH.value());
		sb.append(" AND s.bill_sn=?");
		obj.add(shopOrderBill.getSn());
		if (AppUtils.isNotBlank(subNumber)) {
			sb.append(" AND s.sub_number=?");
			obj.add(subNumber);
		}
		List<SubRefundRetuenDto> list = this.subRefundReturnDao.exportOrder(sb.toString(), obj.toArray());
		if (AppUtils.isNotBlank(list)) {
			for (SubRefundRetuenDto subRefundRetuenDto : list) {
				if (subRefundRetuenDto.getApplyType().equals("1")) {
					subRefundRetuenDto.setApplyType("仅退款");
				} else if (subRefundRetuenDto.getApplyType().equals("2")) {
					subRefundRetuenDto.setApplyType("退款退货");
				}
			}
		}
		return list;
	}

	@Override
	public SubRefundReturn getSubRefundReturnByRefundSn(String refundSn) {
		return subRefundReturnDao.getSubRefundReturnByRefundSn(refundSn);
	}

	@Override
	public void undoApplySubRefundReturn(SubRefundReturn subRefundReturn) {

		Long count = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
		// 只有当订单下的子项中只有一个处于退款退货中时，才更新订单退款状态
		if (count == 1) {
			// 更新订单退款状态
			subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_NO_REFUND.value());
		}
		// 还原sub item state
		subItemDao.updateRefundStateBySubItemId(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_NO_REFUND.value());

		subRefundReturn.setApplyState(RefundReturnStatusEnum.UNDO_APPLY.value());

		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);

		// 关闭退款申请的调度任务
		scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_REFUND.value());
	}

	/**
	 * 插入退款退货操作的定时任务列表 因为都是默认七天 所以由操作类型确定
	 *
	 * @param subRefundReturn
	 * @param type            3 买家申请退款，等待商家处理，时限为7天，超时系统将自动同意退款给买家； 4
	 *                        买家已经退货，等待商家收货，时限为自买家填写物流单后起7天，超时系统将自动同意退款给买家； 5
	 *                        商家同意买家退款申请，提醒买家发货，时限为7天，买家超时未填写物流单号，系统将自动关闭退款申请。
	 */
	private void insertScheduleList(SubRefundReturn subRefundReturn, Integer type) {
		Date date = new Date();
		ScheduleList scheduleList = new ScheduleList();
		scheduleList.setScheduleJson("");
		scheduleList.setScheduleSn(subRefundReturn.getRefundSn());
		scheduleList.setScheduleType(type);
		scheduleList.setShopId(subRefundReturn.getShopId());
		scheduleList.setStatus(0);
		// 获取平台设置的定时时间 默认为七天
		Integer number = 7;
		ConstTable constTable = constTableDao.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
			if (AppUtils.isNotBlank(orderSetting.getAuto_return_review())) {
				number = Integer.parseInt(orderSetting.getAuto_return_review());
			}
		}
		scheduleList.setTriggerTime(DateUtils.rollDay(date, number));
		scheduleList.setCreateTime(date);
		scheduleListDao.save(scheduleList);

	}


	/**
	 * 拒绝收货。或未收到货物 关闭退款申请
	 */
	@Override
	public void closeSubRefundReturnByReturn(SubRefundReturn subRefundReturn, boolean falg) {
		Long num = subItemDao.queryRefundItemCount(subRefundReturn.getSubNumber(), RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value());
		if (num == 1) {// 最后一个在处理的订单项
			subDao.updateRefundState(subRefundReturn.getSubId(), RefundReturnStatusEnum.ORDER_NO_REFUND.value());
		}
		// 更新订单项状态为初始化状态 : 没有发起退款,退货,这样用户就可以重新发起
		subItemDao.updateRefundState(subRefundReturn.getSubItemId(), RefundReturnStatusEnum.ITEM_NO_REFUND.value());

		subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.CANDEL_APPLY.value());// 未收到货物,交易关闭
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);

		// 由于商家在规定时间内做下一步动作 关闭提醒收货的定时器
		if (falg) {
			scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_GOODS_REFUND.value());
		}

	}

	/**
	 * 商家确认收货
	 */
	@Override
	public void confirmSubRefundReturn(SubRefundReturn subRefundReturn) {
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);
		// 关闭提醒收货的定时器
		scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_GOODS_REFUND.value());
	}

	/**
	 * 用户退货给商家
	 */
	@Override
	public void shipSubRefundReturn(SubRefundReturn subRefundReturn) {
		subRefundReturnDao.updateSubRefundReturn(subRefundReturn);
		// 买家已经退货，新增商家收货定时器任务
		insertScheduleList(subRefundReturn, ScheduleTypeEnum.AUTO_7_GOODS_REFUND.value());

		// 关闭提醒发货的定时器
		scheduleListDao.cancelScheduleList(subRefundReturn.getShopId(), subRefundReturn.getRefundSn(), ScheduleTypeEnum.AUTO_7_NOT_WRITE_CLOSE_REFUND.value());

		UserDetail shopUser = userDetailDao.getUserDetailByShopId(subRefundReturn.getShopId());

		// 发送站内信 通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopUser.getUserName(), "退货通知", "亲，您的订单["
				+ subRefundReturn.getSubNumber() + "],买家已经退货啦！").getSource());
	}

	/**
	 * 更新平台退款
	 */
	@Override
	public int updateSubPlatRefund(Long refundId, BigDecimal refundAmount, Integer is_handle_success, String refundType) {
		return subRefundReturnDao.updateSubPlatRefund(refundId, refundAmount, is_handle_success, refundType);
	}

	/**
	 * 更新第三方退款
	 */
	@Override
	public int updateSubThirdRefundAmount(Long returnPayId, BigDecimal refundAmount) {
		return subRefundReturnDao.updateSubThirdRefundAmount(returnPayId, refundAmount);
	}

	@Override
	public int updateNewSubPlatRefund(Long refundId, BigDecimal alpRefundAmount, BigDecimal platformAmount, Integer isDepositHandleSucces,
									  Integer isHandleSuccess, String refundType, String depositHandleType, String outRefundNo, String depositOutRefundNo) {
		return subRefundReturnDao.updateNewSubPlatRefund(refundId, alpRefundAmount, platformAmount, isDepositHandleSucces,
				isHandleSuccess, refundType, depositHandleType, outRefundNo, depositOutRefundNo);
	}

	@Override
	public PageSupport<SubRefundReturn> refundList(Long shopId, Long sellerState, Integer refundReturnType, String curPageNO, int pageSize) {
		return subRefundReturnDao.refundList(shopId, sellerState, refundReturnType, curPageNO, pageSize);
	}

	@Override
	public PageSupport loadShopOrderBill(Long shopId, Long type, String sn, String curPageNO, int pageSize) {
		return subRefundReturnDao.loadShopOrderBill(shopId, type, sn, curPageNO, pageSize);
	}

	@Override
	public PageSupport<SubRefundReturn> refundList(String userId, Integer refundReturnType, String curPageNO, int pageSize) {
		return subRefundReturnDao.refundList(userId, refundReturnType, curPageNO, pageSize);
	}

	/**
	 * 获取用户退款退货记录列表
	 *
	 * @param userId    用户id
	 * @param curPageNO 当前页码
	 * @param status    处理状态
	 * @return
	 */
	@Override
	public PageSupport<SubRefundReturn> querySubRefundReturnList(String userId, String curPageNO, Integer status) {

		return subRefundReturnDao.querySubRefundReturnList(userId, curPageNO, status);
	}

	/**
	 * 再次提交退货退款申请
	 */
	@Override
	public void submitAgainApplySubRefundReturn(SubRefundReturn refund) {

		refund.setApplyState(RefundReturnStatusEnum.ORDER_REFUND_PROCESSING.value());
		refund.setSellerMessage(null);
		refund.setSellerState(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
		refund.setSellerTime(null);
		refund.setGoodsState(0);
		refund.setExpressName("");
		refund.setExpressNo("");
		refund.setShipTime(null);
		refund.setReceiveTime(null);
		refund.setReceiveMessage("");

		if (refund.getApplyType() == RefundReturnTypeEnum.REFUND.value()) { //仅退款
			if (refund.getSubItemId() == 0) { //订单全退款
				saveApplyOrderRefund(refund);
			} else {//订单项退款
				saveApplyItemRefund(refund);
			}
		} else {
			saveApplyItemReturn(refund);
		}

	}

	@Override
	public SubRefundReturn getUnfinishedSubRefundReturn(Long subId, Long subItemId, String userId) {
		return subRefundReturnDao.getUnfinishedSubRefundReturn(subId, subItemId, userId);
	}

	@Override
	public void returnProcess(Long subId, Long subItemId, String userId) {
		SubRefundReturn subRefundReturn = this.getUnfinishedSubRefundReturn(subId, subItemId, userId);
		if (AppUtils.isNotBlank(subRefundReturn)) {
			this.undoApplySubRefundReturn(subRefundReturn);
		}
	}

	/**
	 * 商家收到退货订单日志
	 *
	 * @param sub
	 */
	private void saveOrderHistoryProduct(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNGOOD.value());
		subHistory.setUserName("商家");
		subHistory.setReason("[退货/退款]商家" + "于" + time + "商家收到退货 ");
		subHistoryService.saveSubHistory(subHistory);
	}
}
