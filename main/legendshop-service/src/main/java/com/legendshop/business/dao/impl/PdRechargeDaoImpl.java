/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PdRechargeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.util.AppUtils;

/**
 *预存款充值Dao
 */
@Repository
public class PdRechargeDaoImpl extends GenericDaoImpl<PdRecharge, Long> implements PdRechargeDao {

	public PdRecharge getPdRecharge(Long id) {
		return getById(id);
	}

	public int deletePdRecharge(PdRecharge pdRecharge) {
		return delete(pdRecharge);
	}

	public Long savePdRecharge(PdRecharge pdRecharge) {
		return save(pdRecharge);
	}

	public int updatePdRecharge(PdRecharge pdRecharge) {
		return update(pdRecharge);
	}

	@Override
	public int deleteRechargeDetail(String userId, String pdrSn) {
		return update("delete from ls_pd_recharge where sn=? and user_id=? and payment_state=0 ", new Object[] { pdrSn, userId });
	}

	@Override
	public PdRecharge findRechargePay(String userId, String paySn, Integer status) {
		List<PdRecharge> list = this.queryByProperties(new EntityCriterion().eq("sn", paySn).eq("userId", userId).eq("paymentState", status));
		if (AppUtils.isNotBlank(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public PdRecharge findRechargePay(String sn) {
		PdRecharge pdRecharge = this.getByProperties(new EntityCriterion().eq("sn", sn));
		return pdRecharge;
	}

	@Override
	public PageSupport<PdRecharge> getRechargeDetailedPage(String curPageNO, String userId, String state) {
		curPageNO = AppUtils.isBlank(curPageNO) ? "1" : curPageNO;
		CriteriaQuery cq = new CriteriaQuery(PdRecharge.class, curPageNO);
		cq.setPageSize(20);
		cq.eq("userId", userId);
		cq.like("paymentState", state);
		cq.addDescOrder("addTime");
		return queryPage(cq);
	}

	@Override
	public PageSupport<PdRecharge> getPdRechargeListPage(String curPageNO, PdRecharge pdRecharge, Integer pageSize, DataSortResult result) {
		SimpleSqlQuery query = new SimpleSqlQuery(PdRecharge.class, curPageNO);
		QueryMap paramMap = new QueryMap();
		if(AppUtils.isNotBlank(pdRecharge.getNickName())) {
			paramMap.like("nickName", pdRecharge.getNickName().trim());
		}
		if(AppUtils.isNotBlank(pdRecharge.getUserName())) {
			paramMap.like("userName", pdRecharge.getUserName().trim());
		}
		paramMap.put("paymentState", pdRecharge.getPaymentState());
		if (AppUtils.isNotBlank(pageSize)) {
			query.setPageSize(pageSize);
		}
		// 判断是否为外部排序
		if (!result.isSortExternal()) {
			paramMap.put(Constants.ORDER_INDICATOR, "ORDER BY r.payment_state ASC,r.add_time DESC,r.payment_time DESC");
		}else{
			paramMap.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		String QueryNsortCount = ConfigCode.getInstance().getCode("preDeposit.getPdRechargeListCount", paramMap);
		String QueryNsort = ConfigCode.getInstance().getCode("preDeposit.getPdRechargeList", paramMap);
		query.setAllCountString(QueryNsortCount);
		query.setQueryString(QueryNsort);
		query.setCurPage(curPageNO);
		paramMap.remove(Constants.ORDER_INDICATOR);
		query.setParam(paramMap.toArray());
		return querySimplePage(query);
	}

	@Override
	public Integer batchDelete(String ids) {

		String[] pdIds=ids.split(",");
		StringBuffer buffer=new StringBuffer();
		buffer.append("DELETE FROM ls_pd_recharge WHERE id IN(");

		for(String pdId:pdIds){
			buffer.append("?,");
		}
		buffer.deleteCharAt(buffer.length()-1);
		buffer.append(")");
		return update(buffer.toString(), pdIds);
	}

	/**
	 * 充值明细列表
	 * @param userId 用户Id
	 * @param state 支付状态 0未支付1支付
	 * @param pdrSn 唯一的流水号
	 * @param curPageNO 当前页
	 * @param pageSize 每页大小
	 *
	 */
	@Override
	public PageSupport<PdRecharge> getPdRechargePage(String userId, String state,String pdrSn, String curPageNO, int pageSize) {

		pageSize=pageSize == 0?10:pageSize;
		curPageNO=curPageNO == null?"1":curPageNO;

		CriteriaQuery cq = new CriteriaQuery(PdRecharge.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("userId", userId);
		cq.like("paymentState", state);
		cq.eq("sn", pdrSn);
		cq.addDescOrder("addTime");
		PageSupport ps = queryPage(cq);
		return queryPage(cq);
	}

}
