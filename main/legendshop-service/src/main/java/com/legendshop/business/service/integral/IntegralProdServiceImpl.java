/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.integral;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.integral.IntegralProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.spi.service.IntegralProdService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 积分商品服务实现类.
 */
@Service("integralProdService")
public class IntegralProdServiceImpl implements IntegralProdService {

	@Autowired
	private IntegralProdDao integralProdDao;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 获取积分商品
	 */
	public IntegralProd getIntegralProd(Long id) {
		return integralProdDao.getIntegralProd(id);
	}

	/**
	 * 删除积分商品
	 */
	public void deleteIntegralProd(IntegralProd integralProd) {
		int result = integralProdDao.deleteIntegralProd(integralProd);
		if (result > 0) {
			// 删除附件信息
			attachmentManager.delAttachmentByFilePath(integralProd.getProdImage());
		}
	}

	/**
	 * 保存积分商品
	 */
	public Long saveIntegralProd(IntegralProd integralProd,String userName, String userId, Long shopId) {
		if (!AppUtils.isBlank(integralProd.getId())) {
			IntegralProd prod = buildIntegralProd(integralProd);
			if (prod != null) {
				updateIntegralProd(prod);
			}
			return prod.getId();
		}
		integralProd.setAddTime(new Date());
		integralProd.setSaleNum(0);
		integralProd.setViewNum(0);
		/** 处理品牌 logo **/
		if (integralProd.getImageFile() != null && integralProd.getImageFile().getSize() > 0) {
			String pic = attachmentManager.upload(integralProd.getImageFile());
			integralProd.setProdImage(pic);
		}
		Long id = integralProdDao.saveIntegralProd(integralProd);
		if (AppUtils.isNotBlank(integralProd.getProdImage())) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, integralProd.getProdImage(), integralProd.getImageFile(), AttachmentTypeEnum.INTEGRAL);
		}

		return id;
	}

	/**
	 * 创建积分商品
	 * @param integralProd
	 * @return
	 */
	private IntegralProd buildIntegralProd(IntegralProd integralProd) {
		IntegralProd orgin = integralProdDao.getById(integralProd.getId());
		if (orgin == null) {
			return null;
		}
		orgin.setName(integralProd.getName());
		orgin.setPrice(integralProd.getPrice());
		orgin.setExchangeIntegral(integralProd.getExchangeIntegral());
		orgin.setProdStock(integralProd.getProdStock());
		orgin.setFirstCid(integralProd.getFirstCid());
		orgin.setTwoCid(integralProd.getTwoCid());
		orgin.setThirdCid(integralProd.getThirdCid());
		orgin.setCategoryName(integralProd.getCategoryName());
		orgin.setProdNumber(integralProd.getProdNumber());
		orgin.setStatus(integralProd.getStatus());
		orgin.setIsCommend(integralProd.getIsCommend());
		orgin.setIsLimit(integralProd.getIsLimit());
		orgin.setLimitNum(integralProd.getLimitNum());
		orgin.setProdDesc(integralProd.getProdDesc());
		orgin.setSeoKeyword(integralProd.getSeoKeyword());
		orgin.setSeoDesc(integralProd.getSeoDesc());

		String image = orgin.getProdImage();
		/** 处理品牌 logo **/
		if (integralProd.getImageFile() != null && integralProd.getImageFile().getSize() > 0) {
			String pic = attachmentManager.upload(integralProd.getImageFile());
			if (pic != null) {
				orgin.setProdImage(pic);
				if (AppUtils.isNotBlank(image)) {
					// 删除附件信息
					attachmentManager.delAttachmentByFilePath(image);
					// 真实删除文件
					attachmentManager.deleteTruely(image);
				}

			}
		}
		return orgin;
	}

	/**
	 * 更新积分商品
	 */
	public void updateIntegralProd(IntegralProd integralProd) {
		integralProdDao.updateIntegralProd(integralProd);
	}

	/**
	 * 更新积分商品状态
	 */
	@Override
	@CacheEvict(value = "IntegralProdDto", key = "#integralProd.id")
	public void updateIntegralProdStatus(IntegralProd integralProd) {
		integralProdDao.update("update ls_integral_prod set status=? where id=?", new Object[] { integralProd.getStatus(), integralProd.getId() });
	}

	/**
	 * 查找积分分类
	 */
	@Override
	public List<Category> findIntegralCategory() {
		List<Category> categories = integralProdDao.findIntegralCategory();
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		return categories;
	}

	/**
	 * 查找积分兑换列表
	 */
	@Override
	public List<IntegralProd> findExchangeList(Long firstCid, Long twoCid, Long thirdCid) {
		List<IntegralProd> integralProds = integralProdDao.findExchangeList(firstCid, twoCid, thirdCid);
		return integralProds;
	}

	/**
	 * 查找积分商品
	 */
	@Override
	@Cacheable(value = "IntegralProdDto", key = "#id")
	public IntegralProd findIntegralProd(Long id) {
		IntegralProd integralProd = integralProdDao.getIntegralProd(id);
		if (integralProd != null) {
			if (AppUtils.isNotBlank(integralProd.getFirstCid())) {
				Category category = findCategory(integralProd.getFirstCid());
				if (category != null) {
					integralProd.setCategoryName(category.getName());
				}
			}
			if (AppUtils.isNotBlank(integralProd.getTwoCid())) {
				Category category = findCategory(integralProd.getTwoCid());
				if (category != null) {
					integralProd.setTwoCategoryName(category.getName());
				}
			}
			if (AppUtils.isNotBlank(integralProd.getThirdCid())) {
				Category category = findCategory(integralProd.getThirdCid());
				if (category != null) {
					integralProd.setThirdCategoryName(category.getName());
				}
			}
		}
		return integralProd;
	}

	/**
	 * 查找分类
	 */
	public Category findCategory(Long categoryId) {
		List<Category> categories = integralProdDao.findAllIntegralCategory();
		if (AppUtils.isBlank(categories)) {
			return null;
		}
		for (Category category : categories) {
			if (category.getId().equals(categoryId)) {
				return category;
			}
		}
		return null;
	}

	/**
	 * 获取不同提供方的热门优惠券
	 */
	@Override
	public List<Coupon> getCouponByProvider(String yType, String pType, Date date) {

		return this.integralProdDao.getCouponByProvider(yType, pType, date);
	}

	/**
	 * 获取优惠券
	 */
	@Override
	public Coupon getCoupon(Long cid) {

		return this.integralProdDao.getCoupon(cid);
	}

	/**
	 * 获取积分商品页面
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO) {
		return integralProdDao.getIntegralProdPage(curPageNO);
	}

	/**
	 * 获取积分商品
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid) {
		return integralProdDao.getIntegralProd(curPageNO, firstCid, twoCid, thirdCid);
	}

	/**
	 * 获取积分商品
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid, Integer startIntegral, Integer endIntegral,
			String integralKeyWord, String orders) {
		return integralProdDao.getIntegralProd(curPageNO, firstCid, twoCid, thirdCid, startIntegral, endIntegral, integralKeyWord, orders);
	}

	/**
	 * 获取积分商品页面
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO, IntegralProd integralProd, DataSortResult result) {
		return integralProdDao.getIntegralProdPage(curPageNO, integralProd, result);
	}

	@Override
	public Integer batchDelete(String ids) {
		return integralProdDao.batchDelete(ids);
	}

	@Override
	public Integer batchOffline(String ids) {
		return integralProdDao.batchOffline(ids);
	}

}
