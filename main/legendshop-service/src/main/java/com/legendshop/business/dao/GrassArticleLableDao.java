/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticleLable;

import java.util.Collection;
import java.util.List;

/**
 * The Class GrassArticleLableDao. 种草社区文章关联标签表Dao接口
 */
public interface GrassArticleLableDao extends Dao<GrassArticleLable,Long>{

	/**
	 * 根据Id获取种草社区文章关联标签表
	 */
	public abstract GrassArticleLable getGrassArticleLable(Long id);

	/**
	 *  根据Id删除种草社区文章关联标签表
	 */
    public abstract int deleteGrassArticleLable(Long id);

	/**
	 *  根据对象删除
	 */
    public abstract int deleteGrassArticleLable(GrassArticleLable grassArticleLable);

	/**
	 * 保存种草社区文章关联标签表
	 */
	public abstract Long saveGrassArticleLable(GrassArticleLable grassArticleLable);

	/**
	 *  更新种草社区文章关联标签表
	 */		
	public abstract int updateGrassArticleLable(GrassArticleLable grassArticleLable);

	/**
	 * 分页查询种草社区文章关联标签表列表, TODO 需要根据业务,查询条件
	 */
	public abstract PageSupport<GrassArticleLable> queryGrassArticleLable(String curPageNO, Integer pageSize);

	public void deleteByGrassId(Long graid);

	/**
	 * 根据标签获取文章
	 * @param label
	 * @return
	 */
    List<Long> getArticleLableByLid(Long label);
}
