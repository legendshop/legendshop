/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.SubRefundRetuenDto;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;

/**
 *退款表Dao
 */
public interface SubRefundReturnDao extends Dao<SubRefundReturn, Long> {
	
	public abstract List<SubRefundReturn> getSubRefundsSuccessBySettleSn(String settleSn);

	public abstract SubRefundReturn getSubRefundReturn(Long id);
	
    public abstract int deleteSubRefundReturn(SubRefundReturn subRefundReturn);
	
	public abstract Long saveSubRefundReturn(SubRefundReturn subRefundReturn);
	
	public abstract int updateSubRefundReturn(SubRefundReturn subRefundReturn);
	
	public abstract int updateSubPlatRefund(Long refundId,BigDecimal refundAmount,Integer is_handle_success,String refundType);
	
	public abstract int updateSubThirdRefundAmount(Long returnPayId, BigDecimal refundAmount);

	/**
	 * 查询订单信息,用与申请页面的订单信息回显
	 * @param orderId
	 * @return
	 */
	public abstract ApplyRefundReturnDto getOrderInfo(Long orderId,String userId);

	/**
	 * 查询订单项信息,用与申请页面的订单项信息回显
	 * @param orderItemId
	 * @return
	 */
	public abstract ApplyRefundReturnDto getOrderItemInfo(Long orderItemId,String userId);

	/**
	 * 重新绑定 申请编号(商户退款单号)
	 * @param returnPayId
	 * @param batch_no
	 */
	public abstract void updateRefundSn(Long returnPayId, String batch_no);


	public abstract  int orderReturnFail(String refundSn);

	int orderReturnSucess(String refundSn, String outRefundNo,String handleType);
	
	int orderReturnOnlineSettle(String refundSn, String outRefundNo,Double refundAmount,String handleType);

	
	
	/**
	 * 获取商家当期内所有退款成功的退款单金额
	 * @param shopId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public abstract double getSubRefundSuccess(Long shopId, Date startDate,
			Date endDate);

	/**
	 * 获取商家当期内所有退款成功的退款单
	 * @param shopId
	 * @param endDate
	 * @return
	 */
	public abstract List<SubRefundReturn> getSubRefundSuccess(Long shopId, Date endDate);
	
	
	/**
	 * 检查该该订单项参加退换货的记录信息(包含全部订单退还)
	 *  并且退换货的记录状态处于待审核或者完成状态,并且是未处理过结算的
	 * @param subNumber
	 * @param subItemId
	 * @return
	 */
	public abstract SubRefundReturn getSubRefund(String subNumber,
			Long subItemId);

	/**
	 * @param subNumber
	 * @return
	 */
	public abstract SubRefundReturn getSubRefundReturnBySubNumber(String subNumber);
	
	public abstract SubRefundReturn getSubRefundReturnBySubItemId(String subItemId);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, String status,
			RefundSearchParamDto paramData);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturn(String curPageNO, String status,
			RefundSearchParamDto paramData);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, ShopOrderBill shopOrderBill,
			String subNumber);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, Long shopId, String subNumber,
			ShopOrderBill shopOrderBill);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO,
			RefundSearchParamDto paramData, Long shopId);

	public abstract PageSupport<SubRefundReturn> getSubReturnListPage(String curPageNO, RefundSearchParamDto paramData,
			Long shopId);

	public abstract PageSupport<SubRefundReturn> getRefundListPage(String curPageNO, RefundSearchParamDto paramData,
			String userId);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnMobile(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	public abstract PageSupport<SubRefundReturn> getSubRefundReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	public abstract PageSupport<SubRefundReturn> getReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	public abstract List<SubRefundRetuenDto> exportOrder(String string, Object[] array);
	
	/**
	 * 根据申请编号查找退款单
	 * @param refundSn
	 * @return
	 */
	public abstract SubRefundReturn getSubRefundReturnByRefundSn(String refundSn);

	/**
	 * 退款更新
	 * @param refundId  退款ID
	 * @param alpRefundAmount 第三方退款金额
	 * @param platformAmount  平台退款金额
	 * @param isDepositHandleSucces 订金处理状态
	 * @param isHandleSuccess  退款单处理状态
	 * @param refundType 退款单处理方式
	 * @param depositHandleType 订金退款处理方式
	 * @return
	 */
	public abstract int updateNewSubPlatRefund(Long refundId, BigDecimal alpRefundAmount, BigDecimal platformAmount, Integer isDepositHandleSucces,
			Integer isHandleSuccess, String refundType, String depositHandleType, String outRefundNo, String depositOutRefundNo);

	/**
	 * 获取退款列表
	 */
	PageSupport<SubRefundReturn> refundList(Long shopId, Long sellerState,Integer refundReturnType,  String curPageNO, int pageSize);

	PageSupport loadShopOrderBill(Long shopId, Long type, String sn, String curPageNO, int pageSize);

	PageSupport<SubRefundReturn> refundList(String userId, Integer refundReturnType, String curPageNO, int pageSize);

	/**
	 * 获取用户退款退货记录列表
	 * @param userId 用户id
	 * @param curPageNO 当前页码
	 * @param status 处理状态
	 * @return
	 */
	public abstract PageSupport<SubRefundReturn> querySubRefundReturnList(String userId, String curPageNO,
			Integer status);

	public abstract List<SubRefundReturn> getSubRefundsSuccessBySubNumber(String subNumber);

	/**
	 * 获取未完成的订单
	 * @param subId
	 * @param subItemId
	 * @param userId
	 * @return
	 */
	SubRefundReturn getUnfinishedSubRefundReturn(Long subId, Long subItemId, String userId);
}




