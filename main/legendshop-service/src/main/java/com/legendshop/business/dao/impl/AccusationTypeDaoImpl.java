/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.AccusationTypeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.AccusationType;

/**
 * 举报类型Dao
 */
@Repository
public class AccusationTypeDaoImpl extends GenericDaoImpl<AccusationType, Long> implements AccusationTypeDao {
     
    public List<AccusationType> getAccusationType(String userName){
    	EntityCriterion criterion = new EntityCriterion();
   		return this.queryByProperties(criterion);
    }

	public AccusationType getAccusationType(Long id){
		return getById(id);
	}
	
    public void deleteAccusationType(AccusationType accusationType){
    	delete(accusationType);
    }
	
	public Long saveAccusationType(AccusationType accusationType){
		return (Long)save(accusationType);
	}
	
	public void updateAccusationType(AccusationType accusationType){
		 update(accusationType);
	}

	@Override
	public List<AccusationType> queryAccusationType() {
		return query("select type_id as typeId,name as name  from ls_accu_type where status =1",AccusationType.class);
	}

 }
