/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.Date;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.integral.IntegraLog;

/**
 * 会员积分日志明细Dao.
 */
public interface IntegraLogDao extends GenericDao<IntegraLog, Long> {
     
	IntegraLog getIntegraLog(Long id);
	
    int deleteIntegraLog(IntegraLog integraLog);
	
	Long saveIntegraLog(IntegraLog integraLog);
	
	int updateIntegraLog(IntegraLog integraLog);
	
	PageSupport<IntegraLog> getIntegraLogPage(String userId, String curPageNO);

	PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, Integer pageSize, DataSortResult result);

	PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, String userId, Integer pageSize, DataSortResult result);

	/**
	 * 列表查询会员积分日志明细
	 */
	PageSupport<IntegraLog> getIntegraLog(String userId, String curPageNO, String logDesc, Long logType, Date startDate, Date endDate);

	/**
	 * 可通过获取或使用状况查询会员积分日志明细列表
	 */
	PageSupport<IntegraLog> getIntegraLogByIsUsed(String userId, String curPageNO, Integer type, Long logType);

}
