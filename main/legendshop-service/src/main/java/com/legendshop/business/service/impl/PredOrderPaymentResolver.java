package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.dao.PdRechargeDao;
import com.legendshop.business.dao.SubSettlementDao;
import com.legendshop.business.dao.SubSettlementItemDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.spi.service.IPaymentResolver;
import com.legendshop.spi.service.PreDepositService;
import com.legendshop.util.AppUtils;

/**
 * 预付款业务处理
 *
 */
@Service("predOrderPaymentResolver")
public class PredOrderPaymentResolver implements IPaymentResolver {

	@Autowired
	private PdRechargeDao pdRechargeDao;
	 
	@Autowired
	private SubSettlementDao subSettlementDao;
	 
	@Autowired
	private SubSettlementItemDao subSettlementItemDao;
	 
	@Autowired
	private PreDepositService preDepositService;
	
	@Override
	public Map<String, Object> queryPayto(Map<String, String> params) {
		
		String userId=params.get("userId");
		String paySn=params.get("subNumber");
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("result", false);
		
		PdRecharge pdRecharge = pdRechargeDao.findRechargePay(userId, paySn, 0);
		if (AppUtils.isBlank(pdRecharge)) {
			response.put("message", "没有需要支付的充值单据");
			return response;
		}
		
		String subject= "预付款充值:"+paySn;
		response.put("result", true);
		response.put("subject",subject);
		response.put("totalAmount",pdRecharge.getAmount());
		return response;
	}
	
	

	@Override
	public void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallback) throws BusinessException {
		if (subSettlement.getIsClearing()) { // 没有清算
			return;
		}
		
		if(OrderStatusEnum.UNPAY.value().equals(bankCallback.getValidateStatus().value())){ 
			 //没有付款
			  PaymentLog.info("###################### doBankCallback subSettlementSn={} 开始处理 预付款充值业务 ########################",subSettlement.getSubSettlementSn());
			  Date now=new Date();
			  subSettlement.setFlowTradeNo(bankCallback.getFlowTradeNo());
			  subSettlement.setClearingTime(now);
			  subSettlement.setIsClearing(true);
			  int settlementResult=subSettlementDao.updateSubSettlementForPay(subSettlement);
			  
			if (settlementResult == 0) {
				PaymentLog.info("Settlement record has settled。 TradeNo is  {}", bankCallback.getFlowTradeNo());
				return;
			}
			
			List<SubSettlementItem> subSettlementItems=subSettlementItemDao.getSubSettlementItems(subSettlement.getSubSettlementSn());
			  
			if (subSettlementItems == null) {
				PaymentLog.error("支付异常,预付款订单缺失, doBankCallback error subs is null by subSettlementSn={}", subSettlement.getSubSettlementSn());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}
			
			SubSettlementItem settlementItem=subSettlementItems.get(0);
			
			PdRecharge pdRecharge = pdRechargeDao.findRechargePay(settlementItem.getUserId(), settlementItem.getSubNumber(), 0);
			if (AppUtils.isBlank(pdRecharge)) {
				PaymentLog.error("支付异常,预付款订单缺失, doBankCallback error subs is null by RechargeSn={}", settlementItem.getSubNumber());
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}
			
			pdRecharge.setTradeSn(bankCallback.getFlowTradeNo());;
			pdRecharge.setPaymentCode(subSettlement.getPayTypeId());
			pdRecharge.setPaymentName(subSettlement.getPayTypeName());
			pdRecharge.setPaymentTime(new Date());
			pdRecharge.setPaymentState(1);
			
			preDepositService.rechargeForPaySucceed(pdRecharge);
			PaymentLog.info("###################### doBankCallback subSettlementSn={} 处理预付款充值业务成功  ########################",subSettlement.getSubSettlementSn()); 
		}

	}
}
