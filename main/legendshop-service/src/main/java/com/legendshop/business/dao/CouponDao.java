/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.Date;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;

/**
 * 优惠券Dao.
 */
public interface CouponDao extends Dao<Coupon, Long> {
     
	public List<Coupon> getCouponByShopId(Long  shopId);

	public abstract Coupon getCoupon(Long id);
	
    public abstract int deleteCoupon(Coupon coupon);
	
	public abstract Long saveCoupon(Coupon coupon);
	
	public abstract int updateCoupon(Coupon coupon);
	
	public abstract PageSupport getCoupon(CriteriaQuery cq);
	
	public abstract PageSupport<PersonalCouponDto> query(SimpleSqlQuery query);
	
	public List<Coupon> queryByProperties( EntityCriterion entityCriterion);
	
	public void betchUpdate(List<Coupon> list);

	public List<Coupon> queryCanUsedCoupons(String userId, Double orderTotalAmount,Long shopId);

	public void addCouponUseNum(Long couponId,String userId);

	void reduceCouponUseNum(Long couponId,String userId);

	public UserCoupon getUserCoupon(Long couponId, String userId);

	List<Coupon> getCouponByProvider(String getType, String provider, Date date);

	public PageSupport<Coupon> getCouponsPage(String curPageNO, String type);

	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(String userId, int sizePage, String curPageNO,
			Integer useStatus, String couPro);

	public PageSupport<Coupon> queryCouponPage(String curPageNO, String userName);

	public PageSupport<Coupon> queryCoupon(StringBuffer sql,String curPageNO, StringBuffer sqlCount, List<Object> objects);

	public PageSupport<Coupon> getIntegralCenterList(String curPageNO, String type);

	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(int sizePage, String curPageNO, String userId,
			String couPro);

	public PageSupport<Coupon> queryCoupon(String curPageNO, Coupon coupon, DataSortResult result);

	public PageSupport<UserCoupon> queryUsecouponView(String curPageNO, String sqlString, StringBuilder sqlCount,
			List<Object> objects);

	public PageSupport<Coupon> getPlatform(String curPageNO, Coupon coupon);

	int shopDeleteCoupon(Long couponId);

	PageSupport<Coupon> getIntegralList(String curPageNO, String type, String shopId);

	/**
	 * @Description 根据类型获取卖家的优惠券数量
	 * @param type
	 * @param shopId
	 * @return
	 */
	public long getCouponCountByShopId(String type, String shopId);

	public Integer batchDelete(String platfromIds);

	public int batchOffline(String platfromIds);

	public PageSupport<Coupon> loadSelectCouponByDraw(String curPageNO, String couponName, Date endDate,
			List<Long> couponIds);

	/**
	 * 获取商品优惠券
	 * @param shopId
	 * @param prodId
	 * @return
	 */
	public List<Coupon> getProdsCoupons(Long shopId, Long prodId);

	/**
	 * 根据couponId获取优惠券
	 * @param couponId
	 * @return
	 */
	public Coupon getCouponByCouponId(Long couponId);

	public List<Coupon> getPlatformCoupons(Long shopId);

	PageSupport<Coupon> queryCoupon(SimpleSqlQuery query);

 }
