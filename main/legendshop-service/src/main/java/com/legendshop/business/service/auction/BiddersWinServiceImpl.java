/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.auction;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.BasketDao;
import com.legendshop.business.dao.auction.BiddersWinDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.util.AppUtils;

/**
 * 中标服务实现类.
 */
@Service("biddersWinService")
public class BiddersWinServiceImpl  implements BiddersWinService{
	
	@Autowired
    private BiddersWinDao biddersWinDao;
    
	@Autowired
    private BasketDao basketDao;

    public List<BiddersWin> getBiddersWin(String userId) {
        return biddersWinDao.getBiddersWin(userId);
    }

    public BiddersWin getBiddersWin(Long id) {
        return biddersWinDao.getBiddersWin(id);
    }

    public void deleteBiddersWin(BiddersWin biddersWin) {
        biddersWinDao.deleteBiddersWin(biddersWin);
    }

    public Long saveBiddersWin(BiddersWin biddersWin) {
        if (!AppUtils.isBlank(biddersWin.getId())) {
            updateBiddersWin(biddersWin);
            return biddersWin.getId();
        }
        return biddersWinDao.saveBiddersWin(biddersWin);
    }

    public void updateBiddersWin(BiddersWin biddersWin) {
        biddersWinDao.updateBiddersWin(biddersWin);
    }

	@Override
	public boolean isWin(String userId, Long prodId, Long skuId, Long paimaiId) {
		return biddersWinDao.isWin( userId,  prodId,  skuId,  paimaiId);
	}

	@Override
	public BiddersWin getBiddersWin(String userId, Long id) {
		return biddersWinDao.getBiddersWin(userId, id);
	}

	@Override
	public ShopCartItem findBiddersWinCart(Long paimaiId, Long prodId,
			Long skuId) {
		return basketDao.findBiddersWinCart(paimaiId,prodId,skuId);
	}

	@Override
	public void updateBiddersWin(Long id, String subNember,Long subId, int value) {
		biddersWinDao.updateBiddersWin(id,subNember, subId,value);
	}

	@Override
	public List<BiddersWin> getBiddersWin(Date date, int commitInteval,Integer status) {
		return biddersWinDao.getBiddersWin(date,commitInteval,status);
	}

	@Override
	public List<BiddersWin> getBiddersWinList(Long id) {
		return biddersWinDao.getBiddersWinList(id);
	}

	@Override
	public BiddersWin getBiddersWinByAid(Long aid) {
		return biddersWinDao.getBiddersWinByAid(aid);
	}

	@Override
	public PageSupport<BiddersWin> getBiddersWinPage(String curPageNO, BiddersWin biddersWin) {
		return biddersWinDao.getBiddersWinPage(curPageNO,biddersWin);
	}

	@Override
	public PageSupport<BiddersWin> queryBiddersWinListPage(String curPageNO, String userId) {
		return biddersWinDao.queryBiddersWinListPage(curPageNO,userId);
	}

	@Override
	public PageSupport<BiddersWin> queryUserPartAution(String curPageNO, String userId) {
		return biddersWinDao.queryUserPartAution(curPageNO,userId);
	}
	
	/**
	 * 清空缓存
	 * @param id
	 */
	@CacheEvict(value = "AuctionsDto", key = "#id")
	public void cleanAuctions(Long id) {
		
	}

	/**
	 * 清空缓存
	 * @param id
	 */
	@CacheEvict(value = "AuctionsDetailDto", key = "#id")
	public void cleanAuctionsDetail(Long id) {

	}
}
