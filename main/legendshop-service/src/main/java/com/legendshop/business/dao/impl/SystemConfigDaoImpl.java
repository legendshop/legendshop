/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SystemConfigDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.SystemConfig;
/**
 * 系统配置
 */
@Repository("systemConfigDao")
public class SystemConfigDaoImpl extends GenericDaoImpl<SystemConfig, Long> implements SystemConfigDao {
	
//	@Cacheable(value="SystemConfig")
	public SystemConfig getSystemConfig(){
		SystemConfig systemConfig = getById(1l);
		String qqNumber=systemConfig.getQqNumber();
		//处理QQ号码
		if(qqNumber!=null&&qqNumber!=""){
			List<String> qqNumberList=new ArrayList<>();
			if(qqNumber!=null&&qqNumber!=""&&qqNumber.indexOf(",")!=-1){
				qqNumberList=Arrays.asList(qqNumber.split("[,]"));
			}else if(qqNumber!=null&&qqNumber!=""&&qqNumber.indexOf(",")==-1){
				qqNumberList.add(qqNumber);
			}
			systemConfig.setQqList(qqNumberList);	
		}
		return systemConfig;
	}
	
	@CacheEvict(value="SystemConfig",allEntries=true)
	public Long saveSystemConfig(SystemConfig systemConfig){
		return (Long)save(systemConfig);
	}
	
	@CacheEvict(value="SystemConfig",allEntries=true)
	public void updateSystemConfig(SystemConfig systemConfig){//TODO 要先load出来再修改
		 update(systemConfig);
	}

	@Override
	public SystemConfig getSysWechatCode() {
		SystemConfig systemConfig = getById(1l);
		return systemConfig;
	}
	
	
 }
