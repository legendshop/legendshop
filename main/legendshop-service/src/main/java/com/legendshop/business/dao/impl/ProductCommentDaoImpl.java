/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.base.cache.ProductCommentUpdate;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.AppQueryProductCommentsParams;
import com.legendshop.biz.model.dto.ShopProductCommentDto;
import com.legendshop.business.dao.ProdUsefulDao;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ProductCommentsQueryParams;
import com.legendshop.model.dto.ShopCommProdDto;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.entity.ProdUseful;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.model.form.ProductForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 产品评论Dao.
 */
@Repository
public class ProductCommentDaoImpl extends GenericDaoImpl<ProductComment, Long>  implements ProductCommentDao {

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Autowired
	private SubDao subDao;

	@Autowired
	private ProdUsefulDao prodUsefulDao;

	static String productSql = "SELECT p.prod_id AS prodId,p.cash,p.pic,p.name AS prodName FROM ls_prod p WHERE  EXISTS (SELECT DISTINCT prod_id FROM ls_favorite f WHERE  p.prod_id = f.prod_id  AND p.prod_id <> ? AND f.user_id IN (SELECT  user_id FROM ls_favorite WHERE prod_id = ? AND  user_id <> ?))";
	
	static String prodCommentSql="select  c.id as id, c.content as content, c.addtime as addtime, c.score as score, c.useful_counts as usefulCounts, c.replay_counts as replayCounts, d.portrait_pic as photo, d.nick_name as userName, g.name as gradeName,c.sub_item_id as subItemId,c.photo_file1 photoFile1, " +
			"c.photo_File2 photoFile2,c.photo_file3 photoFile3,c.photo_file4 photoFile4,c.photo_file5 photoFile5,d.portrait_pic photo from ls_prod_comm c,ls_usr_detail d,ls_usr_grad g where  d.user_id = c.user_id and d.grade_id = g.grade_id and c.sub_item_id = ?";

	@Override
	public void deleteProductComment(Long prodId) {
		this.update("delete from ls_prod_comm where prod_id = ?", prodId);
	}

	@Override
	@ProductCommentUpdate
	public Long saveProductComment(ProductComment productComment) {
		return save(productComment);
	}

	@Override
	@ProductCommentUpdate
	public void updateProductComment(ProductComment productComment) {
		update(productComment);

	}

	@Override
	public void deleteProductCommentById(Long id) {
		ProductComment productComment = getById(id);
		if (productComment != null) {
			delete(productComment);
		}
	}
	
	// 用户是否可以发表商品评论
	@Override
	public String validateComment(Long prodId, String userName) {
		String level = systemParameterUtil.getCommentLevel();
		if (Constants.LOGONED_COMMENT.equals(level)) {
			if (AppUtils.isBlank(userName)) {
				return "NOLOGON"; // 没有登录
			}
		} else if (Constants.BUYED_COMMENT.equals(level)) {
			if (AppUtils.isBlank(userName)) {
				return "NOLOGON"; // 没有登录
			}
			if (!subDao.isUserOrderProduct(prodId, userName)) {
				return "NOBUYED";
			}
		} else if (Constants.NO_COMMENT.equals(level)) {
			return "NOCOMMENT";
		}
		return null;
	}

	/**
	 * 计算商品的评论得分
	 */
	@Override
	@Cacheable(value = "ProductCommentCategory", key = "#prodId")
	public ProductCommentCategory initProductCommentCategory(Long prodId) {
		ProductCommentCategory pcc = new ProductCommentCategory();
		String sql = "SELECT score, count(id) as result FROM ls_prod_comm WHERE status = 1 and prod_id = ? GROUP BY score";
		
		//统计追评和有图的数量
		String sql2 = "SELECT " +
				"(SELECT COUNT(1) FROM ls_prod_comm c LEFT JOIN ls_prod_add_comm ac ON ac.prod_comm_id = c.id WHERE 1 = 1 AND c.prod_id = ? AND ((c.photos IS NOT NULL AND c.photos !='' AND c.status = 1) OR (ac.photos IS NOT NULL AND ac.status = 1)) ) AS photo, " +
				"(SELECT COUNT(1) FROM ls_prod_comm lpc, ls_prod_add_comm lpac WHERE 1 = 1 AND lpc.prod_id = ? AND lpc.status = 1 AND lpc.is_add_comm = 1 AND lpac.status = 1 AND lpac.prod_comm_id = lpc.id) AS append, " +
				"(SELECT COUNT(1) FROM ls_prod_comm c LEFT JOIN ls_prod_add_comm ac ON ac.prod_comm_id = c.id WHERE c.status = 1 AND prod_id = ? AND (c.is_reply = 0 OR (c.is_add_comm = 1 AND ac.is_reply = 0))) AS waitReply " +
				"FROM DUAL";
		
		
		List<ScoreCategory> result1 = this.query(sql, new Object[] { prodId }, new ProductCommentCategoryRowMapper());
		
		Map<String, Object> result2 = this.queryForMap(sql2, prodId, prodId, prodId);
		
		if (AppUtils.isNotBlank(result1)) {
			for (ScoreCategory scoreCategory : result1) {  
				// score : 0 - 5
				if (scoreCategory.getSocre() >= 4) {
					pcc.setHigh(pcc.getHigh() + scoreCategory.getResult());
				} else if (scoreCategory.getSocre() >= 3) {
					pcc.setMedium(pcc.getMedium() + scoreCategory.getResult());
				} else {
					pcc.setLow(pcc.getLow() + scoreCategory.getResult());
				}
			}
		}
		
		if(AppUtils.isNotBlank(result2)){
			Long photo = (Long) result2.get("photo");
			Long append = (Long) result2.get("append");
			Long waitReply = (Long) result2.get("waitReply");
			
			pcc.setPhoto(photo.intValue());
			pcc.setAppend(append.intValue());
			pcc.setWaitReply(waitReply.intValue());
		}
		
		int total = pcc.getHigh() + pcc.getMedium() + pcc.getLow();
		if (total > 0) {
			pcc.setTotal(total);
			pcc.setHighRate(Arith.div(pcc.getHigh(), total));
			pcc.setMediumRate(Arith.div(pcc.getMedium(), total));
			pcc.setLowRate(Arith.div(pcc.getLow(), total));
		}
		
		pcc.setProdId(prodId);
		return pcc;
	}
	
	class ProductCommentCategoryRowMapper implements RowMapper<ScoreCategory> {

		@Override
		public ScoreCategory mapRow(ResultSet rs, int arg1) throws SQLException {
			ScoreCategory sc = new ScoreCategory();
			sc.setSocre(rs.getInt("score"));
			sc.setResult(rs.getInt("result"));
			return sc;
		}

	}

	class ScoreCategory implements Serializable {

		private static final long serialVersionUID = 8821497685050946872L;

		private Integer socre;

		private Integer result;

		public Integer getSocre() {
			return socre;
		}

		public void setSocre(Integer socre) {
			this.socre = socre;
		}

		public Integer getResult() {
			return result;
		}

		public void setResult(Integer result) {
			this.result = result;
		}
	}

	@Override
	public String getproductName(Long prodId) {
		return this.get("select name from ls_prod where prod_id = ?",String.class,prodId);
	}

	/**
	 * 收藏此商品的用户，还收藏其他什么商品
	 */
	@Override
	public List<ProductForm> getProductList(String userId,Long prodId) {
		return queryLimit(productSql, ProductForm.class, 0, 6, prodId, prodId,userId);
	}

	@Override
	public String getOwnerName(Long prodId) {
		return get("select user_name from ls_prod where prod_id = ?",String.class,prodId);
	}

	@Override
	public ProductComment getProdCommentByprodId(Long subItemId) {
		return get(prodCommentSql, ProductComment.class, subItemId);
	}

	@Override
	public int updateUsefulCounts(Long prodComId, String userId) {
		ProdUseful prodUseful = new ProdUseful();
		prodUseful.setProdCommId(prodComId);
		prodUseful.setRecDate(new Date());
		prodUseful.setUserId(userId);
		Long id = prodUsefulDao.saveProdUseful(prodUseful);
		int result = 0;
		if(null != id){
			result = this.update("update ls_prod_comm set useful_counts = useful_counts + 1 where id = ?", prodComId); 
		}
		
		return result;
	}

	/**
	 * 查询用户是否 可以评论此商品，必须要订单状态为已完成
	 */
	@Override
	public boolean canCommentThisProd(Long prodId, Long subItemId, String userId) {
		String sql = "select si.comm_sts from ls_sub_item si, ls_sub s where si.sub_number = s.sub_number and si.sub_item_id = ? and si.user_id = ? AND si.prod_id = ? and s.status = 4";
		Integer commSts = this.get(sql, Integer.class ,subItemId,userId,prodId);
		return commSts!=null && commSts.intValue()==0;
	}

	@Override
	public Long setGoodComments(Long prodId) {
		String sql = "SELECT COUNT(1) FROM ls_prod_comm WHERE prod_id=? AND score > 3";
		return this.getLongResult(sql, prodId);
	}
	
	@Override
	public Long getComments(Long prodId) {
		String sql = "SELECT COUNT(1) FROM ls_prod_comm WHERE prod_id=?";
		return this.getLongResult(sql, prodId);
	}

	@Override
	public int updateIsAddComm(boolean isAddComm, Long prodCommId) {
		String sql = "UPDATE ls_prod_comm SET is_add_comm = ? WHERE id = ?";
		return this.update(sql, isAddComm, prodCommId);
	}

	@Override
	public ProductComment getProductComment(Long commId, String userId) {
		
		String sql = "SELECT * FROM ls_prod_comm WHERE id = ? AND user_id = ?";
		return this.get(sql, ProductComment.class, commId, userId);
	}

	@Override
	public ProductCommentDto getProductCommentDetail(Long prodComId) {
		
		String sql = ConfigCode.getInstance().getCode("prod.queryProductCommentDetail");
		
		return this.get(sql, ProductCommentDto.class, prodComId);
	}
	
	@Override
	public PageSupport<ProductReply> replyCommentList(String curPageNO, Long parentReplyId) {
 		SimpleSqlQuery query = new SimpleSqlQuery(ProductReply.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("prodCommId", parentReplyId);
		String queryAllSQL = ConfigCode.getInstance().getCode("mobile.prod.queryCommentReplysCount", map);
		String querySQL = ConfigCode.getInstance().getCode("mobile.prod.queryCommentReplys", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	@Override
	public PageSupport<ProductComment> prodComment(String curPageNO,Long subId,String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductComment.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName",userName);
		map.put("subId",subId);
		String querySQL = ConfigCode.getInstance().getCode("biz.comment", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("biz.commentCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	@Override
	public PageSupport<ProductComment> prodCommentState(String curPageNO,String state,String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductComment.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("state", state);
		map.put("userName",userName);
		String querySQL = ConfigCode.getInstance().getCode("biz.comment", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("biz.commentCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	@Override
	public PageSupport<ProductCommentDto> queryProdCommentByState(String curPageNO,String state,String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductCommentDto.class, systemParameterUtil.getFrontPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		map.put("state", state);
		map.put("userName",userName);
		String querySQL = ConfigCode.getInstance().getCode("biz.userComments", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("biz.commentsCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public Long findByProdIdandUserIdandItemId(Long prodId, String userId, Long itemId) {
		String sql="SELECT COUNT(*) FROM `ls_prod_comm` WHERE `prod_id`=? AND `sub_item_id`=? AND `user_id`=?";
		return get(sql,Long.class,prodId,itemId,userId);
	}

	@Override
	public PageSupport<ProductComment> querySimplePage(String curPageNO, String userName, Long subId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductComment.class, 15, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		map.put("subId", subId);
		String querySQL = ConfigCode.getInstance().getCode("uc.prod.commentProds", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("uc.prod.commentProdsCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductComment> querySimplePage(String curPageNO, String userName) {
		SimpleSqlQuery query = new SimpleSqlQuery(ProductComment.class, 20, curPageNO);
		QueryMap map = new QueryMap();
		map.put("userName", userName);
		String querySQL = ConfigCode.getInstance().getCode("uc.prod.commentProds", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("uc.prod.commentProdsCount", map);
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}
	
	@Override//
	public PageSupport<ShopCommProdDto> queryProdComment(String curPageNO, Long shopId) {
		SimpleSqlQuery query = new SimpleSqlQuery(ShopCommProdDto.class, 10, curPageNO);
		String queryAllSQL = ConfigCode.getInstance().getCode("shop.getProductCommentCount");
		String querySQL = ConfigCode.getInstance().getCode("shop.getProductComment"); 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(new Object[]{shopId});
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductCommentDto> queryProductCommentDto(ProductCommentsQueryParams params) {
		String curPageNO = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(ProductCommentDto.class, 5, curPageNO);
		QueryMap map = new QueryMap();
		
		map.put("prodId", params.getProdId());
		String condition = params.getCondition();

		if(!"all".equals(condition)){//如果不是全部评论
			if("good".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 4"); 
			}else if("medium".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 3 AND c.score < 4"); 
			}else if("poor".equals(condition)){
				map.put("scoreOperater", " AND c.score < 3");
			}else if("photo".equals(condition)){
				map.put("isHasPhoto", " AND (c.photos IS NOT NULL AND c.photos != '' OR ac.photos IS NOT NULL AND ac.photos != '')");
			}else if("append".equals(condition)){
				map.put("isAddComm", 1);
			}else if("waitReply".equals(condition)){
	    		map.put("waitReply", " AND (c.is_reply = 0 OR (c.is_add_comm = 1 AND ac.is_reply = 0))");
	    	}
		}
 
		String queryAllSQL = ConfigCode.getInstance().getCode("prod.queryProductCommentsCount", map);
		String querySQL = ConfigCode.getInstance().getCode("prod.queryProductComments", map); 
		 
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		 
		map.remove("scoreOperater");
		map.remove("isHasPhoto");
		map.remove("waitReply");
		 
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<ProductCommentDto> getProductCommentList(String curPageNO, ProductComment productComment,
			Integer pageSize, String status, DataSortResult result) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(ProductCommentDto.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);
		
		if (AppUtils.isNotBlank(productComment.getShopId())) {
			map.put("shopId", productComment.getShopId());
		}
		
		if (AppUtils.isNotBlank(productComment.getSiteName())) {
			map.like("siteName", productComment.getSiteName().trim());
		}
	
		if (AppUtils.isNotBlank(productComment.getUserName())) {// 评价人
			map.like("userName", productComment.getUserName().trim());
		}
		
		if (AppUtils.isNotBlank(productComment.getProdName())) {
			map.like("prodName", productComment.getProdName().trim());
		}
		
		if(AppUtils.isNotBlank(pageSize)){
			hql.setPageSize(pageSize);
		}
		
		if (!result.isSortExternal()) {
			map.put(Constants.ORDER_INDICATOR, "ORDER BY c.addtime DESC");
		}else {
			map.put(Constants.ORDER_INDICATOR, result.parseSeq());
		}
		
		if(AppUtils.isNotBlank(status)){
			if(status.equals("0")){
				map.put("auditStatus", "AND (c.status = 0 OR (c.status = 1 AND ac.status = 0))");
			}
		}
		
		hql.setAllCountString(ConfigCode.getInstance().getCode("admin.comment.queryProductCommentsCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("admin.comment.queryProductComments", map));
		
		map.remove(Constants.ORDER_INDICATOR);
		map.remove("auditStatus");
		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}

	@Override
	public PageSupport<ProductCommentDto> getProductCommentListPage(String curPageNO, ProductComment productComment,
			String userId, String userName, Integer pageSize, DataSortResult result) {
		QueryMap map = new QueryMap();
		SimpleSqlQuery hql = new SimpleSqlQuery(ProductCommentDto.class);
		hql.setPageSize(systemParameterUtil.getPageSize());
		hql.setCurPage(curPageNO);

		map.put("userId", userId);
		if (AppUtils.isNotBlank(productComment.getSiteName())) {
			map.like("siteName", productComment.getSiteName().trim());
		}
	
		if (AppUtils.isNotBlank(productComment.getUserName())) {//评价人
			map.like("userName", productComment.getUserName().trim());
		}
		
		if (AppUtils.isNotBlank(productComment.getProdName())) {
			map.like("prodName", productComment.getProdName().trim());
		}
		
		if (AppUtils.isNotBlank(pageSize)) {// 非导出情况
			hql.setPageSize(pageSize);
		}
		
		if (!result.isSortExternal()) {
			map.put(Constants.ORDER_INDICATOR, "ORDER BY c.addtime DESC");
		}
		
		hql.setAllCountString(ConfigCode.getInstance().getCode("admin.comment.queryProductCommentsCount", map));
		hql.setQueryString(ConfigCode.getInstance().getCode("admin.comment.queryProductComments", map));
		
		map.remove(Constants.ORDER_INDICATOR);
		hql.setParam(map.toArray());
		return querySimplePage(hql);
	}

	@Override
	public Integer getWaitReplyShopComm(Long prodId) {
		String sql = "SELECT COUNT(1) FROM ls_prod_comm c LEFT JOIN ls_prod_add_comm ac ON ac.prod_comm_id = c.id WHERE c.status = 1 AND prod_id = ?  AND (c.is_reply = 0 OR (c.is_add_comm = 1 AND ac.status = 1 AND ac.is_reply = 0 ))";
		return this.get(sql, Integer.class, prodId);
	}

	@Override
	public PageSupport getProdCommentList(Long shopId, String curPageNO, int pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(ShopProductCommentDto.class, 10, curPageNO);

		String queryAllSQL = ConfigCode.getInstance().getCode("app.shop.comments.getProductCommentCount");
		String querySQL = ConfigCode.getInstance().getCode("app.shop.comments.getProductComment");

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);

		query.setParam(new Object[]{shopId});
		return querySimplePage(query);
	}

	@Override
	public PageSupport getProdComment(AppQueryProductCommentsParams params, int pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdCommDto.class, 10, String.valueOf(params.getCurPageNO()));
		QueryMap map = new QueryMap();

		map.put("prodId", params.getProdId());
		map.put("shopUserName", params.getShopUserName());

		String condition = params.getCondition();
		if(!"all".equals(condition)){//如果不是全部评论
			if("good".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 4");
			}else if("medium".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 3 AND c.score < 4");
			}else if("poor".equals(condition)){
				map.put("scoreOperater", " AND c.score < 3");
			}else if("photo".equals(condition)){
				map.put("isHasPhoto", " AND (c.photos IS NOT NULL OR ac.photos IS NOT NULL)");
			}else if("append".equals(condition)){
				map.put("isAddComm", 1);
			}else if("waitReply".equals(condition)){
				map.put("waitReply", " AND (c.is_reply = 0 OR (c.is_add_comm = 1 AND ac.is_reply = 0))");
			}
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("app.shop.comments.queryProductCommentsCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.shop.comments.queryProductComments", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("scoreOperater");
		map.remove("isHasPhoto");
		map.remove("waitReply");

		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport<UserCommentProdDto> queryComments(String userName, String curPageNO, int pageSize) {
		SimpleSqlQuery query = new SimpleSqlQuery(UserCommentProdDto.class, 8, curPageNO);

		QueryMap map = new QueryMap();
		map.put("userName", userName);

		String querySQL = ConfigCode.getInstance().getCode("app.comment.queryUserCommProds", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("app.comment.queryUserCommProdsCount", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);

		query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
