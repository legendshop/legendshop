/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.criterion.MatchMode;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.util.AppUtils;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ProdGroupDao;

/**
 * The Class ProdGroupDaoImpl. Dao实现类
 */
@Repository
public class ProdGroupDaoImpl extends GenericDaoImpl<ProdGroup, Long> implements ProdGroupDao{

	/**
	 * 根据Id获取
	 */
	public ProdGroup getProdGroup(Long id){
		return getById(id);
	}

	/**
	 *  删除
	 *  @param prodGroup 实体类
	 *  @return 删除结果
	 */	
    public int deleteProdGroup(ProdGroup prodGroup){
    	return delete(prodGroup);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteProdGroup(Long id){
		return deleteById(id);
	}

	/**
	 * 保存
	 */
	public Long saveProdGroup(ProdGroup prodGroup){
		return save(prodGroup);
	}

	/**
	 *  更新
	 */		
	public int updateProdGroup(ProdGroup prodGroup){
		return update(prodGroup);
	}

	/**
	 * 分页查询列表
	 */
	public PageSupport<ProdGroup>queryProdGroup(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(ProdGroup.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<ProdGroup> ps = queryPage(query);
		return ps;
	}
	
	public List<ProdGroup> getProdGroupByType(Integer type){
		String sql ="SELECT id,name,type,conditional,sort,description,rec_date FROM ls_prod_group WHERE type=?";
		return query(sql, ProdGroup.class,type);
	}

	
	@Override
	public PageSupport<ProdGroup> queryProdGroupPage(String curPageNO,ProdGroup prodGroup, Integer pageSize) {
		CriteriaQuery query = new CriteriaQuery(ProdGroup.class, curPageNO);
		query.setPageSize(pageSize);
		query.addAscOrder("type");
		query.addDescOrder("recDate");
		if (AppUtils.isNotBlank(prodGroup) && AppUtils.isNotBlank(prodGroup.getName())) {
			query.like("name", prodGroup.getName().trim(),MatchMode.ANYWHERE);
		}
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport<ProdGroup> ps = queryPage(query);
		return ps;
	}
	
	@Override
	public boolean checkNameIsExist(String name,Long id) {
		List<String> list = null;
		if(AppUtils.isNotBlank(id)){
			list = query("select id from ls_prod_group where name = ? and id <> ?", String.class, name,id);
		}else{
			list = query("select id from ls_prod_group where name = ?", String.class, name);
		}
	
		return AppUtils.isNotBlank(list);
		
	}
}
