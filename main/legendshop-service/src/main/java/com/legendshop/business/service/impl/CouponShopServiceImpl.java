/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.CouponShopDao;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.CouponShop;
import com.legendshop.spi.service.CouponShopService;

/**
 * 红包 店铺关联服务
 */
@Service("couponShopService")
public class CouponShopServiceImpl  implements CouponShopService{
	
	private static final String getShopNamesByCouponIdSql = "SELECT s.site_name AS shopName FROM ls_coupon_shop cs , ls_shop_detail s WHERE cs.shop_id=s.shop_id AND cs.coupon_id=?";
 
	@Autowired
	private CouponShopDao couponShopDao;

	@Override
	public List<Long> saveCouponShops(List<CouponShop> couponShops) {
		return couponShopDao.saveCouponShops(couponShops);
	}

	@Override
	public List<CouponShop> getCouponShopByCouponId(Long couponId) {
		return couponShopDao.queryByProperties(new EntityCriterion().eq("couponId", couponId));
	}

	public CouponShopDao getCouponShopDao() {
		return couponShopDao;
	}

	@Override
	public List<String> getShopNamesByCouponId(Long id) {
		return this.couponShopDao.query(getShopNamesByCouponIdSql, String.class, id);
	}

	@Override
	public List<CouponShop> getCouponShopByCouponIds(List<Long> couponIds) {
		return couponShopDao.getCouponShopByCouponIds(couponIds);
	}

}
