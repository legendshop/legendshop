/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinSubscribeDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.WxMenuInfoypeEnum;
import com.legendshop.model.entity.weixin.WeixinSubscribe;
import com.legendshop.spi.service.WeixinSubscribeService;
import com.legendshop.util.AppUtils;

/**
 * 微信订阅的消息
 */
@Service("weixinSubscribeService")
public class WeixinSubscribeServiceImpl  implements WeixinSubscribeService{
	
	@Autowired
    private WeixinSubscribeDao weixinSubscribeDao;

    public WeixinSubscribe getWeixinSubscribe(Long id) {
        return weixinSubscribeDao.getWeixinSubscribe(id);
    }

    public void deleteWeixinSubscribe(WeixinSubscribe weixinSubscribe) {
        weixinSubscribeDao.deleteWeixinSubscribe(weixinSubscribe);
    }

    public Long saveWeixinSubscribe(WeixinSubscribe weixinSubscribe) {
	   	 if(weixinSubscribe.getMsgType().equals(WxMenuInfoypeEnum.TEXT.value())){
	   		weixinSubscribe.setTemplatename(null);
	   		weixinSubscribe.setTemplateId(null);
	      }else{
	    	  weixinSubscribe.setContent(null);
	      }
        if (!AppUtils.isBlank(weixinSubscribe.getId())) {
        	WeixinSubscribe subscribe=weixinSubscribeDao.getWeixinSubscribe(weixinSubscribe.getId());
        	subscribe.setMsgType(weixinSubscribe.getMsgType());
        	subscribe.setTemplateId(weixinSubscribe.getTemplateId());
        	subscribe.setTemplatename(weixinSubscribe.getTemplatename());
        	subscribe.setContent(weixinSubscribe.getContent());
            updateWeixinSubscribe(subscribe);
            return weixinSubscribe.getId();
        }
        weixinSubscribe.setAddtime(new Date());
        return weixinSubscribeDao.saveWeixinSubscribe(weixinSubscribe);
    }

    public void updateWeixinSubscribe(WeixinSubscribe weixinSubscribe) {
        weixinSubscribeDao.updateWeixinSubscribe(weixinSubscribe);
    }

	@Override
	public PageSupport<WeixinSubscribe> getWeixinSubscribePage(String curPageNO) {
		return weixinSubscribeDao.getWeixinSubscribePage(curPageNO);
	}
}
