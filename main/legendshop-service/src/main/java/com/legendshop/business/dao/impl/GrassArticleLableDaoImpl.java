/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;

import com.legendshop.model.entity.GrassArticleLable;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.GrassArticleLableDao;

import java.util.List;

/**
 * The Class GrassArticleLableDaoImpl. 种草社区文章关联标签表Dao实现类
 */
@Repository
public class GrassArticleLableDaoImpl extends GenericDaoImpl<GrassArticleLable, Long> implements GrassArticleLableDao{

	/**
	 * 根据Id获取种草社区文章关联标签表
	 */
	public GrassArticleLable getGrassArticleLable(Long id){
		return getById(id);
	}

	/**
	 *  删除种草社区文章关联标签表
	 *  @param grassArticleLable 实体类
	 *  @return 删除结果
	 */	
    public int deleteGrassArticleLable(GrassArticleLable grassArticleLable){
    	return delete(grassArticleLable);
    }

	/**
	 * 根据Id删除
	 * @param id 主键
	 * @return 删除结果
	 */
	@Override
	public int deleteGrassArticleLable(Long id){
		return deleteById(id);
	}

	/**
	 * 保存种草社区文章关联标签表
	 */
	public Long saveGrassArticleLable(GrassArticleLable grassArticleLable){
		return save(grassArticleLable);
	}

	/**
	 *  更新种草社区文章关联标签表
	 */		
	public int updateGrassArticleLable(GrassArticleLable grassArticleLable){
		return update(grassArticleLable);
	}

	/**
	 * 分页查询种草社区文章关联标签表列表
	 */
	public PageSupport<GrassArticleLable>queryGrassArticleLable(String curPageNO, Integer pageSize){
		CriteriaQuery query = new CriteriaQuery(GrassArticleLable.class, curPageNO);
		query.setPageSize(pageSize);
		query.addDescOrder("id"); //按ID倒排序, 如果主键不叫Id,则需要改动字段名称
		PageSupport ps = queryPage(query);
		return ps;
	}

	@Override
	public void deleteByGrassId(Long graid) {
		String sql="DELETE FROM ls_grass_article_lable WHERE grarticle_id=?";
		update(sql,graid);
	}

    @Override
    public List<Long> getArticleLableByLid(Long label) {
		String sql="SELECT DISTINCT grarticle_id FROM ls_grass_article_lable WHERE grlabel_id=?";
        return query(sql,Long.class,label);
    }

}
