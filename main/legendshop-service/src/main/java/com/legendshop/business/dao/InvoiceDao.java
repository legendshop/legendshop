package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Invoice;

/**
 * 发票Dao
 */
public interface InvoiceDao extends GenericDao<Invoice, Long> {

	public abstract List<Invoice> getInvoice(String userName);

	public abstract Invoice getInvoice(Long id);

	public abstract int deleteInvoice(Invoice invoice);

	public abstract Long saveInvoice(Invoice invoice);

	public abstract int updateInvoice(Invoice invoice);

	public void updateDefaultInvoice(Long invoiceId, String userName);

	public void delById(Long id);

	public abstract Invoice getDefaultInvoice(String userId);

	public abstract Invoice getInvoice(Long id, String userId);

	public abstract PageSupport<Invoice> getInvoicePage(String userName, String curPageNO, Integer pageSize);

	public abstract Invoice getInvoiceByTitleId(String userId, Integer titleId);

	public abstract void removeDefaultInvoiceStatus(String userId);

	/**
	 * 根据发票类型获取发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @param curPageNo 当前页码
	 * @return
	 */
    PageSupport<Invoice> queryInvoice(String userName, Integer invoiceType, String curPageNo);

	/**
	 * 根据类型获取用户发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @return
	 */
    List<Invoice> getInvoiceByType(String userName, Integer invoiceType);
}
