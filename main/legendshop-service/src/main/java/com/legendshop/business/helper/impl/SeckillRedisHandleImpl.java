/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.helper.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.SeckillConstant;
import com.legendshop.base.util.SeckillStringUtils;
import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.framework.cache.client.RedisCacheClient;
import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.dto.seckill.SeckillStateEnum;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;

/**
 * The Class SeckillMemCachedHandleImpl.
 */
@Service("seckillCachedhandle")
public class SeckillRedisHandleImpl implements SeckillCachedhandle {

	private Logger logger=LoggerFactory.getLogger(SeckillRedisHandleImpl.class);
	
	@Autowired
	private RedisCacheClient redisCacheClient;
	
	@Autowired
	private SeckillActivityDao seckillActivityDao;

	/**
	 * 获取秒杀详情的缓存信息.
	 *
	 * @param seckillId
	 * @return the seckill dto
	 */
	@Override
	public SeckillDto getSeckillDto(Long seckillId) {
		String key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_DETAILS_, seckillId);
		logger.info("##### 秒杀活动 {} ######",seckillId);
		logger.info(">>> 获取秒杀缓存详情的key={}",key);
		SeckillDto seckill = (SeckillDto) redisCacheClient.get(key);
		if (seckill == null) {
			logger.info("##### 秒杀活动 {} 获取失败 ######",seckillId);
			return null;
		}
		logger.info("##### 秒杀活动 {} 获取成功 ######",seckillId);
		return seckill;
	}

	/**
	 * 增加秒杀缓存详情.
	 *
	 * @param seckillDto
	 * @return true, if adds the seckill
	 */
	@Override
	public boolean addSeckill(SeckillDto seckillDto) {
		String key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_DETAILS_, seckillDto.getSid());
		logger.info("##### 秒杀活动 {} ######",seckillDto.getSid());
		logger.info(">>> 增加秒杀缓存详情 key={}",key);
		return redisCacheClient.put(key,10800, seckillDto )==1;
	}

	/**
	 * 删除秒杀缓存详情.
	 *
	 * @param seckillId
	 * @return true, if del seckill
	 */
	@Override
	public void delSeckill(Long seckillId) {
		String key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_DETAILS_, seckillId);
		logger.info("##### 秒杀活动 {} ######",seckillId);
		logger.info(">>> 删除秒杀缓存详情 key={}",key);
		redisCacheClient.delete(key);
	}

	/**
	 * 初始化秒杀商品库存.
	 *
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @param stock
	 */
	@Override
	public synchronized void initSeckillProdStock(Long seckillId, Long prodId, Long skuId, Long stock) {
		String key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_STOCK_, seckillId, prodId, skuId);
		redisCacheClient.delete(key);
		long redisStock=redisCacheClient.incrBy(key,stock);
		logger.info("##### 秒杀活动 {} ######",seckillId);
		logger.info(">>> 初始化秒杀商品库存  key={} ",key);
		logger.info(">>> 初始化信息 seckillId={},prodId={},skuId={},stock={},redisStock={} ",seckillId,prodId,skuId,stock,redisStock);
	}

	/**
	 * 获取秒杀商品库存.
	 *
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return the seckill prod stock
	 */
	@Override
	public Integer getSeckillProdStock(Long seckillId, Long prodId, Long skuId) {
		logger.info("##### 秒杀活动 {} ######",seckillId);
		String key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_STOCK_, seckillId, prodId, skuId);
		logger.info(">>> 获取秒杀redis缓存的秒杀商品库存  key={} ",key);
		logger.info(">>> 初始化信息 seckillId={},prodId={},skuId={}",seckillId,prodId,skuId);
		
		String object = null;
		object = redisCacheClient.getRedisTemplate().execute(new RedisCallback<String>() {
			public String doInRedis(RedisConnection connection) throws DataAccessException {
				byte[] keyByte = key.getBytes();
				
				System.out.println(new String(keyByte));
				
				byte[] value = connection.get(keyByte);
				return new String(value);
			}
		});
		
		if (object == null) {
			return 0;
		}
		Integer stock=Integer.valueOf(object.toString());
		logger.info(">>> 获取到的秒杀商品库存 stock={} ",stock);
		return stock;
	}

	/**
	 * 秒杀活动库存检查.
	 *
	 * @param seckill
	 * @param prodId
	 * @param skuId
	 * @param userId
	 * @return the int
	 */
	@Override
	public int seckillCheck(Long seckillId, long prodId, long skuId, String userId) {
		logger.info("##### 秒杀活动 {} ######",seckillId);
		logger.info(">>> 秒杀活动库存检查 ");
		
		// 观察 活动库存值
		Integer number = getSeckillProdStock(seckillId, prodId, skuId);
		if (number == null || number <= 0 ) {
			return SeckillStateEnum.SELL_OUT.getState();
		}
		
		int buyNumber = 1; // 默认秒杀一件
		logger.info(">>>  判断该用户是否在此活动进行秒杀成功过 ");
		
		//这里的判断一定程度上减轻了数据库的访问
		String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_BUY_USERS_, seckillId, userId, prodId, skuId);
		String result=redisCacheClient.get(user_key);
		if(AppUtils.isNotBlank(result)){
			logger.info(">>> 采用redis判断 该用户已经秒杀成功过 " );
			return SeckillStateEnum.SUCCESS2.getState();
		}
		
		// 采用SQL 判断该用户是否在此活动进行秒杀成功过,
		long count=seckillActivityDao.findSeckillSuccess(seckillId,prodId,skuId,userId);
		if(count>0){
			logger.info(">>>  采用数据库持久化判断 该用户已经秒杀成功过 " );
			return SeckillStateEnum.SUCCESS2.getState();
		}
		
		String _key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_STOCK_, seckillId, prodId, skuId);
		long stock = redisCacheClient.decr(_key, 1); 
		logger.info(">>>  decr redis 库存缓存 key {} ",_key);
		logger.info(">>>  decr redis 库存缓存,剩余商品库存缓存 {} ",stock);
		if (stock <= -1) {
			return SeckillStateEnum.SELL_OUT.getState();
		} else {
			logger.info(">>>  恭喜您，{} 已经秒杀成功 {} 件，商品剩余 {} 件 ",userId,buyNumber,stock);
			
			redisCacheClient.put(user_key, 3600, "true"); // 用户购买过,默认1小时
			
			return SeckillStateEnum.SUCCESS.getState();
		}
	}

	/**
	 * 秒杀成功
	 */
	@Override
	public void seckillSuccess(SeckillSuccess success) {
		logger.info("##### 秒杀活动 {} ######",success.getId());
		
		String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_SUCCESS_, success.getSeckillId(), success.getUserId(), success.getProdId(),
				success.getSkuId());
		logger.info(">>> 秒杀成功 设置用户信息  user_key={}",user_key);
		redisCacheClient.put(user_key, 3600, success); // 用户购买过
	}

	/**
	 * 获取秒杀成功的对象信息
	 */
	@Override
	public SeckillSuccess getSeckillSucess(String userid, Long seckillId, Long prodId, Long skuId) {
		logger.info("##### 秒杀活动 {} ######",seckillId);
		String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_SUCCESS_, seckillId, userid, prodId, skuId);
		logger.info(">>>获取秒杀成功的对象信息   user_key={} ",user_key);
		return (SeckillSuccess) redisCacheClient.get(user_key);
	}

	@Override
	public void rollbackSeckill(Long seckillId, long prodId, long skuId, String userId) {
		String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_BUY_USERS_, seckillId, userId, prodId, skuId);
		redisCacheClient.delete(user_key.toString());
		String STOCK_KEY = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_STOCK_, seckillId, prodId, skuId);
		redisCacheClient.incrBy(STOCK_KEY,1l);	
	}

}
