/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SubSettlement;


/**
 * 订单结算票据中心
 * @author tony
 *
 */
public interface SubSettlementDao extends GenericDao<SubSettlement, Long> {
    
    /**
     * 根据商户系统内部的订单号[微信]查找结算单
     */
    public abstract SubSettlement getSubSettlementBySn(String settlement_sn);
    
	/**
	 * 根据用户和订单支付号查找结算单
	 * @param settlementNumbers
	 * @param userId
	 * @return
	 */
	public abstract SubSettlement getSubSettlement(String settlementSn, String userId);
	
	
	/**
	 * 保存结算单
	 */
	public abstract Long saveSubSettlement(SubSettlement subSettlement);
	
	/**
	 * 更新结算单
	 */
	public abstract int updateSubSettlement(SubSettlement subSettlement);
	

	/**
	 * 更新结算单
	 * @param subSettlement
	 * @return
	 */
	public abstract int updateSubSettlementForPay(SubSettlement subSettlement);

	/**
	 * 检查是否有单据支付成功
	 * @param subNumber
	 * @param userId
	 * @param subSettlementType
	 * @return
	 */
	public abstract boolean checkRepeatPay(String [] subNumber, String userId, String subSettlementType);

 }
