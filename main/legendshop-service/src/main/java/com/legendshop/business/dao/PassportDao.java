/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.IdPassord;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.entity.Passport;

/**
 * The Class PassportDao.
 */

public interface PassportDao extends GenericDao<Passport, Long> {
     
    public abstract List<Passport> getPassport(String userId);

	public abstract Passport getPassport(Long id);
	
    public abstract int deletePassport(Passport passport);
	
	public abstract Long savePassport(Passport passport);
	
	public abstract int updatePassport(Passport passport);
	
	public abstract int updateLastLoginTime(Passport passport);
	
	public abstract List<Passport> getPassport(String userId,String type);
	
	public abstract Passport getUserIdByOpenId(String type, String openId);
	
	public abstract boolean isAccountBind(String userName, String type);

	public abstract Passport getUserIdByOpenIdForUpdate(String type, String openId);
	
	//绑定用户
	public int bindingUser(Passport passport);

	public abstract IdPassord validateMobilePassword(String mobile, String password);

	public abstract Passport getPassportByBindingUserId(String userId);

	/**
	 * 查询通行证ID
	 * @param openid
	 * @return
	 */
	public abstract PassportFull getPassportByOpenId(String openid);

	/**
	 * 根据unionid 获取passport
	 * @param unionid
	 * @return
	 */
	public abstract Passport getPassportByUnionid(String unionid);

	/**
	 * 根据passportId 查询 PassportFull
	 * @param passportId 是passportSub的ID
	 * @return
	 */
	public abstract PassportFull getPassportFullById(Long passportId);

	/**
	 * 绑定用户
	 * @param passPortId
	 * @param userId
	 * @param userName
	 * @return
	 */
	public abstract int bindUser(Long passPortId, String userId, String userName);

	/**
	 * 获取 PassportFull
	 * @param userId
	 * @param type
	 * @param source
	 * @return
	 */
	public abstract PassportFull getPassportFullByUserIdAndTypeAndSource(String userId, String type, String source);

}
