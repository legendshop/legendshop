/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.model.R;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.shop.OpenShopInfoDTO;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.ShopCompanyDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.cache.ShopDetailUpdate;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ShopAuditDao;
import com.legendshop.business.dao.ShopCompanyDetailDao;
import com.legendshop.business.dao.ShopDecotateDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.IdTextEntity;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.ShopOPstatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopAudit;
import com.legendshop.model.entity.ShopCompanyDetail;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopDetailView;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.CommonUtil;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import com.legendshop.util.ip.IPSeeker;

/**
 * 商城详细信息服务.
 */
@Service("shopDetailService")
public class ShopDetailServiceImpl implements ShopDetailService {

	private final Logger log = LoggerFactory.getLogger(ShopDetailServiceImpl.class);

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private ShopAuditDao shopAuditDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private CommonUtil commonUtil;

	@Autowired
	private ShopCompanyDetailDao shopCompanyDetailDao;

	@Autowired
	private ShopDecotateDao shopDecotateDao;

	@Autowired
	private ShopCompanyDetailService shopCompanyDetailService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Resource(name = "productIndexDelProcessor")
	private EventProcessor<ProductIdDto> productIndexDelProcessor;

	@Resource(name = "productIndexSaveProcessor")
	private EventProcessor<ProductIdDto> ProductIndexSaveProcessor;

	private Integer pageSize = 100;

	/**
	 * 系统配置.
	 */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Override
	public ShopDetail getShopDetailById(Long id) {
		if (AppUtils.isBlank(id)) {
			return null;
		}
		return shopDetailDao.getShopDetailByShopId(id);
	}

	@Override
	public UserDetail getShopDetailByName(String userName) {
		return userDetailDao.getUserDetailByName(userName);
	}

	/**
	 * 不用缓存来加载
	 */
	@Override
	public ShopDetail getShopDetailByIdNoCache(final Long shopId) {
		return shopDetailDao.getById(shopId);
	}

	/**
	 * 删除商城相关信息
	 */
	@Override
	public String delete(ShopDetail shopDetail) {
		// 删除该商城的所有信息
		return shopDetailDao.deleteShopDetail(shopDetail);
	}

	@Override
	public void saveShopDetail(ShopDetail shopDetail) {
		shopDetailDao.saveShopDetail(shopDetail);
	}

	@Override
	public ShopDetailView getShopDetailView(Long shopId) {
		ShopDetailView shopDetail = shopDetailDao.getShopDetailView(shopId);
		return shopDetail;
	}

	@Override
	public ShopDetailView getShopDetailViewByUserName(String userName) {
		Long shopId = shopDetailDao.getShopIdByName(userName);
		return getShopDetailView(shopId);
	}

	@Override
	@ShopDetailUpdate
	public void update(ShopDetail shopDetail) {
		shopDetailDao.updateShopDetail(shopDetail);
	}

	@Override
	public boolean updateShop(String userId, ShopDetail shopDetail, Integer status) {
		return shopDetailDao.updateShop(userId, shopDetail, status);
	}

	@Override
	public Boolean isShopExist(Long shopId) {
		return shopDetailDao.isShopExist(shopId);
	}

	@Override
	public Long getShopIdByUserId(String userId) {
		return shopDetailDao.getShopIdByUserId(userId);
	}

	@Override
	public Long getShopIdByDomain(String domainName) {
		return shopDetailDao.getShopIdByDomain(domainName);
	}

	@Override
	public ShopDetail getShopDetailByUserId(String userId) {
		return shopDetailDao.getShopDetailByUserId(userId);
	}

	/**
	 * 再次申请开店
	 *
	 * @param shopDetail
	 * @param userId
	 * @return
	 */
	@Override
	public boolean reApplyOpenShop(ShopDetail shopDetail, String userId, String userName) {
		/*
		 * ShopDetail orinShopDetail = this.getShopDetailById(shopDetail.getShopId());
		 * if(userId.equals(orinShopDetail.getUserId())){ SafeHtml safeHtml = new
		 * SafeHtml(); orinShopDetail.setContactName(shopDetail.getContactName());
		 * orinShopDetail.setContactMobile(shopDetail.getContactMobile());
		 * orinShopDetail.setContactMail(shopDetail.getContactMail());
		 * orinShopDetail.setContactTel(shopDetail.getContactTel());
		 * orinShopDetail.setContactQQ(shopDetail.getContactQQ());
		 * orinShopDetail.setSiteName(safeHtml.makeSafe(shopDetail.getSiteName() ));
		 * orinShopDetail.setIdCardNum(shopDetail.getIdCardNum());
		 * orinShopDetail.setProvinceid(shopDetail.getProvinceid());
		 * orinShopDetail.setCityid(shopDetail.getCityid());
		 * orinShopDetail.setAreaid(shopDetail.getAreaid());
		 * orinShopDetail.setPostAddr(safeHtml.makeSafe(shopDetail.getPostAddr() ));
		 * orinShopDetail.setShopAddr(safeHtml.makeSafe(shopDetail.getShopAddr() ));
		 * orinShopDetail.setType(shopDetail.getType());
		 * orinShopDetail.setCompanyNum(shopDetail.getCompanyNum());
		 * orinShopDetail.setCompanyName(safeHtml.makeSafe(shopDetail.
		 * getCompanyName()));
		 * orinShopDetail.setCompanyIndustry(shopDetail.getCompanyIndustry());
		 * orinShopDetail.setCompanyProperty(shopDetail.getCompanyProperty());
		 * if(shopDetail.getIdCardPicFile()!=null &&
		 * shopDetail.getIdCardPicFile().getSize()>0){ String orinIdCardPic =
		 * orinShopDetail.getIdCardPic(); String filename =
		 * attachmentManager.upload(shopDetail.getIdCardPicFile()); //保存附件表
		 * attachmentManager.saveAttachment(userName, filename,
		 * shopDetail.getIdCardPicFile(), AttachmentTypeEnum.USER); log.info(
		 * "{} 上传身份证正面照片文件 {} ", userName, filename);
		 * orinShopDetail.setIdCardPic(filename); //删除原图片
		 * if(AppUtils.isNotBlank(orinIdCardPic)){
		 * attachmentManager.deleteTruely(orinIdCardPic);
		 * attachmentManager.delAttachmentByFilePath(orinIdCardPic); } }
		 * if(shopDetail.getIdCardBackPicFile()!=null &&
		 * shopDetail.getIdCardBackPicFile().getSize()>0){ String orinIdCardBackPic =
		 * orinShopDetail.getIdCardBackPic(); String filename =
		 * attachmentManager.upload(shopDetail.getIdCardBackPicFile()); //保存附件表
		 * attachmentManager.saveAttachment(userName, filename,
		 * shopDetail.getIdCardBackPicFile(), AttachmentTypeEnum.USER);
		 * log.info("{} 上传身份证反面照片文件 {} ", userName, filename);
		 * orinShopDetail.setIdCardBackPic(filename); //删除原图片
		 * if(AppUtils.isNotBlank(orinIdCardBackPic)){
		 * attachmentManager.deleteTruely(orinIdCardBackPic);
		 * attachmentManager.delAttachmentByFilePath(orinIdCardBackPic); } }
		 * if(shopDetail.getTrafficPicFile()!=null &&
		 * shopDetail.getTrafficPicFile().getSize()>0){ String orinTrafficPic =
		 * orinShopDetail.getTrafficPic(); String filename =
		 * attachmentManager.upload(shopDetail.getTrafficPicFile()); //保存附件表
		 * attachmentManager.saveAttachment(userName, filename,
		 * shopDetail.getTrafficPicFile(), AttachmentTypeEnum.USER); log.info(
		 * "{} 上传营业执照文件 {} ", userName, filename);
		 * orinShopDetail.setTrafficPic(filename); //删除原图片
		 * if(AppUtils.isNotBlank(orinTrafficPic)){
		 * attachmentManager.deleteTruely(orinTrafficPic);
		 * attachmentManager.delAttachmentByFilePath(orinTrafficPic); } }
		 * orinShopDetail.setModifyDate(new Date()); //把状态改成审核中
		 * orinShopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		 * this.update(orinShopDetail); return true; }else{ return false; }
		 */
		return false;
	}

	@Override
	public Long getMaxShopId() {
		return shopDetailDao.getMaxShopId();
	}

	@Override
	public long getCountShopId() {
		return shopDetailDao.getCountShopId();
	}

	/**
	 * 结算统计 查找firstShopId到toShopId 的所有商家 包含firstShopId ，但不包含toShopId
	 */
	@Override
	public List<ShopDetailTimerDto> getShopDetailList(Long firstShopId, Integer commitInteval) {
		return shopDetailDao.getShopDetailList(firstShopId, commitInteval);
	}

	@Override
	public Long getMinShopId() {
		return shopDetailDao.getMinShopId();
	}

	@Override
	@Cacheable(value = "StoreListDto", key = "'StoreList_'+#shopId")
	public StoreListDto findDetfaultStoreListDto(Long shopId) {

		StoreListDto decotateDto = new StoreListDto();
		decotateDto.setShopId(shopId);

		/*
		 * 默认商家信息
		 */
		ShopLayoutShopInfoDto shopDefaultInfo = shopDecotateDao.findShopInfo(shopId);
		decotateDto.setShopDefaultInfo(shopDefaultInfo);

		/*
		 * 热销
		 */
		List<ShopLayoutHotProdDto> hotProds = shopDecotateDao.findShopHotProds(shopId);
		decotateDto.setHotProds(hotProds);

		/*
		 * 店铺分类
		 */
		List<ShopLayoutCateogryDto> cateogryDtos = findShopCateogryDtos(shopId);
		decotateDto.setCateogryDtos(cateogryDtos);

		return decotateDto;
	}

	private List<ShopLayoutCateogryDto> findShopCateogryDtos(Long shopId) {
		List<ShopLayoutCateogryDto> cateogryDtos = shopDecotateDao.findShopCateogryDtos(shopId);
		if (AppUtils.isBlank(cateogryDtos)) {
			return null;
		}
		List<ShopLayoutCateogryDto> list = new ArrayList<ShopLayoutCateogryDto>();
		for (ShopLayoutCateogryDto shopLayoutCateogryDto : cateogryDtos) {
			List<ShopLayoutCateogryDto> dtos = shopDecotateDao.findShopCateogryDtos(shopId, shopLayoutCateogryDto.getId());
			shopLayoutCateogryDto.setChildrentDtos(dtos);
			list.add(shopLayoutCateogryDto);
		}
		return list;
	}

	@Override
	public Long saveContactInfo(ShopDetail shopDetail, String userId, String userName) {
		if (AppUtils.isBlank(shopDetail.getType()) || AppUtils.isBlank(shopDetail.getContactName()) || AppUtils.isBlank(shopDetail.getContactMobile())
				|| AppUtils.isBlank(shopDetail.getContactMail())) {
			throw new BusinessException("shopDetail is null");
		}
		Date data = new Date();
		shopDetail.setUserId(userId);
		shopDetail.setUserName(userName);
		shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		shopDetail.setOpStatus(ShopOPstatusEnum.CONTACT_INFO_OK.value());
		shopDetail.setRecDate(data);
		shopDetail.setModifyDate(data);
		shopDetail.setVisitTimes(0l);
		shopDetail.setOffProductNum(0l);
		shopDetail.setProductNum(0l);
		shopDetail.setCommNum(0l);
		shopDetail.setCapital(0d);
		shopDetail.setCredit(0);
		shopDetail.setGradeId(0);
		// 得到创建时的国家和地区
		if (shopDetail.getIp() != null) {
			shopDetail.setCreateAreaCode(IPSeeker.getInstance().getArea(shopDetail.getIp()));
			shopDetail.setCreateCountryCode(IPSeeker.getInstance().getCountry(shopDetail.getIp()));
		}
		return shopDetailDao.save(shopDetail);
	}

	@Override
	@ShopDetailUpdate
	public Integer updateContactInfo(ShopDetail shopDetail, ShopDetail newShopDetail) {
		shopDetail.setContactName(newShopDetail.getContactName());
		shopDetail.setContactMobile(newShopDetail.getContactMobile());
		shopDetail.setContactMail(newShopDetail.getContactMail());
		shopDetail.setContactQQ(newShopDetail.getContactQQ());
		shopDetail.setOpStatus(ShopOPstatusEnum.CONTACT_INFO_OK.value());
		shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		shopDetail.setModifyDate(new Date());
		return shopDetailDao.update(shopDetail);
	}

	/**
	 * 优化保存商家信息流程
	 */
	@Override
	public Long saveShopDetailInfo(ShopDetail shopDetail, String userName, String userId, Long shopId, String idCardPic, String idCardBackPic) {
		SafeHtml safeHtml = new SafeHtml();
		shopDetail.setRealPath(propertiesUtil.getBigPicPath());
		shopDetail.setSiteName(safeHtml.makeSafe(shopDetail.getSiteName().trim()));
		if (AppUtils.isNotBlank(shopDetail.getPostAddr())) {
			shopDetail.setPostAddr(safeHtml.makeSafe(shopDetail.getPostAddr()));
		}
		shopDetail.setIdCardNum(safeHtml.makeSafe(shopDetail.getIdCardNum()));
		shopDetail.setOpStatus(ShopOPstatusEnum.SHOP_INFO_OK.value());
		if (shopDetail.getType() == 0) {
			MultipartFile idCardPicFile = shopDetail.getIdCardPicFile();
			MultipartFile idCardBackPicFile = shopDetail.getIdCardBackPicFile();
			if ((idCardPicFile != null) && (idCardPicFile.getSize() > 0)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, idCardPic, idCardPicFile, AttachmentTypeEnum.USER);
				log.info("{} 上传身份证照片文件 {} ", userName, idCardPic);
				shopDetail.setIdCardPic(idCardPic);

			} else {
				if (AppUtils.isBlank(shopDetail.getIdCardPic()))
					throw new BusinessException("idpic null");
			}

			if ((idCardBackPicFile != null) && (idCardBackPicFile.getSize() > 0)) {
				// 保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, idCardBackPic, idCardBackPicFile, AttachmentTypeEnum.USER);
				log.info("{} 上传身份证照片文件 {} ", userName, idCardBackPic);
				shopDetail.setIdCardBackPic(idCardBackPic);

			} else {
				if (AppUtils.isBlank(shopDetail.getIdCardBackPic()))
					throw new BusinessException("idpic null");
			}
		}
		Date data = new Date();
		shopDetail.setUserId(userId);
		shopDetail.setUserName(userName);
		// 保存状态
		shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
		shopDetail.setRecDate(data);
		shopDetail.setModifyDate(data);
		shopDetail.setVisitTimes(0l);
		shopDetail.setOffProductNum(0l);
		shopDetail.setProductNum(0l);
		shopDetail.setCommNum(0l);
		shopDetail.setCapital(0d);
		shopDetail.setCredit(0);
		shopDetail.setGradeId(0);
		// 得到创建时的国家和地区
		if (shopDetail.getIp() != null) {
			shopDetail.setCreateAreaCode(IPSeeker.getInstance().getArea(shopDetail.getIp()));
			shopDetail.setCreateCountryCode(IPSeeker.getInstance().getCountry(shopDetail.getIp()));
		}

		return shopDetailDao.save(shopDetail);
	}

	@Override
	public void changeOptionStatus(String userId, Long shopId, Integer option) {
		shopDetailDao.changeOptionStatus(userId, shopId, option);
	}

	@Override
	@ShopDetailUpdate
	public Integer updateShopInfo(ShopDetail shopDetail, ShopDetail newShopDetail, String flag) {
		shopDetail.setContactName(newShopDetail.getContactName());
		shopDetail.setContactMobile(newShopDetail.getContactMobile());
		shopDetail.setContactMail(newShopDetail.getContactMail());
		shopDetail.setContactQQ(newShopDetail.getContactQQ());
		shopDetail.setSiteName(newShopDetail.getSiteName());
		shopDetail.setProvinceid(newShopDetail.getProvinceid());
		shopDetail.setCityid(newShopDetail.getCityid());
		shopDetail.setAreaid(newShopDetail.getAreaid());
		shopDetail.setShopAddr(newShopDetail.getShopAddr());
		shopDetail.setRecipient(newShopDetail.getRecipient());
		shopDetail.setPostAddr(newShopDetail.getPostAddr());
		shopDetail.setCode(newShopDetail.getCode());
		shopDetail.setBriefDesc(newShopDetail.getBriefDesc());
		shopDetail.setDetailDesc(newShopDetail.getDetailDesc());
		shopDetail.setModifyDate(new Date());
		shopDetail.setBankCard(newShopDetail.getBankCard());
		shopDetail.setPayee(newShopDetail.getPayee());
		shopDetail.setStatus(newShopDetail.getStatus());
		if (newShopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			shopDetail.setOpStatus(ShopOPstatusEnum.AUDITED.value());
			userDetailDao.updateShopId(shopDetail.getShopId(), shopDetail.getUserId());
			commonUtil.saveAdminRight(shopDetail.getUserId());
		} else if (newShopDetail.getStatus().equals(ShopStatusEnum.OFFLINE.value()) || newShopDetail.getStatus().equals(ShopStatusEnum.CLOSED.value())) {
			shopDetail.setOpStatus(ShopOPstatusEnum.AUDITING.value());
			commonUtil.removeAdminRight(shopDetail.getUserId());
		}
		shopDetail.setAuditOpinion(newShopDetail.getAuditOpinion());

		Integer result = 0;
		try {
			result = shopDetailDao.update(shopDetail);
		} catch (Exception ignored) {
		}

		if (result > 0) {

			// 后台审核操作才保存审核历史
			if ("admin".equals(flag)) {
				// 保存店铺审核历史
				ShopAudit shopAudit = new ShopAudit();
				shopAudit.setStatus(newShopDetail.getStatus());
				shopAudit.setShopId(shopDetail.getShopId());
				shopAudit.setModifyDate(new Date());
				shopAudit.setAuditOpinion(newShopDetail.getAuditOpinion());
				shopAuditDao.saveShopAudit(shopAudit);
			}
			// 发送站内信 通知
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopDetail.getUserName(), "店铺通知", msginfo(newShopDetail.getStatus())).getSource());
			Long shopId = shopDetail.getShopId();
			// 正常营业
			if (newShopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
				// 添加店铺商品索引
				updateIndex(0, shopId);
			} else {
				// 删除店铺商品索引
				updateIndex(1, shopId);
			}
		}
		return result;
	}

	private void updateIndex(int saveOrDelete, Long shopId) {
		// 获得用户总数
		long totalCount = productDao.getProdTotalCount(shopId);
		// 总页数
		long totalPage = (totalCount + pageSize - 1) / pageSize;
		// 当前页码
		int currPage = 1;
		while (currPage <= totalPage) {
			List<Long> prodIds = productDao.getProdByPage(shopId, currPage, pageSize);
			if (AppUtils.isNotBlank(prodIds)) {
				if (saveOrDelete == 1) {// 删除店铺商品索引
					productIndexDelProcessor.process(new ProductIdDto(prodIds));
				} else {
					ProductIndexSaveProcessor.process(new ProductIdDto(prodIds));
				}
			}
			currPage++;
		}

	}

	private String msginfo(Integer status) {
		String msgText = "";
		switch (status) {
			case 1:
				msgText = "恭喜您，您的店铺已经审核成功！赶快去卖家中心瞧瞧吧!";
				break;
			case -1:
				msgText = "开店通知，您的店铺已成功提交审核";
				break;
			case -2:
				msgText = "对不起,您的开店申请被管理员拒绝,请联系后台客服或管理员!";
				break;
			case 0:
				msgText = "对不起，您的店铺已被下线";
				break;
			case -4:
				msgText = "对不起，您的提交的审核资料，审核联系人失败，请重新填写";
				break;
			case -5:
				msgText = "对不起，您的提交的审核资料，审核公司信息失败，请重新填写";
				break;
			case -6:
				msgText = "对不起，您的提交的审核资料，审核店铺信息失败，请重新填写";
				break;
			case -3:
				msgText = "对不起，您的店铺已被平台下线!";
				break;
			default:
				msgText = "";
				break;
		}
		return msgText;
	}

	@Override
	public boolean saveCompanyInfo(ShopDetail shopDetail, ShopCompanyDetail shopCompanyDetail, String legalIdCard, String licensePics) {
		shopCompanyDetail.setShopId(shopDetail.getShopId());
		if (legalIdCard != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(shopDetail.getUserName(), shopDetail.getUserName(), shopDetail.getShopId(), legalIdCard,
					shopCompanyDetail.getLegalIdCardPic(), AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			if (AppUtils.isNotBlank(shopCompanyDetail.getLegalIdCard())) {
				attachmentManager.delAttachmentByFilePath(shopCompanyDetail.getLegalIdCard());
				attachmentManager.deleteTruely(shopCompanyDetail.getLegalIdCard());
			}
			shopCompanyDetail.setLegalIdCard(legalIdCard);

		}
		if (licensePics != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(shopDetail.getUserName(), shopDetail.getUserName(), shopDetail.getShopId(), licensePics,
					shopCompanyDetail.getLicensePics(), AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			if (AppUtils.isNotBlank(shopCompanyDetail.getLicensePics())) {
				attachmentManager.delAttachmentByFilePath(shopCompanyDetail.getLicensePic());
				attachmentManager.deleteTruely(shopCompanyDetail.getLicensePic());
			}
			shopCompanyDetail.setLicensePic(licensePics);

		}

		Long result = shopCompanyDetailDao.saveShopCompanyDetail(shopCompanyDetail);
		if (result > 0)
			return true;
		else
			return false;
	}

	// 优化
	@Override
	public boolean saveCompanyInfo(ShopDetail shopDetail, Long shopId, String userName, String licensePics) {
		ShopCompanyDetail shopCompanyDetail = new ShopCompanyDetail();
		if (licensePics != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, shopDetail.getUserId(), shopId, licensePics, shopDetail.getLicensePicFile(),
					AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			ShopCompanyDetail oldShopCompanyDetail = shopCompanyDetailDao.getShopCompanyDetailByShopId(shopDetail.getShopId());
			if (AppUtils.isNotBlank(oldShopCompanyDetail)) {
				attachmentManager.delAttachmentByFilePath(oldShopCompanyDetail.getLicensePic());
				attachmentManager.deleteTruely(oldShopCompanyDetail.getLicensePic());
			}
			shopCompanyDetail.setLicensePic(licensePics);
		}
		shopCompanyDetail.setShopId(shopId);
		shopCompanyDetail.setCompanyName(shopDetail.getCompanyName());
		shopCompanyDetail.setLicenseNumber(shopDetail.getLicenseNumber());
		Long result = shopCompanyDetailDao.saveShopCompanyDetail(shopCompanyDetail);
		if (result > 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean updateCompanyInfo(String userName, String userId, Long shopId, ShopCompanyDetail oldshopCompanyDetail, ShopCompanyDetail shopCompanyDetail,
									 String legalIdCard, String licensePics) {
		if (!oldshopCompanyDetail.getShopId().equals(shopCompanyDetail.getShopId())) {
			throw new BusinessException("shopId error");
		}
		shopCompanyDetail.setId(oldshopCompanyDetail.getId());
		shopCompanyDetail.setShopId(oldshopCompanyDetail.getShopId());
		shopCompanyDetail.setBankAccountName(oldshopCompanyDetail.getBankAccountName());
		shopCompanyDetail.setBankCard(oldshopCompanyDetail.getBankCard());
		shopCompanyDetail.setBankName(oldshopCompanyDetail.getBankName());
		shopCompanyDetail.setBankLineNum(oldshopCompanyDetail.getBankLineNum());
		shopCompanyDetail.setBankProvinceid(oldshopCompanyDetail.getBankProvinceid());
		shopCompanyDetail.setBankCityid(oldshopCompanyDetail.getBankCityid());
		shopCompanyDetail.setBankAreaid(oldshopCompanyDetail.getBankAreaid());
		shopCompanyDetail.setBankPermitImg(oldshopCompanyDetail.getBankPermitImg());
		shopCompanyDetail.setCodeType(oldshopCompanyDetail.getCodeType());
		shopCompanyDetail.setSocialCreditCode(oldshopCompanyDetail.getSocialCreditCode());
		shopCompanyDetail.setTaxpayerId(oldshopCompanyDetail.getTaxpayerId());
		shopCompanyDetail.setTaxRegistrationNum(oldshopCompanyDetail.getTaxRegistrationNum());
		shopCompanyDetail.setTaxRegistrationNumE(oldshopCompanyDetail.getTaxRegistrationNumE());
		if (legalIdCard != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, legalIdCard, shopCompanyDetail.getLegalIdCardPic(), AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			if (AppUtils.isNotBlank(shopCompanyDetail.getLegalIdCard())) {
				attachmentManager.delAttachmentByFilePath(shopCompanyDetail.getLegalIdCard());
				attachmentManager.deleteTruely(shopCompanyDetail.getLegalIdCard());
			}
			shopCompanyDetail.setLegalIdCard(legalIdCard);

		} else {
			shopCompanyDetail.setLegalIdCard(oldshopCompanyDetail.getLegalIdCard());
		}
		if (licensePics != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, licensePics, shopCompanyDetail.getLicensePics(), AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			if (AppUtils.isNotBlank(oldshopCompanyDetail.getLicensePic())) {
				attachmentManager.delAttachmentByFilePath(oldshopCompanyDetail.getLicensePic());
				attachmentManager.deleteTruely(oldshopCompanyDetail.getLicensePic());
			}
			shopCompanyDetail.setLicensePic(licensePics);

		} else {
			shopCompanyDetail.setLicensePic(oldshopCompanyDetail.getLicensePic());
		}
		Integer result = shopCompanyDetailDao.update(shopCompanyDetail);
		if (result > 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean saveCertificateInfo(String userName, String userId, Long shopId, ShopCompanyDetail shopCompanyDetail, String taxRegistrationNumE,
									   String bankPermitPic) {
		ShopCompanyDetail CompanyDetail = shopCompanyDetailDao.getShopCompanyDetailByShopId(shopId);
		if (AppUtils.isBlank(CompanyDetail)) {
			return false;
		}
		if (shopCompanyDetail.getCodeType().equals(1l)) {
			CompanyDetail.setCodeType(1l);
			CompanyDetail.setTaxpayerId(shopCompanyDetail.getTaxpayerId());
			CompanyDetail.setTaxRegistrationNum(shopCompanyDetail.getTaxRegistrationNum());
			CompanyDetail.setSocialCreditCode(null);
			if (taxRegistrationNumE != null) {
				// 保存附件表
				attachmentManager.saveImageAttachment(userName, userId, shopId, taxRegistrationNumE, shopCompanyDetail.getTaxRegistrationNumEPic(),
						AttachmentTypeEnum.OPENSHOP);
				// 删除原引用
				if (AppUtils.isNotBlank(CompanyDetail.getTaxRegistrationNumE())) {
					attachmentManager.delAttachmentByFilePath(shopCompanyDetail.getTaxRegistrationNumE());
					attachmentManager.deleteTruely(shopCompanyDetail.getTaxRegistrationNumE());
				}
				CompanyDetail.setTaxRegistrationNumE(taxRegistrationNumE);
			} else {
				CompanyDetail.setTaxRegistrationNumE(CompanyDetail.getTaxRegistrationNumE());
			}
		}
		if (shopCompanyDetail.getCodeType().equals(2l)) {
			CompanyDetail.setCodeType(2l);
			CompanyDetail.setSocialCreditCode(shopCompanyDetail.getSocialCreditCode());
			CompanyDetail.setTaxpayerId(null);
			CompanyDetail.setTaxRegistrationNum(null);
			CompanyDetail.setTaxRegistrationNumE(null);
		}

		CompanyDetail.setBankAccountName(shopCompanyDetail.getBankAccountName());
		CompanyDetail.setBankCard(shopCompanyDetail.getBankCard());
		CompanyDetail.setBankName(shopCompanyDetail.getBankName());
		CompanyDetail.setBankLineNum(shopCompanyDetail.getBankLineNum());
		CompanyDetail.setBankProvinceid(shopCompanyDetail.getBankProvinceid());
		CompanyDetail.setBankCityid(shopCompanyDetail.getBankCityid());
		CompanyDetail.setBankAreaid(shopCompanyDetail.getBankAreaid());

		CompanyDetail.setTaxpayerId(shopCompanyDetail.getTaxpayerId());
		CompanyDetail.setTaxRegistrationNum(shopCompanyDetail.getTaxRegistrationNum());

		if (bankPermitPic != null) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, userId, shopId, bankPermitPic, shopCompanyDetail.getBankPermitPic(), AttachmentTypeEnum.OPENSHOP);
			// 删除原引用
			if (AppUtils.isNotBlank(CompanyDetail.getBankPermitImg())) {
				attachmentManager.delAttachmentByFilePath(shopCompanyDetail.getBankPermitImg());
				attachmentManager.deleteTruely(shopCompanyDetail.getBankPermitImg());
			}
			CompanyDetail.setBankPermitImg(bankPermitPic);

		} else {
			CompanyDetail.setBankPermitImg(CompanyDetail.getBankPermitImg());
		}

		Integer result = shopCompanyDetailDao.update(CompanyDetail);
		if (result > 0)
			return true;
		else
			return false;
	}

	@Override
	public List<ShopAudit> getShopAuditInfo(Long shopId) {
		return shopAuditDao.getShopAuditInfo(shopId);
	}

	@Override
	public void changeStatus(String userId, Long shopId, Integer status) {
		shopDetailDao.changeStatus(userId, shopId, status);
	}

	@Override
	public String getShopName(Long shopId) {
		return shopDetailDao.getShopName(shopId);
	}

	@Override
	public Long getShopIdBySecDomain(String secDomainName) {
		return shopDetailDao.getShopIdBySecDomain(secDomainName);
	}

	@Override
	@Caching(evict = {
			@CacheEvict(value = "ShopDetailView", key = "#shopId"),
			@CacheEvict(value = "ShopDetail", key = "#shopId")
	})
	public Integer updateShopType(Long shopId, Long type) {
		return shopDetailDao.updateShopType(shopId, type);
	}

	@Override
	public String getshopScore(Long shopId) {
		String sql = "SELECT SUM(s.score) AS score, SUM(s.comments) AS comments FROM ls_prod_comm_stat s, ls_prod p WHERE s.prod_id = p.prod_id AND p.shop_id = ?";
		ProductCommentStat commentStat = shopDetailDao.get(sql, ProductCommentStat.class, shopId);
		String score = "";
		if (commentStat.getScore() != null && commentStat.getComments() != null) {
			float sum = (float) commentStat.getScore() / commentStat.getComments();
			DecimalFormat df = new DecimalFormat("0.0");// 格式化小数
			score = df.format(sum);// 返回的是String类型
		}
		return score;
	}

	@Override
	public List<ShopDetail> getAllShopDetail() {
		return shopDetailDao.getAllShopDetail();
	}

	@Override
	public List<IdTextEntity> getShopDetailList(String shopName) {
		return shopDetailDao.getShopDetailList(shopName);
	}

	/**
	 * 商家查询
	 */
	@Override
	public PageSupport<ShopSeekDto> queryShopSeekList(ShopSearchParamDto param) {
		int currPageNO = param.getCurPageNO() == null ? 1 : param.getCurPageNO();
		int offset = (currPageNO - 1) * param.getPageSize();
		int pageSize = param.getPageSize();

		QueryMap queryMap = new QueryMap();
		queryMap.put("prodCount", 0);
		queryMap.put("type", param.getType());
		queryMap.put("status", param.getStatus());
		if (AppUtils.isNotBlank(param.getSearchContent())) {
			queryMap.put("searchContent", "%" + param.getSearchContent() + "%");
		}

		// count语句不需要offset和pageSize,所以放到前面
		String countSQL = ConfigCode.getInstance().getCode("shopSeek.getShopListCount", queryMap);
		Long total = shopDetailDao.getLongResult(countSQL, queryMap.toArray());

		queryMap.put("offset", offset);
		queryMap.put("pagesize", param.getPageSize());
		queryMap.put("prodCount2", param.getProdCount());

		// 排序处理
		String orderWay = param.getOrderWay();
		if (AppUtils.isNotBlank(orderWay)) {
			if (ShopSearchParamDto.TIME.equals(orderWay)) {
				queryMap.put("orderBy", "o.rec_date ASC");
			} else if (ShopSearchParamDto.SYNTHESIZE.equals(orderWay)) {
				queryMap.put("orderBy", "o.visit_times DESC, o.product_num DESC, o.rec_date ASC");
			} else if (ShopSearchParamDto.VISITNUM.equals(orderWay)) {
				queryMap.put("orderBy", "o.visit_times DESC");
			} else {
				queryMap.put("orderBy", "o.product_num DESC");
			}
		} else {
			queryMap.put("orderBy", "o.rec_date ASC");
		}
		String subSQL = ConfigCode.getInstance().getCode("shopSeek.getShopList", queryMap);

		queryMap.remove("orderBy");
		List<ShopSeekDto> shopList = shopDetailDao.getShopSeekList(subSQL, queryMap.toArray());
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, currPageNO, pageSize);
		PageSupport<ShopSeekDto> ps = new PageSupport<ShopSeekDto>(shopList, pageProvider);
		return ps;
	}

	@Override
	public PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail) {
		return shopDetailDao.getShopDetailPage(curPageNO, shopDetail);
	}

	@Override
	public PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail, Integer pageSize, DataSortResult result) {
		return shopDetailDao.getShopDetailPage(curPageNO, shopDetail, pageSize, result);
	}

	@Override
	public PageSupport<ShopAudit> getShopAuditPage(String curPageNO, Long shopId) {
		return shopAuditDao.getShopAuditPage(curPageNO, shopId);
	}

	@Override
	public PageSupport<ShopDetail> getloadSelectShop(String curPageNO, String shopName) {
		return shopDetailDao.getloadSelectShop(curPageNO, shopName);
	}

	@Override
	public int updateShopCommision(Long shopId, Integer commision) {
		return shopDetailDao.updateShopCommision(shopId, commision);
	}

	@Override
	public void save(String userName, String shopPic, ShopDetail shopDetail) {
		if (shopDetail.getFile().getSize() > 0) {
			// 保存附件表
			attachmentManager.saveImageAttachment(userName, shopDetail.getUserId(), shopDetail.getShopId(), shopPic, shopDetail.getFile(), AttachmentTypeEnum.USER);
			shopDetail.setShopPic(shopPic);
		}
		shopDetailDao.saveShopDetail(shopDetail);
	}

	@Override
	public ShopDetail getTypeShopDetailByUserId(String userId) {

		return shopDetailDao.getTypeShopDetailByUserId(userId);
	}

	@Override
	public R<String> savePersonalOpenShopInfo(LoginedUserInfo user, ShopDetail shopDetail, OpenShopInfoDTO openShopInfoDTO) {

		// 保存
		if (AppUtils.isBlank(shopDetail)) {
			shopDetail = new ShopDetail();
			shopDetail.setRealPath(propertiesUtil.getBigPicPath());
			shopDetail.setSiteName(XssFilterUtil.cleanXSS(openShopInfoDTO.getSiteName().trim()));
			shopDetail.setOpStatus(ShopOPstatusEnum.SHOP_INFO_OK.value());
			shopDetail.setIdCardPic(openShopInfoDTO.getIdCardPic());
			shopDetail.setIdCardBackPic(openShopInfoDTO.getIdCardBackPic());

			shopDetail.setShopType(openShopInfoDTO.getShopType());
			shopDetail.setType(openShopInfoDTO.getType());

			shopDetail.setContactName(openShopInfoDTO.getContactName());
			shopDetail.setContactMobile(openShopInfoDTO.getContactMobile());
			shopDetail.setContactMail(openShopInfoDTO.getContactMail());

			shopDetail.setUserId(user.getUserId());
			shopDetail.setUserName(user.getUserName());
			// 保存状态
			shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
			shopDetail.setRecDate(new Date());
			shopDetail.setModifyDate(new Date());
			shopDetail.setVisitTimes(0L);
			shopDetail.setOffProductNum(0L);
			shopDetail.setProductNum(0L);
			shopDetail.setCommNum(0L);
			shopDetail.setCapital(0d);
			shopDetail.setCredit(0);
			shopDetail.setGradeId(0);
			// 得到创建时的国家和地区
			if (shopDetail.getIp() != null) {
				shopDetail.setCreateAreaCode(IPSeeker.getInstance().getArea(openShopInfoDTO.getIp()));
				shopDetail.setCreateCountryCode(IPSeeker.getInstance().getCountry(openShopInfoDTO.getIp()));
			}
			Long result = shopDetailDao.save(shopDetail);
			if (AppUtils.isBlank(result) || result < 0) {
				return R.fail("保存失败");
			}
			this.changeOptionStatus(user.getUserId(), result, ShopOPstatusEnum.SHOP_INFO_OK.value());
			// 更新
		} else {

			shopDetail.setIdCardPic(openShopInfoDTO.getIdCardPic());
			shopDetail.setIdCardBackPic(openShopInfoDTO.getIdCardBackPic());
			shopDetail.setShopType(openShopInfoDTO.getShopType());
			shopDetail.setType(openShopInfoDTO.getType());
			shopDetail.setContactName(openShopInfoDTO.getContactName());
			shopDetail.setContactMobile(openShopInfoDTO.getContactMobile());
			shopDetail.setContactMail(openShopInfoDTO.getContactMail());
			shopDetail.setSiteName(openShopInfoDTO.getSiteName());
			shopDetail.setModifyDate(new Date());
			shopDetail.setStatus(ShopStatusEnum.AUDITING.value());

			int result = shopDetailDao.update(shopDetail);
			if (AppUtils.isBlank(result) || result < 0) {
				return R.fail("更新失败");
			}
		}
		return R.success();
	}

	@Override
	public R<String> saveBusinessOpenShopInfo(LoginedUserInfo user, ShopDetail shopDetail, OpenShopInfoDTO openShopInfoDTO) {

		// 保存
		if (AppUtils.isBlank(shopDetail)) {
			shopDetail = new ShopDetail();
			shopDetail.setRealPath(propertiesUtil.getBigPicPath());
			shopDetail.setSiteName(XssFilterUtil.cleanXSS(openShopInfoDTO.getSiteName().trim()));
			shopDetail.setOpStatus(ShopOPstatusEnum.SHOP_INFO_OK.value());

			shopDetail.setShopType(openShopInfoDTO.getShopType());
			shopDetail.setType(openShopInfoDTO.getType());
			shopDetail.setIdCardNum(openShopInfoDTO.getIdCardNum());

			shopDetail.setContactName(openShopInfoDTO.getContactName());
			shopDetail.setContactMobile(openShopInfoDTO.getContactMobile());
			shopDetail.setContactMail(openShopInfoDTO.getContactMail());

			shopDetail.setUserId(user.getUserId());
			shopDetail.setUserName(user.getUserName());
			// 保存状态
			shopDetail.setStatus(ShopStatusEnum.AUDITING.value());
			shopDetail.setRecDate(new Date());
			shopDetail.setModifyDate(new Date());
			shopDetail.setVisitTimes(0L);
			shopDetail.setOffProductNum(0L);
			shopDetail.setProductNum(0L);
			shopDetail.setCommNum(0L);
			shopDetail.setCapital(0d);
			shopDetail.setCredit(0);
			shopDetail.setGradeId(0);
			// 得到创建时的国家和地区
			if (shopDetail.getIp() != null) {
				shopDetail.setCreateAreaCode(IPSeeker.getInstance().getArea(openShopInfoDTO.getIp()));
				shopDetail.setCreateCountryCode(IPSeeker.getInstance().getCountry(openShopInfoDTO.getIp()));
			}
			Long result = shopDetailDao.save(shopDetail);
			if (AppUtils.isBlank(result) || result < 0) {
				return R.fail("保存店铺信息失败");
			}
			this.changeOptionStatus(user.getUserId(), result, ShopOPstatusEnum.SHOP_INFO_OK.value());

			// 保存企业信息
			ShopCompanyDetail shopCompanyDetail = new ShopCompanyDetail();

			shopCompanyDetail.setShopId(result);
			shopCompanyDetail.setLicensePic(openShopInfoDTO.getLicensePicFilePath());
			shopCompanyDetail.setCompanyName(openShopInfoDTO.getCompanyName());
			shopCompanyDetail.setLicenseNumber(openShopInfoDTO.getLicenseNumber());

			Long shopCompanyresult = shopCompanyDetailDao.saveShopCompanyDetail(shopCompanyDetail);
			if (AppUtils.isBlank(shopCompanyresult) || shopCompanyresult < 0) {
				return R.fail("保存店铺企业信息失败");
			}

			// 更新
		} else {

			shopDetail.setIdCardPic(openShopInfoDTO.getIdCardPic());
			shopDetail.setIdCardBackPic(openShopInfoDTO.getIdCardBackPic());
			shopDetail.setShopType(openShopInfoDTO.getShopType());
			shopDetail.setType(openShopInfoDTO.getType());
			shopDetail.setContactName(openShopInfoDTO.getContactName());
			shopDetail.setContactMobile(openShopInfoDTO.getContactMobile());
			shopDetail.setContactMail(openShopInfoDTO.getContactMail());
			shopDetail.setSiteName(openShopInfoDTO.getSiteName());
			shopDetail.setIdCardNum(openShopInfoDTO.getIdCardNum());
			shopDetail.setModifyDate(new Date());
			shopDetail.setStatus(ShopStatusEnum.AUDITING.value());

			int result = shopDetailDao.update(shopDetail);
			if (AppUtils.isBlank(result) || result < 0) {
				return R.fail("更新店铺信息失败");
			}
			ShopCompanyDetail oldShopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopDetail.getShopId());
			oldShopCompanyDetail.setCompanyName(openShopInfoDTO.getCompanyName());
			oldShopCompanyDetail.setLicenseNumber(openShopInfoDTO.getLicenseNumber());
			oldShopCompanyDetail.setLicensePic(openShopInfoDTO.getLicensePicFilePath());
			shopCompanyDetailService.updateShopCompanyDetail(oldShopCompanyDetail);

		}

		return R.success();
	}

	@Override
	public BusinessMessageDTO getBusinessMessage(Long shopId) {
		BusinessMessageDTO businessMessage = shopDetailDao.getBusinessMessage(shopId);
		ShopCompanyDetail companyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopId);
		if (businessMessage != null) {
			if (companyDetail != null)
				businessMessage.setEnterpriseName(companyDetail.getCompanyName());
			String businessImage = businessMessage.getBusinessImage();
			List<String> list = new ArrayList<>();
			if (businessImage != null && !businessImage.isEmpty()) {
				String[] split = businessMessage.getBusinessImage().split(",");
				for (int x = 0; x < split.length; x++) {
					list.add(split[x]);
				}
				businessMessage.setImageList(list);
			}
		}
		return businessMessage;
	}

	@Override
	public Integer getShopStatus(Long shopId) {
		return shopDetailDao.getShopStatus(shopId);
	}
}
