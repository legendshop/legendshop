/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.group;

import com.legendshop.business.dao.GroupSkuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GroupSku;
import com.legendshop.spi.service.GroupSkuService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class GroupSkuServiceImpl.
 *  团购活动参加商品服务实现类
 */
@Service("groupSkuService")
public class GroupSkuServiceImpl implements GroupSkuService {

    /**
     *
     * 引用的团购活动参加商品Dao接口
     */
    @Autowired
    private GroupSkuDao groupSkuDao;
   
   	/**
	 *  根据Id获取团购活动参加商品
	 */
    public GroupSku getGroupSku(Long id) {
        return groupSkuDao.getGroupSku(id);
    }
    
    /**
	 *  根据Id删除团购活动参加商品
	 */
    public int deleteGroupSku(Long id){
    	return groupSkuDao.deleteGroupSku(id);
    }

   /**
	 *  删除团购活动参加商品
	 */ 
    public int deleteGroupSku(GroupSku groupSku) {
       return  groupSkuDao.deleteGroupSku(groupSku);
    }

   /**
	 *  保存团购活动参加商品
	 */	    
    public Long saveGroupSku(GroupSku groupSku) {
        if (!AppUtils.isBlank(groupSku.getId())) {
            updateGroupSku(groupSku);
            return groupSku.getId();
        }
        return groupSkuDao.saveGroupSku(groupSku);
    }

   /**
	 *  更新团购活动参加商品
	 */	
    public void updateGroupSku(GroupSku groupSku) {
        groupSkuDao.updateGroupSku(groupSku);
    }


    /**
	 *  分页查询列表
	 */	
    public PageSupport<GroupSku> queryGroupSku(String curPageNO, Integer pageSize){
     	return groupSkuDao.queryGroupSku(curPageNO, pageSize);
    }

	@Override
	public void deleteGroupSkuByGroupId(Long id) {
		groupSkuDao.deleteGroupSkuByGroupId(id);
	}

	/**
	 *  设置Dao实现类
	 */	    
    public void setGroupSkuDao(GroupSkuDao groupSkuDao) {
        this.groupSkuDao = groupSkuDao;
    }
    
}
