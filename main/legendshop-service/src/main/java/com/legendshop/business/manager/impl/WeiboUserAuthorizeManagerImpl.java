/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.manager.impl;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.service.weibo.WeiboUserAuthorizeApi;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.ThirdUserToken;
import com.legendshop.model.dto.weibo.WeiboAuthorizeUserInfo;
import com.legendshop.model.dto.weibo.WeiboUserToken;

/**
 * 微博用户授权认证管理器实现
 * @author 开发很忙
 */
@Service
public class WeiboUserAuthorizeManagerImpl extends TemplateThirdUserAuthorizeManagerImpl {
	
	private static Logger log = LoggerFactory.getLogger(WeiboUserAuthorizeManagerImpl.class);
	
	/** 授权范围 */
	private static final String SCOPE = "";
	
	/** 授权类型 */
	private static final String RESPONSE_TYPE = "code";
	
	/** 回调地址路径 */
	private static final String REDIRECT_URL_PATH= "/thirdlogin/sinaweibo/{source}/callback";
	
	/** 是否强制登录 */
	private static final boolean FORCELOGIN= true;
	
	/** 授权地址模板 */
	private static final String AUTHORIZE_URL = "https://api.weibo.com/oauth2/authorize?response_type=RESPONSE_TYPE&client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&state=STATE&scope=SCOPE&display=DISPLAY&forcelogin=FORCELOGIN";
	
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	protected String authorize(String source) {
		
		String state = gennerateState();
		
		String clientId = null;
		String redirectURI = "";
		String display = "";
		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5
			clientId = systemParameterUtil.getWeiboH5AppId();
			redirectURI = propertiesUtil.getUserAppDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
			display = "mobile";
		}else{//否则就是PC了
			clientId = systemParameterUtil.getWeiboAppId();
			redirectURI = propertiesUtil.getPcDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}
		
		String authorizeURL = AUTHORIZE_URL.replace("RESPONSE_TYPE", RESPONSE_TYPE)
				.replace("CLIENT_ID", clientId)
				.replace("REDIRECT_URI", redirectURI)
				.replace("STATE", state)
				.replace("SCOPE", SCOPE)
				.replace("DISPLAY", display)
				.replace("FORCELOGIN", String.valueOf(FORCELOGIN));
		
		return authorizeURL;
	}

	@Override
	protected ThirdUserToken getAccessToken(String code, String source) {
		
		String clientId = null;
		String clientSecret = null;
		
		String redirectURI = "";
		if(PassportSourceEnum.H5.value().equals(source)){//如果是H5
			clientId = systemParameterUtil.getWeiboH5AppId();
			clientSecret = systemParameterUtil.getWeiboH5Secret();
			redirectURI = propertiesUtil.getUserAppDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}else{//否则就是PC了
			clientId = systemParameterUtil.getWeiboAppId();
			clientSecret = systemParameterUtil.getWeiboSecret();
			redirectURI = propertiesUtil.getPcDomainName() + REDIRECT_URL_PATH.replace("{source}", source);
		}
		
		WeiboUserToken token = WeiboUserAuthorizeApi.getAccessToken("authorization_code", clientId, clientSecret, code, redirectURI);
		if(null == token){
			log.info("############## 获取微博 token失败 ####################");
			return null;
		}
		
		if(!token.isSuccess()){
			log.info("############## 获取微博 token失败 = {} ####################", token.getError());
			return null;
		}
		
		return new ThirdUserToken(token);
	}

	@Override
	protected ThirdUserInfo getUserInfo(ThirdUserToken token) {
		
		WeiboAuthorizeUserInfo userInfo = WeiboUserAuthorizeApi.getUserInfo(token.getAccessToken(), token.getOpenId());
		if(null != userInfo){
			if(!userInfo.isSuccess()){
				log.info("############## 获取微博用户信息失败 = {} ####################", userInfo.getError());
				return null;
			}
		}
		
		return new ThirdUserInfo(token, userInfo);
	}

	@Override
	protected ThirdUserInfo getMpUserInfo(String code, String encryptedData, String iv) {
		throw new BusinessException("暂不支持微博小程序");
	}
	
	@Override
	protected ThirdUserAuthorizeResult authThirdUser(ThirdUserInfo thirdUserInfo, String source) {
		
		return thirdLoginService.authThridUser(thirdUserInfo, PassportTypeEnum.SINAWEIBO.value(), source);
	}

}
