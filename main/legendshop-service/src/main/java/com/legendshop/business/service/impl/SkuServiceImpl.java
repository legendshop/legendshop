/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.*;

import com.legendshop.model.entity.Sub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ProductPropertyDao;
import com.legendshop.business.dao.ProductPropertyValueDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyInfo;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;

/**
 * 单品库存ServiceImpl.
 */
@Service("skuService")
public class SkuServiceImpl  implements SkuService{

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SkuServiceImpl.class);

	@Autowired
    private SkuDao skuDao;

    @Autowired
    private ProductPropertyDao productPropertyDao;

    @Autowired
	private ProductPropertyValueDao productPropertyValueDao;

    public Sku getSku(Long id) {
        return skuDao.getSku(id);
    }

    public void deleteSku(Sku sku) {
        skuDao.deleteSku(sku);
    }

    public Long saveSku(Sku sku) {
        if (!AppUtils.isBlank(sku.getSkuId())) {
            updateSku(sku);
            return sku.getSkuId();
        }
        return (Long) skuDao.save(sku);
    }

    public void updateSku(Sku sku) {
        skuDao.updateSku(sku);
    }

	@Override
	public List<Sku> getSkuByProd(Long prodId) {
		return skuDao.getSkuByProd(prodId);
	}

	/**
	 * 根据商品id获取 属性和属性值
	 * @param prodDto
	 * @param productSkus
	 * @return
	 */
	@Override
	public List<ProductPropertyDto> getPropValListByProd(ProductDto prodDto,List<Sku> productSkus,Map<Long,List<String>> valueImagesMap){
		List<Long> selectedVals = new ArrayList<Long>();
		if(AppUtils.isNotBlank(productSkus)){
			//取得价格最低的 sku 作为默认选中的sku
			Sku defaultSku = getDefaultSku(productSkus);
			prodDto.setCash(defaultSku.getPrice());
			prodDto.setSkuId(defaultSku.getSkuId());
			prodDto.setSkuType(defaultSku.getSkuType());
			prodDto.setStocks(Integer.parseInt(defaultSku.getStocks().toString()));
			if(AppUtils.isNotBlank(defaultSku.getName())){
				prodDto.setName(defaultSku.getName());
			}
			//取得sku字符串(k1:v1;k2:v2;k3:v3)中的每对key:value,并对其进行相应处理
			Map<Long,List<Long>> propIdMap = new LinkedHashMap<Long,List<Long>>();
			List<Long> allValIdList = new ArrayList<Long>();
			for (Sku sku : productSkus) {
				String properties = sku.getProperties();
				if(AppUtils.isNotBlank(properties)){
					String skuStrs[] = properties.split(";");
					for (int i = 0; i < skuStrs.length; i++) {
						String skuItems[] = skuStrs[i].split(":");
						Long propId = Long.valueOf(skuItems[0]);
						Long valueId = Long.valueOf(skuItems[1]);
						if(sku.getSkuId().equals(defaultSku.getSkuId())){
							selectedVals.add(valueId);
						}
						List<Long> valIdList = propIdMap.get(propId);
						if(AppUtils.isBlank(valIdList)){
							valIdList = new ArrayList<Long>();
							valIdList.add(valueId);
						}else{
							if(!valIdList.contains(valueId)){
								valIdList.add(valueId);
							}
						}
						propIdMap.put(propId, valIdList);
						allValIdList.add(valueId);
					}
				}else{
					log.warn("sku id {} does not have correct properties setting", sku.getId());
				}
			}

			List<Long> propIds = new ArrayList<Long>(propIdMap.keySet());//set 转 list
			if(AppUtils.isBlank(propIds)){
				return null;
			}

			//根据propId的集合 ，查出属性集合
			List<ProductProperty> pps = productPropertyDao.getProductProperty(propIds, prodDto.getCategoryId());
			List<ProductPropertyDto> ppds = new ArrayList<ProductPropertyDto>();
			//根据valId的集合，查出属性值的集合
			List<ProductPropertyValue> allPropVals = productPropertyValueDao.getProductPropertyValue(allValIdList,prodDto.getProdId());
			for (ProductProperty prodProp : pps) {
				//封装属性DTO
				ProductPropertyDto prodPropDto = new ProductPropertyDto();
				prodPropDto.setPropId(prodProp.getId());//属性id
				prodPropDto.setPropName(prodProp.getMemo());//属性名称

				//根据propId拿到valueId的集合
				List<Long> valIds = propIdMap.get(prodProp.getId());

				//根据valueId的集合,查出属性值的集合
				List<ProductPropertyValue> ppvs = getPropValsByIds(valIds,allPropVals);
				List<ProductPropertyValueDto> ppvds = new ArrayList<ProductPropertyValueDto>();
				for (ProductPropertyValue prodPropVal : ppvs) {
					//封装属性值DTO
					ProductPropertyValueDto ppvd = new ProductPropertyValueDto();
					ppvd.setValueId(prodPropVal.getId());//属性值id
					if(AppUtils.isNotBlank(prodPropVal.getAlias())){
						ppvd.setName(prodPropVal.getAlias());//属性值名称
					}else{
						ppvd.setName(prodPropVal.getName());//属性值名称
					}
					ppvd.setPropId(prodProp.getId());//属性id

					List<String> imgs = valueImagesMap.get(prodPropVal.getId());
					if(AppUtils.isNotBlank(imgs)){
						prodPropVal.setUrl(imgs.get(0));
					}
					if(selectedVals.contains(prodPropVal.getId())){
						ppvd.setIsSelected(true);//是否默认选中
						if(AppUtils.isNotBlank(prodPropVal.getUrl())){
							prodDto.setPic(prodPropVal.getUrl());
						}
					}
					if(AppUtils.isNotBlank(prodPropVal.getUrl())){
						ppvd.setPic(prodPropVal.getUrl());
					}else{
						ppvd.setPic(prodPropVal.getPic());
					}
					ppvds.add(ppvd);
				}

				prodPropDto.setProductPropertyValueList(ppvds);
				ppds.add(prodPropDto);
			}
			return ppds;
		}else{
			return null;
		}
	}

	/**
	 * //根据valueId的集合,查出属性值的集合
	 * @param valIds
	 * @param allPropVals
	 * @return
	 */
	private List<ProductPropertyValue> getPropValsByIds(List<Long> valIds, List<ProductPropertyValue> allPropVals) {
		List<ProductPropertyValue> ppvs = new ArrayList<ProductPropertyValue>();
		for (ProductPropertyValue productPropertyValue : allPropVals) {
			if(valIds.contains(productPropertyValue.getValueId())){
				ppvs.add(productPropertyValue);
			}
		}
		return ppvs;
	}


	/**
	 * 查找最低价格的sku作为默认的sku
	 * @return
	 */
	private Sku getDefaultSku(List<Sku> productSkus){
		double price = 0;
		int minPos = 0; //最小值位置
		for (int i = 0; i < productSkus.size(); i++) {
			// 2020-08-20 update by jzh: 解决拆箱空指针问题
			if (AppUtils.isBlank(productSkus.get(i).getPrice())) {
				continue;
			}
			double minPrice = productSkus.get(i).getPrice();
			if((minPrice < price || price==0) && productSkus.get(i).getStatus().equals(1)){
				price = minPrice;
				minPos = i;
			}

		}
		return productSkus.get(minPos);
	}

	private List<KeyValueEntity> splitJoinSku(String properties,String expr) {
		List<KeyValueEntity>  skuKVs=new ArrayList<KeyValueEntity>();
		if (properties != null && properties != "") {
			String[] proValue = properties.split(expr);
			if (proValue != null && proValue.length > 0) {
				int length = proValue.length;
				for (int i = 0; i < length; i++) {
					String[] valeus = proValue[i].split(":");
					if (valeus != null && valeus.length > 1) {
						KeyValueEntity kv = new KeyValueEntity();
						kv.setKey(valeus[0]);
						kv.setValue(valeus[1]);
						skuKVs.add(kv);
					}
				}
			}
		}
		return skuKVs;
	}


	//TODO 属性已经在sku中缓存，无需再计算
	@Deprecated
	@Override
	public String getSkuByProd(Sku sku) {
			if(AppUtils.isNotBlank(sku)){
				List<KeyValueEntity> keyValueEntities=splitJoinSku(sku.getProperties(),";");
				List <ProductPropertyInfo> props = new ArrayList<ProductPropertyInfo>();
				for (int i = 0; i < keyValueEntities.size(); i++) {
					KeyValueEntity keyValueEntity=keyValueEntities.get(i);

					ProductProperty productProperty = productPropertyDao.getProductProperty(Long.valueOf(keyValueEntity.getKey()));
					ProductPropertyValue  productPropertyValue  = productPropertyValueDao.getProductPropertyValueInfo(Long.valueOf(keyValueEntity.getValue()),sku.getProdId());
					if(productProperty != null && productPropertyValue != null){
						ProductPropertyInfo productPropertyInfo = new ProductPropertyInfo();
						productPropertyInfo.setAttrName(productProperty.getMemo());
						if(AppUtils.isNotBlank(productPropertyValue.getAlias())){
							productPropertyInfo.setAttrValue(productPropertyValue.getAlias());
						}else{
							productPropertyInfo.setAttrValue(productPropertyValue.getName());
						}
						props.add(productPropertyInfo);
					}
				}
				String attribute = "";
				if(AppUtils.isNotBlank(props)){
					for (int i = 0; i < props.size(); i++) {
						ProductPropertyInfo prop = props.get(i);
						attribute += prop.getAttrName()+":"+prop.getAttrValue();
						if(i<props.size()-1){
							attribute+=";";
						}
					}
				}
				return attribute;
			}
			return null;
	}


	@Override
	public List<KeyValueEntity> getSkuImg(String skuProp) {
		List<KeyValueEntity> keyValueEntities=splitJoinSku(skuProp,";");
		return keyValueEntities;
	}


	@Override
	public Sku getSkuByProd(Long prodId, Long skuId) {
		return skuDao.getSkuByProd(prodId,skuId);
	}

	@Override
	public List<Sku>  getSku(Long [] skuId) {
		return skuDao.getSku(skuId);
	}


	@Override
	public BasketDto findBasketDtos(Long prodId,Long skuId) {
		return skuDao.findBasketDtos(prodId,skuId);
	}


	@Override
	public PageSupport<Sku> getSkuPage(String curPageNO, Sku sku) {
		return skuDao.getSkuPage(curPageNO,sku);
	}


	@Override
	public PageSupport<Sku> getAuctionProdSku(String curPageNO, Long prodId) {
		return skuDao.getAuctionProdSku(curPageNO,prodId);
	}


	@Override
	public PageSupport<Sku> getSku(String curPageNO, Long prodId, int status, int stocks) {
		return skuDao.queryPage(curPageNO,prodId,status,stocks);
	}


	@Override
	public PageSupport<Sku> getseckillProdSku(String curPageNO, Long prodId) {
		 return skuDao.getseckillProdSku(curPageNO,prodId);
	}


	@Override
	public PageSupport<Sku> getSku(String curPageNO, Long prodId, int status) {
		   return skuDao.getSku(curPageNO,prodId,status);
	}

	@Override
	public PageSupport<Sku> getloadProdSkuPage(String curPageNO, Long prodId) {
		return skuDao.getloadProdSkuPage(curPageNO,prodId);
	}


	@Override
	public PageSupport<Sku> getLoadProdSkuByProdId(String curPageNO, Long prodId) {
		return skuDao.getLoadProdSkuByProdId(curPageNO,prodId);
	}


	@Override
	public PageSupport<Sku> loadSkuListPage(String curPageNO, Long prodId, String name) {
		return skuDao.loadSkuListPage(curPageNO,prodId,name);
	}


	@Override
	public List<Sku> findSkuByPropId(Long propId) {
		List<Sku> skuByPropIdOrd = skuDao.findSkuByPropId(propId);
		List<Sku> skuByPropIdNew = new ArrayList<>();
		List<Long> prodValues = productPropertyValueDao.getValueIdsByPropId(propId);
		if (AppUtils.isNotBlank(skuByPropIdOrd)) {
			for (Sku sku : skuByPropIdOrd) {
				String[] split = sku.getProperties().split(";");
				List<String> propertiesList = Arrays.asList(split);
				for (Long prodValue : prodValues) {
					String str = propId + ":" + prodValue;
					if (propertiesList.contains(str)) {
						skuByPropIdNew.add(sku);
						break;
					}
				}
			}
		}
		return skuByPropIdNew;
	}

	@Override
	public List<Sku> findSkuByProp(String prop) {
		return skuDao.findSkuByProp(prop);
	}

	@Override
	public List<Sku> findSkuByPropIdAndProdTypeId(Long propId,Long prodTypeId) {
		return skuDao.findSkuByPropIdAndProdTypeId(propId,prodTypeId);
	}

	@Override
	public List<Sku> getSkuForMergeGroup(Long prodId, Long mergeGroupId) {
		return skuDao.getSkuForMergeGroup(prodId,mergeGroupId);
	}

	@Override
	public List<Sku> getSkuListByProdId(Long productId) {
		return skuDao.getSkuByProdId(productId);
	}

	@Override
	public Sku getSkuById(Long skuId) {
		return skuDao.getSkuById(skuId);
	}

	@Override
	public String isProdChange(Long prodId) {
		//获取sku列表
		List<Sku> skuList = skuDao.getSkuByProd(prodId);
		if (AppUtils.isBlank(skuList)) {
			return "商品信息异常！";
		}
		for (Sku sku : skuList) {
			if (SkuActiveTypeEnum.PRODUCT.value().equals(sku.getSkuType())) {
				continue ;
			}
			if (SkuActiveTypeEnum.AUCTION.value().equals(sku.getSkuType())) {
				return "该商品正在参加拍卖活动，暂时无法修改！";
			}
			if (SkuActiveTypeEnum.GROUP.value().equals(sku.getSkuType())) {
				return "该商品正在参加团购活动，暂时无法修改！";
			}
			if (SkuActiveTypeEnum.MERGE_GROUP.value().equals(sku.getSkuType())) {
				return "该商品正在参加拼团活动，暂时无法修改！";
			}
			if (SkuActiveTypeEnum.PRESELL.value().equals(sku.getSkuType())) {
				return "该商品正在参加预售活动，暂时无法修改！";
			}
			if (SkuActiveTypeEnum.SECKILL.value().equals(sku.getSkuType())) {
				return "该商品正在参加秒杀活动，暂时无法修改！";
			}
		}
		return Constants.SUCCESS;
	}

}
