/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliverGoodsAddr;

/**
 * 发货地址Dao.
 */
public interface DeliverGoodsAddrDao extends GenericDao<DeliverGoodsAddr, Long> {
     
    public abstract List<DeliverGoodsAddr> getDeliverGoodsAddrList(Long shopId);

	public abstract DeliverGoodsAddr getDeliverGoodsAddr(Long id);
	
    public abstract int deleteDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr);
	
	public abstract Long saveDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr);
	
	public abstract int updateDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr);
	
	public abstract void turnonDefault(Long id, Long shopId);

	public abstract void closeDefault(Long id, Long shopId);

	public abstract void deleteDeliverGoodsAddr(Long addrId, Long shopId);

	public abstract DeliverGoodsAddr getDefultDeliverGoodsAddr(Long shopId,int i);

	public abstract PageSupport<DeliverGoodsAddr> getDeliverGoodsAddrPage(String curPageNO, Long shopId);

 }
