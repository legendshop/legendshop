/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ShopAuditDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopAudit;

/**
 * 店铺审核Dao
 */
@Repository
public class ShopAuditDaoImpl extends GenericDaoImpl<ShopAudit, Long> implements ShopAuditDao  {

	public ShopAudit getShopAudit(Long id){
		return getById(id);
	}
	
    public int deleteShopAudit(ShopAudit shopAudit){
    	return delete(shopAudit);
    }
	
	public Long saveShopAudit(ShopAudit shopAudit){
		return save(shopAudit);
	}
	
	public int updateShopAudit(ShopAudit shopAudit){
		return update(shopAudit);
	}

	@Override
	public List<ShopAudit> getShopAuditInfo(Long shopId) {
		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId).addDescOrder("modifyDate"));
	}

	@Override
	public PageSupport<ShopAudit> getShopAuditPage(String curPageNO, Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(ShopAudit.class, curPageNO);
		cq.eq("shopId", shopId);
		cq.addDescOrder("modifyDate");
		return queryPage(cq);
	}
	
 }
