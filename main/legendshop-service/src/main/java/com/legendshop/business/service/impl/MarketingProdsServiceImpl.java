/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.SkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.MarketingDao;
import com.legendshop.business.dao.MarketingProdsDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.model.entity.MarketingProds;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;

/**
 * 参加营销活动的商品服务.
 */
@Service("marketingProdsService")
public class MarketingProdsServiceImpl  implements MarketingProdsService{

	@Autowired
    private MarketingProdsDao marketingProdsDao;

	@Autowired
    private MarketingDao marketingDao;

	@Autowired
    private MarketingService marketingService;

	@Autowired
	private SkuService skuService;

	@Override
	public List<MarketingProds> getMarketingProdsByMarketId(Long marketId) {
		return marketingProdsDao.getMarketingProdsByMarketId(marketId);
	}

    public MarketingProds getMarketingProds(Long id) {
        return marketingProdsDao.getMarketingProds(id);
    }

    public void deleteMarketingProds(MarketingProds marketingProds) {
        marketingProdsDao.deleteMarketingProds(marketingProds);
    }

    public Long saveMarketingProds(MarketingProds marketingProds) {
        if (!AppUtils.isBlank(marketingProds.getId())) {
            updateMarketingProds(marketingProds);
            return marketingProds.getId();
        }
        return marketingProdsDao.saveMarketingProds(marketingProds);
    }

    public void updateMarketingProds(MarketingProds marketingProds) {
        marketingProdsDao.updateMarketingProds(marketingProds);
    }

	@Override
	public PageSupport<MarketingProdDto> findMarketingRuleProds(String curPage, Long shopId, Long marketingId) {
		PageSupport<MarketingProdDto> pageSupport= marketingProdsDao.findMarketingRuleProds(curPage,shopId,marketingId);
		List<MarketingProdDto> list= (List<MarketingProdDto>) pageSupport.getResultList();
		if(AppUtils.isNotBlank(list)){
			for (Iterator<MarketingProdDto> iterator = list.iterator(); iterator.hasNext();) {
				MarketingProdDto marketingProdsDto = (MarketingProdDto) iterator.next();
				if(marketingProdsDto.getCnProperties()!=null){
					marketingProdsDto.setProdName(marketingProdsDto.getProdName()+"-"+marketingProdsDto.getCnProperties());
				}
			}
		}
		return pageSupport;
	}

	
	
	@Override
	public PageSupport<MarketingProdDto> findMarketingProds(String curPage, Long shopId,
			String name) {
		PageSupport<MarketingProdDto> pageSupport= marketingProdsDao.findMarketingProds(curPage,shopId,name);
		
		List<MarketingProdDto> list= (List<MarketingProdDto>) pageSupport.getResultList();
		if(AppUtils.isNotBlank(list)){
			for (Iterator<MarketingProdDto> iterator = list.iterator(); iterator.hasNext();) {
				MarketingProdDto marketingProdsDto = (MarketingProdDto) iterator.next();
				if(marketingProdsDto.getCnProperties()!=null){
					marketingProdsDto.setProdName(marketingProdsDto.getProdName()+"-"+marketingProdsDto.getCnProperties());
				}
			}
		}
		return pageSupport;
	}
	
	@Override
	public String addZkMarketingRuleProds(Long id, Long shopId, Long prodId,
			Long skuId, Double cash, Double discountPrice) {
		
		/**
		 * 查询该商品是否参与过其他活动··【活动出于上线,并且是部分商品活动】
		 */
		Long result=0l;
		if(AppUtils.isNotBlank(skuId)){ //Sku 商品
			result=marketingProdsDao.getLongResult("SELECT COUNT(ls_marketing_prods.id) FROM ls_marketing_prods  WHERE  ls_marketing_prods.sku_id=? AND ls_marketing_prods.shop_id=? AND ls_marketing_prods.market_id=? ", 
					new Object[]{skuId,shopId,id});
		}else{
			result=marketingProdsDao.getLongResult("SELECT COUNT(ls_marketing_prods.id) FROM ls_marketing_prods   WHERE  ls_marketing_prods.prod_id=? AND ls_marketing_prods.shop_id=? AND ls_marketing_prods.market_id=? ",
					new Object[]{prodId,shopId,id});
		}
		if(result>0){
			return "该商品已经参加了同时段活动";
		}
		MarketingProds marketingProds =new MarketingProds();
		marketingProds.setMarketId(id);
		marketingProds.setShopId(shopId);
		marketingProds.setProdId(prodId);
		marketingProds.setSkuId(skuId);
		marketingProds.setCash(cash);
		marketingProds.setType(MarketingTypeEnum.XIANSHI_ZE.value());
		marketingProds.setCreateTime(new Date());
		marketingProds.setDiscountPrice(discountPrice);
		marketingProdsDao.saveMarketingProds(marketingProds);
		return Constants.SUCCESS;
	}
	
	

	@Override
	public String addMarketingRuleProds(Long id,Integer type, Long shopId, Long prodId,
			Long skuId, Double cash) {
		/**
		 * 查询该商品是否参与过其他活动··【活动出于上线,并且是部分商品活动】
		 */
//		Long result=0l;
//		if(AppUtils.isNotBlank(skuId)){ //Sku 商品
//			result=marketingProdsDao.getLongResult("SELECT COUNT(lmp.id) FROM ls_marketing_prods lmp INNER JOIN ls_marketing lm ON lm.id=lmp.market_id WHERE lm.state=1 AND lmp.sku_id =? AND lmp.shop_id =?", 
//					new Object[]{skuId,shopId});
//		}else{
//			result=marketingProdsDao.getLongResult("SELECT COUNT(lmp.id) FROM ls_marketing_prods lmp INNER JOIN ls_marketing lm ON lm.id=lmp.market_id WHERE lm.state=1 AND lmp.prod_id =? AND lmp.shop_id =?",
//					new Object[]{prodId,shopId});
//		}
//		if(result>0){
//			return "该商品不能同时参加相同活动";
//		}
		/*if(AppUtils.isNotBlank(skuId)){ //商品
			result=marketingProdsDao.getLongResult("SELECT COUNT(ls_marketing_prods.id) FROM ls_marketing_prods LEFT JOIN ls_marketing m ON  m.id=ls_marketing_prods.market_id " +
					" WHERE  ls_marketing_prods.sku_id=? AND ls_marketing_prods.shop_id=? AND m.type=? AND  ", 
					new Object[]{skuId,shopId,type});
		}else{
			result=marketingProdsDao.getLongResult("SELECT COUNT(ls_marketing_prods.id) FROM ls_marketing_prods LEFT JOIN ls_marketing m ON  m.id=ls_marketing_prods.market_id " +
					"  WHERE ls_marketing_prods.prod_id=? AND ls_marketing_prods.shop_id=? AND m.type=? ",
					new Object[]{prodId,shopId,type});
		}
		*/
		
		MarketingProds marketingProds =new MarketingProds();
		marketingProds.setMarketId(id);
		marketingProds.setShopId(shopId);
		marketingProds.setProdId(prodId);
		marketingProds.setSkuId(skuId);
		marketingProds.setCash(cash);
		marketingProds.setType(type);
		marketingProds.setCreateTime(new Date());
		marketingProdsDao.saveMarketingProds(marketingProds);
		return Constants.SUCCESS;
	}

	@Override
	public String deleteMarketingRuleProds(Long id, Long shopId) {
		int count= marketingProdsDao.update("delete from ls_marketing_prods where shop_id=? and id=? ",shopId,id);
		if(count>0){
			return Constants.SUCCESS;
		}
		return "删除失败";
	}

	@Override
	@CacheEvict(value="GlobalMarketingDtoList",key="#shopId")
	public void clearGlobalMarketCache(Long shopId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@CacheEvict(value="MarketingDtoList",key="#prodId+#shopId")
	public void clearProdMarketCache(Long shopId, Long prodId) {
		
	}

	@Override
	@CacheEvict(value="MarketingDtoList",allEntries=true)
	public void clearProdMarketCache() {
	}

	@Override
	public void deleteMarketingProds(Long marketingId, Long shopId, MarketingProds marketingProds) {
		marketingProdsDao.deleteMarketingProds(marketingProds);
		Long count = marketingService.isExistProds(marketingId, shopId); // 是否添加了商品信息
		if (count == 0) { // 该活动移除所有的商品信息后,会自动下线
			Marketing marketing = marketingDao.getMarketing(marketingId);
			if (marketing.getState().intValue() == 1) {
				marketing.setState(0);
				marketingDao.updateMarketing(marketing);
			}
		}
		
	}

	/**
	 * 检查当前商品sku是否参加了其他促销活动
	 */
	@Override
	public String iSHaveMarketing(Long prodId, Long skuId) {
		
		MarketingProdDto  marketingProdDto = marketingProdsDao.iSHaveMarketing(prodId,skuId);
		if (AppUtils.isNotBlank(marketingProdDto)) {
			
			Integer type = marketingProdDto.getType();
			String marketName = marketingProdDto.getMarketName();
			
			if (type == MarketingTypeEnum.MAN_JIAN.value()) {
				return "该商品正在参加满减促销活动  ["+marketName+"] 请去发布或查看";
			}
			if (type == MarketingTypeEnum.MAN_ZE.value()) {
				return "该商品正在参加满折促销活动 ["+marketName+"] 请去发布或查看";
			}
			if (type == MarketingTypeEnum.XIANSHI_ZE.value()) {
				return "该商品正在参加限时促销活动 ["+marketName+"] 请去发布或查看";
			}
		}
		return Constants.SUCCESS;
	}

	@Override
	public int deleteByMarketId(Long marketId) {
		
		return marketingProdsDao.update("delete from ls_marketing_prods where market_id=? ",marketId);
	}

	@Override
	public List<MarketingProdDto> getMarketingProdsDtoByMarketId(Long id) {
		List<MarketingProdDto> marketingProdDtoList=new ArrayList<MarketingProdDto>();
		List<MarketingProds> marketingProdsList=this.getMarketingProdsByMarketId(id);
		for (MarketingProds marketingProds : marketingProdsList) {
			Sku sku = skuService.getSku(marketingProds.getSkuId());
			MarketingProdDto marketingProdDto = new MarketingProdDto();
			marketingProdDto.setSkuId(sku.getSkuId());
			marketingProdDto.setProdName(sku.getName());
			marketingProdDto.setProdId(sku.getSkuId());
			marketingProdDto.setCnProperties(sku.getCnProperties());
			marketingProdDto.setCash(sku.getPrice());
			marketingProdDto.setPic(sku.getPic());
			marketingProdDto.setStock(Integer.parseInt(sku.getStocks().toString()));
			marketingProdDtoList.add(marketingProdDto);
		}
		return marketingProdDtoList;
	}
}
