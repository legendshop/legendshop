/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProductSnapshot;

/**
 * 商品快照.
 */
public interface ProductSnapshotDao extends Dao<ProductSnapshot, Long> {
     
	public abstract ProductSnapshot getProductSnapshot(Long id);
	
    public abstract int deleteProductSnapshot(ProductSnapshot productSnapshot);
	
	public abstract Long saveProductSnapshot(ProductSnapshot productSnapshot);
	
	public abstract int updateProductSnapshot(ProductSnapshot productSnapshot);
	
	public abstract ProductSnapshot getProductSnapshot(Long prodId,Long skuId,Integer version);
	
	Long createProductSnapshotId();

	public abstract void saveProductSnapshotWithId(ProductSnapshot productSnapshot);
	
 }
