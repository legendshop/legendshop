/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.business.service.impl;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.ThemeModuleDao;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ThemeModuleProdService;
import com.legendshop.spi.service.ThemeModuleService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 专题模块服务.
 */
@Service("themeModuleService")
public class ThemeModuleServiceImpl  implements ThemeModuleService{

	@Autowired
    private ThemeModuleDao themeModuleDao;
    
	@Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private ProductService productService;
    
    @Autowired
    private ThemeModuleProdService themeModuleProdService;

	@Override
	public List<ThemeModule> getThemeModuleByTheme(Long themeId) {
        return themeModuleDao.getThemeModuleByTheme(themeId);
    }

    @Override
	public ThemeModule getThemeModule(Long id) {
        return themeModuleDao.getThemeModule(id);
    }

    @Override
	public void deleteThemeModule(ThemeModule themeModule) {
        themeModuleDao.deleteThemeModule(themeModule);
    }

    @Override
	public Long saveThemeModule(ThemeModule themeModule) {
        if (!AppUtils.isBlank(themeModule.getThemeModuleId())) {
        	ThemeModule originModule = themeModuleDao.getThemeModule(themeModule.getThemeModuleId());
        	themeModule.setUpdateTime(new Date());
        	themeModule.setCreateTime(originModule.getCreateTime());
            updateThemeModule(themeModule);
            return themeModule.getThemeModuleId();
        }
        themeModule.setCreateTime(new Date());
        return themeModuleDao.saveThemeModule(themeModule);
    }

    @Override
	public void updateThemeModule(ThemeModule themeModule) {
        themeModuleDao.updateThemeModule(themeModule);
    }

    @Override
    public Map<ThemeModule, List<ThemeModuleProd>>  getThemeModuleAndProdByThemeId(Long themeId){
    	List<ThemeModule> themeModules =themeModuleDao.getThemeModuleByTheme(themeId);
    	Map<ThemeModule, List<ThemeModuleProd>> map =null;
    	if(AppUtils.isNotBlank(themeModules)){
    		map=new LinkedHashMap<ThemeModule, List<ThemeModuleProd>>();;
    		for(ThemeModule themeModule :themeModules){
    			List<ThemeModuleProd> themeModuleProds =themeModuleDao.findModuleProdsByModuleId(themeModule.getThemeModuleId());
    			map.put(themeModule, themeModuleProds);
    		}
    	}
    	
    	return map;
    }

	@Override
	public void deleteThemeModule(List<ThemeModuleProd> themeModuleProdList, ThemeModule themeModule) {
		for (ThemeModuleProd themeModuleProd : themeModuleProdList) {
			ProductDetail prodInfo = productService.getProdDetail(themeModuleProd.getProdId());
			// 删除板块商品图片 如果是商品表的原有图片则不删
			if (AppUtils.isNotBlank(themeModuleProd.getImg()) && !themeModuleProd.getImg().equals(prodInfo.getPic())) {
				attachmentManager.deleteTruely(themeModuleProd.getImg());
				attachmentManager.delAttachmentByFilePath(themeModuleProd.getImg());
			}
			// 删除角标图片
			if (AppUtils.isNotBlank(themeModuleProd.getAngleIcon())) {
				attachmentManager.deleteTruely(themeModuleProd.getAngleIcon());
				attachmentManager.delAttachmentByFilePath(themeModuleProd.getAngleIcon());
			}
			themeModuleProdService.deleteThemeModuleProd(themeModuleProd);
		}
		// 删除板块 版头背景图 pc版
		if (AppUtils.isNotBlank(themeModule.getTitleBgPcimg())) {
			attachmentManager.deleteTruely(themeModule.getTitleBgPcimg());
			attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgPcimg());
		}
		// 删除板块 版头背景图 手机版
		if (AppUtils.isNotBlank(themeModule.getTitleBgMobileimg())) {
			attachmentManager.deleteTruely(themeModule.getTitleBgMobileimg());
			attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgMobileimg());
		}
		themeModuleDao.deleteThemeModule(themeModule);
	}

	@Override
	public void updateThemeModule(String type, ThemeModule themeModule) {
		if (type.equals("titleBgPcimg")) {
			if (AppUtils.isNotBlank(themeModule.getTitleBgPcimg())) {
				attachmentManager.deleteTruely(themeModule.getTitleBgPcimg());
				attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgPcimg());
				themeModule.setTitleBgPcimg(null);
			}
		} else if (type.equals("titleBgMobileimg")) {
			if (AppUtils.isNotBlank(themeModule.getTitleBgMobileimg())) {
				attachmentManager.deleteTruely(themeModule.getTitleBgMobileimg());
				attachmentManager.delAttachmentByFilePath(themeModule.getTitleBgMobileimg());
				themeModule.setTitleBgMobileimg(null);
			}
		}
		themeModuleDao.updateThemeModule(themeModule);
	}
}
