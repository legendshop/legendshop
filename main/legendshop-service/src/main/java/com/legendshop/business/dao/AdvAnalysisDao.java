package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdvAnalysis;

/**
 * 活动广告.
 */
public interface AdvAnalysisDao extends GenericDao<AdvAnalysis, Long>{

	/**
	 * 保存广告统计
	 * @param advAnalysis
	 */
	public abstract Long saveAdvAnalysis(AdvAnalysis advAnalysis);

	public abstract PageSupport<AdvAnalysis> query(String curPageNO, String url);

	public abstract PageSupport<AdvAnalysis> groupQuery(String userId,String curPageNO, String url);
}
