/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProdTypeParam;

/**
 * The Class ProdTypeParamDao.
 */

public interface ProdTypeParamDao extends Dao<ProdTypeParam, Long> {
	public ProdTypeParam getProdTypeParam(Long id);
     
    public abstract int deleteProdTypeParam(ProdTypeParam prodTypeParam);
	
	public abstract Long saveProdTypeParam(ProdTypeParam prodTypeParam);
	
	public abstract List<Long> saveProdTypeParam(List<ProdTypeParam> prodTypeParamList);

	ProdTypeParam getProdTypeParam(Long propId, Long prodTypeId);

	public void updateProdTypeParam(ProdTypeParam prodTypeParam);
 }
