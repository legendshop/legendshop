/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.MergeGroup;

/**
 * The Class MergeGroupDao.
 * 拼团活动Dao接口
 */
public interface MergeGroupDao extends Dao<MergeGroup, Long> {

	/**
	 * 已开始 未过期 已上线 的活动
	 * 
	 * @param curPageNO
	 * @return
	 */
	PageSupport<MergeGroup> getAvailable(String curPageNO);

	/**
	 * 根据Id获取拼团活动
	 */
	public abstract MergeGroup getMergeGroup(Long id);

	/**
	 *  删除拼团活动
	 */
	public abstract int deleteMergeGroup(MergeGroup mergeGroup);

	/**
	 *  保存拼团活动
	 */	
	public abstract Long saveMergeGroup(MergeGroup mergeGroup);

	/**
	 *  更新拼团活动
	 */		
	public abstract int updateMergeGroup(MergeGroup mergeGroup);

	public abstract PageSupport<MergeGroupDto> getMergeGroupList(String curPageNO, Integer pageSize, MergeGroupDto mergeGroup);

	public abstract void updateMergeGroupStatus(Long id, Integer status);

	Integer updateStatusByShopId(Long id, Integer status, Long shopId);

	PageSupport<MergeGroupOperationDto> getMergeGroupOperationList(String curPageNO, Integer pageSize,Long mergeId, Long shopId);

	PageSupport<MergeGroup> queryMergeGroupList(String curPageNO,Long shopId,MergeGroupDto mergeGroup);

	MergeGroup getMergeGroupByshopId(Long id, Long shopId);

	MergeGroup getMergeGroupByshopId(Long id);

	void deleteMergeGroupByShopId(Long shopId, Long id);

	MergeGroupJoinDto getMergeGroupOperationDtoByAddNumber(String addNumber);

	//拼团成功更改成功订单数+1
	void updateMergeGroupSuccessCount(Long mergeId);

	boolean checkExitproduct(Long prodId);

	List<MergeGroup> getMergeGroupAll();

	List<MergeGroup> queryMergeGroupListByStatus();

	void updateList(List<MergeGroup> cancalGroups);

	boolean isGroupExemption(String addBumber, String userId);

	/***
	 * 获取该活动该用户开启的拼团数，只获取为团长的数量、
	 * @param activeId
	 * @param userId
	 * @return
	 */
	Long joinMergeGroup(Long activeId, String userId);

	/**
	 * 更新拼团活动删除状态为商家删除
	 * @param shopId 店铺ID
	 * @param id 活动ID
	 * @param deleteStatus 删除状态
	 */
	void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus);

    MergeGroup getMergeGroupPrice(Long prodId, Long skuId);
    
    

	/**
	 * 运营数据 -- 统计活动交易总金额跟参与人数
	 * @param mergeId 活动ID 
	 * @param shopId  店铺ID
	 * @return
	 */
	OperateStatisticsDTO getTotalAmountAndParticipants(Long mergeId, Long shopId);

	/**
	 * 统计拼团活动成功交易金额以及成功拼团数
	 * @param mergeId 活动ID 
	 * @param shopId  店铺ID
	 * @return
	 */
	OperateStatisticsDTO getSucceedAmountAndCount(Long mergeId, Long shopId);

	/**
	 * 待成团的数量
	 * @param mergeId 活动ID 
	 * @param shopId  店铺ID
	 * @return
	 */
	Integer getWaitFightCount(Long mergeId, Long shopId);
}
