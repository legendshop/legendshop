package com.legendshop.business.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.legendshop.model.ErrorForm;
import com.legendshop.spi.service.ErrorHandleSerivce;

@Service
public class ErrorHandleSerivceImpl implements ErrorHandleSerivce {

	private Map<String, ErrorForm> mapping;
	@Override
	public ErrorForm getErrorForm(String errorCode) {
		return mapping.get(errorCode);
	}
	public void setMapping(Map<String, ErrorForm> mapping) {
		this.mapping = mapping;
	}

}
