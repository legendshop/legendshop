/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.legendshop.model.constant.Constants;

/**
 * 管理员登录
 */
public class AdminAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	/** The log. */
	Logger log = LoggerFactory.getLogger(AdminAuthenticationFilter.class);

	/** The post only. */
	private boolean postOnly = true;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * UsernamePasswordAuthenticationFilter
	 * #attemptAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

        //兼容3.2版本的spring security
//        String userName = request.getParameter(Constants.SPRING_SECURITY_FORM_USERNAME_KEY);
//        String password = request.getParameter(Constants.SPRING_SECURITY_FORM_PASSWORD_KEY);
        String userName = obtainUsername(request);
        String password = obtainPassword(request);

		return onAuthentication(request, response, userName, password);
	}

	/**
	 * 用户登录动作
	 * 
	 * @param request
	 * @param response
	 * @param username
	 * @param password
	 * @return
	 */
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String password) {
		if (userName == null) {
			userName = "";
		}

		if (password == null) {
			password = "";
		}
		AdminAuthenticationToken authRequest = new AdminAuthenticationToken(userName, password);

		// Allow subclasses to set the "details" property
		this.setDetails(request, authRequest);

		Authentication auth = this.getAuthenticationManager().authenticate(authRequest);
		return auth;
	}
	
    /**
     * Provided so that subclasses may configure what is put into the authentication request's details
     * property.
     *
     * @param request that an authentication request is being created for
     * @param authRequest the authentication request object that should have its details set
     */
    protected void setDetails(HttpServletRequest request, AdminAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

	/**
	 * Checks if is post only.
	 * 
	 * @return true, if is post only
	 */
	public boolean isPostOnly() {
		return postOnly;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * UsernamePasswordAuthenticationFilter#setPostOnly(boolean)
	 */
	@Override
	public void setPostOnly(boolean postOnly) {
		this.postOnly = postOnly;
	}

}