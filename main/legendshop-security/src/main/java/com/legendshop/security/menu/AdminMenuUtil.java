/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.security.menu;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.entity.Menu;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 后台菜单帮助类
 */
@Component
public class AdminMenuUtil {
	
	@Autowired
	private MenuManager menuManager;


	/**
	 * 计算当前菜单的情况
	 * 
	 * @param request
	 * order 第几个菜单
	 */
	public  Menu parseMenu(HttpServletRequest request, Integer order) {

		SecurityUserDetail user = UserManager.getUser(request);

		HttpSession session = request.getSession();
		Menu currentMenu = (Menu) session.getAttribute("currentMenu");
		if (currentMenu != null && currentMenu.getMenuId().intValue() == order) {// 菜单没有变化
			return currentMenu;
		}

		List<Menu> menuList = menuManager.getTopMenus(user, session);
		currentMenu = menuList.isEmpty() ? null : menuList.get(0);
		if (order != 0) {
			for (Menu menu : menuList) {
				if (menu.getMenuId().intValue() == order) {
					currentMenu = menu;
					break;
				}
			}
		}
		session.setAttribute("currentMenu", currentMenu);
		session.setAttribute("currentMenuId", order);
		return currentMenu;
	}

}
