/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.employee;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.framework.model.GrantedFunctionImpl;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.RoleEnum;
import com.legendshop.model.dto.shop.ShopAccountUserDto;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.model.security.ShopUser;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.security.GrantedAuthorityImpl;
import com.legendshop.security.model.SecurityUser;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.security.ShopAuthService;
import com.legendshop.util.AppUtils;

/**
 * 管理员登录.
 */
public class EmployeeDaoAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	private final Logger log = LoggerFactory.getLogger(EmployeeDaoAuthenticationProvider.class);

	private ShopAuthService shopAuthService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * 检查用户名密码
	 */
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication)
			throws AuthenticationException {
		if (authentication.getCredentials() == null) {
			logger.debug("Authentication failed: no credentials provided");

			throw new BadCredentialsException(
					messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		}
	}

	@Override
	protected UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
		UserDetails loadedUser = null;
		try {
			EmployeeAuthenticationToken token = (EmployeeAuthenticationToken) authentication;
			ShopAccountUserDto shopUser = shopAuthService.loadShopAccountByUsername(username, authentication.getCredentials().toString(), token.getShopName());
			if (shopUser != null) {
				boolean isValid = additionalAuthenticationChecks(shopUser, authentication.getCredentials().toString());
				if(!isValid){
					log.debug("User {} password is not valid", username);
				   return null;
				}
				
				loadedUser = createUser(shopUser, LoginUserTypeEnum.EMPLOYEE, LoginTypeEnum.USERNAME_PASSWORD, null, null, null);
			}

		} catch (UsernameNotFoundException notFound) {
			throw notFound;
		} catch (Exception repositoryProblem) {
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}

		if (loadedUser == null) {
			throw new AuthenticationServiceException(
					"UserDetailsService returned null, which is an interface contract violation");
		}
		return loadedUser;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (EmployeeAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication,
			UserDetails user) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		EmployeeAuthenticationToken result = new EmployeeAuthenticationToken(principal, authentication.getCredentials(),
				authoritiesMapper.mapAuthorities(user.getAuthorities()));
		result.setShopName(((EmployeeAuthenticationToken) authentication).getShopName());
		result.setDetails(authentication.getDetails());

		return result;
	}

	public void setShopAuthService(ShopAuthService shopAuthService) {
		this.shopAuthService = shopAuthService;
	}

	/**
	 * 根据商家和用户转化为用户对象
	 * 
	 * @param user
	 * @param appNo
	 * @param loginUserType
	 * @param loginType
	 * @param opendId
	 * @param accessToken
	 * @param type
	 * @return
	 */
	private SecurityUser createUser(ShopAccountUserDto employee, LoginUserTypeEnum loginUserType,
			LoginTypeEnum loginType, String opendId, String accessToken, String type) {

		/** 商家菜单 **/
		List<ShopMenuGroup> shopMenuGroupList = new ArrayList<ShopMenuGroup>();
		/*		*//** 无权访问的商家菜单 **//*
									 * List<String> disableLinks = new ArrayList<String>();
									 */

		// 用户的权限点 采用annotation，准备去掉 TODO
		List<String> permList = employee.getPermList();

		if (AppUtils.isBlank(permList)) {// 什么权限也没有
			// disableLinks.addAll(shopMenuGroupService.getAllLinks());
		} else {
			for (ShopMenuGroup group : shopMenuGroupService.getShopMenuGroup()) {
				List<ShopMenu> shopMenuList = new ArrayList<ShopMenu>();
				for (ShopMenu menu : group.getShopMenuList()) {
					if (permList.contains(menu.getLabel())) {
						shopMenuList.add(menu);
					} else {
						for (String url : menu.getUrlList()) {
							// disableLinks.add(url);
						}
					}
				}
				if (AppUtils.isNotBlank(shopMenuList)) {
					ShopMenuGroup newGroup = group.copy(shopMenuList);
					shopMenuGroupList.add(newGroup);
				}
			}
		}

		Collection<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new GrantedAuthorityImpl(RoleEnum.ROLE_SUPPLIER.name())); // 供应商

		Collection<GrantedFunction> functions = new ArrayList<GrantedFunction>();
		functions.add(new GrantedFunctionImpl(FunctionEnum.FUNCTION_USER.value()));

		ShopUser shopUser = new ShopUser();
		// shopUser.setDisableLinks(disableLinks);
		shopUser.setShopId(employee.getShopId());
		shopUser.setShopMenuGroupList(shopMenuGroupList);
		shopUser.setShopName(employee.getShopName());
		shopUser.setStatus(1);
		shopUser.setUserId(employee.getId().toString());
		shopUser.setUserName(employee.getSellerName());

		SecurityUser minuser = new SecurityUserDetail(employee.getId().toString(), employee.getShopAccountName(),
				employee.getShopAccountPassword(), null, null, employee.isEnabled(), true, true, true, roles, functions,
				opendId, accessToken, type, shopUser, null, null, loginType.value(), loginUserType.value());
		return minuser;
	}

	/**
	 * 检查用户名密码
	 * 
	 * @param userDetails
	 * @param authentication
	 * @return
	 * @throws AuthenticationException
	 */
	private boolean additionalAuthenticationChecks(ShopAccountUserDto shopUser, String password) throws AuthenticationException {
		return passwordEncoder.matches(password, shopUser.getShopAccountPassword());
	}

}
