/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 检查图片验证码的逻辑.
 */
public interface ValidateCodeBusinessHandler {
	
	/**
	 * Do process.
	 *
	 * @param request the request
	 * @param session the session
	 * @return true, if successful
	 */
	public boolean doProcess(HttpServletRequest request, HttpSession session);
}
