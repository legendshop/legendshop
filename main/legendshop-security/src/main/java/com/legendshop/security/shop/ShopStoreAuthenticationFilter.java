package com.legendshop.security.shop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.legendshop.redis.UserOnlineStatusHelper;
import com.legendshop.security.model.SecurityUserDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.legendshop.model.constant.Constants;
import com.legendshop.security.handler.ValidateCodeBusinessHandler;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.CookieUtil;


public class ShopStoreAuthenticationFilter  extends UsernamePasswordAuthenticationFilter {

	/** The log. */
	Logger log = LoggerFactory.getLogger(ShopStoreAuthenticationFilter.class);

	/** The post only. */
	private boolean postOnly = true;
	
	private ValidateCodeBusinessHandler validateCodeBusinessHandler;

	private UserOnlineStatusHelper userOnlineStatusHelper;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * UsernamePasswordAuthenticationFilter
	 * #attemptAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		if (postOnly && !request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}
		String loginCode=request.getParameter("loginCode");
		HttpSession session = request.getSession(true);
		// check validate code
		validateCodeBusinessHandler.doProcess(request, session);

        String userName = obtainUsername(request);
        String password = obtainPassword(request);
        
		if(AppUtils.isBlank(userName) || AppUtils.isBlank(password)){
			log.warn("User name or password can not empty.");
			return null;
		}
		
		if (session != null || getAllowSessionCreation()) {
			CookieUtil.addCookie(response, Constants.LAST_USERNAME_KEY, userName);
		}
	
		return onAuthentication(request, response, userName, password,loginCode);
	}

	/**
	 * 用户登录动作
	 * 
	 * @param request
	 * @param response
	 * @param username
	 * @param password
	 * @return
	 */
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String password,String loginCode) {
		if (userName == null) {
			userName = "";
		}

		if (password == null) {
			password = "";
		}
		ShopStoreAuthenticationToken authRequest = new ShopStoreAuthenticationToken(userName, password,loginCode);

		// Allow subclasses to set the "details" property
		setDetails(request, authRequest);

		Authentication auth = this.getAuthenticationManager().authenticate(authRequest);
		if(auth.isAuthenticated()){
			//记录Redis的登录信息
			SecurityUserDetail userDetail = (SecurityUserDetail) auth.getPrincipal();
			String userId = userDetail.getUserId();
			//绑定用户登录状态
			userOnlineStatusHelper.online(userDetail.getLoginUserType(), userId);

		}

		return auth;
	}
	
	/**
     * Provided so that subclasses may configure what is put into the authentication request's details
     * property.
     *
     * @param request that an authentication request is being created for
     * @param authRequest the authentication request object that should have its details set
     */
    protected void setDetails(HttpServletRequest request, ShopStoreAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }
	
   

	/**
	 * Checks if is post only.
	 * 
	 * @return true, if is post only
	 */
	public boolean isPostOnly() {
		return postOnly;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * UsernamePasswordAuthenticationFilter#setPostOnly(boolean)
	 */
	@Override
	public void setPostOnly(boolean postOnly) {
		this.postOnly = postOnly;
	}
	
	public void setValidateCodeBusinessHandler(ValidateCodeBusinessHandler validateCodeBusinessHandler) {
		this.validateCodeBusinessHandler = validateCodeBusinessHandler;
	}

	public void setUserOnlineStatusHelper(UserOnlineStatusHelper userOnlineStatusHelper) {
		this.userOnlineStatusHelper = userOnlineStatusHelper;
	}
}
