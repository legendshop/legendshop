/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.session.Session;

import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.RoleEnum;
import com.legendshop.model.security.ShopUser;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 *
 * 用户管理,获取用户的在线状态
 */
public class UserManager {

    /**
     *  得到已经登录用户详细信息, 为防止多次访问session或者远程的Redis Session, 先从context上下文中获取, 如果不行则从session中获取
     */
    public static SecurityUserDetail getUser(HttpServletRequest request) {
        Authentication auth = getAuthentication(request);
        if (auth != null) {
            if (auth.getPrincipal() instanceof SecurityUserDetail) {
                return (SecurityUserDetail) auth.getPrincipal();
            }
        }
        return null;
    }

    /**
     *  得到已经登录用户详细信息, 先从context上下文中获取, 如果不行则从Redis session中获取
     */
    public static SecurityUserDetail getUser(Session session) {
        Authentication auth = getAuthentication(session);
        if (auth != null) {
            if (auth.getPrincipal() instanceof SecurityUserDetail) {
                return (SecurityUserDetail) auth.getPrincipal();
            }
        }
        return null;
    }

    /**
     * 检查普通用户是否登录
     * @param session
     * @return
     */
    public static boolean isUserLogined(SecurityUserDetail user) {
        if (user != null) {
            return LoginUserTypeEnum.USER.value().equals(user.getLoginUserType());
        }
        return false;
    }

    //是否是商家, 只是检查权限，包括子账号
    public static boolean isShopUser(SecurityUserDetail user) {
        return hasRole(user, RoleEnum.ROLE_SUPPLIER.name());
    }

    /**
     * 检查管理员是否登录
     * @param session
     * @return
     */
    public static boolean isAdminLogined(SecurityUserDetail user) {
        if (user != null) {
            return LoginUserTypeEnum.ADMIN.value().equals(user.getLoginUserType());
        }
        return false;
    }

    /**
     * 得到用户的昵称
     * 如果用户没有设置昵称，则取username
     */
    public static String getNickName(SecurityUserDetail user) {
        String nickName = null;
        if (user != null) {
            nickName = user.getNickName();
            if(nickName == null){
                nickName =user.getUsername();
            }
        }
        return nickName;
    }


    /**
     * 得到用的的Authentication，并且从Authentication中获得 Authorities，进而得到 授予用户的 Function.
     *
     * @param session
     *            the session
     * @return Collection
     */
    public static Collection<GrantedFunction> getFunctions(SecurityUserDetail user) {
        if (user != null) {
            return user.getFunctions();
        }
        return null;
    }

    /**
     * 用户是否拥有该角色
     *
     * @param request
     * @param role
     * @return
     */
    public static boolean hasRole(SecurityUserDetail user, String roleName) {
        if(user == null){
            return false;
        }
        Collection<GrantedAuthority> rolesCollection = user.getAuthorities();
        if(rolesCollection == null) {
            return false;
        }
        boolean hasRole = false;
        for (Iterator<GrantedAuthority> iterator = rolesCollection.iterator(); iterator.hasNext();) {
            GrantedAuthority grantedAuthority = iterator.next();
            // 角色名称
            String autorityRole = grantedAuthority.getAuthority();
            if (roleName.equals(autorityRole)) {
                hasRole = true;
                break;
            }
        }
        return hasRole;
    }


    /**
     *   所有的都满足才返回true
     * grantedFunctions isBlank
     *
     */
    public static boolean hasFunction(SecurityUserDetail user, String[] functions) {
        if (AppUtils.isBlank(functions) || user == null || AppUtils.isBlank(user.getFunctions())) {
            return false;
        }

        Collection<GrantedFunction> grantedFunctions = user.getFunctions();
        // 当前用户的权限
        for (String function : functions) {
            if (!hasFunction(grantedFunctions, function)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 用户是否拥有该权限.
     *
     */
    public static boolean hasFunction(SecurityUserDetail user, String function) {
        if (AppUtils.isBlank(function) || user == null || AppUtils.isBlank(user.getFunctions())) {
            return false;
        }
        Collection<GrantedFunction> grantedFunctions = user.getFunctions();
        return hasFunction(grantedFunctions, function);
    }

    /**
     * 得到商城用户的名称
     *
     * @param session
     * @return
     */
    public static String getShopUserName(SecurityUserDetail userDetail) {
        if (userDetail == null) {
            return null;
        }
        ShopUser shopUser = userDetail.getShopUser();
        if (shopUser == null) {
            return null;
        }
        return shopUser.getUserName();
    }


    /**
     * 得到商城所属用户的ID
     *
     * @param session
     * @return
     */
    public static String getShopUserId(SecurityUserDetail userDetail) {
        if (userDetail == null) {
            return null;
        }
        ShopUser shopUser = userDetail.getShopUser();
        if (shopUser == null) {
            return null;
        }
        return shopUser.getUserId();
    }

    /**
     * 得到商城所属的用户
     *
     * @param session
     * @return
     */
    public static ShopUser getShopUser(SecurityUserDetail userDetail) {
        if (userDetail == null) {
            return null;
        }
        return userDetail.getShopUser();
    }


    /**
     * 1. 先从SecurityContextHolder取值 2. 对应不经过Spring Security的情况从session中取值
     *
     * @param session
     * @return
     */
    private static Authentication getAuthentication(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null && request != null) {
            HttpSession session = request.getSession();
            if(session != null) {
                SecurityContext context = (SecurityContext) session.getAttribute(AttributeKeys.SPRING_SECURITY_CONTEXT);
                if (context != null) {
                    SecurityContextHolder.getContext().setAuthentication(auth);//设置到context里去,防止下次再从session中取值
                    auth = context.getAuthentication();
                }
            }

        }
        return auth;
    }


    /**
     * 1. 先从SecurityContextHolder取值 2. 对应不经过Spring Security的情况从session中取值
     *
     * @param session
     * @return
     */
    private static Authentication getAuthentication(Session session) {
        Authentication auth = null;
        if (session != null) {
            SecurityContext context = (SecurityContext) session.getAttribute(AttributeKeys.SPRING_SECURITY_CONTEXT);
            if (context != null) {
                auth = context.getAuthentication();
            }
        }
        return auth;
    }


    /**
     * 用户是否拥有该权限.
     *
     * @param functions
     *            用户拥有的权限, 不能为空
     * @param function
     *            指明的权限, 不能为空
     * @return true, if successful
     */
    private static boolean hasFunction(Collection<GrantedFunction> functions, String function) {
        for (GrantedFunction gf : functions) {
            if (gf.getFunction().equals(function)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the session id.
     *
     * @return the session id
     */
    public static String getSessionID() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx != null) {
            if (ctx instanceof SecurityContext) {
                SecurityContext sc = ctx;
                Authentication auth = sc.getAuthentication();
                if (auth != null) {
                    Object details = auth.getDetails();
                    if (details instanceof WebAuthenticationDetails) {
                        return ((WebAuthenticationDetails) details).getSessionId();
                    } else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

}
