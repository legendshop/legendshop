package com.legendshop.security.authorize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.legendshop.model.constant.LoginUserType;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.model.security.ShopUser;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 商家子账号拦截器, 用于过滤需要控制权限的方法
 *
 */
public class ShopUserAuthorityAnnotationInterceptor extends HandlerInterceptorAdapter {

	final Logger logger = LoggerFactory.getLogger(getClass());

	private String NOT_HAVE_AUTHORITY = "NOT_HAVE_AUTHORITY";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if(handler instanceof HandlerMethod) {
			HandlerMethod handler2 = (HandlerMethod) handler;
			Authorize fireAuthority = handler2.getMethodAnnotation(Authorize.class);
			if (null == fireAuthority) {
				// 没有声明权限,放行
				return true;
			}

			logger.debug("fireAuthority", fireAuthority.toString());
			
			//如果没有登录就限制登录
			SecurityUserDetail userDetail = UserManager.getUser(request);
			if(userDetail == null){
				output(request, response, fireAuthority);
				return false;
			}
			

			
			//子账号登录才会限制
			if(LoginUserType.EMPLOYEE_TYPE.equals(userDetail.getLoginUserType())){
				ShopUser shopUser = userDetail.getShopUser();
				if(shopUser == null){//如果不是商家就限制登录
					output(request, response, fireAuthority);
					return false;
				}
				
				List<ShopMenuGroup> shopMenuGroupList = shopUser.getShopMenuGroupList();

				if (shopMenuGroupList == null) {// 没有权限则返回
					return false;
				}

				boolean aflag = false;
				// 检查是否有同名的权限
				for (AuthorityType at : fireAuthority.authorityTypes()) {
					for (ShopMenuGroup group : shopMenuGroupList) {
						if(AppUtils.isNotBlank(group.getShopMenuList())){
							for (ShopMenu shopMenu : group.getShopMenuList()) {
								if (shopMenu.getLabel().equals(at.getValue())) {
									aflag = true;
									break;
								}	
							}
						}
					}
				}

				if (aflag == false) {
					output(request, response, fireAuthority);
					return false;
				}
			}
			
			return true;
		}
		
		return true;
		
	}

	/**
	 * 导航到登录页面或者返回错误的JSON
	 * @param request
	 * @param response
	 * @param fireAuthority
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private void output(HttpServletRequest request, HttpServletResponse response, Authorize fireAuthority)
			throws IOException, UnsupportedEncodingException {
		if (fireAuthority.resultType() == ResultTypeEnum.page) {
			// 传统的登录页面
			StringBuilder sb = new StringBuilder();
			sb.append(request.getContextPath());
			sb.append("/login");
			response.sendRedirect(sb.toString());
		} else if (fireAuthority.resultType() == ResultTypeEnum.adminPage) {
			// 传统的登录页面
			StringBuilder sb = new StringBuilder();
			sb.append(request.getContextPath());
			sb.append("/adminlogin");
			response.sendRedirect(sb.toString());
		} else if (fireAuthority.resultType() == ResultTypeEnum.json) {
			// ajax类型的登录提示
			response.setCharacterEncoding("utf-8");
			response.setContentType("text/html;charset=UTF-8");
			OutputStream out = response.getOutputStream();
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(out, "utf-8"));
			pw.println("{\"result\":false,\"code\":12,\"errorMessage\":\"" + NOT_HAVE_AUTHORITY + "\"}");
			pw.flush();
			pw.close();
		}
	}
}
