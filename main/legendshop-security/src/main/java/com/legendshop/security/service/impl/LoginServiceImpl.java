/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.security.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.NullRememberMeServices;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import com.legendshop.model.constant.Constants;
import com.legendshop.security.ValidateCodeUsernamePasswordAuthenticationFilter;
import com.legendshop.security.handler.AuthenticationFailureBusinessHandler;
import com.legendshop.security.handler.AuthenticationSuccessBusinessHandler;
import com.legendshop.security.handler.ValidateCodeBusinessHandler;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.security.service.LoginService;
import com.legendshop.security.thirdparty.ThirdPartyAuthenticationToken;
import com.legendshop.security.username.UsernameAuthenticationToken;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.CookieUtil;

/**
 * 用户登录服务,包括第三方登录.
 */
public class LoginServiceImpl implements LoginService {

	/** The log. */
	Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	protected AuthenticationSuccessBusinessHandler authenticationSuccessBusinessHandler;

	protected AuthenticationFailureBusinessHandler authenticationFailureBusinessHandler;

	/** 忘记密码服务. */
	protected RememberMeServices rememberMeServices = new NullRememberMeServices();

	/** session存放的地方. */
	protected SessionAuthenticationStrategy sessionStrategy;

	protected ValidateCodeBusinessHandler validateCodeBusinessHandler;

	protected ValidateCodeUsernamePasswordAuthenticationFilter validateCodeAuthenticationFilter;

	protected UserDetailService userDetailService;

	private AuthenticationManager authenticationManager;

	// 当前登录用户,用于第三方登录
	public static String CURRENT_USER_ID = "_CURRENTUSERID_";

	/**
	 * 用户一注册立即登录.
	 *
	 * 用户名密码登录
	 *
	 */
	@Override
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String password) {
		if (!request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}
		HttpSession session = request.getSession();
		if (session == null) {
			return null;
		}
		// 检查图片验证码
		// validateCodeBusinessHandler.doProcess(request, session);

		CookieUtil.addCookie(response, Constants.LAST_USERNAME_KEY, userName);

		logger.debug("userName {}  try to login", userName);
		Authentication authResult = null;
		try {
			authResult = validateCodeAuthenticationFilter.onAuthentication(request, response, userName, password);
			if (authResult != null && authResult.isAuthenticated()) {
				sessionStrategy.onAuthentication(authResult, request, response);
				successfulAuthentication(request, response, authResult);
			}
		} catch (InternalAuthenticationServiceException failed) {
			logger.error("An internal error occurred while trying to authenticate the user.", failed);
			unsuccessfulAuthentication(request, response, failed);
		} catch (AuthenticationException failed) {
			// Authentication failed
			unsuccessfulAuthentication(request, response, failed);
		}

		return authResult;
	}


	/**
	 * 用户第三方登陆.
	 *
	 */
	@Override
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String openId,String accessToken, String type, String source) {

		Authentication authResult = null;
		try {

			ThirdPartyAuthenticationToken authRequest = new ThirdPartyAuthenticationToken(userName, openId, openId, accessToken, type, source);

			this.setDetails(request, authRequest);

			authResult = this.getAuthenticationManager().authenticate(authRequest);

			if(authResult!= null && authResult.isAuthenticated()){
				logger.info("Third part login success");
				 sessionStrategy.onAuthentication(authResult, request, response);
				 successfulAuthentication(request, response, authResult);
				 SecurityUserDetail userDetail = (SecurityUserDetail)authResult.getPrincipal();
				 CookieUtil.addCookie(response, Constants.LAST_USERNAME_KEY, userDetail.getNickName());
			}
		}catch(InternalAuthenticationServiceException failed) {
			logger.error("An internal error occurred while trying to authenticate the user.", failed);
            unsuccessfulAuthentication(request, response, failed);
        }
        catch (AuthenticationException failed) {
            // Authentication failed
			logger.error(failed.getMessage(), failed);
        	logger.error("Third part login fail with userName： {} ", userName);
            unsuccessfulAuthentication(request, response, failed);
        }

		return authResult;
	}


	/**
	 * 其他方式的用户登录动作. 只是需要用户名就可以登录, 例如支持跟其他系统集成,密码校验由其他系统提供,或者用短信验证登录,无需再校验密码的情况
	 * isUserName: 是否是用户名,如果不是用户名,要先转化为用户名
	 *
	 */

	@Override
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, boolean isUserName) {
		if (AppUtils.isBlank(userName)) {
			return null;
		}

		if (!isUserName) {
			userName = userDetailService.convertUserLoginName(userName);
		}

		UsernameAuthenticationToken authRequest = new UsernameAuthenticationToken(userName);

		// Allow subclasses to set the "details" property
		setDetails(request, authRequest);

		Authentication auth = this.getAuthenticationManager().authenticate(authRequest);
		if (auth.isAuthenticated()) {
			sessionStrategy.onAuthentication(auth, request, response);
			successfulAuthentication(request, response, auth);
		}
		return auth;
	}

	/**
	 * 忘记我服务
	 */
	public RememberMeServices getRememberMeServices() {
		return rememberMeServices;
	}

	/**
	 * 设置忘记我的功能
	 */
	public void setRememberMeServices(RememberMeServices rememberMeServices) {
		this.rememberMeServices = rememberMeServices;
	}

	/**
	 * Gets the session strategy.
	 *
	 * @return the session strategy
	 */
	public SessionAuthenticationStrategy getSessionStrategy() {
		return sessionStrategy;
	}

	/**
	 * Sets the session strategy.
	 *
	 * @param sessionStrategy the new session strategy
	 */
	public void setSessionStrategy(SessionAuthenticationStrategy sessionStrategy) {
		this.sessionStrategy = sessionStrategy;
	}

	public void setAuthenticationFailureBusinessHandler(AuthenticationFailureBusinessHandler authenticationFailureBusinessHandler) {
		this.authenticationFailureBusinessHandler = authenticationFailureBusinessHandler;
	}

	public void setValidateCodeBusinessHandler(ValidateCodeBusinessHandler validateCodeBusinessHandler) {
		this.validateCodeBusinessHandler = validateCodeBusinessHandler;
	}

	public void setAuthenticationSuccessBusinessHandler(AuthenticationSuccessBusinessHandler authenticationSuccessBusinessHandler) {
		this.authenticationSuccessBusinessHandler = authenticationSuccessBusinessHandler;
	}

	public void setUserDetailService(UserDetailService userDetailService) {
		this.userDetailService = userDetailService;
	}

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public void setValidateCodeAuthenticationFilter(ValidateCodeUsernamePasswordAuthenticationFilter validateCodeAuthenticationFilter) {
		this.validateCodeAuthenticationFilter = validateCodeAuthenticationFilter;
	}

	/**
	 * Provided so that subclasses may configure what is put into the authentication
	 * request's details property.
	 *
	 * @param request     that an authentication request is being created for
	 * @param authRequest the authentication request object that should have its
	 *                    details set
	 */
	private void setDetails(HttpServletRequest request, AbstractAuthenticationToken authRequest) {
		authRequest.setDetails(new WebAuthenticationDetails(request));
	}

	private void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {

		if (logger.isDebugEnabled()) {
			logger.debug("Authentication success. Updating SecurityContextHolder to contain: " + authResult);
		}

		SecurityContextHolder.getContext().setAuthentication(authResult);

		rememberMeServices.loginSuccess(request, response, authResult);
		authenticationSuccessBusinessHandler.onAuthenticationSuccess(request, response, authResult);

	}

	private void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) {
		SecurityContextHolder.clearContext();

		if (logger.isDebugEnabled()) {
			logger.debug("Authentication request failed: " + failed.toString());
			logger.debug("Updated SecurityContextHolder to contain null Authentication");
		}

		rememberMeServices.loginFail(request, response);
		authenticationFailureBusinessHandler.onAuthenticationFailure(request, response, failed);
	}


}
