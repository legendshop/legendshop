/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.event;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.legendshop.core.event.HttpWrapper;
import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.model.constant.Constants;
import com.legendshop.web.util.CookieUtil;

/**
 * 用户注销操作
 */
@Component("logoutProcessor")
public class LogoutProcessor extends BaseProcessor<HttpWrapper> {

	private boolean supportSSO = false;
	
	public boolean isSupport(HttpWrapper httpWrapper) {
		return true;
	}

	@Override
	public void process(HttpWrapper httpWrapper) {
		if (supportSSO) {
			// 删除Cookies
			CookieUtil.deleteCookie(httpWrapper.getRequest(), httpWrapper.getResponse(), Constants.CLUB_COOIKES_NAME);
		}
		HttpSession session = httpWrapper.getRequest().getSession(false);
		if(session !=null){
			session.removeAttribute(Constants.USER_NAME);
			session.removeAttribute(Constants.MENU_LIST);//后台菜单
			session.removeAttribute(Constants.SPRING_SECURITY_CONTEXT);//安全上下文
			
		}
	}

	public void setSupportSSO(boolean supportSSO) {
		this.supportSSO = supportSSO;
	}
}
