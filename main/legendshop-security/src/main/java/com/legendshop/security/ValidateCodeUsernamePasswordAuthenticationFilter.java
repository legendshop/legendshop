/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.legendshop.model.constant.UserSourceEnum;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.redis.UserOnlineStatusHelper;
import com.legendshop.security.mobileCode.MobileCodeAuthenticationToken;
import com.legendshop.web.helper.IPHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.legendshop.model.constant.Constants;
import com.legendshop.security.employee.EmployeeAuthenticationToken;
import com.legendshop.security.handler.ValidateCodeBusinessHandler;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.CookieUtil;

import static com.legendshop.util.AppUtils.isBlank;
import static com.legendshop.util.AppUtils.isNotBlank;


/**
 * 带验证码校验功能的用户名、密码认证过滤器
 * <p/>
 * 支持不输入验证码；支持验证码忽略大小写。
 * <p/>
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * <p/>
 * 官方网站：http://www.legendesign.net
 */
public class ValidateCodeUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

    Logger log = LoggerFactory.getLogger(ValidateCodeUsernamePasswordAuthenticationFilter.class);

    private boolean postOnly = true;

    private ValidateCodeBusinessHandler validateCodeBusinessHandler;

    protected UserDetailService userDetailService;
    //普通登录
    private static String loginTypeUser = "1";

    //手机登录
    private static String loginTypeMobile = "2";

    //登录上线标记
    private UserOnlineStatusHelper userOnlineStatusHelper;
    
    /**
     * 用户尝试登录动作
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        if (postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        HttpSession session = request.getSession(true);
        String loginType = request.getParameter("loginType");//登录类型.


        // 检查图片验证码
        validateCodeBusinessHandler.doProcess(request, session);
        if (loginTypeUser.equals(loginType)) { //普通登录
            String userName = obtainUsername(request);
            String password = obtainPassword(request);

            if (userName == null  ||password == null) {
                log.warn("User name or password can not empty.");
                return null;
            }

            if (session != null || getAllowSessionCreation()) {
                CookieUtil.addCookie(response, Constants.LAST_USERNAME_KEY, userName);
            }

            String shopName = request.getParameter("shopName");//商家名字.
            CookieUtil.addCookie(response, Constants.LAST_SHOPNAME_KEY, shopName);
            if (AppUtils.isBlank(shopName)) {
                return onAuthentication(request, response, userName, password);
            }else{//商家子账号登录
                return onEmployeeAuthentication(request, response, userName, password, shopName);
            }
        }else if (loginTypeMobile.equals(loginType)) {//手机登录

            String phone = request.getParameter("j_username_mobile");//手机号码.
            String SMSCode = request.getParameter("j_password_mobile");//短信验证码.

            if (isBlank(phone) || isBlank(SMSCode)) {
                log.warn("User phone or SMSCode can not empty.");
                return null;
            }
            User user = new User();
            // 根据手机号查找用户
            user = userDetailService.getUserByMobile(phone);
            if (user == null) {
                //如果用户为空则自动注册手机号
                UserMobileForm userForm = new UserMobileForm();
                userForm.setMobile(phone);
                userForm.setNickName(phone);
                    if(!AppUtils.isBlank( request.getSession().getAttribute(Constants.USER_OPENID))) {
                        userForm.setOpenId(request.getSession().getAttribute(Constants.USER_OPENID).toString());
                    }
                userForm.setSex("1");
                userForm.setPassword("");
                userForm.setActivated(true);
                userForm.setSource(UserSourceEnum.PHONE.value());
                user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userForm,"");
            }
            return onMobileAuthentication(request, response, phone, SMSCode);
        }
        return null;
    }
    /**
     * 用户手机验证码登录动作
     */
    private Authentication onMobileAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String password) {
        MobileCodeAuthenticationToken authRequest = new MobileCodeAuthenticationToken(userName, password);
        setMobileDetails(request, authRequest);
        Authentication auth = this.getAuthenticationManager().authenticate(authRequest);
        if (auth.isAuthenticated()) {
            String USER_OPENID = (String) request.getSession().getAttribute(Constants.USER_OPENID);
            String USER_UNIONID = (String) request.getSession().getAttribute(Constants.USER_UNIONID);

			SecurityUserDetail userDetail = (SecurityUserDetail) auth.getPrincipal();
			String userId = userDetail.getUserId();
            //当OPENID不为空时，判断当前用户的OPENID是否改变，有改变则需要修改为最新的OPENID,否则支付会出错
            if (isNotBlank(USER_OPENID)) {


                if (!USER_OPENID.equals(userDetailService.getUser(userId).getOpenId()))
                    userDetailService.bingUserOpenId(userId, USER_OPENID, USER_UNIONID);
            }

			//绑定用户登录状态
			userOnlineStatusHelper.online(userDetail.getLoginUserType(), userId);

            request.getSession().invalidate();//把上一个session给失效掉
        }
        return auth;
    }
    protected void setMobileDetails(HttpServletRequest request, MobileCodeAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }


	/**
     * 执行用户登录动作
     *
     */
    public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String password) {

        userName = convertUserLoginName(userName.trim());

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(userName, password);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);

        Authentication auth = this.getAuthenticationManager().authenticate(authRequest);

        String USER_OPENID = (String) request.getSession().getAttribute(Constants.USER_OPENID);
        if (auth.isAuthenticated()) {

			SecurityUserDetail userDetail = (SecurityUserDetail) auth.getPrincipal();
			String userId = userDetail.getUserId();

            //当OPENID不为空时，判断当前用户的OPENID是否改变，有改变则需要修改为最新的OPENID,否则支付会出错
            if (AppUtils.isNotBlank(USER_OPENID)) {                


                if (!USER_OPENID.equals(userDetailService.getUser(userId).getOpenId()))
                    userDetailService.bingUserOpenId(userId, USER_OPENID);
            }

            //绑定用户登录状态
			userOnlineStatusHelper.online(userDetail.getLoginUserType(), userId);
        }else {
        	try {
        		//登录失败的情况
				unsuccessfulAuthentication(request, response, new BadCredentialsException("login failed"));
			} catch (Exception e) {
				log.error("",e);
			}
        }
        return auth;
    }

    /**
     * Checks if is post only.
     *
     * @return true, if is post only
     */
    public boolean isPostOnly() {
        return postOnly;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.security.web.authentication.
     * UsernamePasswordAuthenticationFilter#setPostOnly(boolean)
     */
    @Override
    public void setPostOnly(boolean postOnly) {
        this.postOnly = postOnly;
    }

    public void setValidateCodeBusinessHandler(ValidateCodeBusinessHandler validateCodeBusinessHandler) {
        this.validateCodeBusinessHandler = validateCodeBusinessHandler;
    }

    /**
     * 支持手机登陆，邮件登陆
     *
     * @param userName
     * @return
     */
    protected String convertUserLoginName(String userName) {
    	 return userDetailService.convertUserLoginName(userName);
    }


    public void setUserDetailService(UserDetailService userDetailService) {
        this.userDetailService = userDetailService;
    }

    
    /**
     * 商家子账号登录
     * 
     */
    private Authentication onEmployeeAuthentication(HttpServletRequest request, HttpServletResponse response,
			String userName, String password, String shopName) {

        EmployeeAuthenticationToken authRequest = new EmployeeAuthenticationToken(userName, password,shopName);
        // Allow subclasses to set the "details" property
        setEmployeeDetails(request, authRequest);
        Authentication auth = this.getAuthenticationManager().authenticate(authRequest);

        if(auth.isAuthenticated()){
			//绑定用户登录状态
			SecurityUserDetail userDetail = (SecurityUserDetail)auth.getPrincipal();
			userOnlineStatusHelper.online(userDetail.getLoginUserType(), userDetail.getUserId());
		}


        return auth;
	}
    
    
    /**
     * Provided so that subclasses may configure what is put into the authentication request's details
     * property.
     *
     * @param request that an authentication request is being created for
     * @param authRequest the authentication request object that should have its details set
     */
    private void setEmployeeDetails(HttpServletRequest request, EmployeeAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

	public void setUserOnlineStatusHelper(UserOnlineStatusHelper userOnlineStatusHelper) {
		this.userOnlineStatusHelper = userOnlineStatusHelper;
	}
}