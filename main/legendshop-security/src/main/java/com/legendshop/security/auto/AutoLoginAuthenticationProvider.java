package com.legendshop.security.auto;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.spi.service.security.AuthService;
import com.legendshop.util.Assert;

/**
 * 自动登陆验证
 *
 */
public class AutoLoginAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private AuthService authService;

    public boolean supports(Class<?> authentication) {
        return (AutoAuthenticationToken.class.isAssignableFrom(authentication)  );
    }
    
    protected void doAfterPropertiesSet() throws Exception {
        Assert.notNull(this.authService, "A UserDetailsService must be set");
    }

    protected final UserDetails retrieveUser(String username, Authentication authentication)
            throws AuthenticationException {
        UserDetails loadedUser = null;

        try {
        	UserEntity user = authService.loadUserByUsername(username);
        	if(user != null){
        		loadedUser = createUser(user, LoginUserTypeEnum.USER, LoginTypeEnum.USERNAME_PASSWORD, user.getOpenId(), null, null);
        	}
        } catch (UsernameNotFoundException notFound) {
            throw notFound;
        } catch (Exception repositoryProblem) {
            throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }

        if (loadedUser == null) {
            throw new AuthenticationServiceException(
                    "UserDetailsService returned null, which is an interface contract violation");
        }
        return loadedUser;
    }

	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}

	 //no need to check passowrd
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {
		
	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal,
			Authentication authentication, UserDetails user) {
        // Ensure we return the original credentials the user supplied,
        // so subsequent attempts are successful even with encoded passwords.
        // Also ensure we return the original getDetails(), so that future
        // authentication events after cache expiry contain the details
    	AutoAuthenticationToken result = new AutoAuthenticationToken(principal, authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());

        return result;
	}

}