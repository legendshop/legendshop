/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.spi.service.security.AdminAuthService;

/**
 * 管理员登录.
 */
public class AdminDaoAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private final Logger log = LoggerFactory.getLogger(AdminDaoAuthenticationProvider.class);

	private AdminAuthService adminAuthService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * 检查用户名密码
	 */
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {
		if (authentication.getCredentials() == null) {
			logger.debug("Authentication failed: no credentials provided");
			throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		}
	}

	@Override
	protected UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
		UserDetails loadedUser = null;
		try {
			UserEntity userEntity = adminAuthService.loadUserByUsername(username, authentication.getCredentials().toString());
			if (userEntity != null) {
				boolean isValid = additionalAuthenticationChecks(userEntity, authentication.getCredentials().toString());
				if (!isValid) {
					log.debug("User {} password is not valid", userEntity.getName());
					return null;
				}
				loadedUser = createUser(userEntity, LoginUserTypeEnum.ADMIN, LoginTypeEnum.ADMIN_LOGIN, null, null, null);
			}

		} catch (UsernameNotFoundException notFound) {
			throw notFound;
		} catch (Exception repositoryProblem) {
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}

		return loadedUser;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return (AdminAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		AdminAuthenticationToken result = new AdminAuthenticationToken(principal, authentication.getCredentials(),
				authoritiesMapper.mapAuthorities(user.getAuthorities()));
		result.setDetails(authentication.getDetails());

		return result;
	}

	public void setAdminAuthService(AdminAuthService adminAuthService) {
		this.adminAuthService = adminAuthService;
	}

	/**
	 * 检查用户名密码.
	 *
	 * @param userDetails       the user details
	 * @param presentedPassword the presented password
	 * @return true, if additional authentication checks
	 */
	private boolean additionalAuthenticationChecks(UserEntity userDetails, String presentedPassword) {
		return passwordEncoder.matches(presentedPassword, userDetails.getPassword());
	}

}
