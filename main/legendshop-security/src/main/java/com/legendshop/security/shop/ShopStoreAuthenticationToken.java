package com.legendshop.security.shop;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class ShopStoreAuthenticationToken extends AbstractAuthenticationToken {

	private static final long serialVersionUID = 3815976139496438081L;

	/** 用户名称 **/
    private final Object principal;
    
    /**  accessToken 密码 **/
    private Object credentials;
    
    private String loginCode; //登录机构码
	
	
	  /**
     * This constructor can be safely used by any code that wishes to create a
     * <code>UsernamePasswordAuthenticationToken</code>, as the {@link
     * #isAuthenticated()} will return <code>false</code>.
     *
     */
    public ShopStoreAuthenticationToken(Object principal, Object credentials,String loginCode) {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        this.loginCode = loginCode;
        setAuthenticated(false);
    }
    
    public ShopStoreAuthenticationToken(Object principal, Object credentials,Collection<? extends GrantedAuthority> authorities,String loginCode) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        this.loginCode = loginCode;
        super.setAuthenticated(true);
    }
    

    //~ Methods ========================================================================================================

    public Object getCredentials() {
        return this.credentials;
    }

    public Object getPrincipal() {
        return this.principal;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }

        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        credentials = null;
    }

	public String getLoginCode() {
		return loginCode;
	}

	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}


}
