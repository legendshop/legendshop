/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.event;

import com.legendshop.framework.event.CoreEventId;
import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.entity.UserEvent;

/**
 * 记录操作历史 
 */
public class FireEvent extends SystemEvent<UserEvent> {

	public FireEvent(UserEvent userEvent) {
		super(CoreEventId.FIRE_EVENT);
		this.setSource(userEvent);
	}

}
