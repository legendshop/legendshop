// package com.legendshop.security.mobileCode;
//
// import com.legendshop.dao.GenericJdbcDao;
// import com.legendshop.model.constant.ApplicationEnum;
// import com.legendshop.model.constant.RoleEnum;
// import com.legendshop.model.constant.ShopStatusEnum;
//
// import com.legendshop.spi.service.security.AuthService;
// import com.legendshop.util.AppUtils;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.jdbc.core.RowMapper;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.GrantedAuthority;
// import org.springframework.security.core.userdetails.User;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UsernameNotFoundException;
//
// import java.sql.ResultSet;
// import java.sql.SQLException;
// import java.util.*;
//
//
// /**
//  * 手机密码登录登录信息
//  *
//  */
// public class MobileCodeAuthServiceImpl implements AuthService {
// 
//     /** The log. */
//     private static final Logger log = LoggerFactory.getLogger(MobileCodeAuthServiceImpl.class);
//     
//     
//     private GenericJdbcDao genericJdbcDao;
//     
//     //商家菜单服务
//     private ShopMenuGroupService shopMenuGroupService;
// 
//     private String sqlForFindRolesByUser = "SELECT DISTINCT r.role_type as name FROM ls_usr_role ur ,ls_role r WHERE r.enabled ='1'  AND ur.role_id=r.id AND r.app_no =? AND ur.user_id= ? ";
//     
//     private  String sqlForFindFunctionsByUser = "select DISTINCT f.protect_function as name from ls_usr_role ur ,ls_role r,ls_perm p, ls_func f where r.enabled = '1'  and ur.role_id=r.id and r.id=p.role_id and p.function_id=f.id and r.app_no = ? and f.app_no = ? and ur.user_id= ?";
//     
//     // 根据用户名称用户相关资料
//     private static String sqlForFindUserByMobileCode = "SELECT user_phone FROM ls_sms_log WHERE user_phone = ? and mobile_code = ?  AND TYPE = 2 AND rec_date > ? ORDER BY id DESC LIMIT 0,1";
// 
//     //根据商城Id来查找商城
//     private String sqlForfindShopUserById = "SELECT shop_id,status, user_id, user_name,sd.site_name as shopName FROM ls_shop_detail sd WHERE shop_id =  ?";
//     
//     //根据用户名称用户相关资料
//     private String sqlForFindUserByMobile = "SELECT u.id,u.name,u.note, u.password,u.dept_id AS deptId,u.enabled,u.open_id, ud.portrait_pic AS portraitPic,ud.nick_name AS nickName,ud.user_mobile as userMobile,ud.grade_id AS gradeId, ud.shop_id as shopId  " + 
//                 "FROM ls_user u INNER JOIN  ls_usr_detail ud ON u.id=ud.user_id  WHERE u.enabled = '1'  AND ud.user_mobile = ?";
//     /**
//      * 普通用户登录，查找用户
//      * 包括商家子账号登录
//      * 
//      */
//     @Override
//     public UserDetails loadUserByUsername(String sellerName, Authentication authentication) throws UsernameNotFoundException {
//         Date now = new Date();
//         Calendar cal = Calendar.getInstance();
//         cal.setTime(now);
//         cal.add(Calendar.MINUTE, -5);
//         // 获取用户发送的短信，有效时间是5分钟
//         String mobile = authentication.getPrincipal().toString();
//         String password = authentication.getCredentials().toString();
//         Date date = cal.getTime();
//         String mobileResult=genericJdbcDao.get(sqlForFindUserByMobileCode, String.class, mobile, password,date);
//         if (mobileResult == null) {
//             log.debug("Can not find mobile by sellerName {}", sellerName);
//             return null;
//         }
// 
//         UserEntity user = loadUserByMobile(mobile);
//         
//         
//         
//         
//         return createUser(user, ApplicationEnum.FRONT_END.value(), null,LoginTypeEnum.USERNAME_PASSWORD, user.getOpenId(), null, null);
// 
//     }
// 
//      private UserEntity loadUserByMobile(String mobile) throws UsernameNotFoundException {
//         // 获取用户信息
//          log.debug("findUserByName, run sql {}, name {}", sqlForFindUserByMobile, mobile);
//          UserEntity userEntity=genericJdbcDao.get(sqlForFindUserByMobile, UserEntity.class, new Object[] {mobile});
//          return userEntity;
//      }
//      
//         /**
//          * 返回spring security所需的用户信息
//          * @param user  用户信息
//          * @param appNo 应用分类
//          * @param loginUserType 用户登录类型
//          * @param opendId 第三方登录的openId
//          * @param accessToken 第三方登录的accessToken
//          * @param type 第三方登录的类型 qq or weixin等
//          * @param loginType  密码登录还是第三方登录
//          * @return
//          */
//         protected User createUser(UserEntity user, String appNo, LoginUserTypeEnum loginUserType, LoginTypeEnum loginType, String opendId, String accessToken, String type){
//             List<GrantedFunction> functions = this.findFunctionsByUser(user.getId(), appNo);
//             
//             ShopUser shopUser = findShopUser(user,appNo);
//             
//             if(loginUserType == null){
//                 loginUserType = LoginUserTypeEnum.USER;
//             }
//             
//             // 获取角色信息
//             Collection<GrantedAuthority> roles = findRolesByUser(user.getId(), appNo);
//             if (AppUtils.isBlank(roles)) {
//                 throw new UsernameNotFoundException("User has no GrantedAuthority");
//             }
//             
//             //如果店铺状态不正常则去掉该商家或者子账号的权限
//             if(shopUser != null && !ShopStatusEnum.NORMAL.value().equals(shopUser.getStatus())){
//                 for (Iterator<GrantedAuthority> iterator = roles.iterator(); iterator.hasNext();) {
//                     GrantedAuthority grantedAuthority = (GrantedAuthority) iterator.next();
//                     if(RoleEnum.ROLE_SUPPLIER.name().equals(grantedAuthority.getAuthority())){
//                         iterator.remove();
//                     }
//                     
//                 }
//             }
//             
//             //Create user info
//             User minuser = new UserDetail(
//                     user.getId(),
//                     user.getName(), user.getPassword(),user.getPortraitPic(),user.getNickName(), getBoolean(user.getEnabled()), true, true, true, roles,
//                     functions,opendId, accessToken,type, shopUser,user.getGradeId(),user.getDeptId(),loginType.value(), loginUserType.value());
//             return minuser;
//         }
//         
//         
//         /**
//          * Gets the boolean.
//          * 
//          * @param b
//          *            the b
//          * @return the boolean
//          */
//         private boolean getBoolean(String b) {
//             return "1".endsWith(b) ? true : false;
//         }
// 
//     @Override
//     public UserDetails loadUserByUsername(String arg0)
//             throws UsernameNotFoundException {
//         // TODO Auto-generated method stub
//         return null;
//     }
// 
//     @Override
//     public Collection<GrantedFunction> getFunctionsByRoles(
//             Collection<? extends GrantedAuthority> roles) {
//         // TODO Auto-generated method stub
//         return null;
//     }
// 
//     @Override
//     public List<GrantedAuthority> findRolesByUser(String userId, String appNo) {
//         log.debug("findRolesByUser,run sql {}, userId {}", sqlForFindRolesByUser, userId);
//         return genericJdbcDao.query(sqlForFindRolesByUser, new Object[] { appNo, userId }, new RowMapper<GrantedAuthority>() {
//             public GrantedAuthority mapRow(ResultSet rs, int index) throws SQLException {
//                 return new GrantedAuthorityImpl(rs.getString("name"));
//             }
//         });
//     }
// 
//     @Override
//     public List<GrantedFunction> findFunctionsByUser(String userId, String appNo) {
//         log.debug("findFunctionsByUser,run sql {}, userId {}", sqlForFindFunctionsByUser, userId);
//         return genericJdbcDao.query(sqlForFindFunctionsByUser, new Object[] { appNo, appNo, userId }, new RowMapper<GrantedFunction>() {
//             public GrantedFunction mapRow(ResultSet rs, int index) throws SQLException {
//                 return new GrantedFunctionImpl(rs.getString("name"));
//             }
//         });
//     }
// 
//     @Override
//     public ShopUser findShopUser(UserEntity user, String appNo) {
//         if(user.getShopId() == null){
//             return null;
//         }
//         log.debug("findShopUserByName, run sql {}, name {}", sqlForfindShopUserById, user.getName());
//         List<ShopUser> result = null;
//         result =  genericJdbcDao.query(sqlForfindShopUserById, new Object[]{user.getShopId()}, new ShopUserMapper());
//         if(AppUtils.isBlank(result)){
//             return null;
//         }else{
//             ShopUser shopUser = result.iterator().next();
//             if(shopMenuGroupService != null){
//                 shopUser.setShopMenuGroupList(shopMenuGroupService.getShopMenuGroup());
//             }
//             return shopUser;
//         }
//     }
//     
//     public 	class ShopUserMapper implements RowMapper<ShopUser> {
//         public ShopUser mapRow(ResultSet rs, int rowNum) throws SQLException {
//             ShopUser shopUser = new ShopUser();
//             shopUser.setUserName(rs.getString("user_name"));
//             shopUser.setUserId(rs.getString("user_id"));
//             shopUser.setShopId(rs.getLong("shop_id"));
//             shopUser.setStatus(rs.getInt("status"));
//             shopUser.setShopName(rs.getString("shopName"));
//             return shopUser;
//         }
//     }
// 
//     public void setGenericJdbcDao(GenericJdbcDao genericJdbcDao) {
//         this.genericJdbcDao = genericJdbcDao;
//     }
// 
//     public void setShopMenuGroupService(ShopMenuGroupService shopMenuGroupService) {
//         this.shopMenuGroupService = shopMenuGroupService;
//     }
// }
