/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.legendshop.security.handler.AuthenticationSuccessBusinessHandler;

/**
 * 系统登录成功后的动作
 *
 */
public class AuthenticationSuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired
	private AuthenticationSuccessBusinessHandler authenticationSuccessBusinessHandler;
	/**
	 * 系统登录成功后的动作
	 * 
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {
		authenticationSuccessBusinessHandler.onAuthenticationSuccess(request, response, authentication);
		super.onAuthenticationSuccess(request, response, authentication);
	}
	public void setAuthenticationSuccessBusinessHandler(AuthenticationSuccessBusinessHandler authenticationSuccessBusinessHandler) {
		this.authenticationSuccessBusinessHandler = authenticationSuccessBusinessHandler;
	}
}
