/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.authorize;

/**
 * 返回数据类型.
 */
public enum ResultTypeEnum {
	
	/** 前台整页刷新. */
	page,
	
	/** 后台整页刷新. */
	adminPage,

	/** json数据. */
	json
	
}