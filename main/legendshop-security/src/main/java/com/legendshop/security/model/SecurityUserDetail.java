/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.legendshop.framework.model.AbstractUserDetail;
import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.model.security.ShopUser;
import com.legendshop.util.Assert;

/**
 * 扩展的Spring security用户,必须要继承AbstractUserDetail
 * 
 */
public class SecurityUserDetail extends SecurityUser implements AbstractUserDetail {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5898134878028341628L;

	/** The functions. */
	private Collection<GrantedFunction> functions;

	/** The user id. */
	private String userId;

	/** 第三方登陆用户的唯一标识 */
	private String openId;

	/** 第三方用户授权凭证 */
	private String accessToken;

	/** 第三方登陆类型 */
	private String type;

	/** 头像图片路径 */
	private String portraitPic;

	/** 商城所在的用户 */
	private ShopUser shopUser;

	/** 登录类型 **/
	private String logType;

	/** 登录的用户类型 **/
	private String loginUserType;

	/** 登录的IP地址 **/
	private String ipAddress;

	/** 用户商城等级 */
	private Integer gradeId;

	/** 部门Id */
	private Integer deptId;

	/** 用户昵称 */
	private String nickName;

	/**
	 * 在做并发控制时, 以userId来区分一个唯一的用户.
	 */
	public boolean equals(Object object) {
		if (object instanceof SecurityUserDetail) {
			if (this.userId.equals(((SecurityUserDetail) object).getUserId()))
				return true;
		}
		return false;
	}

	public int hashCode() {
		return this.userId.hashCode();
	}

	public SecurityUserDetail() {

	}

	public SecurityUserDetail(String username) {
		super(username);
	}

	public SecurityUserDetail(String userId, String username, String password, String portraitPic, String nickName, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<GrantedAuthority> authorities, Collection<GrantedFunction> functions,
			String openId, String accessToken, String type, ShopUser shopUser, Integer gradeId, Integer deptId, String logType, String loginUserType) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		Assert.notNull(loginUserType);
		Assert.notNull(logType);
		this.functions = functions;
		this.userId = userId;
		this.portraitPic = portraitPic;
		this.nickName = nickName;
		this.openId = openId;
		this.accessToken = accessToken;
		this.type = type;
		this.shopUser = shopUser;
		this.gradeId = gradeId;
		this.deptId = deptId;
		this.logType = logType;
		this.loginUserType = loginUserType;
	}

	/**
	 * 是否是商家
	 */
	@Override
	public boolean isShopUser() {
		return shopUser != null && shopUser.getShopId() != null;
	}

	/**
	 * Gets the functions.
	 * 
	 * @return the functions
	 */
	public Collection<GrantedFunction> getFunctions() {
		return functions;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOpenId() {
		return openId;
	}

	public String getType() {
		return type;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public String getNickName() {
		return nickName;
	}

	public ShopUser getShopUser() {
		return shopUser;
	}
	
	/**
	 * 获取商家的Id
	 * @return
	 */
	public Long getShopId() {
		if(shopUser == null) {
			return null;
		}
		return shopUser.getShopId();
	}

	/**
	 * 返回copy的新对象
	 * 
	 */
	public Object newUser() {
		return new SecurityUserDetail(this.userId, this.getUsername(), this.getPassword(), this.portraitPic, this.nickName, this.isEnabled(),
				this.isAccountNonExpired(), this.isCredentialsNonExpired(), this.isAccountNonLocked(), this.getAuthorities(), this.functions, this.openId,
				this.accessToken, this.type, this.shopUser, this.gradeId, this.deptId, this.logType, this.loginUserType);
	}

	public String getLogType() {
		return logType;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getLoginUserType() {
		return loginUserType;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}
	

}
