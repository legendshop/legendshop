/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.security.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.model.dto.ShopUrlPermissionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.framework.handler.SecurityHandler;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.ShopPermFunctionEnum;
import com.legendshop.model.entity.ShopPermId;
import com.legendshop.model.entity.ShopUser;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopMenuGroupService;
import com.legendshop.spi.service.ShopPermService;
import com.legendshop.spi.service.ShopUserService;
import com.legendshop.util.AppUtils;

/**
 * 根据用户角色， 过滤后台资源链接.
 * 后端使用
 */
@Component("shopSecurityResourceFilterHandler")
public class ShopSecurityResourceFilterHandler implements SecurityHandler {

    private final Logger log = LoggerFactory.getLogger(ShopSecurityResourceFilterHandler.class);
    
    @Autowired
    private ShopUserService shopUserService;
    
    @Autowired
    private ShopPermService shopPermService;
    
    @Autowired
    private ShopMenuGroupService shopMenuGroupService;
    
	/**
     * 检查用户是否有权限访问特定的url
     */
    public boolean handle(HttpServletRequest request) {
    	
    	// 获取请求URL
    	String path = request.getServletPath();
    	
    	// 判断是否登录
		SecurityUserDetail userDetail = UserManager.getUser(request);
		
		// 是否有权限
		boolean hasAccessRight = true;
		
		// 判断是否是商家子账号登录，子账号登录才进行权限判断
		if (userDetail.getLoginUserType().equals(LoginUserTypeEnum.EMPLOYEE.value())) {
			
			log.debug("###########################开始 拦截子账号登录，检查权限################################");
			
			// 子账号用户id
			String userId = userDetail.getUserId();
			Long shopUserId = Long.parseLong(userId);
			
			ShopUser shopUser = shopUserService.getShopUser(shopUserId);
			if (AppUtils.isNotBlank(shopUser)) {
				
				// 获取子账号职位相关的所有权限
				Long shopRoleId = shopUser.getShopRoleId();
				List<ShopPermId> shopPerms = shopPermService.queryShopPermsByRoleId(shopRoleId);
				
				hasAccessRight = checkPath(path,shopPerms);
				
			log.debug("###########################结束 拦截子账号登录，权限为"+ hasAccessRight +"################################");
			}
			
		}
        return hasAccessRight;
    }

    
    //校验子账号是否有访问该URL的权限
	private boolean checkPath(String path, List<ShopPermId> shopPerms) {
		
		// 根据请求URL 定位该权限是查看还是编辑
		ShopUrlPermissionDto menuFunc = shopMenuGroupService.getShopMenuByURL(path);
		if (AppUtils.isNotBlank(menuFunc)){
			for (ShopPermId shopPerm : shopPerms) {

				//获取权限
				String function = shopPerm.getFunction();

				if (ShopPermFunctionEnum.CHECK_FUNC.value().equals(menuFunc.getType())) {//判断用户权是否查看权限
					if (function.contains(ShopPermFunctionEnum.CHECK_FUNC.value())) {
						if (menuFunc.getName().equals(shopPerm.getLabel())){
							return true;
						}
					}
				}
				if (ShopPermFunctionEnum.EDITOR_FUNC.value().equals(menuFunc.getType())) {//判断用户是否有编辑权限
					if (function.contains(ShopPermFunctionEnum.EDITOR_FUNC.value())) {
						if (menuFunc.getName().equals(shopPerm.getLabel())){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
