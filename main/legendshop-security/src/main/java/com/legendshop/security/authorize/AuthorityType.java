/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.authorize;

/**
 * 卖家端权限定义.
 */
public enum AuthorityType{
	// 包含了枚举的中文名称, 枚举的索引值
	ORDERS_MANAGE("订单管理", "ORDERS_MANAGE"),
	
	ORDERS_MANAGE_READ_ONLY("查看订单", "ORDERS_MANAGE_READ_ONLY"),
	
	PRESELL_ORDER_MANAGE("预售订单", "PRESELL_ORDER_MANAGE"),
	
	PRESELL_ORDER_MANAGE_READ_ONLY("查看预售订单", "PRESELL_ORDER_MANAGE_READ_ONLY"),
	
	ORDER_RETURN("退款及退货", "ORDER_RETURN"),
	
	ORDER_RETURN_READ_ONLY("查看退款及退货", "ORDER_RETURN_READ_ONLY"),
	
	SELLING_PROD("出售中的商品", "SELLING_PROD"),
	
	SELLING_PROD_READ_ONLY("查看出售中的商品", "SELLING_PROD_READ_ONLY"),
	
	JOIN_DIST_PROD("分销的商品", "JOIN_DIST_PROD"),
	
	JOIN_DIST_PROD_READ_ONLY("查看分销的商品", "JOIN_DIST_PROD_READ_ONLY"),
	
	INSTORE_PROD("仓库中的商品", "INSTORE_PROD"),
	
	INSTORE_PROD_READ_ONLY("查看仓库中的商品", "INSTORE_PROD_READ_ONLY"),
	
	INDUSBIN_PROD("商品回收站", "INDUSBIN_PROD"),
	
	INDUSBIN_PROD_READ_ONLY("查看商品回收站", "INDUSBIN_PROD_READ_ONLY"),
	
	APPLY_BRAND("品牌申请", "APPLY_BRAND"),
	
	APPLY_BRAND_READ_ONLY("查看品牌", "APPLY_BRAND_READ_ONLY"),
	
	IMAGE_ADMIN("图片管理", "IMAGE_ADMIN"),
	
	IMAGE_ADMIN_READ_ONLY("查看图片管理", "IMAGE_ADMIN_READ_ONLY"),
	
	STOCK_WARNING("库存预警", "STOCK_WARNING"),
	
	STOCK_WARNING_READ_ONLY("查看库存预警", "STOCK_WARNING_READ_ONLY"),
	
	COUPON_MANAGE("优惠券管理", "COUPON_MANAGE"),
	
	COUPON_MANAGE_READ_ONLY("查看优惠券", "COUPON_MANAGE_READ_ONLY"),
	
	TRY_MANAGE("试吃活动管理", "TRY_MANAGE"),
	
	TRY_MANAGE_READ_ONLY("查看试吃活动", "TRY_MANAGE_READ_ONLY"),
	
	SHOP_CATEGORY("商品类目", "SHOP_CATEGORY"),
	
	SHOP_CATEGORY_READ_ONLY("查看商品类目", "SHOP_CATEGORY_READ_ONLY"), 
	
	//权限注解新增
	
	PRODUCT_IMPORT("商品导入","PRODUCT_IMPORT"), 
	
	INVOICDE_MGT("发票管理","INVOICDE_MGT"), 
	
	ORDER_DIST("分销卖出商品","ORDER_DIST"), 
	
	PUBLISH_PROD("商品发布","PUBLISH_PROD"),
	
	PROD_COMMENT("评价管理","PROD_COMMENT"),
	
	PROD_CONS("商品咨询","PROD_CONS"),
	
	REPORT_FORBIT("被举报禁售","REPORT_FORBIT"),
	
	PRODUCT_ARRIVAL("到货通知","PRODUCT_ARRIVAL"),
	
	SHOP_SALES("店铺概况","SHOP_SALES"),
	
	PROD_SALESTREND("商品分析","PROD_SALESTREND"),
	
	OPERATING_REPORT("运营报告","OPERATING_REPORT"),
	
	PURCHASE_ANALYSIS("购买分析","PURCHASE_ANALYSIS"),
	
	TRAFFIC_STATISTICS("流量统计","TRAFFIC_STATISTICS"),
	
	PRICE_RANGE("价格区间","PRICE_RANGE"),
	
	SHOP_ORDER_BILL("结账功能","SHOP_ORDER_BILL"),
	
	SHOP_JOBS("职位管理","SHOP_JOBS"),
	
	SHOP_USERS("员工管理","SHOP_USERS"),
	
	SHOP_SETTING("基本信息","SHOP_SETTING"),
	
	SHOP_NAV("头部菜单","SHOP_NAV"),
	
	SHOP_BANNER("轮播图管理","SHOP_BANNER"),
	
	SHOP_DECORATE("楼层装修","SHOP_DECORATE"),
	
	DELIVER_ADDRESS("发货地址","DELIVER_ADDRESS"),
	
	TRANSPORT_TEMP("运费模版","TRANSPORT_TEMP"),
	
	DELIVERY_TYPE("配送方式","DELIVERY_TYPE"),
	
	PRINT_TMPL("打印模版","PRINT_TMPL"),
	
	MAILINGFEE_SETTING("包邮设置","MAILINGFEE_SETTING"),
	
	STORE_MANAGE("门店管理","STORE_MANAGE"),
	
	PRESELL_MANAGE("预售活动管理","PRESELL_MANAGE"),
	
	PRESELL_ORDERS_MANAGE("预售订单","PRESELL_ORDERS_MANAGE"),
	
	TAG_MANAGE("标签管理","TAG_MANAGE"),
	
	SINGLE_MARKETING("单品满折促销","SINGLE_MARKETING"),
	
	FULL_COURT_MARKETING("全场满折促销","FULL_COURT_MARKETING"),
	
	ZK_MARKETING("限时折扣促销","ZK_MARKETING"),
	
	SHIPPING_MANGE("包邮活动管理","SHIPPING_MANGE"),
	
	FULL_MAIL("包邮促销","FULL_MAIL"),
	
	AUCTIONS_MANAGE("拍卖活动管理","AUCTIONS_MANAGE"),
	
	GROUP_MANAGE("团购活动管理","GROUP_MANAGE"),
	
	MERGE_GROUP_MANAGE("拼团活动管理","MERGE_GROUP_MANAGE"),
	
	SECKILL_MANAGE("秒杀活动管理","SECKILL_MANAGE"),
	
	
	
	
	
	
//	ORDER_PRINT("打印订单", "orderPrint"),
//	
//	ORDER_DETAIL("订单详情", "orderDetail"),
//	
//	PRINT_DELIVERY_DOCUMENT("打印物流单", "printDeliveryDocument"),
//	
//	ORDER_EXPRESS("查看物流", "orderExpress"),
//	

	
	;
	
	/** The name. */
	private String name;
	
	/** The index. */
	private String value;
	
	/**
	 * The Constructor.
	 *
	 * @param name the name
	 * @param index the index
	 */
	private AuthorityType(String name, String value) {
	    this.name = name;
	    this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}