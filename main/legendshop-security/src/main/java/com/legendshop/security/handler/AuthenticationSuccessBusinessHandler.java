/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

/**
 * The Interface AuthenticationSuccessBusinessHandler.
 */
public interface AuthenticationSuccessBusinessHandler {
	
	/**
	 * 登录成功后的逻辑处理流程
	 *
	 */
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication);
}
