/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.model;

import java.io.Serializable;

/**
 * 扩展的Spring security用户,必须要继承AbstractUserDetail
 * 
 */
public class SimpleSecurityUserDetail implements Serializable {

	private static final long serialVersionUID = -8123479542676325952L;

	/** 用户Id. */
	private  String userId;
	
	/** 用户名称 **/
	private String userName;

	/** 商家Id **/
	private Long shopId;

	/** 登录类型 **/
	private String logType;

	/** 登录的用户类型 **/
	private String loginUserType;

	/** 头像图片路径 */
	private String portraitPic;

	public SimpleSecurityUserDetail(SecurityUserDetail user) {
		if(user != null) {
			this.userId = user.getUserId();
			this.userName = user.getUsername();
			this.shopId = user.getShopId();
			this.logType = user.getLogType();
			this.loginUserType = user.getLoginUserType();
			this.portraitPic = user.getPortraitPic();
		}
		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getLoginUserType() {
		return loginUserType;
	}

	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

}
