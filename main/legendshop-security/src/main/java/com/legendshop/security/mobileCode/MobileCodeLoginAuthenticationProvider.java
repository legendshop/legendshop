package com.legendshop.security.mobileCode;

import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.constant.UserSourceEnum;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.security.GrantedAuthorityImpl;
import com.legendshop.web.helper.IPHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.spi.service.security.AuthService;

import java.util.ArrayList;
import java.util.List;

import static com.legendshop.util.AppUtils.isBlank;

/**
 * 手机验证码登录
 *
 * @author newway
 */
public class MobileCodeLoginAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	private final Logger log = LoggerFactory.getLogger(MobileCodeLoginAuthenticationProvider.class);

    private AuthService authService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public boolean supports(Class<?> authentication) {
        return (MobileCodeAuthenticationToken.class.isAssignableFrom(authentication));
    }

    protected void doAfterPropertiesSet() throws Exception {
        Assert.notNull(this.authService, "A UserDetailsService must be set");
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {

    }

    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
        MobileCodeAuthenticationToken result = new MobileCodeAuthenticationToken(principal, authentication.getCredentials(), authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());
        return result;
    }


    @Override
    protected UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
        UserDetails loadedUser = null;
        try {
            UserEntity userEntity = authService.loadUserByMobile(username);
            // 获取角色信息
            loadedUser = createUser(userEntity, LoginUserTypeEnum.USER, LoginTypeEnum.USERNAME_PASSWORD, null, null, null);
        } catch (UsernameNotFoundException notFound) {
            throw notFound;
        } catch (Exception repositoryProblem) {
            throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }
        if (loadedUser == null) {
            throw new AuthenticationServiceException(
                    "UserDetailsService returned null, which is an interface contract violation");
        }
        return loadedUser;
    }

    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }
    
    /**
     * 检查用户名密码
     *
     * @param userDetails
     * @param 
     * @return
     * @throws AuthenticationException
     */
    private boolean additionalAuthenticationChecks(UserEntity userDetails, String presentedPassword) {
        return passwordEncoder.matches(presentedPassword, userDetails.getPassword());
    }

}
