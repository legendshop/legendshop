/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;

/**
 * 登录错误异常处理.
 */
public interface AuthenticationFailureBusinessHandler {
	
	/**
	 * On authentication failure.
	 *
	 * @param request the request
	 * @param response the response
	 * @param exception the exception
	 */
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception);
}
