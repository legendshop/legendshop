package com.legendshop.security.handler.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.AuthenticationException;

import com.legendshop.security.handler.AuthenticationFailureBusinessHandler;

public class AuthenticationFailureBusinessHandlerImpl implements AuthenticationFailureBusinessHandler {
	/** The Constant TRY_LOGIN_COUNT. */
	public static final String TRY_LOGIN_COUNT = "TRY_LOGIN_COUNT";
	
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
		increaseTryLoginCount(request);
	}

	/**
	 * 增加登录错误的次数
	 * 
	 * @param request
	 *            the request
	 */
	private void increaseTryLoginCount(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}

		Integer count = (Integer) session.getAttribute(TRY_LOGIN_COUNT);
		if (count == null) {
			count = 0;
		}
		count++;
		session.setAttribute(TRY_LOGIN_COUNT, count);
	}
}
