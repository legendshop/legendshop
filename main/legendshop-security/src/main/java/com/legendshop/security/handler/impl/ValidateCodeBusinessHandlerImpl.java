/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AuthenticationServiceException;

import com.legendshop.core.utils.UserValidationImageUtil;
import com.legendshop.security.handler.ValidateCodeBusinessHandler;
import com.legendshop.web.util.ValidateCodeUtil;

/**
 * 检查图片验证码的逻辑.
 */
public class ValidateCodeBusinessHandlerImpl implements ValidateCodeBusinessHandler {


	public static final String DEFAULT_SESSION_VALIDATE_CODE_FIELD = "randNum";
	
	/**
	 * 检查图片验证码
	 */
	@Override
	public boolean doProcess(HttpServletRequest request, HttpSession session) {
		// check validate code
		if (UserValidationImageUtil.needToValidation(session)) {
			if (!checkValidateCode(request)) {
				throw new AuthenticationServiceException("验证码失败");
			}
		}
		return true;
	}
	
	private boolean checkValidateCode(HttpServletRequest request) {
		String validateCodeParameter = obtainValidateCodeParameter(request);
		return ValidateCodeUtil.validateCode(validateCodeParameter, request.getSession());
	}
	

	/**
	 * Obtain validate code parameter.
	 * 
	 * @param request
	 *            the request
	 * @return the string
	 */
	private String obtainValidateCodeParameter(HttpServletRequest request) {
		return request.getParameter(DEFAULT_SESSION_VALIDATE_CODE_FIELD);
	}

	
}
