package com.legendshop.security.handler.impl;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;

import com.legendshop.base.event.LoginEvent;
import com.legendshop.core.handler.ShopCartHandler;
import com.legendshop.framework.event.EventHome;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.security.handler.AuthenticationSuccessBusinessHandler;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.CookieUtil;

public class AuthenticationSuccessBusinessHandlerImpl implements AuthenticationSuccessBusinessHandler {
	
	/** The log. */
	private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessBusinessHandlerImpl.class);

	@Value("false")
	private boolean supportSSO=false;

	private BasketService basketService;
	
	private ShopCartHandler shopCartHandler;
	
	private UserGradeService userGradeService;
	
	private ProductService productService;
	
	/** 是否管理员登录. */
	private boolean isAdminLogin;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		
		//如果是采用HTTPS登录，其他链接采用HTTP， 则要将HTTP的cookies设置
		if(request.isSecure()){
	        Cookie cookie = new Cookie("JSESSIONID", request.getSession().getId());  
			cookie.setMaxAge(-1);// no store
			cookie.setSecure(false);
			String contextPath = request.getContextPath();
			if(AppUtils.isBlank(contextPath)){
				contextPath = "/";
			}
			cookie.setPath(contextPath);
	        response.addCookie(cookie);  
		}
		
		this.loginSuccessProcess(request, response,authentication );
	}
	

	/**
	 * 登录成功后的处理
	 * @param request
	 * @param response
	 * @param authentication
	 */
	public void loginSuccessProcess(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
		HttpSession session = request.getSession();
		SecurityUserDetail userDetail = (SecurityUserDetail) authentication.getPrincipal();
		String userName = userDetail.getUsername();
		String userId = userDetail.getUserId();

		String USER_OPENID =(String)session.getAttribute(Constants.USER_OPENID);
		if(USER_OPENID != null){
			logger.info("AuthenticationSuccessBusinessHandlerImpl Login success-------------openId-----------------"+USER_OPENID);
			userDetail.setOpenId(USER_OPENID);
		}
		
		// Place the last username attempted into HttpSession for views
		String nickName = userDetail.getNickName();
		if(AppUtils.isNotBlank(nickName)){
			session.setAttribute(Constants.USER_NAME, nickName);
		}else{
			session.setAttribute(Constants.USER_NAME, userName);
		}
		
		//用户等级
		String gradeName = "注册会员";
		if(userDetail.getGradeId()!=null && userDetail.getGradeId()!=0){
			UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
			if(userGrade!=null){
				gradeName = userGrade.getName();
			}
		}
		session.setAttribute(Constants.USER_GRADE, gradeName);
		
		//管理员登录不需要处理这些情况
		if(!isAdminLogin){
			// remove login error count
			clearTryLoginCount(request);
			// 增加session登录信息
			List<Basket> shopcarts = null;
			try {
				// 处理在session中的购物车
				shopcarts = shopCartHandler.getShopCart(request);
				if(AppUtils.isNotBlank(shopcarts)){
					// 保存进去数据库
					for (Basket basket : shopcarts) {
						if(AppUtils.isBlank(basket))
							continue;
						Product product=productService.getProductById(basket.getProdId());
						if(product!=null && ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus()) && !userId.equals(product.getUserId())){
							basketService.saveToCart(userId, basket.getProdId(), basket.getBasketCount(), basket.getSkuId(),basket.getShopId(),basket.getDistUserName(),basket.getStoreId());
						}
					}
					shopCartHandler.clearShopCarts(request, response);
					session.removeAttribute(Constants.BASKET_HW_COUNT);
					session.removeAttribute(Constants.BASKET_HW_ATTR);
				}
			} catch (Exception e) {
				logger.error("process unsave order", e);
			}
			CookieUtil.addCookie(response, Constants.LOGIN_STATUS, "Y");
			if (supportSSO) {
				// 增加BBS的登录用户 SSO to club
				CookieUtil.addCookie(response, Constants.CLUB_COOIKES_NAME, userName);
			}
		}
			//如果是后台登录,后台登录也不一定有店铺的
			if(userDetail.getShopUser() != null){
				session.setAttribute(Constants.SHOP_ID, userDetail.getShopUser().getShopId());
				session.setAttribute(Constants.SHOP_STATUS, userDetail.getShopUser().getStatus());
			}

		// 发布登录事件, 1.保存登录历史, 2. 发送登录成功的积分
         EventHome.publishEvent(new LoginEvent(new LoginSuccess(userDetail.getUserId(), userDetail.getUsername(), IPHelper.getIpAddr(request), userDetail.getLoginUserType(), VisitSourceEnum.PC.value())));
	}

	private void clearTryLoginCount(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(AuthenticationFailureBusinessHandlerImpl.TRY_LOGIN_COUNT);
		}
	}


	public void setSupportSSO(boolean supportSSO) {
		this.supportSSO = supportSSO;
	}


	public void setBasketService(BasketService basketService) {
		this.basketService = basketService;
	}


	public void setShopCartHandler(ShopCartHandler shopCartHandler) {
		this.shopCartHandler = shopCartHandler;
	}


	public void setUserGradeService(UserGradeService userGradeService) {
		this.userGradeService = userGradeService;
	}


	public void setProductService(ProductService productService) {
		this.productService = productService;
	}


	public void setAdminLogin(boolean isAdminLogin) {
		this.isAdminLogin = isAdminLogin;
	}
}
