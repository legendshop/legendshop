package com.legendshop.security.thirdparty;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.spi.service.security.AuthService;

/**
 * 第三方登陆验证
 *
 */
public class ThirdPartyLoginAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private AuthService authService;

	public boolean supports(Class<?> authentication) {
		return (ThirdPartyAuthenticationToken.class.isAssignableFrom(authentication));
	}

	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {
		if (authentication.getCredentials() == null) {
			logger.debug("Authentication failed: no credentials provided");
			throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		}
	}

	protected void doAfterPropertiesSet() throws Exception {
		Assert.notNull(this.authService, "A UserDetailsService must be set");
	}

	protected final UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
		UserDetails loadedUser = null;
		ThirdPartyAuthenticationToken token = (ThirdPartyAuthenticationToken) authentication;
		try {
			UserEntity userEntity = authService.loadThirdPartyUser(username, token.getAccessToken(), token.getOpenId(), token.getType(), token.getSource());
			if (userEntity != null) {
				loadedUser = createUser(userEntity, LoginUserTypeEnum.USER, LoginTypeEnum.THIRD_PART, token.getOpenId(),
						token.getAccessToken(), token.getType());
			}

		} catch (UsernameNotFoundException notFound) {
			throw notFound;
		} catch (Exception repositoryProblem) {
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}

		if (loadedUser == null) {
			throw new AuthenticationServiceException("UserDetailsService returned null, which is an interface contract violation");
		}
		return loadedUser;
	}

	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		ThirdPartyAuthenticationToken result = new ThirdPartyAuthenticationToken(principal, authentication.getCredentials(),
				authoritiesMapper.mapAuthorities(user.getAuthorities()));
		result.setDetails(authentication.getDetails());

		return result;
	}

}