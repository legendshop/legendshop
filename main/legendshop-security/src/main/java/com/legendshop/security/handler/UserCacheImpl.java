/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

import com.legendshop.framework.cache.LegendCacheManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 登陆时的用户缓存.
 */
public class UserCacheImpl implements UserCache{
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(UserCacheImpl.class);
	
	private LegendCacheManager legendCacheManager;
	
	private String namespace = "";
	
	/** The cache name. */
	private String cacheName = null;

	@Override
	public UserDetails getUserFromCache(String username) {
		Cache cache = legendCacheManager.getCache(cacheName);
		String name = getUserCacheKey(username);
		SimpleValueWrapper valueWrapper = (SimpleValueWrapper) cache.get(name);
		if(valueWrapper != null){
			log.debug(" Load username {} from cache", name);
			return (UserDetails)valueWrapper.get();
		}else{
			return null;
		}
	
	}

//	String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
//	boolean accountNonLocked, Collection<GrantedAuthority> authorities, Collection<GrantedFunction> functions, String userId
	@Override
	public void putUserInCache(UserDetails user) {
		log.debug("Put username {} into cache", user.getUsername());
		Cache cache = legendCacheManager.getCache(cacheName);
		SecurityUserDetail userDetail = (SecurityUserDetail)user;
	 	cache.put(getUserCacheKey(user.getUsername()), userDetail.newUser());

	}

	@Override
	public void removeUserFromCache(String username) {
		log.debug("Remove username {} from cache", username);
		Cache cache = legendCacheManager.getCache(cacheName);
		cache.evict(getUserCacheKey(username));
	}


	public void setLegendCacheManager(LegendCacheManager legendCacheManager) {
		this.legendCacheManager = legendCacheManager;
	}


	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
	
	private String getUserCacheKey(String name){
		return namespace + name;
	}


	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

}
