/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.authorize;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 验证权限的过滤器
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Authorize {
	
	/**
	 * 权限类型,可以有多个
	 * @return
	 */
	AuthorityType[] authorityTypes();
	
	/**
	 * 返回的类型,默认返回错误页面
	 * @return
	 */
	ResultTypeEnum resultType() default ResultTypeEnum.page;
}
