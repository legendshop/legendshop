/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.redis.UserOnlineStatusHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import com.legendshop.core.event.HttpWrapper;
import com.legendshop.core.event.LogoutEvent;
import com.legendshop.framework.event.EventHome;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.web.util.WeiXinUtil;

/**
 * 登出过滤器
 */
public class SecurityContextLogoutHandlerImpl extends SecurityContextLogoutHandler {
	
	private final Logger logger = LoggerFactory.getLogger(SecurityContextLogoutHandlerImpl.class);

    protected UserDetailService userDetailService;

    protected UserOnlineStatusHelper userOnlineStatusHelper;

    /**
     * 系统退出是删除session和spring security的内容
     */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        
        //在微信通出时，需要清空用户的OPENID,否则下次会自动免密登录
        boolean isWeixin = WeiXinUtil.isWeiXin(request);
        boolean isMobile = WeiXinUtil.isMoblie(request);
        logger.info(String.format("logout status is isWeixin : %b ,isMobile : %b ",isWeixin,isMobile));
		SecurityUserDetail user = UserManager.getUser(request);
		if(user != null){
			//登出
			userOnlineStatusHelper.offline(user.getLoginUserType(), user.getUserId());
		}

        if (isWeixin && isMobile) {
            try {
                if (user != null) {
                	String userId = user.getUserId();
                    userDetailService.bingUserOpenId(userId, "");
                }
            } catch (Exception ex) {
            	logger.error("",ex);
            }
        }


		//在redis做session无法清除登录状态，因此需要手动去除session里的内容
		request.getSession().removeAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);


		//发送系统登出事件,只是发送一次即可,有特殊的业务需求请在
		EventHome.publishEvent(new LogoutEvent(new HttpWrapper(request, response, authentication)));

        super.logout(request, response, authentication);


    }

	public void setUserDetailService(UserDetailService userDetailService) {
		this.userDetailService = userDetailService;
	}

	public void setUserOnlineStatusHelper(UserOnlineStatusHelper userOnlineStatusHelper) {
		this.userOnlineStatusHelper = userOnlineStatusHelper;
	}
}
