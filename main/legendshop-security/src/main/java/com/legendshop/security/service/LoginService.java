/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

/**
 * 用户登录服务.
 */
public interface LoginService {

	/**
	 * 普通用户名密码登录
	 * @param username
	 *            the username, 如果不是userName， 则可能是昵称nickName， 手机mobile或者邮件email
	 * @param password
	 *            the password
	 * @return the authentication
	 */
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String username, String password);

	/**
	 * 用于集成其他第三方登陆方式,只要判断用户名即可
	 *
	 * @param isUserName
	 *            the is user name 是否传递了用户名密码
	 * @return the authentication
	 */
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String username, boolean isUserName);

	/**
	 * 第三方登陆,包括微信,QQ和微博登录.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param userName TODO
	 * @param openId
	 *            the open id
	 * @param accessToken
	 *            the access token
	 * @param type
	 *            the type
	 * @param source TODO
	 * @return the authentication
	 */
	public Authentication onAuthentication(HttpServletRequest request, HttpServletResponse response, String userName, String openId, String accessToken, String type, String source);

}
