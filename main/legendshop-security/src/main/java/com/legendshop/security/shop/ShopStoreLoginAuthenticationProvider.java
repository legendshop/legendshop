/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.security.shop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.StoreEntity;
import com.legendshop.security.AbstractUserDetailsAuthenticationProvider;
import com.legendshop.spi.service.security.AuthService;
import com.legendshop.util.Assert;

/**
 * 门店的登录.
 */
public class ShopStoreLoginAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	
	private final Logger log = LoggerFactory.getLogger(ShopStoreLoginAuthenticationProvider.class);
	
	/** The auth service. */
	private AuthService authService;

    @Autowired
    private PasswordEncoder passwordEncoder;

	public boolean supports(Class<?> authentication) {
		return (ShopStoreAuthenticationToken.class.isAssignableFrom(authentication));
	}

	protected void doAfterPropertiesSet() throws Exception {
		Assert.notNull(this.authService, "A UserDetailsService must be set");
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {

	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		ShopStoreAuthenticationToken result = new ShopStoreAuthenticationToken(principal, authentication.getCredentials(),
				authoritiesMapper.mapAuthorities(user.getAuthorities()), null);
		result.setDetails(authentication.getDetails());
		return result;
	}

	@Override
	protected UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
		UserDetails loadedUser = null;
		try {
			StoreEntity store = authService.loadStoreByName(username, authentication.getCredentials().toString());
			if (store != null) {
		        // 检查密码
		        if (!passwordEncoder.matches(authentication.getCredentials().toString(), store.getPassword())) {
		            log.debug("Authentication failed: password does not match stored value");
		            throw new BadCredentialsException("Bad credentials" + ", userName is: " + store.getUserName());
		        }
				
				loadedUser = createUser(store, LoginUserTypeEnum.STORE, LoginTypeEnum.USERNAME_PASSWORD, null, null, null);
			}
		} catch (UsernameNotFoundException notFound) {
			throw notFound;
		} catch (Exception repositoryProblem) {
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}

		if (loadedUser == null) {
			throw new AuthenticationServiceException("UserDetailsService returned null, which is an interface contract violation");
		}
		return loadedUser;
	}

	/**
	 * Sets the auth service.
	 *
	 * @param authService
	 *            the auth service
	 */
	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}

}
