/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.legendshop.core.handler.impl.NormalLegendHandler;

/**
 * Spring session和Spring security都应用这个proxy.
 */
public class DelegatingFilterProxy extends org.springframework.web.filter.DelegatingFilterProxy {


	/**
	 * 过滤器入口
	 *
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if(request.getAttribute(NormalLegendHandler.CONTROLLER_APPLIED) == null){
			//controller之外的请求不再用这个filter
			filterChain.doFilter(request, response);
		}else{
			super.doFilter(request,response, filterChain);
		}
	}



}
