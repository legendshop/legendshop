package com.legendshop.security.thirdparty;


import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;


/**
 * An {@link org.springframework.security.core.Authentication} implementation that is designed for simple presentation
 * of a username and password.
 * <p>
 * The <code>principal</code> and <code>credentials</code> should be set with an <code>Object</code> that provides
 * the respective property via its <code>Object.toString()</code> method. The simplest such <code>Object</code> to use
 * is <code>String</code>.
 *
 * @author Ben Alex
 */
public class ThirdPartyAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

   /** 用户名称 **/
    private final Object principal;
    
    /**  密码, 目前采用accessToken **/
    private Object credentials;
    
    /** openId */
    private String openId;
    
    private String accessToken;
    
    /** 第三方登陆类型 **/
    private String type;
    
    /** 登录来源, 哪个端 */
    private String source;

    /**
     * This constructor can be safely used by any code that wishes to create a
     * <code>UsernamePasswordAuthenticationToken</code>, as the {@link
     * #isAuthenticated()} will return <code>false</code>.
     * @param source TODO
     *
     */
    public ThirdPartyAuthenticationToken(Object principal, Object credentials,String openId, String accessToken, String type, String source) {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        this.openId = openId;
        this.accessToken = accessToken;
        this.type = type;
        this.source = source;
        setAuthenticated(false);
    }

    /**
     * This constructor should only be used by <code>AuthenticationManager</code> or <code>AuthenticationProvider</code>
     * implementations that are satisfied with producing a trusted (i.e. {@link #isAuthenticated()} = <code>true</code>)
     * authentication token.
     *
     * @param principal
     * @param credentials
     * @param authorities
     */
    public ThirdPartyAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        super.setAuthenticated(true); // must use super, as we override
    }

    public Object getCredentials() {
        return this.credentials;
    }

    public Object getPrincipal() {
        return this.principal;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }

        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        credentials = null;
    }


	public String getOpenId() {
		return openId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getType() {
		return type;
	}

	public String getSource() {
		return source;
	}
}