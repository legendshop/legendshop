/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.util.Assert;

/**
 * 用户权限
 *
 */
public final class GrantedAuthorityImpl implements GrantedAuthority, Comparable<GrantedAuthority> {

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	private String authority;
	
	/**
	 * The Constructor.
	 */
	public GrantedAuthorityImpl() {

	}

	public GrantedAuthorityImpl(String role) {
		Assert.hasText(role, "A granted authority textual representation is required");
		this.authority = role;
	}

	public String getAuthority() {
		return authority;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof GrantedAuthorityImpl) {
			return authority.equals(((GrantedAuthorityImpl) obj).authority);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return this.authority.hashCode();
	}

	@Override
	public String toString() {
		return this.authority;
	}

	public int compareTo(GrantedAuthority authority) {
		return authority.getAuthority().compareTo(this.authority);
	}
}
