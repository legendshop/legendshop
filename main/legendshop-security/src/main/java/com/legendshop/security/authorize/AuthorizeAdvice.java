/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.authorize;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;

/**
 * 权限验证.
 */
public class AuthorizeAdvice implements MethodInterceptor {
	
	/** The log. */
	final Log log = LogFactory.getLog(getClass());
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Class targetClazz = invocation.getMethod().getDeclaringClass();
		if(log.isInfoEnabled()){
			log.info("Enter method " + targetClazz.getName() + ":" +  invocation.getMethod().getName());
		}
		Authorize authorize = findAnnotation(invocation.getMethod(), targetClazz, Authorize.class);
		
		if(authorize == null){//如果没有设定则直接跳过
			System.out.println("authorize is null  ");
			return invocation.proceed();
		}else{
			//检查权限
			System.out.println("authorize = " + authorize.authorityTypes());
			return invocation.proceed();
		}
	}
	
	/**
	 * See
	 * {@link org.springframework.security.access.method.AbstractFallbackMethodSecurityMetadataSource#getAttributes(Method, Class)}
	 * for the logic of this method. The ordering here is slightly different in that we
	 * consider method-specific annotations on an interface before class-level ones.
	 */
	private <A extends Annotation> A findAnnotation(Method method, Class<?> targetClass,
			Class<A> annotationClass) {
		// The method may be on an interface, but we need attributes from the target
		// class.
		// If the target class is null, the method will be unchanged.
		Method specificMethod = ClassUtils.getMostSpecificMethod(method, targetClass);
		A annotation = AnnotationUtils.findAnnotation(specificMethod, annotationClass);

		if (annotation != null) {
			log.debug(annotation + " found on specific method: " + specificMethod);
			return annotation;
		}

		// Check the original (e.g. interface) method
		if (specificMethod != method) {
			annotation = AnnotationUtils.findAnnotation(method, annotationClass);

			if (annotation != null) {
				log.debug(annotation + " found on: " + method);
				return annotation;
			}
		}

		// Check the class-level (note declaringClass, not targetClass, which may not
		// actually implement the method)
		annotation = AnnotationUtils.findAnnotation(specificMethod.getDeclaringClass(),
				annotationClass);

		if (annotation != null) {
			log.debug(annotation + " found on: "
					+ specificMethod.getDeclaringClass().getName());
			return annotation;
		}

		return null;
	}

	

}