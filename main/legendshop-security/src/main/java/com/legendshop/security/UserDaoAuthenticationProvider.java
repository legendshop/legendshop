/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.spi.service.security.AuthService;

/**
 * 用户名密码登录验证器
 *
 */
public class UserDaoAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	
	private final Logger log = LoggerFactory.getLogger(UserDaoAuthenticationProvider.class);

	private AuthService authService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	protected void additionalAuthenticationChecks(UserDetails userDetails, Authentication authentication) throws AuthenticationException {

		if (authentication.getCredentials() == null) {
			logger.debug("Authentication failed: no credentials provided");

			throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials")
					+ ", userName is: " + userDetails.getUsername());
		}
	}

	protected void doAfterPropertiesSet() throws Exception {
		Assert.notNull(this.authService, "A AuthService must be set");
	}

	@Override
	protected final UserDetails retrieveUser(String username, Authentication authentication) throws AuthenticationException {
		UserDetails loadedUser = null;
		try {
			UserEntity userEntity = authService.loadUserByUsername(username, authentication.getCredentials().toString());
			if (userEntity != null) {
		        // 检查密码
		        boolean isValid = additionalAuthenticationChecks(userEntity, authentication.getCredentials().toString());
		        if (!isValid) {
					throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
		        }
				
				loadedUser = createUser(userEntity, LoginUserTypeEnum.USER, LoginTypeEnum.USERNAME_PASSWORD, null, null, null);
			}else{
				throw new UsernameNotFoundException("User not exists");
			}
		} catch (UsernameNotFoundException notFound) {
			throw notFound;
		} catch (Exception repositoryProblem) {
			throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
		}

		return loadedUser;
	}

	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}

	/**
	 * 支持的登录类型
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

	@Override
	protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(principal, authentication.getCredentials(),
				authoritiesMapper.mapAuthorities(user.getAuthorities()));
		result.setDetails(authentication.getDetails());

		return result;
	}
	
    /**
     * 检查用户名密码
     *
     * @param userDetails
     * @param 
     * @return
     * @throws AuthenticationException
     */
    private boolean additionalAuthenticationChecks(UserEntity userDetails, String presentedPassword) {
        return passwordEncoder.matches(presentedPassword, userDetails.getPassword());
    }


}
