/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.log;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.framework.handler.RequestHolder;
import com.legendshop.model.entity.UserEvent;
import com.legendshop.processor.UserEventProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.web.helper.IPHelper;

/**
 * 记录管理系统日志.
 */
@Aspect
@Component("logAspect")
public class LogAspect {

	/** 日志. */
	private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);
	
	@Autowired
	private UserEventProcessor userEventProcessor;

	//基本的数据类型
	private static String[] types = { "java.lang.Integer", "java.lang.Double", "java.lang.Float", "java.lang.Long", "java.lang.Short", "java.lang.Byte",
			"java.lang.Boolean", "java.lang.Char", "java.lang.String", "int", "double", "long", "short", "byte", "boolean", "char", "float" };

	/**
	 * Log point cut.
	 */
	@Pointcut("@annotation(com.legendshop.base.aop.SystemControllerLog)")
	public void logPointCut() {
	}

	/**
	 * Around.
	 *
	 * @param point
	 *            the point
	 * @return the object
	 * @throws Throwable
	 *             the throwable
	 */
	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		// 执行方法
		Object result = point.proceed();
		// 执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;
		// 异步保存日志
		saveLog(point, time);
		return result;
	}

	/**
	 * 保存日志.
	 *
	 * @param joinPoint
	 *            the join point
	 * @param time
	 *            the time
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	void saveLog(ProceedingJoinPoint joinPoint, long time) throws InterruptedException {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		UserEvent userEvent = new UserEvent();

		SystemControllerLog syslog = method.getAnnotation(SystemControllerLog.class);
		if (syslog == null) {
			return;
		}
		// 注解上的描述
		userEvent.setOperation(syslog.description());
		// 请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		userEvent.setMethod(className + "." + methodName + "()");

		// 请求的参数
		Object[] args = joinPoint.getArgs();
		try {

			StringBuilder sb = new StringBuilder();

			// 获取所有的参数
			for (int k = 0; k < args.length; k++) {
				Object arg = args[k];
				// 获取对象类型
				String typeName = arg.getClass().getTypeName();
				boolean isBasicType = false;
				for (String t : types) {
					// 1 判断是否是基础类型
					if (t.equals(typeName)) {
						isBasicType = true;
						sb.append(arg + ",");
					}
				}
				
				if(!isBasicType){
					// 2 通过反射获取实体类属性
					String value = getObjJson(arg);
					if(value != null){
						sb.append(value).append(",");
					}
				}

			}
			
			if(sb.length() > 0){
				sb.setLength(sb.length() - 1);//去掉最后一个逗号
				userEvent.setParams(sb.toString());
			}

			
		} catch (Exception e) {
			System.out.println(e);
		}


		HttpServletRequest request = RequestHolder.getRequest();
		if (request != null) {
			userEvent.setIp(IPHelper.getIpAddr(request));
			SecurityUserDetail currUser = UserManager.getUser(request);
			if (null != currUser) {
				userEvent.setUserId(currUser.getUserId());
				userEvent.setUserName(currUser.getUsername());
			}
		}
		userEvent.setTime(time);
		// 系统当前时间
		Date date = new Date();
		userEvent.setCreateTime(date);
		if (logger.isDebugEnabled()) {
			logger.debug("save user event {}", userEvent);
		}
		//保存到数据库
		userEventProcessor.process(userEvent);
	}

	// 解析实体类，获取实体类中的属性
	public static String getObjJson(Object obj) {
		if(obj instanceof ServletRequest || obj instanceof ServletResponse){
			return null;
		}
		return JSON.toJSONString(obj,new PropertyFilter(){
			@Override
			public boolean apply(Object object, String name, Object value) {
	                if(object instanceof InputStreamSource || object instanceof File){
	                	return false;//false表示字段将被排除在外  ,不记录文件类型的字段
	                }
				return true;
			}
			
		});
	}
	

}
