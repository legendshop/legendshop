package com.legendshop.security.menu;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.legendshop.model.entity.Menu;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 菜单管理者
 *
 */
public interface MenuManager {

	void init();

	List<Menu> getTopMenus(SecurityUserDetail userDetail,HttpSession session);

	public List<Menu> calUserMenus(SecurityUserDetail userDetail, HttpSession session);
	
	public Menu getParentMenu(Long menuId);
	
	public Menu getCurrentMenu(Long menuId);
}
