/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.config.PropertiesUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.RedirectStrategy;

import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.util.AppUtils;

/**
 * 登录成功之后的跳转策略
 */
public class OriginUrlRedirectStrategy implements RedirectStrategy {

	/** The logger. */
	protected final Log logger = LogFactory.getLog(getClass());

	/** The context relative. */
	private boolean contextRelative;

	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * Redirects the response to the supplied URL.
	 * <p>
	 * If <tt>contextRelative</tt> is set, the redirect value will be the value
	 * after the request context path. Note that this will result in the loss of
	 * protocol information (HTTP or HTTPS), so will cause problems if a
	 * redirect is being performed to change to HTTPS, for example.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param url
	 *            the url
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
		String service = request.getParameter(AttributeKeys.RETURN_URL);
		if (AppUtils.isNotBlank(service) && service.startsWith(propertiesUtil.getPcDomainName())) {//要检查一下域名，否则可能被导向到其他恶意网站而导致被攻击，用户被错误引导
			response.sendRedirect(service);
			return;
		}

		String redirectUrl = calculateRedirectUrl(request.getContextPath(), url);
		redirectUrl = response.encodeRedirectURL(redirectUrl);

		if (logger.isDebugEnabled()) {
			logger.debug("Redirecting to '" + redirectUrl + "'");
		}

		response.sendRedirect(redirectUrl);
	}

	/**
	 * Calculate redirect url.
	 * 
	 * @param contextPath
	 *            the context path
	 * @param url
	 *            the url
	 * @return the string
	 */
	private String calculateRedirectUrl(String contextPath, String url) {
		if (url != null && !url.startsWith("http://")) {
			if (contextRelative) {
				return url;
			} else {
				return contextPath + url;
			}
		}

		// Full URL, including http(s)://

		if (!contextRelative) {
			return url;
		}

		// Calculate the relative URL from the fully qualified URL, minus the
		// scheme and base context.
		url = url.substring(url.indexOf("://") + 3); // strip off scheme
		url = url.substring(url.indexOf(contextPath) + contextPath.length());

		if (url.length() > 1 && url.charAt(0) == '/') {
			url = url.substring(1);
		}

		return url;
	}

	/**
	 * If <tt>true</tt>, causes any redirection URLs to be calculated minus the
	 * protocol and context path (defaults to <tt>false</tt>).
	 * 
	 * @param useRelativeContext
	 *            the new context relative
	 */
	public void setContextRelative(boolean useRelativeContext) {
		this.contextRelative = useRelativeContext;
	}

}