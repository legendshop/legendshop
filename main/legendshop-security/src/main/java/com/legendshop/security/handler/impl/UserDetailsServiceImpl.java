/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.security.handler.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.framework.model.GrantedFunctionImpl;
import com.legendshop.model.constant.LoginTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.security.UserEntity;
import com.legendshop.security.model.SecurityUser;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopMenuGroupService;
import com.legendshop.spi.service.security.AuthService;
import com.legendshop.util.AppUtils;

public class UserDetailsServiceImpl implements UserDetailsService {
	
	private  AuthService authService;

	protected ShopMenuGroupService shopMenuGroupService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity =  authService.loadUserByUsername(username);
		if(userEntity != null){
			UserDetails loadedUser = createUser(userEntity, LoginUserTypeEnum.USER, LoginTypeEnum.USERNAME_PASSWORD, userEntity.getOpenId(), null, null);
			return loadedUser;
		}
		
		return null;
	}
	
    /**
     * 根据商家和用户转化为用户对象
     * @param user
     * @param appNo
     * @param loginUserType
     * @param loginType
     * @param opendId
     * @param accessToken
     * @param type
     * @return
     */
	private SecurityUser createUser(UserEntity user,LoginUserTypeEnum loginUserType, LoginTypeEnum loginType, String opendId, String accessToken, String type) {
		/*
		 * String userId,String username, String password, String portraitPic,
		 * String nickName,boolean enabled, boolean accountNonExpired, boolean
		 * credentialsNonExpired, boolean accountNonLocked,
		 * Collection<GrantedAuthority> authorities, Collection<GrantedFunction>
		 * functions, String openId, String accessToken, String type, ShopUser
		 * shopUser,Integer gradeId,Integer deptId, String logType, String
		 * loginUserType
		 */

		if(user.getShopUser() != null){
			user.getShopUser().setShopMenuGroupList(shopMenuGroupService.getShopMenuGroup());
		}
		
		SecurityUser minuser = new SecurityUserDetail(user.getId(), user.getName(), user.getPassword(), user.getPortraitPic(), user.getNickName(),
				getBoolean(user.getEnabled()), true, true, true, parseGrantedAuthority(user.getRoles()), parseFunction(user.getFunctions()), opendId, accessToken, type, user.getShopUser(), user.getGradeId(),
				user.getDeptId(), loginType.value(), loginUserType.value());
		return minuser;
	}
	
	
	/**
	 * Gets the boolean.
	 * 
	 * @param b
	 *            the b
	 * @return the boolean
	 */
	private boolean getBoolean(String b) {
		return "1".endsWith(b) ? true : false;
	}

	public void setAuthService(AuthService authService) {
		this.authService = authService;
	}
	
	public void setShopMenuGroupService(ShopMenuGroupService shopMenuGroupService) {
		this.shopMenuGroupService = shopMenuGroupService;
	}
	
    /**
     * 转化为spring security需要的字段
     * @param roles
     * @return
     */
    private Collection<GrantedAuthority> parseGrantedAuthority(Collection<String> roles){
    	if(AppUtils.isNotBlank(roles)) {
    		Collection<GrantedAuthority> roleList = new ArrayList<GrantedAuthority>();
    		for (String roleStr : roles) {
    			GrantedAuthority role = new SimpleGrantedAuthority(roleStr);
    			roleList.add(role);
    			return roleList;
			}
    	}
    	return null;
    }
    
    /**
     * 转化为spring security需要的字段
     * @param functions
     * @return
     */
    private List<GrantedFunction> parseFunction(Collection<String> functions){
    	if(AppUtils.isNotBlank(functions)) {
    		List<GrantedFunction> functionList = new ArrayList<GrantedFunction>();
    		for (String func : functions) {
    			GrantedFunction grantedFunction = new GrantedFunctionImpl(func);
    			functionList.add(grantedFunction);
			}
    		return functionList;
    	}
    	return null;
    }

}
