/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.sms;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import cn.emay.sdk.client.api.Client;

/**
 * @Description 亿美短信接口开发工具类,</br>
 *              用于获取发送短信的Client实例,</br>
 *              注册序列号,注册企业信息,注销序列号等.
 * @author 关开发
 */
public class EmaySMSUtil {
	private EmaySMSUtil() {
	}

	private static Logger log = LoggerFactory.getLogger(EmaySMSUtil.class);
	/*
	 * private static Client client = null;
	 * 
	 *//**
		 * 根据传入的softwareSerialNo和key , </br>
		 * 获取亿美短信接口的Client实例,</br>
		 * 该方法采用了单例模式
		 * 
		 * @param softwareSerialNo 软件序列号 , 通过亿美销售人员获取
		 * @param key              要注册的关键字
		 * @return 返回Client实例
		 */
	/*
	 * public synchronized static Client getClient(String softwareSerialNo,String
	 * key){ if(client==null){ try { client=new Client(softwareSerialNo,key); }
	 * catch (Exception e) { e.printStackTrace(); } } return client; }
	 * 
	 *//**
		 * 根据配置文件获取亿美短信接口的Client实例,</br>
		 * 配置文件在emayclient.jar/client.properties下,</br>
		 * 该方法采用了单例模式
		 * 
		 * @return 返回Client实例
		 */
	/*
	 * public synchronized static Client getClient(){ ResourceBundle
	 * bundle=PropertyResourceBundle.getBundle("config"); if(client == null){ try {
	 * client = new
	 * Client(bundle.getString("softwareSerialNo"),bundle.getString("key")); } catch
	 * (Exception e) { e.printStackTrace(); } } return client; }
	 * 
	 *//**
		 * 注册序列号,首次运行需要注册
		 * 
		 * @param password 序列号密码 , 通过亿美销售人员获取
		 */
	/*
	 * public static void serialNoRegist(Client client,String password){ try{ int
	 * code = client.registEx(password);
	 * 
	 * if(code != 0){ log.error("registEx()方法返回值:"+code);
	 * System.out.println("返回码: "+code); }
	 * 
	 * } catch(Exception e){ e.printStackTrace(); log.error("注册序列号出错!",e); } }
	 * 
	 *//**
		 * 注销序列号
		 */
	/*
	 * public static void logout(Client client){ try{ int code = client.logout();
	 * 
	 * if(code != 0){ log.error("logout()方法返回值:"+code); }
	 * 
	 * } catch(Exception e){ e.printStackTrace(); log.error("注销序列号出错!",e); } }
	 * 
	 *//**
		 * 注册企业信息
		 * 
		 * @param name     企业名字
		 * @param eName    企业名称(最多60字节)
		 * @param linkMan  联系人姓名(最多20字节)
		 * @param phoneNum 联系电话(最多20字节)
		 * @param mobile   联系手机(最多15字节)
		 * @param email    电子邮件(最多60字节)
		 * @param fax      联系传真(最多20字节)
		 * @param address  公司地址(最多60字节)
		 * @param postcode 邮政编码(最多6字节)
		 * @param          以上参数信息都必须填写、每个参数都不能为空
		 *//*
			 * public static void registDetailInfo(Client client, String name,String
			 * linkMan, String phoneNum,String mobile,String email, String fax,String
			 * address,String postcode){ try{ int code = client.registDetailInfo(name,
			 * linkMan, phoneNum, mobile, email, fax, address, postcode);
			 * 
			 * if(code != 0){ log.error("registDetailInfo()方法返回值:"+code); }
			 * 
			 * } catch(Exception e){ e.printStackTrace(); log.error("注册序列号出错!",e); } }
			 * 
			 * public static void main(String[] args) { //第一个参数是序列号,第二个参数是key Client client
			 * = EmaySMSUtil.getClient("0SDK-EBB-0130-JIUML", "123456"); //注销序列号
			 * EmaySMSUtil.logout(client); }
			 */
}
