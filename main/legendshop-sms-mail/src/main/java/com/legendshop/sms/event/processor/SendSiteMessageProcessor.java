package com.legendshop.sms.event.processor;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.entity.MsgText;
import com.legendshop.spi.service.MessageService;
import com.legendshop.util.AppUtils;
/**
 * 发送站内信 
 *
 */
@Component("sendSiteMessageProcessor")
public class SendSiteMessageProcessor implements EventProcessor<SiteMessageInfo> {
	private Logger log = LoggerFactory.getLogger(SendSiteMessageProcessor.class);

	@Autowired
	private MessageService messageService;

	/**
	 * 动作处理者
	 */
	@Override
	public void process(SiteMessageInfo msg) {
		if(!checkMsg(msg)){
			return;
		}
		if (log.isDebugEnabled()) {
			log.debug("Send site message from : {}, to : {}, text : {}", new Object[] {msg.getSendName(), msg.getReceiveName(),msg.getText() });
		}
		MsgText msgText = new MsgText();
		msgText.setText(msg.getText());
		msgText.setTitle(msg.getTitle());
		msgText.setRecDate(new Date());
		messageService.saveSiteInformation(msgText,msg.getSendName(),msg.getReceiveName());
	}

	/**
	 * 检查站内信载体
	 * @param msg
	 * @return
	 */
	private boolean checkMsg(SiteMessageInfo msg){
		boolean result = true;
		if(msg == null){
			result = false;
			log.error("SiteMessage entity can not be empty!");
		}else{
			if(AppUtils.isBlank(msg.getReceiveName())){
				result = false;
				log.error("Can not send site msg without receiveName");
			}else if(AppUtils.isBlank(msg.getTitle())){
				result = false;
				log.error("Can not send site msg without title");
			}else if(AppUtils.isBlank(msg.getText())){
				result = false;
				log.error("Can not send site msg without text");
			}
		}
		return result;
	}

}
