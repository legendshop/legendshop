package com.legendshop.sms.impl;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.sms.SMSSender;
import com.legendshop.sms.constants.ChineseWebResultEnum;
import com.legendshop.util.Assert;
/**
 * 中国网建短信服务
 *
 */
public class ChineseWebSMSSender implements SMSSender {
	private Logger log = LoggerFactory.getLogger(ChineseWebSMSSender.class);
	
	private final String uid;
	
	private final String key;
	
	//http://utf8.sms.webchinese.cn
	private final String smsSendURL;
	
	public ChineseWebSMSSender(JSONObject config){
		this.uid = config.getString("uid");
		this.key = config.getString("key");
		this.smsSendURL = config.getString("smsSendURL");
		Assert.notNull(uid);
		Assert.notNull(key);
		Assert.notNull(smsSendURL);
	}

	@Override
	public int sendSms(ShortMessageInfo sms) {
		int statusCode = 0;
		HttpClient client = new HttpClient();
		// 设置代理服务器地址和端口
		PostMethod post = new PostMethod(smsSendURL);
		post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf8");//在头文件中设置转码
		NameValuePair[] data ={
				new NameValuePair("Uid", uid),  //"本站用户名"
				new NameValuePair("Key", key), // "接口安全密码"
				new NameValuePair("smsMob",sms.getMobile()), //"手机号码"
				new NameValuePair("smsText",sms.getText())}; //"短信内容"
		post.setRequestBody(data);

		try {
			client.executeMethod(post);
			String result = post.getResponseBodyAsString();
			log.info("中国网建短信接口 返回内容：mobile={},result={}",sms.getMobile(),result);
			if(result != null){
				Integer ret = Integer.parseInt(result);
				if(ret <0){
					log.info("中国网建短信接口返回异常：{}",ChineseWebResultEnum.instanceOfValue(result));
					log.info("中国网建短信接口uid：{},  key: {}, smsSendURL :{} ",uid, key, smsSendURL);
				}
				
			}
			 statusCode = post.getStatusCode();
		} catch (Exception e) {
			log.error("send sms failed", e);
		} finally {
			// 释放连接
			post.releaseConnection();
		}
		return statusCode;
	}


}
