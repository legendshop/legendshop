/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.sms.impl;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
//import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.sms.SMSSender;
import com.legendshop.util.Assert;

/**
 * @Description 融通信短信接口
 * @author 关开发
 */
public class YunTongXunSMSSender implements SMSSender {
	
	private Logger log = LoggerFactory.getLogger(YunTongXunSMSSender.class);
	
	/**
	 * 账号名
	 */
	private final String uid;
	/**
	 * 账号令牌
	 */
	private final String key;
	/**
	 * 应用ID
	 */
	private final String appid;
	/**
	 * 服务器URL 
	 * 	测试环境: sandboxapp.cloopen.com
	 *  生产环境: app.cloopen.com
	 */
	private final String smsSendURL;
	/**
	 * 端口  8883
	 */
	private final String smsSendPort;
	
	/**
	 * 短信模板ID
	 */
	private final String templateId;
	
	public YunTongXunSMSSender(JSONObject config) {
		super();
		this.uid = config.getString("uid");
		this.key = config.getString("key");
		this.appid = config.getString("appid");
		this.smsSendURL = config.getString("smsSendURL");
		this.smsSendPort = config.getString("smsSendPort");
		this.templateId = config.getString("templateId");
		
		Assert.notNull(uid);
		Assert.notNull(key);
		Assert.notNull(appid);
		Assert.notNull(smsSendURL);
		Assert.notNull(templateId);
	}
	
	@Override
	public int sendSms(ShortMessageInfo sms) {
		
		//sms = ((YunTongXunShortMessageInfo) sms);
		int statusCode = 0;
		/*
		HashMap<String,Object> result = null;
		
		//融通信发送短信API
		CCPRestSmsSDK restAPI = new CCPRestSmsSDK();
		
		//初始化服务器URL和端口
		restAPI.init(smsSendURL, smsSendPort);
		//设置账号和账号令牌
		restAPI.setAccount(uid, key);
		//设置应用ID
		restAPI.setAppId(appid);
		
		try{
			//发送短信,relust接收发送结果
			result = restAPI.sendTemplateSMS(sms.getMobile(),//手机号
					templateId,//模板ID
					new String[]{sms.getMobileCode(),"5"});//替换模板占位符的字符串数组
			
			if("000000".equals(result.get("statusCode"))){//状态码为000000时代表发送成功
				statusCode = 200;
			}else{
				log.error("发送失败,状态码: "+result.get("statusCode"));
			}
			
		} catch(Exception e){
			log.error("send sms failed", e);
			e.printStackTrace();
		}
	*/
		return statusCode;
	}
}
