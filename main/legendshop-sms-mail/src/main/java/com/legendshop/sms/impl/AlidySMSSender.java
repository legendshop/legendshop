package com.legendshop.sms.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.sms.SMSSender;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Assert;
import com.legendshop.util.JSONUtil;

/**
 * 阿里云云通信短信服务
 * 阿里大于全新升级为“阿里云 · 云通信”，成为阿里云旗下品牌
 */
public class AlidySMSSender implements SMSSender {
    private Logger log = LoggerFactory.getLogger(AlidySMSSender.class);

    private final String uid;

    private final String key;
    
    private final String secret;

    private final String smsSendURL;

    private final Map<String,String> templateCodeList;
    
    private final String freeSignName;

//    private final String smsType;    
//    private final String templateCode;


    public AlidySMSSender(JSONObject config,Map<String,String> templateCodeList) {
        this.uid = config.getString("uid");
        this.key = config.getString("key");
        this.secret = config.getString("secret");
        this.smsSendURL = config.getString("smsSendURL");
        this.freeSignName = config.getString("freeSignName");
        
        this.templateCodeList = templateCodeList;
        
//        this.smsType = config.getString("type");        
//        this.templateCode = config.getString("templateCode");

        
        Assert.notNull(uid);
        Assert.notNull(key);
        Assert.notNull(smsSendURL);
        Assert.notNull(freeSignName);
//        Assert.notNull(smsType);

    }

    @Override
    public int sendSms(ShortMessageInfo sms) {
        int statusCode = 0;
        try {
            log.info("send sms by AlidySMS Sender------------------getMobileCode:{},getText:{},getSmsType:{}",sms.getMobileCode(),sms.getText(),sms.getSmsType());
            if (AppUtils.isNotBlank( sms.getText())) {
                log.info("send sms by AlidySMS Sender match Code");                

                System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
                System.setProperty("sun.net.client.defaultReadTimeout", "10000");
                //初始化ascClient需要的几个参数
                //短信API产品名称（短信产品名固定，无需修改）
                final String product = "Dysmsapi";////
                //短信API产品域名（接口地址固定，无需修改）
//                final String domain = "dysmsapi.aliyuncs.com";////
                final String domain = smsSendURL;////
                //替换成你的AK
                //你的accessKeyId,参考本文档步骤2
//                final String accessKeyId = "LTAIvLnIV9qDmC6n";////
                final String accessKeyId = key;////
                //你的accessKeySecret，参考本文档步骤2
//                final String accessKeySecret = "OScx8KvoNRHpR3OhrlDkQL0DejHf4i";////
                final String accessKeySecret = secret;////

				log.info("send sms by AlidySMS accessKey  {}", key);
				log.info("send sms by AlidySMS secret  {}", secret);


				//初始化ascClient,暂时不支持多region
                IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
                DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
                IAcsClient acsClient = new DefaultAcsClient(profile);
                //组装请求对象
                SendSmsRequest request = new SendSmsRequest();
                //使用post提交
                request.setMethod(MethodType.POST);
                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
                request.setPhoneNumbers(sms.getMobile());
                //必填:短信签名-可在短信控制台中找到
//                request.setSignName("阿里云短信测试专用");
                request.setSignName(freeSignName);
                
//              request.setTemplateParam("{\"code\":\"" + code + "\"}");
                request.setTemplateParam(sms.getTemplateParam());
                
                if(sms.getSmsType() == SMSTypeEnum.VAL) {                   
                    request.setTemplateCode(templateCodeList.get("IDENTIFYING CODE"));
                    log.info("send sms by AlidySMS Sender match Code group 1 {}", sms.getText());
                    //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                    //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
                }
                else if(sms.getSmsType()== SMSTypeEnum.ORDER){
                    request.setTemplateCode(templateCodeList.get("LADINGCODE"));
                    log.info("send sms by AlidySMS Sender ,LADINGCODE is {}", sms.getText());
                }else if(sms.getSmsType()==SMSTypeEnum.DELIVER){
                	request.setTemplateCode(templateCodeList.get("DELIVER_GOODS"));
                    log.info("send sms by AlidySMS Sender ,DELIVER_GOODS is {}", sms.getText());
                }else if(sms.getSmsType()==SMSTypeEnum.ARRIVAL){
                	request.setTemplateCode(templateCodeList.get("ARRIVAL_NOTICE_CODE"));
                    log.info("send sms by AlidySMS Sender ,ARRIVAL_NOTICE_CODE is {}", sms.getText());
                }
                
          
                //可选-上行短信扩展码(无特殊需求用户请忽略此字段)
                //request.setSmsUpExtendCode("90997");
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                request.setOutId("yourOutId");
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
                if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
                    //请求成功
                	log.info("Send sms to mobile {} success!!" , sms.getMobile());
                	statusCode = 200;
                }
                else{
                    log.error("send sms by AlidySMS Sender error , TemplateCode: {}, TemplateParam: {}, SignName : {}, msg :{}", 
                    		request.getTemplateCode(), request.getTemplateParam(), request.getSignName(), JSONUtil.getJson(sendSmsResponse));
                }
            }
            else{
                log.info("send sms by AlidySMS Sender no match Code");
            }
        } catch (Exception e) {
        	statusCode = 0;
            log.error("send sms failed", e);
        }
        
        return statusCode;
    }
}
