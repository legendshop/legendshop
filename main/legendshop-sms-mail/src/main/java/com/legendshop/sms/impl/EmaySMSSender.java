/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.sms.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.sms.EmaySMSUtil;
import com.legendshop.sms.SMSSender;
import com.legendshop.util.Assert;

//import cn.emay.sdk.client.api.Client;

/**
 * @Description 亿美短信接口
 * @author 关开发
 */
public class EmaySMSSender implements SMSSender {
	
	private Logger log = LoggerFactory.getLogger(EmaySMSSender.class);
	
	/**
	 * 序列号
	 */
	private final String uid;
	
	/**
	 * 注册序列号时与序列号绑定的key
	 */
	private final String key;
	
	/**
	 * 优先级 可选值1-5,值越大短信越优先发送
	 */
	private final int priority;
	
	public EmaySMSSender(JSONObject config){
		this.uid = config.getString("uid");
		this.key = config.getString("key");
		this.priority = config.getIntValue("priority");
		
		Assert.notNull(uid);
		Assert.notNull(key);
		Assert.notNull(priority);
	}
	
	@Override
	public int sendSms(ShortMessageInfo sms) {
		int statusCode = 0;
		/*
		try{
			Client client = EmaySMSUtil.getClient(uid,key);
			
			int returnCode = client.sendSMS(new String[]{sms.getMobile()}, sms.getText(), priority);
			
			if(returnCode == 0){//返回码是0代表发送成功
				statusCode = 200;
			}else{
				log.error("发送失败,状态码: " + returnCode);
			}
		} catch(Exception e){
			log.error("未知错误,发送失败 ");
		}
		*/
		return statusCode;
	}
}
