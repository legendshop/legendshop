package com.legendshop.sms;

import com.legendshop.base.event.ShortMessageInfo;

/**
 * 短信发送类
 *
 */
public interface SMSSender {
	/**
	 * 发送短信
	 * @param sms 短信载体对象
	 * @return 状态码 , 200 代表发送成功
	 */
	public int sendSms(ShortMessageInfo sms);

}