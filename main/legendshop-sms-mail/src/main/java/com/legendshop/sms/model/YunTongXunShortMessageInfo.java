///*
// * 
// * LegendShop 多用户商城系统
// * 
// *  版权所有,并保留所有权利。
// * 
// */
//package com.legendshop.sms.model;
//
//import com.legendshop.base.event.ShortMessageInfo;
//import com.legendshop.model.constant.SMSTypeEnum;
//
///**
// * @Description 
// * @author 关开发
// */
//public class YunTongXunShortMessageInfo extends ShortMessageInfo {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 688544114957205966L;
//
//	/**
//	 * 短信模板类型
//	 */
//	private String templateType;
//	
//	/**
//	 * 用于替换短信模板占位符的字符串数组
//	 */
//    private String[] contents;
//	/**
//	 * @param mobile
//	 * @param mobileCode
//	 * @param text
//	 * @param userName
//	 * @param smsType
//	 */
//	public YunTongXunShortMessageInfo(String mobile, String mobileCode, String text, String userName,
//			SMSTypeEnum smsType) {
//		super(mobile, mobileCode, text, userName, smsType);
//	}
//
//	/**
//	 * @param mobile
//	 * @param mobileCode
//	 * @param userName
//	 * @param smsType
//	 */
//	public YunTongXunShortMessageInfo(String mobile, String mobileCode, String userName, SMSTypeEnum smsType) {
//		super(mobile, mobileCode, userName, smsType);
//		
//	}
//
//	/**
//	 * @param mobile
//	 * @param mobileCode
//	 * @param text
//	 * @param userName
//	 * @param smsType
//	 * @param templateType 短信模板类型
//	 * @param contents 用于替换短信模板占位符的字符串数组
//	 */
//	public YunTongXunShortMessageInfo(String mobile, String mobileCode, String userName,
//			SMSTypeEnum smsType, String templateType, String[] contents) {
//		super(mobile, mobileCode, userName, smsType);
//		this.templateType = templateType;
//		this.contents = contents;
//	}
//
//	/**
//	 * @return the templateType
//	 */
//	public String getTemplateType() {
//		return templateType;
//	}
//
//	/**
//	 * @param templateType the templateType to set
//	 */
//	public void setTemplateType(String templateType) {
//		this.templateType = templateType;
//	}
//
//	/**
//	 * @return the contents
//	 */
//	public String[] getContents() {
//		return contents;
//	}
//
//	/**
//	 * @param contents the contents to set
//	 */
//	public void setContents(String[] contents) {
//		this.contents = contents;
//	}
//}
