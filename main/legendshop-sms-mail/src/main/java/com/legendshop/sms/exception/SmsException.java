/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.sms.exception;


/**
 * 发送短信异常.
 */
public class SmsException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8320257247028744302L;


	/**
	 * The Constructor.
	 *
	 * @param message the message
	 */
	public SmsException(String message) {
		super(message);
	}


	/**
	 * The Constructor.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public SmsException(String message, Throwable cause) {
		super(message, cause);
	}


}
