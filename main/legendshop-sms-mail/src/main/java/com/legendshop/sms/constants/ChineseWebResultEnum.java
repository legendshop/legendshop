package com.legendshop.sms.constants;

import com.legendshop.util.constant.StringEnum;

/**
 * 
 * 中国网建异常Enum
 * 
 */
public enum ChineseWebResultEnum implements StringEnum {

	NO_ACCOUNT("-1", "没有该用户账户"),
	PAWWORD_INCORRECT("-2","接口密钥不正确"),
	MD5_INCORRECT("-21", "MD5接口密钥加密不正确"),
	SMS_INSUFFICIENT("-3", "短信数量不足"),
	USER_FORBID("-11",  "该用户被禁用"),
	ILLEGAL_CHARACTER("-14","短信内容出现非法字符"),
	MOBILE_INCORRECT("-4","手机号格式不正确"),
	MOBILE_MISSING("-41","手机号码为空"),
	SMS_CONTENT_MISSING("-42","短信内容为空"),
	SIGN_INCORRECT("-51","短信签名格式不正确"),
	IP_LIMIT("-6","IP限制"),
	
	;

	private final String value;
	
	private final String desc;

	private ChineseWebResultEnum(String value,String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String value() {
		return this.value;
	}
	
	public String desc() {
		return this.desc;
	}

	@Override
	public String toString() {
		return this.name() + ":" + this.desc;
	}

	public static boolean instance(String name) {
		ChineseWebResultEnum[] businessModeEnums = values();
		for (ChineseWebResultEnum chineseWebResultEnum : businessModeEnums) {
			if (chineseWebResultEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public static ChineseWebResultEnum instanceOfValue(String value) {
		ChineseWebResultEnum[] businessModeEnums = values();
		for (ChineseWebResultEnum chineseWebResultEnum : businessModeEnums) {
			if (chineseWebResultEnum.value.equals(value)) {
				return chineseWebResultEnum;
			}
		}
		return null;
	}

	public static String getValue(String name) {
		ChineseWebResultEnum[] businessModeEnums = values();
		for (ChineseWebResultEnum chineseWebResultEnum : businessModeEnums) {
			if (chineseWebResultEnum.name().equals(name)) {
				return chineseWebResultEnum.value();
			}
		}
		return null;
	}
	

}
