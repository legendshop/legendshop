/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.sms.event.processor;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.MailInfo;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.constant.MailPortEnum;
import com.legendshop.model.constant.MailTypeEnum;
import com.legendshop.spi.service.MailEntityService;
import com.legendshop.util.AppUtils;
import com.sun.mail.util.MailSSLSocketFactory;

/**
 * 邮件发送器.
 */
@Component("sendMailProcessor")
public class SendMailProcessor implements EventProcessor<MailInfo> {

    /**
     * The log.
     */
    private Logger log = LoggerFactory.getLogger(SendMailProcessor.class);

    /**
     * The mail entity service.
     */
    @Autowired
    private MailEntityService mailEntityService;

    @Autowired
    private SystemParameterUtil systemParameterUtil;

    /**
     * 处理器
     */
    @Override
    @Async
    public void process(MailInfo task) {
        if (log.isDebugEnabled()) {
            log.debug("send mail to {} ,subject is {}", task.getTo(), task.getSubject());
        }
        if (AppUtils.isBlank(task.getTo())) {
            log.warn("Send mail failed because toAddr is null");
            return;
        }
        if (AppUtils.isBlank(task.getText())) {
            log.warn("Send mail failed because text is null");
            return;
        }
        if (AppUtils.isBlank(task.getSubject())) {
            log.warn("Send mail failed because subject is null");
            return;
        }
        try {
            send(task);

        } catch (Exception e) {
            log.error("send mail fail for ", e);
        }
    }

    @Async
    public String sendMailTest(String email) {
        try {
            mailEntityService = (MailEntityService) ContextServiceLocator.getBean("mailEntityService");
            MailInfo mailInfo = new MailInfo(email, "Email Testing", "这是一封来自测试的邮件，用于测试邮件服务器是否正常工作！谢谢！", "EmailTesting", null, MailCategoryEnum.SYSTEM.value(), MailTypeEnum.ONE_TO_ONE.value());
            send(mailInfo);
        } catch (Exception ex) {
            return String.format("邮件发送失败：%s", ex.getMessage());
        }
        return "邮件发送成功";
    }

    private void send(MailInfo task) {
        //1. get config from system
    	
        String host = systemParameterUtil.getMailHost();
        
        String auth = systemParameterUtil.getMailStmpAuth();
        String timeout =systemParameterUtil.getMailStmpTimeout();
        String mailName = systemParameterUtil.getMailName();
        Integer port = systemParameterUtil.getMailPort();
        String password = systemParameterUtil.getMailPassword();
        
        //2. config properties
        Properties properties = new Properties();
        
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.setProperty("mail.smtp.auth", auth);
        properties.setProperty("mail.smtp.timeout", timeout);
        
        if (port.equals(MailPortEnum.SAFETY.value()) ) {//使用安全协议端口 465
			
        	//指定基于SSL安全协议发送邮件    SSl安全协议指定的端口为465
            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
            properties.put("mail.transport.protocol", "smtp");
            properties.put("mail.smtp.socketFactory.class", SSL_FACTORY);
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.socketFactory.port", port);
            properties.put("mail.smtp.ssl.enable","true");
            
		}

        sendAttachMessage(task.getTo(), mailName, task.getSubject(), task.getText(), true, null, null, properties, mailName, password);

        //3. save to db
        mailEntityService.saveMail(task);
    }

    /**
     * 发送HTML信息并有附件的邮件.
     *
     * @param toAddr      the to addr
     * @param fromAddr    the from addr
     * @param subject     the subject
     * @param conText     the con text
     * @param isHtml      the is html
     * @param inLineImgs  the in line imgs
     * @param attachments the attachments
     * @param properties  the properties
     * @param username    the username
     * @param password    the password
     */
    private void sendAttachMessage(String toAddr, String fromAddr, String subject, String conText, boolean isHtml,
                                   List<File> inLineImgs, List<File> attachments, Properties properties, String username, String password) {

        try {
            JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
            senderImpl.setJavaMailProperties(properties);
            senderImpl.setUsername(username);
            senderImpl.setPassword(password);
            MimeMessage mailMessage = senderImpl.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");

            messageHelper.setTo(toAddr);
            messageHelper.setFrom(fromAddr);
            messageHelper.setSubject(subject);
            messageHelper.setText(conText, isHtml);

            // 附图
            if (AppUtils.isNotBlank(inLineImgs)) {
                for (File file : inLineImgs) {
                    messageHelper.addInline(MimeUtility.encodeWord(file.getName()), file);
                }
            }

            if (AppUtils.isNotBlank(attachments)) {
                // 附件内容
                for (File file : attachments) {
                    messageHelper.addAttachment(MimeUtility.encodeWord(file.getName()), file);
                }
            }
            senderImpl.send(mailMessage);
        } catch (Exception e) {
        	log.error("send mail",e);
            throw new RuntimeException(e);
        }
    }

}
