package com.legendshop.sms.event.processor;

import java.util.Calendar;
import java.util.Date;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.entity.SMSLog;
import com.legendshop.sms.SMSSenderProxy;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 短信发送处理者
 *
 */
@Component
public class SendSMSProcessor implements EventProcessor<ShortMessageInfo> {
	private Logger log = LoggerFactory.getLogger(SendSMSProcessor.class);
	
	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private SMSSenderProxy smsSenderProxy;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 动作处理器
	 */
	@Override
	@Async
	public void process(ShortMessageInfo sms) {
		//检查手机发送次数
		if(!checkSMSLimit(sms.getMobile())){
		   return ;	
		}
		//设置短信验证码
		//configValSMSText(sms);
		
		if (log.isInfoEnabled()) {
			log.info("Send [{}] to mobile : {}, text : {}", systemParameterUtil.sendSms(), sms.getMobile(), sms.getText());
		}
		
		if(SMSTypeEnum.VAL.equals(sms.getSmsType()) || SMSTypeEnum.ORDER.equals(sms.getSmsType())){
		  sms.setText(sms.getText().replace("${siteName}", systemParameterUtil.getSiteName()));
		}
		
		if(!checkSMS(sms)){//如果检查不通过就不发送短信
			return;
		}
		int responseCode = 200;
		if(systemParameterUtil.sendSms()){//如果是模拟发短信，则不发送短信
		     responseCode = smsSenderProxy.sendSms(sms);
		}
		//保存短信发送历史
		SMSLog smsLog = new SMSLog();
		smsLog.setContent(sms.getText());
		smsLog.setMobileCode(sms.getMobileCode());
		smsLog.setRecDate(new Date());
		smsLog.setType(sms.getSmsType().value());
		smsLog.setUserName(sms.getUserName() == null ? "":sms.getUserName());
		smsLog.setUserPhone(sms.getMobile());
		smsLog.setResponseCode(responseCode);
		smsLog.setStatus(1);
		smsLog.setIp(sms.getIp());
		smsLog.setRequestInterface(sms.getRequestInterface());
		smsLog.setTime(sms.getTime());
		smsLogService.saveSMSLog(smsLog);
		if (log.isDebugEnabled()) {
			log.debug("Send to mobile : {}, responseCode : {}", new Object[] {sms.getMobile(), responseCode});
		}
	}


	private boolean checkSMSLimit(String mobile) {
		Integer todayLimit = systemParameterUtil.getSmsTodayLimit();
		Integer betweenMin = systemParameterUtil.getSmsBetweenMin();
		Integer maxSendNum = systemParameterUtil.getSmsMaxSendNum();
		 //获取每个手机每天发送的限制数
		 if(todayLimit!=null&& todayLimit>0){
			 int count=smsLogService.getTodayLimit(mobile);
			 if(count>todayLimit){
				 log.error("SMS Over transmission limit by mobile {} ",mobile);
				 return false;
			 }
		 }
		 //检查时间段可以发送的条数
		 if((betweenMin!=null&& betweenMin>0) && (maxSendNum!=null && maxSendNum>0)){
			 Date oneHourAgo = DateUtil.add(new Date(), Calendar.MINUTE, -betweenMin);
			 boolean allowToSend=smsLogService.allowToSend(mobile, oneHourAgo, maxSendNum);
			 if(!allowToSend){
				 log.error("SMS Send limit over time by mobile {} ",mobile);
			 }
			 return allowToSend;
		 }
		return true;
	}


	/**
	 * 检查短信体
	 * @param sms
	 * @return
	 */
	private boolean checkSMS(ShortMessageInfo sms){
		boolean result = true;
		if(sms == null){
			result = false;
			log.error("SMS entity can not be empty!");
		}else{
			if(AppUtils.isBlank(sms.getMobile())){
				result = false;
				log.error("Can not send sms without mobile");
			}else if(AppUtils.isBlank(sms.getText())){
				result = false;
				log.error("Can not send sms without text");
			}
		}
		return result;
	}
}
