package com.legendshop.sms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.model.constant.SysParamGroupEnum;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.spi.service.SystemParameterService;
import com.legendshop.util.AppUtils;

/**
 * 短信发送代理者
 * 
 */
@Component
public class SMSSenderProxy implements InitializingBean, BeanFactoryAware {

	private Logger log = LoggerFactory.getLogger(SMSSenderProxy.class);

	private BeanFactory beanFactory;

	private List<SMSSender> senderList;
	// 不同的服务商轮流发送短信
	private int currentSender = 0;

	public int sendSms(ShortMessageInfo sms) {
		SMSSender sender = senderList.get(currentSender);
		int result = sender.sendSms(sms);
		if (result != 200) {// 发送不成功
			log.warn("Send to mobile {}  failed with status {}", sms.getMobile(), result);
			sender = senderList.get(nextSender());// 重新发送一次
			result = sender.sendSms(sms);
		}
		return result;
	}

	public List<SMSSender> getSenderList() {
		return senderList;
	}

	public void setSenderList(List<SMSSender> senderList) {
		this.senderList = senderList;
	}

	// 选择下一个短信提供商
	private synchronized int nextSender() {
		currentSender++;
		if (currentSender >= senderList.size()) {
			currentSender = 0;
		}
		return currentSender;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		init();
	}
	
	/**
	 * 短信模板初始化
	 */
	public void init() {
		SystemParameterService systemParameterService = beanFactory.getBean(SystemParameterService.class);

		if (systemParameterService == null) {
			throw new BusinessException("systemParameterService is missing");
		}

		List<SystemParameter> systemParameters = systemParameterService.getSystemParameterByGroupId(SysParamGroupEnum.SMS.value());

		//短信消息模板
		Map<String, String> templateCodeMap = new HashedMap();

		if (AppUtils.isBlank(systemParameters)) {
			throw new BusinessException("systemParameters is missing");
		}

		senderList = new ArrayList<SMSSender>();

		//加载短信模板, for 阿里云
		for (SystemParameter config : systemParameters) {
			if ("JSON".equals(config.getType())) {
				JSONObject configObject = JSONObject.parseObject(config.getValue());
				if (configObject.getString("type") == null && configObject.getString("templateCode") != null) {
					templateCodeMap.put(config.getName(), configObject.getString("templateCode"));
				}
			}
		}

		//加载 发送短信的方式,可以配置多个,目前只是配置了阿里云
		for (SystemParameter config : systemParameters) {
			if ("JSON".equals(config.getType())) {
				JSONObject configObject = JSONObject.parseObject(config.getValue());
				if (configObject.getString("type") != null) {
					SMSSenderFactory factory = new SMSSenderFactory(configObject, templateCodeMap);
					senderList.add(factory.createSender());
				}
			}
		}

	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;

	}

}
