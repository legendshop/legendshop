package com.legendshop.sms;

/**
 * 计算短信配置项目，在启用了短信之后必须要配置.
 */
public interface SMSConfig {
	
	/**
	 * 
	 * 获取短信的配置
	 *
	 */
	public String[] parseSmsConfig();

}
