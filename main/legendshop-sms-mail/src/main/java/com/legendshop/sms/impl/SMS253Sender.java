package com.legendshop.sms.impl;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.sms.SMSSender;
import com.legendshop.util.Assert;
/**
 * 253短信服务
 *
 */
public class SMS253Sender implements SMSSender {
	private Logger log = LoggerFactory.getLogger(SMS253Sender.class);
	
	private final String uid;
	
	private final String key;
	
	private final String smsSendURL;
	
	public SMS253Sender(JSONObject config){
		this.uid = config.getString("account");
		this.key = config.getString("pswd");
		this.smsSendURL = config.getString("smsSendURL");
		Assert.notNull(uid);
		Assert.notNull(key);
		Assert.notNull(smsSendURL);
	}

	@Override
	public int sendSms(ShortMessageInfo sms) {
		int statusCode = 0;
		HttpClient client = new HttpClient();
		// 设置代理服务器地址和端口
		PostMethod post = new PostMethod(smsSendURL);
		post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf8");//在头文件中设置转码
		NameValuePair[] data ={
				new NameValuePair("account", uid),  //"本站用户名"
				new NameValuePair("pswd", key), // "接口安全密码"
				new NameValuePair("mobile",sms.getMobile()), //"手机号码"
				new NameValuePair("msg",sms.getText()), //"短信内容"
				new NameValuePair("needstatus","true")};//是否需要状态报告
		post.setRequestBody(data);

		try {
			client.executeMethod(post);
			String result = post.getResponseBodyAsString();
			log.info("短信接口 返回内容：mobile={},result={}",sms.getMobile(),result);
			 statusCode = post.getStatusCode();
		} catch (Exception e) {
			log.error("send sms failed", e);
		} finally {
			// 释放连接
			post.releaseConnection();
		}
		return statusCode;
	}


}
