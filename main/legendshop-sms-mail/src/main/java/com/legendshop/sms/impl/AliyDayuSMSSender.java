package com.legendshop.sms.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.sms.SMSSender;
import com.legendshop.util.Assert;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * 阿里大于短信服务
 * 
 */
public class AliyDayuSMSSender implements SMSSender {
	private Logger log = LoggerFactory.getLogger(AliyDayuSMSSender.class);

	private final String uid;

	private final String key;

	private final String smsSendURL;

	private final String freeSignName;

	private final String smsType;

	public AliyDayuSMSSender(JSONObject config) {
		this.uid = config.getString("account");
		this.key = config.getString("pswd");
		this.smsSendURL = config.getString("smsSendURL");
		this.freeSignName = config.getString("freeSignName");
		this.smsType = config.getString("smsType");
		Assert.notNull(uid);
		Assert.notNull(key);
		Assert.notNull(smsSendURL);
		Assert.notNull(freeSignName);
		Assert.notNull(smsType);
	}

	@Override
	public int sendSms(ShortMessageInfo sms) {
		int statusCode = 0;
		try {
			TaobaoClient client = new DefaultTaobaoClient(smsSendURL, uid, key);
			AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
			Matcher match = Pattern.compile(
					"验证码为：(.*?), 有效时间是(.*?)分钟【(.*?)】,请勿告诉他人。").matcher(
					sms.getText());
			if (match.find()) {
				String code = match.group(1);
				String usefulMin = match.group(2);
				String siteName = match.group(3);
				req.setSmsType(smsType);
				req.setSmsFreeSignName(freeSignName);
				req.setSmsParamString("{\"code\":\"" + sms.getMobileCode()
						+ "\",\"usefulMin\":\"" + usefulMin
						+ "\",\"siteName\":\"" + siteName + "\"}");
				req.setRecNum(sms.getMobile());
				req.setSmsTemplateCode("SMS_60360415");
				AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
				
				 if (rsp.getBody().contains("error_response")) {// true
					   System.out.println("ali dayu 短信发送次数超出限制,请稍后再试。");
			      }
			}
		} catch (Exception e) {
			log.error("send sms failed", e);
			statusCode = 0;
		}
		statusCode = 1;
		return statusCode;

	}

}
