package com.legendshop.sms;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.sms.exception.SmsException;
import com.legendshop.sms.impl.AlidySMSSender;
import com.legendshop.sms.impl.AliyDayuSMSSender;
import com.legendshop.sms.impl.ChineseWebSMSSender;
import com.legendshop.sms.impl.EmaySMSSender;
import com.legendshop.sms.impl.HuyiSMSSender;
import com.legendshop.sms.impl.SMS253Sender;
import com.legendshop.sms.impl.YunTongXunSMSSender;

/**
 * 短信发送者工厂类
 *
 */
public class SMSSenderFactory {
	
	private final JSONObject config;
	private final Map<String,String> templateCodeMap;

	public SMSSenderFactory(JSONObject config,Map<String,String> templateCodeMap){
		this.config = config;
		this.templateCodeMap = templateCodeMap;
	}
	
	public SMSSender createSender() {
		String type = config.getString("type");
		SMSSender sender = null;
		if("ChineseWeb".equals(type)){
			sender = new ChineseWebSMSSender(config);
		}else if("Huyi".equals(type)){
			sender = new HuyiSMSSender(config);
		}else if("YunTongXun".equals(type)){//云通信
			sender = new YunTongXunSMSSender(config);
		}else if("Emay".equals(type)){//亿美
			sender = new EmaySMSSender(config);
		}else if("253".equals(type)){//253接口
			sender = new SMS253Sender(config);
		}else if("dayu".equals(type)){//阿里大于接口
			sender = new AliyDayuSMSSender(config);
		}else if("dysms".equals(type)){//阿里云通信接口
			sender = new AlidySMSSender(config,templateCodeMap);
		}else{
			throw new SmsException("Incorrect sms sender impletation: " + type);
		}
		return sender;
	}

}
