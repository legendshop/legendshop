/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 图片类型，是否是图片,还是视频
 */
public enum ImageTypeEnum implements IntegerEnum {
	/**
	 * 图片
	 */
	IMG(1),

	/**
	 * 视频
	 */
	VIDEO(0),

    /**
     * 文件
     */
    FILE(2);

	/** 类型. */
	private final Integer type;
	

	private ImageTypeEnum(Integer type) {
		this.type = type;
	}

	public static ImageTypeEnum fromCode(Integer type) {
		for (ImageTypeEnum enum1 : ImageTypeEnum.values()) {
			if (enum1.type.equals(type)) {
				return enum1;
			}
		}
		return null;
	}

	/**
	 * 根据文件路径来判断文件类型
	 * 
	 * @param filePath
	 * @return 类型
	 */
	public static ImageTypeEnum fromCode(String filePath) {
		if (filePath == null) {
			return null;
		}
		
		String fileSuffix = filePath.substring(filePath.lastIndexOf("."));
		
		if(fileSuffix == null){
			return null;
		}
		fileSuffix = fileSuffix.toLowerCase();
		
		if("jpg".equals(fileSuffix) || "gif".equals(fileSuffix) || "png".equals(fileSuffix) || "jpeg".equals(fileSuffix)   || "bmp".equals(fileSuffix)){//代码里面定下来可以上传那种类型
			return IMG;
		}else if("mp4".equals(fileSuffix) || "mpeg".equals(fileSuffix) || "ogg".equals(fileSuffix) || "webm".equals(fileSuffix)){
			return VIDEO;
		}

		return null;
	}
	
	public static void main(String[] args) {
		System.out.println();
	}

	public Integer type() {
		return this.type;
	}

	@Override
	public Integer value() {
		return type;
	}

}
