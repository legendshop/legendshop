/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;


import com.legendshop.util.constant.StringEnum;

/**
 * 图片用途
 * @author Tony
 *
 */
public enum ImagePurposeEnum implements StringEnum {
	
	/**  
	 * 用户文件[用户头像、用户身份文件]
	 **/
	USER_FILE("用户文件"),
	
	/**
	 * 商家入驻
	 */
	SHOP_REG_FILE("商家入驻"),
	
	
	/**
	 * 商家文件[装修、商品上传]
	 */
	SHOP_FILE("商家文件"),

	/**
	 * 后台文件
	 */
	ADMIN_FILE("后台文件")

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private ImagePurposeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
