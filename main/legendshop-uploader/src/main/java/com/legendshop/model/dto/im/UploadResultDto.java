/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.im;
import com.legendshop.util.AppUtils;

import java.io.Serializable;

/**
 * 文件上传结果.
 *
 */
public class UploadResultDto implements Serializable {

	private static final long serialVersionUID = -6401099270005845499L;

	/* 1:上传成功 ; -1:上传失败 ; 0:校验有误 */
	private int status;

	/* 消息 */
	private String message;

	/* 上传的文件路径 */
	private String[] paths;
	
	//附件Id
	private Long[] attachmentId;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String[] getPaths() {
		return paths;
	}
	
	/**
	 * 上传文件是否成功
	 * @return
	 */
	public boolean isSuccess() {
		return status == 1;
	}
	

	public void setPaths(String[] paths) {
		this.paths = paths;
	}

	public Long[] getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long[] attachmentId) {
		this.attachmentId = attachmentId;
	}
	
	/**
	 * 获取第一个文件路径
	 * @return
	 */
	public String getFirstPath() {
		if(AppUtils.isBlank(paths)) {
			return null;
		}
		return paths[0];
	}
	
	/**
	 * 获取第一个文件路径
	 * @return
	 */
	public Long getFirstAttachmentId() {
		if(AppUtils.isBlank(attachmentId)) {
			return null;
		}
		return attachmentId[0];
	}

}
