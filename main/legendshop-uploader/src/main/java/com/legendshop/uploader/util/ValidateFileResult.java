/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

package com.legendshop.uploader.util;

import java.io.InputStream;
import java.io.Serializable;

/**
 * 文件验证结果.
 */
public class ValidateFileResult implements Serializable{

	private static final long serialVersionUID = -3595526975052629143L;

	private boolean upFlag;//true:上传成功, false:上传失败
	
	private InputStream inputStream;
	
	private int fileSize; //文件大小
	
	public ValidateFileResult(boolean upFlag, InputStream inputStream) {
		this.upFlag = upFlag;
		this.inputStream = inputStream;
	}
	
	public ValidateFileResult(boolean upFlag, InputStream inputStream,int fileSize) {
		this.upFlag = upFlag;
		this.inputStream = inputStream;
		this.fileSize = fileSize;
	}

	public boolean isUpFlag() {
		return upFlag;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}



}
