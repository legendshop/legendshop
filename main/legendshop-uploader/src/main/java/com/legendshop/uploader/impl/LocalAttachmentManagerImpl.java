/**
 * 

 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.helper.FileProcessor;
import com.legendshop.base.model.InvokeRetObj;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.service.ProductSnapshotImgService;
import com.legendshop.uploader.util.ValidateFileProcessor;
import com.legendshop.util.AppUtils;

/**
 * 本地文件管理
 *
 */
public class LocalAttachmentManagerImpl extends CommonAttachmentManagerImpl{

	private String bigFilesAbsolutePath;

	private String snapshotBigFilesAbsolutePath;
	
	
	public LocalAttachmentManagerImpl(PropertiesUtil propertiesUtil, AttachmentService attachmentService, Map<String, List<Integer>> scaleList,
			ProductSnapshotImgService productSnapshotImgService) {
		super(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);
		bigFilesAbsolutePath = propertiesUtil.getBigPicPath();
		if (bigFilesAbsolutePath != null && !bigFilesAbsolutePath.endsWith("/")) {
			bigFilesAbsolutePath = bigFilesAbsolutePath + "/";
		}
		
		snapshotBigFilesAbsolutePath = propertiesUtil.getSnapshotBigPicPath();
		if (snapshotBigFilesAbsolutePath != null && snapshotBigFilesAbsolutePath.endsWith("/")) {
			snapshotBigFilesAbsolutePath = snapshotBigFilesAbsolutePath + "/";
		}
	}
	
	@Override
	public String upload(String imgUrl, String fileName) {
		InputStream is = null;
		String photo = null;
		try {
			
			//上传新的头像
			is = super.loadImageURL(imgUrl);
			
			photo = this.upload(is, fileName, is.available());
			
		} catch (IOException e) {
			throw new BusinessException(e, "file upload error: " + imgUrl);
		} finally{
			if(null != is){
				try {
					is.close();
				} catch (IOException e) {
					throw new BusinessException(e, "file upload error: " + imgUrl);
				}
			}
		}
		
		return photo;
	}

	/**
	 * 文件流形式 上传
	 */
	@Override
	public String upload(InputStream is, String fileName, long fileSize) {
		if (!ValidateFileProcessor.validateFile(fileName, fileSize)) {
			log.warn("file {} format error", fileName);
			return Constants.PHOTO_FORMAT_ERR;
		}
		String extName = FileProcessor.getFileExtName(fileName);
		String lastFileName = UUID.randomUUID().toString() + extName;
		return FileProcessor.uploadFile(is, lastFileName);
	}

	/**
	 * For Spring mvc文件上传
	 */
	@Override
	public String upload(MultipartFile file) {
		if (file == null) {
			log.warn("file is null");
			return Constants.PHOTO_FORMAT_ERR;
		}

		if (!ValidateFileProcessor.validateFile(file.getOriginalFilename(), file.getSize())) {
			log.warn("file {} format error", file.getOriginalFilename());
			return Constants.PHOTO_FORMAT_ERR;
		}
		// 保存图片到本地磁盘
		String result = FileProcessor.uploadFileAndCallback(file);
		if (AppUtils.isBlank(result)) {
			log.warn("上传文件失败 file name is {}", file.getName());
		}
		return result;
	}

	/**
	 * 真实删除图片
	 * 
	 * @param fileName
	 *            文件路径
	 * @return 0 成功 -1 删除失败 1 没有文件 2 其他异常
	 */
	@Override
	public int deleteTruely(String fileName) {
		if (AppUtils.isBlank(fileName)) {
			return 0;
		}
		// 删除大图
		InvokeRetObj result = FileProcessor.deleteFile(bigFilesAbsolutePath + fileName, false);
		if (result.isFlag()) {
			// 删除图片的缩略图
			for (String sacle : scaleList.keySet()) {
				StringBuilder sb = new StringBuilder(propertiesUtil.getSmallPicPath());
				sb.append("/").append(sacle).append("/").append(fileName);
				FileProcessor.deleteFile(sb.toString(), false);
			}
		} else {
			log.warn("delete file failed return code is {}, msg is {} ", result.getRetCode(), result.getMsg());
		}

		return result.getRetCode();
	}

	@Override
	public String uploadPrivateFile(InputStream is, String fileName) {
		return null;
	}

	@Override
	public String uploadPrivateFile(InputStream is, String fileName, long fileSize) {
		return upload(is,fileName,fileSize);
	}

	@Override
	public String uploadPrivateFile(MultipartFile file) {
		return upload(file);
	}

	@Override
	public String getPrivateObjectUrl(String key) {
		return null;
	}

	/**
	 * 为商品快照拷贝文件
	 * 
	 * @param filePath
	 *            文件路径
	 * @return true为正常返回, false为异常
	 */
	@Override
	public String copyFileForProdSnapshotImg(String filePath) {
		File srcFile = new File(bigFilesAbsolutePath + filePath);
		File destFile = new File(snapshotBigFilesAbsolutePath + filePath);
		File destPath = new File(snapshotBigFilesAbsolutePath + filePath.substring(0, filePath.lastIndexOf("/")));
		if (!destPath.exists()) {
			destPath.mkdirs();
		}
		boolean copyResult = FileProcessor.copyFile(srcFile, destFile);
		if (copyResult) {
			return filePath;// 成功拷贝
		} else {
			return null;
		}
	}
}
