/**
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.uploader.impl;

import com.aliyun.oss.model.CopyObjectResult;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.helper.FileProcessor;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.oss.MockOSSPropcessorImpl;
import com.legendshop.oss.OSSProcessorImpl;
import com.legendshop.oss.OSSPropcessor;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.service.ProductSnapshotImgService;
import com.legendshop.uploader.util.ValidateFileProcessor;
import com.legendshop.util.AppUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * OSS远程文件上传
 *
 */
public class OSSAttachmentManagerImpl extends CommonAttachmentManagerImpl {

	//oss处理实例
	private OSSPropcessor ossPropcessor;

	public OSSAttachmentManagerImpl(PropertiesUtil propertiesUtil, AttachmentService attachmentService, Map<String, List<Integer>> scaleList,
			ProductSnapshotImgService productSnapshotImgService) {
		super(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);

		String attachmentType = propertiesUtil.getAttachmentType();

		// 参见PhotoPathEnum的类型
		if("OSS".equals(attachmentType)){
			String endpoint = propertiesUtil.getEndpoint();
			String accessKeyId = propertiesUtil.getAccessKeyId();
			String accessKeySecret = propertiesUtil.getAccessKeySecret();
			String bucketName = propertiesUtil.getBucketName();
			String bucketNamePrivate = propertiesUtil.getBucketNamePrivate();
			ossPropcessor = new OSSProcessorImpl(endpoint, accessKeyId,  accessKeySecret, bucketName,bucketNamePrivate);
		// 不启用oss
		}else{
			ossPropcessor = new MockOSSPropcessorImpl();
		}


	}

	@Override
	public String upload(String imgUrl, String fileName) {
		InputStream is = null;
		String photo = null;
		try {

			//上传新的头像
			is = super.loadImageURL(imgUrl);

			photo = this.upload(is, fileName, is.available());

		} catch (IOException e) {
			throw new BusinessException(e, "file upload error: " + imgUrl);
		} finally{
			if(null != is){
				try {
					is.close();
				} catch (IOException e) {
					throw new BusinessException(e, "file upload error: " + imgUrl);
				}
			}
		}

		return photo;
	}

	/**
	 * 文件流形式 上传
	 * @return 返回文件名字
	 */
	@Override
	public String upload(InputStream is, String fileName, long fileSize) {
		// 使用该方法的太多了，如果直接抛异常而其它方法没有捕获，就会有一系列的问题
//		if (!ValidateFileProcessor.validateFile(fileName, fileSize)) {
//			throw new BusinessException("file format error: " + fileName);
//		}
		// 通过文件后名称获取后缀
		String fileExtName = FileProcessor.getFileExtName(fileName);
		// 微信照片上传后，文件名称没有后缀
		if(StringUtils.isBlank(fileExtName)){
			// 读取文件的前几个字节来判断图片格式
			try{
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len;
				while ((len = is.read(buffer)) > -1 ) {
					baos.write(buffer, 0, len);
				}
				baos.flush();
				// 拷贝流
				is = new ByteArrayInputStream(baos.toByteArray());
				InputStream inputStream1 = new ByteArrayInputStream(baos.toByteArray());
				fileExtName = ValidateFileProcessor.getFileExtName(inputStream1);
				if(StringUtils.isNotBlank(fileExtName)){
					fileName = fileName + fileExtName;
				}else{
					throw new RuntimeException("get file ext name error!");
				}
			}catch (Exception e){
				log.error("{}",e);
				log.error("file {} format error", fileName);
				return Constants.PHOTO_FORMAT_ERR;
			}
		}


		if (!ValidateFileProcessor.validateFile(fileName, fileSize)) {
			log.warn("file {} format error", fileName);
			return Constants.PHOTO_FORMAT_ERR;
		}

		String extName = FileProcessor.getFileExtName(fileName);
		String lastFileName = UUID.randomUUID().toString() + extName;

		String subPathAndFileName = getSubPath() + lastFileName;
		try {
			ossPropcessor.putObjectFile(subPathAndFileName, is, is.available());
		} catch (IOException e) {
			subPathAndFileName = null;// 上传到oss失败
			log.error("upload oss error", e);
		}

		return subPathAndFileName;
	}

	/**
	 * For Spring mvc文件上传
	 */
	@Override
	public String upload(MultipartFile file) {
		if (file == null) {
			log.warn("file is null");
			return Constants.PHOTO_FORMAT_ERR;
		}

		String fileName = file.getOriginalFilename();
		try {
			return upload(file.getInputStream(), fileName, file.getSize());
		} catch (IOException e) {
			log.error("", e);
			return Constants.PHOTO_FORMAT_ERR;
		}
	}

	/**
	 * 真实删除图片
	 *
	 * @param fileName
	 *            文件路径
	 * @return 0： 删除成功， -1：文件存在，但删除失败 1： 没有文件 2： 未知原因失败
	 */
	@Override
	public int deleteTruely(String fileName) {
		if (AppUtils.isBlank(fileName)) {
			return 0;
		}
		ossPropcessor.deleteObjectFile(fileName);
		return 0;// 由于oss没有返回状态码,因此都是删除成功的状态
	}

	@Override
	public String uploadPrivateFile(InputStream is, String fileName) {

		String extName = FileProcessor.getFileExtName(fileName);
		String lastFileName = UUID.randomUUID().toString() + extName;

		String subPathAndFileName = getSubPath() + lastFileName;
		try {
			ossPropcessor.putPrivateObjectFile(subPathAndFileName, is, is.available());
		} catch (IOException e) {

			// 上传到oss失败
			subPathAndFileName = null;
			log.error("upload oss error", e);
		}

		return subPathAndFileName;
	}

	@Override
	public String uploadPrivateFile(InputStream is, String fileName, long fileSize) {
		if (!ValidateFileProcessor.validateFile(fileName, fileSize)) {
			log.warn("file {} format error", fileName);
			return Constants.PHOTO_FORMAT_ERR;
		}

		String extName = FileProcessor.getFileExtName(fileName);
		String lastFileName = UUID.randomUUID().toString() + extName;

		String subPathAndFileName = getSubPath() + lastFileName;
		try {
			ossPropcessor.putPrivateObjectFile(subPathAndFileName, is, is.available());
		} catch (IOException e) {

			// 上传到oss失败
			subPathAndFileName = null;
			log.error("upload oss error", e);
		}

		return subPathAndFileName;
	}

	@Override
	public String uploadPrivateFile(MultipartFile file) {
		if (file == null) {
			log.warn("file is null");
			return Constants.PHOTO_FORMAT_ERR;
		}

		String fileName = file.getOriginalFilename();
		try {
			return uploadPrivateFile(file.getInputStream(), fileName, file.getSize());
		} catch (IOException e) {
			log.error("", e);
			return Constants.PHOTO_FORMAT_ERR;
		}
	}

	@Override
	public String getPrivateObjectUrl(String key) {
		return ossPropcessor.getPrivateObjectUrl(key);
	}

	/**
	 * 做文件拷贝,从商品的图片拷贝到商品快照去
	 *
	 * @param filePath
	 */
	@Override
	public String copyFileForProdSnapshotImg(String filePath) {
		CopyObjectResult ret = ossPropcessor.copyObjectFile(filePath);
		if (ret != null) {// 拷贝成功,由于OSS
			return "snapshot/" + filePath;
		} else {
			log.warn("copy file failed ");
			return null;
		}
	}

	private String getSubPath() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/");
		String ymd = sdf.format(new Date());
		return ymd;
	}


}
