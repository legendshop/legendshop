/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.model;

import java.io.Serializable;
import java.util.List;

/**
 * 附件类型.
 */
public class AttachmentDefinition implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7516843118264374866L;

	/** The type. */
	private final Integer type;
	
	/** 允许上传的文件大小. */
	private  final Long maxUploadSize;
	
	/** 允许上传的文件类型. */
	private final  List<String> allowUploadType;
	
	/**
	 * Instantiates a new attachment definition.
	 *
	 * @param type the type
	 * @param maxUploadSize the max upload size
	 * @param allowUploadType the allow upload type
	 */
	public AttachmentDefinition(Integer type, Long maxUploadSize, List<String> allowUploadType) {
		this.type = type;
		this.maxUploadSize = maxUploadSize;
		this.allowUploadType = allowUploadType;
	}

	public Integer getType() {
		return type;
	}

	public Long getMaxUploadSize() {
		return maxUploadSize;
	}

	public List<String> getAllowUploadType() {
		return allowUploadType;
	}

}
