/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * 基础的配置
 *
 */
@Configuration
public class BaseConfiguration {
	
	/**
	 * 图片规格定义
	 * 
	 */
	@Bean("scaleList")
	public Map<String, List<Integer>> scaleList(){
		Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
		
		List<Integer> scale0 = new ArrayList<Integer>();
		scale0.add(350);
		scale0.add(350);
		map.put("0", scale0);
		
		List<Integer> scale1 = new ArrayList<Integer>();
		scale1.add(180);
		scale1.add(180);
		map.put("1", scale1);
		
		List<Integer> scale2 = new ArrayList<Integer>();
		scale2.add(100);
		scale2.add(100);
		map.put("2", scale2);
		
		List<Integer> scale3 = new ArrayList<Integer>();
		scale3.add(65);
		scale3.add(65);
		map.put("3", scale3);
		
		List<Integer> scale4 = new ArrayList<Integer>();
		scale4.add(450);
		scale4.add(450);
		map.put("4", scale4);
		
		List<Integer> scale5 = new ArrayList<Integer>();
		scale5.add(230);
		scale5.add(230);
		map.put("5", scale5);
		
		List<Integer> scale6 = new ArrayList<Integer>();
		scale6.add(1080);
		scale6.add(1080);
		map.put("6", scale6);
		
		return map;
	}

}
