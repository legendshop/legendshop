/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.model;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *  spring boot 文件上传
 */
public class FeignMultipartFile implements MultipartFile{
	
	/**
	 *   文件名字
	 */
	private final String name;

	/**
	 * 文件原名
	 */
	private String originalFilename;

	/**
	 * 内容类型
	 */
	private String contentType;

	/**
	 *  字节内容
	 */
	private final byte[] content;


	/**
	 * Create a new MockMultipartFile with the given content.
	 * @param name the name of the file
	 * @param content the content of the file
	 */
	public FeignMultipartFile(String originalFilename, byte[] content) {
		this("", originalFilename, "multipart/form-data", content);
	}

	/**
	 * Create a new MockMultipartFile with the given content.
	 * @param name the name of the file
	 * @param contentStream the content of the file as stream
	 * @throws IOException if reading from the stream failed
	 */
	public FeignMultipartFile(String originalFilename, InputStream contentStream) throws IOException {
		this("", originalFilename, "multipart/form-data", FileCopyUtils.copyToByteArray(contentStream));
	}

	
	/**
	 * Create a new MockMultipartFile with the given content.
	 *
	 * @param name the name of the file
	 * @param originalFilename 
	 * @param contentStream the content of the file as stream
	 * @throws IOException if reading from the stream failed
	 */
	public FeignMultipartFile(String name,  String originalFilename, InputStream contentStream) throws IOException {
		this(name, originalFilename, "multipart/form-data", FileCopyUtils.copyToByteArray(contentStream));
	}
	
	/**
	 * Create a new MockMultipartFile with the given content.
	 *
	 * @param name the name of the file
	 * @param originalFilename 
	 * @param content 
	 * @throws IOException if reading from the stream failed
	 */
	public FeignMultipartFile(String name,  String originalFilename, byte[] content) throws IOException {
		this(name, originalFilename, "multipart/form-data", content);
	}
	
	/**
	 * Create a new MockMultipartFile with the given content.
	 * @param name the name of the file
	 * @param originalFilename the original filename (as on the client's machine)
	 * @param contentType the content type (if known)
	 * @param content the content of the file
	 */
	public FeignMultipartFile(String name, String originalFilename, String contentType, byte[] content) {

		this.name = name;
		this.originalFilename = (originalFilename != null ? originalFilename : "");
		this.contentType = contentType;
		this.content = (content != null ? content : new byte[0]);
	}
	

	/**
	 * Create a new MockMultipartFile with the given content.
	 * @param name the name of the file
	 * @param originalFilename the original filename (as on the client's machine)
	 * @param contentType the content type (if known)
	 * @param contentStream the content of the file as stream
	 * @throws IOException if reading from the stream failed
	 */
	public FeignMultipartFile(String name, String originalFilename, String contentType, InputStream contentStream)
			throws IOException {
		this(name, originalFilename, contentType, FileCopyUtils.copyToByteArray(contentStream));
	}

	/**
	 * 
	 *
	 * @return 
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 *
	 * @return 
	 */
	@Override
	public String getOriginalFilename() {
		return this.originalFilename;
	}

	/**
	 * 
	 *
	 * @return 
	 */
	@Override
	public String getContentType() {
		return this.contentType;
	}

	/**
	 * 
	 *
	 * @return 
	 */
	@Override
	public boolean isEmpty() {
		return (this.content.length == 0);
	}

	/**
	 * 
	 *
	 * @return 
	 */
	@Override
	public long getSize() {
		return this.content.length;
	}

	/**
	 * 
	 *
	 * @return 
	 * @throws IOException 
	 */
	@Override
	public byte[] getBytes() throws IOException {
		return this.content;
	}

	/**
	 * 
	 *
	 * @return 
	 * @throws IOException 
	 */
	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(this.content);
	}

	/**
	 * 
	 *
	 * @param dest 
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {
		FileCopyUtils.copy(this.content, dest);
	}

}
