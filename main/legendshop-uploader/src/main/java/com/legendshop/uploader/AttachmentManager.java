/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader;

import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.ImagePurposeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.dto.im.UploadResultDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * 附件上传文件
 *
 */
public interface AttachmentManager {
	
	/**
	 * 上传图片,供微信那边使用, 讲一个外部的图片拷贝进来系统内部
	 * @param imgUrl 图片url
	 */
	public String upload(String imgUrl, String fileName);
	
	/**
	 * 上传图片
	 * @param file springMVC文件
	 * @return
	 */
	public String upload(MultipartFile file);
	
	/**
	 * 逻辑删除文件,修改附件表的状态
	 * @param fileName 文件路径,包括文件名
	 */
	public void deleteLogical(String fileName);
	
	/**
	 * 真实删除,从磁盘删除
	 * @param fileName 文件路径,包括文件名
	 * @return
	 */
	public int deleteTruely(String fileName);

	/**
	 * 保存附件表
	 * @param userName 用户名
	 * @param userId 用户id
	 * @param shopId 店铺id
	 * @param filePath 图片路径
	 * @param file 文件
	 * @param type 图片类型
	 * @param imgType  是否是图片,还是视频
	 * @return
	 */
	public Long saveAttachment(String userName, String userId, Long shopId, String filePath, MultipartFile file,AttachmentTypeEnum type, ImageTypeEnum imgType);
	
	/**
	 * 保存附件表,默认是图片
	 * @param userName 用户名
	 * @param filePath 文件路径
	 * @param file 
	 * @param type 附件类型
	 * @return
	 */
	public Long saveImageAttachment(String userName, String userId, Long shopId, String filePath, MultipartFile file, AttachmentTypeEnum type);
	
	/**
	 * 保存附件表
	 * @param userName 用户名
	 * @param filePath 文件路径
	 * @param file 
	 * @param type 附件类型
	 * @param treeId 节点id
	 * @return
	 */
	public Long saveAttachment(String userName, String userId, Long shopId, String filePath, MultipartFile file, AttachmentTypeEnum type, long treeId, ImageTypeEnum imgType);

	/**
	 * 删除附件表记录
	 * @param filePath
	 */
	public void delAttachmentByFilePath(String filePath);
	
	/**
	 * 保存商品快照图片
	 */
	public String saveProdSnapshotImg(String filePath,Long snapshotId);
	
	
	
	///////////////////////////////////////////////////// for IM system /////////////////////////////////////////////////
	

	/**
	 * 上传图片
	 * @return
	 */
	public UploadResultDto uploadImage(ImagePurposeEnum purposeEnum, String userId, String userName, Long shopId, MultipartFile file);


	/**
	 * 上传私有文件
	 * @param is 文件流
	 * @param fileName 文件名
	 * @return
	 */
	public String uploadPrivateFile(InputStream is, String fileName);

	/**
	 * 上传私有文件
	 * @param is 文件流
	 * @param fileName 文件名
	 * @param fileSize 文件大小
	 * @return
	 */
	public String uploadPrivateFile(InputStream is,String fileName,long fileSize);

	/**
	 * 上传私有文件
	 * @param file
	 * @return
	 */
	public String uploadPrivateFile(MultipartFile file);


	/**
	 * 获取私有文件的Url
	 * @param key
	 * @return
	 */
	public String getPrivateObjectUrl(String key);
	
}
