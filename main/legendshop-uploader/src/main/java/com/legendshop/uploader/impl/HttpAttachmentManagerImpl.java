/**
 * LegendShop 多用户商城系统
 * <p>
 * 版权所有,并保留所有权利。
 */
package com.legendshop.uploader.impl;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.model.InvokeRetObj;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ImagePurposeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.dto.im.UploadResultDto;
import com.legendshop.model.entity.Attachment;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.service.ProductSnapshotImgService;
import com.legendshop.uploader.util.ValidateFileProcessor;

/**
 * 远程文件管理
 *
 */
public class HttpAttachmentManagerImpl extends CommonAttachmentManagerImpl {

    private static final Logger log = LoggerFactory.getLogger(HttpAttachmentManagerImpl.class);

    private String systemToken;// 系统参数

    public HttpAttachmentManagerImpl(PropertiesUtil propertiesUtil, AttachmentService attachmentService, Map<String, List<Integer>> scaleList,
                                     ProductSnapshotImgService productSnapshotImgService) {
        super(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);
        systemToken = propertiesUtil.getSystemToken();
        if (AppUtils.isBlank(systemToken)) {
            systemToken = "legendshop";// 默认采用legendshop作为加密因子
        }
    }


    @Override
    public String upload(String imgUrl, String fileName) {

        InputStream is = null;
        String photo = null;
        try {

            //上传新的头像
            is = super.loadImageURL(imgUrl);

            photo = this.upload(is, fileName, is.available());

        } catch (IOException e) {
            throw new BusinessException(e, "file upload error: " + imgUrl);
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    throw new BusinessException(e, "file upload error: " + imgUrl);
                }
            }
        }

        return photo;
    }

    /**
     * 文件流形式 上传,目前只是支持一个文件上传
     */
    @Override
    public String upload(InputStream is, String fileName, long fileSize) {
        if (!ValidateFileProcessor.validateFile(fileName, fileSize)) {
            throw new BusinessException("file format error: " + fileName);
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            // InputStream stream1 = new ByteArrayInputStream(baos.toByteArray());
            is = new ByteArrayInputStream(baos.toByteArray());

            /* 在IOS设备上图片转向
            Metadata metadata = null;
            metadata = JpegMetadataReader.readMetadata(stream1);
            Directory directory = metadata.getFirstDirectoryOfType(ExifDirectoryBase.class);
            int orientation = 0;
            if (directory != null && directory.containsTag(ExifDirectoryBase.TAG_ORIENTATION)) { // Exif信息中有保存方向,把信息复制到缩略图
                orientation = directory.getInt(ExifDirectoryBase.TAG_ORIENTATION); // 原图片的方向信息
                System.out.println(orientation);
            }
            
            if (orientation == 6) {
                BufferedImage image = ImageIO.read(is);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                BufferedImage newImage = Rotate(image, 90);
                if (!ImageIO.write(newImage, "jpg", os)) {
                    throw new IllegalArgumentException(String.format("not found writer for '%s'", "jpg"));
                }
                is = new ByteArrayInputStream(os.toByteArray());  
            }
             */
            // } catch (IOException |JpegProcessingException|MetadataException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }


        Map<String, InputStream> insMap = new HashMap<String, InputStream>();
        insMap.put(fileName, is);
        String ret = formUpload(propertiesUtil.getInnerPhotoServer() + "/upload", fileName, insMap);
        Object result = getObjectFromResult(fileName, ret); // 上传的功能是支持多个文件上传的,但是这里只是支持一个
        if (AppUtils.isNotBlank(result)) {
            List<String> pathList = (List<String>) result;
            return pathList.get(0);
        }
        return Constants.PHOTO_FORMAT_ERR;
    }

    /**
     * For Spring mvc文件上传
     */
    @Override
    public String upload(MultipartFile file) {
        if (file == null) {
            throw new BusinessException("file is null");
        }
        String fileName = file.getOriginalFilename();
        try {
            return upload(file.getInputStream(), fileName, file.getSize());
        } catch (IOException e) {
            throw new BusinessException(e, "file format error: " + fileName);
        }
    }


    public static BufferedImage Rotate(Image src, int angel) {
        int src_width = src.getWidth(null);
        int src_height = src.getHeight(null);
        // calculate the new image size
        Rectangle rect_des = CalcRotatedSize(new Rectangle(new Dimension(src_width, src_height)), angel);

        BufferedImage res = null;
        res = new BufferedImage(rect_des.width, rect_des.height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = res.createGraphics();
        // transform
        g2.translate((rect_des.width - src_width) / 2,
                (rect_des.height - src_height) / 2);
        g2.rotate(Math.toRadians(angel), src_width / 2, src_height / 2);

        g2.drawImage(src, null, null);
        return res;
    }

    public static Rectangle CalcRotatedSize(Rectangle src, int angel) {
        // if angel is greater than 90 degree, we need to do some conversion
        if (angel >= 90) {
            if (angel / 90 % 2 == 1) {
                int temp = src.height;
                src.height = src.width;
                src.width = temp;
            }
            angel = angel % 90;
        }

        double r = Math.sqrt(src.height * src.height + src.width * src.width) / 2;
        double len = 2 * Math.sin(Math.toRadians(angel) / 2) * r;
        double angel_alpha = (Math.PI - Math.toRadians(angel)) / 2;
        double angel_dalta_width = Math.atan((double) src.height / src.width);
        double angel_dalta_height = Math.atan((double) src.width / src.height);

        int len_dalta_width = (int) (len * Math.cos(Math.PI - angel_alpha
                - angel_dalta_width));
        int len_dalta_height = (int) (len * Math.cos(Math.PI - angel_alpha
                - angel_dalta_height));
        int des_width = src.width + len_dalta_width * 2;
        int des_height = src.height + len_dalta_height * 2;
        return new Rectangle(new Dimension(des_width, des_height));
    }

    /**
     * 真实删除图片
     *
     * @param fileName 文件路径
     * @return 0 成功 -1 删除失败 1 没有文件 2 其他异常
     */
    @Override
    public int deleteTruely(String fileName) {
        if (AppUtils.isBlank(fileName)) {
            return 0;
        }

        String ret = sendGet(propertiesUtil.getInnerPhotoServer() + "/del", fileName, "fileName=" + fileName);
        int result = 0;
        try {
            result = getReturnCodeFromResult(fileName, ret);
        } catch (Exception e) {
            log.error("", e);
        }
        return result;
    }

	@Override
	public String uploadPrivateFile(InputStream is, String fileName) {
		return null;
	}

	@Override
	public String uploadPrivateFile(InputStream is, String fileName, long fileSize) {
		return upload(is,fileName,fileSize);
	}

	@Override
	public String uploadPrivateFile(MultipartFile file) {
		return upload(file);
	}

	@Override
	public String getPrivateObjectUrl(String key) {
		return null;
	}

	/**
     * 做文件拷贝,从商品的图片拷贝到商品快照去
     *
     * @param filePath
     */
    @Override
    public String copyFileForProdSnapshotImg(String filePath) {
        String ret = sendGet(propertiesUtil.getInnerPhotoServer() + "/copy", filePath, "fileName=" + filePath);
        Object result = getObjectFromResult(filePath, ret);
        if (result != null) {
            return result.toString();
        }
        log.warn("copy file error filePath = {}", filePath);
        return null;// 上传文件失败
    }

    /**
     * 上传图片
     *
     * @param urlStr  不肯能为空的
     * @return
     */
    private String formUpload(String urlStr, String fileName, Map<String, InputStream> insMap) {
        String res = "[]";
        HttpURLConnection conn = null;
        String BOUNDARY = "---------------------------58487568765418"; // boundary就是request头和上传文件内容的分隔符
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(60000);
            conn.setReadTimeout(60000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

            // 增加头部安全信息
            String dateStr = DateUtil.DateToString(new Date(), "yyyy-MM-dd HH:MM:SS");
            conn.setRequestProperty("ls-time", dateStr);

            String securityResult = MD5Util.toMD5(systemToken + dateStr);
            conn.setRequestProperty("ls-security", securityResult);

            OutputStream out = new DataOutputStream(conn.getOutputStream());

            // file
            Iterator<Entry<String, InputStream>> iter = insMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, InputStream> entry = (Map.Entry<String, InputStream>) iter.next();
                String inputName = (String) entry.getKey();
                InputStream is = (InputStream) entry.getValue();
                String contentType = "application/octet-stream";

                StringBuffer strBuf = new StringBuffer();
                strBuf.append("\r\n").append("--").append(BOUNDARY).append("\r\n");
                strBuf.append("Content-Disposition: form-data; name=\"" + inputName + "\"; filename=\"" + inputName + "\"\r\n");
                strBuf.append("Content-Type:" + contentType + "\r\n\r\n");

                out.write(strBuf.toString().getBytes());

                DataInputStream in = new DataInputStream(is);
                int bytes = 0;
                byte[] bufferOut = new byte[1024];
                while ((bytes = in.read(bufferOut)) != -1) {
                    out.write(bufferOut, 0, bytes);
                }
                in.close();
            }

            byte[] endData = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
            out.write(endData);
            out.flush();
            out.close();

            // 读取返回数据
            StringBuffer strBuf = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                strBuf.append(line).append("\n");
            }
            res = strBuf.toString();
            reader.close();
            reader = null;
        } catch (Exception e) {
            log.error("formUpload error. " + urlStr, e);
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return res;
    }

    private String sendGet(String url, String fileName, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");


            // 增加头部安全信息
            String dateStr = DateUtil.DateToString(new Date(), "yyyy-MM-dd HH:MM:SS");
            connection.setRequestProperty("ls-time", dateStr);

            String securityResult = MD5Util.toMD5(systemToken + dateStr);
            connection.setRequestProperty("ls-security", securityResult);


            // 建立实际的连接
            connection.connect();
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            log.error("sendGet error！", e);
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                log.error("close instream error！", e2);
            }
        }
        return result;
    }

    /**
     * 从返回结果中获取最终的结果,并做好显示记录
     *
     * @return
     */
    private Object getObjectFromResult(String fileName, String ret) {
        InvokeRetObj returnObj = JSONUtil.getObject(ret, InvokeRetObj.class); // 支持错误原因并做好打印显示工作
        if (returnObj != null) {
            if (returnObj.isFlag()) {// 上传成功
                return returnObj.getObj(); // 上传的功能是支持多个文件上传的,但是这里只是支持一个
            } else {
                log.warn("File process failed,  file name is {}, msg is {}", fileName, returnObj.getMsg());
            }
        } else {
            log.warn("File process failed, file name is {}", fileName);
        }
        return null;
    }

    /**
     * 从返回结果中获取最终的结果,并做好显示记录
     *
     * @return
     */
    private int getReturnCodeFromResult(String fileName, String ret) {
        InvokeRetObj returnObj = JSONUtil.getObject(ret, InvokeRetObj.class); // 支持错误原因并做好打印显示工作
        if (returnObj != null) {
            if (returnObj.isFlag()) {// 上传成功
                return returnObj.getRetCode();
            } else {
                log.warn("Delete file failed, file name is {}, msg is {}", fileName, returnObj.getMsg());
            }
        } else {
            log.warn("Delete file failed for unknow reason, file name is {}", fileName);
        }

        return 2;
    }

    /**
     * 将结果封装到Dto中, 图片服务器返回InvokeRetObj
     */
    @Override
    public UploadResultDto saveResultDto(String resultStr, ImagePurposeEnum purposeEnum, String userId, String userName, Long shopId, String fileName, long fileSize, AttachmentTypeEnum attachmentType, ImageTypeEnum imageType) {
        UploadResultDto resultDto = new UploadResultDto();
        InvokeRetObj returnObj = parseUploadResult(fileName, resultStr); // 支持错误原因并做好打印显示工作
        if (returnObj != null && returnObj.isFlag()) { // 上传成功
            @SuppressWarnings("unchecked")
            List<String> pathList = (List<String>) returnObj.getObj();
            String[] paths = new String[pathList.size()];
            Long[] attachmentIds = new Long[pathList.size()];
            for (int i = 0; i < pathList.size(); i++) {
                paths[i] = pathList.get(i);
                Attachment attachment = new Attachment();
                attachment.setUserName(userName);
                attachment.setUserId(userId);
                attachment.setShopId(shopId);
                attachment.setRecDate(new Date());
                attachment.setExt(fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase());
                attachment.setFilePath(paths[i]);
                attachment.setFileSize(fileSize);
                attachment.setStatus(true);
                attachment.setFileName(fileName);
                attachment.setTreeId(0l);
                attachment.setFileType(attachmentType.value());
                attachment.setIsManaged(attachmentType.type());
                attachment.setImgType(imageType.value());
                attachment.setPurpose(purposeEnum.value());
                Long attachmentId = attachmentService.saveAttachment(attachment);
                if (attachmentId > 0) {
                    attachmentIds[i] = attachmentId;
                    // 保存成功
                    resultDto.setStatus(1);
                } else if (attachmentId == 0) {
                    // 保存附件失败
                    resultDto.setMessage("保存文件附件失败");
                    resultDto.setStatus(0);
                }
            }
            resultDto.setMessage(returnObj.getMsg());
            resultDto.setPaths(paths);
            resultDto.setAttachmentId(attachmentIds);

            // 保存附件
            return resultDto;
        } else {
            resultDto.setMessage("上传文件失败");
            resultDto.setStatus(-1);
        }
        return resultDto;
    }


    /**
     * 从返回结果中获取最终的结果,并做好显示记录
     *
     * @return
     */
    private InvokeRetObj parseUploadResult(String fileName, String ret) {
        try {
            InvokeRetObj returnObj = JSONUtil.getObject(ret, InvokeRetObj.class); // 支持错误原因并做好打印显示工作
            if (returnObj != null) {
                if (returnObj.isFlag()) {// 上传成功
                    log.debug("File process success, file name is {}", fileName);
                } else {
                    log.warn("File process failed,  file name is {}, msg is {}", fileName, returnObj.getMsg());
                }
            } else {
                log.warn("File process failed, file name is {}", fileName);
            }
            return returnObj;
        } catch (Exception e) {
            InvokeRetObj result = new InvokeRetObj();
            result.setFlag(true);
            List<String> retList = new ArrayList<String>();
            retList.add(ret);
            result.setObj(retList);

            return result;
        }


    }
}