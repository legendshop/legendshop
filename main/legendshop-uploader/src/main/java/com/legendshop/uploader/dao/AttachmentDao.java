/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Attachment;

/**
 * 附件的Dao.
 */

public interface AttachmentDao extends GenericDao< Attachment, Long> {
     
    List<Attachment> getAttachmentByShopId(Long shopId);

	Attachment getAttachment(Long id);
	
    int deleteAttachment(Attachment attachment);
	
	Long saveAttachment(Attachment attachment);
	
	void updateAttachment(Attachment attachment);
	
	PageSupport<Attachment> getAttachment(CriteriaQuery cq);
	
	void updateAttachmentStsByFilePath(String filePath,int sts);

	PageSupport<Attachment> getAttachment(Long shopId,Long treeId, int isManaged, int status, int pageSize, String curPageNO,String searchName, Integer imgType);

	void updateAttachmentFileNameByFilePath(String filePath, String fileName);

	List<Attachment> getAttachmentByTreeIds(List<Long> ids);
	
	public Attachment getAttachmentByPath(String filePath);

	PageSupport<Attachment> getAttachment(String shopName, int isManaged, int status, int pageSize, String curPageNO,String searchName, Integer imgType);

	PageSupport<Attachment> getPropChildImg(Long treeId, String curPageNO, String orderBy, String searchName, Long shopId);

	PageSupport<Attachment> getAttachmentPage(String curPageNO, String searchName, String orderBy, Long shopId,Long treeId);

	List<Attachment> getAllAttachment();
	
	Attachment getAttachmentByFilePath(String filePath);

	PageSupport<Attachment> getAttachment(Long shopId, Long treeId, int isManaged, int status, int pageSize, String curPageNO, String searchName);
	
	
 }
