/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.entity.ProductSnapshotImg;
import com.legendshop.uploader.dao.ProductSnapshotImgDao;
import com.legendshop.uploader.service.ProductSnapshotImgService;

/**
 * 商品快照图片服务类
 */
@Service("productSnapshotImgService")
public class ProductSnapshotImgServiceImpl  implements ProductSnapshotImgService{
	
	@Autowired
    private ProductSnapshotImgDao productSnapshotImgDao;

    public ProductSnapshotImg getProductSnapshotImg(Long id) {
        return productSnapshotImgDao.getProductSnapshotImg(id);
    }

    public Long saveProductSnapshotImg(ProductSnapshotImg productSnapshotImg) {
        return productSnapshotImgDao.saveProductSnapshotImg(productSnapshotImg);
    }

	@Override
	public ProductSnapshotImg getProductSnapshotImgByPath(String imgPath) {
		return productSnapshotImgDao.getProductSnapshotImgByPath(imgPath);
	}
}
