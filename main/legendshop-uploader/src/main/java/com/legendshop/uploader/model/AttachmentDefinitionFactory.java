package com.legendshop.uploader.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.util.AppUtils;

/**
 *   附件类型定义 目前支持视频和图片
 * 
 */
public class AttachmentDefinitionFactory {

	private static AttachmentDefinitionFactory instance = null;

	private  Map<ImageTypeEnum, AttachmentDefinition> attachmentList;

	public static AttachmentDefinitionFactory getInstance() {
		if (instance == null) {
			synchronized (AttachmentDefinitionFactory.class) {
				instance = new AttachmentDefinitionFactory();
				instance.init();//初始化
			}
		}
		return instance;
	}

	/**
	 *
	 * 设置默认允许上传的文件大小和文件类型
	 *
	 */
	public void init(){
		attachmentList = new HashMap<ImageTypeEnum, AttachmentDefinition>();
		for (ImageTypeEnum type : ImageTypeEnum.values()) {
			if (ImageTypeEnum.IMG.equals(type)) {// 图片
				Long maxUploadSize = new Long(20 * 1024 * 1024); // 默认允许上传的大小是5M;
				List<String> allowUploadType = new ArrayList<String>();

				allowUploadType.add(".jpg");
				allowUploadType.add(".jpeg");
				allowUploadType.add(".png");
				allowUploadType.add(".bmp");
				allowUploadType.add(".gif");

				AttachmentDefinition ad = new AttachmentDefinition(type.value(), maxUploadSize, allowUploadType);
				attachmentList.put(type, ad);
			} else if (ImageTypeEnum.VIDEO.equals(type)) {// 视频
				Long maxUploadSize = new Long(20 * 1024 * 1024); // 视频默认允许上传的大小是10M;
				List<String> allowUploadType = new ArrayList<String>();
				allowUploadType.add(".mp4");
				allowUploadType.add(".webm");
				allowUploadType.add(".mpeg");
				allowUploadType.add(".ogg");

				AttachmentDefinition ad = new AttachmentDefinition(type.value(), maxUploadSize, allowUploadType);
				attachmentList.put(type, ad);
			}else if (ImageTypeEnum.FILE.equals(type)){
				Long maxUploadSize = new Long( 1024 * 1024); // 其他文件默认允许上传的大小是1M;
				List<String> allowUploadType = new ArrayList<String>();
				allowUploadType.add(".txt");
				allowUploadType.add(".p12");
				AttachmentDefinition ad = new AttachmentDefinition(type.value(), maxUploadSize, allowUploadType);
				attachmentList.put(type, ad);
			}
		}
	}

	public AttachmentDefinition getAttachmentDefinitionByType(ImageTypeEnum type) {
		return attachmentList.get(type);
	}
	/**
	 * 根据后缀获取对应的文件类型定义
	 * @param extName
	 * @return
	 */
	public AttachmentDefinition getAttachmentDefinitionByType(String extName) {
		if(AppUtils.isBlank(extName)){
			return null;
		}
		for (AttachmentDefinition ad : attachmentList.values()) {
			if(ad.getAllowUploadType().contains(extName)){
				return ad;
			}
		}
		return null;
	}

	/**
	 * 获取
	 * @return
	 */
	public Map<ImageTypeEnum, AttachmentDefinition> getAttachmentList() {
		return attachmentList;
	}

	/**
	 * 根据文件类型获取他的定义
	 * @param imageTypeEnum
	 * @return
	 */
	public AttachmentDefinition getAttachment(ImageTypeEnum imageTypeEnum) {
		return attachmentList.get(imageTypeEnum);
	}

	/**
	 * 根据文件后缀名获取附件的定义
	 * @param extName
	 * @return
	 */
	public AttachmentDefinition getAttachmentDefinitionByExtName(String extName) {
		for (AttachmentDefinition definition : attachmentList.values()) {
			if (definition.getAllowUploadType().contains(extName)) {
				return definition;
			}
		}

		return null;
	}
}
