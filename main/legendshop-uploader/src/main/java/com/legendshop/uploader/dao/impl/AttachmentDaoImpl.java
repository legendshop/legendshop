/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.dao.impl;

import com.legendshop.dao.criterion.Restrictions;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.*;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.entity.Attachment;
import com.legendshop.uploader.dao.AttachmentDao;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * 附件的Dao实现类.
 */
@Repository
public class AttachmentDaoImpl extends GenericDaoImpl<Attachment, Long> implements AttachmentDao {
     
	private static final String queryStringForSearch = "select la.*" +
			"from ls_attachment la,ls_shop_detail sd where la.shop_id=sd.shop_id and la.is_managed=? and la.status=? ";
	
	private static final String allCountStringForSearch = "select count(la.id) from ls_attachment la,ls_shop_detail sd where la.shop_id=sd.shop_id and la.is_managed=? and la.status=?";
	
	private static final String queryString = "select la.*,(select count(*) from ls_attmnt_rel lr where la.id=lr.attachment_id) relCount " +
			"from ls_attachment la where la.shop_id=? and la.file_path<>'' and la.tree_id=? and la.is_managed=? and la.status=? ";
	
	private static final String allCountString = "select count(la.id) from ls_attachment la where la.shop_id=? and la.file_path<>'' and la.tree_id=? and la.is_managed=? and la.status=?";
	
	@Override
	public List<Attachment> getAttachmentByShopId(Long shopId){
   		return this.queryByProperties(new EntityCriterion().eq("shopId", shopId));
    }

	@Override
	public Attachment getAttachment(Long id){
		return getById(id);
	}
	
    @Override
	public int deleteAttachment(Attachment attachment){
    	return delete(attachment);
    }
	
	@Override
	public Long saveAttachment(Attachment attachment){
		return (Long)save(attachment);
	}
	
	@Override
	public void updateAttachment(Attachment attachment){
		 update(attachment);
	}
	
	@Override
	public PageSupport<Attachment> getAttachment(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public void updateAttachmentStsByFilePath(String filePath, int sts) {
		this.update("update ls_attachment set status=? where file_path=?", sts,filePath);
	}

	@Override
	public PageSupport<Attachment> getAttachment(Long shopId,Long treeId, int isManaged,int status, int pageSize, String curPageNO,String searchName, Integer imgType) {
		QueryMap parameterMap = new QueryMap();
		if(treeId == null) {
			treeId = 0l;//默认节点, 以后可能会改动
		}
		parameterMap.put("shopId", shopId);
		parameterMap.put("treeId", treeId);
		parameterMap.put("isManaged", isManaged);
		parameterMap.put("status", status);
		parameterMap.put("imgType", imgType);
		parameterMap.like("fileName", searchName);

		String sql = ConfigCode.getInstance().getCode("attachment.getAttachment", parameterMap);
		String sqlCount = ConfigCode.getInstance().getCode("attachment.getAttachmentCount", parameterMap);
		SimpleSqlQuery query = new SimpleSqlQuery(Attachment.class, sql, sqlCount, parameterMap.toArray(), pageSize, curPageNO);
		return this.querySimplePage(query);
	}
	
	@Override
	public PageSupport<Attachment> getAttachment(String shopName, int isManaged,int status, int pageSize, String curPageNO,String searchName, Integer imgType) {
		Object[] param = null;
		String sql = queryStringForSearch;
		String sqlCount = allCountStringForSearch;
		if(AppUtils.isNotBlank(shopName)){
			if(AppUtils.isNotBlank(searchName)){
				param = new Object[]{isManaged,status, "%"+shopName.trim()+"%","%"+searchName.trim()+"%"};
				sql += " and sd.site_name like ? and la.file_name like ?"; 
				sqlCount += " and sd.site_name like ? and la.file_name like ?"; 
			}else{
				param = new Object[]{isManaged,status,"%"+shopName.trim()+"%"};
				sql += " and sd.site_name like ? "; 
				sqlCount += " and sd.site_name like? "; 
			}
		}else{
			if(AppUtils.isNotBlank(searchName)){
				param = new Object[]{isManaged,status,"%"+searchName.trim()+"%"};
				sql += " and la.file_name like ?"; 
				sqlCount += " and la.file_name like ?"; 
			}else{
				param = new Object[]{isManaged,status};
			}
		}
		sql+=" order by la.rec_date desc";
		SimpleSqlQuery query = new SimpleSqlQuery(Attachment.class, sql, sqlCount, param, pageSize, curPageNO);
		return this.querySimplePage(query);
	}

	@Override
	public void updateAttachmentFileNameByFilePath(String filePath, String fileName) {
		this.update("update ls_attachment set file_name=? where file_path=?", fileName,filePath);
	}

	@Override
	public List<Attachment> getAttachmentByTreeIds(List<Long> ids) {
		StringBuffer sql = new StringBuffer("select * from ls_attachment where tree_id in (");
		for (int i=0;i<ids.size();i++) {
			sql.append("?,");
		}
		sql.setLength(sql.length()-1);
		sql.append(")");
		return this.query(sql.toString(), Attachment.class, ids.toArray());
	}

	@Override
	public Attachment getAttachmentByPath(String filePath) {
		return this.getByProperties(new EntityCriterion().eq("filePath", filePath));
	}
	
	@Override
	public PageSupport<Attachment> getPropChildImg(Long treeId, String curPageNO,String orderBy,String searchName,Long shopId) {
		CriteriaQuery cq = new CriteriaQuery(Attachment.class,curPageNO);
		cq.setPageSize(10);
		cq.eq("status", true);
		cq.eq("imgType", ImageTypeEnum.IMG.value()); //是否是图片
		cq.eq("isManaged", 1);//是否在管理器中出现
		cq.eq("shopId", shopId);
		if("descDate".equals(orderBy)){
			cq.addDescOrder("recDate");
		}else if("ascDate".equals(orderBy)){
			cq.addAscOrder("recDate");
		}else if("ascSize".equals(orderBy)){
			cq.addAscOrder("fileSize");
		}else if("descSize".equals(orderBy)){
			cq.addDescOrder("fileSize");
		}else if("ascName".equals(orderBy)){
			cq.addAscOrder("fileName");
		}else if("descName".equals(orderBy)){
			cq.addDescOrder("fileName");
		}else{
			cq.addDescOrder("recDate");
		}
		if(searchName!=null && !searchName.equals("")){
			cq.add(Restrictions.or(Restrictions.like("fileName", "%"+searchName+"%"), Restrictions.and(Restrictions.like("id", "%"+searchName+"%"), Restrictions.isNull("fileName"))));
		}else{
			cq.eq("treeId", treeId);
		}

		return queryPage(cq);
	}

	@Override
	public PageSupport<Attachment> getAttachmentPage(String curPageNO, String searchName, String orderBy, Long shopId,Long treeId) {
		CriteriaQuery cq = new CriteriaQuery(Attachment.class,curPageNO);
		cq.setPageSize(10);
		cq.eq("status", true);
		cq.eq("imgType", ImageTypeEnum.VIDEO.value()); //是否是图片
		cq.eq("isManaged", 1);//是否在管理器中出现
		cq.eq("shopId", shopId);
		if("descDate".equals(orderBy)){
			cq.addDescOrder("recDate");
		}else if("ascDate".equals(orderBy)){
			cq.addAscOrder("recDate");
		}else if("ascSize".equals(orderBy)){
			cq.addAscOrder("fileSize");
		}else if("descSize".equals(orderBy)){
			cq.addDescOrder("fileSize");
		}else if("ascName".equals(orderBy)){
			cq.addAscOrder("fileName");
		}else if("descName".equals(orderBy)){
			cq.addDescOrder("fileName");
		}else{
			cq.addDescOrder("recDate");
		}
		if(searchName!=null && !searchName.equals("")){
			cq.add(Restrictions.or(Restrictions.like("fileName", "%"+searchName+"%"), Restrictions.and(Restrictions.like("id", "%"+searchName+"%"), Restrictions.isNull("fileName"))));
		}else{
			cq.eq("treeId", treeId);
		}
		return queryPage(cq);
	}

	@Override
	public Attachment getAttachmentByFilePath(String filePath) {
		Attachment attachment = this.getByProperties(new EntityCriterion().eq("filePath", filePath));
        return attachment;
	}

	@Override
	public PageSupport<Attachment> getAttachment(Long shopId, Long treeId, int isManaged, int status, int pageSize, String curPageNO, String searchName) {
		Object[] param = null;
		String sql = queryString;
		String sqlCount = allCountString;
		if(AppUtils.isNotBlank(searchName)){
			param = new Object[]{shopId,treeId,isManaged,status,"%"+searchName+"%"};
			sql += " and la.file_name like ?"; 
			sqlCount += " and la.file_name like ?"; 
		}else{
			param = new Object[]{shopId,treeId,isManaged,status};
		}
		sql+=" order by la.rec_date desc";
		SimpleSqlQuery query = new SimpleSqlQuery(Attachment.class, sql, sqlCount, param, pageSize, curPageNO);
		return this.querySimplePage(query);
	}
	
	@Override
	public  List<Attachment> getAllAttachment(){
		return queryAll();
	}
	
	
 }
