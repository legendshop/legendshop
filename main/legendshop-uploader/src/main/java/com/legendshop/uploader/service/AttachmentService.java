/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.AttachmentTree;

/**
 *  附件Service.
 */
public interface AttachmentService  {

    public List<Attachment> getAttachmentByShopId(Long shopId);

    public Attachment getAttachment(Long id);
    
    public void deleteAttachment(Attachment attachment);
    
    public void deleteAttachment(String  filePath);
    
    public Long saveAttachment(Attachment attachment);

    public void updateAttachment(Attachment attachment);

	public Attachment getAttachmentByFilePath(String filePath);
	
	public Attachment getAttachmentByFilePath(String filePath,Long shopId);

	public void updateAttachmentStsByFilePath(String filePath, int sts);

	/**
	 * 根据 参数 分页查找 附件表
	 */
	public PageSupport<Attachment> getAttachment(Long shopId,Long treeId, int isManaged,  int status, int pageSize, String curPageNO,String searchName, Integer imgType);

	public void updateAttachmentFileNameByFilePath(String filePath,	String fileName);

	public List<Attachment> getAttachmentByTrees(List<AttachmentTree> attmntTreeList);

	public void deleteAttachment(List<Attachment> attmntList);
	
	public Attachment getAttachmentByPath(String filePath);

	public PageSupport<Attachment> getAttachment(String shopName, int isManaged, int status, int pageSize, String curPageNO,String searchName, Integer imgType);

	public PageSupport<Attachment> getPropChildImg(Long treeId, String curPageNO, String orderBy, String searchName, Long shopId);

	public PageSupport<Attachment> getAttachmentPage(String curPageNO, String searchName, String orderBy, Long shopId,Long treeId);
	
	/**
	 * 获取所有的附件,测试下载用
	 * @return 所有的附件
	 */
	public List<Attachment> getAllAttachment();
}
