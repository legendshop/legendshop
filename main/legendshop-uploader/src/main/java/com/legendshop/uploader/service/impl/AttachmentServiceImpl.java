/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.uploader.dao.impl.AttachmentDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.uploader.dao.AttachmentDao;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.util.AppUtils;

/**
 * 附件服务.
 */
@Service("attachmentService")
public class AttachmentServiceImpl implements AttachmentService {

	@Autowired
    private AttachmentDao attachmentDao;

    public List<Attachment> getAttachmentByShopId(Long shopId) {
        return attachmentDao.getAttachmentByShopId(shopId);
    }

    public Attachment getAttachment(Long id) {
        return attachmentDao.getAttachment(id);
    }

    public void deleteAttachment(Attachment attachment) {
        attachmentDao.deleteAttachment(attachment);
    }

    public void deleteAttachment(String filePath) {
        Attachment attachment = getAttachmentByFilePath(filePath);
        if (attachment != null) {
            attachmentDao.update("delete from ls_attachment where file_path=?", filePath);
        }
    }

    public Long saveAttachment(Attachment attachment) {
        if (!AppUtils.isBlank(attachment.getId())) {
            updateAttachment(attachment);
            return attachment.getId();
        }
        return (Long) attachmentDao.save(attachment);
    }

    public void updateAttachment(Attachment attachment) {
        attachmentDao.updateAttachment(attachment);
    }


    @Override
    public Attachment getAttachmentByFilePath(String filePath) {
        List<Attachment> attachmentList = attachmentDao.queryByProperties(new EntityCriterion().eq("filePath", filePath));
        if (attachmentList == null || attachmentList.size() == 0) {
            return null;
        }
        return attachmentList.get(0);
    }

    @Override
    public void updateAttachmentStsByFilePath(String filePath, int sts) {
        attachmentDao.updateAttachmentStsByFilePath(filePath, sts);
    }

    /**
     * 根据 参数 分页查找 附件表
     *
     * @param treeId     节点id
     * @param isManaged  是否图片空间管理
     * @param shopId
     * @param status     状态
     * @param pageSize   每页条数
     * @param curPageNO  页码
     * @param searchName 图片名称
     * @return
     */
    @Override
    public PageSupport<Attachment> getAttachment(Long shopId, Long treeId, int isManaged, int status, int pageSize, String curPageNO, String searchName, Integer imgType) {
        return attachmentDao.getAttachment(shopId, treeId, isManaged, status, pageSize, curPageNO, searchName, imgType);
    }

    @Override
    public PageSupport<Attachment> getAttachment(String shopName, int isManaged, int status, int pageSize, String curPageNO, String searchName, Integer imgType) {
        return attachmentDao.getAttachment(shopName, isManaged, status, pageSize, curPageNO, searchName, imgType);
    }

    @Override
    public Attachment getAttachmentByFilePath(String filePath, Long shopId) {
        return attachmentDao.getByProperties(new EntityCriterion().eq("filePath", filePath).eq("shopId", shopId));
    }

    @Override
    public void updateAttachmentFileNameByFilePath(String filePath, String fileName) {
        attachmentDao.updateAttachmentFileNameByFilePath(filePath, fileName);
    }

    @Override
    public List<Attachment> getAttachmentByTrees(List<AttachmentTree> attmntTreeList) {
        if (AppUtils.isNotBlank(attmntTreeList)) {
            List<Long> ids = new ArrayList<Long>();
            for (AttachmentTree attachmentTree : attmntTreeList) {
                ids.add(attachmentTree.getId());
            }
            return attachmentDao.getAttachmentByTreeIds(ids);
        }
        return null;
    }

    @Override
    public void deleteAttachment(List<Attachment> attmntList) {
        if (AppUtils.isNotBlank(attmntList)) {
            attachmentDao.delete(attmntList);
        }
    }

    @Override
    public Attachment getAttachmentByPath(String filePath) {
        return attachmentDao.getAttachmentByPath(filePath);
    }

    @Override
    public PageSupport<Attachment> getPropChildImg(Long treeId, String curPageNO, String orderBy, String searchName, Long shopId) {
        return attachmentDao.getPropChildImg(treeId, curPageNO, orderBy, searchName, shopId);
    }

    @Override
    public PageSupport<Attachment> getAttachmentPage(String curPageNO, String searchName, String orderBy, Long shopId, Long treeId) {
        return attachmentDao.getAttachmentPage(curPageNO, searchName, orderBy, shopId, treeId);
    }

	@Override
	public List<Attachment> getAllAttachment() {
		return attachmentDao.getAllAttachment();
	}

	public void setAttachmentDao(AttachmentDaoImpl attachmentDao) {
    	this.attachmentDao = attachmentDao;
	}
}
