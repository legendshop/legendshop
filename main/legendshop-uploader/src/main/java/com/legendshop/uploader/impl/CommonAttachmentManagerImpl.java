/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ImagePurposeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.dto.im.UploadResultDto;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.ProductSnapshotImg;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.service.ProductSnapshotImgService;
import com.legendshop.uploader.util.ValidateFileProcessor;
import com.legendshop.uploader.util.ValidateFileResult;
import com.legendshop.util.AppUtils;

/**
 * 公共文件管理 抽象类
 *
 */
public abstract class CommonAttachmentManagerImpl implements AttachmentManager {
	
	protected final Logger log = LoggerFactory.getLogger(CommonAttachmentManagerImpl.class);

	/** 系统配置. */
	protected PropertiesUtil propertiesUtil;

	/**
	 * 附件服务.
	 */
	protected AttachmentService attachmentService;

	/**
	 * 图片比例列表.
	 */
	protected Map<String, List<Integer>> scaleList;

	/**
	 * 商品快照图片服务
	 */
	protected ProductSnapshotImgService productSnapshotImgService;
	
	public CommonAttachmentManagerImpl(PropertiesUtil propertiesUtil, AttachmentService attachmentService, Map<String, List<Integer>> scaleList,
			ProductSnapshotImgService productSnapshotImgService) {
		this.propertiesUtil = propertiesUtil;
		this.attachmentService = attachmentService;
		this.scaleList = scaleList;
		this.productSnapshotImgService = productSnapshotImgService;
	}


	/**
	 * 保存到附件表
	 *
	 * @param userName
	 *            用户名
	 * @param filePath
	 *            文件路径
	 * @param file
	 * @param type
	 *            附件类型
	 * @param treeId
	 *            节点id
	 */
	@Override
	public Long saveAttachment(String userName,String userId, Long shopId, String filePath, MultipartFile file, AttachmentTypeEnum type, long treeId, ImageTypeEnum imgType) {
		if (AppUtils.isBlank(filePath) || AppUtils.isBlank(file)) {
			log.warn("can not save attachment without file");
			return -1l;
		}
		if (Constants.PHOTO_FORMAT_ERR.equals(filePath)) {
			log.warn("can not save attachment");
			return -3l;
		}


		String fileName = file.getOriginalFilename();//防止有部分文件名字放在了userName
		String ext = ValidateFileProcessor.getValidateExtName(imgType, fileName);
		if(AppUtils.isBlank(ext)) {
			fileName = file.getName();
			ext = ValidateFileProcessor.getValidateExtName(imgType, fileName);
		}

		Attachment attachment = new Attachment();

		if (AppUtils.isBlank(userName)) {
			log.warn("can not save attachment without login");
			return -2l;
		}

		attachment.setUserName(userName);
		attachment.setUserId(userId);
		attachment.setShopId(shopId);
		attachment.setRecDate(new Date());
		attachment.setExt(ext);
		attachment.setFilePath(filePath);
		attachment.setFileSize(file.getSize());
		attachment.setStatus(true);
		attachment.setFileName(file.getOriginalFilename());
		attachment.setTreeId(treeId);
		attachment.setFileType(type.value());
		attachment.setIsManaged(type.type());
		attachment.setImgType(imgType.value());
		try {
			BufferedImage image = ImageIO.read(file.getInputStream());
			attachment.setWidth(Long.valueOf(image.getWidth()));
			attachment.setHeight(Long.valueOf(image.getHeight()));
		} catch (IOException e) {
			log.error("get image width and height error {}", e.toString());
		}catch (Exception e){
			log.error("attach is not image ", e.toString());
		}
		return attachmentService.saveAttachment(attachment);// 保存到附件表
	}


	/**
	 * 保存商品快照图片
	 */
	@Override
	public String saveProdSnapshotImg(String filePath, Long snapshotId) {
		String resultImage = null;
		if(AppUtils.isBlank(filePath)) {
			log.warn("can not saveProdSnapshotImg");
			return resultImage;
		}
		try {
			ProductSnapshotImg productSnapshotImg = productSnapshotImgService.getProductSnapshotImgByPath(filePath);
			if (AppUtils.isBlank(productSnapshotImg)) {
				Attachment attachment = attachmentService.getAttachmentByFilePath(filePath);
				resultImage = copyFileForProdSnapshotImg(filePath); // 拷贝成功Z
				if (resultImage != null) {
					productSnapshotImg = new ProductSnapshotImg();
					productSnapshotImg.setSnapshotId(snapshotId);
					productSnapshotImg.setRecDate(new Date());
					productSnapshotImg.setImgPath(resultImage);
					if (AppUtils.isNotBlank(attachment)) {
						productSnapshotImg.setImgSize(attachment.getFileSize());
						productSnapshotImg.setHeight(attachment.getHeight());
						productSnapshotImg.setWidth(attachment.getWidth());
					}
					productSnapshotImgService.saveProductSnapshotImg(productSnapshotImg);
				} else {
					log.error("Save product snapshot image with filePath:{}, snapshotId:{", filePath, snapshotId);
				}

			}
			else {
				resultImage = productSnapshotImg.getImgPath();
			}
		} catch (Exception e) {
			log.error("Save product snapshot image with filePath:{}, snapshotId:{},error:{}", filePath, snapshotId, e);
		}

		return resultImage;
	}


	public InputStream loadImageURL(String imageurl) throws IOException {
		// 构造URL
		URL url = new URL(imageurl);
		// 打开连接
		URLConnection con = url.openConnection();
		// 输入流
		InputStream is = con.getInputStream();
		return is;
	}
	
	/**
	 * 根据文件名删除附件表记录
	 */
	@Override
	public void delAttachmentByFilePath(String filePath) {
		attachmentService.deleteAttachment(filePath);
	}

	/**
	 * 保存附件表
	 * @param userName 用户名
	 * @param userId 用户id
	 * @param shopId 店铺id
	 * @param filePath 图片路径
	 * @param file 文件
	 * @param type 图片类型
	 * @param imgType  是否是图片,还是视频
	 * @return
	 */
	@Override
	public Long saveAttachment(String userName, String userId, Long shopId, String filePath, MultipartFile file, AttachmentTypeEnum type, ImageTypeEnum imgType) {
		return this.saveAttachment(userName, userId, shopId, filePath, file, type, 0, imgType);
	}
	
	/**
	 * 保存图片到附件表,默认是图片
	 * 
	 * @param userName
	 *            用户名
	 * @param filePath
	 *            文件路径
	 * @param file
	 * @param type
	 *            附件类型
	 */
	@Override
	public Long saveImageAttachment(String userName, String userId, Long shopId, String filePath, MultipartFile file, AttachmentTypeEnum type) {
		return this.saveAttachment(userName, userId, shopId, filePath, file, type, 0, ImageTypeEnum.IMG);
	}

	/**
	 * 逻辑删除文件,修改附件表的状态
	 * 
	 * @param fileName
	 *            文件路径,包括文件名
	 */
	@Override
	public void deleteLogical(String fileName) {
		attachmentService.updateAttachmentStsByFilePath(fileName, 0);
	}


	
	///////////////////////////////////////////////////// for IM system /////////////////////////////////////////////////
	

	/**
	 * 上传图片
	 * @param is 文件流
	 * @param fileName 文件名
	 * @param fileSize 文件大小
	 * @return
	 */
	public UploadResultDto uploadImage(ImagePurposeEnum purposeEnum, String userId, String userName, Long shopId, MultipartFile file) {
		try {
			return doUpload(purposeEnum,userId, userName, shopId, file, AttachmentTypeEnum.IM, ImageTypeEnum.IMG);
		} catch (IOException e) {
			log.error("",e);
		}
		return null;
	}

	/**
     * 上传文件,加入校验
     * 
     */
    private UploadResultDto doUpload(ImagePurposeEnum purposeEnum, String userId, String userName, Long shopId, MultipartFile file, AttachmentTypeEnum attachmentType, ImageTypeEnum imageType) throws IOException{

        if (AppUtils.isBlank(file)) {
            UploadResultDto resultDto = new UploadResultDto();
            resultDto.setMessage("请上传文件");
            resultDto.setStatus(0);
            return resultDto;
        }

		String fileName = file.getOriginalFilename(); //需要用文件的原文件
		String extName = ValidateFileProcessor.getValidateExtName(imageType, fileName);
		if(AppUtils.isBlank(extName)) {//防止部分传递错误
			fileName = file.getName();
			extName = ValidateFileProcessor.getValidateExtName(imageType, fileName);
		}

		long fileSize = file.getSize();

        ValidateFileResult validateFileResult = ValidateFileProcessor.validateFile(file.getInputStream(), extName, fileSize, imageType);
        if (!validateFileResult.isUpFlag()) {
            UploadResultDto resultDto = new UploadResultDto();
			resultDto.setMessage("仅支持JPG、GIF、PNG、JPEG、BMP格式，且大小不超过20MB的图片");
            resultDto.setStatus(0);
            return resultDto;
        }

        //上传文件并获取返回值, 1. PHOTO_SERVER: 图片服务器返回InvokeRetObj的json格式, 2. Local: 本地方式会返回一个字符串表示是上传的文件名, 3. OSS:模式返回会返回一个字符串表示是上传的文件名
        String ret = upload(file);

        //返回值和参数进行入库封装
        UploadResultDto resultDto = saveResultDto(ret, purposeEnum, userId, userName, shopId, fileName, fileSize, attachmentType, imageType);


        return resultDto;
    }
	
    /**
     * 结果集封装入库,图片服务器会返回其他的格式
     * 
     */
	protected UploadResultDto saveResultDto(String resultStr, ImagePurposeEnum purposeEnum, String userId, String userName, Long shopId, String fileName, long fileSize, AttachmentTypeEnum attachmentType, ImageTypeEnum imageType) {
		UploadResultDto resultDto = new UploadResultDto();
		Attachment attachment = new Attachment();
		attachment.setUserName(userName);
		attachment.setUserId(userId);
		attachment.setShopId(shopId);
		attachment.setRecDate(new Date());
		attachment.setExt(fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase());
		attachment.setFilePath(resultStr);
		attachment.setFileSize(fileSize);
		attachment.setStatus(true);
		attachment.setFileName(fileName);
		attachment.setTreeId(0l);
		attachment.setFileType(attachmentType.value());
		attachment.setIsManaged(attachmentType.type());
		attachment.setImgType(imageType.value());
		attachment.setPurpose(purposeEnum.value());
		Long attachmentId = attachmentService.saveAttachment(attachment);
		if (attachmentId > 0) {
			// 保存成功
			resultDto.setPaths(resultStr.split(";"));
			resultDto.setStatus(1);
		} else if (attachmentId == 0) {
			// 保存附件失败
			resultDto.setMessage("保存文件附件失败");
			resultDto.setStatus(0);
		}

		return resultDto;
	}
	
    
  ///////////////////////////////////////////////////// for IM system /////////////////////////////////////////////////

	/**
	 * 为商品快照拷贝文件
	 * 
	 * @param filePath
	 *            文件路径
	 * @return 拷贝之后的文件路径
	 */
	public abstract String copyFileForProdSnapshotImg(String filePath);

	/**
	 * 上传图片
	 * @param is 文件流
	 * @param fileName 文件名
	 * @param fileSize 文件大小
	 * @return
	 */
	public abstract String upload(InputStream is,String fileName,long fileSize);
	
	public void setPropertiesUtil(PropertiesUtil propertiesUtil) {
		this.propertiesUtil = propertiesUtil;
	}

	public void setAttachmentService(AttachmentService attachmentService) {
		this.attachmentService = attachmentService;
	}

	public void setScaleList(Map<String, List<Integer>> scaleList) {
		this.scaleList = scaleList;
	}

	public void setProductSnapshotImgService(ProductSnapshotImgService productSnapshotImgService) {
		this.productSnapshotImgService = productSnapshotImgService;
	}

}
