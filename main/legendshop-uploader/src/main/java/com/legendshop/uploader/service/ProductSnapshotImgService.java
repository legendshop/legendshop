/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.service;

import com.legendshop.model.entity.ProductSnapshotImg;

/**
 * 商品快照图片服务
 */
public interface ProductSnapshotImgService  {

    /**
     * Gets the product snapshot img.
     *
     * @param id the id
     * @return the product snapshot img
     */
    public ProductSnapshotImg getProductSnapshotImg(Long id);
    
    /**
     * Gets the product snapshot img by path.
     *
     * @param imgPath the img path
     * @return the product snapshot img by path
     */
    public ProductSnapshotImg getProductSnapshotImgByPath(String imgPath);
    
    /**
     * Save product snapshot img.
     *
     * @param productSnapshotImg the product snapshot img
     * @return the long
     */
    public Long saveProductSnapshotImg(ProductSnapshotImg productSnapshotImg);
}
