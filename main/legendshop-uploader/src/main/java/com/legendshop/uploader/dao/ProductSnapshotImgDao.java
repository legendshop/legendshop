/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ProductSnapshotImg;

/**
 * 商品快照图片Dao
 */
public interface ProductSnapshotImgDao extends Dao<ProductSnapshotImg, Long> {
     
	public abstract ProductSnapshotImg getProductSnapshotImg(Long id);
	
    public abstract int deleteProductSnapshotImg(ProductSnapshotImg productSnapshotImg);
	
	public abstract Long saveProductSnapshotImg(ProductSnapshotImg productSnapshotImg);
	
	public abstract int updateProductSnapshotImg(ProductSnapshotImg productSnapshotImg);
	
	public abstract ProductSnapshotImg getProductSnapshotImgByPath(String imgPath);
	
 }
