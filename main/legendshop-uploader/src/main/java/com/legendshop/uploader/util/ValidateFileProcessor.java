/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.uploader.util;

import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.uploader.model.AttachmentDefinition;
import com.legendshop.uploader.model.AttachmentDefinitionFactory;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 检查文件大小,定义了各种文件类型的允许上传的大小和类型
 */
public class ValidateFileProcessor {

    /**
     * The log.
     */
    private final static Logger logger = LoggerFactory.getLogger(ValidateFileProcessor.class);

    // 记录各个文件头信息及对应的文件类型
    private static Map<String, String> mFileTypes = new HashMap<String, String>();

    static {
        // images
        mFileTypes.put("FFD8FFE0", ".jpg");
        mFileTypes.put("89504E47", ".png");
        mFileTypes.put("47494638", ".gif");
        mFileTypes.put("49492A00", ".tif");
        mFileTypes.put("424D", ".bmp");
        mFileTypes.put("FFD8FFE1", ".jpg");

        // PS和CAD
        mFileTypes.put("38425053", ".psd");
        mFileTypes.put("41433130", ".dwg"); // CAD
        mFileTypes.put("252150532D41646F6265", ".ps");

        // 办公文档类
        mFileTypes.put("D0CF11E0", ".doc"); // ppt、doc、xls
        mFileTypes.put("504B0304", ".docx");// pptx、docx、xlsx

        /** 注意由于文本文档录入内容过多，则读取文件头时较为多变-START **/
        mFileTypes.put("0D0A0D0A", ".txt");// txt
        mFileTypes.put("0D0A2D2D", ".txt");// txt
        mFileTypes.put("0D0AB4B4", ".txt");// txt
        mFileTypes.put("B4B4BDA8", ".txt");// 文件头部为汉字
        mFileTypes.put("73646673", ".txt");// txt,文件头部为英文字母
        mFileTypes.put("32323232", ".txt");// txt,文件头部内容为数字
        mFileTypes.put("0D0A09B4", ".txt");// txt,文件头部内容为数字
        mFileTypes.put("3132330D", ".txt");// txt,文件头部内容为数字
        /** 注意由于文本文档录入内容过多，则读取文件头时较为多变-END **/

        mFileTypes.put("7B5C727466", ".rtf"); // 日记本

        mFileTypes.put("255044462D312E", ".pdf");

        // 视频或音频类
        mFileTypes.put("3026B275", ".wma");
        mFileTypes.put("00000020", ".mp4");
        mFileTypes.put("57415645", ".wav");
        mFileTypes.put("41564920", ".avi");
        mFileTypes.put("4D546864", ".mid");
        mFileTypes.put("2E524D46", ".rm");
        mFileTypes.put("000001BA", ".mpg");
        mFileTypes.put("000001B3", ".mpg");
        mFileTypes.put("6D6F6F76", ".mov");
        mFileTypes.put("3026B2758E66CF11", ".asf");

        // 压缩包
        mFileTypes.put("52617221", ".rar");
        mFileTypes.put("1F8B08", ".gz");

        // 程序文件
        mFileTypes.put("3C3F786D6C", ".xml");
        mFileTypes.put("68746D6C3E", ".html");
        mFileTypes.put("7061636B", ".java");
        mFileTypes.put("3C254020", ".jsp");
        mFileTypes.put("4D5A9000", ".exe");

        mFileTypes.put("44656C69766572792D646174653A", ".eml"); // 邮件
        mFileTypes.put("5374616E64617264204A", ".mdb");// Access数据库文件

        mFileTypes.put("46726F6D", ".mht");
        mFileTypes.put("4D494D45", ".mhtml");

        mFileTypes.put("30820B38", ".p12");

    }

    /**
     * 检查文件 大小 和 格式
     */
    public static boolean validateFile(String fileName, long fileSize) {

        //获取后缀名
        String extName = getFileExtName(fileName);

        AttachmentDefinition attachmentDefinition = AttachmentDefinitionFactory.getInstance().getAttachmentDefinitionByExtName(extName);
        if (attachmentDefinition == null) {
            logger.warn("Can not find attachmentDefinition by file name " + fileName);
            return false;
        }

        if ((fileSize > attachmentDefinition.getMaxUploadSize())) {
            logger.warn("Can not uppload the file size" + fileSize + " which is big then the system setting " + attachmentDefinition.getMaxUploadSize());
            return false;
        }

        return true;
    }

    /**
     * 判断上传的文件是否合法 （一）、第一：检查文件的扩展名， (二）、 第二：检查文件的MIME类型 。 (三) 检查文件 大小 和 格式
     *
     * @return boolean
     */
    public static ValidateFileResult validateFile(InputStream in, String extName, long fileSize, ImageTypeEnum imageType) {
        boolean upFlag = false;// 为真表示符合上传条件，为假表标不符合
        logger.debug("#######上传的文件后缀: {}, 文件大小: {}", extName, fileSize);

        if (fileSize <= 0) {
            logger.warn("上传的文件不能为空");
            return new ValidateFileResult(upFlag, in);
        }


        AttachmentDefinition attachmentDefinition = AttachmentDefinitionFactory.getInstance().getAttachmentDefinitionByType(imageType);
        if (attachmentDefinition == null) {
            logger.warn("上传的文件类型错误");
            return new ValidateFileResult(upFlag, in);
        }

        if (fileSize > attachmentDefinition.getMaxUploadSize()) {
            logger.warn("上传的文件超出设定的大小");
            return new ValidateFileResult(upFlag, in);
        }

        /** 返回在此字符串中最右边出现的指定子字符串的索引 **/
        List<String> list = attachmentDefinition.getAllowUploadType();
        if (!list.contains(extName)) {
            logger.warn("上传的文件格式错误");
            return new ValidateFileResult(upFlag, in);
        }

        /**
         * 第二步：获取上传附件的文件头，判断属于哪种类型,并获取其扩展名 直接读取文件的前几个字节，来判断上传文件是否符合格式 防止上传附件变更扩展名绕过校验
         ***/
        String req_fileType = null;

        byte[] b = null;
        try {
            b = new byte[in.available()]; // 注意:InputStream只能读取一次,读完之后信息不能再读,所以要拷贝出来
            in.read(b);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        req_fileType = getFileType(b);
        logger.debug("用户上传的文件类型: {}", req_fileType);
        if (req_fileType != null && !"".equals(req_fileType) && !"null".equals(req_fileType)) {
            /** 第四步：校验上传的文件扩展名，是否在其规定范围内 **/
            if (list.contains(req_fileType)) {
                upFlag = true;
            } else {
                upFlag = false;
            }
        } else {
            /** 特殊情况校验,如果用户上传的扩展名为,文本文件,则允许上传-START **/
            if (extName.indexOf(".txt") != -1) {
                upFlag = true;
            } else {
                upFlag = false;
            }
            /** 特殊情况校验,如果用户上传的扩展名为,文本文件,则允许上传-END **/
        }
        return new ValidateFileResult(upFlag, new ByteArrayInputStream(b), b.length);
    }

    /**
     * 根据文件的输入流获取文件头信息
     * <p>
     * 文件路径
     *
     * @return 文件头信息
     */
    private static String getFileType(byte[] b) {
        byte[] byets = new byte[4];
        for (int i = 0; i < byets.length; i++) {
            byets[i] = b[i];
        }
        return mFileTypes.get(getFileHeader(byets));
    }

    /**
     * 根据文件转换成的字节数组获取文件头信息
     * <p>
     * 文件路径
     *
     * @return 文件头信息
     */
    private static String getFileHeader(byte[] b) {
        String value = bytesToHexString(b);
        return value;
    }

    /**
     * 将要读取文件头信息的文件的byte数组转换成string类型表示 下面这段代码就是用来对文件类型作验证的方法，
     * 将字节数组的前四位转换成16进制字符串，并且转换的时候，要先和0xFF做一次与运算。
     * 这是因为，整个文件流的字节数组中，有很多是负数，进行了与运算后，可以将前面的符号位都去掉，
     * 这样转换成的16进制字符串最多保留两位，如果是正数又小于10，那么转换后只有一位， 需要在前面补0，这样做的目的是方便比较，取完前四位这个循环就可以终止了
     *
     * @return 文件头信息
     */
    private static String bytesToHexString(byte[] src) {
        StringBuilder builder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        String hv;
        for (int i = 0; i < src.length; i++) {
            // 以十六进制（基数 16）无符号整数形式返回一个整数参数的字符串表示形式，并转换为大写
            hv = Integer.toHexString(src[i] & 0xFF).toUpperCase();
            if (hv.length() < 2) {
                builder.append(0);
            }
            builder.append(hv);
        }
        System.out.println("获取文件头信息:" + builder.toString());
        return builder.toString();
    }

    /**
     * 获取文件的后缀名
     */
    public static String getFileExtName(String fileName) {
        if (AppUtils.isBlank(fileName)) {
            return "";
        }

        int index = fileName.lastIndexOf(".");
        if (index <= 0) {
            return "";
        }
        return fileName.substring(index).toLowerCase();
    }

	/**
	 * 获取文件的后缀名
	 */
	public static String getFileExtName(InputStream inputStream) {
		byte[] b = null;
		try {
			b = new byte[inputStream.available()]; // 注意:InputStream只能读取一次,读完之后信息不能再读,所以要拷贝出来
			inputStream.read(b);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return getFileType(b);
	}

    /**
     * 获取有效的文件后缀
     *
     * @return
     */
    public static String getValidateExtName(ImageTypeEnum imgType, String fileName) {
        List<String> allowUploadTypes = AttachmentDefinitionFactory.getInstance().getAttachmentDefinitionByType(imgType).getAllowUploadType();
        if (allowUploadTypes == null) {
            return "";//返回为空
        }
        String ext = getFileExtName(fileName);
        return allowUploadTypes.contains(ext) ? ext : "";
    }

}