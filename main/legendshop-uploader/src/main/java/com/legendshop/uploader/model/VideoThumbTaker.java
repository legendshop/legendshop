package com.legendshop.uploader.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * VideoThumbTaker.java 获取视频指定播放时间的图片
 */
public class VideoThumbTaker {
	protected String ffmpegApp;

	public VideoThumbTaker(String ffmpegApp) {
		this.ffmpegApp = ffmpegApp;
	}

	@SuppressWarnings("unused")
	/****
	 * 获取指定时间内的图片
	 * @param videoFilename:视频路径
	 * @param thumbFilename:图片保存路径
	 * @param width:图片长
	 * @param height:图片宽
	 * @param hour:指定时
	 * @param min:指定分
	 * @param sec:指定秒
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void getThumb(String videoFilename, String thumbFilename, int width, int height, int hour, int min, float sec) throws IOException,
			InterruptedException {
		ProcessBuilder processBuilder = new ProcessBuilder(ffmpegApp, "-y", "-i", videoFilename, "-vframes", "1", "-ss", hour + ":" + min + ":" + sec, "-f",
				"mjpeg", "-s", width + "*" + height, "-an", thumbFilename);

		Process process = processBuilder.start();

		InputStream stderr = process.getErrorStream();
		InputStreamReader isr = new InputStreamReader(stderr);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null)
			;
		process.waitFor();

		if (br != null)
			br.close();
		if (isr != null)
			isr.close();
		if (stderr != null)
			stderr.close();
	}

	public static void main(String[] args) {
//		VideoThumbTaker videoThumbTaker = new VideoThumbTaker("C:\\Users\\newway\\Desktop\\video\\ffmpeg.exe");
//		try {
//			videoThumbTaker.getThumb("http://www.paoword.com/static/www/imgs/emergency.mp4", "C:\\Users\\newway\\Desktop\\video\\thumbTest.png", 800, 600, 0, 0, 9);
//			System.out.println("over");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		VideoFirstThumbTaker videoThumbTaker = new VideoFirstThumbTaker("C:\\Users\\newway\\Desktop\\video\\ffmpeg.exe");
		try {
			videoThumbTaker.getThumb("http://utea.oss-cn-shenzhen.aliyuncs.com/2017/video/nanlinhuaxu.mp4", "C:\\Users\\newway\\Desktop\\video\\thumbTest3.png", 800, 600);
			System.out.println("over");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
