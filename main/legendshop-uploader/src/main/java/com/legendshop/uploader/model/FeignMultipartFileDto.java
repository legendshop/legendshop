package com.legendshop.uploader.model;

import java.io.Serializable;

import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.ImagePurposeEnum;
import com.legendshop.model.constant.ImageTypeEnum;

/**
 * 上传文件的封装类, 包括内容和base64之后的图片数据, 可以减少一次图片上传的调用,跟内容一起发布
 *
 */
public class FeignMultipartFileDto implements Serializable{

	private static final long serialVersionUID = -6015410115403010053L;
	
	private String userId;
	
	private String userName;
	
	private Long shopId;
	
	private String fileName;
	
	private String originalFilename;
	
	private Long fileSize;
	
	private String filePath;
	
	private AttachmentTypeEnum type;
	
	private ImageTypeEnum imgType;
	
	private ImagePurposeEnum purposeEnum;
	
	private Long imageWidth;
	
	private Long imageHeight;
	
	private Long treeId = 0l;
	
	/**
	 * base64之后的图片文件内容
	 */
	private String fileData;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public AttachmentTypeEnum getType() {
		return type;
	}

	public void setType(AttachmentTypeEnum type) {
		this.type = type;
	}

	public ImageTypeEnum getImgType() {
		return imgType;
	}

	public void setImgType(ImageTypeEnum imgType) {
		this.imgType = imgType;
	}

	public ImagePurposeEnum getPurposeEnum() {
		return purposeEnum;
	}

	public void setPurposeEnum(ImagePurposeEnum purposeEnum) {
		this.purposeEnum = purposeEnum;
	}

	public Long getImageWidth() {
		return imageWidth;
	}

	public void setImageWidth(Long imageWidth) {
		this.imageWidth = imageWidth;
	}

	public Long getImageHeight() {
		return imageHeight;
	}

	public void setImageHeight(Long imageHeight) {
		this.imageHeight = imageHeight;
	}

	public Long getTreeId() {
		return treeId;
	}

	public void setTreeId(Long treeId) {
		this.treeId = treeId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

}
