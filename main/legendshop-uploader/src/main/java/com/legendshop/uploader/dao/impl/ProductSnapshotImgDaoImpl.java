/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.uploader.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.ProductSnapshotImg;
import com.legendshop.uploader.dao.ProductSnapshotImgDao;

/**
 * 商品快照图片Dao
 */
@Repository
public class ProductSnapshotImgDaoImpl extends GenericDaoImpl<ProductSnapshotImg, Long> implements ProductSnapshotImgDao  {

	public ProductSnapshotImg getProductSnapshotImg(Long id){
		return getById(id);
	}
	
    public int deleteProductSnapshotImg(ProductSnapshotImg productSnapshotImg){
    	return delete(productSnapshotImg);
    }
	
	public Long saveProductSnapshotImg(ProductSnapshotImg productSnapshotImg){
		return save(productSnapshotImg);
	}
	
	public int updateProductSnapshotImg(ProductSnapshotImg productSnapshotImg){
		return update(productSnapshotImg);
	}

	@Override
	public ProductSnapshotImg getProductSnapshotImgByPath(String imgPath) {
		return this.getByProperties(new EntityCriterion().eq("imgPath", imgPath));
	}
	
 }
