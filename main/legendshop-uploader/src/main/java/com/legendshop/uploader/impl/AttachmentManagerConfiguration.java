package com.legendshop.uploader.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.legendshop.base.util.PhotoPathEnum;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.service.ProductSnapshotImgService;

/**
 * AttachmentManager装配
 *
 */
@Configuration
public class AttachmentManagerConfiguration {

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	private ProductSnapshotImgService productSnapshotImgService;

	@Resource(name = "scaleList")
	private Map<String, List<Integer>> scaleList;

	@Bean("attachmentManager")
	public AttachmentManager getAttachmentManager() {
		String attachmentType = propertiesUtil.getAttachmentType();
		AttachmentManager manager = null;
		if (PhotoPathEnum.OSS.name().equalsIgnoreCase(attachmentType)) {
			manager = new OSSAttachmentManagerImpl(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);
		} else if (PhotoPathEnum.HTTP.name().equalsIgnoreCase(attachmentType)) {
			manager = new HttpAttachmentManagerImpl(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);
		} else {// 默认采用local模式
			manager = new LocalAttachmentManagerImpl(propertiesUtil, attachmentService, scaleList, productSnapshotImgService);
		}

		return manager;
	}
}
