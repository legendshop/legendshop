package com.legendshop.processor.backrefund;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.legendshop.spi.service.PaymentRefundProcessor;

/**
 * 退款处理器
 *
 */
@Configuration
public class PaymentRefundProcessorConfiguratoin {

  @Resource(name = "aliPayRefundProcessor")
  private PaymentRefundProcessor aliPayRefundProcessor;

  @Resource(name = "tongLianPayRefundProcessor")
  private PaymentRefundProcessor tongLianPayRefundProcessor;

  @Resource(name = "unionPayRefundProcessor")
  private PaymentRefundProcessor unionPayRefundProcessor;

  @Resource(name = "tenDirectPayRefundProcessor")
  private PaymentRefundProcessor tenDirectPayRefundProcessor;

  @Resource(name = "weXinPayRefundProcessor")
  private PaymentRefundProcessor weXinPayRefundProcessor;

  @Resource(name = "weXinAppPayRefundProcessor")
  private PaymentRefundProcessor weXinAppPayRefundProcessor;
  
  @Resource(name = "weXinMiniProgramPayRefundProcessor")
  private PaymentRefundProcessor weXinMiniProgramPayRefundProcessor;

  @Bean(name = "paymentRefundProcessor")
  public Map<String, PaymentRefundProcessor> processorMap() {
    Map<String, PaymentRefundProcessor> map = new HashMap<String, PaymentRefundProcessor>();
    map.put("ALP", aliPayRefundProcessor);
    map.put("TONGLIAN_PAY", tongLianPayRefundProcessor);
    map.put("UNION_PAY", unionPayRefundProcessor);
    map.put("TDP", tenDirectPayRefundProcessor);
    map.put("WX_PAY", weXinPayRefundProcessor);
    map.put("WX_APP_PAY", weXinAppPayRefundProcessor);
    map.put("WX_MP_PAY", weXinMiniProgramPayRefundProcessor);
    return map;
  }

}
