/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.spi.service.LoginHistoryService;
import com.legendshop.util.AppUtils;

/**
 * 用户登录操作
 */
@Component("loginHistoryProcessor")
public class LoginHistoryProcessor extends BaseProcessor<LoginSuccess> {
	private final Logger log = LoggerFactory.getLogger(LoginHistoryProcessor.class);

	@Autowired
	private LoginHistoryService loginHistoryService;

	/**
	 * 记录登录历史
	 */
	@Override
	public void process(LoginSuccess loginSuccess) {
		if(AppUtils.isBlank(loginSuccess.getUserName()) || AppUtils.isBlank(loginSuccess.getIpAddress())) {
			log.warn("LoginHistoryProcessor without userName and ipAddress");
			return;
		}
		loginHistoryService.saveLoginHistory(loginSuccess);
	}


}