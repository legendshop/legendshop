/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.auction.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.model.entity.JobStatus;
import com.legendshop.spi.service.AuctionsService;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.spi.service.JobStatusService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * The Class AuctionsSchedulerJob.
 */
public class AuctionsSchedulerJob implements Job {

	private static final Logger LOG = LoggerFactory.getLogger(AuctionsSchedulerJob.class);

	private static JobStatusService jobStatusService;

	private static BiddersService biddersService;

	private static BiddersWinService biddersWinService;

	private static AuctionsService auctionsService;

	static {
		jobStatusService = (JobStatusService) ContextServiceLocator.getBean("jobStatusService");
		biddersService = (BiddersService) ContextServiceLocator.getBean("biddersService");
		biddersWinService = (BiddersWinService) ContextServiceLocator.getBean("biddersWinService");
		auctionsService = (AuctionsService) ContextServiceLocator.getBean("auctionsService");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String strData = dataMap.getString("jsonValue");
		if (AppUtils.isBlank(strData)) {
			return;
		}
		Auctions auctions = JSONUtil.getObject(strData, Auctions.class);
		if (AppUtils.isBlank(auctions)) {
			return;
		}

		// Date _now=new Date();
		// Date _endTime=auctions.getEndTime();
		/*
		 * if(_now.before(_endTime)){ //当前时间 < 结束时间 //没有超过有效期的不处理 return; }
		 */
		Long id = auctions.getId();
		if(id == null){
			LOG.error("没有找到对应的拍卖。");
			return;
		}
		String jobName = SchedulerConstant.AUCTIONS_SCHEDULER_ + id;
		JobStatus jobStatus = null;
		boolean isDelay = false;
		// 标识正在处理
		biddersService.disposeBids(id, 1);
		double curPrice = 1d;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" <= 准备处理拍卖活动中标结果 添加作业=> [活动id：").append(id).append("作业组：").append(SchedulerConstant.JOB_GROUP_NAME).append("] ");
			LOG.info(sb.toString());
			jobStatus = jobStatusService.getJobStatusByJobName(jobName);
			if (jobStatus == null || jobStatus.getStatus().intValue() == 0) {
				// 如果没有该JOB的记录，则插入，否则 更改JOB的状态为 running
				if (jobStatus == null) {
					jobStatus = new JobStatus();
					jobStatus.setJobName(jobName);
					jobStatus.setStatus(1L);
					jobStatus.setUpdateTime(new Date());
					Long jobStatusId = jobStatusService.saveJobStatus(jobStatus);
					jobStatus.setId(jobStatusId);
				} else {
					jobStatus.setStatus(1L);
					jobStatus.setUpdateTime(new Date());
					jobStatusService.updateJobStatus(jobStatus);
				}
				
				// 获取最新出价记录
				List<Bidders> bidders = biddersService.getBiddersByAuctionId(id, 0, 1);

				Bidders bid = null;
				if (AppUtils.isNotBlank(bidders)) {
					bid = bidders.get(0);
					if (bid != null) {
						isDelay = isDelay(auctions, bid);
						curPrice = bid.getPrice().doubleValue();
					}
				}
				if (!isDelay) { // 不需要延迟处理
					int result = auctionsService.finishAuction(id);
					if (result > 0 && bid != null) {
						BiddersWin biddersWin = new BiddersWin();
						biddersWin.setAId(id);
						biddersWin.setBidId(bid.getId());
						biddersWin.setProdId(auctions.getProdId());
						biddersWin.setSkuId(auctions.getSkuId());
						biddersWin.setUserId(bid.getUserId());
						biddersWin.setBitTime(new Date());
						biddersWin.setPrice(bid.getPrice());
						biddersWin.setStatus(BiddersWinStatusEnum.NOORDER.value());
						Long bidId = biddersWinService.saveBiddersWin(biddersWin);
						LOG.info(" 中标记录 bidId={} ", bidId);
					}
				}
			} else {
				LOG.info(" 拍卖活动中标调度已经有进程在使用 ");
			}
		} catch (Exception e) {
			LOG.error("拍卖异常");
			e.printStackTrace();
		} finally {
			if (jobStatus != null) {
				jobStatusService.deleteJobStatus(jobStatus);
			}
			SchedulerMamager.getInstance().removeJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME);
			if (isDelay) {
				int minutes = auctions.getDelayTime().intValue();
				Date _endTime = auctions.getEndTime();
				Date _nowTimeDate = getMinutes(_endTime, minutes);
				auctions.setEndTime(_nowTimeDate);
				String jsonValue = JSONUtil.getJson(auctions);
				// 如果需要延迟拍卖
				Long time = auctions.getEndTime().getTime() - 5000;
				Date earlier_date = new Date(time);
				SchedulerMamager.getInstance().addJob(jobName, SchedulerConstant.JOB_GROUP_NAME, SchedulerConstant.TRIGGER_GROUP_NAME,
						AuctionsSchedulerJob.class, earlier_date, jsonValue);
				LOG.info(" 拍卖活动进行了延迟处理操作,延迟时间推至:" + _nowTimeDate);
			}
			int crowdWatch = biddersService.getCrowdWatchAuctions(id);
			long bidCount = biddersService.findBidsCount(id);
			// 更新拍卖活动数据
			auctionsService.updateAuctions(crowdWatch, bidCount, curPrice, auctions.getEndTime(), id);
			// 清除标识正在处理
			biddersService.clearDisposeBids(id);
			if (!isDelay) {
				biddersService.clearCrowdWatchAuctions(id);
			}
			LOG.info(" 拍卖活动中标结果 处理结束 =>");
		}
	}

	/**
	 * 判断是否要延迟处理.
	 *
	 * @param auctions
	 *            the auctions
	 * @param bid
	 *            the bid
	 * @return true, if checks if is delay
	 */
	private boolean isDelay(Auctions auctions, Bidders bid) {
		if (auctions.getIsSecurity().intValue() == 1 && auctions.getDelayTime().intValue() > 0) { // 需要保证金
			if(AppUtils.isNotBlank(auctions.getFixedPrice())){
				if(bid.getPrice().doubleValue()>=auctions.getFixedPrice().doubleValue()){
					return false;
				}
			}
			Date _endTime = auctions.getEndTime();
			Date bitTime = bid.getBitTime();
			long[] diff = getDistanceTimes(_endTime, bitTime);
			long day = diff[0];
			long hour = diff[1];
			long min = diff[2];
			long sec = diff[3];
			if (day == 0 && hour == 0) {
				if ((min > 0 && min <= 2) || (min == 0 && sec > 0)) { // 说明是竞拍结束的前两分钟出价或者
																		// 结束的前几秒进行出价
					// 需要延迟处理
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 得到跟现在若干天时间，如果为负数则向前推.
	 *
	 * @param myDate
	 *            the my date
	 * @param minutes
	 *            the minutes
	 * @return the date
	 */
	private Date getMinutes(Date myDate, int minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(myDate);
		calendar.add(Calendar.MINUTE, minutes);
		return calendar.getTime();
	}

	/**
	 * 两个时间相差距离多少天多少小时多少分多少秒.
	 *
	 * @param one
	 *            活动过期时间
	 * @param two
	 *            中标时间
	 * @return long[] 返回值为：{天, 时, 分, 秒}
	 */
	public static long[] getDistanceTimes(Date one, Date two) {
		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		long time1 = one.getTime();
		long time2 = two.getTime();
		long diff;
		/*
		 * if(time1<time2) { diff = time2 - time1; } else { diff = time1 -
		 * time2; }
		 */
		diff = time1 - time2;
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		long[] times = { day, hour, min, sec };
		return times;
	}

}
