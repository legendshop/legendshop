/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.auction.scheduler;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 拍卖活动任务调度.
 *
 * @author Tony
 */
public class SchedulerMamager {
	
	/** The Constant log. */
	private  static final Logger log = LoggerFactory.getLogger(SchedulerMamager.class);
	
	/** The g scheduler factory. */
	private static  SchedulerFactory gSchedulerFactory = new StdSchedulerFactory(); 
	
	/** The single. */
	private static SchedulerMamager single = null;

	/**
	 * The Constructor.
	 */
	private SchedulerMamager() {
	
	}

	/**
	 * Gets the instance.
	 *
	 * @return the instance
	 */
	public static SchedulerMamager getInstance() {
		if (single == null) {

			synchronized (SchedulerMamager.class) {
				if (single == null)
					single = new SchedulerMamager();
			}
		}
		return single;
	}

    
    /**
     * 添加一个定时任务，使用默认的任务组名，触发器名，触发器组名.
     *
     * @param jobName 任务名
     * @param jobGroupName the job group name
     * @param triggerGroupName the trigger group name
     * @param jobClass 任务
     * @param _time the _time
     * @param jsonValue the json value
     */  
    public  void addJob(String jobName, String jobGroupName,String triggerGroupName,Class<? extends Job> jobClass, Date _time,String jsonValue) {  
        try {  
            Scheduler sched = gSchedulerFactory.getScheduler();  
            
            //用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, jobGroupName).build();
            
            jobDetail.getJobDataMap().put("jsonValue",jsonValue);
            
            //构建一个触发器，规定触发的规则
            Trigger trigger = TriggerBuilder.newTrigger()//创建一个新的TriggerBuilder来规范一个触发器
            		 .withIdentity(jobName, triggerGroupName)//给触发器起一个名字和组名
            		 .startAt(_time)
                     .build();
            
            //向Scheduler中添加job任务和trigger触发器
            sched.scheduleJob(jobDetail, trigger);
            // 启动  
            if (!sched.isShutdown()){  
                sched.start();  
            }  
        } catch (SchedulerException e) {  
            e.printStackTrace();  
            log.error("添加作业=> [作业名称：" + jobName + " 作业组：" + jobGroupName + "]=> [失败]");
        }  
    }  
    
    /**
     * 暂停作业.
     *
     * @param name the name
     * @param jobGroupName the job group name
     */
    public void pauseJob(String name,String jobGroupName){
        try {
        	Scheduler sched = gSchedulerFactory.getScheduler();  
            JobKey jobKey = JobKey.jobKey(name, jobGroupName);
            sched.pauseJob(jobKey);
            log.info("暂停作业=> [作业名称：" + name + " 作业组：" + jobGroupName + "] ");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("暂停作业=> [作业名称：" + name + " 作业组：" + jobGroupName + "]=> [失败]");
        }
    }
    
    /**
     * 恢复作业.
     *
     * @param name the name
     * @param group the group
     * @param jobGroupName the job group name
     */
    public void resumeJob(String name, String group,String jobGroupName){
        try {
        	Scheduler scheduler = gSchedulerFactory.getScheduler();
            JobKey jobKey = JobKey.jobKey(name, jobGroupName);         
            scheduler.resumeJob(jobKey);
            log.info("恢复作业=> [作业名称：" + name + " 作业组：" + jobGroupName + "] ");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("恢复作业=> [作业名称：" + name + " 作业组：" + jobGroupName + "]=> [失败]");
        }       
    }
    
    /**
     * 修改一个任务的触发时间(使用默认的任务组名，触发器名，触发器组名).
     *
     * @param jobName the job name
     * @param jobGroupName the job group name
     * @param triggerGroupName the trigger group name
     * @param _time the _time
     * @param jsonValue the json value
     */  
    public  void modifyJobTime(String jobName, String jobGroupName,String triggerGroupName,Date _time,String jsonValue) {  
    	   try {
    		   Scheduler sched = gSchedulerFactory.getScheduler();  
               //构建一个触发器，规定触发的规则
               Trigger trigger = sched.getTrigger(new TriggerKey(jobName, triggerGroupName));
               if(trigger == null) {  
                   return;  
               } 
    		   if(!trigger.getStartTime().equals(_time)){
    			   JobDetail jobDetail = sched.getJobDetail(new JobKey(jobName,jobGroupName));
    			   Class<? extends Job> objJobClass = jobDetail.getJobClass();  
                   removeJob(jobName,jobGroupName,triggerGroupName);
                   addJob(jobName,jobGroupName,triggerGroupName, objJobClass, _time,jsonValue);  
    		   }
               log.info("修改作业触发时间=> [作业名称：" + jobName + " 作业组：" + jobGroupName + "] ");
           } catch (SchedulerException e) {
               e.printStackTrace();
               log.error("修改作业触发时间=> [作业名称：" + jobName + " 作业组：" + jobGroupName + "]=> [失败]");
           }
    }  
  
    
    
    /**
     * 移除一个任务(使用默认的任务组名，触发器名，触发器组名).
     *
     * @param jobName the job name
     * @param jobGroupName the job group name
     * @param triggerGroupName the trigger group name
     */  
    public void removeJob(String jobName, String jobGroupName,String triggerGroupName) {  
        try {  
            Scheduler sched = gSchedulerFactory.getScheduler(); 
            sched.pauseTrigger(new TriggerKey(jobName, triggerGroupName));// 停止触发器  
            sched.unscheduleJob(new TriggerKey(jobName, triggerGroupName));// 移除触发器 
            	   // 删除任务  
           sched.deleteJob(new JobKey(jobName, jobGroupName));
        } catch (Exception e) {  
        	 e.printStackTrace();
             log.error("移除任务作业=> [作业名称：" + jobName + " 作业组：" + jobGroupName + "]=> [失败]");
        }  
    }  
    
    /**
     * 关闭所有定时任务.
     */  
    public  void shutdownJobs() {  
        try {  
            Scheduler sched = gSchedulerFactory.getScheduler();  
            if(sched!=null){
            	if(!sched.isShutdown()) {  
                    sched.shutdown();  
                } 
            }
             
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
    

}
