/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.promotor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.PromotionCommisService;
import com.legendshop.util.AppUtils;

/**
 * 订单佣金结算处理器
 * 
 */
@Component
/*@Scope("prototype")*/
public class OrderCommisSettleProcessor implements EventProcessor<List<SubItem>> {

	@Autowired
	private PromotionCommisService promotionCommisService;

	/**
	 * 处理逻辑.
	 *
	 */
	@Override
	public void process(List<SubItem> subItems) {
		if (AppUtils.isNotBlank(subItems)) {
			for (int i = 0; i < subItems.size(); i++) {
				promotionCommisService.settleOrderCommis(subItems.get(i));
			}
		}
	}
}
