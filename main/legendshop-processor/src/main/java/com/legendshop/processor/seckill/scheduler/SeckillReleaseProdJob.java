/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.seckill.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动定时器,释放秒杀商品的状态.
 * 上线，发布时需要加入定时任务
 * 
 * 已过时, 目前已经迁移到xxl_job
 */
@Deprecated
public class SeckillReleaseProdJob implements Job {

	private static final Logger LOG = LoggerFactory.getLogger(SeckillReleaseProdJob.class);

	private static ProductService productService;

	static {
		productService = (ProductService) ContextServiceLocator.getBean("productService");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String strData = dataMap.getString("jsonValue");
		if (AppUtils.isBlank(strData)) {
			return;
		}
		
		LOG.info(" <+===  秒杀活动任务调度，自动释放商品锁定   strData={}",strData);
		JSONObject jsonObject=JSON.parseObject(strData);
		Long seckillId=jsonObject.getLongValue("seckillId");
		Long shopId=jsonObject.getLongValue("shopId");
		
		// 已过时, 所以注释
		//productService.releaseSeckill(shopId);

	    LOG.info(" 处理结束 =>");
	}


}
