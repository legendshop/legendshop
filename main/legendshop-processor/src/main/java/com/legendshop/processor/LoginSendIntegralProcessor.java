/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.spi.service.IntegralService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 用户登录成功之后送积分操作
 */
@Component("loginSendIntegralProcessor")
public class LoginSendIntegralProcessor extends BaseProcessor<LoginSuccess> {

	@Autowired
	private IntegralService integralService;
	
	@Autowired
	private UserDetailService userDetailService;

	@Override
	public void process(LoginSuccess loginSuccess) {
		String loginUserType = loginSuccess.getLoginUserType();
		String userId=loginSuccess.getUserId();
		if(LoginUserTypeEnum.USER.value().equals(loginUserType)){
			Date lastLoginTime=userDetailService.getLastLoginTime(userId); //用户最后登录时间
			boolean config=false;
			if(AppUtils.isBlank(lastLoginTime)){
				config=true;
			}else{
				//将其按照以下格式转换成字符串
				SimpleDateFormat sdfLogin = new SimpleDateFormat("yyyy-MM-dd");
				//按照格式转换两个数据
				String lastTime = sdfLogin.format(lastLoginTime);
				String nowTime = sdfLogin.format(new Date());
				
				//判断不是同一天则加上积分，否则就不加
				if(!nowTime.equals(lastTime))
					config=true;
			}
			if(config){
				integralService.addScore(userId, SendIntegralRuleEnum.USER_LOGIN);
			}
		
		}
		//update user lasttime
		
		userDetailService.updateLastLoginTime(userId);
	}

	
}
