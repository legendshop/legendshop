package com.legendshop.processor.backrefund;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import com.legendshop.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.CashLog;
import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.PrePayTypeEnum;
import com.legendshop.model.constant.SubStatusEnum;
import com.legendshop.model.dto.BackRefundResponseDto;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.MergeGroupAdd;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.BackRefundProcessor;
import com.legendshop.spi.service.ExpensesRecordService;
import com.legendshop.spi.service.MergeGroupAddService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.SubSettlementService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.DateUtils;

/**
 * 未拼团成功的订单原路返回
 *
 */
@Service("mergeGroupBackRefundProcessor")
public class MergeGroupBackRefundProcessorImpl implements BackRefundProcessor {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(MergeGroupBackRefundProcessorImpl.class);
	
	/** 支付实现类 */
	@Resource(name="paymentRefundProcessor")
	private Map<String, PaymentRefundProcessor> processorMap;
	
	@Autowired
	private SubSettlementService subSettlementService;

	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private ExpensesRecordService expensesRecordService;
	
	@Autowired
	private PdCashLogService pdCashLogService;

	@Autowired
	private MergeGroupAddService mergeGroupAddService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private SubHistoryService subHistoryService;
	

	@Override
	public BackRefundResponseDto backRefund(String backRefund) {
		
		log.info(" ~~~~~~  开始拼团金额原路业务处理 ~~~~~~~ ");
		
		BackRefundResponseDto valueDto = new BackRefundResponseDto();
		valueDto.setResult(false);
		
		MergeGroupAdd groupAdd=mergeGroupAddService.getMergeGroupAdd(Long.valueOf(backRefund));
		if(AppUtils.isBlank(groupAdd)){
			valueDto.setMessage("未找到需要处理的拼团");
			return valueDto;
		}
		
		Sub sub=subService.getSubById(groupAdd.getSubId());
		
		if(AppUtils.isBlank(sub)){
			log.warn(" 未找到 ls_merge_group_add id = {} 的订单记录 ",groupAdd.getId());
			valueDto.setMessage("未找到需要处理的拼团");
			return valueDto;
		}
		
		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(sub.getSubSettlementSn());
		Integer prePayType=subSettlement.getPrePayType();
		
		/*
		 * 退款情况： 情况一：全额的平台支付； 情况二：混合支付[预付款+支付宝]合并支付,形成两个支付订单，其中用户退款其中一个订单 如：
		 * 100预付款+100元支付宝; 订单A=50元 订单B=150元； 其中线下处理 情况三: 单纯的第三方平台支付 如:
		 * 100元支付宝支付;
		 */
		if (subSettlement.isFullPay()) {
			// 情况一：全额的平台支付；
			RefundLog.info(" ############### 该笔退款的支付单据是全款平台支付方式,直接返回用户的平台账户 subSettlementSn={} refundSn={}  ##############   ");
			if (PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)) { // 预付款支付
				RefundLog.info(" ############### 全款平台支付方式  = 预付款支付  ##############   ");
				boolean prePayReturnResult = plateformPrePayReturn(
						groupAdd.getUserId(),
						groupAdd.getUserName(),
						subSettlement.getPdAmount(),
						sub.getSubNumber());
				if (!prePayReturnResult) {
					valueDto.setMessage("退款失败 ");
					return valueDto;
				}
				Date time = new Date();
				String timeStr = DateUtil.DateToString(time,"yyyy-MM-dd HH:mm:ss");

				String historyReason = "系统于" + timeStr + "取消订单，原因：拼团未成功";
				String subReason = "系统取消订单，原因：拼团未成功";

				groupAdd.setStatus(3);
				groupAdd.setOutRefundNo(subSettlement.getFlowTradeNo());
				groupAdd.setPayTypeId(subSettlement.getPayTypeId());
				mergeGroupAddService.updateMergeGroupAdd(groupAdd);
				
				sub.setMergeGroupStatus(-1);
				sub.setStatus(OrderStatusEnum.CLOSE.value());
				sub.setCancelReason(subReason);
				sub.setUpdateDate(new Date());
				subService.updateSub(sub);
				
				SubHistory subHistory = new SubHistory();

				subHistory.setRecDate(time);
				subHistory.setStatus(SubStatusEnum.CHANGE_STATUS.value());
				subHistory.setSubId(sub.getSubId());
				subHistory.setUserName("系统");

				subHistory.setReason(historyReason);
				subHistoryService.saveSubHistory(subHistory);
				
				
				RefundLog.info(" ############### 全款平台支付方式  = 预付款支付 退款成功  ##############   ");
				valueDto.setResult(true);
				valueDto.setMessage("退款成功 ");
				return valueDto;
				
			} 
		}
		
		RefundLog.info("开始第三方支付退款处理  payTypeId={}", subSettlement.getPayTypeId());
		PaymentRefundProcessor refundProcessor = processorMap.get(subSettlement.getPayTypeId());
		RefundLog.info("开始第三方支付退款处理  refundProcessor = {}, ", refundProcessor);
		if (AppUtils.isBlank(refundProcessor)) {
			RefundLog.warn(" ###### refundProcessor is null for PayTypeId {}  ##### ", subSettlement.getPayTypeId());
		}
		
		String outRefundNo=CommonServiceUtil.getRandomSn();
		Double refundAmount = subSettlement.getCashAmount().doubleValue();
		Double pdAmount=subSettlement.getPdAmount();
		
		SysSubReturnForm returnForm=new SysSubReturnForm();
		returnForm.setOutRequestNo(outRefundNo);
		returnForm.setFlowTradeNo(subSettlement.getFlowTradeNo());
		returnForm.setSubSettlementSn(subSettlement.getSubSettlementSn());
		returnForm.setReturnAmount(refundAmount);
		returnForm.setRefundTypeId(subSettlement.getPayTypeId());
		returnForm.setOutRequestNo(outRefundNo);
		returnForm.setPayAmount(subSettlement.getCashAmount());
		
		if(refundProcessor != null) {
			RefundLog.info(" ############### 第三方退款处理  ##############   ");
			Map<String,Object> refundMap = refundProcessor.refund(returnForm);
			
			if(AppUtils.isNotBlank(refundMap) && !Boolean.valueOf(refundMap.get("result").toString())){
				RefundLog.error("第三方支付退款处理 失败 结果: {} ", refundMap.get("message"));
				valueDto.setMessage((String) refundMap.get("message"));
				return valueDto;
			}
		}
		
		//TODO 这里需要改造,因为是外部系统,会有事务问题，外部退款成功，内部退款不成功问题
		if( pdAmount >0 ){ //需要退的总金额大于第三方已退金额，那么还需要平台退款
			if(PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)){ //预付款支付
				RefundLog.info(" ############### 预付款支付退款  ##############   ");
				boolean prePayReturnResult = plateformPrePayReturn(
						groupAdd.getUserId(),
						groupAdd.getUserName(),
						subSettlement.getPdAmount(),
						sub.getSubNumber());
        		if(!prePayReturnResult){
        			valueDto.setMessage("退款失败 ");
					return valueDto;
        		}
        		RefundLog.info(" ############### 预付款支付退款成功  ##############   ");
        	}
		}
		Date time = new Date();
		String timeStr = DateUtil.DateToString(time,"yyyy-MM-dd HH:mm:ss");

		String historyReason = "系统于" + timeStr + "取消订单，原因：拼团未成功";
		String subReason = "系统取消订单，原因：拼团未成功";

		groupAdd.setStatus(3);
		groupAdd.setOutRefundNo(outRefundNo);
		groupAdd.setPayTypeId(subSettlement.getPayTypeId());
		mergeGroupAddService.updateMergeGroupAdd(groupAdd);
		sub.setCancelReason(subReason);
		sub.setMergeGroupStatus(-1);
		sub.setStatus(OrderStatusEnum.CLOSE.value());
		sub.setUpdateDate(new Date());
		subService.updateSub(sub);
		
		SubHistory subHistory = new SubHistory();

		subHistory.setRecDate(time);
		subHistory.setStatus(SubStatusEnum.CHANGE_STATUS.value());
		subHistory.setSubId(sub.getSubId());
		subHistory.setUserName("系统");

		subHistory.setReason(historyReason);
		subHistoryService.saveSubHistory(subHistory);
		
		valueDto.setResult(true);
		valueDto.setMessage("退款成功 ");
		
		log.info(" ~~~~~~  拼团金额原路业务处理成功 ~~~~~~~ ");
		
		return valueDto;
		
	}
	
	public boolean plateformPrePayReturn(String userId,String userName, Double refundAmount,String sn){
		UserDetail userDetail = userDetailService.getUserDetailByidForUpdate(userId);
		Double originAmount = userDetail.getAvailablePredeposit();
		if (originAmount == null) {
			originAmount = 0d;
		}
		Double available_predeposit = Arith.add(originAmount, refundAmount);
		int count = userDetailService.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return false;
		}
		
		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
		pdCashLog.setAmount( refundAmount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc("拼团自动退款");
		pdCashLog.setSn(sn);
		pdCashLogService.savePdCashLog(pdCashLog);
		CashLog.log("  recharge For PaySucceed  flow success -----！>  ");
		/*
		 * 消费记录
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(refundAmount);
		expensesRecord.setRecordRemark("订单预付款退款");
		expensesRecord.setUserId(userId);
		expensesRecordService.saveExpensesRecord(expensesRecord);
		return true;
	}

}
