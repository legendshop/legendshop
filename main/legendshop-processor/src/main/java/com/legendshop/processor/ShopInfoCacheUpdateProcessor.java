/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.ShopDecotateCacheService;
import com.legendshop.spi.service.ShopDecotateService;
import com.legendshop.spi.service.ShopLayoutService;
import com.legendshop.util.AppUtils;

/**
 * 店铺信息缓存更新处理器.
 */
@Component
public class ShopInfoCacheUpdateProcessor implements EventProcessor<ShopDetail> {

	@Autowired
	private ShopDecotateService shopDecotateService;
	
	@Autowired
	private ShopDecotateCacheService shopDecotateCacheService;
	
	@Autowired
	private ShopLayoutService shopLayoutService;
	
	@Override
	@Async
	public void process(ShopDetail detail) {
		if(detail==null){
			return ;
		}
		
		/*
		 * 获取有没有装修过
		 */
		Long count= shopLayoutService.getLayoutCountOnline(detail.getId(), null);
    	if(count==0){
    		StoreListDto storeDto=shopDecotateService.findStoreListDto(detail.getId());
    		if(AppUtils.isNotBlank(storeDto)){
    			StoreListDto  storeListDto=shopDecotateService.findShopStoreListDtoCache(detail.getShopId());
    			ShopLayoutShopInfoDto shopDefaultInfo=storeListDto.getShopDefaultInfo();
    			if(AppUtils.isNotBlank(storeListDto)){
    				if(AppUtils.isNotBlank(shopDefaultInfo)){
    					shopDefaultInfo.setAddr(detail.getShopAddr());
    					shopDefaultInfo.setArea(detail.getArea());
    					shopDefaultInfo.setBannerPic(detail.getBannerPic());
    					shopDefaultInfo.setBriefDesc(detail.getBriefDesc());
    					shopDefaultInfo.setCity(detail.getCity());
    					shopDefaultInfo.setContactMobile(detail.getContactMobile());
    					shopDefaultInfo.setCredit(detail.getCredit());
    					shopDefaultInfo.setProvince(detail.getProvince());
    					//shopDefaultInfo.setQq(detail.getQqList());
    					shopDefaultInfo.setShopAddr(detail.getShopAddr());
    					shopDefaultInfo.setShopPic(detail.getShopPic());
    					shopDefaultInfo.setSiteName(detail.getSiteName());
    					
    					shopDecotateCacheService.cachePutShopStoreListDto(storeListDto);
    				}
    			}
    		}
    	}else{
    		ShopDecotateDto  decotateDto=shopDecotateService.findShopCache(detail.getShopId());
    		if(AppUtils.isNotBlank(decotateDto)){
    			ShopLayoutShopInfoDto shopDefaultInfo=decotateDto.getShopDefaultInfo();
    			if(AppUtils.isNotBlank(shopDefaultInfo)){
    				shopDefaultInfo.setAddr(shopDefaultInfo.getAddr());
    				shopDefaultInfo.setArea(shopDefaultInfo.getArea());
    				shopDefaultInfo.setBannerPic(detail.getBannerPic());//对应广告图，针对图形不匹配的bug
    				shopDefaultInfo.setBriefDesc(detail.getBriefDesc());
    				shopDefaultInfo.setCity(detail.getCity());
    				shopDefaultInfo.setContactMobile(detail.getContactMobile());
    				shopDefaultInfo.setCredit(detail.getCredit());
    				shopDefaultInfo.setProvince(detail.getProvince());
    				//shopDefaultInfo.setQq(detail.getQqList());
    				shopDefaultInfo.setShopAddr(detail.getShopAddr());
    				shopDefaultInfo.setShopPic(detail.getShopPic());
    				shopDefaultInfo.setSiteName(detail.getSiteName());
//					decotateDto.setIsBanner(detail.getShopPic2() == null ? 0 : 1);
    				
    				shopDecotateCacheService.cachePutShopDecotate(decotateDto);
    			}
    		}
    		
    	}
	
	}
}
