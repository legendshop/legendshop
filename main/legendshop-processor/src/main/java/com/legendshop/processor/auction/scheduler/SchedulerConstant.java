/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.auction.scheduler;

/**
 * The Class SchedulerConstant.
 */
public class SchedulerConstant {

	/** The Constant JOB_GROUP_NAME. */
	public final static String JOB_GROUP_NAME = "JOBGROUP_NAME";

	/** The Constant TRIGGER_GROUP_NAME. */
	public final static String TRIGGER_GROUP_NAME = "TRIGGERGROUP_NAME";

	//拍卖定时器的名称前缀
	public final static String AUCTIONS_SCHEDULER_ = "AUCTIONS_SCHEDULER_";

	//秒杀定时器的名称前缀
	public final static String SECKILL_SCHEDULER_ = "SECKILL_SCHEDULER_";
	
	//取消秒杀资格定时器的名称前缀
	public final static String CANCLE_SECKILL_SCHEDULER_ = "CANCLE_SECKILL_SCHEDULER_";

	//剩余库存和未支付订单库存回滚前缀
	public final static String SECKILL_STOCK_ROLLBACK_SCHEDULER_ = "SECKILL_STOCK_ROLLBACK_SCHEDULER_";

}
