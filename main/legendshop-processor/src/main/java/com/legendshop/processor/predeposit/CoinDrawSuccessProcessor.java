/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.predeposit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.vo.DrawRecharge;
import com.legendshop.spi.service.UserDetailService;

/**
 * 金币退款
 * 
 */
@Component
public class CoinDrawSuccessProcessor implements EventProcessor<DrawRecharge> {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(CoinDrawSuccessProcessor.class);

	@Autowired
	private UserDetailService userDetailService;

	@Override
	public void process(DrawRecharge task) {
		log.info("CoinRefundPaySuccessProcessor process calling, task= " + task);
		if (task != null) {
			String userId = task.getUserId();
			Double amount = task.getAmount();
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			String result = userDetailService.refundCoin(userDetail, amount);
			task.setResult(result);
		}
	}
}
