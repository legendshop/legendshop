/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.UserEvent;
import com.legendshop.spi.service.EventService;

/**
 * 用户的操作历史
 */
@Component
public class UserEventProcessor implements EventProcessor<UserEvent> {
	
	private final Logger logger = LoggerFactory.getLogger(UserEventProcessor.class);
	
	@Autowired
	private EventService eventService;
	
	/**
	 * 动作执行器
	 */
	@Override
	@Async
	public void process(UserEvent event) {
		if(event != null && event.getUserId() != null){
			eventService.saveEvent(event);
		}else {
			logger.warn("UserEventProcessor process without userId");
		}
	}

}
