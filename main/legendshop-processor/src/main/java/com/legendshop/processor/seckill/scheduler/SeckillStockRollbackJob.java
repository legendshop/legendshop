/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.seckill.scheduler;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动定时器,剩余库存和未支付订单库存回滚.
 * 上线，发布时需要加入定时任务
 */
public class SeckillStockRollbackJob implements Job {

	private static final Logger LOG = LoggerFactory.getLogger(SeckillStockRollbackJob.class);

	private static SeckillActivityService seckillActivityService;
	private static SubService subService;
	private static SeckillService seckillService;
	
	static {
		seckillActivityService = (SeckillActivityService) ContextServiceLocator.getBean("seckillActivityService");
		subService = (SubService) ContextServiceLocator.getBean("subService");
		seckillService = (SeckillService) ContextServiceLocator.getBean("seckillService");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String strData = dataMap.getString("jsonValue");
		if (AppUtils.isBlank(strData)) {
			return;
		}
		
		LOG.info(" <+===  秒杀活动任务调度，剩余库存和未支付订单库存回滚   strData={}",strData);
		JSONObject jsonObject=JSON.parseObject(strData);
		Long seckillId=jsonObject.getLongValue("seckillId");

		//获取超时未支付秒杀订单
		List<Sub> subs = subService.getUnpaymentseckillOrder(seckillId);
		if(AppUtils.isNotBlank(subs)){
			//关闭超时未支付秒杀订单
			subService.finishUnPay(subs);
			//循环取消秒杀资格
			for (Sub sub : subs) {
				seckillService.cancleUnpaymentSeckillOrder(seckillId, sub.getUserId());
			}
		}

		// TODO 定时器已经执行了该方法，将缓存重新放入商品中
		//boolean result =  seckillActivityService.stockRollback(seckillId);
		//if(!result){
		//	 LOG.info(" 库存回滚失败 =>");
		//}
		//

	    LOG.info(" 处理结束 =>");
	}


}
