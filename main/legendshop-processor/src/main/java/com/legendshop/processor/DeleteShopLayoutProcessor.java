/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.spi.service.ShopDecotateCacheService;
import com.legendshop.spi.service.ShopLayoutService;
import com.legendshop.util.JSONUtil;

/**
 * 删除商品的布局
 *
 */
@Component
public class DeleteShopLayoutProcessor implements EventProcessor<List<ShopLayout>> {

	private final Logger log = LoggerFactory.getLogger(DeleteShopLayoutProcessor.class);
	
	@Autowired
	private ShopDecotateCacheService shopDecotateCacheService;

	@Autowired
	private ShopLayoutService shopLayoutService;
	
	@Override
	@Async
	public void process(List<ShopLayout> deleteShopLayouts) {
		 log.debug("<!--- DeleteShopLayoutProcessor calling object = {}",JSONUtil.getJson(deleteShopLayouts));
		 for (ShopLayout shopLayout : deleteShopLayouts) {
			 shopLayoutService.deleteFullShopLayout(shopLayout);
		 }
		 
		 log.debug("clear shop decorate all cache---------- ");
		 shopDecotateCacheService.clearAllDecCache();
		 log.debug("DeleteShopLayoutProcessor success -----!> ");

	}
}
