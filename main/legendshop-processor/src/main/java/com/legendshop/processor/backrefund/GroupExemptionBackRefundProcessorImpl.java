package com.legendshop.processor.backrefund;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import cn.hutool.core.util.NumberUtil;
import com.legendshop.model.constant.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.CashLog;
import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.dto.BackRefundResponseDto;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 * 团长免单原路返回
 *
 */
@Service("groupExemptionBackRefundProcessor")
public class GroupExemptionBackRefundProcessorImpl implements BackRefundProcessor {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(GroupExemptionBackRefundProcessorImpl.class);


	@Autowired
	private SubService subService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private PdCashLogService pdCashLogService;
	
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private MergeGroupAddService mergeGroupAddService;

	@Autowired
	private SubSettlementService subSettlementService;
	
	@Autowired
	private ExpensesRecordService expensesRecordService;
	/** 支付实现类 */
	@Resource(name="paymentRefundProcessor")
	private Map<String, PaymentRefundProcessor> processorMap;

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
	private SubRefundReturnDetailService subRefundReturnDetailService;
	

	@Override
	@Transactional
	public BackRefundResponseDto backRefund(String backRefund) {
		
		log.info(" ~~~~~~  开始拼团活动原路业务处理 ~~~~~~~ ");
		
		BackRefundResponseDto valueDto = new BackRefundResponseDto();
		valueDto.setResult(false);
		
		MergeGroupAdd groupAdd=mergeGroupAddService.getMergeGroupAdd(Long.valueOf(backRefund));
		if(AppUtils.isBlank(groupAdd)){
			valueDto.setMessage("未找到需要处理的拼团");
			return valueDto;
		}
		
		Sub sub=subService.getSubById(groupAdd.getSubId());

		if(AppUtils.isBlank(sub)){
			log.warn(" 未找到 ls_merge_group_add id = {} 的订单记录 ",groupAdd.getId());
			valueDto.setMessage("未找到需要处理的拼团");
			return valueDto;
		}

		BigDecimal actualTotal = new BigDecimal(sub.getActualTotal());
		SubRefundReturn number = subRefundReturnService.getSubRefundReturnBySubNumber(sub.getSubNumber());
		if (number != null){
			if (!(actualTotal.compareTo(number.getRefundAmount()) == 1)){
				log.warn(" 已发起了退款 ");
				valueDto.setMessage("已发起了退款");
				return valueDto;
			}
		}

		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(sub.getSubSettlementSn());

		List<SubItem> subItemList = subItemService.getSubItem(sub.getSubNumber());
		SubItem subItem = new SubItem();
		if(CollectionUtils.isEmpty(subItemList) || subItemList.size() != 1){
			log.warn(" 未找到 退款订单项 subNumber = {} ",sub.getSubNumber());
			valueDto.setMessage("未找到需要处理的订单项");
			return valueDto;
		}
		subItem = subItemList.get(0);
		SubRefundReturn subRefundReturn = new SubRefundReturn();
		try {
			// 生成退款记录 By xslong 2020-4-15
			subRefundReturn.setSubId(sub.getSubId());
			subRefundReturn.setSubNumber(sub.getSubNumber());
			subRefundReturn.setSubItemId(subItem.getSubItemId());
			subRefundReturn.setSubMoney(BigDecimal.valueOf(sub.getActualTotal()));
			subRefundReturn.setSubSettlementSn(subSettlement.getSubSettlementSn());
			subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(), "yyyyMMdd"));
			subRefundReturn.setFlowTradeNo(sub.getFlowTradeNo());
			subRefundReturn.setSubItemMoney(subItem.getActualAmount());//订单项的金额
			subRefundReturn.setOrderDatetime(sub.getPayDate());
			subRefundReturn.setPayTypeId(sub.getPayTypeId());
			subRefundReturn.setPayTypeName(sub.getPayTypeName());
			subRefundReturn.setShopId(sub.getShopId());
			subRefundReturn.setShopName(sub.getShopName());
			subRefundReturn.setUserId(sub.getUserId());
			subRefundReturn.setUserName(sub.getUserName());
			subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_AGREE.value());
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_FINISH.value());
			subRefundReturn.setApplyTime(new Date());
			subRefundReturn.setBuyerMessage("拼团团长免单");
			subRefundReturn.setReasonInfo("拼团团长免单");
			subRefundReturn.setRefundSouce(RefundSouceEnum.PLATFORM.value());
			subRefundReturn.setIsPresell(false);
			subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
			subRefundReturn.setIsHandleSuccess(RefundReturnStatusEnum.HANDLE_SUCCESS.value());
			subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());
			double sub1 = NumberUtil.sub(sub.getActualTotal(), sub.getFreightAmount());
			BigDecimal refundAmount = NumberUtil.div(new BigDecimal(sub1), sub.getProductNums());
			subRefundReturn.setRefundAmount(refundAmount);
			subRefundReturn.setGoodsNum(1L);
			subRefundReturn.setSkuId(subItem.getSkuId());
			subRefundReturn.setProductId(subItem.getProdId());
			subRefundReturn.setIsBill(false);
			subRefundReturn.setAdminTime(new Date());

			Long subRefundReturnId = subRefundReturnService.saveSubRefundReturn(subRefundReturn);

			SubRefundReturnDetail subRefundReturnDetail = new SubRefundReturnDetail();
			subRefundReturnDetail.setRefundId(subRefundReturnId);
			subRefundReturnDetail.setRefundType("拼团团长免单");
			subRefundReturnDetail.setPayType(sub.getPayTypeName());
			subRefundReturnDetail.setRefundAmount(subRefundReturn.getRefundAmount());
			subRefundReturnDetail.setRefundStatus(Boolean.TRUE);
			Long subRefundReturnDetailId = subRefundReturnDetailService.saveSubRefundReturnDetail(subRefundReturnDetail);

		} catch (Exception e) {
			log.error("{}", e);
			log.warn("团长免单退款记录！错误 subNumber：{}", sub.getSubNumber());
		}

		Integer prePayType=subSettlement.getPrePayType();
		
		/*
		 * 退款情况： 情况一：全额的平台支付； 情况二：混合支付[预付款+支付宝]合并支付,形成两个支付订单，其中用户退款其中一个订单 如：
		 * 100预付款+100元支付宝; 订单A=50元 订单B=150元； 其中线下处理 情况三: 单纯的第三方平台支付 如:
		 * 100元支付宝支付;
		 */
		if (subSettlement.isFullPay()) {
			// 情况一：全额的平台支付；
			RefundLog.info(" ############### 该笔退款的支付单据是全款平台支付方式,直接返回用户的平台账户 subSettlementSn={} refundSn={}  ##############   ");
			if (PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)) { // 预付款支付
				RefundLog.info(" ############### 全款平台支付方式  = 预付款支付  ##############   ");
				boolean prePayReturnResult = plateformPrePayReturn(
						groupAdd.getUserId(),
						groupAdd.getUserName(),
						subSettlement.getPdAmount(),//当付款为全额预存款付款时，金额是存在PdAmount
						sub.getSubNumber());
				if (!prePayReturnResult) {
					valueDto.setMessage("退款失败 ");
					return valueDto;
				}
				
				//groupAdd.setStatus(3);
				groupAdd.setExemption(true);
				groupAdd.setOutRefundNo(subSettlement.getFlowTradeNo());
				groupAdd.setPayTypeId(subSettlement.getPayTypeId());
				mergeGroupAddService.updateMergeGroupAdd(groupAdd);
				
				RefundLog.info(" ############### 全款平台支付方式  = 预付款支付 退款成功  ##############   ");
				valueDto.setResult(true);
				valueDto.setMessage("退款成功 ");
				return valueDto;
			} 
		}
		
		RefundLog.info("开始第三方支付退款处理  payTypeId={}", subSettlement.getPayTypeId());
		PaymentRefundProcessor refundProcessor = processorMap.get(subSettlement.getPayTypeId());
		RefundLog.info("开始第三方支付退款处理  refundProcessor ={}", refundProcessor);
		if (AppUtils.isBlank(refundProcessor)) {
			RefundLog.warn(" ###### refundProcessor is null for PayTypeId {}  ##### ", subSettlement.getPayTypeId());
		}
		
		String outRefundNo=CommonServiceUtil.getRandomSn();
		double sub1 = NumberUtil.sub(sub.getActualTotal(), sub.getFreightAmount());
		BigDecimal refundAmount = NumberUtil.div(new BigDecimal(sub1), sub.getProductNums());
		//Double refundAmount = subSettlement.getCashAmount().doubleValue();
		Double pdAmount=subSettlement.getPdAmount();
		
		SysSubReturnForm returnForm=new SysSubReturnForm();
		returnForm.setOutRequestNo(outRefundNo);
		returnForm.setFlowTradeNo(subSettlement.getFlowTradeNo());
		returnForm.setSubSettlementSn(subSettlement.getSubSettlementSn());
		returnForm.setReturnAmount(refundAmount.doubleValue());
		returnForm.setRefundTypeId(subSettlement.getPayTypeId());
		returnForm.setOutRequestNo(outRefundNo);
		returnForm.setPayAmount(subSettlement.getCashAmount());
		
		if(refundProcessor != null) {
			RefundLog.info(" ############### 第三方退款处理  ##############   ");
			Map<String,Object> refundMap = refundProcessor.refund(returnForm);
			if(AppUtils.isNotBlank(refundMap) && !Boolean.valueOf(refundMap.get("result").toString())){
				RefundLog.error("第三方支付退款处理 失败 结果: {} ", refundMap.get("message"));
				valueDto.setMessage((String) refundMap.get("message"));
				return valueDto;
			}
		}
		
		//TODO 这里需要改造,因为是外部系统,会有事务问题，外部退款成功，内部退款不成功问题
		if( pdAmount >0 ){ //需要退的总金额大于第三方已退金额，那么还需要平台退款
			if(PrePayTypeEnum.PREDEPOSIT.value().equals(prePayType)){ //预付款支付
				RefundLog.info(" ############### 预付款支付退款  ##############   ");
				boolean prePayReturnResult = plateformPrePayReturn(
						groupAdd.getUserId(),
						groupAdd.getUserName(),
						subSettlement.getPdAmount(),
						sub.getSubNumber());
        		if(!prePayReturnResult){
        			valueDto.setMessage("退款失败 ");
					return valueDto;
        		}
        		RefundLog.info(" ############### 预付款支付退款成功  ##############   ");
        	}
		}
		
		groupAdd.setExemption(true);
		groupAdd.setOutRefundNo(outRefundNo);
		groupAdd.setPayTypeId(subSettlement.getPayTypeId());
		mergeGroupAddService.updateMergeGroupAdd(groupAdd);
		
		valueDto.setResult(true);
		valueDto.setMessage("退款成功 ");
		
		log.info(" ~~~~~~  团长免单原路业务处理成功 ~~~~~~~ ");

		return valueDto;
		
	}
	
	public boolean plateformPrePayReturn(String userId,String userName, Double refundAmount,String sn){
		UserDetail userDetail = userDetailService.getUserDetailByidForUpdate(userId);
		Double originAmount = userDetail.getAvailablePredeposit();
		if (originAmount == null) {
			originAmount = 0d;
		}
		Double available_predeposit = Arith.add(originAmount, refundAmount);
		int count = userDetailService.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return false;
		}
		
		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
		pdCashLog.setAmount( refundAmount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc("团长免单退款");
		pdCashLog.setSn(sn);
		pdCashLogService.savePdCashLog(pdCashLog);
		CashLog.log("  recharge For PaySucceed  flow success -----！>  ");
		/*
		 * 消费记录
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(refundAmount);
		expensesRecord.setRecordRemark("团长免单退款");
		expensesRecord.setUserId(userId);
		expensesRecordService.saveExpensesRecord(expensesRecord);
		return true;
	}

	
}
