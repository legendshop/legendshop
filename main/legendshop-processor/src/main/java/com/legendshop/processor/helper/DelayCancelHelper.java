/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.helper;

import java.util.concurrent.DelayQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.legendshop.model.dto.order.CancelOrderDto;
import com.legendshop.util.AppUtils;

/**
 * 定时取消.
 *
 */
@Component
public class DelayCancelHelper {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(DelayCancelHelper.class);
	
	/** The start. */
	private volatile static boolean start;
	
	/** The delay queue. */
	private static DelayQueue<CancelOrderDto> delayQueue = new DelayQueue<CancelOrderDto>();
	
	/** The listener. */
	private OnDelayedListener listener;

	/**
	 * Start.
	 *
	 * @param listener the listener
	 */
	public void start(OnDelayedListener listener) {
		if (start) {
			return;
		}
		init(listener);
	}

	/**
	 * Inits the.
	 *
	 * @param listener the listener
	 */
	private synchronized void init(OnDelayedListener listener) {
		if (start) {
			return;
		}
		log.info("DelayCancelHelper 启动");
		start = true;
		this.listener=listener;
		new Thread(new Runnable() {
			public void run() {
				try {
					while (true) {
						CancelOrderDto order = delayQueue.take();
						if (AppUtils.isNotBlank(order)) {
							if (DelayCancelHelper.this.listener != null) {
								DelayCancelHelper.this.listener.onDelayedArrived(order);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					log.error("DelayCancelHelper delayQueue error {}",e.getMessage());
				}
			}
		}).start();
	}

	/**
	 * offer 延迟队列.
	 *
	 * @param orderDto the order dto
	 */
	public void offer(CancelOrderDto orderDto) {
		delayQueue.offer(orderDto);
	}
	
	/**
	 * Removes the.
	 *
	 * @param subNumbers the sub numbers
	 */
	public void remove(String [] subNumbers){ 
		if(AppUtils.isBlank(subNumbers)){
			return;
		}
		CancelOrderDto[] array = delayQueue.toArray(new CancelOrderDto[]{});  
        if(array == null || array.length <= 0){  
            return;  
        }  
        for (String id:subNumbers) {
        	CancelOrderDto target = null;  
            for(CancelOrderDto order : array){  
                if(order.getSubNumber().equals(Long.valueOf(id))){  
                    target = order;  
                    break;  
                }  
            }  
            if(target != null){  
                delayQueue.remove(target);  
            }  
		}
    } 
	
	
	/**
	 * Removes the.
	 *
	 * @param subNumber the sub number
	 */
	public void remove(String subNumber){  
		CancelOrderDto[] array = delayQueue.toArray(new CancelOrderDto[]{});  
        if(array == null || array.length <= 0){  
            return;  
        }  
        CancelOrderDto target = null;  
        for(CancelOrderDto order : array){  
            if(order.getSubNumber().equals(subNumber)){  
                target = order;  
                break;  
            }  
        }  
        if(target != null){  
            delayQueue.remove(target);  
        }  
    } 
	

	/**
	 * 移除延迟队列.
	 *
	 * @param order the order
	 * @return true, if successful
	 */
	public boolean remove(CancelOrderDto order) {
		return delayQueue.remove(order);
	}
	
	/**
	 * The listener interface for receiving onDelayed events.
	 * The class that is interested in processing a onDelayed
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addOnDelayedListener<code> method. When
	 * the onDelayed event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see OnDelayedEvent
	 */
	public static interface OnDelayedListener {
		
		/**
		 * On delayed arrived.
		 *
		 * @param order the order
		 */
		public void onDelayedArrived(CancelOrderDto order);
	}
	

}
