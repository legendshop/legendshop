/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendMailEvent;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.model.MailInfo;
import com.legendshop.model.SendArrivalInform;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.ProdArrivalInform;
import com.legendshop.spi.service.ProductArrivalInformService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;

/**
 * 发送到货通知（短信和邮箱）
 */
@Component("sendArrivalInformProcessor")
public class SendArrivalInformProcessor implements EventProcessor<SendArrivalInform> {
	
	/** The log. */
	private Logger log = LoggerFactory.getLogger(SendArrivalInformProcessor.class);
	
	@Autowired
	private ProductArrivalInformService productArrivalInformService;
	
	@Autowired
	private SkuService skuService;
	
	@Resource(name = "sendMailProcessor")
	private EventProcessor<MailInfo> sendMailProcessor;
	
	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;	
	
	/**
	 * 处理器
	 */
	@Override
	public void process(SendArrivalInform task) {
		long skuId = task.getSkuId();
		String shopName = task.getShopName();
		long stocks = task.getStocks();
		
		if (log.isDebugEnabled()) {
			log.debug("<------start-------- send arrivalInform to ----------------");
		}
		if(AppUtils.isBlank(skuId)){
			log.warn("Send arrivalInform failed because skuId is null");
			return;
		}
		if(AppUtils.isBlank(shopName)){
			log.warn("Send arrivalInform failed because shopName is null");
			return;
		}
		try {
			//如果更新的库存为0，不需要通知用户
			if(stocks == 0){
				return;
			}
			//查看商品名称
			String productName = skuService.getSku(skuId).getName();
			//查询需要到货通知的用户
			List<ProdArrivalInform> users = productArrivalInformService.getUserBySkuIdAndWhId(skuId);
			if(users != null){
				for(ProdArrivalInform user : users){
					//尚未进行到货通知的用户
					if(user.getStatus() != 1){
						String mobile = user.getMobilePhone();
						String email = user.getEmail();
						String content = "亲爱的，你钟意的"+productName+"有货啦，快点买买买！！！";
						if(AppUtils.isNotBlank(mobile)){
							//发送手机短信
							//TODO TODO短信提醒添加到货提醒  Jason 2018-2-26
							sendSMSProcessor.process(new SendSMSEvent(mobile,productName).getSource());
						}
						if(AppUtils.isNotBlank(email)){
							//发送邮件
							sendMailProcessor.process(new SendMailEvent(email, "到货通知", content, shopName, MailCategoryEnum.INFORM.value()).getSource());
						}
						//将到货通知设置为已通知
						user.setStatus(1);
					}
					//修改到货通知表的状态为已通知
					productArrivalInformService.updateArrivalInform(users);
				}
			}
			
 		} catch (Exception e) {
			log.error("send arrivalInform fail for ", e);
		}finally{
			log.debug("------------- send arrivalInform to -------end--------->");
		}
	}


}
