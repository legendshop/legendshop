/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.dto.SystemParameterDto;

/**
 * 邮件相关配置更改,支持在邮件修改之后马上做出修改后的效果
 */
@Configuration
public class MailPropertiesUpdatedProcessor implements EventProcessor<SystemParameterDto> {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(MailPropertiesUpdatedProcessor.class);

	
	/** 系统配置. */
	@Autowired
	private SystemParameterProvider propertiesUtilUpdate;

	/**
	 * 动作处理器
	 * 
	 */
	@Override
	public void process(SystemParameterDto systemParameterDto) {
		log.info("PropertiesUpdater update mail parameter {} , value {}", systemParameterDto.getKey(), systemParameterDto.getValue());
		//propertiesUtilUpdate.setObject(SysParameterEnum.MAIL_PROPERTIES_CHANGED, true);
	}

	

}
