/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.predeposit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.vo.DrawRecharge;
import com.legendshop.spi.service.PreDepositService;

/**
 * 微信抽奖送预付款
 * 
 */
@Component
public class WeiXinDrawSuccessProcessor implements EventProcessor<DrawRecharge> {

	private final Logger log = LoggerFactory.getLogger(WeiXinDrawSuccessProcessor.class);

	@Autowired
	private PreDepositService preDepositService;

	@Override
	@Async
	public void process(DrawRecharge task) {
		log.info("PdRechargePaySuccessProcessor process calling, task= " + task);
		if (task != null) {
			String result = preDepositService.recharge(task.getUserId(), task.getUserName(), task.getAmount(), task.getSn(), task.getMessage());
			task.setResult(result);
		}
	}
}
