/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.predeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.log.CashLog;
import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.spi.service.PreDepositService;

/**
 * 预存款处理成功
 */
@Component("predepositPaySuccessProcessor")
public class PredepositPaySuccessProcessor implements EventProcessor<PredepositPaySuccess> {

	@Autowired
	private PreDepositService preDepositService;

	/**
	 * 是否支持
	 * com.legendshop.event.processor.AbstractProcessor#isSupport(java.lang.Object)
	 */
	/*@Override
	public boolean isSupport(PredepositPaySuccess success) {
		return true;
	}*/

	/**
	 * 进程
	 */
	@Override
	public void process(PredepositPaySuccess task) {
		CashLog.log("PredepositPaySuccess process calling, task= {} ", task);
		String result=preDepositService.orderPaySuccess(task.getSubSettlementSn(), task.getPdAmount(), task.getUserId(),task.getPrePayType());
		task.setResult(result);
	}
}
