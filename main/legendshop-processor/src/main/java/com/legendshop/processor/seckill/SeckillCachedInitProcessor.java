/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.seckill;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动缓存预热. TODO 暂时没有用上
 *
 */
@Component
public class SeckillCachedInitProcessor extends BaseProcessor<String> {

	private static volatile int logo = 1;

	@Autowired
	private SeckillService seckillService;

	/**
	 * 处理逻辑
	 */
	@Override
	public void process(String arg0) {
		if (logo == 1) {
			logo = 0;
			List<Long> activities = seckillService.findOnActive();
			if (AppUtils.isNotBlank(activities)) {

			}
		}
	}

}
