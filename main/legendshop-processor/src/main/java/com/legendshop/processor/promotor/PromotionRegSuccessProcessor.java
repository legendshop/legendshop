/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.promotor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.spi.service.PromotionCommisService;
import com.legendshop.util.AppUtils;

/**
 * 推广员推广注册成功 处理器
 *
 */
@Component
public class PromotionRegSuccessProcessor implements EventProcessor<UserDetail> {

	private final Logger log = LoggerFactory.getLogger(PromotionRegSuccessProcessor.class);

	@Autowired
	private PromotionCommisService promotionCommisService;

	@Autowired
	private DistSetService distSetService;

	/**
	 * 处理逻辑
	 */
	@Override
	public void process(UserDetail userDetail) {
		log.info("PromotionRegSuccessProcessor process calling, userId={}", userDetail.getUserId());
		String parentUserName = userDetail.getParentUserName();
		if (AppUtils.isNotBlank(parentUserName)) {// 有父节点所以要记录推广关系
			promotionCommisService.handlePromotionReg(userDetail);
		} else {// 注册马上成为推广员
			DistSet originDistSet = distSetService.getDistSet(1L);
			if (originDistSet != null && originDistSet.getRegAsPromoter().intValue() == 1) {
				promotionCommisService.handlePromotionReg(userDetail);
			}
		}

	}
}
