/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.promotor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.OrderCommis;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.PromotionCommisService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.util.AppUtils;

/**
 * 下订单时触发的处理器订单三级分佣逻辑
 * 
 */
@Component("orderCommisProcessor")
public class OrderCommisProcessor implements EventProcessor<OrderCommis> {

	private final Logger log = LoggerFactory.getLogger(OrderCommisProcessor.class);

	@Autowired
	private PromotionCommisService promotionCommisService;

	@Autowired
	private SubItemService subItemService;

	/**
	 * 处理逻辑.
	 */
	@Override
	@Async
	public void process(OrderCommis orderCommis) {
		if (AppUtils.isBlank(orderCommis) || AppUtils.isBlank(orderCommis.getSubNumbers())) {
			log.warn("OrderCommisEvent SubNumbers is null");
			return;
		}
		log.debug("Starting process OrderCommis");
		
		List<String> subNumbers = orderCommis.getSubNumbers();
		for(String subNumber: subNumbers){
			List<SubItem> subItems = subItemService.getSubItem(subNumber);
			if (AppUtils.isBlank(subItems)) {
				continue;
			}
			promotionCommisService.handleOrderCommis(subItems, orderCommis.getUserId());
		}
	}

}
