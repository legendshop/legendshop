/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.auction;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.model.entity.Auctions;
import com.legendshop.processor.auction.scheduler.AuctionsSchedulerJob;
import com.legendshop.processor.auction.scheduler.SchedulerConstant;
import com.legendshop.processor.auction.scheduler.SchedulerMamager;
import com.legendshop.spi.service.AuctionsService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 初始化处理,在xml中定义, 如果引入了拍卖插件则可以使用。
 *
 */
public class AuctionsSchedulerInitProcessor extends BaseProcessor<String>{

	/** The auctions service. */
	private AuctionsService auctionsService;
	
	/** The logo. */
	private int logo=1;
	
	/**
	 * 定时自动上线在线并且不过期的活动
	 */
	@Override
	public void process(String task) {
		if(logo==1){
			logo=0;
			List<Auctions> onlineList=auctionsService.getonlineList();
			if(AppUtils.isNotBlank(onlineList)){
				for (Iterator<Auctions> iterator = onlineList.iterator(); iterator.hasNext();) {
					Auctions auctions = (Auctions) iterator.next();
					//活动上线 同时Add 任务调度去
 	               String jobName = SchedulerConstant.AUCTIONS_SCHEDULER_ + auctions.getId();
 	               Auctions _object=new Auctions();
 	               _object.setId(auctions.getId());
 	               _object.setAuctionsTitle(auctions.getAuctionsTitle());
 	               _object.setStartTime(auctions.getStartTime());
 	               _object.setEndTime(auctions.getEndTime());
 	               _object.setProdId(auctions.getProdId());
 	               _object.setSkuId(auctions.getSkuId());
 	               _object.setDelayTime(auctions.getDelayTime());
 	               _object.setIsSecurity(auctions.getIsSecurity());
 	               String jsonValue=JSONUtil.getJson(_object);
 	               Long time=auctions.getEndTime().getTime()-5000; 
				   Date earlier_date = new Date(time);
				   if(earlier_date.before(new Date())){ //如果提前5秒>当前时间 则用auctions.getEndTime();
					   earlier_date=auctions.getEndTime();
				   }
				   SchedulerMamager.getInstance().addJob(jobName,SchedulerConstant.JOB_GROUP_NAME,SchedulerConstant.TRIGGER_GROUP_NAME, AuctionsSchedulerJob.class,earlier_date, jsonValue);
				}
			}
			//这里可以缓存预热
		   onlineList=null;
		}
	}

	public void setAuctionsService(AuctionsService auctionsService) {
		this.auctionsService = auctionsService;
	}

}
