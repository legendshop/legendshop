/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.spi.service.VisitLogService;
/**
 * 浏览历史处理器
 */
@Component
@EnableAsync
public class VisitLogProcessor implements EventProcessor<VisitLog> {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(VisitLogProcessor.class);

	@Autowired
	private VisitLogService visitLogService;


	/**
	 * 处理访问历史
	 */
	@Override
	@Async
	public void process(VisitLog visitLog) {
		if (log.isDebugEnabled()) {
			log.debug("[{}],{} visit index {}, this {}",
					visitLog.getIp(), visitLog.getUserName(), visitLog.getShopName(), this);
		}
	   visitLogService.process(visitLog);

	}

}
