/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.seckill.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动定时器,自动取消超时未转订单资格
 * 上线，发布时需要加入定时任务
 */
public class SeckillCancleOrderJob implements Job {

	private static final Logger LOG = LoggerFactory.getLogger(SeckillCancleOrderJob.class);

	private static SeckillService seckillService;

	static {
		seckillService = (SeckillService) ContextServiceLocator.getBean("seckillService");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String strData = dataMap.getString("jsonValue");
		if (AppUtils.isBlank(strData)) {
			return;
		}
		
		LOG.info(" <+===  秒杀活动任务调度，自动取消超时未转订单资格   strData={}",strData);
		JSONObject jsonObject=JSON.parseObject(strData);
		Long seckillId=jsonObject.getLongValue("seckillId");
		
		seckillService.cancleSeckillOrder(seckillId,null);

	    LOG.info(" 处理结束 =>");
	}


}
