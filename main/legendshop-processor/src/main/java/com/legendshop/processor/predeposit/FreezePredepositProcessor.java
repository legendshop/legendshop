/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.predeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.vo.FreezePredeposit;
import com.legendshop.spi.service.PreDepositService;

@Component
public class FreezePredepositProcessor implements EventProcessor<FreezePredeposit> {

	@Autowired
	private PreDepositService preDepositService;
	
	@Override
	@Async
	public void process(FreezePredeposit predeposit) {
		String result=preDepositService.freezePredeposit(predeposit.getUserId(), predeposit.getAmount(), predeposit.getSn(), PdCashLogEnum.AUCTION_PAY_FREEZE.value());
		predeposit.setResult(result);
	}
}
