package com.legendshop.processor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.dto.shopDecotate.HotDataDto;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.entity.shopDecotate.ShopBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.spi.service.ShopDecotateCacheService;
import com.legendshop.util.AppUtils;


/**
 *  深度复制
 * @author tony
 *
 */
@Component
public class DeepCopyDecoateObjProcessor implements EventProcessor<ShopDecotateDto> {

	private final Logger log = LoggerFactory.getLogger(DeepCopyDecoateObjProcessor.class);
	
	@Autowired
	private ShopDecotateCacheService shopDecotateCacheService;
	
	@Override
	@Async
	public void process(ShopDecotateDto copyDecotateDto) {
		
		 log.info("<!--- DeepCopyDecoateObjProcessor calling by shopId = {} ",copyDecotateDto.getShopId());
		 Date now=new Date();
		 
		 List<ShopLayoutDto> topShopLayouts=cloneToShopLayoutDtos(copyDecotateDto.getTopShopLayouts());
		 
		 List<ShopLayoutDto> mainShopLayouts=cloneToShopLayoutDtos(copyDecotateDto.getMainShopLayouts());
		 
		 List<ShopLayoutDto> bottomShopLayouts=cloneToShopLayoutDtos(copyDecotateDto.getBottomShopLayouts());
		 
		 List<ShopBanner> shopDefaultBanners=cloneTo(copyDecotateDto.getShopDefaultBanners());
		 
		 List<ShopNav> shopDefaultNavs=cloneTo(copyDecotateDto.getShopDefaultNavs());
		 
		 ShopLayoutShopInfoDto shopDefaultInfo=cloneTo(copyDecotateDto.getShopDefaultInfo());
		 
		 
		  ShopDecotateDto orgin=new ShopDecotateDto();
		  orgin.setDecId(copyDecotateDto.getDecId());
		  orgin.setShopId(copyDecotateDto.getShopId());
		  orgin.setIsBanner(copyDecotateDto.getIsBanner()==null?0:copyDecotateDto.getIsBanner());
		  orgin.setIsInfo(copyDecotateDto.getIsInfo()==null?0:copyDecotateDto.getIsInfo());
		  orgin.setIsNav(copyDecotateDto.getIsNav()==null?0:copyDecotateDto.getIsNav());
		  orgin.setIsSlide(copyDecotateDto.getIsSlide()==null?0:copyDecotateDto.getIsSlide());
		  orgin.setBgColor(copyDecotateDto.getBgColor());
		  orgin.setBgImgId(copyDecotateDto.getBgImgId());
		  orgin.setBgStyle(copyDecotateDto.getBgStyle());
		  orgin.setTopCss(copyDecotateDto.getTopCss());
		  
		  
		  orgin.setBottomShopLayouts(bottomShopLayouts);
		  orgin.setTopShopLayouts(topShopLayouts);
		  orgin.setMainShopLayouts(mainShopLayouts);
		  
		  orgin.setShopDefaultBanners(shopDefaultBanners);
		  orgin.setShopDefaultInfo(shopDefaultInfo);
		  orgin.setShopDefaultNavs(shopDefaultNavs);
		  


		  shopDecotateCacheService.cachePutOnlineShopDecotate(orgin);
		
		
		  Date date=new Date();
		  long l=now.getTime()-date.getTime();
		  long day=l/(24*60*60*1000);
		  long hour=(l/(60*60*1000)-day*24);
		  long min=((l/(60*1000))-day*24*60-hour*60);
		  long s=(l/1000-day*24*60*60-hour*60*60-min*60);
		  
		  
		 System.out.println("time-consuming -----: "+min+"分"+s+"秒");
		
		 log.info("deep copy time-consuming -----: "+min+"分"+s+"秒");
		
		 log.info(" DeepCopyDecoateObjProcessor success -----!> ");
		
	}
	
	
	/**
	 * 深度复制 ShopLayoutDtos
	 * @param layoutDtos
	 * @return
	 */
	public List<ShopLayoutDto> cloneToShopLayoutDtos(List<ShopLayoutDto> layoutDtos){
		if(AppUtils.isBlank(layoutDtos)){
			return null;
		}
		List<ShopLayoutDto> shopLayoutDtos=new ArrayList<ShopLayoutDto>(layoutDtos.size());
		for (Iterator<ShopLayoutDto> iterator = layoutDtos.iterator(); iterator.hasNext();) {
			ShopLayoutDto shopLayoutDto = (ShopLayoutDto) iterator.next();
			
			ShopLayoutModuleDto layoutModuleDto=shopLayoutDto.getShopLayoutModule();
			
			List<ShopNav> shopNavs=cloneTo(layoutModuleDto.getShopNavs());
			
			List<ShopLayoutBanner> shopLayoutBanners=cloneTo(layoutModuleDto.getShopLayoutBanners());
			
			List<ShopLayoutCateogryDto> cateogryDtos=null;
			if(AppUtils.isNotBlank(layoutModuleDto.getCateogryDtos())){
				cateogryDtos=new ArrayList<ShopLayoutCateogryDto>(layoutModuleDto.getCateogryDtos().size());
				for (ShopLayoutCateogryDto cateogryDto : layoutModuleDto.getCateogryDtos()) {
					List<ShopLayoutCateogryDto> childrentDtos=cloneTo(cateogryDto.getChildrentDtos());
					ShopLayoutCateogryDto parentDto=cloneTo(cateogryDto.getParentDto());
					ShopLayoutCateogryDto orgin=cloneTo(cateogryDto);
					
					orgin.setChildrentDtos(null);
					orgin.setParentDto(null);
					orgin.setChildrentDtos(childrentDtos);
					orgin.setParentDto(parentDto);
					
					cateogryDtos.add(orgin);
				}
			}
			
			List<ShopLayoutHotProdDto> hotProds=cloneTo(layoutModuleDto.getHotProds());
			
			ShopLayoutShopInfoDto shopInfo=cloneTo(layoutModuleDto.getShopInfo());
			
			ShopLayoutHotDto shopLayoutHotDto=null;
			if(AppUtils.isNotBlank(layoutModuleDto.getShopLayoutHotDto())){
				 List<HotDataDto> dataDtos=cloneTo(layoutModuleDto.getShopLayoutHotDto().getDataDtos());
				 shopLayoutHotDto=cloneTo(layoutModuleDto.getShopLayoutHotDto());
				 shopLayoutHotDto.setDataDtos(null);
				 shopLayoutHotDto.setDataDtos(dataDtos); 
			}
			
			List<ShopLayoutProdDto> layoutProdDtos=cloneTo(layoutModuleDto.getLayoutProdDtos());
			
			ShopLayoutModuleDto moduleDto=new ShopLayoutModuleDto();
			moduleDto.setLayoutId(layoutModuleDto.getLayoutId());
			moduleDto.setShopId(layoutModuleDto.getShopId());
			moduleDto.setShopDecotateId(layoutModuleDto.getShopDecotateId());
			moduleDto.setLayoutContent(layoutModuleDto.getLayoutContent());
			
			moduleDto.setCateogryDtos(cateogryDtos);
			moduleDto.setHotProds(hotProds);
			moduleDto.setLayoutProdDtos(layoutProdDtos);
			moduleDto.setShopInfo(shopInfo);
			moduleDto.setShopLayoutBanners(shopLayoutBanners);
			moduleDto.setShopLayoutHotDto(shopLayoutHotDto);
			moduleDto.setShopNavs(shopNavs);
			
			
			List<ShopLayoutDivDto> shopLayoutDivDtos=cloneTo(shopLayoutDto.getShopLayoutDivDtos());
			
			ShopLayoutDto orgin=new ShopLayoutDto();
			orgin.setLayoutId(shopLayoutDto.getLayoutId());
			orgin.setLayoutModuleType(shopLayoutDto.getLayoutModuleType());
			orgin.setLayoutType(shopLayoutDto.getLayoutType());
			orgin.setSeq(shopLayoutDto.getSeq());
			orgin.setShopDecotateId(shopLayoutDto.getShopDecotateId());
			orgin.setShopId(shopLayoutDto.getShopId());
			orgin.setTitle(shopLayoutDto.getTitle());
			orgin.setBackColor(shopLayoutDto.getBackColor());
			orgin.setFontColor(shopLayoutDto.getFontColor());
			orgin.setFontImg(shopLayoutDto.getFontImg());
			
			orgin.setShopLayoutDivDtos(null);
			orgin.setShopLayoutModule(null);
			orgin.setShopLayoutDivDtos(shopLayoutDivDtos);
			orgin.setShopLayoutModule(moduleDto);
			
			shopLayoutDtos.add(orgin);
		}
		return shopLayoutDtos;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T> T cloneTo(T src) throws RuntimeException {

		ByteArrayOutputStream memoryBuffer = new ByteArrayOutputStream();

		ObjectOutputStream out = null;

		ObjectInputStream in = null;

		T dist = null;

		try {

			out = new ObjectOutputStream(memoryBuffer);

			out.writeObject(src);

			out.flush();

			in = new ObjectInputStream(new ByteArrayInputStream(memoryBuffer.toByteArray()));

			dist = (T) in.readObject();

		} catch (Exception e) {

			throw new RuntimeException(e);

		} finally {

			if (out != null)

				try {

					out.close();

					out = null;

				} catch (IOException e) {

					throw new RuntimeException(e);

				}

			if (in != null)

				try {

					in.close();

					in = null;

				} catch (IOException e) {

					throw new RuntimeException(e);

				}

		}

		return dist;

	}
}
