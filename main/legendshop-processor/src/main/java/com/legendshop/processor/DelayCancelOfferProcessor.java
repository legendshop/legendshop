package com.legendshop.processor;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.dto.order.CancelOrderDto;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 订单延迟取消队列
 *
 */
@Component
public class DelayCancelOfferProcessor implements EventProcessor<List<String>> {
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private DelayCancelHelper delayCancelHelper;
	
	/**
	 * task 为subId订单号集合
	 * 拆分为多个线程一起执行
	 */
	@Override
	public void process(List<String> task) {
		if(AppUtils.isBlank(task)){
			return ;
		}
		final Date time=new Date();
		//获取订单自动取消的时间
		final int cancelMins = subService.getOrderCancelExpireDate();
        try {
        	for(int i=0;i<task.size();i++){
        		CancelOrderDto orderDto=new CancelOrderDto(task.get(i), time.getTime(), cancelMins);
        		delayCancelHelper.offer(orderDto);
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
