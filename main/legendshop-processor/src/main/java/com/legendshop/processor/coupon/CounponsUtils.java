/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.coupon;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.model.entity.CouponShop;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.CouponShopService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 下单 Counpons 工具类.
 */
@Component
public class CounponsUtils {

	@Autowired
    private UserCouponService userCouponService;

	@Autowired
    private CouponProdService couponProdService;

	@Autowired
    private CouponShopService couponShopService;

    /**
     * 分析处理 优惠券和红包：(全平台红包、店铺红包、店铺通用优惠券、店铺商品优惠券)
     * <p/>
     * 全平台红包 只能使用一个 全平台红包和店铺红包 不能同时使用 同一种 红包或者优惠券 不能使用多个 红包和优惠券 可以同时使用
     * <p/>
     * 全平台红包 按整个订单计算 店铺红包 按参与红包的 店铺订单总和 计算 店铺优惠券 按店铺订单金额 计算 商品优惠券 按参与商品优惠券
     * 的商品总金额 计算.
     *
     * @param userId           用户ID
     * @param userShopCartList 购物车商品信息
     * @param userCouponIds    页面传过来 选中的用户优惠券ID集合
     * @return the user coupon list dto
     */
    public UserCouponListDto analysisCoupons(String userId, UserShopCartList userShopCartList, List<Long> userCouponIds) {
        UserCouponListDto couponListDto = new UserCouponListDto();
        List<UserCoupon> userShopRedpackList = new ArrayList<UserCoupon>();// 店铺红包
        List<UserCoupon> userCommonRedpackList = new ArrayList<UserCoupon>();// 通用红包
        List<UserCoupon> userCommonCouponList = new ArrayList<UserCoupon>();// 店铺通用优惠券
        List<UserCoupon> userProdCouponList = new ArrayList<UserCoupon>();// 店铺商品优惠券

        // 获取用户有效的 优惠券和红包
        getAllActiveCoupons(userId, userShopRedpackList, userCommonRedpackList, userCommonCouponList, userProdCouponList);

        if (AppUtils.isNotBlank(userShopRedpackList)) {
            // 找到店铺红包 下面的店铺
            findShopsInShopRedpacks(userShopRedpackList);
        }

        if (AppUtils.isNotBlank(userProdCouponList)) {
            // 找到商品优惠券下面 的商品
            findProdsInProdCoupons(userProdCouponList);
        }
//		Collections.sort(userProdCouponList,new Comparator<UserCoupon>() {
//
//			@Override
//			public int compare(UserCoupon o1, UserCoupon o2) {
//				return 0;
//			}
//			
//		});
        List<UserCoupon> usableUserCouponList = new ArrayList<UserCoupon>();// 可用的优惠券和红包
        List<UserCoupon> disableUserCouponList = new ArrayList<UserCoupon>();// 不可用的优惠券和红包
        List<UserCoupon> selCoupons = new ArrayList<UserCoupon>();// 选中的 红包和优惠券

        // 处理红包,得到已选中的红包，可用的红包，不可用的红包
        processRedpacks(userShopCartList, userCouponIds, userShopRedpackList, userCommonRedpackList, usableUserCouponList, disableUserCouponList, selCoupons);

        // 处理优惠券,得到已选中的优惠券，可用的优惠券，不可用的优惠券
        processCoupons(userShopCartList, userCouponIds, userProdCouponList, userCommonCouponList, usableUserCouponList, disableUserCouponList, selCoupons);

        // 优惠券和红包 优惠的总金额
        double allOffPrice = 0;
        for (UserCoupon userCoupon : selCoupons) {
            allOffPrice = Arith.add(allOffPrice, userCoupon.getOffPrice());
        }

        // 把已选中优惠券的设置进缓存
        userShopCartList.setCouponOffPrice(allOffPrice);
        userShopCartList.setSelCoupons(selCoupons);

        // subService.putUserShopCartList(CartTypeEnum.NORMAL.toCode(), userId,
        // userShopCartList);
        for (int i = 0; i < disableUserCouponList.size(); i++) {
            disableUserCouponList.get(i).setFlag("no");
            // usableUserCouponList.add(disableUserCouponList.get(i));
        }

        couponListDto.setUsableUserCouponList(usableUserCouponList); // 可用的
        // 优惠券和红包
        couponListDto.setDisableUserCouponList(disableUserCouponList); // 不可用的
        // 优惠券和红包
        return couponListDto;
    }

    /**
     * 处理优惠券,得到已选中的优惠券，可用的优惠券，不可用的优惠券.
     *
     * @param userShopCartList      the user shop cart list
     * @param userCouponIds         用户传过来 选中的用户红包或优惠券的ID集合
     * @param userProdCouponList    用户所有有效的商品优惠券
     * @param userCommonCouponList  用户所有有效的店铺优惠券
     * @param usableUserCouponList  可用的优惠券和红包
     * @param disableUserCouponList 不可用的优惠券和红包
     * @param selCoupons            选中的红包和优惠券
     */
    private void processCoupons(UserShopCartList userShopCartList, List<Long> userCouponIds, List<UserCoupon> userProdCouponList,
                                List<UserCoupon> userCommonCouponList, List<UserCoupon> usableUserCouponList, List<UserCoupon> disableUserCouponList, List<UserCoupon> selCoupons) {

        // 可用的店铺通用券
        List<UserCoupon> usableCommonCouponList = new ArrayList<UserCoupon>();
        // 找出可用和不可用的 通用优惠券
        if (AppUtils.isNotBlank(userCommonCouponList)) {
            // 遍历每个店铺的订单
            for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                // 店铺订单商品总金额
                //	Double shopActualTotalCash = shopCarts.getShopActualTotalCash();

                //修改后的逻辑
                Double shopActualTotalCash = shopCarts.getCalculatedCouponAmount();
                // Double shopTotalCash = shopCarts.getShopTotalCash();
                // 选中的通用优惠券
                UserCoupon selCommCoupon = null;
                // 满足使用条件的通用优惠券集合
                List<UserCoupon> tempCommonCouponList = new ArrayList<UserCoupon>();
                // 遍历每个 店铺优惠券
                for (UserCoupon userCoupon : userCommonCouponList) {
                    // 判断是否是当前店铺的 优惠券
                    if (shopCarts.getShopId().equals(userCoupon.getShopId())) {
                        // 判断是否满足使用条件
                        if (Double.compare(shopActualTotalCash, userCoupon.getFullPrice()) >= 0) {
                            // 用户传过来的ID集合 包含当前ID,并且之前没有选中的红包,则选中当前红包
                            if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(userCoupon.getUserCouponId()) && selCommCoupon == null) {
                                userCoupon.setSelectSts(1);
                                selCommCoupon = userCoupon;
                                selCoupons.add(selCommCoupon);

                                //新增逻辑
                                double reductionRate = Arith.div(userCoupon.getOffPrice(), shopActualTotalCash);//每一块钱所占的比例
                                double usedCouponOffPrice = 0.0;

                                int shopCartsSize = shopCarts.getCartItems().size();//商家购物车的数量
                                for (int i = 0; i < shopCartsSize; i++) {
                                    ShopCartItem shopCartItem = shopCarts.getCartItems().get(i);
                                    double newCouponOffPrice = 0;
                                    if (i == shopCartsSize - 1) {//最后一个购物车项的金额为剩下的红包金额，防止出现小数点的误差
                                        newCouponOffPrice = Arith.sub(userCoupon.getOffPrice(), usedCouponOffPrice);
                                    } else {
                                        newCouponOffPrice = Arith.mul(reductionRate, shopCartItem.getCalculatedCouponAmount());//单个购物车项所占的红包金额
                                    }
                                    shopCartItem.setCouponOffPrice(Arith.add(newCouponOffPrice, shopCartItem.getCouponOffPrice()));
                                    usedCouponOffPrice = Arith.add(newCouponOffPrice, usedCouponOffPrice);
                                }
                            }
                            tempCommonCouponList.add(userCoupon);
                        }
                    }
                }
                // 一个店铺只能使用 一张 店铺通用优惠券
                if (selCommCoupon != null) {
                    usableCommonCouponList.add(selCommCoupon);
                } else {
                    usableCommonCouponList.addAll(tempCommonCouponList);
                }
            }
        }

        // 可用的商品券
        List<UserCoupon> usableProdCouponList = new ArrayList<UserCoupon>();
        // 满足使用条件的商品优惠券
        List<UserCoupon> tempProdCouponList = new ArrayList<UserCoupon>();
        // 选中的商品优惠券
        List<UserCoupon> selProdCoupons = new ArrayList<UserCoupon>();


        ///优先排序，把使用过的优惠劵先计算。
        Collections.sort(userProdCouponList, new Comparator<UserCoupon>() {
            @Override
            public int compare(UserCoupon o1, UserCoupon o2) {
                if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(o1.getUserCouponId()) && userCouponIds.contains(o2.getUserCouponId())) {
                    return 0;
                }
                if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(o1.getUserCouponId()) && !userCouponIds.contains(o2.getUserCouponId())) {
                    return -1;
                }
                if (AppUtils.isNotBlank(userCouponIds) && !userCouponIds.contains(o1.getUserCouponId()) && userCouponIds.contains(o2.getUserCouponId())) {
                    return 1;
                }
                return 0;
            }
        });


        // 找出可用和不可用的 商品优惠券
        if (AppUtils.isNotBlank(userProdCouponList)) {

            //已经参与过选中优惠券计算的商品
            Set<String> joinedSku = new HashSet<>();

            // 遍历每个店铺的订单
            for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                List<ShopCartItem> shopCartItems = shopCarts.getCartItems();
                if (AppUtils.isNotBlank(shopCartItems)) {
                    // 遍历每个 商品优惠券
                    for (UserCoupon userCoupon : userProdCouponList) {
                        Double hitTotalPrice = 0d;// 命中的所有店铺商品 的总金额

                        //新增，可使用此商品优惠券的商品
                        List<ShopCartItem> shopCartItemList = new ArrayList<>();
                        // 遍历每个订单 项
                        for (ShopCartItem shopCartItem : shopCartItems) {
                            if (userCoupon.getProdIds().contains(shopCartItem.getProdId())) {
                                if (!joinedSku.contains("coupon_join_" + shopCartItem.getProdId() + "_" + shopCartItem.getSkuId())) {
                                    //hitTotalPrice = Arith.add(hitTotalPrice, shopCartItem.getTotalMoeny());
                                    //修改后的逻辑
                                    hitTotalPrice = Arith.add(hitTotalPrice, shopCartItem.getCalculatedCouponAmount());

                                    shopCartItemList.add(shopCartItem);
                                }
                            }
                        }
                        // 判断是否满足使用条件
                        if (Double.compare(hitTotalPrice, userCoupon.getFullPrice()) >= 0) {
                            if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(userCoupon.getUserCouponId())) {
                                // 同一种商品优惠券不能使用多个
                                boolean hasSameCoupon = false;
                                for (UserCoupon selCoupon : selProdCoupons) {
                                    if (selCoupon.getCouponId().equals(userCoupon.getCouponId())) {
                                        hasSameCoupon = true;
                                        break;
                                    }
                                }
                                if (!hasSameCoupon) {
                                    userCoupon.setSelectSts(1);
                                    selCoupons.add(userCoupon);
                                    selProdCoupons.add(userCoupon);

                                    //新增逻辑
                                    double reductionRate = Arith.div(userCoupon.getOffPrice(), hitTotalPrice);//每一块钱所占的比例
                                    double usedCouponOffPrice = 0.0;
                                    int shopCartItemSize = shopCartItemList.size();//命中此张优惠券的商家购物车的项的数量
                                    for (int i = 0; i < shopCartItemSize; i++) {
                                        ShopCartItem shopCartItem = shopCartItemList.get(i);
                                        double newCouponOffPrice = 0;//每个购物车项占的优惠券金额
                                        if (i == shopCartItemSize - 1) {//最后一个购物车项的金额为剩下的优惠券金额，防止出现小数点的误差
                                            newCouponOffPrice = Arith.sub(userCoupon.getOffPrice(), usedCouponOffPrice);
                                        } else {
                                            newCouponOffPrice = Arith.mul(reductionRate, shopCartItem.getCalculatedCouponAmount());//单个购物车项所占的红包金额
                                        }
                                        shopCartItem.setCouponOffPrice(Arith.add(newCouponOffPrice, shopCartItem.getCouponOffPrice()));
                                        usedCouponOffPrice = Arith.add(newCouponOffPrice, usedCouponOffPrice);

                                        joinedSku.add("coupon_join_" + shopCartItem.getProdId() + "_" + shopCartItem.getSkuId());
                                    }
                                }
                            }
                            tempProdCouponList.add(userCoupon);
                        }
                    }
                }
            }

            usableProdCouponList.addAll(selProdCoupons);
            for (UserCoupon userCoupon : tempProdCouponList) {
                // 同一种商品优惠券不能使用多个
                boolean hasSameCoupon = false;
                for (UserCoupon userCoupon2 : selProdCoupons) {
                    if (userCoupon.getCouponId().equals(userCoupon2.getCouponId())) {
                        hasSameCoupon = true;
                        break;
                    }
                }
                if (!hasSameCoupon) {
                    usableProdCouponList.add(userCoupon);
                }
            }
        }

        //使用商品优惠劵后，重新计算店铺优惠劵是否满足使用条件。指金额是否满足。
        usableUserCouponList.addAll(usableCommonCouponList);
        disableUserCouponList.addAll(userCommonCouponList);
        disableUserCouponList.removeAll(usableCommonCouponList);

        for (UserCoupon userCoupon : usableCommonCouponList) {
            //选中的优惠劵也需要重新计算，也有可能需要移除。
            boolean isSelect = selCoupons.stream().anyMatch(u -> u.getId().equals(userCoupon.getId()));

            //如果选用了商品优惠劵后，店铺优惠劵不能满足使用条件，则移除。
            Double shopActualTotalCash = 0d;
            for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                if (Objects.equals(userCoupon.getShopId(), shopCarts.getShopId())) {
                    shopActualTotalCash = shopCarts.getCalculatedCouponAmount();
                    break;
                }
            }

            //当前优惠劵选中的情况下，要把自身加上运算，才能确认自身是否可以使用。
            if(isSelect){
                shopActualTotalCash = Arith.add(shopActualTotalCash, userCoupon.getOffPrice());
            }
            if (userCoupon.getFullPrice() > shopActualTotalCash) {
                usableUserCouponList.remove(userCoupon);
                disableUserCouponList.add(userCoupon);
            }
        }

        // 说明有选择红包或优惠券
        if(selCoupons.size() > 0) {
        	
        	//因为选中了优惠劵，会影响红包的可使用数量，
            //只有是有选中了优惠劵才会影响
        	// 循环过滤出 selCoupons 中是否有优惠券(优惠券是指 非平台类型都是优惠券)
        	List<UserCoupon> selNotShopCoupon = selCoupons.stream().filter(u -> !u.getCouponProvider().equals(CouponProviderEnum.PLATFORM.value())).collect(Collectors.toList());
        	if(AppUtils.isNotBlank(selNotShopCoupon)){
        		
                //1、找到是红包，并且是店铺红包。
                //2、找出命中的店铺的总金额。不满足时，设为不可用。
                for (UserCoupon userCoupon : usableUserCouponList.stream().filter(u -> u.getCouponProvider().equals(CouponProviderEnum.PLATFORM.value()))
                        .filter(u -> u.getCouponType().equals(CouponTypeEnum.SHOP.value())).collect(Collectors.toList())) {
                    Double hitActualTotalCash = 0d;
                    for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                        if (userCoupon.getShopIds().contains(shopCarts.getShopId())) {
                            hitActualTotalCash = Arith.add(shopCarts.getCalculatedCouponAmount(), hitActualTotalCash);
                        }
                    }

                    if (userCoupon.getFullPrice() > hitActualTotalCash) {
                        usableUserCouponList.remove(userCoupon);
                        disableUserCouponList.add(userCoupon);
                    }
                }
                
                //因为选中了优惠劵，会影响红包的可使用数量，
                //计算使用优惠劵后的总金额，判断平台通用红包的金额是否会影响
                Double hitActualTotalCash = 0d;
                for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                    hitActualTotalCash = Arith.add(shopCarts.getCalculatedCouponAmount(), hitActualTotalCash);
                }
                for (UserCoupon userCoupon : usableUserCouponList.stream().filter(u -> u.getCouponProvider().equals(CouponProviderEnum.PLATFORM.value()))
                        .filter(u -> u.getCouponType().equals(CouponTypeEnum.COMMON.value())).collect(Collectors.toList())) {
                	//如果是选中的优惠券,判断的金额要加上优惠券减去的金额
                	if(AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(userCoupon.getId())){//TODO 待测试
                		if (userCoupon.getFullPrice() > hitActualTotalCash + userCoupon.getOffPrice()) {
                            usableUserCouponList.remove(userCoupon);
                            disableUserCouponList.add(userCoupon);
                        }
                	}else{
                		if (userCoupon.getFullPrice() > hitActualTotalCash ) {
                            usableUserCouponList.remove(userCoupon);
                            disableUserCouponList.add(userCoupon);
                        }
                	}
                }
        	}
        }


        //添加可用的商品优惠劵及不可用的商品优惠劵
        usableUserCouponList.addAll(usableProdCouponList);
        disableUserCouponList.addAll(userProdCouponList);
        disableUserCouponList.removeAll(usableProdCouponList);

        // 最后默认选中结果以usableUserCouponList 并且selectSts标记为1的才是当前选中的

		if(AppUtils.isNotBlank(usableUserCouponList)){
			selCoupons.clear();
			for (UserCoupon userCoupon : usableUserCouponList) {

				if (userCoupon.getSelectSts() == 1){

					selCoupons.add(userCoupon);
				}
			}
		}

        //选中的优惠劵的优惠金额记录下来
        userShopCartList.getShopCarts().forEach(
                u -> {
                    u.setCouponOffPrice(0d);
                    selCoupons.forEach(
                            i -> {
                                if (AppUtils.isNotBlank(i.getShopId()) && i.getShopId().equals(u.getShopId()))
                                    u.setCouponOffPrice(Arith.add(u.getCouponOffPrice(), i.getOffPrice()));
                            }
                    );
                }
        );
    }

    /**
     * 获取用户有效的 优惠券和红包, 并分好类.
     *
     * @param userId                the user id
     * @param userShopRedpackList   the user shop redpack list
     * @param userCommonRedpackList the user common redpack list
     * @param userCommonCouponList  the user common coupon list
     * @param userProdCouponList    the user prod coupon list
     */
    private void getAllActiveCoupons(String userId, List<UserCoupon> userShopRedpackList, List<UserCoupon> userCommonRedpackList,
                                     List<UserCoupon> userCommonCouponList, List<UserCoupon> userProdCouponList) {

        List<UserCoupon> userCouponList = userCouponService.getAllActiveUserCoupons(userId);
        // System.out.println(JSONUtil.getJson(userCouponList));
        if (AppUtils.isNotBlank(userCouponList)) {
            for (UserCoupon userCoupon : userCouponList) {
                String couponType = userCoupon.getCouponType();
                // 红包
                if (CouponProviderEnum.PLATFORM.value().equals(userCoupon.getCouponProvider())) {

                    // 通用红包
                    if (CouponTypeEnum.COMMON.value().equals(couponType)) {
                        userCommonRedpackList.add(userCoupon);
                    }

                    // 店铺红包
                    else if (CouponTypeEnum.SHOP.value().equals(couponType)) {
                        userShopRedpackList.add(userCoupon);
                    }
                }

                // 优惠券
                else if (CouponProviderEnum.SHOP.value().equals(userCoupon.getCouponProvider())) {

                    // 店铺通用优惠券
                    if (CouponTypeEnum.COMMON.value().equals(couponType)) {
                        userCommonCouponList.add(userCoupon);
                    }

                    // 店铺商品优惠券
                    else if (CouponTypeEnum.PRODUCT.value().equals(couponType)) {
                        userProdCouponList.add(userCoupon);
                    }
                }
            }
        }
    }

    /**
     * 处理红包,得到已选中的红包，可用的红包，不可用的红包.
     *
     * @param userShopCartList      the user shop cart list
     * @param userCouponIds         用户传过来 选中的用户红包或优惠券的ID集合
     * @param userShopRedpackList   用户所有有效的店铺红包
     * @param userCommonRedpackList 用户所有有效的通用红包
     * @param usableUserCouponList  可用的优惠券和红包
     * @param disableUserCouponList 不可用的优惠券和红包
     * @param selCoupons            选中的红包和优惠券
     */
    private void processRedpacks(UserShopCartList userShopCartList, List<Long> userCouponIds, List<UserCoupon> userShopRedpackList,
                                 List<UserCoupon> userCommonRedpackList, List<UserCoupon> usableUserCouponList, List<UserCoupon> disableUserCouponList,
                                 List<UserCoupon> selCoupons) {
        // 预选中的通用红包
        UserCoupon preSelCommRedpack = null;
        // 满足使用条件的通用红包集合
        List<UserCoupon> tempCommonRedpackList = new ArrayList<UserCoupon>();
        // 找出可用和不可用的 通用红包
        if (AppUtils.isNotBlank(userCommonRedpackList)) {
//            Double orderActualTotal = userShopCartList.getOrderActualTotal();
            //计算可使用红包时，不能包括运费。
            Double orderActualTotal = userShopCartList.getOrderActualTotal() - userShopCartList.getOrderFreightAmount();
            // Double orderTotalCash = userShopCartList.getOrderTotalCash();//订单
            // 商品总金额
            for (UserCoupon commonRedpack : userCommonRedpackList) {
                // 判断是否 满足使用条件
                if (Double.compare(orderActualTotal, commonRedpack.getFullPrice()) >= 0) {
                    // 用户传过来的ID集合 包含当前ID,并且之前没有选中的红包,则选中当前红包
                    if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(commonRedpack.getUserCouponId()) && preSelCommRedpack == null) {
                        preSelCommRedpack = commonRedpack;
                    }
                    tempCommonRedpackList.add(commonRedpack);
                }
            }
        }

        // 预选中的店铺红包集合
        List<UserCoupon> preSelShopRedpacks = new ArrayList<UserCoupon>();
        // 满足使用条件的店铺红包集合
        List<UserCoupon> tempShopRedpackList = new ArrayList<UserCoupon>();

        //选中的店铺红包及其对应的商家购物车
        Map<Long, List<ShopCarts>> shopRedpackShopCarts = new HashMap<>();
        // 找到可用和不可用的 店铺红包
        if (AppUtils.isNotBlank(userShopRedpackList)) {
            for (UserCoupon shopRedpack : userShopRedpackList) {
                Double hitTotalPrice = 0d;// 命中的所有店铺商品 的总金额

                //新增，可使用此红包的商家购物车
                List<ShopCarts> shopCartsList = new ArrayList<>();
                for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
                    if (shopRedpack.getShopIds().contains(shopCarts.getShopId())) {
                        //修改前
                        //hitTotalPrice = Arith.add(hitTotalPrice, shopCarts.getShopTotalCash());

                        //修改后
                        hitTotalPrice = Arith.add(hitTotalPrice, shopCarts.getCalculatedCouponAmount());
                        shopCartsList.add(shopCarts);
                    }
                }
                // 判断是否满足使用条件
                if (Double.compare(hitTotalPrice, shopRedpack.getFullPrice()) >= 0) {
                    if (AppUtils.isNotBlank(userCouponIds) && userCouponIds.contains(shopRedpack.getUserCouponId())) {
                        // 同一种店铺红包不能使用多个
                        boolean hasSameCoupon = false;
                        for (UserCoupon userCoupon : preSelShopRedpacks) {
                            if (shopRedpack.getCouponId().equals(userCoupon.getCouponId())) {
                                hasSameCoupon = true;
                                break;
                            }
                        }
                        if (!hasSameCoupon) {
                            shopRedpack.setHitTotalPrice(hitTotalPrice);
                            preSelShopRedpacks.add(shopRedpack);

                            //新增
                            shopRedpackShopCarts.put(shopRedpack.getCouponId(), shopCartsList);
                        }
                    }
                    tempShopRedpackList.add(shopRedpack);
                }
            }
        }

        // 全平台红包和店铺红包 不能同时使用
        if (preSelCommRedpack != null) {
            /**
             * 如果有选中全平台红包，则其他平台红包 和店铺红包 都不能使用
             */
            preSelCommRedpack.setSelectSts(1);
            selCoupons.add(preSelCommRedpack);

            usableUserCouponList.add(preSelCommRedpack);
            disableUserCouponList.addAll(userCommonRedpackList);
            disableUserCouponList.addAll(userShopRedpackList);
            disableUserCouponList.remove(preSelCommRedpack);

            //新增逻辑  计算比例，不应包括运费在里面计算。
            double orderActualTotal = userShopCartList.getOrderActualTotal()- userShopCartList.getOrderFreightAmount();//购物车的实际金额


            double reductionRate = Arith.div(preSelCommRedpack.getOffPrice(), orderActualTotal);//每一块钱所占的比例
            double usedRedpackOffPrice = 0.0;
            List<ShopCarts> shopCartsList = userShopCartList.getShopCarts();
            int shopCartsSize = shopCartsList.size();//商家购物车的数量
            for (int i = 0; i < shopCartsSize; i++) {
                List<ShopCartItem> cartItems = shopCartsList.get(i).getCartItems();
                int j = 0;
                int cartItemsSize = shopCartsList.get(i).getCartItems().size();//商家购物车项的数量
                for (; j < cartItemsSize; j++) {
                    ShopCartItem shopCartItem = cartItems.get(j);
                    double newRedpackOffPrice = 0;
                    if (i == shopCartsSize - 1 && j == cartItemsSize - 1) {//最后一个购物车项的金额为剩下的红包金额，防止出现小数点的误差
                        newRedpackOffPrice = Arith.sub(preSelCommRedpack.getOffPrice(), usedRedpackOffPrice);
                    } else {
                        newRedpackOffPrice = Arith.mul(reductionRate, cartItems.get(j).getDiscountedPrice());//单个购物车项所占的红包金额
                    }
                    shopCartItem.setRedpackOffPrice(Arith.add(newRedpackOffPrice, shopCartItem.getRedpackOffPrice()));
                    usedRedpackOffPrice = Arith.add(newRedpackOffPrice, usedRedpackOffPrice);
                }
            }
        } else {
            if (AppUtils.isNotBlank(preSelShopRedpacks)) {
                /**
                 * 如果没有选中全平台红包，并且 有选中 店铺红包,则 全平台红包 都不能 使用，其他同种的店铺红包 也不能使用
                 */
                for (UserCoupon userCoupon : preSelShopRedpacks) {
                    userCoupon.setSelectSts(1);
                    selCoupons.add(userCoupon);
                    usableUserCouponList.add(userCoupon);

					disableUserCouponList.addAll(userCommonRedpackList);
					disableUserCouponList.addAll(userShopRedpackList);
					disableUserCouponList.remove(userCoupon);

                    //新增逻辑
                    double hitTotalPrice = userCoupon.getHitTotalPrice();
                    double reductionRate = Arith.div(userCoupon.getOffPrice(), hitTotalPrice);//每一块钱所占的比例
                    double usedRedpacksOffPrice = 0.0;//优惠券已经用掉的金额

                    List<ShopCarts> shopCartsList = shopRedpackShopCarts.get(userCoupon.getCouponId());//命中了此张优惠券使的店铺购物车
                    int shopCartsSize = shopCartsList.size();//商家购物车的数量
                    //for (ShopCarts shopCarts : shopRedpackShopCarts.get(userCoupon.getCouponId())) {
                    for (int i = 0; i < shopCartsSize; i++) {
                        List<ShopCartItem> cartItems = shopCartsList.get(i).getCartItems();
                        int j = 0;
                        int cartItemsSize = shopCartsList.get(i).getCartItems().size();//商家购物车项的数量
                        //for(ShopCartItem shopCartItem:shopCarts.getCartItems()){
                        for (; j < cartItemsSize; j++) {
                            ShopCartItem shopCartItem = cartItems.get(j);
                            double newRedpacksOffPrice = 0;
                            if (i == shopCartsSize - 1 && j == cartItemsSize - 1) {//最后一个购物车项的金额为剩下的红包金额，防止出现小数点的误差
                                newRedpacksOffPrice = Arith.sub(userCoupon.getOffPrice(), usedRedpacksOffPrice);
                            } else {
                                newRedpacksOffPrice = Arith.mul(reductionRate, cartItems.get(j).getDiscountedPrice());//单个购物车项所占的红包金额
                            }
                            shopCartItem.setRedpackOffPrice(Arith.add(newRedpacksOffPrice, shopCartItem.getRedpackOffPrice()));
                            usedRedpacksOffPrice = Arith.add(newRedpacksOffPrice, usedRedpacksOffPrice);
                        }
                    }
                }
                // 同一种店铺红包 不能使用多个 TODO 这句注释恰恰相反，加上这段代码同一种店铺红包可以使用多个，这里先注释掉
               /* for (UserCoupon userCoupon : tempShopRedpackList) {
                    boolean hasSameCoupon = false;
                    for (UserCoupon userCoupon2 : preSelShopRedpacks) {
                        if (userCoupon.getCouponId().equals(userCoupon2.getCouponId())) {
                            hasSameCoupon = true;
                            break;
                        }
                    }
                    if (!hasSameCoupon) {
                        usableUserCouponList.add(userCoupon);
                    }
                }*/
            } else {
                /**
                 * 如果没有选中全平台红包，也没有选中店铺红包,则 所有满足使用条件的红包 都是可选状态
                 */
                usableUserCouponList.addAll(tempShopRedpackList);
                usableUserCouponList.addAll(tempCommonRedpackList);
                disableUserCouponList.addAll(userCommonRedpackList);
                disableUserCouponList.addAll(userShopRedpackList);
                disableUserCouponList.removeAll(usableUserCouponList);
            }
        }
		//选中的红包的优惠金额记录下来
		userShopCartList.getShopCarts().forEach(
				u -> {
					u.setRedpackOffPrice(0D);
					selCoupons.forEach(
							i -> {

								u.setRedpackOffPrice(Arith.add(u.getRedpackOffPrice(), i.getOffPrice()));
							}
					);
				}
		);
    }

    /**
     * 找到商品优惠券下面 的商品.
     *
     * @param userProdCouponList the user prod coupon list
     */
    private void findProdsInProdCoupons(List<UserCoupon> userProdCouponList) {
        List<Long> couponIds = new ArrayList<Long>();
        for (UserCoupon userCoupon : userProdCouponList) {
            couponIds.add(userCoupon.getCouponId());
        }
        // 根据 优惠券的ID集合 找出 所有的 关联商品
        List<CouponProd> couponShops = couponProdService.getCouponProdByCouponIds(couponIds);
        if (AppUtils.isNotBlank(couponShops)) {
            for (CouponProd couponProd : couponShops) {
                for (UserCoupon userCoupon : userProdCouponList) {
                    if (userCoupon.getCouponId().equals(couponProd.getCouponId())) {
                        userCoupon.getProdIds().add(couponProd.getProdId());
                    }
                }
            }
        }
    }

    /**
     * 找到店铺红包 下面的店铺.
     *
     * @param userShopRedpackList the user shop redpack list
     */
    private void findShopsInShopRedpacks(List<UserCoupon> userShopRedpackList) {
        List<Long> couponIds = new ArrayList<Long>();
        for (UserCoupon userCoupon : userShopRedpackList) {
            couponIds.add(userCoupon.getCouponId());
        }
        // 根据 红包的ID集合 找出 所有的 关联店铺
        List<CouponShop> couponShops = couponShopService.getCouponShopByCouponIds(couponIds);
        if (AppUtils.isNotBlank(couponShops)) {
            for (CouponShop couponShop : couponShops) {
                for (UserCoupon userCoupon : userShopRedpackList) {
                    if (userCoupon.getCouponId().equals(couponShop.getCouponId())) {
                        userCoupon.getShopIds().add(couponShop.getShopId());
                    }
                }
            }
        }
    }


}
