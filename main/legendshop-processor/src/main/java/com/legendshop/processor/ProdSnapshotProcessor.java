package com.legendshop.processor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.ProductSnapshotService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.util.AppUtils;

/**
 * 订单快照
 * 异步执行
 */
@Component("prodSnapshotProcessor")
public class ProdSnapshotProcessor implements EventProcessor<List<String>> {
	
	private Logger log = LoggerFactory.getLogger(ProdSnapshotProcessor.class);
    
    @Autowired
	private ProductSnapshotService productSnapshotService;
	
    @Autowired
	private SubItemService subItemService;
	
	/**
	 * task 为subId订单号集合,异步执行
	 */
	@Override
	@Async
	public void process(List<String> task) {
		if(AppUtils.isBlank(task)){
			return ;
		}
		log.debug("<----- Prod Snapshot  Processor starting");
		for(String subnumber: task){
			log.debug("processor task by subnumber {}", subnumber);
			List<SubItem> subItems = subItemService.getSubItem(subnumber);
			if(AppUtils.isNotBlank(subItems)){
				for (SubItem subItem : subItems) {
					try {
						//保存商品快照
						productSnapshotService.saveProdSnapshot(subItem);
						subItem=null;
					} catch (Exception e) {
						log.error("product Snapshot save fail ",e.getMessage());
					}
				}
			}
		}
		log.debug(" end ---->   ");
	}

}
