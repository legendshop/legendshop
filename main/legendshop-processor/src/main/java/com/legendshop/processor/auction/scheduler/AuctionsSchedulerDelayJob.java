/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.processor.auction.scheduler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 拍卖活动延迟时长处理.
 *
 * @author Tony
 * 需求：
 * 在设置了出价延时的拍品竞拍结束的前两分钟（以系统接受竞价的时间显示为准），
 * 如果有用户出价竞拍，那么该次拍卖时间将自动延时五分钟，
 * 循环往复直到最后两分钟没有用户出价竞拍时，拍卖结束。
 */
public class AuctionsSchedulerDelayJob {
	
	  /**
  	 * 两个时间相差距离多少天多少小时多少分多少秒.
  	 *
  	 * @param str1 时间参数 1 格式：1990-01-01 12:00:00
  	 * @param str2 时间参数 2 格式：2009-01-01 12:00:00
  	 * @return long[] 返回值为：{天, 时, 分, 秒}
  	 */  
    public static long[] getDistanceTimes(String str1, String str2) {  
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
        Date one;  
        Date two;  
        long day = 0;  
        long hour = 0;  
        long min = 0;  
        long sec = 0;  
        try {  
            one = df.parse(str1);  
            two = df.parse(str2);  
            long time1 = one.getTime();  
            long time2 = two.getTime();  
            long diff ;  
            /*if(time1<time2) {  
                diff = time2 - time1;  
            } else {  
                diff = time1 - time2;  
            } */
            diff = time1 - time2;  
            day = diff / (24 * 60 * 60 * 1000);  
            hour = (diff / (60 * 60 * 1000) - day * 24);  
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);  
            sec = (diff/1000-day*24*60*60-hour*60*60-min*60);  
        } catch (ParseException e) {  
            e.printStackTrace();  
        }  
        long[] times = {day, hour, min, sec};  
        return times;  
    } 
    
    
    /**
     * The main method.
     *
     * @param args the args
     */
    public static void main(String[] args) {
    	 long[] xx= getDistanceTimes("2016-04-29 11:29:01", "2016-04-29 11:29:02") ;
    	 System.out.println("day="+xx[0]+";hour="+xx[1]+" min="+xx[2]+" sec="+xx[3]);
    	 
    	
	}
	
}
