package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTag;
/**
 * 商品标签服务
 */
public interface ProdTagService {

	public Long saveProdTag(ProdTag prodTag, String userName, String userId, Long shopId);

	public ProdTag getById(Long id);

	public boolean deleteProdTagById(Long id);

	public boolean updateByProdTag(ProdTag prodTag);

	public ProdTag getByIdAndStatus(Long tagId);

	public PageSupport<ProdTag> getProdTagManage(String curPageNO, ProdTag prodTag, Long shopId);

}
