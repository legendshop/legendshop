/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.MergeProduct;

/**
 * The Class MergeProductService.
 * 拼团活动参加商品服务接口
 */
public interface MergeProductService  {

   	/**
	 *  根据Id获取拼团活动参加商品
	 */
    public MergeProduct getMergeProduct(Long id);

   /**
	 *  删除拼团活动参加商品
	 */    
    public void deleteMergeProduct(MergeProduct mergeProduct);

   /**
	 *  保存拼团活动参加商品
	 */	    
    public Long saveMergeProduct(MergeProduct mergeProduct);

   /**
	 *  更新拼团活动参加商品
	 */	
    public void updateMergeProduct(MergeProduct mergeProduct);

}
