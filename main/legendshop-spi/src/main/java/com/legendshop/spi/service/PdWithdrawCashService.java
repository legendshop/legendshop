/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdWithdrawCash;

/**
 *预存款提现服务
 */
public interface PdWithdrawCashService  {

    public PdWithdrawCash getPdWithdrawCash(Long id);
    
    /**
     * 获取申请
     * @param id
     * @param userId
     * @return
     */
    public PdWithdrawCash getPdWithdrawCash(Long id,String userId);

    public void deletePdWithdrawCash(PdWithdrawCash pdWithdrawCash);
    
    public Long savePdWithdrawCash(PdWithdrawCash pdWithdrawCash);

    public void updatePdWithdrawCash(PdWithdrawCash pdWithdrawCash);

	public PageSupport<PdWithdrawCash> getPdWithdrawCashPage(String curPageNO, String userId, String state);

	public PageSupport<PdWithdrawCash> getPdWithdrawCashListPage(String curPageNO, PdWithdrawCash pdWithdrawCash, DataSortResult result, Integer pageSize);

	public PageSupport<PdWithdrawCash> getFindBalanceWithdrawal(String curPageNO, String pdcSn, Integer status, String userId);

	PageSupport getPdWithdrawCashByState(String userId,String state, String curPageNO, Integer pageSize);
}
