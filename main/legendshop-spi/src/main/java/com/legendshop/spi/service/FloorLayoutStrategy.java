package com.legendshop.spi.service;

import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;

/**
 * 首页楼层策略
 */
public interface FloorLayoutStrategy {

	public void deleteFloor(Floor floor);

	public FloorDto findFloorDto(Floor floor);
	
}
