/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.weixin.WeixinTexttemplate;

/**
 * 微信文字模板服务
 */
public interface WeixinTexttemplateService  {

    public WeixinTexttemplate getWeixinTexttemplate(Long id);
    
    public void deleteWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);
    
    public Long saveWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);

    public void updateWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate);

}
