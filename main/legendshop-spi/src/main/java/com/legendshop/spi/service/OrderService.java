package com.legendshop.spi.service;

import java.text.ParseException;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.dto.OrderExprotDto;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.order.PresellSubDto;

/**
 * 订单服务
 *
 */
public interface OrderService {

	/**
	 * 查找我的订单
	 */
	PageSupport<MySubDto> getMyorderDtos( OrderSearchParamDto paramDto);
	
	PageSupport<MySubDto> getShopOrderDtos( OrderSearchParamDto paramDto);
	
	PageSupport<MySubDto> getAdminOrderDtos( OrderSearchParamDto paramDto);
	
	void cleanOrderCache(String userId,Long shopId);

	Integer calUnpayOrderCount(String userId);
	
	Integer calConsignmentOrderCount(String userId);

	boolean getStoreProdCount(Long poductId, Long skuId);

	/**
	 * 查询预售订单列表
	 * @param paramDto 订单查询条件DTO
	 */
	PageSupport<PresellSubDto> getPresellOrderList(OrderSearchParamDto paramDto);
	
	/**
	 * 导出订单
	 */
	List<OrderExprotDto> exportOrder(OrderSearchParamDto orderSearchParmDto) throws ParseException;
	
	/**
	 * 获取详情的配送方式信息
	 */
	DeliveryTypeDto getDeliveryTypeDto(Long id);

	/**
	 * 预售订单导出
	 */
	List<OrderExprotDto> exportPresellOrders(OrderSearchParamDto orderSearchParamDto);
	
}
