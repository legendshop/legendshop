/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;

/**
 * The Class ZhiboNewLiveRecordService.
 * 新版直播记录表服务接口
 */
public interface ZhiboNewLiveRecordService  {


   	/**
	 *  根据Id获取新版直播记录表
	 */
    public ZhiboNewLiveRecord getZhiboNewLiveRecord(Long id);

   /**
	 *  删除新版直播记录表
	 */    
    public void deleteZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord);

   /**
	 *  保存新版直播记录表
	 */	    
    public Long saveZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord);

   /**
	 *  更新新版直播记录表
	 */	
    public void updateZhiboNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord);


    /**
     * 查询房间列表
     * @return
     */
	public List<ZhiboNewLiveRecord> getRoomList(Integer index, Integer size);

    /**
	 * 将用户hostUid的直播记录删除。一个用户同一时间只能开启一个直播，成功返回true 失败返回false
	 * @return
	 */
	public int deleteUid(String id, Integer roomid);
	
	
	
	/**
	 * 清空房间成员,用于直播结束清空房间成员；成功则返回1
	 * @param id
	 * @return
	 */
	public int clearRoom(Integer id);
    
	
	public ZhiboNewLiveRecord getRoomById(Integer roomnum);
	
	public List<ZhiboInteractAvRoom> getRoomidList(Integer index, Integer size, Integer roomid);
	
	public List getInfoByUid(String uid);
	
	public int enterRoom(String uid, Integer roomid, String status, String modifyTime, Integer role);
	
	public int exitRoom(String uid);

	public PageSupport<ZhiboNewLiveRecord> getRoom(String curPageNO, String avRoomId);
}
