/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.weixin.WeixinMenu;

/**
 * 微信菜单服务
 */
public interface WeixinMenuService  {

    public WeixinMenu getWeixinMenu(Long id);
    
    public void deleteWeixinMenu(WeixinMenu weixinMenu);
    
    public Long saveWeixinMenu(WeixinMenu weixinMenu);

    public void updateWeixinMenu(WeixinMenu weixinMenu);

	public WeixinMenu getMenu(Long parentId);

	public List<KeyValueEntity> getParentMenu(Long parentId);

	public boolean hasSubWeixinMenu(Long menuId);

	public boolean isExitMenuKey(String menukey);

	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, WeixinMenu weixinMenu, DataSortResult result);

	public PageSupport<WeixinMenu> getWeixinMenu(String curPageNO, Long parentId, int pageSize, DataSortResult result);
}
