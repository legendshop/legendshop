/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.store.StoreSku;

import java.util.List;

/**
 * 门店的单品Sku服务.
 */
public interface StoreSkuService  {

    /** 获取商品Sku */
    public StoreSku getStoreSku(Long id);
    
    /** 删除商品Sku */
    public void deleteStoreSku(StoreSku storeSku);
    
    /** 保存商品Sku */
    public Long saveStoreSku(StoreSku storeSku);

    /** 更新商品Sku */
    public void updateStoreSku(StoreSku storeSku);

    /** 获取商品Sku */
	public List<StoreSku> getStoreSku(Long storeId, Long prodId);
	
	public StoreSku getStoreSkuBySkuId(Long storeId, Long skuId);
}
