/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.group.GroupDto;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Group;

/**
 * 团购服务接口.
 */
public interface GroupService {

	/** 获取团购 */
	public Group getGroup(Long id);

	/** 删除团购 */
	public void deleteGroup(Group group);

	/** 保存团购 */
	public Long saveGroup(Group group);

	/** 更新团购数据 */
	public void updateGroup(Group group);

	/** 保存团购商品 */
	public boolean saveGroupProd(Group group, Long shopId, String userNme, String userId, String shopName, Long value);

	/** 更新团购数据 */
	public boolean updateGroup(Group group, Group oldGroup, Long shopId, String userName,String userId, Long status);

	/** 获取团购 */
	public Group getGroup(Long shopId, Long id);

	/** 获取团购类目 */
	public List<Category> findgroupCategory();

	/** 获取GroupDto */
	public GroupDto getGroupDto(Long id);

	/** 团购数据 */
	public PageSupport<Group> getGroupPage(String curPageNO, Group group);

	/** 查询团购列表 */
	public PageSupport<Group> queryGouplist(String curPageNO, GroupProdSeachParam param);

	/** 查询团购数据 */
	public PageSupport<Group> queryGroupPage(String curPageNO, Integer firstCid, Integer twoCid, Integer thirdCid);

	/** 查询团购数据 */
	public PageSupport<Group> queryGroupList(String curPageNO, GroupProdSeachParam param);

	/** 查询团购数据 */
	public PageSupport<Group> getGroupPage(String curPageNO, DataSortResult result, Group group);

	public int batchOffline(String ids);
	
	/** 团购审核 */
	public void auditGroup(Group groupPo, Group group);

	/** 团购下线 
	 * @param group */
	public String offlineGroup(Group group);

	public boolean findIfJoinGroup(Long productId);

	/** 上下线
	 * @param groupPo
	 * @param status
	 * @return
	 */
	public String updateGroup(Group groupPo, Integer status);

	/**
	 * 逻辑删除团购活动 
	 * @param shopId 商家ID
	 * @param deleteStatus 删除状态
	 */
	public void updateDeleteStatus(Long shopId, Group group, Integer deleteStatus);

	/**
	 * 运营数据统计
	 * @param groupId 团购ID
	 * @return
	 */
	public OperateStatisticsDTO getOperateStatistics(Long groupId);

}
