/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.model.floor.ProductFloor;

/**
 * 首页楼层服务
 */
public interface SubFloorItemService  {

    public SubFloorItem getSubFloorItem(Long id);
    
    public void deleteSubFloorItem(SubFloorItem subFloorItem);
    
    public Long saveSubFloorItem(SubFloorItem subFloorItem);

    public void updateSubFloorItem(SubFloorItem subFloorItem);

    public List<SubFloorItem> getAllSubFloorItem(Long sfId);
    
    public List<SubFloorItem> getItemLimit(Long sfId);
    
    /**
     * 默认混合模型
     */
    public void saveSubFloorItem(List<ProductFloor> productFloorList, Long sfId,Long fId);

	public void saveWideGoodsFloor(List<ProductFloor> productFloorList, Long fId,String layout);

}
