/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.AppDecorate;

/**
 * APP首页装修.
 */
public interface AppDecorateService  {

    public AppDecorate getAppDecorate(Long shopId);
    
    public void deleteAppDecorate(AppDecorate appDecorate);
    
    public Long saveAppDecorate(AppDecorate appDecorate);

    public void updateAppDecorate(AppDecorate appDecorate);
}
