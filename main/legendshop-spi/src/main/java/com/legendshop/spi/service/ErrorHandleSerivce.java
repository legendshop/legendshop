/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.ErrorForm;

/**
 * 错误处理器.
 */
public interface ErrorHandleSerivce {
	
	/**
	 * 返回错误信息
	 */
	public ErrorForm getErrorForm(String errorCode);
}
