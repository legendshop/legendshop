/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinImgFile;

/**
 *微信图片服务
 */
public interface WeixinImgFileService  {

    public WeixinImgFile getWeixinImgFile(Long id);
    
    public void deleteWeixinImgFile(List<Long> idList);
    
    public Long saveWeixinImgFile(WeixinImgFile weixinImgFile);

    public void updateWeixinImgFile(WeixinImgFile weixinImgFile);

	public List<WeixinImgFile> getWeixinImgFilesByGroupId(Long groupId);

	public void updateWeixinImgFiles(List<WeixinImgFile> weixinImgFiles);

	public void changeImgsGroup(List<Long> idList, Long groupId);

	public PageSupport<WeixinImgFile> getWeixinImgFile(String curPageNO, int pageSize, Long type);

}
