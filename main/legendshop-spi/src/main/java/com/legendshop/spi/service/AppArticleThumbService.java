/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AppArticleThumb;

/**
 * The Class AppArticleThumbService.
 * 种草和发现文章点赞表服务接口
 */
public interface AppArticleThumbService  {

   	/**
	 *  根据Id获取种草和发现文章点赞表
	 */
    public AppArticleThumb getAppArticleThumb(Long id);

    /**
	 *  根据Id删除种草和发现文章点赞表
	 */
    public int deleteAppArticleThumb(Long id);
    
    /**
	 *  根据对象删除种草和发现文章点赞表
	 */
    public int deleteAppArticleThumb(AppArticleThumb appArticleThumb);
    /**
  	 *  根据disId删除种草和发现文章点赞表
  	 */
     public int deleteAppArticleThumbByDiscoverId(Long disId,String uid);
    
   /**
	 *  保存种草和发现文章点赞表
	 */	    
    public Long saveAppArticleThumb(AppArticleThumb appArticleThumb);

   /**
	 *  更新种草和发现文章点赞表
	 */	
    public void updateAppArticleThumb(AppArticleThumb appArticleThumb);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<AppArticleThumb> queryAppArticleThumb(String curPageNO,Integer pageSize);

	/**
	 * 判断用户是否点赞
	 * @param discoverId
	 * @param userId
	 * @return
	 */
    Integer rsThumb(Long discoverId, String userId);
}
