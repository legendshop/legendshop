/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.MarketingRule;

/**
 * 营销活动规则服务.
 */
public interface MarketingRuleService  {

    public MarketingRule getMarketingRule(Long id);

    public void deleteMarketingRule(MarketingRule marketingRule);

    public void deleteMarketingRuleByMarketing(Long marketingId);
    
    public Long saveMarketingRule(MarketingRule marketingRule);

    public void updateMarketingRule(MarketingRule marketingRule);

}
