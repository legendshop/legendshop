/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassComm;

/**
 * The Class GrassCommService.
 * 种草文章评论表服务接口
 */
public interface GrassCommService  {

   	/**
	 *  根据Id获取种草文章评论表
	 */
    public GrassComm getGrassComm(Long id);

    /**
	 *  根据Id删除种草文章评论表
	 */
    public int deleteGrassComm(Long id);
    
    /**
	 *  根据对象删除种草文章评论表
	 */
    public int deleteGrassComm(GrassComm grassComm);
    
   /**
	 *  保存种草文章评论表
	 */	    
    public Long saveGrassComm(GrassComm grassComm);

   /**
	 *  更新种草文章评论表
	 */	
    public void updateGrassComm(GrassComm grassComm);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassComm> queryGrassComm(String curPageNO,Integer pageSize);
    
}
