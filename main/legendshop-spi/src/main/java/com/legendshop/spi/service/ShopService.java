/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ShopDetailView;

/**
 * 商家服务.
 */
public interface ShopService {

	/**
	 * 根据用户名拿到商城实体.
	 */
	public ShopDetailView getShopDetailView(Long shopId);

	public abstract Boolean isShopExist(final Long shopId);

	public Long getShopIdByDomain(String domainName);

	public ShopDetailView getShopDetailViewByUserName(String value);
	
	public Integer getShopStatus(Long shopId);
	
}
