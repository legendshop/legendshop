/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.FloorDto;

/**
 * 首页Api
 */
public interface IndexApiService {

	public List<FloorDto> loadFloor();
}
