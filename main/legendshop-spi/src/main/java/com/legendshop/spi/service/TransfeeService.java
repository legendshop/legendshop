package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Transfee;
/**
 * 运输费用服务
 */
public interface TransfeeService {

	public List<Transfee> getTransfee();

    public Transfee getTransfee(Long id);
    
    public void deleteTransfee(Transfee transfee);
    
    public Long saveTransfee(Transfee transfee);

    public void updateTransfee(Transfee transfee);

    public List<Transfee> getTranfee(Long transportId);

	public List<Transfee> getTranfeeByShopId(Long shopId);

	public PageSupport<Transfee> getTransfeePage(String curPageNO, Transfee transfee);
}
