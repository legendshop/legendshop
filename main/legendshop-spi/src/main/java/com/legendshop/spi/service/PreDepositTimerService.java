package com.legendshop.spi.service;

/**
 * 分销佣金收入,充值到预存款
 */
public interface PreDepositTimerService {

	/**
	 * 分销佣金收入,充值到预存款
	 * @param userId  用户ID
	 * @param amount  佣金收入金额
	 * @param subItemNumber 订单项流水号
	 * @return
	 * @throws Exception 
	 */
	String rechargeForDistCommis(String userId, Double amount, String subItemNumber) throws Exception;
	
}
