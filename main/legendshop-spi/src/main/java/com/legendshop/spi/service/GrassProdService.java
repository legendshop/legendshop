/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassProd;

/**
 * The Class GrassProdService.
 * 种草文章关联商品表服务接口
 */
public interface GrassProdService  {

   	/**
	 *  根据Id获取种草文章关联商品表
	 */
    public GrassProd getGrassProd(Long id);

    /**
	 *  根据Id删除种草文章关联商品表
	 */
    public int deleteGrassProd(Long id);
    
    /**
	 *  根据对象删除种草文章关联商品表
	 */
    public int deleteGrassProd(GrassProd grassProd);
    
    /**
	 *  根据文章id删除种草文章关联商品表
	 */
    public void deleteByGrassId(Long graid);
    
   /**
	 *  保存种草文章关联商品表
	 */	    
    public Long saveGrassProd(GrassProd grassProd);

   /**
	 *  更新种草文章关联商品表
	 */	
    public void updateGrassProd(GrassProd grassProd);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassProd> queryGrassProd(String curPageNO,Integer pageSize);
    
}
