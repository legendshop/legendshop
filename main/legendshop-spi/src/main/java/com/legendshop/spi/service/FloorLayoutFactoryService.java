package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.model.entity.FloorItem;

/**
 * 首页楼层布局
 */
public interface FloorLayoutFactoryService {

	void deleteFloor(Floor floor);

	FloorItem findFloorAdv(Floor floor, String position);

	List<FloorItem> findFloorAdvs(Floor floor, String position);

	/**
	 * 后台楼层
	 * 
	 * @param floor
	 * @return
	 */
	FloorDto findAdminFloorDto(Floor floor);

	/**
	 * 加载用户的楼层信息,包括子楼层的信息.
	 *
	 */
	List<FloorDto> loadFloor();

}
