/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.zhibo.ZhiboRoomMemberDto;
import com.legendshop.model.dto.zhibo.ZhiboRoomProdDto;
import com.legendshop.model.entity.ZhiboProd;

/**
 * 直播服务接口
 */
public interface ZhiboProdService {

	/**
	 * 根据Id获取
	 */
	public ZhiboProd getZhiboProd(Long id);

	/**
	 * 删除
	 */
	public void deleteZhiboProd(ZhiboProd zhiboProd);

	/**
	 * 保存
	 */
	public Long saveZhiboProd(ZhiboProd zhiboProd);

	/**
	 * 更新
	 */
	public void updateZhiboProd(ZhiboProd zhiboProd);

	/**
	 * 更新房间的商品
	 */
	public List<Long> updateAvRoomProd(Long avRoomId, Long[] prodId);

	/**
	 * 查询直播房间的商品
	 */
	public PageSupport<ZhiboRoomProdDto> queryZhiboRoomProd(String curPageNO, String avRoomId, String prodName);

	/**
	 * 查询直播房间的商品
	 */
	public PageSupport<ZhiboRoomMemberDto> queryZhiboRoomProdq(String curPageNO, String avRoomId, String userName);
}
