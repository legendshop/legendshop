/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headmenu;

/**
 * Headmenu首页菜单服务.
 */
public interface HeadmenuService  {

    public Headmenu getHeadmenu(Long id);
    
    public void deleteHeadmenu(Headmenu headmenu);
    
    public Long saveHeadmenu(Headmenu headmenu);

    public void updateHeadmenu(Headmenu headmenu);

    public  List<Headmenu> getHeadmenuAll(String type);

	public PageSupport<Headmenu> getHeadmenu(String curPageNO, Headmenu headmenu, String type);
}
