/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawRecord;

/**
 * 活动抽奖记录服务.
 */
public interface DrawRecordService  {

    public DrawRecord getDrawRecord(Long id);
    
    public void deleteDrawRecord(DrawRecord drawRecord);
    
    public Long saveDrawRecord(DrawRecord drawRecord);

    public void updateDrawRecord(DrawRecord drawRecord);

    /**
     * @Description: 根据用户id/actId获取数据
     * @date 2016-4-26 
     */
    public Long getDrawRecordCount(String userId,Long actId);
    
    /**
     * @Description: 根据用户id/actId获取当天记录数
     * @date 2016-4-26 
     */
    public Long getDRCountByCurrentDay(String userId,Long actId);

	public PageSupport<DrawRecord> queryDrawRecordPage(String curPageNO, DrawRecord drawRecord);
}
