/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service.security;

import com.legendshop.model.security.StoreEntity;
import com.legendshop.model.security.UserEntity;


/**
 * @author Newway
 * 用户登录验证服务接口.
 */
public interface AuthService {
	
	/**
	 *  查找用户, remember me 使用
	 *  需要把username 和shopId拆分出来
	 *  
	 */
	public UserEntity loadUserByUsername(String username);
	
	/**
	 *  根据用户名,手机号, 邮件查找用户
	 *  
	 */
	public UserEntity loadUserByUsernameOrMobileOrMail(String username);
	
	/**
	 * 某个商城的用户登录
	 */
	public UserEntity loadUserByUsername(String username, String presentedPassword) ;
	
	/**
	 * 加载第三方登录用户
	 * @param username
	 * @param accessToken
	 * @param openId
	 * @param type
	 * @param source
	 * @return
	 */
	public UserEntity loadThirdPartyUser(String username, String accessToken, String openId, String type, String source);
	
	/**
	 * 根据手机号码来找到用户
	 * @param mobile
	 * @return
	 */
	public UserEntity loadUserByMobile(String mobile);

	/**
	 * 加载门店
	 * @param username 用户名
	 * @param password 密码
	 * @return 门店实体
	 */
	public StoreEntity loadStoreByName(String username, String password);
	
}