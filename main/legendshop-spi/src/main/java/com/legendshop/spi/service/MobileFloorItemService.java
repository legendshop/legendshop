/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloorItem;

/**
 * 手机端html5 楼层内容关联服务.
 */
public interface MobileFloorItemService  {

    public MobileFloorItem getMobileFloorItem(Long id);
    
    public void deleteMobileFloorItem(MobileFloorItem mobileFloorItem);
    
    public Long saveMobileFloorItem(MobileFloorItem mobileFloorItem);

    public void updateMobileFloorItem(MobileFloorItem mobileFloorItem);

	public List<MobileFloorItem> queryMobileFloorItem(Long parentId);

	public Long getCountMobileFloorItem(Long parentId);

	public PageSupport<MobileFloorItem> getMobileFloorItem(String curPageNO, String title, Long id);

}
