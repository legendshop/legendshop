/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassConcern;

/**
 * The Class GrassConcernService.
 * 种草社区关注表服务接口
 */
public interface GrassConcernService  {

   	/**
	 *  根据Id获取种草社区关注表
	 */
    public GrassConcern getGrassConcern(Long id);

    /**
	 *  根据Id删除种草社区关注表
	 */
    public int deleteGrassConcern(Long id);
    
    /**
	 *  根据对象删除种草社区关注表
	 */
    public int deleteGrassConcern(GrassConcern grassConcern);
    
    
    
    /**
  	 *  根据grassUserId删除种草和发现文章关注表
  	 */
     public void deleteGrassConcernByDiscoverId(String grassUserId,String uid);
    
   /**
	 *  保存种草社区关注表
	 */	    
    public Long saveGrassConcern(GrassConcern grassConcern);

   /**
	 *  更新种草社区关注表
	 */	
    public void updateGrassConcern(GrassConcern grassConcern);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassConcern> queryGrassConcern(String curPageNO,Integer pageSize);

	/**
	 * 获取是否关注作者
	 * @param grassUserId
	 * @param userId
	 * @return
	 */
	Integer rsConcern(String grassUserId, String userId);
}
