/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.DeliveryTypeDto;
import com.legendshop.model.entity.DeliveryType;

/**
 * 物流配送方式服务.
 */
public interface DeliveryTypeService {

	public List<DeliveryType> getDeliveryTypeByShopId(Long shopId);

	public DeliveryType getDeliveryType(Long id);
	
	public DeliveryType getDeliveryType(Long shopId,Long id);
	
	public void deleteDeliveryType(DeliveryType deliveryType);

	public Long saveDeliveryType(DeliveryType deliveryType);

	public void updateDeliveryType(DeliveryType deliveryType);

	List<DeliveryType> getDeliveryTypeByShopId();

	public long checkMaxNumber(Long shopId);

	public PageSupport<DeliveryType> getDeliveryTypePage(String curPageNO, DeliveryType deliveryType);

	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId);
	
	public PageSupport<DeliveryType> getDeliveryTypeCq(Long shopId, String curPageNO);

	public PageSupport<DeliveryTypeDto> getAppDeliveryTypePage(String curPageNO, Long shopId);
	
	public Integer getDeliveryTypeByprintTempId(Long printTempId);
	
}
