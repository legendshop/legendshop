/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.manager;

import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.ThirdUserInfo;

/**
 * 第三方用户账号授权管理器.
 */
public interface ThirdUserAuthorizeManager {
	
	/**
	 * 构造授权URL
	 * @param source
	 * @return
	 */
	String constructAuthorizeUrl(String source);
	
	/**
	 * 网页授权认证回调
	 * @param source
	 * @return
	 */
	ThirdUserAuthorizeResult authorizeCallback(String code, String state, String source);
	
	/**
	 * APP授权认证回调
	 * @param thirdUserInfo 第三方用户信息
	 * @return
	 */
	ThirdUserAuthorizeResult appAuthorizeCallback(ThirdUserInfo thirdUserInfo);
	
	/**
	 * 小程序授权认证回调
	 * @param code 小程序授权码
	 * @param encryptedData 用户加密数据
	 * @param iv 用于解密的向量
	 * @return
	 */
	ThirdUserAuthorizeResult mpAuthorizeCallback(String code, String encryptedData, String iv);
}
