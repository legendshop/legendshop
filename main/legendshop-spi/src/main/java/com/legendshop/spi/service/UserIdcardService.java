/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.overseas.UserIdcard;

/**
 *用户实名认证服务
 */
public interface UserIdcardService  {

    public List<UserIdcard> getUserIdcard(String userId);

    public UserIdcard getUserIdcard(Long id);
    
    public void deleteUserIdcard(UserIdcard userIdcard);
    
    public Long saveUserIdcard(UserIdcard userIdcard);

    public void updateUserIdcard(UserIdcard userIdcard);

	public UserIdcard getDefaultUserIdcard(String userId);
}
