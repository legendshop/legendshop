/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.ProdPropImage;

/**
 * 商品属性图片
 */
public interface ProdPropImageService  {

    public ProdPropImage getProdPropImage(Long id);
    
    public void deleteProdPropImage(ProdPropImage prodPropImage);
    
    public Long saveProdPropImage(ProdPropImage prodPropImage);

    public void updateProdPropImage(ProdPropImage prodPropImage);

    List<ProdPropImage> getProdPropImageByProdId(Long prodId);
    
    public List<ProdPropImage> getProdPropImageByKeyVal(Long prodId,Long key,Long val);
}
