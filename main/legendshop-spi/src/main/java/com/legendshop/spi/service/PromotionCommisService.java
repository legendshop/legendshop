/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserDetail;

/**
 * 推广员订单佣金Service
 * 
 */
public interface PromotionCommisService {

	/** 推广员 推广注册成功 */
	public void handlePromotionReg(UserDetail userDetail);

	/** 订单佣金结算 */
	public void settleOrderCommis(SubItem subItem);

	/** 订单分配佣金 ---》 支持分销的订单项 */
	public void handleOrderCommis(List<SubItem> subItems, String userId);
	
	/** 订单佣金回退 */
	public void cancleOrderCommis(SubItem subItem);

}
