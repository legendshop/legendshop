/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.shopDecotate.ShopLayoutHot;

/**
 *商家装修热点模块服务
 */
public interface ShopLayoutHotService  {

    public ShopLayoutHot getShopLayoutHot(Long id);
    
    public void deleteShopLayoutHot(ShopLayoutHot shopLayoutHot);
    
    public Long saveShopLayoutHot(ShopLayoutHot shopLayoutHot);

    public void updateShopLayoutHot(ShopLayoutHot shopLayoutHot);

	public void update(String string, Object[] objects);

	public void deleteShopLayoutHot(Long shopId, Long layoutId);
}
