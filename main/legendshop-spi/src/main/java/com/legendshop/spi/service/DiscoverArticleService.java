/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.model.entity.ProdGroup;

/**
 * The Class DiscoverArticleService.
 * 发现文章表服务接口
 */
public interface DiscoverArticleService  {

   	/**
	 *  根据Id获取发现文章表
	 */
    public DiscoverArticle getDiscoverArticle(Long id);

    /**
	 *  根据Id删除发现文章表
	 */
    public int deleteDiscoverArticle(Long id);
    
    /**
	 *  根据对象删除发现文章表
	 */
    public int deleteDiscoverArticle(DiscoverArticle discoverArticle);
    
   /**
	 *  保存发现文章表
	 */	    
    public Long saveDiscoverArticle(DiscoverArticle discoverArticle);

   /**
	 *  更新发现文章表
	 */	
    public void updateDiscoverArticle(DiscoverArticle discoverArticle);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverArticle> queryDiscoverArticle(String curPageNO,Integer pageSize,String condition,String order);
    
    /**
  	 *  分页查询通过标题列表
  	 */	
	public abstract PageSupport<DiscoverArticle> queryDiscoverArticlePage(String curPageNO, DiscoverArticle discoverArticle,Integer pageSize);
 
    
}
