/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticle;

/**
 * The Class GrassArticleService.
 * 种草社区文章表服务接口
 */
public interface GrassArticleService  {

   	/**
	 *  根据Id获取种草社区文章表
	 */
    public GrassArticle getGrassArticle(Long id);

    /**
	 *  根据Id删除种草社区文章表
	 */
    public int deleteGrassArticle(Long id);
    
    /**
	 *  根据对象删除种草社区文章表
	 */
    public int deleteGrassArticle(GrassArticle grassArticle);
    
   /**
	 *  保存种草社区文章表
	 */	    
    public Long saveGrassArticle(GrassArticle grassArticle);

   /**
	 *  更新种草社区文章表
	 */	
    public void updateGrassArticle(GrassArticle grassArticle);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassArticle> queryGrassArticle(String curPageNO,Integer pageSize);
    
    
    public PageSupport<GrassArticle> queryGrassArticlePage(String curPageNo,String name,Integer pageSize);
}
