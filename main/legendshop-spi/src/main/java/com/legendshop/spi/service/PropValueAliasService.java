/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.PropValueAlias;

/**
 *商品属性值别名服务
 */
public interface PropValueAliasService  {

    public PropValueAlias getPropValueAlias(Long id);
    
    public void deletePropValueAlias(PropValueAlias propValueAlias);
    
    public Long savePropValueAlias(PropValueAlias propValueAlias);

    public void updatePropValueAlias(PropValueAlias propValueAlias);

}
