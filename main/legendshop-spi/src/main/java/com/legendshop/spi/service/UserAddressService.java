/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.UserAddress;

/**
 * 用户地址服务.
 */
public interface UserAddressService {

	public List<UserAddress> getUserAddress(String userName);

	public UserAddress getUserAddress(Long id);

	public int deleteUserAddress(UserAddress userAddress);

	public Long saveUserAddress(UserAddress userAddress,String userId);

	public void updateUserAddress(UserAddress userAddress,String userId);

	public Long getMaxNumber(String userName);
	
	public Area getAreaById(Integer id);
	
	public void updateDefaultUserAddress(Long addrId,String userId);
	
	public UserAddress  getDefaultAddress(String userId);
	
	public PageSupport<UserAddress> UserAddressPage(String curPageNO, String userId);

	public PageSupport<UserAddress> queryUserAddress(String curPageNO, String userId);

	public PageSupport<UserAddress> getUserAddressPage(String curPageNO, UserAddress userAddress);

	public PageSupport<UserAddress> getUserAddress(String curPageNO, String userName);

	/**
	 * 更新其他默认地址的commonAddr
	 * @param addrId 当前地址Id， 除当前地址外的地址算其他地址
	 * @param commonAddr 是否默认地址， 0： 非默认， 1： 默认
	 */
	public void updateOtherDefault(Long addrId, String userId, String commonAddr);

	/**
	 * @param userAddress
	 * @param userId
	 * @return
	 */
	public Long saveUserAddressAndDefault(UserAddress userAddress, String userId);

	/**
	 * 分页查询地址
	 */
	PageSupport<UserAddress> queryUserAddress(String userId, String curPageNO, int pageSize);
}
