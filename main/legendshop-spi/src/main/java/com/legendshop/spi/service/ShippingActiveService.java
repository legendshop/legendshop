/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShippingActive;

/**
 * 包邮活动Service
 */
public interface ShippingActiveService  {
    /**
     * 根据Id获取包邮活动
     */
    public ShippingActive getShippingActive(Long id);
    
    /**
     * 删除包邮活动
     */
    public void deleteShippingActive(ShippingActive shippingActive);
    
    /**
     * 保存包邮活动
     */
    public Long saveShippingActive(ShippingActive shippingActive);

    /**
     * 删除包邮活动
     */
    public void updateShippingActive(ShippingActive shippingActive);

    /**
     * 获取包邮活动列表
     */
	public PageSupport<ShippingActive> queryShippingActiveList(String curPageNO, Long shopId, ShippingActive shippingActive);

	/**
	 * 根据ID获取包邮活动
	 */
	public ShippingActive getShippingActiveById(Long activeId, Long shopId);
    
	public void saveActiveAndProd(ShippingActive active, Long[] prodIds);

	public void updateShippingActiveByProd(ShippingActive active, String[] prodIds);

	/** 获取商品的包邮情况 */
	public String queryShopMailfeeStr(Long shopId, Long prodId);

	/**
	 * app商家端修改包邮活动
	 * @param id
	 * @param name
	 * @param prodId
	 */
	int appUpdateShipping(Long id ,String name ,Long [] prodId);
}
