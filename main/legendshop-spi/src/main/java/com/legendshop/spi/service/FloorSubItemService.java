/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FloorSubItem;

/**
 * 首页主楼层子内容服务.
 */
public interface FloorSubItemService  {

    public FloorSubItem getFloorSubItem(Long id);
    
    public void deleteFloorSubItem(FloorSubItem floorSubItem);
    
    public Long saveFloorSubItem(FloorSubItem floorSubItem);

    public void updateFloorSubItem(FloorSubItem floorSubItem);

    public List<FloorSubItem> getAllFloorSubItem(Long id);

	public void deleteFloorSubItem(List<FloorSubItem> floorSubItemList);

	public PageSupport<FloorSubItem> getFloorSubItemPage(String curPageNO);
}
