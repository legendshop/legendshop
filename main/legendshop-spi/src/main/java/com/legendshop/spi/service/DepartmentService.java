/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.Department;

/**
 * 部门服务.
 */
public interface DepartmentService  {

    public Department getDepartment(Long id);
    
    public void deleteDepartment(Department department);
    
    public Long saveDepartment(Department department);

    public void updateDepartment(Department department);

	public List<Department> getAllDepartment();

	public String getDepartmentName(Long deptId);

}
