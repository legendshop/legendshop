/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.AttachmentTree;

/**
 * 附件类型Service.
 */
public interface AttachmentTreeService  {

    public AttachmentTree getAttachmentTree(Long id);
    
    public void deleteAttachmentTree(AttachmentTree attachmentTree);
    
    public Long saveAttachmentTree(AttachmentTree attachmentTree);

    public void updateAttachmentTree(AttachmentTree attachmentTree);

    /** 根据父节点查询所有子节点 **/
    public List<AttachmentTree> getAttachmentTreeByPid(Long pId,String userName);

	public void updateAttmntTreeNameById(Integer id, String name);

	public AttachmentTree getAttachmentTree(Long treeId, Long shopId);

	public List<AttachmentTree> getAllChildByTreeId(Long treeId);

	public void deleteAttachmentTree(List<AttachmentTree> attmntTreeList);

	/**
	 * 获取图片目录树结构
	 */
	public List<AttachmentTree> getAttachmentTreeDtoByUserName(String userName);
}
