/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;

/**
 * 首页楼层服务.
 */
public interface FloorService  {

    public Floor getFloor(Long id);
    
    public Long saveFloor(Floor floor);

    public void updateFloor(Floor floor);

	public boolean checkPrivilege(Long floorId);
	
	public Integer getLeftSwitch(Long floorId);

	public PageSupport<Floor> getFloorPage(String curPageNO, Floor floor);

	public Integer batchOffline(String floorIdStr);
}
