/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StoreOrderSerachDto;
import com.legendshop.model.dto.order.StoreOrderDto;
import com.legendshop.model.entity.store.StoreOrder;

/**
 * 门店订单服务接口
 * 
 */
public interface StoreOrderService  {

    /** 查看门店订单 */
    public StoreOrder getDeliveryOrder(Long id);
    
    /** 根据订单对象编号获取门店订单 */
    public StoreOrder getDeliveryOrderBySubNumber(String subNumber);
    
    /** 删除门店订单 */
    public void deleteDeliveryOrder(StoreOrder deliveryOrder);
    
    /** 更新门店订单状态 */
    public int updateDeliveryOrderStatus(String subNumber,Integer status);
    
    /** 保存门店订单 */
    public Long saveDeliveryOrder(StoreOrder deliveryOrder);

    /** 更新门店订单 */
    public void updateDeliveryOrder(StoreOrder deliveryOrder);

    /** 查看门店订单Dto */
	public PageSupport<StoreOrderDto> getDeliveryOrderDtos(Long shopId,Long storeId,StoreOrderSerachDto paramdto);

}
