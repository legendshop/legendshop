package com.legendshop.spi.service;

import com.legendshop.model.dto.BackRefundResponseDto;

/**
 * 原路返回退款操作处理器
 */
public interface BackRefundProcessor {
	
	/**
	 * 原路返回退款操作
	 */
	BackRefundResponseDto backRefund( String backRefund);

}
