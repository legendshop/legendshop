package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductCommentReplyDto;
import com.legendshop.model.entity.ProductReply;

/**
 * 商品评论回复服务
 */
public interface ProductReplyService {

	public  List<ProductReply> getProductReply();

	public ProductReply getProductReply(Long id);

	public void deleteProductReply(ProductReply productReply);

	public Long saveProductReply(ProductReply productReply);

	public void updateProductReply(ProductReply productReply);

	public Integer getUseful(String userId,Long prodComId);

	public PageSupport<ProductCommentReplyDto> queryProductReplyList(Long prodComId, String curPageNO);

	public PageSupport<ProductReply> getProductReplyPage(String curPageNO);
}
