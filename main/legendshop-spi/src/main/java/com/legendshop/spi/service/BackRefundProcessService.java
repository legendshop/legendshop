package com.legendshop.spi.service;

import com.legendshop.model.dto.BackRefundResponseDto;


/**
 *  原路退款策略上下文
 *
 */
public interface BackRefundProcessService {
	
	 public BackRefundResponseDto backRefund(String subSettlementType,String backRefund);

}
