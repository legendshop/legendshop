/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Partner;

/**
 * 合作伙伴服务.
 */
public interface PartnerService {

	public Partner getPartner(Long id);

	public void deletePartner(Partner partner);

	public Long savePartner(Partner partner);

	public void updatePartner(Partner partner);

	public PageSupport<Partner> getPartnerPage(String curPageNO, Partner partner);

}
