/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StockAdminDto;
import com.legendshop.model.dto.UpdSkuStockDto;
import com.legendshop.model.dto.stock.StockArmDto;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;

/**
 * 库存服务.
 */
public interface StockService {

	/**
	 * 释放holding，客户退货或者订单取消，还原订单数量.
	 *
	 * @param prodId the prod id
	 * @param skuId the count
	 */
	public void releaseHold(Long prodId, Long skuId, Long basketCount,String subType);

	void makeUpStocks(Long skuId, Long basketCount);

	/**
	 * 检查库存，是否可以订购.
	 *
	 * @param product the product
	 * @param count the count
	 * @return true, if successful
	 */
	public boolean canOrder(Product product, Integer count);

	/**
	 * 减库存
	 * @param prodId
	 * @param skuId
	 * @param basketCount
	 * @return
	 */
	public boolean addHold(Long prodId, Long skuId, Long basketCount);

	/**
	 * 查看库存
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public abstract Integer getStocksByLockMode(Long prodId,Long skuId);

	/**
	 * 查看库存
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public abstract Integer getStocksByMode(Long prodId,Long skuId);

	/**
	 * 发货减少实际库存
	 * @param prodId 商品Id
	 * @param skuId 单品Id
	 * @param basketCount 订购数
	 * @return 成功与否
	 */
	boolean addActualHold(Long prodId, Long skuId, Long basketCount);

	/**
	 * 是否可以下单
	 * @param sku
	 * @param count
	 * @return
	 */
	public boolean canOrder(Sku sku, Integer count);

	/**
	 * 根据 城市计算 区域限售
	 * @param prodId
	 * @param skuId
	 * @param cityId
	 * @param transportId
	 * @param shopId
	 * @return
	 */
	Boolean isRegionalSales(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId);

	/**
	 * 根据 城市计算 SKU的库存
	 */
	public Map<String,String> calculateSkuStocksByCity(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId);

	/**
	 * 根据 城市计算 SKU的库存(门店库存)
	 */
	public Map<String,String> calculateSkuStocksByCity(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId, Long storeId);

	/**
	 * 根据skuid查出库存
	 */
	public List<UpdSkuStockDto> getStockBySkuId(Long skuId);

	/**
	 * 更新sku库存
	 */
	public int updSkuStockDto(Long skuId,Long stocks);


	/**
	 * 检测虚拟库存, 看看是否可以减库存
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public boolean checkStocks(Long prodId, Long skuId, long basketCount);


	public PageSupport<StockAdminDto> getStockcontrol(String curPageNO, String productName);


	public PageSupport<StockArmDto> getStockwarning(Long shopId, String curPageNO, String productName);


	PageSupport<StockArmDto> getStockList(Long shopId, String curPageNO, int pageSize);
}
