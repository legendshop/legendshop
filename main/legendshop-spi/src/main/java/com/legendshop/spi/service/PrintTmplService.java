/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PrintTmpl;

/**
 *打印模板服务
 */
public interface PrintTmplService  {

    public List<PrintTmpl> getPrintTmplByShop(Long shopId);

    public PrintTmpl getPrintTmpl(Long id);
    
    public void deletePrintTmpl(PrintTmpl printTmpl);
    
    public Long savePrintTmpl(PrintTmpl printTmpl);

    public void updatePrintTmpl(PrintTmpl printTmpl);


	public PrintTmpl getPrintTmplByDelvId(Long delvId);

	public PageSupport<PrintTmpl> getPrintTmplPage(String curPageNO, PrintTmpl printTmpl);

	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId);
	
	public PageSupport<PrintTmpl> getPrintTmplPageByShopId(Long shopId, String curPageNO);

	public void updatePrintTmpl(PrintTmpl origin,PrintTmpl printTmpl);
}
