/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.entity.ProductProperty;

/**
 * 商品属性服务
 */
public interface ProductPropertyService  {

    public ProductProperty getProductProperty(Long id);
    
    public void deleteProductProperty(ProductProperty productProperty);
    
    public Long saveProductProperty(ProductProperty productProperty,String userId, Long shopId);

    public void updateProductProperty(ProductProperty productProperty);

    public List<ProductProperty> getProductPropertyList(Long proTypeId);
    
    public List<ProductProperty> getParameterPropertyList(Long proTypeId);

    @Deprecated
	public PublishProductDto getProductProperty(Long sortId, Long nsortId, Long subsortId);

	/** 用于保存自定义商品属性,其实没真正保存到，只是获取id后就返回 **/
	public ProductProperty saveCustomAttribute(String propertyName,String userName, int sequence);

	/** 根据 商品ID 和 用户名 查找商品属性**/
	public List<ProductProperty> queryUserProp(Long prodId, String userName);

	/** 根据 分类Id 查找对应的参数和属性**/
	public PublishProductDto queryProductProperty(Long categoryId);

	/** 根据 分类Id 查找对应的属性**/
	public List<ProductPropertyDto> findProductProper(Long categoryId);

	/** 根据 商品Id 查取用户自定义属性 **/
	public List<ProductProperty> queryUserPropByProdId(Long prodId);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId);

	public PageSupport<ProductProperty> getProductPropertyParamPage(String curPageNO, Integer isRuleAttributes,
			String propName, String memo, Long proTypeId);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes);

	public PageSupport<ProductProperty> getProductPropertyPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty);

	public PageSupport<ProductProperty> getProductPropertyByPage(String curPageNO, Integer isRuleAttributes,
			ProductProperty productProperty);

	/** 根据规格id查找对应的商品id */
	public Integer findProdId(Long propId);

	public List<ResultCustomPropertyDto> applyProperty(String originUserProperties, String userName, String customAttribute);
	
	
}
