/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.entity.store.Store;

/**
 * 门店服务接口
 */
public interface StoreService {
	
	/** 获取门店 */
	public Store getStore(Long id);

	/** 获取门店 */
	public Store getStore(Long id, Long shopId);

	/** 删除门店 */
	public void deleteStore(Store store);

	/** 保存门店 */
	public Long saveStore(Store store);

	/** 更新门店 */
	public void updateStore(Store store);

	/** 获取门店 */
	public PageSupport<Store> getStore(String curPageNO, Long shopId);

	/** 判断登录名是否存在 */
	public boolean checkStoreName(String username);

	/** 获取门店 */
	public PageSupport<StoreProductDto> getStore(String curPageNO, int pageSize, String storeId, Long shopId, Long poductId, String productname);

}
