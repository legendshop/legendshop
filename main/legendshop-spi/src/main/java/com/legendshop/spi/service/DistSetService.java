/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.promoter.model.DistSet;

import java.util.List;

/**
 * 分销设置Service.
 */
public interface DistSetService {

	/** 获取分销设置 */
	public List<DistSet> getDistSet(String userName);

	/** 获取分销设置 */
	public DistSet getDistSet(Long id);

	/** 删除分销设置 */
	public void deleteDistSet(DistSet distSet);

	/** 保存分销设置 */
	public Long saveDistSet(DistSet distSet);

	/** 更新分销设置 */
	public void updateDistSet(DistSet distSet);

}
