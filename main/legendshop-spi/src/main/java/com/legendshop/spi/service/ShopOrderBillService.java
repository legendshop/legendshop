/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;

/**
 * 商家结算服务.
 */
public interface ShopOrderBillService  {

    /**
     * Gets the shop order bill.
     *
     * @param id the id
     * @return the shop order bill
     */
    public ShopOrderBill getShopOrderBill(Long id);
    
    /**
     * Delete shop order bill.
     *
     * @param shopOrderBill the shop order bill
     */
    public void deleteShopOrderBill(ShopOrderBill shopOrderBill);
    
    /**
     * Save shop order bill.
     *
     * @param shopOrderBill the shop order bill
     * @return the long
     */
    public Long saveShopOrderBill(ShopOrderBill shopOrderBill);

    /**
     * Update shop order bill.
     *
     * @param shopOrderBill the shop order bill
     */
    public void updateShopOrderBill(ShopOrderBill shopOrderBill);

	
	/**
	 * 获取当前期结算的月份 例如：201512.
	 *
	 * @return the shop bill month
	 */
	public String getShopBillMonth();
	
	/**
	 * Gets the last shop order bill by shop id.
	 *
	 * @param shopId the shop id
	 * @return the last shop order bill by shop id
	 */
	public ShopOrderBill getLastShopOrderBillByShopId(Long shopId);

	
	
	/**
	 * 保存结算单并更新订单状态和退货单为结束状态.
	 *
	 * @param shopOrderBill 结算单
	 * @param billSubIds   要结算的订单Id列表
	 * @param billRefundId 要结算的退货单Id列表
	 * @param billPresellSubIds 要结算的预售单Id列表
	 */
	public void generateShopBill(ShopOrderBill shopOrderBill, List<Long> billSubIds, List<Long> billRefundId,List<Long> billPresellSubIds,List<Long> billAuctionDepositRecIds);

	/**
	 * Gets the shop order bill page.
	 *
	 * @param curPageNO the cur page no
	 * @param shopOrderBill the shop order bill
	 * @return the shop order bill page
	 */
	public PageSupport<ShopOrderBill> getShopOrderBillPage(String curPageNO, ShopOrderBill shopOrderBill);

	/**
	 * Gets the shop order bill page.
	 *
	 * @param shopId the shop id
	 * @param curPageNO the cur page no
	 * @param shopOrderBill the shop order bill
	 * @return the shop order bill page
	 */
	public PageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO, ShopOrderBill shopOrderBill);

	/**
	 * 查找系统是否有现成的结算单
	 */
	public boolean getShopOrderBill(Long shopId, Date startDate, Date endDate);

	PageSupport<ShopOrderBill> getShopOrderBill(String curPageNO, Long shopId, Integer status, int pageSize);
}
