/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.SubRefundReturnDetail;

/**
 *退款明细服务
 */
public interface SubRefundReturnDetailService {

	/**
	 * 根据Id获取
	 */
	public SubRefundReturnDetail getSubRefundReturnDetail(Long id);

	/**
	 * 删除
	 */
	public void deleteSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);

	/**
	 * 保存
	 */
	public Long saveSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);

	/**
	 * 更新
	 */
	public void updateSubRefundReturnDetail(SubRefundReturnDetail subRefundReturnDetail);
	
	/**
	 * 根据refundId获取
	 */
	public List<SubRefundReturnDetail> getSubRefundReturnDetailByRefundId(Long refundId);
}
