/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Floor;
import com.legendshop.model.entity.SubFloor;

/**
 * 首页子楼层服务
 */
public interface SubFloorService  {

    public List<SubFloor> getSubFloorByShopId();

    public SubFloor getSubFloor(Long id);
    
    public void deleteSubFloor(SubFloor subFloor);
    
    public Long saveSubFloor(SubFloor subFloor);

    public void updateSubFloor(SubFloor subFloor);

    public List<SubFloor> getAllSubFloor(List<Floor> floorList);
    
    public List<SubFloor> getSubFloorByfId(Long id);

	public boolean checkPrivilege(String userName, Long floorId);

	public PageSupport<SubFloor> getSubFloorPage(String curPageNO, SubFloor subFloor);
}
