/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.PurchaseProdDto;
import com.legendshop.model.entity.Marketing;

/**
 * 营销活动表服务类
 */
public interface MarketingService  {

    public Marketing getMarketing(Long id);
    
    public void deleteMarketing(Marketing marketing);
    
    public String saveMarketing(Marketing marketing);
    
    public String saveMarketingByFullmail(Marketing marketing);

    public void updateMarketing(Marketing marketing);

	public Marketing getMarketing(Long shopId, Long id);
	
	public Marketing getMarketingDetail(Long shopId, Long id);

	public Long isExistProds(Long id, Long shopId);
	
	public List<MarketingDto> findAvailableBusinessRuleList(Long shopId,Long productId);
	
	public List<MarketingDto> findAvailableBusinessRuleList(Long shopId,List<ShopCartItem> cartItems);

	public PageSupport<Marketing> getMarketingPage(String curPageNO, Long shopId, Marketing marketing);

	public PageSupport<Marketing> getMarketingPage(String curPageNO, Marketing marketing);

	public PageSupport<Marketing> getAssemblefullmail(String curPageNO, Marketing marketing);

	public PageSupport<Marketing> getShopMarketingMansong(String curPageNO, Marketing marketing, DataSortResult result,Integer pageSize);

	public PageSupport<Marketing> getShopMatketingZhekou(String curPageNO, Marketing marketing, Integer pageSize, DataSortResult result);

	public List<PurchaseProdDto> getMarketingList(Date startTime, Date endTime);

	/**
	 * 查找时间重叠的满折活动
	 * 三种情况：
	 * a、开始时间 在     startTime  和  endTime 之间
	 * b、开始时间少于  startTime 且结束时间大于 endTime
	 * c、结束时间在  startTime 和 endTime之间
	 */
	public List<Marketing> findOngoingMarketing(Long shopId, Integer isAllProds, Date startTime, Date endTime);

	public int batchDetele(String marketIds);
	
	public int getAllProdsMarketing(Long shopId, Integer type, Date startTime, Date endTime);

	/**
	 * 获取已过期的促销活动
	 * @param commitInteval 条数
	 * @param endTime 结束时间
	 * @return List<Marketing>
	 */
	public List<Marketing> getExpiredMarketing(Integer commitInteval, Date endTime);

	/**
	 * 新增促销规则
	 * @param marketing
	 */
	void saveMarketingRule(Marketing marketing);

	/**
	 * 清除限时折扣缓存缓存
	 */
	void cleanMarketCache();

}
