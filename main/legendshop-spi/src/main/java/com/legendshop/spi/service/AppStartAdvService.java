/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppStartAdvDto;
import com.legendshop.model.entity.AppStartAdv;

/**
 * APP启动广告服务.
 */
public interface AppStartAdvService  {

    public AppStartAdv getAppStartAdv(Long id);
    
    public void deleteAppStartAdv(AppStartAdv appStartAdv);
    
    public Long saveAppStartAdv(AppStartAdv appStartAdv);

    public void updateAppStartAdv(AppStartAdv appStartAdv);

	public boolean updateStatus(Long id, Integer status);

	/**
	 * 获取所有记录总数
	 * @return
	 */
	public int getAllCount();

	/** 检查name是否已存在 */
	public boolean checkNameIsExits(String name);

	public String getName(Long id);

	/**
	 * 获取已上线的广告 
	 * @return
	 */
	public List<AppStartAdvDto> getOnlines();

	public PageSupport<AppStartAdv> getAppStartAdvPage(String curPageNO, AppStartAdv appStartAdv);
}
