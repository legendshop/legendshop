package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PayType;
/**
 * 支付类型服务
 *
 */
public interface PayTypeService {
	
	/**
	 * 查询支付服务
	 */
	public abstract PayType getPayTypeById(String payTypeId);

	/**
	 * Load.
	 * 
	 * @param id
	 *            the id
	 * @return the pay type
	 */
	public abstract PayType getPayTypeById(Long id);
	
	
	/**
	 * Load from db,not cache.
	 * 
	 * @param id
	 *            the id
	 * @return the pay type
	 */
	public abstract PayType getPayTypeByIdFromDb(Long id);


	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void delete(Long id);

	/**
	 * Save.
	 * 
	 * @param payType
	 *            the pay type
	 * @return the long
	 */
	public abstract Long save(PayType payType);

	/**
	 * Update.
	 * 
	 * @param payType
	 *            the pay type
	 */
	public abstract void update(PayType payType);

	/**
	 * 根据payTypeId查找支付类型
	 * @param payTypeId
	 * @return
	 */
	public abstract PayType getPayTypeByPayTypeId(String payTypeId);

	public abstract Map<String, Integer> getPayTypeMap();

	public abstract PageSupport<PayType> getPayTypeListPage(String curPageNO);

	/**
	 * 获取所有支付方式
	 */
	public abstract List<PayType> getPayTypeList();

}