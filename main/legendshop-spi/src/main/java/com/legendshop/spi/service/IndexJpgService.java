/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Indexjpg;

/**
 * 首页图片服务.
 */
public interface IndexJpgService {

	/**
	 * Query index jpg.
	 *
	 */
	public abstract Indexjpg getIndexJpgById(Long id);

	/**
	 * Delete index jpg.
	 * 
	 * @param indexjpg
	 *            the indexjpg
	 */
	public abstract void deleteIndexJpg(Indexjpg indexjpg);

	/**
	 * Update indexjpg.
	 * 
	 * @param origin
	 *            the origin
	 */
	public abstract void updateIndexjpg(Indexjpg origin);

	/**
	 * Save indexjpg.
	 *
	 * @param indexjpg the indexjpg
	 * @return the long
	 */
	public abstract Long saveIndexjpg(Indexjpg indexjpg);

	/**
	 * Evict index jpg cache.
	 *
	 */
	public abstract void evictIndexJpgCache();
	
	/**
	 * Gets the index jpeg by shop id.
	 *
	 */
	public List<Indexjpg> getIndexJpegByShopId();

	/**
	 * Gets the index jpg page.
	 *
	 */
	public abstract PageSupport<Indexjpg> getIndexJpgPage(String curPageNO, Indexjpg indexjpg, int location,
			Integer pageSize, DataSortResult result);

	/**
	 * Gets the index jpg page by data.
	 *
	 */
	public abstract PageSupport<Indexjpg> getIndexJpgPageByData(String curPageNO, Indexjpg indexjpg, int location,
			Integer pageSize, DataSortResult result);

}