package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.KeyValueEntity;


/**
 * 模版中心 处理所有的模版数据
 *
 */
public interface WeiXintemplateService {

	public  List<KeyValueEntity> getTemplates(String msgType);
}
