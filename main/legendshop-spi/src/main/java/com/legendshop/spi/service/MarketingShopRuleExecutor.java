package com.legendshop.spi.service;

import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
/**
 * 营销活动执行器
 * @author newway
 *
 */
public interface MarketingShopRuleExecutor {

	public void execute(MarketingDto marketingDto,ShopCarts shopCarts, MarketRuleContext context);
	
}
