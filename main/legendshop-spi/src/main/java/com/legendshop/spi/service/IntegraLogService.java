/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.integral.IntegraLog;

/**
 * 会员积分日志明细服务.
 */
public interface IntegraLogService  {

    public IntegraLog getIntegraLog(Long id);
    
    public void deleteIntegraLog(IntegraLog integraLog);
    
    public Long saveIntegraLog(IntegraLog integraLog);

    public void updateIntegraLog(IntegraLog integraLog);

    public PageSupport<IntegraLog> getIntegraLog(String userId,String curPageNO,String logDesc,Long logType,Date startDate,Date endDate);
    
	public PageSupport<IntegraLog> getIntegraLogPage(String userId, String curPageNO);

	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, Integer pageSize, DataSortResult result);

	public PageSupport<IntegraLog> getIntegraLogListPage(String curPageNO, IntegraLog integraLog, String userId, Integer pageSize, DataSortResult result);
}
