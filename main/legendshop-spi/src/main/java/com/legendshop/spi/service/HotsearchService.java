/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Hotsearch;

/**
 * 热门搜索服务.
 */
public interface HotsearchService {

	/**
	 * 找到热门的商品
	 */
	public abstract List<Hotsearch> getHotsearch();

	/**
	 * 根据ID获取热门商品
	 */
	public abstract Hotsearch getHotsearchById(Long id);

	/**
	 * 删除
	 */
	public abstract void delete(Long id);


	/**
	 * 保存
	 */
	public abstract Long saveHotsearch(Hotsearch hotsearch);

	/**
	 * 更新
	 */
	public abstract void updateHotsearch(Hotsearch hotsearch);

	/**
	 * 搜索
	 */
	public abstract PageSupport<Hotsearch> queryHotsearch(DataSortResult result, String curPageNO, Hotsearch hotsearch);

}