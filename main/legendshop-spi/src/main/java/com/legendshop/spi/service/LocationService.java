/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.Region;

/**
 * 位置服务
 */
public interface LocationService{

	/**
	 * Load province.
	 *
	 * @return the list
	 */
	List<KeyValueEntity>loadProvinces();
	
	/**
	 * Load cities.
	 *
	 * @param provinceid the provinceid
	 * @return the list
	 */
	List<KeyValueEntity> loadCities(Integer provinceid);
	

	/**
	 * Load areas.
	 *
	 * @param cityid the cityid
	 * @return the list
	 */
	List<KeyValueEntity> loadAreas(Integer cityid);
	
	/**
	 * 装载所有的省份.
	 *
	 * @return the all province
	 */
	List<Province> getAllProvince();
	
	
	/**
	 * 转载所以的城市.
	 *
	 * @return the all city
	 */
	List<City> getAllCity();
	
	/**
	 * 根据省份装载城市.
	 *
	 * @param provinceid the provinceid
	 * @return the city
	 */
	List<City> getCity(Integer provinceid);
	
	/**
	 * 根据城市Id装载地区.
	 *
	 * @param cityid the cityid
	 * @return the Area
	 */
	List<Area> getArea(Integer cityid);
	
	/**
	 * 根据省份Id查找省份.
	 *
	 * @param id the id
	 * @return the  province
	 */
	Province getProvinceById(Integer id);
	
	/**
	 * 根据城市Id查找城市.
	 *
	 * @param id the id
	 * @return the city by id
	 */
	City getCityById(Integer id);
	
	/**
	 * 根据地区Id查找地区.
	 *
	 * @param id the id
	 * @return the area by id
	 */
	Area getAreaById(Integer id);
	
	/**
	 * 删除省份.
	 *
	 * @param provinceid the provinceid
	 */
	void deleteProvince(Integer provinceid);
	
	/**
	 * 删除城市.
	 *
	 * @param cityid the cityid
	 */
	void deleteCity(Integer cityid);
	
	/**
	 * 删除地区.
	 *
	 * @param areaid the areaid
	 */
	void deleteArea(Integer areaid);
	
	/**
	 * 添加省份.
	 *
	 * @param province the province
	 */
	void saveProvince(Province province);
	
	/**
	 * 添加城市.
	 *
	 * @param city the city
	 */
	void saveCity(City city);
	
	/**
	 * 添加地区.
	 *
	 * @param area the area
	 */
	void saveArea(Area area);
	
	/**
	 * 更新省份.
	 *
	 * @param province the province
	 */
	void updateProvince(Province province);
	
	/**
	 * 更新城市.
	 *
	 * @param city the city
	 */
	 void updateCity(City city);
	
	/**
	 * 更新地区.
	 *
	 * @param area the area
	 */
	 void updateArea(Area area);
	 
	 /**
 	 * 查询省份所在的区域.
 	 *
 	 * @return the region list
 	 */
	 List<Region> getRegionList();

	/**
	 * Delete area list.
	 *
	 * @param idList the id list
	 */
	void deleteAreaList(List<Integer> idList);
	
	/**
	 * 根据邮政编码获取信息
	 * @param provinceId
	 * @return
	 */
	Province getProvince(String provinceId);

	List<Province> getProvinceByName(String provinceName);

	List<City> getCityByName(String cityName);
}
