package com.legendshop.spi.service;

import com.legendshop.model.entity.ConstTable;

/**
 * 常量服务
 */
public interface ConstTableService   {
	
	ConstTable getConstTablesBykey(String type,String keyValue);
	
	abstract void updateConstTableByType(String payType, String payTypeId, String payParams);

	ConstTable getConstTablesBykeyForNoCache(String type,String keyValue);
}



