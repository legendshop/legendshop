/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Theme;
import com.legendshop.model.vo.ThemeVo;

/**
 * 专题服务.
 */
public interface ThemeService  {

    public List<Theme> getTheme();
    
    public Theme getTheme(Long id);
    
    public void deleteTheme(Theme theme);
    
    public Long saveTheme(Theme theme);

    public void updateTheme(Theme theme);

	public String deleteTheme(Long id);

	public String updataStatus(Long id, Integer status);
	
	PageSupport getTheme(String curPageNo, Integer pageSize);
	
	/*
	 * 查询专题详情
	 */
	public ThemeVo getThemeDetail(Long themeId);
	
	/**
	 * 清理专题详情缓存
	 */
	void clearThemeDetailCache(Long themeId);

	PageSupport<Theme> allThemes(String curPageNO);
	
	public PageSupport<Theme> getThemePage(String curPageNO, String name);

	public PageSupport<Theme> getThemePage(String curPageNO);

	public PageSupport<Theme> getThemePage(String curPageNO, Theme entity, DataSortResult result);

	public PageSupport<Theme> getThemeQueryContent(String curPageNO,Integer status, DataSortResult result, String title);

	public void updateTheme(String type, Theme theme);

    PageSupport<Theme> getSelectTheme(String curPageNO, String title,Long themeId);
}
