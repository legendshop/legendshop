/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminUser;
import com.legendshop.model.entity.Role;

/**
 * 管理员服务.
 */
public interface AdminUserService  {

    public AdminUser getAdminUser(String id);
    
    public void deleteAdminUser(AdminUser adminUser);
    
    public String saveAdminUser(AdminUser adminUser);

    public void updateAdminUser(AdminUser adminUser);

	public Boolean isAdminUserExist(String userName);

	public int updateAdminUserPassword(String id, String newPassword);

	public List<Role> loadRole(String appNo);

	public List<Role> loadRole(String userId, String appNo);

	public PageSupport<AdminUser> getAdminUser(String curPageNO, AdminUser adminUser);
}
