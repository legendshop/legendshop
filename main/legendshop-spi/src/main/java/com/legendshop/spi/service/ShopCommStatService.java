/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;

/**
 *店铺评分统计服务
 */
public interface ShopCommStatService  {

    public ShopCommStat getShopCommStat(Long id);
    
    public ShopCommStat getShopCommStatByShopId(Long shopId);
    
    public void deleteShopCommStat(ShopCommStat shopCommStat);
    
    public Long saveShopCommStat(ShopCommStat shopCommStat);

    public void updateShopCommStat(ShopCommStat shopCommStat);

}
