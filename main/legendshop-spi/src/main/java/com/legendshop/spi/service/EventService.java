/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserEvent;

/**
 * 记录系统日志.
 */
public interface EventService {

	public UserEvent getEvent(Long id);

	public void deleteEvent(UserEvent userEvent);

	public Long saveEvent(UserEvent userEvent);

	public void updateEvent(UserEvent userEvent);

	public PageSupport<UserEvent> getEventPage(String curPageNO, UserEvent userEvent);
}
