/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProductCommentStat;

/**
 * 评论统计服务.
 */
public interface ProductCommentStatService  {

    public ProductCommentStat getProductCommentStat(Long id);
    
    public void deleteProductCommentStat(ProductCommentStat productCommentStat);
    
    public Long saveProductCommentStat(ProductCommentStat productCommentStat);

    public void updateProductCommentStat(ProductCommentStat productCommentStat);

	public ProductCommentStat getProductCommentStatByProdId(Long prodId);
	
	public ProductCommentStat getProductCommentStatByShopId(Long shopId);

	public ProductCommentStat getProductCommentStatByProdIdForUpdate(Long prodId);

	public void reCalculateProdComments(Long prodId);
}
