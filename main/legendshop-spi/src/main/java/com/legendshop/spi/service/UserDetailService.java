/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.CoinImportResult;
import com.legendshop.model.dto.CoinRechargeLog;
import com.legendshop.model.dto.Select2Dto;
import com.legendshop.model.dto.UserDetailExportDto;
import com.legendshop.model.dto.UserResultDto;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.form.UserForm;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.model.securityCenter.UserInformation;

/**
 * 用户服务
 */
public interface UserDetailService{
	
	
	/**
	 *用户注册, 密码在调用方中加密
	 * 
	 */
	public abstract UserResultDto saveUserReg(String ip,UserForm form, String encodedPassword);
	

	/**
	 *  手机用户注册, 密码在调用方中加密
	 *  
	 **/
	public User saveUserReg(String userRegip, UserMobileForm form, String encodedPassword);


	/**
	 * 根据用户ID获取积分.
	 * 
	 * @param userName
	 *            the user name
	 * @return the long
	 */
	public abstract Integer getScore(String userId);


	/**
	 * 金币充值
	 */
	public String rechargeUserCoin(UserDetail userDetail,Double account);
	
	/** 金币退款 */
	public String refundCoin(UserDetail userDetail,Double amount);

	/**
	 * 根据用户名称找到用户详情
	 * 
	 */
	public abstract UserDetail getUserDetail(String userName);
	
	/**
	 * 根据Userid获得用户详情
	 */
	public abstract UserDetail getUserDetailById(String userId);
	
	/**
	 * 金币充值
	 */
	public CoinRechargeLog batchUpdateCoin(CoinImportResult result);
	
	/**
	 * 获取用户的可用余额
	 * @param userId
	 * @return
	 */
	public abstract Double getAvailablePredeposit(String userId);
	
	/**
	 * 获取金币
	 * @param userId
	 * @return
	 */
	public abstract Double getUserCoin(String userId);
	
	/**
	 * 删除用户,只能删除普通用户 因为没有采用外键，所以要一个一个删除 注意删除无法恢复，慎用.
	 *
	 */
	public abstract String deleteUserDetail(String userId, String userName);

	/**
	 * 重置密码.
	 * 
	 */
	public abstract boolean updatePassword(String userName, String mail,String password);

	/**
	 * 更新用户的注册信息
	 * 
	 */
	public abstract UserDetail updateUserReg(String userName, String registerCode);

	/**
	 * 检查客户是否存在 
	 * true: 客户已经存在 false: 客户不存在.
	 * 
	 */
	public abstract boolean isUserExist(String userName);

	/**
	 * 检查Email是否存在 
	 * true: Email已经存在 false: Email不存在.
	 */
	public abstract boolean isEmailExist(String email);
	
	
	/**
	 * 检查Email 是否唯一
	 * true: Email已经存在 false: Email不存在.
	 */
	public abstract boolean isEmailOnly(String email,String userName);
	
	/**
	 *检查 phone 是否唯一
	 *true: phone是唯一存在 false: phone不唯一.
	 */
	public abstract boolean isPhoneOnly(String phone,String userName);
	
	/**
	 * Phone是否存在true: Phone已经存在false: Phone不存在
	 * true: phone已经存在 false: phone不存在.
	 */
	public abstract boolean isPhoneExist(String Phone);
	
	/**
	 * 根据用户Id获取用户信息
	 * 
	 */
	public abstract User getUser(String userId);

	/**
	 * 更新用户信息
	 * 
	 */
	public abstract void uppdateUser(User user);
	
	
	/**
	 * 通过userName来获取用户的信息，联合UserSecurity
	 * 
	 */
	public abstract UserInformation getUserInfo(String userName);
	
	/**
	 * 通过Phone来获取用户的信息，联合UserSecurity
	 * 
	 */
	public abstract UserInformation getUserInfoByPhone(String Phone);
	
	/**
	 * 通过Email来获取用户的信息，联合UserSecurity
	 * 
	 */
	public abstract UserInformation getUserInfoByMail(String Email);
	
	/**
	 * 根据用户ID查找password
	 * 
	 */
	public abstract String getUserPassword(String UserId);
	
	/**
	 * 根据用户ID查找邮箱
	 * 
	 */
	public abstract String getUserEmail(String userId);
	
	/**
	 * 根据用户ID查找手机
	 * 
	 */
	public abstract String getUserMobile(String userId);
	
	/**
	 * 设置手机验证设为1，表示已经通过手机验证
	 * @param userName
	 */
	public abstract void updatePhoneVerifn(String userName);
	
	/**
	 * 设置支付密码验证设为1，表示支付密码已经设置
	 * @param userName
	 */
	public abstract void updatePaypassVerifn(String userName);
	
	/**
	 * 设置邮箱验证设为1，表示邮箱已经通过验证
	 * 
	 */
	public abstract void updatEmailVerifn(String username);
	
	/**
	 * 根据用户名称查找UserSecurity用户安全信息
	 * 
	 */
	public abstract UserSecurity getUserSecurity(String userName);
	
	/**
	 * 更新用户信息
	 * 
	 */
	public abstract void updateUser(User user);
	
	/**
	 * 更新用户详情
	 * 
	 */
	public abstract void updateUserDetail(UserDetail userDetail);
	
	/**
	 * 更新userSecurity用户信息
	 * 重新发送短信验证码
	 */
	public abstract void updateUserSecurity(String userName,String validateCode);

	/**
	 * 保存用户安全等级
	 */
	public abstract void saveDefaultUserSecurity(String userName);
	
	/**
	 * 对比验证码是否一致
	 * @param userName
	 * @param code
	 * @return
	 */
	public abstract boolean verifySMSCode(String userName,String code);
	
	/**
	 * 是否已验证邮箱
	 * @param userName
	 * @return
	 */
	public abstract Integer getMailVerifn(String userName);
	
	/**
	 * 是否已验证手机
	 * @param userName
	 * @return
	 */
	public abstract Integer getPhoneVerifn(String userName);
	
	/**
	 * 是否已验证支付密码
	 * @param userName
	 * @return
	 */
	public abstract Integer getPaypassVerifn(String userName);
	
	/**
	 * 更新账户的密码
	 * @param userName
	 * @param newPassword
	 */
	public abstract Boolean updateNewPassword(String userName,String newPassword);
	
	/**
	 * 将邮箱及邮箱验证码 更新到 ls_usr_security
	 * @param userName
	 * @param validateCode
	 * @param userEmail
	 */
	public abstract void updateSecurityMail(String userName,String validateCode,String userEmail);
	
	/**
	 * 更新已验证邮箱
	 * @param userEmail
	 * @param userName
	 */
	public abstract void updateUserEmail(String userEmail,String userName);
	
	/**
	 * 获得userSecurity表中的update_mail
	 * @param userName
	 * @return
	 */
	public abstract String getSecurityEmail(String userName);
	
	/**
	 * 更新次数
	 * @param number
	 */
	public abstract void updatetimes(Integer number,String userName);
	
	/**
	 * 根据用户昵称获取用户详情
	 * @return
	 */
	public abstract UserDetail getUserDetailForScoreUser(String nickName);
	
	
	/**
	 * 根据用户输入，把可能的邮件或者手机号码找到对应的用户名称
	 */
	public abstract String convertUserLoginName(String name);
	
	
	/**
	 * 根据商家输入，把可能的邮件或者手机号码找到对应的用户名称
	 */
	public abstract String convertShopUserLoginName(String name);
	
	/**
	 * 更新手机号码
	 */
	public abstract void updateUserMobile(String userMobile,String userName);
	
	/**
	 * 查找用户对应的出安全等级
	 */
	public abstract Integer getSecLevel(String userName);
	
	/**
	 * 查找头像路径
	 * @param userName
	 * @return
	 */
	public abstract String getPortraitPic(String userName);

	/**
	 * 获取用户的最后登录时间
	 * @param userName
	 * @return
	 */
	public abstract Date getLastLoginTime(String userName);

	/**
	 * 检查用户昵称是否存在
	 * @param nickName 用户昵称
	 * @return
	 */
	public abstract boolean isNickNameExist(String nickName);
	
	/**
	 * 检查站点是否存在
	 * @param siteName
	 * @return
	 */
	public abstract boolean isSiteNameExist(String siteName);
	
	
	/**
	 * 更新用户信息
	 * @param userSecurity
	 * @param userDetail
	 */
	public abstract void updateUserInfo(UserSecurity userSecurity, UserDetail userDetail);

	/** 
	 * 根据用户ID 修改 昵称 
	 * 
	 */
	public abstract void updateNickName(String nickName, String userId);

	/** 
	 * 根据用户ID修改用户的性别 
	 * 
	 */
	public abstract void updateSex(String sex, String userId);

	/** 
	 * 根据用户ID 更新用户的生日 
	 * 
	 */
	public abstract void updateBirthday(Date birthDate, String userId);

	/** 
	 * 根据 用户的手机号码 查找用户 
	 * 
	 */
	public abstract User getUserByMobile(String userMobile);
	
	/**
	 * @Description: 根据用户名查找用户
	 * @date 2016-5-6 
	 */
	public abstract User getUserByUserName(String userName);

	/**
	 * 根据昵称查找用户信息
	 */
	public abstract UserInformation getUserInfoByNickName(String name);
	
	/**
	 * 检查商店是否存在
	 */
	public boolean isShopExist(Long shopId);

	/**
	 * 根据用户名获取用户信息
	 * 
	 */
	public abstract User getUserByName(String userName);

	/**
	 * 根据微信OPENID获取用户信息
	 * @param wxOpenId
	 * @return
	 */
	public abstract User getUserByWxOpenId(String wxOpenId);
	
	/**
	 * 检查是否发过邮件Code
	 * @param userName
	 * @param code
	 * @return
	 */
	public abstract boolean isMailCodeExist(String userName, String code);

	/**
	 * 清除用户发过的邮件Code
	 * @param userName
	 */
	public abstract void clearMailCode(String userName);
	
	/**
	 * 清除用户发过的验证码
	 * @param userName
	 */
	public abstract void clearValidateCode(String userName);

	/**
	 * 商家注册
	 * @param userName
	 * @param shopDetail
	 * @return
	 */
	public abstract boolean saveShopReg(String userName, ShopDetail shopDetail);
	
	/**
	 * 根据昵称获取用户名称
	 * @param nickName
	 * @return
	 */
	public abstract String getUserNameByNickName(String nickName);
	
	/**
	 * 根据昵称获取用户详情
	 * @param nickName
	 * @return
	 */
	public abstract UserDetail getUserDetailByAll(String nickName);
	
	/**
	 * 根据用户名称获取用户昵称
	 * @param userName
	 * @return
	 */
	public abstract String getNickNameByUserName(String userName);
	
	/**
	 * 生成随机数的用户名称
	 */
	public abstract String generateUserName();
	
	/**
	 * 更新用户积分
	 * @param nickName
	 * @param userId
	 * @param score
	 * @return
	 */
	public abstract String updateUserScore(UserDetail user, IntegraLog integraLog);
	
	/**
	 * 检查微信是否有绑定过该手机号码
	 * @param mobile
	 * @return
	 */
	public boolean checkBinding(String mobile);

	/**
	 * 根据用户Id获取用户详情,加上表记录锁定
	 * @param userId
	 * @return
	 */
	public abstract UserDetail getUserDetailByidForUpdate(String userId);
	
	/**
	 *计算用户可用的优惠券数量
	 * 
	 */
	public Integer calUnuseCouponCount(String userId,String proType);

	/**
	 * 获取用户总数
	 * 
	 */
	public abstract long getUserTotalCount();

	/**
	 * 获取特定分页的用户详情
	 * @param currPage
	 * @param pageSize
	 * @return
	 */
	public abstract List<UserDetail> getUserDetailsByPage(int currPage,int pageSize);

	/**
	 * 计算用户等级
	 * 
	 */
	public abstract void calculateUserGrade(String userId,Integer gradeId,List<UserGrade> userGrades);


	public abstract UserDetail getUserDetailByIdNoCache(String userId);
	
	/**
	 * 更改用户状态
	 * @param user
	 * @param status
	 */
	public abstract void changeUserStatus(User user, Integer status);

	/**
	 * 查询用户是否是推广员
	 * @param userId
	 * @return
	 */
	public abstract boolean isPromoter(String userId);
	
	/**
	 * 根据shopId获取用户信息
	 * @param shopId
	 * @return
	 */
	public abstract UserDetail getUserDetailByShopId(Long shopId);


	public abstract List<UserDetailExportDto> findExportUserDetail(UserDetail user);


	/**
	 * 检查该用户是否为推广员
	 * @param userId
	 * @return
	 */
	public abstract boolean checkPromoter(String userId);


	public abstract String getNickNameByUserId(String userId);
	
	public abstract Long getShopIdByUserId(String userId);
	
	public abstract Integer upIsRegIM(String userId, Integer param);


	public abstract boolean isExitMail(String mail);
	
	public abstract List<Select2Dto> getSelect2User(String q, Integer currPage,
			Integer pageSize, String userId);


	int getSelect2UserTotal(String q, String userId);
	
	/**
	 * 根据OpenId获取用户名
	 * @param openId
	 * @param type
	 * @return
	 */
	public abstract String getUserNameByOpenId(String openId, String type);


	public abstract PageSupport<UserDetail> getUserDetailListPage(String haveShop, DataSortResult result,
			String curPageNO, UserDetail userDetail);
	
	void updateLastLoginTime(String userId);


	public abstract User getByOpenId(String openId);


	public abstract void bingUserOpenId(String userId, String openId);

    public abstract void bingUserOpenId(String userId, String openId,String unionid);



    public abstract int batchOfflineUser(String userIds);
	
	
	/**
	 * 通过手机号码和验证码验证是否正确
	 * @param mobile
	 * @param mobileCode
	 */
	public abstract boolean verifyMobileCode(String mobile, String mobileCode);

	//通过手机号或用户名来获取用户名
	public abstract String getUserNameByPhoneOrUsername(String userMobile, String userName);

	/**
	 * 更新账户可用预付款
	 * @param updatePredeposit 要更新的预付款金额
	 * @param originalPredeposit 原来旧的预付款金额
	 * @param userId 用户ＩＤ
	 * @return
	 */
	public abstract int updateAvailablePredeposit(Double updatePredeposit,Double originalPredeposit, String userId);

	/**
	 * 手动过期验证码
	 * @param mobile
	 * @param mobileCode
	 */
	void expiredVerificationCode(String mobile, String mobileCode);

	/**
	 * App 商家端根据手机号匹配用户信息
	 * @param userPhone
	 * @param userId
	 * @return
	 */
	List<UserDetail> getUserDetailByPhone(String userPhone, String userId);
}