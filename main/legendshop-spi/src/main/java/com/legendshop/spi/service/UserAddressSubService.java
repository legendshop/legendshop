/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.UserAddressSub;

/**
 *用户配送地址服务
 */
public interface UserAddressSubService  {

    public UserAddressSub getUserAddressSub(Long id);
    
    public UserAddressSub getUserAddressSubForOrderDetail(Long id);
    
    public void deleteUserAddressSub(UserAddressSub userAddressSub);
    
    public Long saveUserAddressSub(UserAddressSub userAddressSub);

    public void updateUserAddressSub(UserAddressSub userAddressSub);

	public UserAddressSub getUserAddressSub(Long addrId, Long version);
}
