/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ParamGroup;

/**
 * 参数组列表Service.
 */
public interface ParamGroupService  {

    public ParamGroup getParamGroup(Long id);
    
    public void deleteParamGroup(ParamGroup paramGroup);
    
    public Long saveParamGroup(ParamGroup paramGroup);

    public void updateParamGroup(ParamGroup paramGroup);

	public List<ParamGroup> getParamGroupList(Long groupId);

	public PageSupport<ParamGroup> getParamGroupPage(String curPageNO, ParamGroup paramGroup);

	public PageSupport<ParamGroup> getParamGroupPage(String groupName);
}
