/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.model.dto.presell.PresellSearchParamDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.PresellProd;

/**
 * 预售商品服务
 */
public interface PresellProdService {

	/** 获取预售商品 */
	public PresellProd getPresellProd(Long id);

	/** 删除预售商品 */
	public void deletePresellProd(PresellProd presellProd);

	/** 保存预售商品 */
	public Long savePresellProd(PresellProd presellProd);

	/** 更新预售商品 */
	public void updatePresellProd(PresellProd presellProd);


	/** 获取预售商品Dto */
	public PresellProdDto getPresellProdDto(Long id);

	/** 获取预售商品 */
	public PresellProd getPresellProd(String schemeName, Long shopId);

	/** 查询预售商品详情 */
	public PresellProdDetailDto getPresellProdDetailDto(Long id);

	/** 判断指定sku是否已参与其他营销活动 */
	public Boolean isAttendActivity(Long activeId, Long prodId, Long skuId);

	/** 获取预售商品 */
	public PresellProd getPresellProd(Long prodId, Long skuId);

	/** 查询预售商品页面 */
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, boolean isExpires, PresellProdDto presellProd);

	/** 查询预售商品页面 */
	public PageSupport<PresellProdDto> queryPresellProdListPage(String curPageNO, Long shopId, PresellSearchParamDto paramDto);

	public PageSupport<PresellProd> queryPresellProdListPage(String curPageNO, ProductSearchParms parms);

	public Integer batchDelete(String ids);

	public int batchStop(String ids);

	/***
	 * 获取已过期未完成的预售活动
	 * @param now
	 * @return
	 */
	public List<PresellProd> getPresellProdByTime(Date now);

	/**
	 * 获取审核过程中，未审核或审核拒绝后导致未能正常上线过期的预售活动
	 * @param now 当前时间
	 * @return
	 */
	public List<PresellProd> getAuditExpiredPresellProd(Date now);

	/**
	 * 释放预售活动商品sku
	 * @param presellProd 预售活动
	 */
	public void releasePresellProd(PresellProd presellProd);

	/**
	 * 获取营销数据
	 * @param id
	 * @return
	 */
    OperateStatisticsDTO getOperateStatistics(Long id);
}
