/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Map;

import com.legendshop.model.form.SysPaymentForm;


/**
 * 支付配置处理器
 */
public interface PaymentProcessor {

	/**
	 * return {status:ok,result:'处理成功'}
	 * @param paymentForm 
	 * @return
	 */
	public Map<String,Object> payto(SysPaymentForm paymentForm);

}
