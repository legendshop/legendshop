package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.UserPredepositDto;
import com.legendshop.model.entity.PdCashHolding;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.vo.CoinVo;
/**
 * 用户中心预付款管理服务
 *
 */
public interface UserCenterPredepositService {

	/**
	 * 账户余额及充值记录信息
	 * @param userId
	 * @param curPageNO
	 * @return
	 */
	UserPredepositDto findUserPredeposit(String userId, String curPageNO);

	
	/**
	 * 充值明细记录
	 * @param userId
	 * @param curPageNO
	 * @param pdrSn
	 * @return
	 */
	UserPredepositDto findRechargeDetail(String userId, String curPageNO,
			String pdrSn);
	
	/**
	 * 余额提现信息
	 * @param userId
	 * @param curPageNO
	 * @param pdcSn
	 * @param status
	 * @return
	 */
	UserPredepositDto findBalanceWithdrawal(String userId, String curPageNO,
			String pdcSn, Integer status);
	
	
	/**
	 * 用户冻结记录
	 */
	UserPredepositDto findBalanceHoding(String userId, String curPageNO, String sn, String type);

	/**
	 * 用户冻结记录
	 */
	PageSupport<PdCashHolding> getBalanceHoding(String userId, String curPageNO, String sn, String type);

	/**
	 * 删除充值单
	 * @param userId
	 * @param pdrSn
	 * @return
	 */
	String deleteRechargeDetail(String userId, String pdrSn);


	/**
	 * 去充值
	 * @param userId
	 * @param pdrAmount
	 * @return
	 */
	PdRecharge rechargePay(String userId, String userName,Double pdrAmount);


	CoinVo findUserCoin(String userId, String curPageNO);


	


}
