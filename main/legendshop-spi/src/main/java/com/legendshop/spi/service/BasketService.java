/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.entity.Basket;

/**
 * 购物车服务
 *
 */
public interface BasketService {
	
	/**
	 * 通过商品id删除购物车中的商品
	 * @param prodId
	 * @return
	 */
	public abstract void deleteBasketGoodsByProdId(Long prodId);

	/**
	 * 根据用户删除购物车
	 */
	public abstract void deleteBasketByUserId(String userId);

	/**
	 * 删除某一个购物车
	 */
	public abstract void deleteBasketById(Long id);


	/**
	 * 得到用户有效订单总数.
	 *
	 */
	public abstract Long getTotalBasketByUserId(String userId);


	/**
	 * 更新购物车
	 *
	 */
	public abstract void updateBasket(Basket basket);

	/**
	 * 获取购物车信息
	 */
	public abstract UserShopCartList getShoppingCarts(List<ShopCartItem> baskets);

	/**
	 * 移除购物车商品
	 */
	public abstract void deleteBasketById(Long id, String userId);

	public abstract Basket getBasketById(Long id);

	public abstract void deleteById(List<Long> removeBaskets);

	/**
	 * 获取购物车件数
	 */
	public abstract int getBasketCountByUserId(String userId);
	

	/**
	 * 保存到购物车.
	 */
	Long saveToCart(String userId, Long prodId, Integer count, Long skuId, Long shopId, String distUserName,Long storeId);
	
	/**
	 * 获取用户购物车信息
	 */
	public Basket getBasketByUserId(Long prodId, String  userId, Long sku_id);
	

	/**
	 * 未登录用户的购物车商品信息
	 */
	public abstract List<ShopCartItem> getShopCartItems(List<Basket> baskets);

	/**
	 * 登录用户的购物车商品信息
	 */
	List<ShopCartItem> getShopCartByUserId(String userId);

	/**
	 * 更新购物车商品数量
	 */
	public abstract void updateBasket(Long productId, Long skuId, Integer basketCount, String userId);

	/**
	 * 删除购物车商品
	 */
	public abstract void deleteBasketById(Long prodId, Long skuId, String userId);
	
	
	/**
	 * 根据购物车ID 查询购物信息
	 */
	public abstract List<ShopCartItem> loadBasketByIds(List<Long> basketIds,String userId);

	public abstract List<ShopCartItem> loadBasketByIds(List<Long> ids, String userId, Long shopId);
	
	/**
	 * 获取用户团购购物车信息
	 */
	ShopCartItem findGroupCart(Long groupId, Long productId, Long skuId);
	
	

	/**
	 * 获取购物车订单信息
	 */
	ShopCartItem findSeckillCart(Long seckillId, Long prodId, Long skuId);

	/**
	 * 改变  购物车项 的选中状态
	 */
	public abstract void updateBasketChkSts(Long basketId, String userId, int checkSts);

	/**
	 * 批量改变  购物车项 的选中状态
	 */
	public abstract void batchUpdateBasketChkSts(String userId, List<Basket> basketList);
	
	/**
	 * 根据商品ID更新购物车商品为失效状态
	 */
	public abstract int updateIsFailureByProdId(boolean isFailure, Long prodId);

	/**
	 * 批量删除购物车商品
	 */
	public abstract void deleteBasketByIds(String selectedSkuId, String userId);

	public abstract ShopCartItem findMergeGroupCart(Long mergeGroupId, Long productId, Long skuId);

	/**
	 * 区分失效商品
	 */
	public abstract List<ShopCartItem> differInvalidList(List<ShopCartItem> cartItems);

	/**
	 * 清空失效购物车商品
	 */
	public abstract void clearInvalidBaskets(String userId);

	/**
	 * 更改购物车已选门店
	 * @param productId
	 * @param skuId
	 * @param storeId
	 * @param userId
	 */
	public abstract void updateShopStore(Long productId, Long skuId, Long storeId, String userId);

}