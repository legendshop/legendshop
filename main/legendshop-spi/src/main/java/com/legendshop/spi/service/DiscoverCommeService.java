/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverComme;

/**
 * The Class DiscoverCommeService.
 * 发现文章评论表服务接口
 */
public interface DiscoverCommeService  {

   	/**
	 *  根据Id获取发现文章评论表
	 */
    public DiscoverComme getDiscoverComme(Long id);

    /**
	 *  根据Id删除发现文章评论表
	 */
    public int deleteDiscoverComme(Long id);
    
    /**
	 *  根据对象删除发现文章评论表
	 */
    public int deleteDiscoverComme(DiscoverComme discoverComme);
    
   /**
	 *  保存发现文章评论表
	 */	    
    public Long saveDiscoverComme(DiscoverComme discoverComme);

   /**
	 *  更新发现文章评论表
	 */	
    public void updateDiscoverComme(DiscoverComme discoverComme);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverComme> queryDiscoverComme(String curPageNO,Integer pageSize);
    
}
