/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.BiddersWin;

/**
 * 拍卖中标服务.
 */
public interface BiddersWinService  {

    public BiddersWin getBiddersWin(String userId,Long id);
    
    public BiddersWin getBiddersWinByAid(Long aid);
    
    public List<BiddersWin> getBiddersWin(String userId);
    
    public List<BiddersWin> getBiddersWin(Date date,int commitInteval,Integer status);

    public BiddersWin getBiddersWin(Long id);
    
    public List<BiddersWin> getBiddersWinList(Long id);
    
    public void deleteBiddersWin(BiddersWin biddersWin);
    
    public Long saveBiddersWin(BiddersWin biddersWin);

    public void updateBiddersWin(BiddersWin biddersWin);

	public boolean isWin(String userId, Long prodId, Long skuId, Long paimaiId);

	public ShopCartItem findBiddersWinCart(Long paimaiId, Long prodId,Long skuId);

	public void updateBiddersWin(Long id, String subNember,Long subId, int value);

	public PageSupport<BiddersWin> getBiddersWinPage(String curPageNO, BiddersWin biddersWin);

	public PageSupport<BiddersWin> queryBiddersWinListPage(String curPageNO, String userId);

	public PageSupport<BiddersWin> queryUserPartAution(String curPageNO, String userId);
	
	/**
	 * 清空缓存
	 * @param id
	 */
	public void cleanAuctions(Long id);
	public void cleanAuctionsDetail(Long id);
}
