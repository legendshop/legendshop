/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdRecharge;

/**
 * 预存款充值表服务.
 */
public interface PdRechargeService  {

    public PdRecharge getPdRecharge(Long id);
    
    public void deletePdRecharge(PdRecharge pdRecharge);
    
    public Long savePdRecharge(PdRecharge pdRecharge);

    public void updatePdRecharge(PdRecharge pdRecharge);

	public int deleteRechargeDetail(String userId, String pdrSn);

	public PdRecharge findRechargePay(String userId, String paySn,Integer status);
	
	public PdRecharge findRechargePay(String paySn) ;
	
	public PdRecharge getPdRecharge(Long id, String userId);

	public PageSupport<PdRecharge> getRechargeDetailedPage(String curPageNO, String userId, String state);

	public PageSupport<PdRecharge> getPdRechargeListPage(String curPageNO, PdRecharge pdRecharge, Integer pageSize, DataSortResult result);

	public Integer batchDelete(String ids);

	/**
	 * 充值明细列表
	 * @param userId 用户Id
	 * @param state 支付状态 0未支付1支付
	 * @param pdrSn 唯一的流水号
	 * @param curPageNO 当前页
	 * @param pageSize 每页大小
	 *
	 */
	public PageSupport<PdRecharge> getPdRechargePage(String userId, String state, String pdrSn,String curPageNO, int pageSize);
}
