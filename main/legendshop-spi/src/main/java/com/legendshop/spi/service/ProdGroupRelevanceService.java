/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdGroupRelevance;
import com.legendshop.model.entity.Product;

/**
 * The Class ProdGroupRelevanceService.
 * 服务接口
 */
public interface ProdGroupRelevanceService  {

   	/**
	 *  根据Id获取
	 */
    public ProdGroupRelevance getProdGroupRelevance(Long id);

    /**
	 *  根据Id删除
	 */
    public int deleteProdGroupRelevance(Long id);

	/**
	 * 根据分组id判断商品
	 * @param prod
	 * @param groupId
	 * @return
	 */
	public int getProductByGroupId(Long prod, Long groupId);

    /**
	 *  根据prodId删除
	 */
    public int deleteProdGroupRelevanceByprod(Long id);
    
    /**
	 *  根据对象删除
	 */
    public int deleteProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);
    
   /**
	 *  保存
	 */	    
    public Long saveProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);

   /**
	 *  更新
	 */	
    public void updateProdGroupRelevance(ProdGroupRelevance prodGroupRelevance);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<ProdGroupRelevance> queryProdGroupRelevance(String curPageNO,Integer pageSize);

	/**
	 * 移除分组内商品
	 * @param prodId
	 * @param groupId
	 */
	void removeProd(Long prodId, Long groupId);
}
