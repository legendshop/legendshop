/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;

/**
 * 头条管理服务
 */
public interface HeadlineAdminService  {

    public Headline getHeadlineAdmin(Long id);
    
    public void deleteHeadlineAdmin(Long id);
    
    public Long saveHeadlineAdmin(Headline headline);

    public void updateHeadlineAdmin(Headline headline);

	public PageSupport<Headline> getHeadlineAdmin(String curPageNO, Headline headline);

	public Long saveHeadlineAdmin(String userName, String userId, Long shopId, String pic, MultipartFile imageFile, Headline headline);
}
