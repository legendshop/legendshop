/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * 微信用户服务.
 */
public interface WeiXinUserService {

	/**
	 * Gets the gzuser info.
	 *
	 * @param openId the open id
	 * @return the gzuser info
	 */
	abstract WeixinGzuserInfo getGzuserInfo(String openId);

	/**
	 * Gets the user id by open id.
	 *
	 * @param openId the open id
	 * @return the user id by open id
	 */
	String getUserIdByOpenId(String openId);


	/**
	 * Auto reg bind.
	 *
	 * @param ip the ip
	 * @param gzuserInfo the gzuser info
	 * @return the passport
	 */
	abstract Passport autoRegBind(String ip, WeixinGzuserInfo gzuserInfo);

	/**
	 * Save weixin gzuser info.
	 *
	 * @param weixinGzuserInfo the weixin gzuser info
	 * @return the long
	 */
	Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
}
