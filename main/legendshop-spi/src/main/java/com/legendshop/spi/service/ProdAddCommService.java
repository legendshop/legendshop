/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProdAddComm;

/**
 * 商品二次评论服务
 */
public interface ProdAddCommService  {

    public ProdAddComm getProdAddComm(Long id);
    
    public void deleteProdAddComm(ProdAddComm prodAddComm);
    
    public Long saveProdAddComm(ProdAddComm prodAddComm);

    public void updateProdAddComm(ProdAddComm prodAddComm);

	public ProdAddComm getProdAddCommByCommId(Long commId);
}
