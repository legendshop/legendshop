/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.integral.IntegralOrderItem;

/**
 * 积分订单项表服务.
 */
public interface IntegralOrderItemService  {

    public IntegralOrderItem getIntegralOrderItem(Long id);
    
    public void deleteIntegralOrderItem(IntegralOrderItem integralOrderItem);
    
    public Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem);

    public void updateIntegralOrderItem(IntegralOrderItem integralOrderItem);

    public List<IntegralOrderItem> queryByUserIdAndProdId(String userId, Long prodId);
}
