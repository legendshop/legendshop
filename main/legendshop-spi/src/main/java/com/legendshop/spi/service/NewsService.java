/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.constant.NewsPositionEnum;
import com.legendshop.model.entity.News;

/**
 * 文章的服务.
 */
public interface NewsService {

	/**
	 * 获取所有的文章.
	 *
	 * @return the list
	 */
	public abstract List<News> getNewsList();

	/**
	 * 根据文章ID找到上一个下一个文章.
	 *
	 * @param id the id
	 * @return the news
	 */
	public abstract List<News> getNearNews(Long id);

	/**
	 * 查找文章.
	 * 
	 * @param id
	 *            the id
	 * @return the news
	 */
	public abstract News getNewsById(Long id);

	/**
	 * Gets the news by cat id.
	 *
	 * @param catid
	 *            the catid
	 * @return the news by cat id
	 */
	public abstract List<News> getNewsByCatId(Long catid);

	/**
	 * 删除文章.
	 *
	 * @param newsId the news id
	 */
	public abstract void delete(Long newsId);

	/**
	 * 保存文章.
	 * 
	 * @param news
	 *            the news
	 * @return the long
	 */
	public abstract Long save(News news);

	/**
	 * Update.
	 * 
	 * @param news
	 *            the news
	 */
	public abstract void update(News news);

	/**
	 * Gets the news.
	 *
	 * @param newsPositionEnum the news position enum
	 * @param num the num
	 * @return the news
	 */
	public abstract List<News> getNews(final NewsPositionEnum newsPositionEnum, final Integer num);

	/**
	 * Gets the news.
	 *
	 * @param curPageNO the cur page no
	 * @param newsCategoryId the news category id
	 * @return the news
	 */
	public abstract PageSupport<News> getNews(String curPageNO, Long newsCategoryId);

	/**
	 * Gets the news by category.
	 *
	 * @return the news by category
	 */
	public abstract Map<KeyValueEntity, List<News>> getNewsByCategory();

	/**
	 * Gets the news byposition.
	 *
	 * @param catid the catid
	 * @return the news byposition
	 */
	public abstract List<News> getNewsByposition(Long catid);

	/**
	 * Gets the serach news.
	 *
	 * @param keyword the keyword
	 * @param curPageNO the cur page no
	 * @return the serach news
	 */
	public abstract PageSupport<News> getSerachNews(String keyword, String curPageNO);

	/**
	 * Help.
	 *
	 * @param curPageNO the cur page no
	 * @return the page support< news>
	 */
	PageSupport<News> help(String curPageNO);

	/**
	 * Gets the news list page.
	 *
	 * @param curPageNO the cur page no
	 * @param news the news
	 * @param pageSize the page size
	 * @param result the result
	 * @return the news list page
	 */
	public abstract PageSupport<News> getNewsListPage(String curPageNO, News news, Integer pageSize, DataSortResult result);

	/**
	 * Gets the news list result dto.
	 *
	 * @param curPageNO the cur page no
	 * @param pageSize the page size
	 * @param result the result
	 * @return the news list result dto
	 */
	public abstract PageSupport<News> getNewsListResultDto(String curPageNO, Integer pageSize, DataSortResult result);

	/**
	 * 更新状态
	 * @param news
	 */
	public abstract void updateStatus(News news);

	/**
	 * 按分页获取商家的新闻
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	PageSupport<News> getNewsList(String curPageNO, Long shopId);

	Map<KeyValueEntity, List<News>> findByCategoryId();
}