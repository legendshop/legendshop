package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.NewsCategory;

/**
 * 新闻分类服务
 */
public interface NewsCategoryService {


	public abstract List<NewsCategory> getNewsCategoryList();

	public abstract NewsCategory getNewsCategoryById(Long id);

	public abstract void delete(Long id);

	public abstract Long save(NewsCategory newsCategory);

	public abstract void update(NewsCategory newsCategory);

	public abstract PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO, NewsCategory newsCategory);

	/**
	 * 获取新闻栏目类表
	 * @param curPageNO
	 * @return
	 */
	public abstract PageSupport<NewsCategory> getNewsCategoryListPage(String curPageNO);

}