package com.legendshop.spi.service;

import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;

/**
 * 预览商家装修
 */
public interface ShopDecotatePreviewService {
	
	/**
	 * 预览商家装修
	 */
	public  ShopDecotateDto findShopPreviewDecotateDto(ShopDecotateDto decotate);

}
