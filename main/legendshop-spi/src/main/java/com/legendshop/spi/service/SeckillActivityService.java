/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * 秒杀活动Service.
 */
public interface SeckillActivityService  {

	/** 获取秒杀活动数据 */
    public SeckillActivity getSeckillActivity(Long id);
    
    /** 删除秒杀活动数据 */
    public void deleteSeckillActivity(SeckillActivity seckillActivity);
    
    /** 保存秒杀活动数据 */
    public Long saveSeckillActivity(SeckillActivity seckillActivity);
    
    /** 保存秒杀活动商品 */
    public boolean saveSeckillActivityAndProd(SeckillActivity seckillActivity,String userId,Long shopId,String shopName,String userName,Long status);

    /** 更新秒杀活动商品 */
    public boolean updateSeckillActivityAndProd(SeckillActivity seckillActivity,Long shopId,String userName);
    
	/** 更新秒杀活动数据 */
    public void updateSeckillActivity(SeckillActivity seckillActivity);

	/** 获取秒杀活动详情 */
	public SeckillDto getSeckillDetail(Long seckillId);

	/** 更新秒杀商品类型 */
	public void updateProdType(Long id, String value);

	/** 查找秒杀活动列表 */
	public PageSupport<SeckillActivity> querySeckillActivityList(String startTime);

	/** 获取秒杀活动数据 */
	public PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, int pageSize, SeckillActivity seckillActivity);

	/** 获取秒杀活动数据 */
	public PageSupport<SeckillActivity> getSeckillActivity(String curPageNO, Long shopId, int pageSize, SeckillActivity seckillActivity);

	/** 获取秒杀活动列表 */
	public PageSupport<SeckillActivity> querySeckillActivityList(String curPageNO, String startTime);

	/** 获取秒杀活动列表 */
	public PageSupport<SeckillActivity> queryFrontSeckillList(String curPageNO, String startTime);

	public boolean findIfJoinSeckill(Long productId);

	/**
	 * 平台上下线
	 */
	public String updateSeckillActivity(Long id, Long status);

	/**
	 * 商家上下线
	 * @param seckillPo 
	 */
	public String shopUpdateSeckillActivity(SeckillActivity seckillPo, Long id, Long status);
	
	/**
	 * 获取过期未完成秒杀活动
	 * @param now
	 * @return
	 */
	public List<SeckillActivity> getSeckillActivityByTime(Date now);

	/**
	 * 根据秒杀id，回滚剩余库存和下单后未付款订单库存
	 * @param seckillId
	 * @return
	 */
	public boolean stockRollback(Long seckillId);
	
	/**
	 * 获取状态为“上线中”的未完成的秒杀活动
	 * @return
	 */
	public List<SeckillActivity> queryUnfinishSeckillActivity();
	
	/**
	 * 获取状态为“已完成，释放sku”的秒杀活动
	 */
	public List<SeckillActivity> querySeckillActivityByStatus(Long status);

	/**
	 * 根据shopId和活动Id查询秒杀活动
	 * @param shopId
	 * @param acitveId
	 * @return
	 */
	public SeckillActivity getSeckillActivityByShopIdAndActiveId(Long shopId, Integer acitveId);

	/**
	 * 根据id获取秒杀成功记录
	 * @param id
	 * @return
	 */
	public List<SeckillSuccess> getSeckillSucessBySeckillId(Long seckillId);

}
