package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.buy.UserShopCartList;

/**
 * 订单处理器
 *
 */
public interface OrderCartResolverManager {

	/**
	 * 获取订单详情
	 * @param params
	 * @return
	 */
	UserShopCartList queryOrders(CartTypeEnum typeEnum, Map<String, Object> params);

	/**
	 * 下单
	 * @param typeEnum 
	 * @param userShopCartList
	 * @return
	 */
	List<String> addOrder(CartTypeEnum typeEnum, UserShopCartList userShopCartList);

}