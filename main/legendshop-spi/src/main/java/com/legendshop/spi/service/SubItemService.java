/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.SubItem;

import java.util.Date;
import java.util.List;

/**
 * 订单项服务.
 */
public interface SubItemService  {

    public List<SubItem> getSubItem(String subNumber,String userId);

    public SubItem getSubItem(Long id);
    
    public void deleteSubItem(SubItem subItem);
    
    public Long saveSubItem(SubItem subItem);

    public void updateSubItem(SubItem subItem);

	public List<SubItem> getSubItem(String subNumber);

	public void updateSnapshotIdById(Long snapshotId, Long subItemId);

	/**
	 * 更新订单项 的评论状态
	 * @param subItemId
	 */
	public void updateSubItemCommSts(Long subItemId,String userId);
	
	/**
	 * 获取待评价商品条数
	 * @param userId
	 * @return
	 */
	public Integer getUnCommProdCount(String userId);
	

	public List<Long> saveSubItem(List<SubItem> subItems);

	/**
	 * 根据订单ID获取订单项信息
	 * @param subId
	 * @return
	 */
	public List<SubItem> getSubItemBySubId(Long subId);


	public PageSupport<SubItem> querySubItems(String curPageNO, Long shopId, String distUserName, Integer status,
			String subItemNum, Date fromDate, Date toDate);

	public PageSupport<SubItem> querySubItems(String curPageNO, String userName, Integer status, String subItemNum,
			Date fromDate, Date toDate);
	
	/**
	 * 根据订单numbet获取订单项信息
	 * @param subNumber
	 * @return
	 */
	public List<SubItem> getDistSubItemBySubNumber(String subNumber);

	/**
	 * 查询已完成，未评论，且满足时间要求的订单
	 * */
	List<SubItem> getNoCommonSubItemList(Date time);

	/**
	 * 查询结算分销订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<SubItem> queryDistSubItemList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill);

	/**
	 * 查询退货退款订单项
	 * @param refundId
	 * @return
	 */
	List<SubItem> getSubItemByReturnId(Long refundId);
}
