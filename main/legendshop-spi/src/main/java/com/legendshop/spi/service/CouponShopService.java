/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.CouponShop;
/**
 * 红包 店铺关联服务
 */
public interface CouponShopService  {

    public List<Long> saveCouponShops(List<CouponShop> couponShops);

    public List<CouponShop> getCouponShopByCouponId(Long couponId);

	public List<String> getShopNamesByCouponId(Long id);

	public List<CouponShop> getCouponShopByCouponIds(List<Long> couponIds);


}
