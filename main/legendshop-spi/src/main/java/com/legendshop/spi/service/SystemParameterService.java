/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SystemParameter;

/**
 * 系统参数服务.
 */
public interface SystemParameterService {

	/**
	 * Load.
	 * 
	 * @param id
	 *            the id
	 * @return the system parameter
	 */
	public abstract SystemParameter getSystemParameter(String id);

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void delete(String id);

	/**
	 * Update.
	 * 
	 * @param systemParameter
	 *            the system parameter
	 */
	public abstract void update(SystemParameter systemParameter);

	/**
	 * 初始化System parameter
	 */
	public abstract List<SystemParameter>  initSystemParameter();

	/**
	 * 根据配置分类查询系统配置
	 * @param groupId 配置分类
	 * @return
	 */
	public abstract List<SystemParameter> getSystemParameterByGroupId(String groupId);

	public abstract PageSupport<SystemParameter> getSystemParameterListPage(String curPageNO, String name, String groupId);

}