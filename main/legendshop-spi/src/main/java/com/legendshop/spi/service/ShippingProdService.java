/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.FullActiveProdDto;

/**
 * 包邮活动商品Service
 */
public interface ShippingProdService  {

    public ShippingProd getShippingProd(Long id);
    
    public void deleteShippingProd(ShippingProd shippingProd);
    
    public Long saveShippingProd(ShippingProd shippingProd);

    public void updateShippingProd(ShippingProd shippingProd);

	public List<FullActiveProdDto> getActiveProdByActiveId(Long id, Long shopId);

	public List<ShippingProd> getProdByActiveId(Long id, Long shopId);

}
