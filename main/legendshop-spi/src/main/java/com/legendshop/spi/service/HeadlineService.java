/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Headline;

/**
 * WAP头条新闻表服务.
 */
public interface HeadlineService  {

    public Headline getHeadline(Long id);
    
    public void deleteHeadline(Headline headline);
    
    public Long saveHeadline(Headline headline);

    public void updateHeadline(Headline headline);

    /**
     * 查询所有的头条
     * @return
     */
    public List<Headline> queryByAll();

    PageSupport<Headline> query(String curPageNO);
    
	public PageSupport<Headline> getHeadlinePage(String curPageNO);
}
