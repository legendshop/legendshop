/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.model.entity.Category;

import java.util.List;
import java.util.Set;

/**
 * 商品分类树.
 */
public interface CategoryService  {

    public Category getCategory(Long id);
    
    public void deleteCategory(Category category);
    
    public Long saveCategory(Category category);

    public void updateCategory(Category category);

    /**
     * 获取所有分类信息 ，用于后台分类树组建
     * @return
     */
	public List<Category> getCategoryList(String type);


	/**
	 * 获取所有分类信息 ，用于后台分类树组建
	 * @return
	 */
	public List<Category> getCategoryListByStatus(String type);

	/**
	 * 获取到所有的商品分类
	 * @return
	 */
	public abstract Set<CategoryDto> getAllCategoryByType(String type);
	

	/** 根据 parentId 查找 商品分类 **/
	public List<Category> findCategory(Long parentId);

	/** 根据 分类名称  查找 商品分类 **/
	public List<Category> findCategory(String cateName,Integer level,Long parentId);

	/**
	 * 传入节点 得到该所有的子节点 
	 * @param parentId
	 * @param navigationMenu
	 * @param type
	 * @param status
	 * @return
	 */
	List<Category> getAllChildrenByFunction(Long parentId,Integer navigationMenu, String type, Integer status);

	public List<Category> getCategoryByIds(List<Long> catIdList);

	public void updateCategoryList(List<Category> categorys);

	public List<Category> getCategorysByTypeId(Integer typeId);

	/** 查找 首页导航栏上显示的分类 **/
	public List<Category> queryCategoryList();

	/**
	 * 更新商品分类树的缓存
	 */
	void cleanCategoryCache();

	/**
	 * 根据名称 找出所有的分类
	 * @param categoryName
	 * @return
	 */
	public List<Category> getCategoryListByName(String categoryName);



	/** 根据  产品类目类型 和  等级 查找 分类**/
	public List<Category> queryCategoryList(String type, Integer level);

	public PageSupport<Category> getCategoryPage(String curPageNO);
	
	public List<Category> getCategory(String sortType, Integer headerMenu,Integer navigationMenu, Integer status,Integer level);

	/** 根据类型id获取商品类目 */
	public List<Category> findProdTypeId(Integer prodTypeId);


	/**
	 * 根据分类等级获取分类
	 * @param grade 等级
	 * @param parentId 父级Id
	 * @return
	 */
	List<Category> findCategoryByGradeAndParentId(Integer grade, Long parentId);
}
