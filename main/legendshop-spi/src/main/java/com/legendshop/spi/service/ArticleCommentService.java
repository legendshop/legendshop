/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ArticleComment;

/**
 * 文章评论服务接口
 */
public interface ArticleCommentService {

	/**
	 * 根据Id获取
	 */
	public ArticleComment getArticleComment(Long id);

	/**
	 * 删除
	 */
	public void deleteArticleComment(ArticleComment articleComment);

	/**
	 * 保存
	 */
	public Long saveArticleComment(ArticleComment articleComment);

	/**
	 * 更新
	 */
	public void updateArticleComment(ArticleComment articleComment);

	public ArticleComment getArticleCommentLinkArt(Long id);

	/**
	 * @Description: TODO 修改文章评论状态
	 * @param articleComment 文章实体
	 * @param status 审核状态
	 */
	public int updateStatus(ArticleComment articleComment, int status);

	PageSupport<ArticleComment> queyArticleComment(String curPageNO, Long id);

	PageSupport<ArticleComment> queyArticleCommentScroll(String curPageNO, Long id);
	
	public PageSupport<ArticleComment> getArticleCommentPage(String curPageNO, ArticleComment articleComment);
}
