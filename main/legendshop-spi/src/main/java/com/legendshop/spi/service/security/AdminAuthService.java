/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service.security;

import com.legendshop.model.security.UserEntity;

/**
 * 管理员登录验证服务接口.
 */
public interface AdminAuthService {
	
	/**
	 * 凭用户名直接登录， 查找用户
	 */
	public UserEntity loadUserByUsername(String username);
	
	/**
	 * 根据用户名密码获取用户
	 */
	public UserEntity loadUserByUsername(String username,String  presentPassword);

	
}