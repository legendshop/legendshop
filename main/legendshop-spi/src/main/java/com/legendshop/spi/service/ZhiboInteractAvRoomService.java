/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;

import com.legendshop.model.entity.ZhiboAvRoom;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;

/**
 * 直播角色心跳表.
 * 服务接口
 */
public interface ZhiboInteractAvRoomService  {

   	/**
	 *  根据Id获取
	 */
    public ZhiboInteractAvRoom getZhiboInteractAvRoom(String id);

   /**
	 *  删除
	 */    
    public void deleteZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);

   /**
	 *  保存
	 */	    
    public String saveZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);

   /**
	 *  更新
	 */	
    public void updateZhiboInteractAvRoom(ZhiboInteractAvRoom zhiboInteractAvRoom);

    public int createRoom(String uid);
    
    public ZhiboAvRoom getLastAvRoom(String uid);
   
    public int enterRoom(String uid,Long av_room_id,String status,Date modify_time,Long role);
    
    public int updateRoomInfoById(String uid,Long av_room_id,String title,String cover);
    
    public int saveNewLiveRecord(ZhiboNewLiveRecord zhiboNewLiveRecord);
    
    /**
	 * 踢人
	 * @return
	 */
	public int kickingMemberByUid(String uid, Integer roomid);
}
