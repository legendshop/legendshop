/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProductSnapshot;
import com.legendshop.model.entity.SubItem;

/**
 * 商品快照Service.
 */
public interface ProductSnapshotService  {

    public ProductSnapshot getProductSnapshot(Long id);
    
    /**
     * 保存商品快照
     */
	public void saveProdSnapshot(SubItem subItem);

}
