/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MobileFloor;

/**
 * 手机端html5 首页楼层服务.
 */
public interface MobileFloorService  {

    public MobileFloor getMobileFloor(Long id);
    
    public void deleteMobileFloor(MobileFloor mobileFloor);
    
    public Long saveMobileFloor(MobileFloor mobileFloor);

    public void updateMobileFloor(MobileFloor mobileFloor);

	public List<MobileFloor> queryMobileFloor(Long shopId);

	public PageSupport<MobileFloor> getMobileFloorPage(String curPageNO, MobileFloor mobileFloor);
}
