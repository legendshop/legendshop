/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.ShopUrlPermissionDto;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;

/**
 * 计算商家菜单服务
 */
public interface ShopMenuGroupService {
	
	public  List<ShopMenuGroup>  getShopMenuGroup();
	
	public List<String> getAllLinks();

	/**
	 * 根据请求URL获取权限
	 * @param path url
	 */
	public ShopUrlPermissionDto getShopMenuByURL(String path);

}
