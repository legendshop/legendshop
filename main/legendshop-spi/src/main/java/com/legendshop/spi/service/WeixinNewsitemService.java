/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.weixin.WeixinNewsItemsDto;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * 微信新闻项目服务
 */
public interface WeixinNewsitemService  {

    public WeixinNewsitem getWeixinNewsitem(Long id);
    
    public void deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem);
    
    public Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem);

    public void updateWeixinNewsitem(WeixinNewsitem weixinNewsitem);

	public List<WeixinNewsitem> getNewsItemsByTempId(Long tempId);

	public void saveWeixinNewsitemList(List<WeixinNewsitem> addItems);

	public void updateWeixinNewsitemList(List<WeixinNewsitem> updateItems);

	public void deleteWeixinNewsitemByIds(List<Long> ids);

	public PageSupport<WeixinNewsitem> getWeixinNewsitemPage(String curPageNO);

	public void saveWeixinNewsitemList(WeixinNewsItemsDto newsItemsDto, String userId);
}
