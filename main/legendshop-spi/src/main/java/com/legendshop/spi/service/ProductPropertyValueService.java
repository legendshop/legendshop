/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;

/**
 *
 * 商品属性值
 *
 */
public interface ProductPropertyValueService  {

    public List<ProductPropertyValue> getAllProductPropertyValue(List<ProductProperty> propertyList);
    
    public ProductPropertyValue getProductPropertyValue(Long id);
    
    public void deleteProductPropertyValue(ProductPropertyValue productPropertyValue);
    
    public Long saveProductPropertyValue(ProductPropertyValue productPropertyValue);

    public void updateProductPropertyValue(ProductPropertyValue productPropertyValue);

    public List<ProductPropertyValue> queryProductPropertyValue(Long propId);

    /** 保存自定义属性值 **/
	public List<ProductPropertyValue> saveCustomAttributeValues(Long propId, List<String> propValue);

	ProductPropertyValue getProductPropertyValueInfo(Long valueId, Long propId);

    public ProductPropertyValue getProductPropertyValueInfo(String name, Long propId);

	public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String curPageNO);

	public PageSupport<ProductPropertyValue> getProductPropertyValue(String valueName, Long propId);

	public PageSupport<ProductPropertyValue> getProductPropertyValuePage(String valueName, Long propId);


}
