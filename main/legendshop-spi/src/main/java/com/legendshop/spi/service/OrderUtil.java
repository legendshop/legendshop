package com.legendshop.spi.service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.entity.Product;

/**
 * 订单处理工具
 *
 */
public interface OrderUtil {

	/**
	 * 下单
	 */
	JSONObject buyNow(Boolean buyNow, Long prodId, Long skuId, Integer count);

	/**
	 * 检查订单参数
	 * @return
	 */
	AddOrderMessage checkSubmitParam(String userId, Integer sessionToken, CartTypeEnum typeEnum, OrderAddParamDto paramDto);

	/**
	 * remarkText：买家留言
	 * 处理买家留言
	 */
	void handlerRemarkText(String remarkText, UserShopCartList userShopCartList);

	/**
	 * 处理运费模板
	 * @param delivery
	 * @param userShopCartList
	 * @return
	 */
	String handlerDelivery(String delivery, UserShopCartList userShopCartList);

	/**
	 * 处理订单的优惠券信息
	 * @param couponStr
	 * @param userShopCartList
	 */
	void handleCouponStr(UserShopCartList userShopCartList);

	/**
	 * 检测下单的清单有商品库存不足的清单
	 * @return
	 */
	String verifySafetyStock(UserShopCartList userShopCartList);

	/**
	 * 检查并计算订单的库存
	 * @param userShopCartList
	 */
	String handelOrderStocks(UserShopCartList userShopCartList);

	/**
	 * 检查商品是否可以购买
	 * TODO  这里的库存检查只是粗略的检查商品和SKU的库存，没有检查仓库的库存
	 * @param prodId
	 * @param skuId
	 * @param count
	 * @param userId 
	 * @return
	 */
	String checkCartParams(Product product, Long skuId, Integer count, String userId);
	
	
	/**
	 * 检查门店商品是否可以购买
	 * TODO  检查门店的sku库存是否可以购买
	 * @param prodId
	 * @param skuId
	 * @param count
	 * @param userId 
	 * @param storeId 门店ID
	 * @return
	 */
	String checkStoreCartParams(Product product, Long skuId, Integer count, String userId, Long storeId);

	
	/**
	 * 处理门店提货人名称
	 * @param buyerName 提货人名称
	 */
	void handlerStoreBuyerName(String buyerName, UserShopCartList userShopCartList);

	
	/**
	 * 处理门店提货人手机号码 
	 * @param telPhone 提货人手机号码
	 */
	void handlerStoreTelPhone(String telPhone, UserShopCartList userShopCartList);

	/**
	 * 处理门店买家留言 
	 * @param remarkText 买家留言
	 */
	void handlerStoreRemarkText(String remarkText, UserShopCartList userShopCartList);

	/**
	 * 门店商品立即购买
	 * @param isBuyNow
	 * @param prodId
	 * @param skuId
	 * @param count
	 * @param storeId
	 * @return
	 */
	JSONObject storeBuyNow(Boolean isBuyNow, Long prodId, Long skuId, Integer count, Long storeId);

}