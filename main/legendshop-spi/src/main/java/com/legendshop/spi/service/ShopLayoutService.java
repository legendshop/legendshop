/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;

/**
 * 装修布局服务.
 */
public interface ShopLayoutService  {

    public ShopLayout getShopLayout(Long id);
    
    public void deleteShopLayout(ShopLayout shopLayout);
    
    public Long saveShopLayout(ShopLayout shopLayout);

    public void updateShopLayout(ShopLayout shopLayout);

    /**
     * 添加装修布局
     * @param layout
     * @param shopId
     * @param decId
     * @return
     */
	public ShopLayoutDto saveShopLayout(String layout, Long shopId, Long decId);

	
	/**
	 *  商城装修布局
	 */
	public ShopDecotateDto deleLayout(ShopDecotateDto decotateDto,ShopLayoutParam layoutParam);

	public Long getLayoutCount(Long shopId, Long decotate_id);
	
	public Long getLayoutCountOnline(Long shopId, Long decotate_id);

	public void delete(ShopLayout shopLayout);
	
	/**
	 * 删除布局相关的内容
	 * @param shopLayout
	 */
	public void deleteFullShopLayout(ShopLayout shopLayout);

	public void deleteShopLayout(Long shopId, Long layoutId);
}
