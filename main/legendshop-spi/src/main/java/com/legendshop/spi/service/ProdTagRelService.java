package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProdTagRel;
import com.legendshop.model.entity.Product;

/**
 * 商品标签关联服务
 */
public interface ProdTagRelService{

	public void saveProdTagRel(ProdTagRel prodTagRel);

	public ProdTagRel getProdTagRelByProdId(Long prodId);

	public boolean deleteProdTagRelByProdId(Long prodId);

	public void deleteProdTagRelByTagId(Long id);

	public PageSupport<ProdTagRel> queryProdTagRel(String curPageNO, Product product, Long shopId);

	public PageSupport<ProdTagRel> queryBindProdManage(String curPageNO, Long id, String prodName);

}
