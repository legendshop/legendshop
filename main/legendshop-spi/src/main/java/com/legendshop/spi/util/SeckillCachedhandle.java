/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.util;

import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * 秒杀缓存处理
 *
 */
public interface SeckillCachedhandle {

	/**
	 * 获取.
	 *
	 * @param seckillId
	 * @return the seckill dto
	 */
	public SeckillDto getSeckillDto(Long seckillId);

	/**
	 * 添加缓存
	 * 
	 * @param seckillId
	 * @return
	 */
	boolean addSeckill(SeckillDto seckillDto);

	/**
	 * 添加缓存
	 * 
	 * @param seckillId
	 * @return
	 */
	void delSeckill(Long seckillId);

	/**
	 * 初始化库存缓存
	 * 
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @param stock
	 */
	void initSeckillProdStock(Long seckillId, Long prodId, Long skuId, Long stock);

	/**
	 * 获取商品库存
	 * 
	 * @param seckId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	Integer getSeckillProdStock(Long seckId, Long prodId, Long skuId);

	/**
	 * 秒杀库存检查
	 * 
	 * @param seckill
	 * @param prodId
	 * @param skuId
	 * @param userId
	 * @return
	 */
	int seckillCheck(Long seckillId, long prodId, long skuId, String userId);

	/**
	 * 秒杀成功
	 */
	void seckillSuccess(SeckillSuccess success);

	/**
	 * 获取秒杀成功的对象信息
	 * 
	 * @param userid
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	SeckillSuccess getSeckillSucess(String userid, Long seckillId, Long prodId, Long skuId);
	
	public void rollbackSeckill(Long seckillId, long prodId, long skuId, String userId);

}
