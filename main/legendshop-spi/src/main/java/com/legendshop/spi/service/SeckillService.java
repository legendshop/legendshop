/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.seckill.*;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillSuccess;

import java.util.List;

/**
 * 秒杀服务.
 */
public interface SeckillService {

	/** 获取秒杀详情 */
	public SeckillDto getSeckillDetail(Long seckillId);

	/** 秒杀开关 */
	SeckillJsonDto<SeckillExposer> exportSeckillUrl(long seckillId, long prodId, long skuId);

	/** 执行秒杀 */
	int executeSeckill(long seckillId, long prodId, long skuId, String userId, Double price, int sckNumber);

	/**  数据回滚 */
	public void rollbackSeckill(long seckillId, Long prodId, Long skuId, String userId);

	/** 查询所有在线的秒杀活动 */
	public List<Long> findOnActive();

	/** 获取秒杀成功记录 */
	public SeckillSuccess getSeckillSucess(String userId, Long seckillId, long prodId, long skuId);

	/** 获取秒杀成功商品Dto */
	public SuccessProdDto findSuccessProdDto(SeckillSuccess seckillSuccess);

	/** 添加秒杀订单 */
	public List<String> addSeckillOrder(UserShopCartList userShopCartList, SeckillSuccess seckillSuccess);

	/** 更新秒杀成功数据 */
	public int updateSeckillSuccess(Long basketId, String userId);

	/** 删除秒杀记录 */
	public void deleteRecord(Long id, String userId);

	/** 查询秒杀成功的记录信息 */
	public PageSupport<SeckillSuccessRecord> querySeckillRecord(String curPageNO, String userId);

	/** 查询用户秒杀记录 */
	public PageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId);
	
	/** 根据秒杀活动id查询秒杀成功的记录信息*/
	public PageSupport<SeckillSuccess> querySeckillRecordListPage(String curPageNO, Long id);
	
	/** 取消超时未转订单资格 */
	public void cancleSeckillOrder(Long seckillId,List<SeckillSuccess> seckillSuccessList);
	
	/**
	 * 取消未支付秒杀订单的秒杀资格
	 * @param seckillId 秒杀活动ID
	 * @param userId 用户ID
	 */
	public int cancleUnpaymentSeckillOrder(Long seckillId, String userId);

	/**
	 * 根据活动获取运营数据
	 * @param id
	 * @return
	 */
    OperateStatisticsDTO getOperateStatistics(Long id);

	/**
	 * 获取数量
	 * @param id
	 * @return
	 */
	Integer getPersonCount(Long id);

	/**
	 *通过商品获取秒杀活动
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	SeckillActivity getSeckillByProd(Long prodId, Long skuId);
}
