/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.dto.Select2Dto;
import com.legendshop.model.entity.Brand;

/**
 * 品牌服务.
 */
public interface BrandService {

	public abstract List<Brand> getBrand(String userName);

	public abstract Brand getBrand(Long id);

	public abstract void delete(Long id);

	public abstract Long save(Brand brand);

	public abstract void update(Brand brand);

	public abstract String saveBrandItem(List<String> idList, Long nsortId, String userName);

	public abstract String saveBrandItem(String idJson, String nameJson, Long nsortId, String userName);

	public abstract boolean hasChildProduct(Long brandId);

	/**
	 * 显示所有默认商家精品品牌
	 * @return
	 */
	public abstract List<Brand> getAllCommentBrand();
	
	/**
	 * 显示所有品牌
	 * @return
	 */
	public abstract List<Brand> getAllBrand();
	
	/**
	 * 根据一级商品的id 查找出对应的品牌
	 */
	public abstract List<Brand> getBrandBySortId(Long sortId);
	
	/**
	 * 根据一级商品的id 查找出后30对应的品牌
	 * @param sortId
	 * @return
	 */
	public abstract List<Brand> getMoreBrandList(Long sortId);
	
	/**
	 * 查出全部品牌
	 * @return
	 */
	public abstract List<Brand> getBrandList(Long prodTypeId);


	/** 获取正常品牌列表 **/
	public abstract List<Brand> queryAllBrand();

	/** 根据名字品牌列表 **/
	public abstract List<Brand> getAvailableBrands();

	public abstract List<Brand> getBrandsByName(String name);

	public abstract List<Brand> getBrandByIds(List<Long> brandIds);

	public abstract PageSupport<Brand> queryBrands(String curPageNO, Integer ststus);

	public abstract PageSupport<Brand> getBrandsPage(String curPageNO, String brandName);

	public abstract PageSupport<Brand> getBrand(String curPageNO, String brandName, Long proTypeId);

	public abstract PageSupport<Brand> getBrandsPage();
	
	public abstract PageSupport<Brand> brand(String curPageNO);

	public abstract PageSupport<Brand> getBrand(String userName, String brandName);

	public abstract PageSupport<Brand> getDataByPage(String curPageNO, String userName);

	public abstract PageSupport<Brand> getDataByPageByUserId(String curPageNO, String userId);
	
	public abstract PageSupport<Brand> getDataByBrandName(String brandName,String userName);

	public abstract PageSupport<Brand> queryBrandListPage(String curPageNO, Brand brand, DataSortResult result);

	public abstract PageSupport<Brand> getBrandsByNamePage(String brandName);

	public abstract boolean checkBrandByName(String brandName, Long brandId);

	public abstract int batchDel(String ids);

	public abstract int batchOffOrOnLine(String ids,int brandStatus);

	public abstract List<BrandDto> likeBrandName(String brandName, Long categoryId);

	//用于select2的简单查询
	public abstract List<Select2Dto> getBrandSelect2(String brandName, Integer currPage, Integer pageSize);

	public abstract int getBrandSelect2Count(String brandName);

	public abstract boolean checkBrandByNameByShopId(String brandName, Long brandId, Long shopId);

	/**
	 * 获取导出品牌列表
	 * @param brand
	 * @return
	 */
	public abstract List<Brand> getExportBrands(Brand brand);  


}