/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Map;

import com.legendshop.model.form.SysSubReturnForm;


/**
 * 支付退款处理器
 */
public interface PaymentRefundProcessor {

	/**
	 * return {status:ok,result:'处理成功'}
	 * @param returnForm 订单退款参数
	 * @return
	 */
	public Map<String,Object> refund(SysSubReturnForm returnForm);

}
