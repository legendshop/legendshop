/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.promotor.SubLevelUserDto;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.UserCommis;

import java.util.Date;
import java.util.Map;

/**
 * 用户佣金Service
 */
public interface UserCommisService {

	/** 加载下级用户列表 */
	public UserCommis getUserCommis(String userId);
	
	/** 
	 * 根据用户ID查询用户分佣信息
	 * @param userId
	 * @return
	 */
	public UserCommis getUserCommisByUserId(String userId);

	/** 删除用户佣金 */
	public void deleteUserCommis(UserCommis userCommis);

	/** 保存用户佣金 */
	public String saveUserCommis(UserCommis userCommis);

	/** 新增用户佣金 */
	public void addSaveUserCommis(UserCommis userCommisPo);
	
	/** 更新用户佣金 */
	public void updateUserCommis(UserCommis userCommis);

	/** 查询各级用户数量 */
	public SubUserCountDto getSubUserCount(String userName);

	/** 处理用户佣金提现 */
	public Map<String, Object> processWithDraw(String userName, Double money);

	/** 查询本月发展会员数 */
	public Long getMonthUser(Date startTime, Date endTime, String userName);

	/** 根据用户名获取下级用户 */
	public UserCommis getUserCommisByName(String userName);

	/** 加载下级用户列表 */
	public PageSupport<UserCommis> getUserCommis(String curPageNO, String userName, UserCommis userCommis, int pageSize);

	/** 获取推广员列表数据 */
	public PageSupport<UserCommis> getUserCommis(String curPageNO, UserCommis userCommis);

	/** 加载下级用户列表 */
	public PageSupport<SubLevelUserDto> getUserCommis(String curPageNO, String level, String userName, String nickName);

	public Integer batchCloseCommis(String userIdStr);

	/**
	 * 提交申请
	 */
	public String submitPromoter(UserDetail userDetail, String userId, String userName);

	/**
	 * 手机端提交申请
	 */
	public String mobileSubmitPromoter(UserDetail userDetail, String userId, String userName);
}
