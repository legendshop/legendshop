/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.constant.SmsTemplateTypeEnum;
import com.legendshop.model.entity.SMSLog;

/**
 * 短信记录服务
 */
public interface SMSLogService  {

    public SMSLog getSmsLog(Long id);
    
    public void deleteSMSLog(SMSLog sMSLog);
    
    public Long saveSMSLog(SMSLog sMSLog);

    public void updateSMSLog(SMSLog sMSLog);

    /** 根据手机号码   **/
	public SMSLog getValidSMSCode(String mobile, SMSTypeEnum type);
	
	public boolean allowToSend(String mobile);
	
//	/** 判断验证码是否正确,验证后 验证码不会失效 **/
//	public abstract boolean verifyCode(String mobile,String verifyCode);
//	
//	/** 判断验证码是否正确,验证后 验证码不会失效 **/
//	boolean verifyCode(String mobile, String verifyCode, SMSTypeEnum typeEnum);

	/** 判断验证码是否正确 ，验证成功后使验证码失效 **/
	boolean verifyCodeAndClear(String mobile, String verifyCode);

	/** 判断验证码是否正确 ，验证成功后使验证码失效 **/
	boolean verifyCodeAndClear(String mobile, String verifyCode, SMSTypeEnum typeEnum);

    /** 判断验证码是否正确 ，验证成功后使验证码失效 **/
    boolean verifyCodeAndClear(String mobile, String verifyCode, SmsTemplateTypeEnum typeEnum);

	
	/** 提交表单前判断验证码是否正确 ，验证成功后不使验证码失效 **/
	boolean verifyCodeBeforeSubmit(String mobile, String verifyCode, SMSTypeEnum typeEnum);

	public PageSupport<SMSLog> getSMSLogPage(String curPageNO, SMSLog sMSLog);

	public int getTodayLimit(String mobile);

	public boolean allowToSend(String mobile, Date oneHourAgo, Integer maxSendNum);

	public int getUsefulMin();

	/**
	 * 判断手机验证码是否正确且有效
	 * @param validateCode
	 * @param mobile
	 * @return
	 */
	public boolean verifyMobileCode(String validateCode, String mobile);
}
