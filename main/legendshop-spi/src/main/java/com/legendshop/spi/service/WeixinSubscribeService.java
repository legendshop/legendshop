/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinSubscribe;

/**
 * 微信订阅的消息
 */
public interface WeixinSubscribeService  {

    public WeixinSubscribe getWeixinSubscribe(Long id);
    
    public void deleteWeixinSubscribe(WeixinSubscribe weixinSubscribe);
    
    public Long saveWeixinSubscribe(WeixinSubscribe weixinSubscribe);

    public void updateWeixinSubscribe(WeixinSubscribe weixinSubscribe);

	public PageSupport<WeixinSubscribe> getWeixinSubscribePage(String curPageNO);
}
