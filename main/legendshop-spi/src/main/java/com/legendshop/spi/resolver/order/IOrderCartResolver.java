package com.legendshop.spi.resolver.order;

import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.buy.UserShopCartList;

/**
 * 订单处理器，分为普通订单，团购订单，秒杀订单等
 * @author newway
 *
 */
public interface IOrderCartResolver {

	 /**
     * 获取订单详情
     */
	UserShopCartList queryOrders(Map<String, Object> params);

	/**
	 * 下单
	 * @param userShopCartList
	 * @return
	 */
	public List<String> addOrder(UserShopCartList userShopCartList);
	
}
