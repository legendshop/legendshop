package com.legendshop.spi.util;

import java.util.List;

import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.entity.Category;

/**
 * 商品分类的内存树
 *
 */
public interface CategoryManagerUtil {

	/**
	 * 找到一颗树中某个节点.
	 *
	 * @param tree the tree
	 * @param id the id
	 * @return the tree node by id
	 */
	TreeNode getTreeNodeById(TreeNode tree, long id);

	/**
	 * 根据节点ID找到TreeNote.
	 *
	 * @param id the id
	 * @return the tree node by id
	 */
	TreeNode getTreeNodeById(long id);

	/**
	 * 根据节点ID找到TreeNote.
	 *
	 * @param id the id
	 * @return the tree node by id
	 */
	TreeNode getTreeNodeByIdForAll(long id);

	/**
	 * 刷新TreeNode节点树,不会刷新时间戳,如果缓存中不存在分类树数据,则会刷新时间戳
	 */
	void refreshTree();
	
	/**
	 * 生成菜单树
	 */
	void generateTree();

	/**
	 * add a tree node to the tempNodeList
	 * 添加一个树节点的tempNodeList.
	 *
	 * @param treeNode the tree node
	 */
	void addTreeNode(TreeNode treeNode);

	void addTreeNodeForAll(TreeNode treeNode);

	/**
	 *  删除节点和它下面的晚辈.
	 *
	 * @param treeNode the tree node
	 */
	void delTreeNode(TreeNode treeNode);

	void delTreeNodeForAll(TreeNode treeNode);

	/**
	 * 删除当前节点的某个子节点.
	 *
	 * @param treeNode the tree node
	 * @param childId the child id
	 */
	void delTreeNode(TreeNode treeNode, long childId);

	/**
	 * 更新树节点.
	 *
	 * @param sourceTreeNode 源对象
	 * @param category the category
	 */
	void updateTreeNode(TreeNode sourceTreeNode, Category category);

	void updateTreeNodeForAll(TreeNode sourceTreeNode, Category category);

	/**
	 * 根据父节点找到子节点
	 */
	public List<Category> getAllChildrenByFunction(Long parentId,Integer navigationMenu,String type,Integer status);
	
	/**
	 * 获取分类关联的TypeId
	 */
	public abstract Integer getRelationTypeId(Long categoryID);
}