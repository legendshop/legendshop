/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;

/**
 * 竞拍服务接口.
 */
public interface BiddersService  {

    public Bidders getBidders(Long id);
    
    public void deleteBidders(Bidders bidders);
    
    public Long saveBidders(Bidders bidders);

    public void updateBidders(Bidders bidders);

	List<Bidders>  getBiddersByAuctionId(Long auctionsId,int offset, int limit) ;
	
	public List<Bidders> getBidders(Long aId,Long prodId,Long skuId);

	public long findBidsCount(Long paimaiId);

	
	/**
	 * 中标
	 * @param bid
	 * @return
	 */
	public Long bidAuctions(Bidders bid);
	
	/**
	 * 用于标识正在执行中标处理
	 * @param status
	 * @return 
	 */
	public int disposeBids(Long paimaiId,int status);
	
	/**
	 * 获取正在执行中标处理标识
	 * @param paimaiId
	 * @param status
	 */
	public int getDisposeBids(Long paimaiId);

	/**
	 * 清除拍卖的正在执行中标处理标识
	 * @param paimaiId
	 */
	void clearDisposeBids(Long paimaiId);
	
	/**
	 * 
	 */
	public Integer crowdWatchAuctions(Long paimaiId,int count);
	
	public Integer getCrowdWatchAuctions(Long paimaiId);
	
	void clearCrowdWatchAuctions(Long paimaiId);

	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long id);

	public PageSupport<Bidders> queryBiddListPage(String curPageNO, Long id);

	public PageSupport<Bidders> queryBiddersListPage(String curPageNO, Long aId, Long prodId, Long skuId);

	public PageSupport<Bidders> getBiddersPage(String curPageNO, Bidders bidders);

	/**
	 * 出价，并更新出价记录
	 * @param auction 
	 */
	public String bidAuctions(Auctions auction, Bidders bid, Long id, Long prodId, Long skuId);
	
}
