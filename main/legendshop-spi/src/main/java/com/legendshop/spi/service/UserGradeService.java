/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.UserGrade;

/**
 * 用户等级服务.
 */
public interface UserGradeService  {

    public UserGrade getUserGrade(Integer id);
    
    public void deleteUserGrade(UserGrade userGrade);
    
    public Integer saveUserGrade(UserGrade userGrade);

    public void updateUserGrade(UserGrade userGrade);

	public List<StatusKeyValueEntity> retrieveUserGrade();

	public List<UserGrade> queryUserGrades();

	public PageSupport<UserGrade> getUserGradePage(String curPageNO, UserGrade userGrade);
	
}
