/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.FavoriteShop;

/**
 * 店铺收藏服务.
 */
public interface FavoriteShopService  {

    public FavoriteShop getfavoriteShop(Long id);
    
    public void deletefavoriteShop(FavoriteShop favoriteShop);
    
    public Long savefavoriteShop(FavoriteShop favoriteShop);

    public void updatefavoriteShop(FavoriteShop favoriteShop);

	public boolean deletefavoriteShop(Long id, String userId);
	
	public boolean isExistsFavoriteShop(String userId,Long shopId);
	
	public Long getShopId(String shopName);
	
	public void deletefavoriteShop(Long id);

	public void deletefavoriteShop(String userId, String ids);

	public void deleteAllfavoriteShop(String userId);

	public boolean deletefavoriteShopByShopIdAndUserId(Long shopId, String userId);
	
	public Long getfavoriteShopLength(String userId);

	PageSupport<FavoriteShop> collectShop(String curPageNO, String userId);

	public PageSupport<FavoriteShop> query(String curPageNO, String userId);
}
