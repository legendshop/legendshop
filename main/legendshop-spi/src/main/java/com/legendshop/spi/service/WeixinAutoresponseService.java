/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;

/**
 * 微信响应服务
 */
public interface WeixinAutoresponseService  {

    public WeixinAutoresponse getWeixinAutoresponse(Long id);
    
    public void deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
    
    public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);

    public void updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);

	public PageSupport<WeixinAutoresponse> getWeixinnAutoresponse(String curPageNO);
}
