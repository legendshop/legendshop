package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.Sub;

/**
 * 订单定时服务
 */
public interface OrderScheduledService {
	
	
	/**
	 * 结束超时不付费的订单.
	 */
	public void finishUnPay(List<Sub> list);

	/**
	 * 结束超时不确认收货的订单.
	 */
	public void finishUnAcklodge(List<Sub> list);


	/**
	 * 给买家发送即将自动确认收货的短信、邮件提醒
	 */
	public void finishUnAcklodgeNotice();

}
