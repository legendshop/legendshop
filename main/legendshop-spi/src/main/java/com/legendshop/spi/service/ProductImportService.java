/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.CateGoryExportDTO;
import com.legendshop.model.dto.ProdExportDTO;
import com.legendshop.model.dto.order.TenderProdExcelDTO;
import com.legendshop.model.entity.ProductImport;

/**
 * 商品导入Service.
 */
public interface ProductImportService  {

    public ProductImport getProductImport(Long id);
    
    public void deleteProductImport(ProductImport productImport);
    
    public Long saveProductImport(ProductImport productImport);

    public void updateProductImport(ProductImport productImport);

    /**
     * 获取平台类目导出list(只获取有三级目录的分类)
     * @return
     */
    List<CateGoryExportDTO> getCateGoryExportDTOList();

    /**
     * 获取店铺类目导出list
     * @return
     * @param shopId
     */
    List<CateGoryExportDTO> getShopCateGoryExportDTOList(Long shopId);

    /** 保存导入的本地商品
     * @return*/
    void saveImportProduct(String userId, String userName, Long shopId, ProdExportDTO prodExportDTO);

    /**
     * 保存
     */
    public void saveProductImport(List<ProductImport> importList);
    
    
    /**
     * 删除
     */
    public void deleteProductImport(List<Long> ids);

	public void deleteProductImportById(Long impId);

	public PageSupport<ProductImport> getProductImportPage(String curPageNO, ProductImport productImport, Long shopId);

	/**
            *保存导入的中标商品,返回错误数据
	 */
    List<TenderProdExcelDTO> saveImportTenderProduct(String userId, String userName, Long shopId, List<TenderProdExcelDTO> tenderProdExcelDTOList);

}
