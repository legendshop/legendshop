/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.math.BigDecimal;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.dto.SubRefundRetuenDto;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;

/**
 *退款服务
 */
public interface SubRefundReturnService  {

    public List<SubRefundReturn> getSubRefundsSuccessBySettleSn(String settleSn);

	public SubRefundReturn getSubRefundReturn(Long id);

	void receiveGoods(Integer goodsState, SubRefundReturn subRefundReturn);

    public void deleteSubRefundReturn(SubRefundReturn subRefundReturn);
    
    public Long saveSubRefundReturn(SubRefundReturn subRefundReturn);

    public void updateSubRefundReturn(SubRefundReturn subRefundReturn);

    /**
     * 保存 订单"仅退款"申请
     * @param subRefundReturn
     */
    public void saveApplyOrderRefund(SubRefundReturn subRefundReturn);
    
    /**
     * 保存 订单项 "仅退款"申请
     * @param subRefundReturn
     */
    public void saveApplyItemRefund(SubRefundReturn subRefundReturn);
    
    /**
     * 保存 订单项 "退款且退货"申请
     * @param subRefundReturn
     */
    public void saveApplyItemReturn(SubRefundReturn subRefundReturn);

	/**
	 * 获取申请退款及退货的Dto,用于申请页面订单相关信息的回显.
	 * @param opType 操作类型: 1:订单退款, 2:订单项退款/退货
	 * @return
	 */
    public ApplyRefundReturnDto getApplyRefundReturnDto(Long id, RefundReturnTypeEnum opType,String userId);

	/**
	 * 平台审核退款(订单退款,订单项退款)
	 * @param subRefundReturn
	 * @param adminMessage 管理员备注
	 */
	public void adminAuditRefund(SubRefundReturn subRefundReturn, String adminMessage);

	/**
	 * 平台审核退货
	 * @param subRefundReturn
	 * @param adminMessage 管理员备注
	 */
	public void adminAuditReturn(SubRefundReturn subRefundReturn, String adminMessage);

	/**
	 * 重新绑定 申请编号(商户退款单号)
	 * @param returnPayId
	 * @param batch_no
	 */
	public void updateRefundSn(Long returnPayId, String batch_no);

	/**
	 * 退款处理成功
	 * @param refundSn  申请编号(商户退款单号)
	 * @param outRefundNo 第三方退款单号(微信退款单号)
	 * @param handleType 退款方式
	 * 
	 */
	public int orderReturnSucess(String refundSn, String outRefundNo, String handleType);

	/**
	 * 退款处理失败
	 * @param refundSn  申请编号(商户退款单号)
	 */
	public int orderReturnFail(String refundSn);

	/**
	 * shan
	 * @param subRefundReturn
	 */
	public void deleteRefundReturn(SubRefundReturn subRefundReturn);

	/**
	 * @param subNumber 订单号
	 * @return
	 */
	public SubRefundReturn getSubRefundReturnBySubNumber(String subNumber);
	
	public SubRefundReturn getSubRefundReturnBySubItemId(String subItemId);

	public String updateRefundSn(SubRefundReturn refundReturn);

	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, String status,
			RefundSearchParamDto paramData);

	public PageSupport<SubRefundReturn> getSubRefundReturn(String curPageNO, String status,
			RefundSearchParamDto paramData);

	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, ShopOrderBill shopOrderBill,
			String subNumber);

	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, Long shopId, String subNumber,
			ShopOrderBill shopOrderBill);

	public PageSupport<SubRefundReturn> getSubRefundReturnPage(String curPageNO, RefundSearchParamDto paramData,
			Long shopId);

	public PageSupport<SubRefundReturn> getSubReturnListPage(String curPageNO, RefundSearchParamDto paramData,
			Long shopId);

	public PageSupport<SubRefundReturn> getRefundListPage(String curPageNO, RefundSearchParamDto paramData,
			String userId);

	public PageSupport<SubRefundReturn> getSubRefundReturnMobile(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	public PageSupport<SubRefundReturn> getSubRefundReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	public PageSupport<SubRefundReturn> getReturnByCenter(String curPageNO, String userId,
			RefundSearchParamDto paramData);

	/**
	 * 根据申请编号查找退款单
	 * @param refundSn
	 * @return
	 */
	public SubRefundReturn getSubRefundReturnByRefundSn(String refundSn);

	/**
	 * 用户撤销退款申请
	 * @param subRefundReturn
	 */
	public void undoApplySubRefundReturn(SubRefundReturn subRefundReturn);

	public List<SubRefundRetuenDto> exportOrder(ShopOrderBill shopOrderBill, String subNumber);

	
	/**
	 * 商家审核订单或订单项的退款
	 * 
	 * @param subRefundReturn 退款,退货对象
	 * @param isAgree 是否同意
	 * @param sellerMessage 审核意见
	 * @param falg 标记定时任务关闭 true为商家手动操作关闭，还是定时器轮询关闭
	 */
	public void shopAuditRefund(SubRefundReturn subRefundReturn, boolean isAgree, String sellerMessage, boolean falg);

	
	/**
	 * 商家审核订单项的退货
	 * 
	 * @param subRefundReturn 退款,退货对象
	 * @param isAgree 是否同意
	 * @param isJettison 是否弃货
	 * @param sellerMessage 审核意见
	 * @param falg 标记定时任务关闭 true为商家手动操作关闭，还是定时器轮询关闭
	 */
	public void shopAuditReturn(SubRefundReturn subRefundReturn, Boolean isAgree, Boolean isJettison, String sellerMessage, boolean falg);

	/**
	 * 拒绝收货或未收到货物
	 * 
	 * @param subRefundReturn
	 * @param falg
	 *            标记定时任务关闭 true为商家手动操作关闭，还是定时器轮询关闭
	 */
	public void closeSubRefundReturnByReturn(SubRefundReturn subRefundReturn, boolean falg);

	/**
	 * 商家确认收货
	 * @param subRefundReturn
	 */
	public void confirmSubRefundReturn(SubRefundReturn subRefundReturn);

	/**
	 * 用户退货给商家
	 * @param subRefundReturn
	 */
	public void shipSubRefundReturn(SubRefundReturn subRefundReturn);

	/**
	 * 更新平台退款
	 * @param refundId
	 * @param refundAmount
	 * @param is_handle_success
	 * @param refundType
	 * @return
	 */
	public int updateSubPlatRefund(Long refundId,BigDecimal refundAmount,Integer is_handle_success,String refundType);
	
	/**
	 * 更新第三方退款
	 * @param returnPayId
	 * @param refundAmount
	 * @return
	 */
	public int updateSubThirdRefundAmount(Long returnPayId, BigDecimal refundAmount);
	
	/**
	 * 退款更新
	 * @param refundId  退款ID
	 * @param alpRefundAmount 第三方退款金额
	 * @param platformAmount  平台退款金额
	 * @param isDepositHandleSucces 订金处理状态
	 * @param isHandleSuccess  退款单处理状态
	 * @param refundType 退款单处理方式
	 * @param depositHandleType 订金退款处理方式
	 * @param outRefundNo 第三方退款单号
	 * @param depositOutRefundNo 订金第三方退款单号
	 * @return
	 */
	public int updateNewSubPlatRefund(Long refundId,BigDecimal alpRefundAmount, BigDecimal platformAmount, 
			Integer isDepositHandleSucces, Integer isHandleSuccess,String refundType, String depositHandleType, 
			String outRefundNo, String depositOutRefundNo);

	PageSupport<SubRefundReturn> refundList(Long shopId, Long sellerState, Integer refundReturnType, String curPageNO, int pageSize);

	PageSupport loadShopOrderBill(Long shopId, Long type, String sn, String curPageNO, int pageSize);

	PageSupport<SubRefundReturn> refundList(String userId, Integer refundReturnType, String curPageNO, int pageSize);

	/**
	 * 获取用户退款退货记录列表
	 * @param userId 用户id
	 * @param curPageNO 当前页码
	 * @param status 处理状态
	 * @return
	 */
	public PageSupport<SubRefundReturn> querySubRefundReturnList(String userId, String curPageNO, Integer status);

	/**
	 * 再次提交退款退货申请
	 * @param refund
	 */
	public void submitAgainApplySubRefundReturn(SubRefundReturn refund);

	/**
	 * 获取未完成的订单
	 * @param subId
	 * @param subItemId
	 * @param userId
	 * @return
	 */
	SubRefundReturn getUnfinishedSubRefundReturn(Long subId, Long subItemId, String userId);

	/**
	 * 退款退货处理方法，用来处理同订单
	 * @param subId
	 * @param subItemId
	 * @param userId
	 */
	void returnProcess(Long subId, Long subItemId, String userId);
}
