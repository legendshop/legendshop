/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DiscoverProd;

/**
 * The Class DiscoverProdService.
 * 发现文章商品关联表服务接口
 */
public interface DiscoverProdService  {

   	/**
	 *  根据Id获取发现文章商品关联表
	 */
    public DiscoverProd getDiscoverProd(Long id);
    
    /**
	 *  根据Id获取发现文章商品关联表
	 */
    public List<DiscoverProd> getByDisId(Long id);

    /**
	 *  根据Id删除发现文章商品关联表
	 */
    public int deleteDiscoverProd(Long id);
    
    /**
	 *  根据对象删除发现文章商品关联表
	 */
    public int deleteDiscoverProd(DiscoverProd discoverProd);
    
   /**
	 *  保存发现文章商品关联表
	 */	    
    public Long saveDiscoverProd(DiscoverProd discoverProd);
    
    /**
	 *  通过发现文章id删除
	 */	    
    public int deleteByDisId(Long disId);

   /**
	 *  更新发现文章商品关联表
	 */	
    public void updateDiscoverProd(DiscoverProd discoverProd);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<DiscoverProd> queryDiscoverProd(String curPageNO,Integer pageSize);
    
}
