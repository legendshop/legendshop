/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.FloorItem;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.SubFloorItem;
import com.legendshop.model.floor.NewsFloor;
import com.legendshop.model.floor.ProductFloor;
import com.legendshop.model.floor.SortFloor;

/**
 * 子楼层服务.
 */
public interface FloorItemService  {

    public FloorItem getFloorItem(Long id);
    
    public Long saveFloorItem(FloorItem floorItem);

    public void updateFloorItem(FloorItem floorItem);

    public List<FloorItem> getAllSortFloorItem(Long flId);
    
	public void saveFloorItem(List<SortFloor> sortFloorList, Long flId,String layout);
	
	public List<FloorItem> getBrandList(Long id,String layout,int limit);
	
	public void saveBrands(List<ProductFloor> brandFloorList, Long flId,String layout);
	
	public List<FloorItem> getRankList(Long id); 
	
	public void saveRanks(List<ProductFloor> rankFloorList, Long flId);
	
	public void saveGroups(List<ProductFloor> groupFloorList, Long flId);
	
	public FloorItem getFloorGroup(Long id); 
	
	public void saveNews(List<NewsFloor> newsFloorList, Long sfId);

	public List<FloorItem> getNewsList(Long flId);

	public FloorItem getAdvFloorItem(Long flId,Long fiId,String type);
	
	public List<FloorItem> queryAdvFloorItem(Long flId,String type,String layout);
	
	public void deleteFloorItem(Long id);
	
	public FloorItem getAdvFloorItem(Long flId,String type,String layout);

	public String deleteAdvtItem(Long fiId,Long advId);

	public List<SubFloorItem> findSubFloorItem(Long flId, String layout, int limit);

	public PageSupport<Brand> getBrandListPage(String curPageNO, String brandName);

	public PageSupport<Product> getGroupListPage(String curPageNO, String groupName);

	public PageSupport<FloorItem> queryAdvFloorItemPage(String curPageNO, Long flId, String layout, String title);

	public PageSupport<News> getNewsListPage(String curPageNO, Long newsCategoryId, String newsTitle,String userName);
}
