package com.legendshop.spi.service;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdCashLogDto;

/**
 * app 预存款明细记录service
 * @author linzh
 */
public interface AppPdCashLogService {


	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	public abstract AppPageSupport<AppPdCashLogDto> getPdCashLogPageByType(String userId, Integer state,
			String curPageNO);

	/**
	 * 获取用户预存款明细期详情
	 * @param id
	 * @return
	 */
	public abstract AppPdCashLogDto getPdCashLog(Long id);

}
