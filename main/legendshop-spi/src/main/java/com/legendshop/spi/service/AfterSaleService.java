package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AfterSale;

public interface AfterSaleService {

    public AfterSale getAfterSale(Long id);
    
    public void deleteAfterSale(AfterSale afterSale);
    
    public Long saveAfterSale(AfterSale afterSale);

    public void updateAfterSale(AfterSale afterSale);

    public void deleteByAfterSaleId(Long id);

	public PageSupport<AfterSale> getAfterSale(String curPageNO, String userName, String userId);

	public PageSupport<AfterSale> getAfterSalePage(String curPageNO, String userName, String userId);
}
