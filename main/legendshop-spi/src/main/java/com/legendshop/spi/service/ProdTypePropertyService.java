/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProdTypeProperty;

/**
 *
 * 商品类型跟属性的关系服务
 *
 */
public interface ProdTypePropertyService  {

	void updateProdTypeProperty(ProdTypeProperty prodTypeProperty);
	
	ProdTypeProperty getProdTypeProperty(Long id);

	ProdTypeProperty getProdTypeProperty(Long propId, Long typeId);

}
