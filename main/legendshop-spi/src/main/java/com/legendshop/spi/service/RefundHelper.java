package com.legendshop.spi.service;

import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;

/**
 * 退款工具类接口.
 */
public interface RefundHelper {
	
	/**
	 * 检查退换单是否正常.
	 *
	 * @param refundReturn the refund return
	 * @return the string
	 */
	public abstract String checkSubRefundReturn(SubRefundReturn refundReturn);

	/**
	 * 查看订单目前允许支付宝或者微信的退款金额(这个主要是针对混合支付的订单，某些订单项已经退过微信了，微信只能退它当时支付的最大值).
	 *
	 * @param subSettlement 支付结算单据
	 * @return 退款金额
	 */
	public abstract Double allowReturnAmount(SubSettlement subSettlement);
	
	
	/**
	 * 金币退款
	 * @param refundReturn 退款表
	 * @return
	 */
	public abstract boolean plateformCoinReturn(SubRefundReturn refundReturn);

	/**
	 * 预存款退款
	 * @param refundReturn
	 * @return
	 */
	public abstract boolean plateformPrePayReturn(SubRefundReturn refundReturn);

	/**
	 * 
	 * @param refundId 退款单据ID
	 * @param outRefundNo 退款流水号
	 * @param alpRefundAmount 第三方退款金额
	 * @param prePayType  平台退款的方式[1:预付款 2:金币]
	 * @return
	 */
	public abstract void orderReturnOnlineSettle(Long refundId, String outRefundNo, Double alpRefundAmount,Integer prePayType);

	/**
	 * 预存款退款
	 */
	public boolean plateformPrePayReturn2(String userId, String userName, double amount, String sn, String message);
	

}
