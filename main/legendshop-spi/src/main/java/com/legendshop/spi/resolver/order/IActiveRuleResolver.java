package com.legendshop.spi.resolver.order;

import com.legendshop.model.dto.marketing.MarketRuleContext;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.marketing.RuleResolver;


public interface IActiveRuleResolver {

	 /**
     * 执行规则
     * @param marketingDto
     */
    void execute(MarketingDto marketingDto,RuleResolver item, MarketRuleContext context);
    
    
}
