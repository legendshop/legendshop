/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.model.entity.ActiveBanner;

/**
 * The Class GroupBannerService.
 * 服务接口
 */
public interface ActiveBannerService  {

	/**
	 *  根据Id获取
	 */
	public ActiveBanner getBanner(Long id);

	/**
	 *  删除
	 */    
	public void deleteBanner(ActiveBanner activeBanner);

	/**
	 *  保存
	 * @param file 
	 * @param userName 
	 */	    
	public Long saveBanner(ActiveBanner activeBanner, String userName, String userId, Long shopId, MultipartFile file);

	/**
	 *  更新
	 */	
	public void updateBanner(ActiveBanner activeBanner);

	//获得所有轮播图
	public List<ActiveBanner> getBannerList(String type);

}
