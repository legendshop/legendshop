/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.UserFeedBack;

/**
 * 用户反馈服务.
 */
public interface UserFeedBackService  {

    public UserFeedBack getUserFeedBack(Long id);
    
    public void deleteUserFeedBack(UserFeedBack userFeedBack);
    
    public Long saveUserFeedBack(UserFeedBack userFeedBack);

    public void updateUserFeedBack(UserFeedBack userFeedBack);

	public PageSupport<UserFeedBack> getUserFeedBackPage(String curPageNO, UserFeedBack userFeedback);
}
