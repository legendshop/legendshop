/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.AdminMessageDtoList;
import com.legendshop.model.entity.Msg;
import com.legendshop.model.entity.MsgText;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;

/**
 * 站内信服务.
 */
public interface MessageService {
	
	/**
	 * 删除 发件箱  站内信 
	 * @param msgId
	 */
	public void deleteOutbox(Long msgId);
	
	/**
	 * 删除 收件箱  站内信(单条删除)
	 */
	public void deleteInbox(Long msgId,String userName);
	
	/**
	 * 删除站内信(批量删除)
	 * @return TODO
	 */
	public int deleteInboxByMsgIds(String[] msgIds,String userName);
	
	/**
	 * 保存站内信
	 */
	public void saveSiteInformation(MsgText msgText,String userName,String receiveNames);
	
	/**
	 * 查找出邮件的内容
	 * @param msgId
	 * @return
	 */
	public SiteInformation getMsgText(String userName, Long msgId, boolean updateStatus);
	
	
	/**
	 * 通过id获取系统通知
	 * @param msgId
	 * @return
	 */
	public SiteInformation getSystemMessages(Long msgId);
	
	/**
	 * 保存系统通知
	 * @param siteInformation
	 * @param userGradeArray
	 */
	public void  saveSystemMessages(SiteInformation siteInformation,Integer[] userGradeArray);
	
	/**
	 * 通过用户名查找出用户等级
	 * @param userName
	 * @return
	 */
	public Integer getGradeId(String userName);
	
	/**
	 * 将ls_msg_status中的状态更新为1
	 * @param userName
	 * @param msgId
	 */
	public void updateSystemMsgStatus(String userName,Long msgId, int status);
	
	/**
	 * 将系统通知 lm_msg 中的status 改为-1
	 * @param msgId
	 */
	public void deleteSystemMsgStatus(Long msgId);
	
	/**
	 * 验证接受人是否存在
	 * @return
	 */
	public boolean isReceiverExist(String receiveName);

	/**
	 * 获取用户未读 消息条数
	 * @param userName
	 * @return
	 */
	public Integer calUnreadMsgCount(String userName);
	
	//获取用户未读 消息条数
	public Integer calUnreadMsgCount(String userName, Integer gradeId, Date userRegtime);

	public void clearInboxMessage(String userName);

	public Long getCountById(String userName); 
	
	public int getCalUnreadSystemMessagesCount(String userName, Integer gradeId, Date userRegtime);

	public Msg getMessage(Long msgId);

	/**
	 * 设置该消息已读
	 * @param userName
	 * @param msgId
	 */
	void updateMsgRead(String userName, Long msgId);

	PageSupport<SiteInformation> inboxSiteInformation(String userName, String curPageNO);

	PageSupport<SiteInformation> outboxSiteInformation(String userName, String curPageNO);

	PageSupport<SiteInformation> systemInformation(String userName, Integer gradeId, String curPageNO, UserDetail userDetail);

	public PageSupport<SiteInformation> query(String curPageNO, String userName);
	
	public PageSupport<SiteInformation> querySiteInformation(String curPageNO, String title, Integer pageSize);

	public PageSupport<SiteInformation> queryMessage(String curPageNO, String userName);

	public PageSupport<SiteInformation> query(String curPageNO, Integer gradeId, String userName, UserDetail userDetail);

	public AdminMessageDtoList getMessage();

	public int batchDelete(String msgIds);
	
	/**
	 * 查询待审核商家
	 * @return
	 */
	public Integer getAuditShop();

	/**
	 * 查询待回复的咨询
	 * @return
	 */
	public Integer getProductConsultCount();

	/**
	 * 查询待审核商品
	 * @return
	 */
	public Integer getAuditProductCount();

	/**
	 * 查询待审核品牌
	 * @return
	 */
	public Integer getAuditBrandCount();

	/**
	 * 查询退货申请
	 * @return
	 */
	public Integer getReturnGoodsCount();

	/**
	 * 查询退款申请
	 * @return
	 */
	public Integer getReturnMoneyCount();

	/**
	 * 查询 待处理的举报
	 * @return
	 */
	public Integer getAccusationCount();

	/**
	 * 查询待确认的结算单
	 * @return
	 */
	public Integer getShopBillCount();

	/**
	 * 查询待审核的结算单
	 * @return
	 */
	public Integer getShopBillConfirmCount();

	/**
	 * 查询待审核的推广员
	 * @return
	 */
	public Integer getUserCommisCount();

	/**
	 * 待处理的反馈意见
	 * @return
	 */
	public Integer getUserFeedBackCount();

	/**
	 * 待处理拍卖活动管理
	 * @return
	 */
	public Integer getAuctionCount();

	/**
	 * 秒杀活动管理
	 * @return
	 */
	public Integer getSeckillActivityCount();

	/**
	 * 待审核的团购活动
	 * @return
	 */
	public Integer getGroupCount();

	/**
	 * 待预售活动管理
	 * @return
	 */
	public Integer getPresellProdCount();

	/**
	 * 用户评论
	 * @return
	 */
	public Integer getProductcommentCount();
	
	/**
	 * 获取收件箱未读消息数
	 */
	public Integer getUnReadMsgNum(String userName);

	/**
	 * 获取商家的站内信
	 */
	PageSupport<SiteInformation> queryShopMessage(String curPageNO, String userName);
}
