package com.legendshop.spi.service;

import com.legendshop.model.dto.compare.ProductCompareDto;

/**
 * 商品对比服务
 *
 */
public interface ProductCompareService {

	public abstract ProductCompareDto getProductCompareDto(Long [] prodIds);
}
