package com.legendshop.spi.service;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdWithdrawCashDto;

/**
 * app 预存款提现记录service
 * @author linzh
 */
public interface AppPdWithdrawCashService {
	
	/**
	 * 获取预存款提现记录列表
	 * @param curPageNO 当前页码
	 * @param userId 用户id
	 * @param state 提现状态
	 * @return
	 */
	public abstract AppPageSupport<AppPdWithdrawCashDto> getPdWithdrawCashPage(String curPageNO, String userId, String state);

	/**
	 * 获取预存款提现记录详情
	 * @param id
	 * @param userId
	 * @return
	 */
	public abstract AppPdWithdrawCashDto getPdWithdrawCash(Long id, String userId);

}
