/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.TagItemNews;

/**
 *标签里的文章
 */
public interface TagItemNewsService  {

    public List<TagItemNews> getTagItemNewsByNewsId(Long  newsId);

    public TagItemNews getTagItemNews(Long id);
    
    public void deleteTagItemNews(TagItemNews tagItemNews);
    
    public Long saveTagItemNews(TagItemNews tagItemNews);

    public void updateTagItemNews(TagItemNews tagItemNews);

}
