package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.coin.CoinConsumeDto;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.model.vo.CoinVo;
/**
 * 金币日志服务
 *
 */
public interface CoinLogService {

	public CoinVo findUserCoin(String userId, String curPageNO);

	public PageSupport<CoinConsumeDto> getRechargeDetailsList(String curPageNO, String userName, String mobile);

	public PageSupport<CoinConsumeDto> getConsumeDetailsListPage(String curPageNO, String mobile, String userName);

	public PageSupport<CoinLog> getUserCoinPage(String curPageNO, String userId);
}
