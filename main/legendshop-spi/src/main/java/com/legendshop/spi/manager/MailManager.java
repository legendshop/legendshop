package com.legendshop.spi.manager;

import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;

/**
 * 发送邮件的管理者
 *
 */

public interface MailManager {
	/**
	 * 系统注册时发送邮件
	 */
	public void register(User user, UserDetail userDetail);
	
	/**
	 * 重置密码时发送邮件
	 */
	public void resetPassword(UserDetail userDetail, String newPassword);
	
	/**
	 * 找回密码时，发送的邮箱验证
	 */
	public void verifyEmail(User user, String mail,String code);
	
	
	/**
	 * 验证邮件，去往修改页面
	 */
	public void confirmationMail(User user, String mail,String code);
	
	/**
	 * 修改密码时发送邮件
	 */
	public void updatePassword(User user,String mail,String code);
	
	/**
	 * 修改密码时发送邮件
	 */
	public void updateMail(User user, String mail,String code);
	
	/**
	 * 订单修改时发送邮件
	 */
	public void orderChanges();

	/**
	 * 订单支付提醒时发送邮件
	 */
	public void remindOrder();

	/**
	 * 修改支付密码时发送邮件
	 */
	public void updatePaymentpassword(User user, String userEmail, String code);
	
	

	/**
	 * 用户提现申请发送校验码
	 * @param user
	 * @param code
	 */
	public void withdrawApply(UserDetail user, String userEmail, String code);
	
}
