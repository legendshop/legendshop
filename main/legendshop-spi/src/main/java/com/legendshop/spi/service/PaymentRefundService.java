package com.legendshop.spi.service;

import java.util.Map;

import com.legendshop.model.form.SysSubReturnForm;

public interface PaymentRefundService {

	/**
	 * 
	 * @param returnForm
	 * @return
	 */
	public Map<String, Object> refund(SysSubReturnForm returnForm);
	
}
