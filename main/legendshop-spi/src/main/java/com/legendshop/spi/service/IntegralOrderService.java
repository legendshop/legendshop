/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.model.entity.integral.IntegralProd;

/**
 * 会员积分兑换订单服务.
 */
public interface IntegralOrderService  {

    public IntegralOrder getIntegralOrder(Long id);
    
	/**
	 * 根据订单号和用户ID查询积分订单
	 * @param orderSn
	 * @param userId
	 * @return
	 */
	public IntegralOrder getIntegralOrder(String orderSn, String userId);
    
    public void deleteIntegralOrder(IntegralOrder integralOrder);
    
    public Long saveIntegralOrder(IntegralOrder integralOrder);

    public void updateIntegralOrder(IntegralOrder integralOrder);


    /**
     * 查询积分订单
     * @param paramDto
     * @return
     */
	public PageSupport<IntegralOrderDto> getIntegralOrderDtos(OrderSearchParamDto paramDto);

	public IntegralOrder getIntegralOrderByOrderSn(String orderSn);

	public void fahuo(String orderSn, Long deliv, String dvyFlowId);
	
	public String shouhuo(String orderSn);

	public IntegralOrderDto findIntegralOrderDetail(String ordeSn);

	
	/**
	 * 用户积分兑换商品
	 */
	public String shopBuyIntegral(String userId, UserAddress userAddress ,IntegralProd integralProd,
			Integer count,String orderDesc);

	/**
	 * 删除积分订单
	 */
	public String orderDel(String sn);
	
	/**
	 * 删除积分订单
	 */
	public String orderDel(String sn,String userId);

	
	/**
	 * 取消积分订单
	 * @param orderSn 订单编号
	 * @param updateUserId 后台更新用户ID
	 * @param logDesc 积分操作日记
	 * @return
	 */
	String orderCancel(String orderSn, String userId,String updateUserId, String logDesc);

	public List<IntegralProd> getHotIntegralProds();
	
	public Long getUserBuyShopCount(String userId,Long prodId);

	public PageSupport<Coupon> getCouponListPage(String curPageNO, String type);

}
