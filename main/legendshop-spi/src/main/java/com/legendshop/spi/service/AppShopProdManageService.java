/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.prod.AppProdListDto;
import com.legendshop.model.prod.AppProdSearchParms;


/**
 * App首页装修的商品.
 */
public interface AppShopProdManageService  {

	/**
	 * app 数据查询
	 */
	public abstract AppProdListDto getAppProdList(AppProdSearchParms searchParms,Long shopId);
	
	/**
	 * 通过roomnum查询商家的商品
	 */
	public abstract AppProdListDto getAppProdListByRoomnum(AppProdSearchParms searchParms,Integer roomnum, String curPageNO);
}
