/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassThumb;

/**
 * The Class GrassThumbService.
 * 种草文章点赞表服务接口
 */
public interface GrassThumbService  {

   	/**
	 *  根据Id获取种草文章点赞表
	 */
    public GrassThumb getGrassThumb(Long id);

    /**
	 *  根据Id删除种草文章点赞表
	 */
    public int deleteGrassThumb(Long id);
    
    /**
	 *  根据对象删除种草文章点赞表
	 */
    public int deleteGrassThumb(GrassThumb grassThumb);
    
   /**
	 *  保存种草文章点赞表
	 */	    
    public Long saveGrassThumb(GrassThumb grassThumb);

   /**
	 *  更新种草文章点赞表
	 */	
    public void updateGrassThumb(GrassThumb grassThumb);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassThumb> queryGrassThumb(String curPageNO,Integer pageSize);
    
    
    /**
  	 *  根据grassId删除种草和发现文章点赞表
  	 */
     public void deleteGrassThumbByDiscoverId(Long grassId,String uid);

	Integer rsThumb(Long grassId, String userId);
}
