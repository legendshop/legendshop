package com.legendshop.spi.service;

import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.SysPaymentForm;


/**
 * 收银台控制中心(用于记录订单清算票据服务,控制支付服务,预付款控制)
 *
 */
public interface CashiersControlCenterService {

	public abstract String CashiersControlCenter(SubSettlement selectSettlement,SysPaymentForm paymentForm);

	/**
	 * 订单通过预付款全额支付 ----普通订单
	 * @param userId 用户Id
	 * @param totalAmount 金额
	 * @param subIds 所有的订单ID 1,2 --> 合并支付
	 * @param prePayType 余额支付类型(1:预付款  2:金币)
	 */
	public abstract  void predepositPay(String userId, Double totalAmount,String subIds,String prePayType) ;
	
	
	/**
	 * 预付款全额支付 ----预售订单
	 * @param userId  用户Id
	 * @param totalAmount 金额
	 * @param numbers 所有的订单ID 1,2 --> 合并支付
	 * @param pay_pct_type
	 */
	public abstract void predepositPayPreSell(String userId, Double totalAmount,String numbers,int pay_pct_type, String prePayTypeVal);
	
	
	
}
