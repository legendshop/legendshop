/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopBillPeriod;

/**
 * 结算档期服务
 */
public interface ShopBillPeriodService  {

    public ShopBillPeriod getShopBillPeriod(Long id);
    
    public void deleteShopBillPeriod(ShopBillPeriod shopBillPeriod);
    
    public Long saveShopBillPeriod(ShopBillPeriod shopBillPeriod);
    
    public void saveShopBillPeriodWithId(ShopBillPeriod shopBillPeriod,Long id);

    public void updateShopBillPeriod(ShopBillPeriod shopBillPeriod);

	public Long getShopBillPeriodId();

	public PageSupport<ShopBillPeriod> getShopBillPeriod(String curPageNO, ShopBillPeriod shopBillPeriod);
}
