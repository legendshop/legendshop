/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;

/**
 * 布局Banner服务
 */
public interface ShopLayoutBannerService  {

    public ShopLayoutBanner getShopLayoutBanner(Long id);
    
    public void deleteShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);
    
    public Long saveShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);

    public void updateShopLayoutBanner(ShopLayoutBanner shopLayoutBanner);

	public List<ShopLayoutBanner> getShopLayoutBanner(Long shopId, Long layoutId);

	public void delete(List<ShopLayoutBanner> banners);
}
