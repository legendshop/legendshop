/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.ProdGroup;

/**
 * The Class ProdGroupService.
 * 服务接口
 */
public interface ProdGroupService  {

   	/**
	 *  根据Id获取
	 */
    public ProdGroup getProdGroup(Long id);

    /**
	 *  根据Id删除
	 */
    public int deleteProdGroup(Long id);
    
    /**
	 *  根据对象删除
	 */
    public int deleteProdGroup(ProdGroup prodGroup);
    
   /**
	 *  保存
	 */	    
    public Long saveProdGroup(ProdGroup prodGroup);

   /**
	 *  更新
	 */	
    public void updateProdGroup(ProdGroup prodGroup);
    
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<ProdGroup> queryProdGroup(String curPageNO,Integer pageSize);
    
    
    /**
	 *  自定义分组列表
	 */	
    public List<ProdGroup> getProdGroupByType(Integer type);
    
    /**
  	 *  分页查询通过名字列表
  	 */	
	public abstract PageSupport<ProdGroup> queryProdGroupListPage(String curPageNO, ProdGroup prodGroup,Integer pageSize);
    
	 /**
  	 *  检查名字是否存在
  	 */	
	public abstract boolean checkNameIsExist(String name, Long id);
    
}
