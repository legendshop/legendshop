/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * 微信用户抽奖服务.
 */
public interface DrawWXUserService {

	/**
	 * Gets the gzuser info by user id.
	 *
	 * @param userId the user id
	 * @return the gzuser info by user id
	 */
	WeixinGzuserInfo getGzuserInfoByUserId(String userId);
}
