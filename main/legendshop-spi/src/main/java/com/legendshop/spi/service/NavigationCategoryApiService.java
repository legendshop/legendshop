package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.api.NavigationCategory;

/**
 * 
 * 首页导航条服务
 *
 */
public interface NavigationCategoryApiService {

	List<NavigationCategory> findNavigationCategory();

}
