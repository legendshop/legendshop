/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Myfavorite;

/**
 * 我的商品收藏
 *
 */
public interface MyfavoriteService{

	void deleteFavs(String userId, String selectedFavs);
	
	void deleteAllFavs(String userId);
	
	void saveFavorite(Myfavorite myfavorite);
	
	void deleteFav(String userId, Long prodId);
	
	Boolean isExistsFavorite(Long prodId,String userName);
	
	Long getfavoriteLength(String userId);
	
	/**
	 * @Description: 礼券
	 * @date 2016-6-21 
	 */
	Long getCouponLength(String userName);
	
	/**
	 * @Description: 修改支付密码
	 * @date 2016-7-5 
	 */
	void updatePaymentPassword(String userName, String paymentPassword);


	/**
	 * 收集我的商品收藏, 带有收藏的商品总数
	 */
	public PageSupport<Myfavorite> collect(String curPageNO, Integer pageSize, String userId);

}
