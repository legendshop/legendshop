/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service.security;

import com.legendshop.model.dto.shop.ShopAccountUserDto;
import com.legendshop.model.entity.ShopUser;


/**
 * 商家登录验证服务Dao.
 */
public interface ShopAuthService{
	
	/**
	 *  查找用户, remember me 使用
	 */
	public ShopUser loadUserByUsername(String username);
	
	/**
	 * 凭用户名密码直接登录， 查找用户
	 */
	public ShopUser loadUserByUsername(String username, String password) ;
	
	/**
	 * 根据手机验证码登录
	 * @param sellerName
	 * @param mobile
	 * @param mobileCode
	 * @return
	 */
	public ShopUser loadUserByUsername(String sellerName, String mobile, String mobileCode);
	

	/**
	 * 商家子账号登录
	 * @param sellerName
	 * @param password
	 * @param loginCode
	 * @return
	 */
	public ShopAccountUserDto loadShopAccountByUsername(String sellerName, String password,  String loginCode);
	


}