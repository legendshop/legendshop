/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.StatusKeyValueEntity;
import com.legendshop.model.entity.ShopGrade;

/**
 * 用户等级服务.
 */
public interface ShopGradeService {

 	public Integer saveShopGrade(ShopGrade shopGrade);

 	public ShopGrade getShopGrade(Integer id);

 	public void deleteShopGrade(ShopGrade shopGrade);

 	public void updateShopGrade(ShopGrade shopGrade);

	public List<StatusKeyValueEntity> retrieveShopGrade();

	public PageSupport<ShopGrade> getShopGradePage(String curPageNO, ShopGrade shopGrade);
}
