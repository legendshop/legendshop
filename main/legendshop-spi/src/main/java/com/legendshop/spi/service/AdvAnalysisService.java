package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdvAnalysis;

/**
 * 活动广告
 */
public interface AdvAnalysisService {

	public Long save(AdvAnalysis advAnalysis);

	public PageSupport<AdvAnalysis> query(String curPageNO, String url);

	/**
	 * 查询每个链接的访问数
	 *
	 * @param curPageNO 当前页
	 * @param url
	 * @return
	 */
	public abstract PageSupport<AdvAnalysis> groupQuery(String userId, String curPageNO, String url);
}
