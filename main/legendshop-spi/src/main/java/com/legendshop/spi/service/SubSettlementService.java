/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.dto.app.AppRechargeDto;
import com.legendshop.model.entity.SubSettlement;

/**
 * 订单结算票据中心
 *
 */
public interface SubSettlementService  {
	
	 /**
     * 根据结算号获取结算单.
     *
     * @return the sub settlement
     */
    public abstract SubSettlement getSubSettlementBySn(String settlementSn);
    
    public SubSettlement getSubSettlement(String settlementSn, String userId);
    
    public int updateSubSettlement(SubSettlement subSettlement);

	public abstract AppRechargeDto getSubSettlementToAppOrderDto(String subSettlementSn, String userId);
	 
}
