/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.model.entity.Menu;

/**
 * 菜单管理服务.
 */
public interface MenuManagerService {
	
	/**
	 * Gets the all menu.
	 *
	 * @return the all menu
	 */
	public List<Menu> getAllMenu();
	
	/**
	 * Builds the role menu.
	 *
	 * @param menuList the menu list
	 * @return the map
	 */
	public Map<String,List<Menu>> buildRoleMenu(List<Menu> menuList);
	
	/**
	 * Gets the menu.
	 *
	 * @return the menu
	 */
	public List<Menu> getMenu();

}
