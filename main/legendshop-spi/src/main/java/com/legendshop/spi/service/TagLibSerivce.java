/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;
import java.util.List;

/**
 * 自定义标签.
 */
public interface TagLibSerivce {

	public List<Object[]> query(String sql, Object param) ;

	public List<Object[]> query(String sql);

}
