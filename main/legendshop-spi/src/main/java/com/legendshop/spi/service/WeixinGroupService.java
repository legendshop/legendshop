/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.weixin.WeiXinGroups;
import com.legendshop.model.entity.weixin.WeixinGroup;

/**
 * 微信组服务
 */
public interface WeixinGroupService  {

    public List<WeixinGroup> getWeixinGroup(String userName);
    
    public List<WeixinGroup> getWeixinGroup();

    public WeixinGroup getWeixinGroup(Long id);
    
    public void deleteWeixinGroup(WeixinGroup weixinGroup);
    
    public Long saveWeixinGroup(WeixinGroup weixinGroup);

    public void updateWeixinGroup(WeixinGroup weixinGroup);

    public void synchronousGroup(List<WeiXinGroups> dtList,List<WeixinGroup> dbList,Object obj);
}
