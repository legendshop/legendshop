/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;

/**
 * 
  * 布局服务缓存服务
 *
 */
public interface ShopDecotateCacheService  {
	
	/**
	 * 设置装修缓存
	 * 
	 * @param decotateDto
	 * @return
	 */
	ShopDecotateDto cachePutShopDecotate(ShopDecotateDto decotateDto);
	
	/**
	 * 更新线上缓存
	 * 
	 * @param decotateDto
	 * @return
	 */
	ShopDecotateDto cachePutOnlineShopDecotate(ShopDecotateDto decotateDto);

	StoreListDto cachePutShopStoreListDto(StoreListDto storeListDto);
	
	/**
	 * 撤销动作
	 * 
	 * @param shopId
	 */
	void cachePutShopDecotate(Long shopId);
	

	/**
	 * 清理缓存(ShopLayoutHotProdDtoList/ShopLayoutBannerList)
	 */
	void clearAllDecCache();

	
}
