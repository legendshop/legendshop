/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.SubHistory;

/**
 * 订单历史服务
 */
public interface SubHistoryService  {

	/**
	 * 保存订单历史
	 */
	void saveSubHistory(SubHistory subHistory);

}
