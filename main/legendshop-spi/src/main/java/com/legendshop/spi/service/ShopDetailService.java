/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.IdTextEntity;
import com.legendshop.model.R;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.BusinessMessageDTO;
import com.legendshop.model.dto.ShopDetailTimerDto;
import com.legendshop.model.dto.ShopSearchParamDto;
import com.legendshop.model.dto.ShopSeekDto;
import com.legendshop.model.dto.shop.OpenShopInfoDTO;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ShopAudit;
import com.legendshop.model.entity.ShopCompanyDetail;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserDetail;

/**
 * 商城信息服务.
 */
public interface ShopDetailService extends ShopService {

	/**
	 *用缓存来读取商城信息.
	 * 
	 * @param id
	 *            the id
	 * @return the shop detail
	 */
	public abstract ShopDetail getShopDetailById(Long id);
	
	/**
	 * 不用缓存来读取商城信息.
	 *
	 * @param shopId the shop id
	 * @return the shop detail by shop id
	 */
	public abstract ShopDetail getShopDetailByIdNoCache(final Long shopId);

	/**
	 * Find user detail by name.
	 * 
	 * @param userName
	 *            the user name
	 * @return the user detail
	 */
	public abstract UserDetail getShopDetailByName(String userName);

	/**
	 * Delete.
	 * 
	 * @param shopDetail
	 *            the shop detail
	 */
	public abstract String delete(ShopDetail shopDetail);

	// UserId 一定不能为空
	/**
	 * Save.
	 * 
	 * @param shopDetail
	 *            the shop detail
	 */
	public abstract void saveShopDetail(ShopDetail shopDetail);

	/**
	 * Update.
	 * 
	 * @param shopDetail
	 *            the shop detail
	 */
	public abstract void update(ShopDetail shopDetail);

	/**
	 * 审核商城.
	 *
	 * @param userId the user id
	 * @param shopDetail the shop detail
	 * @param status 状态,是否上线1：在线，0下线，-1审核中,-2拒绝,-3关闭（管理员操作）
	 * @return true, if successful
	 */
	public abstract boolean updateShop(String userId, ShopDetail shopDetail, Integer status);

	public abstract Long getShopIdByUserId(String userId);
	
	public ShopDetail getShopDetailByUserId(String userId);

	/**
	 * 再次申请开店
	 * @param shopDetail
	 * @param userId
	 * @param userName
	 * @return
	 */
	public abstract boolean reApplyOpenShop(ShopDetail shopDetail, String userId,String userName);

	public abstract Long getMaxShopId();
	
	public long getCountShopId();

	/**
	 * 查找firstShopId到toShopId 的所有商家
	 * 包含firstShopId ，但不包含toShopId
	 */
	public abstract List<ShopDetailTimerDto> getShopDetailList(Long firstShopId,  Integer commitInteval);

	public abstract Long getMinShopId();
	
	public abstract Integer updateShopInfo(ShopDetail OldShopDetail,ShopDetail newShopDetail,String flag);


	/**
	 * 查询默认的店铺主页信息
	 * @param shopId
	 * @return
	 */
	public abstract StoreListDto findDetfaultStoreListDto(Long shopId);
	
	public abstract Long saveContactInfo(ShopDetail shopDetail,String  userId,String userName);

	public abstract Integer updateContactInfo(ShopDetail OldShopDetail,ShopDetail newShopDetail);

	public abstract Long saveShopDetailInfo(ShopDetail shopdetail,String userName, String userId, Long shopId,String idCardPic,String idCardBackPic);
	
	//保存企业信息
	public abstract boolean saveCompanyInfo(ShopDetail shopDetail,ShopCompanyDetail shopCompanyDetail,String legalIdCard,String licensePics);
	
	public boolean saveCompanyInfo(ShopDetail shopDetail, Long shopId, String userName,String licensePics);
	
	public abstract boolean updateCompanyInfo(String userName,String userId, Long shopId, ShopCompanyDetail oldshopCompanyDetail,ShopCompanyDetail shopCompanyDetail,String legalIdCard,String licensePics);
	
	public abstract boolean saveCertificateInfo(String userName,String userId, Long shopId,ShopCompanyDetail shopCompanyDetail,String taxRegistrationNumE,String bankPermitPic);
	
	public abstract void changeOptionStatus(String userId,Long shopId,Integer option);
	
	public abstract List<ShopAudit> getShopAuditInfo(Long shopId);
	
	public abstract void changeStatus(String userId,Long shopId,Integer status);
	
	public abstract String  getShopName(Long shopId);
	
	public abstract Long getShopIdBySecDomain(String SecDomainName);

	public abstract List<ShopDetail> getAllShopDetail();

	/**
	 * 获取店铺id和店铺名,用于select2
	 * @return
	 */
	public abstract List<IdTextEntity> getShopDetailList(String shopName);
	
	public abstract Integer updateShopType(Long shopId,Long type);
	
	/**
	 * 获得店铺的平均分
	 */
	public abstract String getshopScore(Long shopId);

	public abstract PageSupport<ShopSeekDto> queryShopSeekList(ShopSearchParamDto param);

	public abstract PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail);

	public abstract PageSupport<ShopDetail> getShopDetailPage(String curPageNO, ShopDetail shopDetail, Integer pageSize,
			DataSortResult result);

	public abstract PageSupport<ShopAudit> getShopAuditPage(String curPageNO, Long shopId);

	public abstract PageSupport<ShopDetail> getloadSelectShop(String curPageNO, String shopName);

	public abstract int updateShopCommision(Long shopId, Integer commision);

	public abstract void save(String userName, String shopPic, ShopDetail shopDetail);

	/**
	 * 关联查询店铺及其类型信息
	 */
	public abstract ShopDetail getTypeShopDetailByUserId(String userId);


	/**
	 * 保存/更新个人入驻商家信息
	 *
	 * @param user
	 * @param shopDetail
	 * @param openShopInfoDTO
	 * @return
	 */
	R<String> savePersonalOpenShopInfo(LoginedUserInfo user, ShopDetail shopDetail, OpenShopInfoDTO openShopInfoDTO);

	/**
	 * 保存/更新企业入驻商家信息
	 * @param user
	 * @param shopDetail
	 * @param openShopInfoDTO
	 * @return
	 */
	R<String> saveBusinessOpenShopInfo(LoginedUserInfo user, ShopDetail shopDetail, OpenShopInfoDTO openShopInfoDTO);

	/**
	 * 根据店铺id获取营业执照信息
	 * @param shopId
	 * @return
	 */
	BusinessMessageDTO getBusinessMessage(Long shopId);



}