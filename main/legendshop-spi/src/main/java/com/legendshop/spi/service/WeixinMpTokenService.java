/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.weixin.WeixinMpToken;

/**
 * The Class WeixinMpTokenService.
 */
public interface WeixinMpTokenService  {

    public WeixinMpToken getWeixinMpToken(Long id);
    
    public void deleteWeixinMpToken(WeixinMpToken weixinMpToken);
    
    public Long saveWeixinMpToken(WeixinMpToken weixinMpToken);

    public void updateWeixinMpToken(WeixinMpToken weixinMpToken);

    /**
     * 获取accessToken
     * @return
     */
	public String getAccessToken();
}
