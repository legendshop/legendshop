package com.legendshop.spi.manager;

import java.io.IOException;


/**
 * 系统编辑的文件管理者
 *
 */

public interface BusinessFileManager {
	
	public static final String REGISTER_FILE_PATH = "/register";
	
	public static final String MAIL_FILE_PATH = "/mail";
	
	
	public static final String VALIDATION_MAIL = "validationEmail.html";
	
	public static final String REG_ITEM = "regItem.html";
	
	public static final String REGISTER_SUCCESS = "registersuccess.html";

	
	
	public String readFile(String parentFilePath, String fileName) throws IOException;
	
	/**
	 * 读取系统注册agreement文件
	 */
	public String readRegItem() throws IOException;
	
	/**
	 * 读取重置密码文件
	 * @return
	 */
	public String readResetPassMail() throws IOException;
	
	/**
	 * 读取注册成功说明文件
	 * @return
	 */
	public String readRegistersuccess() throws IOException;
	
	
}
