/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ShopCommStat;

/**
 * 物流评分统计表.
 */
public interface DvyTypeCommStatService  {

    public DvyTypeCommStat getDvyTypeCommStat(Long id);
    
    public DvyTypeCommStat getDvyTypeCommStatByShopId(Long shopId);
    
    public void deleteDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);
    
    public Long saveDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);

    public void updateDvyTypeCommStat(DvyTypeCommStat dvyTypeCommStat);

}
