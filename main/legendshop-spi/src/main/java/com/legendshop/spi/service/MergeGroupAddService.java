/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.entity.MergeGroupAdd;

/**
 * 拼团参加表服务接口
 */
public interface MergeGroupAddService  {

	/**
	 *  根据Id获取拼团参加表
	 */
	public MergeGroupAdd getMergeGroupAdd(Long id);

	/**
	 *  删除拼团参加表
	 */    
	public void deleteMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 *  保存拼团参加表
	 */	    
	public Long saveMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 *  更新拼团参加表
	 */	
	public void updateMergeGroupAdd(MergeGroupAdd mergeGroupAdd);

	/**
	 * 查询正在进行的团列表
	 * @param mergeId 拼团活动ID
	 * @param count 查询的数量, 0或null代表查询所有
	 * @return
	 */
	public List<MergeGroupingDto> getMergeGroupingByMergeId(Long mergeId, Integer count);

	/**
	 * 查询所有为成团的拼团订单
	 * @return
	 */
	public List<MergeGroupAdd> findHaveInMergeGroupAdd(Long long1);

	/**
	 * 
	 * @return
	 */
	public List<Long> findGroupExemption();

	/**
	 * 判断用户是否有资格参加拼团活动
	 * @param activeId
	 * @param userId
	 * @return
	 */
	public boolean joinMergeGroup(Long activeId, String userId);

	/**
	 * 查询团下面的已参团列表
	 * @param operateId 团ID
	 * @param count 查询数量, 为空或0代表不限制
	 * @return
	 */
	public List<MergeGroupAdd> queryMergeGroupAddList(Long operateId, Integer count);

	/**
	 * 根据团Id和用户ID查询参团记录
	 * @param operateId
	 * @param userId
	 * @return
	 */
	public MergeGroupAdd getMergeGroupAddByOperateIdAndUserId(Long operateId, String userId);

	/**
	 * 判断订单是否为拼团的团长免单订单
	 * @param subId
	 * @return
	 */
	Boolean isMergeGroupHeadFreeSub(Long subId);
}
