/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopNav;

/**
 *商家导航表
 */
public interface ShopNavService  {

    public ShopNav getShopNav(Long id);
    
    public void deleteShopNav(ShopNav shopNav);
    
    public Long saveShopNav(ShopNav shopNav);

    public void updateShopNav(ShopNav shopNav);

    public abstract List<ShopNav> getLayoutNav(Long shopId);

	public PageSupport<ShopNav> getShopNavPage(String curPageNO, Long shopId);

}
