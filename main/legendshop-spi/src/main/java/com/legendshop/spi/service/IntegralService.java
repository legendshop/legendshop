package com.legendshop.spi.service;

import com.legendshop.model.constant.SendIntegralRuleEnum;


/**
 * 
 * 送积分服务
 *
 */
public interface IntegralService {

	/**
	 * 送积分
	 * @param userId 用户ID
	 * @param ruleEnum 赠送积分类型
	 */
	public abstract void addScore(String userId,SendIntegralRuleEnum ruleEnum);

	
	
}
