/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.shopDecotate.ShopDecotate;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;

/**
 * 
 * 布局服务类
 *
 */
public interface ShopDecotateService {

	public ShopDecotate getShopDecotate(Long id);

	public void deleteShopDecotate(ShopDecotate shopDecotate);

	public Long saveShopDecotate(ShopDecotate shopDecotate);

	public void updateShopDecotate(ShopDecotate shopDecotate);

	public ShopDecotate getShopDecotateByShopId(Long shopId);

	public ShopDecotateDto findShopDecotateDto(ShopDecotate decotate);

	public ShopDecotateDto findShopDecotateCache(Long shopId);



	/**
	 * 模块编辑 设置导航mo
	 * 
	 * @param layoutParam
	 * @return
	 */
	public ShopLayoutDto decorateModuleShopNavs(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);

	/**
	 * 保存退出
	 * 
	 * @param decotateDto
	 * @return
	 */
	public ShopDecotateDto decorateSaveQuite(ShopDecotateDto decotateDto);

	/**
	 * 获取店铺的分类信息
	 * 
	 * @param layoutParam
	 * @param layoutDto
	 * @return
	 */
	public List<ShopLayoutCateogryDto> findShopCateogryDtos(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);

	/**
	 * 获取店铺的热销信息
	 * 
	 * @param layoutParam
	 * @param layoutDto
	 * @return
	 */
	public List<ShopLayoutHotProdDto> findShopHotProds(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);

	/**
	 * 获取店铺信息
	 * 
	 * @param layoutParam
	 * @param layoutDto
	 * @return
	 */
	public ShopLayoutShopInfoDto findShopInfo(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);

	/**
	 * 获取商品自定义商品
	 * 
	 * @param layoutParam
	 * @param layoutDto
	 * @return
	 */
	public List<ShopLayoutProdDto> findShopLayoutProdDtos(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);

	/**
	 * 获取热点
	 * 
	 * @param layoutParam
	 * @param layoutDto
	 * @return
	 */
	public ShopLayoutHotDto findShopLayoutHotDto(ShopLayoutParam layoutParam, ShopLayoutDto layoutDto);


	/**
	 * 查看线上缓存
	 * 
	 * @param shopId
	 * @return
	 */
	public ShopDecotateDto findOnlineShopDecotateCache(Long shopId);


	/**
	 * 查询商家列表
	 * 
	 * @param shopId
	 * @return
	 */
	StoreListDto findStoreListDto(Long shopId);

	ShopDecotateDto findShopCache(Long shopId);

	StoreListDto findShopStoreListDtoCache(Long shopId);

	/**
	 * 获取礼券
	 * 
	 * @param shopId
	 * @return
	 */
	public List<Coupon> getCouponByShopId(Long shopId);

}
