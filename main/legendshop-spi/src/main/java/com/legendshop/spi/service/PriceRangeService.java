package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PriceRange;

/**
 * 统计报表 价格区间服务
 */
public interface PriceRangeService {

	public List<PriceRange> queryPriceRange(Long shopId,Integer type);

    public PriceRange getPriceRange(Long id);

    public void deletePriceRange(PriceRange priceRange);

    public Long savePriceRange(PriceRange priceRange);

    public void updatePriceRange(PriceRange priceRange);

	public void savePriceRangeList(List<PriceRange> priceRangeList,Long shopId);
	
	public PageSupport<PriceRange> getPriceRange(String curPageNO, Long shopId, Integer type);
}
