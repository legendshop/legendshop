/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;


import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.AccusationDto;
import com.legendshop.model.entity.Accusation;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.model.entity.AccusationType;

/**
 * 举报服务.
 */
public interface AccusationService  {

    public List<Accusation> getAccusation(String userName);

    public Accusation getAccusation(Long id);
    
    public void deleteAccusation(Accusation accusation);
    
    public Long saveAccusation(Accusation accusation);

    public void updateAccusation(Accusation accusation);

    public List<AccusationSubject> queryAccusationSubject();
    
    public List<AccusationType> queryAccusationType();
    
    public Accusation getDetailedAccusation(Long id);
    
    // 获取所有类型的键值对
    public abstract List<KeyValueEntity> AccusationTypeList();
    
    //根据typeId 获取主题
    public abstract  List<KeyValueEntity> getAccusationSubject(Long typeId);

    public abstract Accusation getAccusationByUserAndProd(String userId, Long prodId);

	public void deleteAccusation(List<Accusation> accs);

	public List<Accusation> queryUnHandler();

	/** 根据 shopId 查找 商家被举报的个数 **/
	public int getAccusationCounts(Long shopId);

	public PageSupport<Accusation> query(Accusation accusation, String curPageNO);

	public PageSupport<Accusation> queryReportForbit(String curPageNO, Long shopId);

	public PageSupport<Accusation> queryOverlayLoad(Long id);

	public PageSupport<AccusationDto> query(String curPageNO, String userId);

	public PageSupport<Accusation> query(Long id, String userId);

	/**
	 * @param accusation
	 * @param handleAccusation
	 */
	public Long saveAccusation(Accusation accusation, Accusation handleAccusation);
}
