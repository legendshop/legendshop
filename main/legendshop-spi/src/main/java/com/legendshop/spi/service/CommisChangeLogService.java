/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.promoter.model.CommisChangeLog;

/**
 * 佣金变更历史Service.
 */
public interface CommisChangeLogService  {

    /** 获取佣金变更历史 */
    public CommisChangeLog getCommisChangeLog(Long id);
    
    /** 删除佣金变更历史 */
    public void deleteCommisChangeLog(CommisChangeLog commisChangeLog);
    
    /** 保存佣金变更历史 */
    public Long saveCommisChangeLog(CommisChangeLog commisChangeLog);

    /** 更新佣金变更历史 */
    public void updateCommisChangeLog(CommisChangeLog commisChangeLog);

    /** 获取佣金变更历史 */
	public PageSupport<CommisChangeLog> getCommisChangeLogByCq(String curPageNO);

	/** 获取佣金变更历史 */
	public PageSupport<CommisChangeLog> getCommisChangeLog(int pageSize, String userId);

	/** 获取佣金变更历史 */
	public PageSupport<CommisChangeLog> getCommisChangeLog(String curPageNO,int pageSize, String userId, AwardSearchParamDto paramDto);
}
