package com.legendshop.spi.service;

import com.legendshop.model.dto.order.KuaiDiLogs;

/**
 * 快递100接口
 */
public interface KuaiDiService {
	/**
	 * 根据URL查询快递信息
	 * @param url
	 * @return
	 */
	public KuaiDiLogs query(String url);
}
