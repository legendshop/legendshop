/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductConsultDto;
import com.legendshop.model.entity.ProductConsult;

/**
 * 商品咨询服务
 */
public interface ProductConsultService{

	/**
	 * Gets the product consult list.
	 *
	 */
	public abstract List<ProductConsult> getProductConsultList(Long prodId);

	/**
	 * Gets the product consult.
	 *
	 */
	public abstract ProductConsult getProductConsult(Long id);

	/**
	 * Save product consult.
	 */
	public abstract Long saveProductConsult(ProductConsult productConsult);

	/**
	 * Update product consult.
	 * 
	 */
	public void updateProductConsult(String name, ProductConsult productConsult);


	/**
	 * Gets the product consult.
	 */
	public abstract PageSupport<ProductConsult> getProductConsult(String curPageNO, ProductConsult productConsult);

	/**
	 * Check frequency. 检查用户发布频率
	 *
	 */
	public abstract long checkFrequency(ProductConsult consult);

	public abstract PageSupport<ProductConsult> getProductConsultForList(String curPageNO, Integer pointType, Long prodId);

	public void deleteProductConsultById(Long consId);
	
	/**
	 * 商家更改商家删除标记  0：保留 1：删除
	 * @param consId
	 */
	public void deleteProductConsultByShopUser(Long consId);

	public abstract ProductConsultDto getConsult(String userId, Long consId);

	public abstract int updateConsult(String userId,ProductConsultDto consult);

	public abstract int deleteProductConsult(String userId, Long consId);

	public abstract List<ProductConsult> queryUnReply();

	/**
	 * 根据商品Id 查找 咨询个数（没回答的）
	 *
	 */
	public abstract int getConsultCounts(Long shopId);

	public abstract PageSupport<ProductConsult> getProductConsultPage(String userName, ProductConsult productConsult, String curPageNO);

	public abstract PageSupport<ProductConsult> queryQarseConsult(String curPageNO, Long shopId, Integer replyed);

	public abstract PageSupport<ProductConsult> queryProdcons(String curPageNO, Long shopId, ProductConsult productConsult);

	public abstract PageSupport<ProductConsult> query(String curPageNO, String userName);

	public abstract int batchDeleteConsultByIds(String consIdStr);


}
