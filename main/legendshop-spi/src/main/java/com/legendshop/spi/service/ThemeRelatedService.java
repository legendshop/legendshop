/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.ThemeRelatedDto;
import com.legendshop.model.entity.ThemeRelated;

/**
 *相关专题服务
 */
public interface ThemeRelatedService  {

    public List<ThemeRelated> getThemeRelatedByTheme(Long themeId);

    public ThemeRelated getThemeRelated(Long id);
    
    public void deleteThemeRelated(ThemeRelated themeRelated);
    
    public Long saveThemeRelated(ThemeRelated themeRelated);

    public void updateThemeRelated(ThemeRelated themeRelated);


    List<ThemeRelatedDto> getThemeRelatedDtoByTheme(Long id);
}
