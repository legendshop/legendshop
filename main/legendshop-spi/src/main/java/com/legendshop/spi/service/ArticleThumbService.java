/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ArticleThumb;

/**
 * 文章点赞服务接口
 */
public interface ArticleThumbService  {

   	/**
	 *  根据Id获取
	 */
    public ArticleThumb getArticleThumb(Long id);

   /**
	 *  删除
	 */    
    public void deleteArticleThumb(ArticleThumb articleThumb);

   /**
	 *  保存
	 */	    
    public Long saveArticleThumb(ArticleThumb articleThumb);

   /**
	 *  更新
	 */	
    public void updateArticleThumb(ArticleThumb articleThumb);

	/**
	 * 保存
	 *
	 */
	public String saveThumb(ArticleThumb articleThumb, Long artId, String userId);
}
