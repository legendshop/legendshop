/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AdminDoc;

/**
 * 后台帮助文档服务接口
 */
public interface AdminDocService {

	/**
	 * 根据Id获取
	 */
	public AdminDoc getAdminDoc(Long id);

	/**
	 * 删除
	 */
	public void deleteAdminDoc(AdminDoc adminDoc);

	/**
	 * 保存
	 */
	public Long saveAdminDoc(AdminDoc adminDoc);

	/**
	 * 更新
	 */
	public void updateAdminDoc(AdminDoc adminDoc);

	/**
	 * 查询列表
	 */
	public PageSupport<AdminDoc> queryAdminDocList(String curPageNO, AdminDoc adminDoc);
}
