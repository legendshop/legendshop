
/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;

/**
 * 优惠券服务.
 */
public interface CouponService {

	public List<Coupon> getCouponByShopId(Long shopId);

	public Coupon getCoupon(Long id);

	public void deleteCoupon(Coupon coupons);

	public Long saveCoupon(Coupon coupons);

	public void updateCoupon(Coupon coupons);

	public String deleteCoupon(Long id, Long shopId, String source);

	public void activateCoupon(String userId, String userName, Coupon coupon, UserCoupon userCoupon);

	public Long saveByCoupon(Coupon coupon, String userName, String userId, Long shopId, String fileName, MultipartFile rightFile);

	public List<Coupon> getCouponByProvider(String getType, String provider, Date date);

	public PageSupport<Coupon> getCouponsPage(String curPageNO, String type);

	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(String userId, int sizePage, String curPageNO, Integer useStatus, String couPro);

	public PageSupport<Coupon> queryCouponPage(String curPageNO, String userName);

	public PageSupport<Coupon> queryCoupon(StringBuffer sql, String curPageNO, StringBuffer sqlCount, List<Object> objects);

	public PageSupport<UserCoupon> queryUserCoupon(String sqlString, StringBuilder sqlCount, List<Object> objects, String curPageNO);

	public PageSupport<Coupon> getIntegralCenterList(String curPageNO, String type);

	public PageSupport<PersonalCouponDto> queryPersonalCouponDto(int sizePage, String curPageNO, String userId, String couPro);

	public PageSupport<Coupon> queryCoupon(String curPageNO, Coupon coupon, DataSortResult result);

	public PageSupport<UserCoupon> queryUsecouponView(String curPageNO, String sqlString, StringBuilder sqlCount, List<Object> objects);

	public PageSupport<Coupon> getPlatform(String curPageNO, Coupon coupon);

	PageSupport<Coupon> getIntegralList(String curPageNO, String type, String shopId);

	long getCouponCountByShopId(String type, String shopId);

	public Integer batchDelete(String platfromIds);

	public int batchOffline(String platfromIds);

	public PageSupport<Coupon> loadSelectCouponByDraw(String curPageNO, String couponName, Date startDate, Date endDate);

	/**
	 * 获取商品优惠券
	 */
	public List<Coupon> getProdsCoupons(Long shopId, Long prodId);

	/**
	 * 添加红包
	 */
	public Long saveCouponByCoupon(Coupon coupon, String userName,String userId, Long shopId,  String fileName, MultipartFile rightFile);
	
	
	public Long saveByCouponAndShopId(Coupon coupon, String userName,String userId, Long shopId);

	/**
	 * 发送优惠卷
	 */
	public Integer updateCoupon(Coupon coupon, List<String> result, Long couponId, List<String> result2, List<UserCoupon> lists);

	/**
	 * 添加优惠券
	 */
	public void saveByCouponAndShopId(Coupon coupon, String userName,String userId, Long shopId, String prodidList);

	/**
	 * 根据couponId获取优惠券
	 */
	public Coupon getCouponByCouponId(Long couponId);

	/**
	 * 获取店铺和商品相关可领的优惠券
	 */
	public List<Coupon> getShopAndProdsCoupons(Long shopId, Long prodId);

}
