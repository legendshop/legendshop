/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.seckill.SeckillActivityDto;
import com.legendshop.model.entity.SeckillProd;

/**
 * 秒杀商品Service
 */
public interface SeckillProdService {

	/** 获取秒杀商品 */
	public List<SeckillProd> getSeckillProds(long sId);

	/** 获取秒杀商品 */
	public SeckillProd getSeckillProd(Long id);

	/** 删除秒杀商品 */
	public void deleteSeckillProd(SeckillProd seckillProd);

	/** 保存秒杀商品 */
	public Long saveSeckillProd(SeckillProd seckillProd);

	/** 更新秒杀商品 */
	public void updateSeckillProd(SeckillProd seckillProd);

	/** 保存秒杀商品列表 */
	public List<Long> saveSeckillProdList(List<SeckillProd> seckillProdList);

	/** 查询商品sku */
	public List<SeckillActivityDto> findSkuProd(List<SeckillProd> seckillProds);

	/** 检查秒杀商品是否可以参加秒杀活动 */
	public boolean checkSeckillProdSku(Long prodId);

	/** 查询秒杀商品列表 */
	public List<SeckillProd> querySeckillProdList(List<Long> ids);
	
}
