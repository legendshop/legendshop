/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.draw.AjaxJson;
import com.legendshop.model.entity.draw.DrawActivity;
import com.legendshop.model.entity.draw.DrawWinRecord;

/**
 * 抽奖活动服务.
 */
public interface DrawActivityService  {

    /**
     * @Description: 查询正在进行的活动
     * @date 2016-5-3 
     */
    public List<DrawActivity> getDrawActivity();

    public DrawActivity getDrawActivityById(Long id);
    
    public void deleteDrawActivity(DrawActivity drawActivity);
    
    public Long saveDrawActivity(DrawActivity drawActivity) throws Exception;

    public void updateDrawActivity(DrawActivity drawActivity);

	public PageSupport<DrawActivity> getDrawActivityPage(String curPageNO, DrawActivity drawActivity);
	
	/**领奖(预付款、积分充值)*/
	public AjaxJson receiveCashOrIntegral(DrawWinRecord drawWinRecord, Long awardsRecordId, String userId, String userName, DrawActivity activityPo, AjaxJson json);
	
    
}
