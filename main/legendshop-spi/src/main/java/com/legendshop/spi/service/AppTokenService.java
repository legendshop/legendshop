package com.legendshop.spi.service;

public interface AppTokenService {

	/**
	 * 用于后台管理下线app用户
	 *
	 * @param userId
	 * @return
	 */
	public boolean deleteAppToken(String userId);
}
