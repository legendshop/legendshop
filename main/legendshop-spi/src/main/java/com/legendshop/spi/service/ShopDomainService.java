/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ShopDomain;

/**
 * 店铺域名服务
 */
public interface ShopDomainService  {

    public ShopDomain getShopDomain(Long id);
    
    public void deleteShopDomain(ShopDomain shopDomain);
    
    public Long saveShopDomain(ShopDomain shopDomain);

    public void updateShopDomain(ShopDomain shopDomain);

}
