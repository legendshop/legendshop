/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.SystemConfig;

/**
 * 系统配置
 */
public interface SystemConfigService  {

    public SystemConfig getSystemConfig();
    
    public void updateSystemConfig(SystemConfig systemConfig);
    
    public SystemConfig getSysWechatCode();

}
