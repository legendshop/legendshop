/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.weixin.WeixinReceivetext;

/**
 * 微信接受的文字服务
 */
public interface WeixinReceivetextService  {

    public WeixinReceivetext getWeixinReceivetext(Long id);
    
    public void deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext);
    
    public Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext);

    public void updateWeixinReceivetext(WeixinReceivetext weixinReceivetext);

}
