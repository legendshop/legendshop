package com.legendshop.spi.service;

import com.legendshop.model.dto.marketing.RuleResolver;

/**
 * 促销规则管理器
 */
public interface ActiveRuleResolverManager {

	void executorMarketing(RuleResolver resolver);

}
