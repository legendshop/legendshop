/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.dto.store.StoreCitys;
import com.legendshop.model.entity.store.StoreProd;
import com.legendshop.model.entity.store.StoreSku;

/**
 * 门店商品服务接口
 */
public interface StoreProdService {

	/** 获取门店商品 */
	public StoreProd getStoreProd(Long id);

	/** 删除门店商品 */
	public void deleteStoreProd(StoreProd storeProd);

	/** 保存门店商品 */
	public Long saveStoreProd(StoreProd storeProd);

	/** 更新门店商品 */
	public void updateStoreProd(StoreProd storeProd);

	/** 获取门店商品数量 */
	public long getStoreProdCount(Long poductId, Long skuId);

	/** 按商品来查找地区的门店 */
	public List<StoreCitys> findStoreCitys(Long poductId, String province, String city, String area);

	/** 按商城来查找地区的门店 */
	public List<StoreCitys> findStoreShopCitys(Long shopId, String province, String city, String area);

	/** 获取库存 */
	public Integer getStocks(Long storeId, Long prodId, Long skuId);

	/** 弹出设置库存窗口 */
	public StoreProductDto getStoreProductDto(Long storeId, Long prodId);

	/** 删除商品 */
	public String deleteProd(StoreProd storeProd);

	/** 删除商品Sku */
	public String deleteSku(StoreSku storeSku);

	/** 获取门店商品 */
	public StoreProd getStoreProd(Long storeId, Long prodId);

	/** 往门店添加商品 */
	public String addProd(StoreProductDto prod);

	/** 设置门店商品库存 */
	String setStock(StoreProductDto prod);
	
	/** 获取门店商品 */
	public StoreProd getStoreProdByShopIdAndProdId(Long shopId, Long prodId);
	
}
