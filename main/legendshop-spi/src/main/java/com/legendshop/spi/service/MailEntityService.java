/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.MailInfo;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.entity.MailEntity;

/**
 * 邮件服务.
 */
public interface MailEntityService  {

    public MailEntity getMailEntity(Long id);
    
    public void deleteMailEntity(MailEntity mailEntity);
    
    public Long saveMailEntity(MailEntity mailEntity);

    public void updateMailEntity(MailEntity mailEntity);

	public void saveMail(MailInfo task);

	MailEntity getMailEntity(String receiveMail, MailCategoryEnum type);

	public void clearMailCode(Long id);

	public PageSupport<MailEntity> getMailEntityPage(String curPageNO);
}
