/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 *专题板块服务
 */
public interface ThemeModuleService  {

    public abstract List<ThemeModule> getThemeModuleByTheme(Long themeId);

    public ThemeModule getThemeModule(Long id);
    
    public void deleteThemeModule(ThemeModule themeModule);
    
    public Long saveThemeModule(ThemeModule themeModule);

    public void updateThemeModule(ThemeModule themeModule);

    /**
     * 获取专题相关的产品
     */
	Map<ThemeModule, List<ThemeModuleProd>> getThemeModuleAndProdByThemeId(Long id);

	public abstract void deleteThemeModule(List<ThemeModuleProd> themeModuleProdList, ThemeModule themeModule);

	public abstract void updateThemeModule(String type, ThemeModule themeModule);
}
