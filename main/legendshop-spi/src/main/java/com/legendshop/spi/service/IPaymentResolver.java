package com.legendshop.spi.service;

import java.util.Map;

import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.BankCallbackForm;

/**
 * 支付处理者
 *
 */
public interface IPaymentResolver {
	
	/**
	 * 查询支付的单据信息
	 * @param params
	 * @return
	 */
	Map<String, Object> queryPayto(Map<String, String> params);


	/**
	 * 支付回调信息
	 */
	void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallbackForm);

}
