/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ZhiboAccount;


/**
 * 直播账号服务接口
 */
public interface ZhiboAccountService  {

   	/**
	 *  根据Id获取
	 */
    public ZhiboAccount getZhiboAccountById(String id);

   /**
	 *  删除
	 */    
    public void deleteZhiboAccount(ZhiboAccount zhiboAccount);

   /**
	 *  保存
	 */	    
    public String saveZhiboAccount(ZhiboAccount zhiboAccount);

   /**
	 *  更新
	 */	
    public void updateZhiboAccount(ZhiboAccount zhiboAccount);

    public int insertZhiboAccount(ZhiboAccount zhiboAccount);
    
    public int loginUpdate(ZhiboAccount zhiboAccount);
    
    public ZhiboAccount getAccountByToken(String token,String type);
    
    /**
     * 根据类型和名称获取直播帐号
     */
	public ZhiboAccount getZhiboAccountByType(String userName, String type);
}
