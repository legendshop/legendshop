/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSku;

import java.util.List;

/**
 * The Class PresellSkuService.
 * 预售活动sku服务接口
 */
public interface PresellSkuService  {

   	/**
	 *  根据Id获取预售活动sku
	 */
    public PresellSku getPresellSku(Long id);

    /**
	 *  根据Id删除预售活动sku
	 */
    public int deletePresellSku(Long id);
    
    /**
	 *  根据对象删除预售活动sku
	 */
    public int deletePresellSku(PresellSku presellSku);
    
   /**
	 *  保存预售活动sku
	 */	    
    public Long savePresellSku(PresellSku presellSku);

   /**
	 *  更新预售活动sku
	 */	
    public void updatePresellSku(PresellSku presellSku);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<PresellSku> queryPresellSku(String curPageNO, Integer pageSize);

	/**
	 *  根据预售id删除预售活动sku
	 */
	public void deletePresellSkuByPid(Long presellId);

	/**
	 * 通过sku获取预售sku
	 * @param skuId
	 * @return
	 */
	PresellSku getPresellSkuBySkuId(Long presellId,Long skuId);

	/**
	 * 根据活动id获取Skuid集合
	 * @param id
	 * @return
	 */
	List<Long> getSkuIdByPresellId(Long id);
}
