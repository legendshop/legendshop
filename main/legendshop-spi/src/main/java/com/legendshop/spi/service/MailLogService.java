/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.MailLog;

/**
 * 邮件发送历史Service.
 */
public interface MailLogService  {

    public MailLog getMailLog(Long id);
    
    public void deleteMailLog(MailLog mailLog);
    
    public Long saveMailLog(MailLog mailLog);

    public void updateMailLog(MailLog mailLog);

	public PageSupport<MailLog> getMailLogPage(String curPageNO, MailLog mailLog);
}
