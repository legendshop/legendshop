/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.JobStatus;

/**
 * 定时任务接口.
 */
public interface JobStatusService  {

    public JobStatus getJobStatus(Long id);
    
    public void deleteJobStatus(JobStatus jobStatus);
    
    public Long saveJobStatus(JobStatus jobStatus);

    public void updateJobStatus(JobStatus jobStatus);

	public JobStatus getJobStatusByJobName(String jobName);
	
	//启动一个Job
	public JobStatus startJob(String jobName);
	
	//结束一个Job
	public boolean stopJob(JobStatus jobStatus);
}
