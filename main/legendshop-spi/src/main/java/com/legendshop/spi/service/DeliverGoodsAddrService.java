/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliverGoodsAddr;

/**
 * 发货地址服务.
 */
public interface DeliverGoodsAddrService  {

    public List<DeliverGoodsAddr> getDeliverGoodsAddrList(Long shopId);

    public DeliverGoodsAddr getDeliverGoodsAddr(Long id);
    
    public void deleteDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr);
    
    public Long saveDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr, Long shopId);

    public void updateDeliverGoodsAddr(DeliverGoodsAddr deliverGoodsAddr);

	public Long getMaxNumber(Long shopId);

	public DeliverGoodsAddr getDefaultDeliverGoodsAddr(Long shopId);

	public void updateDefaultDeliverAddress(Long addrId, Long shopId);

    /**
     * 打开默认地址
     */
	public void turnonDefault(Long id, Long shopId);

	/**
	 * 关闭默认地址
	 */
	public void closeDefault(Long id, Long shopId);

	/**
	 * 删除商家的发货地址
	 */
	public void deleteDeliverGoodsAddr(Long addrId, Long shopId);
	
	/**
	 * 获取默认地址
	 */
	public DeliverGoodsAddr getDeliverGoodsAddr(Long shopId, int i);

	public PageSupport<DeliverGoodsAddr> getDeliverGoodsAddrPage(String curPageNO, Long shopId);
}
