package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.ExternalLink;

/**
 * 友情链接服务
 */
public interface ExternalLinkService {

	public abstract List<ExternalLink> getExternalLink();

	public abstract ExternalLink getExternalLinkById(Long id);

	public abstract void delete(Long id);

	public abstract Long save(ExternalLink externalLink);

	public abstract void update(ExternalLink externalLink);

	public abstract PageSupport<ExternalLink> getFriendLink(String curPageNO, String userName);

	public abstract PageSupport<ExternalLink> getDataByCriteriaQuery(String curPageNO, ExternalLink externalLink, Integer pageSize, DataSortResult result);

	public abstract void delete(Long id, ExternalLink externalLink);

	public abstract void saveOrUpdate(String userId, Long shopId, String userName, ExternalLink externalLink,String picUrl);

}