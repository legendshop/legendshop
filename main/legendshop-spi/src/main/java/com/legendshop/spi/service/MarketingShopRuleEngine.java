package com.legendshop.spi.service;

import com.legendshop.model.dto.buy.ShopCarts;



/**
 * 营销计算规则引擎
 *
 */
public interface MarketingShopRuleEngine {

	/**
	 * 
	 * @param shopCarts 一个店铺里的购物车里的商品
	 */
	public void  executorMarketing(ShopCarts shopCarts); 
	
}
