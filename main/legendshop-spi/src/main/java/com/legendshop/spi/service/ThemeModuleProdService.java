/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ThemeModuleProd;

/**
 *专题板块商品
 */
public interface ThemeModuleProdService  {

    public List<ThemeModuleProd> getThemeModuleProdByModuleid(Long id);

    public ThemeModuleProd getThemeModuleProd(Long id);
    
    public void deleteThemeModuleProd(ThemeModuleProd themeModuleProd);
    
    public Long saveThemeModuleProd(ThemeModuleProd themeModuleProd);

    public void updateThemeModuleProd(ThemeModuleProd themeModuleProd);

	public void updateThemeModuleProdt(ThemeModuleProd themeModuleProd);

	public void deleteThemeModuleProd(ProductDetail prodInfo, ThemeModuleProd themeModuleProd);

}
