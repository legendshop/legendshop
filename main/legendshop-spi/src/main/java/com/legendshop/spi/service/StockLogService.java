/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.StockLog;

/**
 *库存历史服务
 */
public interface StockLogService  {

    public StockLog getStockLog(Long id);
    
    public void deleteStockLog(StockLog stockLog);
    
    public Long saveStockLog(StockLog stockLog);

    public void updateStockLog(StockLog stockLog);

	public PageSupport<StockLog> loadStockLog(Long prodId, Long skuId, String curPageNO);
}
