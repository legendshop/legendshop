/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Set;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SensitiveWord;

/**
 *
 * 敏感字服务
 *
 */
public interface SensitiveWordService  {

    public SensitiveWord getSensitiveWord(Long id);
    
    public void deleteSensitiveWord(SensitiveWord sensitiveWord);
    
    public Long saveSensitiveWord(SensitiveWord sensitiveWord);

    public void updateSensitiveWord(SensitiveWord sensitiveWord);

    public Set<String> checkSensitiveWords(String src);

	public boolean checkSensitiveWordsNameExist(String nickName);

	public PageSupport<SensitiveWord> getSensitiveWord(String curPageNO, SensitiveWord sensitiveWord);
}
