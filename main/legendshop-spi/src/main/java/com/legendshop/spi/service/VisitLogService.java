/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.text.ParseException;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.VisitLog;

/**
 *浏览历史服务类.
 */
public interface VisitLogService {

	/**
	 * Load.
	 * 
	 * @param id
	 *            the id
	 * @return the visit log
	 */
	public abstract VisitLog getVisitLogById(Long id);

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void delete(Long id);

	/**
	 * Save.
	 * 
	 * @param visitLog
	 *            the visit log
	 * @return the long
	 */
	public abstract Long save(VisitLog visitLog);

	/**
	 * Update.
	 * 
	 * @param visitLog
	 *            the visit log
	 */
	public abstract void update(VisitLog visitLog);

	/**
	 * Process.
	 * 
	 * @param visitLog
	 *            the visit log
	 */
	public abstract void process(VisitLog visitLog);

	/**
	 * 根据用户名获取浏览历史
	 * @param curPageNO
	 * @param userName
	 * @param visitLog
	 * @return
	 * @throws ParseException
	 */
	public abstract PageSupport<VisitLog> getVisitLogListPage(String curPageNO, String userName, VisitLog visitLog) throws ParseException;

	public abstract PageSupport<VisitLog> getVisitLogListPage(String curPageNO, VisitLog visitLog,
			DataSortResult result, Integer pageSize);

	/**
	 * 根据用户获取浏览历史的统计数量
	 * @param userName
	 * @return
	 */
	public abstract Long getVisitLogCountByUserName(String userName);
	
}