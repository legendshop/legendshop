/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.LoginHistorySum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.model.entity.LoginHistory;

/**
 * 登录历史服务.
 */
public interface LoginHistoryService {

	/**
	 * 保存登录历史.
	 *
	 */
	public abstract void saveLoginHistory(LoginSuccess loginSuccess);

	/** 根据 用户名  查取用户上一次登录时间 **/
	public abstract LoginHistory getLastLoginTime(String userName);

	public abstract PageSupport<LoginHistory> getLoginHistoryPage(String curPageNO, LoginHistory login);

	public abstract PageSupport<LoginHistorySum> getLoginHistoryBySQL(String curPageNO, LoginHistory login);

}