/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopRole;
import com.legendshop.model.security.ShopMenuGroup;

/**
 *商家角色服务
 */
public interface ShopRoleService  {

    public ShopRole getShopRole(Long id);
    
    public void deleteShopRole(ShopRole shopRole);
    
    public Long saveShopRole(ShopRole shopRole);

    public void updateShopRole(ShopRole shopRole);

	public ShopRole getShopRole(Long shopId, String name);

	public List<ShopRole> getShopRolesByShopId(Long shopId);

	public PageSupport<ShopRole> getShopRolePage(String curPageNO, Long shopId);

	/**
	 * 保存设置职位权限
	 */
	public void saveJobRole(ShopRole shopRole, List<ShopMenuGroup> shopMenuGroups, Long shopId);
}
