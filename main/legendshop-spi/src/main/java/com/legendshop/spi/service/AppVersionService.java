package com.legendshop.spi.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.model.dto.app.AppVersion;

/**
 * App版本服务
 */
public interface AppVersionService {
	
	public abstract AppVersion getAppVersion();
	
	public abstract String uploadApk(MultipartFile apkFile, String version, String type) throws IOException;
	
	public abstract String deleteApk(String type);

	public abstract String updateIOSAppstore(String url, String type);
}
