/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.AccusationSubject;

/**
 *
 *  举报主题服务
 *
 */
public interface AccusationSubjectService  {

    public AccusationSubject getAccusationSubject(Long id);
    
    public void deleteAccusationSubject(AccusationSubject accusationSubject);
    
    public Long saveAccusationSubject(AccusationSubject accusationSubject);

    public void updateAccusationSubject(AccusationSubject accusationSubject);

}
