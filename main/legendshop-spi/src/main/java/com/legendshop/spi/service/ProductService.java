/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.ProdUserAttribute;
import com.legendshop.model.entity.ProdUserParam;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.prod.FullActiveProdDto;

/**
 * 产品服务类接口.
 */
public interface ProductService {

	/**
	 * 根据ID获取一个产品.
	 *
	 */
	public abstract Product getProductById(Long prodId);

	/**
	 * 更新产品.
	 *
	 */
	public abstract int updateProduct(Product product, Product origin);

    /**
     * 保存导入的商品
     *
     * @param userId
     * @param userName
     * @param shopId
     * @param prodExportDTO
     * @return
     */
    void saveImportProduct(String userId, String userName, Long shopId, ProdExportDTO prodExportDTO);

	/**
	 * 保存产品.
	 */
	public abstract Long saveProduct(Product product, String prodType);


	/**
	 * 加载商品动态参数
	 * 
	 */
	public abstract String getProdParameter(Long prodId);



	/**
	 * 更新商品.
	 *
	 */
	public abstract void updateProd(Product product);
	
	/**
	 * 获取商品详情
	 *
	 */
	public abstract ProductDetail getProdDetail(final Long prodId);
	
	/**
	 * 根据prodId获取ProductDetail
	 */
	public abstract List<ProductDetail> getProdDetail(final Long [] prodId);

	/**
	 * 更新商品查看数
	 *
	 */
	public abstract void updateProdViews(Long prodId);


	/**
	 * 获取商家热门商品.
	 *
	 */
	public List<Product> getHotOn(Long shopId, Long categoryId);
	
	/**
	 * 获取全场热门商品.
	 *
	 */
	public List<Product> getHotAll(Integer max);

	/**
	 * 获取热门商品.
	 *
	 */
	public abstract List<Product> getHotViewProd(Long shopId, int maxNum);


	/**
	 * 获取热门评论
	 * 
	 */
	public abstract List<Product> getHotComment(Long shopId, Long categoryId, int maxNum);

	/**
	 * 获取热门推荐.
	 *
	 */
	public abstract List<Product> getHotRecommend(Long shopId, Long sortId, int maxNum);

	/**
	 * 猜你喜欢
	 */
	public abstract List<ProductDetail> getLikeProd(Long categoryId);

	/**
	 * 删除商品
	 */
	public abstract String delete(Long shopId, Long prodId,boolean isAdmin);

	public abstract ProdUserAttribute  getProdUserAttribute(Long id);
	
	public abstract void saveProdUserAttribute(ProdUserAttribute prodUserAttribute);
	
	public abstract void deleteProdUserAttribute(Long id,Long shopId);
	
	public abstract void deleteProdUserParam(Long id,Long shopId);
	
	public abstract Long saveProdUserParam(ProdUserParam prodUserParam);
	
	public abstract ProdUserParam getProdUserParam(Long id);
	
	/**
	 * 更改商品状态
	 * @param status
	 */
	public abstract void updateStatus(Integer status,Long productId,String auditOpin);

	public ProductDto getProductDto(Long prodId);
	
	/** 
	 * 
	 * 卖家中心 发布商品或编辑商品  
	 * 
	 **/
	public abstract Long releaseOfProduct(Long shopId, String userId, String userName,Product product);

	/** 卖家中心编辑商品 **/
	public abstract ProductDto getProductDto(PublishProductDto publishProductDto,Product product,Long prodId, boolean isSimilarImport);

	

	List<ProdParamDto> getPropGroupByProdId(Long prodId);

	List<ParamGroupDto> getParamGroups(Long prodId);
	
	/**
	 * 获取最新上架的商品
	 * @param mount 条数
	 * @return
	 */
	public abstract List<Product> getNewProds(int mount);

	/**
	 * 获取最近收藏的商品
	 * @param amount
	 * @return
	 */
	public abstract List<Product> getRencFavorProd(Integer amount,String userId);

	/**
	 * 获取商品的运费信息
	 * @param shopId 商家ＩＤ
	 * @param transportId 运费模版
	 * @param cityId　城市地址
	 * @param totalCount　总件数
	 * @param totalWeight　总重量
	 * @param totalvolume　总体积
	 * @return
	 */
	public abstract List<TransfeeDto> getProdFreight(Long shopId,Long transportId, Integer cityId,double totalCount,
	 double totalWeight ,
	 double totalvolume);

	/**
	 * 保存商品属性值图片
	 */
	public abstract void savePropValueImage(String valueImages, Long prodId);

	public abstract List<Product> getHotsaleProdByCategoryId(Long categoryId,int i);

	/** 商品销售排行 **/
	public abstract List<SalesRankDto> getSalesRank(Long shopId);

	/** 根据 shopId 获取 商品每个状态的数量  **/
	public abstract List<KeyValueEntity> getProdCounts(Long shopId);

	/** 更新商品的 分销佣金**/
	public abstract void updateProdDistCommis(Product product);

	public abstract AuctionsDto getAuctionProd(Long prodId, Long skuId);

	public abstract int checkStock(Long prodId, Long skuId);
	
	/**修改商品为团购商品**/
	public abstract boolean updataIsGroup(Long prodId,Integer status);
	
	/**获得商品的团购价格*/
	public abstract Group getGroupPrice(Long prodId);
	
	/**获得商品的秒杀价格*/
	public abstract Seckill getSeckillPrice(Integer seckillId,Long prodId,Long skuId);
	
	/**获得商品的拍卖的信息*/
	public abstract Auctions getAuctionsPrice(Long prodId,Long skuId);
	
	/**获得商品的预售的信息*/
	public abstract PresellProd getPresellProdPrice(Long prodId,Long skuId);
	

	/** 增加商品表评论数**/
	public abstract void addProdComments(Long prodId);

	/**
	 * 获取商品的库存数量
	 * @param productId
	 * @param skuId
	 * @return
	 */
	public abstract int getveryBuyNum(Long productId, Long skuId);

	public abstract List<ProductExportDto> findExportProd(Product prod);

	public abstract List<Product> getProdByCateId(Long id);

	public abstract void releaseSeckill(SeckillActivity seckillActivity);

	public abstract PageSupport<Product> getProductList(String name, String curPageNO,Long shopId);
	
	public PageSupport<Product> storeProdctList(Long shopId,String curPageNO,Long fireCat,Long secondCat,Long thirdCat,String keyword,String order);
	
	public PageSupport<Product> newProd(Long shopId);

	public abstract PageSupport<Product> getProductListPage(String curPageNO, Product product);

	public abstract PageSupport<ProdUserAttribute> getProdUserAttributePage(String curPageNO, Long shopId);

	public abstract PageSupport<ProdUserParam> getProdUserParamPage(String curPageNO, Long shopId);

	public abstract PageSupport<Product> getAuctionProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProductListPage(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getloadProdListPage(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getDustbinProdListPage(String curPageNO, String name, String siteName);

	public abstract PageSupport<Product> getloadSelectProd(Long shopId, String curPageNO, Product product);

	public abstract PageSupport<ProductDetail> getAdvancedSearchProds(Integer curPageNO, ProductSearchParms parms);

	public abstract PageSupport<Product> getProductFloorLoad(String curPageNO, String name,Long shopId);

	public abstract PageSupport<Product> getGroupProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> groupProdLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getloadProdListPageByshopId(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getloadWantDistProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<ProdUserAttribute> getProdUserAttributeByPage(String curPageNO, Long shopId);

	public abstract PageSupport<ProdUserParam> getUserParamOverlay(String curPageNO, Long shopId);

	public abstract PageSupport<Product> getSellingProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProdInStoreHouse(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getViolationProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getAuditingProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getAuditFailProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProdInDustbin(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getJoinDistProd(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getReportForbit(String curPageNO, String userName, Product product);

	public abstract PageSupport<Product> getProductList(String curPageNO, int pageSize, Long shopId, Product product);

	public abstract PageSupport<Product> getSeckillLayer(String curPageNO, Long shopId, int pageSize, Product product);

	public abstract PageSupport<Product> getProductLists(String curPageNO, int pageSize, Long shopId, Product product);

	public abstract PageSupport<Product> getStoreProdcts(Long shopId, String curPageNO, Long fireCat, Long secondCat, Long thirdCat, String keyword, String order);

	public abstract PageSupport<Product> getAddProdSkuLayoutPage(String curPageNO, Product product, Long shopId);

	public abstract PageSupport<Product> getAddProdSkuLayout(String curPageNO, Long shopId, Product product);

	public abstract PageSupport<Product> getProductListPage(Product product, String curPageNO, Integer pageSize, DataSortResult result);

	public abstract PageSupport<ProductPurchaseRateDto> getPurchaseRate(String curPageNO, Long shopId, String prodName, DataSortResult result,Date startDate,Date endDate);

	public abstract PageSupport<Product> getLoadSelectProd(String curPageNO, Product product);

	public abstract PageSupport<SalesRankDto> getProductList(String curPageNO, Long shopId, String prodName, Date startDate, Date endDate);
	
	public abstract PageSupport<Product> getProductListMobile(String curPageNO, Long brandId);

	public abstract int upStocks(Long prodId, Long stocks);

	Integer batchOffline(List<Long> prodIdList);


	void batchOnlineLine(List<Long> prodIdList);

	public abstract void updateProdType(Long prodId, String type, Long seckillId);
	
	/** 获取包邮的商品列表 */
	public abstract PageSupport<FullActiveProdDto> getFullActiveProdDto(Long shopId, String curPageNo, String prodName);

	/**
	 * 根据商品名字查询
	 * @param curPageNo
	 * @param prodName
	 * @return
	 */
	public abstract PageSupport<Product> getProdductByName(String curPageNo, String prodName);

	/**
	 * 根据用户查询
	 * @param curPageNo
	 * @param prodName
	 * @param shopId
	 * @return
	 */
	public abstract PageSupport<Product> getProdductByShopId(String curPageNo, String prodName,Long shopId);

	public abstract List<MergeGroupSelProdDto> queryMergeGroupProductToSku(Long shopId, Long[] prodIds);

	public abstract PageSupport<Product> queryMergeGroupProduct(Long shopId);

	public abstract PageSupport<Product> queryMergeGroupProductList(Long shopId, String curPageNO, String prodName);

	/**
	 * 获取拼团商品
	 * @param id
	 * @return
	 */
	public abstract List<MergeGroupSelProdDto> queryMergeGroupProductByMergeId(Long id);

	/**
	 *获取预售商品
	 * @param id
	 * @return
	 */
	public abstract List<PresellSkuDto> queryPresellProductByPresellId(Long id);

	/**
	 * 获取商品DTO(含SKU), 给拼团详情用的
	 * @param prodId
	 * @param mergeGroupId
	 * @return
	 */
	public abstract ProductDto getProductDtoForMergrGroup(Long prodId, Long mergeGroupId);

	
	/**
	 * 检查三级分类有无被使用
	 */
	public abstract boolean checkShopSubCategoryUse(Long subCatId);

	public abstract boolean checkShopCategoryUse(Long shopCatId);

	public abstract boolean checkShopNextCategoryUse(Long nextCatId);

	/**
	 *更新库存
	 */
	public abstract String updateStock(String shopName, Long skuId, Long stocks, Long prodId);

	public abstract Boolean isAttendActivity(Long prodId);

	/**
	 * 获取简单的商品信息数据
	 */
	public abstract List<Select2Dto> getProductSelect2(String q, Integer currPage, Integer pageSize);

	/**
	 * 获取简单的商品信息数据
	 */
	public abstract int getProductSelect2Count(String q);

	/**查询优惠券相关接口
	 *
	 */
	public abstract PageSupport<Product> getCouponProds(Long shopId, List<Long> prodIds, String curPageNO, String orders, String prodName);

	/**
	 * 查询店铺包相关商品
	 * @param shopIds
	 * @param curPageNo
	 * @param orders
	 * @return
	 */
	PageSupport<Product> queryRedpackProd(List<Long> shopIds, String curPageNo, String orders);
	
	/**
	 * 根据shopId获取商家所有商品
	 */
	public abstract List<Product> findAllProduct(Long shopId);
	
	public abstract int updateProdList(List<Product> list);

	/**
	 * 构建参数组
	 * @param prodId
	 * @param parameter
	 * @param userParameter
	 * @return
	 */
	List<ParamGroupDto> buildParamGroups(Long prodId, String parameter, String userParameter);

	/**
	 * 根据用户获取商品id集合
	 * @param shopId
	 * @return
	 */
	List<Long> getProdductIdByShopId(Long shopId);

	/**
	 * 根据属性id获取商品
	 * @param propId
	 * @return
	 */
	List<Product> findProdByPropId(Long propId);

	/**
	 * 通过团购id获取商品
	 * @param id
	 * @return
	 */
	List<GroupSelProdDto> queryGroupProductByGroupId(Long id);

	/**
	 * 修改商家自定义参数
	 */
	void updateProdUserParam(ProdUserParam prodUserParam);

	/**
	 * 修改商品的权重
	 * @param prodId
	 * @param searchWeight
	 */
	Boolean updateSearchWeight(Long prodId, Double searchWeight);

	List<Long> getAllprodIdStr(Long shopId,Integer status);

	List<Product> queryProductByProdIds(List<Long> prodIds);

	/**
	 * 根据商品id获取商品服务说明
	 * @param prodId
	 */
	String getServerShowById(Long prodId);

}