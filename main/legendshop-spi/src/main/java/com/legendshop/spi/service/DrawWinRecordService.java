/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.draw.DrawWinRecord;

/**
 * 中奖纪录服务.
 */
public interface DrawWinRecordService  {

    public DrawWinRecord getDrawWinRecord(Long id);
    
    public void deleteDrawWinRecord(DrawWinRecord drawWinRecord);
    
    public boolean saveDrawWinRecord(DrawWinRecord drawWinRecord,Long awardsRecordId);

    public void updateDrawWinRecord(DrawWinRecord drawWinRecord);

    public List<DrawWinRecord> getDrawWinRecordsByActId(Long actId);
    
    public List<DrawWinRecord> getDrawWinRecords(String userId,Long actId);

	public PageSupport<DrawWinRecord> queryDrawWinRecordPage(String curPageNO, DrawWinRecord drawWinRecord);
    
}
