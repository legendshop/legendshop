/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;
import java.util.Map;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.BasketDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.Sub;

/**
 * 单品SKU服务.
 */
public interface SkuService  {

    public Sku getSku(Long id);
    
    public void deleteSku(Sku sku);
    
    public Long saveSku(Sku sku);

    public void updateSku(Sku sku);

	public List<Sku> getSkuByProd(Long prodId);
	
	public Sku getSkuByProd(Long prodId,Long skuId);
	
	public String getSkuByProd(Sku sku);
	
	public List<KeyValueEntity> getSkuImg(String skuProp);

	List<ProductPropertyDto> getPropValListByProd(ProductDto prodDto,List<Sku> productSkus,Map<Long,List<String>> valueImagesMap);

	List<Sku> getSku(Long[] skuId);

	/**
	 * 查询购物车商品信息
	 * @param prodId
	 * @return
	 */
	BasketDto findBasketDtos(Long prodId, Long skuId);

	public PageSupport<Sku> getSkuPage(String curPageNO, Sku sku);

	/**
	 * 获取该商品未参加营销活动的sku
	 * @param curPageNO
	 * @param prodId
	 * @return
	 */
	public PageSupport<Sku> getAuctionProdSku(String curPageNO, Long prodId);

	public PageSupport<Sku> getSku(String curPageNO, Long prodId, int status, int stocks);

	public PageSupport<Sku> getseckillProdSku(String curPageNO, Long prodId);

	public PageSupport<Sku> getSku(String curPageNO, Long prodId, int status);
	
	public PageSupport<Sku> getloadProdSkuPage(String curPageNO, Long prodId);

	public PageSupport<Sku> getLoadProdSkuByProdId(String curPageNO, Long prodId);
	
	/**
	 * 搜索Sku
	 * @param curPageNO
	 * @param prodId
	 * @param name
	 * @return
	 */
	public PageSupport<Sku> loadSkuListPage(String curPageNO, Long prodId, String name);

	/** 根据规格id查找对应的sku */
	public List<Sku> findSkuByPropId(Long propId);

	public List<Sku> findSkuByProp(String prop);

	public List<Sku> findSkuByPropIdAndProdTypeId(Long propId,Long prodTypeId);
	
	public List<Sku> getSkuForMergeGroup(Long prodId, Long mergeGroupId);

	/**
	 * 根据 productId 查找sku
	 * @param productId
	 * @return
	 */
	public List<Sku> getSkuListByProdId(Long productId);
	
	/**
	 * 根据 skuId 查找sku
	 * @param skuId
	 */
	public Sku getSkuById(Long skuId);

	/**
	 * 判断商品是否可修改，正在参加营销活动不能修改
	 * @param prodId
	 * @return
	 */
	public String isProdChange(Long prodId);
}
