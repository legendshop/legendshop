/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassArticleLable;

/**
 * The Class GrassArticleLableService.
 * 种草社区文章关联标签表服务接口
 */
public interface GrassArticleLableService  {

   	/**
	 *  根据Id获取种草社区文章关联标签表
	 */
    public GrassArticleLable getGrassArticleLable(Long id);

    /**
	 *  根据Id删除种草社区文章关联标签表
	 */
    public int deleteGrassArticleLable(Long id);
    
    /**
   	 *  根据文章id删除种草文章关联标签表
   	 */
    public void deleteByGrassId(Long graid);
    
    /**
	 *  根据对象删除种草社区文章关联标签表
	 */
    public int deleteGrassArticleLable(GrassArticleLable grassArticleLable);
    
   /**
	 *  保存种草社区文章关联标签表
	 */	    
    public Long saveGrassArticleLable(GrassArticleLable grassArticleLable);

   /**
	 *  更新种草社区文章关联标签表
	 */	
    public void updateGrassArticleLable(GrassArticleLable grassArticleLable);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassArticleLable> queryGrassArticleLable(String curPageNO,Integer pageSize);
    
}
