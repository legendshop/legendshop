/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ExpensesRecord;

/**
 * 消费记录服务.
 */
public interface  ExpensesRecordService{

    public ExpensesRecord getExpensesRecord(Long id);
    
    public void deleteExpensesRecord(ExpensesRecord expensesRecord);
    
    public boolean deleteExpensesRecord(String userId,Long id);
    
    public void deleteExpensesRecord(String userId,String ids);
    
    public Long saveExpensesRecord(ExpensesRecord expensesRecord);

    public void updateExpensesRecord(ExpensesRecord expensesRecord);

	public PageSupport<ExpensesRecord> getExpensesRecordPage(String curPageNO, String userId);

	public PageSupport<ExpensesRecord> getExpensesRecord(String curPageNO, String userId, int pageSize);
}
