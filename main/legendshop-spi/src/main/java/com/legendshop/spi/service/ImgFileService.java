/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.Product;

/**
 * 产品图片服务.
 */
public interface ImgFileService {

	public abstract ImgFile getImgFileById(Long id);

	public abstract Product getProd(Long id);

	public abstract void delete(Long id);

	public abstract Long save(ImgFile imgFile);

	public abstract void update(ImgFile imgFile);

	/**
	 * 得到有效的产品的描述图片.
	 *
	 */
	public abstract List<ImgFile> getProductPics(final Long prodId);
	
	public abstract List<String> queryProductPics(final Long prodId);

	public abstract List<ImgFile> getAllProductPics(Long prodId);

	public abstract PageSupport<ImgFile> getImgFileList(String curPageNO, Long productId, ImgFile imgFile);

	public abstract Long saveImgFile(String userName, String userId, Long shopId, ImgFile imgFile,String filePath);

}