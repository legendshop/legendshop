/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.MarketingProds;

/**
 * 参加营销活动的商品服务.
 */
public interface MarketingProdsService  {

    public List<MarketingProds> getMarketingProdsByMarketId(Long marketId);

    public MarketingProds getMarketingProds(Long id);
    
    public void deleteMarketingProds(MarketingProds marketingProds);
    
    public Long saveMarketingProds(MarketingProds marketingProds);

    public void updateMarketingProds(MarketingProds marketingProds);

	PageSupport<MarketingProdDto> findMarketingRuleProds(String curPage, Long shopId, Long marketingId);

	
	/**
	 * 查询商品信息
	 */
	public PageSupport<MarketingProdDto> findMarketingProds(String curPage, Long shopId, String name);

	
	/**
	 * 添加满减满折促销的商品
	 */
	public String addMarketingRuleProds(Long id, Integer type,Long shopId, Long prodId, Long skuId, Double cash);

	
	/**
	 * 删除促销活动的商品
	 */
	public String deleteMarketingRuleProds(Long id, Long shopId);

	/**
	 * 添加限时折扣促销的商品
	 */
	public String addZkMarketingRuleProds(Long id,Long shopId, Long prodId, Long skuId, Double cash, Double discountPrice);

	public void clearGlobalMarketCache(Long shopId);

	public void clearProdMarketCache(Long shopId, Long prodId);

	public void clearProdMarketCache();

	public void deleteMarketingProds(Long marketingId, Long shopId, MarketingProds prods);

	/**
	 * 检查当前商品sku是否参加了其他促销活动
	 */
	public String iSHaveMarketing(Long prodId, Long skuId);

	/**
	 * 根据促销活动Id 释放活动下的商品
	 */
	public int deleteByMarketId(Long marketId);

	/**
	 * 根据活动id获取商品
	 * @param id
	 * @return
	 */
    List<MarketingProdDto> getMarketingProdsDtoByMarketId(Long id);
}
