/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GrassLabel;

/**
 * The Class GrassLabelService.
 * 种草社区标签表服务接口
 */
public interface GrassLabelService  {

   	/**
	 *  根据Id获取种草社区标签表
	 */
    public GrassLabel getGrassLabel(Long id);

    /**
	 *  根据Id删除种草社区标签表
	 */
    public int deleteGrassLabel(Long id);
    
    /**
	 *  根据对象删除种草社区标签表
	 */
    public int deleteGrassLabel(GrassLabel grassLabel);
    
   /**
	 *  保存种草社区标签表
	 */	    
    public Long saveGrassLabel(GrassLabel grassLabel);

   /**
	 *  更新种草社区标签表
	 */	
    public void updateGrassLabel(GrassLabel grassLabel);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GrassLabel> queryGrassLabel(String curPageNO,Integer pageSize);
    
    
    public PageSupport<GrassLabel> queryGrassLabelPage(String curPageNO,String name,Integer pageSize);
    
    public PageSupport<GrassLabel> getGrassLabelByLabel(String curPageNo,GrassLabel grassLabel,Integer PageSize);

    /**
     *增加标签的引用次数
     * @param lid
     */
    public abstract void addRefNum(Long lid);
}
