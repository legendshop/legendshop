/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopNavigation;

/**
 *
 * 商城导航服务
 *
 */
public interface ShopNavigationService  {

    public List<ShopNavigation> getShopNavigationByShopId(Long shopId);

    public ShopNavigation getShopNavigation(Long id);
    
    public void deleteShopNavigation(ShopNavigation shopNavigation);
    
    public Long saveShopNavigation(ShopNavigation shopNavigation);

    public void updateShopNavigation(ShopNavigation shopNavigation);

	public PageSupport<ShopNavigation> getShopNavigationPage(String curPageNO, ShopNavigation shopNavigation);
}
