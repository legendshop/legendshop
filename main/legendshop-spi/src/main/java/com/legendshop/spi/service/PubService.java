/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Pub;

/**
 * 公告服务，分为买家公告和卖家公告.
 */
public interface PubService{

	public abstract Pub getPubById(Long id);

	public abstract void deletePub(Pub pub);

	public abstract Long save(Pub pub);

	public abstract void update(Pub pub);

	public abstract List<Pub> getPub(String userName, Long id, boolean isUser);

	/** 根据 shopId 查找 最新公告 **/
	public abstract List<Pub> queryPubByShopId();

	/**
	 * 获取 公告  *.
	 *
	 * @return the list< pub>
	 */
	public abstract List<Pub> queryShopPub();
	
	/** 获取买家公告 **/
	public abstract List<Pub> queryUserPub();

	public  List<Pub> getPubByShopId();

	public abstract PageSupport<Pub> getPubListPage(String curPageNO, Pub pub, DataSortResult result);

}