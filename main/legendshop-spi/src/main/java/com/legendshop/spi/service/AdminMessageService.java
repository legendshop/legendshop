/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.dto.AdminMessageDtoList;

/**
 * 后台消息的服务器类.
 */
public interface AdminMessageService  {

    public AdminMessageDtoList getMessage();

}
