/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.DeliveryCorp;

/**
 * 物流公司.
 */
public interface DeliveryCorpService {

	public List<DeliveryCorp> getDeliveryCorpByShop();

	public DeliveryCorp getDeliveryCorp(Long id);

	public void deleteDeliveryCorp(DeliveryCorp deliveryCorp);

	public Long saveDeliveryCorp(DeliveryCorp deliveryCorp);

	public void updateDeliveryCorp(DeliveryCorp deliveryCorp);

	/**
	 * 根据配送方式ID 查询物流公司的信息
	 * @param dvyId
	 * @return
	 */
	public DeliveryCorp getDeliveryCorpByDvyId(Long dvyId);

	public PageSupport<DeliveryCorp> getDeliveryCorpPage(String curPageNO, DeliveryCorp deliveryCorp);
	
	public List<DeliveryCorp> getAllDeliveryCorp();
}
