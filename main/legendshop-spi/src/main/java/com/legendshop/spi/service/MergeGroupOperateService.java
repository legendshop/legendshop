/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.entity.MergeGroupOperate;

/**
 * The Class MergeGroupOperateService.
 * 每个团的运营信息服务接口
 */
public interface MergeGroupOperateService  {

	/**
	 *  根据Id获取每个团的运营信息
	 */
	public MergeGroupOperate getMergeGroupOperate(Long id);

	/**
	 *  删除每个团的运营信息
	 */    
	public void deleteMergeGroupOperate(MergeGroupOperate mergeGroupOperate);

	/**
	 *  保存每个团的运营信息
	 */	    
	public Long saveMergeGroupOperate(MergeGroupOperate mergeGroupOperate);

	/**
	 *  更新每个团的运营信息
	 */	
	public void updateMergeGroupOperate(MergeGroupOperate mergeGroupOperate);

	//获取团运营信息，用于校检人数是否满团
	public MergeGroupJoinDto getMergeGroupOperateByAddNumber(Long activeId, String addNumber);

	//校检用户是否参加了该团
	public Long getMergeGroupAddByUserId(String userId, String addNumber);

	public MergeGroupOperate getMergeGroupOperateByAddNumber(String addNumber);

	public List<MergeGroupOperate> findHaveInMergeGroupOperate();

	public List<MergeGroupOperate> findHaveInMergeGroupOperate(Long mergeId);

}
