/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.buy.UserShopCartList;


/**
 * 配送方式管理.
 *
 */
public interface TransportManagerService {

	/**
	 * 用于计算订单购物车的运费规则
	 * @param userShopCartList
	 */
	public void clacCartDeleivery(final UserShopCartList userShopCartList, boolean isCouponBack);
	
	
	/**
	 * 用于计算商品的运费
	 * @param transType 配送方式[ems ,mail ....]
	 * @param transfee 配送规则实体
	 * @param totalWeight 总重量
	 * @param totalVolume 总体积
	 * @param totalCount 总数
	 * @return
	 */
	public List<TransfeeDto> getTransfeeDtos(Long shopId,Long transportId, Integer cityId ,double totalCount,
			 double totalWeight,
			 double totalvolume);
}
