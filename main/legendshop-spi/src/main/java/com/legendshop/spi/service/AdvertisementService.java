/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Advertisement;
/**
 * 
 * 广告服务
 *
 */
public interface AdvertisementService{

	public abstract Advertisement getAdvertisement(Long id);

	public abstract boolean isMaxNum(Long shopId, String type);

	public abstract void delete(Long id);

	public abstract Long save(Advertisement advertisement);

	public abstract void update(Advertisement advertisement);

	public abstract String updateAdvt(Advertisement advertisement,Advertisement orin,String originPic,String picUrl,String userName, String userId, Long shopId);

	public abstract String saveAdvt(Advertisement advertisement,String pic, String userName, String userId, Long shopId);

	public abstract PageSupport<Advertisement> queryAdvertisement(String curPageNO, Advertisement advertisement);

	public abstract void delete(Long id, Advertisement advertisement);

	public abstract void saveOrUpdate(String userName, String userId, Long shopId, Advertisement advertisement,String picUrl);

}