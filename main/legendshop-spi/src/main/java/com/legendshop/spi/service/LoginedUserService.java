/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.dto.user.LoginedUserInfo;

/**
 * 获取已经登录的用户信息, 手动获取用户信息看AppTokenUtil
 *
 */
public interface LoginedUserService {
	
	/**
	  *  获取用户信息
	 */
	public LoginedUserInfo getUser();
	
}
