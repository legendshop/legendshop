package com.legendshop.spi.service;

import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.entity.User;

/**
 * 第三方登录服务
 * @author 开发很忙
 */
public interface ThirdLoginService {

	/**
	 * 认证第三方用户
	 * @param thridUserInfo 第三方用户授权认证后的用户数据
	 * @param passportType
	 * @param passportSource
	 * @return
	 */
	ThirdUserAuthorizeResult authThridUser(ThirdUserInfo thridUserInfo, String passportType, String passportSource);

	/**
	 * 通行证绑定手机号用户
	 * @param user TODO
	 * @param passport 通行证
	 * @return
	 */
	String bindAccount(User user, PassportFull passport);
	
	/**
	 * 缓存通行证ID到redis(有效时间10分钟), 返回存放的key; 
	 * 用户通过这个key如果能拿到 通行证ID, 则可以进行下一步的登录或绑定手机
	 * @param passportId 需要缓存的passportId
	 * @param appSecret 密钥, 通常是第三方应用的appSecret
	 * @return
	 */
	String cachePassportID(Long passportId);
	
	/**
	 * 从缓存中获取通行证ID
	 * @param key
	 * @return
	 */
	Long getPassportIDFromCache(String key);

	/**
	 * 从缓存中移除通行证ID
	 * @param key
	 */
	void removePassportIDFromCache(String key);

	PassportFull checkPassportIdKey(String passportKey);
}
