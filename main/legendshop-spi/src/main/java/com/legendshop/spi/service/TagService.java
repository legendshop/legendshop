/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.NewsTagDto;
import com.legendshop.model.dto.TagNewsDto;
import com.legendshop.model.entity.Tag;
import com.legendshop.model.entity.TagItems;

/**
 * 标签服务
 */
public interface TagService {

	/**
	 * Gets the tag.
	 * 
	 * @param id
	 *            the id
	 * @return the tag
	 */
	public Tag getTag(Long id);

	/**
	 * Delete tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public void deleteTag(Tag tag);

	/**
	 * Save tag.
	 * 
	 * @param tag
	 *            the tag
	 * @return the long
	 */
	public void saveTag(Tag tag);

	/**
	 * Update tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public void updateTag(Tag tag);


	/**
	 * Gets the tag.
	 * 
	 * @param name
	 *            the name
	 * @param userName
	 *            the user name
	 * @return the tag
	 */
	public Tag getTag(String name, String userName);

	public List<Tag> getPageTag(Long shopId, String page);

	public List<TagItems> queryTagItemsList(List<Tag> list);
	
	public List<TagItems> queryTagItemsListByType(String type);

	public TagItems getItemsByTagId(Long id);

	public void deleteTagItems(TagItems tagItems);

	public TagItems getTagItems(Long itemId);

	public void saveTagItems(TagItems tagItems);

	public void deleteTagItems(Long itemId);

	public List<TagItems> getTagItems(String string);
	
	public List<NewsTagDto> getNewsTagDto(String type);
	
	public List<TagNewsDto> getTagNewsDto(String type);
	
	public Long getTagNewsDtoCount(String type);
	
	public void saveTagItemsNews(Long newsid,Long itemid);
	
	public void delTagItemsNews(Long newsid,Long itemid);

	public PageSupport<Tag> getTagPage(String curPageNO, Tag tag);

	public void deleteTag(Tag tag, TagItems tagItems);
}
