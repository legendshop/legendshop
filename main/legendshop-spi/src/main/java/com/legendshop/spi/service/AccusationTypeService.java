/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationSubject;
import com.legendshop.model.entity.AccusationType;

/**
 * 举报类型服务.
 */
public interface AccusationTypeService  {

    AccusationType getAccusationType(Long id);
    
    void deleteAccusationType(AccusationType accusationType);
    
    Long saveAccusationType(AccusationType accusationType);

    void updateAccusationType(AccusationType accusationType);

    List<AccusationSubject> getAllAccusationSubject();

	PageSupport<AccusationType> getAccusationType(String curPageNO, AccusationType accusationType);
	
	
	/**
	 * 根据与举报类型 获取关联的举报主题
	 * @param typeId 举报类型id
	 * @return
	 */
	List<AccusationSubject> getSubjectByTypeId(Long typeId);
}
