package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.entity.ProdArrivalInform;

/**
 * 商品到货通知
 */
public interface ProductArrivalInformService {

	public abstract Long saveProdArriInfo(ProdArrivalInform prodArrivalInform);
	
	public abstract ProdArrivalInform getAlreadySaveUser(String userId,long skuId,int status);
	
	/**
	 * 根据skuId 查看设置了到货通知的用户
	 */
	public abstract List<ProdArrivalInform> getUserBySkuIdAndWhId(long skuId);
	
	/**
	 * 批量更新到货通知表
	 */
	public int updateArrivalInform(List<ProdArrivalInform> arrivalInformList);

	public abstract PageSupport<ProdArrivalInformDto> getProdArrival(Long shopId, String productName, String curPageNO);

	public abstract PageSupport<ProdArrivalInformDto> getSelectArrival(String curPageNO, String skuId, Long shopId);
}
