/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.InvoiceSub;

/**
 * 发票订单信息表服务.
 */
public interface InvoiceSubService  {

    public InvoiceSub getInvoiceSub(Long id);
    
    public void deleteInvoiceSub(InvoiceSub invoiceSub);
    
    public Long saveInvoiceSub(InvoiceSub invoiceSub);

    public void updateInvoiceSub(InvoiceSub invoiceSub);

}
