/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;

/**
 *微信素材服务
 */
public interface WeixinNewstemplateService  {

    public WeixinNewstemplate getWeixinNewstemplate(Long id);
    
    public void deleteWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);
    
    public Long saveWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);

    public void updateWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);

	public PageSupport<WeixinNewstemplate> getWeixinNewstemplate(String curPageNO, WeixinNewstemplate weixinNewstemplate, int pageSize);
}
