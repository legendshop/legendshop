/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

/**
 * 公共角色权限服务.
 */
public interface CommonUtil {

	/**
	 * 保存后台管理员角色
	 * 
	 * @param userId 用户Id
	 */
	public abstract void saveAdminRight(String userId);


	/**
	 * 保存用户角色
	 *
	 * @param userId 用户Id
	 */
	public abstract void saveUserRight(String userId);

	/**
	 * 删除管理员角色.
	 * 
	 * @param userId 管理员Id
	 */
	public abstract void removeAdminRight(String userId);

	/**
	 * 删除用户角色.
	 * 
	 * @param userId 用户角色
	 */
	public abstract void removeUserRight(String userId);

}