/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ProdTypeParam;

/**
 * 商品类型跟参数的关系表
 */
public interface ProdTypeParamService  {

	ProdTypeParam getProdTypeParam(Long propId, Long prodTypeId);
	
	Long saveProdTypeParam(ProdTypeParam prodTypeParam);
	
	void updateProdTypeParam(ProdTypeParam prodTypeParam);
	
	ProdTypeParam getProdTypeParam(Long id);
}
