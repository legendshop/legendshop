/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;

/**
 *微信菜单扩展服务
 */
public interface WeixinMenuExpandConfigService  {

    public WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id);
    
    public void deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
    
    public Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);

    public void updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);

	public PageSupport<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String curPageNO,
			WeixinMenuExpandConfig weixinMenuExpandConfig);
}
