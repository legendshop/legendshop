/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.biz.model.dto.AppQueryProductCommentsParams;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.model.form.ProductForm;

import java.util.List;
import java.util.Map;

/**
 * 产品评论服务
 */
public interface ProductCommentService {

	/**
	 * Load.
	 * 
	 * @param id
	 *            the id
	 * @return the product comment
	 */
	public abstract ProductComment getProductCommentById(Long id);

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 */
	public abstract void delete(Long id);

	/**
	 * Save.
	 * 
	 * @param productComment
	 *            the product comment
	 * @return the long
	 */
	public abstract Long save(ProductComment productComment);

	/**
	 * Update.
	 * 
	 * @param productComment
	 *            the product comment
	 */
	public abstract void update(ProductComment productComment);


	/**
	 * Inits the product comment category.
	 * 
	 * @param prodId
	 *            the prod id
	 * @return the product comment category
	 */
	public abstract ProductCommentCategory initProductCommentCategory(Long prodId);
	
	/**
	 * Validate comment.
	 * 
	 * @param prodId
	 *            the prod id
	 * @param userName
	 *            the user name
	 * @return the string
	 */
	public abstract String validateComment(Long prodId, String userName);
	
	
	public abstract String getproductName(Long prodId);
	
	/**
	 * 收藏此商品的用户，还收藏其他什么商品
	 */
	public abstract List<ProductForm> getProductList(String userId, Long prodId);

	public abstract void delete(List<Long> idList);
	
	/**
	 * 根据subItemId，查出ProductComment
	 */
	public abstract ProductComment  getProdCommentByprodId(Long subItemId);
	
	/**
	 * 更新有用计数
	 */
	public abstract int updateUsefulCounts(Long prodComId,String userId);

	/**
	 * 查看用户是否可以评论此商品
	 */
	public abstract boolean canCommentThisProd(Long prodId, Long subItemId, String userId);
	
	public abstract boolean commentTimeout(Long subItemId);

	public abstract PageSupport<ProductCommentDto> queryProductComment(ProductCommentsQueryParams params);

	public abstract void setGoodComments(List<Product> productList);

	/**
	 * 保存商品品论
	 */
	public abstract boolean saveProductComment(ProductComment productComment);
	
	/**
	 * 处理匿名评论
	 */
	public abstract Map<String, Object> handleAnonymousComments(String userName, boolean isAnonymous);

	/**
	 * 判断是否能追加评论
	 */
	public abstract String isCanAddComment(Long prodCommId, String userId);

	/**
	 * 追加商品评论
	 */
	public abstract boolean addProdComm(ProdAddComm prodAddComm);

	/**
	 * 用户是否已经点赞过指定评论了
	 */
	public abstract boolean isAlreadyUseful(Long prodComId, String userId);

	public abstract ProductComment getProductComment(Long commId, String userId);

	public abstract ProductCommentDto getProductCommentDetail(Long prodComId,String userId);

	public abstract UserCommentDto getUserCommentDto(Long prodId, Long subItemId, String userId);

	public abstract UserAddCommentDto getUserAddCommentDto(Long prodCommId, String userId);

	/**
	 * 审核初次评论
	 */
	public abstract void auditFirstComment(ProductComment productComment, Integer auditStatus);

	PageSupport<com.legendshop.model.entity.ProductReply> replyCommentList(String curPageNO, Long parentReplyId);

	PageSupport<ProductComment> prodComment(String curPageNO, Long subId, String userName);

	PageSupport<ProductComment> prodCommentState(String curPageNO, String state, String userName);

	public abstract PageSupport<ProductComment> query(String curPageNO, String userName, Long subId);

	public abstract PageSupport<ProductComment> query(String curPageNO, String userName);
	
	public abstract PageSupport<ShopCommProdDto> queryProdComment(String curPageNO, Long shopId);

	public abstract PageSupport<ProductCommentDto> queryProductCommentDto(ProductCommentsQueryParams params);

	public abstract PageSupport<ProductCommentDto> getProductCommentList(String curPageNO,
			ProductComment productComment, Integer pageSize, String status, DataSortResult result);

	public abstract PageSupport<ProductCommentDto> getProductCommentListPage(String curPageNO,
			ProductComment productComment, String userId, String userName, Integer pageSize, DataSortResult result);

	public abstract PageSupport getProdCommentList(Long shopId, String curPageNO, int pageSize);

	public abstract PageSupport getProdComment(AppQueryProductCommentsParams params, int pageSize);

	public abstract  PageSupport<UserCommentProdDto> queryComments(String userName, String curPageNO, int pageSize);

	/**
	 * 根据状态获取用户评论列表
	 */
	PageSupport<ProductCommentDto> queryProdCommentByState(String curPageNO, String state, String userName);

	/**
	 * 更新评论统计表
	 * @param productComment
	 */
	void updateCommentStatInfo(ProductComment productComment);

	/**
	 * 获取用户是否评论过商品
	 * @param prodId
	 * @param userId
	 * @return
	 */
	Boolean findByProdIdandUserIdandItemId(Long prodId, String userId,Long itemId);
}