/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.SubSettlementItem;

/**
 * 订单结算票据中心
 *
 */
public interface SubSettlementItemService  {
	
    public List<SubSettlementItem> getSettlementItem(String settlementSn);
    
}
