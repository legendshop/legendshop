/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopBanner;

/**
 * 商家默认的轮换图服务
 *
 */
public interface ShopBannerService  {

    public ShopBanner getShopBanner(Long id);
    
    public void deleteShopBanner(ShopBanner shopBanner);
    
    public Long saveShopBanner(ShopBanner shopBanner);

    public void updateShopBanner(ShopBanner shopBanner);

	public PageSupport<ShopBanner> getShopBanner(String curPageNO, Long shopId);
}
