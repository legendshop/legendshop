/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.IdPassord;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.ThirdUserInfo;
import com.legendshop.model.dto.UserResultDto;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.form.UserForm;

/**
 * 第三方登录的服务.
 */
public interface PassportService  {

    public List<Passport> getPassport(String userId);

    public Passport getPassport(Long id);
    
    public void deletePassport(Passport passport);
    
    public Long savePassport(Passport passport);

    public void updatePassport(Passport passport);
    
    public int updateLastLoginTime(Passport passport);

	public Passport getUserIdByOpenId(String type, String openId);
	
	public Passport getUserIdByOpenIdForUpdate(String type, String openId);
	
	public void deletePassport(String useId,String type);

	/**
	 * 注册并保持通行证
	 */
	public UserResultDto savePassportAndReg(String ip, UserForm userForm, Passport passport, String encodedPassword);

	/**
	 * 账户是否已经绑定
	 */
	public boolean isAccountBind(String userName, String type);
	
	//绑定用户
	public int bindingUser(Passport passport);

	/**
	 * 检查用户名（手机）密码是否正确
	 */
	public IdPassord validateMobilePassword(String mobile, String password);

	/**
	 * 是否绑定微信账号
	 */
	public boolean isBindWeixinAccount(String userId);
	
	/**
	 * 解绑微信账号
	 */
	public void unbindWeixinAccount(String userId);

	/**
	 * 判断是否存在通行证
	 * @param userId 用户ID
	 * @param type 通行证类型
	 * @param source 通行证来源
	 * @return
	 */
	public boolean isExistPassport(String userId, String type, String source);

	/**
	 * 根据openId 获取通信证
	 * @param openid
	 * @return
	 */
	public PassportFull getPassportByOpenId(String openid);

	/**
	 * 根据unionid获取Passport
	 * @param unionid
	 * @return
	 */
	public Passport getPassportByUnionid(String unionid);

	/**
	 * 生成通行证
	 * @param thridUserInfo 第三方用户信息
	 * @param passportType 通行证类型
	 * @param passportSource 通行证来源
	 * @param passport 已存的父通行证, unionid级别的
	 * @return
	 */
	public PassportFull generatePassport(ThirdUserInfo thridUserInfo, String passportType,
			String passportSource, Passport passport);

	/**
	 * 生成通行证
	 * @param thridUserInfo
	 * @param passportType
	 * @param passportSource
	 * @return
	 */
	public PassportFull generatePassport(ThirdUserInfo thridUserInfo, String passportType,
			String passportSource);

	/**
	 * 合并通行证
	 * @param passportFull 当前通行证
	 * @param unionid 用于查询已存在的其他通行证
	 * @return
	 */
	public PassportFull mergePassport(PassportFull passportFull, String unionid);

	/**
	 * 更新通信证的信息
	 * @param passportFull
	 */
	public void updatePassportFull(PassportFull passportFull);

	/**
	 * 根据passportId 查询 PassportFull
	 * @param passportId 是passportSub的ID
	 * @return
	 */
	public PassportFull getPassportFullById(Long passportId);

	/**
	 * 绑定用户
	 * @param passPortId
	 * @param userId
	 * @param userName
	 * @return
	 */
	public int bindUser(Long passPortId, String userId, String userName);

	/**
	 * 获取 PassportFull
	 * @param userId
	 * @param type
	 * @param source
	 * @return
	 */
	public PassportFull getPassportFullByUserIdAndTypeAndSource(String userId, String type, String source);
}
