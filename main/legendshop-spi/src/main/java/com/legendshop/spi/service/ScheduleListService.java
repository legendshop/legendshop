package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.ScheduleList;

/**
 * 任务调度服务
 */
public interface ScheduleListService {

	/**
	 * 根据商城获取任务调度服务表列表
	 */
	public  List<ScheduleList> getNonexecutionScheduleList(Date today, Integer value);
	

	/**
	 * 删除任务调度服务表
	 */
	public abstract int deleteScheduleList(ScheduleList ScheduleList);

	/**
	 * 保存任务调度服务表
	 */
	public abstract List<Long> saveScheduleList(List<ScheduleList> scheduleLists);

	/**
	 * 
	 * @param shopId
	 * @return
	 */
	public abstract List<ScheduleList> getScheduleList(Long shopId, String scheduleSn,Integer scheduleType);
	
    /**
	 *  保存任务调度服务表
	*/	    
	public Long saveScheduleList(ScheduleList ScheduleList);

	/**
	 * 更新任务调度状态
	 * @param status
	 * @param id
	 */
	public abstract int updateScheduleListStatus(Long shopId,int status, Long id);

	
}
