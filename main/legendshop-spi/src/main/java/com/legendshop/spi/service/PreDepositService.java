package com.legendshop.spi.service;

import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.PdWithdrawCash;



/**
 * 预付款业务处理Service
 *
 */
public interface PreDepositService {
	
	 /**
	  *  用户简单充值
	  */
	 public String recharge(String userId,String userName, Double amount, String sn, String message);
	     
	
	 /**
	 *  用户提交申请操作
	 */
     public String withdrawalApply(PdWithdrawCash withdrawCash);

     /**
 	 * 提现完成，财务审核结束
 	 * @param pdWithdrawCash 提现对象
 	 */
	 String withdrawalApplyFinally(PdWithdrawCash pdWithdrawCash);

	 /**
		 * 用户取消提现申请
		 * @param sn 用户提现申请的流水号
		 * @param amount 用户提现金额
		 * @param userId 用户ID
		 */
	String withdrawalApplyCancel(String sn, Double amount, String userId);


	/**
	 * 充值 -- 在线支付成功之后
	 */
	String rechargeForPaySucceed(PdRecharge pdRecharge);
	
	
	/**
	 * 充值---提交充值后，后台管理员操作通过充值
	 *
	 */
	String adminRechargeForPaySucceed(PdRecharge pdRecharge);
	
	/**
	 * 分销佣金收入,充值到预存款
	 * @param userId  用户ID
	 * @param amount  佣金收入金额
	 * @param subItemNumber 订单项流水号
	 */
	String rechargeForDistCommis(String userId, Double amount, String subItemNumber);
	

	/**
	 * 下单 支付宝成功返回前   
	 * @param sn 订单号
	 * @param amount 预付款支付金额
	 * @param userId 用户ID
	 * @param fullpay 是否通过预付款全额支付
	 */
	String beforeOrderPay(String sn, Double amount, String userId,
			boolean fullpay);


	/**
	 * 下单--后支付宝成功返回后
	 * @param sn 订单号
	 * @param amount 预付款支付的订单金额
	 * @param userId 订单的用户Id
	 */
	@Deprecated
	String orderPaySuccess1(String sn, Double amount, String userId);
	
	
	/**
	 * 下单--后支付宝成功返回后
	 * @param sn 订单号
	 * @param amount 预付款支付的订单金额
	 * @param userId 订单的用户Id
	 * @param prePayType 1:预付款  2:金币
	 */
	String orderPaySuccess(String sn, Double amount, String userId,Integer prePayType);
	
	

	/**
	 * 取消订单
	 * @param sn 订单号
	 * @param amount 预付款支付的订单金额
	 * @param userId 订单的用户Id
	 */
	String orderCancel(String sn, Double amount, String userId);

	

	/**
	 * 退款订单
	 * @param sn 退款流水号
	 * @param amount 退款金额
	 * @param userId 订单的用户Id
	 */
	String orderRefund(String sn, Double amount, String userId);


	/**
	 * 冻结操作
	 */
	String freezePredeposit(String userId, double amount, String sn,String type);

	
	/**
	  *  预存款退款
	  */
	 public String refund(String userId,String userName, Double amount, String sn, String message);
	     
}
