package com.legendshop.spi.service;

import com.legendshop.model.entity.FileParameter;

import java.util.List;

public interface FileParameterService {


    FileParameter findByName(String fileName);

    void deleteByName(String fileName);

    void save(FileParameter fileParameter);

    void update(FileParameter fileParameter);

}
