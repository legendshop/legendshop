/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductCoupon;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.vo.UserCouponVo;

/**
 *用户优惠券服务
 */
public interface UserCouponService  {

    public UserCoupon getUserCoupon(Long id);
    
    public UserCoupon getCouponByCouponPwd(String couponPwd);
    
    public UserCoupon getCouponByCouponSn(String couponSn);

    public int getUserCouponCount(String userId,long couponId);
    
    public void deleteUserCoupon(UserCoupon userCoupon);
    
    public Long saveUserCoupon(UserCoupon userCoupon);

    public void updateUserCoupon(UserCoupon userCoupon);

	public String sendUserCnps(String userids, Long couponId);

	List<UserCoupon> getUserCouponByCoupon(Long couponId);

	public boolean saveUserCoupon(List<UserCoupon> userCoupon, long cnps_number, Long couponId);
    
	public abstract List<String> getSendUsers(String sql, Object[] praObjects);

	boolean saveOffUserCoupon(List<UserCoupon> userCoupon, long cnps_number, Long couponId);
	
	
	List<UserCoupon> getOffExportExcelCoupon(Long couponId);
	
	
	public boolean exportExcel (String title, String[] headers,
			List<UserCoupon> dataset, OutputStream out, String pattern);
	
	public void betchUpdate(List<UserCoupon> userCoupon);
	
	void updateUsercouponProperty(UserCoupon userCoupon,String userId, String userName,Coupon cou);

	public List<UserCoupon> getUnUseUserCouponsByCoupon(Long couponId);
	
	void updateCouponBindNum(Coupon cou);

	public UserCoupon getUsercoupon(Long couponId, String userId);
	
	/**
	 * 获取用户有效的 商品优惠券
	 * @param userId
	 * @return
	 */
	public List<ProductCoupon> getProdCouponList(String userId);

	/**
	 * 获取用户有效的 所有优惠券和红包
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> getAllActiveUserCoupons(String userId);

	public List<UserCoupon> queryUnbindCoupon(Long couponId);

	public Integer BindCouponByUser(List<String> result, List<UserCoupon> lists);

	public void saveUserCoupons(List<UserCoupon> userCoupons);

	public PageSupport<UserCoupon> getUserCouponListPage(String curPageNO, Long id);

	public PageSupport<UserCouponVo> getUserCoupon(String curPageNO, StringBuilder sql, StringBuilder sqlCount, List<Object> objects);

	public List<String> getSendUsers(String curPageNO, StringBuilder sql, List<Object> objects);

	public PageSupport<UserCoupon> getUserCouponList(String curPageNO, Long id);

	public PageSupport<UserCoupon> getWatchRedPacket(Long id, String curPageNO);

	/**pc领取优惠卷
	 * @param couponId
	 * @param userName
	 * @return
	 */
	public String couponReceive(Long couponId, String userName, String userId);

	/**mobile领取优惠卷
	 * @param couponId
	 * @param userName
	 * @return
	 */
	public String mobileCouponReceive(Long couponId, String userName,String usreId);
	
	/**
	 * 领取优惠卷(新)
	 * 2018-12-4 日改造，旧的方法也在用，逻辑没有改，只是改变了返回类型
	 * @return
	 */
	public Map<String, Object> newMobileCouponReceive(Coupon coupon, UserDetail userDetail);

	/**
	 * 根据couponSn查询用户优惠券
	 * @param couponSn
	 * @return
	 */
	public UserCoupon getUserCouponBySn(String couponSn);

	/**
	 * 获取用户优惠券
	 * @param userName
	 * @param couponId
	 * @return
	 */
	public List<UserCoupon> queryUserCouponList(String userName, Long couponId);

	/**
	 * app优惠券领取
	 */
	Map<String, Object> newAppCouponReceive(Coupon c, UserDetail userDetail);

	/**
	 * 获取用户优惠券
	 * @param couponId
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> getUserCoupons(Long couponId, String userId);
	
}
