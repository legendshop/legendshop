/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.PassportSub;

/**
 * The Class PassportSubService.
 * 服务接口
 */
public interface PassportSubService  {

   	/**
	 *  根据Id获取
	 */
    public PassportSub getPassportSub(Long id);

   /**
	 *  删除
	 */    
    public void deletePassportSub(PassportSub passportSub);

   /**
	 *  保存
	 */	    
    public Long savePassportSub(PassportSub passportSub);

   /**
	 *  更新
	 */	
    public void updatePassportSub(PassportSub passportSub);
}
