/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Article;

/**
 * 文章服务接口
 */
public interface ArticleService  {

   	/**
	 *  根据Id获取
	 */
    public Article getArticle(Long id);

   /**
	 *  删除
	 */    
    void deleteArticle(Article article);

   /**
	 *  保存
	 */	    
    Long saveArticle(Article article);

   /**
	 *  更新
	 */	
    void updateArticle(Article article);

	PageSupport<Article> query(String curPageNO);

	PageSupport<Article> getArticleList(String curPageNO);
	
	PageSupport<Article> getArticlePage(String curPageNO, Article article);

	void saveArticle(String userName,String userId, Long shopId, Article article,String pic);
}
