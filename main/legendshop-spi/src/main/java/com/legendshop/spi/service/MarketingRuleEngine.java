package com.legendshop.spi.service;

import com.legendshop.model.dto.buy.ShopCartItem;


/**
 * 营销计算规则引擎
 *
 */
public interface MarketingRuleEngine {

	/**
	 * 
	 * @param item 购物车里的商品
	 */
	public void  executorMarketing(ShopCartItem item); 
	
}
