/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.PdCashLog;

/**
 * 预存款变更日志服务.
 */
public interface PdCashLogService  {

    public PdCashLog getPdCashLog(Long id);

    public void deletePdCashLog(PdCashLog pdCashLog);

    public Long savePdCashLog(PdCashLog pdCashLog);

    public void updatePdCashLog(PdCashLog pdCashLog);

	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, String userId, String userName);

	public PageSupport<PdCashLog> getPdCashLogListPage(String curPageNO, PdCashLog pdCashLog, Integer pageSize, DataSortResult result);

	public PageSupport<PdCashLog> getPdCashLogPage(String userId, String curPageNO);

	/**
	 * 获取用户预存款明细列表
	 * @param userId 用户id
	 * @param state [1:收入，0：支出]
	 * @param curPageNO
	 * @return
	 */
	public PageSupport<PdCashLog> getPdCashLogPageByType(String userId, Integer state, String curPageNO);
	
	/** 查询分销的历史转账 */
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, String uesrId, String logType, int pageSize);

	/** 查询分销的历史转账 */
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, PdCashLog pdCashLog, int pageSize);

	/** 查询分销的历史转账 */
	public PageSupport<PdCashLog> getPdCashLog(String curPageNO, int pageSize, String userId, String logType);

	/** 查询个人提现记录 */
	public PageSupport<PdCashLog> getPresentRecord(String curPageNO, int pageSize, String userId, String logType);

}
