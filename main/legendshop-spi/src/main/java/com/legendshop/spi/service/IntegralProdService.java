/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.integral.IntegralProd;

/**
 * 积分商品服务.
 */
public interface IntegralProdService  {

    public IntegralProd getIntegralProd(Long id);
    
    public void deleteIntegralProd(IntegralProd integralProd);
    
    public Long saveIntegralProd(IntegralProd integralProd,String userName, String userId, Long shopId);

    public void updateIntegralProd(IntegralProd integralProd);

	public void updateIntegralProdStatus(IntegralProd integralProd);

	public List<Category> findIntegralCategory();
	
	public abstract Category findCategory(Long categoryId);

	
	/**
	 * 积分兑换
	 */
	public List<IntegralProd> findExchangeList(Long firstCid, Long twoCid,
			Long thirdCid);

	
	/**
	 * 查看积分的详情信息
	 */
	public IntegralProd findIntegralProd(Long id);
	
	/**
	 * 获取不同提供方的热门优惠券
	 * @param yType 领取类型
	 * @param pType 提供方
	 */
	public List<Coupon> getCouponByProvider(String yType,String pType,Date date);

	public Coupon getCoupon(Long cid);

	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO);

	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid);

	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, Long firstCid, Long twoCid, Long thirdCid,
			Integer startIntegral, Integer endIntegral, String integralKeyWord, String orders);

	public PageSupport<IntegralProd> getIntegralProdPage(String curPageNO, IntegralProd integralProd,
			DataSortResult result);

	public Integer batchDelete(String ids);

	public Integer batchOffline(String ids);

	
}
