/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.model.entity.ShopOrderBill;

/**
 * 预售订单服务
 */
public interface PresellSubService {


	/** 删除预售订单对象 */
	public void deletePresellSub(PresellSub presellSub);

	/** 保存预售订单对象 */
	public Long savePresellSub(PresellSub presellSub);

	/** 更新预售订单对象 */
	public void updatePresellSub(PresellSub presellSub);

	/** 获取预售订单对象 */
	public PresellSub getPresellSub(String userId, String subNumber);

	//检查该商品是否参见过预售
	public boolean findIfJoinPresell(Long productId);

	/**
	 * 获取预售订单
	 * @param presellId
	 * @return
	 */
	public PresellSub getPresellSubByPresell(String presellId);

	/**
	 * 根据订单号获取订单
	 * @param subNumber
	 * @return
	 */
	public PresellSub getPresellSub(String subNumber);

	/**
	 * 根据结算单号获取当期结算的预售订单列表
	 * @param curPageNO
	 * @param shopId
	 * @param subNumber
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<PresellSub> queryPresellSubList(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill);
}
