/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.model.dto.ShopCategoryDto;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.ShopCategory;

import java.util.List;
import java.util.Set;

/**
 *商家的商品类目服务
 */
public interface ShopCategoryService  {

    public ShopCategory getShopCategory(Long id);
    
    public void deleteShopCategory(ShopCategory shopCategory);
    
    public Long saveShopCategory(ShopCategory shopCategory,String userName ,String userId, Long shopId);

	/**
	 * app 商家端保存店铺分类
	 * @param grade
	 * @param shopId
	 * @return
	 */
	public Long saveShopCategory( Integer grade, Long shopId ,String name,Long parentId );

    public void updateShopCategory(ShopCategory shopCategory);

    public int deleteShopCategory(Long id);

    /** 根据  List<ShopCategory> 来查取  第三级 类目 **/
	public List<ShopCategory> subCatQuery(List<ShopCategory> nextCatList);
	
	/** 根据id 查找有无三级分类，有则一并删除 **/
	public void deleteNextCategory(Long nextCatId);

	/** 根据id 查找有无二级分类 **/
	public List<ShopCategory> queryByParentId(Long shopCatId);

	/** 用于填充一级商品分类的select options **/
	public List<KeyValueEntity> loadCat(Long shopId);

	/** 用于填充二级商品分类的select options **/
	public List<KeyValueEntity> loadNextCat(Long shopCatId, Long shopId);

	public Set<CategoryDto> getAllCategoryByType(Long shopId, String name);

	public Long getNextCategoryCount(Long shopCatId);

	public PageSupport<ShopCategory> queryShopCategory(String curPageNO, ShopCategory shopCategory, Long shopId);

	public PageSupport<ShopCategory> getNextShopCat(String curPageNO, ShopCategoryDto shopCatDto);

	public PageSupport<ShopCategory> getSubShopCat(String curPageNO, ShopCategoryDto shopCatDto);
	
	/** 获取商家类目*/
	public List<ShopCategory> getFirstShopCategory(Long shopId);

	/**
	 * 获取店铺分类
	 */
	public List<ShopCategory> getShopCategoryList(Long shopId);

	
	/** 获取店铺分类列表  */
	public List<DecorateShopCategoryDto> getShopCategoryDtoList(Long shopId);

	String getShopCategoryName(Long shopFirstCatId);

	/**
	 * 手动清除缓存
	 * @param shopId
	 */
	void expireShopCategory(Long shopId);

	/**
	 * 清楚装修商家类目缓存
	 * @param shopId
	 */
	void expireShopCategoryIndex(Long shopId);
}
