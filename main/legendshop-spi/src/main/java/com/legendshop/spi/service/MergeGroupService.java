/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.MergeProduct;

/**
 * The Class MergeGroupService. 拼团活动服务接口
 */
public interface MergeGroupService {

	/**
	 * 已开始 未过期 已上线 的活动
	 * 
	 * @param curPageNO
	 * @return
	 */
	PageSupport<MergeGroup> getAvailable(String curPageNO);

	/**
	 * 根据Id获取拼团活动
	 */
	public MergeGroup getMergeGroup(Long id);

	/**
	 * 删除拼团活动
	 */
	public void deleteMergeGroup(MergeGroup mergeGroup);

	/**
	 * 保存拼团活动
	 */
	public Long saveMergeGroup(MergeGroup mergeGroup);

	/**
	 * 更新拼团活动
	 */
	public void updateMergeGroup(MergeGroup mergeGroup);

	/**
	 * 获得所有商家的拼团活动列表
	 */
	public PageSupport<MergeGroupDto> getMergeGroupList(String curPageNO, Integer pageSize, MergeGroupDto mergeGroup);
	/**
	 * 通过ID更新所有商家的拼团活动状态
	 */

	public void updateMergeGroupStatus(Long id, Integer status);

	/**
	 * 通过ID和SHOPID更改商家的拼团活动状态
	 */
	Integer updateStatusByShopId(Long id, Integer status, Long shopId);

	PageSupport<MergeGroupOperationDto> getMergeGroupOperationList(String curPageNO, Integer pageSize,Long mergeId, Long shopId);

	void saveMergeGroupAndProd(MergeGroup mergeGroup,List<MergeProduct> mergeProductList, MultipartFile file, String userName, String userId, Long shopId);

	void updateMergeGroupAndProd(MergeGroup mergeGroup,List<MergeProduct> mergeProductList, MultipartFile file, String userName, String userId, Long shopId);

	PageSupport<MergeGroup> queryMergeGroupList(String curPageNO,Long shopId,MergeGroupDto mergeGroup);

	MergeGroup getMergeGroupByshopId(Long id, Long shopId);

	MergeGroup getMergeGroupByshopId(Long id);

	void deleteMergeGroupByShopId(Long shopId, Long id);
	
	Integer findHaveInMergeGroupOperate(Long id);

	boolean checkExitproduct(Long prodId);

	List<MergeGroup> getMergeGroupAll();

	List<MergeGroup> queryMergeGroupListByStatus();

	void dealWithMergeGroup(List<MergeGroup> cancalGroups, List<MergeGroupOperate> cancalOperates);

	boolean isGroupExemption(String addBumber, String userId);

	void finishDepositJob(List<MergeGroupOperate> mergeGroupOperates, Integer groupSurvivalTime, Long currentTimeMillis);

	void mergeGroupExemption(List<Long> ids);

	int updateOffineMergeGroup(List<MergeGroup> groups);


	/**
	 * 更新拼团活动删除状态为商家删除
	 * @param shopId 店铺ID
	 * @param id 活动ID
	 * @param deleteStatus 删除状态
	 */
	void updateDeleteStatus(Long shopId, Long id, Integer deleteStatus);

	/**
	 * 根据商品获取活动
	 * @param prodId
	 * @param skuId
	 * @return
	 */
    MergeGroup getMergeGroupPrice(Long prodId, Long skuId);
	/**
	 * 获取运营结果统计数据
	 * @param mergeId 拼团活动ID
	 * @param shopId 店铺ID
	 * @return
	 */
	OperateStatisticsDTO getOperateStatistics(Long mergeId, Long shopId);

}
