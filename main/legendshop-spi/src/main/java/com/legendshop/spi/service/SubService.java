/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.app.dto.IncomeTodayDto;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.presell.PresellSubEntity;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 订单服务接口.
 */
public interface SubService {

	/**
	 * 根据订单号查找订单
	 * 
	 * @param subNumber 订单号
	 * @return the sub 订单
	 */
	public Sub getSubBySubNumber(String subNumber);
	
	
	public Sub getSubBySubNumberByUserId(String subNumber,String userId);
	
	/**
	 * 根据订单号和用户ID查询预售订单
	 * @param subNumber 订单号
	 * @param userId 用户ID
	 * @return 预售订单
	 */
	public PresellSubEntity getPreSubBySubNoAndUserId(String subNumber,String userId);
	

	/**
	 * Update sub.
	 * 
	 * @param sub
	 *            the sub
	 */
	public void updateSub(Sub sub);
	
	/**
	 * 更新订单的支付类型
	 */
	public void updateSubPayType(String payTypeId,String payTypeName,Long subId);

	/**
	 * 根据ID，获取订单.
	 *
	 * @param subId the sub id
	 * @return the sub by id
	 */
	public abstract Sub getSubById(Long subId);
	
	/**
	 * 根据ID，获取订单.
	 *
	 * @param subId the sub id
	 * @return the sub by id
	 */
	public abstract Sub getSubByShopId(Long subId,Long shopId);
	
	/**
	 * 根据ID，获取订单.
	 *
	 * @param objects the sub id
	 * @return the sub by id
	 */
	public abstract List<Sub> getSubByIds(String [] objects,String userId,Integer status);
	
	
	public abstract boolean updateSubPrice(String subNumber, Double freight,
			Double orderAmount,String userName);
	
	/**
	 * 更新预售订单
	 * @param userName
	 * @return
	 */
	public abstract boolean updateSubPrice(PresellSubEntity presellSub,String userName);

	/**
	 * 删除订单.
	 *
	 * @param sub the sub
	 * @return true, if successful
	 */
	public boolean deleteSub(Sub sub);
	

	public void updateSubForPay(Sub sub,Integer currentStatus);

	/**
	 * 取消订单
	 * @param sub
	 * @return
	 */
	public boolean cancleOrder(Sub sub);

	public MySubDto findOrderDetail(String subNumber);


	public boolean changeDeliverGoods(String subNumber, Long dvyTypeId, String dvyFlowId, String info,String userName);

	/**
	 * 发货
	 * @param subNumber
	 * @param dvyTypeId
	 * @param dvyFlowId
	 * @param info
	 * @param shopId
	 * @return
	 */
	public String fahuo(String subNumber, Long dvyTypeId, String dvyFlowId, String info, Long shopId);

	/**
	 * 获取订单的操作操作历史
	 */
	public String findSubhisInfo(Long subId);

	/**
	 * 获取导出Excel的数据信息
	 */
	public List<OrderExcelWriterDto> getOutPutExcelOrder(Long[] items);
	
	public MySubDto findOrderDetail(String subNumber, String userId);
	
	public MySubDto findOrderDetail(String subNumber, Long shopId);
	
	public boolean orderReceive(String subNumber, String userId);

	public List<SubCountsDto> querySubCounts(String userId);

	/**
	 * 永久删除订单信息
	 * @param sub
	 * @return
	 */
	public boolean dropOrder(Sub sub);

	/**
	 * 根据 shopId 获取 订单每个状态的数量 （最近一个月）
	 * @param shopId
	 * @return
	 */
	public List<KeyValueEntity> getSubCounts(Long shopId);

	Sub getSubBySubNumberByShopId(String subNumber, Long ShopId);

	
	public List<Sub> getFinishUnPay(int commitInteval, Date date);

	public List<Sub> getUnAcklodgeSub(int commitInteval, Date date);

	/*
	 * 设置订单缓存
	 */
	public UserShopCartList putUserShopCartList(String type,String userId, UserShopCartList userShopCartList);

	public UserShopCartList findUserShopCartList(String type,String userId);
	
	public void evictUserShopCartCache(String type,String userId);

	public String changeDeliverNum(String subNumber, Long deliv, String dvyFlowId, String info, Long shopId);

	
	/**
	 * 获取订单能使用的礼券
	 * @param orderTotalAmount
	 * @param shopId
	 * @return
	 */
	public List<Coupon> queryCanUsedCoupons(String userId,
			Double orderTotalAmount, Long shopId);

	public Long saveSub(Sub sub);

	public abstract boolean updateSubPrice(Sub sub, String userName,
			Double price);

	/**
	 * 查询用户的团购订单数量
	 * @param userId
	 * @param productId
	 * @param skuId
	 * @return
	 */
	public Integer findUserOrderGroup(String userId, Long productId, Long skuId, Long groupId);

	/**
	 * 查找预售订单详情
	 * @param subNumber 订单号
	 * @param userId
	 * @return
	 */
	public abstract PresellSubDto findPresellOrderDetail(String subNumber, String userId);

	/**
	 * @param subNumber
	 * @return
	 */
	public abstract PresellSubDto findPresellOrderDetail(String subNumber);

	/**
	 * @param subNumber
	 * @param shopId
	 * @return
	 */
	public abstract PresellSubEntity getPreSubBySubNoAndShopId(String subNumber, Long shopId);
	
	/**
	 * @param subNumber
	 * @return
	 */
	public abstract PresellSubEntity getPreSubBySubNo(String subNumber);

	/**
	 * @param oldSub
	 */
	public abstract void receiveMoney(Sub oldSub, SubSettlement subSettlement);

	/**
	 * 获取商家今天的订单数量
	 * @param shopId 商家Id
	 * @return
	 */
	public Integer getTodaySubCount(Long shopId);


	/**
	 * //已完成的订单未评价的 订单个数
	 * @param userId
	 * @return
	 */
	public Integer getUnCommentOrderCount(String userId);


	/**
	 * 今日收入
	 * @param shopId
	 * @return
	 */
	public Double getIncomeToday(Long shopId);


	/**
	 * 提现卖家发货
	 * @param subId
	 * @param userId
	 * @return 
	 */
	public int remindDelivery(Long subId, String userId);
	
	/**
	 * 获取所有未支付的订单信息
	 * @return
	 */
	public List<Sub> findCancelOrders();


	/**
	 * 获取订单的过期时间
	 * @return
	 */
	public int getOrderCancelExpireDate();

	public int remindDeliveryBySN(String subNumber, String userId);

	public PageSupport<Sub> getSubListPage(String curPageNO, ShopOrderBill shopOrderBill, String subNumber);

	public PageSupport<OrderSub> getOrdersPage(OrderSearchParamDto paramDto, int pageSize);

	public PageSupport<Sub> getSubListPage(String curPageNO, Long shopId, String subNumber, ShopOrderBill shopOrderBill);

	public PageSupport<Sub> getOrderList(String curPageNO, String userName);

	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO,Long shopId); 
	
	public PageSupport<InvoiceSubDto> getInvoiceOrder(String curPageNO,Long shopId, InvoiceSubDto invoiceSubDto); 
	
	/**
	 *  查询该用户的订单信息
	 * @param subNums
	 * @param userId
	 * @param orderStatus
	 * @return
	 */
	public List<Sub> getSubBySubNums(String[] subNums, String userId, Integer  orderStatus);


	public List<Sub> findHaveInGroupSub(Long id, Integer value);


	public MergeGroupSubDto getMergeGroupSubDtoBySettleSn(String subSettlementSn);


	public List<String> queryUserPicByMergeGroupSub(Long mergeId, Long operateId);
	
	public List<SubDto> exportSubOrder(ShopOrderBill shopOrderBill, String subNumber);

	/**
	 * 获取MySubDto
	 * @param subNumber
	 * @return
	 */
	public MySubDto getMySubDtoBySubNumber(String subNumber);
	
	/**
	 * 结束超时不付费的订单.
	 */
	public void finishUnPay(List<Sub> list);


	/**
	 * 查询退款的总红包金额
	 */
	public Double getReturnRedpackOffPrice(Set<String> subNumbers);
	
	/**
	 * 根据秒杀id获取未支付的秒杀订单
	 * @param seckillId
	 * @return
	 */
	@Deprecated
	public List<Sub> getUnpaymentseckillOrder(Long seckillId);

	PageSupport<IncomeTodayDto> getOrders(Long shopId, String curPageNO, int pageSize);

	PageSupport<VisitLog> getVisitLogs(Long shopId, String curPageNO, int pageSize);

	/**
	 * 获取订单自动确认时间
	 * @return
	 */
	public Integer getOrderConfirmExpireDate();


	public MySubDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId);

	/**
	 * 根据交易单号查询订单列表
	 * @param subSettlementSn 交易单号
	 * @param userId
	 * @return
	 */
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn, String userId);

	/**
	 * 根据交易单号查询订单列表
	 * @param subSettlementSn 交易单号
	 * @return
	 */
	public List<Sub> getSubBySubSettlementSn(String subSettlementSn);

	/**
	 * 查询当前秒杀活动最后一个已提交但未支付的订单
	 * @param id 
	 * @return 
	 */
	public Sub getLastOneSubmitAndUnPaySeckillSub(Long id);


	/**
	 * 获取团购运营数据
	 * @param curPageNO 当前页码
	 * @param pageSize 页数
	 * @param groupId 团购ID
	 * @param shopId  店铺ID
	 * @return
	 */
	public PageSupport<Sub> getGroupOperationList(String curPageNO, Integer pageSize, Long groupId,
			Long shopId);

	/**
	 * 获取秒杀订单
	 * @param curPageNO
	 * @param pageSize
	 * @param seckillId
	 * @param shopId
	 * @return
	 */
	PageSupport<Sub> getSeckillOperationList(String curPageNO,int pageSize,Long seckillId,Long shopId);

	/**
	 * 获取预售订单
	 * @param curPageNO
	 * @param i
	 * @param id
	 * @param shopId
	 * @return
	 */
	PageSupport<Sub> getPresellOperationList(String curPageNO, int size, Long id, Long shopId);
}
