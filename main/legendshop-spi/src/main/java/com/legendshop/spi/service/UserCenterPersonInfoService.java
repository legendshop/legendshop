/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.MyPersonInfo;
import com.legendshop.model.entity.UserGrade;

/**
 * 用户中心服务
 * 
 */
public interface UserCenterPersonInfoService{

	/**
	 * Gets the user center person info.
	 *
	 * @param userId the user id
	 * @return the user center person info
	 */
	MyPersonInfo getUserCenterPersonInfo(String userId);
	
	/**
	 * Update user info.
	 *
	 * @param myPersonInfo the my person info
	 */
	void updateUserInfo(MyPersonInfo myPersonInfo);

	/**
	 * 验证手机验证码是否正确
	 * @param userName
	 * @param validateCode
	 */
	boolean verifySMSCode(String userName, String validateCode);

	/**
	 * 更新支付密码和支付强度
	 */
	void updatePaymentPassword(String userName, String paymentPassword, Integer payStrength);
	
	/**
	 * 查询用户等级
	 * @param userId
	 * @return
	 */
	UserGrade getUserGrade(String userId);
	
	/**
	 * 查询用户的下一个等级
	 * @param gradeId
	 * @return
	 */
	String getNextGradeName(Integer gradeId);
}
