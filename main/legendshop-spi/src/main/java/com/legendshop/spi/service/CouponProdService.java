/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.CouponProd;

/**
 * 优惠券商品.
 */
public interface CouponProdService  {

    public CouponProd getCouponProd(Long id);
    
    public void deleteCouponProd(CouponProd couponProd);
    
    public Long saveCouponProd(CouponProd couponProd);

    public void updateCouponProd(CouponProd couponProd);

    public List<CouponProd> getCouponProdByCouponId(Long couponId);

	public List<CouponProd> getCouponProdByCouponIds(List<Long> couponIds);
	
	public List<CouponProd> getCouponProdByProdId(Long prodId);

	/**
	 * 获取商品券关联的商品Id
	 * @param couponId
	 * @return
	 */
	public List<Long> getCouponProdIdByCouponId(Long couponId);


}
