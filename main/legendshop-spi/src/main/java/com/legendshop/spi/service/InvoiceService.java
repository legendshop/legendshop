package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Invoice;

/**
 * 发票服务
 */
public interface InvoiceService {

	public List<Invoice> getInvoice(String userName);

    public Invoice getInvoice(Long id);
    
    public void deleteInvoice(Invoice invoice);
    
    public Long saveInvoice(Invoice invoice);

    public int updateInvoice(Invoice invoice);

    public void updateDefaultInvoice(Long invoiceId,String userName);
    
    public void delById(Long id);

	public Invoice getDefaultInvoice(String userId);

	public Invoice getInvoice(Long id, String userId);

	public PageSupport<Invoice> getInvoicePage(String userName, String curPageNO, Integer pageSize);
	
	public int updateInvoiceList(List<Invoice> list);

	public void saveInvoice(Invoice item, String userName);

	/**
	 * 根据用户id和发票标题查找发票
	 */
	public Invoice getInvoiceByTitleId(String userId, Integer titleId);

	/**
	 * 改变用户默认发票的状态
	 */
	public void removeDefaultInvoiceStatus(String userId);

	/**
	 * 根据发票类型获取发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @param curPageNo 当前页码
	 * @return
	 */
    PageSupport<Invoice> queryInvoice(String userName, Integer invoiceType, String curPageNo);

	/**
	 * 根据类型获取用户发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @return
	 */
    List<Invoice> getInvoiceByType(String userName, Integer invoiceType);
}
