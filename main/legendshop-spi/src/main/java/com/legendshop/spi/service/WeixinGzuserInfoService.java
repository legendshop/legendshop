/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.weixin.WeiXinGzUserInfoDto;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 *微信关注用户服务
 */
public interface WeixinGzuserInfoService  {

    public WeixinGzuserInfo getWeixinGzuserInfo(Long id);
    
    public void deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
    
    public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);

    public void updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);

	public List<WeixinGzuserInfo> getUserByGroupId(Long groupid);
	
	public List<WeixinGzuserInfo> getUserInfo();
	
	public void synchronousUser(List<WeiXinGzUserInfoDto> dtList,List<WeixinGzuserInfo> dbList,Object obj);

	public PageSupport<WeixinGzuserInfo> query(String curPageNO, String key);

}
