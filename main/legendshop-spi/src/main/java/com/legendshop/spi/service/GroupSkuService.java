/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.GroupSku;

/**
 * The Class GroupSkuService.
 * 团购活动参加商品服务接口
 */
public interface GroupSkuService  {

   	/**
	 *  根据Id获取团购活动参加商品
	 */
    public GroupSku getGroupSku(Long id);

    /**
	 *  根据Id删除团购活动参加商品
	 */
    public int deleteGroupSku(Long id);
    
    /**
	 *  根据对象删除团购活动参加商品
	 */
    public int deleteGroupSku(GroupSku groupSku);
    
   /**
	 *  保存团购活动参加商品
	 */	    
    public Long saveGroupSku(GroupSku groupSku);

   /**
	 *  更新团购活动参加商品
	 */	
    public void updateGroupSku(GroupSku groupSku);
    
    /**
	 *  分页查询列表
	 */	
    public PageSupport<GroupSku> queryGroupSku(String curPageNO,Integer pageSize);

	/**
	 * 根据活动id删除商品
	 * @param id
	 */
	void deleteGroupSkuByGroupId(Long id);
}
