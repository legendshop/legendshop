/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.Map;

import com.legendshop.model.constant.PaymentProcessorEnum;
import com.legendshop.model.form.SysPaymentForm;

/**
 * 支付接口
 * @author Tony哥
 *
 */
public interface PaymentService {

	
	/**
	 * 
	 * @param processorEnum
	 * @param paymentForm
	 * @return
	 */
	public Map<String, Object> payto(PaymentProcessorEnum processorEnum, SysPaymentForm paymentForm);

}
