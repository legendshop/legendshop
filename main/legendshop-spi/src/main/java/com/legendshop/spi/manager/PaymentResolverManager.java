package com.legendshop.spi.manager;

import java.util.Map;

import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.BankCallbackForm;

/**
 * 支付
 *
 */
public interface PaymentResolverManager {

	/**
	 * 查询支付的单据信息
	 * @param params
	 * @return
	 */
	Map<String, Object> queryPayto(SubSettlementTypeEnum typeEnum, Map<String, String> params);

	/**
	  *  支付回调
	  *  
	 */
	void doBankCallback(SubSettlement subSettlement, BankCallbackForm bankCallbackForm);

}