/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.weixin.WeixinImgGroup;

/**
 * 微信图片组服务
 */
public interface WeixinImgGroupService  {

    public WeixinImgGroup getWeixinImgGroup(Long id);
    
    public void deleteWeixinImgGroup(WeixinImgGroup weixinImgGroup);
    
    public Long saveWeixinImgGroup(WeixinImgGroup weixinImgGroup);

    public void updateWeixinImgGroup(WeixinImgGroup weixinImgGroup);

	public List<WeixinImgGroup> getAllGroupsInfo();

	public WeixinImgGroup getWeixinImgGroupByName(String groupName);
}
