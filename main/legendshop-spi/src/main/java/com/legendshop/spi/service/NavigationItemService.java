/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.NavigationPositionEnum;
import com.legendshop.model.entity.NavigationItem;

/**
 * 导航子项目服务.
 */
public interface NavigationItemService  {

    public List<NavigationItem> getNavigationItem();
    
    public List<NavigationItem> getAllNavigationItem();

    public NavigationItem getNavigationItem(Long id);
    
    public void deleteNavigationItem(NavigationItem navigationItem);
    
    public Long saveNavigationItem(NavigationItem navigationItem);

    public void updateNavigationItem(NavigationItem navigationItem);

	/**
	 * 查找对应位置的导航
	 * @param position
	 * @return
	 */
    public List<NavigationItem> getNavigationItem(NavigationPositionEnum position);

	public PageSupport<NavigationItem> getNavigationItemPage(String curPageNO);
}
