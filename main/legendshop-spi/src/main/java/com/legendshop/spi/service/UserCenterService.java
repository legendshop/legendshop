/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Myfavorite;

/**
 * 用户中心服务.
 */
public interface UserCenterService  {

	/**
	 * Delete favs.
	 *
	 * @param userId the user id
	 * @param selectedFavs the selected favs
	 */
	void deleteFavs(String userId, String selectedFavs);
	
	/**
	 * Delete all favs.
	 *
	 * @param userId the user id
	 */
	void deleteAllFavs(String userId);
	
	/**
	 * Save favorite.
	 *
	 * @param myfavorite the myfavorite
	 */
	void saveFavorite(Myfavorite myfavorite);
	
	/**
	 * Checks if is exists favorite.
	 *
	 * @param prodId the prod id
	 * @param userName the user name
	 * @return the boolean
	 */
	Boolean isExistsFavorite(Long prodId,String userName);
	
	/**
	 * Gets the favorite length.
	 *
	 * @param userId the user id
	 * @return the favorite length
	 */
	Long getfavoriteLength(String userId);

	public PageSupport<Myfavorite> getFavoriteList(String curPageNO,Integer pageSize, String userId);
}
