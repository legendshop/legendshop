/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdTypeBrand;
import com.legendshop.model.entity.ProdTypeParam;
import com.legendshop.model.entity.ProdTypeProperty;

/**
 * 商品类型Service.
 */
public interface ProdTypeService  {

    public ProdType getProdType(Long id);
    
    /**
     * 返回一定数量的商品类型, 并包含typeId
     * @param tpyeId
     * @return
     */
    public List<ProdType> getProdTypeListWithId(Long tpyeId);
    
    public void deleteProdType(ProdType prodType);
    
    public Long saveProdType(ProdType prodType);

    public void updateProdType(ProdType prodType);

    public void saveTypeProp(List<ProdTypeProperty> propertyList,Long id);
    
    public void saveTypeBrand(List<ProdTypeBrand> brandList,Long id);
    
    public void saveParameterProperty(List<ProdTypeParam> propertyList,Long prodTypeId);
    
    public void deleteProdProp(Long propId,Long prodTypeId);
    
    public void deleteBrand(Long brandId,Long prodTypeId);
    
    public void deleteParameterProp(Long propId,Long prodTypeId);
    
    public List<KeyValueEntity> loadProdType();

    ProdTypeParam getProdTypeParam(Long propId, Long prodTypeId);

	public PageSupport<ProdType> getProdTypePage(String typeName);

	public PageSupport<ProdType> queryProdTypePage(String curPageNO, ProdType prodType, Long categoryId);

	public PageSupport<ProdType> getProdTypePage(String curPageNO, String name);

	public PageSupport<ProdType> getProdTypeByName(String typeName);

	/**
	 * 获取类型下的规格关联的sku
	 * @param proTypeId
	 * @param propId
	 * @return
	 */
	public List<Long> ifDeleteProductProperty(Long proTypeId, Long propId);
}
