/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ShopUser;

/**
 *商家用户服务
 */
public interface ShopUserService  {

	public List<ShopUser> getShopUseByShopId(Long shopId);

    public ShopUser getShopUser(Long id);
    
    public void deleteShopUser(ShopUser shopUser);
    
    public Long saveShopUser(ShopUser shopUser);

    public void updateShopUser(ShopUser shopUser);

	public ShopUser getShopUserByNameAndShopId(String name, Long shopId);

	public List<ShopUser> getShopUseRoleId(Long shopRoleId);

	public PageSupport<ShopUser> queryShopUser(String curPageNO, Long shopId);
}
