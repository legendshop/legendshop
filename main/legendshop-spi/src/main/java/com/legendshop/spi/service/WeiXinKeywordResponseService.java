/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * 微信关键字回复服务
 */
public interface WeiXinKeywordResponseService  {

    public WeiXinKeywordResponse getWeiXinKeywordResponse(Long id);
    
    public void deleteWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);
    
    public Long saveWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);

    public void updateWeiXinKeywordResponse(WeiXinKeywordResponse weiXinKeywordResponse);

	public PageSupport<WeiXinKeywordResponse> getWeiXinnKeywordResponse(String curPageNO);
}
