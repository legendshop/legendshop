/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationAppeal;

/**
 * 商家上诉服务接口
 */
public interface AccusationAppealService {

	/** 获取商家上诉 */
	public AccusationAppeal getAccusationAppeal(Long id);

	/** 删除商家上诉 */
	public void deleteAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 保存商家上诉 */
	public Long saveAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 更新商家上诉 */
	public void updateAccusationAppeal(AccusationAppeal accusationAppeal);

	/** 获取商家上诉列表 */
	public PageSupport<AccusationAppeal> getAccusationAppealPage(String curPageNO);
}
