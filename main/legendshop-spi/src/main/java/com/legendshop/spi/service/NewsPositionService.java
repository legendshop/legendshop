/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.entity.NewsPosition;

/**
 * 文章位置服务.
 */
public interface NewsPositionService {

	Long saveNewsPosition(NewsPosition p);

	public abstract NewsPosition getNewsPositionyById(Long id);

	public abstract List<NewsPosition> getNewsPositionByNewsId(Long newsId);

	public abstract List<NewsPosition> getNewsPositionList();

	public abstract void delete(Long id);

	public abstract void updateNewsPosition(NewsPosition  newsPosition);

	public List<NewsDto> getNewsList();
}
