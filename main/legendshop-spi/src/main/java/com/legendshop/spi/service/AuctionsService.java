/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.spi.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.Auctions;

/**
 * 拍卖活动服务接口.
 */
public interface AuctionsService  {

	public List<Auctions> getAuctionsBystatus(Long firstId,Long lastId,Long status);

    public Auctions getAuctions(Long id);

    public Auctions getAuctionsDetails(Long id);

    public void deleteAuctions(Auctions auctions);

    public Long saveAuctions(Auctions auctions);

    public void updateAuctions(Auctions auctions);

    public int finishAuction(Long id);

	/**
	 * 获取当前商品的价格
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public String currentQueryPrice(Long prodId, Long skuId);

	/**
	 * 更新出价记录
	 * @param crowdWatch
	 * @param bidCount
	 * @param date
	 * @param id
	 */
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice,Date date, Long id);

	/**
	 * 查询所有在线并且不过期的活动
	 * @return
	 */
	public List<Auctions> getonlineList();

	public Long getMinId();

	public Long getMaxId();


	/**
	 * 支付保证金
	 * @param paimaiId
	 * @param amount
	 * @return
	 */
	String payAuction(Long paimaiId, double amount, String userId,Long shopId,String auctionsTitle);

	/**
	 * 排除预错商品
	 * @return
	 */
	public boolean excludePresell(Long prodId,Long skuId);

	/**
	 * 更新出价记录
	 * @param id
	 */
	public void updateAuctionsBidNumber(Long id);

	void updateAuctionsEnrolNumber(Long id,Long count);

	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Auctions auctions);

	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, Long shopId, Auctions auctions);

	public PageSupport<Auctions> queryAuctionListPage(String curPageNO, ProductSearchParms parms);

	public PageSupport<Auctions> queryPageAuctionList(String curPageNO, ProductSearchParms parms);

	public int updateAuctionStatus(Long auctionStatus,Long auctionId);

	public boolean findIfJoinAuctions(Long productId);

	/**
	 * sku是否参加拍卖
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public Auctions isAuction(Long prodId, Long skuId);

	/**
	 * 终止拍卖活动,退还拍卖保证金以及释放sku
	 * @param id
	 * @return
	 */
	String offlineAuctions(Long id);

	/**
	 * 竞拍失败的用户退款操作 回滚用户的保证金
	 * @param auctionDepositRec
	 * @return
	 */
	boolean bidUserRefund(AuctionDepositRec auctionDepositRec);
}
