/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;

/**
 * 布局产品服务.
 */
public interface ShopLayoutProdService  {

    public ShopLayoutProd getShopLayoutProd(Long id);
    
    public void deleteShopLayoutProd(ShopLayoutProd shopLayoutProd);
    
    public Long saveShopLayoutProd(ShopLayoutProd shopLayoutProd);

    public void updateShopLayoutProd(ShopLayoutProd shopLayoutProd);

	public void deleteShopLayoutProd(Long shopId, Long layoutId);
}
