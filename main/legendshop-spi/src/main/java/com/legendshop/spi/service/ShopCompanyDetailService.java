/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import com.legendshop.model.entity.ShopCompanyDetail;

/**
 *公司详细信息服务
 */
public interface ShopCompanyDetailService  {

    public ShopCompanyDetail getShopCompanyDetail(Long id);
    
    public abstract ShopCompanyDetail getShopCompanyDetailByShopId(Long shopId);
    
    public void deleteShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);
    
    public Long saveShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);

    public void updateShopCompanyDetail(ShopCompanyDetail shopCompanyDetail);

	/**
	 * 修改公司信息
	 */
	public boolean updataShopCompanyInfo(ShopCompanyDetail detail,ShopCompanyDetail newCompanyDetail);

}
