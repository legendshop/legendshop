/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.entity.ShopPerm;
import com.legendshop.model.entity.ShopPermId;

/**
 *商家权限服务
 */
public interface ShopPermService  {

    public ShopPerm getShopPerm(Long id);
    
    public void deleteShopPerm(ShopPerm shopPerm);
    
    public Long saveShopPerm(ShopPerm shopPerm);

    public void updateShopPerm(ShopPerm shopPerm);

	public void saveShopPerm(List<ShopPerm> shopPermList);

	public List<String> getShopPermsByRoleId(Long shopRoleId);
	
	void deleteUserRole(List<ShopPerm> shopPermList);

	/** 根据职位Id和菜单标识获取权限 */
	public String getFunction(Long shopRoleId, String label);

	/**
	 * 根据职位id获取所有权限
	 * @param shopRoleId 职位id
	 */
	public List<ShopPermId> queryShopPermsByRoleId(Long shopRoleId);
	
}
