package com.legendshop.spi.service;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdRechargeDto;

/**
 * app 预存款充值记录service
 * @author linzh
 */
public interface AppPdRechargeService {

	/**
	 * 获取用户的预存款充值列表
	 * @param curPageNO 当前页码
	 * @param userId 用户id
	 * @param state 支付状态
	 * @return
	 */
	public abstract AppPageSupport<AppPdRechargeDto> getRechargeDetailedPage(String curPageNO,
			String userId, String state);

	/**
	 * 获取用户预存款记录详情
	 * @param id
	 * @param userId
	 * @return
	 */
	public abstract AppPdRechargeDto getPdRecharge(Long id, String userId);

}
