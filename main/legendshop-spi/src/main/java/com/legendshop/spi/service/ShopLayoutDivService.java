/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

/**
 * 首页装修部分布局服务.
 */
public interface ShopLayoutDivService  {

	/**
	 * 删除布局layout
	 * @param shopId
	 * @param layoutId
	 */
	public void deleteShopLayoutDiv(Long shopId, Long layoutId);

   
}
