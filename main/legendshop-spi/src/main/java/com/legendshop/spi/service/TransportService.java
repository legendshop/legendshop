package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.Transport;

/**
 * 运费模板服务
 */
public interface TransportService {

	public List<Transport> getTransports(Long shopId);

    public Transport getTransport(Long id);
    
    public void deleteTransport(Transport transport);
    
    public void saveTransport(Transport transport);

    public void updateTransport(Transport transport);

    public List<Area> getCtyName(Long ctyId);
    
    public Transport getDetailedTransport(Long shopId,Long id);

	public void deleteTransport(Long id,Long shopId);
	
	public boolean isAppTransport(Long id);

	public PageSupport<Transport> getTransportPage(String curPageNO, Transport transport, Long shopId);

	public PageSupport<Transport> getFreightChargesTemp(String curPageNO, Long shopId, String transName);

	public Integer checkByName(String name,Long shopId);
}
