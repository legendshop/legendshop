/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.entity.Category;


/**
 * 商品树处理业务类
 * @author tony
 *
 */
public interface CategoryManagerService {

	/**
	 * 删除分类树
	 */
	public void delTreeNode(Category category);
	
	/**
	 * 删除当前节点的某个子节点
	 */
	public void delChildNode(Category category,long childId);

	/**
	 * 增加分类树
	 */
	public Long addTreeNode(Category category);
	

	/**
	 * 更新分类树
	 */
	public void updateTreeNode(Category category);
	
	/**
	 * 根据节点ID找到TreeNote
	 */
	public abstract TreeNode getTreeNodeById(long treeId);
	
	/**
	 * 获取所有TreeNode
	 */
	public abstract TreeNode getAllTreeNodeById(long treeId);
	
	/**
	 * 重新加载分类树
	 */
	public abstract void generateTree();
	
	/**
	 * 刷新树
	 */
	public void refreshTree();
	
	/**
	 * 计算树的path，如:1/2/3/4 如：55/236/119 --->家用电器/大 家 电/洗衣机/ 
	 *
	 * @param fromNodeId 起始节点ID null
	 * @param toNodeId 结束的节点ID
	 * @param separator 分隔符 如 '/'
	 * @return the node object id path
	 */
	public abstract String getNodeObjectIdPath(Long fromNodeId, Long toNodeId,String separator);
	
	
	/**
	 * 分类面包屑导航
	 */
	public abstract List<TreeNode> getTreeNodeNavigation(Long treeId,String separator);

	/**
	 * 分类面包屑导航 不包括下线的
	 */
	public abstract List<TreeNode> getTreeNodeOnline(Long treeId, String separator);

	String getNodeObjectIdPathOnline(Long fromNodeId, Long toNodeId, String separator);


	
}
