/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.spi.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.ShopOrderBill;

/**
 *拍卖保证金服务
 */
public interface AuctionDepositRecService  {

    public List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId);

    public AuctionDepositRec getAuctionDepositRec(Long id);
    
    public void deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec);
    
    public Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec);

    public void updateAuctionDepositRec(AuctionDepositRec auctionDepositRec);

	public boolean isPaySecurity(String userId, Long paimaiId);

	/**
	 * 查询报名人数
	 * @param paimaiId
	 * @return
	 */
	public Long  queryAccess(Long paimaiId);

	public AuctionDepositRec getAuctionDepositRecByUserIdAndaId(String userId, Long paimaiId);

	public PageSupport<AuctionDepositRec> queryDepositRecList(String curPageNO, Long id);

	public AuctionDepositRec getAuctionDepositRec(String userId, String paySn);

	/**
	 * 根据结算当期获取结算的拍卖保证金记录
	 * @param curPageNO
	 * @param shopId
	 * @param shopOrderBill
	 * @return
	 */
	PageSupport<AuctionDepositRec> queryAuctionDepositRecList(String curPageNO, Long shopId, ShopOrderBill shopOrderBill);
}
