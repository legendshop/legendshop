/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.oss;

import java.io.Serializable;
import java.util.List;

/**
 * The Class OssPage.
 */
public class OssPage implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1459743482000382421L;

	/** The next marker. */
	private String nextMarker;
	
	/** The item list. */
	private List<OssItem> summrayList;

	/**
	 * Gets the next marker.
	 *
	 * @return the next marker
	 */
	public String getNextMarker() {
		return nextMarker;
	}

	/**
	 * Sets the next marker.
	 *
	 * @param nextMarker the new next marker
	 */
	public void setNextMarker(String nextMarker) {
		this.nextMarker = nextMarker;
	}

	/**
	 * Gets the summray list.
	 *
	 * @return the summray list
	 */
	public List<OssItem> getSummrayList() {
		return summrayList;
	}

	/**
	 * Sets the summray list.
	 *
	 * @param summrayList the new summray list
	 */
	public void setSummrayList(List<OssItem> summrayList) {
		this.summrayList = summrayList;
	}
	
}
