/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.oss;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.legendshop.util.AppUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.CopyObjectResult;
import com.aliyun.oss.model.DownloadFileRequest;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;

/**
 * 阿里云的oss处理器
 * 
 * @author newway
 *
 */
public class OSSProcessorImpl implements OSSPropcessor {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(OSSProcessorImpl.class);

	public String bucketName = null;

	private OSSClient client;

	public String bucketName_private = null;

	public OSSProcessorImpl(String endpoint, String accessKeyId, String accessKeySecret, String bucketName,String bucketName_private) {
		this.bucketName = bucketName;
		this.bucketName_private = bucketName_private;
		this.client = new OSSClient(endpoint, accessKeyId, accessKeySecret, getConfig());
	}

	// 创建Bucket
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#makeBucket(java.lang.String)
	 */
	@Override
	public void makeBucket(String bucketName) {
		boolean exist = client.doesBucketExist(bucketName);
		if (exist) {
			log.warn("The bucket exist.");
			return;
		}
		client.createBucket(bucketName);
	}

	/*
	 * 上传图片到阿里云服务器Object
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#putObjectFile(java.lang.String,
	 * java.io.InputStream, long)
	 */
	@Override
	public PutObjectResult putObjectFile(String key, InputStream inputStream, long fLength) {
		PutObjectResult result = null;
		try {
			// 初始化OSSClient
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setContentLength(fLength);
			// meta.setContentType("text/plain");
			// 上传Object.
			result = client.putObject(bucketName, key, inputStream, meta);
		} catch (Exception ex) {
			log.error("upload file error:  " + key, ex);
		}
		return result;
	}

	/*
	 * 上传图片到阿里云服务器Object
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#putObjectFile(java.lang.String,
	 * java.io.File, long)
	 */
	@Override
	public PutObjectResult putObjectFile(String key, File file, long fLength) {
		PutObjectResult result = null;
		try {
			// 初始化OSSClient
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setContentLength(fLength);
			// meta.setContentType("text/plain");
			// 上传Object.
			result = client.putObject(bucketName, key, file, meta);
		} catch (Exception ex) {
			log.error("upload file error:  " + key, ex);
		}
		return result;
	}

	/**
	 * 拷贝一个在OSS上已经存在的Object成另外一个Object的请求结果。
	 */
	@Override
	public CopyObjectResult copyObjectFile(String key) {
		String destinationKey = "snapshot/"+key;
		CopyObjectResult result = client.copyObject(bucketName, key,bucketName,destinationKey);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#deleteObjectFile(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void deleteObjectFile(String key) {
		client.deleteObject(bucketName, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#makeDir(java.lang.String)
	 */
	@Override
	public void makeDir(String keySuffixWithSlash) {
		/*
		 * Create an empty folder without request body, note that the key must be
		 * suffixed with a slash
		 */
		if (StringUtils.isEmpty(keySuffixWithSlash)) {
			return;
		}
		if (!keySuffixWithSlash.endsWith("/")) {
			keySuffixWithSlash += "/";
		}
		client.putObject(bucketName, keySuffixWithSlash, new ByteArrayInputStream(new byte[0]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#downloadFile(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean downloadFile(String downloadFile, String key) {
		OSSObject object = client.getObject(bucketName, key);
		if (object != null) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
				String storeDir = downloadFile.substring(0, downloadFile.lastIndexOf("/"));
				File parentPath = new File(storeDir);
				if (!parentPath.exists()) {
					parentPath.mkdirs();
				}
				OutputStream os = null;
				try {
					os = new FileOutputStream(new File(downloadFile));
					FileCopyUtils.copy(object.getObjectContent(), os);
					return true;
				} catch (Exception e) {
					log.error("downloadFile:", e);
				} finally {
					if (os != null) {
						os.close();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						log.error("downloadFile:", e);
					}
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#downloadFileHttp(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean downloadFileHttp(String downloadFile, String key) {
		try {
			DownloadFileRequest downloadFileRequest = new DownloadFileRequest(bucketName, key);
			// 设置本地文件
			downloadFileRequest.setDownloadFile(downloadFile);
			// 设置并发下载数，默认1
			downloadFileRequest.setTaskNum(5);
			// 设置分片大小，默认100KB
			downloadFileRequest.setPartSize(1024 * 1024 * 1);
			// 开启断点续传，默认关闭
			downloadFileRequest.setEnableCheckpoint(true);
			client.downloadFile(downloadFileRequest);
		} catch (OSSException oe) {
			log.error("", oe);
			return false;
		} catch (ClientException ce) {
			log.error("", ce);
			return false;
		} catch (Throwable e) {
			log.error("", e);
			return false;
		}
		return true;
	}

	// 实时的分页查询
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.oss.OSSPropcessor#listPage(java.lang.String,
	 * java.lang.String, java.lang.Integer)
	 */
	@Override
	public OssPage listPage(String dir, String nextMarker, Integer maxKeys) {
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName);
		if (dir != null) {
			listObjectsRequest.setPrefix(dir);
		}
		if (nextMarker != null) {
			listObjectsRequest.setMarker(nextMarker);
		}
		if (maxKeys != null) {
			listObjectsRequest.setMaxKeys(maxKeys);
		}
		ObjectListing objectListing = client.listObjects(listObjectsRequest);
		List<OSSObjectSummary> summrayList = objectListing.getObjectSummaries();
		List<OssItem> itemList = summaryToItem(summrayList);
		OssPage page = new OssPage();
		String newxNextMarker = objectListing.getNextMarker();
		page.setNextMarker(newxNextMarker);
		page.setSummrayList(itemList);
		return page;
	}


	//添加私有读写库的方法  ------------------------------------------- 2019-09-26  Jason wu

	@Override
	public String getPrivateObjectUrl(String key) {
		if (AppUtils.isBlank(key)) {
			return null;
		}

		// 设置URL过期时间为1小时
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, 1);

		// 生成URL
		return client.generatePresignedUrl(bucketName_private, key, calendar.getTime()).toString();

	}

	@Override
	public PutObjectResult putPrivateObjectFile(String key, InputStream inputStream, long fLength) {
		PutObjectResult result = null;
		try {
			// 初始化OSSClient
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setContentLength(fLength);
			// meta.setContentType("text/plain");
			// 上传Object.
			result = client.putObject(bucketName_private, key, inputStream, meta);
		} catch (Exception ex) {
			log.error("upload file error:  " + key, ex);
		}
		return result;
	}

	@Override
	public PutObjectResult putPrivateObjectFile(String key, File file, long fLength) {
		PutObjectResult result = null;
		try {
			// 初始化OSSClient
			// 创建上传Object的Metadata
			ObjectMetadata meta = new ObjectMetadata();
			// 必须设置ContentLength
			meta.setContentLength(fLength);
			// meta.setContentType("text/plain");
			// 上传Object.
			result = client.putObject(bucketName_private, key, file, meta);
		} catch (Exception ex) {
			log.error("upload file error:  " + key, ex);
		}
		return result;
	}

	@Override
	public void deletePrivateObjectFile(String key) {
		client.deleteObject(bucketName_private, key);
	}


	// 把OSS的对象，转换成自己的。因为OSS的对象没有实现Serialiable，不能序列化。
	private List<OssItem> summaryToItem(List<OSSObjectSummary> summrayList) {
		List<OssItem> itemList = new ArrayList<OssItem>();
		for (OSSObjectSummary summary : summrayList) {
			OssItem item = new OssItem();
			try {
				BeanUtils.copyProperties(item, summary);
				itemList.add(item);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return itemList;
	}

	// 配置参数
	private ClientConfiguration getConfig() {
		ClientConfiguration conf = new ClientConfiguration();
		conf.setMaxConnections(2000);
		conf.setConnectionTimeout(5000);
		conf.setMaxErrorRetry(3);
		conf.setSocketTimeout(2000);
		return conf;
	}

}