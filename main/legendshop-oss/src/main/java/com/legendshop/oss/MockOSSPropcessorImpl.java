package com.legendshop.oss;

import java.io.File;
import java.io.InputStream;

import com.aliyun.oss.model.CopyObjectResult;
import com.aliyun.oss.model.PutObjectResult;
/**
 * 在不启用的oss功能时候， 使用此oss处理器
 * @author newway
 *
 */
public class MockOSSPropcessorImpl implements OSSPropcessor {

	@Override
	public void makeBucket(String bucketName) {
		// TODO Auto-generated method stub
	}

	@Override
	public PutObjectResult putObjectFile(String key, InputStream inputStream, long fLength) {
		return null;
	}

	@Override
	public PutObjectResult putObjectFile(String key, File file, long fLength) {
		return null;
	}

	@Override
	public void deleteObjectFile(String key) {
		// TODO Auto-generated method stub

	}

	@Override
	public void makeDir(String keySuffixWithSlash) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean downloadFile(String downloadFile, String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean downloadFileHttp(String downloadFile, String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public OssPage listPage(String dir, String nextMarker, Integer maxKeys) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrivateObjectUrl(String key) {
		return null;
	}

	@Override
	public PutObjectResult putPrivateObjectFile(String key, InputStream inputStream, long fLength) {
		return null;
	}

	@Override
	public PutObjectResult putPrivateObjectFile(String key, File file, long fLength) {
		return null;
	}

	@Override
	public void deletePrivateObjectFile(String key) {

	}

	@Override
	public CopyObjectResult copyObjectFile(String key) {
		// TODO Auto-generated method stub
		return null;
	}

}
