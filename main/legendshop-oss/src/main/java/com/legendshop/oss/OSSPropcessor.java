package com.legendshop.oss;

import java.io.File;
import java.io.InputStream;

import com.aliyun.oss.model.CopyObjectResult;
import com.aliyun.oss.model.PutObjectResult;

/**
 * 阿里云的oss处理器
 * @author newway
 *
 */
public interface OSSPropcessor {

	/**
	 * 创建Bucket  
	 * @param bucketName
	 */
	public abstract void makeBucket(String bucketName);

	/**
	 * 上传图片到阿里云服务器Object
	 */
	public abstract PutObjectResult putObjectFile(String key, InputStream inputStream, long fLength);

	/**
	 * 上传图片到阿里云服务器Object
	 */
	public abstract PutObjectResult putObjectFile(String key, File file, long fLength);
	
	/**
	 * 拷贝文件
	 * @param key
	 * @return
	 */
	public abstract CopyObjectResult copyObjectFile(String key);

	/**
	 * 删除对象或者空目录
	 * 
	 * @param bucketName
	 * @param key
	 * @return
	 */
	public abstract void deleteObjectFile(String key);

	/**
	 * 创建目录，不能以斜杠“/”开头  
	 * @param keySuffixWithSlash
	 */
	public abstract void makeDir(String keySuffixWithSlash);

	/**
	 * 下载文件
	 * 
	 * @param downloadFile
	 *            下载的全文件路劲
	 * @param key
	 * @return
	 */
	public abstract boolean downloadFile(String downloadFile, String key);

	/**
	 * 断点续传下载用法示例.
	 *
	 * @param downloadFile the download file
	 * @param key the key
	 * @return true, if successful
	 */
	public abstract boolean downloadFileHttp(String downloadFile, String key);

	/**
	 * 实时的分页查询  
	 *
	 * @param dir the dir
	 * @param nextMarker the next marker
	 * @param maxKeys the max keys
	 * @return the oss page
	 */
	public abstract OssPage listPage(String dir, String nextMarker, Integer maxKeys);


	/**
	 * 获取私有库的文件地址
	 * @param key
	 * @return
	 */
	public String getPrivateObjectUrl(String key);

	/**
	 * 上传文件到私有库
	 * @param key
	 * @param inputStream
	 * @param fLength
	 * @return
	 */
	public PutObjectResult putPrivateObjectFile(String key, InputStream inputStream, long fLength);

	/**
	 * 上传文件到私有库
	 * @param key
	 * @param file
	 * @param fLength
	 * @return
	 */
	public PutObjectResult putPrivateObjectFile(String key, File file, long fLength);

	/**
	 * 删除私有库文件
	 * @param key
	 */
	public void deletePrivateObjectFile(String key);



}