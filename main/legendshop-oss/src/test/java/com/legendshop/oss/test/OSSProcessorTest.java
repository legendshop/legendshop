package com.legendshop.oss.test;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.activation.MimetypesFileTypeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import com.legendshop.oss.OSSProcessorImpl;
import com.legendshop.oss.OSSPropcessor;
import com.legendshop.oss.OssItem;
import com.legendshop.oss.OssPage;

public class OSSProcessorTest {

	OSSPropcessor processor = null;

	public String accessKeyId = "LTAIfDk9qqUrIQMK";
	public String accessKeySecret = "sNF0K8gFnomT754zgmonxDN9iM7sI8";
	public String endpoint = "http://oss-cn-beijing.aliyuncs.com";
	public String bucketName = "kangqiaopubwrite";

	@Before
	public void setUp() throws Exception {
		processor = new OSSProcessorImpl(endpoint, accessKeyId, accessKeySecret, bucketName,null);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPutObjectFile() throws FileNotFoundException {
		String filePath = "C:/Users/newway/Desktop/e7b68ee6-53aa-42b2-96fe-bd46133da438.png";
		// 获取指定文件的输入流
		File file = new File(filePath);
		// 获取指定文件的输入流
		InputStream inputStream = new FileInputStream(file);
		// 创建上传Object的Metadata
		ObjectMetadata meta = new ObjectMetadata();
		// 必须设置ContentLength
		meta.setContentLength(file.length());
		// 从文件名获取mime类型
		meta.setContentType(MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(file.getName()));
		PutObjectResult result = processor.putObjectFile(
				"photoserver/photo/2017/01/22/e7b68ee6-53aa-42b2-96fe-bd46133da439.png", inputStream, file.length());
		System.out.println("新增结果 = " + result);
	}

	@Test
	public void testDeleteObjectFile() {
		processor.deleteObjectFile("photoserver/photo/2017/01/22/e7b68ee6-53aa-42b2-96fe-bd46133da438.png");
		System.out.println("删除成功");
	}

	@Test
	public void testDownloadFile() {
		processor.downloadFile("C:/Users/newway/Desktop/oss/file/a1.png", "小米.png");
	}

	@Test
	public void testDownloadFileHttp() {
		processor.downloadFile("C:/Users/newway/Desktop/oss/file/a.png", "小米.png");
	}

	@Test
	public void testMakeDir() {
		processor.makeDir("test/");
	}

	@Test
	public void testDeleteDir() {
		processor.deleteObjectFile("abc/logo.jpg");
		processor.deleteObjectFile("abc/");
		System.out.println("删除成功");
	}

	@Test
	public void testListPage() {
		OssPage page = processor.listPage("photoserver/", "marker", null);
		System.out.println(page.getSummrayList());
		if (page.getSummrayList() != null) {
			for (OssItem ossItem : page.getSummrayList()) {
				processor.downloadFile("C:/Users/newway/Desktop/photo/" + ossItem.getKey(), ossItem.getKey());
			}
		}

		System.out.println("List成功");
	}

}
