package com.legendshop.test;

import java.io.Serializable;

public class Province  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3119411918508216379L;

	private Integer id;
	
	private String provinceid;
	
	private String province;
	
	public Province(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProvinceid() {
		return provinceid;
	}

	public void setProvinceid(String provinceid) {
		this.provinceid = provinceid;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id = ").append(id).append(", provinceid = ").append(provinceid).append(", province = ").append(province);
		return sb.toString();
	}
}
