/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.plugins;

import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.util.AppUtils;

/**
 * 解析conditional:plugin,根据数据库配置来决定是否加载插件.
 */
public class PluginImportMatcher extends ImportMatcher {

	/**
	 * 解析conditional:plugin,根据数据库配置来决定是否加载插件.
	 * 
	 * @return true, if is match
	 */
	@Override
	public boolean isMatch() {
		String pluginName = this.getValue();
		if (AppUtils.isBlank(pluginName)) {
			return false;
		}
		PluginManager pluginManager = getPluginManager();
		if (pluginManager != null && pluginManager.getPlugins() != null) {
			for (Plugin plugin : pluginManager.getPlugins()) {
				if (isThePluginNormal(plugin, pluginName)) {
					PluginConfig pluginConfig = plugin.getPluginConfig();
					// found record
					if (pluginConfig.getStatus().equals(PluginStatusEnum.Y)) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
		return false;
	}

	protected boolean isThePluginNormal(Plugin plugin, String pluginName) {
		if (plugin.getPluginConfig() == null) {
			throw new RuntimeException("Missing config for plugin name " + pluginName);
		}
		return PluginStatusEnum.Y.equals(plugin.getPluginConfig().getStatus())
				&& pluginName.equals(plugin.getPluginConfig().getPulginId());
	}

	public PluginManager getPluginManager() {
		return PluginRepository.getInstance();
	}

}