package com.legendshop.framework.handler.impl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.handler.StartupService;

@Component
public class StartupServiceImpl implements StartupService{

	/** The is inited. */
	private Boolean isInited = false;

	/**
	 * 初始化,注意顺序不能调转.
	 * 
	 * @param servletContext
	 *            the servlet context
	 */
	@PostConstruct
	public void startup() {
		synchronized (isInited) {
			// init all plugins
			if (!isInited) {
				PluginRepository.getInstance().startPlugins();
				isInited = true;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.StartupService#destory(javax.servlet.ServletContext)
	 */
	@PreDestroy
	public void destory() {
		synchronized (isInited) {
			if (!isInited) {
				return;
			}
			PluginRepository.getInstance().stopPlugins();
		}
	}

}
