/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.handler.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * 
 * Spring 上下文
 * 
 */
@Component
public final class ContextServiceLocator implements ApplicationContextAware {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(ContextServiceLocator.class);

	@Autowired
	private static ApplicationContext applicationContext; // Spring应用上下文环境

	/**
	 * 实现ApplicationContextAware接口的回调方法，设置上下文环境
	 * 
	 * @param applicationContext
	 * @throws BeansException
	 */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextServiceLocator.applicationContext = applicationContext;
		//printBeanFactory(applicationContext);
	}
	
	


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.util.ServiceLocatorIF#getBean(java.lang.String)
	 */

	public static Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.util.ServiceLocatorIF#getBean(java.lang.Class,
	 * java.lang.String)
	 */

	public static <T> T getBean(Class<T> type, String bean) {
		return applicationContext.getBean(bean, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.util.ServiceLocatorIF#getBean(java.lang.Class)
	 */

	public static <T> T getBean(Class<T> type) {
		return applicationContext.getBean(type);
	}


	/**
	 * 如果BeanFactory包含一个与所给名称匹配的bean定义，则返回true
	 * 
	 * @param name
	 * @return boolean
	 */
	public static boolean containsBean(String name) {
		return applicationContext.containsBean(name);
	}

	/**
	 * 判断以给定名字注册的bean定义是一个singleton还是一个prototype。
	 * 如果与给定名字相应的bean定义没有被找到，将会抛出一个异常（NoSuchBeanDefinitionException）
	 * 
	 * @param name
	 * @return boolean
	 * @throws NoSuchBeanDefinitionException
	 */
	public static boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
		return applicationContext.isSingleton(name);
	}

	/**
	 * @param name
	 * @return Class 注册对象的类型
	 * @throws NoSuchBeanDefinitionException
	 */
	public static Class<?> getType(String name) throws NoSuchBeanDefinitionException {
		return applicationContext.getType(name);
	}

	/**
	 * 如果给定的bean名字在bean定义中有别名，则返回这些别名
	 * 
	 * @param name
	 * @return
	 * @throws NoSuchBeanDefinitionException
	 */
	public static String[] getAliases(String name) throws NoSuchBeanDefinitionException {
		return applicationContext.getAliases(name);
	}

	/**
	 * 找到一个类型的class的所有的bean的定义
	 * @param type
	 * @return
	 */
	public static <T> Map<String, T> getBeansOfType(@Nullable Class<T> type) {
		return applicationContext.getBeansOfType(type);
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	

}
