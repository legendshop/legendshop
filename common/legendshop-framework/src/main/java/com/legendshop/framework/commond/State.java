/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.commond;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 */
public interface State extends java.io.Serializable {

	/** The OK. */
	public static String OK = "0";

	/**
	 * 返回业务方法运行时出错代码.
	 * 
	 * @return String
	 */
	public String getErrCode();

	/**
	 * 设置错误代码.
	 * 
	 * @param errCode
	 *            the new err code
	 */
	public void setErrCode(String errCode);

	/**
	 * 返回业务方法调用时抛出的异常.
	 * 
	 * @return Throwable
	 */
	public Throwable getThrowable();

	/**
	 * Sets the throwable.
	 * 
	 * @param throwable
	 *            the new throwable
	 */
	public void setThrowable(Throwable throwable);

	/**
	 * Checks if is oK.
	 * 
	 * @return boolean
	 */
	public boolean isOK();

}
