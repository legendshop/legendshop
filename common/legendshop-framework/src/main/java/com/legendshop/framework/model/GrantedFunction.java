/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.model;

import java.io.Serializable;

/**
 *用户的权限，扩展于spring security的role
 */
public interface GrantedFunction extends Serializable {

	/**
	 * Gets the function.
	 * 
	 * @return the function
	 */
	public String getFunction();
}
