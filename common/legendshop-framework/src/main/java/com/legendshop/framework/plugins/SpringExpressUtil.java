/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.plugins;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.legendshop.util.AppUtils;

/**
 * Spring 表达式计算，提取表达式其中的字符
 */
public class SpringExpressUtil {

	private static final String BLOCK_PATTERN = "\\${.*?}"; // 外面包有{}的任何字符

	/**
	 * 默认采用spring的 '${}' 格式，取中间的值
	 * 
	 * @param text
	 * @return
	 * @throws MalformedPatternException
	 */
	public static String convert(String text) {
		return convert(text, BLOCK_PATTERN);
	}

	/**
	 * 去掉外面的大括号，并取里面的值.
	 * 
	 */
	public static String convert(String text, String patternString) {
		PatternMatcher matcher;
		PatternCompiler compiler;
		Pattern pattern = null;
		PatternMatcherInput input;
		MatchResult result;
		compiler = new Perl5Compiler();
		String resultStr = "";
		try {
			pattern = compiler.compile(patternString);
		} catch (MalformedPatternException e) {
			return resultStr;
		}
		matcher = new Perl5Matcher();
		input = new PatternMatcherInput(text.toString());
		while (matcher.contains(input, pattern)) {
			result = matcher.getMatch();
			resultStr = text.substring(result.beginOffset(0) + 2, result.endOffset(0) - 1);
			if (AppUtils.isNotBlank(resultStr)) {
				resultStr = resultStr.trim();
			}
		}
		return resultStr;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the args
	 * @throws MalformedPatternException
	 */
	public static void main(String[] args) throws MalformedPatternException {
		String text = "     ${  BBB  }  ";
		System.out.println("返回结果： '" + convert(text, BLOCK_PATTERN) + "'");

	}

}
