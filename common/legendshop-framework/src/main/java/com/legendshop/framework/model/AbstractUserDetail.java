/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.model;

import java.util.Collection;

/**
 * 扩展的Spring security用户
 * 
 */
public interface AbstractUserDetail{

	/**
	 * 是否是商家
	 * @return
	 */
	public boolean isShopUser();
	
	

	/**
	 * 获取权限
	 */
	public Collection<GrantedFunction> getFunctions();
	
	/**
	 * 用户登录类型,参考LoginUserType
	 */
	public String getLoginUserType();

}
