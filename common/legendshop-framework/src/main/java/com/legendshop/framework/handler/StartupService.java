package com.legendshop.framework.handler;

public interface StartupService {
	/**
	 * 初始化
	 * 
	 * @param servletContext
	 */
	public void startup();

	/**
	 * 系统关闭
	 * 
	 * @param servletContext
	 */
	public void destory();
}
