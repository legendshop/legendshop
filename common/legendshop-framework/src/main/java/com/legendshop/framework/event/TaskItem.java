/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.event;

/**
 * 为了提高性能，多线程用的任务
 * 
 */
public interface TaskItem {

	/**
	 * Execute.
	 */
	public void execute();

}
