package com.legendshop.framework.plugins;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Decrypt {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(Decrypt.class);
	
	// 这些对象在构造函数中设置，以后loadClass()方法将利用它们解密类
	private SecretKey key;
	private Cipher cipher;

	public Decrypt() {
		
	}
	
	
	// 构造函数：设置解密所需要的对象
	public Decrypt(SecretKey key) throws GeneralSecurityException,
			IOException {
		try {
			this.key = key;
			String algorithm = "DES";
			SecureRandom sr = new SecureRandom();
			cipher = Cipher.getInstance(algorithm);
			cipher.init(Cipher.DECRYPT_MODE, key, sr);
		} catch (Exception e) {
			log.error("",e);
		}

	}

	/**
	 * 解密
	 * @param classData
	 * @return
	 */
	public byte[] doFinal(byte[] classData) {
		byte decryptedClassData[] = null;
		try {
			decryptedClassData = cipher.doFinal(classData);
		} catch (Exception  e) {
		}
		return decryptedClassData;
	}


}
