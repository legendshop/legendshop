/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.model;

/**
 * 代表用户权限
 */
public class GrantedFunctionImpl implements GrantedFunction, Comparable<GrantedFunction> {
	private static final long serialVersionUID = -5538145818148135353L;
	/** The function. */
	private String function;

	/**
	 * Instantiates a new granted function impl.
	 * 
	 * @param function
	 *            the function
	 */
	public GrantedFunctionImpl(String function) {
		super();
		this.function = function;
	}

	/**
	 * Instantiates a new granted function impl.
	 */
	protected GrantedFunctionImpl() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.security.GrantedFunction#getFunction()
	 */
	public String getFunction() {
		return this.function;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) {
			return obj.equals(this.function);
		}

		if (obj instanceof GrantedFunction) {
			GrantedFunction attr = (GrantedFunction) obj;

			return this.function.equals(attr.getFunction());
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.function.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.function;
	}

	public int compareTo(GrantedFunction function) {
		return function.getFunction().compareTo(this.function);
	}

}
