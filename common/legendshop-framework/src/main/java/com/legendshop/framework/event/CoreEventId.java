package com.legendshop.framework.event;

public enum CoreEventId implements BaseEventId {

	/**
	 * 根据账号激活license
	 */
	LICENSE_STATUS_CHECK_EVENT("LICENSE_STATUS_CHECK"),

	/**
	 * 根据激活码更新license
	 */
	LICENSE_UPDATE_BY_KEY_EVENT("LICENSE_UPDATE_BY_KEY"),


	FUNCTION_CHECK_EVENT("FUNCTION_CHECK"),

	// 系统启动
	SYS_INIT_EVENT("SYS_INIT"),

	// 系统关闭
	SYS_DESTORY_EVENT("SYS_DESTORY"),

	/**
	 * log event
	 **/
	FIRE_EVENT("FIRE_EVENT") ;

	/** The value. */
	private final String value;

	public String getEventId() {
		return this.value;
	}

	private CoreEventId(String value) {
		this.value = value;
	}

	public boolean instance(String name) {
		// TODO Auto-generated method stub
		return false;
	}

}
