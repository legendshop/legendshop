/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.plugins.Plugin;
import com.legendshop.framework.plugins.PluginManager;
import com.legendshop.framework.plugins.PluginRuntimeException;
import com.legendshop.framework.plugins.PluginStatusEnum;
import com.legendshop.util.AppUtils;

/**
 * 插件仓库
 */
public class PluginRepository implements PluginManager {
	
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(PluginRepository.class);

    /** 私有化实例. */
	private static PluginRepository instance = null;

	/** The plugins. */
	private List<Plugin> plugins = null;

	/**
	 * 构造函数
	 * 
	 */
	private PluginRepository() {
	}

	/**
	 * Gets the single instance of PluginRepository.
	 * 
	 * @return single instance of PluginRepository
	 */
	public static PluginManager getInstance() {
		if (instance == null) {
			synchronized (PluginRepository.class) {
				if (instance == null) {
					instance = new PluginRepository();
				}
			}
		}
		return instance;
	}

	/**
	 * Adds the plugin list.
	 * 
	 * @param plugin the plugin
	 */
	public void registerPlugin(Plugin plugin) {
		if (plugins == null) {
			plugins = new ArrayList<Plugin>();
		}
		plugins.add(plugin);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.plugins.PluginManager#startPlugins(javax.servlet.
	 * ServletContext)
	 */
	public synchronized void startPlugins() {
		if (AppUtils.isBlank(plugins)) {// 如果
			return;
		}
		List<Plugin> runningPlugins = new ArrayList<Plugin>();
		for (Plugin plugin : plugins) {
			if (plugin.getPluginConfig().getStatus().equals(PluginStatusEnum.Y)) {
				runningPlugins.add(plugin);
			}

		}
		// validate plugins
		String result = validatePlugin(runningPlugins);
		if (AppUtils.isNotBlank(result)) {
			throw new RuntimeException("Plugin validate failed: " + result);
		}

		for (Plugin plugin : runningPlugins) {
			log.info("start to init plugin {}, version {}", plugin.getPluginConfig().getPulginId(),
					plugin.getPluginConfig().getPulginVersion());
			plugin.bind();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.plugins.PluginManager#registerPlugins(com.legendshop
	 * .core.plugins.Plugin)
	 */
	public void registerPlugins(Plugin plugin) {
		// 在线或停止状态也要加入
		if (PluginStatusEnum.S.equals(plugin.getPluginConfig().getStatus())
				|| PluginStatusEnum.Y.equals(plugin.getPluginConfig().getStatus())) {
			if (!plugins.contains(plugin)) {
				plugins.add(plugin);
			} else {
				throw new PluginRuntimeException(
						"Plugin '" + plugin.getClass().getSimpleName() + "' had been registed");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.plugins.PluginManager#stoptPlugins(javax.servlet.
	 * ServletContext)
	 */
	public void stopPlugins() {
		for (Plugin plugin : plugins) {
			if (plugin.getPluginConfig().getStatus().equals(PluginStatusEnum.Y)) {
				log.info("Start to destory plugin {}, version {}", plugin.getPluginConfig().getPulginId(),
						plugin.getPluginConfig().getPulginVersion());
				plugin.unbind();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.plugins.PluginManager#turnOn(java.lang.String)
	 */
	public String turnOn(String pluginId) {
		for (Plugin plugin : plugins) {
			if (plugin.getPluginConfig().getPulginId().equals(pluginId)) {
				log.info("Turn on plugin {}, version {}", plugin.getPluginConfig().getPulginId(),
						plugin.getPluginConfig().getPulginVersion());
				plugin.getPluginConfig().setStatus(PluginStatusEnum.Y);
			}
		}
		return PluginStatusEnum.Y.name();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.plugins.PluginManager#turnOff(java.lang.String)
	 */
	public String turnOff(String pluginId) {
		for (Plugin plugin : plugins) {
			if (plugin.getPluginConfig().getPulginId().equals(pluginId)) {
				log.info("Turn off plugin {}, version {}", plugin.getPluginConfig().getPulginId(),
						plugin.getPluginConfig().getPulginVersion());
				plugin.getPluginConfig().setStatus(PluginStatusEnum.N);
			}
		}
		return PluginStatusEnum.N.name();
	}

	/**
	 * @return the plugins
	 */
	public List<Plugin> getPlugins() {
		return plugins;
	}

	public boolean isPluginRunning(String pluginId) {
		if (AppUtils.isBlank(pluginId) || plugins == null) {
			return false;
		}
		for (Plugin plugin : plugins) {
			if (pluginId.equals(plugin.getPluginConfig().getPulginId())
					&& (plugin.getPluginConfig().getStatus().equals(PluginStatusEnum.Y))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 验证插件的相互关系
	 * 
	 * @param runningPlugins
	 * @return
	 */
	private String validatePlugin(List<Plugin> runningPlugins) {
		StringBuilder result = new StringBuilder();

		List<PluginRelationShip> depends = new ArrayList<PluginRelationShip>();
		List<PluginRelationShip> excludes = new ArrayList<PluginRelationShip>();
		Map<String, Plugin> pluginMap = new HashMap<String, Plugin>();

		for (Plugin plugin : runningPlugins) {
			pluginMap.put(plugin.getPluginConfig().getPulginId(), plugin);

			if (AppUtils.isNotBlank(plugin.getPluginConfig().getDepends())) {
				depends.add(new PluginRelationShip(plugin.getPluginConfig().getPulginId(),
						plugin.getPluginConfig().getDepends()));
			}

			if (AppUtils.isNotBlank(plugin.getPluginConfig().getExcludes())) {
				excludes.add(new PluginRelationShip(plugin.getPluginConfig().getPulginId(),
						plugin.getPluginConfig().getExcludes()));
			}
		}

		// check depends
		if (AppUtils.isNotBlank(depends)) {
			for (PluginRelationShip pluginRelationShip : depends) {
				String[] dependPluginIds = pluginRelationShip.getRelationName().split(",");
				StringBuilder requiredPlguinId = new StringBuilder();
				for (String pluginId : dependPluginIds) {
					if (pluginMap.get(pluginId.trim()) == null) {
						requiredPlguinId.append(" ").append(pluginId);
					}
				}
				if (requiredPlguinId.length() > 0) {
					result.append("\nPlugin '").append(pluginRelationShip.getPluginName())
							.append("' miss depending plguins: ").append(requiredPlguinId);
				}
			}
		}

		// check excludes
		if (AppUtils.isNotBlank(excludes)) {
			for (PluginRelationShip pluginRelationShip : excludes) {
				String[] excludePluginIds = pluginRelationShip.getRelationName().split(",");
				StringBuilder excludePlguinId = new StringBuilder();
				for (String pluginId : excludePluginIds) {
					if (pluginMap.get(pluginId.trim()) != null) {
						excludePlguinId.append(" ").append(pluginId);
					}
				}

				if (excludePlguinId.length() > 0) {
					result.append("\nPlugin '").append(pluginRelationShip.getPluginName())
							.append("' conflict with plguins: ").append(excludePlguinId);
				}
			}
		}

		return result.toString();
	}

}

class PluginRelationShip {
	private String pluginName;
	private String relationName;

	public PluginRelationShip(String pluginName, String relationName) {
		this.pluginName = pluginName;
		this.relationName = relationName;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

}
