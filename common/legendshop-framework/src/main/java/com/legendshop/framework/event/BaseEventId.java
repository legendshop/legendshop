/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.event;

/**
 * 事件Id基类.
 */
public interface BaseEventId {

	/**
	 * 获取事件Id.
	 * 
	 * @return the event id
	 */
	public String getEventId();

	/**
	 * 返回事件的实例.
	 * 
	 * @return true, if successful
	 */
	public boolean instance(String name);
}
