/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.handler;

import java.util.Date;

/**
 * The Interface LicenseManger.
 */
public interface LicenseManger {
	
	public boolean updateLicense(String cert);
	
	public Date getInstallDate();

	public String getVersion();

	String getLicenseType();

	String getDomainName();

}
