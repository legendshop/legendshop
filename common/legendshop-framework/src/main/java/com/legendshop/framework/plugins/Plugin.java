/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.plugins;

/**
 * 代表一个插件.
 */
public interface Plugin {

	/**
	 * 系统启动时调用.
	 */
	public void bind();

	/**
	 * 系统关闭时调用
	 */
	public void unbind();

	/**
	 * 更改插件状态.
	 *
	 */
	public void updateStatus(PluginStatusEnum status);

	/**
	 * 获取插件的配置.
	 * 
	 * @return the plugin config
	 */
	public PluginConfig getPluginConfig();
	
	/**
	 *  设置插件的配置
	 */
	public void setPluginConfig(PluginConfig pluginConfig);

}
