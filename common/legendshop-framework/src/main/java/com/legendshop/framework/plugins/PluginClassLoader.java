/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.plugins;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.commond.ClientException;

/**
 * The Class PluginClassLoader.
 */
public class PluginClassLoader extends ClassLoader {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(PluginClassLoader.class);

	private Decrypt decrypt;

	private static Object lock = new Object();

	private Map<String, Class<?>> loadedClassPool = new HashMap<String, Class<?>>();

	/**
	 * Construtor.
	 * 
	 * @param urls
	 *            Array of urls with own libraries and all exported libraries of
	 *            plugins that are required to this plugin
	 * @param parent
	 *            the parent
	 */
	public PluginClassLoader(ClassLoader parent) {
		super(parent);
	}
	
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return loadClass(name, false);
    }

	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		Class<?> claz = null;
		if (loadedClassPool.containsKey(name)) {
			claz = this.loadedClassPool.get(name);
		} else {
			try {
				if (claz == null) {
					claz = super.loadClass(name, false);
				}
			} catch (Exception e) {
			} catch (ClassFormatError e1) {
			}
			try {
				if (claz == null) {
					claz = loadByLSClassLoader(name);
				}
			} catch (Exception e) {
			}
		}
		if (resolve) {
			resolveClass(claz);
		}
		return claz;
	}

	private Class<?> loadByLSClassLoader(String name) {
		Class<?> result = null;
		try {
			getDecrypt();
			result = loadedClassPool.get(name);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	private Decrypt getDecrypt() throws Exception {
		if (decrypt == null) {
			synchronized (lock) {
				if (decrypt == null) {
					// 读取密匙
					byte rawKey[] = readFile("key.data");

					if (rawKey == null) {
						decrypt = new Decrypt();
						return decrypt;
					}

					DESKeySpec dks = new DESKeySpec(rawKey);
					SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
					SecretKey key = keyFactory.generateSecret(dks);
					decrypt = new Decrypt(key);
					String path = this.getClass().getResource("/").getPath(); // classes目录
					File list = new File(path);
					File[] files = list.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							boolean result = name.endsWith(".zip");
							return result;
						}
					});
					if (files != null && files.length > 0) {
						for (File file : files) {
							readZipFile(file);
						}

					} else {
						log.warn("Framework zip can not be found in path: " + path);
					}

				}
			}
		}
		return decrypt;
	}

	// 把key.data文件读入byte数组
	private byte[] readFile(String fileName) throws IOException {
		InputStream is = null;// 定义输入流is
		try {
			if (fileName.startsWith("/"))
				// 用getResourceAsStream()方法用于定位并打开外部文件。
				is = PluginClassLoader.class.getResourceAsStream(fileName);
			else
				is = PluginClassLoader.class.getResourceAsStream("/" + fileName);
		} catch (Exception e) {
		}

		if (is == null) {
			throw new ClientException("No key data found");
		}

		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
		byte[] buff = new byte[100];
		int rc = 0;
		while ((rc = is.read(buff, 0, 100)) > 0) {
			swapStream.write(buff, 0, rc);
		}
		swapStream.close();
		is.close();
		return swapStream.toByteArray();
	}

	private void readZipFile(File file) {
		try {
			ZipFile zf = new ZipFile(file);
			InputStream in = new BufferedInputStream(new FileInputStream(file));
			ZipInputStream zin = new ZipInputStream(in);
			ZipEntry ze;
			while ((ze = zin.getNextEntry()) != null) {
				if (ze.isDirectory()) {
				} else {
					String name = ze.getName();
					if (name.endsWith(".class")) {
						byte[] rawData = readContent(zf, ze);
						if (rawData != null) {
							byte[] classData = getDecrypt().doFinal(rawData);
							String clazzName = name.replace("/", ".").substring(0, name.lastIndexOf(".class"));
							// log.error("加载 clazzName " + clazzName);
							Class<?> claz = defineClass(clazzName, classData, 0, classData.length);
							// log.error("加载完毕 clazzName " + clazzName);
							loadedClassPool.put(clazzName, claz);
						}
					}
				}
			}
			zin.closeEntry();
			if (zin != null) {
				zin.close();
			}
		} catch (Exception e) {
			log.error("readZipFile error", e);
		}

	}

	private byte[] readContent(ZipFile zipFile, ZipEntry entry) throws IOException {
		InputStream input = zipFile.getInputStream(entry);
		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
		byte[] buff = new byte[100];
		int rc = 0;
		while ((rc = input.read(buff, 0, 100)) > 0) {
			swapStream.write(buff, 0, rc);
		}
		swapStream.close();
		input.close();
		return swapStream.toByteArray();
	}

}
