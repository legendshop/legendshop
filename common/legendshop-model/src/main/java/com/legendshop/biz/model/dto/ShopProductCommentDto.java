/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 产品评论表Dto.
 */
@ApiModel(value="ShopProductCommentDto") 
public class ShopProductCommentDto implements Serializable {

	private static final long serialVersionUID = 6317298566845679559L;
	
	@ApiModelProperty(value="商品Id")  
	private Long prodId;

	@ApiModelProperty(value="商品名称")  
	private String prodName;

	@ApiModelProperty(value="图片")  
	private String pic;
	
	@ApiModelProperty(value="评分")  
	private Integer reviewScores;
	
	@ApiModelProperty(value="评论")  
	private Integer comments;
	
	/**  平均得分 **/
	@ApiModelProperty(value="平均得分")  
	private double avgScore;
	
	@ApiModelProperty(value="价格") 
	private Double price;
	/**
	 * Instantiates a new product comment.
	 */
	public ShopProductCommentDto(){
		
	}
	
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}
	
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}
	
	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}
	
	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	/**
	 * Gets the review scores.
	 *
	 * @return the review scores
	 */
	public Integer getReviewScores() {
		return reviewScores;
	}
	
	/**
	 * Sets the review scores.
	 *
	 * @param reviewScores the new review scores
	 */
	public void setReviewScores(Integer reviewScores) {
		this.reviewScores = reviewScores;
	}
	
	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public Integer getComments() {
		return comments;
	}
	
	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public double getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}