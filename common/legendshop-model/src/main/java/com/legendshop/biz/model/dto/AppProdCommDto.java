package com.legendshop.biz.model.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** 用户评论  **/
@ApiModel(value="用户评论") 
public class AppProdCommDto implements Serializable {

	private static final long serialVersionUID = 5030172556415667984L;

	/** 评论ID **/
	@ApiModelProperty(value="评论ID")  
	private Long id;
	
	/** 商品ID */
	@ApiModelProperty(value="商品ID") 
	private Long prodId;
	
	/** 用户ID */
	@ApiModelProperty(value="用户ID") 
	private String userId;
	
	/** 用户名 */
	@ApiModelProperty(value="用户名") 
	private String userName;
	
	/** 用户头像 */
	@ApiModelProperty(value="用户头像") 
	private String portrait;
	
	/** 用户等级 */
	@ApiModelProperty(value="用户等级") 
	private String gradeName;
	
	/** 评论内容 */
	@ApiModelProperty(value="评论内容") 
	private String content;
	
	/** 评论图片 */
	@ApiModelProperty(value="评论图片",hidden = true) 
	private String photos;
	
	/** 评论图片列表 */
	@ApiModelProperty(value="评论图片列表") 
	private List<String> photoList;
	
	/** 评论时间 */
	@ApiModelProperty(value="评论时间") 
	private Date addtime;
	
	/** 评论得分 */
	@ApiModelProperty(value="评论得分") 
	private Integer score;
	
	/** 评论状态   是否显示，1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1*/
	@ApiModelProperty(value="评论状态,是否显示，1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1") 
	private Integer status;
	
	/** 购买属性 */
	@ApiModelProperty(value="购买属性") 
	private String attribute;
	
	/** 购物时间 */
	@ApiModelProperty(value="购物时间") 
	private Date buyTime;

	/** 有用的评论点赞计数  */
	@ApiModelProperty(value="有用的评论点赞计数") 
	private Integer usefulCounts;
	
	/** 当前用户是否给该评论点赞  */
	@ApiModelProperty(value="当前用户是否给该评论点赞") 
	private boolean isAlreadyUseful;
	
	/** 回复次数 */
	@ApiModelProperty(value="回复次数") 
	private Integer replayCounts;
	
	/** 是否匿名 */
	@ApiModelProperty(value="是否匿名") 
	private boolean isAnonymous;
	
	/** 商家是否已回复 */
	@ApiModelProperty(value="商家是否已回复") 
	private Boolean isReply;
	
	/** 商家回复内容 */
	@ApiModelProperty(value="商家回复内容") 
	private String shopReplyContent;
	
	/** 商家回复时间 */
	@ApiModelProperty(value="商家回复时间") 
	private Date shopReplyTime;
	
	/** 是否已追加 */
	@ApiModelProperty(value="是否已追加") 
	private Boolean isAddComm;
	
	/** 追加主键ID */
	@ApiModelProperty(value="追加主键ID") 
	private Long addId; 
		
	/** 追加评论内容 */
	@ApiModelProperty(value="追加评论内容") 
	private String addContent; 
	
	/** 追加评论图片 */
	@ApiModelProperty(value="追加评论图片",hidden = true) 
	private String addPhotos;
	
	/** 追加评论图片列表 */
	@ApiModelProperty(value="追加评论图片列表") 
	private List<String> addPhotoList;
	
	/** 追加评论状态   是否显示，1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1*/
	@ApiModelProperty(value="追加评论状态,是否显示，1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1") 
	private Integer addStatus;
		
	/** 追加创建时间 */
	@ApiModelProperty(value="追加创建时间") 
	private Date addAddTime;
	
	/** 商家是否已回复追加评论 */
	@ApiModelProperty(value="商家是否已回复追加评论") 
	private Boolean addIsReply;
		
	/** 商家追加回复 */
	@ApiModelProperty(value="商家追加回复") 
	private String addShopReplyContent;
		
	/** 商家追加回复时间 */
	@ApiModelProperty(value="商家追加回复时间") 
	private Date addShopReplyTime; 
	
	/** 多少天后追加的评论 */
	@ApiModelProperty(value="多少天后追加的评论,如果为0则显示为用户当天追加评论")
	private Integer AppendDays;

	@ApiModelProperty(value="订单号")
	private String subNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Date getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	public Integer getUsefulCounts() {
		return usefulCounts;
	}

	public void setUsefulCounts(Integer usefulCounts) {
		this.usefulCounts = usefulCounts;
	}

	public Integer getReplayCounts() {
		return replayCounts;
	}

	public void setReplayCounts(Integer replayCounts) {
		this.replayCounts = replayCounts;
	}

	public boolean getIsAnonymous() {
		return isAnonymous;
	}

	public void setIsAnonymous(boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public Boolean getIsReply() {
		return isReply;
	}

	public void setIsReply(Boolean isReply) {
		this.isReply = isReply;
	}

	public String getShopReplyContent() {
		return shopReplyContent;
	}

	public void setShopReplyContent(String shopReplyContent) {
		this.shopReplyContent = shopReplyContent;
	}

	public Date getShopReplyTime() {
		return shopReplyTime;
	}

	public void setShopReplyTime(Date shopReplyTime) {
		this.shopReplyTime = shopReplyTime;
	}

	public Boolean getIsAddComm() {
		return isAddComm;
	}

	public void setIsAddComm(Boolean isAddComm) {
		this.isAddComm = isAddComm;
	}

	public Long getAddId() {
		return addId;
	}

	public void setAddId(Long addId) {
		this.addId = addId;
	}

	public String getAddContent() {
		return addContent;
	}

	public void setAddContent(String addContent) {
		this.addContent = addContent;
	}

	public String getAddPhotos() {
		return addPhotos;
	}

	public void setAddPhotos(String addPhotos) {
		this.addPhotos = addPhotos;
	}

	public Integer getAddStatus() {
		return addStatus;
	}

	public void setAddStatus(Integer addStatus) {
		this.addStatus = addStatus;
	}

	public Date getAddAddTime() {
		return addAddTime;
	}

	public void setAddAddTime(Date addAddTime) {
		this.addAddTime = addAddTime;
	}

	public Boolean getAddIsReply() {
		return addIsReply;
	}

	public void setAddIsReply(Boolean addIsReply) {
		this.addIsReply = addIsReply;
	}

	public String getAddShopReplyContent() {
		return addShopReplyContent;
	}

	public void setAddShopReplyContent(String addShopReplyContent) {
		this.addShopReplyContent = addShopReplyContent;
	}

	public Date getAddShopReplyTime() {
		return addShopReplyTime;
	}

	public void setAddShopReplyTime(Date addShopReplyTime) {
		this.addShopReplyTime = addShopReplyTime;
	}

	public void setAnonymous(boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public List<String> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<String> photoList) {
		this.photoList = photoList;
	}

	public List<String> getAddPhotoList() {
		return addPhotoList;
	}

	public void setAddPhotoList(List<String> addPhotoList) {
		this.addPhotoList = addPhotoList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public boolean getIsAlreadyUseful() {
		return isAlreadyUseful;
	}

	public void setIsAlreadyUseful(boolean isAlreadyUseful) {
		this.isAlreadyUseful = isAlreadyUseful;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * 获取多少天后追加的评论
	 * @return
	 */
	public Integer getAppendDays(){
		if(null != this.isAddComm && this.isAddComm){
			if(null != this.addtime && null != this.addAddTime){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(this.addtime);
				
				int day1 = calendar.get(Calendar.DAY_OF_YEAR);
				
				calendar.setTime(this.addAddTime);
				int day2 = calendar.get(Calendar.DAY_OF_YEAR);
				
				return day2 - day1;
			}
		}
		
		return null;
	}
	
	public void setAppendDays(Integer appendDays) {
		this.AppendDays = appendDays;
	}
}
