package com.legendshop.biz.model.dto;

import com.legendshop.util.AppUtils;

/**
 * APP端商品评论查询参数 
 *
 */
public class AppQueryProductCommentsParams {
	
	private Long prodId;
	
	private String shopUserName;
	
	private Integer curPageNO;
	
	/** 条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append, 待回复: waitReply */
	private String condition;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public String getShopUserName() {
		return shopUserName;
	}

	public void setShopUserName(String shopUserName) {
		this.shopUserName = shopUserName;
	}

	public Integer getCurPageNO() {
		if(null == curPageNO){
			curPageNO = 1;
		}
		return curPageNO;
	}

	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}

	public String getCondition() {
		if(AppUtils.isBlank(condition)){
			condition = "all";
		}
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
