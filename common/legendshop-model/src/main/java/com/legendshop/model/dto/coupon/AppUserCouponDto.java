/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.coupon;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户礼券Dto
 *
 */
@ApiModel(value="用户礼券Dto")
public class AppUserCouponDto {

	/** 用户礼券ID */
	@ApiModelProperty(value="用户礼券ID")
	private Long userCouponId;

	/** 礼券ID */
	@ApiModelProperty(value="礼券ID")
	private Long couponId;

	/** 礼券名称 */
	@ApiModelProperty(value="礼券名称")
	private String couponName;
	
	/** 劵值满多少金额 */
	@ApiModelProperty(value="劵值满多少金额")
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	@ApiModelProperty(value="劵值减多少金额")
	private Double offPrice; 
	
	/** 优惠券图片 **/
	@ApiModelProperty(value="优惠券图片")
	private String couponPic;
	
	/** 领取来源 */
	@ApiModelProperty(value="领取来源")
	private String getSources; 
	
	/** 领取时间 */
	@ApiModelProperty(value="领取时间")
	private Date getTime; 
		
	/** 使用时间 */
	@ApiModelProperty(value="")
	private Date useTime; 
	
	/** 结束时间*/
	@ApiModelProperty(value="结束时间")
	private Date endDate;
	
	/** 礼券的类型 */
	@ApiModelProperty(value="礼券的类型 ")
	private String couponType; 

	/** 礼券提供方：平台: platform，店铺:shop */
	@ApiModelProperty(value="礼券提供方：平台: platform，店铺:shop")
	private String couponProvider;

	/** 是否选中 */
	@ApiModelProperty(value="是否选中")
	private int selectSts = 0;

	/** 判断是否可用. */
	@ApiModelProperty(value="判断是否可用")
	private String flag;
	
	/** 优惠券使用状态 1:可使用  2:已使用 */
	@ApiModelProperty(value="优惠券使用状态 1:可使用  2:已使用")
	private Integer useStatus; 
	
	/** 关联的商品ID集合**/
	@ApiModelProperty(value="关联的商品ID集合")
	private List<Long> prodIds = new ArrayList<Long>();
	
	/** 关联的店铺ID集合**/
	@ApiModelProperty(value="关联的店铺ID集合")
	private List<Long> shopIds = new ArrayList<Long>();
	
	/** 命中的商品总金额 **/
	@ApiModelProperty(value="命中的商品总金额")
	private Double hitTotalPrice;

	public Long getUserCouponId() {
		return userCouponId;
	}

	public void setUserCouponId(Long userCouponId) {
		this.userCouponId = userCouponId;
	}

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Double getOffPrice() {
		return offPrice;
	}

	public void setOffPrice(Double offPrice) {
		this.offPrice = offPrice;
	}

	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getCouponProvider() {
		return couponProvider;
	}

	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}

	public int getSelectSts() {
		return selectSts;
	}

	public void setSelectSts(int selectSts) {
		this.selectSts = selectSts;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getGetSources() {
		return getSources;
	}

	public void setGetSources(String getSources) {
		this.getSources = getSources;
	}

	public Date getGetTime() {
		return getTime;
	}

	public void setGetTime(Date getTime) {
		this.getTime = getTime;
	}

	public Date getUseTime() {
		return useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	public Integer getUseStatus() {
		return useStatus;
	}

	public void setUseStatus(Integer useStatus) {
		this.useStatus = useStatus;
	}

	public List<Long> getProdIds() {
		return prodIds;
	}

	public void setProdIds(List<Long> prodIds) {
		this.prodIds = prodIds;
	}

	public List<Long> getShopIds() {
		return shopIds;
	}

	public void setShopIds(List<Long> shopIds) {
		this.shopIds = shopIds;
	}

	public Double getHitTotalPrice() {
		return hitTotalPrice;
	}

	public void setHitTotalPrice(Double hitTotalPrice) {
		this.hitTotalPrice = hitTotalPrice;
	}
	
}
