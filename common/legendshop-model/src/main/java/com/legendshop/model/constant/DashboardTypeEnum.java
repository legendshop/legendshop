/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 后台dashboard统计字段
 */
public enum DashboardTypeEnum implements StringEnum {
	
	/** 用户数量 **/
	USER_COUNTS("USER_COUNTS"),
	
	/** 店铺数量 **/
	SHOP_COUNTS("SHOP_COUNTS"),
	
	/** 商品数量 **/
	PROD_COUNTS("PROD_COUNTS"),
	
	/** 订单数量**/
	SUB_COUNTS("SUB_COUNTS"),
	
	/** 咨询数量 **/
	CONSULT_COUNTS("CONSULT_COUNTS"),
	
	/** 举报数量 **/
	ACCUSATION_COUNTS("ACCUSATION_COUNTS"),
	
	/** 品牌数量**/
	BRAND_COUNTS("BRAND_COUNTS");

	private final String value;

	private DashboardTypeEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}


}
