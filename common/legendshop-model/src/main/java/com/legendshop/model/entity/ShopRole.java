package com.legendshop.model.entity;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家角色表
 */
@Entity
@Table(name = "ls_shop_role")
public class ShopRole implements GenericEntity<Long> {

	private static final long serialVersionUID = -5958814685607076650L;

	/** 主键 */
	private Long id; 
		
	/**  */
	private Long shopId; 
		
	/** 名称 */
	private String name; 
		
	/** 建立时间 */
	private Date recDate; 
	
	/** 菜单组标签**/
	private List<String> menuGroupLabels;
	
	/** 菜单标签**/
	private List<String> menuLabels;
	
	/** 权限标识（check为查看，editor为编辑）以逗号分隔  */
	private List<String> menuFunctions;
		
	
	public ShopRole() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_ROLE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}

	@Transient
	public List<String> getMenuGroupLabels() {
		return menuGroupLabels;
	}

	public void setMenuGroupLabels(List<String> menuGroupLabels) {
		this.menuGroupLabels = menuGroupLabels;
	}

	@Transient
	public List<String> getMenuLabels() {
		return menuLabels;
	}

	public void setMenuLabels(List<String> menuLabels) {
		this.menuLabels = menuLabels;
	}

	@Transient
	public List<String> getMenuFunctions() {
		return menuFunctions;
	}

	public void setMenuFunctions(List<String> menuFunctions) {
		this.menuFunctions = menuFunctions;
	}

} 
