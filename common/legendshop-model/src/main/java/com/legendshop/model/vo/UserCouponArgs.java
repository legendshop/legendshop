package com.legendshop.model.vo;

/**
 * 批量用户赠送券用户范围筛选条件
 *
 */
public class UserCouponArgs {

	private Long couponId;
	private int filMethod; //Tap方法
	
	
	//普通模式
	private String name;
	private String attribute;
	
	//活跃度筛选模式
	private String logNumberFilter;
	private int logNumber;
	private String totalAmountFilter;
	private double totalAmount;
	private String orderNumberFilter;
	private int orderNumber;
	
	//注册筛选模式
	private String regtime_start;
	private String regtime_end;
	
	private String curPageNO;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	


	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}



	public int getFilMethod() {
		return filMethod;
	}

	public void setFilMethod(int filMethod) {
		this.filMethod = filMethod;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getLogNumberFilter() {
		return logNumberFilter;
	}

	public void setLogNumberFilter(String logNumberFilter) {
		this.logNumberFilter = logNumberFilter;
	}

	public int getLogNumber() {
		return logNumber;
	}

	public void setLogNumber(int logNumber) {
		this.logNumber = logNumber;
	}

	public String getTotalAmountFilter() {
		return totalAmountFilter;
	}

	public void setTotalAmountFilter(String totalAmountFilter) {
		this.totalAmountFilter = totalAmountFilter;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOrderNumberFilter() {
		return orderNumberFilter;
	}

	public void setOrderNumberFilter(String orderNumberFilter) {
		this.orderNumberFilter = orderNumberFilter;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCurPageNO() {
		return curPageNO;
	}

	public void setCurPageNO(String curPageNO) {
		this.curPageNO = curPageNO;
	}

	public String getRegtime_start() {
		return regtime_start;
	}

	public void setRegtime_start(String regtime_start) {
		this.regtime_start = regtime_start;
	}

	public String getRegtime_end() {
		return regtime_end;
	}

	public void setRegtime_end(String regtime_end) {
		this.regtime_end = regtime_end;
	}
	
	
	
}
