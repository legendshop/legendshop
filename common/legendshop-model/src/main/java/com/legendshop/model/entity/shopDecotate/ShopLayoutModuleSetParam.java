package com.legendshop.model.entity.shopDecotate;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * 装修模块参数
 * 
 */
public class ShopLayoutModuleSetParam extends ShopLayoutParam {

	private static final long serialVersionUID = 1704187096175654789L;

	private String title; // 标题名称

	private String fontImg; // 标题背景

	private String fontColor; // 标题颜色

	private String backColor; // 标题背景颜色

	/** The file. */
	private MultipartFile file;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFontImg() {
		return fontImg;
	}

	public void setFontImg(String fontImg) {
		this.fontImg = fontImg;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public String getBackColor() {
		return backColor;
	}

	public void setBackColor(String backColor) {
		this.backColor = backColor;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
