package com.legendshop.model.dto.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app团购分类
 * @author linzh
 */
@ApiModel(value="团购分类") 
public class AppGroupCategoryDto {

	@ApiModelProperty(value="团购分类ID") 
	private Long id;
		
	@ApiModelProperty(value="团购分类名称") 
	private String name;
	
	@ApiModelProperty(value="团购分类排序") 
	private Integer seq; 
	
	@ApiModelProperty(value="团购分类状态 [默认是1，表示正常状态,0为下线状态]") 
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
