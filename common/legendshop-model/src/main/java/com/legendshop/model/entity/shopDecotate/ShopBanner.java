package com.legendshop.model.entity.shopDecotate;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商家默认的轮换图
 *
 */
@Entity
@Table(name = "ls_shop_banner")
public class ShopBanner implements GenericEntity<Long>,Cloneable {

	private static final long serialVersionUID = 269627592525452872L;

	/**  */
	private Long id; 
		
	/**  */
	private Long shopId; 
		
	/**  */
	private String imageFile; 
		
	/**  */
	private String url; 
		
	/**  */
	private Long seq; 
	
	/** The file. */
	private MultipartFile file;
		
	
	public ShopBanner() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_BANNER_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "image_file")
	public String  getImageFile(){
		return imageFile;
	} 
		
	public void setImageFile(String imageFile){
			this.imageFile = imageFile;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}

	@Transient
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	

	
	

} 
