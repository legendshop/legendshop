package com.legendshop.model.entity;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;

import java.util.Date;

/**
 * 首页楼层
 */
@Entity
@Table(name = "ls_sub_floor_item")
public class SubFloorItem implements GenericEntity<Long>, Comparable<SubFloorItem> {

	private static final long serialVersionUID = 1222373715605021669L;

	// ID
	private Long sfiId;

	// 记录时间
	private Date recDate;

	// ls_floor_id[5X商品版式]
	private Long sfId;
	
	//ls_sub_floor_id[混合模式]
	private Long fId;
	

	//
	private Long referId;

	// 类型ID
	private String type;

	// 顺序
	private Integer seq;

	// 商品id
	private String productId;

	// 商品名称
	private String productName;

	// 商品图片
	private String productPic;

	// 商品价格
	private Double productPrice;
	
	// 商品现价
	private Double productCash;

	public SubFloorItem() {
	}

	@Id
	@Column(name = "sfi_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_FLOOR_ITEM_SEQ")
	public Long getSfiId() {
		return sfiId;
	}

	public void setSfiId(Long sfiId) {
		this.sfiId = sfiId;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Column(name = "sf_id")
	public Long getSfId() {
		return sfId;
	}

	public void setSfId(Long sfId) {
		this.sfId = sfId;
	}

	@Column(name = "refer_id")
	public Long getReferId() {
		return referId;
	}

	public void setReferId(Long referId) {
		this.referId = referId;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	
	@Column(name = "f_id")
	public Long getfId() {
		return fId;
	}

	public void setfId(Long fId) {
		this.fId = fId;
	}
	
	private String layout;
	
	
	

	@Transient
	public Long getId() {
		return sfiId;
	}

	public void setId(Long id) {
		this.sfiId = id;
	}

	@Transient
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Transient
	public String getProductPic() {
		return productPic;
	}

	public void setProductPic(String productPic) {
		this.productPic = productPic;
	}

	@Transient
	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	@Transient
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Transient
	public Double getProductCash() {
		return productCash;
	}

	public void setProductCash(Double productCash) {
		this.productCash = productCash;
	}

	@Column(name = "layout")
	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	
	@Override
	public int compareTo(SubFloorItem item) {
		if (item == null || item.getSeq() == null) {
			return -1;
		}else {
			return item.getSeq().compareTo(this.seq);
		}
	}


}
