package com.legendshop.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * APP秒杀活动查询DTO
 */
@Data
@ApiModel(value="APP秒杀活动查询DTO")
public class AppSecKillActivityQueryDTO {

	@ApiModelProperty("当前页码")
	private Integer curPage;

	private Integer pageSize;

	@ApiModelProperty("秒杀开始时间")
	private Date startTime;

	@ApiModelProperty("秒杀活动状态,0 未开始，1 进行中，-1 已结束")
	private Integer status;

	@ApiModelProperty("秒杀活动名称")
	private String name;

}
