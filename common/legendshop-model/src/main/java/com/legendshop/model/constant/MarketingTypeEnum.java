/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 营销类型
 */
public enum MarketingTypeEnum implements IntegerEnum {
	/**  
	 * 满减
	 */
	MAN_JIAN(0),
	
	/**  
	 * 满折
	 */
	MAN_ZE(1),
	
	
	/**  
	 * 限时折扣
	 */
	XIANSHI_ZE(2),
	
	
	/**  
	 * 直降
	 */
	DOWN_PRICE(3),
	
	/**  
	 * 满件包邮
	 */
	FULL_NUM_MAIL(4),
	
	/**  
	 * 满金额包邮
	 */
	FULL_AMOUNT_MAIL(5);
	


	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}
	
	public static MarketingTypeEnum fromCode(Integer code){
			try{
				return values()[code];
			}catch(Exception e){
				return null;
			}
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	MarketingTypeEnum(Integer num) {
		this.num = num;
	}


}
