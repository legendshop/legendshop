package com.legendshop.model.dto;

import java.io.Serializable;


/**
 * 营销活动运营数据统计
 * @author linzh
 */
public class OperateStatisticsDTO implements Serializable {


	/**  */
	private static final long serialVersionUID = 1756056918995169691L;

	
	/** 交易总金额 */
	private Double totalAmount;
	
	/** 成功交易金额 */
	private Double succeedAmount;
	
	/** 成功数量 */
	private Integer succeedCount;
	
	/** 拼团----待成团 */
	private Integer waitFightCount;
	
	/** 活动参与人数  */
	private Integer participants;
	
	/**团购 ---成团人数**/
	private Integer peopleCount;

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getSucceedAmount() {
		return succeedAmount;
	}

	public void setSucceedAmount(Double succeedAmount) {
		this.succeedAmount = succeedAmount;
	}

	public Integer getSucceedCount() {
		return succeedCount;
	}

	public void setSucceedCount(Integer succeedCount) {
		this.succeedCount = succeedCount;
	}

	public Integer getWaitFightCount() {
		return waitFightCount;
	}

	public void setWaitFightCount(Integer waitFightCount) {
		this.waitFightCount = waitFightCount;
	}

	public Integer getParticipants() {
		return participants;
	}

	public void setParticipants(Integer participants) {
		this.participants = participants;
	}

	public Integer getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Integer peopleCount) {
		this.peopleCount = peopleCount;
	}
	
}
