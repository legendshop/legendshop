package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品类型跟参数的关系表
 */
@Entity
@Table(name = "ls_prod_type_param")
public class ProdTypeParam implements GenericEntity<Long> {

	private static final long serialVersionUID = 4221640380341924581L;

	/** 主键 */
	private Long id; 
		
	/** 类型ID */
	private Long typeId; 
		
	/** 参数属性ID */
	private Long propId; 
	
	/** 分组id */
	private Long groupId;
	
	/**排序*/
	private Integer seq;
	
	private String propName;//属性名称
		
	@Column(name = "group_id")
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public ProdTypeParam() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_TYPE_PARAM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "type_id")
	public Long  getTypeId(){
		return typeId;
	} 
		
	public void setTypeId(Long typeId){
			this.typeId = typeId;
		}
		
    @Column(name = "prop_id")
	public Long  getPropId(){
		return propId;
	} 
		
	public void setPropId(Long propId){
			this.propId = propId;
		}

	@Transient
	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}
	


} 
