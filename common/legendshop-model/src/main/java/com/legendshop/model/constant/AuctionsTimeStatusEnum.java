/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.LongEnum;

/**
 * 拍卖的时间状态
 * 
 */
public enum AuctionsTimeStatusEnum implements LongEnum {
	
	/** 未开始 */
	UNSTART(0l),
	
	/** 进行中的拍卖 */
	PROCESSING(1l),

	/** 过期的拍卖 */
	OVER_TIME(2l);


	/** The num. */
	private Long num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.LongEnum#value()
	 */
	public Long value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	AuctionsTimeStatusEnum(Long num) {
		this.num = num;
	}

}
