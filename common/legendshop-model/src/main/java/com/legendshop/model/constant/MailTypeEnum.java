/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 产品状态.
 */
public enum MailTypeEnum implements IntegerEnum {
	// 单独发送
	ONE_TO_ONE(0),

	// 群发
	ONE_TO_MANY(1);

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	MailTypeEnum(Integer num) {
		this.num = num;
	}

}
