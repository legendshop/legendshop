/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.support.GenericEntity;



/**
 * 首页楼层
 */
public class FloorDto implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2045804814438964700L;

	// ID
	/** The fl id. */
	private Long flId;

	// 名称
	/** The name. */
	private String name;

	// 楼层样式
	/** The css style. */
	private String cssStyle;
	
/*	// 内容类型
	private String contentType;

	//下面的内容类型
	private String contentType2;
	
	//右边栏开关
	private Integer leftSwitch;
	*/
	
	
	private String layout;

	
	private Integer seq;
	
	
	//子楼层Item[商品 版式：混合模式]
	private Set<SubFloor> subFloors = null;
	
	//子楼层Item[商品 版式：5X商品]
	private List<SubFloorItem> subFloorItems = null;
	
	//子楼层信息[品牌、广告、分类]
	private  Map<String, List<FloorItem>> floorItems =null;
	
	
	public Long getFlId() {
		return flId;
	}
	public void setFlId(Long flId) {
		this.flId = flId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCssStyle() {
		return cssStyle;
	}
	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}
	
	public Set<SubFloor> getSubFloors() {
		return subFloors;
	}
	
	public Map<String, List<FloorItem>> getFloorItems() {
		return floorItems;
	}

	public Long getId() {
		return flId;
	}
	
	public void setId(Long id) {
		this.flId = id;
	}

	
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public List<SubFloorItem> getSubFloorItems() {
		return subFloorItems;
	}
	
	public void addFloorItems(String floorItemType, List<FloorItem> items) {
		if(floorItems==null){
			floorItems = new HashMap<String, List<FloorItem>>();
		}
		this.floorItems.put(floorItemType, items);
	}
	
	
	public void addSubFloor(SubFloor subFloor) {
		if(subFloors==null){
			subFloors=new TreeSet<SubFloor>(new SubFloorComparator());
		}
		if (this.getFlId().equals(subFloor.getFloorId())) {
			subFloors.add(subFloor);
		}
	}
	
	
	public void addSubFloorItems(SubFloorItem subFloorItem) {
		if(subFloorItems==null){
			subFloorItems=new ArrayList<SubFloorItem>();
		}
		subFloorItems.add(subFloorItem);
	}
	
	public void setSubFloors(Set<SubFloor> subFloors) {
		this.subFloors = subFloors;
	}
	
	public void setSubFloorItems(List<SubFloorItem> subFloorItems) {
		this.subFloorItems = subFloorItems;
	}
	
	
	
	
	
}
