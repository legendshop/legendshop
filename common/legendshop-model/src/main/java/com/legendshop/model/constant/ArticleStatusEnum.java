package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 文章 审核状态
 *
 */
public enum ArticleStatusEnum implements IntegerEnum{
	
	OFFLINE_HIDDEN(0),//下线隐藏
	ONLINE_DISPALY(1),//上线显示
	
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private ArticleStatusEnum(Integer num) {
		this.num = num;
	}
	
}
