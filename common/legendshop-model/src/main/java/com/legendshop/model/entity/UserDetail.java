/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
@Entity
@Table(name = "ls_usr_detail")
public class UserDetail extends AbstractEntity implements GenericEntity<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3648830158220060652L;

	/** The user id. */
	private String userId;

	/** The grade id. */
	private Integer gradeId;

	/** The user name. */
	private String userName;

	/** 昵称 */
	private String nickName;
	
	/** 真实姓名 **/
	private String realName;

	/** The user mail. */
	private String userMail;

	/** The user adds. */
	private String userAdds;

	/** The user tel. */
	private String userTel;

	/** The user postcode. */
	private String userPostcode;
	
	private String payPassword;

	/** The msn. */
	private String msn;

	/** The qq. */
	private String qq;

	/** The qq list. */
	private List qqList;

	/** The fax. */
	private String fax;

	/** The modify time. */
	private Date modifyTime;

	/** The user regtime. */
	private Date userRegtime;

	/** The user regip. */
	private String userRegip;

	/** The user lasttime. */
	private Date userLasttime;

	/** The user lastip. */
	private String userLastip;

	/** The user memo. */
	private String userMemo;

	/** The password. */
	private String password;

	/** The sex. */
	private String sex;

	/** The birth date. */
	private Date birthDate;

	/** The user mobile. */
	private String userMobile;

	/** 用户所在的商城Id，如果是买家就没有shopId. */
	private Long shopId;

	/** The shop detail. */
	private ShopDetail shopDetail;

	/** The register code.注册码 */
	private String registerCode;

	/** The score. 总积分*/
	private Integer score;
	
	/** The total cash. 总钱数 */
	private Double totalCash;
	
	/** The total consume. 总消耗钱数*/
	private Double totalConsume;
	
	/** The user addresses. 用户地址 */
	private Set<UserAddress> userAddresses;
	
	/** The default addr id. 默认地址 */
	private Long defaultAddrId;
	
	/** 从用户信息表中来. */
	private String enabled;
	
    /** The provinceid. */
    protected Integer provinceid;
    
    /** The cityid. */
    protected Integer cityid;
    
    /** The areaid. */
    protected Integer areaid;
    
    /** 身份证号码 **/
    private String idCard;
    
    /** 头像图片**/
    private String portraitPic;
    
    /** 是否允许举报商品 **/
    private Integer accusation;
    
    /** 年 （生日） **/
    private Integer year;
    
    /** 月 （生日） **/
    private Integer month;
    
    /** 日 （生日） **/
    private Integer day;

    /** 二维码 **/
	private String zxingImage;

	/** 个性签名 **/
	private String userSignature;
	
    private Double availablePredeposit; //可用余额  Pre-deposit
	
	private Double freezePredeposit;//冻结余额  Pre-deposit
	
	private String verifyCode;//短信校验码(推广)
	
	private String parentUserName;//分销上级用户
	
	private Date parentBindingTime;//绑定上级时间
	
	private Integer isRegIM; //是否注册环信账号   0：否  1：是

	private Double userCoin;//金币剩余数量
	
	private String haveShop;
	
	private String siteName;
	
    private int source;//用户来源, 0:主动注册, 1:服务人员注册, 2:添加家属, 3:订单导入
 	
 	private boolean isActivated;//是否已经激活
 	
 	private String updatePic;//修改头像

	/**
	 * 店铺状态 ShopStatusEnum
	 */
	private Integer shopStatus;
	/**
	 * Instantiates a new user detail.
	 *
	 */
	public UserDetail(String userId, Integer gradeId, String userName, String nickName, String userMail,
			String userAdds, String userTel, String userPostcode, String msn, String qq, String fax, Date modifyTime,
			Date userRegtime, String userRegip, Date userLasttime, String userLastip, String userMemo, String password,
			String sex, Date birthDate, String userMobile, Long shopId) {
		this.userId = userId;
		this.gradeId = gradeId;
		this.userName = userName;
		this.nickName = nickName;
		this.userMail = userMail;
		this.userAdds = userAdds;
		this.userTel = userTel;
		this.userPostcode = userPostcode;
		this.msn = msn;
		this.qq = qq;
		this.fax = fax;
		this.modifyTime = modifyTime;
		this.userRegtime = userRegtime;
		this.userRegip = userRegip;
		this.userLasttime = userLasttime;
		this.userLastip = userLastip;
		this.userMemo = userMemo;
		this.password = password;
		this.sex = sex;
		this.birthDate = birthDate;
		this.userMobile = userMobile;
		this.shopId = shopId;
	}

	/**
	 * default constructor.
	 */
	public UserDetail() {
	}

	/**
	 * Instantiates a new user detail.
	 * 
	 * @param userDetailMap
	 *            the user detail map
	 */
	public UserDetail(Map userDetailMap) {
		if (userDetailMap != null) {
			this.userId = (String) userDetailMap.get("USER_ID");
			Number grade = (Number) userDetailMap.get("GRADE_ID");
			if (grade != null) {
				this.gradeId = grade.intValue();
			}

			this.userName = (String) userDetailMap.get("USER_NAME");
			;
			this.nickName = (String) userDetailMap.get("NICK_NAME");
			this.userMail = (String) userDetailMap.get("USER_MAIL");
			// this.userAdds = (String)userDetailMap.get("USER_ADDS");
			// this.userTel = (String)userDetailMap.get("USER_TEL");
			// this.userPostcode = (String)userDetailMap.get("USER_POSTCODE");
			// this.msn = (String)userDetailMap.get("MSN");
			// this.qq = (String)userDetailMap.get("QQ");
			// this.fax = (String)userDetailMap.get("FAX");
			this.userRegip = (String) userDetailMap.get("USER_REGIP");
			this.userLastip = (String) userDetailMap.get("USER_LASTIP");
			// this.userMemo = (String)userDetailMap.get("USER_MEMO");
			// this.userMobile = (String)userDetailMap.get("USER_MOBILE");
			Number shop = (Number) userDetailMap.get("SHOP_ID");
			if (shop != null) {
				this.shopId = shop.longValue();
			}
			this.modifyTime = (Date) userDetailMap.get("MODIFY_TIME");
			this.userRegtime = (Date) userDetailMap.get("USER_REGTIME");

			// this.password = password;
			// this.sex = sex;
			// this.birthDate = birthDate;
			// this.userLasttime = userLasttime;

		}

	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	@Column(name="user_id")
	public String getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the grade id.
	 * 
	 * @return the grade id
	 */
	@Column(name = "grade_id")
	public Integer getGradeId() {
		return this.gradeId;
	}

	/**
	 * Sets the grade id.
	 * 
	 * @param gradeId
	 *            the new grade id
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the user mail.
	 * 
	 * @return the user mail
	 */
	@Column(name = "user_mail")
	public String getUserMail() {
		return this.userMail;
	}

	/**
	 * Sets the user mail.
	 * 
	 * @param userMail
	 *            the new user mail
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	/**
	 * Gets the user adds.
	 * 
	 * @return the user adds
	 */
	@Column(name = "user_adds")
	public String getUserAdds() {
		return this.userAdds;
	}

	/**
	 * Sets the user adds.
	 * 
	 * @param userAdds
	 *            the new user adds
	 */
	public void setUserAdds(String userAdds) {
		this.userAdds = userAdds;
	}

	/**
	 * Gets the user tel.
	 * 
	 * @return the user tel
	 */
	@Column(name = "user_tel")
	public String getUserTel() {
		return this.userTel;
	}

	/**
	 * Sets the user tel.
	 * 
	 * @param userTel
	 *            the new user tel
	 */
	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	/**
	 * Gets the user postcode.
	 * 
	 * @return the user postcode
	 */
	@Column(name = "user_postcode")
	public String getUserPostcode() {
		return this.userPostcode;
	}

	/**
	 * Sets the user postcode.
	 * 
	 * @param userPostcode
	 *            the new user postcode
	 */
	public void setUserPostcode(String userPostcode) {
		this.userPostcode = userPostcode;
	}

	/**
	 * Gets the msn.
	 * 
	 * @return the msn
	 */
	@Column(name = "msn")
	public String getMsn() {
		return this.msn;
	}

	/**
	 * Sets the msn.
	 * 
	 * @param msn
	 *            the new msn
	 */
	public void setMsn(String msn) {
		this.msn = msn;
	}

	/**
	 * Gets the qq.
	 * 
	 * @return the qq
	 */
	@Column(name = "qq")
	public String getQq() {
		return this.qq;
	}

	/**
	 * Sets the qq.
	 * 
	 * @param qq
	 *            the new qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * Gets the fax.
	 * 
	 * @return the fax
	 */
	@Column(name = "fax")
	public String getFax() {
		return this.fax;
	}

	/**
	 * Sets the fax.
	 * 
	 * @param fax
	 *            the new fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	

	

	/**
	 * Gets the modify time.
	 * 
	 * @return the modify time
	 */
	@Column(name = "modify_time")
	public Date getModifyTime() {
		return this.modifyTime;
	}

	/**
	 * Sets the modify time.
	 * 
	 * @param modifyDate
	 *            the new modify time
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	/**
	 * Gets the user regtime.
	 * 
	 * @return the user regtime
	 */
	@Column(name = "user_regtime")
	public Date getUserRegtime() {
		return this.userRegtime;
	}

	/**
	 * Sets the user regtime.
	 * 
	 * @param userRegtime
	 *            the new user regtime
	 */
	public void setUserRegtime(Date userRegtime) {
		this.userRegtime = userRegtime;
	}

	/**
	 * Gets the user regip.
	 * 
	 * @return the user regip
	 */
	@Column(name = "user_regip")
	public String getUserRegip() {
		return this.userRegip;
	}

	/**
	 * Sets the user regip.
	 * 
	 * @param userRegip
	 *            the new user regip
	 */
	public void setUserRegip(String userRegip) {
		this.userRegip = userRegip;
	}

	/**
	 * Gets the user lasttime.
	 * 
	 * @return the user lasttime
	 */
	@Column(name = "user_lasttime")
	public Date getUserLasttime() {
		return this.userLasttime;
	}

	/**
	 * Sets the user lasttime.
	 * 
	 * @param userLasttime
	 *            the new user lasttime
	 */
	public void setUserLasttime(Date userLasttime) {
		this.userLasttime = userLasttime;
	}

	/**
	 * Gets the user lastip.
	 * 
	 * @return the user lastip
	 */
	@Column(name = "user_lastip")
	public String getUserLastip() {
		return this.userLastip;
	}

	/**
	 * Sets the user lastip.
	 * 
	 * @param userLastip
	 *            the new user lastip
	 */
	public void setUserLastip(String userLastip) {
		this.userLastip = userLastip;
	}

	/**
	 * Gets the user memo.
	 * 
	 * @return the user memo
	 */
	@Column(name = "user_memo")
	public String getUserMemo() {
		return this.userMemo;
	}

	/**
	 * Sets the user memo.
	 * 
	 * @param userMemo
	 *            the new user memo
	 */
	public void setUserMemo(String userMemo) {
		this.userMemo = userMemo;
	}

	/**
	 * Gets the shop detail.
	 * 
	 * @return the shop detail
	 */
	@Transient
	public ShopDetail getShopDetail() {
		return shopDetail;
	}

	/**
	 * Sets the shop detail.
	 * 
	 * @param shopDetail
	 *            the new shop detail
	 */
	public void setShopDetail(ShopDetail shopDetail) {
		this.shopDetail = shopDetail;
	}

	/**
	 * Gets the nick name.
	 * 
	 * @return the nick name
	 */
	@Column(name = "nick_name")
	public String getNickName() {
		return nickName;
	}

	/**
	 * Sets the nick name.
	 * 
	 * @param nickName
	 *            the new nick name
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	@Transient
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the sex.
	 * 
	 * @return the sex
	 */
	@Column(name = "sex")
	public String getSex() {
		return sex;
	}

	/**
	 * Sets the sex.
	 * 
	 * @param sex
	 *            the new sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * Gets the birth date.
	 * 
	 * @return the birth date
	 */
	@Column(name = "birth_date")
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * Sets the birth date.
	 * 
	 * @param birthDate
	 *            the new birth date
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * Gets the user mobile.
	 * 
	 * @return the user mobile
	 */
	@Column(name = "user_mobile")
	public String getUserMobile() {
		return userMobile;
	}

	/**
	 * Sets the user mobile.
	 * 
	 * @param userMobile
	 *            the new user mobile
	 */
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	/**
	 * Gets the shop id.
	 * 
	 * @return the shop id
	 */
	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 * 
	 * @param shopId
	 *            the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the register code.
	 * 
	 * @return the register code
	 */
	@Column(name = "register_code")
	public String getRegisterCode() {
		return registerCode;
	}

	/**
	 * Sets the register code.
	 * 
	 * @param registerCode
	 *            the new register code
	 */
	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	/**
	 * Gets the qq list.
	 * 
	 * @return the qq list
	 */
	@Transient
	public List getQqList() {
		return qqList;
	}

	/**
	 * Sets the qq list.
	 * 
	 * @param qqList
	 *            the new qq list
	 */
	public void setQqList(List qqList) {
		this.qqList = qqList;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the new score
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * Gets the total cash.
	 * 
	 * @return the total cash
	 */
	@Column(name = "total_cash")
	public Double getTotalCash() {
		return totalCash;
	}

	/**
	 * Sets the total cash.
	 * 
	 * @param totalCash
	 *            the new total cash
	 */
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}

	/**
	 * Gets the user addresses.
	 * 
	 * @return the user addresses
	 */
	@Transient
	public Set<UserAddress> getUserAddresses() {
		return userAddresses;
	}

	/**
	 * Sets the user addresses.
	 * 
	 * @param userAddresses
	 *            the new user addresses
	 */
	public void setUserAddresses(Set<UserAddress> userAddresses) {
		this.userAddresses = userAddresses;
	}

	/**
	 * Gets the default addr id.
	 * 
	 * @return the default addr id
	 */
	@Column(name = "addr_id")
	public Long getDefaultAddrId() {
		return defaultAddrId;
	}

	/**
	 * Sets the default addr id.
	 * 
	 * @param defaultAddrId
	 *            the new default addr id
	 */
	public void setDefaultAddrId(Long defaultAddrId) {
		this.defaultAddrId = defaultAddrId;
	}

	/**
	 * Gets the total consume.
	 * 
	 * @return the total consume
	 */
	@Column(name = "total_consume")
	public Double getTotalConsume() {
		return totalConsume;
	}

	/**
	 * Sets the total consume.
	 * 
	 * @param totalConsume
	 *            the new total consume
	 */
	public void setTotalConsume(Double totalConsume) {
		this.totalConsume = totalConsume;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public String getId() {
		return userId;
	}
	
	public void setId(String id) {
		this.userId = id;
	}

	/**
	 * Gets the enabled.
	 * 
	 * @return the enabled
	 */
	@Transient
	public String getEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param enabled
	 *            the new enabled
	 */
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	@Column(name = "provinceid")
	public Integer getProvinceid() {
		return provinceid;
	}

	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	@Column(name = "cityid")
	public Integer getCityid() {
		return cityid;
	}

	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	@Column(name = "areaid")
	public Integer getAreaid() {
		return areaid;
	}

	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

	@Column(name = "id_card")
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	/**
	 * @return the realName
	 */
	@Column(name = "real_name")
	public String getRealName() {
		return realName;
	}

	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	@Column(name = "pay_password")
	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}

	@Column(name = "portrait_pic")
	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	@Column(name = "accusation")
	public Integer getAccusation() {
		return accusation;
	}

	public void setAccusation(Integer accusation) {
		this.accusation = accusation;
	}

	@Transient
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Transient
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Transient
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	@Transient
	public String getZxingImage() {
		return zxingImage;
	}

	public void setZxingImage(String zxingImage) {
		this.zxingImage = zxingImage;
	}

	@Column(name = "available_predeposit")
	public Double getAvailablePredeposit() {
		if(availablePredeposit==null){
			availablePredeposit=0d;
		}
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	@Column(name = "freeze_predeposit")
	public Double getFreezePredeposit() {
		if(freezePredeposit==null){
			freezePredeposit=0d;
		}
		return freezePredeposit;
	}

	public void setFreezePredeposit(Double freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}
	
	@Column(name = "parent_user_name")
	public String getParentUserName() {
		return parentUserName;
	}

	@Transient
	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}

	@Column(name = "parent_binding_time")
	public Date getParentBindingTime() {
		return parentBindingTime;
	}

	public void setParentBindingTime(Date parentBindingTime) {
		this.parentBindingTime = parentBindingTime;
	}

	@Column(name = "is_reg_im")
	public Integer getIsRegIM() {
		return isRegIM;
	}

	public void setIsRegIM(Integer isRegIM) {
		this.isRegIM = isRegIM;
	}

	@Transient
	public String getHaveShop() {
		return haveShop;
	}

	public void setHaveShop(String haveShop) {
		this.haveShop = haveShop;
	}

	@Column(name = "user_coin")
	public Double getUserCoin() {
		return userCoin;
	}

	public void setUserCoin(Double userCoin) {
		this.userCoin = userCoin;
	}
	
	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
/*
	@Column(name = "source")
	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}*/

	@Column(name = "is_activated")
	public boolean isActivated() {
		return isActivated;
	}

	public void setActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}

	@Column(name = "update_pic")
	public String getUpdatePic() {
		return updatePic;
	}

	public void setUpdatePic(String updatePic) {
		this.updatePic = updatePic;
	}

	@Column(name = "user_signature")
	public String getUserSignature() {
		return userSignature;
	}

	public void setUserSignature(String userSignature) {
		this.userSignature = userSignature;
	}

	@Transient
	public Integer getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(Integer shopStatus) {
		this.shopStatus = shopStatus;
	}
}