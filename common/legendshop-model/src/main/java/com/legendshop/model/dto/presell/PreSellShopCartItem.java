/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import java.util.Date;

import com.legendshop.model.dto.buy.ShopCartItem;

/**
 * 预售商店类目
 */
public class PreSellShopCartItem extends ShopCartItem {

	private static final long serialVersionUID = 3201750742847396936L;

	/** 支付方式 */
	private Integer payPctType;

	/** 预售价格 */
	private Double prePrice;

	/** The pre deposit price. */
	private Double preDepositPrice;
	
	/** 订金支付开始时间. */
	private Date preSaleStart; 
	
	/** 订金支付结束时间. */
	private Date preSaleEnd; 

	/** 尾款支付开始时间. */
	private Date finalMStart;

	/** 尾款支付结束时间. */
	private Date finalMEnd;

	/**
	 * Gets the pay pct type.
	 *
	 * @return the pay pct type
	 */
	public Integer getPayPctType() {
		return payPctType;
	}

	/**
	 * Sets the pay pct type.
	 *
	 * @param payPctType
	 */
	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	/**
	 * Gets the pre price.
	 *
	 * @return the pre price
	 */
	public Double getPrePrice() {
		return prePrice;
	}

	/**
	 * Sets the pre price.
	 *
	 * @param prePrice
	 */
	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	/**
	 * Gets the pre deposit price.
	 *
	 * @return the pre deposit price
	 */
	public Double getPreDepositPrice() {
		return preDepositPrice;
	}

	/**
	 * Sets the pre deposit price.
	 *
	 * @param preDepositPrice
	 *            the pre deposit price
	 */
	public void setPreDepositPrice(Double preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}
	

	/**
	 * Gets the final m start.
	 *
	 * @return the final m start
	 */
	public Date getFinalMStart() {
		return finalMStart;
	}

	/**
	 * Sets the final m start.
	 *
	 * @param finalMStart
	 *            the final m start
	 */
	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	/**
	 * Gets the final m end.
	 *
	 * @return the final m end
	 */
	public Date getFinalMEnd() {
		return finalMEnd;
	}

	/**
	 * Sets the final m end.
	 *
	 * @param finalMEnd
	 *            the final m end
	 */
	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public Date getPreSaleStart() {
		return preSaleStart;
	}

	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}

}
