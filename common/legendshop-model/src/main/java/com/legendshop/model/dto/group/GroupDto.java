/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

import java.util.Date;

import com.legendshop.model.dto.ProductDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 团购商品详情.
 *
 * @author liyuan
 */
@ApiModel(value="团购商品详情") 
public class GroupDto {

	/** id. */
	@ApiModelProperty(value="主键ID") 
	private Long id;

	/** 团购名称. */
	@ApiModelProperty(value="团购名称") 
	private String groupName;

	/** 开始时间. */
	@ApiModelProperty(value="开始时间") 
	private Date startTime;

	/** 结束时间. */
	@ApiModelProperty(value="结束时间") 
	private Date endTime;

	/** 商品ID. */
	@ApiModelProperty(value="商品ID") 
	private Long productId;

	/** 店铺ID. */
	@ApiModelProperty(value="店铺ID") 
	private Long shopId;

	/** 店铺名称. */
	@ApiModelProperty(value="店铺名称") 
	private String shopName;

	/** 团购价格. */
	@ApiModelProperty(value="团购价格") 
	private java.math.BigDecimal groupPrice;

	/** 已购买人数. */
	@ApiModelProperty(value="已购买人数") 
	private Long buyerCount;

	/** 每人限购数量, 为0 则不限购 */
	@ApiModelProperty(value="每人限购数量, 为0 则不限购") 
	private Integer buyQuantity;

	/** 活动的状态有：未提审、已提审、上线（审核通过）、审核不通过。. */
	@ApiModelProperty(value="活动的状态有：未提审、已提审、上线（审核通过）、审核不通过") 
	private Integer status;

	/** 分类名称. */
	@ApiModelProperty(value="分类名称") 
	private String categoryName;

	/** 团购1级分类ID. */
	@ApiModelProperty(value="团购1级分类ID") 
	private Long firstCatId;

	/** 团购2级分类ID. */
	@ApiModelProperty(value="团购2级分类ID") 
	private Long secondCatId;

	/** 团购3级分类ID. */
	@ApiModelProperty(value="团购3级分类ID") 
	private Long thirdCatId;

	/** 团购图片. */
	@ApiModelProperty(value="团购图片") 
	private String groupImage;

	/** 团购详情. */
	@ApiModelProperty(value="团购详情") 
	private String groupDesc;

	/** 审计选项. */
	@ApiModelProperty(value="审计选项") 
	private String auditOpinion;

	/** 审计时间. */
	@ApiModelProperty(value="审计时间") 
	private Date auditTime;

	/** 商品dto. */
	@ApiModelProperty(value="商品dto") 
	private ProductDto productDto;
	
	/** 成团人数 */
	@ApiModelProperty(value="成团人数") 
	private Integer peopleCount;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the group name.
	 *
	 * @return the group name
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the group name.
	 *
	 * @param groupName
	 *            the group name
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime
	 *            the start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime
	 *            the end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId
	 *            the product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId
	 *            the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName
	 *            the shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the group price.
	 *
	 * @return the group price
	 */
	public java.math.BigDecimal getGroupPrice() {
		return groupPrice;
	}

	/**
	 * Sets the group price.
	 *
	 * @param groupPrice
	 *            the group price
	 */
	public void setGroupPrice(java.math.BigDecimal groupPrice) {
		this.groupPrice = groupPrice;
	}

	/**
	 * Gets the buyer count.
	 *
	 * @return the buyer count
	 */
	public Long getBuyerCount() {
		return buyerCount;
	}

	/**
	 * Sets the buyer count.
	 *
	 * @param buyerCount
	 *            the buyer count
	 */
	public void setBuyerCount(Long buyerCount) {
		this.buyerCount = buyerCount;
	}

	/**
	 * Gets the buy quantity.
	 *
	 * @return the buy quantity
	 */
	public Integer getBuyQuantity() {
		return buyQuantity;
	}

	/**
	 * Sets the buy quantity.
	 *
	 * @param buyQuantity
	 *            the buy quantity
	 */
	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the category name.
	 *
	 * @return the category name
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * Sets the category name.
	 *
	 * @param categoryName
	 *            the category name
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * Gets the first cat id.
	 *
	 * @return the first cat id
	 */
	public Long getFirstCatId() {
		return firstCatId;
	}

	/**
	 * Sets the first cat id.
	 *
	 * @param firstCatId
	 *            the first cat id
	 */
	public void setFirstCatId(Long firstCatId) {
		this.firstCatId = firstCatId;
	}

	/**
	 * Gets the second cat id.
	 *
	 * @return the second cat id
	 */
	public Long getSecondCatId() {
		return secondCatId;
	}

	/**
	 * Sets the second cat id.
	 *
	 * @param secondCatId
	 *            the second cat id
	 */
	public void setSecondCatId(Long secondCatId) {
		this.secondCatId = secondCatId;
	}

	/**
	 * Gets the third cat id.
	 *
	 * @return the third cat id
	 */
	public Long getThirdCatId() {
		return thirdCatId;
	}

	/**
	 * Sets the third cat id.
	 *
	 * @param thirdCatId
	 *            the third cat id
	 */
	public void setThirdCatId(Long thirdCatId) {
		this.thirdCatId = thirdCatId;
	}

	/**
	 * Gets the group image.
	 *
	 * @return the group image
	 */
	public String getGroupImage() {
		return groupImage;
	}

	/**
	 * Sets the group image.
	 *
	 * @param groupImage
	 *            the group image
	 */
	public void setGroupImage(String groupImage) {
		this.groupImage = groupImage;
	}

	/**
	 * Gets the group desc.
	 *
	 * @return the group desc
	 */
	public String getGroupDesc() {
		return groupDesc;
	}

	/**
	 * Sets the group desc.
	 *
	 * @param groupDesc
	 *            the group desc
	 */
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	/**
	 * Gets the audit opinion.
	 *
	 * @return the audit opinion
	 */
	public String getAuditOpinion() {
		return auditOpinion;
	}

	/**
	 * Sets the audit opinion.
	 *
	 * @param auditOpinion
	 *            the audit opinion
	 */
	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	/**
	 * Gets the audit time.
	 *
	 * @return the audit time
	 */
	public Date getAuditTime() {
		return auditTime;
	}

	/**
	 * Sets the audit time.
	 *
	 * @param auditTime
	 *            the audit time
	 */
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	/**
	 * Gets the product dto.
	 *
	 * @return the product dto
	 */
	public ProductDto getProductDto() {
		return productDto;
	}

	/**
	 * Sets the product dto.
	 *
	 * @param productDto
	 *            the product dto
	 */
	public void setProductDto(ProductDto productDto) {
		this.productDto = productDto;
	}

	public Integer getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Integer peopleCount) {
		this.peopleCount = peopleCount;
	}
	

}
