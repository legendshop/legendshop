/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.dto.order;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.UserAddressSub;

import io.swagger.annotations.ApiModelProperty;

/**
 * 预售订单DTO
 * @author 开发很忙
 * 
 */
public class PresellSubDto implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 订单ID */
	private Long subId;

	private Long activeId;

	/** 订单号 */
	private String subNo;
	
	/** 所属商家ID */
	private Long shopId;

	/** 店铺名称 */
	private String shopName;
	
	/** 用户ID */
	private String userId;
	
	/** 用户名称 */
	private String userName;
	
	/** 订单生成日期 */
	private Date subCreateTime;
	
	/** 商品总价(不包含任何其他价格) */
	private Double 	totalPrice; 
	
	/** 订单实际需要支付金额 */
	private Double actualTotalPrice;
	
	/** 运费 */
	private Double freightAmount;
	
	/** 购买数量 */
	private Long basketCount;
	
	/**支付方式：货到付款，在线支付*/
	private Integer payManner;
	
	/** 订金金额 */
	private Double depositPrice;
	
	/** 是否已支付订金 */
	private Long isPayDeposit;
	
	private String depositPayName;
	
	private String depositTradeNo;
	
	private String finalPayName;
	
	private String finalTradeNo;
	
	/** 订金支付时间 */
	private Date depositPayTime;
	
	/** 尾款金额 */
	private Double finalPrice;
	
	/** 尾款支付开始时间 */
	private Date finalMStart;
	
	/** 尾款支付结束时间 */
	private Date finalMEnd;
	
	/** 是否已支付尾款 */
	private Long isPayFinal;
	
	/** 尾款支付时间 */
	private Date finalPayTime;
	
	/** 订单状态 */
	private Long status;
	
	/** 是否已支付 */
	private Long isPayed;
	
	/** 支付日期 */
	private Date payDate;
	
	/** 支付方式名称 */
	private String payTypeName;

	/** 发货时间 */
	private Date dvyDate;
	
	/** 完成时间/确认收货时间 */
	private Date finallyDate;
	
	private Double couponOffPrice;
	
	private Double discountPrice;
	
	/** 配送方式 */
	private String dvyType;
	
	/** 配送方式ID */
	private Long dvyTypeId;
	
	/** 物流单号 */
	private String dvyFlowId; 
	
	/** 订单中的商品 */
	private List<PresellSubItemDto> orderItems;
	
	/** 发票单号 **/
	private Long invoiceSubId;
	
	/** 订单地址 */
	private Long addrOrderId;
	
	/** 给买家留言 */
	private String orderRemark;
	
	/** 订单更新时间 */
	private Date updateDate;
	
	/** 订单类型 */
	private Long subType;
	
	/**
	 * 用户的订单地址
	 */
	private UserAddressSub userAddressSub;
	
	/**
	 * 用户的订单发票
	 */
	private InvoiceSub invoiceSub; 
	
	private List<SubHistory> subHistorys;
	
	private DeliveryDto  delivery;
	
	/**
	 * 支付方式,0:全额,1:订金
	 */
	private Integer payPctType;
	
	
	/**
	 * 预售订单应付金额
	 */
	private Double payableAmount;
	
	/** 0:默认,1:在处理,2:处理完成 */
	private Long refundState;

	/**
	 * 取消原因
	 */
	private String cancelReason;


	@Override
	public Long getId() {
		return subId;
	}

	@Override
	public void setId(Long id) {
		this.subId = id;
	}

	/**
	 * @return the subId
	 */
	public Long getSubId() {
		return subId;
	}

	/**
	 * @param subId the subId to set
	 */
	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	/**
	 * @return the subNum
	 */
	public String getSubNo() {
		return subNo;
	}

	/**
	 * @param subNum the subNum to set
	 */
	public void setSubNo(String subNo) {
		this.subNo = subNo;
	}

	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the subDate
	 */
	public Date getSubCreateTime() {
		return subCreateTime;
	}

	/**
	 * @param subDate the subDate to set
	 */
	public void setSubCreateTime(Date subCreateTime) {
		this.subCreateTime = subCreateTime;
	}

	/**
	 * @return the total
	 */
	public Double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the actualTotal
	 */
	public Double getActualTotalPrice() {
		return actualTotalPrice;
	}

	/**
	 * @param actualTotal the actualTotal to set
	 */
	public void setActualTotalPrice(Double actualTotalPrice) {
		this.actualTotalPrice = actualTotalPrice;
	}

	/**
	 * @return the freightAmount
	 */
	public Double getFreightAmount() {
		return freightAmount;
	}

	/**
	 * @param freightAmount the freightAmount to set
	 */
	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	/**
	 * @return the basketCount
	 */
	public Long getBasketCount() {
		return basketCount;
	}

	/**
	 * @param basketCount the basketCount to set
	 */
	public void setBasketCount(Long basketCount) {
		this.basketCount = basketCount;
	}

	/**
	 * @return the payManner
	 */
	public Integer getPayManner() {
		return payManner;
	}

	/**
	 * @param payManner the payManner to set
	 */
	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	/**
	 * @return the depositPrice
	 */
	public Double getDepositPrice() {
		return depositPrice;
	}

	/**
	 * @param depositPrice the depositPrice to set
	 */
	public void setDepositPrice(Double depositPrice) {
		this.depositPrice = depositPrice;
	}

	/**
	 * @return the isPayDeposit
	 */
	public Long getIsPayDeposit() {
		return isPayDeposit;
	}

	/**
	 * @param isPayDeposit the isPayDeposit to set
	 */
	public void setIsPayDeposit(Long isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}
	
	/**
	 * @return the depositPayTime
	 */
	public Date getDepositPayTime() {
		return depositPayTime;
	}

	/**
	 * @param depositPayTime the depositPayTime to set
	 */
	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	/**
	 * @return the finalPrice
	 */
	public Double getFinalPrice() {
		return finalPrice;
	}

	/**
	 * @param finalPrice the finalPrice to set
	 */
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	/**
	 * @return the finalMStart
	 */
	public Date getFinalMStart() {
		return finalMStart;
	}

	/**
	 * @param finalMStart the finalMStart to set
	 */
	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	/**
	 * @return the finalMEnd
	 */
	public Date getFinalMEnd() {
		return finalMEnd;
	}

	/**
	 * @param finalMEnd the finalMEnd to set
	 */
	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	/**
	 * @return the isPayFinal
	 */
	public Long getIsPayFinal() {
		return isPayFinal;
	}

	/**
	 * @param isPayFinal the isPayFinal to set
	 */
	public void setIsPayFinal(Long isPayFinal) {
		this.isPayFinal = isPayFinal;
	}
	
	/**
	 * @return the finalPayTime
	 */
	public Date getFinalPayTime() {
		return finalPayTime;
	}

	/**
	 * @param finalPayTime the finalPayTime to set
	 */
	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	/**
	 * @return the status
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return the dvyType
	 */
	public String getDvyType() {
		return dvyType;
	}

	/**
	 * @param dvyType the dvyType to set
	 */
	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	/**
	 * @return the dvyTypeId
	 */
	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	/**
	 * @param dvyTypeId the dvyTypeId to set
	 */
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	/**
	 * @return the dvyFlowId
	 */
	public String getDvyFlowId() {
		return dvyFlowId;
	}

	/**
	 * @param dvyFlowId the dvyFlowId to set
	 */
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}
	
	/**
	 * @return the isPayed
	 */
	public Long getIsPayed() {
		return isPayed;
	}

	/**
	 * @param isPayed the isPayed to set
	 */
	public void setIsPayed(Long isPayed) {
		this.isPayed = isPayed;
	}

	/**
	 * @return the payDate
	 */
	public Date getPayDate() {
		return payDate;
	}

	/**
	 * @param payDate the payDate to set
	 */
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	/**
	 * @return the payTypeName
	 */
	public String getPayTypeName() {
		return payTypeName;
	}

	/**
	 * @param payTypeName the payTypeName to set
	 */
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	/**
	 * @return the orderItems
	 */
	public List<PresellSubItemDto> getOrderItems() {
		return orderItems;
	}

	/**
	 * @param orderItems the orderItems to set
	 */
	public void setOrderItems(List<PresellSubItemDto> orderItems) {
		this.orderItems = orderItems;
	}

	/**
	 * @return the invoiceSubId
	 */
	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	/**
	 * @param invoiceSubId the invoiceSubId to set
	 */
	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	/**
	 * @return the addrOrderId
	 */
	public Long getAddrOrderId() {
		return addrOrderId;
	}

	/**
	 * @param addrOrderId the addrOrderId to set
	 */
	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	/**
	 * @return the userAddressSub
	 */
	public UserAddressSub getUserAddressSub() {
		return userAddressSub;
	}

	/**
	 * @param userAddressSub the userAddressSub to set
	 */
	public void setUserAddressSub(UserAddressSub userAddressSub) {
		this.userAddressSub = userAddressSub;
	}

	/**
	 * @return the invoiceSub
	 */
	public InvoiceSub getInvoiceSub() {
		return invoiceSub;
	}

	/**
	 * @param invoiceSub the invoiceSub to set
	 */
	public void setInvoiceSub(InvoiceSub invoiceSub) {
		this.invoiceSub = invoiceSub;
	}

	/**
	 * @return the subHistorys
	 */
	public List<SubHistory> getSubHistorys() {
		return subHistorys;
	}

	/**
	 * @param subHistorys the subHistorys to set
	 */
	public void setSubHistorys(List<SubHistory> subHistorys) {
		this.subHistorys = subHistorys;
	}

	/**
	 * @return the delivery
	 */
	public DeliveryDto getDelivery() {
		return delivery;
	}

	/**
	 * @param delivery the delivery to set
	 */
	public void setDelivery(DeliveryDto delivery) {
		this.delivery = delivery;
	}

	/**
	 * @return the orderRemark
	 */
	public String getOrderRemark() {
		return orderRemark;
	}

	/**
	 * @param orderRemark the orderRemark to set
	 */
	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}
	
	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * @return the subType
	 */
	public Long getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(Long subType) {
		this.subType = subType;
	}

	/**
	 * @return the dvyDate
	 */
	public Date getDvyDate() {
		return dvyDate;
	}

	/**
	 * @param dvyDate the dvyDate to set
	 */
	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	/**
	 * @return the finallyDate
	 */
	public Date getFinallyDate() {
		return finallyDate;
	}

	/**
	 * @param finallyDate the finallyDate to set
	 */
	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	/**
	 * @return the couponOffPrice
	 */
	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	/**
	 * @param couponOffPrice the couponOffPrice to set
	 */
	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	/**
	 * @return the discountPrice
	 */
	public Double getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * @param discountPrice the discountPrice to set
	 */
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}

	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}

	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}

	public void setFinalTradeNo(String finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}

	public String getDepositPayName() {
		return depositPayName;
	}

	public String getDepositTradeNo() {
		return depositTradeNo;
	}

	public String getFinalPayName() {
		return finalPayName;
	}

	public String getFinalTradeNo() {
		return finalTradeNo;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public Double getPayableAmount() {
		
		
		return payableAmount;
	}

	public void setPayableAmount(Double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public Long getRefundState() {
		return refundState;
	}

	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
}
