package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *  直播账号
 */
@Entity
@Table(name = "ls_zhibo_account")
public class ZhiboAccount implements GenericEntity<String> {

	private static final long serialVersionUID = 4280786940678679143L;

	/** 用户名 */
	private String uid; 
		
	/** 用户密码 */
	private String pwd; 
		
	/** 用户token */
	private String token; 
		
	/** 登录状态 */
	private Boolean state; 
		
	/** sig */
	private String userSig; 
		
	/** 注册时间戳 */
	private Date registerTime; 
		
	/** 登录时间戳 */
	private Date loginTime; 
		
	/** 退出时间戳 */
	private Date logoutTime; 
		
	/** 最新请求时间戳 */
	private Date lastRequestTime; 
	
	//判断商家端或者用户端
	private String type;
		
	
	public ZhiboAccount() {
    }
		
	@Id
	@Column(name = "uid")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "T_ACCOUNT_SEQ")
	public String  getUid(){
		return uid;
	} 
		
	public void setUid(String uid){
			this.uid = uid;
		}
		
    @Column(name = "pwd")
	public String  getPwd(){
		return pwd;
	} 
		
	public void setPwd(String pwd){
			this.pwd = pwd;
		}
		
    @Column(name = "token")
	public String  getToken(){
		return token;
	} 
		
	public void setToken(String token){
			this.token = token;
		}
		
    @Column(name = "state")
	public Boolean  getState(){
		return state;
	} 
		
	public void setState(Boolean state){
			this.state = state;
		}
		
    @Column(name = "user_sig")
	public String  getUserSig(){
		return userSig;
	} 
		
	public void setUserSig(String userSig){
			this.userSig = userSig;
		}
		
    @Column(name = "register_time")
	public Date  getRegisterTime(){
		return registerTime;
	} 
		
	public void setRegisterTime(Date registerTime){
			this.registerTime = registerTime;
		}
		
    @Column(name = "login_time")
	public Date  getLoginTime(){
		return loginTime;
	} 
		
	public void setLoginTime(Date loginTime){
			this.loginTime = loginTime;
		}
		
    @Column(name = "logout_time")
	public Date  getLogoutTime(){
		return logoutTime;
	} 
		
	public void setLogoutTime(Date logoutTime){
			this.logoutTime = logoutTime;
		}
		
    @Column(name = "last_request_time")
	public Date  getLastRequestTime(){
		return lastRequestTime;
	} 
		
	public void setLastRequestTime(Date lastRequestTime){
			this.lastRequestTime = lastRequestTime;
		}

	@Transient
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setId(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	
} 
