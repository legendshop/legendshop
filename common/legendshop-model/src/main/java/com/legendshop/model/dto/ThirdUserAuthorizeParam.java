/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

/**
 * 用于构建第三方用户授权url的参数
 * @author 开发很忙
 */
public class ThirdUserAuthorizeParam {
	
	/** appid或者client_id, 微信叫appId, QQ和微博叫client_id */
	private String appId;
	
	/** 授权回调地址 */
	private String redirectURI;
	
	/** 
	 * 用于保持请求和回调的状态，在回调时，会在Query Parameter中回传该参数。
	 * 开发者可以用这个参数验证请求有效性，也可以记录用户请求授权页前的位置。这个参数可用于防止跨站请求伪造（CSRF）攻击
	 */
	private String state;
	
	/** 授权范围, 微信,QQ,微博都不一样 */
	private String scope;
	
	/** 授权类型, 固定写code. 微博没有这个字段 */
	private String responseType;

	/**
	 *  显示的样式, 可以用于控制授权页面显示的是PC端样式还是mobile端样式;
	 *  微信没有此字段
	 */
	private String display;
	
	/** 是否强制用户重新登录，true：是，false：否。默认false。 仅微博有这个字段*/
	private Boolean forcelogin;
	
	/** 授权页面的语言.  仅微博有这个字段*/
	private String language;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getRedirectURI() {
		return redirectURI;
	}

	public void setRedirectURI(String redirectURI) {
		this.redirectURI = redirectURI;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public Boolean getForcelogin() {
		return forcelogin;
	}

	public void setForcelogin(Boolean forcelogin) {
		this.forcelogin = forcelogin;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
