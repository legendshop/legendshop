/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description 
 * @author 关开发
 */
public class ApplyRefundReturnBase {
	
	/** 订单ID **/
	@NotNull
	protected Long orderId;

	/** 原因 */
	@NotBlank
	@Length(min=1,max=300)
	protected String buyerMessage;
	
	/** 退款金额 */
	@NotNull
	protected Double refundAmount;
	
	/**预售订单，订金退款金额*/
	protected Double depositRefundAmount;
	
	/**是否退订金*/
	protected Boolean isRefundDeposit;
	
	/** 原因说明 */
	@NotBlank
	@Length(min=1,max=500)
	protected String reasonInfo;
	
	/** 凭证图片1 */
	protected MultipartFile photoFile1;
	
	/** 凭证图片2 */
	protected MultipartFile photoFile2;
	
	/** 凭证图片3 */
	protected MultipartFile photoFile3;

	/**
	 * @return the buyerMessage
	 */
	public String getBuyerMessage() {
		return buyerMessage;
	}

	/**
	 * @param buyerMessage the buyerMessage to set
	 */
	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}
	
	/**
	 * @return the orderId
	 */
	public Long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the refundAmount
	 */
	public Double getRefundAmount() {
		return refundAmount;
	}

	/**
	 * @param refundAmount the refundAmount to set
	 */
	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	/**
	 * @return the reasonInfo
	 */
	public String getReasonInfo() {
		return reasonInfo;
	}

	/**
	 * @param reasonInfo the reasonInfo to set
	 */
	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	/**
	 * @return the photoFile1
	 */
	public MultipartFile getPhotoFile1() {
		return photoFile1;
	}

	/**
	 * @param photoFile1 the photoFile1 to set
	 */
	public void setPhotoFile1(MultipartFile photoFile1) {
		this.photoFile1 = photoFile1;
	}

	/**
	 * @return the photoFile2
	 */
	public MultipartFile getPhotoFile2() {
		return photoFile2;
	}

	/**
	 * @param photoFile2 the photoFile2 to set
	 */
	public void setPhotoFile2(MultipartFile photoFile2) {
		this.photoFile2 = photoFile2;
	}

	/**
	 * @return the photoFile3
	 */
	public MultipartFile getPhotoFile3() {
		return photoFile3;
	}

	/**
	 * @param photoFile3 the photoFile3 to set
	 */
	public void setPhotoFile3(MultipartFile photoFile3) {
		this.photoFile3 = photoFile3;
	}

	public Double getDepositRefundAmount() {
		return depositRefundAmount;
	}

	public void setDepositRefundAmount(Double depositRefundAmount) {
		this.depositRefundAmount = depositRefundAmount;
	}

	public Boolean getIsRefundDeposit() {
		return isRefundDeposit;
	}

	public void setIsRefundDeposit(Boolean isRefundDeposit) {
		this.isRefundDeposit = isRefundDeposit;
	}
	
}
