/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * sku活动类型，用户标记sku参加什么营销活动
 */
public enum SkuActiveTypeEnum implements StringEnum {

	/** 默认类型，没有参加营销活动*/
	PRODUCT("P"),

	/** 团购活动 */
	GROUP("GROUP"),

	/**拼团活动  */
	MERGE_GROUP("MERGE"),

	/** 秒杀活动 */
	SECKILL("SECKILL"),
	
	/** 拍卖活动  */
	AUCTION("AUCTION"),
	
	/** 预售活动 */
	PRESELL("PRESELL");

	private String value;

	
	public String value() {
		return value;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param value
	 *            the value
	 */
	SkuActiveTypeEnum(String value) {
		this.value = value;
	}
	
	public static String getSkuTypeMes(String type){
		if (type.equals(SkuActiveTypeEnum.GROUP.value())) {
			return "团购";
		}else if (type.equals(SkuActiveTypeEnum.MERGE_GROUP.value())) {
			return "拼团";
		}else if (type.equals(SkuActiveTypeEnum.SECKILL.value())) {
			return "秒杀";
		}else if (type.equals(SkuActiveTypeEnum.AUCTION.value())) {
			return "拍卖";
		}else if (type.equals(SkuActiveTypeEnum.PRESELL.value())) {
			return "预售";
		}else {
			return "最新";
		}
		
	}

}
