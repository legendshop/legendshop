/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 *配置测试DTO
 */
public class ConfigTestDto implements Serializable{

	private static final long serialVersionUID = -2495724539180815435L;

	private String configItemName;

    private String configItemExplain;

    private String configItemParas;
    
    private Integer type;

    public String getConfigItemName() {
        return configItemName;
    }

    public void setConfigItemName(String configItemName) {
        this.configItemName = configItemName;
    }

    public String getConfigItemParas() {
        return configItemParas;
    }

    public void setConfigItemParas(String configItemParas) {
        this.configItemParas = configItemParas;
    }

    public String getConfigItemExplain() {
        return configItemExplain;
    }

    public void setConfigItemExplain(String configItemExplain) {
        this.configItemExplain = configItemExplain;
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
    
}
