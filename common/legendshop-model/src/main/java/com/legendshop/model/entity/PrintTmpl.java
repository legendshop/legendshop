package com.legendshop.model.entity;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *打印模板
 */
@Entity
@Table(name = "ls_print_tmpl")
@ApiModel(value="PrintTmpl") 
public class PrintTmpl implements GenericEntity<Long> {
	
	private static final long serialVersionUID = 6411963848066780940L;

	@ApiModelProperty(value="打印模板Id") 
	private Long prtTmplId; 
		
	/** 单据名称 */
	@ApiModelProperty(value="单据名称") 
	private String prtTmplTitle; 
		
	/** 是否启用 */
	@ApiModelProperty(value="是否启用") 
	private Integer prtDisabled; 
		
	/** 单据尺寸宽 */
	@ApiModelProperty(value="单据尺寸宽") 
	private Integer prtTmplWidth; 
		
	/** 单据尺寸高 */
	@ApiModelProperty(value="单据尺寸高") 
	private Integer prtTmplHeight; 
		
	/** 单据的数据项 */
	@ApiModelProperty(value="单据的数据项") 
	private String prtTmplData; 
		
	/** 单据背景 */
	@ApiModelProperty(value="单据背景") 
	private String bgimage; 
	
	@ApiModelProperty(value="商家Id") 
	private Long shopId;
	
	@ApiModelProperty(value="是否系统自带，如果是系统自带的则商家无法编辑") 
	private Integer isSystem;
	
	
	/** The file. */
	protected MultipartFile bgimageFile;
	
		
	
	public PrintTmpl() {
    }
		
	@Id
	@Column(name = "prt_tmpl_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PRINT_TMPL_SEQ")
	public Long  getPrtTmplId(){
		return prtTmplId;
	} 
		
	public void setPrtTmplId(Long prtTmplId){
			this.prtTmplId = prtTmplId;
		}
		
    @Column(name = "prt_tmpl_title")
	public String  getPrtTmplTitle(){
		return prtTmplTitle;
	} 
		
	public void setPrtTmplTitle(String prtTmplTitle){
			this.prtTmplTitle = prtTmplTitle;
		}
		
    @Column(name = "prt_disabled")
	public Integer  getPrtDisabled(){
		return prtDisabled;
	} 
		
	public void setPrtDisabled(Integer prtDisabled){
			this.prtDisabled = prtDisabled;
		}
		
    @Column(name = "prt_tmpl_width")
	public Integer  getPrtTmplWidth(){
		return prtTmplWidth;
	} 
		
	public void setPrtTmplWidth(Integer prtTmplWidth){
			this.prtTmplWidth = prtTmplWidth;
		}
		
    @Column(name = "prt_tmpl_height")
	public Integer  getPrtTmplHeight(){
		return prtTmplHeight;
	} 
		
	public void setPrtTmplHeight(Integer prtTmplHeight){
			this.prtTmplHeight = prtTmplHeight;
		}
		
    @Column(name = "prt_tmpl_data")
	public String  getPrtTmplData(){
		return prtTmplData;
	} 
		
	public void setPrtTmplData(String prtTmplData){
			this.prtTmplData = prtTmplData;
		}
		
    @Column(name = "bgimage")
	public String  getBgimage(){
		return bgimage;
	} 
		
	public void setBgimage(String bgimage){
			this.bgimage = bgimage;
		}
	
	
	
	@Transient
	public Long getId() {
		return prtTmplId;
	}
	
	public void setId(Long id) {
		prtTmplId = id;
	}

	@Transient
	public MultipartFile getBgimageFile() {
		return bgimageFile;
	}

	public void setBgimageFile(MultipartFile bgimageFile) {
		this.bgimageFile = bgimageFile;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	
	@Column(name = "is_system")
	public Integer getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}


} 
