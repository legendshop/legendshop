package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *中标历史
 */
@Entity
@Table(name = "ls_bidders_win")
public class BiddersWin implements GenericEntity<Long> {

	private static final long serialVersionUID = 4032595166232632611L;

	/**  */
	private Long id; 
		
	/** 拍卖活动ID */
	private Long aId; 
		
	/** 出价记录ID */
	private Long bidId; 
		
	/** 中标商品ID */
	private Long prodId; 
		
	/** 中标SKUID */
	private Long skuId; 
		
	/** 中标用户 */
	private String userId; 
		
	/** 中标时间 */
	private Date bitTime; 
		
	/** 中标金额 */
	private java.math.BigDecimal price; 
		
	/** 状态 */
	private Integer status; 
		
	/** 订单编号 */
	private String subNumber; 
	
	/** 订单id */
	private Long subId;
	
	/** 商品图片 */
	private String prodPic;
	
	/** 商品名称 */
	private String prodName;
	
	/** 订单时间 */
	private Date orderTime;
	
	/** 商品价格 */
	private java.math.BigDecimal prodPrice;
	
	/** 商家ID --- > auction-User */
	private String shopUserId;
	
	/** 是否交了保证金 */
	private Integer isSecurity;
	
	/** 是否领先   1领先   0出局 */
	private Integer first;
	
	public BiddersWin() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "BIDDERS_WIN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "a_id")
	public Long  getAId(){
		return aId;
	} 
		
	public void setAId(Long aId){
			this.aId = aId;
		}
		
    @Column(name = "bid_id")
	public Long  getBidId(){
		return bidId;
	} 
		
	public void setBidId(Long bidId){
			this.bidId = bidId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "bit_time")
	public Date  getBitTime(){
		return bitTime;
	} 
		
	public void setBitTime(Date bitTime){
			this.bitTime = bitTime;
		}
		
    @Column(name = "price")
	public java.math.BigDecimal  getPrice(){
		return price;
	} 
		
	public void setPrice(java.math.BigDecimal price){
			this.price = price;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "sub_number")
	public String  getSubNumber(){
		return subNumber;
	} 
		
	public void setSubNumber(String subNumber){
			this.subNumber = subNumber;
		}
	@Transient
	public String getProdPic() {
		return prodPic;
	}
	
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	@Transient
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	 @Column(name = "order_time")
	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	 @Column(name = "sub_id")
	public Long getSubId() {
		return subId;
	}
	 
	public void setSubId(Long subId) {
		this.subId = subId;
	}
	
	@Transient
	public String getShopUserId() {
		return shopUserId;
	}

	public void setShopUserId(String shopUserId) {
		this.shopUserId = shopUserId;
	}

	@Transient
	public java.math.BigDecimal getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(java.math.BigDecimal prodPrice) {
		this.prodPrice = prodPrice;
	}

	@Transient
	public Integer getIsSecurity() {
		return isSecurity;
	}

	public void setIsSecurity(Integer isSecurity) {
		this.isSecurity = isSecurity;
	}

	@Transient
	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}
	
	
} 
