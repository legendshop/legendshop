/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.io.Serializable;
import java.util.List;


/**
 * 首页装修的布局
 */
public class ShopLayoutDto implements Serializable {
	
	private static final long serialVersionUID = 2357957389570216485L;

	/**  布局ID. */
	private Long layoutId; 
		
	/**  商家ID. */
	private Long shopId; 
		
	/**  商家店铺装修ID. */
	private Long shopDecotateId; 
		
	/**  布局类型. */
	private String layoutType; 
		
	/**  布局模块类型. */
	private String layoutModuleType; 
		
	/**  排序. */
	private Integer seq;
	
	/** 标题名称. */
	private String title;
	
	/** 标题背景. */
	private String fontImg; 
	
	/** 标题颜色. */
	private String fontColor;
	
	/** 背景颜色. */
	private String backColor; 
	
	
	/** 商品模块. */
	private ShopLayoutModuleDto shopLayoutModule;
	
	
	/** The shop layout div dtos. */
	private List<ShopLayoutDivDto> shopLayoutDivDtos;
	

	/**
	 * Gets the layout id.
	 *
	 * @return the layout id
	 */
	public Long getLayoutId() {
		return layoutId;
	}

	/**
	 * Sets the layout id.
	 *
	 * @param layoutId the new layout id
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop decotate id.
	 *
	 * @return the shop decotate id
	 */
	public Long getShopDecotateId() {
		return shopDecotateId;
	}

	/**
	 * Sets the shop decotate id.
	 *
	 * @param shopDecotateId the new shop decotate id
	 */
	public void setShopDecotateId(Long shopDecotateId) {
		this.shopDecotateId = shopDecotateId;
	}

	/**
	 * Gets the layout type.
	 *
	 * @return the layout type
	 */
	public String getLayoutType() {
		return layoutType;
	}

	/**
	 * Sets the layout type.
	 *
	 * @param layoutType the new layout type
	 */
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}


	/**
	 * Gets the layout module type.
	 *
	 * @return the layout module type
	 */
	public String getLayoutModuleType() {
		return layoutModuleType;
	}

	/**
	 * Sets the layout module type.
	 *
	 * @param layoutModuleType the new layout module type
	 */
	public void setLayoutModuleType(String layoutModuleType) {
		this.layoutModuleType = layoutModuleType;
	}


	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	
	

	/**
	 * Gets the shop layout module.
	 *
	 * @return the shop layout module
	 */
	public ShopLayoutModuleDto getShopLayoutModule() {
		return shopLayoutModule;
	}

	/**
	 * Sets the shop layout module.
	 *
	 * @param shopLayoutModule the new shop layout module
	 */
	public void setShopLayoutModule(ShopLayoutModuleDto shopLayoutModule) {
		this.shopLayoutModule = shopLayoutModule;
	}

	/**
	 * Gets the shop layout div dtos.
	 *
	 * @return the shop layout div dtos
	 */
	public List<ShopLayoutDivDto> getShopLayoutDivDtos() {
		return shopLayoutDivDtos;
	}

	/**
	 * Sets the shop layout div dtos.
	 *
	 * @param shopLayoutDivDtos the new shop layout div dtos
	 */
	public void setShopLayoutDivDtos(List<ShopLayoutDivDto> shopLayoutDivDtos) {
		this.shopLayoutDivDtos = shopLayoutDivDtos;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the font img.
	 *
	 * @return the font img
	 */
	public String getFontImg() {
		return fontImg;
	}

	/**
	 * Sets the font img.
	 *
	 * @param fontImg the new font img
	 */
	public void setFontImg(String fontImg) {
		this.fontImg = fontImg;
	}

	/**
	 * Gets the font color.
	 *
	 * @return the font color
	 */
	public String getFontColor() {
		return fontColor;
	}

	/**
	 * Sets the font color.
	 *
	 * @param fontColor the new font color
	 */
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	/**
	 * Gets the back color.
	 *
	 * @return the back color
	 */
	public String getBackColor() {
		return backColor;
	}

	/**
	 * Sets the back color.
	 *
	 * @param backColor the new back color
	 */
	public void setBackColor(String backColor) {
		this.backColor = backColor;
	}

	
	
	

}
