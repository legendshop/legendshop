package com.legendshop.model.dto;

import io.swagger.models.auth.In;

import java.io.Serializable;


/**
 * 预售订单Dto
 * @author linzh
 */
public class PresellSubDTO implements Serializable {


	/**  */
	private static final long serialVersionUID = 1756056918995169691L;

	/** 订单号 */
	private String subNumber;

	/** 用户名 */
	private String userName;
	
	/** 数量 */
	private Integer productNums;
	
	/** 实际金额 */
	private Double actualTotal;
	
	/** 是否支付定金 */
	private Integer isPayDeposit;
	
	/** 是否支付尾款  */
	private Integer isPayFinal;

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}
}
