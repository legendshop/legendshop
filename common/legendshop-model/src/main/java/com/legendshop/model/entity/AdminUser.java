package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *管理员用户表
 */
@Entity
@Table(name = "ls_admin_user")
public class AdminUser implements GenericEntity<String> {

	private static final long serialVersionUID = -4380038386301964667L;

	/** 用户ID */
	private String id; 
		
	/** 名称 */
	private String name; 
		
	/** 密码 */
	private String password; 
		
	/** 状态 */
	private String enabled; 
		
	/** 注释 */
	private String note; 
		
	/** 部门ID */
	private Long deptId; 
	
	/** 联系手机 */
	private String mobile;
	
	/** 真实名字 */
	private String realName;
	
	/** 身份证号 */
	private String idCardNum;
	
	/** 住址 */
	private String addr;
	
	/** 入职时间 */
	private String hireDate;
	
	/** 记录时间 */
	private Date recDate;
	
	/** 更新时间 */
	private Date modifyDate;
	
	private String[] roleId;
	
	private Date activeTime;
 		
	
	public AdminUser() {
    }
		
	@Id
	@Column(name = "id")
 	@GeneratedValue(strategy = GenerationType.UUID)
	public String  getId(){
		return id;
	} 
		
	public void setId(String id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "password")
	public String  getPassword(){
		return password;
	} 
		
	public void setPassword(String password){
			this.password = password;
		}
		
    @Column(name = "enabled")
	public String  getEnabled(){
		return enabled;
	} 
		
	public void setEnabled(String enabled){
			this.enabled = enabled;
		}
		
    @Column(name = "note")
	public String  getNote(){
		return note;
	} 
		
	public void setNote(String note){
			this.note = note;
		}
		
    @Column(name = "dept_id")
	public Long  getDeptId(){
		return deptId;
	} 
		
	public void setDeptId(Long deptId){
			this.deptId = deptId;
		}

    @Column(name = "mobile")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

    @Column(name = "real_name")
	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

    @Column(name = "id_card_num")
	public String getIdCardNum() {
		return idCardNum;
	}

	public void setIdCardNum(String idCardNum) {
		this.idCardNum = idCardNum;
	}

    @Column(name = "addr")
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

    @Column(name = "hire_date")
	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

    @Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

    @Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Transient
	public String[] getRoleId() {
		return roleId;
	}

	public void setRoleId(String[] roleId) {
		this.roleId = roleId;
	}

    @Column(name = "active_time")
	public Date getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Date activeTime) {
		this.activeTime = activeTime;
	}
	


} 
