/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 系统缓存的对象
 */
public enum SystemCacheEnum implements StringEnum {
	/**  
	 * 分类树
	 */
	CATEGORY("CATEGORY"),
	
	/**
	 * 属性
	 */
	PROPERTIES("PROPERTIES");

	private final String value;

	private SystemCacheEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		SystemCacheEnum[] enums = values();
		for (SystemCacheEnum appEnum : enums) {
			if (appEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
