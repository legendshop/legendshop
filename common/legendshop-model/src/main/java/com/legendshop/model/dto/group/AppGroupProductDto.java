/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.dto.ProdPicDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.SkuDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 团购商品dto
 * 
 */
@ApiModel(value="App团购商品Dto") 
public class AppGroupProductDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2643640716122153527L;

	/** 商品id. */
	@ApiModelProperty(value="商品id") 
	private Long prodId;

	/** 商品名称. */
	@ApiModelProperty(value="商品名称") 
	private String name;

	/** 简要描述,卖点等. */
	@ApiModelProperty(value="简要描述,卖点等") 
	private String brief;

	/** 商品原价. */
	@ApiModelProperty(value="商品原价") 
	private Double price;

	/** 商品现价. */
	@ApiModelProperty(value="商品现价") 
	private Double cash;

	/** 商品主图. */
	@ApiModelProperty(value="商品主图") 
	private String pic;

	/** 商品状态. */
	@ApiModelProperty(value="商品状态") 
	private Integer status;

	/** 商品数量. */
	@ApiModelProperty(value="商品数量") 
	private Integer stocks;

	/** 商品图文详情内容. */
	@ApiModelProperty(value="商品图文详情内容") 
	private String content;

	/** 商品属性集合. */
	@ApiModelProperty(value="商品属性集合") 
	private List<ProductPropertyDto> prodPropList;

	/** 商品图片. */
	@ApiModelProperty(value="商品图片") 
	private List<ProdPicDto> prodPics;

	/** 商品sku集合. */
	@ApiModelProperty(value="商品sku集合") 
	private List<SkuDto> skuDtoList;

	/** 商品属性图片. */
	@ApiModelProperty(value="商品属性图片") 
	List<PropValueImgDto> propValueImgList;

	/** 商品视频 **/
	@ApiModelProperty(value="商品视频")
	private String proVideoUrl;

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId
	 *            the prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the brief.
	 *
	 * @return the brief
	 */
	public String getBrief() {
		return brief;
	}

	/**
	 * Sets the brief.
	 *
	 * @param brief
	 *            the brief
	 */
	public void setBrief(String brief) {
		this.brief = brief;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price
	 *            the price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash
	 *            the cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic
	 *            the pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the stocks.
	 *
	 * @return the stocks
	 */
	public Integer getStocks() {
		return stocks;
	}

	/**
	 * Sets the stocks.
	 *
	 * @param stocks
	 *            the stocks
	 */
	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 *
	 * @param content
	 *            the content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the prod prop list.
	 *
	 * @return the prod prop list
	 */
	public List<ProductPropertyDto> getProdPropList() {
		return prodPropList;
	}

	/**
	 * Sets the prod prop list.
	 *
	 * @param prodPropList
	 *            the prod prop list
	 */
	public void setProdPropList(List<ProductPropertyDto> prodPropList) {
		this.prodPropList = prodPropList;
	}

	/**
	 * Gets the prod pics.
	 *
	 * @return the prod pics
	 */
	public List<ProdPicDto> getProdPics() {
		return prodPics;
	}

	/**
	 * Sets the prod pics.
	 *
	 * @param prodPics
	 *            the prod pics
	 */
	public void setProdPics(List<ProdPicDto> prodPics) {
		this.prodPics = prodPics;
	}

	/**
	 * Gets the sku dto list.
	 *
	 * @return the sku dto list
	 */
	public List<SkuDto> getSkuDtoList() {
		return skuDtoList;
	}

	/**
	 * Sets the sku dto list.
	 *
	 * @param skuDtoList
	 *            the sku dto list
	 */
	public void setSkuDtoList(List<SkuDto> skuDtoList) {
		this.skuDtoList = skuDtoList;
	}

	/**
	 * Gets the prop value img list.
	 *
	 * @return the prop value img list
	 */
	public List<PropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	/**
	 * Sets the prop value img list.
	 *
	 * @param propValueImgList
	 *            the prop value img list
	 */
	public void setPropValueImgList(List<PropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}
}
