package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

/**
 * 装修弹层商品DTO
 */
public class DecorateProdDto implements Serializable  {

	
	/**  */
	private static final long serialVersionUID = 7571919886736103222L;

	/** 商品ID */
	private Long prodId;
	
	/** 商品名称  */
	private String spuName;
	
	/** 价格  */
	private Double price;
	
	/** 商品图片 */
	private String pic;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getSpuName() {
		return spuName;
	}

	public void setSpuName(String spuName) {
		this.spuName = spuName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}