package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 预存款提现记录Dto
 */
@ApiModel(value="预存款提现记录Dto") 
public class AppPdWithdrawCashDto implements Serializable{

	/**  */
	private static final long serialVersionUID = 934433330825151004L;

	/** 主键id */
	@ApiModelProperty(value = "主键id")
	private Long id; 
		
	/** 提现单号  */
	@ApiModelProperty(value = "提现单号  ")
	private String pdcSn; 
		
	/** 用户id  */
	@ApiModelProperty(value = "用户id")
	private String userId; 
		
	/** 用户名 */
	@ApiModelProperty(value = "用户名")
	private String userName; 
		
	/** 提现金额 */
	@ApiModelProperty(value = "提现金额")
	private Double amount; 
		
	/** 提现方式 */
	@ApiModelProperty(value = "提现方式")
	private String bankName; 
		
	/** 提现账号 */
	@ApiModelProperty(value = "提现账号")
	private String bankNo; 
		
	/** 开户人姓名 */
	@ApiModelProperty(value = "开户人姓名")
	private String bankUser; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间")
	private Date addTime; 
		
	/** 付款时间[后台管理员] */
	@ApiModelProperty(value = "付款时间[后台管理员]")
	private Date paymentTime; 
		
	/** 提现支付状态 [0:待审核 1：已完成 -1：已拒绝] */
	@ApiModelProperty(value = "提现支付状态 [0:待审核 1：已完成 -1：已拒绝]")
	private Integer paymentState; 
		
	/** 支付管理员 */
	@ApiModelProperty(value = "支付管理员")
	private String updateAdminName; 
		
	/** 管理员备注 */
	@ApiModelProperty(value = "管理员备注 ")
	private String adminNote; 
		
	/** 用户提现备注 */
	@ApiModelProperty(value = "用户提现备注")
	private String userNote;

	public AppPdWithdrawCashDto() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPdcSn() {
		return pdcSn;
	}

	public void setPdcSn(String pdcSn) {
		this.pdcSn = pdcSn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankNo() {
		return bankNo;
	}

	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}

	public String getBankUser() {
		return bankUser;
	}

	public void setBankUser(String bankUser) {
		this.bankUser = bankUser;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public Integer getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(Integer paymentState) {
		this.paymentState = paymentState;
	}

	public String getUpdateAdminName() {
		return updateAdminName;
	}

	public void setUpdateAdminName(String updateAdminName) {
		this.updateAdminName = updateAdminName;
	}

	public String getAdminNote() {
		return adminNote;
	}

	public void setAdminNote(String adminNote) {
		this.adminNote = adminNote;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}
}
