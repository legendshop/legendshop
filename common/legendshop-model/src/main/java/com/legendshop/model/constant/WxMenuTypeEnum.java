package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 微信菜单类型
 * @author tony
 *
 */
public enum WxMenuTypeEnum  implements  StringEnum{
	
	VIEW("view"), //页面跳转
	
	CLICK("click"); //事件点击
	
	/** The value. */
	private final String value;
	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private WxMenuTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
