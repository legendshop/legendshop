/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import com.legendshop.util.AppUtils;

/**
 *  佣金奖励类型
 */
public enum CommisTypeEnum {

	//{ 'FIRST_COMMIS':'直接下级奖励', 'SECOND_COMMIS':'下二级奖励', 'THIRD_COMMIS':'下三级奖励','REG_COMMIS':'推广注册奖励', 'REDUCE_COMMIS':'商品退款，回退奖励' }
	/** 一级分销佣金奖励 */
	FIRST_COMMIS("FIRST_COMMIS", "直接下级奖励"),
	
	/** 二级分销佣金奖励 */
	SECOND_COMMIS("SECOND_COMMIS", "下二级奖励"),

	/** 三级分销佣金奖励 */
	THIRD_COMMIS("THIRD_COMMIS", "下三级奖励"),
	
	/** 佣金奖励 */
	REG_COMMIS("REG_COMMIS", "推广注册奖励"),
	
	/** 回退佣金奖励 */
	REDUCE_COMMIS("REDUCE_COMMIS", "商品退款，回退奖励"),

	/**
	 * 其它（不可能会返回"其它"的，如果返回，就是有新增的状态没有加到该枚举类中）
	 */
	OTHER("other", "其它")
	;
	
	/** The value. */
	private final String value;

	private final String desc;

	private CommisTypeEnum(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String value() {
		return this.value;
	}

	public static String getDesc(String value) {
		if (AppUtils.isNotBlank(value)) {
			CommisTypeEnum[] licenseEnums = values();
			for (CommisTypeEnum licenseEnum : licenseEnums) {
				if (licenseEnum.value.equals(value)) {
					return licenseEnum.desc;
				}
			}
		}
		return OTHER.desc;
	}
}
