package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *移动端商品分组条件DTO
 */
@ApiModel(value="移动端商品分组条件DTO") 
public class ProdGroupConditionalDto implements Serializable  {
	
	
	/**  */
	private static final long serialVersionUID = -912887183076192088L;
	
	
	/** 条件类型 */
	@ApiModelProperty(value="条件类型")  
	private String type; 
		
	
	public ProdGroupConditionalDto() {
    }


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}

} 
