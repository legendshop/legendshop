/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

/**
 * The Class SaveToCartResult.
 */
public class SaveToCartResult {
	
	/** 是否成功加入购物车 **/
	private SaveToCartStatusEnum statusEnum;
	
	/** 是否加入新的商品 **/ 
	private boolean addNewProd;
	
	public SaveToCartResult(SaveToCartStatusEnum statusEnum){
		this.statusEnum = statusEnum;
		this.addNewProd = false;
	}
	
	public SaveToCartResult(SaveToCartStatusEnum statusEnum, boolean addNewProd){
		this.statusEnum = statusEnum;
		this.addNewProd = addNewProd;
	}
	

	public SaveToCartStatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(SaveToCartStatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}

	public boolean isAddNewProd() {
		return addNewProd;
	}

	public void setAddNewProd(boolean addNewProd) {
		this.addNewProd = addNewProd;
	}


}
