/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 商城状态
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------.
 */
public enum ShopOPstatusEnum implements IntegerEnum {
	/**等待审核*/
	AUDITING(0),
	
	/**联系人信息完善*/
	CONTACT_INFO_OK(1),

	/**公司信息完善*/
	COMPANY_INFO_OK(2),

	/**店铺信息完善*/
	SHOP_INFO_OK(3),
	/**完成*/
	AUDITED(4);


	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ShopOPstatusEnum(Integer num) {
		this.num = num;
	}

}
