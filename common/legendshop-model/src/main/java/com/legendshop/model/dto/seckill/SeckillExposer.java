/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 秒杀开关
 *
 */
@ApiModel(value="秒杀开关")
public class SeckillExposer {

	/** 秒杀商品的id */
	@ApiModelProperty(value="秒杀商品的id")
	private long seckillId;

	/** 秒杀地址加密. */
	@ApiModelProperty(value="秒杀地址加密")
	private String md5;

	/** 秒杀开关. */
	@ApiModelProperty(value="秒杀开关")
	private boolean isExpose;

	/** 秒杀的开始时间. */
	@ApiModelProperty(value="秒杀的开始时间")
	private Date startTime;

	/** 秒杀结束时间. */
	@ApiModelProperty(value="秒杀结束时间")
	private Date endTime;

	/**
	 * Gets the seckill id.
	 *
	 * @return the seckill id
	 */
	public long getSeckillId() {
		return seckillId;
	}

	/**
	 * Sets the seckill id.
	 *
	 * @param seckillId
	 *            the seckill id
	 */
	public void setSeckillId(long seckillId) {
		this.seckillId = seckillId;
	}

	/**
	 * Gets the md5.
	 *
	 * @return the md5
	 */
	public String getMd5() {
		return md5;
	}

	/**
	 * Sets the md5.
	 *
	 * @param md5
	 *            the md5
	 */
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	/**
	 * Gets the is expose.
	 *
	 * @return the checks if is expose
	 */
	public boolean getIsExpose() {
		return isExpose;
	}

	/**
	 * Sets the is expose.
	 *
	 * @param isExpose
	 *            the checks if is expose
	 */
	public void setIsExpose(boolean isExpose) {
		this.isExpose = isExpose;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime
	 *            the start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime
	 *            the end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * The Constructor.
	 *
	 * @param isExpose
	 *            the is expose
	 */
	public SeckillExposer(boolean isExpose) {
		super();
		this.isExpose = isExpose;
	}

	/**
	 * The Constructor.
	 *
	 * @param isExpose
	 *            the is expose
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 */
	public SeckillExposer(boolean isExpose, Date startTime, Date endTime) {
		super();
		this.isExpose = isExpose;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/**
	 * The Constructor.
	 *
	 * @param seckillId
	 *            the seckill id
	 * @param md5
	 *            the md5
	 * @param isExpose
	 *            the is expose
	 */
	public SeckillExposer(long seckillId, String md5, boolean isExpose) {
		super();
		this.seckillId = seckillId;
		this.md5 = md5;
		this.isExpose = isExpose;
	}

}
