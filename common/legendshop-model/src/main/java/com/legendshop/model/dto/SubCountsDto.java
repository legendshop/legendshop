package com.legendshop.model.dto;

public class SubCountsDto {

	private Integer status;
	
	private Integer subCounts;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSubCounts() {
		return subCounts;
	}

	public void setSubCounts(Integer subCounts) {
		this.subCounts = subCounts;
	}
	
	
}
