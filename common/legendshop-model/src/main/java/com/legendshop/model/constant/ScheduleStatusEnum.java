package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 定时计划状态
 */
public enum ScheduleStatusEnum implements IntegerEnum {

	/** 未执行 */
	NONEXECUTION(0),

	/** 已执行 */
	EXECUTED(1),
	
	/** 失效 */
	LOSE_EFFICACY(-1);

	private Integer num;

	public Integer value() {
		return num;
	}

	ScheduleStatusEnum(Integer num) {
		this.num = num;
	}
}

