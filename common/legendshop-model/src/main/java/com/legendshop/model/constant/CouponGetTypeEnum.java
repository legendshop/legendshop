/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 礼券
 * 领取方式：积分兑换:points，卡密兑换:pwd，免费领取:free
 */
public enum CouponGetTypeEnum implements StringEnum {

	/** 积分兑换*/
	POINTS("points"),

	/** 卡密兑换*/
	PWD("pwd"),
	
	/** 免费领取*/
	FREE("free"); 

	/** The num. */
	private String type;

	CouponGetTypeEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}

}
