package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 店铺域名
 */
@Entity
@Table(name = "ls_shop_domain")
public class ShopDomain implements GenericEntity<Long> {

	private static final long serialVersionUID = 1077108542147735840L;

	/**  */
	private Long id; 
		
	/** shopId */
	private Long shopId; 
		
	/** 独立域名 */
	private String domainName; 
		
	/** 二级域名 */
	private String secDomainName; 
		
	/** 备案信息 */
	private String icpInfo; 
		
	/** 审核意见 */
	private String auditOpinion; 
		
	/** 审核时间 */
	private Date auditTime; 
		
	/** 状态 */
	private Long status; 
	
	/**
	 * 店铺名称
	 */
	private String shopName;
	
	public ShopDomain() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_DOMAIN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "domain_name")
	public String  getDomainName(){
		return domainName;
	} 
		
	public void setDomainName(String domainName){
		if(domainName != null){
			String domain = domainName.trim();
			if(domain.toLowerCase().startsWith("http://")){
				domain = domain.substring(7);
			}
			
			if(domain.startsWith("www.")){
				this.domainName = domain.substring(4).trim();
				return;
			}
			
			this.domainName = domain.trim();
		}
	}
		
    @Column(name = "sec_domain_name")
	public String  getSecDomainName(){
		return secDomainName;
	} 
		
	public void setSecDomainName(String secDomainName){
			this.secDomainName = secDomainName;
		}
		
    @Column(name = "icp_info")
	public String  getIcpInfo(){
		return icpInfo;
	} 
		
	public void setIcpInfo(String icpInfo){
			this.icpInfo = icpInfo;
		}
		
    @Column(name = "audit_opinion")
	public String  getAuditOpinion(){
		return auditOpinion;
	} 
		
	public void setAuditOpinion(String auditOpinion){
			this.auditOpinion = auditOpinion;
		}
		
    @Column(name = "audit_time")
	public Date  getAuditTime(){
		return auditTime;
	} 
		
	public void setAuditTime(Date auditTime){
			this.auditTime = auditTime;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
	}
	
	@Transient
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

} 
