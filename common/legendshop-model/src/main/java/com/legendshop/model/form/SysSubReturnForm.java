package com.legendshop.model.form;

import java.util.Date;

/**
 * 订单退款参数
 * 
 */
public class SysSubReturnForm {

	private Double returnAmount; // 退款总金额
	
	private Double payAmount; // 外部第三方支付金额

	private String subSettlementSn; // 商户支付单据号

	private String flowTradeNo; // 流水号(支付宝交易号)
	
	/**
	 * 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
	 */
	private String outRequestNo; // 商城的退款流水(用于退款申请)

	private String refundTypeId; // 退款方式

	private Date orderDateTime;

	public Double getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(Double returnAmount) {
		this.returnAmount = returnAmount;
	}

	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	public Date getOrderDateTime() {
		return orderDateTime;
	}

	public void setOrderDateTime(Date orderDateTime) {
		this.orderDateTime = orderDateTime;
	}

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public String getRefundTypeId() {
		return refundTypeId;
	}

	public void setRefundTypeId(String refundTypeId) {
		this.refundTypeId = refundTypeId;
	}

	public String getOutRequestNo() {
		return outRequestNo;
	}

	public void setOutRequestNo(String outRequestNo) {
		this.outRequestNo = outRequestNo;
	}

	public Double getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}
	

}
