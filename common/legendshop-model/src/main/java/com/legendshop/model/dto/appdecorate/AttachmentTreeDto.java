package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 图片目录DTO
 */
public class AttachmentTreeDto implements Serializable  {
	

	/**  */
	private static final long serialVersionUID = -4994274590288509946L;

	/** 目录ID */
	private Long id;
	
	/** 目录名称  */
	private String name;
	
	/** 父目录ID */
	private Long parentId;
	
	/** 子目录*. */
	private List<AttachmentTreeDto> childrenList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<AttachmentTreeDto> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<AttachmentTreeDto> childrenList) {
		this.childrenList = childrenList;
	}
	
	public void addChildren(AttachmentTreeDto categoryDto) {
		if(childrenList == null){
			childrenList = new ArrayList<AttachmentTreeDto>();
		}
		childrenList.add(categoryDto);
	}

}