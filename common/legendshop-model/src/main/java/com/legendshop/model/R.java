package com.legendshop.model;

import com.legendshop.model.constant.CommonConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @param <T>
 * @author Joe
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "响应信息主体")
public class R<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@ApiModelProperty(value = "返回标记：成功标记=0，失败标记=1")
	private int code;

	@Getter
	@Setter
	@ApiModelProperty(value = "返回信息")
	private String msg;


	@Getter
	@Setter
	@ApiModelProperty(value = "请求结果")
	private Boolean success;


	@Getter
	@Setter
	@ApiModelProperty(value = "数据")
	private T data;

	public static <T> R<T> success() {
		return restResult(null, CommonConstants.SUCCESS, null,true);
	}

	public static <T> R<T> success(T data) {
		return restResult(data, CommonConstants.SUCCESS, null,true);
	}
	public static <T> R<T> success(String msg) {
		return restResult(null, CommonConstants.SUCCESS, msg,true);
	}

	public static <T> R<T> success(T data, String msg) {
		return restResult(data, CommonConstants.SUCCESS, msg,true);
	}

	public static <T> R<T> fail() {
		return restResult(null, CommonConstants.FAIL, null,false);
	}

	public static <T> R<T> fail(String msg) {
		return restResult(null, CommonConstants.FAIL, msg,false);
	}

	public static <T> R<T> fail(T data) {
		return restResult(data, CommonConstants.FAIL, null,false);
	}

	public static <T> R<T> fail(T data, String msg) {
		return restResult(data, CommonConstants.FAIL, msg,false);
	}


	private static <T> R<T> restResult(T data, int code, String msg, Boolean success) {
		R<T> apiResult = new R<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(msg);
		apiResult.setSuccess(success);
		return apiResult;
	}
}
