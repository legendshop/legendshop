package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Transient;

public class OrderSub extends Sub {

	/**
	 * 用户订单地址
	 */
	private static final long serialVersionUID = 1L;

	private String receiver;

	private String subAdds;

	private String subPost;

	private String province;

	private String city;

	private String area;

	private String mobile;

	private String telphone;

	private String email;
	

	@Transient
	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	@Transient
	public String getSubAdds() {
		return subAdds;
	}

	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}

	@Transient
	public String getSubPost() {
		return subPost;
	}

	public void setSubPost(String subPost) {
		this.subPost = subPost;
	}

	
	
	@Transient
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Transient
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Transient
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Transient
	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	@Transient
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
