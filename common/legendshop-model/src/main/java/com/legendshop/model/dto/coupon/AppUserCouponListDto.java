/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.coupon;

import java.util.List;

/**
 * 用户优惠卷集合Dto
 *
 */
public class AppUserCouponListDto {

	private List<AppUserCouponDto> usableUserCouponList ;//可用的优惠券和红包
	
	private List<AppUserCouponDto> disableUserCouponList; //不可用的优惠券和红包

	public List<AppUserCouponDto> getUsableUserCouponList() {
		return usableUserCouponList;
	}

	public void setUsableUserCouponList(List<AppUserCouponDto> usableUserCouponList) {
		this.usableUserCouponList = usableUserCouponList;
	}

	public List<AppUserCouponDto> getDisableUserCouponList() {
		return disableUserCouponList;
	}

	public void setDisableUserCouponList(List<AppUserCouponDto> disableUserCouponList) {
		this.disableUserCouponList = disableUserCouponList;
	}
	
}
