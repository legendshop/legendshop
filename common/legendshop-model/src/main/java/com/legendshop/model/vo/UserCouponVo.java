package com.legendshop.model.vo;

import java.util.Date;

/**
 *礼券赠送用户参数 
 */
public class UserCouponVo {

	/** The user id. */
	private String userId;
	
	
	/** The user name. */
	private String userName;

	/** 昵称 */
	private String nickName;
	
	/** The user mail. */
	private String userMail;
	
	/** The user mobile. */
	private String userMobile;
	
	/** The user regtime. */
	private Date userRegtime;
	
	
	private Long logNumber;
	
	private Long totalAmount;
	
	private Long orderNumber;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public Date getUserRegtime() {
		return userRegtime;
	}

	public void setUserRegtime(Date userRegtime) {
		this.userRegtime = userRegtime;
	}

	public Long getLogNumber() {
		return logNumber;
	}

	public void setLogNumber(Long logNumber) {
		this.logNumber = logNumber;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	public UserCouponVo(String userId, String userName, String nickName, String userMail, String userMobile, Date userRegtime, Long logNumber, Long totalAmount, Long orderNumber) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.nickName = nickName;
		this.userMail = userMail;
		this.userMobile = userMobile;
		this.userRegtime = userRegtime;
		this.logNumber = logNumber;
		this.totalAmount = totalAmount;
		this.orderNumber = orderNumber;
	}

	public UserCouponVo() {
	}
	
}
