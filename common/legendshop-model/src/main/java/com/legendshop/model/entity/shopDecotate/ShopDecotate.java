package com.legendshop.model.entity.shopDecotate;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *店铺装修
 */
@Entity
@Table(name = "ls_shop_decotate")
public class ShopDecotate implements GenericEntity<Long> {
	
	private static final long serialVersionUID = -1680308421308665260L;

	/** ID */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 是否启用Banner */
	private Integer isBanner; 
		
	/** 是否启用店铺信息 */
	private Integer isInfo; 
		
	/** 是否启用店铺导航 */
	private Integer isNav; 
		
	/** 是否启用店铺轮播图 */
	private Integer isSlide; 
		
	/** 主题样式 */
	private String topCss; 
		
	/** 背景图片文件ID */
	private String bgImgId; 
		
	/** 背景颜色 */
	private String bgColor; 
		
	/** 背景样式[repeat: 平铺, stretch: 拉伸] */
	private String bgStyle; 
	
	/** 创建时间 */
	private Date createDate;
		
	
	public ShopDecotate() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_DECOTATE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "is_banner")
	public Integer  getIsbanner(){
		return isBanner;
	} 
		
	public void setIsbanner(Integer isbanner){
			this.isBanner = isbanner;
		}
		
    @Column(name = "is_info")
	public Integer  getIsInfo(){
		return isInfo;
	} 
		
	public void setIsInfo(Integer isInfo){
			this.isInfo = isInfo;
		}
		
    @Column(name = "is_nav")
	public Integer  getIsNav(){
		return isNav;
	} 
		
	public void setIsNav(Integer isNav){
			this.isNav = isNav;
		}
		
    @Column(name = "is_slide")
	public Integer  getIsSlide(){
		return isSlide;
	} 
		
	public void setIsSlide(Integer isSlide){
			this.isSlide = isSlide;
		}
		
    @Column(name = "top_css")
	public String  getTopCss(){
		return topCss;
	} 
		
	public void setTopCss(String topCss){
			this.topCss = topCss;
		}
		
    @Column(name = "bg_img_id")
	public String  getBgImgId(){
		return bgImgId;
	} 
		
	public void setBgImgId(String bgImgId){
			this.bgImgId = bgImgId;
		}
		
    @Column(name = "bg_color")
	public String  getBgColor(){
		return bgColor;
	} 
		
	public void setBgColor(String bgColor){
			this.bgColor = bgColor;
		}
		
    @Column(name = "bg_style")
	public String  getBgStyle(){
		return bgStyle;
	} 
		
	public void setBgStyle(String bgStyle){
			this.bgStyle = bgStyle;
		}

	 @Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	


} 
