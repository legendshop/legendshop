package com.legendshop.model.dto.buy;

import java.io.Serializable;


/**
 * 门店信息
 * @author Tony
 *
 */
public class ChainDto  implements Serializable{

	private static final long serialVersionUID = 8344979499349629974L;

	/** 门店ID */
	private Long id; 
	
	/** 提货人  */
	private String buyerName;
	
	/** 提货电话  */
	private String telPhone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getTelPhone() {
		return telPhone;
	}

	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}

	public ChainDto(Long id, String buyerName, String telPhone) {
		super();
		this.id = id;
		this.buyerName = buyerName;
		this.telPhone = telPhone;
	}
	
	
	
	
}
