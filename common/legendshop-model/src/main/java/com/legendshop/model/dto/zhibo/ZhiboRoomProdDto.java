/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.zhibo;

/**
 * 直播房间商品Dto.
 */
public class ZhiboRoomProdDto {

	/** The prod id. */
	private Long prodId;
	
	/** The shop id. */
	private Long shopId;
	
	/** The name. */
	private String name;
	
	/** The cash. */
	private Double cash;
	
	/** The pic. */
	private String pic;
	
	/** The stocks. */
	private Integer stocks;
	
	/** The av room id. */
	private Long avRoomId;

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the new cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the stocks.
	 *
	 * @return the stocks
	 */
	public Integer getStocks() {
		return stocks;
	}

	/**
	 * Sets the stocks.
	 *
	 * @param stocks the new stocks
	 */
	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	/**
	 * Gets the av room id.
	 *
	 * @return the av room id
	 */
	public Long getAvRoomId() {
		return avRoomId;
	}

	/**
	 * Sets the av room id.
	 *
	 * @param avRoomId the new av room id
	 */
	public void setAvRoomId(Long avRoomId) {
		this.avRoomId = avRoomId;
	}
	

	
	
} 
