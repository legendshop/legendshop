/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.security;

import java.util.Collection;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;

/**
 * 用户实体
 */
public class UserEntity implements GenericEntity<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8401714272377570649L;

	/** The id. */
	private String id;

	/** The name. */
	private String name;

	/** The password. */
	private String password;

	/** The passwordag. */
	private String passwordag;

	/** 用户状态 1：有效， 0：无效. */
	private String enabled;

	/** The note. */
	private String note;

	/** 用户商城等级 **/
	private Integer gradeId;

	/** 头像 **/
	private String portraitPic;

	/** 昵称 **/
	private String nickName;

	/** 部门Id */
	private Integer deptId;

	private Long shopId;

	// 商城所在的用户
	private ShopUser shopUser;

	private String openId;

	private String shopName;

	/** 角色 **/
	private Collection<String> roles;

	/** 权限 **/
	private List<String> functions;

	public UserEntity() {

	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the passwordag.
	 * 
	 * @return the passwordag
	 */
	public String getPasswordag() {
		return passwordag;
	}

	/**
	 * Sets the passwordag.
	 * 
	 * @param passwordag
	 *            the new passwordag
	 */
	public void setPasswordag(String passwordag) {
		this.passwordag = passwordag;
	}

	/**
	 * Gets the enabled.
	 * 
	 * @return the enabled
	 */
	public String getEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param enabled
	 *            the new enabled
	 */
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	/**
	 * Gets the note.
	 * 
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Sets the note.
	 * 
	 * @param note
	 *            the new note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public List<String> getFunctions() {
		return functions;
	}

	public void setFunctions(List<String> functions) {
		this.functions = functions;
	}

	public ShopUser getShopUser() {
		return shopUser;
	}

	public void setShopUser(ShopUser shopUser) {
		this.shopUser = shopUser;
	}

}