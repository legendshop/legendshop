package com.legendshop.model.dto;

import java.math.BigDecimal;

import com.legendshop.model.entity.Coupon;

/**
 * @author quanzc
 *  抽奖活动奖项
 */
public class DrawAwardsDto {
    
	private Integer id; //1.2.3分别匹配一、二、三等奖
	
	private String giftName;//奖品名称
	
	private BigDecimal cash; //现金
	
	private Integer integral;//积分
	
	private Long coupon; //红包、优惠券
	
	private Integer awardsType;//奖项类型（1：实体  2:现金  3：积分）
	
	private Integer amount;//奖项数量
	
	private Integer weight;//权重
	
	private Coupon drawCoupon; //红包、优惠券详情

	public Coupon getDrawCoupon() {
		return drawCoupon;
	}

	public void setDrawCoupon(Coupon drawCoupon) {
		this.drawCoupon = drawCoupon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public BigDecimal getCash() {
		return cash;
	}

	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}

	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}

	public Integer getAwardsType() {
		return awardsType;
	}

	public void setAwardsType(Integer awardsType) {
		this.awardsType = awardsType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getGiftName() {
		return giftName;
	}

	public void setGiftName(String giftName) {
		this.giftName = giftName;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Long getCoupon() {
		return coupon;
	}

	public void setCoupon(Long coupon) {
		this.coupon = coupon;
	}

	
	
} 
