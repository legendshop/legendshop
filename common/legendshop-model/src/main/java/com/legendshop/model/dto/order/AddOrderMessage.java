/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import java.io.Serializable;

/**
 * 加入购物车结果
 */
public class AddOrderMessage implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4451205566011169842L;

	/** The result. */
	private boolean result = false;
	
	/** The code 默认OK为成功. */
	private String code = "OK";
	
	/** The message. */
	private String message;
	
	/** The url. */
	private String url;

	/**
	 * Checks if is result.
	 *
	 * @return true, if is result
	 */
	public boolean isResult() {
		return result;
	}
	
	/**
	 * 是否正确
	 * @return
	 */
	public boolean hasError() {
		return !"OK".equals(code);
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(boolean result) {
		this.result = result;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
}
