package com.legendshop.model.dto.weibo;

/**
 * 微博授权用户信息
 * @author 开发很忙
 */
public class WeiboAuthorizeUserInfo extends WeiboReponseMsg{
	
	/** 字符串的用户ID */
	private String idstr;
	
	/** 用户昵称 */
	private String screen_name;
	
	/** 性别，m：男、f：女、n：未知 */
	private String gender;
	
	/** 用户头像地址（高清），高清头像原图 */
	private String avatar_hd;
	
	public String getIdstr() {
		return idstr;
	}

	public void setIdstr(String idstr) {
		this.idstr = idstr;
	}

	public String getScreen_name() {
		return screen_name;
	}

	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAvatar_hd() {
		return avatar_hd;
	}

	public void setAvatar_hd(String avatar_hd) {
		this.avatar_hd = avatar_hd;
	}
}
