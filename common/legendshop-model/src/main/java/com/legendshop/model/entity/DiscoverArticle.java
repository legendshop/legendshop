package com.legendshop.model.entity;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 * 发现文章表
 */
@Entity
@Table(name = "ls_discover_article")
public class DiscoverArticle implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** id */
	private Long id;

	/** 文章名字 */
	private String name;

	/** 文章简介 */
	private String intro;

	/** 文章内容 */
	private String content;

	/** 文章审核状态(0:下线;1:上线) */
	private Long status;

	/** 文章的创建时间 */
	private Date createTime;

	/** 文章的发表时间 */
	private Date publishTime;

	/** 访问量 */
	private Long pageView;

	/** 点赞数 */
	private Long thumbNum;

	/** 评论数 */
	private Long commentsNum;

	/** 文章图片 */
	private String image;

	/** 文章作者 */
	private String writerName;

	/** 图片文件 */
	private MultipartFile picFile;

	public DiscoverArticle() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DISCOVER_ARTICLE_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "intro")
	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "status")
	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "publish_time")
	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	@Column(name = "page_view")
	public Long getPageView() {
		return pageView;
	}

	public void setPageView(Long pageView) {
		this.pageView = pageView;
	}

	@Column(name = "thumb_num")
	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	@Column(name = "comments_num")
	public Long getCommentsNum() {
		return commentsNum;
	}

	public void setCommentsNum(Long commentsNum) {
		this.commentsNum = commentsNum;
	}

	@Column(name = "image")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Transient
	public MultipartFile getPicFile() {
		return picFile;
	}

	public void setPicFile(MultipartFile picFile) {
		this.picFile = picFile;
	}

	@Column(name = "writer_name")
	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

}
