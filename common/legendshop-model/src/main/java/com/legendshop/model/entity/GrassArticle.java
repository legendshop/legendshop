package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *种草社区文章表
 */
@Entity
@Table(name = "ls_grass_article")
public class GrassArticle implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 文章标题 */
	private String title; 
		
	/** 文章简介 */
	private String intro; 
		
	/** 文章内容 */
	private String content; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 发布时间 */
	private Date publishTime; 
		
	/** 点赞数 */
	private Long thumbNum; 
		
	/** 评论数 */
	private Long commentsNum;
	
	/** 作者姓名 */
	private String writerName; 
		
	/** 文章图片 */
	private String image; 
		
	/** 用户id */
	private String userId; 
	
	/** 图片文件 */
	private MultipartFile picFile;
	
	public GrassArticle() {
    }
	
	
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GRASS_ARTICLE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "intro")
	public String  getIntro(){
		return intro;
	} 
		
	public void setIntro(String intro){
			this.intro = intro;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "publish_time")
	public Date  getPublishTime(){
		return publishTime;
	} 
		
	public void setPublishTime(Date publishTime){
			this.publishTime = publishTime;
		}
		
    @Column(name = "thumb_num")
	public Long  getThumbNum(){
		return thumbNum;
	} 
		
	public void setThumbNum(Long thumbNum){
			this.thumbNum = thumbNum;
		}
		
    @Column(name = "comments_num")
	public Long  getCommentsNum(){
		return commentsNum;
	} 
		
	public void setCommentsNum(Long commentsNum){
			this.commentsNum = commentsNum;
		}
		
    @Column(name = "image")
	public String  getImage(){
		return image;
	} 
		
	public void setImage(String image){
			this.image = image;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}


	@Transient
	public MultipartFile getPicFile() {
		return picFile;
	}



	public void setPicFile(MultipartFile picFile) {
		this.picFile = picFile;
	}


    @Column(name = "writer_name")
	public String getWriterName() {
		return writerName;
	}



	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	


} 
