/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 商品咨询类型 The Enum CpnsTypeEnum.
 */
public enum SendCouponTypeEnum implements IntegerEnum {

	ONLINE(0), // 线上用户

	OFFLINE(1);// 线下发送


	/** The num. */
	private Integer type;

	/**
	 * Instantiates a new consult type enum.
	 * 
	 * @param num
	 *            the num
	 */
	SendCouponTypeEnum(Integer type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	@Override
	public Integer value() {
		return type;
	}

}
