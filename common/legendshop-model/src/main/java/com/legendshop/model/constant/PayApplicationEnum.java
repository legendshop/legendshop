package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 支付用途
 * 
 * @author Tony
 * 
 */
public enum PayApplicationEnum implements StringEnum {

	ADVANCE_PAYMENT("advance_payment"), // 预付款充值

	ORDER_PAYMENT("order_payment"), // 订单支付

	;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private PayApplicationEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

}
