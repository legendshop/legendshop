/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.BaseAttributeKeys;

/**
 * LegendShop 常量定义.
 */
public interface AttributeKeys extends BaseAttributeKeys {

	/** 下载的文件路径. */
	public static final String DOWNLOAD_PATH = "WEB-INF/pages/download";

	/** 用户名. */
	public static final String USER_NAME = "SPRING_SECURITY_LAST_USERNAME";
	
	/** 商城Id. */
	public static final String SHOP_ID = "SPRING_SECURITY_LAST_SHOP_ID";
	
	/** 商城状态. */
	public static final String SHOP_STATUS = "SPRING_SECURITY_LAST_SHOP_STATUS";

	/** The Constant SPRING_SECURITY_CONTEXT. */
	public static final String SPRING_SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";

	/** The Constant RETURN_URL. */
	public static final String RETURN_URL = "returnUrl";

	/** 排序指示器，作为全局参数不可以修改. */
	public static final String ORDER_INDICATOR = "orderIndicator";

	/** The Constant EDITOR_PIC_PATH. */
	public static final String EDITOR_PIC_PATH = "/editor";

	/** The Constant RANDNUMBER. */
	public static final String RANDNUMBER = "LEGENDSHOP_RANDNUM";

	/** 登录重复次数. */
	public static final String TRY_LOGIN_COUNT = "TRY_LOGIN_COUNT";


	/** 图片随机验证码 *. */
	public static final String RANDOM_VALIDATE_CODE_KEY = "RANDOM_VALIDATE_CODE_KEY";
	
	
	/** 用户等级. */
	public static final String USER_GRADE = "LAST_USERGRADE";


}
