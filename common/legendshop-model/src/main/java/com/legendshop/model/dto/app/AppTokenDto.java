package com.legendshop.model.dto.app;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 用户登录凭证
 */
@ApiModel(value="Token信息") 
public class AppTokenDto implements Serializable {

	private static final long serialVersionUID = 6806801785535245685L;
	
	@ApiModelProperty(value="Token信息唯一ID")  
	private Long id;
	
	/** 用户Id */
	@ApiModelProperty(value="用户ID")  
	private String userId; 
		
	/** 商家Id */
	@ApiModelProperty(value="商家Id")  
	private Long shopId; 
	
	/** 用户名 */
	@ApiModelProperty(value="用户名")  
	private String userName; 
		
	/** token */
	@ApiModelProperty(value="accessToken")  
	private String accessToken; 
		
	/** 安全码 */
	@ApiModelProperty(value="安全码")  
	private String securiyCode; 
	
	/** 版本号 */
	@ApiModelProperty(value="版本号, 默认是1.0")  
	private String verId; 
	
	/** openId */
	@ApiModelProperty(hidden = true)
	private String openId;
	
	/**设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序"**/
	@ApiModelProperty(value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序")
	private String platform;
	
	/**
	 * 检查信息是否完整
	 * @return
	 */
	public boolean validate() {
		if(userId == null || "".equals(userId) ||
			userName == null || "".equals(userName) ||
			accessToken == null || "".equals(accessToken) ||
			securiyCode == null || "".equals(securiyCode) ||
			verId == null || "".equals(verId) ||
			platform == null || "".equals(platform)){
			return false;
		}
		return true;
	}

	
	public AppTokenDto() {
		
	}
		
	public AppTokenDto(Long id,String userId, String userName, String accessToken, String verId, String openId,String platform) {
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.accessToken = accessToken;
		this.verId = verId;
		this.openId = openId;
		this.platform = platform;
	}
	
	public AppTokenDto(String userId, String userName, String accessToken, String verId,Long shopId) {
		this.userId = userId;
		this.userName = userName;
		this.accessToken = accessToken;
		this.verId = verId;
		this.shopId=shopId;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getAccessToken() {
		return accessToken;
	}


	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}


	public String getSecuriyCode() {
		return securiyCode;
	}


	public void setSecuriyCode(String securiyCode) {
		this.securiyCode = securiyCode;
	}


	public String getVerId() {
		return verId;
	}


	public void setVerId(String verId) {
		this.verId = verId;
	}
	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}


	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
} 
