package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 首页主楼层子内容
 *
 */
@Entity
@Table(name = "ls_floor_sub_item")
public class FloorSubItem implements GenericEntity<Long>{

	private static final long serialVersionUID = 2413979076525770299L;

	// 主键ID
	private Long fsiId ; 
		
	// floor item id
	private Long fiId ; 
		
	// 二级或者三级分类ID
	private Long subSortId ; 
	
	//类型名称
	private String nsortName;
	
	private Integer seq;
	
	public FloorSubItem() {
    }
		
	@Id
	@Column(name = "fsi_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "FLOOR_SUB_ITEM_SEQ")
	public Long  getFsiId(){
		return fsiId ;
	} 
		
	public void setFsiId(Long fsiId){
		this.fsiId = fsiId ;
	}
		
	@Column(name = "fi_id")
	public Long  getFiId(){
		return fiId ;
	} 
		
	public void setFiId(Long fiId){
		this.fiId = fiId ;
	}
		
		
	@Column(name = "sub_sort_id")
	public Long  getSubSortId(){
		return subSortId ;
	} 
		
	public void setSubSortId(Long subSortId){
		this.subSortId = subSortId ;
	}
		
	@Transient
  public Long getId() {
		return 	fsiId;
	}
	
	public void setId(Long id) {
		this.fsiId = id;
	}

	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

public void setSeq(Integer seq) {
	this.seq = seq;
}

@Transient
public String getNsortName() {
	return nsortName;
}

public void setNsortName(String nsortName) {
	this.nsortName = nsortName;
}

} 
