package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 网站导航
 * 
 */
@Entity
@Table(name = "ls_navi_item")
public class NavigationItem implements GenericEntity<Long> {

	private static final long serialVersionUID = 6385487397211680962L;

	// 主键
	private Long itemId;

	// 导航ID
	private Long naviId;

	// 名称
	private String name;

	// 连接
	private String link;

	// 顺序
	private Long seq;

	// 状态
	private Integer status;

	public NavigationItem() {
	}

	@Id
	@Column(name = "item_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "NAVI_ITEM_SEQ")
	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	@Column(name = "navi_id")
	public Long getNaviId() {
		return naviId;
	}

	public void setNaviId(Long naviId) {
		this.naviId = naviId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "link")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Column(name = "seq")
	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Transient
	public Long getId() {
		return itemId;
	}

	public void setId(Long id) {
		this.itemId = id;
	}

}
