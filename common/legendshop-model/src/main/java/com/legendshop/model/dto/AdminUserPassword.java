package com.legendshop.model.dto;

import java.io.Serializable;

/**
 *管理员用户修改密码Dto
 */

public class AdminUserPassword implements Serializable {

	private static final long serialVersionUID = -2314172092744817510L;

	/** 用户ID */
	private String id; 
	
	/** 旧密码 */
	private String oldPassword; 

	/** 新密码 */
	private String password; 
	
	/** 确认密码 */
	private String password2;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	} 

} 
