/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;


/**
 * 广告对象.
 */
@Entity
@Table(name = "ls_adv")
public class Advertisement implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3118175679673767409L;

    /** The id. */
    private Long id;

    /** The pic url. */
    private String picUrl;

    /** The link url. */
    private String linkUrl;

    /** The enabled. */
    private Integer enabled;

    /** The type. */
    private String type;
    
    /** The source input. */
    private String sourceInput;
    
    /** The title. */
    private String title;
    
    
    private Long floorId;
    
    private Long fiId;
    
    private String layout; //版式广告
    
    private MultipartFile file;


    /**
	 * Gets the source input.
	 * 
	 * @return the source input
	 */
    @Column(name = "source_input")
    public String getSourceInput() {
		return sourceInput;
	}

	/**
	 * Sets the source input.
	 * 
	 * @param sourceInput
	 *            the new source input
	 */
	public void setSourceInput(String sourceInput) {
		this.sourceInput = sourceInput;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Instantiates a new advertisement.
	 */
	public Advertisement() {
    }

    /**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ADV_SEQ")
    public Long getId() {
        return id;
    }

    /**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
    public void setId(Long id) {
        this.id = id;
    }

    /**
	 * Gets the pic url.
	 * 
	 * @return the pic url
	 */
    @Column(name = "pic_url")
    public String getPicUrl() {
        return picUrl;
    }

    /**
	 * Sets the pic url.
	 * 
	 * @param picUrl
	 *            the new pic url
	 */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
	 * Gets the link url.
	 * 
	 * @return the link url
	 */
    @Column(name = "link_url")
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
	 * Sets the link url.
	 * 
	 * @param linkUrl
	 *            the new link url
	 */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
	 * Gets the enabled.
	 * 
	 * @return the enabled
	 */
    @Column(name = "enabled")
    public Integer getEnabled() {
        return enabled;
    }

    /**
	 * Sets the enabled.
	 * 
	 * @param enabled
	 *            the new enabled
	 */
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    /**
	 * Gets the type.
	 * 
	 * @return the type
	 */
    @Column(name = "type")
    public String getType() {
        return type;
    }

    /**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
    public void setType(String type) {
        this.type = type;
    }

    @Transient
	public Long getFloorId() {
		return floorId;
	}

	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}

	@Transient
	public Long getFiId() {
		return fiId;
	}

	public void setFiId(Long fiId) {
		this.fiId = fiId;
	}

	
	 @Column(name = "layout")
	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	@Transient
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
    
}
