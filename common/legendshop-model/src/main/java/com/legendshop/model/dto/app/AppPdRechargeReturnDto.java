package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 预存款点充值后返回数据Dto
 */
@ApiModel(value="预存款点充值后返回数据Dto") 
public class AppPdRechargeReturnDto implements Serializable{

	/**  */
	private static final long serialVersionUID = -5888887525102207625L;

	/** 充值订单号*/
	@ApiModelProperty(value="充值订单号") 
	private String pdrSn; 
	
	/** 选择的支付方式  */
	@ApiModelProperty(value="选择的支付方式") 
	private String paymentId;
	
	@ApiModelProperty(value="订单支付类型") 
	private String subSettlementType;
	
	/** 充值金额 */
	@ApiModelProperty(value = "充值金额")
	private Double amount; 

	public AppPdRechargeReturnDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPdrSn() {
		return pdrSn;
	}

	public void setPdrSn(String pdrSn) {
		this.pdrSn = pdrSn;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	} 
}
