package com.legendshop.model.entity.weixin;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信关键字回复
 */
@Entity
@Table(name = "ls_weixin_keywordresponse")
public class WeiXinKeywordResponse implements GenericEntity<Long> {

	private static final long serialVersionUID = 1510576822372790264L;
	/**  */
	private Long id; 
		
	/** 关键字 */
	private String keyword; 
	
	private Integer pptype;
		
	/** 回复语类型 */
	private String msgtype; 
		
	/** 模版ID */
	private Long templateid; 
		
	/** 回复语消息模板 */
	private String templatename; 
		
	/** 文本内容 */
	private String content; 
		
	
	public WeiXinKeywordResponse() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_KEYWORDRESPONSE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "keyword")
	public String  getKeyword(){
		return keyword;
	} 
		
	public void setKeyword(String keyword){
			this.keyword = keyword;
		}
		
    @Column(name = "msgType")
	public String  getMsgtype(){
		return msgtype;
	} 
		
	public void setMsgtype(String msgtype){
			this.msgtype = msgtype;
		}
		
    @Column(name = "templateId")
	public Long  getTemplateid(){
		return templateid;
	} 
		
	public void setTemplateid(Long templateid){
			this.templateid = templateid;
		}
		
    @Column(name = "templateName")
	public String  getTemplatename(){
		return templatename;
	} 
		
	public void setTemplatename(String templatename){
			this.templatename = templatename;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}

	@Column(name = "pptype")
	public Integer getPptype() {
		return pptype;
	}

	public void setPptype(Integer pptype) {
		this.pptype = pptype;
	}
	
	


} 
