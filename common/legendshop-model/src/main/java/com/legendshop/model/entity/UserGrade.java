/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户等级.
 */
@Entity
@Table(name = "ls_usr_grad")
public class UserGrade implements GenericEntity<Integer> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7775238608218891693L;

	/** The grade id. */
	private Integer gradeId;

	/** The name. */
	private String name;

	/** The score. */
	private Integer score;

	/** The memo. */
	private String memo;

	private String grouthValue;

	@Transient
	public Integer getId() {
		return gradeId;
	}

	public void setId(Integer id) {
		this.gradeId = id;
	}

	/**
	 * Gets the grade id.
	 * 
	 * @return the gradeId
	 */
	@Id
	@Column(name = "grade_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USR_GRAD_SEQ")
	public Integer getGradeId() {
		return gradeId;
	}

	/**
	 * Sets the grade id.
	 * 
	 * @param gradeId
	 *            the gradeId to set
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the score to set
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * Gets the memo.
	 * 
	 * @return the memo
	 */
	@Column(name = "memo")
	public String getMemo() {
		return memo;
	}

	/**
	 * Sets the memo.
	 * 
	 * @param memo
	 *            the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Transient
	public String getGrouthValue() {
		return grouthValue;
	}

	public void setGrouthValue(String grouthValue) {
		this.grouthValue = grouthValue;
	}

}