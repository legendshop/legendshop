/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 购物车对象.
 */
@Entity
@Table(name = "ls_basket")
public class Basket implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2428049829073308967L;

	/** 购物车ID */
	private Long basketId;

	/** 店铺ID */
	private Long shopId;

	/** 商品ID */
	private Long prodId;

	/** 单品ID */
	private Long skuId;

	/** 用户ID */
	private String userId;

	/** 购买数量  */
	private Integer basketCount;

	/** 加入时间  */
	private Date basketDate;

	/** 分销的用户名称  */
	private String distUserName;
	
	/** 选中状态  */
	private int checkSts=1;
	
	/** 是否失效  */
	private boolean isFailure;
	
	/** 门店ID */
	private Long storeId;
	

	/**
	 * Gets the basket id.
	 * 
	 * @return the basket id
	 */
	@Id
	@Column(name = "basket_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "BASKET_SEQ")
	public Long getBasketId() {
		return this.basketId;
	}

	/**
	 * Sets the basket id.
	 * 
	 * @param basketId
	 *            the new basket id
	 */
	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	/**
	 * Gets the prod id.
	 * 
	 * @return the prod id
	 */
	@Column(name = "prod_id")
	public Long getProdId() {
		return this.prodId;
	}

	/**
	 * Sets the prod id.
	 * 
	 * @param prodId
	 *            the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the basket count.
	 * 
	 * @return the basket count
	 */
	@Column(name = "basket_count")
	public Integer getBasketCount() {
		return this.basketCount;
	}

	/**
	 * Sets the basket count.
	 * 
	 * @param basketCount
	 *            the new basket count
	 */
	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	/**
	 * Gets the basket date.
	 * 
	 * @return the basket date
	 */
	@Column(name = "basket_date")
	public Date getBasketDate() {
		return this.basketDate;
	}

	/**
	 * Sets the basket date.
	 * 
	 * @param basketDate
	 *            the new basket date
	 */
	public void setBasketDate(Date basketDate) {
		this.basketDate = basketDate;
	}

	@Transient
	public Long getId() {
		return basketId;
	}

	public void setId(Long id) {
		this.basketId = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "sku_id")
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	@Column(name = "dist_user_name")
	public String getDistUserName() {
		return distUserName;
	}

	public void setDistUserName(String distUserName) {
		this.distUserName = distUserName;
	}

	@Column(name = "check_sts")
	public int getCheckSts() {
		return checkSts;
	}

	public void setCheckSts(int checkSts) {
		this.checkSts = checkSts;
	}

	@Column(name = "is_failure")
	public boolean isFailure() {
		return isFailure;
	}

	public void setFailure(boolean isFailure) {
		this.isFailure = isFailure;
	}
	
	@Column(name = "store_id")
	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	
	
}