/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity.shopDecotate;

import java.io.Serializable;

/**
 * 布局参数
 */
public class ShopLayoutParam implements Serializable{

	private static final long serialVersionUID = -4116691966824406910L;

	private Long layoutId;

	private String layoutDiv;

	private Long divId;

	private String layout;

	private Long shopId;

	private Long decId;
	
	private Integer seq;
	
	private String layoutModuleType;
	
	private boolean delLayout=false;

	public Long getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}


	

	public Long getDivId() {
		return divId;
	}

	public void setDivId(Long divId) {
		this.divId = divId;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getDecId() {
		return decId;
	}

	public void setDecId(Long decId) {
		this.decId = decId;
	}

	public boolean isDelLayout() {
		return delLayout;
	}

	public void setDelLayout(boolean delLayout) {
		this.delLayout = delLayout;
	}

	public String getLayoutModuleType() {
		return layoutModuleType;
	}

	public void setLayoutModuleType(String layoutModuleType) {
		this.layoutModuleType = layoutModuleType;
	}

	public String getLayoutDiv() {
		return layoutDiv;
	}

	public void setLayoutDiv(String layoutDiv) {
		this.layoutDiv = layoutDiv;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	

	@Override
	public String toString() {
		return "ShopLayoutParam [layoutId=" + layoutId + ", layoutDiv=" + layoutDiv + ", divId=" + divId + ", layout="
				+ layout + ", shopId=" + shopId + ", decId=" + decId + ", seq=" + seq + ", layoutModuleType="
				+ layoutModuleType + ", delLayout=" + delLayout + "]";
	}
	
	
}
