package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 参数组表
 */
@Entity
@Table(name = "ls_param_group")
public class ParamGroup implements GenericEntity<Long> {

	private static final long serialVersionUID = -3690259318091360947L;

	/** 主键 */
	private Long id; 
		
	/** 名称 */
	private String name; 
		
	/** 排序 */
	private Long seq; 
		
	
	public ParamGroup() {
    }
		
	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PARAM_GROUP_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
	


} 
