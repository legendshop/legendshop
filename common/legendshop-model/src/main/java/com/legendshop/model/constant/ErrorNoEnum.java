/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 订单类型枚举
 */
public enum ErrorNoEnum implements IntegerEnum {

	ERR_SUCCESS(0),

	ERR_INVALID_REQ(10001),

	ERR_REQ_JSON(10002),

	ERR_REQ_DATA(10003),

	ERR_REGISTER_USER_EXIST(10004), //用户名已注册

	ERR_USER_NOT_EXIST(10005), //用户不存在

	ERR_PASSWORD(10006), //密码有误

	ERR_REPEATE_LOGIN(10007), //重复登录

	ERR_REPEATE_LOGOUT(10008), //重复退出

	ERR_TOKEN_EXPIRE(10009), //token过期

	ERR_AV_ROOM_NOT_EXIST(10010), //直播房间不存在

	ERR_LIVE_NO_AV_ROOM_ID(20001),  // 用户没有av房间ID

	ERR_USER_NO_LIVE(20002),  // 用户没有在直播

	ERR_SERVER(90000);  // 服务器内部错误


	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ErrorNoEnum(Integer num) {
		this.num = num;
	}
	
	public static ErrorNoEnum instance(Integer value) {
		ErrorNoEnum[] enums = values();
		for (ErrorNoEnum statusEnum : enums) {
			if (statusEnum.value().equals(value)) {
				return statusEnum;
			}
		}
		return null;
	}

}
