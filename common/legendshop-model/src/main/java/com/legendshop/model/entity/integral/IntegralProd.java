package com.legendshop.model.entity.integral;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 */
@Entity
@Table(name = "ls_integral_prod")

@ApiModel(value="IntegralProd积分商品") 
public class IntegralProd implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 唯一标识*/
	@ApiModelProperty(value="唯一标识") 
	private Long id;

	/** 积分商品名称 */
	@ApiModelProperty(value="积分商品名称") 
	private String name;

	/** 商品价格 */
	@ApiModelProperty(value="商品价格") 
	private Double price;

	/** 兑换积分 */
	@ApiModelProperty(value="兑换积分") 
	private Integer exchangeIntegral;

	/** 商品库存 */
	@ApiModelProperty(value="商品库存") 
	private Long prodStock;

	/** 商品编号 */
	@ApiModelProperty(value="商品编号") 
	private String prodNumber;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片") 
	private String prodImage;

	/** 状态 */
	@ApiModelProperty(value="状态") 
	private Integer status;

	/** 是否推荐商品 */
	@ApiModelProperty(value="是否推荐商品")  
	private Integer isCommend;

	/** 销售数量 */
	@ApiModelProperty(value="销售数量")  
	private Integer saleNum;

	/** 浏览次数 */
	@ApiModelProperty(value="浏览次数")  
	private Integer viewNum;

	/** 是否限制购买数量 */
	@ApiModelProperty(value="是否限制购买数量")  
	private Integer isLimit;

	/** 限制数量 */
	@ApiModelProperty(value="限制数量")  
	private Integer limitNum;

	/** 商品描述 */
	@ApiModelProperty(value="商品描述")  
	private String prodDesc;

	/** SEO关键字 */
	@ApiModelProperty(value="SEO关键字")  
	private String seoKeyword;

	/** SEO 描述 */
	@ApiModelProperty(value="SEO 描述")  
	private String seoDesc;

	/** 添加时间 */
	@ApiModelProperty(value="添加时间 ")  
	private Date addTime;

	/** 商城 分类 **/
	@ApiModelProperty(value="商城 一级分类id")  
	protected Long firstCid;

	/** 商城 分类 **/
	@ApiModelProperty(value="商城二级 分类id")  
	protected Long twoCid;

	/** 商城 分类 **/
	@ApiModelProperty(value="商城 三级分类id")  
	protected Long thirdCid;
	
	/** 最底层分类名称 **/
	@ApiModelProperty(value="最底层分类名称")  
	protected String prodCatName;


	/** 商城 分类 **/
	@ApiModelProperty(value="分类名称")  
	protected String categoryName;
	
	@ApiModelProperty(value="二级分类名称")  
	private String twoCategoryName;
	
	@ApiModelProperty(value="三级分类名称")  
	private String thirdCategoryName;

	@ApiModelProperty(value="图片路径")  
	private MultipartFile imageFile;

	@ApiModelProperty(value="用户名")  
	private String userName;

	public IntegralProd() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INTEGRAL_PROD_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "exchange_integral")
	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}

	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}

	@Column(name = "prod_number")
	public String getProdNumber() {
		return prodNumber;
	}

	public void setProdNumber(String prodNumber) {
		this.prodNumber = prodNumber;
	}

	@Column(name = "prod_image")
	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "is_commend")
	public Integer getIsCommend() {
		return isCommend;
	}

	public void setIsCommend(Integer isCommend) {
		this.isCommend = isCommend;
	}

	@Column(name = "sale_num")
	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	@Column(name = "view_num")
	public Integer getViewNum() {
		return viewNum;
	}

	public void setViewNum(Integer viewNum) {
		this.viewNum = viewNum;
	}

	@Column(name = "is_limit")
	public Integer getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Integer isLimit) {
		this.isLimit = isLimit;
	}

	@Column(name = "limit_num")
	public Integer getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(Integer limitNum) {
		this.limitNum = limitNum;
	}

	@Column(name = "prod_desc")
	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	@Column(name = "seo_keyword")
	public String getSeoKeyword() {
		return seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	@Column(name = "seo_desc")
	public String getSeoDesc() {
		return seoDesc;
	}

	public void setSeoDesc(String seoDesc) {
		this.seoDesc = seoDesc;
	}

	@Column(name = "add_time")
	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	@Column(name = "prod_stock")
	public Long getProdStock() {
		return prodStock;
	}

	public void setProdStock(Long prodStock) {
		this.prodStock = prodStock;
	}
	
	

	@Transient
	public MultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "category_name")
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Column(name = "first_cid")
	public Long getFirstCid() {
		return firstCid;
	}

	public void setFirstCid(Long firstCid) {
		this.firstCid = firstCid;
	}

	@Column(name = "two_cid")
	public Long getTwoCid() {
		return twoCid;
	}

	public void setTwoCid(Long twoCid) {
		this.twoCid = twoCid;
	}

	@Column(name = "third_cid")
	public Long getThirdCid() {
		return thirdCid;
	}

	public void setThirdCid(Long thirdCid) {
		this.thirdCid = thirdCid;
	}

	@Transient
	public String getTwoCategoryName() {
		return twoCategoryName;
	}

	public void setTwoCategoryName(String twoCategoryName) {
		this.twoCategoryName = twoCategoryName;
	}

	@Transient
	public String getThirdCategoryName() {
		return thirdCategoryName;
	}

	public void setThirdCategoryName(String thirdCategoryName) {
		this.thirdCategoryName = thirdCategoryName;
	}
	
	@Transient
	public String getProdCatName() {
		return prodCatName;
	}

	public void setProdCatName(String prodCatName) {
		this.prodCatName = prodCatName;
	}
	
	

}
