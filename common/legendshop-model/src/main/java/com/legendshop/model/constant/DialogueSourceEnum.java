/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 客服会话来源
 * 
 * @author Tony
 *
 */
public enum DialogueSourceEnum implements StringEnum {

	PC("PC"),

	H5("H5"),

	APP("APP");

	/** The value. */
	private final String value;

	private DialogueSourceEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
