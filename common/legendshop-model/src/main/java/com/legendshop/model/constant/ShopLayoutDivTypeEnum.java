/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商家首页装修 布局模块
 * @author tony
 *
 */
public enum ShopLayoutDivTypeEnum implements StringEnum {
	/**  
	 * 通栏布局上
	 **/
	DIV_1("div1"),
	
	/**  
	 * 通栏布局下
	 **/
	DIV_2("div2"),
	

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private ShopLayoutDivTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
