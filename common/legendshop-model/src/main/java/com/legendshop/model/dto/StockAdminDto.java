package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 库存管理
 * @author legend-15
 *
 */
public class StockAdminDto implements Serializable{

	private static final long serialVersionUID = 6488734985180285548L;

	private String prodId;
	private String skuId;
	/** 商品名称 */
	private String productName;
	/** 商品属性 */
	private String cnProperties;
	/** 商品sku图片 */
	private String skuPicture;
	/** 库存 */
	private String stocks;
	/** 实际库存 */
	private String actualStocks;
	/** 状态 */
	private String status;
	/** 商家Id */
	private String shopId;
	/** 商城名称 */
	private String siteName;
	
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSkuPicture() {
		return skuPicture;
	}
	public void setSkuPicture(String skuPicture) {
		this.skuPicture = skuPicture;
	}
	public String getCnProperties() {
		return cnProperties;
	}
	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}
	public String getStocks() {
		return stocks;
	}
	public void setStocks(String stocks) {
		this.stocks = stocks;
	}
	public String getActualStocks() {
		return actualStocks;
	}
	public void setActualStocks(String actualStocks) {
		this.actualStocks = actualStocks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	
}
