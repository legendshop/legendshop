package com.legendshop.model.dto.search;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * 属性参数
 *
 */
public class AttrParam implements Serializable {
	
	private final static Logger logger = LoggerFactory.getLogger(AttrParam.class);
	
	private static final long serialVersionUID = 1L;

	private String field;
	
	private List<String> values = new ArrayList<String>();

	public AttrParam() {
	}

	public AttrParam(String field, String value) {
		this.field = field;
		values.add(value);
	}

	public AttrParam(String field) {
		this.field = field;
	}

	public AttrParam(String field, List<String> values) {
		this.field = field;
		this.values = values;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getUrlQueryString() {
		StringBuffer buff = new StringBuffer();
		try {
			if (values.size() > 0) {
				buff.append(field).append(":");
				for (int i = 0; i < values.size(); i++) {

					buff.append(URLEncoder.encode(values.get(i), "UTF-8"));

					if (i < values.size() - 1) {
						buff.append(","); // todo 用逗号隔开，意味着支持多选的情况
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}
		return buff.toString();
	}

	public String getQueryString() {
		StringBuffer buff = new StringBuffer();
		if (values.size() > 0) {
			buff.append(field).append(":");
			for (int i = 0; i < values.size(); i++) {
				buff.append(values.get(i));
				if (i < values.size() - 1) {
					buff.append(",");
				}
			}
		}
		return buff.toString();
	}
}
