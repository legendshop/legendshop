/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 首页版式
 * @author Tony
 *
 */
public enum FloorLayoutEnum implements StringEnum {
	
	
	WIDE_ADV_RECTANGLE_FOUR("wide_adv_rectangle_four"), //(4X矩形广告)
	
	WIDE_ADV_BRAND("wide_adv_brand"), //(广告+品牌)
	
	WIDE_GOODS("wide_goods"), //(5x商品)
	
	WIDE_ADV_FIVE("wide_adv_five"), //(5x广告)
	
	WIDE_ADV_SQUARE_FOUR("wide_adv_square_four"), //(4x方形广告)
	
	WIDE_ADV_EIGHT("wide_adv_eight"), //(8x广告),
	
	WIDE_ADV_ROW("wide_adv_row"), //横排广告
	
	WIDE_BLEND("wide_blend") //(混合模式)
	

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private FloorLayoutEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	 public static FloorLayoutEnum fromString(String text) {
		    if (text != null) {
		      for (FloorLayoutEnum b : FloorLayoutEnum.values()) {
		        if (text.equalsIgnoreCase(b.value)) {
		          return b;
		        }
		      }
		    }
		    return null;
	}
}
