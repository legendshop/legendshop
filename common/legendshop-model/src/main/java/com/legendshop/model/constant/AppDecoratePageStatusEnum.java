package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * app装修页面状态
 *
 */
public enum AppDecoratePageStatusEnum implements IntegerEnum{
	
	/** 草稿状态  */
	DRAFT(-1),
	
	/** 已修改未发布  */
	MODIFIED(0),
	
	/** 已发布  */
	RELEASED(1),
	
	;
	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private AppDecoratePageStatusEnum(Integer num) {
		this.num = num;
	}
	
}
