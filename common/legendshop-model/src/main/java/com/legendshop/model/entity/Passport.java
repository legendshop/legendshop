package com.legendshop.model.entity;
import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;

import java.util.Date;

/**
 * 第三方登录通行证，记录会员与其他社区的帐户关联关系
 */
@Entity
@Table(name = "ls_passport")
public class Passport implements GenericEntity<Long> {

	private static final long serialVersionUID = -8453181666891629096L;

	/** 主键 */
	private Long passPortId; 
		
	/** 用户ID */
	private String userId; 
	
	/** 用户名称 */
	private String userName; 
	
	/** openId */
	private String openId;
	
	/** unionid */
	private String unionid;
	
	/** accessToken 暂时没用 **/
	private String accessToken;
	
	/** 昵称，QQ或微博名字 */
	private String nickName; 
	
	/** 头像 */
	private String headPortraitUrl;
	
	/** 性别 */
	private String gender;
	
	/** 国家 */
	private String country;
	
	/** 省份 */
	private String province;
	
	/** 城市 */
	private String city;
	
	/**  第三方类型 PassportTypeEnum*/
	private String type;
	
	/** 来源 PassportSourceEnum */
	private String source;
	
	/** 属性 */
	private String props; 
		
	/** 建立时间 */
	private Date createTime; 
	
	/** 最后一次跟新时间 */
	private Date updateTime;
	
	/** 最后登录时间  **/
	private Date lastLoginTime; 
	
	/** 微信的绑定的用户ID **/
	private String bindingUserId;
	
	/** 当前使用的用户Id，微信使用 **/
	private String currentUserId;
	
	public Passport() {
    }
		
	@Id
	@Column(name = "pass_port_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PASSPORT_SEQ")
	public Long  getPassPortId(){
		return passPortId;
	} 
		
	public void setPassPortId(Long passPortId){
			this.passPortId = passPortId;
		}
		
	@Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "nick_name")
	public String  getNickName(){
		return nickName;
	} 
		
 
	public void setNickName(String name){
			this.nickName = name;
		}
		
    @Column(name = "props")
	public String  getProps(){
		return props;
	} 
		
    
	public void setProps(String props){
			this.props = props;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}

	@Column(name = "update_time")
    public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Transient
	public Long getId() {
		return 	passPortId;
	}

	
	public void setId(Long id) {
		passPortId = id;
		
	}
	
	@Column(name = "open_id")
	public String getOpenId() {
		return openId;
	}


	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@Column(name = "access_token")
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name = "source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "last_login_time")
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	@Column(name = "binding_user_id")
	public String getBindingUserId() {
		return bindingUserId;
	}

	public void setBindingUserId(String bindingUserId) {
		this.bindingUserId = bindingUserId;
	}

    @Column(name = "current_user_id")
	public String getCurrentUserId() {
		return currentUserId;
	}

	public void setCurrentUserId(String currentUserId) {
		this.currentUserId = currentUserId;
	}

	@Column(name = "unionid")
	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	@Column(name = "head_portrait_url")
	public String getHeadPortraitUrl() {
		return headPortraitUrl;
	}

	public void setHeadPortraitUrl(String headPortraitUrl) {
		this.headPortraitUrl = headPortraitUrl;
	}

	@Column(name = "gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "province")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
} 
