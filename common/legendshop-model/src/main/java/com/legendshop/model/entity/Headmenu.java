package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Headmenu首页菜单
 */
@Entity
@Table(name = "ls_headmenu")
@ApiModel(value="Headmenu首页菜单")
public class Headmenu implements GenericEntity<Long> {

	private static final long serialVersionUID = -7388256626319864781L;

	/** 主键 */
	@ApiModelProperty(value="主键")
	private Long id; 
		
	/** 菜单名称 */
	@ApiModelProperty(value="菜单名称")
	private String name; 
		
	/** 菜单链接 */
	@ApiModelProperty(value="菜单链接")
	private String url; 
		
	/** 菜单图片：mobile有，pc没有 */
	@ApiModelProperty(value="菜单图片：mobile有，pc没有")
	private String pic; 
		
	/** 类型：mobile和pc */
	@ApiModelProperty(value="类型：mobile和pc")
	private String type; 
		
	/** 修改时间 */
	@ApiModelProperty(value="类型：mobile和pc")
	private Date updateTime; 
	
	/** 安卓appid */
	@ApiModelProperty(value="安卓appid")
	private String androidId;
	
	/** 苹果appid */
	@ApiModelProperty(value="苹果appid")
	private String iOSId;
		
	/**顺序  */
	@ApiModelProperty(value="顺序")
	private Long seq; 
	
	@ApiModelProperty(value="图片文件")
	private MultipartFile imageFile;
	
	/**上下线状态：1为上线，0为下线  */
	@ApiModelProperty(value="苹果appid")
	private Integer status;
	
	public Headmenu() {
	}
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "HEADMENU_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "type")
	public String  getType(){
		return type;
	} 
		
	public void setType(String type){
			this.type = type;
		}
		
    @Column(name = "update_time")
	public Date  getUpdateTime(){
		return updateTime;
	} 
		
	public void setUpdateTime(Date updateTime){
			this.updateTime = updateTime;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
	@Transient
	public MultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	@Column(name = "android_id")
	public String getAndroidId() {
		return androidId;
	}

	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}

	@Column(name = "ios_id")
	public String getiOSId() {
		return iOSId;
	}

	public void setiOSId(String iOSId) {
		this.iOSId = iOSId;
	}
	
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
} 
