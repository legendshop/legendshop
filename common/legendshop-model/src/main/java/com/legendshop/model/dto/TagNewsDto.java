package com.legendshop.model.dto;



public class TagNewsDto {
	
	private Long itemId;
	
	private String itemName;
	
	private Long newsId;
	
	private String newsTitle;
	

	public Long getItemId() {
		return itemId;
	}



	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public Long getNewsId() {
		return newsId;
	}



	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}



	public String getNewsTitle() {
		return newsTitle;
	}



	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

}
