package com.legendshop.model.dto;

/**
 * 店铺编辑 修改bannerPic 发送事件实体类
 * @author Tony
 *
 */
public class ShopDecorateCacheUpdate {
	
	private Long shopId;
	
	private String bannerPic;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getBannerPic() {
		return bannerPic;
	}

	public void setBannerPic(String bannerPic) {
		this.bannerPic = bannerPic;
	}

	public ShopDecorateCacheUpdate(Long shopId, String bannerPic) {
		super();
		this.shopId = shopId;
		this.bannerPic = bannerPic;
	}

}
