/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 退款及退货的类型
 */
public enum RefundReturnTypeEnum implements IntegerEnum {
	
/** ------ 操作类型 ------ */
	/** 订单退款 1*/
	OP_ORDER(1),
	/** 订单项退款或退货 2*/
	OP_ORDER_ITEM(2),

	/** 仅退款 1*/
	REFUND(1), 

	/** 退款及退货 2*/
	REFUND_RETURN(2),
	
	/** 不用退货 1*/
	NO_NEED_GOODS(1),
	
	/** 需要退货 2*/
	NEED_GOODS(2),
	;

	private Integer type;

	RefundReturnTypeEnum(Integer type) {
		this.type = type;
	}

	@Override
	public Integer value() {
		return type;
	}

}
