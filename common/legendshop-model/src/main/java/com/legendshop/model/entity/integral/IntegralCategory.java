package com.legendshop.model.entity.integral;

import java.io.Serializable;
import java.util.List;


/**
 * 积分分类
 *
 */
public class IntegralCategory implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** The self id. */
	private long categoryId;
	
	/** The parent id. */
	private long parentId; 
	
	private String parentName;
	
	/** The node name. */
	private String nodeName;
	
	private List<IntegralCategory> childList;

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public List<IntegralCategory> getChildList() {
		return childList;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public void setChildList(List<IntegralCategory> childList) {
		this.childList = childList;
	}
	
	
}
