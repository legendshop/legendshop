/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
/**
 * 用户权限
 */
@Entity
@Table(name = "ls_func")
public class Function  implements GenericEntity<String> {

	 /** The Constant serialVersionUID. */
 	private static final long serialVersionUID = -3353517712968544688L;   
     
     /** The id. */
     private String id;
     
     /** 权限名称. */
     private String name;
     
     /** url地址. */
     private String url;
     
     /** 父权限名称，备用. */
     private String parentName;
     
     /** 受保护的权限. */
     private String protectFunction;
     
     /** 类型 **/
     private Integer category;
     
     /** 菜单ID **/
     private Long menuId;
     
     /** 菜单名字 **/
     private String menuName;
     
     /** 备注. */
     private String note;
     
     /** 应用编号. */
     private String appNo;
     
     /** The roles. */
     private List<Role> roles = new ArrayList<Role>();
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
     @Id
 	@Column(name = "id")
 	@GeneratedValue(strategy = GenerationType.UUID)
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	@Column(name = "url")
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * Gets the protect function.
	 * 
	 * @return the protect function
	 */
	@Column(name = "protect_function")
	public String getProtectFunction() {
		return protectFunction;
	}
	
	/**
	 * Sets the protect function.
	 * 
	 * @param protectFunction
	 *            the new protect function
	 */
	public void setProtectFunction(String protectFunction) {
		this.protectFunction = protectFunction;
	}
	
	/**
	 * Gets the note.
	 * 
	 * @return the note
	 */
	@Column(name = "note")
	public String getNote() {
		return note;
	}
	
	/**
	 * Sets the note.
	 * 
	 * @param note
	 *            the new note
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 * Gets the roles.
	 * 
	 * @return the roles
	 */
	@Transient
	public List<Role> getRoles() {
		return roles;
	}
	
	/**
	 * Sets the roles.
	 * 
	 * @param roles
	 *            the new roles
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	/**
	 * Gets the parent name.
	 * 
	 * @return the parent name
	 */
	@Column(name = "parent_name")
	public String getParentName() {
		return parentName;
	}
	
	/**
	 * Sets the parent name.
	 * 
	 * @param parentName
	 *            the new parent name
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	@Column(name = "category")
	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	@Column(name = "menu_id")
	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	@Transient
	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Column(name = "app_no")
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

}