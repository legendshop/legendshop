package com.legendshop.model.report;

import java.io.Serializable;
import java.util.Date;

public class SalerankDto implements Serializable{

	private static final long serialVersionUID = -8288216801107215579L;
	
	private Long shopId;
	
	private String shopName;
	
	//销售订单
	private long salesOrder;
	
	//销售金额
	private Double  salesAmount;
	
	private Date startDate;

	private Date endDate;

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public long getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(long salesOrder) {
		this.salesOrder = salesOrder;
	}

	public Double getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	
}
