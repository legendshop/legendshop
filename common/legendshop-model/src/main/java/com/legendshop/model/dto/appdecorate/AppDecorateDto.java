package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.Headmenu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 手机端首页
 * @author Tony
 *
 */
@ApiModel(value="手机端首页Dto") 
public class AppDecorateDto implements Serializable {
	 
	private static final long serialVersionUID = 2796650001116793686L;

	@ApiModelProperty(value="图片路径")  
	private String photopath;
	
	/**
	 * 手机端域名
	 */
	@ApiModelProperty(value="手机端域名")  
	private String mobileDomain;
	
	/**
	 * 轮播
	 */
	@ApiModelProperty(value="轮播列表")  
	private List<SlidersDto> sliders;
	
	/**
	 * 广告位
	 */
	@ApiModelProperty(value="广告位列表")  
	private List<AdvertsDto> adverts;
	
	/**
	 * 楼层
	 */
	@ApiModelProperty(value="楼层列表")  
	private List<FloorsDto> floors;
	
	/**
	 * 头部菜单列表
	 */
	@ApiModelProperty(value="头部菜单列表")  
	private List<Headmenu> headMenuList;
	
	/**
	 * 菜单
	 */
	@ApiModelProperty(value="菜单")  
	private String menus;
	
	

	public void setPhotopath(String photopath) {
		this.photopath = photopath;
	}

	public String getPhotopath() {
		return photopath;
	}

	public void setSliders(List<SlidersDto> sliders) {
		this.sliders = sliders;
	}

	public List<SlidersDto> getSliders() {
		return sliders;
	}

	public void setAdverts(List<AdvertsDto> adverts) {
		this.adverts = adverts;
	}

	public List<AdvertsDto> getAdverts() {
		return adverts;
	}

	public void setFloors(List<FloorsDto> floors) {
		this.floors = floors;
	}

	public List<FloorsDto> getFloors() {
		return floors;
	}

	public void setMenus(String menus) {
		this.menus = menus;
	}

	public String getMenus() {
		return menus;
	}

	public String getMobileDomain() {
		return mobileDomain;
	}

	public void setMobileDomain(String mobileDomain) {
		this.mobileDomain = mobileDomain;
	}

	public List<Headmenu> getHeadMenuList() {
		return headMenuList;
	}

	public void setHeadMenuList(List<Headmenu> headMenuList) {
		this.headMenuList = headMenuList;
	}
	
}