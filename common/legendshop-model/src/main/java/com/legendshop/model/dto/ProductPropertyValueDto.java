package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="商品属性值Dto")
public class ProductPropertyValueDto implements Serializable{

	private static final long serialVersionUID = 6598280148952180408L;

		/**
		 * 属性值ID
		 */
		@ApiModelProperty(value="属性值ID")
		private Long valueId;

		/**
		 * 属性ID
		 */
		@ApiModelProperty(value="属性ID")
		private Long propId;

		/**
		 * 属性值名称
		 */
		@ApiModelProperty(value="属性值名称")
		private String name;
		
		/**
		 * 属性值名称别名
		 */
		@ApiModelProperty(value="属性值名称别名")
		private String alias;

		/**
		 * 图片路径
		 */
		@ApiModelProperty(value="图片路径")
		private String pic;

		/**
		 * 是否被sku选中
		 */
		@ApiModelProperty(value="是否被sku选中")
		private boolean isSelected = false;

		public Long getValueId() {
			return valueId;
		}

		public void setValueId(Long valueId) {
			this.valueId = valueId;
		}

		public Long getPropId() {
			return propId;
		}

		public void setPropId(Long propId) {
			this.propId = propId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPic() {
			return pic;
		}

		public void setPic(String pic) {
			this.pic = pic;
		}

		public String getAlias() {
			return alias;
		}

		public void setAlias(String alias) {
			this.alias = alias;
		}

		public boolean getIsSelected() {
			return isSelected;
		}

		public void setIsSelected(boolean isSelected) {
			this.isSelected = isSelected;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((valueId == null) ? 0 : valueId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ProductPropertyValueDto other = (ProductPropertyValueDto) obj;
			if (valueId == null) {
				if (other.valueId != null)
					return false;
			} else if (!valueId.equals(other.valueId))
				return false;
			return true;
		}

		
		
}
