/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * LegendShop 版权所有,并保留所有权利。
 * 首页楼层的种类
 */
public enum FloorTemplateEnum implements StringEnum {
	/**  
	 * 商品分类 
	 **/
	SORT("ST"),
	/**
	 * 左边的广告位
	 */
	LEFT_ADV("LA"),
	
	/**
	 * 右边的广告位
	 */
	RIGHT_ADV("RA"),
	
	/**
	 * 底部广告位
	 */
	BOTTOM_ADV("BA"),
	
	/**默认
	 * 
	 */
	DEFAULT_ADV("RA"),
	
	/**
	 * 商品
	 */
	PRODUCT("PT"),
	
	/**
	 * 品牌
	 */
	BRAND("BD"),
	
	/**
	 * 文章
	 */
	NEWS("NS"),
	
	/**
	 * 团购商品
	 */
	GROUP_PRODUCT("GP"),

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private FloorTemplateEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
