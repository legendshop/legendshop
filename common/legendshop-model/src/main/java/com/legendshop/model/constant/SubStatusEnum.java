/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 订单操作状态Enum
 */
public enum SubStatusEnum implements StringEnum {

	/** 下订单. */
	ORDER_CAPTURE("CA"), 

	/** 删除订单. */
	ORDER_DEL("DE"), 

	/** 改价格. */
	PRICE_CHANGE("PC"), 

	/** 增加积分. */
	CREDIT_SCORE("CS"), 

	/** 使用积分. */
	DEBIT_SCORE("DS"), 

	/** 超时. */
	ORDER_OVER_TIME("OT"), 

	/** 订单状态变化. */
	CHANGE_STATUS("ST"); 

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new sub status enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SubStatusEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		SubStatusEnum[] licenseEnums = values();
		for (SubStatusEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		SubStatusEnum[] licenseEnums = values();
		for (SubStatusEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
