/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.form;

import java.util.Date;

import com.legendshop.model.constant.PaymentProcessorEnum;

/**
 * 支付参数Form
 */
public class SysPaymentForm {

	/** 发起支付的用户ID */
	private String userId;

	/** openID */
	private String openId;

	/** 订单单据号 */
	private String[] subNumbers;

	/** 支付方式ID */
	private String payTypeId;
	
	/** 支付方式名称 */
	private String payTypeName;

	/** 单据类型 */
	private String typeEnum;

	/** 结算交易号 */
	private String subSettlementSn;

	/** 支付总金额 */
	private Double totalAmount;

	/** 帐号金额 */
	private Double accountAmount;

	/**
	 * 支付逻辑处理者
	 */
	private PaymentProcessorEnum processorEnum;

	/** 是否全额支付 */
	private boolean fullPay;

	/** 预售支付:尾款/订金 */
	private Integer payPctType;

	/** 是否预售 */
	private boolean isPresell = false;

	/** 余额支付方式(1:预付款 2：金币) */
	private Integer prePayTypeVal;

	/** 商品名称 */
	private String subject;

	/** 用户IP地址 */
	private String ip;

	/** 下单时间  */
	private Date orderDatetime;

	/** PC商城域名 */
	private String domainName;
	
	/**
	 * 用于支付宝支付, 收银台界面中商品的链接地址，不允许加?id=123这类自定义参数
	 */
	private String showUrl;
	
	/**
	 * 仅用于微信H5支付, 
	 * 正常流程用户支付完成后会返回至发起支付的页面，如需返回至指定页面，则可以在MWEB_URL后拼接上redirect_url参数，来指定回调页面。
	 */
	private String redirectUrl;

	/**
	 * 仅用于微信H5支付 场景信息里的WAP网站URL地址
	 */
	private String wapDomain;
	
	/**
	 * 用于支付宝手机网站支付, 用户在收银台取消支付时跳转的url
	 */
	private String quitUrl;

	/** 支付来源【PaySourceEnum】 */
	private String source;
	
	/**海关类别（通联分配的海关类别代码HG001）  -- 用于海关支付**/
	private String ustomsType; 
	
	/** 电商企业代码（10位海关代码） */
	private String eshopEntCode;
	
	/**  电商企业名称（企业备案的企业全称） */
	private String eshopEntName;
	
	/** 付款人证件号码 */
	private String payerIDCard;
	
	/** 付款人姓名 */
	private String payerName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getAccountAmount() {
		return accountAmount;
	}

	public void setAccountAmount(Double accountAmount) {
		this.accountAmount = accountAmount;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getOrderDatetime() {
		return orderDatetime;
	}

	public void setOrderDatetime(Date orderDatetime) {
		this.orderDatetime = orderDatetime;
	}

	public String getShowUrl() {
		return showUrl;
	}

	public void setShowUrl(String showUrl) {
		this.showUrl = showUrl;
	}

	public String getQuitUrl() {
		return quitUrl;
	}

	public void setQuitUrl(String quitUrl) {
		this.quitUrl = quitUrl;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String[] getSubNumbers() {
		return subNumbers;
	}

	public void setSubNumbers(String[] subNumbers) {
		this.subNumbers = subNumbers;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getTypeEnum() {
		return typeEnum;
	}

	public void setTypeEnum(String typeEnum) {
		this.typeEnum = typeEnum;
	}

	public PaymentProcessorEnum getProcessorEnum() {
		return processorEnum;
	}

	public void setProcessorEnum(PaymentProcessorEnum processorEnum) {
		this.processorEnum = processorEnum;
	}

	public boolean isFullPay() {
		return fullPay;
	}

	public void setFullPay(boolean fullPay) {
		this.fullPay = fullPay;
	}

	public Integer getPrePayTypeVal() {
		return prePayTypeVal;
	}

	public void setPrePayTypeVal(Integer prePayTypeVal) {
		this.prePayTypeVal = prePayTypeVal;
	}

	public boolean isPresell() {
		return isPresell;
	}

	public void setPresell(boolean isPresell) {
		this.isPresell = isPresell;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public String getWapDomain() {
		return wapDomain;
	}

	public void setWapDomain(String wapDomain) {
		this.wapDomain = wapDomain;
	}
	
	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getUstomsType() {
		return ustomsType;
	}

	public void setUstomsType(String ustomsType) {
		this.ustomsType = ustomsType;
	}

	public String getEshopEntCode() {
		return eshopEntCode;
	}

	public void setEshopEntCode(String eshopEntCode) {
		this.eshopEntCode = eshopEntCode;
	}

	public String getEshopEntName() {
		return eshopEntName;
	}

	public void setEshopEntName(String eshopEntName) {
		this.eshopEntName = eshopEntName;
	}

	public String getPayerIDCard() {
		return payerIDCard;
	}

	public void setPayerIDCard(String payerIDCard) {
		this.payerIDCard = payerIDCard;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	
	
}
