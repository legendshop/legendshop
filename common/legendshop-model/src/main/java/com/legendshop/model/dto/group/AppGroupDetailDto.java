/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * App团购活动详情.
 *
 */
@ApiModel(value="团购活动详情") 
public class AppGroupDetailDto implements Serializable {

	private static final long serialVersionUID = -5189934416226534007L;

	/** id */
	@ApiModelProperty(value="主键ID") 
	private Long id;

	/** 团购名称. */
	@ApiModelProperty(value="团购名称") 
	private String groupName;

	/** 开始时间. */
	@ApiModelProperty(value="开始时间") 
	private Date startTime;

	/** 结束时间. */
	@ApiModelProperty(value="结束时间") 
	private Date endTime;

	/** 商品ID. */
	@ApiModelProperty(value="商品ID") 
	private Long productId;

	/** 店铺ID. */
	@ApiModelProperty(value="店铺ID") 
	private Long shopId;

	/** 店铺名称. */
	@ApiModelProperty(value="店铺名称") 
	private String shopName;

	/** 团购价格. */
	@ApiModelProperty(value="团购价格") 
	private java.math.BigDecimal groupPrice;
	
	/** 每人限购数量, 为0 则不限购 */
	@ApiModelProperty(value="每人限购数量, 为0 则不限购") 
	private Integer buyQuantity;
	
	/** 已购买人数. */
	@ApiModelProperty(value="已购买人数") 
	private Long buyerCount;

	/** 成团人数 */
	@ApiModelProperty(value="成团人数") 
	private Integer peopleCount;

	/** 活动的状态有：未提审、已提审、上线（审核通过）、审核不通过。. */
	@ApiModelProperty(value="活动的状态有：未提审、已提审、上线（审核通过）、审核不通过") 
	private Integer status;

	/** 团购图片. */
	@ApiModelProperty(value="团购图片") 
	private String groupImage;

	/** 团购详情. */
	@ApiModelProperty(value="团购详情") 
	private String groupDesc;

	/** 团购商品详情*. */
	@ApiModelProperty(value="团购商品详情") 
	private AppGroupProductDto productDto;

	/**
	 * Gets the id.
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the group name.
	 *
	 * @return the group name
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the group name.
	 *
	 * @param groupName
	 *            the group name
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime
	 *            the start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime
	 *            the end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId
	 *            the product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId
	 *            the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName
	 *            the shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the group price.
	 *
	 * @return the group price
	 */
	public java.math.BigDecimal getGroupPrice() {
		return groupPrice;
	}

	/**
	 * Sets the group price.
	 *
	 * @param groupPrice
	 *            the group price
	 */
	public void setGroupPrice(java.math.BigDecimal groupPrice) {
		this.groupPrice = groupPrice;
	}

	/**
	 * Gets the buyer count.
	 *
	 * @return the buyer count
	 */
	public Long getBuyerCount() {
		return buyerCount;
	}

	/**
	 * Sets the buyer count.
	 *
	 * @param buyerCount
	 *            the buyer count
	 */
	public void setBuyerCount(Long buyerCount) {
		this.buyerCount = buyerCount;
	}

	/**
	 * Gets the buy quantity.
	 *
	 * @return the buy quantity
	 */
	public Integer getBuyQuantity() {
		return buyQuantity;
	}

	/**
	 * Sets the buy quantity.
	 *
	 * @param buyQuantity
	 *            the buy quantity
	 */
	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}
	
	public Integer getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Integer peopleCount) {
		this.peopleCount = peopleCount;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the group image.
	 *
	 * @return the group image
	 */
	public String getGroupImage() {
		return groupImage;
	}

	/**
	 * Sets the group image.
	 *
	 * @param groupImage
	 *            the group image
	 */
	public void setGroupImage(String groupImage) {
		this.groupImage = groupImage;
	}

	/**
	 * Gets the group desc.
	 *
	 * @return the group desc
	 */
	public String getGroupDesc() {
		return groupDesc;
	}

	/**
	 * Sets the group desc.
	 *
	 * @param groupDesc
	 *            the group desc
	 */
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	/**
	 * Gets the product dto.
	 *
	 * @return the product dto
	 */
	public AppGroupProductDto getProductDto() {
		return productDto;
	}

	/**
	 * Sets the product dto.
	 *
	 * @param productDto
	 *            the product dto
	 */
	public void setProductDto(AppGroupProductDto productDto) {
		this.productDto = productDto;
	}

}
