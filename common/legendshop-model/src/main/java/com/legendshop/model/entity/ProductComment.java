/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.ModelUtil;
import com.legendshop.util.AppUtils;

/**
 * 产品评论表.
 */
@Entity
@Table(name = "ls_prod_comm")
public class ProductComment implements GenericEntity<Long> {

	private static final long serialVersionUID = 3118765177322131488L;
	//主键
	private Long id;
	//商品id
	private Long prodId;
	
	private Long basketId;
	
	private String ownerName;
	//用户id
	private String userId;
	//用户名
	private String userName;
	
	private String content;
	
	/** 评论图片 多个图片以逗号隔开*/
	private String photos;
	
	private Date addtime;
	
	private String postip;
	//宝贝评分
	private Integer score;
	//服务评分
	private Integer shopScore;
	//物流评分
	private Integer logisticsScore;
	//商品名字
	private String prodName;

	private String pic;
	
	private Date buyTime;
	
	private String gradeName;
	
	private Integer usefulCounts;
	
	private Integer replayCounts;
	
	private String subNumber;
	
	/**订单项id*/
	private Long subItemId;
	
	/**是否匿名(1:是   0：否)**/
	private Integer isAnonymous;
	
	/** 是否已追加评论 */
	private Boolean isAddComm;
	
	/** 评论状态**/
	private Integer status;
	
	/** 商家是否已回复*/
	private Boolean isReply;
	
	/** 商家回复内容 */
	private String shopReplyContent;
	
	/** 商家回复时间 */
	private Date shopReplyTime;
	
	/** 购买属性 */
	private String attribute;
	
	/** 店铺名 */
	private String siteName;
	
	/** 店铺id */
	private String shopId;
	
	public ProductComment(){
		
	}

	/**
	 * Instantiates a new product comment.
	 *
	 * @param id the id
	 * @param prodId the prod id
	 * @param ownerName the owner name
	 * @param userName the user name
	 * @param content the content
	 * @param addtime the addtime
	 * @param postip the postip
	 * @param score the score
	 * @param replyContent the reply content
	 * @param replyName the reply name
	 * @param replyTime the reply time
	 * @param prodName the prod name
	 */
	public ProductComment(Long id, Long prodId,String ownerName, String userName, String content,
			Date addtime, String postip, Integer score, String replyContent,
			String replyName, Date replyTime, String prodName) {
		super();
		this.id = id;
		this.prodId = prodId;
		this.ownerName = ownerName;
		this.userName = userName;
		this.content = content;
		this.addtime = addtime;
		this.postip = postip;
		this.score = score;
		this.prodName = prodName;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_COMM_SEQ")
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the prod id.
	 * 
	 * @return the prod id
	 */
	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 * 
	 * @param prodId
	 *            the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	
	
	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name="user_name")
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	@Column(name="content")
	public String getContent() {
		return this.content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	@Column(name="photos")
	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	/**
	 * Gets the recDate.
	 * 
	 * @return the recDate
	 */
	@Column(name="addtime")
	public Date getAddtime() {
		return this.addtime;
	}

	/**
	 * Sets the recDate.
	 * @param addtime the new addtime
	 */
	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	/**
	 * Gets the postip.
	 * 
	 * @return the postip
	 */
	@Column(name="postip")
	public String getPostip() {
		return this.postip;
	}

	/**
	 * Sets the postip.
	 * 
	 * @param postip
	 *            the new postip
	 */
	public void setPostip(String postip) {
		this.postip = postip;
	}

	@Column(name="score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name="shop_score")
	public Integer getShopScore() {
		return shopScore;
	}

	public void setShopScore(Integer shopScore) {
		this.shopScore = shopScore;
	}

	@Column(name="logistics_score")
	public Integer getLogisticsScore() {
		return logisticsScore;
	}

	public void setLogisticsScore(Integer logisticsScore) {
		this.logisticsScore = logisticsScore;
	}

	/**
	 * Gets the prod name.
	 * 
	 * @return the prod name
	 */
	@Transient
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 * 
	 * @param prodName
	 *            the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the owner name.
	 * 
	 * @return the owner name
	 */
	@Column(name = "owner_name")
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the owner name.
	 * 
	 * @param ownerName
	 *            the new owner name
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	/**
	 * 验证对象是否满足需求.
	 */
	public void validate(){
		ModelUtil util = new ModelUtil();
		util.range(this.getContent(), 5, 100, "Content");
		util.isNotNull(this.getProdId(), "ProdId");
		util.isNotNull(this.getUserName(), "UserName");
		
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	@Transient
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the buy time.
	 *
	 * @return the buy time
	 */
	@Transient
	public Date getBuyTime() {
		return buyTime;
	}

	/**
	 * Sets the buy time.
	 *
	 * @param buyTime the new buy time
	 */
	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	/**
	 * Gets the grade name.
	 *
	 * @return the grade name
	 */
	@Transient
	public String getGradeName() {
		return gradeName;
	}

	/**
	 * Sets the grade name.
	 *
	 * @param gradeName the new grade name
	 */
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	/**
	 * Gets the useful counts.
	 *
	 * @return the useful counts
	 */
	@Column(name="useful_counts")
	public Integer getUsefulCounts() {
		return usefulCounts;
	}

	/**
	 * Sets the useful counts.
	 *
	 * @param usefulCounts the new useful counts
	 */
	public void setUsefulCounts(Integer usefulCounts) {
		this.usefulCounts = usefulCounts;
	}

	/**
	 * Gets the replay counts.
	 *
	 * @return the replay counts
	 */
	@Column(name="replay_counts")
	public Integer getReplayCounts() {
		return replayCounts;
	}

	/**
	 * Sets the replay counts.
	 *
	 * @param replayCounts the new replay counts
	 */
	public void setReplayCounts(Integer replayCounts) {
		this.replayCounts = replayCounts;
	}

	@Column(name="basket_id")
	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	@Column(name="sub_item_id")
	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}
	
	@Column(name="is_anonymous")
	public Integer getIsAnonymous() {
		return isAnonymous;
	}

	public void setIsAnonymous(Integer isAnonymous) {
		this.isAnonymous = isAnonymous;
	}
	
	@Column(name="is_add_comm")
	public Boolean getIsAddComm() {
		return isAddComm;
	}

	public void setIsAddComm(Boolean isAddComm) {
		this.isAddComm = isAddComm;
	}

	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name="is_reply")
	public Boolean getIsReply() {
		return isReply;
	}

	public void setIsReply(Boolean isReply) {
		this.isReply = isReply;
	}

	@Column(name="shop_reply_content")
	public String getShopReplyContent() {
		return shopReplyContent;
	}

	public void setShopReplyContent(String shopReplyContent) {
		this.shopReplyContent = shopReplyContent;
	}

	@Column(name="shop_reply_time")
	public Date getShopReplyTime() {
		return shopReplyTime;
	}

	public void setShopReplyTime(Date shopReplyTime) {
		this.shopReplyTime = shopReplyTime;
	}

	@Transient
	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	@Transient
	public String[] getPhotoPaths(){
		if(AppUtils.isNotBlank(this.photos)){
			String[] photoPaths = this.photos.split(",");
			return photoPaths;
		}
		
		return null;
	}

	@Transient
	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	@Transient
	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	
	

}