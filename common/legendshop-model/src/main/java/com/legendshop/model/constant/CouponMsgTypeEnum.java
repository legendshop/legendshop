package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 礼券通知方式
 *
 */
public enum CouponMsgTypeEnum implements StringEnum {

	/** 产品引用类型 *. */
	INTERIOR("站内信"),

	/** 用户引用  */
	MAIL("邮件"),
	
	/** 用户引用  */
	SMS("短信");

	
	
	private String value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	@Override
	public String value() {
		return value;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param value
	 *            the value
	 */
	CouponMsgTypeEnum(String value) {
		this.value = value;
	}
}
