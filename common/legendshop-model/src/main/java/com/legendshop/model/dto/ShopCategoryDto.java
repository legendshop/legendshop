package com.legendshop.model.dto;

public class ShopCategoryDto {

	/** 第一级 分类 ID */
	private Long shopCatId; 
	
	/** 下一级 分类id **/
	private Long nextCatId;
	
	/** 最后一级分类id **/
	private Long subCatId;
	
	/** 类目名称 **/
	private String name;

	public Long getShopCatId() {
		return shopCatId;
	}

	public void setShopCatId(Long shopCatId) {
		this.shopCatId = shopCatId;
	}

	public Long getNextCatId() {
		return nextCatId;
	}

	public void setNextCatId(Long nextCatId) {
		this.nextCatId = nextCatId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
