package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

public enum UserStatusEnum implements IntegerEnum{
	/** 启用 */
	ENABLED(1),
	
	/** 禁用 */
	DISABLED(0);
	
	private UserStatusEnum(Integer status) {
		this.status = status;
	}
	private Integer status;
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return status;
	}

}
