/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.io.Serializable;

/**
 * 商家布局Div.
 */
public class ShopLayoutDivDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1921844495454499033L;

	/** Id. */
	private Long layoutDivId;

	/** 布局Id. */
	private Long layoutId;

	/** 商家Id. */
	private Long shopId;

	/** 商家装修Id. */
	private Long shopDecotateId;

	/**  Div1:左边布局;div2:右边布局[layout3:1:2布局、layout4:2:1布局]. */
	private String layoutDiv;

	/** 布局类型. */
	private String layoutType;
	
	/** 布局模块类型. */
	private String layoutModuleType;

	/** 顺序. */
	private Long seq;
	
	/** 布局内容. */
	private String layoutContent;

	/**
	 * Gets the layout div id.
	 *
	 * @return the layout div id
	 */
	public Long getLayoutDivId() {
		return layoutDivId;
	}

	/**
	 * Sets the layout div id.
	 *
	 * @param layoutDivId the new layout div id
	 */
	public void setLayoutDivId(Long layoutDivId) {
		this.layoutDivId = layoutDivId;
	}

	/**
	 * Gets the layout id.
	 *
	 * @return the layout id
	 */
	public Long getLayoutId() {
		return layoutId;
	}

	/**
	 * Sets the layout id.
	 *
	 * @param layoutId the new layout id
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop decotate id.
	 *
	 * @return the shop decotate id
	 */
	public Long getShopDecotateId() {
		return shopDecotateId;
	}

	/**
	 * Sets the shop decotate id.
	 *
	 * @param shopDecotateId the new shop decotate id
	 */
	public void setShopDecotateId(Long shopDecotateId) {
		this.shopDecotateId = shopDecotateId;
	}

	/**
	 * Gets the layout div.
	 *
	 * @return the layout div
	 */
	public String getLayoutDiv() {
		return layoutDiv;
	}

	/**
	 * Sets the layout div.
	 *
	 * @param layoutDiv the new layout div
	 */
	public void setLayoutDiv(String layoutDiv) {
		this.layoutDiv = layoutDiv;
	}

	/**
	 * Gets the layout type.
	 *
	 * @return the layout type
	 */
	public String getLayoutType() {
		return layoutType;
	}

	/**
	 * Sets the layout type.
	 *
	 * @param layoutType the new layout type
	 */
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Long getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Long seq) {
		this.seq = seq;
	}

	/**
	 * Gets the layout module type.
	 *
	 * @return the layout module type
	 */
	public String getLayoutModuleType() {
		return layoutModuleType;
	}

	/**
	 * Sets the layout module type.
	 *
	 * @param layoutModuleType the new layout module type
	 */
	public void setLayoutModuleType(String layoutModuleType) {
		this.layoutModuleType = layoutModuleType;
	}

	/**
	 * Gets the layout content.
	 *
	 * @return the layout content
	 */
	public String getLayoutContent() {
		return layoutContent;
	}

	/**
	 * Sets the layout content.
	 *
	 * @param layoutContent the new layout content
	 */
	public void setLayoutContent(String layoutContent) {
		this.layoutContent = layoutContent;
	}
	
	
	

}
