/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.entity;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 首页子楼层
 */
@Entity
@Table(name = "ls_sub_floor")
public class SubFloor implements GenericEntity<Long>, Comparable<SubFloor>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7974165069566578170L;

	// ID
	/** The sf id. */
	private Long sfId;

	// 名称
	/** The name. */
	private String name;

	// 记录时间
	/** The rec date. */
	private Date recDate;

	// 状态
	/** The status. */
	private Integer status;

	// 父节点
	/** The floor id. */
	private Long floorId;

	// 顺序
	/** The seq. */
	private Long seq;

	/** The sub floor items. */
	Set<SubFloorItem> subFloorItems = new TreeSet<SubFloorItem>(new SubFloorItemComparator());

	/**
	 * The Constructor.
	 */
	public SubFloor() {
	}

	/**
	 * Gets the sf id.
	 *
	 * @return the sf id
	 */
	@Id
	@Column(name = "sf_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_FLOOR_SEQ")
	public Long getSfId() {
		return sfId;
	}

	/**
	 * Sets the sf id.
	 *
	 * @param sfId the sf id
	 */
	public void setSfId(Long sfId) {
		this.sfId = sfId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 *
	 * @param recDate the rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the floor id.
	 *
	 * @return the floor id
	 */
	@Column(name = "floor_id")
	public Long getFloorId() {
		return floorId;
	}

	/**
	 * Sets the floor id.
	 *
	 * @param floorId the floor id
	 */
	public void setFloorId(Long floorId) {
		this.floorId = floorId;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	@Column(name = "seq")
	public Long getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the seq
	 */
	public void setSeq(Long seq) {
		this.seq = seq;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public Long getId() {
		return sfId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		this.sfId = id;
	}

	/**
	 * Gets the sub floor items.
	 *
	 * @return the sub floor items
	 */
	@Transient
	public Set<SubFloorItem> getSubFloorItems() {
		return subFloorItems;
	}

	/**
	 * Sets the sub floor items.
	 *
	 * @param subFloorItems the sub floor items
	 */
	public void setSubFloorItems(Set<SubFloorItem> subFloorItems) {
		this.subFloorItems = subFloorItems;
	}

	/**
	 * 增加子item.
	 *
	 * @param subFloorItem the sub floor item
	 */
	public void addSubFloorItem(SubFloorItem subFloorItem) {
		if (this.getSfId().equals(subFloorItem.getSfId())) {
			subFloorItems.add(subFloorItem);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SubFloor o) {
		if (o == null || o.getSeq() == null) {
			return -1;
		}else {
			return o.getSeq().compareTo(this.seq);
		}
	}

}
