package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家装修轮播图模块
 */
@Entity
@Table(name = "ls_shop_layout_banner")
public class ShopLayoutBanner implements GenericEntity<Long>,Cloneable {

	private static final long serialVersionUID = 233005224834363535L;

	/** ID */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商家装修ID */
	private Long shopDecotateId; 
		
	/** 布局id */
	private Long layoutId; 
		
	/** 背景图片ID */
	private String imageFile; 
		
	/** url */
	private String url; 
		
	/** 顺序 */
	private Integer seq; 
		
	
	public ShopLayoutBanner() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_LAYOUT_BANNER_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_decotate_id")
	public Long  getShopDecotateId(){
		return shopDecotateId;
	} 
		
	public void setShopDecotateId(Long shopDecotateId){
			this.shopDecotateId = shopDecotateId;
		}
		
    @Column(name = "layout_id")
	public Long  getLayoutId(){
		return layoutId;
	} 
		
	public void setLayoutId(Long layoutId){
			this.layoutId = layoutId;
		}
		
    @Column(name = "image_file")
	public String  getImageFile(){
		return imageFile;
	} 
		
	public void setImageFile(String imageFile){
			this.imageFile = imageFile;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	


} 
