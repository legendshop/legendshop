package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 包邮活动关联商品
 */
@Entity
@Table(name = "ls_shipping_prod")
public class ShippingProd implements GenericEntity<Long> {

	private static final long serialVersionUID = -7057551327353415392L;

	/** 包邮活动关联商品表Id */
	private Long id; 
		
	/** 包邮活动Id */
	private Long shippingId; 
		
	/** 商品Id */
	private Long prodId; 
		
	/** 商家Id */
	private Long shopId; 
		
	
	public ShippingProd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHIPPING_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shipping_id")
	public Long  getShippingId(){
		return shippingId;
	} 
		
	public void setShippingId(Long shippingId){
			this.shippingId = shippingId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
	


} 
