package com.legendshop.model.dto;

import java.util.List;

/**
 * 商品关联sku集合
 */

public class SkuSearchMapDto {
    private List<SkuSearchDto> skuSearchDto;

    public List<SkuSearchDto> getSkuSearchDto() {
        return skuSearchDto;
    }

    public void setSkuSearchDto(List<SkuSearchDto> skuSearchDto) {
        this.skuSearchDto = skuSearchDto;
    }
}
