/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 商品 参数属性dto
 *
 */
@ApiModel("商品参数属性dto")
public class ProdParamDto implements Serializable  {

	private static final long serialVersionUID = 9127844181627450794L;

	/** 属性id */
	@ApiModelProperty(value = "属性id")
	private Long propId;
	
	/** 参数id */
	@ApiModelProperty(value = "参数id")
	private Long paramId;
	
	/** 参数名 */
	@ApiModelProperty(value = "参数名")
	private String paramName;
	
	/** 参数值Id */
	@ApiModelProperty(value = "参数值Id")
	private Long paramValueId; 
	
	/** 参数值 */
	@ApiModelProperty(value = "参数值")
	private String paramValueName;
	
	/** 分组名称 */
	@ApiModelProperty(value = "分组名称")
	private String groupName;

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public Long getParamId() {
		return paramId;
	}

	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValueName() {
		return paramValueName;
	}

	public void setParamValueName(String paramValueName) {
		this.paramValueName = paramValueName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getParamValueId() {
		return paramValueId;
	}

	public void setParamValueId(Long paramValueId) {
		this.paramValueId = paramValueId;
	}
	
	
}
