/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;

/**
 * 本地缓存Entity
 */
public class CacheEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 关键字 */
	private String key;

	/** 缓存 */
	private Object cacheContext;

	/** 过期时间戳 */
	private long timeoutStamp;// 过期时间戳

	/**
	 * The Constructor.
	 *
	 * @param cacheKey
	 *            the cache key
	 * @param details
	 *            the details
	 */
	public CacheEntity(String cacheKey, Object details) {
		this.key = cacheKey;
		this.cacheContext = details;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key
	 *            the key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the timeout stamp.
	 *
	 * @return the timeout stamp
	 */
	public long getTimeoutStamp() {
		return timeoutStamp;
	}

	/**
	 * Sets the timeout stamp.
	 *
	 * @param timeoutStamp
	 *            the timeout stamp
	 */
	public void setTimeoutStamp(long timeoutStamp) {
		this.timeoutStamp = timeoutStamp;
	}

	/**
	 * Gets the cache context.
	 *
	 * @return the cache context
	 */
	public Object getCacheContext() {
		return cacheContext;
	}

	/**
	 * Sets the cache context.
	 *
	 * @param cacheContext
	 *            the cache context
	 */
	public void setCacheContext(Object cacheContext) {
		this.cacheContext = cacheContext;
	}

}
