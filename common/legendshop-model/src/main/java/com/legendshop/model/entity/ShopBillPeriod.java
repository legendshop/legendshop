package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 结算档期
 */
@Entity
@Table(name = "ls_shop_bill_period")
public class ShopBillPeriod implements GenericEntity<Long> {

	private static final long serialVersionUID = 8929138489483314546L;

	/** 主键 */
	private Long id;

	/** 档期标示，例如 201512-ID */
	private String flag;

	/** 应结金额 */
	private volatile Double resultTotalAmount = 0d;

	/** 这期订单的实际金额 */
	private volatile Double orderAmount = 0d;

	/** 平台佣金金额 */
	private volatile Double commisTotals = 0d;

	/** 分销佣金金额 */
	private volatile Double distCommisTotals = 0d;

	/** 退单金额 */
	private volatile Double orderReturnTotals = 0d;

	/** 红包总额 */
	private volatile Double redpackTotals = 0d;

	/** 建立时间 */
	private Date recDate;

	public ShopBillPeriod() {
	}

	/**
	 * 是否应该做计算,只要有值存在就应该结算
	 * 
	 * @return
	 */
	public boolean shouldSettle() {
		if (resultTotalAmount != 0 || orderAmount != 0 || commisTotals != 0 || distCommisTotals != 0 || orderReturnTotals != 0 || redpackTotals != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_BILL_PERIOD_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "flag")
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Column(name = "result_total_amount")
	public Double getResultTotalAmount() {
		return resultTotalAmount;
	}

	public void setResultTotalAmount(Double resultTotalAmount) {
		this.resultTotalAmount = resultTotalAmount;
	}

	@Column(name = "order_amount")
	public Double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	@Column(name = "commis_totals")
	public Double getCommisTotals() {
		return commisTotals;
	}

	public void setCommisTotals(Double commisTotals) {
		this.commisTotals = commisTotals;
	}

	@Column(name = "order_return_totals")
	public Double getOrderReturnTotals() {
		return orderReturnTotals;
	}

	public void setOrderReturnTotals(Double orderReturnTotals) {
		this.orderReturnTotals = orderReturnTotals;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Column(name = "dist_commis_totals")
	public Double getDistCommisTotals() {
		return distCommisTotals;
	}

	public void setDistCommisTotals(Double distCommisTotals) {
		this.distCommisTotals = distCommisTotals;
	}

	@Column(name = "redpack_totals")
	public Double getRedpackTotals() {
		return redpackTotals;
	}

	public void setRedpackTotals(Double redpackTotals) {
		this.redpackTotals = redpackTotals;
	}

	@Override
	public String toString() {
		return "ShopBillPeriod [id=" + id + ", flag=" + flag + ", resultTotalAmount=" + resultTotalAmount + ", orderAmount=" + orderAmount + ", commisTotals="
				+ commisTotals + ", distCommisTotals=" + distCommisTotals + ", orderReturnTotals=" + orderReturnTotals + ", redpackTotals=" + redpackTotals
				+ ", recDate=" + recDate + "]";
	}

}
