/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 预支付的类型
 * 
 * 余额支付类型(1:预付款  2:金币)
 */
public enum PrePayTypeEnum implements IntegerEnum {

	/** 预存款 **/
	PREDEPOSIT(1),

	/** 金币 **/
	COIN(2),

	;

	/** The value. */
	private final Integer value;

	/**
	 * Instantiates a new role enum.
	 * 
	 * @param value
	 *            the value
	 */
	private PrePayTypeEnum(Integer value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public Integer value() {
		return this.value;
	}

	
}
