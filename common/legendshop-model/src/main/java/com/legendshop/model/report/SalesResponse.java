/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.report;

import java.io.Serializable;
import java.util.List;

/**
 * 销售Dto.
 */
public class SalesResponse implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6191034843490482515L;

	/** The year total orders. */
	private List<List<Object>> yearTotalOrders;
	
	/** The year total amount. */
	private List<List<Object>> yearTotalAmount;
	
	
	/** The month total orders. */
	private List<List<Object>> monthTotalOrders;
	
	/** The month total amount. */
	private List<List<Object>> monthTotalAmount;
	
	private String userName;

	private String selectedYear;
	
	private String selectedMonth;
	/**
	 * Gets the year total orders.
	 *
	 * @return the year total orders
	 */
	public List<List<Object>> getYearTotalOrders() {
		return yearTotalOrders;
	}

	/**
	 * Sets the year total orders.
	 *
	 * @param yearTotalOrders the new year total orders
	 */
	public void setYearTotalOrders(List<List<Object>> yearTotalOrders) {
		this.yearTotalOrders = yearTotalOrders;
	}

	/**
	 * Gets the year total amount.
	 *
	 * @return the year total amount
	 */
	public List<List<Object>> getYearTotalAmount() {
		return yearTotalAmount;
	}

	/**
	 * Sets the year total amount.
	 *
	 * @param yearTotalAmount the new year total amount
	 */
	public void setYearTotalAmount(List<List<Object>> yearTotalAmount) {
		this.yearTotalAmount = yearTotalAmount;
	}

	/**
	 * Gets the month total orders.
	 *
	 * @return the month total orders
	 */
	public List<List<Object>> getMonthTotalOrders() {
		return monthTotalOrders;
	}

	/**
	 * Sets the month total orders.
	 *
	 * @param monthTotalOrders the new month total orders
	 */
	public void setMonthTotalOrders(List<List<Object>> monthTotalOrders) {
		this.monthTotalOrders = monthTotalOrders;
	}

	/**
	 * Gets the month total amount.
	 *
	 * @return the month total amount
	 */
	public List<List<Object>> getMonthTotalAmount() {
		return monthTotalAmount;
	}

	/**
	 * Sets the month total amount.
	 *
	 * @param monthTotalAmount the new month total amount
	 */
	public void setMonthTotalAmount(List<List<Object>> monthTotalAmount) {
		this.monthTotalAmount = monthTotalAmount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(String selectedYear) {
		this.selectedYear = selectedYear;
	}

	public String getSelectedMonth() {
		return selectedMonth;
	}

	public void setSelectedMonth(String selectedMonth) {
		this.selectedMonth = selectedMonth;
	}

	
	
}
