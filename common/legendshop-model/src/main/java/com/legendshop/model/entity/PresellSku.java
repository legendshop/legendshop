package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *预售活动sku
 */
@Entity
@Table(name = "ls_presell_sku")
public class PresellSku implements GenericEntity<Long> {

	/** 编号*/
	private Long id; 
		
	/** 预售活动id */
	private Long pId; 
		
	/** 预售活动商品id */
	private Long prodId; 
		
	/** 预售活动skuid */
	private Long skuId; 
		
	/** 预售商品的价格 */
	private java.math.BigDecimal prsellPrice; 
		
	
	public PresellSku() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PRESELL_SKU_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "p_id")
	public Long  getPId(){
		return pId;
	} 
		
	public void setPId(Long pId){
			this.pId = pId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "prsell_price")
	public java.math.BigDecimal  getPrsellPrice(){
		return prsellPrice;
	} 
		
	public void setPrsellPrice(java.math.BigDecimal prsellPrice){
			this.prsellPrice = prsellPrice;
		}
	


} 
