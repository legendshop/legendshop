/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 秒杀活动Dto.
 */
public class SeckillActivityDto implements Serializable {

	private static final long serialVersionUID = -5322615901224128343L;

	/** 商品id */
	private Long prodId;

	/** skuId. */
	private Long skuId;

	/** sku价格. */
	private java.math.BigDecimal price;

	/** 秒杀库存. */
	private Long stock;

	/** sku库存. */
	private Long skuStock;

	/** 秒杀总库存. */
	private Long activityStock;

	/** 属性. */
	private String prop;

	/** 名字. */
	private String prodName;

	/** 商品图片*/
	private String pic;

	/** sku价格. */
	private Double skuPrice;

	/** 商品属性*/
	private String cnProperties;


	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId
	 *            the prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the sku id.
	 *
	 * @return the sku id
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * Sets the sku id.
	 *
	 * @param skuId
	 *            the sku id
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public java.math.BigDecimal getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price
	 *            the price
	 */
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}

	/**
	 * Gets the stock.
	 *
	 * @return the stock
	 */
	public Long getStock() {
		return stock;
	}

	/**
	 * Sets the stock.
	 *
	 * @param stock
	 *            the stock
	 */
	public void setStock(Long stock) {
		this.stock = stock;
	}

	/**
	 * Gets the prop.
	 *
	 * @return the prop
	 */
	public String getProp() {
		return prop;
	}

	/**
	 * Sets the prop.
	 *
	 * @param prop
	 *            the prop
	 */
	public void setProp(String prop) {
		this.prop = prop;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName
	 *            the prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Double getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	public Long getSkuStock() {
		return skuStock;
	}

	public void setSkuStock(Long skuStock) {
		this.skuStock = skuStock;
	}

	public Long getActivityStock() {
		return activityStock;
	}

	public void setActivityStock(Long activityStock) {
		this.activityStock = activityStock;
	}
}
