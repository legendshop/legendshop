/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.form;

import java.io.Serializable;

/**
 * 产品展示Form.
 */
public class ProductForm implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4381624464831157862L;

	/** The prod name. */
	private String prodName;
	
	/** The prod id. */
	private Long prodId;
	
	/** The pic. */
	private String pic;
	
	
	/** The cash. */
	private Double cash;

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the new cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}
	
}
