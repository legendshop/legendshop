/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 加入订单的Dto
 */
@ApiModel("加入订单的Dto")
public class OrderAddParamDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 购物车字符串. */
	@ApiModelProperty(value = "basket str")
	private String basketStr;

	/** 订单备注. shopId:备注 */
	@ApiModelProperty(value = "订单备注. shopId:备注")
	private String remarkText;
	
	/** 配送方式. */
	@ApiModelProperty(value = "配送方式")
	private String delivery; 

	/** token， 防止重复提交. */
	@ApiModelProperty(value = "token， 防止重复提交",required=true)
	private String token;

	/** 发票Id. */
	@ApiModelProperty(value = "发票Id")
	private Long invoiceId;
	
	/** 支付方式 */
	@ApiModelProperty(value = "支付方式(1:货到付款,2:在线支付)")
	private Integer payManner;
	
	/** 支付类型Id. */
	@ApiModelProperty(value = "支付类型Id")
	private String payTypeId;
	
	/** 优惠券字符串. */
	@ApiModelProperty(value = "优惠券字符串")
	private String couponStr;
	
	/** 订单类型. */
	@ApiModelProperty(value = "订单类型",required=true)
	private String type;
	
	/** 活动id */
	@ApiModelProperty(value ="活动id")
	private Long activeId;
	
	/** 当前商品所属商家是否开启发票 1开启 0关闭*/
	@ApiModelProperty(value ="当前商品所属商家是否开启发票 1：开启  0：关闭")
	private Integer switchInvoice;
	
	/**
	 * Gets the basket str.
	 *
	 * @return the basket str
	 */
	public String getBasketStr() {
		return basketStr;
	}

	/**
	 * Sets the basket str.
	 *
	 * @param basketStr the new basket str
	 */
	public void setBasketStr(String basketStr) {
		this.basketStr = basketStr;
	}

	/**
	 * Gets the remark text.
	 *
	 * @return the remark text
	 */
	public String getRemarkText() {
		return remarkText;
	}

	/**
	 * Sets the remark text.
	 *
	 * @param remarkText the new remark text
	 */
	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token the new token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Gets the invoice id.
	 *
	 * @return the invoice id
	 */
	public Long getInvoiceId() {
		return invoiceId;
	}

	/**
	 * Sets the invoice id.
	 *
	 * @param invoiceId the new invoice id
	 */
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	/**
	 * Gets the pay manner.
	 *
	 * @return the pay manner
	 */
	public Integer getPayManner() {
		return payManner;
	}

	/**
	 * Sets the pay manner.
	 *
	 * @param payManner the new pay manner
	 */
	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	/**
	 * Gets the delivery.
	 *
	 * @return the delivery
	 */
	public String getDelivery() {
		return delivery;
	}

	/**
	 * Sets the delivery.
	 *
	 * @param delivery the new delivery
	 */
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	/**
	 * Gets the pay type id.
	 *
	 * @return the pay type id
	 */
	public String getPayTypeId() {
		return payTypeId;
	}

	/**
	 * Sets the pay type id.
	 *
	 * @param payTypeId the new pay type id
	 */
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	/**
	 * Gets the coupon str.
	 *
	 * @return the coupon str
	 */
	public String getCouponStr() {
		return couponStr;
	}

	/**
	 * Sets the coupon str.
	 *
	 * @param couponStr the new coupon str
	 */
	public void setCouponStr(String couponStr) {
		this.couponStr = couponStr;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}
	
}
