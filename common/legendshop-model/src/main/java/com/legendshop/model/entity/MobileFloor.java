package com.legendshop.model.entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *手机端html5 首页楼层表
 */
@Entity
@Table(name = "ls_m_floor")
@ApiModel(value="MobileFloor首页楼层表")
public class MobileFloor implements GenericEntity<Long> {

	private static final long serialVersionUID = -5402043496364326239L;

	/** id */
	@ApiModelProperty(value="id")
	private Long id; 
		
	/** 楼层名称 */
	@ApiModelProperty(value="楼层名称")
	private String name; 
		
	/** 状态；0：下线；1：上线； */
	@ApiModelProperty(value="状态；0：下线；1：上线；")
	private Integer status; 
		
	/** 顺序 */
	@ApiModelProperty(value=" 顺序")
	private Long seq; 
		
	/** 创建时间 */
	@ApiModelProperty(value="创建时间")
	private Date recDate; 
		
	/** 所属店铺ID */
	@ApiModelProperty(value="所属店铺ID")
	private Long shopId; 
		
	/** 链接 */
	@ApiModelProperty(value="链接")
	private String url; 
		
	/** 开关；0：楼层左边大；1楼层左边小 */
	@ApiModelProperty(value="开关；0：楼层左边大；1楼层左边小")
	private Long style; 
	
	
	List<MobileFloorItem> floorItemList = new ArrayList<MobileFloorItem>();
		
	
	public MobileFloor() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "M_FLOOR_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}

	@Column(name = "style")
	public Long getStyle() {
		return style;
	}

	public void setStyle(Long style) {
		this.style = style;
	}

	@Transient
	public List<MobileFloorItem> getFloorItemList() {
		return floorItemList;
	}

	public void setFloorItemList(List<MobileFloorItem> floorItemList) {
		this.floorItemList = floorItemList;
	}

	public void addFloorItemList(MobileFloorItem floorItem){
		if(AppUtils.isNotBlank(floorItem) && floorItem.getParentId().equals(this.id)){
			this.floorItemList.add(floorItem);
		}
	}
	
} 
