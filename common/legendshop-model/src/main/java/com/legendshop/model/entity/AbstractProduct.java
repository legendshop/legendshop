/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *商品实体父类.
 */
public abstract class AbstractProduct extends UploadFile implements GenericEntity<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 639772483681855956L;

	/** 商品Id */
	protected Long prodId;
	
	/** 版本号 */
	protected Integer version;
	
	/** 商品条形码. */
	protected String modelId;
	
	/** 商家编码. */
	protected String partyCode;
	
	/** 商品名称. */
	protected String name;
	
	/** 产品原价，市场价. */
	protected Double price;
	
	/** 产品现价，客户最终价格. */
	protected Double cash;
	
	/** 代理价. */
	protected Double proxyPrice;
	
	/** 是否支持分销 **/
	protected int supportDist = 0;
	
	/** 分销佣金比例 **/
	protected Double distCommisRate;
	
	/** 运费. */
	protected Double carriage;
	
	/** 简称. */
	protected String brief;
	
	/** PC端详细描述 */
	protected String content;
	
	/** 手机端详细描述 */
	protected String contentM;
	
	/** 用户查看数. */
	protected long views;
	
	/** 用户购买数量. */
	protected long buys;
	
	/** 评论数. */
	protected long comments;
	
	/** 评论总得分 **/
	protected int reviewScores = 0;
	
	/** 评论平均得分 **/
	protected int avgScores;
	
	/** The rec date. */
	protected Date recDate;
	
	/** 商品的小图片. */
	protected String smallPic;
	
	/** 是否使用小图片. */
	protected String useSmallPic;
	
	/** 商品图片. */
	protected String pic;
	
	/** 商品状态. {@link com.legendshop.model.constant.ProductStatusEnum}*/
	protected Integer status;
	
	/** 商品原状态. */
	protected Integer preStatus;
	
	/** 修改时间. */
	protected Date modifyDate;
	
	/** 用户Id. */
	protected String userId;
	
	/** 用户名称. */
	protected String userName;
	
	/** 开始时间，在商品为有时间段有效期. */
	protected Date startDate;
	
	/** 结束时间，在商品为有时间段有效期. */
	protected Date endDate;
	
	/**  库存. */
	protected Integer stocks;
	
	/** 商品预警库存. */
	protected Integer stocksArm;
	
	/** 商品类型，见ProductTypeEnum. */
	protected String prodType;
	
	/** 活动Id [秒杀活动ID]. */
	protected Integer acitveId;
	
	/** 关键字. */
	protected String keyWord;
	
	/**
	 * 参数属性列表
	 *  property id: property value id
	 */
	protected String parameter;
	
	/**
	 * 用户自定义的参数属性列表, key:value 格式
	 */
	protected String userParameter;
	
	/** 品牌Id. */
	protected Long brandId;
	
	/** 实际库存. */
	protected Integer actualStocks;
	
	/** 产品缩略图. */
	protected MultipartFile smallPicFile;
	
	/** 省份Id. */
	protected Integer provinceid;

	/** 城市Id. */
	protected Integer cityid;

	/** 地区Id. */
	protected Integer areaid;
	
	/** 评论数*/
	protected Integer commentNum;
	
	/** 物流体积(立方米) **/
	protected Double volume=0d;

	/** 物流重量(千克) **/
	protected Double weight=0d;
	
	/** 有没发票 **/
	protected int hasInvoice;
	
	/** 是否保修 **/
	protected int hasGuarantee;
	
	/** 售后服务ID **/
	protected Long afterSaleId;
	
	/** 库存计数方式，0：拍下减库存，1：付款减库存 **/
	protected int stockCounting;
	
	/**  退换货承诺 **/
	protected Integer rejectPromise;
	
	/** 服务保障 **/
	protected Integer serviceGuarantee;
	
	/** 商家ID **/
	protected  Long shopId;
	
	/** 商城 分类 **/
	protected Long categoryId;
	
	/** (商家小分类) 一级分类  **/
	protected Long shopFirstCatId;
	
	/** (商家小分类) 二级分类  **/
	protected Long shopSecondCatId;
	
	/** (商家小分类) 三级分类  **/
	protected Long shopThirdCatId;
	
	/** 货到付款; 0:普通商品 , 1:货到付款商品  **/
	protected int isSupportCod;
	
	/** seo的标题  **/
	private String metaTitle;
	
	/** seo的描述  **/
	private String metaDesc;
	
	/**审核意见**/
	private String auditOpinion;
	
	/**是否参加团购**/
	private int isGroup;	
	
	/** 运费模板ID **/
	protected Long transportId;

	/** 是否免运费 **/
	private Integer supportTransportFree;
	
	/** 是否免运费 ，参见TransportTypeEnum **/
	private Integer transportType;
	
	/** EMS运费  **/
	private Double emsTransFee;
	
	/** 快递运费  **/
	private Double expressTransFee;
	
	/** 邮件运费  **/
	private Double mailTransFee;
	
	/** 会员直接上级分佣比例  **/
	protected Double firstLevelRate;
	
	/** 会员上二级分佣比例  **/
	protected Double secondLevelRate;
	
	/** 会员上三级分佣比例  **/
	protected Double thirdLevelRate;
	
	/** 微信小程序码 */
	protected String wxAcode;

	/** 商品视频地址  */
    protected String proVideoUrl;

    /** 商品分组ID */
    private Long groupId;

	/**商家端微信小程序码*/
	protected String shopWxCode;

	/**商品搜索权重  商品搜索权重,默认为1.00,最少为0，数字越大则查询约靠前，商品下架不可以编辑搜索权重 **/
    private Double	searchWeight = 1.0d;

	/**
	 * Gets the prod id.
	 * 
	 * @return the prod id
	 */
	@Id
	@Column(name="prod_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_SEQ")
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 * 
	 * @param prodId
	 *            the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the model id.
	 * 
	 * @return the model id
	 */
	@Column(name="model_id")
	public String getModelId() {
		return modelId;
	}

	/**
	 * Sets the model id.
	 * 
	 * @param modelId
	 *            the new model id
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name="name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the price.
	 * 
	 * @return the price
	 */
	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 * 
	 * @param price
	 *            the new price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * Gets the cash.
	 * 
	 * @return the cash
	 */
	@Column(name = "cash")
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 * 
	 * @param cash
	 *            the new cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * Gets the proxy price.
	 * 
	 * @return the proxy price
	 */
	@Column(name="proxy_price")
	public Double getProxyPrice() {
		return proxyPrice;
	}

	/**
	 * Sets the proxy price.
	 * 
	 * @param proxyPrice
	 *            the new proxy price
	 */
	public void setProxyPrice(Double proxyPrice) {
		this.proxyPrice = proxyPrice;
	}

	/**
	 * Gets the carriage.
	 * 
	 * @return the carriage
	 */
	@Column(name = "carriage")
	public Double getCarriage() {
		return carriage;
	}

	/**
	 * Sets the carriage.
	 * 
	 * @param carriage
	 *            the new carriage
	 */
	public void setCarriage(Double carriage) {
		this.carriage = carriage;
	}

	/**
	 * Gets the brief.
	 * 
	 * @return the brief
	 */
	@Column(name = "brief")
	public String getBrief() {
		return brief;
	}

	/**
	 * Sets the brief.
	 * 
	 * @param brief
	 *            the new brief
	 */
	public void setBrief(String brief) {
		this.brief = brief;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	@Column(name = "content")
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Gets the views.
	 * 
	 * @return the views
	 */
	@Column(name="views")
	public long getViews() {
		return views;
	}

	/**
	 * Sets the views.
	 * 
	 * @param views
	 *            the new views
	 */
	public void setViews(long views) {
		this.views = views;
	}

	/**
	 * Gets the buys.
	 * 
	 * @return the buys
	 */
	@Column(name = "buys")
	public long getBuys() {
		return buys;
	}

	/**
	 * Sets the buys.
	 * 
	 * @param buys
	 *            the new buys
	 */
	public void setBuys(long buys) {
		this.buys = buys;
	}

	/**
	 * Gets the rec date.
	 * 
	 * @return the rec date
	 */
	@Column(name="rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 * 
	 * @param recDate
	 *            the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the pic.
	 * 
	 * @return the pic
	 */
	@Column(name="pic")
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 * 
	 * @param pic
	 *            the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}


	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the modify date.
	 * 
	 * @return the modify date
	 */
	@Column(name="modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * Sets the modify date.
	 * 
	 * @param modifyDate
	 *            the new modify date
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name="user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the start date.
	 * 
	 * @return the start date
	 */
	@Column(name="start_date")
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 * 
	 * @param startDate
	 *            the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 * 
	 * @return the end date
	 */
	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 * 
	 * @param endDate
	 *            the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 得到虚拟库存.
	 * 
	 * @return the stocks
	 */
	@Column(name="stocks")
	public Integer getStocks() {
		return stocks == null ? 0: stocks;
	}

	/**
	 * Sets the stocks.
	 * 
	 * @param stocks
	 *            the new stocks
	 */
	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	/**
	 * Gets the prod type.
	 * 
	 * @return the prod type
	 */
	@Column(name="prod_type")
	public String getProdType() {
		return prodType;
	}

	/**
	 * Sets the prod type.
	 * 
	 * @param prodType
	 *            the new prod type
	 */
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	/**
	 * Gets the key word.
	 * 
	 * @return the key word
	 */
	@Column(name="key_word")
	public String getKeyWord() {
		return keyWord;
	}

	/**
	 * Sets the key word.
	 * 
	 * @param keyWord
	 *            the new key word
	 */
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	/**
	 * Gets the parameter.
	 * 
	 * @return the parameter
	 */
	@Column(name="parameter")
	public String getParameter() {
		return parameter;
	}

	/**
	 * Sets the parameter.
	 * 
	 * @param parameter
	 *            the new parameter
	 */
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	@Column(name="user_parameter")
	public String getUserParameter() {
		return userParameter;
	}

	public void setUserParameter(String userParameter) {
		this.userParameter = userParameter;
	}


	/**
	 * Gets the brand id.
	 * 
	 * @return the brand id
	 */
	@Column(name = "brand_id")
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 * 
	 * @param brandId
	 *            the new brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * 得到实际库存，不能为空
	 * 
	 * @return the actual stocks
	 */
	@Column(name = "actual_stocks")
	public Integer getActualStocks() {
		return actualStocks == null ? 0 : actualStocks;
	}

	/**
	 * Sets the actual stocks.
	 * 
	 * @param actualStocks
	 *            the new actual stocks
	 */
	public void setActualStocks(Integer actualStocks) {
		this.actualStocks = actualStocks;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return prodId;
	}
	
	public void setId(Long id) {
		this.prodId = id;
	}

	/**
	 * Gets the small pic.
	 *
	 * @return the small pic
	 */
	@Column(name="small_pic")
	public String getSmallPic() {
		return smallPic;
	}

	/**
	 * Sets the small pic.
	 *
	 * @param smallPic the new small pic
	 */
	public void setSmallPic(String smallPic) {
		this.smallPic = smallPic;
	}



	/**
	 * Gets the small pic file.
	 *
	 * @return the small pic file
	 */
	@Transient
	public MultipartFile getSmallPicFile() {
		return smallPicFile;
	}

	/**
	 * Sets the small pic file.
	 *
	 * @param smallPicFile the new small pic file
	 */
	public void setSmallPicFile(MultipartFile smallPicFile) {
		this.smallPicFile = smallPicFile;
	}

	/**
	 * Gets the use small pic.
	 *
	 * @return the use small pic
	 */
	@Column(name="use_small_pic")
	public String getUseSmallPic() {
		return useSmallPic;
	}

	/**
	 * Sets the use small pic.
	 *
	 * @param useSmallPic the new use small pic
	 */
	public void setUseSmallPic(String useSmallPic) {
		this.useSmallPic = useSmallPic;
	}

	
	/**
	 * Gets the provinceid.
	 * 
	 * @return the provinceid
	 */
	@Transient
	public Integer getProvinceid() {
		return provinceid;
	}

	/**
	 * Sets the provinceid.
	 * 
	 * @param provinceid
	 *            the new provinceid
	 */
	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	/**
	 * Gets the cityid.
	 * 
	 * @return the cityid
	 */
	@Transient
	public Integer getCityid() {
		return cityid;
	}

	/**
	 * Sets the cityid.
	 * 
	 * @param cityid
	 *            the new cityid
	 */
	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	/**
	 * Gets the areaid.
	 * 
	 * @return the areaid
	 */
	@Transient
	public Integer getAreaid() {
		return areaid;
	}

	/**
	 * Sets the areaid.
	 * 
	 * @param areaid
	 *            the new areaid
	 */
	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

	/**
	 * Gets the comment num.
	 *
	 * @return the comment num
	 */
	@Transient
	public Integer getCommentNum() {
		return commentNum;
	}

	/**
	 * Sets the comment num.
	 *
	 * @param commentNum the new comment num
	 */
	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	/**
	 * 评论数
	 *
	 * @return the comments
	 */
	@Column(name = "comments")
	public long getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(long comments) {
		this.comments = comments;
	}
	
	@Column(name = "reviewScores")
	public int getReviewScores() {
		return reviewScores;
	}

	public void setReviewScores(int reviewScores) {
		this.reviewScores = reviewScores;
	}

	@Transient
	public int getAvgScores() {
		return avgScores;
	}

	public void setAvgScores(int avgScores) {
		this.avgScores = avgScores;
	}

	@Column(name = "party_code")
	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	@Column(name = "volume")
	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	@Column(name = "weight")
	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	@Column(name = "transport_id")
	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportd) {
		this.transportId = transportd;
	}

	@Column(name = "has_invoice")
	public int getHasInvoice() {
		return hasInvoice;
	}

	public void setHasInvoice(int hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	@Column(name = "has_guarantee")
	public int getHasGuarantee() {
		return hasGuarantee;
	}

	public void setHasGuarantee(int hasGuarantee) {
		this.hasGuarantee = hasGuarantee;
	}

	@Column(name = "after_sale_id")
	public Long getAfterSaleId() {
		return afterSaleId;
	}

	public void setAfterSaleId(Long afterSaleId) {
		this.afterSaleId = afterSaleId;
	}

	@Column(name = "stock_counting")
	public int getStockCounting() {
		return stockCounting;
	}

	public void setStockCounting(int stockCounting) {
		this.stockCounting = stockCounting;
	}

	@Column(name = "reject_promise")
	public Integer getRejectPromise() {
		return rejectPromise;
	}

	public void setRejectPromise(Integer rejectPromise) {
		this.rejectPromise = rejectPromise;
	}

	@Column(name = "service_guarantee")
	public Integer getServiceGuarantee() {
		return serviceGuarantee;
	}

	public void setServiceGuarantee(Integer serviceGuarantee) {
		this.serviceGuarantee = serviceGuarantee;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "shop_first_cat_id")
	public Long getShopFirstCatId() {
		return shopFirstCatId;
	}

	public void setShopFirstCatId(Long shopFirstCatId) {
		this.shopFirstCatId = shopFirstCatId;
	}

	@Column(name = "shop_second_cat_id")
	public Long getShopSecondCatId() {
		return shopSecondCatId;
	}

	public void setShopSecondCatId(Long shopSecondCatId) {
		this.shopSecondCatId = shopSecondCatId;
	}

	@Column(name = "shop_third_cat_id")
	public Long getShopThirdCatId() {
		return shopThirdCatId;
	}

	public void setShopThirdCatId(Long shopThirdCatId) {
		this.shopThirdCatId = shopThirdCatId;
	}

	@Column(name = "category_id")
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "is_support_cod")
	public int getIsSupportCod() {
		return isSupportCod;
	}

	public void setIsSupportCod(int isSupportCod) {
		this.isSupportCod = isSupportCod;
	}

	@Column(name = "meta_title")
	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	@Column(name = "meta_desc")
	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	@Column(name = "support_transport_free")
	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	@Column(name = "transport_type")
	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	@Column(name = "ems_trans_fee")
	public Double getEmsTransFee() {
		return emsTransFee;
	}

	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	@Column(name = "express_trans_fee")
	public Double getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	@Column(name = "mail_trans_fee")
	public Double getMailTransFee() {
		return mailTransFee;
	}

	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	@Column(name = "audit_opinion")
	public String getAuditOpinion() {
		return auditOpinion;
	}

	
	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}
	
	
	@Column(name = "is_group")
	public int getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(int isGroup) {
		this.isGroup = isGroup;
	}

	@Column(name = "pre_status")
	public Integer getPreStatus() {
		return preStatus;
	}

	public void setPreStatus(Integer preStatus) {
		this.preStatus = preStatus;
	}

	@Column(name = "version")
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "support_dist")
	public int getSupportDist() {
		return supportDist;
	}

	public void setSupportDist(int supportDist) {
		this.supportDist = supportDist;
	}

	@Column(name = "dist_commis_rate")
	public Double getDistCommisRate() {
		return distCommisRate;
	}

	public void setDistCommisRate(Double distCommisRate) {
		this.distCommisRate = distCommisRate;
	}

	@Column(name = "stocks_arm")
	public Integer getStocksArm() {
		return stocksArm;
	}

	public void setStocksArm(Integer stocksArm) {
		this.stocksArm = stocksArm;
	}

	@Column(name = "active_id")
	public Integer getAcitveId() {
		return acitveId;
	}
	@Column(name = "content_m")
	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	@Column(name = "first_level_rate")
	public Double getFirstLevelRate() {
		return firstLevelRate;
	}

	public void setFirstLevelRate(Double firstLevelRate) {
		this.firstLevelRate = firstLevelRate;
	}

	@Column(name = "second_level_rate")
	public Double getSecondLevelRate() {
		return secondLevelRate;
	}

	public void setSecondLevelRate(Double secondLevelRate) {
		this.secondLevelRate = secondLevelRate;
	}

	@Column(name = "third_level_rate")
	public Double getThirdLevelRate() {
		return thirdLevelRate;
	}

	public void setAcitveId(Integer acitveId) {
		this.acitveId = acitveId;
	}

	public void setThirdLevelRate(Double thirdLevelRate) {
		this.thirdLevelRate = thirdLevelRate;
	}

	@Column(name = "wx_a_code")
	public String getWxACode() {
		return wxAcode;
	}

	public void setWxACode(String wxAcode) {
		this.wxAcode = wxAcode;
	}

    @Column(name = "pro_video")
    public String getProVideoUrl() {
        return proVideoUrl;
    }

    public void setProVideoUrl(String proVideoUrl) {
        this.proVideoUrl = proVideoUrl;
    }

	@Column(name = "shop_wx_code")
	public String getShopWxCode() {
		return shopWxCode;
	}

	public void setShopWxCode(String shopWxCode) {
		this.shopWxCode = shopWxCode;
	}

	@Column(name = "search_weight")
	public Double getSearchWeight() {
		return searchWeight;
	}

	public void setSearchWeight(Double searchWeight) {
		this.searchWeight = searchWeight;
	}

	@Transient
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
}
