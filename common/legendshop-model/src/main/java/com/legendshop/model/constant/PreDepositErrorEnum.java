package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 预付款操作出现的错误情况
 * @author Tony
 *
 */
public enum PreDepositErrorEnum implements StringEnum{

	OK("OK"), //成功
	
	ERR("ERR"), //失败
	
	NOT_USER("NOT_USER"), //用户帐号不存在
	
	NOT_FIND("NOT_FIND"), //没有找到对象，
	
	NON_COMPLIANCE("NON_COMPLIANCE"), //不符合操作条件 ---> 提现完成，财务审核结束操作    取消提现申请操作 
	
	INSUFFICIENT_AMOUNT("INSUFFICIENT_AMOUNT"), //账户余额不足
	
	IS_PAY_SECURITY("IS_PAY_SECURITY"), //已支付
	;
	
	/** The value. */
	private final String value;

	private PreDepositErrorEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	
	
}
