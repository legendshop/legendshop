package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 正在进行拼团的小组列表
 */
public class MergeGroupingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 团编号 */
	private String addNumber;

	/** 已参团人数 */
	private Integer propleCount;

	/** 团长头像 */
	private String portraitPic;
	
	/** 创建团时间 */
	private Date createTime;
	
	/** 成团截止时间 */
	private Date endTime;

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Integer getPropleCount() {
		return propleCount;
	}

	public void setPropleCount(Integer propleCount) {
		this.propleCount = propleCount;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
