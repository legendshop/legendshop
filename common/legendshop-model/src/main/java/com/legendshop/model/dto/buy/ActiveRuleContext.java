package com.legendshop.model.dto.buy;

import java.util.HashMap;
import java.util.Map;

/**
 * 活动参与规则排除
 * 
 */
public class ActiveRuleContext {

	private static final String EXECU_SHIPPING_ = "EXECU_SHIPPING_"; // 包邮活动

	private Map<String, Boolean> runtimeEnv = new HashMap<String, Boolean>();

	// 满件包邮
	public void executeShipping(Long prodId, Long skuId) {
		runtimeEnv.put(EXECU_SHIPPING_ + prodId + "_" + skuId, true);
	}

	// 包邮活动是否执行过
	public boolean isShippingExecuted(Long prodId, Long skuId) {
		Boolean result = runtimeEnv.get(EXECU_SHIPPING_ + prodId + "_" + skuId);
		return result == null ? false : true;
	}

	/* 设置针对店铺的活动参与 */
	public void executeShopShipping() {
		runtimeEnv.put(EXECU_SHIPPING_ +"HIS" , true);
	}
	
	/* 判断是否已经设置针对店铺的活动参与 */
	public boolean isShopShippingExecuted() {
		Boolean result = runtimeEnv.get(EXECU_SHIPPING_ +"HIS" );
		return result == null ? false : true;
	}
	
}
