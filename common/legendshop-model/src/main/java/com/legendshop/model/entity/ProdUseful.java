package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *产品是否有用表
 */
@Entity
@Table(name = "ls_prod_useful")
public class ProdUseful implements GenericEntity<Long> {

	private static final long serialVersionUID = -665504646335023468L;

	/** ID */
	private Long id; 
		
	/** 商品评论ID */
	private Long prodCommId; 
		
	/** 回复有用的人 */
	private String userId; 
		
	/** 时间 */
	private Date recDate; 
		
	
	public ProdUseful() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_USEFUL_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_comm_id")
	public Long  getProdCommId(){
		return prodCommId;
	} 
		
	public void setProdCommId(Long prodCommId){
			this.prodCommId = prodCommId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
		this.userId = userId;
	}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	


} 
