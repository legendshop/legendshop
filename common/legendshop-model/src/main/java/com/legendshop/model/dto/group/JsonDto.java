/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

/**
 * 用于前后台传输json的工具类.
 *
 * @param <T> the generic type
 */
public class JsonDto<T> {
	
	/** 是否发送. */
	private boolean isSuccess;
    
    /** 状态 */
    private  int status;
    
    /** 错误 */
    private String error;
    
    /** 数据 */
    private T data;
    
    /**
     * Gets the error.
     *
     * @return the error
     */
    public String getError() {
        return error;
    }
    
    /**
     * Sets the error.
     *
     * @param error the new error
     */
    public void setError(String error) {
        this.error = error;
    }
    
    /**
     * Gets the data.
     *
     * @return the data
     */
    public T getData() {
        return data;
    }
    
    /**
     * Sets the data.
     *
     * @param data the new data
     */
    public void setData(T data) {
        this.data = data;
    }
    
    /**
     * <p>Title: </p>
     * <p>Description: 如果错误,返回错误信息.
     *
     * @param isSuccess the is success
     * @param error the error
     */
    public JsonDto(boolean isSuccess, String error) {
        super();
        this.isSuccess = isSuccess;
        this.error = error;
    }
    
    /**
     * <p>Title: </p>
     * <p>Description: 如果正确，返回正确的数据 1.
     *
     * @param isSuccess the is success
     * @param data the data
     */
    public JsonDto(boolean isSuccess, T data) {
        super();
        this.isSuccess = isSuccess;
        this.data = data;
    }
    
    
    
    /**
     * Instantiates a new json dto.
     *
     * @param isSuccess the is success
     * @param status the status
     * @param error the error
     */
    public JsonDto(boolean isSuccess, int status, String error) {
		super();
		this.isSuccess = isSuccess;
		this.status = status;
		this.error = error;
	}
    
    
	/**
	 * Instantiates a new json dto.
	 *
	 * @param isSuccess the is success
	 * @param status the status
	 * @param data the data
	 */
	public JsonDto(boolean isSuccess, int status, T data) {
		super();
		this.isSuccess = isSuccess;
		this.status = status;
		this.data = data;
	}
	
	
	/**
	 * Instantiates a new json dto.
	 */
	public JsonDto() {
    	
    }
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * Gets the checks if is success.
	 *
	 * @return the checks if is success
	 */
	public boolean getIsSuccess() {
		return isSuccess;
	}
	
	/**
	 * Sets the checks if is success.
	 *
	 * @param isSuccess the new checks if is success
	 */
	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
}
