/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;

/**
 * 秒杀sku
 */
public class SeckillSku implements Serializable{

	private static final long serialVersionUID = 1L;

	/** skuId. */
	private Long skuId;

    /** 秒杀库存. */
    private Long seckillStock;
	
	/** 秒杀价格 */
	private Double  seckillPrice;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getSeckillStock() {
		return seckillStock;
	}

	public void setSeckillStock(Long seckillStock) {
		this.seckillStock = seckillStock;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public SeckillSku(Long skuId, Long seckillStock, Double seckillPrice) {
		super();
		this.skuId = skuId;
		this.seckillStock = seckillStock;
		this.seckillPrice = seckillPrice;
	}
	
}
