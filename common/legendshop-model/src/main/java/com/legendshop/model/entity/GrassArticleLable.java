package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *种草社区文章关联标签表
 */
@Entity
@Table(name = "ls_grass_article_lable")
public class GrassArticleLable implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 种草文章id */
	private Long grarticleId; 
		
	/** 种草标签id */
	private Long grlabelId; 
		
	
	public GrassArticleLable() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GRASS_ARTICLE_LABLE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "grarticle_id")
	public Long  getGrarticleId(){
		return grarticleId;
	} 
		
	public void setGrarticleId(Long grarticleId){
			this.grarticleId = grarticleId;
		}
		
    @Column(name = "grlabel_id")
	public Long  getGrlabelId(){
		return grlabelId;
	} 
		
	public void setGrlabelId(Long grlabelId){
			this.grlabelId = grlabelId;
		}
	


} 
