package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.UserDetail;
/**
 * 商品导入结果封装
 *
 */
public class CoinImportResult implements Serializable{
	
	private static final long serialVersionUID = -4136053404729457826L;

		private String message;
		
		private List<UserDetail> result; 
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public List<UserDetail> getResult() {
			return result;
		}
		public void setResult(List<UserDetail> result) {
			this.result = result;
		}
}
