package com.legendshop.model.dto.marketing;

import java.util.ArrayList;
import java.util.List;


/**
 * SKU RUlE
 * @author Tony
 *
 */
public class MarketingSkuRuleEngineDto {
	
	
	private Long skuId;
	
	/**
	 * 商品原价
	 */
	private Double price;

	/**
	 * 商品现价
	 */
	private Double cash;
	
	/**
	 * 促销价格
	 */
	private Double promotionPrice;
	
    /**
  	 * 营销规则
  	 */
    private List<MarketingDto> ruleList;
  


	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public List<MarketingDto> getRuleList() {
		return ruleList;
	}

	public void setRuleList(List<MarketingDto> ruleList) {
		this.ruleList = ruleList;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	

	public void addRuleList(MarketingDto rule) {
		if(ruleList == null){
			ruleList = new ArrayList<MarketingDto>();
		}
		ruleList.add(rule);
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	

}
