/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *物流评分统计表
 */
@Entity
@Table(name = "ls_dvy_type_comm_stat")
public class DvyTypeCommStat implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2095663934315351919L;

	/** 主键 */
	private Long id; 
		
	/** 配送方式ID */
	private Long dvyTypeId; 
		
	/** 点评次数 */
	private Integer count; 
		
	/** 总分数 */
	private Integer score; 
		
	
	public DvyTypeCommStat() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DVY_TYPE_COMM_STAT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "dvy_type_id")
	public Long  getDvyTypeId(){
		return dvyTypeId;
	} 
		
	public void setDvyTypeId(Long dvyTypeId){
			this.dvyTypeId = dvyTypeId;
		}
		
    @Column(name = "count")
	public Integer  getCount(){
		return count;
	} 
		
	public void setCount(Integer count){
			this.count = count;
		}
		
    @Column(name = "score")
	public Integer  getScore(){
		return score;
	} 
		
	public void setScore(Integer score){
			this.score = score;
		}
	


} 
