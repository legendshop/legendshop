package com.legendshop.model.entity;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 拍卖表
 */
@Entity
@Table(name = "ls_auctions")
public class Auctions implements GenericEntity<Long> {

	private static final long serialVersionUID = 241279228873972554L;

	/** 活动编号 */
	private Long id; 
		
	private String userId;
	
	/** 商家编号 */
	private Long shopId; 
		
	/** 竞买方店铺名称 */
	private String shopName; 
		
	/** 拍卖活动名称 */
	private String auctionsTitle; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** SKU ID */
	private Long skuId; 
		
	/** 拍卖开始时间 */
	private Date startTime; 
		
	/** 拍卖结束时间 */
	private Date endTime; 
		
	/** 起拍价 */
	private java.math.BigDecimal floorPrice; 
		
	/** 最小加价幅度 */
	private java.math.BigDecimal minMarkupRange; 
		
	/** 最高加价幅度 */
	private java.math.BigDecimal maxMarkupRange; 
		
	/** 是否封顶 */
	private Long isCeiling; 
		
	/** 一口价 */
	private java.math.BigDecimal fixedPrice; 
		
	/** 是否保证金 */
	private Long isSecurity; 
		
	/** 保证金 */
	private java.math.BigDecimal securityPrice; 
		
	/** 是否隐藏起拍价 */
	private Long hideFloorPrice; 
		
	/** 延迟时长 分钟 */
	private Long delayTime; 
		
	/** 状态 */
	private Long status; 
		
	/** 拍品描述 */
	private String auctionsDesc; 
		
	/** 报名人数 */
	private Long enrolNumber; 
		
	/** 围观数量 */
	private Long crowdWatch; 
		
	/** 出价次数 */
	private Long biddingNumber;
	/**
	 * 审核意见
	 */
	private String auditOpinion;
	/**
	 * 审核时间
	 */
	private Date auditTime;
	/**
	 * 商品名称
	 */
	private String prodName;
	/**
	 * 商品图片
	 */
	private String prodPic;
	/**
	 * 商品价格（原价）
	 */
	private String prodPrice;
	
	/** SKU 属性 */
	private String cnProperties;
	
	/**
	 * 当前价
	 */
	private java.math.BigDecimal curPrice;
	
	/**
	 * 处理状态   0:未处理,1:已处理
	 */
	private Integer flagStatus;
	
	/**
	 * 商品状态 参考ProductStatusEnum
	 */
	private Long prodStatus;
	
	/** 商品sku状态   1：上线  0：下线  */
	private Long skuStatus;

	/**
	 * 用于查询
	 */
	private String searchType;
	
	private List<String> skuPicList;

	public Auctions() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "AUCTIONS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
	
	@Column(name = "user_id")
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_name")
	public String  getShopName(){
		return shopName;
	} 
		
	public void setShopName(String shopName){
			this.shopName = shopName;
		}
		
    @Column(name = "auctions_title")
	public String  getAuctionsTitle(){
		return auctionsTitle;
	} 
		
	public void setAuctionsTitle(String auctionsTitle){
			this.auctionsTitle = auctionsTitle;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
    
    @Transient
    public Long  getStartTimeStr(){
    	if(startTime!=null){
    		return startTime.getTime();
    	}
		return 0l;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
    
    @Transient
    public Long  getEndTimeStr(){
    	if(endTime!=null){
    		return endTime.getTime();
    	}
		return 0l;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}
		
    @Column(name = "floor_price")
	public java.math.BigDecimal  getFloorPrice(){
		return floorPrice;
	} 
		
	public void setFloorPrice(java.math.BigDecimal floorPrice){
			this.floorPrice = floorPrice;
		}
		
    @Column(name = "min_markup_range")
	public java.math.BigDecimal  getMinMarkupRange(){
		return minMarkupRange;
	} 
		
	public void setMinMarkupRange(java.math.BigDecimal minMarkupRange){
			this.minMarkupRange = minMarkupRange;
		}
		
    @Column(name = "max_markup_range")
	public java.math.BigDecimal  getMaxMarkupRange(){
		return maxMarkupRange;
	} 
		
	public void setMaxMarkupRange(java.math.BigDecimal maxMarkupRange){
			this.maxMarkupRange = maxMarkupRange;
		}
		
    @Column(name = "is_ceiling")
	public Long  getIsCeiling(){
		return isCeiling;
	} 
		
	public void setIsCeiling(Long isCeiling){
			this.isCeiling = isCeiling;
		}
		
    @Column(name = "fixed_price")
	public java.math.BigDecimal  getFixedPrice(){
		return fixedPrice;
	} 
		
	public void setFixedPrice(java.math.BigDecimal fixedPrice){
			this.fixedPrice = fixedPrice;
		}
		
    @Column(name = "is_security")
	public Long  getIsSecurity(){
		return isSecurity;
	} 
		
	public void setIsSecurity(Long isSecurity){
			this.isSecurity = isSecurity;
		}
		
    @Column(name = "security_price")
	public java.math.BigDecimal  getSecurityPrice(){
		return securityPrice;
	} 
		
	public void setSecurityPrice(java.math.BigDecimal securityPrice){
			this.securityPrice = securityPrice;
		}
		
    @Column(name = "hide_floor_price")
	public Long  getHideFloorPrice(){
		return hideFloorPrice;
	} 
		
	public void setHideFloorPrice(Long hideFloorPrice){
			this.hideFloorPrice = hideFloorPrice;
		}
		
    @Column(name = "delay_time")
	public Long  getDelayTime(){
		return delayTime;
	} 
		
	public void setDelayTime(Long delayTime){
			this.delayTime = delayTime;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
		}
		
    @Column(name = "auctions_desc")
	public String  getAuctionsDesc(){
		return auctionsDesc;
	} 
		
	public void setAuctionsDesc(String auctionsDesc){
			this.auctionsDesc = auctionsDesc;
		}
		
    @Column(name = "enrol_number")
	public Long  getEnrolNumber(){
		return enrolNumber;
	} 
		
	public void setEnrolNumber(Long enrolNumber){
			this.enrolNumber = enrolNumber;
		}
		
    @Column(name = "crowd_watch")
	public Long  getCrowdWatch(){
		return crowdWatch;
	} 
		
	public void setCrowdWatch(Long crowdWatch){
			this.crowdWatch = crowdWatch;
		}
		
    @Column(name = "bidding_number")
	public Long  getBiddingNumber(){
		return biddingNumber;
	} 
		
	public void setBiddingNumber(Long biddingNumber){
			this.biddingNumber = biddingNumber;
		}
	
	
	@Column(name = "audit_opinion")
	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}
	@Column(name = "audit_time")
	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	@Column(name = "cur_price")
	public java.math.BigDecimal getCurPrice() {
		return curPrice;
	}

	public void setCurPrice(java.math.BigDecimal curPrice) {
		this.curPrice = curPrice;
	}
	
	
	@Column(name = "flag_status")
	public Integer getFlagStatus() {
		return flagStatus;
	}

	public void setFlagStatus(Integer flagStatus) {
		this.flagStatus = flagStatus;
	}

	@Transient
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	@Transient
	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	@Transient
	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}
	@Transient
	public String getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(String prodPrice) {
		this.prodPrice = prodPrice;
	}
	
	

	@Transient
	public List<String> getSkuPicList() {
		return skuPicList;
	}

	public void setSkuPicList(List<String> skuPicList) {
		this.skuPicList = skuPicList;
	}
	@Transient
	public Long getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Long prodStatus) {
		this.prodStatus = prodStatus;
	}
	
	@Transient
	public Long getSkuStatus() {
		return skuStatus;
	}

	public void setSkuStatus(Long skuStatus) {
		this.skuStatus = skuStatus;
	}
	@Transient
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
}
