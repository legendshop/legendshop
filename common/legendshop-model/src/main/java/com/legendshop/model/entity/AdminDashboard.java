package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_admin_dashboard")
public class AdminDashboard implements GenericEntity<String> {

	private static final long serialVersionUID = 1529559802428334490L;

	/** 主键 */
	private String dashboardName; 
		
	/** 参考值 */
	private String value; 
		
	/** 记录时间 */
	private Date recDate; 
		
	
	public AdminDashboard() {
    }

		
    @Column(name = "value")
	public String  getValue(){
		return value;
	} 
		
	public void setValue(String value){
			this.value = value;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	
	@Transient
	public String getId() {
		return dashboardName;
	}
	
	public void setId(String id) {
		dashboardName = id;
	}

	@Id
	@Column(name = "dashboard_name")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ADMIN_DASHBOARD_SEQ")
	public String getDashboardName() {
		return dashboardName;
	}

	public void setDashboardName(String dashboardName) {
		this.dashboardName = dashboardName;
	}

	

} 
