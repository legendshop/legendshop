/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 用于购物车Cookie商品信息存放
 * 
 * @author tony
 */
public class BasketCookieDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 购物车ID */
	private Long basketId;
	
	/** 商品ID */
	private Long prodId;

	/** 购买数量 */
	private Integer basketCount;

	/** 单品ID */
	private Long skuId;

	/** 店铺ID */
	private Long shopId;

	/** 分销的用户名称  */
	private String distUserName;
	
	/** 选中状态 */
	private int checkSts;
	
	/** 门店ID */
	private Long storeId;

	public Long getProdId() {
		return prodId;
	}

	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getDistUserName() {
		return distUserName;
	}

	public void setDistUserName(String distUserName) {
		this.distUserName = distUserName;
	}

	public int getCheckSts() {
		return checkSts;
	}

	public void setCheckSts(int checkSts) {
		this.checkSts = checkSts;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	
}
