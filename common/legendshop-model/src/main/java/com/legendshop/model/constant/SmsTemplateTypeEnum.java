package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 消息类型
 * @author Tony
 *
 */
public enum SmsTemplateTypeEnum implements StringEnum {
    /** 校验流程   */
    VAL("2"),
	/* 短信通知 */
    /**会员修改交易密码时 */
    CHANGED_DEAL_PASSWORD("changed_deal_password"),

    /** 会员修改登录密码时  */
    CHANGED_PASSWORD("changed_password"),

    /** 会员找回登录密码   */
    FORGOTTEN_PASSWORD("forgotten_password"),

    /** 会员注册成功时   */
    NEWUSER_ACCOUNT_CREATED("newuser_account_created"),

    /** 提现验证码 */
    WITHDRAW_CODE("withdraw_apply"),

    /** 手机验证码登录   */
    LOGIN("login"),

    /** 订单关闭以后   */
    ORDER_CLOSED("order_closed"),

    /** 订单创建时   */
    ORDER_CREATED("order_created"),

    /** 订单支付时   */
    ORDER_PAYMENT("order_payment"),

    /** 订单退款以后   */
    ORDER_REFUND("order_refund"),

    /** 订单发货时   */
    ORDER_SHIPPING("order_shipping"),

    /** 会员提现申请   */
    WITHDRAW_APPLY("withdraw_apply"),

    /** 商品到货通知 */
    ARRIVAL_INFORM("arrival_inform"),

    /** 商品到货通知 */
    EMAIL_TESTING("email_testing"),

    /** 自定义通知 */
    EMAIL_CUSTOM("email_custom"),


    ;


    /** The value. */
    private final String value;

    /**
     * Instantiates a new function enum.
     *
     * @param value
     *            the value
     */
    private SmsTemplateTypeEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    /**
     * Instance.
     *
     * @param name
     *            the name
     * @return true, if successful
     */
    public static boolean instance(String name) {
        SmsTemplateTypeEnum[] licenseEnums = values();
        for (SmsTemplateTypeEnum licenseEnum : licenseEnums) {
            if (licenseEnum.value().equals(name)) {
                return true;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println(SmsTemplateTypeEnum.VAL.value());
    }
}
