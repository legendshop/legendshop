package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_seckill_activity")
public class SeckillActivity implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4007685827296343977L;

	/**  */
	private Long id; 
		
	/** 用户编号 */
	private String userId; 
		
	/** 商家编号 */
	private Long shopId; 
		
	/** 秒杀活动店铺名称 */
	private String shopName; 
		
	/** 活动名称 */
	private String seckillTitle; 
	
	/** 开始时间 */
	private Date startTime; 
		
	/** 结束时间 */
	private Date endTime; 
		
	/** 活动状态 */
	private Long status; 
	
	/** 秒杀活动描述 */
	private String seckillDesc; 
	
	/** 秒杀活动图片 */
	private String seckillPic; 
	
	/** 秒杀活动摘要 */
	private String seckillBrief; 
	
	/** 秒杀活动最低价 */
	private java.math.BigDecimal seckillLowPrice; 
		
	/** 审核意见 */
	private String auditOpinion; 
		
	/** 审核时间 */
	private Date auditTime; 
		
	private String skus;
	
	private Date overdue;//查询过期标识
	
	private Date overdate;//过滤过期
	
	private MultipartFile seckillPicFile;
	
	private Long stockTotal=0l;//秒杀总库存
	
	private double price=0;//商品原价
	
	/** 商品状态 参考  ProductStatusEnum */
	private Integer prodStatus;
	
	/** 商品id */
	private Long prodId;

	/** 用于搜索 */
	private String searchType;

	/** 活动id */
	private Long activeId;

	@Transient
	public Long getStockTotal() {
		return stockTotal;
	}

	public void setStockTotal(Long stockTotal) {
		this.stockTotal = stockTotal;
	}
	@Transient
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public SeckillActivity() {
    }

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SECKILL_ACTIVITY_SEQ")
	public Long  getId(){
		return id;
	}

	public void setId(Long id){
			this.id = id;
		}

    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	}

	public void setUserId(String userId){
			this.userId = userId;
		}

    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	}

	public void setShopId(Long shopId){
			this.shopId = shopId;
		}

    @Column(name = "shop_name")
	public String  getShopName(){
		return shopName;
	}

	public void setShopName(String shopName){
			this.shopName = shopName;
		}

    @Column(name = "seckill_title")
	public String  getSeckillTitle(){
		return seckillTitle;
	}

	public void setSeckillTitle(String seckillTitle){
			this.seckillTitle = seckillTitle;
		}

	@Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	}

	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}

    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	}

	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}

    @Column(name = "status")
	public Long  getStatus(){
		return status;
	}

	public void setStatus(Long status){
			this.status = status;
		}


	@Column(name="seckill_desc")
    public String getSeckillDesc() {
		return seckillDesc;
	}

	public void setSeckillDesc(String seckillDesc) {
		this.seckillDesc = seckillDesc;
	}

    @Column(name = "audit_opinion")
	public String  getAuditOpinion(){
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion){
			this.auditOpinion = auditOpinion;
		}

    @Column(name = "audit_time")
	public Date  getAuditTime(){
		return auditTime;
	}

	public void setAuditTime(Date auditTime){
			this.auditTime = auditTime;
		}

	@Column(name = "seckill_pic")
	public String getSeckillPic() {
		return seckillPic;
	}

	public void setSeckillPic(String seckillPic) {
		this.seckillPic = seckillPic;
	}
	@Column(name = "seckill_brief")
	public String getSeckillBrief() {
		return seckillBrief;
	}

	public void setSeckillBrief(String seckillBrief) {
		this.seckillBrief = seckillBrief;
	}

	@Column(name = "seckill_low_price")
	public java.math.BigDecimal getSeckillLowPrice() {
		return seckillLowPrice;
	}

	public void setSeckillLowPrice(java.math.BigDecimal seckillLowPrice) {
		this.seckillLowPrice = seckillLowPrice;
	}

	@Transient
	public String getSkus() {
		return skus;
	}

	public void setSkus(String skus) {
		this.skus = skus;
	}
	@Transient
	public Date getOverdue() {
		return overdue;
	}

	public void setOverdue(Date overdue) {
		this.overdue = overdue;
	}
	@Transient
	public Date getOverdate() {
		return overdate;
	}

	public void setOverdate(Date overdate) {
		this.overdate = overdate;
	}
	@Transient
	public MultipartFile getSeckillPicFile() {
		return seckillPicFile;
	}

	public void setSeckillPicFile(MultipartFile seckillPicFile) {
		this.seckillPicFile = seckillPicFile;
	}

	@Transient
	public Integer getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Integer prodStatus) {
		this.prodStatus = prodStatus;
	}

	@Transient
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Transient
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	@Transient
	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}
}
