/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 */
@Entity
@Table(name = "ls_login_hist")
public class LoginHistory  implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8643893857459673801L;

	/** The id. */
	private Long id;
	
	/** The user id. */
	private String userId;

	/** The user name. */
	private String userName;

	/** The ip. */
	private String ip;

	/** The time. */
	private Date time;

	/** The start time. */
	private Date startTime;

	/** The end time. */
	private Date endTime;

	/** The login times. */
	private Integer loginTimes;

	/** The country. */
	private String country;

	/** The area. */
	private String area;
	
	private String loginType;

	/**
	 * Instantiates a new login history.
	 */
	public LoginHistory() {
		
	}
	
	public LoginHistory(String userName, String ip) {
		this.userName = userName;
		this.ip = ip;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "LOGIN_HIST_SEQ")
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the ip.
	 * 
	 * @return the ip
	 */
	@Column(name = "ip")
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 * 
	 * @param ip
	 *            the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the time.
	 * 
	 * @return the time
	 */
	@Column(name = "time")
	public Date getTime() {
		return time;
	}

	/**
	 * Sets the time.
	 * 
	 * @param time
	 *            the new time
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * Gets the end time.
	 * 
	 * @return the end time
	 */
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 * 
	 * @param endTime
	 *            the new end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the start time.
	 * 
	 * @return the start time
	 */
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 * 
	 * @param startTime
	 *            the new start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the login times.
	 * 
	 * @return the login times
	 */
	@Transient
	public Integer getLoginTimes() {
		return loginTimes;
	}

	/**
	 * Sets the login times.
	 * 
	 * @param loginTimes
	 *            the new login times
	 */
	public void setLoginTimes(Integer loginTimes) {
		this.loginTimes = loginTimes;
	}

	/**
	 * Instantiates a new login history.
	 * 
	 * @param userName
	 *            the user name
	 * @param loginTimes
	 *            the login times
	 */
	public LoginHistory(String userName, Integer loginTimes) {
		super();
		this.userName = userName;
		this.loginTimes = loginTimes;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	@Column(name = "country")
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the area.
	 * 
	 * @return the area
	 */
	@Column(name = "area")
	public String getArea() {
		return area;
	}

	/**
	 * Sets the area.
	 * 
	 * @param area
	 *            the new area
	 */
	public void setArea(String area) {
		this.area = area;
	}
	
	@Column(name = "login_type")
	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
