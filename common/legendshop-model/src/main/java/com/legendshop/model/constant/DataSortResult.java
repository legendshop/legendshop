package com.legendshop.model.constant;

import java.io.Serializable;
/**
 * 返回是否排序的结果
 *
 */
public class DataSortResult implements Serializable{

	private static final long serialVersionUID = 8421366695766867480L;
	
	private boolean sortExternal;
	
	private String sortName;
	
	private String orderIndicator;
	
	public boolean isSortExternal() {
		return sortExternal;
	}

	public void setSortExternal(boolean sortExternal) {
		this.sortExternal = sortExternal;
	}


	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getOrderIndicator() {
		return orderIndicator;
	}

	public void setOrderIndicator(String orderIndicator) {
		this.orderIndicator = orderIndicator;
	}
	
	/**
	 * 处理排序顺序
	 * @return
	 */
	public String parseSeq(){
		return "order by " + sortName + " " + orderIndicator;
	}

}
