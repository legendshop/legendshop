package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 移动端店铺首页装修状态
 *
 */
public enum ShopAppDecorateStatusEnum implements IntegerEnum{
	
	/** 平台下线  */
	PLATFORM_OFF_LINE(-2),
	
	/** 下线  */
	OFF_LINE(-1),
	
	/** 已修改未上线  */
	MODIFIED(0),
	
	/** 上线   */
	ON_LINE(1),
	
	;
	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private ShopAppDecorateStatusEnum(Integer num) {
		this.num = num;
	}
	
}
