package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信接受的文字
 */
@Entity
@Table(name = "ls_weixin_receivetext")
public class WeixinReceivetext implements GenericEntity<Long> {

	private static final long serialVersionUID = -1373298870661619180L;

	/**  */
	private Long id; 
		
	/**  */
	private String content; 
		
	/**  */
	private Date createtime; 
		
	/**  */
	private String fromusername; 
		
	/**  */
	private String msgid; 
		
	/**  */
	private String msgtype; 
		
	/**  */
	private String rescontent; 
		
	/**  */
	private String response; 
		
	/**  */
	private String tousername; 
		
	/**  */
	private String nickname; 
		
	
	public WeixinReceivetext() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_RECEIVETEXT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "createtime")
	public Date  getCreatetime(){
		return createtime;
	} 
		
	public void setCreatetime(Date createtime){
			this.createtime = createtime;
		}
		
    @Column(name = "fromusername")
	public String  getFromusername(){
		return fromusername;
	} 
		
	public void setFromusername(String fromusername){
			this.fromusername = fromusername;
		}
		
    @Column(name = "msgid")
	public String  getMsgid(){
		return msgid;
	} 
		
	public void setMsgid(String msgid){
			this.msgid = msgid;
		}
		
    @Column(name = "msgtype")
	public String  getMsgtype(){
		return msgtype;
	} 
		
	public void setMsgtype(String msgtype){
			this.msgtype = msgtype;
		}
		
    @Column(name = "rescontent")
	public String  getRescontent(){
		return rescontent;
	} 
		
	public void setRescontent(String rescontent){
			this.rescontent = rescontent;
		}
		
    @Column(name = "response")
	public String  getResponse(){
		return response;
	} 
		
	public void setResponse(String response){
			this.response = response;
		}
		
    @Column(name = "tousername")
	public String  getTousername(){
		return tousername;
	} 
		
	public void setTousername(String tousername){
			this.tousername = tousername;
		}
		
    @Column(name = "nickname")
	public String  getNickname(){
		return nickname;
	} 
		
	public void setNickname(String nickname){
			this.nickname = nickname;
		}
	


} 
