package com.legendshop.model.entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.recomm.RecommConDto;

/**
 *公用的产品类目
 */
@Entity
@Table(name = "ls_category")
public class Category implements GenericEntity<Long> {

	private static final long serialVersionUID = 6502437395407034984L;

	/** 产品类目ID */
	private Long id; 
		
	/** 父节点 */
	private Long parentId; 
		
	/** 产品类目名称 */
	private String name; 
	
	/** 分类小图标*/
	private String icon;
		
	/** 类目的显示图片 */
	private String pic; 
		
	/** 产品类目类型,参见ProductTypeEnum */
	private String type; 
		
	/** 排序 */
	private Integer seq; 
		
	/** 默认是1，表示正常状态,0为下线状态 */
	private Integer status; 
		
	/** 是否Header菜单展示，0否，1是 */
	private Boolean headerMenu; 
		
	/** 导航菜单中显示，0否1是 */
	private Boolean navigationMenu; 
		
	/** 类型ID */
	private Integer typeId; 
	
	/** 类型名称 */
	private String typeName; 
		
	/** SEO关键字 */
	private String keyword; 
		
	/** SEO描述 */
	private String catDesc; 
		
	/** SEO标题 */
	private String title; 
		
	/** 记录时间 */
	private Date recDate; 
	
	/**分类层级*/
	private Integer grade;
	
	/**推荐内容json字符串*/
	private String recommCon;
	
	/**推荐内容dto*/
	private RecommConDto recommConDto;
	
	/**退换货有效时间**/
	private Integer returnValidPeriod;
	
	/**父类名称*/
	private String parentName;
	
	private MultipartFile catPicFile;
	
	private MultipartFile catIconFile;
		
	/**子分类**/
	private List<Category> childrenList;
	
	public Category() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "CATEGORY_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "parent_id")
	public Long  getParentId(){
		return parentId;
	} 
		
	public void setParentId(Long parentId){
			this.parentId = parentId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "type")
	public String  getType(){
		return type;
	} 
		
	public void setType(String type){
			this.type = type;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "header_menu")
	public Boolean  getHeaderMenu(){
		return headerMenu;
	} 
		
	public void setHeaderMenu(Boolean headerMenu){
			this.headerMenu = headerMenu;
		}
		
    @Column(name = "navigation_menu")
	public Boolean  getNavigationMenu(){
		return navigationMenu;
	} 
		
	public void setNavigationMenu(Boolean navigationMenu){
			this.navigationMenu = navigationMenu;
		}
		
    @Column(name = "type_id")
	public Integer  getTypeId(){
		return typeId;
	} 
		
	public void setTypeId(Integer typeId){
			this.typeId = typeId;
		}
		
    @Column(name = "keyword")
	public String  getKeyword(){
		return keyword;
	} 
		
	public void setKeyword(String keyword){
			this.keyword = keyword;
		}
		
    @Column(name = "cat_desc")
	public String  getCatDesc(){
		return catDesc;
	} 
		
	public void setCatDesc(String catDesc){
			this.catDesc = catDesc;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	
	@Column(name = "return_valid_period")
	public Integer getReturnValidPeriod() {
		return returnValidPeriod;
	}

	public void setReturnValidPeriod(Integer returnValidPeriod) {
		this.returnValidPeriod = returnValidPeriod;
	}

	
	@Column(name = "grade")
	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	@Transient
	public MultipartFile getCatPicFile() {
		return catPicFile;
	}

	public void setCatPicFile(MultipartFile catPicFile) {
		this.catPicFile = catPicFile;
	}

	@Transient
	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	@Transient
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Category){
			Category node=(Category)obj;
			return this.id==node.getId();
		}
		return false;
	}
	
	public void addChildren(Category Children){
		if(childrenList == null){
			childrenList = new ArrayList<Category>();
		}
		if(this.id.equals(Children.getParentId())){
			childrenList.add(Children);
		}
	}

	@Transient
	public List<Category> getChildrenList() {
		return childrenList;
	}

	
	public void setChildrenList(List<Category> childrenList) {
		this.childrenList = childrenList;
	}

	@Column(name = "recomm_con")
	public String getRecommCon() {
		return recommCon;
	}

	public void setRecommCon(String recommCon) {
		this.recommCon = recommCon;
	}

	@Transient
	public RecommConDto getRecommConDto() {
		return recommConDto;
	}

	public void setRecommConDto(RecommConDto recommConDto) {
		this.recommConDto = recommConDto;
	}

	@Column(name = "icon")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Transient
	public MultipartFile getCatIconFile() {
		return catIconFile;
	}

	public void setCatIconFile(MultipartFile catIconFile) {
		this.catIconFile = catIconFile;
	}

	
	
} 
