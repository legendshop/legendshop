package com.legendshop.model.dto.stock;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品预警
 */
@ApiModel(value="StockArmDto") 
public class StockArmDto implements Serializable{

	private static final long serialVersionUID = 8603339400272532192L;
	
	/** 商家id */
	@ApiModelProperty(value="商家Id")
	private int shopId;
	
	
	/** 商品id */
	@ApiModelProperty(value="商品Id")
	private int prodId;
	
	@ApiModelProperty(value="单品Id")
	private int skuId;
	
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String picture;
	
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String productName;
	
	
	/** 销售属性组合 */
	@ApiModelProperty(value="销售属性组合的中文表达， key value用:隔开")
	private String cnProperties;
	
	
	/** 库存 */
	@ApiModelProperty(value="库存数量")
	private int stocks;
	
	
	/** 实际库存 */
	@ApiModelProperty(value="实际库存数量")
	private int actualStocks;
	
	
	/** 库存预警 */
	@ApiModelProperty(value="库存预警数量")
	private int stocksArm;
	
	
	/** 状态  */
	@ApiModelProperty(value="状态")
	private int status;
	
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public int getSkuId() {
		return skuId;
	}
	public void setSkuId(int skuId) {
		this.skuId = skuId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCnProperties() {
		return cnProperties;
	}
	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}
	public int getStocks() {
		return stocks;
	}
	public void setStocks(int stocks) {
		this.stocks = stocks;
	}
	public int getActualStocks() {
		return actualStocks;
	}
	public void setActualStocks(int actualStocks) {
		this.actualStocks = actualStocks;
	}
	public int getStocksArm() {
		return stocksArm;
	}
	public void setStocksArm(int stocksArm) {
		this.stocksArm = stocksArm;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	
}






