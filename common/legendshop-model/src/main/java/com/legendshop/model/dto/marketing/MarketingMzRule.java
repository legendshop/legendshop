package com.legendshop.model.dto.marketing;


/*
 * 促销活动规则
 */
public class MarketingMzRule {
	
	/**
	 * 订单购满金额下限
	 */
	private Double fullAmount;
	
	
	private Float offDiscount;
	
	private Integer calType;

	public Integer getCalType() {
		return calType;
	}

	public void setCalType(Integer calType) {
		this.calType = calType;
	}

	public Double getFullAmount() {
		return fullAmount;
	}

	public void setFullAmount(Double fullAmount) {
		this.fullAmount = fullAmount;
	}

	public Float getOffDiscount() {
		return offDiscount;
	}

	public void setOffDiscount(Float offDiscount) {
		this.offDiscount = offDiscount;
	}


}
