package com.legendshop.model;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;

/**
 * 用户自定义属性
 * 一个属性有多个属性值
 */
public class ResultCustomPropertyDto {

	private ProductProperty productProperty;
	
	private List<ProductPropertyValue> propertyValueList = new ArrayList<ProductPropertyValue>();

	public ProductProperty getProductProperty() {
		return productProperty;
	}

	public void setProductProperty(ProductProperty productProperty) {
		this.productProperty = productProperty;
	}

	public List<ProductPropertyValue> getPropertyValueList() {
		return propertyValueList;
	}

	public void setPropertyValueList(List<ProductPropertyValue> propertyValueList) {
		this.propertyValueList = propertyValueList;
	}

	
}
