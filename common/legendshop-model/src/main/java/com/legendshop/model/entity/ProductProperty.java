package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 商品属性
 * 
 */
@Entity
@Table(name = "ls_prod_prop")
public class ProductProperty implements GenericEntity<Long> {

	private static final long serialVersionUID = 7157140918610933422L;

	// 属性ID
	private Long propId;

	// 属性名称
	private String propName;

	// 别名
	private String memo;

	// 是否必须
	private boolean isRequired;

	// 是否多选
	private boolean isMulti;

	// 排序
	private Long sequence;

	// 状态
	private Short status;

	// 属性类型，1：有图片，0：文字
	private Integer type;

	// 修改时间
	private Date modifyDate;

	// 记录时间
	private Date recDate;

	// 规则属性：（1:销售属性; 2:参数属性; 3:关键属性）
	private Integer isRuleAttributes;

	// 是否可以搜索
	private boolean isForSearch;

	// 是否输入属性
	private boolean isInputProp;

	//用户名
	private String userName;
	
	//商品 id
	private Long prodId;
	
	//1： 用户自定义   0：系统自带
	private Integer isCustom;
	
	private Integer seq;
	
	private Long groupId;
	
	private String groupName;

	// 商品属性
	private List<ProductPropertyValue> valueList = new ArrayList<ProductPropertyValue>();

	public void setValueList(List<ProductPropertyValue> valueList) {
		this.valueList = valueList;
	}

	public ProductProperty() {
	}

	@Id
	@Column(name = "prop_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_PROP_SEQ")
	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	@Column(name = "prop_name")
	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	@Column(name = "memo")
	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "is_required")
	public boolean getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	@Column(name = "is_multi")
	public boolean getIsMulti() {
		return isMulti;
	}

	public void setIsMulti(boolean isMulti) {
		this.isMulti = isMulti;
	}

	@Column(name = "sequence")
	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	@Column(name = "status")
	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Column(name = "is_for_search")
	public boolean getIsForSearch() {
		return isForSearch;
	}

	public void setIsForSearch(boolean isForSearch) {
		this.isForSearch = isForSearch;
	}

	@Column(name = "is_input_prop")
	public boolean getIsInputProp() {
		return isInputProp;
	}

	public void setIsInputProp(boolean isInputProp) {
		this.isInputProp = isInputProp;
	}

	@Column(name = "is_rule_attributes")
	public Integer getIsRuleAttributes() {
		return isRuleAttributes;
	}

	public void setIsRuleAttributes(Integer isRuleAttributes) {
		this.isRuleAttributes = isRuleAttributes;
	}

	@Transient
	public Long getId() {
		return propId;
	}

	public void setId(Long id) {
		this.propId = id;
	}

	/**
	 * @return the productPropertyValueList
	 */
	@Transient
	public List<ProductPropertyValue> getValueList() {
		return valueList;
	}

	/**
	 * @param valueList
	 *            the productPropertyValueList to set
	 */
	public void addProductPropertyValueList(ProductPropertyValue productPropertyValue) {
		if (this.getPropId().equals(productPropertyValue.getPropId())) {
			valueList.add(productPropertyValue);
		}
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Column(name = "is_custom")
	public Integer getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Integer isCustom) {
		this.isCustom = isCustom;
	}

	@Transient
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Transient
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	@Transient
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}



}
