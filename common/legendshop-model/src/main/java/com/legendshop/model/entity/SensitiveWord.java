package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
/**
 * 敏感字过滤表
 */
@Entity
@Table(name = "ls_sens_word")
public class SensitiveWord implements GenericEntity<Long>{

	private static final long serialVersionUID = -8021084501779040179L;

	// ID
	private Long sensId;
		
	// 关键字
	private String words;
		
	// 是否全局敏感字
	private Integer isGlobal;
		
	
	public SensitiveWord() {
    }
		
	@Id
	@Column(name = "sens_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SENS_WORD_SEQ")
	public Long  getSensId(){
		return sensId ;
	} 
		
	public void setSensId(Long sensId){
		this.sensId = sensId ;
	}
		
	@Column(name = "words")
	public String  getWords(){
		return words ;
	} 
		
	public void setWords(String words){
		this.words = words ;
	}
		
	@Column(name = "is_global")	
	public Integer  getIsGlobal(){
		return isGlobal ;
	} 
		
	public void setIsGlobal(Integer isGlobal){
		this.isGlobal = isGlobal ;
	}
		
	@Transient
  public Long getId() {
		return 	sensId;
	}

	public void setId(Long id) {
		this.sensId = id;
	}
} 
