package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

public enum UserSourceEnum implements IntegerEnum{
	
	
	/** 手机号 */
	PHONE(0),
	
	/** 微信注册 */
	WEIXIN(1),
	
	/** */
	QQ(1),
	
	/** 微博 */
	WEIBO(2),
	;
	
	private Integer status;
	
	private UserSourceEnum(Integer status) {
		this.status = status;
	}
	@Override
	public Integer value() {
		return status;
	}

}
