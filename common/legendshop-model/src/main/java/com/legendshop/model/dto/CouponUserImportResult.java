package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CouponUserImportResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	
	private List<String> result=new ArrayList<String>(); 
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getResult() {
		return result;
	}
	public void setResult(List<String> result) {
		this.result = result;
	}
	
}
