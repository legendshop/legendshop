package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * APP首页装修
 */
@Entity
@Table(name = "ls_app_decorate")
public class AppDecorate implements GenericEntity<Long> {

	private static final long serialVersionUID = 8179747689022557915L;

	/** ID */
	private Long id; 
		
	/**  */
	private Long shopId; 
		
	/** 主题样式 */
	private String decorateCss; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 主题数据 */
	private String decotateData; 
		
	
	public AppDecorate() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "APP_DECORATE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "decorate_css")
	public String  getDecorateCss(){
		return decorateCss;
	} 
		
	public void setDecorateCss(String decorateCss){
			this.decorateCss = decorateCss;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "decotate_data")
	public String  getDecotateData(){
		return decotateData;
	} 
		
	public void setDecotateData(String decotateData){
			this.decotateData = decotateData;
		}
	


} 
