/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

/**
 * LegendShop 常量.
 */
public class Constants implements AttributeKeys {
	
	/** 活动结束 */
	public static final String ACTIVITY_END = "activity_end";
    /**  USER_UNIONID **/
    public static final String USER_UNIONID = "USER_UNIONID";
	
	/** 激活码错误 */
	public static final String ACTIVATE_CODE_ERROR = "actiError";

	/** 购物车. */
	public static final String BASKET_KEY = "BASKET_KEY";

	/** 当前购物车里面商品数量. */
	public static final String BASKET_COUNT = "BASKET_COUNT";
	
	/** 当前购物车里面总的商品数量. */
	public static final String BASKET_TOTAL_COUNT = "BASKET_TOTAL_COUNT";
	
	/** 购物车最大数量. */
	public static final int MAX_BASKET_SIZE = 99;

	/** The Constant PRODUCT_LESS. */
	public static final String SAVE_TO_CART_STATUS = "status";
	
	/** 错误的验证码代码. */
	public static final String ERROR_CODE = "errorCode";
	
	/** 错误的安全验证码. */
	public static final String ERROR_SECURITY_CODE = "errorSecurityCode";
	
	/** The Constant errorCode. */
	public static final String ERR_MSG = "errMsg";
	

	/** The Constant BASKET_TOTAL_CASH. */
	public static final String BASKET_TOTAL_CASH = "BASKET_TOTAL_CASH";

	/** The Constant BASKET_HW_COUNT. */
	public static final String BASKET_HW_COUNT = "BASKET_HW_COUNT";

	/** The Constant BASKET_HW_ATTR. */
	public static final String BASKET_HW_ATTR = "BASKET_HW_ATTR";

	/** The Constant CLUB_COOIKES_NAME. */
	public static final String CLUB_COOIKES_NAME = "jforumUserInfo";
	
	/** Login status **/
	public static final String LOGIN_STATUS = "LS";

	/** The Constant REG_USER_EXIST. */
	public static final Integer REG_USER_EXIST = 1;

	/** The Constant REG_EMAIL_EXIST. */
	public static final Integer REG_EMAIL_EXIST = 2;

	/** The Constant REG_USER_EMAIL_EXIST. */
	public static final Integer REG_USER_EMAIL_EXIST = 3;

	/** The Constant REG_CHECK_NORMAL. */
	public static final Integer REG_CHECK_NORMAL = 0;

	/** The Constant PRODUCTTYPE_HW. */
	public static final Short PRODUCTTYPE_HW = 1;

	/** The Constant PRODUCTTYPE_NEWS. */
	public static final Short PRODUCTTYPE_NEWS = 2;

	/** The Constant PRODUCTTYPE_SHOP. */
	public static final Short PRODUCTTYPE_SHOP = 3;

	public static final Integer TEMPLATE_ATTRIBUTE = 1;

	/** The Constant ORDER_STATUS. */
	public static final String ORDER_STATUS = "ORDER_STATUS";

	/** The Constant TOKEN. */
	public static final String TOKEN = "SESSION_TOKEN"; // 放在session中的token
	
	/** The Constant TOKEN. */
	public static final String INTEGRAL_TOKEN = "INTEGRAL_SESSION_TOKEN"; // 放在session中的token
	

	/** The Constant LOGONED_COMMENT. */
	public static final String LOGONED_COMMENT = "LOGONED_COMMENT";

	/** The Constant ANONYMOUS_COMMENT. */
	public static final String ANONYMOUS_COMMENT = "ANONYMOUS_COMMENT";

	/** The Constant NO_COMMENT. */
	public static final String NO_COMMENT = "NO_COMMENT";

	/** The Constant BUYED_COMMENT. */
	public static final String BUYED_COMMENT = "BUYED_COMMENT";
	
	/** The Constant BUYED_COMMENT. */
	public static final String PASSWORD_CALLBACK = "PASSWORD_CALLBACK";

	/**
	 * 操作失败
	 */
	public static final String FAIL = "fail";
	
	/**
	 * 操作受限，例如发短信过于频繁
	 */
	public static final String LIMIT = "limit";
	
	public static final String MAIL_EXISTS = "MAIL_EXISTS";
	
	/**
	 * 操作成功
	 */
	public static final String SUCCESS = "OK";
	
	/**
	 * 需要登录
	 */
	public static final String USER_LOGIN_REQUIRED = "LR";
	
	/**
	 * 用户已经回复
	 */
	public static final String USER_REPLYED = "UR";
	
	/**
	 * 最大限制数目
	 */
	public static final String MAX_NUM = "MAX";

	// 动态模版，商品动态属性和参数
	public static final String DYNAMIC_TYPE = "DYNAMIC_TYPE";

	// 动态属性和参数的JSON string
	public static final String DYN_TEMP_JSON = "dynTempJson";
	
	//菜单
	public static final String MENU_LIST = "MENU_LIST";
	
	//URL
	public static final String URL = "URL";
	
	//平邮
	public static final String TRANSPORT_MAIL = "mail";
	
	//快递
	public static final String TRANSPORT_EXPRESS = "express";
	
	//ems
	public static final String TRANSPORT_EMS = "ems";
	
	//通行证
	public static String PASSPORT = "passport";
	
	public static final String USER_STATUS_ENABLE = "1"; //用户正常状态
	
	public static final String USER_STATUS_DISABLE = "0"; //用户无效状态，不可使用
	
	//请删除该类型对应的商品
	public static final String PROD_EXISTS_ERROR = "PROD"; 
	
	//请删除该类型对应的二级商品类型
	public static final String SUBSORT_EXISTS_ERROR = "SUBSORT";

	/**用户最大收货地址数*/
	public static final int MAX_USER_ADDRESS_COUNT = 20;

	public static final Integer YES =  1; 
	
	public static final Integer NO =  0;

	public static final Long PRODUCT_CATEGORY_ROOT =  0l; 

	/** 在导航栏显示的分类  **/
	public static final Integer CATEGORY_HEADER_MENU = 1;

    public static final String SOLR_OPERATION_QUERY = "query";
    
    public static final String SOLR_OPERATION_CREATE = "create";
    
    /** 操作成功状态  **/
    public static final Integer SUCCESS_STATUS = 1;
    
    /** 操作失败状态  **/
    public static final Integer ERROR_STATUS = 0;
    
    /** 没有登录状态  **/
    public static final Integer NO_LOGIN = -1;
    
    /** 按照天数计算  **/
    public static final Integer CALCULATION_DAYS = 0;
    
    /** 按照月份计算  **/
    public static final Integer CALCULATION_MONTH = 1;
    
    /** 按照年计算  **/
    public static final Integer CALCULATION_YEAR = 2;
    
    /** 按照周计算  **/
    public static final Integer CALCULATION_WEEK = 3;

    /**已经存在*/
	public static final String EXIST = "EXIST";
	
	/** 非法数据或空数据 **/
	public static final String ILLEGAL_DATA = "illegalData";
	
	/** 未知错误 **/
	public static final String UNKNOWN_ERROR = "unknownError";
	
	/** 数据找不到 **/
	public static final String NOT_FOUND = "notFind";
	
	public static final String _MD5 = "@$%ETYDTY^#$@##%$L";
	
	/** 上传文件大小 1M 单位B */
	public static final Double _1M= 1024*1024D;

	
	/** 上传文件大小 5M 单位B */
	public static final Double _5M= 1024*1024*5D;
	
	public static final String MESSAGE = "message";
	
	/**  微信的openId **/
	public static final String USER_OPENID = "USER_OPENID";
	
	/**  app授权版本号 **/
	public static final String APP_VERSION = "V1.0";
	
	public static final String LAST_USERNAME_KEY = "LAST_LOGINING_USERNAME";

	public static final String LAST_SHOPNAME_KEY = "LAST_LOGINING_SHOPNAME";
	
	/** 图片格式错误 **/
	public static final String PHOTO_FORMAT_ERR = "FORMAT_ERR";
	
	public static final String PC_DOMAIN_NAME = "PC_DOMAIN_NAME";
	
	public static final String MOBILE_DOMAIN_NAME = "MOBILE_DOMAIN_NAME";
	
	/** spring security的用户名,为了兼容3.2版本 **/
    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "j_username";
    
	/** spring security的密码,为了兼容3.2版本  **/
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "j_password";

    /** 用于APP下单表单重复提交 */
	public static final String APP_FROM_SESSION_TOKEN = "APP_FROM_SESSION_TOKEN";

	/** APP下单表单重复提交失效时间 */
	public static final int APP_FROM_SESSION_TOKEN_EXPIRE = 60 * 10;
}
