package com.legendshop.model.dto;

import java.io.Serializable;

public class SalesRankDto implements Serializable{

	private static final long serialVersionUID = -668480486607090676L;

	private Long prodId;
	
	private String prodName;
	
	private Integer buys;
	
	private Double prodCash;
	
	private String prodPic;
	
	private Double totalCash;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Integer getBuys() {
		return buys;
	}

	public void setBuys(Integer buys) {
		this.buys = buys;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Double getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}

	public Double getProdCash() {
		return prodCash;
	}

	public void setProdCash(Double prodCash) {
		this.prodCash = prodCash;
	}
	
	
}
