package com.legendshop.model;

import java.util.List;
import java.util.Set;

import org.javia.arity.SyntaxException;

import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.model.constant.TransTypeEnum;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.util.AppUtils;

/**
 * 
 * 运费模板
 * 已经不再被使用
 */
@Deprecated
public class TransportUtil {

	
	/**
	 * 获取运费规则[1、符合城市地址的运费规则 2、默认的运费规则]
	 * @param transport
	 * @param userCityId
	 * @param trans_type
	 * @return
	 */
    public static Transfee getTransfeeDto(Transport transport,Integer userCityId,String trans_type){
		
		/*TransfeeDto transfeeDto=new TransfeeDto();*/
		Transfee defaultransfee=null;
		Transfee currentTransfee=null;
		
		if(FreightModeEnum.TRANSPORT_MAIL.value().equals(trans_type)){  //EMS快递方式
			
			/**
			 * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元)
			 * 广州：首重 10(kg) 首费5(元) 续重1(kg) 续费1(元)
			 * 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
			 */
			List<Transfee> mailList=transport.getMailList();
			for (int k = 0; k < mailList.size(); k++) {
				Transfee transfee2=mailList.get(k);
				Set<Integer> citys=transfee2.getCityList();
				if(AppUtils.isBlank(citys)){
					//默认方式
					defaultransfee=transfee2;
				}
				
				if(AppUtils.isNotBlank(citys) && citys.contains(userCityId) ){ //找到符合地址的运送模版
					currentTransfee=transfee2;
					break;
				}
			}
			
		}else if(FreightModeEnum.TRANSPORT_EXPRESS.value().equals(trans_type)){

			/**
			 * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元)
			 * 广州：首重 10(kg) 首费5(元) 续重1(kg) 续费1(元)
			 * 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
			 */
			List<Transfee> expressList=transport.getExpressList();
			for (int k = 0; k < expressList.size(); k++) {
				Transfee transfee2=expressList.get(k);
				Set<Integer> citys=transfee2.getCityList();
				if(AppUtils.isBlank(citys)){
					//默认方式
					defaultransfee=transfee2;
				}
				if(AppUtils.isNotBlank(citys) && citys.contains(userCityId) ){ //找到符合地址的运送模版
					currentTransfee=transfee2;
					break;
				}
				
			}
			
		}else if(FreightModeEnum.TRANSPORT_EMS.value().equals(trans_type)){
			/**
			 * 默认：首重 10(kg) 首费6(元) 续重1(kg) 续费1(元)
			 * 广州：首重 10(kg) 首费5(元) 续重1(kg) 续费1(元)
			 * 深圳：首重 10(kg) 首费4(元) 续重1(kg) 续费1(元)
			 */
			List<Transfee> emsList=transport.getEmsList();
			for (int k = 0; k < emsList.size(); k++) {
				Transfee transfee2=emsList.get(k);
				Set<Integer> citys=transfee2.getCityList();
				if(AppUtils.isBlank(citys)){
					//默认方式
					defaultransfee=transfee2;
				}
				if(AppUtils.isNotBlank(citys) && citys.contains(userCityId) ){ //找到符合地址的运送模版
					currentTransfee=transfee2;
					break;
				}
			}
			
		}
		if(currentTransfee==null)
			currentTransfee=defaultransfee;
		defaultransfee=null;
	   return currentTransfee;
	}
    
    
//	/**
//	 * 计算运费金额 TODO 暂时没用上
//	 * @param transType 配送方式[ems ,mail ....]
//	 * @param transfee 配送规则实体
//	 * @param totalWeight 总重量
//	 * @param totalVolume 总体积
//	 * @param totalCount 总数
//	 * @return
//	 */
//	public static  double clacCartDeleivery(String transType,Transfee transfee, double totalWeight,double totalVolume,double totalCount){
//		  ArithmeticEngine arithmeticEngine = ArithmeticEngine.getInstance();
//		  double first = transfee.getTransWeight().doubleValue(); // 首件
//		  double firstPrice = transfee.getTransFee().doubleValue();// 首费;
//		  double xujian = transfee.getTransAddWeight().doubleValue();//续件
//		  double xujian_price = transfee.getTransAddFee().doubleValue();//续费
//		  double price=0.0d;
//		  try {
//			if (TransTypeEnum.NUMBER.value().equals(transType)) {
//				price= arithmeticEngine.clacStanderd(first,xujian,firstPrice,xujian_price,totalCount);
//			} else if (TransTypeEnum.WEIGHT.value().equals(transType)) {
//
//				price= arithmeticEngine.clacStanderd(first,xujian,firstPrice,xujian_price,totalWeight);
//			} else if (TransTypeEnum.VOLUME.value().equals(transType)) {
//				price= arithmeticEngine.clacStanderd(first,xujian,firstPrice,xujian_price,totalVolume);
//			}
//		 return price;
//		} catch (SyntaxException e) {
//			e.printStackTrace();
//			 return 0.0d;
//		}
//	}
//
	

    
    
}
