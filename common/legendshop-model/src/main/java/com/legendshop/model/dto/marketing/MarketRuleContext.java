package com.legendshop.model.dto.marketing;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 * 营销活动的相互排斥的关系
 * @author newway
 *
 */
public class MarketRuleContext implements Serializable {

	private static final long serialVersionUID = -3362050343970765484L;
	
	private Map<String, Boolean> runtimeEnv = new HashMap<String, Boolean>();
	
	private static final String EXECU_MJ="EXECU_MJ_"; //满减
	
	private static final String EXECU_MZ="EXECU_MZ_";//满折
	
	private static final String EXECU_XS="EXECU_XS_";//限时
	
	private static final String EXECU_MFY="EXECU_MFY_";  // 满金额 包邮 
	
	private static final String EXECU_MFJ="EXECU_MFJ_";  //满件 包邮
	
	
	//满件 包邮
	public void executeManJianFull(Long prodId, Long skuId){
		runtimeEnv.put(EXECU_MFJ +  prodId +"_"+ skuId, true);
	}
	
	// 满金额 包邮
	public void executeManYuanFull(Long prodId, Long skuId) {
		runtimeEnv.put(EXECU_MFY + prodId + "_" + skuId, true);
	}
	
	// 满元包邮是否执行过
	public boolean isManYuanFullExecuted(Long prodId, Long skuId) {
		Boolean result = runtimeEnv.get(EXECU_MFY + prodId + "_" + skuId);
		return result != null ? result : false;
	}
	
	// 满件包邮是否执行过
	public boolean isManJianFullExecuted(Long prodId, Long skuId) {
		Boolean result = runtimeEnv.get(EXECU_MFJ + prodId + "_" + skuId);
		return result != null ? result : false;
	}
	
	
	//满减
	public void executeManJian(Long prodId, Long skuId){
		runtimeEnv.put(EXECU_MJ +  prodId +"_"+ skuId, true);
	}
	
	//满减是否执行过
	public boolean isManJianExecuted(Long prodId, Long skuId){
		Boolean result = runtimeEnv.get(EXECU_MJ +  prodId +"_"+ skuId );
		return result != null?result:false;
	}
	
	
	//限时折扣
	public void executeXianShi(Long prodId, Long skuId){
		runtimeEnv.put(EXECU_XS +  prodId +"_"+ skuId, true);
	}
	
	
	//限时折扣是否执行过
	public boolean isXianShiExecuted(Long prodId, Long skuId){
		Boolean result = runtimeEnv.get(EXECU_XS +  prodId +"_"+ skuId );
		return result != null?result:false;
	}
	
	//满折
	public void executeManZe(Long prodId, Long skuId){
		runtimeEnv.put(EXECU_MZ +  prodId +"_"+ skuId, true);
	}
	
	//限时折扣是否执行过
	public boolean isManZeExecuted(Long prodId, Long skuId){
		Boolean result = runtimeEnv.get(EXECU_MZ +  prodId +"_"+ skuId );
		return result != null?result:false;
	}

	/**
	 * 返回对应的商品
	 * @param marketingProdDtos
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	public MarketingProdDto filterMarketing(List<MarketingProdDto> marketingProdDtos ,Long prodId,Long skuId){
		for (Iterator<MarketingProdDto> iterator = marketingProdDtos.iterator(); iterator.hasNext();) {
			MarketingProdDto marketingProdDto = (MarketingProdDto) iterator.next();
			if(marketingProdDto.getProdId().equals(prodId) && marketingProdDto.getSkuId().equals(skuId) ){
				return marketingProdDto;
			}
		}
		return null;
	}
	

}
