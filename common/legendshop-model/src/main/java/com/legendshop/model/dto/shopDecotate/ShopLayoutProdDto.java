/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import com.legendshop.dao.support.GenericEntity;

/**
 * 自定义商品.
 */
public class ShopLayoutProdDto  implements GenericEntity<Long>{

	private static final long serialVersionUID = -509263071557869399L;

	/** 商品布局Id. */
	private Long layoutProdId;
	
	/** 商品Id. */
	private Long prodId;
	
	/** 商品名称. */
	private String prodName;
	
	/** 图片. */
	private String pic;
	
	/** 现金价格. */
	private Double cash;
	
	/** 顺序. */
	private Integer seq;
	
	/** 商家Id. */
	private Long shopId;
	
	/** 店铺装修Id. */
	private Long shopDecotateId;
	
	/** 布局Id. */
	private Long layoutId;
	

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the new cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	
	

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Override
	public Long getId() {
		return layoutProdId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Long id) {
        this.layoutProdId=id;		
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * Gets the layout prod id.
	 *
	 * @return the layout prod id
	 */
	public Long getLayoutProdId() {
		return layoutProdId;
	}

	/**
	 * Sets the layout prod id.
	 *
	 * @param layoutProdId the new layout prod id
	 */
	public void setLayoutProdId(Long layoutProdId) {
		this.layoutProdId = layoutProdId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop decotate id.
	 *
	 * @return the shop decotate id
	 */
	public Long getShopDecotateId() {
		return shopDecotateId;
	}

	/**
	 * Sets the shop decotate id.
	 *
	 * @param shopDecotateId the new shop decotate id
	 */
	public void setShopDecotateId(Long shopDecotateId) {
		this.shopDecotateId = shopDecotateId;
	}

	/**
	 * Gets the layout id.
	 *
	 * @return the layout id
	 */
	public Long getLayoutId() {
		return layoutId;
	}

	/**
	 * Sets the layout id.
	 *
	 * @param layoutId the new layout id
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}
	

}
