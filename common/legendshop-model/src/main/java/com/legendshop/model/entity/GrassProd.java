package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *种草文章关联商品表
 */
@Entity
@Table(name = "ls_grass_prod")
public class GrassProd implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 种草文章id */
	private Long grarticleId; 
		
	/** 商品id */
	private Long prodId; 
		
	
	public GrassProd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GRASS_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "grarticle_id")
	public Long  getGrarticleId(){
		return grarticleId;
	} 
		
	public void setGrarticleId(Long grarticleId){
			this.grarticleId = grarticleId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
	


} 
