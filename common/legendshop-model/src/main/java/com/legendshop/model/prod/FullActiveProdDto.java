package com.legendshop.model.prod;

import java.io.Serializable;


/**
 * 包邮的商品
 */
public class FullActiveProdDto implements Serializable{

	private static final long serialVersionUID = 8603339400272532192L;
	
	//商品ID
	private Long prodId;
	
	//商品名称
	private String prodName;
	
	//商品图片
	private String pic;
	
	//商品实际库存
	private Integer actualStocks; 
	
	//商品销售价格
	private Double price;
	
	//包邮活动表的ID，如果为空，证明没有参加包邮活动
	private Integer activeId;
	
	//编辑自己的已选择
	private Integer hook;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Integer actualStocks) {
		this.actualStocks = actualStocks;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getActiveId() {
		return activeId;
	}

	public void setActiveId(Integer activeId) {
		this.activeId = activeId;
	}

	public Integer getHook() {
		return hook;
	}

	public void setHook(Integer hook) {
		this.hook = hook;
	}

	
}






