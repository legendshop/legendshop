/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;
import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopNav;

/**
 * 布局模块.
 */
public class ShopLayoutModuleDto implements Serializable {

	private static final long serialVersionUID = -5360155062150736795L;

	/**  布局ID. */
	private Long layoutId; 
		
	/**  商家ID. */
	private Long shopId; 
		
	/**  商家店铺装修ID. */
	private Long shopDecotateId; 
	
	/**  商家导航. */
	private List<ShopNav> shopNavs;
	
	/** 导航轮播图. */
	private List<ShopLayoutBanner> shopLayoutBanners;
	
	
	/**  自定义模块内容. */
	private String layoutContent; 
	
	/** 商家分类信息. */
	private  List<ShopLayoutCateogryDto> cateogryDtos;
	
	/** 店铺热销. */
	private List<ShopLayoutHotProdDto> hotProds;
	
	/** 店铺信息. */
	private  ShopLayoutShopInfoDto shopInfo;
	
	/** 商品自定义. */
	private List<ShopLayoutProdDto> layoutProdDtos;
	
	
	/** 热点模块. */
	private ShopLayoutHotDto shopLayoutHotDto;
	
	/**
	 * Gets the layout id.
	 *
	 * @return the layout id
	 */
	public Long getLayoutId() {
		return layoutId;
	}

	/**
	 * Sets the layout id.
	 *
	 * @param layoutId the new layout id
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop decotate id.
	 *
	 * @return the shop decotate id
	 */
	public Long getShopDecotateId() {
		return shopDecotateId;
	}

	/**
	 * Sets the shop decotate id.
	 *
	 * @param shopDecotateId the new shop decotate id
	 */
	public void setShopDecotateId(Long shopDecotateId) {
		this.shopDecotateId = shopDecotateId;
	}

	/**
	 * Gets the layout content.
	 *
	 * @return the layout content
	 */
	public String getLayoutContent() {
		return layoutContent;
	}

	/**
	 * Sets the layout content.
	 *
	 * @param layoutContent the new layout content
	 */
	public void setLayoutContent(String layoutContent) {
		this.layoutContent = layoutContent;
	}

	/**
	 * Gets the shop navs.
	 *
	 * @return the shop navs
	 */
	public List<ShopNav> getShopNavs() {
		return shopNavs;
	}

	/**
	 * Sets the shop navs.
	 *
	 * @param shopNavs the new shop navs
	 */
	public void setShopNavs(List<ShopNav> shopNavs) {
		this.shopNavs = shopNavs;
	}

	/**
	 * Gets the shop layout banners.
	 *
	 * @return the shop layout banners
	 */
	public List<ShopLayoutBanner> getShopLayoutBanners() {
		return shopLayoutBanners;
	}

	/**
	 * Sets the shop layout banners.
	 *
	 * @param shopLayoutBanners the new shop layout banners
	 */
	public void setShopLayoutBanners(List<ShopLayoutBanner> shopLayoutBanners) {
		this.shopLayoutBanners = shopLayoutBanners;
	}

	/**
	 * Gets the cateogry dtos.
	 *
	 * @return the cateogry dtos
	 */
	public List<ShopLayoutCateogryDto> getCateogryDtos() {
		return cateogryDtos;
	}

	/**
	 * Sets the cateogry dtos.
	 *
	 * @param cateogryDtos the new cateogry dtos
	 */
	public void setCateogryDtos(List<ShopLayoutCateogryDto> cateogryDtos) {
		this.cateogryDtos = cateogryDtos;
	}

	/**
	 * Gets the hot prods.
	 *
	 * @return the hot prods
	 */
	public List<ShopLayoutHotProdDto> getHotProds() {
		return hotProds;
	}

	/**
	 * Sets the hot prods.
	 *
	 * @param hotProds the new hot prods
	 */
	public void setHotProds(List<ShopLayoutHotProdDto> hotProds) {
		this.hotProds = hotProds;
	}

	/**
	 * Gets the shop info.
	 *
	 * @return the shop info
	 */
	public ShopLayoutShopInfoDto getShopInfo() {
		return shopInfo;
	}

	/**
	 * Sets the shop info.
	 *
	 * @param shopInfo the new shop info
	 */
	public void setShopInfo(ShopLayoutShopInfoDto shopInfo) {
		this.shopInfo = shopInfo;
	}

	/**
	 * Gets the layout prod dtos.
	 *
	 * @return the layout prod dtos
	 */
	public List<ShopLayoutProdDto> getLayoutProdDtos() {
		return layoutProdDtos;
	}

	/**
	 * Sets the layout prod dtos.
	 *
	 * @param layoutProdDtos the new layout prod dtos
	 */
	public void setLayoutProdDtos(List<ShopLayoutProdDto> layoutProdDtos) {
		this.layoutProdDtos = layoutProdDtos;
	}

	/**
	 * Gets the shop layout hot dto.
	 *
	 * @return the shop layout hot dto
	 */
	public ShopLayoutHotDto getShopLayoutHotDto() {
		return shopLayoutHotDto;
	}

	/**
	 * Sets the shop layout hot dto.
	 *
	 * @param shopLayoutHotDto the new shop layout hot dto
	 */
	public void setShopLayoutHotDto(ShopLayoutHotDto shopLayoutHotDto) {
		this.shopLayoutHotDto = shopLayoutHotDto;
	} 
	
}
