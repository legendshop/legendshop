package com.legendshop.model.dto;

/**
 *移动端基础设置
 */
public class AppSetting  {
	
	/** 风格主题名称 */
	private String theme; 
		
	/** 主题颜色 */
	private String color; 
		
	/** 分类设置 */
	private String categorySetting; 
	
	
	public AppSetting() {
    }


	public String getTheme() {
		return theme;
	}


	public void setTheme(String theme) {
		this.theme = theme;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getCategorySetting() {
		return categorySetting;
	}


	public void setCategorySetting(String categorySetting) {
		this.categorySetting = categorySetting;
	}

} 
