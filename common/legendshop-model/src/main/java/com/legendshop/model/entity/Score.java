/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * --------------------------------------------
 * ----------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ------------------
 * ------------------------------------------------------------------.
 */
@Entity
@Table(name = "ls_score")
public class Score implements GenericEntity<Long> {

	private static final long serialVersionUID = 7442720488698346761L;

	/** The score id. */
	private Long scoreId;

	/** The sub id. */
	private Long subId;

	/** The score. */
	private Integer score;

	/** The score type. */
	private String scoreType;

	/** The user name. */
	private String userName;

	/** The rec date. */
	private Date recDate;

	// Constructors

	/**
	 * default constructor.
	 */
	public Score() {
	}

	/**
	 * Gets the score id.
	 * 
	 * @return the score id
	 */
	@Id
	@Column(name = "score_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SCORE_SEQ")
	public Long getScoreId() {
		return scoreId;
	}

	/**
	 * Sets the score id.
	 * 
	 * @param scoreId
	 *            the new score id
	 */
	public void setScoreId(Long scoreId) {
		this.scoreId = scoreId;
	}

	/**
	 * Gets the sub id.
	 * 
	 * @return the sub id
	 */
	@Column(name = "sub_id")
	public Long getSubId() {
		return subId;
	}

	/**
	 * Sets the sub id.
	 * 
	 * @param subId
	 *            the new sub id
	 */
	public void setSubId(Long subId) {
		this.subId = subId;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the new score
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * Gets the score type.
	 * 
	 * @return the score type
	 */
	@Column(name = "score_type")
	public String getScoreType() {
		return scoreType;
	}

	/**
	 * Sets the score type.
	 * 
	 * @param scoreType
	 *            the new score type
	 */
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	/**
	 * Gets the rec date.
	 * 
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 * 
	 * @param recDate
	 *            the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Transient
	public Long getId() {
		return scoreId;
	}
	
	public void setId(Long id) {
		this.scoreId = id;
	}

}