package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 预存款账户明细记录Dto
 */
@ApiModel(value="预存款账户明细记录Dto") 
public class AppPdCashLogDto implements Serializable{

	/**  */
	private static final long serialVersionUID = 4037667860445255191L;

	/** 主键 */
	@ApiModelProperty(value = "主键id")
	private Long id; 
		
	/** 用户id */
	@ApiModelProperty(value = "用户id")
	private String userId; 
		
	/** 用户名 */
	@ApiModelProperty(value = "用户名")
	private String userName; 
		
	/** 交易流水号 */
	@ApiModelProperty(value = "交易流水号")
	private String sn; 
		
	/** order_pay日记类型 PdCashLogEnum */
	@ApiModelProperty(value = "日记类型：[order_freeze 下单支付被冻结,  auctions_freeze 冻结 , order_finally 下单, recharge 充值, withdraw 提现, commission 佣金收入, promoter 退还保证金,refund 退款 , auctions_deduct 扣除保证金] ")
	private String logType;

	/** order_pay日记类型名称 PdCashLogEnum */
	@ApiModelProperty(value = "日记类型名称")
	private String logTypeName;
		
	/** 金额变更 */
	@ApiModelProperty(value = "金额变更 ")
	private Double amount; 
		
	/** 描述 */
	@ApiModelProperty(value = "描述")
	private String logDesc; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间 ")
	private Date addTime;
	

	public AppPdCashLogDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getLogDesc() {
		return logDesc;
	}

	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getLogTypeName() {
		return logTypeName;
	}

	public void setLogTypeName(String logTypeName) {
		this.logTypeName = logTypeName;
	}
}
