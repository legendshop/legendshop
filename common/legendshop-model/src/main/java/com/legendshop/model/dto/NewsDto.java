
package com.legendshop.model.dto;

import com.legendshop.dao.support.GenericEntity;

/**
 * 文章DTO
 *
 */
public class NewsDto  implements GenericEntity<Long> {

	private static final long serialVersionUID = 8245381133760748434L;

	private Long newsId;
	
	private String newsTitle;

	public NewsDto() {
	}
	public NewsDto(Long newsId, String newsTitle) {
		super();
		this.newsId = newsId;
		this.newsTitle = newsTitle;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	@Override
	public Long getId() {
		return newsId;
	}
	@Override
	public void setId(Long id) {
		this.newsId = id;
		
	}
}
