package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.util.AppUtils;
/**
 * 
 *商品优惠券
 */
public class ProductCoupon implements Serializable{
	
	private static final long serialVersionUID = 7313362676981238371L;
	
	/** 用户礼券ID */
	private Long userCouponId; 
		
	/** 礼券ID */
	private Long couponId; 
		
	/** 礼券名称 */
	private String couponName; 
	
	/** 劵号 */
	private String couponSn; 
	
	/** 卡密 */
	private String couponPwd; 
		
	/** 用户ID */
	private String userId; 
		
	/** 用户名称 */
	private String userName; 
		
	/** 店铺ID**/
	private Long shopId;
	
	/** 劵值满多少金额 */
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	private Double offPrice; 
		
	/** 优惠券图片 **/
	private String couponPic;
	
	/** 店铺名称 **/
	private String siteName;
	
	private Long prodId;
	
	/** 结束时间*/
	private Date endDate;
	
	/** 是否选中*/
	private int selectSts = 0;
	
	/** 商品优惠券的 关联商品*/
	private List<Long> prodIdList = new ArrayList<Long>();

	/**
	 * 是否包含该商品
	 * @param prodId
	 * @return
	 */
	public boolean containProd(Long prodId){
		if(AppUtils.isNotBlank(prodIdList)){
			for (Long productId:prodIdList) {
				if(productId.equals(prodId)){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null){
			return false;
		}
		if(getClass() != obj.getClass()){
			return false;
		}
		ProductCoupon prodCoupon = (ProductCoupon)obj;
		return this.userCouponId.equals(prodCoupon.userCouponId);
	}

	public Long getUserCouponId() {
		return userCouponId;
	}

	public void setUserCouponId(Long userCouponId) {
		this.userCouponId = userCouponId;
	}

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponSn() {
		return couponSn;
	}

	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}

	public String getCouponPwd() {
		return couponPwd;
	}

	public void setCouponPwd(String couponPwd) {
		this.couponPwd = couponPwd;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Double getOffPrice() {
		return offPrice;
	}

	public void setOffPrice(Double offPrice) {
		this.offPrice = offPrice;
	}

	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public List<Long> getProdIdList() {
		return prodIdList;
	}

	public void setProdIdList(List<Long> prodIdList) {
		this.prodIdList = prodIdList;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getSelectSts() {
		return selectSts;
	}

	public void setSelectSts(int selectSts) {
		this.selectSts = selectSts;
	}

}
