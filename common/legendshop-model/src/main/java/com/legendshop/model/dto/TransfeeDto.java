
package com.legendshop.model.dto;

import java.io.Serializable;
import java.text.NumberFormat;

import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 配送方式.
 *
 * @author tony
 */
@ApiModel("配送方式")
public class TransfeeDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7236491929810542908L;

	/** 运费. */
	@ApiModelProperty(value = "运费")
	private Double deliveryAmount;
	
	/** 运费模板. */
	@ApiModelProperty(value = "运费模板")
	private String freightMode;

	/** 描述. */
	@ApiModelProperty(value = "描述")
	private String desc;
	
	public TransfeeDto(){}

	public TransfeeDto(Double deliveryAmount, String freightMode) {
		super();
		this.deliveryAmount = deliveryAmount;
		this.freightMode = freightMode;
	}

	/**
	 * Gets the delivery amount.
	 *
	 * @return the delivery amount
	 */
	public Double getDeliveryAmount() {
		return deliveryAmount;
	}

	/**
	 * Sets the delivery amount.
	 *
	 * @param deliveryAmount the new delivery amount
	 */
	public void setDeliveryAmount(Double deliveryAmount) {
		this.deliveryAmount = deliveryAmount;
	}

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		NumberFormat nf = NumberFormat.getNumberInstance();  
        nf.setMaximumFractionDigits(2);
		StringBuilder sb = new StringBuilder();
		if(AppUtils.isNotBlank(freightMode)){
			if(FreightModeEnum.TRANSPORT_MAIL.value().equals(freightMode)){
				sb.append("平邮[").append(nf.format(deliveryAmount)).append("元]");
			}else if(FreightModeEnum.TRANSPORT_EXPRESS.value().equals(freightMode)){
				sb.append("快递[").append(nf.format(deliveryAmount)).append("元]");
			}else if(FreightModeEnum.TRANSPORT_EMS.value().equals(freightMode)){
				sb.append("EMS[").append(nf.format(deliveryAmount)).append("元]");
			}
		}else{
			sb.append("免邮");
		}
		return sb.toString();
	}


	/**
	 * Gets the freight mode.
	 *
	 * @return the freight mode
	 */
	public String getFreightMode() {
		return freightMode;
	}

	/**
	 * Sets the freight mode.
	 *
	 * @param freightMode the new freight mode
	 */
	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((freightMode == null) ? 0 : freightMode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransfeeDto other = (TransfeeDto) obj;
		if (freightMode == null) {
			if (other.freightMode != null)
				return false;
		} else if (!freightMode.equals(other.freightMode))
			return false;
		return true;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
