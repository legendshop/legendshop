/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import java.io.Serializable;

/**
 * 我的推广 返回 app的参数
 *
 */
public class AppMyPromoterDto implements Serializable{

	private static final long serialVersionUID = -3353629992593301259L;
	
	/** 累积赚得佣金. */
	private Double totalDistCommis; 
		
	/** 直接下级会员贡献佣金. */
	private Double firstDistCommis; 
	
	/** 下两级会员贡献佣金. */
	private Double secondDistCommis; 
		
	/** 下三级会员贡献佣金. */
	private Double thirdDistCommis; 
		
	/** 已结算佣金. */
	private Double settledDistCommis; 
	
	/** 未结算佣金. */
	private Double unsettleDistCommis; 
	
	/** 直接下级用户数量*. */
	private String firstCount;
	
	/** 下二级用户数量*. */
	private String secondCount;
	
	/** 下三级用户数量*. */
	private String thirdCount;

	/**
	 * Gets the total dist commis.
	 *
	 * @return the total dist commis
	 */
	public Double getTotalDistCommis() {
		return totalDistCommis;
	}

	/**
	 * Sets the total dist commis.
	 *
	 * @param totalDistCommis the total dist commis
	 */
	public void setTotalDistCommis(Double totalDistCommis) {
		this.totalDistCommis = totalDistCommis;
	}

	/**
	 * Gets the first dist commis.
	 *
	 * @return the first dist commis
	 */
	public Double getFirstDistCommis() {
		return firstDistCommis;
	}

	/**
	 * Sets the first dist commis.
	 *
	 * @param firstDistCommis the first dist commis
	 */
	public void setFirstDistCommis(Double firstDistCommis) {
		this.firstDistCommis = firstDistCommis;
	}

	/**
	 * Gets the second dist commis.
	 *
	 * @return the second dist commis
	 */
	public Double getSecondDistCommis() {
		return secondDistCommis;
	}

	/**
	 * Sets the second dist commis.
	 *
	 * @param secondDistCommis the second dist commis
	 */
	public void setSecondDistCommis(Double secondDistCommis) {
		this.secondDistCommis = secondDistCommis;
	}

	/**
	 * Gets the third dist commis.
	 *
	 * @return the third dist commis
	 */
	public Double getThirdDistCommis() {
		return thirdDistCommis;
	}

	/**
	 * Sets the third dist commis.
	 *
	 * @param thirdDistCommis the third dist commis
	 */
	public void setThirdDistCommis(Double thirdDistCommis) {
		this.thirdDistCommis = thirdDistCommis;
	}

	/**
	 * Gets the settled dist commis.
	 *
	 * @return the settled dist commis
	 */
	public Double getSettledDistCommis() {
		return settledDistCommis;
	}

	/**
	 * Sets the settled dist commis.
	 *
	 * @param settledDistCommis the settled dist commis
	 */
	public void setSettledDistCommis(Double settledDistCommis) {
		this.settledDistCommis = settledDistCommis;
	}

	/**
	 * Gets the unsettle dist commis.
	 *
	 * @return the unsettle dist commis
	 */
	public Double getUnsettleDistCommis() {
		return unsettleDistCommis;
	}

	/**
	 * Sets the unsettle dist commis.
	 *
	 * @param unsettleDistCommis the unsettle dist commis
	 */
	public void setUnsettleDistCommis(Double unsettleDistCommis) {
		this.unsettleDistCommis = unsettleDistCommis;
	}

	/**
	 * Gets the first count.
	 *
	 * @return the first count
	 */
	public String getFirstCount() {
		return firstCount;
	}

	/**
	 * Sets the first count.
	 *
	 * @param firstCount the first count
	 */
	public void setFirstCount(String firstCount) {
		this.firstCount = firstCount;
	}

	/**
	 * Gets the second count.
	 *
	 * @return the second count
	 */
	public String getSecondCount() {
		return secondCount;
	}

	/**
	 * Sets the second count.
	 *
	 * @param secondCount the second count
	 */
	public void setSecondCount(String secondCount) {
		this.secondCount = secondCount;
	}

	/**
	 * Gets the third count.
	 *
	 * @return the third count
	 */
	public String getThirdCount() {
		return thirdCount;
	}

	/**
	 * Sets the third count.
	 *
	 * @param thirdCount the third count
	 */
	public void setThirdCount(String thirdCount) {
		this.thirdCount = thirdCount;
	}
	
	
}
