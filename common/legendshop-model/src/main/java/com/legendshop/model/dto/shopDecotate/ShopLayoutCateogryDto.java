/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.util.List;

import com.legendshop.dao.support.GenericEntity;

/**
 * 商家布局类型Dto.
 *
 */
public class ShopLayoutCateogryDto implements GenericEntity<Long> {

	private static final long serialVersionUID = 4444779496116617658L;

	/** 产品类目ID */
	private Long id;

	/** 父节点 */
	private Long parentId;

	/** 产品类目名称 */
	private String name;

	/** 排序 */
	private Long seq;

	private ShopLayoutCateogryDto parentDto;

	private List<ShopLayoutCateogryDto> childrentDtos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public ShopLayoutCateogryDto getParentDto() {
		return parentDto;
	}

	public void setParentDto(ShopLayoutCateogryDto parentDto) {
		this.parentDto = parentDto;
	}

	public List<ShopLayoutCateogryDto> getChildrentDtos() {
		return childrentDtos;
	}

	public void setChildrentDtos(List<ShopLayoutCateogryDto> childrentDtos) {
		this.childrentDtos = childrentDtos;
	}

}
