/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * App礼券类型枚举
 */
public enum AppCouponTypeEnum implements StringEnum {

	/** 全场通用*/
	ALL_PLATFORM("allPlatform"),

	/** 店铺商品券*/
	SHOP_PROD("shopProd"),

	/** 店铺券*/
	SHOP("shop"); 

	
	/** The num. */
	private String type;

	AppCouponTypeEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}

}
