package com.legendshop.model.report;

import java.io.Serializable;

/**
 *平均订单金额
 */
public class OrdersConversionRate implements Serializable {

	private static final long serialVersionUID = 1133213159946259747L;

	// 总订单金额 
	private Double totalAmount;
	
	//总订单数
	private long totalOrder;
	
	//平均订单金额
	private Double averageAmout;
	
	/**
	 * 平均订单金额
	 * @param totalAmount
	 * @param totalOrder
	 * @param averageAmout
	 */
	public OrdersConversionRate(double totalAmount, long totalOrder, double averageAmout) {
		this.totalAmount = totalAmount;
		this.totalOrder = totalOrder;
		this.averageAmout = averageAmout;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public long getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(long totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Double getAverageAmout() {
		return averageAmout;
	}

	public void setAverageAmout(Double averageAmout) {
		this.averageAmout = averageAmout;
	}
}
