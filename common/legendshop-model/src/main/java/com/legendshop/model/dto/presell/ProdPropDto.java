/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import java.io.Serializable;
import java.util.List;

/**
 * 商品属性DTO
 */
public class ProdPropDto implements Serializable{

	private static final long serialVersionUID = -4262283197776277458L;

	/** 商品属性ID */
	private Long propId;
	
	/** 属性名称 */
	private String propName;
	
	/** 属性别名 */
	private String memo;
	
	/** 属性状态 */
	private Integer status;
	
	/** 属性类型 0:文字,1:有图片*/
	private Integer type;
	
	/** 属性规则 1:销售属性,2:参数属性,3:关键属性*/
	private Integer isRuleAttributes;
	
	/** 属性值列表 */
	private List<PropValueDto> propValueDtos;
	

	/**
	 * @return the propId
	 */
	public Long getPropId() {
		return propId;
	}

	/**
	 * @param propId the propId to set
	 */
	public void setPropId(Long propId) {
		this.propId = propId;
	}

	/**
	 * @return the propName
	 */
	public String getPropName() {
		return propName;
	}

	/**
	 * @param propName the propName to set
	 */
	public void setPropName(String propName) {
		this.propName = propName;
	}

	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return the isRuleAttributes
	 */
	public Integer getIsRuleAttributes() {
		return isRuleAttributes;
	}

	/**
	 * @param isRuleAttributes the isRuleAttributes to set
	 */
	public void setIsRuleAttributes(Integer isRuleAttributes) {
		this.isRuleAttributes = isRuleAttributes;
	}

	/**
	 * @return the propValueDtos
	 */
	public List<PropValueDto> getPropValueDtos() {
		return propValueDtos;
	}

	/**
	 * @param propValueDtos the propValueDtos to set
	 */
	public void setPropValueDtos(List<PropValueDto> propValueDtos) {
		this.propValueDtos = propValueDtos;
	}
}
