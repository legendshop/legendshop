/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.model.dto.integral;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.order.DeliveryDto;
import com.legendshop.model.dto.order.UsrAddrSubDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import springfox.documentation.annotations.ApiIgnore;


/**
 * The Class IntegralOrderDto.
 */
@ApiModel(value="积分订单Dto") 
public class IntegralOrderDto  implements GenericEntity<Long> {
	
	/** 主键id */
	@ApiModelProperty(value = "主键id")
	private Long id;
	
	/** 积分订单id. */
	@ApiModelProperty(value="唯一标识id") 
	private Long orderId;
	
	/** 订单流水号. */
	@ApiModelProperty(value="订单流水号") 
	private String orderSn;
	
	/** 用户名称. */
	@ApiModelProperty(value="用户名称") 
    private String userName;
	
    /** 用户ID. */
	@ApiModelProperty(value="用户ID") 
	private String userId;
	
	/** 订单商品总数. */
	@ApiModelProperty(value="订单商品总数") 
	private Integer productNums;
	
	/** 积分总数. */
	@ApiModelProperty(value="积分总数") 
	private Integer integralTotal;
	
	/** 配送方式. */
	@ApiModelProperty(value="配送方式") 
	private Long dvyTypeId;
	
	/** 物流单号. */
	@ApiModelProperty(value="物流单号") 
	private String  dvyFlowId;
	
	/** 0(默认):待发货;1:待收货;2:已完成;3:已取消 */
	@ApiModelProperty(value="0(默认):待发货;1:待收货;2:已完成;3:已取消 ") 
	private Integer orderStatus;
	
	/** 用户订单地址Id. */
	@ApiModelProperty(value="用户订单地址Id") 
	private Long addrOrderId;
	
	/** 添加时间. */
	@ApiModelProperty(value="添加时间") 
	private Date addTime;
	
	/** 物流时间. */
	@ApiModelProperty(value="发货时间") 
	private Date dvyTime;
	
	/** 完成时间. */
	@ApiModelProperty(value="完成时间") 
	private Date finallyTime;
	
	/** 订单备注. */
	@ApiModelProperty(value="订单备注") 
	private String orderDesc;

	/** 积分订单项列表. */
	@ApiModelProperty(value="积分订单项列表")
	private List<IntegralOrderItemDto> orderItemDtos;
	
	/** 积分订单项Dto. */
	@ApiModelProperty(value="积分订单项Dto", hidden = true)
	private IntegralOrderItemDto orderItemDto;
	
	/** 用户的订单地址. */
	@ApiModelProperty(value="用户的订单地址")
	private UsrAddrSubDto usrAddrSubDto;
	
	/** 物流信息. */
	@ApiModelProperty(value="物流信息")
	private DeliveryDto  delivery;
	
	private static final long serialVersionUID = 1L;

	@Override
	public Long getId() {
		return orderId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Long id) {
		this.orderId=id;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public Long getOrderId() {
		return orderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param orderId the order id
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/**
	 * Gets the order sn.
	 *
	 * @return the order sn
	 */
	public String getOrderSn() {
		return orderSn;
	}

	/**
	 * Sets the order sn.
	 *
	 * @param orderSn the order sn
	 */
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the product nums.
	 *
	 * @return the product nums
	 */
	public Integer getProductNums() {
		return productNums;
	}

	/**
	 * Sets the product nums.
	 *
	 * @param productNums the product nums
	 */
	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	/**
	 * Gets the integral total.
	 *
	 * @return the integral total
	 */
	public Integer getIntegralTotal() {
		return integralTotal;
	}

	/**
	 * Sets the integral total.
	 *
	 * @param integralTotal the integral total
	 */
	public void setIntegralTotal(Integer integralTotal) {
		this.integralTotal = integralTotal;
	}

	/**
	 * Gets the dvy type id.
	 *
	 * @return the dvy type id
	 */
	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	/**
	 * Sets the dvy type id.
	 *
	 * @param dvyTypeId the dvy type id
	 */
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	/**
	 * Gets the dvy flow id.
	 *
	 * @return the dvy flow id
	 */
	public String getDvyFlowId() {
		return dvyFlowId;
	}

	/**
	 * Sets the dvy flow id.
	 *
	 * @param dvyFlowId the dvy flow id
	 */
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	/**
	 * Gets the order status.
	 *
	 * @return the order status
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}

	/**
	 * Sets the order status.
	 *
	 * @param orderStatus the order status
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * Gets the addr order id.
	 *
	 * @return the addr order id
	 */
	public Long getAddrOrderId() {
		return addrOrderId;
	}

	/**
	 * Sets the addr order id.
	 *
	 * @param addrOrderId the addr order id
	 */
	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	/**
	 * Gets the add time.
	 *
	 * @return the adds the time
	 */
	public Date getAddTime() {
		return addTime;
	}

	/**
	 * Sets the add time.
	 *
	 * @param addTime the adds the time
	 */
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	/**
	 * Gets the dvy time.
	 *
	 * @return the dvy time
	 */
	public Date getDvyTime() {
		return dvyTime;
	}

	/**
	 * Sets the dvy time.
	 *
	 * @param dvyTime the dvy time
	 */
	public void setDvyTime(Date dvyTime) {
		this.dvyTime = dvyTime;
	}

	/**
	 * Gets the finally time.
	 *
	 * @return the finally time
	 */
	public Date getFinallyTime() {
		return finallyTime;
	}

	/**
	 * Sets the finally time.
	 *
	 * @param finallyTime the finally time
	 */
	public void setFinallyTime(Date finallyTime) {
		this.finallyTime = finallyTime;
	}

	/**
	 * Gets the order desc.
	 *
	 * @return the order desc
	 */
	public String getOrderDesc() {
		return orderDesc;
	}

	/**
	 * Sets the order desc.
	 *
	 * @param orderDesc the order desc
	 */
	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}

	
	/**
	 * Gets the usr addr sub dto.
	 *
	 * @return the usr addr sub dto
	 */
	public UsrAddrSubDto getUsrAddrSubDto() {
		return usrAddrSubDto;
	}

	/**
	 * Sets the usr addr sub dto.
	 *
	 * @param usrAddrSubDto the usr addr sub dto
	 */
	public void setUsrAddrSubDto(UsrAddrSubDto usrAddrSubDto) {
		this.usrAddrSubDto = usrAddrSubDto;
	}

	/**
	 * Gets the order item dtos.
	 *
	 * @return the order item dtos
	 */
	public List<IntegralOrderItemDto> getOrderItemDtos() {
		return orderItemDtos;
	}
	
	/**
	 * Adds the order item dtos.
	 *
	 * @param itemDto the item dto
	 */
	public void addOrderItemDtos(IntegralOrderItemDto itemDto){
		if(orderItemDtos==null){
			orderItemDtos=new ArrayList<IntegralOrderItemDto>();
		}
		orderItemDtos.add(itemDto);
	}

	/**
	 * Gets the order item dto.
	 *
	 * @return the order item dto
	 */
	public IntegralOrderItemDto getOrderItemDto() {
		return orderItemDto;
	}

	/**
	 * Sets the order item dto.
	 *
	 * @param orderItemDto the order item dto
	 */
	public void setOrderItemDto(IntegralOrderItemDto orderItemDto) {
		this.orderItemDto = orderItemDto;
	}

	/**
	 * Gets the delivery.
	 *
	 * @return the delivery
	 */
	public DeliveryDto getDelivery() {
		return delivery;
	}

	/**
	 * Sets the delivery.
	 *
	 * @param delivery the delivery
	 */
	public void setDelivery(DeliveryDto delivery) {
		this.delivery = delivery;
	}

}
