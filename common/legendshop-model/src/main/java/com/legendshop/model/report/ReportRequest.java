package com.legendshop.model.report;

import java.io.Serializable;
import java.util.Date;

/**
 * 销售Dto.
 */
public class ReportRequest implements Serializable {

	private static final long serialVersionUID = 1783257271077419381L;
	
	private Long shopId;

	private String userName;
	
	private Date selectedDate;
	
	private Integer queryTerms;
	
	private Integer selectedYear;
	
	private Integer selectedMonth;
	
	private String selectedWeek;
	
	private Integer status;
	
	private String shopName;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getQueryTerms() {
		return queryTerms;
	}

	public void setQueryTerms(Integer queryTerms) {
		this.queryTerms = queryTerms;
	}

	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public Integer getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(Integer selectedYear) {
		this.selectedYear = selectedYear;
	}

	public Integer getSelectedMonth() {
		return selectedMonth;
	}

	public void setSelectedMonth(Integer selectedMonth) {
		this.selectedMonth = selectedMonth;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSelectedWeek() {
		return selectedWeek;
	}

	public void setSelectedWeek(String selectedWeek) {
		this.selectedWeek = selectedWeek;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	
}
