package com.legendshop.model.dto;

import java.io.Serializable;
/**
 * 
 * 简单的返回值
 *
 */
public class SimpleResultDto implements Serializable{

	private static final long serialVersionUID = -8614022892454371917L;
	
	public static int STATUS_OK = 1;//成功
	public static int STATUS_FALIED= 0;//失败

	//返回说明
	private int statusCode = 1;
	
	//返回结果
	private Object result;
	
	public SimpleResultDto(int statusCode){
		this.statusCode = statusCode;
	}
	
	public SimpleResultDto(Object result){
		this.result = result;
	}
	
	public SimpleResultDto(int statusCode, Object result){
		this.statusCode = statusCode;
		this.result = result;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
	/**
	 * 结果是否成功
	 * @return
	 */
	public boolean success(){
		return STATUS_OK == statusCode;
	}
	
}
