/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 中标状态
 * 
 */
public enum BiddersWinStatusEnum implements IntegerEnum {
	/** 未转订单 */
	NOORDER(-1),

	/** 等待支付 */
	WAITPAY(0),
	
	/** 已支付 */
	PAYMENT(1);

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.LongEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	BiddersWinStatusEnum(Integer num) {
		this.num = num;
	}

}
