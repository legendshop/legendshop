/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.security;

import java.util.Collection;

import com.legendshop.dao.support.GenericEntity;


/**
 * 门店的实体类
 */
public class StoreEntity  implements GenericEntity<Long> {

	private static final long serialVersionUID = -2987731703146750453L;

	/** The id. */
     private Long id;
     
     /** The name. */
     private String name;
     
     /** The password. */
     private String password;
     
     /** The enabled. */
     private Boolean enabled;
     
 	private String shopUserId;
       
 	private Long shopId;
 	
 	private 	ShopUser shopUser;
 	
	/** 门店登录名 */
	private String userName; 
	
	private String shopUserName;
	
	private String shopName;
 	
 	/** 角色 **/
 	private Collection<String> roles;
 	
 	/** 权限 **/
 	private Collection<String> functions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getShopUserId() {
		return shopUserId;
	}

	public void setShopUserId(String shopUserId) {
		this.shopUserId = shopUserId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	public Collection<String> getFunctions() {
		return functions;
	}

	public void setFunctions(Collection<String> functions) {
		this.functions = functions;
	}

	public ShopUser getShopUser() {
		return shopUser;
	}

	public void setShopUser(ShopUser shopUser) {
		this.shopUser = shopUser;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShopUserName() {
		return shopUserName;
	}

	public void setShopUserName(String shopUserName) {
		this.shopUserName = shopUserName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	

}