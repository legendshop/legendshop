/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;


/**
 * The Class MsgStatus.
 */
@Entity
@Table(name = "ls_msg_status")
public class MsgStatus implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 360799176532688946L;

	/** The id. */
	private Long id;
	
	/** The user name. */
	private String userName;
	
	/** The msg id. */
	private Long msgId;
	
	/** The status. */
	private int status;
	


	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}


	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * Gets the msg id.
	 *
	 * @return the msg id
	 */
	@Column(name = "msg_id")
	public Long getMsgId() {
		return msgId;
	}


	/**
	 * Sets the msg id.
	 *
	 * @param msgId the new msg id
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}


	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public int getStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(int status) {
		this.status = status;
	}


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MSG_STATUS_SEQ")
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


}
