package com.legendshop.model.entity.orderreturn;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.model.entity.SubItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 退款表
 */
@Entity
@Table(name = "ls_sub_refund_return")
@ApiModel(value = "SubRefundReturn退款表")
public class SubRefundReturn implements GenericEntity<Long> {

	/**
	 *
	 */
	@ExcelIgnore
	private static final long serialVersionUID = -4203221919563336869L;

	/**
	 * 主键ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "主键ID")
	private Long id;

	/**
	 * 退款记录ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "退款记录ID")
	private Long refundId;

	/**
	 * 订单ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单ID")
	private Long subId;

	/**
	 * 订单编号
	 */
	@ExcelProperty(value = "订单编号",index = 0)
	@ColumnWidth(25)
	@ApiModelProperty(value = "订单编号")
	private String subNumber;

	/**
	 * 订单支付时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单支付时间")
	private Date orderDatetime;

	/**
	 * 订单总金额
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单总金额")
	private BigDecimal subMoney;

	/**
	 * 订单项ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单项ID")
	private Long subItemId;

	/**
	 * 申请编号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "申请编号")
	private String refundSn;

	/**
	 * 流水号(订单支付流水号)
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "流水号(订单支付流水号)")
	private String flowTradeNo;

	/**
	 * ls_sub_settlement 订单支付-清算单据号 去第三方支付的订单单据号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "ls_sub_settlement 订单支付-清算单据号 去第三方支付的订单单据号")
	private String subSettlementSn;

	/**
	 * 第三方退款单号(微信退款单号)
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "第三方退款单号(微信退款单号)")
	private String outRefundNo;

	/**
	 * 订单支付Id
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单支付Id")
	private String payTypeId;

	/**
	 * 订单支付Id
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单支付Id")
	private String payTypeName;

	/**
	 * 店铺ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "店铺ID")
	private Long shopId;

	/**
	 * 店铺名称
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "店铺名称")
	private String shopName;

	/**
	 * 买家ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "买家ID")
	private String userId;

	/**
	 * 买家会员
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "买家会员")
	private String userName;

	/**
	 * 订单商品ID,全部退款是0,默认0
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单商品ID,全部退款是0,默认0")
	private long productId;

	/**
	 * 订单SKU ID,全部退款是0,默认0
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单SKU ID,全部退款是0,默认0")
	private long skuId;

	/**
	 * 商品名称
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "商品名称")
	private String productName;

	/**
	 * 商品数量
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "商品数量")
	private Long goodsNum;

	/**
	 * 退款金额
	 */
	@ExcelProperty(value = "退款金额",index = 1)
	@ColumnWidth(15)
	@ApiModelProperty(value = "退款金额")
	private java.math.BigDecimal refundAmount;

	/**
	 * 预存款或者金币退款金额
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "预存款或者金币退款金额")
	private java.math.BigDecimal refundPdAmount;

	/**
	 * 商品图片
	 */
	@ExcelIgnore
	@ApiModelProperty(value = " 商品图片")
	private String productImage;

	/**
	 * 申请类型:1,仅退款,2退款退货,默认为1
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "申请类型:1,仅退款,2退款退货,默认为1")
	private int applyType;

	/**
	 * 卖家处理状态:1为待审核,2为同意,3为不同意
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "卖家处理状态:1为待审核,2为同意,3为不同意")
	private long sellerState;

	/**
	 * 申请状态:1为处理中,2为待管理员处理,3为已完成
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "申请状态:1为处理中,2为待管理员处理,3为已完成")
	private long applyState;

	/**
	 * 退货类型:1为不用退货,2为需要退货,默认为1
	 */
	@ExcelIgnore
	@ApiModelProperty(value = " 退货类型:1为不用退货,2为需要退货,默认为1")
	private long returnType;

	/**
	 * 物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为0
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为0")
	private long goodsState;

	/**
	 * 处理退款状态:[ 0:退款处理中 1:退款成功 -1:退款失败]
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "处理退款状态:[ 0:退款处理中 1:退款成功 -1:退款失败]")
	private int isHandleSuccess;

	/**
	 * 处理退款的方式 : 线下处理 预存款处理 等
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "处理退款的方式 : 线下处理 预存款处理 等")
	private String handleType;

	/**
	 * 是否结算
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否结算")
	private transient Boolean isBill;

	/**
	 * 订单结算编号[用于结算档期统计]
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单结算编号[用于结算档期统计]")
	private transient String billSn;

	/**
	 * 申请时间
	 */
	@ExcelProperty(value = "申请时间",index = 3)
	@ColumnWidth(25)
	@ApiModelProperty(value = "申请时间 ")
	private Date applyTime;

	/**
	 * 卖家处理时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "卖家处理时间")
	private Date sellerTime;

	/**
	 * 管理员处理时间
	 */
	@ExcelProperty(value = "完成时间",index = 4)
	@ColumnWidth(21)
	@ApiModelProperty(value = "管理员处理时间")
	private Date adminTime;

	/**
	 * 申请原因
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "申请原因")
	private String buyerMessage;

	/**
	 * 退款说明
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "退款说明")
	private String reasonInfo;

	/**
	 * 文件凭证1
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "文件凭证1")
	private String photoFile1;

	/**
	 * 文件凭证2
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "文件凭证2")
	private String photoFile2;

	/**
	 * 文件凭证3
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "文件凭证3")
	private String photoFile3;

	/**
	 * 卖家备注
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "卖家备注")
	private String sellerMessage;

	/**
	 * 管理员备注
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "管理员备注")
	private String adminMessage;

	/**
	 * 物流公司名称
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "物流公司名称")
	private String expressName;

	/**
	 * 物流单号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = " 物流单号")
	private String expressNo;

	/**
	 * 发货时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "发货时间 ")
	private Date shipTime;

	/**
	 * 收货时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "收货时间")
	private Date receiveTime;

	/**
	 * 收货备注
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "收货备注")
	private String receiveMessage;

	/**
	 * 第三方已退款金额
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "第三方已退款金额")
	private BigDecimal thirdPartyRefund;

	/**
	 * 平台已退款金额（金币或预存款）
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "平台已退款金额（金币或预存款）")
	private BigDecimal platformRefund;

	/**
	 * 是否预售
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否预售的退款单，默认为false")
	private Boolean isPresell;

	/**
	 * 是否退订金
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否退订金，默认为false")
	private Boolean isRefundDeposit;

	/**
	 * 订金退款状态  RefundReturnStatusEnum
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金退款状态，0为不用处理， 1为退款退货中， 2退款成功， -2为退款失败；需要退订金，默认为1，不需要退订金，默认为0")
	private int isDepositHandleSucces;

	/**
	 * 订金处理退款方式
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金处理退款方式")
	private String depositHandleType;

	/**
	 * 订金退款金额
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金退款金额，默认为0")
	private BigDecimal depositRefund;

	/**
	 * 订金支付流水号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金支付流水号")
	private String depositFlowTradeNo;

	/**
	 * 订金第三方退款单号(微信退款单号)
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金第三方退款单号(微信退款单号)")
	private String depositOutRefundNo;

	/**
	 * 订金第三方退款单号(微信退款单号)
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金支付ID")
	private String depositPayTypeId;

	/**
	 * 订金支付名称
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金支付名称")
	private String depositPayTypeName;

	/**
	 * ls_sub_settlement 预售订单订金支付-清算单据号 去第三方支付的订单单据号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "ls_sub_settlement 预售订单订金支付-清算单据号 去第三方支付的订单单据号")
	private String depositSubSettlementSn;

	/**
	 * 订金支付时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订金支付时间")
	private Date depositOrderDatetime;

	/**
	 * 退款单创建来源  RefundSouceEnum
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "退款单创建来源，0为用户 ，1为商家，2为平台")
	private int refundSouce;

	/**
	 * 订单商品数量
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单商品数量")
	private Long productNum;

	/**
	 * 订单商品规格属性
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单商品规格属性 ")
	private String productAttribute;

	/**
	 * 订单商品金额
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单商品金额")
	private Double subItemMoney;

	/**
	 * 退单红包金额（结算用）
	 */
	@ExcelProperty(value = "退单红包金额",index = 2)
	@ColumnWidth(18)
	@ApiModelProperty(value = "退单红包金额")
	private java.math.BigDecimal returnRedpackOffPrice;

	/**
	 * 结束时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "结束时间")
	private Long endTime;

	/**
	 * 卖家处理时间时间差
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "卖家处理时间时间差")
	private Long sellerTimeNumber;

	/**
	 * 发货时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "发货时间")
	private Long shipTimeNumber;


	/**
	 * 退款订单项
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "退订单项")
	private List<SubItem> subItemList;

	/** 退货地址. */
	@ExcelIgnore
	@ApiModelProperty(value = "退货地址")
	protected String returnShopAddr;

	/** 退货省份Id. */
	@ExcelIgnore
	@ApiModelProperty(value = "退货省份Id")
	protected Integer returnProvinceId;

	/** 退货城市Id. */
	@ExcelIgnore
	@ApiModelProperty(value = "退货城市Id")
	protected Integer returnCityId;

	/** 退货地区Id. */
	@ExcelIgnore
	@ApiModelProperty(value = "退货地区Id")
	protected Integer returnAreaId;

	/** 详细收货地址 */
	@ExcelIgnore
	@ApiModelProperty(value="详细退货地址")
	protected String returnDetailAddress;

	/**
	 * 退货联系人
	 */
	@ExcelIgnore
	@ApiModelProperty(value="退货联系人")
	protected String returnContact;

	/**
	 * 退货联系电话
	 */
	@ExcelIgnore
	@ApiModelProperty(value="退货联系电话")
	protected String returnPhone;

	public SubRefundReturn() {
	}

	@Id
	@Column(name = "refund_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB _REFUND_RETURN_SEQ")
	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	@Column(name = "sub_id")
	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	@Column(name = "sub_number")
	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * @return the subMoney
	 */
	@Column(name = "sub_money")
	public BigDecimal getSubMoney() {
		return subMoney;
	}

	/**
	 * @param subMoney the subMoney to set
	 */
	public void setSubMoney(BigDecimal subMoney) {
		this.subMoney = subMoney;
	}

	@Column(name = "sub_item_id")
	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	@Column(name = "refund_sn")
	public String getRefundSn() {
		return refundSn;
	}

	public void setRefundSn(String refundSn) {
		this.refundSn = refundSn;
	}

	@Column(name = "flow_trade_no")
	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	/**
	 * @return the outRefundNo
	 */
	@Column(name = "out_refund_no")
	public String getOutRefundNo() {
		return outRefundNo;
	}

	/**
	 * @param outRefundNo the outRefundNo to set
	 */
	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	@Column(name = "pay_type_id")
	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}


	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "shop_name")
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "product_id")
	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	@Column(name = "sku_id")
	public long getSkuId() {
		return skuId;
	}

	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}

	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "goods_num")
	public Long getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	@Column(name = "refund_amount")
	public java.math.BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(java.math.BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	@Column(name = "product_image")
	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	@Column(name = "apply_type")
	public int getApplyType() {
		return applyType;
	}

	public void setApplyType(int applyType) {
		this.applyType = applyType;
	}

	@Column(name = "seller_state")
	public long getSellerState() {
		return sellerState;
	}

	public void setSellerState(long sellerState) {
		this.sellerState = sellerState;
	}

	@Column(name = "return_type")
	public long getReturnType() {
		return returnType;
	}

	public void setReturnType(long returnType) {
		this.returnType = returnType;
	}

	@Column(name = "goods_state")
	public long getGoodsState() {
		return goodsState;
	}

	public void setGoodsState(long goodsState) {
		this.goodsState = goodsState;
	}

	@Column(name = "apply_time")
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	@Column(name = "seller_time")
	public Date getSellerTime() {
		return sellerTime;
	}

	public void setSellerTime(Date sellerTime) {
		this.sellerTime = sellerTime;
	}

	@Column(name = "admin_time")
	public Date getAdminTime() {
		return adminTime;
	}

	public void setAdminTime(Date adminTime) {
		this.adminTime = adminTime;
	}

	@Column(name = "reason_info")
	public String getReasonInfo() {
		return reasonInfo;
	}

	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	@Column(name = "photo_file1")
	public String getPhotoFile1() {
		return photoFile1;
	}

	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}

	@Column(name = "photo_file2")
	public String getPhotoFile2() {
		return photoFile2;
	}

	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}

	@Column(name = "photo_file3")
	public String getPhotoFile3() {
		return photoFile3;
	}

	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}

	@Column(name = "buyer_message")
	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	@Column(name = "seller_message")
	public String getSellerMessage() {
		return sellerMessage;
	}

	public void setSellerMessage(String sellerMessage) {
		this.sellerMessage = sellerMessage;
	}

	@Column(name = "admin_message")
	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	@Column(name = "express_name")
	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	@Column(name = "express_no")
	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}

	@Column(name = "ship_time")
	public Date getShipTime() {
		return shipTime;
	}

	public void setShipTime(Date shipTime) {
		this.shipTime = shipTime;
	}

	@Column(name = "receive_time")
	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	@Column(name = "receive_message")
	public String getReceiveMessage() {
		return receiveMessage;
	}

	public void setReceiveMessage(String receiveMessage) {
		this.receiveMessage = receiveMessage;
	}

	@Override
	@Transient
	public Long getId() {
		return refundId;
	}

	@Override
	public void setId(Long id) {
		refundId = id;
	}

	/**
	 * @return the applyState
	 */
	@Column(name = "apply_state")
	public long getApplyState() {
		return applyState;
	}

	/**
	 * @param applyState the applyState to set
	 */
	public void setApplyState(long applyState) {
		this.applyState = applyState;
	}

	@Column(name = "pay_type_name")
	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	@Column(name = "is_handle_success")
	public int getIsHandleSuccess() {
		return isHandleSuccess;
	}


	public void setIsHandleSuccess(int isHandleSuccess) {
		this.isHandleSuccess = isHandleSuccess;
	}

	@Column(name = "sub_settlement_sn")
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	@Column(name = "handle_type")
	public String getHandleType() {
		return handleType;
	}

	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}

	@Column(name = "is_bill")
	public Boolean getIsBill() {
		return isBill;
	}

	public void setIsBill(Boolean isBill) {
		this.isBill = isBill;
	}

	@Column(name = "bill_sn")
	public String getBillSn() {
		return billSn;
	}

	public void setBillSn(String billSn) {
		this.billSn = billSn;
	}

	@Column(name = "third_party_refund")
	public BigDecimal getThirdPartyRefund() {
		return thirdPartyRefund;
	}

	public void setThirdPartyRefund(BigDecimal thirdPartyRefund) {
		this.thirdPartyRefund = thirdPartyRefund;
	}

	@Column(name = "platform_refund")
	public BigDecimal getPlatformRefund() {
		return platformRefund;
	}

	public void setPlatformRefund(BigDecimal platformRefund) {
		this.platformRefund = platformRefund;
	}

	@Transient
	public java.math.BigDecimal getRefundPdAmount() {
		return refundPdAmount;
	}

	public void setRefundPdAmount(java.math.BigDecimal refundPdAmount) {
		this.refundPdAmount = refundPdAmount;
	}

	@Column(name = "order_datetime")
	public Date getOrderDatetime() {
		return orderDatetime;
	}

	public void setOrderDatetime(Date orderDatetime) {
		this.orderDatetime = orderDatetime;
	}

	@Column(name = "is_presell")
	public Boolean getIsPresell() {
		return isPresell;
	}

	public void setIsPresell(Boolean isPresell) {
		this.isPresell = isPresell;
	}

	@Column(name = "is_refund_deposit")
	public Boolean getIsRefundDeposit() {
		return isRefundDeposit;
	}

	public void setIsRefundDeposit(Boolean isRefundDeposit) {
		this.isRefundDeposit = isRefundDeposit;
	}

	@Column(name = "is_deposit_handle_succes")
	public int getIsDepositHandleSucces() {
		return isDepositHandleSucces;
	}

	public void setIsDepositHandleSucces(int isDepositHandleSucces) {
		this.isDepositHandleSucces = isDepositHandleSucces;
	}

	@Column(name = "deposit_handle_type")
	public String getDepositHandleType() {
		return depositHandleType;
	}

	public void setDepositHandleType(String depositHandleType) {
		this.depositHandleType = depositHandleType;
	}

	@Column(name = "deposit_refund")
	public BigDecimal getDepositRefund() {
		return depositRefund;
	}

	public void setDepositRefund(BigDecimal depositRefund) {
		this.depositRefund = depositRefund;
	}

	@Column(name = "deposit_flow_trade_no")
	public String getDepositFlowTradeNo() {
		return depositFlowTradeNo;
	}

	public void setDepositFlowTradeNo(String depositFlowTradeNo) {
		this.depositFlowTradeNo = depositFlowTradeNo;
	}

	@Column(name = "deposit_out_refund_no")
	public String getDepositOutRefundNo() {
		return depositOutRefundNo;
	}

	public void setDepositOutRefundNo(String depositOutRefundNo) {
		this.depositOutRefundNo = depositOutRefundNo;
	}

	@Column(name = "deposit_pay_type_id")
	public String getDepositPayTypeId() {
		return depositPayTypeId;
	}

	public void setDepositPayTypeId(String depositPayTypeId) {
		this.depositPayTypeId = depositPayTypeId;
	}

	@Column(name = "deposit_pay_type_name")
	public String getDepositPayTypeName() {
		return depositPayTypeName;
	}

	public void setDepositPayTypeName(String depositPayTypeName) {
		this.depositPayTypeName = depositPayTypeName;
	}

	@Column(name = "deposit_sub_settlement_sn")
	public String getDepositSubSettlementSn() {
		return depositSubSettlementSn;
	}

	public void setDepositSubSettlementSn(String depositSubSettlementSn) {
		this.depositSubSettlementSn = depositSubSettlementSn;
	}

	@Column(name = "deposit_order_datetime")
	public Date getDepositOrderDatetime() {
		return depositOrderDatetime;
	}

	public void setDepositOrderDatetime(Date depositOrderDatetime) {
		this.depositOrderDatetime = depositOrderDatetime;
	}

	@Column(name = "refund_souce")
	public int getRefundSouce() {
		return refundSouce;
	}

	public void setRefundSouce(int refundSouce) {
		this.refundSouce = refundSouce;
	}

	@Column(name = "product_num")
	public Long getProductNum() {
		return productNum;
	}

	public void setProductNum(Long productNum) {
		this.productNum = productNum;
	}

	@Column(name = "product_attribute")
	public String getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(String productAttribute) {
		this.productAttribute = productAttribute;
	}

	@Column(name = "sub_item_money")
	public Double getSubItemMoney() {
		return subItemMoney;
	}

	public void setSubItemMoney(Double subItemMoney) {
		this.subItemMoney = subItemMoney;
	}

	@Transient
	public BigDecimal getReturnRedpackOffPrice() {
		return returnRedpackOffPrice;
	}

	public void setReturnRedpackOffPrice(BigDecimal returnRedpackOffPrice) {
		this.returnRedpackOffPrice = returnRedpackOffPrice;
	}

	@Transient
	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	@Transient
	public Long getSellerTimeNumber() {
		return sellerTimeNumber;
	}

	public void setSellerTimeNumber(Long sellerTimeNumber) {
		this.sellerTimeNumber = sellerTimeNumber;
	}

	@Transient
	public Long getShipTimeNumber() {
		return shipTimeNumber;
	}

	public void setShipTimeNumber(Long shipTimeNumber) {
		this.shipTimeNumber = shipTimeNumber;
	}

	@Transient
	public List<SubItem> getSubItemList() {
		return subItemList;
	}

	public void setSubItemList(List<SubItem> subItemList) {
		this.subItemList = subItemList;
	}

	@Transient
	public String getReturnShopAddr() {
		return returnShopAddr;
	}

	public void setReturnShopAddr(String returnShopAddr) {
		this.returnShopAddr = returnShopAddr;
	}

	@Transient
	public Integer getReturnProvinceId() {
		return returnProvinceId;
	}

	public void setReturnProvinceId(Integer returnProvinceId) {
		this.returnProvinceId = returnProvinceId;
	}

	@Transient
	public Integer getReturnCityId() {
		return returnCityId;
	}

	public void setReturnCityId(Integer returnCityId) {
		this.returnCityId = returnCityId;
	}

	@Transient
	public Integer getReturnAreaId() {
		return returnAreaId;
	}

	public void setReturnAreaId(Integer returnAreaId) {
		this.returnAreaId = returnAreaId;
	}

	@Column(name="return_detail_address")
	public String getReturnDetailAddress() {
		return returnDetailAddress;
	}

	public void setReturnDetailAddress(String returnDetailAddress) {
		this.returnDetailAddress = returnDetailAddress;
	}

	@Column(name="return_contact")
	public String getReturnContact() {
		return returnContact;
	}

	public void setReturnContact(String returnContact) {
		this.returnContact = returnContact;
	}

	@Column(name="return_phone")
	public String getReturnPhone() {
		return returnPhone;
	}

	public void setReturnPhone(String returnPhone) {
		this.returnPhone = returnPhone;
	}
}
