package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

public class MergeGroupJoinDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//活动ID
	private Long mergeId;
	
	//活动状态
	private Integer mergeStatus;
	
	//活动结束时间
	private Date endTime;

	// 成团人数(n人团)
	private Integer peopleNumber;
	
	//该团Id
	private Long operateId;

	// 该团参团人数
	private Integer number;
	
	//该团状态
	private Integer operateStatus;

	public Long getMergeId() {
		return mergeId;
	}

	public void setMergeId(Long mergeId) {
		this.mergeId = mergeId;
	}

	public Integer getMergeStatus() {
		return mergeStatus;
	}

	public void setMergeStatus(Integer mergeStatus) {
		this.mergeStatus = mergeStatus;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Long getOperateId() {
		return operateId;
	}

	public void setOperateId(Long operateId) {
		this.operateId = operateId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getOperateStatus() {
		return operateStatus;
	}

	public void setOperateStatus(Integer operateStatus) {
		this.operateStatus = operateStatus;
	}
}
