package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class SubRefundRetuenDto implements Serializable{
	
	private String subNumber;//订单编号
	private String applyType; //类型
	private Double refundAmount;//退款金额
	private Date applyTime;//退单时间
	private Double returnRedpackOffPrice;//退单红包金额
	private Date adminTime;//完成时间

	
	public String getSubNumber() {
		return subNumber;
	}
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	public String getApplyType() {
		return applyType;
	}
	public void setApplyType(String applyType) {
		this.applyType = applyType;
	}
	public Double getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}
	public Date getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	public Date getAdminTime() {
		return adminTime;
	}
	public void setAdminTime(Date adminTime) {
		this.adminTime = adminTime;
	}

	public Double getReturnRedpackOffPrice() {
		return returnRedpackOffPrice;
	}

	public void setReturnRedpackOffPrice(Double returnRedpackOffPrice) {
		this.returnRedpackOffPrice = returnRedpackOffPrice;
	}
}
