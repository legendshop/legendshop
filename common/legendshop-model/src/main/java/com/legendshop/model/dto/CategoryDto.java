/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.support.GenericEntity;

/**
 * 分类树
 * 
 * @author tony
 * 
 */
public class CategoryDto  implements GenericEntity<CategoryDto> {

	private static final long serialVersionUID = 5433541922923156943L;

	// 分类ID
	private Long categoryId;
	
	private Long parentId;
	// 分类名称
	private String categoryName;

	private Integer typeId;
	// 网页关键字
	private String metaKeywords;
	// 网页标题
	private String metaTitle;
	// 网页描述
	private String metaDescription;
	
	private Integer seq;
	
	private Integer level;
	
	private Integer status; 

	private Set<CategoryDto> childrens;
	
	private CategoryDto parentCategory;

	
	public CategoryDto(Long categoryId, Long parentId, String categoryName,Integer seq,Integer status) {
		super();
		this.categoryId = categoryId;
		this.parentId = parentId;
		this.categoryName = categoryName;
		this.seq=seq;
		this.status=status;
	}

	public CategoryDto(Long categoryId, Long parentId, String categoryName,
			Integer typeId, String metaKeywords, String metaTitle,
			String metaDescription,Integer seq,Integer level) {
		super();
		this.categoryId = categoryId;
		this.parentId = parentId;
		this.categoryName = categoryName;
		this.typeId = typeId;
		this.metaKeywords = metaKeywords;
		this.metaTitle = metaTitle;
		this.metaDescription = metaDescription;
		this.seq=seq;
		this.level=level;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getMetaKeywords() {
		return metaKeywords;
	}

	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public Set<CategoryDto> getChildrens() {
		return childrens;
	}

	public void setChildrens(Set<CategoryDto> childrens) {
		this.childrens = childrens;
	}
	

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoryId == null) ? 0 : categoryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TreeNode){
			CategoryDto node=(CategoryDto)obj;
			return this.categoryId==node.getCategoryId();
		}
		return false;
	}
	
	public void addChildCategory(CategoryDto categoryDto) {
		if (childrens == null)
			childrens = new TreeSet<CategoryDto>(new CategoryDtoComparator());
		childrens.add(categoryDto);
	}
	

	public CategoryDto getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(CategoryDto parentCategory) {
		this.parentCategory = parentCategory;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Override
	public CategoryDto getId() {
		return this;
	}

	@Override
	public void setId(CategoryDto id) {
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	
}