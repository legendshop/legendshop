package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品类型跟品牌的关系表
 */
@Entity
@Table(name = "ls_prod_type_brand")
public class ProdTypeBrand implements GenericEntity<Long> {

	private static final long serialVersionUID = 7146719268669281364L;

	/** 主键 */
	private Long id; 
		
	/**  */
	private Long typeId; 
		
	/**  */
	private Long brandId; 
	
	private Integer seq;
		
	
	public ProdTypeBrand() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_TYPE_BRAND_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "type_id")
	public Long  getTypeId(){
		return typeId;
	} 
		
	public void setTypeId(Long typeId){
			this.typeId = typeId;
		}
		
    @Column(name = "brand_id")
	public Long  getBrandId(){
		return brandId;
	} 
		
	public void setBrandId(Long brandId){
			this.brandId = brandId;
		}

	 @Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	


} 
