package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 预存款充值记录Dto
 */
@ApiModel(value="预存款充值记录Dto") 
public class AppPdRechargeDto implements Serializable{

	/**  */
	private static final long serialVersionUID = -5067519141535834642L;

	/** 主键id */
	@ApiModelProperty(value = "主键id")
	private Long id; 
		
	/** 唯一的流水号 */
	@ApiModelProperty(value = "唯一的流水号")
	private String sn; 
		
	/** 会员编号 */
	@ApiModelProperty(value = "会员编号")
	private String userId; 
		
	/** 用户名 */
	@ApiModelProperty(value = "用户名")
	private String userName; 
		
	/** 充值金额 */
	@ApiModelProperty(value = "充值金额")
	private Double amount; 
		
	/** 支付方式 */
	@ApiModelProperty(value = "支付方式")
	private String paymentCode; 
		
	/** 支付名称 */
	@ApiModelProperty(value = "支付名称 ")
	private String paymentName; 
		
	/** 第三方支付接口交易号 */
	@ApiModelProperty(value = "第三方支付接口交易号")
	private String tradeSn; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间")
	private Date addTime; 
		
	/** 支付状态 0未支付1支付 */
	@ApiModelProperty(value = "支付状态 0未支付1支付")
	private Integer paymentState; 
		
	/** 支付时间 */
	@ApiModelProperty(value = "支付时间")
	private Date paymentTime; 
		
	/** 管理员名 */
	@ApiModelProperty(value = "管理员名")
	private String adminUserName; 
		
	/** 后台管理员操作备注 */
	@ApiModelProperty(value = "后台管理员操作备注")
	private String adminUserNote; 
	
	/** 昵称 */
	@ApiModelProperty(value = "昵称")
	private String nickName;
	
	public AppPdRechargeDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	public String getPaymentName() {
		return paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}

	public String getTradeSn() {
		return tradeSn;
	}

	public void setTradeSn(String tradeSn) {
		this.tradeSn = tradeSn;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Integer getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(Integer paymentState) {
		this.paymentState = paymentState;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminUserNote() {
		return adminUserNote;
	}

	public void setAdminUserNote(String adminUserNote) {
		this.adminUserNote = adminUserNote;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
}
