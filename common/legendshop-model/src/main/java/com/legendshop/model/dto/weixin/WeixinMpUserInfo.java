package com.legendshop.model.dto.weixin;

/**
 * 微信小程序用户信息
 */
public class WeixinMpUserInfo{

	/** 用户在商家微信下的唯一标识 */
	private java.lang.String openId;
	
	/** 用户的昵称 */
	private java.lang.String nickName;
	
	/** 性别 */
	private java.lang.String gender;
	
	/** 用户所在城市 */
	private java.lang.String city;
	
	/** 用户所在国家 */
	private java.lang.String country;
	
	/** 用户所在省份 */
	private java.lang.String province;
	
	/** 用户的语言zh_CN */
	private java.lang.String language;
	
	/** 用户头像 */
	private java.lang.String avatarUrl;

	/** 微信开放平台统一账号下的多个应用的统一标识 */
	private java.lang.String unionid;
	
	/**
	 * 如果有获取到敏感数据, 则需要数据水印
	 */
	private Watermark watermark;
	
	public Watermark getWatermark() {
		return watermark;
	}

	public void setWatermark(Watermark watermark) {
		this.watermark = watermark;
	}

	public java.lang.String getOpenId() {
		return openId;
	}

	public void setOpenId(java.lang.String openId) {
		this.openId = openId;
	}

	public java.lang.String getNickName() {
		return nickName;
	}

	public void setNickName(java.lang.String nickName) {
		this.nickName = nickName;
	}

	public java.lang.String getGender() {
		return gender;
	}

	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}

	public java.lang.String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(java.lang.String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public java.lang.String getCity() {
		return city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.String getCountry() {
		return country;
	}

	public void setCountry(java.lang.String country) {
		this.country = country;
	}

	public java.lang.String getProvince() {
		return province;
	}

	public void setProvince(java.lang.String province) {
		this.province = province;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	public java.lang.String getUnionid() {
		return unionid;
	}

	public void setUnionid(java.lang.String unionid) {
		this.unionid = unionid;
	}

	public static class Watermark {
		
		private String appid;
		
		private long timestamp;

		public String getAppid() {
			return appid;
		}

		public void setAppid(String appid) {
			this.appid = appid;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}
	}
}
