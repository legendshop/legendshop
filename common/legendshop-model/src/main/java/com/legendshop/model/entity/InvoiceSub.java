package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发票订单信息表
 */
@Entity
@Table(name = "ls_invoice_sub")
@ApiModel("用户的订单发票")
public class InvoiceSub implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键id")
	private Long id;

	@ApiModelProperty(value = "用户id")
	private String userId;

	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "发票类型")
	private Integer type;

	@ApiModelProperty(value = "发票抬头[1:个人 2：单位]")
	private Integer title;

	@ApiModelProperty(value = "发票抬头备注")
	private String company;

	@ApiModelProperty(value = "发票内容")
	private Integer content;

	@ApiModelProperty(value = "纳税人号")
	private String invoiceHumNumber;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	/** 注册地址（增值税发票） */
	@ApiModelProperty(value = "注册地址（增值税发票）")
	private String registerAddr;

	/** 注册电话（增值税发票） */
	@ApiModelProperty(value = "注册电话（增值税发票）")
	private String registerPhone;

	/** 开户银行（增值税发票） */
	@ApiModelProperty(value = " 开户银行（增值税发票）")
	private String depositBank;

	/** 开户银行账号（增值税发票） */
	@ApiModelProperty(value = "开户银行账号（增值税发票）")
	private String bankAccountNum;
	
	public InvoiceSub() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INVOICE_SUB_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "title")
	public Integer getTitle() {
		return title;
	}

	public void setTitle(Integer title) {
		this.title = title;
	}

	@Column(name = "content")
	public Integer getContent() {
		return content;
	}

	public void setContent(Integer content) {
		this.content = content;
	}

	@Column(name = "company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "invoice_hum_number")
	public String getInvoiceHumNumber() {
		return invoiceHumNumber;
	}

	public void setInvoiceHumNumber(String invoiceHumNumber) {
		this.invoiceHumNumber = invoiceHumNumber;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "register_addr")
	public String getRegisterAddr() {
		return registerAddr;
	}

	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}

	@Column(name = "register_phone")
	public String getRegisterPhone() {
		return registerPhone;
	}

	public void setRegisterPhone(String registerPhone) {
		this.registerPhone = registerPhone;
	}

	@Column(name = "deposit_bank")
	public String getDepositBank() {
		return depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	@Column(name = "bank_account_num")
	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}
}
