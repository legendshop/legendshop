/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;
public enum IntegralOrderStatusEnum implements IntegerEnum {
	/** 待发货. */
	SUBMIT(0),

	/** 待收货 */
	CONSIGNMENT(1),

	/** 已经完成. */
	SUCCESS(2),

	/**取消状态 */
	CANCEL(3);

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	IntegralOrderStatusEnum(Integer num) {
		this.num = num;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		System.out.println(IntegralOrderStatusEnum.SUCCESS.value());
		System.out.println(IntegralOrderStatusEnum.SUCCESS);
	}
}
