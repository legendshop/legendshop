package com.legendshop.model.dto.marketing;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.util.AppUtils;

/**
 * 促销规则
 */
public class RuleResolver {

	
	/* 商品原价*/
	private Double price=0d;
	
	/* 促销价格  */
	private Double promotionPrice; 
	
    /* 商品ID */
    private Long prodId;
    
    /* shop ID  */
    private Long shopId;
	
    /* skuId */
	private Long skuId;
	
	  /* 购物车商品  */
    private Integer basketCount=0;
    
    
    private List<CartMarketRules> cartMarketRules;
    

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}
    
	public List<CartMarketRules> getCartMarketRules() {
		return cartMarketRules;
	}

	public void setCartMarketRules(List<CartMarketRules> cartMarketRules) {
		this.cartMarketRules = cartMarketRules;
	}

	public void addCartMarketRules(CartMarketRules cartMarketRule) {
		if(AppUtils.isBlank(this.cartMarketRules)){
			this.cartMarketRules=new ArrayList<CartMarketRules>(); 
		}
		this.cartMarketRules.add(cartMarketRule);
	}
    
	
}
