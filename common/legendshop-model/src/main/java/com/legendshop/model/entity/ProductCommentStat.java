package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *评论统计表
 */
@Entity
@Table(name = "ls_prod_comm_stat")
public class ProductCommentStat implements GenericEntity<Long> {

	private static final long serialVersionUID = -8251965872522981068L;

	/** 主键 */
	private Long id; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** 评论数 */
	private Integer comments; 
		
	/** 评论分总数 */
	private Integer score; 
		
	
	public ProductCommentStat() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_COMM_STAT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "comments")
	public Integer  getComments(){
		return comments;
	} 
		
	public void setComments(Integer comments){
			this.comments = comments;
		}
		
    @Column(name = "score")
	public Integer  getScore(){
		return score;
	} 
		
	public void setScore(Integer score){
			this.score = score;
		}
	


} 
