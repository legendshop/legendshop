package com.legendshop.model.entity.appDecorate;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *移动端页面装修页面
 */
@Entity
@Table(name = "ls_app_page_manage")
public class AppPageManage implements GenericEntity<Long> {

	/**  */
	private static final long serialVersionUID = 8027064118274410225L;

	/** 主键 */
	private Long id; 
		
	/** 页面名称 */
	private String name; 
		
	/** 页面类型 [INDEX：首页 POSTER：海报页] 参考枚举APPTemplateCategoryEnum */
	private String category; 
		
	/** 状态 [-1：草稿  0:已修改未发布 1:已发布 ] */
	private Integer status; 
		
	/** 是否已使用[1:使用中 0:未使用] */
	private Integer isUse; 
		
	/** 可编辑的装修数据 */
	private String data; 
	
	/** 已发布的装修数据  */
	private String releaseData;
		
	/** 创建时间 */
	private Date recDate; 
		
	
	public AppPageManage() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "APP_PAGE_MANAGE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "category")
	public String  getCategory(){
		return category;
	} 
		
	public void setCategory(String category){
			this.category = category;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
	@Column(name = "is_use")	
	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}
	
		
    @Column(name = "data")
	public String  getData(){
		return data;
	} 
    
	public void setData(String data){
			this.data = data;
		}
	
	@Column(name = "release_data")
    public String getReleaseData() {
		return releaseData;
	}

	public void setReleaseData(String releaseData) {
		this.releaseData = releaseData;
	}

	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	


} 
