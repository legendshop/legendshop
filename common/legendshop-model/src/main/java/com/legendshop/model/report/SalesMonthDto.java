/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.report;


/**
 * 销售Dto.
 */
public class SalesMonthDto extends AbstractSalesDto {

	private static final long serialVersionUID = -496647548037558700L;
	/** The day. */
	private int salesDay;

	/**
	 * Gets the sales day.
	 *
	 * @return the sales day
	 */
	public int getSalesDay() {
		return salesDay;
	}

	/**
	 * Sets the sales day.
	 *
	 * @param salesDay the new sales day
	 */
	public void setSalesDay(int salesDay) {
		this.salesDay = salesDay;
	}
	
	
}
