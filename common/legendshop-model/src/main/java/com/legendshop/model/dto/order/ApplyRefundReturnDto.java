/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description 退款及退货的DTO
 * @author 关开发
 */
@ApiModel(value="退货退款Dto")
public class ApplyRefundReturnDto {
	
	/** 申请类型:1,仅退款,2退款退货,默认为1 */
	@ApiModelProperty(value="申请类型:1,仅退款,2退款退货,默认为1")
	private Long applyType; 
	
	/** 订单ID */
	@ApiModelProperty(value="订单ID")
	private Long orderId;

	/** 订单类型 */
	@ApiModelProperty(value="订单类型 ")
	private String subType;
	
	/** 订单项Id */
	@ApiModelProperty(value="订单项Id")
	private Long orderItemId;
	
	/** 订单号 */
	@ApiModelProperty(value="订单号")
	private String subNumber;
	
	/** 订单总金额 */
	@ApiModelProperty(value="订单总金额")
	private Double subMoney;
		
	/** 商品名称 或 订单编号 */
	@ApiModelProperty(value="商品名称 或 订单编号")
	private String name; 
		
	/** 退款金额 */
	@ApiModelProperty(value="退款金额")
	private Double refundAmount;
	
	/** 退货数量 */
	@ApiModelProperty(value="退货数量 ")
	private Integer goodsCount;
	
	/** shop Id */
	@ApiModelProperty(value="店铺id")
	private Long shopId;
	
	/** shopName */
	@ApiModelProperty(value="店铺名称")
	private String shopName;
	
	/** 支付类型Id */
	@ApiModelProperty(value="支付类型Id")
	private String payTypeId;
	
	/** 支付类型名称 */
	@ApiModelProperty(value="支付类型名称")
	private String payTypeName;
	
	/** 支付流水号 */
	@ApiModelProperty(value="支付流水号 ")
	private String flowTradeNo;
	
	/**sub_settlement_sn */
	@ApiModelProperty(value="ls_sub_settlement 清算单据号")
	private String subSettlementSn;
	
	/**sub_settlement_sn */
	@ApiModelProperty(value="ls_sub_settlement 创建时间")
	private Date orderDateTime;
	
	/** 订单商品ID */
	@ApiModelProperty(value="订单商品ID")
	private Long prodId; 
		
	/** 订单SKU ID */
	@ApiModelProperty(value="订单SKU ID")
	private Long skuId; 
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;
	
	/** 订单支付方式 : 在线支付,货到付款 */
	@ApiModelProperty(value="订单支付方式 : 在线支付,货到付款")
	private Integer payManner;
	
	/** 是否货到付款 */
	@ApiModelProperty(value="是否货到付款 ")
	private Integer isCod;
	
	/** 订单状态 */
	@ApiModelProperty(value="订单状态")
	private Integer orderStatus;
	
	/** 订单退款状态 */
	@ApiModelProperty(value="订单退款状态")
	private Integer refundStatus;
	
	/** 订单项退款/退货状态 */
	@ApiModelProperty(value="订单项退款/退货状态")
	private Integer itemRefundStatus;
	
	/** 订单完成时间 */
	@ApiModelProperty(value="订单完成时间 ")
	private Date finallyDate;
	
	/** 预售支付方式  0  全额支付, 1 订金支付的订金  2 订金支付尾款   PayPctTypeEnum */
	@ApiModelProperty(value="如果是预售订单，预售支付方式  0  全额支付, 1 订金支付的订金  2 订金支付尾款  ")
	private Integer payPctType;
	
	/**预售订金退款金额 */
	@ApiModelProperty(value="预售订金退款金额")
	private Double depositRefundAmount;
	
	/**是否支付订金 */
	@ApiModelProperty(value="是否支付订金")
	private Integer isPayDeposit;
	
	/**预售尾款退款金额 */
	@ApiModelProperty(value="预售尾款退款金额")
	private Double finalRefundAmount;
	
	/**是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款")
	private Integer isPayFinal;
	
	/**预售尾款支付类型Id */
	@ApiModelProperty(value="预售尾款支付类型Id")
	private String finalPayTypeId;
	
	/** 预售尾款支付类型名称 */
	@ApiModelProperty(value="预售尾款支付类型名称")
	private String finalPayTypeName;
	
	/** 预售尾款支付流水号 */
	@ApiModelProperty(value="预售尾款支付流水号")
	private String finalFlowTradeNo;
	
	/**  ls_sub_settlement 预售尾款 清算单据号 */
	@ApiModelProperty(value="预售尾款 清算单据号")
	private String finalSettlementSn;
	
	/**预售尾款  ls_sub_settlement 创建时间*/
	@ApiModelProperty(value="预售尾款 创建时间")
	private Date finalOrderDateTime;	
	
	/** 商品属性 */
	@ApiModelProperty(value="商品属性")
	private String attribute;
	
	/** 商品购买数量 */
	@ApiModelProperty(value="商品购买数量")
	private Long basketCount;
	
	/** 商品价格 */
	@ApiModelProperty(value="商品价格")
	private Double prodCash;

	/** 商品价格 */
	@ApiModelProperty(value="用户id（用于商家端）")
	private String userId;

	/**
	 * @return the applyType
	 */
	public Long getApplyType() {
		return applyType;
	}

	/**
	 * @param applyType the applyType to set
	 */
	public void setApplyType(Long applyType) {
		this.applyType = applyType;
	}

	/**
	 * @return the orderId
	 */
	public Long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the orderItemId
	 */
	public Long getOrderItemId() {
		return orderItemId;
	}

	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the refundAmount
	 */
	public Double getRefundAmount() {
		return refundAmount;
	}

	/**
	 * @param refundAmount the refundAmount to set
	 */
	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	/**
	 * @return the goodsCount
	 */
	public Integer getGoodsCount() {
		return goodsCount;
	}

	/**
	 * @param goodsCount the goodsCount to set
	 */
	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}

	/**
	 * @return the isCod
	 */
	public Integer getIsCod() {
		return isCod;
	}

	/**
	 * @param isCod the isCod to set
	 */
	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	/**
	 * @return the orderStatus
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}

	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * @return the refundStatus
	 */
	public Integer getRefundStatus() {
		return refundStatus;
	}

	/**
	 * @param refundStatus the refundStatus to set
	 */
	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}

	/**
	 * @return the subNumber
	 */
	public String getSubNumber() {
		return subNumber;
	}

	/**
	 * @param subNumber the subNumber to set
	 */
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * @return the sunMoney
	 */
	public Double getSubMoney() {
		return subMoney;
	}

	/**
	 * @param sunMoney the sunMoney to set
	 */
	public void setSubMoney(Double subMoney) {
		this.subMoney = subMoney;
	}

	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the payTypeId
	 */
	public String getPayTypeId() {
		return payTypeId;
	}

	/**
	 * @param payTypeId the payTypeId to set
	 */
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}
	
	/**
	 * @return the payTypeName
	 */
	public String getPayTypeName() {
		return payTypeName;
	}

	/**
	 * @param payTypeName the payTypeName to set
	 */
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	/**
	 * @return the flowTradeNo
	 */
	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	/**
	 * @param flowTradeNo the flowTradeNo to set
	 */
	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}
	
	/**
	 * @return the subSettlementSn
	 */
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	/**
	 * @param subSettlementSn the subSettlementSn to set
	 */
	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	/**
	 * @return the productId
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * @return the skuId
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * @param skuId the skuId to set
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the prodPic
	 */
	public String getProdPic() {
		return prodPic;
	}

	/**
	 * @param prodPic the prodPic to set
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	/**
	 * @return the itemRefundStatus
	 */
	public Integer getItemRefundStatus() {
		return itemRefundStatus;
	}

	/**
	 * @param itemRefundStatus the itemRefundStatus to set
	 */
	public void setItemRefundStatus(Integer itemRefundStatus) {
		this.itemRefundStatus = itemRefundStatus;
	}

	/**
	 * @return the payManner
	 */
	public Integer getPayManner() {
		return payManner;
	}

	/**
	 * @param payManner the payManner to set
	 */
	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	} 
	
	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Date getOrderDateTime() {
		return orderDateTime;
	}

	public void setOrderDateTime(Date orderDateTime) {
		this.orderDateTime = orderDateTime;
	}

	public Double getFinalRefundAmount() {
		return finalRefundAmount;
	}

	public void setFinalRefundAmount(Double finalRefundAmount) {
		this.finalRefundAmount = finalRefundAmount;
	}

	public String getFinalPayTypeId() {
		return finalPayTypeId;
	}

	public void setFinalPayTypeId(String finalPayTypeId) {
		this.finalPayTypeId = finalPayTypeId;
	}

	public String getFinalPayTypeName() {
		return finalPayTypeName;
	}

	public void setFinalPayTypeName(String finalPayTypeName) {
		this.finalPayTypeName = finalPayTypeName;
	}

	public String getFinalFlowTradeNo() {
		return finalFlowTradeNo;
	}

	public void setFinalFlowTradeNo(String finalFlowTradeNo) {
		this.finalFlowTradeNo = finalFlowTradeNo;
	}

	public String getFinalSettlementSn() {
		return finalSettlementSn;
	}

	public void setFinalSettlementSn(String finalSettlementSn) {
		this.finalSettlementSn = finalSettlementSn;
	}

	public Date getFinalOrderDateTime() {
		return finalOrderDateTime;
	}

	public void setFinalOrderDateTime(Date finalOrderDateTime) {
		this.finalOrderDateTime = finalOrderDateTime;
	}

	public Double getDepositRefundAmount() {
		return depositRefundAmount;
	}

	public void setDepositRefundAmount(Double depositRefundAmount) {
		this.depositRefundAmount = depositRefundAmount;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Long getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Long basketCount) {
		this.basketCount = basketCount;
	}

	public Double getProdCash() {
		return prodCash;
	}

	public void setProdCash(Double prodCash) {
		this.prodCash = prodCash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
