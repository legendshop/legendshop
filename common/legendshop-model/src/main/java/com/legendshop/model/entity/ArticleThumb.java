package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 文章点赞
 */
@Entity
@Table(name = "ls_article_thumb")
public class ArticleThumb implements GenericEntity<Long> {

	private static final long serialVersionUID = 1805251790174369615L;

	/** id */
	private Long id; 
		
	/** 文章id */
	private Long artId; 
		
	/** 点赞人的id */
	private String userId; 
		
	/** 点赞时间 */
	private Date createTime; 
		
	
	public ArticleThumb() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ARTICLE_THUMB_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "art_id")
	public Long  getArtId(){
		return artId;
	} 
		
	public void setArtId(Long artId){
			this.artId = artId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	


} 
