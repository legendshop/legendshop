/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;
import java.util.List;

/**
 * 客户自定义属性.
 */
public class CustomPropertyDto implements Serializable, Cloneable {

	private static final long serialVersionUID = 5873753039951403592L;

	/** The key. */
	private String key;

	/** The value. */
	private List<String> value;

	/**
	 * Instantiates a new key value entity.
	 */
	public CustomPropertyDto() {

	}

	/**
	 * Instantiates a new key value entity.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public CustomPropertyDto(String key, List<String> value) {
		this.key = key;
		this.value = value;
	}
	
	public CustomPropertyDto(Integer key, List<String> value) {
		this.key = String.valueOf(key);
		this.value = value;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public List<String> getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(List<String> value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int result = 17;
		result = 31 * result + key.hashCode();
		result = 31 * result + value.hashCode();
		return result;
	}


	public CustomPropertyDto clone(){
		CustomPropertyDto entity = new CustomPropertyDto();
		entity.setKey(this.getKey());
		entity.setValue(this.getValue());
		
		return entity;
	}
}
