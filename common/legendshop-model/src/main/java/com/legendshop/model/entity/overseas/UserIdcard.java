package com.legendshop.model.entity.overseas;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *用户实名认证
 */
@Entity
@Table(name = "ls_user_idcard")
public class UserIdcard implements GenericEntity<Long> {

	private static final long serialVersionUID = 7211399590911458512L;

	/** id */
	private Long id; 
		
	/** 用户ID */
	private String userId; 
		
	/** 真实姓名 */
	private String realName; 
		
	/** 身份证号码 */
	private String idcardNum; 
		
	/** 身份证正面 */
	private String frontPic; 
		
	/** 身份证反面 */
	private String backPic; 
		
	/** 是否默认：0 否 1 是 */
	private Integer defaultSts; 
		
	/** 修改时间 */
	private Date updateTime; 
		
	
	public UserIdcard() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USER_IDCARD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "real_name")
	public String  getRealName(){
		return realName;
	} 
		
	public void setRealName(String realName){
			this.realName = realName;
		}
		
    @Column(name = "idcard_num")
	public String  getIdcardNum(){
		return idcardNum;
	} 
		
	public void setIdcardNum(String idcardNum){
			this.idcardNum = idcardNum;
		}
		
    @Column(name = "front_pic")
	public String  getFrontPic(){
		return frontPic;
	} 
		
	public void setFrontPic(String frontPic){
			this.frontPic = frontPic;
		}
		
    @Column(name = "back_pic")
	public String  getBackPic(){
		return backPic;
	} 
		
	public void setBackPic(String backPic){
			this.backPic = backPic;
		}
		
    @Column(name = "default_sts")
	public Integer  getDefaultSts(){
		return defaultSts;
	} 
		
	public void setDefaultSts(Integer defaultSts){
			this.defaultSts = defaultSts;
		}
		
    @Column(name = "update_time")
	public Date  getUpdateTime(){
		return updateTime;
	} 
		
	public void setUpdateTime(Date updateTime){
			this.updateTime = updateTime;
		}
	


} 
