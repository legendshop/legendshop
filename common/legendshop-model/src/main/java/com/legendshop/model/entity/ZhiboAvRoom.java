package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *直播角色心跳表
 */
@Entity
@Table(name = "ls_zhibo_av_room")
public class ZhiboAvRoom implements GenericEntity<Long> {

	/** 房间ID */
	private Long id; 
		
	/** 房间主播名 */
	private String uid; 
		
	/** groupid_userid_aux */
	private String auxMd5; 
		
	/** groupid_userid_main */
	private String mainMd5; 
		
	/**  */
	private String title; 
		
	/**  */
	private String cover; 
		
	/** 心跳时间戳 */
	private Long lastUpdateTime; 
		
	
	public ZhiboAvRoom() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ZHIBO_AV_ROOM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "uid")
	public String  getUid(){
		return uid;
	} 
		
	public void setUid(String uid){
			this.uid = uid;
		}
		
    @Column(name = "aux_md5")
	public String  getAuxMd5(){
		return auxMd5;
	} 
		
	public void setAuxMd5(String auxMd5){
			this.auxMd5 = auxMd5;
		}
		
    @Column(name = "main_md5")
	public String  getMainMd5(){
		return mainMd5;
	} 
		
	public void setMainMd5(String mainMd5){
			this.mainMd5 = mainMd5;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "cover")
	public String  getCover(){
		return cover;
	} 
		
	public void setCover(String cover){
			this.cover = cover;
		}
		
    @Column(name = "last_update_time")
	public Long  getLastUpdateTime(){
		return lastUpdateTime;
	} 
		
	public void setLastUpdateTime(Long lastUpdateTime){
			this.lastUpdateTime = lastUpdateTime;
		}
	


} 
