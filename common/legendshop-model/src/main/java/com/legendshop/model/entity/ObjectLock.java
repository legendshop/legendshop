package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *实现对象行数
 */
@Entity
@Table(name = "ls_object_lock")
public class ObjectLock implements GenericEntity<Long> {

	private static final long serialVersionUID = -846931357843473372L;

	/** 主键 */
	private Long id; 
		
	/** 对象 */
	private String obj; 
		
	/** 对象Id */
	private Long objId; 
	
	private Date recDate;
		
	
	public ObjectLock() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "OBJECT_LOCK_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "obj")
	public String  getObj(){
		return obj;
	} 
		
	public void setObj(String obj){
			this.obj = obj;
		}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Column(name = "obj_id")
	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}
		


} 
