package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.ProductProperty;
/**
 * 发布产品Dto
 * 
 * 专门用于发布界面，装载商品分类下的属性，参数和品牌
 */
public class PublishProductDto implements Serializable{

	private static final long serialVersionUID = 624277799086482609L;

	private Integer isAttrEditable;

	private Integer isParamEditable;

	/**
	 * 参数列表， TODO 重命名
	 */
	private List<ProductPropertyDto> propertyDtoList;
	
	/**
	 * 属性列表
	 */
	private List<ProductPropertyDto> specDtoList;
	
	/**
	 * 品牌列表
	 */
	private List<BrandDto> brandList;
	
	/**
	 * 个人自定义属性列表
	 */
	private List<ProductProperty> userPropList;
	

	public List<ProductPropertyDto> getPropertyDtoList() {
		return propertyDtoList;
	}

	public void setPropertyDtoList(List<ProductPropertyDto> propertyDtoList) {
		this.propertyDtoList = propertyDtoList;
	}

	public List<ProductPropertyDto> getSpecDtoList() {
		return specDtoList;
	}

	public void setSpecDtoList(List<ProductPropertyDto> specDtoList) {
		this.specDtoList = specDtoList;
	}

	public List<BrandDto> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<BrandDto> brandList) {
		this.brandList = brandList;
	}

	public List<ProductProperty> getUserPropList() {
		return userPropList;
	}

	public void setUserPropList(List<ProductProperty> userPropList) {
		this.userPropList = userPropList;
	}

	public Integer isAttrEditable() {
		return isAttrEditable;
	}

	public void setAttrEditable(Integer isAttrEditable) {
		this.isAttrEditable = isAttrEditable;
	}

	public Integer isParamEditable() {
		return isParamEditable;
	}

	public void setParamEditable(Integer isParamEditable) {
		this.isParamEditable = isParamEditable;
	}

}
