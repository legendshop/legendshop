/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.buy;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 用户购物车列表 在多商家系统中，每个商家一个购物车，一个用户拥有多个购物车，这些购物车列表就代表为一个 UserShopCartList
 *
 * @author tony
 */
public class UserShopCartList implements java.io.Serializable, Cloneable {

    private static final long serialVersionUID = -4010181120977065236L;

    private String userId;//用户Id

    private String userName;//用户名

    private String type; //订单类型 [拍卖、普通、秒杀、门店订单]

    private Long invoiceId; //发票ID

    private Invoice defaultInvoice; //默认发票

    private Integer payManner;  //支付方式

    private Double pdAmount = 0d; //预付款支付金额

    /*
     * 订单商品总数量
     */
    private int orderTotalQuanlity;

    private double orderTotalWeight;

    private double orderTotalVolume;

    /* 商品总价 */
    private double orderTotalCash;

    /* 实际总值   商品总价+运费-优惠 -优惠券 */
    private double orderActualTotal;

    /* 订单运费 */
    private double orderFreightAmount;

    /* 订单优惠总价 */
    private double allDiscount;

    private boolean isCod; //是否支持货到付款

    private String reciverName; //收货人

    private String reciverMobile; //收货人电话

    private UserAddress userAddress;  //用户地址

    private List<ShopCarts> shopCarts = new ArrayList<ShopCarts>(); //订单是按店铺区分， 对应的店铺的购物车信息。

    private List<UserCoupon> selCoupons = new ArrayList<UserCoupon>();//已选中的 优惠券

    private List<Long> userCouponIds = new ArrayList<Long>();//已选中的 优惠券ID

    private Double couponOffPrice;//总的 优惠券 优惠金额

    private String orderToken;

    /*活动Id [秒杀活动ID/团购活动id]*/
    private Long activeId;
    
    /** 用于拼团订单记录参团的团编号 */
	private String addNumber;
	
	/** 当前订单所属商家是否开启发票 1开启 0关闭*/ 
	private Integer switchInvoice;

	/**
	 * 是否需要选择默认优惠券
	 */
	private Boolean loadDefaultCoupon = true;
	
    public Invoice getDefaultInvoice() {
        return defaultInvoice;
    }

    public void setDefaultInvoice(Invoice defaultInvoice) {
        this.defaultInvoice = defaultInvoice;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public UserAddress getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(UserAddress userAddress) {
        this.userAddress = userAddress;
    }

    public Integer getPayManner() {
        return payManner;
    }

    public void setPayManner(Integer payManner) {
        this.payManner = payManner;
    }

    public void setShopCarts(List<ShopCarts> shopCarts) {
        this.shopCarts = shopCarts;
    }

    public List<ShopCarts> getShopCarts() {
        return shopCarts;
    }


    public Double getPdAmount() {
        return pdAmount;
    }

    public void setPdAmount(Double pdAmount) {
        this.pdAmount = pdAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public boolean isCod() {
        return isCod;
    }

    public void setCod(boolean isCod) {
        this.isCod = isCod;
    }

    public int getOrderTotalQuanlity() {
        return orderTotalQuanlity;
    }

    public void setOrderTotalQuanlity(int orderTotalQuanlity) {
        this.orderTotalQuanlity = orderTotalQuanlity;
    }

    public double getOrderTotalWeight() {
        return orderTotalWeight;
    }

    public void setOrderTotalWeight(double orderTotalWeight) {
        this.orderTotalWeight = orderTotalWeight;
    }

    public double getOrderTotalVolume() {
        return orderTotalVolume;
    }

    public void setOrderTotalVolume(double orderTotalVolume) {
        this.orderTotalVolume = orderTotalVolume;
    }

    public double getOrderTotalCash() {
        return orderTotalCash;
    }

    public void setOrderTotalCash(double orderTotalCash) {
        this.orderTotalCash = orderTotalCash;
    }

    public double getOrderActualTotal() {
        return orderActualTotal;
    }

    public void setOrderActualTotal(double orderActualTotal) {
        this.orderActualTotal = orderActualTotal;
    }


    public double getOrderCurrentFreightAmount() {
        Double n = 0.0;
        if (AppUtils.isNotBlank(shopCarts)) {
            for (ShopCarts shopCart : shopCarts) {
                if (!shopCart.isFreePostage())
                    n = Arith.add(shopCart.getFreightAmount(), n);
            }
        }
        return n;
    }


    public double getOrderFreightAmount() {
        return orderFreightAmount;
    }

    public void setOrderFreightAmount(double orderFreightAmount) {
        this.orderFreightAmount = orderFreightAmount;
    }

    public ShopCarts getShopCarts(Long shopId) {
        if (AppUtils.isNotBlank(shopCarts)) {
            for (int i = 0; i < shopCarts.size(); i++) {
                if (shopCarts.get(i).getShopId().equals(shopId)) {
                    return shopCarts.get(i);
                }
            }
        }
        return null;
    }

    public Integer getCityId() {
        if (AppUtils.isNotBlank(userAddress)) {
            return userAddress.getCityId();
        }
        return null;
    }

    public Integer getProvinceId() {
        if (AppUtils.isNotBlank(userAddress)) {
            return userAddress.getProvinceId();
        }
        return null;
    }

    public double getAllDiscount() {
        return allDiscount;
    }

    public void setAllDiscount(double allDiscount) {
        this.allDiscount = allDiscount;
    }

    public List<UserCoupon> getSelCoupons() {
        return selCoupons;
    }

    public void setSelCoupons(List<UserCoupon> selCoupons) {
        this.selCoupons = selCoupons;
    }

    public List<Long> getUserCouponIds() {
        return userCouponIds;
    }

    public void setUserCouponIds(List<Long> userCouponIds) {
        this.userCouponIds = userCouponIds;
    }

    public Double getCouponOffPrice() {
        return couponOffPrice;
    }

    public void setCouponOffPrice(Double couponOffPrice) {
        this.couponOffPrice = couponOffPrice;
    }

    public Long getActiveId() {
        return activeId;
    }

    public void setActiveId(Long activeId) {
        this.activeId = activeId;
    }

    public String getOrderToken() {
        return orderToken;
    }

    public void setOrderToken(String orderToken) {
        this.orderToken = orderToken;
    }

    public String getReciverName() {
        return reciverName;
    }

    public void setReciverName(String reciverName) {
        this.reciverName = reciverName;
    }

    public String getReciverMobile() {
        return reciverMobile;
    }

    public void setReciverMobile(String reciverMobile) {
        this.reciverMobile = reciverMobile;
    }

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}

	public Boolean getLoadDefaultCoupon() {
		return loadDefaultCoupon;
	}

	public void setLoadDefaultCoupon(Boolean loadDefaultCoupon) {
		this.loadDefaultCoupon = loadDefaultCoupon;
	}
}
