/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Embeddable;
import com.legendshop.dao.persistence.Id;

/**
 * 用户角色ID.
 */
@Embeddable
public class UserRoleId implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5614770818903175975L;
	
	/** The user id. */
	private String userId;
	
	/** The role id. */
	private String roleId;
	
	private String appNo;

	/**
	 * Instantiates a new user role id.
	 */
	public UserRoleId() {
	}

	/**
	 * Instantiates a new user role id.
	 * 
	 * @param userId
	 *            the user id
	 * @param roleId
	 *            the role id
	 */
	public UserRoleId(String userId, String roleId, String appNo) {
		this.userId = userId;
		this.roleId = roleId;
		this.appNo = appNo;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Id
	@Column(name="user_id")
	public String getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the role id.
	 * 
	 * @return the role id
	 */
	@Column(name="role_id")
	public String getRoleId() {
		return roleId;
	}

	/**
	 * Sets the role id.
	 * 
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}


	@Column(name="app_no")
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appNo == null) ? 0 : appNo.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRoleId other = (UserRoleId) obj;
		if (appNo == null) {
			if (other.appNo != null)
				return false;
		} else if (!appNo.equals(other.appNo))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}