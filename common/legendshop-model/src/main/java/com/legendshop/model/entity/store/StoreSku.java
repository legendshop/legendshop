package com.legendshop.model.entity.store;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 门店sku
 */
@Entity
@Table(name = "ls_store_sku")
public class StoreSku implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1668051779036508738L;

	/**  */
	private Long id; 
		
	/**  */
	private Long skuId; 
		
	/**  */
	private Long prodId; 
		
	/**  */
	private Long storeId; 
		
	/**  */
	private Long stock; 
	
	//父节点的Id
	private Long storeProdId; 
		
	
	public StoreSku() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STORE_SKU_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "store_id")
	public Long  getStoreId(){
		return storeId;
	} 
		
	public void setStoreId(Long storeId){
			this.storeId = storeId;
		}
		
    @Column(name = "stock")
	public Long  getStock(){
		return stock;
	} 
		
	public void setStock(Long stock){
			this.stock = stock;
		}

    @Column(name = "sp_id")
	public Long getStoreProdId() {
		return storeProdId;
	}

	public void setStoreProdId(Long storeProdId) {
		this.storeProdId = storeProdId;
	}
	


} 
