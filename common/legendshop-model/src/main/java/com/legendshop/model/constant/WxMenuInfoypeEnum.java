package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 微信菜单消息类型
 * @author tony
 *
 */
public enum WxMenuInfoypeEnum  implements  StringEnum{
	
	TEXT("text"),
	
	NEWS("news"),
	
	VOICE("voice"),
	
	VIDEO("video"),
	
	IMAGE("image"), //图片
	
	EXPAND("expand"), //扩展接口
	
	LINK("link");
	
	/** The value. */
	private final String value;
	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private WxMenuInfoypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
