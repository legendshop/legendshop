package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 促销活动状态
 */
public enum PromotionStatusEnum implements IntegerEnum{

	/** 未发布 **/
	NO_PUBLISH(0),

	/** 进行中 **/
	ONLINE(1),

	/** 已暂停 **/
	PAUSE(2),

	/** 已失效  **/
	OFFLINE(3),
	;


	private Integer num;

	@Override
	public Integer value() {
		return num;
	}

	private PromotionStatusEnum(Integer num) {
		this.num = num;
	}
	
}
