/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 商城类型0：个人用户，1：商家用户
 * 
 */
public enum ShopTypeEnum implements IntegerEnum {

	/** 个人用户. */
	PERSONAL(0),
	
	/** 商家用户. */
	BUSINESS(1);

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop type enum.
	 * 
	 * @param num
	 *            the num
	 */
	ShopTypeEnum(Integer num) {
		this.num = num;
	}

}
