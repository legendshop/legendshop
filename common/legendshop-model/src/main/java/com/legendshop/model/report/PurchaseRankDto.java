package com.legendshop.model.report;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 购买量排名Dto
 *
 */
public class PurchaseRankDto implements Serializable{

	private static final long serialVersionUID = 1800556397157564014L;
	
	private String userId; 

	private String userName; //用户名称

	private String shopName;//商家名称
	
	private String userMobile; //手机号码

	private Date startDate; //开始时间

	private Date endDate;//结束时间
	
	private int status;  //1 .按店铺查询, 0按用户查询
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// 用户订单总和
	private long userOeders;

	// 用户总消费金额
	private Double totalConsumption;

	private Double totalBalanceVolume;
	
	private Double totalRefundAmount;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getUserOeders() {
		return userOeders;
	}

	public void setUserOeders(long userOeders) {
		this.userOeders = userOeders;
	}

	public Double getTotalConsumption() {
		return totalConsumption;
	}

	public void setTotalConsumption(Double totalConsumption) {
		this.totalConsumption = totalConsumption;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Double getTotalBalanceVolume() {
		return totalBalanceVolume;
	}

	public void setTotalBalanceVolume(Double totalBalanceVolume) {
		this.totalBalanceVolume = totalBalanceVolume;
	}

	public Double getTotalRefundAmount() {
		return totalRefundAmount;
	}

	public void setTotalRefundAmount(Double totalRefundAmount) {
		this.totalRefundAmount = totalRefundAmount;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

}
