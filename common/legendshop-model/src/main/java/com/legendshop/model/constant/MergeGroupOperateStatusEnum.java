package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 团状态
 */
public enum MergeGroupOperateStatusEnum implements IntegerEnum{
	
	/** 进行中 */
	ING(1),
	
	/** 拼团成功 */
	SUCCESS(2),
	
	/** 拼团失败 */
	FAIL(3),
	
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private MergeGroupOperateStatusEnum(Integer num) {
		this.num = num;
	}
	
}
