/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.integral;

/**
 * 积分列表查询.
 *
 */
public class IntegralProdSeachParam {

	/** 订单流水号 */
	private String orders;

	/** 一级分类ID */
	private Long firstCid;

	/** 二级分类ID */
	private Long twoCid;

	/** 三级分类ID */
	private Long thirdCid;

	/** 积分开始 */
	private Integer startIntegral;

	/** 积分结束 */
	private Integer endIntegral;

	/** 积分关键字 */
	private String integralKeyWord;

	/**
	 * Gets the orders.
	 *
	 * @return the orders
	 */
	public String getOrders() {
		return orders;
	}

	/**
	 * Sets the orders.
	 *
	 * @param orders
	 *            the orders
	 */
	public void setOrders(String orders) {
		this.orders = orders;
	}

	/**
	 * Gets the start integral.
	 *
	 * @return the start integral
	 */
	public Integer getStartIntegral() {
		return startIntegral;
	}

	/**
	 * Sets the start integral.
	 *
	 * @param startIntegral
	 *            the start integral
	 */
	public void setStartIntegral(Integer startIntegral) {
		this.startIntegral = startIntegral;
	}

	/**
	 * Gets the end integral.
	 *
	 * @return the end integral
	 */
	public Integer getEndIntegral() {
		return endIntegral;
	}

	/**
	 * Sets the end integral.
	 *
	 * @param endIntegral
	 *            the end integral
	 */
	public void setEndIntegral(Integer endIntegral) {
		this.endIntegral = endIntegral;
	}

	/**
	 * Gets the integral key word.
	 *
	 * @return the integral key word
	 */
	public String getIntegralKeyWord() {
		return integralKeyWord;
	}

	/**
	 * Sets the integral key word.
	 *
	 * @param integralKeyWord
	 *            the integral key word
	 */
	public void setIntegralKeyWord(String integralKeyWord) {
		this.integralKeyWord = integralKeyWord;
	}

	/**
	 * Gets the first cid.
	 *
	 * @return the first cid
	 */
	public Long getFirstCid() {
		return firstCid;
	}

	/**
	 * Sets the first cid.
	 *
	 * @param firstCid
	 *            the first cid
	 */
	public void setFirstCid(Long firstCid) {
		this.firstCid = firstCid;
	}

	/**
	 * Gets the two cid.
	 *
	 * @return the two cid
	 */
	public Long getTwoCid() {
		return twoCid;
	}

	/**
	 * Sets the two cid.
	 *
	 * @param twoCid
	 *            the two cid
	 */
	public void setTwoCid(Long twoCid) {
		this.twoCid = twoCid;
	}

	/**
	 * Gets the third cid.
	 *
	 * @return the third cid
	 */
	public Long getThirdCid() {
		return thirdCid;
	}

	/**
	 * Sets the third cid.
	 *
	 * @param thirdCid
	 *            the third cid
	 */
	public void setThirdCid(Long thirdCid) {
		this.thirdCid = thirdCid;
	}

}
