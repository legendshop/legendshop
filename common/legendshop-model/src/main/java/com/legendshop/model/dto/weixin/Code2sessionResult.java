package com.legendshop.model.dto.weixin;

/**
 * 微信小程序登录的code换取openId和session 接口返回结果
 * 参见: https://developers.weixin.qq.com/miniprogram/dev/api-backend/auth.code2Session.html
 * @author 开发很忙
 */
public class Code2sessionResult extends WeiXinMsg{
	
	/** 微信用户在微信公众平台下的唯一ID */
	private String openid;
	
	/** 会话密钥 */
	private String session_key;
	
	/** 微信用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回 */
	private String unionid;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getSession_key() {
		return session_key;
	}

	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	
}
