package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品标签关联表
 */
@Entity
@Table(name = "ls_prod_tag_rel")
public class ProdTagRel implements GenericEntity<Long>{

	private static final long serialVersionUID = -7571396124663475715L;
	
	private Long id;
	private Long tagId;
	private Long prodId;
	private String prodName;
	private String pic;
	
	public ProdTagRel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProdTagRel(Long id, Long tagId, Long prodId) {
		super();
		this.id = id;
		this.tagId = tagId;
		this.prodId = prodId;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_TAG_REL_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
		this.id = id;
	}

	@Column(name = "tag_id")
	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	
	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Transient
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	@Transient
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
	
	
	
}
