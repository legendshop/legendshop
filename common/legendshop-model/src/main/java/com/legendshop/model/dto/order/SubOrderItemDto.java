package com.legendshop.model.dto.order;

import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Tony
 *
 */
@ApiModel("订单商品Dto")
public class SubOrderItemDto  implements GenericEntity<Long>{

	private static final long serialVersionUID = 1L;

	/** 订单项id */
	private Long subItemId;
	
	/** 订单项流水号 */
	private String subItemNumber;
	
	/** 商品id */
	private Long prodId;
	
	/** skuID */
	private Long skuId;
	
	/** 商品快照id */
	private Long snapshotId;
	
	/** 购物车商品数量 */
	private long basketCount;
	
	/** 商品名称 */
	private String prodName;
	
	/** 商品属性 */
	private String attribute;
	
	/** 商品图片 */
	private String pic;
	
	/** 商品现价 */
	private Double cash;  
	
	/** 商品原价 */
	private Double price;
	
	/** 商品总金额 */
	private Double productTotalAmout;
	
	/** 获得积分数 */
	private Double obtainIntegral;
	
	/** 物流重量(千克) */
	private Double weight;
	
	/** 物流体积(立方米) */
	private Double volume;
	
	/** 会员直接上级分销会员 */
	private String distUserName;
	
	/** 分销的佣金金额 */
	private Double distCommisCash;
	
	/** 促销信息备注 */
	private String promotionInfo;
	
	/** 评论状态： 0 未评价 1 已评价 */
	private Integer commSts;
	
	/** 退款/退货记录ID */
	private Long refundId;
	
	/** 退款/退货状态 */
	private Long refundState;
	
	/** 退款金额 */
	private Double refundAmount;
	
	/** 申请类型 1:仅退款,2:退货*/
	private Long refundType;
	
	/** 退货数量 */
	private Long refundCount;

	/** 是否在退换货有效时间内 */
	private Boolean isInReturnValidPeriod;

	@Override
	public Long getId() {
		return subItemId;
	}

	@Override
	public void setId(Long id) {
		subItemId=id;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}
	
	public String getSubItemNumber() {
		return subItemNumber;
	}

	public void setSubItemNumber(String subItemNumber) {
		this.subItemNumber = subItemNumber;
	}
	

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(Long snapshotId) {
		this.snapshotId = snapshotId;
	}

	public long getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(long basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getProductTotalAmout() {
		return productTotalAmout;
	}

	public void setProductTotalAmout(Double productTotalAmout) {
		this.productTotalAmout = productTotalAmout;
	}

	public Double getObtainIntegral() {
		return obtainIntegral;
	}

	public void setObtainIntegral(Double obtainIntegral) {
		this.obtainIntegral = obtainIntegral;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getDistUserName() {
		return distUserName;
	}

	public void setDistUserName(String distUserName) {
		this.distUserName = distUserName;
	}

	public Double getDistCommisCash() {
		return distCommisCash;
	}

	public void setDistCommisCash(Double distCommisCash) {
		this.distCommisCash = distCommisCash;
	}

	public String getPromotionInfo() {
		return promotionInfo;
	}

	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}

	public Integer getCommSts() {
		return commSts;
	}

	public void setCommSts(Integer commSts) {
		this.commSts = commSts;
	}

	/**
	 * @return the refundId
	 */
	public Long getRefundId() {
		return refundId;
	}

	/**
	 * @param refundId the refundId to set
	 */
	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	/**
	 * @return the refundState
	 */
	public Long getRefundState() {
		return refundState;
	}

	/**
	 * @param refundState the refundState to set
	 */
	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	/**
	 * @return the refundAmount
	 */
	public Double getRefundAmount() {
		return refundAmount;
	}

	/**
	 * @param refundAmount the refundAmount to set
	 */
	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	/**
	 * @return the refundType
	 */
	public Long getRefundType() {
		return refundType;
	}

	/**
	 * @param refundType the refundType to set
	 */
	public void setRefundType(Long refundType) {
		this.refundType = refundType;
	}

	/**
	 * @return the refundCount
	 */
	public Long getRefundCount() {
		return refundCount;
	}

	/**
	 * @param refundCount the refundCount to set
	 */
	public void setRefundCount(Long refundCount) {
		this.refundCount = refundCount;
	}

	public Boolean getInReturnValidPeriod() {
		return isInReturnValidPeriod;
	}

	public void setInReturnValidPeriod(Boolean inReturnValidPeriod) {
		isInReturnValidPeriod = inReturnValidPeriod;
	}
}
