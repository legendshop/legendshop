/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 礼券
 * 礼券提供方：平台: platform，店铺:shop
 */
public enum CouponProviderEnum implements StringEnum {

	/** 平台*/
	PLATFORM("platform"),

	/** 店铺*/
	SHOP("shop"); 

	/** The num. */
	private String type;

	CouponProviderEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}

}
