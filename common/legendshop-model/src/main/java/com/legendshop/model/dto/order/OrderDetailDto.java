package com.legendshop.model.dto.order;

import java.util.Date;


/**
 * 用于封装查询订单数据
 * @author tony
 *
 */
public class OrderDetailDto {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long subId;

	private String subNum;
	/**
	 * 总交易额
	 */
	private double actualTotal;

	private double freightAmount;
	
	private double 	total; //商品总价(不包含任何其他价格)

	private Integer status;

	private Date subDate;

	private Long shopId;

	private String shopName;
	
	private Integer payManner;
	
	private String payTypeId;
	
	private String payTypeName;
	
	private Date payDate;
	
	/** The pay date. */
	private Date dvyDate; // 发货时间
	
	private Date finallyDate; // 完成时间/确认收货时间
	
	private Date updateDate;
	
	private String orderRemark;
	
    private boolean ispay;
    
	private String userName;
	
	private String userId;
	
	private String dvyType;
	
	private Long dvyTypeId;
	
	private String subType;
	
	private String dvyFlowId;
	
	private Integer productNums;//订单商品总数 
	
	private String prodName;
	
	
	private SubOrderItemDto subOrderItemDto;
	
  
	private UsrAddrSubDto usrAddrSubDto;
	

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNum() {
		return subNum;
	}

	public void setSubNum(String subNum) {
		this.subNum = subNum;
	}

	public double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	public boolean isIspay() {
		return ispay;
	}

	public void setIspay(boolean ispay) {
		this.ispay = ispay;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public SubOrderItemDto getSubOrderItemDto() {
		return subOrderItemDto;
	}

	public void setSubOrderItemDto(SubOrderItemDto subOrderItemDto) {
		this.subOrderItemDto = subOrderItemDto;
	}

	public UsrAddrSubDto getUsrAddrSubDto() {
		return usrAddrSubDto;
	}

	public void setUsrAddrSubDto(UsrAddrSubDto usrAddrSubDto) {
		this.usrAddrSubDto = usrAddrSubDto;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	
}
