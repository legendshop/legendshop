package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 红包 店铺 关联表
 */
@Entity
@Table(name = "ls_coupon_shop")
public class CouponShop implements GenericEntity<Long> {

	private static final long serialVersionUID = -3099580127144134693L;

	/** id */
	private Long couponShopId; 
		
	/** 优惠券id */
	private Long couponId; 
		
	/** 店铺id */
	private Long shopId; 
		
	
	public CouponShop() {}
		
	@Id
	@Column(name = "coupon_shop_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "COUPON_SHOP_SEQ")
	public Long  getCouponShopId(){
		return couponShopId;
	} 
		
	public void setCouponShopId(Long couponShopId){
		this.couponShopId = couponShopId;
	}
		
    @Column(name = "coupon_id")
	public Long  getCouponId(){
		return couponId;
	} 
		
	public void setCouponId(Long couponId){
		this.couponId = couponId;
	}
		
    @Column(name = "shop_id")
    public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	@Transient
	public Long getId() {
		return couponShopId;
	}
	
	public void setId(Long id) {
		couponShopId = id;
	}

} 
