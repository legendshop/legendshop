package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *商家订单结算表
 */
@Entity
@Table(name = "ls_shop_order_bill")
@ApiModel(value="ShopOrderBill") 
public class ShopOrderBill implements GenericEntity<Long> {
	private static final long serialVersionUID = 627000605726634484L;

	/**  */
	@ApiModelProperty(value="主键Id")  
	private Long id; 
	
	/**档期标示*/
	@ApiModelProperty(value="档期标示")  
	private String flag;
		
	/** 订单结算编号 */
	@ApiModelProperty(value="订单结算编号")  
	private String sn; 
		
	/** 开始时间 */
	@ApiModelProperty(value="开始时间")  
	private Date startDate; 
		
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")  
	private Date endDate; 
		
	/** 这期订单的实际金额 */
	@ApiModelProperty(value="这期订单的实际金额") 
	private Double orderAmount; 
		
	/** 这期的运费 */
	@ApiModelProperty(value="这期的运费") 
	private Double shippingTotals; 
		
	/** 退单金额 */
	@ApiModelProperty(value="退单金额") 
	private Double orderReturnTotals; 
		
	/** 平台佣金金额 */
	@ApiModelProperty(value="平台佣金金额") 
	private Double commisTotals; 
	
	/** 分销佣金金额 */
	@ApiModelProperty(value="分销佣金金额") 
	private Double distCommisTotals; 
		
	/** 退还佣金 */
	@ApiModelProperty(value="退还佣金") 
	private Double commisReturnTotals; 
	
	/** 退还佣金总数 */
	@ApiModelProperty(value="退还佣金总数") 
	private Double redpackTotals; 
		
	/** 应结金额 */
	@ApiModelProperty(value="应结金额") 
	private Double resultTotalAmount; 
		
	/** 生成结算单日期 */
	@ApiModelProperty(value="生成结算单日期") 
	private Date createDate; 
		
	/** 结算单年月  例如：201208 */
	@ApiModelProperty(value="结算单年月  例如：201208") 
	private String month; 
		
	/** 1默认 2店家已确认  3平台已审核  4结算完成 */
	@ApiModelProperty(value="结算单状态，1默认 2店家已确认  3平台已审核  4结算完成") 
	private Integer status; 
		
	/** 付款日期 */
	@ApiModelProperty(value="付款日期") 
	private Date payDate; 
		
	/** 支付备注 */
	@ApiModelProperty(value="支付备注") 
	private String payContent; 
		
	/** 店铺ID */
	@ApiModelProperty(value="店铺ID") 
	private Long shopId; 
		
	/** 店铺名 */
	@ApiModelProperty(value="店铺名") 
	private String shopName; 
		
	/** 管理员备注 */
	@ApiModelProperty(value="管理员备注") 
	private String adminNote;

	/** 商城结算日 */
	@ApiModelProperty(value = "商城结算日")
	private Integer shopBillDay;

	/** 普通订单集合 */
	@ApiModelProperty(value = "普通订单集合")
	private AppPageSupport<Sub> subList;

	/** 退款订单集合 */
	@ApiModelProperty(value = "退款订单集合")
	private AppPageSupport<SubRefundReturn> subRefundReturnList;

	/** 当前档期使用的平台结算佣金比例 */
	@ApiModelProperty(value="当前档期使用的平台结算佣金比例")
	private Double commisRate;

	/** 当前档期退单红包金额 */
	@ApiModelProperty(value="当前档期退单红包金额")
	private Double orderReturnRedpackTotals;

	/** 档期内结算的预售定金总额 */
	@ApiModelProperty(value="档期内结算的预售定金总额")
	private Double preDepositPriceTotal;

	/** 档期内结算的拍卖保证金总额 */
	@ApiModelProperty(value="档期内结算的拍卖保证金总额")
	private Double auctionDepositTotal;


	public ShopOrderBill() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_ORDER_BILL_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "sn")
	public String  getSn(){
		return sn;
	} 
		
	public void setSn(String sn){
			this.sn = sn;
		}
		
    @Column(name = "start_date")
	public Date  getStartDate(){
		return startDate;
	} 
		
	public void setStartDate(Date startDate){
			this.startDate = startDate;
		}
		
    @Column(name = "end_date")
	public Date  getEndDate(){
		return endDate;
	} 
		
	public void setEndDate(Date endDate){
			this.endDate = endDate;
		}
		
    @Column(name = "order_amount")
	public Double  getOrderAmount(){
		return orderAmount;
	} 
		
	public void setOrderAmount(Double orderAmount){
			this.orderAmount = orderAmount;
		}
		
    @Column(name = "shipping_totals")
	public Double  getShippingTotals(){
		return shippingTotals;
	} 
		
	public void setShippingTotals(Double shippingTotals){
			this.shippingTotals = shippingTotals;
		}
		
    @Column(name = "order_return_totals")
	public Double  getOrderReturnTotals(){
		return orderReturnTotals;
	} 
		
	public void setOrderReturnTotals(Double orderReturnTotals){
			this.orderReturnTotals = orderReturnTotals;
		}
		
    @Column(name = "commis_totals")
	public Double  getCommisTotals(){
		return commisTotals;
	} 
		
	public void setCommisTotals(Double commisTotals){
			this.commisTotals = commisTotals;
		}
		
    @Column(name = "commis_return_totals")
	public Double  getCommisReturnTotals(){
		return commisReturnTotals;
	} 
		
	public void setCommisReturnTotals(Double commisReturnTotals){
			this.commisReturnTotals = commisReturnTotals;
		}
		
    @Column(name = "result_total_amount")
	public Double  getResultTotalAmount(){
		return resultTotalAmount;
	} 
		
	public void setResultTotalAmount(Double resultTotalAmount){
			this.resultTotalAmount = resultTotalAmount;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "month")
	public String  getMonth(){
		return month;
	} 
		
	public void setMonth(String month){
			this.month = month;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "pay_date")
	public Date  getPayDate(){
		return payDate;
	} 
		
	public void setPayDate(Date payDate){
			this.payDate = payDate;
		}
		
    @Column(name = "pay_content")
	public String  getPayContent(){
		return payContent;
	} 
		
	public void setPayContent(String payContent){
			this.payContent = payContent;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_name")
	public String  getShopName(){
		return shopName;
	} 
		
	public void setShopName(String shopName){
			this.shopName = shopName;
		}
		
    @Column(name = "admin_note")
	public String  getAdminNote(){
		return adminNote;
	} 
		
	public void setAdminNote(String adminNote){
			this.adminNote = adminNote;
		}

	@Column(name = "flag")
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Column(name = "dist_commis_totals")
	public Double getDistCommisTotals() {
		return distCommisTotals;
	}

	public void setDistCommisTotals(Double distCommisTotals) {
		this.distCommisTotals = distCommisTotals;
	}

	@Column(name = "redpack_totals")
	public Double getRedpackTotals() {
		return redpackTotals;
	}

	public void setRedpackTotals(Double redpackTotals) {
		this.redpackTotals = redpackTotals;
	}

	@Column(name = "commis_rate")
	public Double getCommisRate() {
		return commisRate;
	}

	public void setCommisRate(Double commisRate) {
		this.commisRate = commisRate;
	}

	@Column(name = "order_return_redpack_totals")
	public Double getOrderReturnRedpackTotals() {
		return orderReturnRedpackTotals;
	}

	public void setOrderReturnRedpackTotals(Double orderReturnRedpackTotals) {
		this.orderReturnRedpackTotals = orderReturnRedpackTotals;
	}

	@Column(name = "pre_deposit_price_total")
	public Double getPreDepositPriceTotal() {
		return preDepositPriceTotal;
	}

	public void setPreDepositPriceTotal(Double preDepositPriceTotal) {
		this.preDepositPriceTotal = preDepositPriceTotal;
	}

	@Column(name = "auction_deposit_total")
	public Double getAuctionDepositTotal() {
		return auctionDepositTotal;
	}

	public void setAuctionDepositTotal(Double auctionDepositTotal) {
		this.auctionDepositTotal = auctionDepositTotal;
	}

	@Transient
	public Integer getShopBillDay() {
		return shopBillDay;
	}

	public void setShopBillDay(Integer shopBillDay) {
		this.shopBillDay = shopBillDay;
	}

	@Transient
	public AppPageSupport<Sub> getSubList() {
		return subList;
	}

	public void setSubList(AppPageSupport<Sub> subList) {
		this.subList = subList;
	}

	@Transient
	public AppPageSupport<SubRefundReturn> getSubRefundReturnList() {
		return subRefundReturnList;
	}

	public void setSubRefundReturnList(AppPageSupport<SubRefundReturn> subRefundReturnList) {
		this.subRefundReturnList = subRefundReturnList;
	}
}
