/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Embeddable;

/**
 * 权限Id
 */
@Embeddable
public class PerssionId implements java.io.Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5510312063680243453L;
	
	/** The role id. */
	private String roleId;
	
	/** The function id. */
	private String functionId;

	// Constructors

	/**
	 * default constructor.
	 */
	public PerssionId() {
	}

	/**
	 * full constructor.
	 * 
	 * @param roleId
	 *            the role id
	 * @param functionId
	 *            the function id
	 */
	public PerssionId(String roleId, String functionId) {
		this.roleId = roleId;
		this.functionId = functionId;
	}

	// Property accessors

	/**
	 * Gets the role id.
	 * 
	 * @return the role id
	 */
	@Column(name="role_id")
	public String getRoleId() {
		return this.roleId;
	}

	/**
	 * Sets the role id.
	 * 
	 * @param roleId
	 *            the new role id
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * Gets the function id.
	 * 
	 * @return the function id
	 */
	@Column(name="function_id")
	public String getFunctionId() {
		return this.functionId;
	}

	/**
	 * Sets the function id.
	 * 
	 * @param functionId
	 *            the new function id
	 */
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((functionId == null) ? 0 : functionId.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerssionId other = (PerssionId) obj;
		if (functionId == null) {
			if (other.functionId != null)
				return false;
		} else if (!functionId.equals(other.functionId))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}

	
}