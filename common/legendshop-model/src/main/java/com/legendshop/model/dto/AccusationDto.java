package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;
/**
 * 拍卖Dto
 *
 */
public class AccusationDto implements Serializable{

	private static final long serialVersionUID = -8414979356767710211L;

	private Long id ; 
	
	// 产品ID
	private Long prodId ; 
	
	// 处理结果
	private Long result ; 
	
	//产品名称
	private String prodName;

	//产品图片
	private String prodPic;
	
	// 举报时间
	private Date recDate ; 
	
	//举报类型
	private String accuType;
	
	// 处理结果
	private String handleInfo ;
	
	//状态
	private Long status;
	
	public AccusationDto(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public String getAccuType() {
		return accuType;
	}

	public void setAccuType(String accuType) {
		this.accuType = accuType;
	}

	public String getHandleInfo() {
		return handleInfo;
	}

	public void setHandleInfo(String handleInfo) {
		this.handleInfo = handleInfo;
	}

	public Long getResult() {
		return result;
	}

	public void setResult(Long result) {
		this.result = result;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	} 
}
