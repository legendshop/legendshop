/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 营销活动 计算类型：0:按金额  1：按件.
 */
public enum MarketingRuleCalTypeEnum implements IntegerEnum {
	
	/**
	 * 按金额
	 */
	BY_MONEY(0),
	
	/**
	 * 按件数
	 */
	BY_NUMBER(1); 
	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	MarketingRuleCalTypeEnum(Integer num) {
		this.num = num;
	}

}
