/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * @author tony
 * 
 */
public enum IndexObjectTypeCodeEnum implements StringEnum {

	/** 商品 0. */
	PRODUCT("product"),

	/** 用户 1. */
	USER("user"),

	/** 文章 2. */
	ARTICLE("aricle");
	

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private IndexObjectTypeCodeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	public static IndexObjectTypeCodeEnum fromCode(String code) {
		try {
			for(IndexObjectTypeCodeEnum codeEnum:values()){
				if(codeEnum.value.equals(code)){
					return codeEnum;
				}
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}
	
	

}
