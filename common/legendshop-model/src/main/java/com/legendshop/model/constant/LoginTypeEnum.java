/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 登录方式
 * 密码登录还是第三方登录
 */
public enum LoginTypeEnum implements StringEnum {
	/**  
	 * 密码登录
	 */
	USERNAME_PASSWORD("UP"),
	
	/** 管理员登录 **/
	ADMIN_LOGIN("AL"),
	
	/**
	 * ThirdPart
	 */
	THIRD_PART("TP");

	private final String value;

	private LoginTypeEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}


}
