/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;


/**
 * 直播商品.
 */
@Entity
@Table(name = "ls_zhibo_prod")
public class ZhiboProd implements GenericEntity<Long> {

	private static final long serialVersionUID = 3205149036718193556L;

	/**  id. */
	private Long id; 
		
	/**  av房间ID. */
	private Long avRoomId; 
		
	/**  产品ID. */
	private Long prodId; 
		
	
	/**
	 * Instantiates a new zhibo prod.
	 */
	public ZhiboProd() {
    }
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ZHIBO_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id){
			this.id = id;
		}
		
    /**
     * Gets the av room id.
     *
     * @return the av room id
     */
    @Column(name = "av_room_id")
	public Long  getAvRoomId(){
		return avRoomId;
	} 
		
	/**
	 * Sets the av room id.
	 *
	 * @param avRoomId the new av room id
	 */
	public void setAvRoomId(Long avRoomId){
			this.avRoomId = avRoomId;
		}
		
    /**
     * Gets the prod id.
     *
     * @return the prod id
     */
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
	


} 
