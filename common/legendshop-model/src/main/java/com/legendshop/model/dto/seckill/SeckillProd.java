/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.util.List;

/**
 * 秒杀商品
 */
public class SeckillProd implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 商品id. */
	private Long prodId;
	
	/** 秒杀库存 */
	private Long seckillStock;
	
	/** 秒杀价格 */
	private Double  seckillPrice;
    
    /** 是否有sku */
    private boolean existSku;
    
    /** 秒杀sku */
    private List<SeckillSku> seckillSkus;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSeckillStock() {
		return seckillStock;
	}

	public void setSeckillStock(Long seckillStock) {
		this.seckillStock = seckillStock;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public boolean isExistSku() {
		return existSku;
	}

	public void setExistSku(boolean existSku) {
		this.existSku = existSku;
	}

	public List<SeckillSku> getSeckillSkus() {
		return seckillSkus;
	}

	public void setSeckillSkus(List<SeckillSku> seckillSkus) {
		this.seckillSkus = seckillSkus;
	}
    
}
