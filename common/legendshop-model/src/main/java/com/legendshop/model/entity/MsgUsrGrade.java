/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.io.Serializable;

import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.Table;

/**
 * The Class MsgUsrGrade.
 */
@Entity
@Table(name = "ls_msg_usrgrad")
public class MsgUsrGrade implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8255398692960644001L;

	/** The msg id. */
	private Long msgId;

	/** The grade id. */
	private Integer gradeId;

	/**
	 * Gets the msg id.
	 * 
	 * @return the msg id
	 */
	public Long getMsgId() {
		return msgId;
	}

	/**
	 * Sets the msg id.
	 * 
	 * @param msgId
	 *            the new msg id
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	/**
	 * Gets the grade id.
	 * 
	 * @return the grade id
	 */
	public Integer getGradeId() {
		return gradeId;
	}

	/**
	 * Sets the grade id.
	 * 
	 * @param gradeId
	 *            the new grade id
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object == this) {
			return true;
		}
		if (object instanceof MsgUsrGrade) {
			if (this.msgId.equals(((MsgUsrGrade) object).getMsgId())
					&& this.gradeId.equals(((MsgUsrGrade) object).getGradeId()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (msgId == null) {
			return (gradeId == null) ? 0 : gradeId.hashCode() + 1;
		} else if (gradeId == null) {
			return msgId.hashCode() + 2;
		} else {
			return msgId.hashCode() * 17 + gradeId.hashCode();
		}
	}

}
