/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 邮件表
 */
@Entity
@Table(name = "ls_mail")
public class MailEntity implements GenericEntity<Long>{

	private static final long serialVersionUID = -8960128411388462590L;

	// 
	/** The id. */
	private Long id ; 
		
	// 发送用户名称
	/** The send name. */
	private String sendName ; 
		
	// 用户等级
	/** The user level. */
	private Integer userLevel ; 
		
	// 邮件内容
	/** The mail content. */
	private String mailContent ; 
		
	// 邮件类型，0表示群发，1表示单个邮件发送
	/** The type. */
	private Integer type ; 
		
	// 建立时间
	private Date recTime ; 
	
	/** 邮件类型. */
	private String category;
	
	private String mailCode; //邮件验证码
	
	private Integer status;
		
	
	/**
	 * Instantiates a new mail entity.
	 */
	public MailEntity() {
    }

		
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id){
		this.id = id ;
	}
		
		
	/**
	 * Gets the send name.
	 *
	 * @return the send name
	 */
	@Column(name = "send_name")
	public String  getSendName(){
		return sendName ;
	} 
		
	/**
	 * Sets the send name.
	 *
	 * @param sendName the new send name
	 */
	public void setSendName(String sendName){
		this.sendName = sendName ;
	}
		
		
	/**
	 * Gets the user level.
	 *
	 * @return the user level
	 */
	@Column(name = "user_level")
	public Integer  getUserLevel(){
		return userLevel ;
	} 
		
	/**
	 * Sets the user level.
	 *
	 * @param userLevel the new user level
	 */
	public void setUserLevel(Integer userLevel){
		this.userLevel = userLevel ;
	}
		
		
	/**
	 * Gets the mail content.
	 *
	 * @return the mail content
	 */
	@Column(name = "mail_content")
	public String  getMailContent(){
		return mailContent ;
	} 
		
	/**
	 * Sets the mail content.
	 *
	 * @param mailContent the new mail content
	 */
	public void setMailContent(String mailContent){
		this.mailContent = mailContent ;
	}
		
		
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	@Column(name = "type")
	public Integer  getType(){
		return type ;
	} 
		
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type){
		this.type = type ;
	}
		
		
	/**
	 * Gets the rec time.
	 *
	 * @return the rec time
	 */
	@Column(name = "rec_time")
	public Date  getRecTime(){
		return recTime ;
	} 
		
	/**
	 * Sets the rec time.
	 *
	 * @param recTime the new rec time
	 */
	public void setRecTime(Date recTime){
		this.recTime = recTime ;
	}
		
	
  /* (non-Javadoc)
   * @see com.legendshop.model.entity.BaseEntity#getId()
   */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MAIL_SEQ")
 	public Long getId() {
		return 	id;
	}


	@Column(name = "category")
	public String getCategory() {
		return category;
	}
	
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	
	@Column(name = "mail_code")
	public String getMailCode() {
		return mailCode;
	}
	
	
	public void setMailCode(String mailCode) {
		this.mailCode = mailCode;
	}
	
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	
	
	public void setStatus(Integer status) {
		this.status = status;
	}

} 
