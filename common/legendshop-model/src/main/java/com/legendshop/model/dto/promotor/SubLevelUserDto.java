/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import java.io.Serializable;
import java.util.Date;

/**
 * 分销下级用户DTO.
 */
public class SubLevelUserDto implements Serializable{

	private static final long serialVersionUID = 471562742691105059L;

	/** The user id. */
	private String userId;
	
	/** The user name. */
	private String userName;
	
	/** The nick name. */
	private String nickName;
	
	/** The user reg time. */
	private Date userRegTime;
	
	/** The total dist commis. */
	private Double totalDistCommis;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the nick name.
	 *
	 * @return the nick name
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * Sets the nick name.
	 *
	 * @param nickName the nick name
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * Gets the user reg time.
	 *
	 * @return the user reg time
	 */
	public Date getUserRegTime() {
		return userRegTime;
	}

	/**
	 * Sets the user reg time.
	 *
	 * @param userRegTime the user reg time
	 */
	public void setUserRegTime(Date userRegTime) {
		this.userRegTime = userRegTime;
	}

	/**
	 * Gets the total dist commis.
	 *
	 * @return the total dist commis
	 */
	public Double getTotalDistCommis() {
		return totalDistCommis;
	}

	/**
	 * Sets the total dist commis.
	 *
	 * @param totalDistCommis the total dist commis
	 */
	public void setTotalDistCommis(Double totalDistCommis) {
		this.totalDistCommis = totalDistCommis;
	}
	
	
}
