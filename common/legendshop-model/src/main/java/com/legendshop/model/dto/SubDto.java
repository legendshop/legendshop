package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class SubDto implements Serializable{
	
	private String subNumber;//订单编号
	private Date payDate;//下单时间
	private Date finallyDate;//成交时间
	private Double actualTotal;//订单金额
	private Double freightAmount;//运费
	private Double redpackOffPrice;//红包金额
	
	public String getSubNumber() {
		return subNumber;
	}
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public Date getFinallyDate() {
		return finallyDate;
	}
	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}
	public Double getActualTotal() {
		return actualTotal;
	}
	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}
	public Double getFreightAmount() {
		return freightAmount;
	}
	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}
	public Double getRedpackOffPrice() {
		return redpackOffPrice;
	}
	public void setRedpackOffPrice(Double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}
	
}
