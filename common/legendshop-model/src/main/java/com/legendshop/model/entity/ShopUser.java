package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家用户表
 */
@Entity
@Table(name = "ls_shop_user")
public class ShopUser implements GenericEntity<Long> {

	private static final long serialVersionUID = -5037060067739384001L;

	/** 用户ID */
	private Long id; 
		
	/** 店铺ID */
	private Long shopId; 
		
	/** 对应的角色ID */
	private Long shopRoleId; 
	
	/** 对应的角色 */
	private String roleName; 
		
	/** 名称 */
	private String name; 
		
	/** 密码 */
	private String password; 
		
	/** 状态 */
	private boolean enabled; 
		
	/** 注释 */
	private String note; 
		
	/** 手机号码 */
	private String mobile; 
		
	/** 名字 */
	private String realName; 
		
	/** 更新时间 */
	private Date modifyDate; 
		
	/** 建立时间 */
	private Date recDate; 
		
	
	public ShopUser() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_USER_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_role_id")
	public Long  getShopRoleId(){
		return shopRoleId;
	} 
		
	public void setShopRoleId(Long shopRoleId){
			this.shopRoleId = shopRoleId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "password")
	public String  getPassword(){
		return password;
	} 
		
	public void setPassword(String password){
			this.password = password;
		}
		
    @Column(name = "enabled")
	public boolean  getEnabled(){
		return enabled;
	} 
		
	public void setEnabled(boolean enabled){
			this.enabled = enabled;
		}
		
    @Column(name = "note")
	public String  getNote(){
		return note;
	} 
		
	public void setNote(String note){
			this.note = note;
		}
		
    @Column(name = "mobile")
	public String  getMobile(){
		return mobile;
	} 
		
	public void setMobile(String mobile){
			this.mobile = mobile;
		}
		
    @Column(name = "real_name")
	public String  getRealName(){
		return realName;
	} 
		
	public void setRealName(String realName){
			this.realName = realName;
		}
		
    @Column(name = "modify_date")
	public Date  getModifyDate(){
		return modifyDate;
	} 
		
	public void setModifyDate(Date modifyDate){
			this.modifyDate = modifyDate;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}

	@Transient
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	


} 
