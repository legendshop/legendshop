/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

/**
 * 检查团购Dto
 *
 */
public class GroupExposerDto {

	/** 团购活动id */
	private Long id;

	/** 团购加密Token. */
	private String token;

	/** 是否导出 */
	private boolean isExpose;

	/** 信息 */
	private String msg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isExpose() {
		return isExpose;
	}

	public void setExpose(boolean isExpose) {
		this.isExpose = isExpose;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
