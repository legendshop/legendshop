package com.legendshop.model.dto.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.UserAddressSub;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 我的订单DTO
 * 
 * @author tony
 * 
 */
@ApiModel(value="MySubDto我的订单") 
public class MySubDto  implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;
	
	/** 主键ID */
	@ApiModelProperty(value="主键ID") 
	private Long id;
	
	/** 订购ID */
	@ApiModelProperty(value="订购ID") 
	private Long subId;

	/** 产品名称,多个产品将会以逗号隔开 */
	@ApiModelProperty(value="产品名称,多个产品将会以逗号隔开") 
	private String prodName;

	/** 订购流水号 */
	@ApiModelProperty(value="订购流水号") 
	private String subNum;
	
	/** 订购用户ID */
	@ApiModelProperty(value="订购用户ID") 
	private String userId;
	
	/** 订购用户名称 */
	@ApiModelProperty(value="订购用户名称") 
	private String userName;
	
	/** 订购时间 */
	@ApiModelProperty(value="订购时间") 
	private Date subDate;

	/** 订单类型[预售商品订单] */
	@ApiModelProperty(value="订单类型[预售商品订单]") 
	private String subType;
	
	/** 商品总价(不包含任何其他价格) */
	@ApiModelProperty(value="商品总价(不包含任何其他价格)") 
	private Double 	total; 
	
	/** 实际总值 */
	@ApiModelProperty(value="实际总值") 
	private Double actualTotal;
	
	/** 支付方式(1:货到付款,2:在线支付) */
	@ApiModelProperty(value="支付方式(1:货到付款,2:在线支付)") 
	private Integer payManner;
	
	/** 付款类型ID */
	@ApiModelProperty(value="付款类型ID") 
	private String payTypeId;
	
	/** 支付类型名称 */
	@ApiModelProperty(value="支付类型名称") 
	private String payTypeName;
	
	/** 支付时间 */
	@ApiModelProperty(value="支付时间") 
	private Date payDate;
	
	/**商家备注**/
	@ApiModelProperty(value="商家备注")
	private String shopRemark;
	
	/**是否已经备注**/
	@ApiModelProperty(value="是否已经备注")
	private Boolean isShopRemarked;
	
	/**备注时间**/
	@ApiModelProperty(value="备注时间")
	private Date shopRemarkDate;

	@ApiModelProperty(value="其他备注") 
	private String other;
	
	/** 所属店铺 */
	@ApiModelProperty(value="所属店铺") 
	private Long shopId;

	/** 商城名称 */
	@ApiModelProperty(value="商城名称") 
	private String shopName;
	
	/** 订单状态,参见OrderStatusEnum */
	@ApiModelProperty(value="订单状态,参见OrderStatusEnum") 
	private Integer status;
	
	/** 使用积分数 */
	@ApiModelProperty(value="使用积分数") 
	private Integer score;
	
	/** 配送类型 */
	@ApiModelProperty(value="配送类型") 
	private String dvyType;
	
	/** 配送方式ID */
	@ApiModelProperty(value="配送方式ID") 
	private Long dvyTypeId;
	
	/** 物流单号 */
	@ApiModelProperty(value="物流单号") 
	private String dvyFlowId; 
	
	/** 0:默认,1:在处理,2:处理完成 */
	@ApiModelProperty(value="0:默认,1:在处理,2:处理完成") 
	private Long refundState;
	
	/** 发票单号 **/
	@ApiModelProperty(value="发票单号") 
	private Long invoiceSubId;
	
	/** 给买家留言 */
	@ApiModelProperty(value="给买家留言") 
	private String orderRemark;
	
	/** 用户订单地址Id */
	@ApiModelProperty(value="用户订单地址Id") 
	private Long addrOrderId;
	
	/** 是否需要发票(0:不需要;1:需要) */
	@ApiModelProperty(value="是否需要发票(0:不需要;1:需要)") 
	private Boolean isNeedInvoice;
	
	/** 订单物流重量(千克) */
	@ApiModelProperty(value="订单物流重量(千克)") 
	private Double weight;
	
	/** 订单物流体积(立方米) */
	@ApiModelProperty(value="订单物流体积(立方米)") 
	private Double volume;
	
	/** 订单商品总数 */
	@ApiModelProperty(value="订单商品总数") 
	private Integer productNums; 
	
	/** 发货时间 */
	@ApiModelProperty(value="发货时间") 
	private Date dvyDate; 
	
	/** 完成时间/确认收货时间 */
	@ApiModelProperty(value="完成时间/确认收货时间") 
	private Date finallyDate; 
	
	/** 订单更新时间 */
	@ApiModelProperty(value="订单更新时间") 
	private Date updateDate;
	
	/** 是否货到付款 */
	@ApiModelProperty(value="是否货到付款") 
	private Boolean isCod;
	
	/** 是否已经支付，1：已经支付过，0：，没有支付过 */
	@ApiModelProperty(value="是否已经支付，1：已经支付过，0：，没有支付过") 
	private boolean ispay;
	
	/** 用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除 */
	@ApiModelProperty(value="用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除") 
	private Integer deleteStatus;
	
	/** 分销的佣金总额 */
	@ApiModelProperty(value="分销的佣金总额") 
	private Double  distCommisAmount;
	
	/** 优惠总金额 */
	@ApiModelProperty(value="优惠总金额") 
	private Double discountPrice;
	
	/** 订单运费 */
	@ApiModelProperty(value="订单运费") 
	private Double freightAmount;
	
	/** 收件人 */
	@ApiModelProperty(value="收件人") 
	private String reciverName;
	
	/** 收件人电话 */
	@ApiModelProperty(value="收件人电话") 
	private String reciverMobile;

	/** 预售skuId */
	@ApiModelProperty(value="预售skuId")
	private String skuId;
	
	/** 使用红包支付金额 */
	@ApiModelProperty(value="使用红包支付金额") 
	private Double redpackOffPrice;
	
	/** 使用优惠券支付金额 */
	@ApiModelProperty(value="使用优惠券支付金额") 
	private Double couponOffPrice;
	
	/** 是否提醒过发货  */
	@ApiModelProperty(value="是否提醒过发货") 
	private boolean remindDelivery;
	
	/** 提醒发货时间  */
	@ApiModelProperty(value="提醒发货时间") 
	private Date remindDeliveryTime;
	
	/** 使用平台支付金额（金币和预存款） */
	@ApiModelProperty(value="使用平台支付金额（金币和预存款）") 
	private Double predepositAmount;
	
	/** 平台支付类型：1：预存款  2：金币 */
	@ApiModelProperty(value="平台支付类型：1：预存款  2：金币") 
	private String predPayType;
	
	/** 商城支付流水号 */
	@ApiModelProperty(value="商城支付流水号") 
	private String subSettlement;
	
	/** 第三方支付流水号 */
	@ApiModelProperty(value="第三方支付流水号") 
	private String flowTradeNo;
	
	/**
	 * 订单里的商品
	 */
	@ApiModelProperty(value="订单里的商品") 
	private List<SubOrderItemDto> subOrderItemDtos=new ArrayList<SubOrderItemDto>();
	
	/**
	 * 用户的订单地址
	 */
	@ApiModelProperty(value=" 用户的订单地址") 
	private UserAddressSub userAddressSub;
	
	/**
	 * 用户的订单发票
	 */
	@ApiModelProperty(value="用户的订单发票") 
	private InvoiceSub invoiceSub; 
	
	/**
	 *  订购历史表 
	 */
	@ApiModelProperty(value="订购历史表",hidden = true) 
	private List<SubHistory> subHistorys;
	
	/**
	 * 物流信息
	 */
	@ApiModelProperty(value="物流") 
	private DeliveryDto  delivery;
	
	/** 订单取消原  */
	@ApiModelProperty(value="订单取消原因") 
	private String cancelReason;
	
	@ApiModelProperty("活动ID, 如拼团活动ID")
	private Long activeId;
	
	@ApiModelProperty(value="团购状态") 
	private Integer groupStatus;
	
	/** 拼团状态 */
	private Integer mergeGroupStatus;
	
	/** 拼团活动, 已开团的团编号 */
	private String addNumber;
	
	@ApiModelProperty(value="是否已开发票") 
	private Integer hasInvoice;
	
	/** 支付方式,0:全额,1:订金 */
	@ApiModelProperty(value="支付方式  0:全额,1:订金 ")
	private Integer payPctType; 
		
	/** 订金金额 */
	@ApiModelProperty(value="订金金额")
	private java.math.BigDecimal preDepositPrice; 
		
	/** 尾款金额 */
	@ApiModelProperty(value="尾款金额 ") 
	private java.math.BigDecimal finalPrice; 
		
	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间 ") 
	private Date finalMStart; 
		
	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间 ") 
	private Date finalMEnd; 
		
	/** 订金支付名称 */
	@ApiModelProperty(value="订金支付名称 ")
	private String depositPayName; 
		
	/** 订金支付流水号 */
	@ApiModelProperty(value="订金支付流水号 ")
	private String depositTradeNo; 
		
	/** 订金支付时间 */
	@ApiModelProperty(value="订金支付时间 ")
	private Date depositPayTime; 
		
	/** 是否支付订金 */
	@ApiModelProperty(value="是否支付订金 ")
	private Integer isPayDeposit; 
		
	/** 尾款支付名称 */
	@ApiModelProperty(value="尾款支付名称 ") 
	private String finalPayName; 
		
	/** 尾款支付时间 */
	@ApiModelProperty(value="尾款支付时间  ") 
	private Date finalPayTime; 
		
	/** 尾款流水号 */
	@ApiModelProperty(value="尾款流水号  ") 
	private String finalTradeNo; 
		
	/** 是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款") 
	private Integer isPayFinal; 
	
	/** 详细收货地址 */
	@ApiModelProperty(value="详细收货地址") 
	private String detailAddress;
	
	@ApiModelProperty(value="订单已调整的金额")
	private Double changedPrice;

	/**退款id**/
	@ApiModelProperty(value="退款id")
	private Long refundId;

	@ExcelIgnore
	@ApiModelProperty(value = "收货人区域ID")
	private Integer areaId;

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getSubNum() {
		return subNum;
	}

	public void setSubNum(String subNum) {
		this.subNum = subNum;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	public Long getAddrOrderId() {
		return addrOrderId;
	}

	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Boolean getIsCod() {
		return isCod;
	}

	public void setIsCod(Boolean isCod) {
		this.isCod = isCod;
	}

	public boolean isIspay() {
		return ispay;
	}

	public void setIspay(boolean ispay) {
		this.ispay = ispay;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Double getDistCommisAmount() {
		return distCommisAmount;
	}

	public void setDistCommisAmount(Double distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}

	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public List<SubOrderItemDto> getSubOrderItemDtos() {
		return subOrderItemDtos;
	}

	public List<SubHistory> getSubHistorys() {
		return subHistorys;
	}

	public void setSubHistorys(List<SubHistory> subHistorys) {
		this.subHistorys = subHistorys;
	}

	public DeliveryDto getDelivery() {
		return delivery;
	}

	public void setDelivery(DeliveryDto delivery) {
		this.delivery = delivery;
	}

	@Override
	public Long getId() {
		return subId;
	}

	@Override
	public void setId(Long id) {
		this.subId = id ;
	}

	public void addItem(SubOrderItemDto mySubItemDto) {
		subOrderItemDtos.add(mySubItemDto);
	}

	public UserAddressSub getUserAddressSub() {
		return userAddressSub;
	}

	public void setUserAddressSub(UserAddressSub userAddressSub) {
		this.userAddressSub = userAddressSub;
	}

	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	public InvoiceSub getInvoiceSub() {
		return invoiceSub;
	}

	public void setInvoiceSub(InvoiceSub invoiceSub) {
		this.invoiceSub = invoiceSub;
	}

	public String getSubType() {
		return subType;
	}

	public Long getRefundState() {
		return refundState;
	}

	public void setSubOrderItemDtos(List<SubOrderItemDto> subOrderItemDtos) {
		this.subOrderItemDtos = subOrderItemDtos;
	}

	public Double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(Double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	public Double getPredepositAmount() {
		return predepositAmount;
	}

	public void setPredepositAmount(Double predepositAmount) {
		this.predepositAmount = predepositAmount;
	}

	public String getPredPayType() {
		return predPayType;
	}

	public void setPredPayType(String predPayType) {
		this.predPayType = predPayType;
	}

	public boolean isRemindDelivery() {
		return remindDelivery;
	}

	public void setRemindDelivery(boolean remindDelivery) {
		this.remindDelivery = remindDelivery;
	}

	public Date getRemindDeliveryTime() {
		return remindDeliveryTime;
	}

	public void setRemindDeliveryTime(Date remindDeliveryTime) {
		this.remindDeliveryTime = remindDeliveryTime;
	}

	public String getSubSettlement() {
		return subSettlement;
	}

	public void setSubSettlement(String subSettlement) {
		this.subSettlement = subSettlement;
	}

	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	public String getReciverName() {
		return reciverName;
	}

	public void setReciverName(String reciverName) {
		this.reciverName = reciverName;
	}

	public String getReciverMobile() {
		return reciverMobile;
	}

	public void setReciverMobile(String reciverMobile) {
		this.reciverMobile = reciverMobile;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	
	public Boolean getIsShopRemarked() {
		return isShopRemarked;
	}

	public void setIsShopRemarked(Boolean isShopRemarked) {
		this.isShopRemarked = isShopRemarked;
	}
	
	public String getShopRemark() {
		return shopRemark;
	}

	public void setShopRemark(String shopRemark) {
		this.shopRemark = shopRemark;
	}

	public Date getShopRemarkDate() {
		return shopRemarkDate;
	}

	public void setShopRemarkDate(Date shopRemarkDate) {
		this.shopRemarkDate = shopRemarkDate;
	}

	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	public Integer getMergeGroupStatus() {
		return mergeGroupStatus;
	}

	public void setMergeGroupStatus(Integer mergeGroupStatus) {
		this.mergeGroupStatus = mergeGroupStatus;
	}

	public Integer getHasInvoice() {
		return hasInvoice;
	}

	public void setHasInvoice(Integer hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	public Boolean getIsNeedInvoice() {
		return isNeedInvoice;
	}

	public void setIsNeedInvoice(Boolean isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public java.math.BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public java.math.BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(java.math.BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public String getDepositPayName() {
		return depositPayName;
	}

	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}

	public String getDepositTradeNo() {
		return depositTradeNo;
	}

	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}

	public Date getDepositPayTime() {
		return depositPayTime;
	}

	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public String getFinalPayName() {
		return finalPayName;
	}

	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}

	public Date getFinalPayTime() {
		return finalPayTime;
	}

	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	public String getFinalTradeNo() {
		return finalTradeNo;
	}

	public void setFinalTradeNo(String finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Double getChangedPrice() {
		return changedPrice;
	}

	public void setChangedPrice(Double changedPrice) {
		this.changedPrice = changedPrice;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}
}
