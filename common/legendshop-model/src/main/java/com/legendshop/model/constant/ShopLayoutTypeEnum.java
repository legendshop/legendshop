/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商家首页装修 布局模块
 * @author tony
 *
 */
public enum ShopLayoutTypeEnum implements StringEnum {
	/**  
	 * 通栏布局上
	 **/
	LAYOUT_0("layout0"),
	
	/**  
	 * 通栏布局下
	 **/
	LAYOUT_1("layout1"),
	
	/**  
	 * 居中布局
	 **/
	LAYOUT_2("layout2"),
	
	/**  
	 * 1:2布局
	 **/
	LAYOUT_3("layout3"),
	
	
	/**  
	 *2:1布局
	 **/
	LAYOUT_4("layout4")

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private ShopLayoutTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
