package com.legendshop.model.dto.weixin;

/**
 * {"errcode": 0, "errmsg": "ok"}
 * {"errcode":40013,"errmsg":"invalid appid"}
 * @author tony
 *
 */
public class WeiXinMsg {

	private Integer errcode;
	
	private String errmsg;

	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public boolean isSuccess(){
		if (errcode != null && errcode != 0) {
			return false;
		}
		
		return true;
	}
}
