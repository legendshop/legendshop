package com.legendshop.model.entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *拼团活动
 */
@Entity
@Table(name = "ls_merge_group")
public class MergeGroup implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
	
	/** 商品Id */
	private Long prodId;
		
	/** 活动名称 */
	private String mergeName; 
		
	/** 活动编号 */
	private String mergeNumber; 
		
	/** 开始时间 */
	private Date startTime; 
		
	/** 结束时间 */
	private Date endTime; 
		
	/** 状态[-3:已过期 -2审核不通过、-1下线、0待审核、1上线]  */
	private Integer status; 
		
	/** 活动图片 */
	private String pic; 
		
	/** 拼团人数 */
	private Integer peopleNumber; 
		
	/** 是否团长免单,默认否 */
	private Boolean isHeadFree; 
	
	/** 最低拼团价格 */
	private Double minPrice; 
	
	/** 最低商品价格 */
	private Double minProdPrice; 
	
	/** 是否限购,默认否 */
	private Boolean isLimit; 
	
	/** 限购数量 */
	private Long limitNumber; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 成团订单数 */
	private Long count; 
	
	/** 成团倒计时长 */
	private int countdown;
	
	/** 0:未删除 1: 商家逻辑删除  */
	private Integer deleteStatus;
	
	List<MergeProduct> mergeProductList = new ArrayList<MergeProduct>();
	
	public MergeGroup() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MERGE_GROUP_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
	
	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
		
    @Column(name = "merge_name")
	public String  getMergeName(){
		return mergeName;
	} 
		
	public void setMergeName(String mergeName){
			this.mergeName = mergeName;
		}
		
    @Column(name = "merge_number")
	public String  getMergeNumber(){
		return mergeNumber;
	} 
		
	public void setMergeNumber(String mergeNumber){
			this.mergeNumber = mergeNumber;
		}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}
		
    @Column(name = "status")
    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "people_number")
	public Integer  getPeopleNumber(){
		return peopleNumber;
	} 
		
	public void setPeopleNumber(Integer peopleNumber){
			this.peopleNumber = peopleNumber;
		}
		
    @Column(name = "is_head_free")
	public Boolean  getIsHeadFree(){
		return isHeadFree;
	} 
		
	public void setIsHeadFree(Boolean isHeadFree){
			this.isHeadFree = isHeadFree;
		}
		
	@Column(name = "min_price")	
	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	@Column(name = "min_prod_price")	
	public Double getMinProdPrice() {
		return minProdPrice;
	}

	public void setMinProdPrice(Double minProdPrice) {
		this.minProdPrice = minProdPrice;
	}
	
	@Column(name = "is_limit")
	public Boolean getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Boolean isLimit) {
		this.isLimit = isLimit;
	}
		
    @Column(name = "limit_number")
	public Long  getLimitNumber(){
		return limitNumber;
	} 

	public void setLimitNumber(Long limitNumber){
			this.limitNumber = limitNumber;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "count")
	public Long  getCount(){
		return count;
	} 
	
	public void setCount(Long count){
			this.count = count;
		}
	
	@Column(name = "count_down")
	public int getCountdown() {
		return countdown;
	}

	public void setCountdown(int countdown) {
		this.countdown = countdown;
	}

	@Column(name = "delete_status")
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	@Transient
	public List<MergeProduct> getMergeProductList() {
		return mergeProductList;
	}

	public void setMergeProductList(List<MergeProduct> mergeProductList) {
		this.mergeProductList = mergeProductList;
	}

} 
