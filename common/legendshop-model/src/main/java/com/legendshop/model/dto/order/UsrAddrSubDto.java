package com.legendshop.model.dto.order;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户的订单地址Dto")
public class UsrAddrSubDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value  = "收货人")
	private String receiver;
	
	@ApiModelProperty(value  = "手机号码")
	private String mobile;
	
	@ApiModelProperty(value  = "电话号码")
	private String telphone;
	
	@ApiModelProperty(value  = "邮政编码")
	private String subPost;
	
	@ApiModelProperty(value  = "邮箱")
	private String email;
	
	@ApiModelProperty(value  = "省份")
	private String province; 
		
	@ApiModelProperty(value  = "城市")
	private String city; 
		
	@ApiModelProperty(value  = "区域 ")
	private String area; 
	
	@ApiModelProperty(value  = "详细地址")
	private String subAdds; 
	

	

	public UsrAddrSubDto(String receiver, String mobile, String telphone,
			String subPost, String province, String city,
			String area, String subAdds) {
		super();
		this.receiver = receiver;
		this.mobile = mobile;
		this.telphone = telphone;
		this.subPost = subPost;
		this.province = province;
		this.city = city;
		this.area = area;
		this.subAdds = subAdds;
	}
	
	

	public UsrAddrSubDto() {
	}



	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getSubPost() {
		return subPost;
	}

	public void setSubPost(String subPost) {
		this.subPost = subPost;
	}


	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSubAdds() {
		return subAdds;
	}

	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
}
