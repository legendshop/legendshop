/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;

import java.util.List;

/**
 * 产品详细信息表.
 */
public class ProductDetail  extends AbstractProduct {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6275498676062425893L;
	
	/** The brand name. */
	private String brandName;

	/** The prod pics. */
	List<ImgFile> prodPics;
	
	/** The rel prods. */
	List<Product> relProds;
	
	/** 商品标签 */
	private ProdTag prodTag;
	
	/** 店铺类型 */
	private Integer shopType;
	
	private String categoryName;


	
	/**
	 * Gets the brand name.
	 * 
	 * @return the brand name
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * Sets the brand name.
	 *
	 * 
	 * @param brandName
	 *            the new brand name
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * Gets the prod pics.
	 * 
	 * @return the prod pics
	 */
	public List<ImgFile> getProdPics() {
		return prodPics;
	}

	/**
	 * Sets the prod pics.
	 * 
	 * @param prodPics
	 *            the new prod pics
	 */
	public void setProdPics(List<ImgFile> prodPics) {
		this.prodPics = prodPics;
	}

	/**
	 * Gets the rel prods.
	 * 
	 * @return the rel prods
	 */
	public List<Product> getRelProds() {
		return relProds;
	}

	/**
	 * Sets the rel prods.
	 * 
	 * @param relProds
	 *            the new rel prods
	 */
	public void setRelProds(List<Product> relProds) {
		this.relProds = relProds;
	}
	
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public ProdTag getProdTag() {
		return prodTag;
	}

	public void setProdTag(ProdTag prodTag) {
		this.prodTag = prodTag;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

}