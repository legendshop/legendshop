/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import com.legendshop.dao.support.GenericEntity;

/**
 * 店铺信息.
 */
public class ShopLayoutShopInfoDto implements GenericEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2499102239170712426L;

	/** 店铺Id. */
	private Long shopId;
	
	/** 信用点数. */
	private Integer credit;
	
	/** 站点名称. */
	private String siteName;
	
	/** 商家地址. */
	private String shopAddr;
	
	/** 地址. */
	private String addr;
	
	/** 省份. */
	private String province;
	
	/** 城市y. */
	private String city;
	
	/** 地区. */
	private String area;
	
	/** 联系电话. */
	private String contactMobile;
	
	/** 简要描述. */
	private String briefDesc;
	
	/** qq号码. */
	private String qq;
	
	/** banner图. */
	private String bannerPic;
	
	/** 商城图片. */
	private String shopPic;

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	/**
	 * Gets the credit.
	 *
	 * @return the credit
	 */
	public Integer getCredit() {
		return credit;
	}

	/**
	 * Sets the credit.
	 *
	 * @param credit the new credit
	 */
	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * Sets the site name.
	 *
	 * @param siteName the new site name
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * Gets the shop addr.
	 *
	 * @return the shop addr
	 */
	public String getShopAddr() {
		return shopAddr;
	}

	/**
	 * Sets the shop addr.
	 *
	 * @param shopAddr the new shop addr
	 */
	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}

	/**
	 * Gets the contact mobile.
	 *
	 * @return the contact mobile
	 */
	public String getContactMobile() {
		return contactMobile;
	}

	/**
	 * Sets the contact mobile.
	 *
	 * @param contactMobile the new contact mobile
	 */
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	/**
	 * Gets the brief desc.
	 *
	 * @return the brief desc
	 */
	public String getBriefDesc() {
		return briefDesc;
	}

	/**
	 * Sets the brief desc.
	 *
	 * @param briefDesc the new brief desc
	 */
	public void setBriefDesc(String briefDesc) {
		this.briefDesc = briefDesc;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * Sets the area.
	 *
	 * @param area the new area
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * Gets the addr.
	 *
	 * @return the addr
	 */
	public String getAddr() {
		return addr;
	}

	/**
	 * Sets the addr.
	 *
	 * @param addr the new addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Override
	public Long getId() {
		return shopId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Long id) {
		this.shopId=id;
	}

	/**
	 * Gets the qq.
	 *
	 * @return the qq
	 */
	public String getQq() {
		return qq;
	}

	/**
	 * Sets the qq.
	 *
	 * @param qq the new qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * Gets the banner pic.
	 *
	 * @return the banner pic
	 */
	public String getBannerPic() {
		return bannerPic;
	}

	/**
	 * Sets the banner pic.
	 *
	 * @param bannerPic the new banner pic
	 */
	public void setBannerPic(String bannerPic) {
		this.bannerPic = bannerPic;
	}

	/**
	 * Gets the shop pic.
	 *
	 * @return the shop pic
	 */
	public String getShopPic() {
		return shopPic;
	}

	/**
	 * Sets the shop pic.
	 *
	 * @param shopPic the new shop pic
	 */
	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}
	
	
	
	
}
