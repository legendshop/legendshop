package com.legendshop.model.constant;

/**
 * 基本的常量
 *
 * @author Joe
 */
public interface CommonConstants {

	/**
	 * 编码
	 */
	String UTF8 = "UTF-8";
	/**
	 * 成功标记
	 */
	Integer SUCCESS = 1;
	/**
	 * 失败标记
	 */
	Integer FAIL = 0;


	/**
	 * 锁定
	 */
	Integer STATUS_LOCK = -1;

	/**
	 * 正常状态
	 */
	Integer STATUS_NORMAL = 0;


	/**
	 * 滑块验证码
	 */
	String IMAGE_CODE_TYPE = "blockPuzzle";

}
