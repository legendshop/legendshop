package com.legendshop.model.entity;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *专题板块
 */
@Entity
@Table(name = "ls_theme_module")
public class ThemeModule implements GenericEntity<Long> {

	private static final long serialVersionUID = -2189625546795914921L;

	/** 板块id */
	private Long themeModuleId; 
		
	/** 专题id */
	private Long themeId; 
		
	/** 标题 */
	private String title; 
		
	/** 标题颜色 */
	private String titleColor; 
		
	/** 标题是否在横幅区显示 */
	private Long isTitleShow; 
		
	/** 板块标题背景色 */
	private String titleBgColor; 
		
	/** pc标题背景图 */
	private String titleBgPcimg; 
		
	/** 手机标题背景图 */
	private String titleBgMobileimg; 
	
	/** pc标题背景图文件 */
	private MultipartFile titleBgPcimgFile; 
		
	/** 手机标题背景图 文件*/
	private MultipartFile titleBgMobileimgFile; 
		
	/** 更多跳转链接 */
	private String moreUrl; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 更新时间 */
	private Date updateTime; 
	
	/**排序*/
	private Integer seq;

	/** pc端广告链接*/
	private String adPcUrl;

	/** 手机端广告链接*/
	private String adMobileUrl;
	
	/**板块下的商品列表*/
	private List<ThemeModuleProd> moduleProdList;
		
	
	public ThemeModule() {
    }
		
	@Id
	@Column(name = "theme_module_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "LS_THEME_MODULE_SEQ")
	public Long  getThemeModuleId(){
		return themeModuleId;
	} 
		
	public void setThemeModuleId(Long themeModuleId){
			this.themeModuleId = themeModuleId;
		}
		
    @Column(name = "theme_id")
	public Long  getThemeId(){
		return themeId;
	} 
		
	public void setThemeId(Long themeId){
			this.themeId = themeId;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "title_color")
	public String  getTitleColor(){
		return titleColor;
	} 
		
	public void setTitleColor(String titleColor){
			this.titleColor = titleColor;
		}
		
    @Column(name = "is_title_show")
	public Long  getIsTitleShow(){
		return isTitleShow;
	} 
		
	public void setIsTitleShow(Long isTitleShow){
			this.isTitleShow = isTitleShow;
		}
		
    @Column(name = "title_bg_color")
	public String  getTitleBgColor(){
		return titleBgColor;
	} 
		
	public void setTitleBgColor(String titleBgColor){
			this.titleBgColor = titleBgColor;
		}
		
    @Column(name = "title_bg_pcimg")
	public String  getTitleBgPcimg(){
		return titleBgPcimg;
	} 
		
	public void setTitleBgPcimg(String titleBgPcimg){
			this.titleBgPcimg = titleBgPcimg;
		}
		
    @Column(name = "title_bg_mobileimg")
	public String  getTitleBgMobileimg(){
		return titleBgMobileimg;
	} 
		
	public void setTitleBgMobileimg(String titleBgMobileimg){
			this.titleBgMobileimg = titleBgMobileimg;
		}
		
    @Column(name = "more_url")
	public String  getMoreUrl(){
		return moreUrl;
	} 
		
	public void setMoreUrl(String moreUrl){
			this.moreUrl = moreUrl;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "update_time")
	public Date  getUpdateTime(){
		return updateTime;
	} 
		
	public void setUpdateTime(Date updateTime){
			this.updateTime = updateTime;
		}
	
	@Transient
	public Long getId() {
		return themeModuleId;
	}
	
	public void setId(Long id) {
		themeModuleId = id;
	}

	@Transient
	public MultipartFile getTitleBgPcimgFile() {
		return titleBgPcimgFile;
	}

	public void setTitleBgPcimgFile(MultipartFile titleBgPcimgFile) {
		this.titleBgPcimgFile = titleBgPcimgFile;
	}

	@Transient
	public MultipartFile getTitleBgMobileimgFile() {
		return titleBgMobileimgFile;
	}

	public void setTitleBgMobileimgFile(MultipartFile titleBgMobileimgFile) {
		this.titleBgMobileimgFile = titleBgMobileimgFile;
	}

	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Transient
	public List<ThemeModuleProd> getModuleProdList() {
		return moduleProdList;
	}

	public void setModuleProdList(List<ThemeModuleProd> moduleProdList) {
		this.moduleProdList = moduleProdList;
	}

	@Column(name = "ad_pc_url")
	public String getAdPcUrl() {
		return adPcUrl;
	}

	public void setAdPcUrl(String adPcUrl) {
		this.adPcUrl = adPcUrl;
	}

	@Column(name = "ad_mobile_url")
	public String getAdMobileUrl() {
		return adMobileUrl;
	}

	public void setAdMobileUrl(String adMobileUrl) {
		this.adMobileUrl = adMobileUrl;
	}
}
