package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *部门表
 */
@Entity
@Table(name = "ls_department")
public class Department implements GenericEntity<Long> {

	private static final long serialVersionUID = 248331852792825737L;

	/**  */
	private Long id; 
		
	/** 部门名称 */
	private String name; 
		
	/** 联系人 */
	private String contact; 
		
	/** 联系电话 */
	private String mobile; 
		
	/**  */
	private Long parentId; 
		
	/** 部门负责人（负责该部门所有操作） */
	private String userId; 
	
	/** 是否为父节点 */
	private boolean isParent = false;
		
	/** 所属店铺 */
	private Long shopId; 
		
	/** 记录时间 */
	private Date recDate; 
		
	
	public Department() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DEPARTMENT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "contact")
	public String  getContact(){
		return contact;
	} 
		
	public void setContact(String contact){
			this.contact = contact;
		}
		
    @Column(name = "mobile")
	public String  getMobile(){
		return mobile;
	} 
		
	public void setMobile(String mobile){
			this.mobile = mobile;
		}
		
    @Column(name = "parent_id")
	public Long  getParentId(){
		return parentId;
	} 
		
	public void setParentId(Long parentId){
			this.parentId = parentId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	
	@Transient
	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

} 
