/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.siteInformation;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 站内信.
 */
@ApiModel(value="站内信详情") 
public class SiteInformation implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4557731453390997590L;

	@ApiModelProperty(value="站内信Id")
	private Long id;

	/** The msg id. */
	@ApiModelProperty(value="消息Id")
	private Long msgId;

	/** The send name. */
	@ApiModelProperty(value="发送人")
	private String sendName;

	/** The receive name. */
	@ApiModelProperty(value="接收人")
	private String receiveName;

	/** The status. */
	@ApiModelProperty(value="站内信状态")
	private Integer status;

	/** The user level. */
	@ApiModelProperty(value="用户等级")
	private String userLevel;

	/** The is global. */
	@ApiModelProperty(value="是否全局，如果是全局的话就可以实现1对多的消息发送")
	private Integer isGlobal;

	/** The delete status. */
	@ApiModelProperty(value="发件人删除状态，如果发件人删除了之后该值会变为1")  
	private Integer deleteStatus;

	/** The title. */
	@ApiModelProperty(value="标题")
	private String title;

	/** The text. */
	@ApiModelProperty(value="内容")
	private String text;

	/** The rec date. */
	@ApiModelProperty(value="记录时间")
	private Date recDate;

	@ApiModelProperty(value="收件人删除状态，如果收件人删除了之后该值会变为1")
	private Integer deleteStatus2;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Serializable getId() {
		return msgId;
	}

	/**
	 * Gets the send name.
	 *
	 * @return the send name
	 */
	public String getSendName() {
		return sendName;
	}

	/**
	 * Sets the send name.
	 *
	 * @param sendName
	 *            the new send name
	 */
	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	/**
	 * Gets the receive name.
	 *
	 * @return the receive name
	 */
	public String getReceiveName() {
		return receiveName;
	}

	/**
	 * Sets the receive name.
	 *
	 * @param receiveName
	 *            the new receive name
	 */
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the checks if is global.
	 *
	 * @return the checks if is global
	 */
	public Integer getIsGlobal() {
		return isGlobal;
	}

	/**
	 * Sets the checks if is global.
	 *
	 * @param isGlobal
	 *            the new checks if is global
	 */
	public void setIsGlobal(Integer isGlobal) {
		this.isGlobal = isGlobal;
	}

	/**
	 * Gets the delete status.
	 *
	 * @return the delete status
	 */
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	/**
	 * Sets the delete status.
	 *
	 * @param deleteStatus
	 *            the new delete status
	 */
	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text
	 *            the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 *
	 * @param recDate
	 *            the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the user level.
	 *
	 * @return the user level
	 */
	public String getUserLevel() {
		return userLevel;
	}

	/**
	 * Sets the user level.
	 *
	 * @param userLevel
	 *            the new user level
	 */
	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Integer getDeleteStatus2() {
		return deleteStatus2;
	}

	public void setDeleteStatus2(Integer deleteStatus2) {
		this.deleteStatus2 = deleteStatus2;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
