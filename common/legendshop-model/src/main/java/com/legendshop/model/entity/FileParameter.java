package com.legendshop.model.entity;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;

@Entity
@Table(name = "ls_file_param")
public class FileParameter implements GenericEntity<String> {

    private String name;

    private String value;

    private String memo;

    private String changeOnline;

    public FileParameter() {
    }

    public FileParameter(String name, String value, String memo, String changeOnline) {
        this.name = name;
        this.value = value;
        this.memo = memo;
        this.changeOnline = changeOnline;
    }

    @Override
    public String toString() {
        return "FileParameter{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", memo='" + memo + '\'' +
                ", changeOnline='" + changeOnline + '\'' +
                '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.ASSIGNED)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "memo")
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Column(name = "change_online")
    public String getChangeOnline() {
        return changeOnline;
    }

    public void setChangeOnline(String changeOnline) {
        this.changeOnline = changeOnline;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(String s) {

    }
}
