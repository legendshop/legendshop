package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 支付结算单据
 * @author tony
 *
 */
@Entity
@Table(name = "ls_sub_settlement_item")
public class SubSettlementItem implements GenericEntity<Long> {

	private static final long serialVersionUID = 6665917191772945065L;

	/** ID */
	private Long id; 
	
	/**  商户系统内部的订单号[微信]   */
	private String subSettlementSn;
	
	/** 用户ID */
	private String userId; 
	
	/** 订单流水号  */
	private String subNumber; 

	
	public SubSettlementItem() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_SETTLEMENT_ITEM_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "sub_settlement_sn")
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "sub_number")
	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

} 
