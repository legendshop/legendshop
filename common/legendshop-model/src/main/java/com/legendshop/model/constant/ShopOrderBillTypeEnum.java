/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.LongEnum;

/**
 * 结算单tab类型.
 * @author linzh
 */
public enum ShopOrderBillTypeEnum implements LongEnum {

	/** 订单列表   */
	ORDER(1L),

	/** 退款单列表 */
	RETURN_ORDER(2L),

	/** 分销单列表 **/
	DIST_ORDER(3L),

	/** 预售订单列表 **/
	PRESELL_ORDER(4L),

	/** 拍卖订单列表 **/
	AUCTION_ORDER(5L);

	/** The num. */
	private Long num;

	@Override
	public Long value() {
		return num;
	}

	ShopOrderBillTypeEnum(Long num) {
		this.num = num;
	}

}
