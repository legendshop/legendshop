package com.legendshop.model.entity;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.util.AppUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *商家的商品类目
 */
@Entity
@Table(name = "ls_shop_cat")
public class ShopCategory implements GenericEntity<Long> {

	private static final long serialVersionUID = -2066089194686839543L;

	/** 产品类目ID */
	private Long id; 
		
	/** 父节点 */
	private Long parentId; 
		
	/** 产品类目名称 */
	private String name; 
		
	/** 类目的显示图片 */
	private String pic; 
		
	/** 排序 */
	private Integer seq; 
		
	/** 默认是1，表示正常状态,0为下线状态 */
	private Integer status; 
		
	/** 类型ID */
	private Long shopId; 
		
	/** 记录时间 */
	private Date recDate; 
	
	/** 分类层级  */
	private Integer grade;
		
	/** The file. */
	protected MultipartFile file;
	
	/** 第三级分类 list **/
	private List<ShopCategory> subCatList;
	
	/** 下一级 分类id **/
	private Long nextCatId;
	
	/** 最后一级分类id **/
	private Long subCatId;
	
	public ShopCategory() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_CAT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "parent_id")
	public Long  getParentId(){
		return parentId;
	} 
		
	public void setParentId(Long parentId){
			this.parentId = parentId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
	}
	
	 @Column(name = "grade")
	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	@Transient
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public void addList(ShopCategory subCat){
		if(AppUtils.isBlank(subCatList)){
			subCatList = new ArrayList<ShopCategory>();
		}
		
		if(this.id.equals(subCat.parentId)){
			subCatList.add(subCat);
		}
	}

	@Transient
	public List<ShopCategory> getSubCatList() {
		return subCatList;
	}

	public void setSubCatList(List<ShopCategory> subCatList) {
		this.subCatList = subCatList;
	}

	@Transient
	public Long getNextCatId() {
		return nextCatId;
	}

	public void setNextCatId(Long nextCatId) {
		this.nextCatId = nextCatId;
	}

	@Transient
	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}
	
	
} 
