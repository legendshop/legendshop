/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * LegendShop 版权所有,并保留所有权利。
 * 首页楼层的种类
 */
public enum AdminDashboardEnum implements StringEnum {
	/**  
	 * 所有用户数 
	 **/
	USER_CONUT("userCounts"),
	
	/**
	 * 在线用户数
	 */
	ONLINE_USER_CONUT("userOnlineCounts"),
	
	
	/**
	 *订单数
	 */
	ORDER_CONUT("USER"),

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private AdminDashboardEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
