package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *1:2， 2:1 布局的div部门内容
 */
@Entity
@Table(name = "ls_shop_layout_div")
public class ShopLayoutDiv implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/**  */
	private Long layoutDivId; 
		
	/**  */
	private Long layoutId; 
		
	/**  */
	private Long shopId; 
		
	/**  */
	private Long shopDecotateId; 
		
	/** Div1:左边布局;div2:右边布局[layout3:1:2布局、layout4:2:1布局] */
	private String layoutDiv; 
	
	/** 布局模块类型 **/
	private String layoutModuleType;
		
	/** 布局类型Id */
	private String layoutType; 
	
	/** 自定义模块内容 **/
	private String layoutContent;
	
	public ShopLayoutDiv() {
    }
		
	@Id
	@Column(name = "layout_div_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_LAYOUT_DIV_SEQ")
	public Long  getLayoutDivId(){
		return layoutDivId;
	} 
		
	public void setLayoutDivId(Long layoutDivId){
			this.layoutDivId = layoutDivId;
		}
		
    @Column(name = "layout_id")
	public Long  getLayoutId(){
		return layoutId;
	} 
		
	public void setLayoutId(Long layoutId){
			this.layoutId = layoutId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_decotate_id")
	public Long  getShopDecotateId(){
		return shopDecotateId;
	} 
		
	public void setShopDecotateId(Long shopDecotateId){
			this.shopDecotateId = shopDecotateId;
		}
		
    @Column(name = "layout_div")
	public String  getLayoutDiv(){
		return layoutDiv;
	} 
		
	public void setLayoutDiv(String layoutDiv){
			this.layoutDiv = layoutDiv;
		}
		
    @Column(name = "layout_type")
	public String  getLayoutType(){
		return layoutType;
	} 
		
	public void setLayoutType(String layoutType){
			this.layoutType = layoutType;
		}
	
	@Transient
	public Long getId() {
		return layoutDivId;
	}
	
	public void setId(Long id) {
		layoutDivId = id;
	}

	
	@Column(name = "layout_module_type")
	public String getLayoutModuleType() {
		return layoutModuleType;
	}

	public void setLayoutModuleType(String layoutModuleType) {
		this.layoutModuleType = layoutModuleType;
	}

	@Column(name = "layout_content")
	public String getLayoutContent() {
		return layoutContent;
	}

	public void setLayoutContent(String layoutContent) {
		this.layoutContent = layoutContent;
	}
	
	


} 
