package com.legendshop.model.dto.order;

import java.util.List;
/**
 * 订单详情参数Dto
 *
 */
public class OrderDetailsParamsDto {
	
	private List<Long> shopCartItems;
	
	private String type;
	
	private String couponBack;
	
	private Long adderessId;

	private Boolean buyNow;
	
	private Long prodId;
	
	private Long skuId;
	
	private Integer count;
	
	/**
	 * 活动ID[秒杀ID,团购ID,预售ID,拼团ID]
	 */
	private Long activeId;

	/** 发票ID */
	private Long invoiceId;

	
	/**拼团编号，用于参团,区分哪个团*/
	private String addNumber;

	public List<Long> getShopCartItems() {
		return shopCartItems;
	}

	public void setShopCartItems(List<Long> shopCartItems) {
		this.shopCartItems = shopCartItems;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCouponBack() {
		return couponBack;
	}

	public void setCouponBack(String couponBack) {
		this.couponBack = couponBack;
	}

	public Long getAdderessId() {
		return adderessId;
	}

	public void setAdderessId(Long adderessId) {
		this.adderessId = adderessId;
	}

	public Boolean getBuyNow() {
		if(null == buyNow){
			return false;
		}
		return buyNow;
	}

	public void setBuyNow(Boolean buyNow) {
		this.buyNow = buyNow;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
}
