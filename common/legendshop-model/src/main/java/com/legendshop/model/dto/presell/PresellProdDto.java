/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.Sku;

import java.util.List;

/**
 * 预售商品Dto
 */
public class PresellProdDto extends PresellProd {

	private static final long serialVersionUID = -6764248157402611306L;



	/** 商品名称 */
	private String prodName;

	/** 商品价格 */
	private Double prodPrice;

	/** 商品图片 */
	private String prodPic;

	/** 商品编码 */
	private String prodCode;

	/** SKU名称 */
	private String skuName;

	/** SKU 价格 */
	private Double skuPrice;

	/** SKU 属性 */
	private String cnProperties;

	/** SKU 主图 */
	private String skuPic;
	
	/** 商品状态 参考ProductStatusEnum */
	private Integer prodStatus;

	/** 预售商品集合 **/
	private List<Sku> skuList;
	

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the prodPrice
	 */
	public Double getProdPrice() {
		return prodPrice;
	}

	/**
	 * @param prodPrice
	 *            the prodPrice to set
	 */
	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}

	/**
	 * @param prodCode
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	/**
	 * @return the prodPic
	 */
	public String getProdPic() {
		return prodPic;
	}

	/**
	 * @param prodPic
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	/**
	 * @return the skuName
	 */
	public String getSkuName() {
		return skuName;
	}

	/**
	 * @param skuName
	 */
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	/**
	 * @return the skuPrice
	 */
	public Double getSkuPrice() {
		return skuPrice;
	}

	/**
	 * @param skuPrice
	 */
	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	/**
	 * @return the cnProperties
	 */
	public String getCnProperties() {
		return cnProperties;
	}

	/**
	 * @param cnProperties
	 */
	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	/**
	 * @return the skuPic
	 */
	public String getSkuPic() {
		return skuPic;
	}

	/**
	 * @param skuPic
	 */
	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	public Integer getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Integer prodStatus) {
		this.prodStatus = prodStatus;
	}

	public List<Sku> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<Sku> skuList) {
		this.skuList = skuList;
	}
}
