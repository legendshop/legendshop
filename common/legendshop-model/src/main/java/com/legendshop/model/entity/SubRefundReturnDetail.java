package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *退款明细
 */
@Entity
@Table(name = "ls_sub_refund_return_detail")
public class SubRefundReturnDetail implements GenericEntity<Long> {

	private static final long serialVersionUID = -7147123089392611814L;

	/** ID */
	private Long id; 
		
	/** 退款单ID */
	private Long refundId; 
		
	/** 退款类型 */
	private String refundType; 
		
	/** 支付类型 */
	private String payType; 
		
	/** 退款金额 */
	private java.math.BigDecimal refundAmount; 
		
	/** 退款状态，0 失败， 1成功 */
	private Boolean refundStatus; 
		
	/** 备注 */
	private String remark; 
		
	
	public SubRefundReturnDetail() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_REFUND_RETURN_DETAIL_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "refund_id")
	public Long  getRefundId(){
		return refundId;
	} 
		
	public void setRefundId(Long refundId){
			this.refundId = refundId;
		}
		
    @Column(name = "refund_type")
	public String  getRefundType(){
		return refundType;
	} 
		
	public void setRefundType(String refundType){
			this.refundType = refundType;
		}
		
    @Column(name = "pay_type")
	public String  getPayType(){
		return payType;
	} 
		
	public void setPayType(String payType){
			this.payType = payType;
		}
		
    @Column(name = "refund_amount")
	public java.math.BigDecimal  getRefundAmount(){
		return refundAmount;
	} 
		
	public void setRefundAmount(java.math.BigDecimal refundAmount){
			this.refundAmount = refundAmount;
		}
		
    @Column(name = "refund_status")
	public Boolean  getRefundStatus(){
		return refundStatus;
	} 
		
	public void setRefundStatus(Boolean refundStatus){
			this.refundStatus = refundStatus;
		}
		
    @Column(name = "remark")
	public String  getRemark(){
		return remark;
	} 
		
	public void setRemark(String remark){
			this.remark = remark;
		}

} 
