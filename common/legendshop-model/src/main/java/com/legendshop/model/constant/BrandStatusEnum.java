/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 品牌状态.
 */
public enum BrandStatusEnum implements IntegerEnum {
	// 1: 上线， 0： 下线, -1审核中, -2 审核失败
	
	BRAND_AUDIT(-1),
	
	BRAND_AUDIT_FAIL(-2), //-1 -> -2 审核失败, -2> -1: 卖家重新提交审核

	BRAND_ONLINE(1), // -1 -> 1 审核成功， 成功后可以上线，下线
	
	BRAND_OFFLINE(0); //平台自己操作
	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	BrandStatusEnum(Integer num) {
		this.num = num;
	}

}
