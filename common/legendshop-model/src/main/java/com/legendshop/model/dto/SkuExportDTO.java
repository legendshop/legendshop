package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品导出dto
 * 
 * @author Zhongweihan
 * 
 */
public class SkuExportDTO implements Serializable {

    /** 副标题 */
    private String skuName;

    /** 规格项1 */
    private String firstProperties;

    /** 规格值1 */
    private String firstPropertiesValue;

    /** 规格项2 */
    private String secondProperties;

    /** 规格值2 */
    private String secondPropertiesValue;

    /** 规格项3 */
    private String thirdProperties;

    /** 规格值3 */
    private String thirdPropertiesValue;

    /** 规格项4 */
    private String fourthProperties;

    /** 规格值4 */
    private String fourthPropertiesValue;

    /** SKU售价 */
    private String skuPrice;

    /** 可售库存 */
    private String stocks;

    /** 物流重量 */
   /* private String weight;*/

    /** 商家编码 */
    private String partyCode;

    /** 商品条形码 */
    private String modelId;

    /** 是否上架 */
    private String status;

    /** 备注 */
    private String remark;

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getFirstProperties() {
        return firstProperties;
    }

    public void setFirstProperties(String firstProperties) {
        this.firstProperties = firstProperties;
    }

    public String getFirstPropertiesValue() {
        return firstPropertiesValue;
    }

    public void setFirstPropertiesValue(String firstPropertiesValue) {
        this.firstPropertiesValue = firstPropertiesValue;
    }

    public String getSecondProperties() {
        return secondProperties;
    }

    public void setSecondProperties(String secondProperties) {
        this.secondProperties = secondProperties;
    }

    public String getSecondPropertiesValue() {
        return secondPropertiesValue;
    }

    public void setSecondPropertiesValue(String secondPropertiesValue) {
        this.secondPropertiesValue = secondPropertiesValue;
    }

    public String getThirdProperties() {
        return thirdProperties;
    }

    public void setThirdProperties(String thirdProperties) {
        this.thirdProperties = thirdProperties;
    }

    public String getThirdPropertiesValue() {
        return thirdPropertiesValue;
    }

    public void setThirdPropertiesValue(String thirdPropertiesValue) {
        this.thirdPropertiesValue = thirdPropertiesValue;
    }

    public String getFourthProperties() {
        return fourthProperties;
    }

    public void setFourthProperties(String fourthProperties) {
        this.fourthProperties = fourthProperties;
    }

    public String getFourthPropertiesValue() {
        return fourthPropertiesValue;
    }

    public void setFourthPropertiesValue(String fourthPropertiesValue) {
        this.fourthPropertiesValue = fourthPropertiesValue;
    }

    public String getSkuPrice() {
        return skuPrice;
    }

    public void setSkuPrice(String skuPrice) {
        this.skuPrice = skuPrice;
    }

    public String getStocks() {
        return stocks;
    }

    public void setStocks(String stocks) {
        this.stocks = stocks;
    }

   /* public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }*/

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
	 * 检查数据是否合法
	 * 
	 * @return
	 */
	public List<String> checkData() {
		List<String> result = new ArrayList<String>();
//		if (AppUtils.isBlank(this.getProdName()) || AppUtils.isBlank(this.getBrief()) || AppUtils.isBlank(this.getPrice())) {
//			 	result.add("数值为空");
//		}
//
//		if (this.getProdName().length() > 300 || this.getBrief().length() > 500) {
//			result.add("长度超长");
//		}
//
//		if(this.getPrice() <=0 || this.getCash() <= 0){
//			result.add("价格非法");
//		}
//
		return result;
	}

}
