/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.util.AppUtils;

/**
 * 单品SKU表.
 */
@Entity
@Table(name = "ls_sku")
public class Sku implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2102752405219316621L;

	/** 单品ID. */
	private Long skuId ; 
		
	/** 商品ID. */
	private Long prodId ; 
		
	/** sku的销售属性组合字符串（颜色，大小，等等，可通过类目API获取某类目下的销售属性）,格式是p1:v1;p2:v2. */
	private String properties ; 
	
	/** 中文 销售属性组合. */
	private String cnProperties;
	
	/** 用户自定义的销售属性，key:value 格式. */
	private String userProperties ; 
		
	/** 价格. */
	private Double price ; 
		
	/** 商品在付款减库存的状态下，该sku上未付款的订单数量. */
	private Long stocks ; 
		
	/** 实际库存. */
	private Long actualStocks ; 
		
	/** SKU名称. */
	private String name ; 
		
	/** sku状态。 1l:正常 ；0:删除. */
	private Integer status ; 
		
	/** sku级别发货时间. */
	private Date skuDeliveryTime ; 
		
	/** 商家设置的外部id. */
	private String outerId ; 
		
	/** 修改时间. */
	private Date modifyDate ; 
		
	/** 记录时间. */
	private Date recDate ; 
		
	/** 商家编码. */
	private String partyCode;
	
	/** 商品条形码. */
	private String modelId;
	
	/** 属性. */
	private String property;
	
	/** 图片. */
	private String pic;
	
	/** 之前的库存. */
	private Long beforeStock;

	/** 之前的实际库存 **/
	private Long beforeActualStock;
	
	/**拼团价格*/
	private Double mergePrice;
	
	/**物流体积(立方米)*/
	private Double volume;
	
	/**物流重量(千克)*/
	private Double weight;
	
	/** sku活动类型 SkuActiveTypeEnum**/
	private String skuType;
	
	/**
	 * Instantiates a new sku.
	 */
	public Sku() {
    }
	
	/**
	 * Gets the sku id.
	 *
	 * @return the sku id
	 */
	@Id
	@Column(name = "sku_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SKU_SEQ")
	public Long  getSkuId(){
		return skuId ;
	} 
		
	/**
	 * Sets the sku id.
	 *
	 * @param skuId the new sku id
	 */
	public void setSkuId(Long skuId){
		this.skuId = skuId ;
	}
		
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	@Column(name = "prod_id")
	public Long  getProdId(){
		return prodId ;
	} 
		
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId){
		this.prodId = prodId ;
	}
		
		
	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	@Column(name = "properties")
	public String  getProperties(){
		return properties ;
	} 
		
	/**
	 * Sets the properties.
	 *
	 * @param properties the new properties
	 */
	public void setProperties(String properties){
		this.properties = properties ;
	}
		
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	@Column(name = "price")
	public Double  getPrice(){
		//根据sku的计价方式来返回对应的价格
		
		return price ;
	} 
		
	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(Double price){
		this.price = price ;
	}
		
	/**
	 * Gets the stocks.
	 *
	 * @return the stocks
	 */
	@Column(name = "stocks")
	public Long  getStocks(){
		return stocks ;
	} 
		
	/**
	 * Sets the stocks.
	 *
	 * @param stocks the new stocks
	 */
	public void setStocks(Long stocks){
		this.stocks = stocks ;
	}
		
	/**
	 * Gets the actual stocks.
	 *
	 * @return the actual stocks
	 */
	@Column(name = "actual_stocks")
	public Long  getActualStocks(){
		return actualStocks ;
	} 
		
	/**
	 * Sets the actual stocks.
	 *
	 * @param actualStocks the new actual stocks
	 */
	public void setActualStocks(Long actualStocks){
		this.actualStocks = actualStocks ;
	}
		
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(name = "name")
	public String  getName(){
		return name ;
	} 
		
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name){
		this.name = name ;
	}
		
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer  getStatus(){
		return status ;
	} 
		
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status){
		this.status = status ;
	}
		
	/**
	 * Gets the sku delivery time.
	 *
	 * @return the sku delivery time
	 */
	@Column(name = "sku_delivery_time")
	public Date  getSkuDeliveryTime(){
		return skuDeliveryTime ;
	} 
		
	/**
	 * Sets the sku delivery time.
	 *
	 * @param skuDeliveryTime the new sku delivery time
	 */
	public void setSkuDeliveryTime(Date skuDeliveryTime){
		this.skuDeliveryTime = skuDeliveryTime ;
	}
		
	/**
	 * Gets the outer id.
	 *
	 * @return the outer id
	 */
	@Column(name = "outer_id")
	public String  getOuterId(){
		return outerId ;
	} 
		
	/**
	 * Sets the outer id.
	 *
	 * @param outerId the new outer id
	 */
	public void setOuterId(String outerId){
		this.outerId = outerId ;
	}
		
	/**
	 * Gets the modify date.
	 *
	 * @return the modify date
	 */
	@Column(name = "modify_date")
	public Date  getModifyDate(){
		return modifyDate ;
	} 
		
	/**
	 * Sets the modify date.
	 *
	 * @param modifyDate the new modify date
	 */
	public void setModifyDate(Date modifyDate){
		this.modifyDate = modifyDate ;
	}
		
	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate ;
	} 
		
	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
  public Long getId() {
		return 	skuId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		this.skuId = id;
	}

	/**
	 * Gets the user properties.
	 *
	 * @return the user properties
	 */
	@Deprecated
	@Column(name = "user_properties")
	public String getUserProperties() {
		return userProperties;
	}

	/**
	 * Sets the user properties.
	 *
	 * @param userProperties the new user properties
	 */
	public void setUserProperties(String userProperties) {
		this.userProperties = userProperties;
	}

	/**
	 * Gets the party code.
	 *
	 * @return the party code
	 */
	@Column(name = "party_code")
	public String getPartyCode() {
		return partyCode;
	}

	/**
	 * Sets the party code.
	 *
	 * @param partyCode the new party code
	 */
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	/**
	 * Gets the model id.
	 *
	 * @return the model id
	 */
	@Column(name = "model_id")
	public String getModelId() {
		return modelId;
	}

	/**
	 * Sets the model id.
	 *
	 * @param modelId the new model id
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	
	/**
	 * 检查sku的每个属性和属性值Id是否为数字，否则报错
	 * 如果没有properties则不保存到数据库.
	 *
	 * @return true, if successful
	 */
	public boolean checkProperties(){
		String properties = getProperties();
		if(AppUtils.isNotBlank(properties)){
			String skuStrs[] = properties.split(";");
			for (int i = 0; i < skuStrs.length; i++) {
				String skuItems[] = skuStrs[i].split(":");
				Long.valueOf(skuItems[0]);
				Long.valueOf(skuItems[1]);
			}
			return true;
		}else{
			return true;
		}
			
	}
	
	/**
	 * Gets the property.
	 *
	 * @return the property
	 */
	@Transient
	public String getProperty() {
		return property;
	}

	/**
	 * Sets the property.
	 *
	 * @param property the new property
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * Gets the cn properties.
	 *
	 * @return the cn properties
	 */
	@Column(name = "cn_properties")
	public String getCnProperties() {
		return cnProperties;
	}

	/**
	 * Sets the cn properties.
	 *
	 * @param cnProperties the new cn properties
	 */
	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	
	/**
	 * Gets the before stock.
	 *
	 * @return the before stock
	 */
	@Transient
	public Long getBeforeStock() {
		if(AppUtils.isBlank(beforeStock)){
			return -1L;
		}
		return beforeStock;
	}
	
	/**
	 * Sets the before stock.
	 *
	 * @param beforeStock the new before stock
	 */
	public void setBeforeStock(Long beforeStock) {
		this.beforeStock = beforeStock;
	}

	@Transient
	public Long getBeforeActualStock() {
		if(AppUtils.isBlank(beforeActualStock)){
			return -1L;
		}
		return beforeActualStock;
	}

	public void setBeforeActualStock(Long beforeActualStock) {
		this.beforeActualStock = beforeActualStock;
	}
	
	@Transient
	public Double getMergePrice() {
		return mergePrice;
	}
	
	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}

	@Column(name = "volume")
	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	@Column(name = "weight")
	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
	
	@Column(name = "sku_type")
	public String getSkuType() {
		return skuType;
	}

	public void setSkuType(String skuType) {
		this.skuType = skuType;
	}

	    @Override
	public String toString() {
		return "Sku [skuId=" + skuId + ", prodId=" + prodId + ", properties=" + properties + ", cnProperties="
				+ cnProperties + ", userProperties=" + userProperties + ", price=" + price + ", stocks=" + stocks
				+ ", actualStocks=" + actualStocks + ", name=" + name + ", status=" + status + ", skuDeliveryTime="
				+ skuDeliveryTime + ", outerId=" + outerId + ", modifyDate=" + modifyDate + ", recDate=" + recDate
				+ ", partyCode=" + partyCode + ", modelId=" + modelId + ", property="
				+ property + ", pic=" + pic + ", beforeStock=" + beforeStock  + ", mergePrice=" + mergePrice + ", volume="
				+ volume + ", weight=" + weight + ", skuType=" + skuType + "]";
	}

}
