package com.legendshop.model.entity;
import java.util.Date;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 发货地址表.
 */
@Entity
@Table(name = "ls_deliver_goods_addr")
public class DeliverGoodsAddr implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 地址Id. */
	private Long addrId; 
		
	/** 店铺Id. */
	private Long shopId; 
		
	/**  联系人. */
	@NotBlank
	private String contactName; 
		
	/** 省份Id. */
	private Integer provinceId; 
		
	/** 城市Id. */
	private Integer cityId; 
		
	/** 地区Id. */
	private Integer areaId; 
		
	/**  手机. */
	@Pattern(regexp="^1\\d{10}$")
	private String mobile; 
		
	/**  地址. */
	@NotBlank
	private String adds; 
		
	/** 建立时间. */
	private Date createTime; 
	
	/**是否默认. */
	private Integer detault;
	
	/** 省份. */
	private String  province;
	
	/** 城市. */
	private String city;
	
	/** 地区. */
	private String area;
		
	
	/**
	 * Instantiates a new deliver goods addr.
	 */
	public DeliverGoodsAddr() {
    }
		
	/**
	 * Gets the addr id.
	 *
	 * @return the addr id
	 */
	@Id
	@Column(name = "addr_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DELIVER_GOODS_ADDR_SEQ")
	public Long  getAddrId(){
		return addrId;
	} 
		
	/**
	 * Sets the addr id.
	 *
	 * @param addrId the new addr id
	 */
	public void setAddrId(Long addrId){
			this.addrId = addrId;
		}
		
    /**
     * Gets the shop id.
     *
     * @return the shop id
     */
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    /**
     * Gets the contact name.
     *
     * @return the contact name
     */
    @Column(name = "contact_name")
	public String  getContactName(){
		return contactName;
	} 
		
	/**
	 * Sets the contact name.
	 *
	 * @param contactName the new contact name
	 */
	public void setContactName(String contactName){
			this.contactName = contactName;
		}
		
    /**
     * Gets the province id.
     *
     * @return the province id
     */
    @Column(name = "province_id")
	public Integer  getProvinceId(){
		return provinceId;
	} 
		
	/**
	 * Sets the province id.
	 *
	 * @param provinceId the new province id
	 */
	public void setProvinceId(Integer provinceId){
			this.provinceId = provinceId;
		}
		
    /**
     * Gets the city id.
     *
     * @return the city id
     */
    @Column(name = "city_id")
	public Integer  getCityId(){
		return cityId;
	} 
		
	/**
	 * Sets the city id.
	 *
	 * @param cityId the new city id
	 */
	public void setCityId(Integer cityId){
			this.cityId = cityId;
		}
		
    /**
     * Gets the area id.
     *
     * @return the area id
     */
    @Column(name = "area_id")
	public Integer  getAreaId(){
		return areaId;
	} 
		
	/**
	 * Sets the area id.
	 *
	 * @param areaId the new area id
	 */
	public void setAreaId(Integer areaId){
			this.areaId = areaId;
		}
		
    /**
     * Gets the mobile.
     *
     * @return the mobile
     */
    @Column(name = "mobile")
	public String  getMobile(){
		return mobile;
	} 
		
	/**
	 * Sets the mobile.
	 *
	 * @param mobile the new mobile
	 */
	public void setMobile(String mobile){
			this.mobile = mobile;
		}
		
    /**
     * Gets the adds.
     *
     * @return the adds
     */
    @Column(name = "adds")
	public String  getAdds(){
		return adds;
	} 
		
	/**
	 * Sets the adds.
	 *
	 * @param adds the new adds
	 */
	public void setAdds(String adds){
			this.adds = adds;
		}
		
    /**
     * Gets the creates the time.
     *
     * @return the creates the time
     */
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	/**
	 * Sets the creates the time.
	 *
	 * @param createTime the new creates the time
	 */
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public Long getId() {
		return addrId;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		addrId = id;
	}

	 /**
 	 * Gets the detault.
 	 *
 	 * @return the detault
 	 */
 	@Column(name = "is_default")
	public Integer getDetault() {
		return detault;
	}

	/**
	 * Sets the detault.
	 *
	 * @param detault the new detault
	 */
	public void setDetault(Integer detault) {
		this.detault = detault;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	@Transient
	public String getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(String province) {
		this.province = province;
	}
	
	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	@Transient
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	@Transient
	public String getArea() {
		return area;
	}

	/**
	 * Sets the area.
	 *
	 * @param area the new area
	 */
	public void setArea(String area) {
		this.area = area;
	}
	
} 
