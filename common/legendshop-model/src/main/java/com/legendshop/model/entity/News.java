/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文章实体
 */
@Entity
@Table(name = "ls_news")
@ApiModel(value="News文章") 
public class News extends AbstractEntity implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5866824730655175491L;

	/** 主键ID */
	@ApiModelProperty(value="主键ID") 
	private Long id;
	
	/** 文章ID */
	@ApiModelProperty(value="文章ID")  
	private Long newsId;

    /** 文章分类id */
	@ApiModelProperty(value="文章分类id")  
    private Long newsCategoryId;
    
    /** 文章分类名称 */
	@ApiModelProperty(value="文章分类名称")  
    private String newsCategoryName;
    
    /**文章栏目图片*/
	@ApiModelProperty(value="文章栏目图片")  
    private String newsCategoryPic;
    
    /** 文章标题 */
	@ApiModelProperty(value="文章标题 ")  
    private String newsTitle;

    /** 文章内容 */
	@ApiModelProperty(value="文章内容")  
    private String newsContent;

    /** 发表时间 */
	@ApiModelProperty(value="发表时间")  
    private Date newsDate;
    
    /** 1:上线 0：下线. */
	@ApiModelProperty(value="1:上线 0：下线")  
    private Integer status;
    
    /** 位置 *. */
	@ApiModelProperty(value="位置")  
    private Integer position;
    
	/** 是否高亮,1:yes,0:no */
	@ApiModelProperty(value="是否高亮,1:yes,0:no")  
    private Integer highLine;

    /** 用户ID */
	@ApiModelProperty(value="用户ID")  
    private String userId;

    /** 所属用户名称 */
	@ApiModelProperty(value="所属用户名称")  
    private String userName;
    
	/** 文章提要 */
	@ApiModelProperty(value="文章提要")  
    private String newsBrief;
    
	/** 顺序 */
	@ApiModelProperty(value="顺序")  
    private Long seq;
    
	/** 新闻编号 */
	@ApiModelProperty(value="新闻编号")  
    private Long positionId;
    
	/** 文章标签 */
	@ApiModelProperty(value="文章标签")  
    private String newsTags;
    
	/** 新闻标签 */
	@ApiModelProperty(value="新闻标签")  
    private String positionTags;
    


	/**
	 * default constructor.
	 */
    public News() {
    }

    /**
	 * minimal constructor.
	 * 
	 * @param newsId
	 *            the news id
	 */
    public News(Long newsId) {
        this.newsId = newsId;
    }
    
    public News(Long newsId, Long newsCategoryId,String newsCategoryName, String newsTitle) {
        this.newsId = newsId;
        this.newsCategoryId = newsCategoryId;
        this.newsCategoryName = newsCategoryName;
        this.newsTitle = newsTitle;
    }

    /**
	 * Gets the news id.
	 * 
	 * @return the news id
	 */
    @Id
	@Column(name = "news_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "NEWS_SEQ")
    public Long getNewsId() {
        return this.newsId;
    }

    /**
	 * Sets the news id.
	 * 
	 * @param newsId
	 *            the new news id
	 */
    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }
    
    /**
	 * Gets the news category id.
	 * 
	 * @return the news category id
	 */
    @Column(name = "news_category_id")
    public Long getNewsCategoryId() {
        return newsCategoryId;
      }
      
      /**
		 * Sets the news category id.
		 * 
		 * @param newsCategoryId
		 *            the new news category id
		 */
      public void setNewsCategoryId(Long newsCategoryId){
        this.newsCategoryId = newsCategoryId;
      }

    /**
	 * Gets the news title.
	 * 
	 * @return the news title
	 */
      @Column(name = "news_title")
    public String getNewsTitle() {
        return this.newsTitle;
    }

    /**
	 * Sets the news title.
	 * 
	 * @param newsTitle
	 *            the new news title
	 */
    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    /**
	 * Gets the news content.
	 * 
	 * @return the news content
	 */
    @Column(name = "news_content")
    public String getNewsContent() {
        return this.newsContent;
    }

    /**
	 * Sets the news content.
	 * 
	 * @param newsContent
	 *            the new news content
	 */
    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    /**
	 * Gets the news date.
	 * 
	 * @return the news date
	 */
    @Column(name = "news_date")
    public Date getNewsDate() {
        return this.newsDate;
    }

    /**
	 * Sets the news date.
	 * 
	 * @param newsDate
	 *            the new news date
	 */
    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    /**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
    @Column(name = "user_id")
    public String getUserId() {
        return this.userId;
    }

    /**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
    @Column(name = "user_name")
    public String getUserName() {
        return this.userName;
    }

    /**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
	 * Gets the status.
	 * 
	 * @return the status
	 */
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    /**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
    public void setStatus(Integer status) {
        this.status = status;
    }

	/**
	 * Gets the news category name.
	 * 
	 * @return the news category name
	 */
	@Transient
	public String getNewsCategoryName() {
		return newsCategoryName;
	}

	/**
	 * Sets the news category name.
	 * 
	 * @param newsCategoryName
	 *            the new news category name
	 */
	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}

	/**
	 * Gets the high line.
	 * 
	 * @return the high line
	 */
	@Column(name = "high_line")
	public Integer getHighLine() {
		return highLine;
	}

	/**
	 * Sets the high line.
	 * 
	 * @param highLine
	 *            the new high line
	 */
	public void setHighLine(Integer highLine) {
		this.highLine = highLine;
	}

	/**
	 * Gets the news brief.
	 * 
	 * @return the news brief
	 */
	@Column(name = "news_brief")
	public String getNewsBrief() {
		return newsBrief;
	}

	/**
	 * Sets the news brief.
	 * 
	 * @param newsBrief
	 *            the new news brief
	 */
	public void setNewsBrief(String newsBrief) {
		this.newsBrief = newsBrief;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return newsId;
	}
	
	public void setId(Long id) {
		this.newsId = id;
	}

	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	@Transient
	public Integer getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 * 
	 * @param position
	 *            the new position
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	@Column(name = "seq")
	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}
	@Transient
	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	@Transient
	public String getNewsCategoryPic() {
		return newsCategoryPic;
	}

	public void setNewsCategoryPic(String newsCategoryPic) {
		this.newsCategoryPic = newsCategoryPic;
	}
	@Transient
	public String getNewsTags() {
		return newsTags;
	}

	public void setNewsTags(String newsTags) {
		this.newsTags = newsTags;
	}
	@Transient
	public String getPositionTags() {
		return positionTags;
	}

	public void setPositionTags(String positionTags) {
		this.positionTags = positionTags;
	}
	
	
}