/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 参加营销活动的商品
 */
@Entity
@Table(name = "ls_marketing_prods")
public class MarketingProds implements GenericEntity<Long> {

	private static final long serialVersionUID = -6944019023242939798L;
	/**  */
	private Long id; 
		
	/** 营销活动编号 */
	private Long marketId; 
		
	/** 商家编号 */
	private Long shopId; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** SKUID */
	private Long skuId; 
		
	/** 商品原价格 */
	private Double cash; 
		
	/** 商品折扣 */
	private Float discount; 
		
	/** 商品促销价格[折后价,直降价] */
	private Double discountPrice; 
		
	/** 1: 满减促销 ; 2: 满折促销 3：限时促销 4：直降 */
	private Integer type; 
		
	/** 创建时间 */
	private Date createTime; 
		
	
	public MarketingProds() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MARKETING_PRODS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "market_id")
	public Long  getMarketId(){
		return marketId;
	} 
		
	public void setMarketId(Long marketId){
			this.marketId = marketId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "cash")
	public Double  getCash(){
		return cash;
	} 
		
	public void setCash(Double cash){
			this.cash = cash;
		}
		
    @Column(name = "discount")
	public Float  getDiscount(){
		return discount;
	} 
		
	public void setDiscount(Float discount){
			this.discount = discount;
		}
		
    @Column(name = "discount_price")
	public Double  getDiscountPrice(){
		return discountPrice;
	} 
		
	public void setDiscountPrice(Double discountPrice){
			this.discountPrice = discountPrice;
		}
		
    @Column(name = "type")
	public Integer  getType(){
		return type;
	} 
		
	public void setType(Integer type){
			this.type = type;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	


} 
