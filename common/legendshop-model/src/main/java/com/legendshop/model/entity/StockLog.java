package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 库存历史表
 */
@Entity
@Table(name = "ls_stock_log")
public class StockLog implements GenericEntity<Long> {

	/**
	 *
	 */
	private static final long serialVersionUID = -3466552615511625652L;

	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 商品Id
	 */
	private Long prodId;

	/**
	 * sku_id
	 */
	private Long skuId;

	/**
	 * 商品名称
	 */
	private String name;

	/**
	 * 变更之前的库存
	 */
	private Long beforeStock;

	/**
	 * 变更之后的库存
	 */
	private Long afterStock;

	/**
	 * 变更之前的库存
	 */
	private Long beforeActualStock;

	/**
	 * 变更之后的库存
	 */
	private Long afterActualStock;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 变更备注
	 */
	private String updateRemark;


	public StockLog() {
	}

	@Id
	@Override
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STOCK_LOG_SEQ")
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Column(name = "sku_id")
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "before_stock")
	public Long getBeforeStock() {
		return beforeStock;
	}

	public void setBeforeStock(Long beforeStock) {
		this.beforeStock = beforeStock;
	}

	@Column(name = "after_stock")
	public Long getAfterStock() {
		return afterStock;
	}

	public void setAfterStock(Long afterStock) {
		this.afterStock = afterStock;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "update_remark")
	public String getUpdateRemark() {
		return updateRemark;
	}

	public void setUpdateRemark(String updateRemark) {
		this.updateRemark = updateRemark;
	}

	@Column(name = "before_actual_stock")
	public Long getBeforeActualStock() {
		return beforeActualStock;
	}

	public void setBeforeActualStock(Long beforeActualStock) {
		this.beforeActualStock = beforeActualStock;
	}

	@Column(name = "after_actual_stock")
	public Long getAfterActualStock() {
		return afterActualStock;
	}

	public void setAfterActualStock(Long afterActualStock) {
		this.afterActualStock = afterActualStock;
	}
}
