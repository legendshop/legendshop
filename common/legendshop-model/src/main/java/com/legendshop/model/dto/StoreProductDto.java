/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 门店商品Dto.
 *
 */
public class StoreProductDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private Long id;
	
	/** 门店id */
	private Long storeId;
	
	/** 商家id */
	private Long shopId;
	
	/** 商品名字 */
	private String productName;
	
	/** 商品id */
	private Long productId;
	
	/** 图片 */
	private String pic;
	
	/** 价格 */
	private Double cash;
	
	/** 库存 */
	private Long stock;
	
	/** The store sku dto. */
	private List<StoreSkuDto> storeSkuDto;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId the product id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * Gets the stock.
	 *
	 * @return the stock
	 */
	public Long getStock() {
		return stock;
	}

	/**
	 * Sets the stock.
	 *
	 * @param stock the stock
	 */
	public void setStock(Long stock) {
		this.stock = stock;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the store id.
	 *
	 * @return the store id
	 */
	public Long getStoreId() {
		return storeId;
	}

	/**
	 * Sets the store id.
	 *
	 * @param storeId the store id
	 */
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	/**
	 * Gets the store sku dto.
	 *
	 * @return the storeSkuDto
	 */
	public List<StoreSkuDto> getStoreSkuDto() {
		return storeSkuDto;
	}

	/**
	 * Sets the store sku dto.
	 *
	 * @param storeSkuDto the storeSkuDto to set
	 */
	public void setStoreSkuDto(List<StoreSkuDto> storeSkuDto) {
		this.storeSkuDto = storeSkuDto;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	
}
