/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 *  商品包邮类型枚举
 */
public enum ShippingTypeEnum implements IntegerEnum {

	/**
	 * 店铺包邮
	 */
	SHOP(0),
	
	/**
	 * 商品包邮
	 */
	PROD(1), 
	
	
	/**
	 * 满件
	 */
	FULL_PIECE(2),
	
	/**
	 * 满金额 
	 */
	FULL_MONEY(1),
	
	/** 下线 */
	OFFLINE(0),
	
	/** 上线 */
	ONLINE(1),
	;
	private Integer type;

	ShippingTypeEnum(Integer type) {
		this.type = type;
	}

	public Integer value() {
		return type;
	}

}
