package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 拼团活动add状态
 * ls_merge_group_operate 和 ls_merge_group_add 共用
 */
public enum MergeGroupAddStatusEnum implements IntegerEnum{
	
	IN(1),//进行中
	
	SUCCESS(2),//成功
	
	FAIL(3),//失败
	
	REFUND_CANCLE(4),//退款取消
	
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private MergeGroupAddStatusEnum(Integer num) {
		this.num = num;
	}
	
}
