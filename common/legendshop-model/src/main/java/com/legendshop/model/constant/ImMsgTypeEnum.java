/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * IM消息类型
 */
public enum ImMsgTypeEnum implements IntegerEnum {
	
	/**
	 * 文本信息
	 */
	IM_TEXT(0),
	
	/**
	 * 图片
	 */
	IM_IMAGE(1),
	
	/**
	 * 声音
	 */
	IM_VOICE(2),
	
	/**
	 * 视频
	 */
	IM_VEDIO(3),
	
	/**
	 * 音乐
	 */
	IM_MUSIC(4),
	
	/**
	 * 商品
	 */
	IM_PROD(5),
	/**
	 * 订单
	 */
	IM_ORDER(6),
	/**
	 * 时间
	 */
	IM_TIME(7);
	
	private Integer value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.LongEnum#value()
	 */
	public Integer value() {
		return value;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ImMsgTypeEnum(Integer value) {
		this.value = value;
	}

}
