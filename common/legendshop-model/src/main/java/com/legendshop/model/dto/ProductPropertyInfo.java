/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 
 * @author tony
 *
 */
public class ProductPropertyInfo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5791341454636722721L;

	/**
	 * The attribute name, e.g. "大小","颜色"
	 */
	private String attrName;

	/**
	 * The attribute value, e.g. "XL","红色"
	 */
	private String attrValue;

	/**
	 * Gets the attr name.
	 *
	 * @return the attr name
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * Sets the attr name.
	 *
	 * @param attrName the new attr name
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * Gets the attr value.
	 *
	 * @return the attr value
	 */
	public String getAttrValue() {
		return attrValue;
	}

	/**
	 * Sets the attr value.
	 *
	 * @param attrValue the new attr value
	 */
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}
	
	
}
