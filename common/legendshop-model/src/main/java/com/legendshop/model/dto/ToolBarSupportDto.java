/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

import com.legendshop.dao.support.PageSupport;

/**
 * 分页工具条包装类.
 */
public class ToolBarSupportDto implements Serializable{
	
	private static final long serialVersionUID = -254160669536038833L;
	/**
	 * 分页的内容
	 */
	private PageSupport<?> pageData;
	
	/**
	 * 分页条
	 */
	private String toolBar;
	
	public ToolBarSupportDto(PageSupport<?> pageData, String toolBar) {
		this.pageData = pageData;
		this.toolBar = toolBar;
	}


	public String getToolBar() {
		return toolBar;
	}

	public void setToolBar(String toolBar) {
		this.toolBar = toolBar;
	}


	public PageSupport<?> getPageData() {
		return pageData;
	}


	public void setPageData(PageSupport<?> pageData) {
		this.pageData = pageData;
	}


}
