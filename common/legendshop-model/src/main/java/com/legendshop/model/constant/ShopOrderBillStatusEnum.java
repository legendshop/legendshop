/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 结算单状态.
 */
public enum ShopOrderBillStatusEnum implements IntegerEnum {
	
	/** 初始状态   */
	INIT(1),
	
	/** 商家确认 */
	SHOP_CONFIRM(2),

	/** 平台确认 **/
	ADMIN_CONFIRM(3),
	
	/** 结算完成 **/
	FINISH(4);

	/** The num. */
	private Integer num;

	public Integer value() {
		return num;
	}

	ShopOrderBillStatusEnum(Integer num) {
		this.num = num;
	}

}
