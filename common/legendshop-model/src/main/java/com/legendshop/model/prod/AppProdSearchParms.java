/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.prod;


/**
 * 查询参数
 * 
 */
public class AppProdSearchParms {

	private Long categoryId;// 分类ID
	
	private Long brandId;//品牌ID

	private Integer curPageNO=1;//页码
	
	private Integer pageSize;//每页条数
	
	/**
	 * 排序方式:
	 * cash:价格
	 * buys: 购买数
	 * comments: 评论数
	 */
	private String orderBy;//排序
	
	/**
	 * 排序方向：
	 * asc:升序
	 * desc:降序
	 */
	private String dir;

	private String keyword;
	
	private String status;


	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Integer getCurPageNO() {
		return curPageNO;
	}

	public void setCurPageNO(Integer page) {
		this.curPageNO = page;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}



	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
