/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 商家上诉，只能上诉3次
 *
 */
@Entity
@Table(name = "ls_accu_appeal")
public class AccusationAppeal implements GenericEntity<Long> {

	private static final long serialVersionUID = 1500747754634948251L;
	
	/** 主键 **/
	private Long aaId;

	public AccusationAppeal() {
	}

	@Id
	@Column(name = "aa_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ACCU_APPEAL_SEQ")
	public Long getAaId() {
		return aaId;
	}

	public void setAaId(Long aaId) {
		this.aaId = aaId;
	}

	@Transient
	public Long getId() {
		return aaId;
	}

	public void setId(Long id) {
		this.aaId = id;
	}

}
