package com.legendshop.model.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户追加评论请求参数对象
 */
@ApiModel(value="用户追加评论请求参数对象")
public class UserAddCommParamsDto {
	
	/** 评论Id */
	@ApiModelProperty(value="评论Id",required=true)
	private Long prodCommId;
	
	/** 评论内容 */
	@ApiModelProperty(value="评论内容")
	private String content;
	
	/** 评论图片, 多个已逗号隔开 */
	@ApiModelProperty(value="评论图片列表集合",hidden=true)
	private List<MultipartFile> photos;
	
	/** 评论图片, 多个已逗号隔开 */
	@ApiModelProperty(value="评论图片路径列表集合")
	private List<String> photoList;

	public Long getProdCommId() {
		return prodCommId;
	}

	public void setProdCommId(Long prodCommId) {
		this.prodCommId = prodCommId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<MultipartFile> getPhotos() {
		return photos;
	}

	public void setPhotos(List<MultipartFile> photos) {
		this.photos = photos;
	}

	public List<String> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<String> photoList) {
		this.photoList = photoList;
	}
	
}
