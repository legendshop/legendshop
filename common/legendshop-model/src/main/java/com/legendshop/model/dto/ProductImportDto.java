package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.util.AppUtils;

/**
 * 商品导入dto
 * 
 * @author fqb
 * 
 */
public class ProductImportDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4458749936502002151L;

	/** 商品名字 */
	private String prodName;

	/** 原价 */
	private Double price;
	/** 现价 */
	private Double cash;
	/** 是否已经发布，1：是，0：否 */
	private Long status;
	/** 关键字 */
	private String keyWord;

	/** 商品SEOTitle */
	private String metaTitle;

	/** 商品SEO desc */
	private String metaDesc;

	/** 商家编码 */
	private String partyCode;

	/** 副标题 */
	private String brief;

	/** 商品条形码 */
	private String modelId;
	/** 库存警告 */
	private Long stocksArm;

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public Long getStocksArm() {
		return stocksArm;
	}

	public void setStocksArm(Long stocksArm) {
		this.stocksArm = stocksArm;
	}

	/**
	 * 检查数据是否合法
	 * 
	 * @return
	 */
	public List<String> checkData() {
		List<String> result = new ArrayList<String>();
		if (AppUtils.isBlank(this.getProdName())
				|| AppUtils.isBlank(this.getBrief())
				|| AppUtils.isBlank(this.getPrice())) {
			 	result.add("数值为空");
		}

		if (this.getProdName().length() > 300 || this.getBrief().length() > 500) {
			result.add("长度超长");
		}
		
		if(this.getPrice() <=0 || this.getCash() <= 0){
			result.add("价格非法");
		}
		
		return result;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

}
