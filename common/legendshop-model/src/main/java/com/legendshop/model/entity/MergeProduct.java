package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *拼团活动参加商品
 */
@Entity
@Table(name = "ls_merge_product")
public class MergeProduct implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id; 
		
	/** 活动ID */
	private Long mergeId; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** 商品skuID */
	private Long skuId; 
		
	/** 拼团价格 */
	private Double mergePrice; 
	
	/** sku商品原价格 */
	private Double skuPrice;
		
	
	public MergeProduct() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MERGE_PRODUCT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "merge_id")
	public Long  getMergeId(){
		return mergeId;
	} 
		
	public void setMergeId(Long mergeId){
			this.mergeId = mergeId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}

    @Column(name = "merge_price")
    public Double getMergePrice() {
		return mergePrice;
	}

	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}

	@Transient
	public Double getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

} 
