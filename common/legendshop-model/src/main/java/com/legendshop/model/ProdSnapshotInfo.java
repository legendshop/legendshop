/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;

/**
 * 商品快照载体
 */
public class ProdSnapshotInfo implements Serializable {

	private static final long serialVersionUID = 8954648620830648185L;

	/** 商品ID */
	private final Long prodId;
	
	/** SKU ID */
	private final Long skuId;
	
	/** 运费 */
	private final Double carriage;
	
	/** 订单项ID */
	private final Long subItemId;

	public ProdSnapshotInfo(Long prodId, Long skuId, Double carriage, Long subItemId) {
		super();
		this.prodId = prodId;
		this.skuId = skuId;
		this.carriage = carriage;
		this.subItemId = subItemId;
	}

	public Long getProdId() {
		return prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public Double getCarriage() {
		return carriage;
	}

	public Long getSubItemId() {
		return subItemId;
	}
	

	
}
