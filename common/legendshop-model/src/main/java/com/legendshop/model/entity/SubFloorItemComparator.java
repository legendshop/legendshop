/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.io.Serializable;
import java.util.Comparator;

/**
 * 楼层子元素排序器.
 */
public class SubFloorItemComparator implements Comparator<SubFloorItem>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3755529456231762117L;


	public int compare(SubFloorItem o1, SubFloorItem o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		}else if(o1.getSeq().equals(o2.getSeq())){
			return 0;
		} else if (o1.getSeq() < o2.getSeq()) {
			return -1;
		} else {
			return 1;
		}
	}


}
