/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商品排序方式
 */
public enum ShopOrderWayEnum implements StringEnum {
	/**  
	 * 综合排序: synthesize
	 */
	SYNTHESIZE("synthesize"),
	
	/**  
	 * 月销量排序
	 */
	SALES("sales"),
	
	/**
	 * 评论数排序: comments
	 */
	COMMENTS("comments"), 
	;

	private final String value;

	private ShopOrderWayEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		ShopOrderWayEnum[] enums = values();
		for (ShopOrderWayEnum appEnum : enums) {
			if (appEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
