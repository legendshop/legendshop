package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 装修点击动作类型
 * @author 开发很忙
 */
public enum AppDecorateActionTypeEnum implements StringEnum{
	
	/** 商品详情 */
	PROD_DETAIL("prodDetail"),
	
	/** 商品列表 */
	PROD_LIST("prodList"),
	
	/** 优惠券或红包 */
	COUPON("coupon"),
	
	/** 常用功能 */
	METHOD("method"),
	
	/** 营销活动 */
	MARKETING("marketing"),
	
	/** 自定义页面 */
	PAGE("page"),
	
	/** 自定义url */
	URL("url")
	;
	
	private final String value;

	private AppDecorateActionTypeEnum(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return value;
	}
}
