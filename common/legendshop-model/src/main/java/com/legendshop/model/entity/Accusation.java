/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 举报表.
 */
@Entity
@Table(name = "ls_accusation")
public class Accusation implements GenericEntity<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2785346036857331053L;

	/** The id. */
	private Long id ; 
		
	/** 举报内容. */
	private String content ; 
		
	/** 处理结果. */
	private Long result ; 
		
	/** 处理时间. */
	private Date handleTime ; 
		
	/** 处理结果. */
	private String handleInfo ; 
		
	/** 状态. */
	private Long status ; 
		
	/** 处理图片 pic1. */
	private String pic1 ; 
		
	/** 处理图片 pic2. */
	private String pic2 ; 
		
	/** 处理图片 pic3. */
	private String pic3 ; 
		
	/** 记录时间. */
	private Date recDate ; 
		
	/** 主题主题. */
	private Long subjectId ; 
		
	// 举报人ID
	/** The user id. */
	private String userId ; 
		
	// 举报人名称
	/** The user name. */
	private String userName ; 
	
	//举报人昵称
	/** The nick name. */
	private String nickName;
		
	// 产品ID
	/** The prod id. */
	private Long prodId ; 
	
	//产品名称
	/** The prod name. */
	private String prodName;

	//产品图片
	/** The prod pic. */
	private String prodPic;
	
	//举报类型
	/** The accu type. */
	private String accuType;
	
	//举报主题
	/** The title. */
	private String title;
	
	/** The shop id. */
	private Long shopId;
	
	/** The shop name. */
	private String shopName;
		
	/** The pic1 file. */
	private MultipartFile pic1File;
	
	/** The pic2 file. */
	private MultipartFile pic2File;
	
	/** The pic3 file. */
	private MultipartFile pic3File;
	
	//类型ID
	/** The type id. */
	private Long typeId;
	
	//商品处理 
	/** The illegal off. */
	private Integer illegalOff;
	//用户是否删除,0未删除,1已经删除
	private Integer userDelStatus;
	
	/**
	 * The Constructor.
	 */
	public Accusation() {
    }
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ACCUSATION_SEQ")
	public Long  getId(){
		return id ;
	} 
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id){
		this.id = id ;
	}
		
	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	@Column(name = "content")
	public String  getContent(){
		return content ;
	} 
		
	/**
	 * Sets the content.
	 *
	 * @param content the content
	 */
	public void setContent(String content){
		this.content = content ;
	}
		
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	@Column(name = "result")	
	public Long  getResult(){
		return result ;
	} 
		
	/**
	 * Sets the result.
	 *
	 * @param result the result
	 */
	public void setResult(Long result){
		this.result = result ;
	}
		
	/**
	 * Gets the handle time.
	 *
	 * @return the handle time
	 */
	@Column(name = "handle_time")	
	public Date  getHandleTime(){
		return handleTime ;
	} 
		
	/**
	 * Sets the handle time.
	 *
	 * @param handleTime the handle time
	 */
	public void setHandleTime(Date handleTime){
		this.handleTime = handleTime ;
	}
		
	/**
	 * Gets the handle info.
	 *
	 * @return the handle info
	 */
	@Column(name = "handle_info")	
	public String  getHandleInfo(){
		return handleInfo ;
	} 
		
	/**
	 * Sets the handle info.
	 *
	 * @param handleInfo the handle info
	 */
	public void setHandleInfo(String handleInfo){
		this.handleInfo = handleInfo ;
	}
		
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")	
	public Long  getStatus(){
		return status ;
	} 
		
	/**
	 * Sets the status.
	 *
	 * @param status the status
	 */
	public void setStatus(Long status){
		this.status = status ;
	}
		
	/**
	 * Gets the pic1.
	 *
	 * @return the pic1
	 */
	@Column(name = "pic1")	
	public String  getPic1(){
		return pic1 ;
	} 
		
	/**
	 * Sets the pic1.
	 *
	 * @param pic1 the pic1
	 */
	public void setPic1(String pic1){
		this.pic1 = pic1 ;
	}
		
	/**
	 * Gets the pic2.
	 *
	 * @return the pic2
	 */
	@Column(name = "pic2")	
	public String  getPic2(){
		return pic2 ;
	} 
		
	/**
	 * Sets the pic2.
	 *
	 * @param pic2 the pic2
	 */
	public void setPic2(String pic2){
		this.pic2 = pic2 ;
	}
		
	/**
	 * Gets the pic3.
	 *
	 * @return the pic3
	 */
	@Column(name = "pic3")	
	public String  getPic3(){
		return pic3 ;
	} 
		
	/**
	 * Sets the pic3.
	 *
	 * @param pic3 the pic3
	 */
	public void setPic3(String pic3){
		this.pic3 = pic3 ;
	}
		
	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")	
	public Date  getRecDate(){
		return recDate ;
	} 
		
	/**
	 * Sets the rec date.
	 *
	 * @param recDate the rec date
	 */
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
	/**
	 * Gets the subject id.
	 *
	 * @return the subject id
	 */
	@Column(name = "subject_id")	
	public Long  getSubjectId(){
		return subjectId ;
	} 
		
	/**
	 * Sets the subject id.
	 *
	 * @param subjectId the subject id
	 */
	public void setSubjectId(Long subjectId){
		this.subjectId = subjectId ;
	}
		
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String  getUserId(){
		return userId ;
	} 
		
	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId){
		this.userId = userId ;
	}
		
	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String  getUserName(){
		return userName ;
	} 
		
	/**
	 * Sets the user name.
	 *
	 * @param userName the user name
	 */
	public void setUserName(String userName){
		this.userName = userName ;
	}
		
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	@Column(name = "prod_id")
	public Long  getProdId(){
		return prodId ;
	} 
		
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the prod id
	 */
	public void setProdId(Long prodId){
		this.prodId = prodId ;
	}
	
	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	@Transient
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the accu type.
	 *
	 * @return the accu type
	 */
	@Transient
	public String getAccuType() {
		return accuType;
	}

	/**
	 * Sets the accu type.
	 *
	 * @param accuType the accu type
	 */
	public void setAccuType(String accuType) {
		this.accuType = accuType;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@Transient
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the pic1 file.
	 *
	 * @return the pic1 file
	 */
	@Transient
	public MultipartFile getPic1File() {
		return pic1File;
	}

	/**
	 * Sets the pic1 file.
	 *
	 * @param pic1File the pic1 file
	 */
	public void setPic1File(MultipartFile pic1File) {
		this.pic1File = pic1File;
	}
	
	/**
	 * Gets the pic2 file.
	 *
	 * @return the pic2 file
	 */
	@Transient
	public MultipartFile getPic2File() {
		return pic2File;
	}

	/**
	 * Sets the pic2 file.
	 *
	 * @param pic2File the pic2 file
	 */
	public void setPic2File(MultipartFile pic2File) {
		this.pic2File = pic2File;
	}

	/**
	 * Gets the pic3 file.
	 *
	 * @return the pic3 file
	 */
	@Transient
	public MultipartFile getPic3File() {
		return pic3File;
	}

	/**
	 * Sets the pic3 file.
	 *
	 * @param pic3File the pic3 file
	 */
	public void setPic3File(MultipartFile pic3File) {
		this.pic3File = pic3File;
	}

	/**
	 * Gets the prod pic.
	 *
	 * @return the prod pic
	 */
	@Transient
	public String getProdPic() {
		return prodPic;
	}

	/**
	 * Sets the prod pic.
	 *
	 * @param prodPic the prod pic
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	/**
	 * Gets the type id.
	 *
	 * @return the type id
	 */
	@Transient
	public Long getTypeId() {
		return typeId;
	}

	/**
	 * Sets the type id.
	 *
	 * @param typeId the type id
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	
	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	@Transient
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	@Transient
	public String getShopName() {
		return shopName;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName the shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the nick name.
	 *
	 * @return the nick name
	 */
	@Transient
	public String getNickName() {
		return nickName;
	}

	/**
	 * Sets the nick name.
	 *
	 * @param nickName the nick name
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * Gets the illegal off.
	 *
	 * @return the illegal off
	 */
	@Column(name = "illegal_off")
	public Integer getIllegalOff() {
		return illegalOff;
	}

	/**
	 * Sets the illegal off.
	 *
	 * @param illegalOff the illegal off
	 */
	public void setIllegalOff(Integer illegalOff) {
		this.illegalOff = illegalOff;
	}
	@Column(name = "user_del_status")	
	public Integer getUserDelStatus() {
		return userDelStatus;
	}

	public void setUserDelStatus(Integer userDelStatus) {
		this.userDelStatus = userDelStatus;
	}
} 
