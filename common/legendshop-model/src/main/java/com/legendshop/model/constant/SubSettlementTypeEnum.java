/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 支付单据类型
 * 
 * @author Tony
 * 
 */
public enum SubSettlementTypeEnum implements StringEnum {

	/**
	 * 订单支付
	 */
	USER_ORDER_PAY("USER_ORDER"),

	/**
	 * 用户预付款充值
	 */
	USER_PREPAID_RECHARGE("USER_PRED_RECHARGE"),
	
	/**
	 * 拍卖保证金支付
	 */
	AUCTION_DEPOSIT("AUCTION_DEPOSIT"),
	
	
	/**
	 * 预售订单服务
	 */
	PRESELL_ORDER_PAY("PRESELL_ORDER_PAY")

	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SubSettlementTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	public static SubSettlementTypeEnum fromCode(String code) {
		for (SubSettlementTypeEnum cartTypeEnum : SubSettlementTypeEnum.values()) {
			if (cartTypeEnum.value.equals(code)) {
				return cartTypeEnum;
			}
		}
		return null;
	}

}
