package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 秒杀商品
 */
@Entity
@Table(name = "ls_seckill_prod")
public class SeckillProd implements GenericEntity<Long> {

	private static final long serialVersionUID = -1651893678403542515L;

	/**  */
	private Long id;

	/** 秒杀活动id */
	private Long sId;

	/** 秒杀活动的商品id */
	private Long prodId;

	/** 商品sku的id */
	private Long skuId;

	/** 秒杀商品的库存 */
	private Long seckillStock;

	/** 秒杀商品的价格 */
	private java.math.BigDecimal seckillPrice;

	/** 秒杀活动总库存 */
	private Long activityStock;

	public SeckillProd() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SECKILL_PROD_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "s_id")
	public Long getSId() {
		return sId;
	}

	public void setSId(Long sId) {
		this.sId = sId;
	}

	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Column(name = "sku_id")
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	@Column(name = "seckill_stock")
	public Long getSeckillStock() {
		return seckillStock;
	}

	public void setSeckillStock(Long seckillStock) {
		this.seckillStock = seckillStock;
	}

	@Column(name = "seckill_price")
	public java.math.BigDecimal getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(java.math.BigDecimal seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	@Column(name = "activity_stock")
	public Long getActivityStock() {
		return activityStock;
	}

	public void setActivityStock(Long activityStock) {
		this.activityStock = activityStock;
	}
	
	/*
	 * @Column(name = "propreties") public String getPropreties(){ return
	 * propreties; }
	 * 
	 * public void setPropreties(String propreties){ this.propreties =
	 * propreties; }
	 */

}
