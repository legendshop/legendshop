package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 招标SKU导出dto
 * 
 * @author Zhongweihan
 * 
 */
public class TenderSkuImportDTO implements Serializable {

    /** SPU编码 */
    private String spuId;

    /** SKU编码 */
    private String skuId;

    /** 副标题 */
    private String skuName;

    /** 项目号 */
    private String projectNum;

	/** 物料号 */
	private String materialCode;

	/** 中标有效日期 */
	private String effectiveDate;

    /** 规格项1 */
    private String firstProperties;

	/** 规格值1 */
	private String firstPropertiesValue;

    /** SKU售价 */
    private String skuPrice;

    /** 招标价格 */
    private String tenderPrice;

    /** 可售库存 */
    private String stocks;

    /** 物流重量 */
    private String weight;

    /** 商家编码 */
    private String partyCode;

    /** 商品条形码 */
    private String modelId;

    /** 是否上架 */
    private String status;

    /** 备注 */
    private String remark;

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getProjectNum() {
		return projectNum;
	}

	public void setProjectNum(String projectNum) {
		this.projectNum = projectNum;
	}

	public String getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFirstProperties() {
		return firstProperties;
	}

	public void setFirstProperties(String firstProperties) {
		this.firstProperties = firstProperties;
	}

	public String getFirstPropertiesValue() {
		return firstPropertiesValue;
	}

	public void setFirstPropertiesValue(String firstPropertiesValue) {
		this.firstPropertiesValue = firstPropertiesValue;
	}

	public String getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(String skuPrice) {
		this.skuPrice = skuPrice;
	}

	public String getTenderPrice() {
		return tenderPrice;
	}

	public void setTenderPrice(String tenderPrice) {
		this.tenderPrice = tenderPrice;
	}

	public String getStocks() {
		return stocks;
	}

	public void setStocks(String stocks) {
		this.stocks = stocks;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
