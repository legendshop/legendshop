package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *微信token表
 */
@Entity
@Table(name = "ls_weixin_mp_token")
public class WeixinMpToken implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6797492227547684614L;

	/** 主键 */
	private Long id; 
		
	/** accessToken */
	private String accessToken; 
		
	/** 过期时长 */
	private Integer expiresIn; 
		
	/** 检验时间 */
	private Date validateTime; 
		
	public WeixinMpToken() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_MP_TOKEN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "access_token")
	public String  getAccessToken(){
		return accessToken;
	} 
		
	public void setAccessToken(String accessToken){
			this.accessToken = accessToken;
		}
		
    @Column(name = "expires_in")
	public Integer  getExpiresIn(){
		return expiresIn;
	} 
		
	public void setExpiresIn(Integer expiresIn){
			this.expiresIn = expiresIn;
		}
		
    @Column(name = "validate_time")
	public Date  getValidateTime(){
		return validateTime;
	} 
		
	public void setValidateTime(Date validateTime){
			this.validateTime = validateTime;
		}
	
} 
