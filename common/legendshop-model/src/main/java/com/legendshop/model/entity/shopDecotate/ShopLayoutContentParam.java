package com.legendshop.model.entity.shopDecotate;

/**
 * 
 * 布局内容参数
 * 
 */
public class ShopLayoutContentParam extends ShopLayoutParam {

	private static final long serialVersionUID = -8546059633441511985L;
	
	private String layoutContent;

	public String getLayoutContent() {
		return layoutContent;
	}

	public void setLayoutContent(String layoutContent) {
		this.layoutContent = layoutContent;
	}

}
