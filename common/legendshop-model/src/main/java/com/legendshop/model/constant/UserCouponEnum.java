package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 用户优惠券使用情况
 */
public enum UserCouponEnum implements IntegerEnum {
	/** 1:可使用 */
	UNUSED(1), // 可使用

	/** 2:已使用 */
	USED(2), // 已使用
	
	/** 3:已过期  [不作数据存储]*/
	OVERTIME(3); // 已过期

	/** The num. */
	private Integer useStatus;

	private UserCouponEnum(Integer useStatus) {
		this.useStatus = useStatus;
	}

	@Override
	public Integer value() {
		return useStatus;
	}

}
