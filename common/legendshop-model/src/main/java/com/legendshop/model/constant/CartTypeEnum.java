package com.legendshop.model.constant;


/**
 * 订单类型
 * @author Tony
 *
 */
public enum CartTypeEnum {

	/**
	 * 普通订单
	 */
    NORMAL("NORMAL"), 
    
    /**
     * 秒杀订单
     */
    SECKILL("SECKILL"),  
    
    /**
     * 门店订单
     */
    SHOP_STORE("SHOP_STORE"),  
    
    /**
     * 预售订单
     */
    PRESELL("PRE_SELL"),  
    
    /**
     * 团购
     */
    GROUP("GROUP"),  
    
    /**
     * 拼团
     */
    MERGE_GROUP("MERGE_GROUP"),  
    
    /**
     * 拍卖
     */
    AUCTIONS("AUCTIONS"); 

    private String code;

    private CartTypeEnum(String code) {
        this.code = code;
    }

    public static CartTypeEnum fromCode(String code) {
        for (CartTypeEnum cartTypeEnum : CartTypeEnum.values()) {
            if (cartTypeEnum.code.equals(code)) {
                return cartTypeEnum;
            }
        }
        return null;
    }

    public String toCode() {
        return code;
    }
    
}
