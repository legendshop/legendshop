/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.util.Date;

/**
 * 秒杀活动
 * 
 */
public class Seckill implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 秒杀活动id */
	private long seckillId;

	/** 开始时间 */
	private Date startTime;

	/** 结束时间 */
	private Date endTime;

	/** 商品id */
	private Long prodId;

	/** skuId */
	private Long skuId;

	/** 秒杀库存 */
	private int seckillStock;

	/** 秒杀价格 */
	private Double seckillPrice;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public long getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(long seckillId) {
		this.seckillId = seckillId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public int getSeckillStock() {
		return seckillStock;
	}

	public void setSeckillStock(int seckillStock) {
		this.seckillStock = seckillStock;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

}
