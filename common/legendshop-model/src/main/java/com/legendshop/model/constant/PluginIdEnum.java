package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum PluginIdEnum implements StringEnum {
	
	
	/**  
	 * 营销活动
	 */
	ACTIVITY("activity"),
	
	/**  
	 * 高级搜索
	 */
	ADVANCED_SEARCH("advanced_search"),
	
	B2C("b2c"),
	
	CACHEMANAGER("cachemanager"),
	
	GROUP("group"),
	
	INTEGRAL("integral"),
	
	MULTISHOP("multishop"),
	
	PAYMENT("payment"),
	
	PREDEPOSIT("predeposit"),
	
	DECORATE("decorate"),
	
	STORE("store")
	
	;

	private final String value;

	private PluginIdEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
