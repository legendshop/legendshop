package com.legendshop.model.dto.group;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP端团购列表Dto
 */
@ApiModel(value="团购列表项") 
public class AppGroupDto {
	
	@ApiModelProperty(value="活动的唯一标识") 
	private Long id;
		
	@ApiModelProperty(value="团购活动名称") 
	private String groupName; 
	
	@ApiModelProperty(value="团购活动图片") 
	private String groupImage; 
		
	@ApiModelProperty(value="团购活动开始时间") 
	private Date startTime; 
		
	@ApiModelProperty(value="团购活动结束时间") 
	private Date endTime; 
		
	@ApiModelProperty(value="团购活动的商品ID", hidden = true) 
	private Long productId; 
		
	@ApiModelProperty(value="团购活动的所属店铺ID", hidden = true) 
	private Long shopId; 
		
	@ApiModelProperty(value="团购活动的所属店铺名", hidden = true) 
	private String shopName; 
		
	@ApiModelProperty(value="团购价格") 
	private java.math.BigDecimal groupPrice;
	
	@ApiModelProperty(value="团购状态") 
	private Integer status;
		
	@ApiModelProperty(value="已购买人数") 
	private Long buyerCount; 
		
	@ApiModelProperty(value="每人限购数量", hidden = true) 
	private Integer buyQuantity; 
	
	@ApiModelProperty(value="成团人数, 暂时没用", hidden = true) 
	private Integer peopleCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupImage() {
		return groupImage;
	}

	public void setGroupImage(String groupImage) {
		this.groupImage = groupImage;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getShopId() {
		return shopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public java.math.BigDecimal getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(java.math.BigDecimal groupPrice) {
		this.groupPrice = groupPrice;
	}

	public Long getBuyerCount() {
		return buyerCount;
	}

	public void setBuyerCount(Long buyerCount) {
		this.buyerCount = buyerCount;
	}

	public Integer getBuyQuantity() {
		return buyQuantity;
	}

	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	public Integer getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Integer peopleCount) {
		this.peopleCount = peopleCount;
	}
}
