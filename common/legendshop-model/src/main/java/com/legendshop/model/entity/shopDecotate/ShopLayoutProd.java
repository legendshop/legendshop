package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家装修自定义商品模块
 */
@Entity
@Table(name = "ls_shop_layout_prod")
public class ShopLayoutProd implements GenericEntity<Long> {

	private static final long serialVersionUID = -384198550659757068L;

	/** ID  */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商家装修ID */
	private Long shopDecotateId; 
		
	/** 布局id */
	private Long layoutId; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** 顺序 */
	private Integer seq; 
		
	
	public ShopLayoutProd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_LAYOUT_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_decotate_id")
	public Long  getShopDecotateId(){
		return shopDecotateId;
	} 
		
	public void setShopDecotateId(Long shopDecotateId){
			this.shopDecotateId = shopDecotateId;
		}
		
    @Column(name = "layout_id")
	public Long  getLayoutId(){
		return layoutId;
	} 
		
	public void setLayoutId(Long layoutId){
			this.layoutId = layoutId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}
	


} 
