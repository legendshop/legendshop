package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 装修弹层分类DTO
 */
public class DecorateCategoryDto implements Serializable  {
	

	/**  */
	private static final long serialVersionUID = -4994274590288509946L;

	/** 分类ID */
	private Long id;
	
	/** 分类层级  */
	private Integer grade;
	
	/** 分类名称  */
	private String name;
	
	/** 父类ID */
	private Long parentId;
	
	/** 子分类*. */
	private List<DecorateCategoryDto> childrenList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<DecorateCategoryDto> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<DecorateCategoryDto> childrenList) {
		this.childrenList = childrenList;
	}
	
	public void addChildren(DecorateCategoryDto categoryDto) {
		if(childrenList == null){
			childrenList = new ArrayList<DecorateCategoryDto>();
		}
		childrenList.add(categoryDto);
	}

}