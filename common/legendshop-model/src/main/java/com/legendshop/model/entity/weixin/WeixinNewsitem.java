package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信新闻项目
 */
@Entity
@Table(name = "ls_weixin_newsitem")
public class WeixinNewsitem implements GenericEntity<Long> {

	private static final long serialVersionUID = 4683707778406681176L;

	/**  */
	private Long id; 
		
	/** 标题 */
	private String title; 
		
	/** 作者 */
	private String author; 
		
	/** 封面（大图片建议尺寸：900像素 * 500像素） */
	private String imagepath; 
		
	/** 封面图片显示在正文中 */
	private Long imagepathContent; 
		
	/** 摘要 */
	private String desction; 
		
	/** 图文类型：图文还是外部url */
	private Long newType; 
		
	/** 外部URL */
	private String url; 
		
	/** 排序[如果是1 代表是主图文] */
	private Long orders; 
		
	/** 图文模版ID */
	private Long newsTemplateid; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 创建用户 */
	private String createUserId; 
	
	private String content;
	
	private String thumbMediaId;
		
	
	public WeixinNewsitem() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_NEWSITEM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "author")
	public String  getAuthor(){
		return author;
	} 
		
	public void setAuthor(String author){
			this.author = author;
		}
		
    @Column(name = "imagepath")
	public String  getImagepath(){
		return imagepath;
	} 
		
	public void setImagepath(String imagepath){
			this.imagepath = imagepath;
		}
		
    @Column(name = "imagepath_content")
	public Long  getImagepathContent(){
		return imagepathContent;
	} 
		
	public void setImagepathContent(Long imagepathContent){
			this.imagepathContent = imagepathContent;
		}
		
    @Column(name = "desction")
	public String getDesction() {
		return desction;
	}

	public void setDesction(String desction) {
		this.desction = desction;
	}
	
    
		
    @Column(name = "new_type")
	public Long  getNewType(){
		return newType;
	} 
		
	public void setNewType(Long newType){
			this.newType = newType;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "orders")
	public Long  getOrders(){
		return orders;
	} 
		
	public void setOrders(Long orders){
			this.orders = orders;
		}
		
    @Column(name = "news_templateid")
	public Long  getNewsTemplateid(){
		return newsTemplateid;
	} 
		
	public void setNewsTemplateid(Long newsTemplateid){
			this.newsTemplateid = newsTemplateid;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "create_user_id")
	public String  getCreateUserId(){
		return createUserId;
	} 
		
	public void setCreateUserId(String createUserId){
			this.createUserId = createUserId;
		}

	 @Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "thumb_media_id")
	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}


	
	


} 
