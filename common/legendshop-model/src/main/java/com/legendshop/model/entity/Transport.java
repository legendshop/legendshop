package com.legendshop.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.util.AppUtils;

/**
 * 运费模板
 */
@Entity
@Table(name = "ls_transport")
public class Transport implements GenericEntity<Long>, Serializable {

	private static final long serialVersionUID = 7954562376790018123L;

	private Long id ; 
	
	//记录时间
	private Date recDate ; 
	
	//状态
	private Integer status ; 
	
	//运费名称
	private String transName;
	
	//商家id
	private Long shopId;
	
	//配送时间
	private Integer transTime;
	
	//计价方式
	private String transType;
	
	//是否区域限售  1.支持  0.不支持
	private int isRegionalSales;
	
	
	private boolean transMail;
	
	private boolean transExpress;
	
	private boolean transEms;
	
	private List<Transfee> feeList = new ArrayList<Transfee>();
	
	private List<Transfee> mailList = new ArrayList<Transfee>();
	
	private List<Transfee> expressList = new ArrayList<Transfee>();
	
	private List<Transfee> emsList = new ArrayList<Transfee>();
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TRANSPORT_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
	
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "trans_name")
	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "trans_time")
	public Integer getTransTime() {
		return transTime;
	}

	public void setTransTime(Integer transTime) {
		this.transTime = transTime;
	}

	@Column(name = "trans_type")
	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}
		
	@Column(name = "is_regional_sales")
	public int getIsRegionalSales() {
		if(AppUtils.isBlank(isRegionalSales)){
			return 0;
		}
		return isRegionalSales;
	}

	public void setIsRegionalSales(int isRegionalSales) {
		this.isRegionalSales = isRegionalSales;
	}
	
	
	@Transient
	public List<Transfee> getFeeList() {
		return feeList;
	}


	public void setFeeList(List<Transfee> feeList) {
		this.feeList = feeList;
	}
	
	public void addFeeList(Transfee transfeet) {
		if(feeList == null){
			feeList = new ArrayList<Transfee>();
		}
		feeList.add(transfeet);
	}

	@Transient
	public List<Transfee> getMailList() {
		return mailList;
	}

	public void setMailList(List<Transfee> mailList) {
		this.mailList = mailList;
	}

	@Transient
	public List<Transfee> getExpressList() {
		return expressList;
	}

	public void setExpressList(List<Transfee> expressList) {
		this.expressList = expressList;
	}

	@Transient
	public List<Transfee> getEmsList() {
		return emsList;
	}

	public void setEmsList(List<Transfee> emsList) {
		this.emsList = emsList;
	}

	@Transient
	public boolean isTransMail() {
		return transMail;
	}

	public void setTransMail(boolean transMail) {
		this.transMail = transMail;
	}

	@Transient
	public boolean isTransExpress() {
		return transExpress;
	}

	public void setTransExpress(boolean transExpress) {
		this.transExpress = transExpress;
	}

	@Transient
	public boolean isTransEms() {
		return transEms;
	}

	public void setTransEms(boolean transEms) {
		this.transEms = transEms;
	}
	
	public boolean geTransfeeIsCity(long cityId){
		if(isTransMail()){
			for (Transfee transfee : mailList) {
				if(transfee.getCityIdList().contains(cityId)){
					System.out.println(12321);
					return true;
				}
			}
		}
		if(isTransExpress()){
			for (Transfee transfee : expressList) {
				if(transfee.getCityIdList().contains(cityId)){
					return true;
				}
			}
		}
		if(isTransEms()){
			for (Transfee transfee : emsList) {
				if(transfee.getCityIdList().contains(cityId)){
					return true;
				}
			}
		}
		return false;
	}
	
}
