package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *微信关注用户
 */
@Entity
@Table(name = "ls_weixin_gzuserinfo")
public class WeixinGzuserInfo implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2298301388205112827L;

	/**  */
	private Long id; 
		
	/** 是否关注 */
	private String subscribe; 
		
	/** openid */
	private String openid; 
	
	private String unionid;
	
	/** 昵称 */
	private String nickname; 
		
	/** 性别 */
	private String sex; 
		
	/** 城市 */
	private String city; 
		
	/** 省份 */
	private String province; 
		
	/** 国家 */
	private String country; 
		
	/** 用户头像 */
	private String headimgurl; 
		
	/** 备注名称 */
	private String bzname; 
		
	/** 用户组ID */
	private String groupid; 
		
	/** 关注时间 */
	private Date subscribeTime; 
		
	/** 创建时 */
	private Date addtime; 

	private String userId; 
	
	private String groupName;
		
	@Transient
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public WeixinGzuserInfo() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_GZUSERINFO_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "subscribe")
	public String  getSubscribe(){
		return subscribe;
	} 
		
	public void setSubscribe(String subscribe){
			this.subscribe = subscribe;
		}
		
    @Column(name = "openId")
	public String  getOpenid(){
		return openid;
	} 
		
	public void setOpenid(String openid){
			this.openid = openid;
		}
	
	@Column(name = "unionid")
    public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	@Column(name = "nickname")
	public String  getNickname(){
		return nickname;
	} 
		
	public void setNickname(String nickname){
			this.nickname = nickname;
		}
		
    @Column(name = "sex")
	public String  getSex(){
		return sex;
	} 
		
	public void setSex(String sex){
			this.sex = sex;
		}
		
    @Column(name = "city")
	public String  getCity(){
		return city;
	} 
		
	public void setCity(String city){
			this.city = city;
		}
		
    @Column(name = "province")
	public String  getProvince(){
		return province;
	} 
		
	public void setProvince(String province){
			this.province = province;
		}
		
    @Column(name = "country")
	public String  getCountry(){
		return country;
	} 
		
	public void setCountry(String country){
			this.country = country;
		}
		
    @Column(name = "headimgurl")
	public String  getHeadimgurl(){
		return headimgurl;
	} 
		
	public void setHeadimgurl(String headimgurl){
			this.headimgurl = headimgurl;
		}
		
    @Column(name = "bzName")
	public String  getBzname(){
		return bzname;
	} 
		
	public void setBzname(String bzname){
			this.bzname = bzname;
		}
		
    @Column(name = "groupId")
	public String  getGroupid(){
		return groupid;
	} 
		
	public void setGroupid(String groupid){
			this.groupid = groupid;
		}
		
    @Column(name = "subscribe_time")
	public Date  getSubscribeTime(){
		return subscribeTime;
	} 
		
	public void setSubscribeTime(Date subscribeTime){
			this.subscribeTime = subscribeTime;
		}
		
    @Column(name = "addtime")
	public Date  getAddtime(){
		return addtime;
	} 
		
	public void setAddtime(Date addtime){
			this.addtime = addtime;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}



} 
