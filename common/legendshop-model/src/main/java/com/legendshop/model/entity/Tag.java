package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *Tag管理
 */
@Entity
@Table(name = "ls_tag")
public class Tag implements GenericEntity<Long> {

	private static final long serialVersionUID = -6083575348587528300L;

	/** 标签ID */
	private Long tagId; 
		
	/** 标签名字 */
	private String name; 
		
	/** P:产品，N：文章，B:品牌，G:团购产品，A:广告 */
	private String type; 
		
	/** 状态 */
	private Long status; 
		
	/** 建立时间 */
	private Date createTime; 
		
	/** 用户ID */
	private String userId; 
		
	/** 用户名称 */
	private String userName; 
		
	/** 英文code，用于页面对应的ID */
	private String code; 
		
	/** 顺序 */
	private Long seq; 
		
	List<TagItems> tagItemList = new ArrayList<TagItems>();
	
	public Tag() {
    }
		
	@Id
	@Column(name = "tag_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TAG_SEQ")
	public Long  getTagId(){
		return tagId;
	} 
		
	public void setTagId(Long tagId){
			this.tagId = tagId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "type")
	public String  getType(){
		return type;
	} 
		
	public void setType(String type){
			this.type = type;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "code")
	public String  getCode(){
		return code;
	} 
		
	public void setCode(String code){
			this.code = code;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
	


	public void addTagItems(TagItems tagItem){
		if(this.tagId.equals(tagItem.getTagId())){
			tagItemList.add(tagItem);
		}
	}

	@Transient
	public List<TagItems> getTagItemList() {
		return tagItemList;
	}

	public void setTagItemList(List<TagItems> tagItemList) {
		this.tagItemList = tagItemList;
	}

	@Override
	@Transient
	public Long getId() {
		return tagId;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		tagId = id;
	}
	
	
	
} 
