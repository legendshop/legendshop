package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 用户登录成功的实体
 *
 */
public class LoginSuccess implements Serializable {

	private static final long serialVersionUID = 2559811594936950714L;

	/**
	 * 用户Id
	 */
	private String userId;

	/**
	 * 用户名称
	 */
	private String userName;

	/**
	 * IP地址
	 */
	private String ipAddress;

	/**
	 * 用户登录类型
	 */
	private String loginUserType;

	/**
	 * 用户登录来源
	 */
	private String loginSource;

	/**
	 * 构造函数
	 * 
	 */
	public LoginSuccess(String userId, String userName, String ipAddress, String loginUserType, String loginSource) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.ipAddress = ipAddress;
		this.loginUserType = loginUserType;
		this.loginSource = loginSource;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLoginUserType() {
		return loginUserType;
	}

	public void setLoginUserType(String loginUserType) {
		this.loginUserType = loginUserType;
	}

	public String getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

}
