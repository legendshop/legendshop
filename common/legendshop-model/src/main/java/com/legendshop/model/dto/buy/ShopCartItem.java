package com.legendshop.model.dto.buy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.TransportUtil;
import com.legendshop.model.constant.FreightModeEnum;
import com.legendshop.model.constant.SupportTransportFreeEnum;
import com.legendshop.model.constant.TransportTypeEnum;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.entity.Transfee;
import com.legendshop.model.entity.Transport;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 购物车里的商品
 *
 */
public class ShopCartItem  implements Serializable {

	private static final long serialVersionUID = 6617946845821083794L;
    
	/** 购物车ID */
    private Long basketId;
    
    /**选中状态 */
    private int checkSts;

    /** 商品ID */
    private Long prodId;
    
    /** shop ID  */
    private Long shopId;
	
    /** skuId */
	private Long skuId;
    
	/** 商品原价*/
	private Double price=0d;
	
	/** 促销价格  */
	private Double promotionPrice; 
    
	/** sku图片  */
    private String pic;

    /** 购物车商品  */
    private Integer basketCount=0;

    /** 商品名称  */
    private String prodName;
    
    /** sku名称  */
    private String skuName;

    /** 订单号  */
    private String subNumber;
    
	 /** 商品状态 */
    private Integer status;

    /** 库存*/
    private Integer stocks=0;
    
    /** 实际库存*/
	protected Integer actualStocks=0;
	
	/** 库存计数方式，0：拍下减库存，1：付款减库存*/
    private Integer stockCounting;
    
    /** 物流重量(千克)  */
    private Double weight=0d;
    
    /** 物流体积(立方米)  */
    private Double volume=0d;
    
    /** 运费模板ID  */
    private Long transportId;
    
    /** 是否参与包邮活动*/
	private boolean isActiv=false;
	
	/** 单品包邮简介*/
	private String activIntro;
    
    /** 是否承担运费[1:商家承担运费;0: 买家承担运费]   */
    private Integer transportFree;
	
    /** 是否固定运费[0:使用运费模板;1:固定运费]   */
	private Integer transportType;
	
	  /** 固定运费-EMS  */
	private Float emsTransFee;
	
	  /** 固定运费-快递   */
	private Float expressTransFee;
	
	  /** 固定运费-平邮   */
	private Float mailTransFee;
	
	 /** 货到付款; 0:普通商品 , 1:货到付款商品   */
	private boolean isSupportCod;
	
	 /** 是否参加团购    */
	private int isGroup;
	
    /** 销售属性组合（中文）  */
    private String cnProperties;//中文属性
    
    /** 属性Id组合 */
    private String properties;
    
	/** 商品总费用  商品总价格   单价*数量 */
	private Double total;
	
    /** 商品实际总费用  商品总价格  + 运费  - 促销费用*/
	private Double totalMoeny;
	
	/** 总重量 */
	private Double totalWeight=0d;
	
	/** 总大小 */
	private Double totalVolume=0d;
	
	/** 是否支持门店购买  */
	private boolean supportStore=false; 
	
	/** 商品省份ID 用于商品仓库结算 */
	private Integer provinceId;
	
	/** 是否支持分销 **/
    private Integer supportDist;
	
	/** 分销佣金比例 **/
    private Double distCommisRate;
	
    /** 分销的用户名称 **/
    private String distUserName;
    
    /** 会员直接上级分佣比例 **/
    private Double firstLevelRate;
    
    /** 会员上二级分佣比例  **/
    private Double secondLevelRate;
    
    /** 会员上三级分佣比例  **/
    private Double thirdLevelRate;
	
	
	/** -------------------------营销活动信息----------------------  */
   
	/** 优惠价价格 */
	private Double discountPrice=0d; 
	
	/** 折扣 */
	private Double discount=0d;
	
	/**  营销规则  **/
    private List<CartMarketRules> cartMarketRules;
	
	/** -------------------------营销活动信息----------------------  */
    
    /* 运费 模版 */
    private Transport transport;
    
    /* 是否计算营销活动[团购 预售 秒杀 ]  */
    private  boolean calculateMarketing=true;
    
    /** 是否失效 */
    private boolean isFailure;


	//计算了营销活动之后的价格（包括限 时折扣促销  ，满折 ， 满减 ）
	private double discountedPrice=0.0;

	//使用的红包的优惠金额
	private double redpackOffPrice=0.0;

	//使用的优惠券优惠金额
	private double couponOffPrice=0.0;
	
	private String shippingStr;//商品活动包邮信息


	/** -------------------------END 营销活动信息----------------------  */
	/** 所选门店是否存在该商品（false为不存在 ）  用于门店自提 */
	private Boolean storeIsExist;
	
	/** 门店ID */
	private Long storeId;
	
	/** 门店名称  */
	private String storeName;
	
	/** 门店详细地址  */
	private String storeAddr;

	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public Integer getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Integer actualStocks) {
		this.actualStocks = actualStocks;
	}

	public Integer getStockCounting() {
		return stockCounting;
	}

	public void setStockCounting(Integer stockCounting) {
		this.stockCounting = stockCounting;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public Integer getTransportFree() {
		return transportFree;
	}

	public void setTransportFree(Integer transportFree) {
		this.transportFree = transportFree;
	}

	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	public Float getEmsTransFee() {
		return emsTransFee;
	}

	public void setEmsTransFee(Float emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	public Float getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Float expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	public Float getMailTransFee() {
		return mailTransFee;
	}

	public void setMailTransFee(Float mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	public boolean isSupportCod() {
		return isSupportCod;
	}

	public void setSupportCod(boolean isSupportCod) {
		this.isSupportCod = isSupportCod;
	}

	public int getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(int isGroup) {
		this.isGroup = isGroup;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getSupportDist() {
		return supportDist;
	}

	public void setSupportDist(Integer supportDist) {
		this.supportDist = supportDist;
	}

	public Double getDistCommisRate() {
		return distCommisRate;
	}

	public void setDistCommisRate(Double distCommisRate) {
		this.distCommisRate = distCommisRate;
	}

	public String getDistUserName() {
		return distUserName;
	}

	public void setDistUserName(String distUserName) {
		this.distUserName = distUserName;
	}

	public Double getFirstLevelRate() {
		return firstLevelRate;
	}

	public void setFirstLevelRate(Double firstLevelRate) {
		this.firstLevelRate = firstLevelRate;
	}

	public Double getSecondLevelRate() {
		return secondLevelRate;
	}

	public void setSecondLevelRate(Double secondLevelRate) {
		this.secondLevelRate = secondLevelRate;
	}

	public Double getThirdLevelRate() {
		return thirdLevelRate;
	}

	public void setThirdLevelRate(Double thirdLevelRate) {
		this.thirdLevelRate = thirdLevelRate;
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public Double getTotalMoeny() {
		return totalMoeny;
	}

	public void setTotalMoeny(Double totalMoeny) {
		this.totalMoeny = totalMoeny;
	}
    
//	/**
//	 * 运费模版配送方式 没有人调用, 暂时屏蔽 Newway,如果没有影响则可以删除
//	 * @return
//	 */
//	public List<TransfeeDto> getTransportTransfeeDtos(Integer userCityId) {
//		if (AppUtils.isBlank(userCityId)) {
//			return null;
//		}
//		if (SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(this.getTransportFree())) {
//			if (TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(this.getTransportType())) { //如果是有运费模版
//				Long transportId = this.getTransportId();
//				Transport transport = this.getTransport();
//				if (transportId != null && transport != null) { // 加载运费模板
//					List<TransfeeDto> dtos = geTransfeeDtos(transport,userCityId);
//					return dtos;
//				}
//				return null;
//			}
//		}
//		return null;
//	}
	
//	/**
//	 * 固定运费 暂时屏蔽 Newway,如果没有影响则可以删除
//	 * @return
//	 */
//	public List<TransfeeDto> getFixedTransportTransfeeDtos() {
//		if (SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(this.getTransportFree())) {
//			if(TransportTypeEnum.FIXED_TEMPLE.value().equals(this.getTransportType())) { //如果是固定模版
//				List<TransfeeDto> dtos = getFixedTransport();
//				return dtos;
//			}
//		}
//		return null;
//	}
	
	
//	private  List<TransfeeDto> getFixedTransport(){
//		List<TransfeeDto> transfeeDtos=new ArrayList<TransfeeDto>();
//		 if(AppUtils.isNotBlank(this.getEmsTransFee())){
//			 double totalEms=this.getEmsTransFee();
//			 transfeeDtos.add(new TransfeeDto(totalEms,FreightModeEnum.TRANSPORT_EMS.value()));
//		 }
//		 if(AppUtils.isNotBlank(this.getExpressTransFee())){
//			 double totalExpress=this.getExpressTransFee();
//			 transfeeDtos.add(new TransfeeDto(totalExpress,FreightModeEnum.TRANSPORT_EXPRESS.value()));
//		 }
//		 if(AppUtils.isNotBlank(this.getMailTransFee())){
//			 double totalMail=this.getMailTransFee();
//			 transfeeDtos.add(new TransfeeDto(totalMail,FreightModeEnum.TRANSPORT_MAIL.value()));
//		 }
//		 return transfeeDtos;
//	}
	
//	private List<TransfeeDto> geTransfeeDtos(Transport  _transport ,Integer userCityId) {
//		List<TransfeeDto> transfeeDtos=new ArrayList<TransfeeDto>();
//		if(_transport.isTransEms()){
//			 Transfee transfee  = TransportUtil.getTransfeeDto(_transport, userCityId,FreightModeEnum.TRANSPORT_EMS.value());
//			 if(transfee!=null){
//				  double freeAmount=TransportUtil.clacCartDeleivery(_transport.getTransType(),transfee,getTotalWeight(),getTotalVolume(),getBasketCount()); //该配送方式的总价格
//				  TransfeeDto emsDto=new TransfeeDto();
//				  emsDto.setDeliveryAmount(freeAmount);
//				  emsDto.setDesc("EMS");
//				  emsDto.setFreightMode(FreightModeEnum.TRANSPORT_EMS.value());
//				  transfeeDtos.add(emsDto);
//			  }
//		}
//		if(_transport.isTransExpress()){
//			 Transfee transfee  = TransportUtil.getTransfeeDto(_transport, userCityId,FreightModeEnum.TRANSPORT_EXPRESS.value());
//			 if(transfee!=null){
//				  double freeAmount=TransportUtil.clacCartDeleivery(_transport.getTransType(),transfee,getTotalWeight(),getTotalVolume(),getBasketCount()); //该配送方式的总价格
//				  TransfeeDto express=new TransfeeDto();
//				  express.setDeliveryAmount(freeAmount);
//				  express.setDesc("express");
//				  express.setFreightMode(FreightModeEnum.TRANSPORT_EXPRESS.value());
//				  transfeeDtos.add(express);
//			 }
//		}
//       if(_transport.isTransMail()){
//			 Transfee transfee  = TransportUtil.getTransfeeDto(_transport, userCityId,FreightModeEnum.TRANSPORT_MAIL.value());
//			 if(transfee!=null){
//				  double freeAmount=TransportUtil.clacCartDeleivery(transport.getTransType(),transfee,getTotalWeight(),getTotalVolume(),getBasketCount()); //该配送方式的总价格
//				  TransfeeDto mail=new TransfeeDto();
//				  mail.setDeliveryAmount(freeAmount);
//				  mail.setDesc("mail");
//				  mail.setFreightMode(FreightModeEnum.TRANSPORT_MAIL.value());
//				  transfeeDtos.add(mail);
//			  }
//		}
//	  return transfeeDtos;
//	}
	
	public void addCartMarketRules(CartMarketRules cartMarketRule) {
		if(AppUtils.isBlank(this.cartMarketRules)){
			this.cartMarketRules=new ArrayList<CartMarketRules>(); 
		}
		this.cartMarketRules.add(cartMarketRule);
	}

	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Double getTotalWeight() {
		return totalWeight;
	}

	public Double getTotalVolume() {
		return totalVolume;
	}

	public List<CartMarketRules> getCartMarketRules() {
		return cartMarketRules;
	}

	public void setCartMarketRules(List<CartMarketRules> cartMarketRules) {
		this.cartMarketRules = cartMarketRules;
	}

	public boolean isCalculateMarketing() {
		return calculateMarketing;
	}

	public void setCalculateMarketing(boolean calculateMarketing) {
		this.calculateMarketing = calculateMarketing;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public int getCheckSts() {
		return checkSts;
	}

	public void setCheckSts(int checkSts) {
		this.checkSts = checkSts;
	}

	public boolean getIsFailure() {
		return isFailure;
	}

	public void setIsFailure(boolean isFailure) {
		this.isFailure = isFailure;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prodId == null) ? 0 : prodId.hashCode());
		result = prime * result + ((skuId == null) ? 0 : skuId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopCartItem other = (ShopCartItem) obj;
		if (prodId == null) {
			if (other.prodId != null)
				return false;
		} else if (!prodId.equals(other.prodId))
			return false;
		if (skuId == null) {
			if (other.skuId != null)
				return false;
		} else if (!skuId.equals(other.skuId))
			return false;
		return true;
	}


	public double getDiscountedPrice() {
		return this.discountedPrice;
	}

	public void setDiscountedPrice(double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}
	
	
	public String getShippingStr() {
		return shippingStr;
	}

	public void setShippingStr(String shippingStr) {
		this.shippingStr = shippingStr;
	}

	//计算营销活动及红包，优惠券之后的真实金额
	public double getCalculatedCouponAmount(){
		return Arith.sub(Arith.sub(discountedPrice, redpackOffPrice),couponOffPrice);
	}

	public Boolean getStoreIsExist() {
		return storeIsExist;
	}

	public void setStoreIsExist(Boolean storeIsExist) {
		this.storeIsExist = storeIsExist;
	}

	public boolean isActiv() {
		return isActiv;
	}

	public void setActiv(boolean isActiv) {
		this.isActiv = isActiv;
	}

	public String getActivIntro() {
		return activIntro;
	}

	public void setActivIntro(String activIntro) {
		this.activIntro = activIntro;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreAddr() {
		return storeAddr;
	}

	public void setStoreAddr(String storeAddr) {
		this.storeAddr = storeAddr;
	}
	
}
