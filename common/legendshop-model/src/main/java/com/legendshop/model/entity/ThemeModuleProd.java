package com.legendshop.model.entity;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *专题板块商品
 */
@Entity
@Table(name = "ls_theme_module_prd")
public class ThemeModuleProd implements GenericEntity<Long> {

	private static final long serialVersionUID = 102880968426431956L;

	/** 专题板块商品id */
	private Long modulePrdId; 
		
	/** 板块id */
	private Long themeModuleId; 
		
	/** 商品id */
	private Long prodId; 
		
	/** 商品名称 */
	private String prodName; 
		
	/** 商品图片 */
	private String img; 
		
	/** 角标图片 */
	private String angleIcon; 
		
	/** 是否售罄 */
	private Long isSoldOut; 
	
	private MultipartFile imgFile;
	
	private MultipartFile angleIconFile;
	
	private Double cash;
	
	private String cnSymbol;
		
	/**
	 * 人民币价格
	 */
	private double rmbCash;
	
	/**
	 * 货币符号
	 */
	private String symbol;
	
	public ThemeModuleProd() {
    }
		
	@Id
	@Column(name = "module_prd_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "LS_THEME_MODULE_PRD_SEQ")
	public Long  getModulePrdId(){
		return modulePrdId;
	} 
		
	public void setModulePrdId(Long modulePrdId){
			this.modulePrdId = modulePrdId;
		}
		
    @Column(name = "theme_module_id")
	public Long  getThemeModuleId(){
		return themeModuleId;
	} 
		
	public void setThemeModuleId(Long themeModuleId){
			this.themeModuleId = themeModuleId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "prod_name")
	public String  getProdName(){
		return prodName;
	} 
		
	public void setProdName(String prodName){
			this.prodName = prodName;
		}
		
    @Column(name = "img")
	public String  getImg(){
		return img;
	} 
		
	public void setImg(String img){
			this.img = img;
		}
		
    @Column(name = "angle_icon")
	public String  getAngleIcon(){
		return angleIcon;
	} 
		
	public void setAngleIcon(String angleIcon){
			this.angleIcon = angleIcon;
		}
		
    @Column(name = "is_sold_out")
	public Long  getIsSoldOut(){
		return isSoldOut;
	} 
		
	public void setIsSoldOut(Long isSoldOut){
			this.isSoldOut = isSoldOut;
		}
	
	@Transient
	public Long getId() {
		return modulePrdId;
	}
	
	public void setId(Long id) {
		modulePrdId = id;
	}

	@Transient
	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	@Transient
	public String getCnSymbol() {
		return cnSymbol;
	}

	public void setCnSymbol(String cnSymbol) {
		this.cnSymbol = cnSymbol;
	}

	@Transient
	public MultipartFile getImgFile() {
		return imgFile;
	}

	public void setImgFile(MultipartFile imgFile) {
		this.imgFile = imgFile;
	}

	@Transient
	public MultipartFile getAngleIconFile() {
		return angleIconFile;
	}

	public void setAngleIconFile(MultipartFile angleIconFile) {
		this.angleIconFile = angleIconFile;
	}

	@Transient
	public double getRmbCash() {
		return rmbCash;
	}

	public void setRmbCash(double rmbCash) {
		this.rmbCash = rmbCash;
	}

	@Transient
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


} 
