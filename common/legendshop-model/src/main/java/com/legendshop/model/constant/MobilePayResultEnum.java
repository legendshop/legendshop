package com.legendshop.model.constant;

/**
 * 移动端支付完成跳转地址
 * */
public enum MobilePayResultEnum {
	// 普通订单
	COMMON_ORDER_RESULT("/commonModules/submitOrder/orderPayResult",1),
	// 预存款
	PRE_DEPOSIT_RESULT("/walletModules/preDeposit/rechargeResult",2),
	// 营销活动
	MARKETING_ORDER_RESULT("/marketingModules/fight/fightGroupDetail",3),

	;

	private String addr;

	private Integer code;

	MobilePayResultEnum(String addr, Integer code) {
		this.addr = addr;
		this.code = code;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
