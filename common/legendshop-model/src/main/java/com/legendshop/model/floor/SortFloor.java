package com.legendshop.model.floor;

import java.io.Serializable;

public class SortFloor implements Serializable{
	
	private static final long serialVersionUID = 3972728907660141795L;

	private Long pid;
	
	private Long id;
	
	private Integer level;
	
	private Long referId ; 
	
	public SortFloor() {
	}
	
	public SortFloor( Long pid,Long id,Integer level) {
		this.pid = pid;
		this.id = id;
		this.level = level;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getReferId() {
		return referId;
	}

	public void setReferId(Long referId) {
		this.referId = referId;
	}
	
}
