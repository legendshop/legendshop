/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModelProperty;

/**
 * 商城实体抽象类.
 */
public abstract class AbstractShopDetail extends AbstractEntity implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9153182932362623738L;

	/** 商家Id，全局唯一. */
	@ApiModelProperty(value = "商家Id，全局唯一")
	protected Long shopId;

    /** 用户Id. */
	@ApiModelProperty(value = "用户Id")
    protected String userId;

    /** 用户名. */
	@ApiModelProperty(value = "用户名")
    protected String userName;

    /** 商城名字. */
	@ApiModelProperty(value = "商城名字")
    protected String siteName;

    /** 店铺地址. */
	@ApiModelProperty(value = "店铺地址")
    protected String shopAddr;

    /** 银行汇款帐号. */
	@ApiModelProperty(value = "银行汇款帐号",hidden = true)
    protected String bankCard;

    /** 收款人姓名. */
	@ApiModelProperty(value = "收款人姓名",hidden = true)
    protected String payee;

    /** 邮政编码. */
    @ApiModelProperty(value = "邮政编码",hidden = true)
    protected String code;

    /**  邮寄地址. */
    @ApiModelProperty(value = "邮寄地址",hidden = true)
    protected String postAddr;

    /** 邮递接收人. */
    @ApiModelProperty(value = " 邮递接收人",hidden = true)
    protected String recipient;

    /** 商城状态. */
    @ApiModelProperty(value = "商城状态")
    protected Integer status;
    
    /** 商城操作状态，临时状态. */
    @ApiModelProperty(value = "商城操作状态，临时状态")
    protected  Integer opStatus;

    /** 访问次数. */
    @ApiModelProperty(value = "访问次数")
    protected Long visitTimes;
    
    /** 今天的访问次数. */
    @ApiModelProperty(value = "今天的访问次数")
    protected Long visitTimesToday;
    
    /** 产品数量. */
    @ApiModelProperty(value = " 产品数量")
    protected Long productNum;
    
    /** 评论数量. */
    @ApiModelProperty(value = "评论数量")
    protected Long commNum;
    
    /** 下线产品数量. */
    @ApiModelProperty(value = "下线产品数量")
    protected Long offProductNum;
    
    /** 修改时间. */
    @ApiModelProperty(value = "修改时间")
    protected Date modifyDate;

    /** 记录时间. */
    @ApiModelProperty(value = "记录时间",hidden = true)
    protected Date recDate;

    /** 简要描述. */
    @ApiModelProperty(value = "简要描述")
    protected String briefDesc;

    /** 详细描述. */
    @ApiModelProperty(value = "详细描述")
    protected String detailDesc;

    /** 商城图片. */
    @ApiModelProperty(value = "商城图片")
    protected String shopPic;
    
    /** 商城图片2. */
    @ApiModelProperty(value = "商城图片2")
    protected String shopPic2;
    
    /** Tlogo图片. */
    @ApiModelProperty(value = "Tlogo图片")
    protected String logoPic;

    /** 用户Email. */
    @ApiModelProperty(value = "用户Email",hidden = true)
    protected String userMail;

    /** 用户地址. */
    @ApiModelProperty(value = "用户地址",hidden = true)
    protected String userAdds;

    /** 用户电话. */
    @ApiModelProperty(value = "用户电话",hidden = true)
    protected String userTel;

    /** 邮编. */
    @ApiModelProperty(value = "邮编",hidden = true)
    protected String userPostcode;
    
    /** 商家等级Id. */
    @ApiModelProperty(value = "商家等级Id")
    protected Integer gradeId;
    
    /** 商城类型0：个人用户，1：商家用户， 参见ShopTypeEnum. */
    @ApiModelProperty(value = "商城类型0：个人用户，1：商家用户， 参见ShopTypeEnum")
    protected Integer type;
    
    /** 身份证图片. */
    @ApiModelProperty(value = "身份证图片",hidden = true)
    protected String idCardPic;
    
    /** 身份证背面图片. */
    @ApiModelProperty(value = "身份证背面图片",hidden = true)
    protected String idCardBackPic;
    
	/** 身份证图片. */
    @ApiModelProperty(value = "身份证图片",hidden = true)
	protected MultipartFile idCardPicFile;
	
	/** 身份证背面图片. */
	@ApiModelProperty(value = "身份证背面图片",hidden = true)
	protected MultipartFile idCardBackPicFile;
	
	/** The traffic pic file. */
	@ApiModelProperty(value = "",hidden = true)
	protected MultipartFile trafficPicFile;
	
	/** 广告图片. */
	@ApiModelProperty(value = "广告图片",hidden = true)
	protected MultipartFile bannerfile;
	
	/** 广告图片. */
	@ApiModelProperty(value = "广告图片",hidden = true)
	private String bannerPic;
    
    /** 身份证号码. */
	@ApiModelProperty(value = "身份证号码",hidden = true)
    protected String idCardNum;
    
    /** 真实路径. */
    @ApiModelProperty(value = "真实路径",hidden = true)
    protected String realPath;
    
    /**创建时的地区. */
    @ApiModelProperty(value = "创建时的地区")
    protected String createAreaCode;
    
    /** 创建时的国家码. */
    @ApiModelProperty(value = "创建时的国家码")
    protected String createCountryCode;
    
    /** ip地址. */
    @ApiModelProperty(value = "ip地址",hidden = true)
    protected String ip;
    
    /** 省份Id. */
    @ApiModelProperty(value = "省份Id")
    protected Integer provinceid;
    
    /** 城市Id. */
    @ApiModelProperty(value = "城市Id")
    protected Integer cityid;
    
    /** 地区Id. */
    @ApiModelProperty(value = "地区Id")
    protected Integer areaid;
    
    /** 前台模版. */
    @ApiModelProperty(value = "前台模版",hidden = true)
    protected String frontEndTemplet;
    
    /** 前台模版. */
    @ApiModelProperty(value = "前台模版",hidden = true)
    protected String backEndTemplet;
    
    /** 前台语言选项. */
    @ApiModelProperty(value = "前台语言选项",hidden = true)
    protected String frontEndLanguage;
    
    /** 后台语言选项. */
    @ApiModelProperty(value = "后台语言选项",hidden = true)
    protected String backEndLanguage;

    /** 前台风格. */
    @ApiModelProperty(value = "前台风格",hidden = true)
    protected String frontEndStyle;
	
	/** 后台风格. */
    @ApiModelProperty(value = "后台风格",hidden = true)
	protected String backEndStyle;
	
	/** 独立域名. */
	@ApiModelProperty(value = "独立域名",hidden = true)
	protected String domainName;
	
	/** 二级域名. */
	@ApiModelProperty(value = "二级域名",hidden = true)
	protected String secDomainName;
	
	/** 二级域名注册时间 */
	@ApiModelProperty(value = "二级域名注册时间",hidden = true)
	protected Date secDomainRegDate;
	
	/** 备案信息. */
	@ApiModelProperty(value = "备案信息",hidden = true)
	protected String icpInfo;
	
	/** qq号列表. */
	@ApiModelProperty(value = "qq号列表",hidden = true)
	protected List<String> qqList; //copy from userDetail qq
	
	/** 商家的现金. */
	@ApiModelProperty(value = "商家的现金",hidden = true)
	protected Double capital;
	
	/** 商家信誉度. */
	@ApiModelProperty(value = "商家信誉度")
	protected Integer credit;
	
	/** 商家主题. */
	@ApiModelProperty(value = "商家主题",hidden = true)
	protected String theme;

	/** 联系人姓名. */
	@ApiModelProperty(value = "联系人姓名")
	protected String contactName;
	
	/** 联系人电话. */
	@ApiModelProperty(value = "联系人电话")
	protected String contactTel;
	
	/** 联系人手机. */
	@ApiModelProperty(value = "联系人手机")
	protected String contactMobile;
	
	/** 联系人QQ. */
	@ApiModelProperty(value = "联系人QQ")
	protected String contactQQ;
	
	/** 联系人邮件. */
	@ApiModelProperty(value = "联系人邮件")
	protected String contactMail;
	
	/** 审核意见. */
	@ApiModelProperty(value = "审核意见",hidden = true)
	protected String auditOpinion;
	
	/** 商品是否需要审核 1:是 0:否 为空则采用平台总设置 */
	@ApiModelProperty(value = "商品是否需要审核 1:是 0:否 为空则采用平台总设置",hidden = true)
	protected Integer prodRequireAudit;
	
	/**店铺类型 0专营,1旗舰,2自营**/
	@ApiModelProperty(value = "店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;
	
	/**包邮状态：1：开启  0 关闭**/
	@ApiModelProperty(value = "包邮状态：1：开启  0 关闭")
	protected Integer mailfeeSts;
	
	/**包邮类型：1：满金额  2：满件**/
	@ApiModelProperty(value = "包邮类型：1：满金额  2：满件")
	protected Integer mailfeeType;
	
	/**包邮条件的数量**/
	@ApiModelProperty(value = "包邮条件的数量")
	protected Double mailfeeCon;
	
	/** 是否开启发票 */
	@ApiModelProperty(value = "是否开启发票")
	protected Integer switchInvoice;
	
	/** 结算佣金 */
	@ApiModelProperty(hidden=true)
	protected Integer commissionRate;
	
	/** 代表一个文件. */
	@ApiModelProperty(hidden=true)
	protected MultipartFile file;

	/** 退货地址. */
	@ApiModelProperty(value = "退货地址")
	protected String returnShopAddr;

	/** 退货省份Id. */
	@ApiModelProperty(value = "退货省份Id")
	protected Integer returnProvinceId;

	/** 退货城市Id. */
	@ApiModelProperty(value = "退货城市Id")
	protected Integer returnCityId;

	/** 退货地区Id. */
	@ApiModelProperty(value = "退货地区Id")
	protected Integer returnAreaId;

	/** 营业时间. */
	@ApiModelProperty(value = "营业时间")
	protected String businessHours;
	/** 企业名称. */
	@ApiModelProperty(value = "企业名称")
	protected String enterpriseName;
	/** 营业执照注册号. */
	@ApiModelProperty(value = "营业执照注册号")
	protected String registerNumber;
	/** 法定代表人姓名. */
	@ApiModelProperty(value = "法定代表人姓名")
	protected String representative;
	/** 营业执照所在地. */
	@ApiModelProperty(value = "营业执照所在地")
	protected String businessAddress;
	/** 企业注册资金. */
	@ApiModelProperty(value = "企业注册资金")
	protected String registerAmount;
	/** 营业执照有效期. */
	@ApiModelProperty(value = "营业执照有效期")
	protected String availableTime;
	/** 营业执照经营范围. */
	@ApiModelProperty(value = "营业执照经营范围")
	protected String businessRange;
	/** 营业执照图片. */
	@ApiModelProperty(value = "营业执照图片")
	protected String businessImage;
	/** 营业执照图片. */
	@ApiModelProperty(value = "营业执照图片")
	protected MultipartFile businessImageFile;




	/**
	 * Gets the shop id.
	 * 
	 * @return the shop id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_DETAIL_SEQ")
	@Column(name="shop_id")
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 * 
	 * @param shopId
	 *            the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name="user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 商城名称
	 * @return the user name
	 */
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the site name.
	 * 
	 * @return the site name
	 */
	@Column(name="site_name")
	public String getSiteName() {
		return siteName;
	}

	/**
	 * Sets the site name.
	 * 
	 * @param siteName
	 *            the new site name
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * Gets the shop addr.
	 * 
	 * @return the shop addr
	 */
	@Column(name="shop_addr")
	public String getShopAddr() {
		return shopAddr;
	}

	/**
	 * Sets the shop addr.
	 * 
	 * @param shopAddr
	 *            the new shop addr
	 */
	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}

	/**
	 * Gets the bank card.
	 * 
	 * @return the bank card
	 */
	@Column(name="bank_card")
	public String getBankCard() {
		return bankCard;
	}

	/**
	 * Sets the bank card.
	 * 
	 * @param bankCard
	 *            the new bank card
	 */
	public void setBankCard(String bankCard) {
		this.bankCard = bankCard;
	}

	/**
	 * Gets the payee.
	 * 
	 * @return the payee
	 */
	@Column(name="payee")
	public String getPayee() {
		return payee;
	}

	/**
	 * Sets the payee.
	 * 
	 * @param payee
	 *            the new payee
	 */
	public void setPayee(String payee) {
		this.payee = payee;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	@Column(name="code")
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the post addr.
	 * 
	 * @return the post addr
	 */
	@Column(name="post_addr")
	public String getPostAddr() {
		return postAddr;
	}

	/**
	 * Sets the post addr.
	 * 
	 * @param postAddr
	 *            the new post addr
	 */
	public void setPostAddr(String postAddr) {
		this.postAddr = postAddr;
	}

	/**
	 * Gets the recipient.
	 * 
	 * @return the recipient
	 */
	@Column(name="recipient")
	public String getRecipient() {
		return recipient;
	}

	/**
	 * Sets the recipient.
	 * 
	 * @param recipient
	 *            the new recipient
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the visit times.
	 * 
	 * @return the visit times
	 */
	@Column(name="op_status")
	public Integer getOpStatus() {
		return opStatus;
	}

	public void setOpStatus(Integer opStatus) {
		this.opStatus = opStatus;
	}

	
	
	@Column(name="visit_times")
	public Long getVisitTimes() {
		return visitTimes;
	}

	/**
	 * Sets the visit times.
	 * 
	 * @param visitTimes
	 *            the new visit times
	 */
	public void setVisitTimes(Long visitTimes) {
		this.visitTimes = visitTimes;
	}

	/**
	 * Gets the visit times today.
	 * 
	 * @return the visit times today
	 */
	@Transient
	public Long getVisitTimesToday() {
		return visitTimesToday;
	}

	/**
	 * Sets the visit times today.
	 * 
	 * @param visitTimesToday
	 *            the new visit times today
	 */
	public void setVisitTimesToday(Long visitTimesToday) {
		this.visitTimesToday = visitTimesToday;
	}

	/**
	 * Gets the product num.
	 * 
	 * @return the product num
	 */
	@Column(name="product_num")
	public Long getProductNum() {
		return productNum;
	}

	/**
	 * Sets the product num.
	 * 
	 * @param productNum
	 *            the new product num
	 */
	public void setProductNum(Long productNum) {
		this.productNum = productNum;
	}

	/**
	 * Gets the comm num.
	 * 
	 * @return the comm num
	 */
	@Column(name="comm_num")
	public Long getCommNum() {
		return commNum;
	}

	/**
	 * Sets the comm num.
	 * 
	 * @param commNum
	 *            the new comm num
	 */
	public void setCommNum(Long commNum) {
		this.commNum = commNum;
	}

	/**
	 * Gets the off product num.
	 * 
	 * @return the off product num
	 */
	@Column(name="off_product_num")
	public Long getOffProductNum() {
		return offProductNum;
	}

	/**
	 * Sets the off product num.
	 * 
	 * @param offProductNum
	 *            the new off product num
	 */
	public void setOffProductNum(Long offProductNum) {
		this.offProductNum = offProductNum;
	}


	/**
	 * Gets the modify time.
	 * 
	 * @return the modify time
	 */
	@Column(name="modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * Sets the modify time.
	 *
	 * @param modifyTime the new modify date
	 */
	public void setModifyDate(Date modifyTime) {
		this.modifyDate = modifyTime;
	}

	/**
	 * Gets the recDate.
	 * 
	 * @return the recDate
	 */
	@Column(name="rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the recDate.
	 * 
	 * @param recDate
	 *            the new recDate
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the brief desc.
	 * 
	 * @return the brief desc
	 */
	@Column(name="brief_desc")
	public String getBriefDesc() {
		return briefDesc;
	}

	/**
	 * Sets the brief desc.
	 * 
	 * @param briefDesc
	 *            the new brief desc
	 */
	public void setBriefDesc(String briefDesc) {
		this.briefDesc = briefDesc;
	}

	/**
	 * Gets the detail desc.
	 * 
	 * @return the detail desc
	 */
	@Column(name="detail_desc")
	public String getDetailDesc() {
		return detailDesc;
	}

	/**
	 * Sets the detail desc.
	 * 
	 * @param detailDesc
	 *            the new detail desc
	 */
	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}

	/**
	 * Gets the shop pic.
	 * 
	 * @return the shop pic
	 */
	@Column(name="shop_pic")
	public String getShopPic() {
		return shopPic;
	}

	/**
	 * Sets the shop pic.
	 * 
	 * @param shopPic
	 *            the new shop pic
	 */
	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	/**
	 * Gets the user mail.
	 * 
	 * @return the user mail
	 */
	@Transient
	public String getUserMail() {
		return userMail;
	}

	/**
	 * Sets the user mail.
	 * 
	 * @param userMail
	 *            the new user mail
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	/**
	 * Gets the user adds.
	 * 
	 * @return the user adds
	 */
	@Transient
	public String getUserAdds() {
		return userAdds;
	}

	/**
	 * Sets the user adds.
	 * 
	 * @param userAdds
	 *            the new user adds
	 */
	public void setUserAdds(String userAdds) {
		this.userAdds = userAdds;
	}

	/**
	 * Gets the user tel.
	 * 
	 * @return the user tel
	 */
	@Transient
	public String getUserTel() {
		return userTel;
	}

	/**
	 * Sets the user tel.
	 * 
	 * @param userTel
	 *            the new user tel
	 */
	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	/**
	 * Gets the user postcode.
	 * 
	 * @return the user postcode
	 */
	@Transient
	public String getUserPostcode() {
		return userPostcode;
	}

	/**
	 * Sets the user postcode.
	 * 
	 * @param userPostcode
	 *            the new user postcode
	 */
	public void setUserPostcode(String userPostcode) {
		this.userPostcode = userPostcode;
	}

	/**
	 * Gets the grade id.
	 * 
	 * @return the grade id
	 */
	@Column(name="grade_id")
	public Integer getGradeId() {
		return gradeId;
	}

	/**
	 * Sets the grade id.
	 * 
	 * @param gradeId
	 *            the new grade id
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	@Column(name="type")
	public Integer getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Gets the id card pic.
	 * 
	 * @return the id card pic
	 */
	@Column(name="id_card_pic")
	public String getIdCardPic() {
		return idCardPic;
	}

	/**
	 * Sets the id card pic.
	 * 
	 * @param idCardPic
	 *            the new id card pic
	 */
	public void setIdCardPic(String idCardPic) {
		this.idCardPic = idCardPic;
	}

/*	@Column(name="traffic_pic")
	public String getTrafficPic() {
		return trafficPic;
	}

	public void setTrafficPic(String trafficPic) {
		this.trafficPic = trafficPic;
	}*/

	/**
	 * Gets the id card pic file.
	 * 
	 * @return the id card pic file
	 */
	@Transient
	public MultipartFile getIdCardPicFile() {
		return idCardPicFile;
	}

	/**
	 * Sets the id card pic file.
	 * 
	 * @param idCardPicFile
	 *            the new id card pic file
	 */
	public void setIdCardPicFile(MultipartFile idCardPicFile) {
		this.idCardPicFile = idCardPicFile;
	}

	/**
	 * Gets the traffic pic file.
	 * 
	 * @return the traffic pic file
	 */
	@Transient
	public MultipartFile getTrafficPicFile() {
		return trafficPicFile;
	}

	/**
	 * Sets the traffic pic file.
	 * 
	 * @param trafficPicFile
	 *            the new traffic pic file
	 */
	public void setTrafficPicFile(MultipartFile trafficPicFile) {
		this.trafficPicFile = trafficPicFile;
	}

	/**
	 * Gets the id card num.
	 * 
	 * @return the id card num
	 */
	@Column(name="id_card_num")
	public String getIdCardNum() {
		return idCardNum;
	}

	/**
	 * Sets the id card num.
	 * 
	 * @param idCardNum
	 *            the new id card num
	 */
	public void setIdCardNum(String idCardNum) {
		this.idCardNum = idCardNum;
	}

	/**
	 * Gets the real path.
	 * 
	 * @return the real path
	 */
	@Transient
	public String getRealPath() {
		return realPath;
	}

	/**
	 * Sets the real path.
	 * 
	 * @param realPath
	 *            the new real path
	 */
	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}

	/**
	 * Gets the creates the area code.
	 * 
	 * @return the creates the area code
	 */
	@Column(name="create_area_code")
	public String getCreateAreaCode() {
		return createAreaCode;
	}

	/**
	 * Sets the creates the area code.
	 * 
	 * @param createAreaCode
	 *            the new creates the area code
	 */
	public void setCreateAreaCode(String createAreaCode) {
		this.createAreaCode = createAreaCode;
	}

	/**
	 * Gets the creates the country code.
	 * 
	 * @return the creates the country code
	 */
	@Column(name="create_country_code")
	public String getCreateCountryCode() {
		return createCountryCode;
	}

	/**
	 * Sets the creates the country code.
	 * 
	 * @param createCountryCode
	 *            the new creates the country code
	 */
	public void setCreateCountryCode(String createCountryCode) {
		this.createCountryCode = createCountryCode;
	}

	/**
	 * Gets the ip.
	 * 
	 * @return the ip
	 */
	@Transient
	public String getIp() {
		return ip;
	}

	/**
	 * Sets the ip.
	 * 
	 * @param ip
	 *            the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the provinceid.
	 * 
	 * @return the provinceid
	 */
	@Column(name="provinceid")
	public Integer getProvinceid() {
		return provinceid;
	}

	/**
	 * Sets the provinceid.
	 * 
	 * @param provinceid
	 *            the new provinceid
	 */
	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	/**
	 * Gets the cityid.
	 * 
	 * @return the cityid
	 */
	@Column(name="cityid")
	public Integer getCityid() {
		return cityid;
	}

	/**
	 * Sets the cityid.
	 * 
	 * @param cityid
	 *            the new cityid
	 */
	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	/**
	 * Gets the areaid.
	 * 
	 * @return the areaid
	 */
	@Column(name="areaid")
	public Integer getAreaid() {
		return areaid;
	}

	/**
	 * Sets the areaid.
	 * 
	 * @param areaid
	 *            the new areaid
	 */
	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

	/**
	 * Gets the front type.
	 * 
	 * @return the front type
	 */
	@Column(name="fe_style")
	public String getFrontEndStyle() {
		return frontEndStyle;
	}


	/**
	 * Sets the front end style.
	 *
	 * @param frontEndStyle the new front end style
	 */
	public void setFrontEndStyle(String frontEndStyle) {
		this.frontEndStyle = frontEndStyle;
	}

	/**
	 * Gets the end type.
	 * 
	 * @return the end type
	 */
	@Column(name="be_style")
	public String getBackEndStyle() {
		return backEndStyle;
	}


	/**
	 * Sets the back end style.
	 *
	 * @param backEndStyle the new back end style
	 */
	public void setBackEndStyle(String backEndStyle) {
		this.backEndStyle = backEndStyle;
	}

	/**
	 * Gets the qq list.
	 * 
	 * @return the qq list
	 */
	@Transient
	public List<String> getQqList() {
		return qqList;
	}

	/**
	 * Sets the qq list.
	 * 
	 * @param qqList
	 *            the new qq list
	 */
	public void setQqList(List<String> qqList) {
		this.qqList = qqList;
	}
	

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return shopId;
	}
	
	public void setId(Long id) {
		this.shopId = id;
	}

	/**
	 * Gets the domain name.
	 *
	 * @return the domain name
	 */
	@Column(name="domain_name")
	public String getDomainName() {
		return domainName;
	}

	/**
	 * Sets the domain name.
	 *
	 * @param domainName the new domain name
	 */
	public void setDomainName(String domainName) {
		if(domainName != null){
			String domain = domainName.trim();
			if(domain.toLowerCase().startsWith("http://")){
				domain = domain.substring(7);
			}
			
			if(domain.startsWith("www.")){
				this.domainName = domain.substring(4).trim();
				return;
			}
			
			this.domainName = domain.trim();
		}
	
	}

	/**
	 * Gets the icp info.
	 *
	 * @return the icp info
	 */
	@Column(name="icp_info")
	public String getIcpInfo() {
		return icpInfo;
	}

	/**
	 * Sets the icp info.
	 *
	 * @param icpInfo the new icp info
	 */
	public void setIcpInfo(String icpInfo) {
		this.icpInfo = icpInfo;
	}

	/**
	 * Gets the logo pic.
	 *
	 * @return the logo pic
	 */
	@Column(name="logo_pic")
	public String getLogoPic() {
		return logoPic;
	}

	/**
	 * Sets the logo pic.
	 *
	 * @param logoPic the new logo pic
	 */
	public void setLogoPic(String logoPic) {
		this.logoPic = logoPic;
	}

	/**
	 * Gets the capital.
	 *
	 * @return the capital
	 */
	@Column(name="capital")
	public Double getCapital() {
		return capital;
	}

	/**
	 * Sets the capital.
	 *
	 * @param capital the capital to set
	 */
	public void setCapital(Double capital) {
		this.capital = capital;
	}

	/**
	 * Gets the credit.
	 *
	 * @return the credit
	 */
	@Column(name="credit")
	public Integer getCredit() {
		return credit;
	}

	/**
	 * Sets the credit.
	 *
	 * @param credit the credit to set
	 */
	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	/**
	 * Gets the front end templet.
	 *
	 * @return the frontEndTemplet
	 */
	@Column(name="fe_templet")
	public String getFrontEndTemplet() {
		return frontEndTemplet;
	}

	/**
	 * Sets the front end templet.
	 *
	 * @param frontEndTemplet the frontEndTemplet to set
	 */
	public void setFrontEndTemplet(String frontEndTemplet) {
		this.frontEndTemplet = frontEndTemplet;
	}

	/**
	 * Gets the back end templet.
	 *
	 * @return the backEndTemplet
	 */
	@Column(name="be_templet")
	public String getBackEndTemplet() {
		return backEndTemplet;
	}

	/**
	 * Sets the back end templet.
	 *
	 * @param backEndTemplet the backEndTemplet to set
	 */
	public void setBackEndTemplet(String backEndTemplet) {
		this.backEndTemplet = backEndTemplet;
	}

	/**
	 * Gets the front end language.
	 *
	 * @return the frontEndLanguage
	 */
	@Column(name="fe_lang")
	public String getFrontEndLanguage() {
		return frontEndLanguage;
	}

	/**
	 * Sets the front end language.
	 *
	 * @param frontEndLanguage the frontEndLanguage to set
	 */
	public void setFrontEndLanguage(String frontEndLanguage) {
		this.frontEndLanguage = frontEndLanguage;
	}

	/**
	 * Gets the back end language.
	 *
	 * @return the backEndLanguage
	 */
	@Column(name="be_lang")
	public String getBackEndLanguage() {
		return backEndLanguage;
	}

	/**
	 * Sets the back end language.
	 *
	 * @param backEndLanguage the backEndLanguage to set
	 */
	public void setBackEndLanguage(String backEndLanguage) {
		this.backEndLanguage = backEndLanguage;
	}

	@Column(name="theme")
	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	@Column(name="sec_domain_name")
	public String getSecDomainName() {
		return secDomainName;
	}

	public void setSecDomainName(String secDomainName) {
		this.secDomainName = secDomainName;
	}

	@Column(name="sec_domain_reg_date")
	public Date getSecDomainRegDate() {
		return secDomainRegDate;
	}

	public void setSecDomainRegDate(Date secDomainRegDate) {
		this.secDomainRegDate = secDomainRegDate;
	}

	@Column(name="contact_name")
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	@Column(name="contact_tel")
	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	@Column(name="contact_mobile")
	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}
	
	
	@Column(name="contact_QQ")
	public String getContactQQ() {
		return contactQQ;
	}

	public void setContactQQ(String contactQQ) {
		this.contactQQ = contactQQ;
	}

	@Column(name="contact_mail")
	public String getContactMail() {
		return contactMail;
	}

	public void setContactMail(String contactMail) {
		this.contactMail = contactMail;
	}

	@Column(name="audit_opinion")
	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	@Column(name="id_card_backpic")
	public String getIdCardBackPic() {
		return idCardBackPic;
	}

	public void setIdCardBackPic(String idCardBackPic) {
		this.idCardBackPic = idCardBackPic;
	}

	@Transient
	public MultipartFile getIdCardBackPicFile() {
		return idCardBackPicFile;
	}

	public void setIdCardBackPicFile(MultipartFile idCardBackPicFile) {
		this.idCardBackPicFile = idCardBackPicFile;
	}

	@Column(name="prod_require_audit")
	public Integer getProdRequireAudit() {
		return prodRequireAudit;
	}

	public void setProdRequireAudit(Integer prodRequireAudit) {
		this.prodRequireAudit = prodRequireAudit;
	}
	
	
	@Column(name="shop_type")
	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	@Transient
	public MultipartFile getBannerfile() {
		return bannerfile;
	}

	public void setBannerfile(MultipartFile bannerfile) {
		this.bannerfile = bannerfile;
	}

	@Column(name="banner_pic")
	public String getBannerPic() {
		return bannerPic;
	}

	public void setBannerPic(String bannerPic) {
		this.bannerPic = bannerPic;
	}

	@Column(name="shop_pic2")
	public String getShopPic2() {
		return shopPic2;
	}

	public void setShopPic2(String shopPic2) {
		this.shopPic2 = shopPic2;
	}

	@Column(name="mailfee_sts")
	public Integer getMailfeeSts() {
		return mailfeeSts;
	}

	public void setMailfeeSts(Integer mailfeeSts) {
		this.mailfeeSts = mailfeeSts;
	}

	@Column(name="mailfee_type")
	public Integer getMailfeeType() {
		return mailfeeType;
	}

	public void setMailfeeType(Integer mailfeeType) {
		this.mailfeeType = mailfeeType;
	}

	@Column(name="mailfee_con")
	public Double getMailfeeCon() {
		return mailfeeCon;
	}

	public void setMailfeeCon(Double mailfeeCon) {
		this.mailfeeCon = mailfeeCon;
	}
	
	@Column(name="switch_invoice")
	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}
	
	@Column(name="commission_rate")
	public Integer getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(Integer commissionRate) {
		this.commissionRate = commissionRate;
	}
	
	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	@Transient
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	@Column(name="return_shop_addr")
	public String getReturnShopAddr() {
		return returnShopAddr;
	}

	public void setReturnShopAddr(String returnShopAddr) {
		this.returnShopAddr = returnShopAddr;
	}

	@Column(name="return_province_id")
	public Integer getReturnProvinceId() {
		return returnProvinceId;
	}

	public void setReturnProvinceId(Integer returnProvinceId) {
		this.returnProvinceId = returnProvinceId;
	}

	@Column(name="return_city_id")
	public Integer getReturnCityId() {
		return returnCityId;
	}

	public void setReturnCityId(Integer returnCityId) {
		this.returnCityId = returnCityId;
	}

	@Column(name="return_area_id")
	public Integer getReturnAreaId() {
		return returnAreaId;
	}

	public void setReturnAreaId(Integer returnAreaId) {
		this.returnAreaId = returnAreaId;
	}


	@Column(name="business_hours")
	public String getBusinessHours() {
		return businessHours;
	}

	public void setBusinessHours(String businessHours) {
		this.businessHours = businessHours;
	}

	@Column(name="enterprise_name")
	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	@Column(name="register_number")
	public String getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	@Column(name="representative")
	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	@Column(name="business_address")
	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	@Column(name="register_amount")
	public String getRegisterAmount() {
		return registerAmount;
	}

	public void setRegisterAmount(String registerAmount) {
		this.registerAmount = registerAmount;
	}

	@Column(name="available_time")
	public String getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}

	@Column(name="business_range")
	public String getBusinessRange() {
		return businessRange;
	}

	public void setBusinessRange(String businessRange) {
		this.businessRange = businessRange;
	}

	@Column(name="business_image")
	public String getBusinessImage() {
		return businessImage;
	}

	public void setBusinessImage(String businessImage) {
		this.businessImage = businessImage;
	}

	@Transient
	public MultipartFile getBusinessImageFile() {
		return businessImageFile;
	}

	public void setBusinessImageFile(MultipartFile businessImageFile) {
		this.businessImageFile = businessImageFile;
	}


}
