package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *店铺评分统计表
 */
@Entity
@Table(name = "ls_shop_comm_stat")
public class ShopCommStat implements GenericEntity<Long> {

	private static final long serialVersionUID = 7049201498062472018L;

	/** 主键 */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 评分次数 */
	private Integer count; 
		
	/** 总分数 */
	private Integer score; 
		
	
	public ShopCommStat() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_COMM_STAT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "count")
	public Integer  getCount(){
		return count;
	} 
		
	public void setCount(Integer count){
			this.count = count;
		}
		
    @Column(name = "score")
	public Integer  getScore(){
		return score;
	} 
		
	public void setScore(Integer score){
			this.score = score;
		}
	


} 
