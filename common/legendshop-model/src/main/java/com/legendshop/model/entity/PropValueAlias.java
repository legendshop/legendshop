package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商品属性值别名表
 */
@Entity
@Table(name = "ls_prop_value_alias")
public class PropValueAlias implements GenericEntity<Long> {

	private static final long serialVersionUID = -6298462970541785068L;

	/** ID */
	private Long id; 
		
	/** 属性值别名 */
	private String alias; 
		
	/** 属性值ID */
	private Long valueId; 
		
	/** 用户ID */
	private String userId; 
	
	/** 商品ID **/
	private Long prodId;
		
	
	public PropValueAlias() {
    }
		
	@Id
    @Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROP_VALUE_ALIAS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "alias")
	public String  getAlias(){
		return alias;
	} 
		
	public void setAlias(String alias){
			this.alias = alias;
		}
		
    @Column(name = "value_id")
	public Long  getValueId(){
		return valueId;
	} 
		
	public void setValueId(Long valueId){
			this.valueId = valueId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
	}

	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
} 
