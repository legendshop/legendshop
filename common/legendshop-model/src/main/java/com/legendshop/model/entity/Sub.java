/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.model.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.buy.ShopCartItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

/**
 * 订单对象.
 */
@Entity
@Table(name = "ls_sub")
@ApiModel(value = "Sub订单对象")
public class Sub extends AbstractEntity implements GenericEntity<Long> {

	/**
	 * The Constant serialVersionUID.
	 */
	@ExcelIgnore
	private static final long serialVersionUID = -4030702631697447802L;

	/**
	 * The sub id.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订购ID")
	private Long subId;

	/**
	 * The prod name.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "产品名称,多个产品将会以逗号隔开")
	private String prodName;

	@ExcelIgnore
	@ApiModelProperty(value = "订购用户ID")
	private String userId;

	/**
	 * The user name.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订购用户名称")
	private String userName;

	/**
	 * The sub date.
	 */
	@ExcelProperty(value = "下单时间",index = 1)
	@ColumnWidth(25)
	@ApiModelProperty(value = "订购时间")
	private Date subDate; // 下订单的时间

	/**
	 * The pay date.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "购买时间")
	private Date payDate; // 支付的时间

	/**
	 * The update date.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单更新时间")
	private Date updateDate; // 更新时间

	/**
	 * The sub number.
	 */
	@ExcelProperty(value = "订单编号",index = 0)
	@ColumnWidth(30)
	@ApiModelProperty(value = "订购流水号")
	private String subNumber;


	/**
	 * 订单类型[预售商品订单].
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单类型[预售商品订单]")
	private String subType;

	/**
	 * 订单商品原价.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "总值")
	private Double total;

	/**
	 * 订单商品实际价格(运费 折扣 促销).
	 */
	@ExcelProperty(value = "订单金额",index = 3)
	@ColumnWidth(15)
	@ApiModelProperty(value = "实际总值")
	private Double actualTotal;

	/**
	 * 支付ID.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "支付ID")
	private Long payId;

	/**
	 * 付款类型ID.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "付款类型ID")
	private String payTypeId;

	/**
	 * 订单状态,参见OrderStatusEnum.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单状态,参见OrderStatusEnum")
	private Integer status;

	/**
	 * 支付方式名称.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "支付类型名称")
	private String payTypeName;

	/**
	 * 支付流水号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "支付流水号")
	private String flowTradeNo;

	/**
	 * ls_sub_settlement 清算单据号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "清算单据号")
	private String subSettlementSn;

	/**
	 * The other.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "其他备注")
	private String other;

	@ExcelIgnore
	@ApiModelProperty(value = "所属店铺")
	private Long shopId;

	/**
	 * The shop name.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "商城名称")
	private String shopName;

	//购物车，每个basket代表一个商品
	@ExcelIgnore
	@ApiModelProperty(value = "购物车，每个basket代表一个商品")
	private List<Basket> basket;

	@ExcelIgnore
	@ApiModelProperty(value = "购物车中的商品")
	private List<ShopCartItem> cartItems;

	/**
	 * The pay type.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "支付类型")
	private List<PayType> payType;

	/**
	 * The score id.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "用户积分ID")
	private Long scoreId;

	/**
	 * 使用积分支付，暂时没用.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "使用积分支付，暂时没用")
	private Integer score;

	@ExcelIgnore
	@ApiModelProperty(value = "")
	private String dvyType;

	/**
	 * The dvy type id.物流公司ID
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "配送类型")
	private Long dvyTypeId;

	/**
	 * The delivery flow id. 物流单号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "物流单号")
	private String dvyFlowId;

	/**
	 * The delivery invoice id. 发票单号
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "发票单号")
	private Long invoiceSubId;

	/**
	 * 物流费用
	 */
	@ExcelProperty(value = "运费",index = 4)
	@ColumnWidth(15)
	@ApiModelProperty(value = "物流费用")
	private Double freightAmount;


	/**
	 * The delivery order_remark. 给买家留言
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "给买家留言")
	private String orderRemark;


	/**
	 * The delivery addr_order_id. 用户订单地址Id
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "用户订单地址Id")
	private Long addrOrderId;

	/**
	 * 是否需要发票(0:不需要;1:需要)
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否需要发票(0:不需要;1:需要)")
	private boolean needInvoice;

	@ExcelIgnore
	@ApiModelProperty(value = "支付方式(1:货到付款;2:在线支付)")
	private Integer payManner; //支付方式(1:货到付款;2:在线支付)

	@ExcelIgnore
	@ApiModelProperty(value = "物流重量(千克)")
	private Double weight; //物流重量(千克)

	@ExcelIgnore
	@ApiModelProperty(value = "物流体积(立方米)")
	private Double volume;//物流体积(立方米)

	@ExcelIgnore
	@ApiModelProperty(value = "优惠总金额")
	private Double discountPrice; //优惠总金额

	@ExcelIgnore
	@ApiModelProperty(value = "分销的佣金总额")
	private Double distCommisAmount;//分销的佣金总额

	@ExcelIgnore
	@ApiModelProperty(value = "使用优惠券的金额")
	private Double couponOffPrice;//使用优惠券的金额

	@ExcelProperty(value = "红包金额",index = 5)
	@ColumnWidth(15)
	@ApiModelProperty(value = "使用红包的金额")
	private Double redpackOffPrice;//使用红包的金额

	@ExcelIgnore
	@ApiModelProperty(value = "订单商品总数")
	private Integer productNums;//订单商品总数

	/**
	 * The pay date.
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "发货时间")
	private Date dvyDate; // 发货时间

	@ExcelProperty(value = "成交时间",index = 2)
	@ColumnWidth(25)
	@ApiModelProperty(value = "完成时间/确认收货时间")
	private Date finallyDate; // 完成时间/确认收货时间

	@ExcelIgnore
	@ApiModelProperty(value = "是否货到付款")
	private boolean isCod; //是否货到付款

	@ExcelIgnore
	@ApiModelProperty(value = "是否已经支付")
	private boolean isPayed; //是否已经支付

	@ExcelIgnore
	@ApiModelProperty(value = "用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除")
	private Integer deleteStatus; //用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除

	@ExcelIgnore
	@ApiModelProperty(value = "订单项列表")
	private List<SubItem> subItems;//订单项列表

	@ExcelIgnore
	@ApiModelProperty(value = "收货人城市ID")
	private Integer cityId;

	@ExcelIgnore
	@ApiModelProperty(value = "收货人省份ID")
	private Integer provinceId;

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	@ExcelIgnore
	@ApiModelProperty(value = "收货人区域ID")
	private Integer areaId;

	@ExcelIgnore
	@ApiModelProperty(value = "preDeliveryTime")
	private String preDeliveryTime;

	/**
	 * 退换货状态  0:默认,1:在处理,2:处理完成
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "退换货状态  0:默认,1:在处理,2:处理完成")
	private long refundState;

	/**
	 * 是否结算
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否结算")
	private boolean isBill;

	/**
	 * 订单结算编号[用于结算档期统计]
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "订单结算编号[用于结算档期统计]")
	private String billSn;

	/**
	 * 是否提醒过发货
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "是否提醒过发货")
	private Integer remindDelivery;

	/**
	 * 提醒发货时间
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "提醒发货时间")
	private Date remindDeliveryTime;

	/*活动Id [秒杀活动ID/团购活动id]*/
	@ExcelIgnore
	@ApiModelProperty(value = "活动Id [秒杀活动ID/团购活动id]")
	private Long activeId;

	@ExcelIgnore
	@ApiModelProperty(value = "是否已开发票")
	private Integer hasInvoice;

	//取消订单的原因
	@ExcelIgnore
	@ApiModelProperty(value = "取消订单的原因")
	private String cancelReason;

	@ExcelIgnore
	@ApiModelProperty(value = "商家备注")
	private String shopRemark;

	@ExcelIgnore
	@ApiModelProperty(value = "是否已经备注")
	private Boolean isShopRemarked = false;

	@ExcelIgnore
	@ApiModelProperty(value = "备注时间")
	private Date shopRemarkDate;

	@ExcelIgnore
	@ApiModelProperty(value = "团购状态")
	private Integer groupStatus;

	@ExcelIgnore
	@ApiModelProperty(value = "拼团商品状态  [0:拼团中 -1:拼团失败 2：拼团成功]")
	private Integer mergeGroupStatus;

	@ExcelIgnore
	@ApiModelProperty(value = "拼团活动编号,ls_merge_group_add活动的编号")
	private String addNumber;

	/**
	 * 当前商品所属商家是否开启发票 1开启 0关闭
	 */
	@ExcelIgnore
	@ApiModelProperty(value = "当前商品所属商家是否开启发票 1开启 0关闭")
	private Integer switchInvoice;

	@ExcelIgnore
	@ApiModelProperty(value = "订单已调整的金额")
	private Double changedPrice;

	@ExcelIgnore
	@ApiModelProperty(value = "预售订单尾款是否支付")
	private Integer isPayFinal;

	@ExcelIgnore
	@ApiModelProperty(value = "预售订单定金是否支付")
	private Integer isPayDeposit;

	@ExcelIgnore
	@ApiModelProperty(value = "预售订单支付类型")
	private Integer payPctType;

	/**
	 * Instantiates a new sub.
	 */
	public Sub() {
	}

	/**
	 * Gets the sub id.
	 *
	 * @return the sub id
	 */
	@Id
	@Column(name = "sub_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_SEQ")
	public Long getSubId() {
		return this.subId;
	}

	/**
	 * Sets the sub id.
	 *
	 * @param subId the new sub id
	 */
	public void setSubId(Long subId) {
		this.subId = subId;
	}


	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * Gets the sub date.
	 *
	 * @return the sub date
	 */
	@Column(name = "sub_date")
	public Date getSubDate() {
		return this.subDate;
	}

	/**
	 * Sets the sub date.
	 *
	 * @param subDate the new sub date
	 */
	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	/**
	 * Gets the sub number.
	 *
	 * @return the sub number
	 */
	@Column(name = "sub_number")
	public String getSubNumber() {
		return this.subNumber;
	}

	/**
	 * Sets the sub number.
	 *
	 * @param subNumber the new sub number
	 */
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	@Column(name = "total")
	public Double getTotal() {
		return this.total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the new total
	 */
	public void setTotal(Double total) {
		this.total = total;
	}


	/**
	 * Gets the pay id.
	 *
	 * @return the pay id
	 */
	@Column(name = "pay_id")
	public Long getPayId() {
		return this.payId;
	}

	/**
	 * Sets the pay id.
	 *
	 * @param payId the new pay id
	 */
	public void setPayId(Long payId) {
		this.payId = payId;
	}

	/**
	 * Gets the other.
	 *
	 * @return the other
	 */
	@Column(name = "other")
	public String getOther() {
		return this.other;
	}

	/**
	 * Sets the other.
	 *
	 * @param other the new other
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	@Column(name = "shop_name")
	public String getShopName() {
		return shopName;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName the new shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the basket.
	 *
	 * @return the basket
	 */
	@Transient
	public List<Basket> getBasket() {
		return basket;
	}

	/**
	 * Sets the basket.
	 *
	 * @param basket the new basket
	 */
	public void setBasket(List<Basket> basket) {
		this.basket = basket;
	}

	/**
	 * Gets the pay type name.
	 *
	 * @return the pay type name
	 */
	@Column(name = "pay_type_name")
	public String getPayTypeName() {
		return payTypeName;
	}

	/**
	 * Sets the pay type name.
	 *
	 * @param payTypeName the new pay type name
	 */
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the pay type.
	 *
	 * @return the pay type
	 */
	@Transient
	public List<PayType> getPayType() {
		return payType;
	}

	/**
	 * Sets the pay type.
	 *
	 * @param payType the new pay type
	 */
	public void setPayType(List<PayType> payType) {
		this.payType = payType;
	}

	/**
	 * Gets the pay date.
	 *
	 * @return the pay date
	 */
	@Column(name = "pay_date")
	public Date getPayDate() {
		return payDate;
	}

	/**
	 * Sets the pay date.
	 *
	 * @param payDate the new pay date
	 */
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	@Column(name = "prod_name")
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the update date.
	 *
	 * @return the update date
	 */
	@Column(name = "update_date")
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the update date.
	 *
	 * @param updateDate the new update date
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * Gets the pay type id.
	 *
	 * @return the pay type id
	 */
	@Column(name = "pay_type_id")
	public String getPayTypeId() {
		return payTypeId;
	}

	/**
	 * Sets the pay type id.
	 *
	 * @param payTypeId the new pay type id
	 */
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	/**
	 * Gets the score id.
	 *
	 * @return the score id
	 */
	@Column(name = "score_id")
	public Long getScoreId() {
		return scoreId;
	}

	/**
	 * Sets the score id.
	 *
	 * @param scoreId the new score id
	 */
	public void setScoreId(Long scoreId) {
		this.scoreId = scoreId;
	}

	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 *
	 * @param score the new score
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * Gets the actual total.
	 *
	 * @return the actual total
	 */
	@Column(name = "actual_total")
	public Double getActualTotal() {
		return actualTotal;
	}

	/**
	 * Sets the actual total.
	 *
	 * @param actualTotal the new actual total
	 */
	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}


	/**
	 * Gets the sub type.
	 *
	 * @return the sub type
	 */
	@Column(name = "sub_type")
	public String getSubType() {
		return subType;
	}

	/**
	 * Sets the sub type.
	 *
	 * @param subType the new sub type
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}


	@Column(name = "dvy_type")
	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	@Column(name = "dvy_type_id")
	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}


	/**
	 * Gets the delivery flow id.
	 *
	 * @return the delivery flow id
	 */
	@Column(name = "dvy_flow_id")
	public String getDvyFlowId() {
		return dvyFlowId;
	}

	/**
	 * Sets the delivery flow id.
	 *
	 * @param dvyFlowId the new delivery flow id
	 */
	public void setDvyFlowId(String deliveryFlowId) {
		this.dvyFlowId = deliveryFlowId;
	}

	@Transient
	public Long getId() {
		return subId;
	}

	public void setId(Long id) {
		this.subId = id;
	}


	@Column(name = "invoice_sub_id")
	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	@Column(name = "order_remark")
	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	@Column(name = "addr_order_id")
	public Long getAddrOrderId() {
		return addrOrderId;
	}

	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "pay_manner")
	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	@Column(name = "freight_amount")
	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	@Column(name = "weight")
	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	@Column(name = "volume")
	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	@Column(name = "product_nums")
	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	@Column(name = "dvy_date")
	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	@Column(name = "finally_date")
	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}


	@Transient
	public List<ShopCartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<ShopCartItem> cartItems) {
		this.cartItems = cartItems;
	}


	/**
	 * @return the flowTradeNo
	 */
	@Column(name = "flow_trade_no")
	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	/**
	 * @param flowTradeNo the flowTradeNo to set
	 */
	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	@Transient
	public List<SubItem> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<SubItem> subItems) {
		this.subItems = subItems;
	}

	@Column(name = "delete_status")
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	@Column(name = "dist_commis_amount")
	public Double getDistCommisAmount() {
		return distCommisAmount;
	}

	public void setDistCommisAmount(Double distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}

	@Column(name = "discount_price")
	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	@Column(name = "coupon_off_price")
	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	@Column(name = "city_id")
	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	@Column(name = "province_id")
	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * 退换货状态
	 *
	 * @return the refundState
	 */
	@Column(name = "refund_state")
	public long getRefundState() {
		return refundState;
	}


	/**
	 * 退换货状态
	 *
	 * @param refundState the refundState to set
	 */
	public void setRefundState(long refundState) {
		this.refundState = refundState;
	}

	@Column(name = "sub_settlement_sn")
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	@Column(name = "is_cod")
	public boolean isCod() {
		return isCod;
	}

	public void setCod(boolean isCod) {
		this.isCod = isCod;
	}

	@Column(name = "is_payed")
	public boolean isPayed() {
		return isPayed;
	}

	public void setPayed(boolean isPayed) {
		this.isPayed = isPayed;
	}


	@Column(name = "is_bill")
	public boolean isBill() {
		return isBill;
	}

	public void setBill(boolean isBill) {
		this.isBill = isBill;
	}

	@Column(name = "bill_sn")
	public String getBillSn() {
		return billSn;
	}

	public void setBillSn(String billSn) {
		this.billSn = billSn;
	}

	@Column(name = "redpack_off_price")
	public Double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(Double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	@Column(name = "remind_delivery")
	public Integer getRemindDelivery() {
		return remindDelivery;
	}

	public void setRemindDelivery(Integer remindDelivery) {
		this.remindDelivery = remindDelivery;
	}

	@Column(name = "remind_delivery_time")
	public Date getRemindDeliveryTime() {
		return remindDeliveryTime;
	}

	public void setRemindDeliveryTime(Date remindDeliveryTime) {
		this.remindDeliveryTime = remindDeliveryTime;
	}

	@Column(name = "active_id")
	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	@Column(name = "is_need_invoice")
	public boolean isNeedInvoice() {
		return needInvoice;
	}

	public void setNeedInvoice(boolean needInvoice) {
		this.needInvoice = needInvoice;
	}

	@Column(name = "has_invoice")
	public Integer getHasInvoice() {
		return hasInvoice;
	}

	public void setHasInvoice(Integer hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	@Column(name = "cancel_reason")
	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	@Column(name = "shop_remark")
	public String getShopRemark() {
		return shopRemark;
	}

	public void setShopRemark(String shopRemark) {
		this.shopRemark = shopRemark;
	}

	@Column(name = "is_shop_remarked")
	public Boolean getIsShopRemarked() {
		return isShopRemarked;
	}

	public void setIsShopRemarked(Boolean isShopRemarked) {
		this.isShopRemarked = isShopRemarked;
	}

	@Column(name = "shop_remark_date")
	public Date getShopRemarkDate() {
		return shopRemarkDate;
	}

	public void setShopRemarkDate(Date shopRemarkDate) {
		this.shopRemarkDate = shopRemarkDate;
	}

	@Column(name = "group_status")
	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	@Column(name = "merge_group_status")
	public Integer getMergeGroupStatus() {
		return mergeGroupStatus;
	}

	public void setMergeGroupStatus(Integer mergeGroupStatus) {
		this.mergeGroupStatus = mergeGroupStatus;
	}

	@Column(name = "add_number")
	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	@Column(name = "switch_invoice")
	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}

	@Column(name = "changed_price")
	public Double getChangedPrice() {
		return changedPrice;
	}

	public void setChangedPrice(Double changedPrice) {
		this.changedPrice = changedPrice;
	}

	@Transient
	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	@Transient
	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	@Transient
	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}
}