package com.legendshop.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.Transient;

@Entity
public class Region implements Serializable{

	private static final long serialVersionUID = -8372288245526759650L;

	private String groupby;
	
	private List<Province> provinces= new ArrayList<Province>();

	@Transient
	public String getGroupby() {
		return groupby;
	}

	public void setGroupby(String groupby) {
		this.groupby = groupby;
	}

	@Transient
	public List<Province> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<Province> provinces) {
		this.provinces = provinces;
	}
	
	/**
	 * 往区域中增加省份元素
	 * @param item
	 */
	public void addProvinces(Province province) {
		if(this.getGroupby().equals(province.getGroupby())){
			provinces.add(province);
		}
		
	}
}
