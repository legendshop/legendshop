package com.legendshop.model.prod;

import java.io.Serializable;


/**
 * 包邮Dto
 */
public class EditActiveNameDto implements Serializable{

	private static final long serialVersionUID = 8603339400272532192L;
	
	private String activeName;
	
	private String [] prodIds;//活动商品Id

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}
	public String[] getProdIds() {
		return prodIds;
	}

	public void setProdIds(String[] prodIds) {
		this.prodIds = prodIds;
	}
	
}






