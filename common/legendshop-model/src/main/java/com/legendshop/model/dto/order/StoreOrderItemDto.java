package com.legendshop.model.dto.order;

import java.io.Serializable;



public class StoreOrderItemDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 629141927476156824L;

	private Long prodId;
	
	private Integer basketCount; 
	
	/** 产品名称 */
	private String prodName; 
	
	/** 产品动态属性 */
	private String attribute; 
	
	/** 产品主图片路径 */
	private String pic; 
	
	/** 产品现价 */
	private Double cash;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	} 
	
}
