/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class LoginHistorySum.
 */
public class LoginHistorySum implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1913043732868691515L;

	/** The user name. */
	private String userName;
	
	/** The login count. */
	private long loginCount;
	
	private Date loginTime;

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the login count.
	 *
	 * @return the login count
	 */
	public long getLoginCount() {
		return loginCount;
	}

	/**
	 * Sets the login count.
	 *
	 * @param loginCount the new login count
	 */
	public void setLoginCount(long loginCount) {
		this.loginCount = loginCount;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
}
