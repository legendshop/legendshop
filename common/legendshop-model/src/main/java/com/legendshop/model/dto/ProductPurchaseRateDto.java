/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品购买率展示DTO
 *
 */
public class ProductPurchaseRateDto implements Serializable {

	private static final long serialVersionUID = 7107968212491161984L;

	/** 商品id  */
	private Long prodId;

	/**  商品价格 */
	private Double cash;
	
	/**  商品名称 */
	private String name;
	
	/**  商品查看数 */
	private long views;
	
	/**  商品购买数 */
	private long buys;
	
	/**  商城的名字 */
	private String siteName;
	
	/**  商城Id */
	private Long shopId;
	
	/**  已经支付的购买数 */
	private Long payedBuys;
	
	/**  已经支付的退款的购买数 */
	private Long refundBuys;
	
	private Date startDate;
	
	private Date endDate;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getViews() {
		return views;
	}

	public void setViews(long views) {
		this.views = views;
	}

	public long getBuys() {
		return buys;
	}

	public void setBuys(long buys) {
		this.buys = buys;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Long getPayedBuys() {
		return payedBuys;
	}

	public void setPayedBuys(Long payedBuys) {
		this.payedBuys = payedBuys;
	}

	public Long getRefundBuys() {
		return refundBuys;
	}

	public void setRefundBuys(Long refundBuys) {
		this.refundBuys = refundBuys;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
}
