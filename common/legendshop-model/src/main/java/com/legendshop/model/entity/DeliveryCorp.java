/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 物流公司
 */
@Entity
@Table(name = "ls_delivery")
@ApiModel(value="DeliveryCorp") 
public class DeliveryCorp implements GenericEntity<Long> {

	private static final long serialVersionUID = -589313267697921480L;

	/** 物流公司Id */
	@ApiModelProperty(value="物流公司Id") 
	private Long dvyId;
	
	/** 用户Id. */
	@ApiModelProperty(value="用户Id") 
	private String userId;
	
	/** 用户名称. */
	@ApiModelProperty(value="用户名称") 
	private String userName;
	
	/** 物流公司名称. */
	@ApiModelProperty(value="物流公司名称") 
	private String name;
	
	/** The url. */
	@ApiModelProperty(value="物流公司官网URL") 
	private String companyHomeUrl;
	
	/** 建立时间. */
	@ApiModelProperty(value="建立时间") 
	private Date createTime;
	
	/** 修改时间. */
	@ApiModelProperty(value="建立时间") 
	private Date modifyTime;
	
	/** 商家Id. */
	@ApiModelProperty(value="商家Id") 
	private Long shopId;
	
	/** 物流查询接口地址. */
	@ApiModelProperty(value="物流查询接口地址") 
	private String queryUrl;
	
	/** 物流查询编号 快递100提供. */
	@ApiModelProperty(value="物流查询接口地址") 
	private String companyCode;
	
	/**
	 * 获取物流公司Id
	 * 
	 * @return the 物流公司Id
	 */
	@Id
	@Column(name = "dvy_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DELIVERY_SEQ")
	public Long getDvyId() {
		return dvyId;
	}
	
	/**
	 * Sets the dvy id.
	 * 
	 * @param dvyId
	 *            the new dvy id
	 */
	public void setDvyId(Long dvyId) {
		this.dvyId = dvyId;
	}
	
	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	@Column(name = "company_home_url")
	public String getCompanyHomeUrl() {
		return companyHomeUrl;
	}
	
	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setCompanyHomeUrl(String companyHomeUrl) {
		this.companyHomeUrl = companyHomeUrl;
	}
	
	/**
	 * Gets the creates the time.
	 * 
	 * @return the creates the time
	 */
	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}
	
	/**
	 * Sets the creates the time.
	 * 
	 * @param createTime
	 *            the new creates the time
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	/**
	 * Gets the modify time.
	 * 
	 * @return the modify time
	 */
	@Column(name = "modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}
	
	/**
	 * Sets the modify time.
	 * 
	 * @param modifyDate
	 *            the new modify time
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Transient
	public Long getId() {
		return dvyId;
	}
	
	public void setId(Long id) {
		this.dvyId = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "query_url")
	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	
	@Column(name = "company_code")
	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	

	

}