/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 快递100日志
 *
 */
public class KuaiDiLogs {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(KuaiDiLogs.class);

	private List<Status> entries = new ArrayList<Status>();
	
	private int code;
	

	public List<Status> getEntries() {
		return entries;
	}

	public void setEntries(List<Status> entries) {
		this.entries = entries;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}


	public void addEntry(String timeString, String content) {
		Status entry = new Status();
		entry.setContext(content);
		entry.setFtime(timeString);
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date time = dateFormat.parse(timeString);
			entry.setDate(time);
		} catch (ParseException e) {
			logger.error("日期转换出错", e);
		}
		entries.add(entry);
	}
    
	public static class Status {
		private String ftime;
		private Date date;
		private String context;

		

		public String getFtime() {
			return ftime;
		}

		public void setFtime(String ftime) {
			this.ftime = ftime;
		}

		public String getContext() {
			return context;
		}

		public void setContext(String context) {
			this.context = context;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

	}

}
