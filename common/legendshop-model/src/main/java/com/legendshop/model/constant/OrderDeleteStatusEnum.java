/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 订单删除状态
 */
public enum OrderDeleteStatusEnum implements IntegerEnum {
	
	/** 正常状态. */
	NORMAL(0),

	/** 删除到回收站. */
	DELETED(1),

	/** 永久删除. */
	PERMANENT_DELETED(2);
	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	OrderDeleteStatusEnum(Integer num) {
		this.num = num;
	}

}
