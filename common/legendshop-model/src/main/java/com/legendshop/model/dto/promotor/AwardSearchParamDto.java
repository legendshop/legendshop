/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import java.util.Date;

/**
 * 奖金参数
 */
public class AwardSearchParamDto {
	
	/** 奖励类型,佣金类型. */
	private String commisType;
	
	/** 起始时间. */
	private Date startTime;
	
	/** 结束时间. */
	private Date endTime;

	public String getCommisType() {
		return commisType;
	}

	public void setCommisType(String commisType) {
		this.commisType = commisType;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
