package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.model.entity.Category;
import com.legendshop.util.AppUtils;

/**
 * 商品分类树结构.
 *
 * @author tony
 */
public class TreeNode implements Serializable,Comparable<TreeNode> {
	
	private final static Logger LOGGER=LoggerFactory.getLogger(TreeNode.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The parent id. */
	private long parentId; 
	
	/** The self id. */
	private long selfId;
	
	/** The node name. */
	protected String nodeName;
	
	protected String pic;
	
	/** The obj. */
	protected Category obj;
	
	/** The seq. */
	private Integer seq;
	
	private int level;
	
	/** The parent node. */
	protected TreeNode parentNode;
	
	/** The child list. */
	protected CopyOnWriteArrayList<TreeNode> childList;

	/**
	 * Instantiates a new tree node.
	 */
	public TreeNode() {
		initChildList();
	}

	/**
	 * Instantiates a new tree node.
	 *
	 * @param parentNode the parent node
	 */
	public TreeNode(TreeNode parentNode) {
		this.getParentNode();
		initChildList();
	}

	/**
	 * Checks if is leaf.
	 *
	 * @return true, if is leaf
	 */
	public boolean isLeaf() {
		if (childList == null) {
			return true;
		} else {
			if (childList.isEmpty()) {
				return true;
			} else {
				return false;
			}
		}
	}

	/* 插入一个child节点到当前节点中 */
	/**
	 * Adds the child node.
	 *
	 * @param treeNode the tree node
	 */
	public void addChildNode(TreeNode treeNode) {
		initChildList();
		childList.add(treeNode);
		Collections.sort(childList);  
	}

	/**
	 * Inits the child list.
	 */
	public void initChildList() {
		if (childList == null)
			childList = new CopyOnWriteArrayList<TreeNode>();
	}

	/**
	 * Checks if is valid tree.
	 *
	 * @return true, if is valid tree
	 */
	public boolean isValidTree() {
		return true;
	}

	/* 返回当前节点的父辈节点集合 */
	/**
	 * Gets the elders.
	 *
	 * @return the elders
	 */
	public List<TreeNode> getElders() {
		List<TreeNode> elderList = new ArrayList<TreeNode>();
		TreeNode parentNode = this.getParentNode();
		if (parentNode == null) {
			return elderList;
		} else if(parentNode.getParentId()==-1) {
			return elderList;
		}else{
			elderList.add(parentNode);
			elderList.addAll(parentNode.getElders());
			return elderList;
		}
	}

	/* 返回当前节点的晚辈集合 */
	/**
	 * Gets the juniors.
	 *
	 * @return the juniors
	 */
	public List<TreeNode> getJuniors() {
		List<TreeNode> juniorList = new ArrayList<TreeNode>();
		List<TreeNode> childList = this.getChildList();
		if (childList == null) {
			return juniorList;
		} else {
			int childNumber = childList.size();
			for (int i = 0; i < childNumber; i++) {
				TreeNode junior = childList.get(i);
				juniorList.add(junior);
				juniorList.addAll(junior.getJuniors());
			}
			return juniorList;
		}
	}
	/* 返回当前节点的同级的集合  不包含自己的 */
	public List<TreeNode> getCompatriots(){
		List<TreeNode> compatriotList = new ArrayList<TreeNode>();
		TreeNode parentNode = this.getParentNode();
		if (parentNode == null) {
			return compatriotList;
			
		}if(this.getParentId()==-1) { //说明是根节点
			List<TreeNode> orginList = this.getChildList();	
			for (TreeNode treeNode : orginList) {
				if(!treeNode.equals(this)){
					compatriotList.add(treeNode);
				}
			}
		}else{
			List<TreeNode> orginList = parentNode.getChildList();
			for (TreeNode treeNode : orginList) {
				if(!treeNode.equals(this)){
					compatriotList.add(treeNode);
				}
			}
		}
		return compatriotList;
	}
	
	
	

	/* 返回当前节点的孩子集合 */
	/**
	 * Gets the child list.
	 *
	 * @return the child list
	 */
	public List<TreeNode> getChildList() {
		if(childList != null && childList.size() > 0){
			Collections.sort(childList, new TreeNodeComparator());
		}
		return childList;
	}

	/* 删除节点和它下面的晚辈 */
	/**
	 * Delete node.
	 */
	public void deleteNode() {
		TreeNode parentNode = this.getParentNode();
		long id = this.getSelfId();

		if (parentNode != null) {
			parentNode.deleteChildNode(id);
		}
	}

	/* 删除当前节点的某个子节点 */
	/**
	 * Delete child node.
	 *
	 * @param childId the child id
	 */
	public void deleteChildNode(long childId) {
		List<TreeNode> childList = this.getChildList();
		Iterator<TreeNode> iterator=childList.iterator();
		while(iterator.hasNext()){
			TreeNode child=iterator.next();
			if (child.getSelfId() == childId) {
				try {
					iterator.remove();
					LOGGER.info(" delete Memory Category name={} , categoryId={} ",child.getNodeName(),child.getSelfId());
				} catch (Exception e) {
					LOGGER.warn(" delete Memory Category name={} , categoryId={} Failed",child.getNodeName(),child.getSelfId());
				}
				return;
			}
		}
	
	
	}

	/* 动态的插入一个新的节点到当前树中 */
	/**
	 * Insert junior node.
	 *
	 * @param treeNode the tree node
	 * @return true, if successful
	 */
	public boolean insertJuniorNode(TreeNode treeNode) {
		long juniorParentId = treeNode.getParentId();
		if(juniorParentId==0){
			addChildNode(treeNode);
			return true;
		}
		else if(this.selfId==juniorParentId ){
			addChildNode(treeNode);
			return true;
		}
		else if (this.parentId == juniorParentId) {
			List<TreeNode> list=this.getParentNode().getChildList();
			if(AppUtils.isBlank(obj)){
				list = new ArrayList<TreeNode>();
				list.add(treeNode);
			}else{
				list.add(treeNode);
			}
			return true;
		} else {
			List<TreeNode> childList = this.getChildList();
			if(AppUtils.isBlank(childList)){
				return false;
			}
			int childNumber = childList.size();
			boolean insertFlag;

			for (int i = 0; i < childNumber; i++) {
				TreeNode childNode = childList.get(i);
				insertFlag = childNode.insertJuniorNode(treeNode);
				if (insertFlag == true){
					LOGGER.info(" add Memory Category name={} , categoryId={} ",treeNode.getNodeName(),treeNode.getSelfId());
					return true;
				}
					
			}
			return false;
		}
	}

	/* 找到一颗树中某个节点 */
	/**
	 * Find tree node by id.
	 *
	 * @param id the id
	 * @return the tree node
	 */
	public TreeNode findTreeNodeById(long id) {
		if (this.selfId == id)
			return this;
		if (childList == null || childList.size() == 0) {
			return null;
		} else {
			int childNumber = childList.size();
			for (int i = 0; i < childNumber; i++) {
				TreeNode child = childList.get(i);
				TreeNode resultNode = child.findTreeNodeById(id);
				if (resultNode != null) {
					return resultNode;
				}
			}
			return null;
		}
	}

	/* 遍历一棵树，层次遍历 */
	/**
	 * Traverse.
	 */
	public void traverse() {
		if (selfId < 0)
			return;
		print(this.selfId);
		if (childList == null || childList.isEmpty())
			return;
		int childNumber = childList.size();
		for (int i = 0; i < childNumber; i++) {
			TreeNode child = childList.get(i);
			child.traverse();
		}
	}

	/**
	 * Prints the.
	 *
	 * @param content the content
	 */
	public void print(String content) {
		System.out.println(content);
	}

	/**
	 * Prints the.
	 *
	 * @param content the content
	 */
	public void print(long content) {
		System.out.println(String.valueOf(content));
	}

	/**
	 * Sets the child list.
	 *
	 * @param childList the new child list
	 */
	public void setChildList(CopyOnWriteArrayList<TreeNode> childList) {
	    this.childList = childList;
	}

	/**
	 * Gets the parent id.
	 *
	 * @return the parent id
	 */
	public long getParentId() {
		return parentId;
	}

	/**
	 * Sets the parent id.
	 *
	 * @param parentId the new parent id
	 */
	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	/**
	 * Gets the self id.
	 *
	 * @return the self id
	 */
	public long getSelfId() {
		return selfId;
	}

	/**
	 * Sets the self id.
	 *
	 * @param selfId the new self id
	 */
	public void setSelfId(long selfId) {
		this.selfId = selfId;
	}

	/**
	 * Gets the parent node.
	 *
	 * @return the parent node
	 */
	public TreeNode getParentNode() {
		return parentNode;
	}

	/**
	 * Sets the parent node.
	 *
	 * @param parentNode the new parent node
	 */
	public void setParentNode(TreeNode parentNode) {
		this.parentNode = parentNode;
	}

	/**
	 * Gets the node name.
	 *
	 * @return the node name
	 */
	public String getNodeName() {
		return nodeName;
	}

	/**
	 * Sets the node name.
	 *
	 * @param nodeName the new node name
	 */
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	/**
	 * Gets the obj.
	 *
	 * @return the obj
	 */
	public Category getObj() {
		return obj;
	}

	/**
	 * Sets the obj.
	 *
	 * @param obj the new obj
	 */
	public void setObj(Category obj) {
		this.obj = obj;
	}

	

	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (selfId ^ (selfId >>> 32));
		return result;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = (int) (parentId * 31 + seq*prime);
//		return result;
//	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TreeNode){
			TreeNode node=(TreeNode)obj;
			return this.selfId==node.getSelfId();
		}
		return false;
	}
	

	/**
	 * Instantiates a new tree node.
	 *
	 * @param parentId the parent id
	 * @param selfId the self id
	 * @param nodeName the node name
	 * @param obj the obj
	 */
	public TreeNode(long parentId, long selfId, String nodeName, Integer seq,int level, Category obj) {
		super();
		this.parentId = parentId;
		this.selfId = selfId;
		this.nodeName = nodeName;
		this.seq=seq;
		this.level=level;
		this.obj = obj;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Override
	public int compareTo(TreeNode o) {
		return this.getSeq() - o.getSeq();
	}
	
	
	

}
