package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信图片组
 */
@Entity
@Table(name = "ls_weixin_img_group")
public class WeixinImgGroup implements GenericEntity<Long> {

	private static final long serialVersionUID = 8398708193863902583L;
	/**  */
	private Long id; 
		
	/** 组名 */
	private String name; 
		
	/** 描述 */
	private String desction; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 创建人 */
	private String createUserId; 
		
	/** 修改时间 */
	private Date updateDate; 
		
	/** 修改人 */
	private String updateUser; 
		
	/** 排序 */
	private Long seq; 
	
	private Integer count;
		
	
	public WeixinImgGroup() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_IMG_GROUP_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "desction")
	public String  getDesction(){
		return desction;
	} 
		
	public void setDesction(String desction){
			this.desction = desction;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "create_user_id")
	public String  getCreateUserId(){
		return createUserId;
	} 
		
	public void setCreateUserId(String createUserId){
			this.createUserId = createUserId;
		}
		
    @Column(name = "update_date")
	public Date  getUpdateDate(){
		return updateDate;
	} 
		
	public void setUpdateDate(Date updateDate){
			this.updateDate = updateDate;
		}
		
    @Column(name = "update_user")
	public String  getUpdateUser(){
		return updateUser;
	} 
		
	public void setUpdateUser(String updateUser){
			this.updateUser = updateUser;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}

	@Transient
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	


} 
