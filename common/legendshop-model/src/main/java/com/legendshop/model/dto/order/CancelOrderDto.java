package com.legendshop.model.dto.order;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 用于 DelayQueue 队列实现高效延迟取消
 * 
 * @author Tony
 * 
 */
public class CancelOrderDto implements Delayed {

	/*
	 * 订购流水号ID
	 */
	private final String subNumber;

	/*
	 * 过期时间
	 */
	private final long expireTime;// 过期时刻=订购时间+保存时间

	/**
	 * 
	 */
	private int timeout = 1800000; //1800000毫秒   默认是30分钟

	/**
	 * timeout：自动收货的超时时间，秒
	 * 
	 * @param subNumber
	 * @param timeout
	 */
	public CancelOrderDto(String subNumber, long subDate, int timeout) {
		this.subNumber = subNumber;
		this.timeout=timeout;
		this.expireTime = subDate + this.timeout*60000;
	}
	
	/**
	 * 
	 * @param subNumber
	 * @param subDate
	 */
	public CancelOrderDto(String subNumber, long subDate) {
		this.subNumber = subNumber;
		this.timeout = 30;
		this.expireTime = subDate + this.timeout*60000;
	}
	

	public String getSubNumber() {
		return subNumber;
	}

	@Override
	public int compareTo(Delayed other) {
		if (other == this) {
			return 0;
		}
		if (other instanceof CancelOrderDto) {
			CancelOrderDto orderDto = (CancelOrderDto) other;
			if (this.expireTime > orderDto.getExpireTime()) {
				return 1;
			} else if (this.expireTime == orderDto.getExpireTime()) {
				return 0;
			} else {
				return -1;
			}
		}
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		// TimeUnit.MILLISECONDS 毫秒 千分之一秒
		return unit.convert(expireTime - System.currentTimeMillis(),TimeUnit.MILLISECONDS);
	}

	

	public long getExpireTime() {
		return expireTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (expireTime ^ (expireTime >>> 32));
		result = prime * result + ((subNumber == null) ? 0 : subNumber.hashCode());
		result = prime * result + timeout;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CancelOrderDto other = (CancelOrderDto) obj;
		if (expireTime != other.expireTime)
			return false;
		if (subNumber == null) {
			if (other.subNumber != null)
				return false;
		} else if (!subNumber.equals(other.subNumber))
			return false;
		if (timeout != other.timeout)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CancelOrderDto [subNumber=" + subNumber + ", expireTime=" + expireTime + ", timeout=" + timeout + "]";
	}
	
	

}
