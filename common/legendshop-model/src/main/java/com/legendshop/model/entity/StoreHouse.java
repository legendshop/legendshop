package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * @author Legendshop
 * @version 1.0.0
 *
 */

// 仓库
@Entity
@Table(name = "ls_storehouse")
public class StoreHouse implements GenericEntity<Long>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5187000147627478449L;

	// 
	private Long id ; 
		
	// 
	private String userId ; 
		
	// 
	private String userName ; 
		
	// 状态
	private Integer status ; 
		
	// 仓库名字
	private String name ; 
		
	// 描述
	private String memo ; 
	
	private Long shopId;
		
	private String desc;
	
	public StoreHouse() {
    }
		
	public void setId(Long id){
		this.id = id ;
	}
		
		
	@Column(name = "user_id")
	public String  getUserId(){
		return userId ;
	} 
		
	public void setUserId(String userId){
		this.userId = userId ;
	}
		
		
	@Column(name = "user_name")
	public String  getUserName(){
		return userName ;
	} 
		
	public void setUserName(String userName){
		this.userName = userName ;
	}
		
		
	@Column(name = "status")
	public Integer  getStatus(){
		return status ;
	} 
		
	public void setStatus(Integer status){
		this.status = status ;
	}
		
		
	@Column(name = "name")
	public String  getName(){
		return name ;
	} 
		
	public void setName(String name){
		this.name = name ;
	}
		
		
	@Column(name = "memo")
	public String  getMemo(){
		return memo ;
	} 
		
	public void setMemo(String desc){
		this.memo = desc ;
	}
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STOREHOUSE_SEQ")
  public Long getId() {
		return 	id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Transient
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	
} 
