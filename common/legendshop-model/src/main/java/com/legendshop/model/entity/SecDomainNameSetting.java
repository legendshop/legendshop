/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.util.DateUtil;

/**
 * @Description 二级域名全局配置数据载体
 * @author 关开发
 */
public class SecDomainNameSetting {
	
	/**
	 * 是否开启域名 可选值0,1;
	 * 0代表关闭
	 * 1代表开启
	 */
	private Boolean isEnable;
	/**
	 * 保留域名 如www
	 * 多个域名之间用逗号隔开
	 */
	private String retain;
	/**
	 * 长度限制
	 * 格式: 3-12代表长度>=3,<=12
	 */
	private String sizeRange;
	/**
	 * 修改周期,限制卖家指定天数后才能修改
	 */
	private Integer modifyCycle;
	/**
	 * @return the isEnable
	 */
	public Boolean getIsEnable() {
		return isEnable;
	}
	/**
	 * @param isEnable the isEnable to set
	 */
	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}
	/**
	 * @return the retain
	 */
	public String getRetain() {
		return retain;
	}
	/**
	 * @param retain the retain to set
	 */
	public void setRetain(String retain) {
		this.retain = retain;
	}
	/**
	 * @return the sizeRange
	 */
	public String getSizeRange() {
		return sizeRange;
	}
	/**
	 * @param sizeRange the sizeRange to set
	 */
	public void setSizeRange(String sizeRange) {
		this.sizeRange = sizeRange;
	}
	/**
	 * @return the modifyCycle
	 */
	public Integer getModifyCycle() {
		return modifyCycle;
	}
	/**
	 * @param modifyCycle the modifyCycle to set
	 */
	public void setModifyCycle(Integer modifyCycle) {
		this.modifyCycle = modifyCycle;
	}
	
	/**
	 * 是否系统保留二级域名
	 * @param secDomainName
	 * @return
	 */
	public boolean isRetain(String secDomainName){
		return this.retain.toLowerCase().contains((secDomainName.toLowerCase()));
	}
	
	/**
	 * 是否允许修改
	 * @param registDate 域名注册日期
	 * @return true or false
	 */
	public boolean isAllowModify(Date registDate){
		if(registDate == null){
			return true;//第一次允许修改
		}		
		return DateUtil.getDay(registDate, modifyCycle).before(new Date());
	}
}
