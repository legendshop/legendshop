package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum ActiveBannerTypeEnum implements StringEnum {
	
	
	GROUP_BANNER("GROUP"),
	
	TRY_BANNER("TRY"),
	
	MERGER_BANNER("MERGER"),
	
	PRESELL_BANNER("PRESELL"),
	
	SECKILL_BANNER("SECKILL"),
	
	; 

	/** The num. */
	private String type;

	ActiveBannerTypeEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}
}
