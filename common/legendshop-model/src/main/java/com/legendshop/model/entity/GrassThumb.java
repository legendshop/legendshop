package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *种草文章点赞表
 */
@Entity
@Table(name = "ls_app_grass_thumb")
public class GrassThumb implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 种草文章id */
	private Long graId; 
		
	/** 用户id */
	private String userId; 
		
	
	public GrassThumb() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "APP_GRASS_THUMB_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "gra_id")
	public Long  getGraId(){
		return graId;
	} 
		
	public void setGraId(Long graId){
			this.graId = graId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
	


} 
