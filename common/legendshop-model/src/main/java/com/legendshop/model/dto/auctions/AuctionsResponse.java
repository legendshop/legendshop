/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.auctions;

import java.util.List;


/**
 * 拍卖响应结果
 */
public class AuctionsResponse {

	private int auctionStatus; // 1:代表上线 ; 2 :已完成
	
	private int remainTime;  //活动时间范围 -1:未开始   1:已结束  0：正在进行
 	
	private double currentPrice;
	
	private long bidCount;
	
	private int orderStatus; //是否中标
	
	private int qualification; //是否缴纳保证金 0: 未缴纳保证金 1: 缴纳
	
	private int crowdWatch;
	
	//投标记录
	private List<BidDto> bidList;
	

	public int getAuctionStatus() {
		return auctionStatus;
	}

	public void setAuctionStatus(int auctionStatus) {
		this.auctionStatus = auctionStatus;
	}

	public int getRemainTime() {
		return remainTime;
	}

	public void setRemainTime(int remainTime) {
		this.remainTime = remainTime;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public List<BidDto> getBidList() {
		return bidList;
	}

	public void setBidList(List<BidDto> bidList) {
		this.bidList = bidList;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public long getBidCount() {
		return bidCount;
	}

	public void setBidCount(long bidCount) {
		this.bidCount = bidCount;
	}

	public int getCrowdWatch() {
		return crowdWatch;
	}

	public void setCrowdWatch(int crowdWatch) {
		this.crowdWatch = crowdWatch;
	}

	public int getQualification() {
		return qualification;
	}

	public void setQualification(int qualification) {
		this.qualification = qualification;
	}
	
	
}
