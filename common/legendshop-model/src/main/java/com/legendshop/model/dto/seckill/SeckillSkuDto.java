/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.KeyValueEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品sku DTO.
 */
@ApiModel(value="商品sku DTO")
public class SeckillSkuDto implements Serializable {

	private static final long serialVersionUID = 6830009574780972733L;

	/** sku id. */
	@ApiModelProperty(value="sku id")
	private Long skuId;

	/** sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2. */
	@ApiModelProperty(value="sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2")
	private String properties;

	/** 价格. */
	@ApiModelProperty(value="价格")
	private Double price;

	/** sku 名称. */
	@ApiModelProperty(value="sku 名称")
	private String name;

	/** SKU标题. */
	@ApiModelProperty(value="SKU标题")
	private List<KeyValueEntity> propertiesNameList;

	/** sku状态。 1:正常 0:删除. */
	@ApiModelProperty(value="sku状态。 1:正常 0:删除")
	private Integer status;

	/** 对应属性值ID. */
	@ApiModelProperty(value="对应属性值ID")
	private String propValueIds;

	/** 秒杀价. */
	@ApiModelProperty(value="秒杀价")
	private Double seckPrice;

	/** 秒杀库存. */
	@ApiModelProperty(value="秒杀库存")
	private int seckStock;

	/**
	 * Gets the sku id.
	 *
	 * @return the sku id
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * Sets the sku id.
	 *
	 * @param skuId
	 *            the sku id
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public String getProperties() {
		return properties;
	}

	/**
	 * Sets the properties.
	 *
	 * @param properties
	 *            the properties
	 */
	public void setProperties(String properties) {
		this.properties = properties;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price
	 *            the price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the prop value ids.
	 *
	 * @return the prop value ids
	 */
	public String getPropValueIds() {
		return propValueIds;
	}

	/**
	 * Sets the prop value ids.
	 *
	 * @param propValueIds
	 *            the prop value ids
	 */
	public void setPropValueIds(String propValueIds) {
		this.propValueIds = propValueIds;
	}

	/**
	 * Gets the properties name list.
	 *
	 * @return the properties name list
	 */
	public List<KeyValueEntity> getPropertiesNameList() {
		return propertiesNameList;
	}

	/**
	 * Sets the properties name list.
	 *
	 * @param propertiesNameList
	 *            the properties name list
	 */
	public void setPropertiesNameList(List<KeyValueEntity> propertiesNameList) {
		this.propertiesNameList = propertiesNameList;
	}

	/**
	 * Gets the seck price.
	 *
	 * @return the seck price
	 */
	public Double getSeckPrice() {
		return seckPrice;
	}

	/**
	 * Sets the seck price.
	 *
	 * @param seckPrice
	 *            the seck price
	 */
	public void setSeckPrice(Double seckPrice) {
		this.seckPrice = seckPrice;
	}

	/**
	 * Gets the seck stock.
	 *
	 * @return the seck stock
	 */
	public int getSeckStock() {
		return seckStock;
	}

	/**
	 * Sets the seck stock.
	 *
	 * @param seckStock
	 *            the seck stock
	 */
	public void setSeckStock(int seckStock) {
		this.seckStock = seckStock;
	}

}
