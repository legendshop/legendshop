/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *系统事件
 */
@Entity
@Table(name = "ls_event")
public class UserEvent implements GenericEntity<Long> {

	private static final long serialVersionUID = 2999395776110544653L;

	/** ID */
	private Long eventId; 
		
	/** 操作员ID */
	private String userId; 
		
	/** 操作员名称 */
	private String userName; 
		
	/** 操作描述 */
	private String operation; 
		
	/** 响应时间 */
	private Long time; 
		
	/** 请求方法 */
	private String method; 
		
	/** 请求参数 */
	private String params; 
		
	/** IP地址 */
	private String ip; 
		
	/** 建立时间 */
	private Date createTime; 
	
	/** The start time. */
	private Date startTime;

	/** The end time. */
	private Date endTime;
		
	public UserEvent() {
    }
		
	@Id
	@Column(name = "event_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "EVENT_SEQ")
	public Long  getEventId(){
		return eventId;
	} 
		
	public void setEventId(Long eventId){
			this.eventId = eventId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "operation")
	public String  getOperation(){
		return operation;
	} 
		
	public void setOperation(String operation){
			this.operation = operation;
		}
		
    @Column(name = "time")
	public Long  getTime(){
		return time;
	} 
		
	public void setTime(Long time){
			this.time = time;
		}
		
    @Column(name = "method")
	public String  getMethod(){
		return method;
	} 
		
	public void setMethod(String method){
			this.method = method;
		}
		
    @Column(name = "params")
	public String  getParams(){
		return params;
	} 
		
	public void setParams(String params){
			this.params = params;
		}
		
    @Column(name = "ip")
	public String  getIp(){
		return ip;
	} 
		
	public void setIp(String ip){
			this.ip = ip;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	
	@Transient
	public Long getId() {
		return eventId;
	}
	
	public void setId(Long id) {
		eventId = id;
	}
	
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


} 