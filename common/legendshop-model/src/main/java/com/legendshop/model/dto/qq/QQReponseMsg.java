package com.legendshop.model.dto.qq;

/**
 * 
 * @author 开发很忙
 */
public class QQReponseMsg {
	
	public static final int SUCCESS = 0;
	
	/** 响应码 */
	private Integer code;
	
	/** 响应消息 */
	private String msg;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public boolean isSuccess(){
		return SUCCESS == code;
	}
}
