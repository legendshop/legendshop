package com.legendshop.model.dto.buy;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 购物车 用于购物车信息展示 ShopCarts:代表店铺1:{prod1;prod2}
 * 每个店铺所对应的购物车
 * 按店铺分单
 * @author tony
 * 
 */
public class ShopCarts implements Serializable {

	private static final long serialVersionUID = 8574607544429617660L;

	 /**
     * 商家ID
     */
	private Long shopId;
	
	 /**
     * 店铺名称
     */
	private String shopName;
	
	/*
	 * 商家状态
	 */
	private int shopStatus=1;
	
	  /*  总价  */
	private Double shopTotalCash=0d;
	
	  /*  店铺实际支付总额  */
	private Double shopActualTotalCash=0d;
    
    /*  商品总重量  */
    private Double shopTotalWeight;
    
    /* 商品总体积*/
    private Double shopTotalVolume;
    
    /* 商品总数量*/
    private int totalcount;

   /**
     * 折扣价
    */
    private Double discountPrice=0d;
    
    
    /**
     * 当前店铺的运费
     */
    private Double freightAmount=0d;
    
    /**
     *是否承担运费
     */
	private boolean isFreePostage;

	/**
	 *是否商家承担运费
	 */
	private boolean isFreePostageByShop = false;

	/**
	 *是否满件承担运费
	 */
	private boolean isFreePostageByFullPiece = false;

	/**
	 *是否满额承担运费
	 */
	private boolean isFreePostageByFullAmount = false;
	
	/**
     * 店铺状态
     */
	private int status; // 0:代表商家不存在或者被冻结
	
	/**
     * 获得积分
     */
    private Double totalIntegral = 0D; // 获得积分
    
    /** 普通商品item */
	private List<ShopCartItem> cartItems;
	
	/** 参与营销活动的商品和规格集合 */
	private List<MarketingDto> marketingDtoList; 
	
	/**
     * 商家配送方式[不分单情况],分单则通过每个商品来处理配送方式
     */
	private List<TransfeeDto>  transfeeDtos;
	
	/**
	 * 当前订单选中的配送模版[不分单情况]
	 * @return
	 */
	private TransfeeDto currentSelectTransfee;
	
    /**
     * 卖家留言
     */
    private String remark;
    
    /**
     * 是否支持门店
     */
    private boolean supportStore=false;
    
    /** 门店信息  门店改造已废弃掉 */
    private ChainDto chain;
    
    /** 订单号  */
    private String subNember;
    
    /** 订单ID */
    private Long subId;
    
    /** 包邮信息*/
    private String mailfeeStr;
    
    /** 优惠券优惠的金额*/
    private double couponOffPrice;
    
    /** 店铺总共有多少个优惠券 */
    private Long ShopCouponCount;
    
    /** 红包优惠的金额 */
    private double redpackOffPrice;
    
    /** 是否区域限售  */
    private boolean isRegionalSales;
    
    /** 门店购物车分组  （门店自提用） */
   	private List<StoreCarts> storeCarts;
    

   	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<ShopCartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<ShopCartItem> cartItems) {
		this.cartItems = cartItems;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public Double getTotalIntegral() {
		return totalIntegral;
	}

	public void setTotalIntegral(Double totalIntegral) {
		this.totalIntegral = totalIntegral;
	}
	


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


	public void setTransfeeDtos(List<TransfeeDto> transfeeDtos) {
		this.transfeeDtos = transfeeDtos;
	}
	
	
	
	
	public TransfeeDto getCurrentSelectTransfee() {
		return currentSelectTransfee;
	}

	public void setCurrentSelectTransfee(TransfeeDto currentSelectTransfee) {
		this.currentSelectTransfee = currentSelectTransfee;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (shopId ^ (shopId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopCarts other = (ShopCarts) obj;
		if (shopId != other.shopId)
			return false;
		return true;
	}


	/**
	 * 配送方式
	 * @return
	 */
	public List<TransfeeDto> getTransfeeDtos() {
		return transfeeDtos;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public boolean isFreePostage() {
		return isFreePostage;
	}

	public void setFreePostage(boolean isFreePostage) {
		this.isFreePostage = isFreePostage;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}

	public ChainDto getChain() {
		return chain;
	}

	public void setChain(ChainDto chain) {
		this.chain = chain;
	}

	public int getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(int shopStatus) {
		this.shopStatus = shopStatus;
	}

	public Double getShopTotalCash() {
		return shopTotalCash;
	}

	public void setShopTotalCash(Double shopTotalCash) {
		this.shopTotalCash = shopTotalCash;
	}

	public Double getShopActualTotalCash() {
		return shopActualTotalCash;
	}

	public void setShopActualTotalCash(Double shopActualTotalCash) {
		this.shopActualTotalCash = shopActualTotalCash;
	}

	public Double getShopTotalWeight() {
		return shopTotalWeight;
	}

	public void setShopTotalWeight(Double shopTotalWeight) {
		this.shopTotalWeight = shopTotalWeight;
	}

	public Double getShopTotalVolume() {
		return shopTotalVolume;
	}

	public void setShopTotalVolume(Double shopTotalVolume) {
		this.shopTotalVolume = shopTotalVolume;
	}

	public String getSubNember() {
		return subNember;
	}

	public void setSubNember(String subNember) {
		this.subNember = subNember;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getMailfeeStr() {
		return mailfeeStr;
	}

	public void setMailfeeStr(String mailfeeStr) {
		this.mailfeeStr = mailfeeStr;
	}
	
	public List<MarketingDto> getMarketingDtoList() {
		return marketingDtoList;
	}

	public void setMarketingDtoList(List<MarketingDto> marketingDtoList) {
		this.marketingDtoList = marketingDtoList;
	}

	public double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	public void setRegionalSales(boolean isRegionalSales) {
		this.isRegionalSales = isRegionalSales;
	}

	public boolean isRegionalSales() {
		boolean config=false;
		if(AppUtils.isNotBlank(marketingDtoList)){
			for (MarketingDto dto : marketingDtoList) {
				if(dto.isRegionalSales()){
					config=true;
				}
			}
		}
		return config;
	}

	public Long getShopCouponCount() {
		return ShopCouponCount;
	}

	public void setShopCouponCount(Long shopCouponCount) {
		ShopCouponCount = shopCouponCount;
	}
	
	public boolean isFreePostageByShop() {
		return isFreePostageByShop;
	}

	public void setFreePostageByShop(boolean isFreePostageByShop) {
		this.isFreePostageByShop = isFreePostageByShop;
	}

	public boolean isFreePostageByFullPiece() {
		return isFreePostageByFullPiece;
	}

	public void setFreePostageByFullPiece(boolean isFreePostageByFullPiece) {
		this.isFreePostageByFullPiece = isFreePostageByFullPiece;
	}

	public boolean isFreePostageByFullAmount() {
		return isFreePostageByFullAmount;
	}

	public void setFreePostageByFullAmount(boolean isFreePostageByFullAmount) {
		this.isFreePostageByFullAmount = isFreePostageByFullAmount;
	}

	public List<StoreCarts> getStoreCarts() {
		return storeCarts;
	}

	public void setStoreCarts(List<StoreCarts> storeCarts) {
		this.storeCarts = storeCarts;
	}

	//获取计算营销活动及已经使用优惠券之后的价格
	public double getCalculatedCouponAmount(){
		if(AppUtils.isBlank(cartItems)){
			return 0.0;
		}
		double finalPrice=0.0;
		for(ShopCartItem shopCartItem:cartItems){
			finalPrice=Arith.add(shopCartItem.getCalculatedCouponAmount(), finalPrice);
		}
		return finalPrice;
	}
	//获取整个店铺购物车使用的红包金额
	public double getTotalRedpackOffPrice(){
		double result=0.0;
		if(AppUtils.isBlank(cartItems)){
			return result;
		}
		
		for(ShopCartItem shopCartItem:cartItems){
			result=Arith.add(shopCartItem.getRedpackOffPrice(), result);
		}
		return result;
	}
	//获取整个店铺购物车使用的优惠券金额
	public double getTotalCouponOffPrice(){
		double result=0.0;
		if(AppUtils.isBlank(cartItems)){
			return result;
		}
		
		for(ShopCartItem shopCartItem:cartItems){
			result=Arith.add(shopCartItem.getCouponOffPrice(), result);
		}
		return result;
	}
}
