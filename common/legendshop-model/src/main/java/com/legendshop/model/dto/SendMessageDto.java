package com.legendshop.model.dto;

import java.io.Serializable;

public class SendMessageDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8863976492386353678L;
	private String mobile;
	private String email;
	private String text;
	private String userName;
	private Integer type;
	private String title;
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
}
