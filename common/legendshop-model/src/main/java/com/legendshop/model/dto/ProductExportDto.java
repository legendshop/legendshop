package com.legendshop.model.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProductExportDto implements Serializable{
	
	private String prodName;
	private String brief;
	private String attr;
	private Double price;
	private Double cash;
	private Long stocks;
	private Long totalStocks;
	private Long arm;
	private String modelCode;
	private String partycode;
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
	public Long getStocks() {
		return stocks;
	}
	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}
	public Long getTotalStocks() {
		return totalStocks;
	}
	public void setTotalStocks(Long totalStocks) {
		this.totalStocks = totalStocks;
	}
	public Long getArm() {
		return arm;
	}
	public void setArm(Long arm) {
		this.arm = arm;
	}

	public String getPartycode() {
		return partycode;
	}
	public void setPartycode(String partycode) {
		this.partycode = partycode;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
}
