package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 更新仓库库存
 */
@ApiModel(value = "UpdSkuStockDto")
public class UpdSkuStockDto implements Serializable {

	private static final long serialVersionUID = -3706715510163950035L;

	/** 商品Id */
	@ApiModelProperty(value = "商品Id")
	private String prodId;

	/** 商品名 */
	@ApiModelProperty(value = "商品名")
	private String name;

	/** 中文属性 */
	@ApiModelProperty(value = "中文属性")
	private String cnProperties;

	@ApiModelProperty(value = "单品Id")
	private String skuId;

	/** 库存 */
	@ApiModelProperty(value = "库存数量")
	private Long stocks;

	/** 实际库存 */
	@ApiModelProperty(value = "实际库存")
	private Long actualStocks;

	public String getProdId() {
		return prodId;
	}

	public void setProdId(String prodId) {
		this.prodId = prodId;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public Long getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Long actualStocks) {
		this.actualStocks = actualStocks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}
}
