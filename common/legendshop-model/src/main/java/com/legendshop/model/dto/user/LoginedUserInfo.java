/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.user;

import java.io.Serializable;

/**
 * 已经登录的用户详细信息,使用者不能修改
 * 
 */
public class LoginedUserInfo implements Serializable {

	private static final long serialVersionUID = 3908632520035702509L;

	/** 用户Id */
	private final String userId;

	/** 用户名 */
	private final String userName;
	
	/** 商家Id */
	private final Long shopId;

	/** openId */
	private final String accessToken;
	
	/** 微信用户openId */
	private String openId;
	
	public LoginedUserInfo(String userId, String userName, Long shopId, String accessToken) {
		this(userId, userName, shopId, accessToken, null);
	}
	
	public LoginedUserInfo(String userId, String userName, Long shopId, String accessToken, String openId) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.shopId = shopId;
		this.accessToken = accessToken;
		this.openId = openId;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}


	public Long getShopId() {
		return shopId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
}
