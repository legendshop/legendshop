package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *拼团参加表
 */
@Entity
@Table(name = "ls_merge_group_add")
public class MergeGroupAdd implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id; 
		
	/** 活动ID */
	private Long mergeId; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 订单ID */
	private Long subId; 
	
	/** 订单编号 */
	private String subNumber;
		
	/** 用户ID */
	private String userId; 
		
	/** 用户名称 */
	private String userName; 
	
	/** 用户昵称 */
	private String nickName;
	
	/** 用户头像 */
	private String portraitPic;
		
	/** 是否团长，默认不是 */
	private Boolean isHead; 
		
	/** 状态[1:进行中,2:成功,3:失败/已退款 ] */
	private Integer status; 
	
	/** 拼团运营信息ID,ls_merge_group_operate主键 */
	private Long operateId;
	
	/** 参团时间 */
	private Date createTime;
	
	/** 退款单号 */
	private String outRefundNo;
	
    /** 支付类型 */
    private String payTypeId; 
    
    /** 是否是团长免单退款  */
    private boolean  isExemption; 
	
	public MergeGroupAdd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MERGE_GROUP_ADD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "merge_id")
	public Long  getMergeId(){
		return mergeId;
	} 
		
	public void setMergeId(Long mergeId){
			this.mergeId = mergeId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "sub_id")
	public Long  getSubId(){
		return subId;
	} 
		
	public void setSubId(Long subId){
			this.subId = subId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
	
	@Column(name = "sub_number")
    public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	@Column(name = "user_name")
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "nick_name")
    public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "portrait_pic")
	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	@Column(name = "is_head")
	public Boolean  getIsHead(){
		return isHead;
	} 
		
	public void setIsHead(Boolean isHead){
			this.isHead = isHead;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "operate_id")
	public Long getOperateId() {
		return operateId;
	}

	public void setOperateId(Long operateId) {
		this.operateId = operateId;
	}

	@Column(name = "out_refund_no")
	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	@Column(name = "pay_type_id")
	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	@Column(name = "is_exemption")
	public boolean isExemption() {
		return isExemption;
	}

	public void setExemption(boolean isExemption) {
		this.isExemption = isExemption;
	}
	

} 
