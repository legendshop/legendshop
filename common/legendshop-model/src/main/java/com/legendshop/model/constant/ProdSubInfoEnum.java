/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 退货Enum
 */
public enum ProdSubInfoEnum implements IntegerEnum{
	
		// 待审核
		PRODSUBINFO_AUDIT(0),

		// 已读审核通过
		PRODSUBINFO_AUDIT_SUCCEE(1),
		
		PRODSUBINFO_AUDIT_FAIL(-1);
		//删除
		
		/** The num. */
		private Integer num;
	@Override
	public Integer value() {
		return num;
	}
	ProdSubInfoEnum(Integer num) {
		this.num = num;
	}
}
