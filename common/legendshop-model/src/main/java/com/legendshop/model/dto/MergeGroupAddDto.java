package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

public class MergeGroupAddDto implements Serializable {

	private static final long serialVersionUID = 1L;

	// 订单商品实际价格(运费 折扣 促销)
	private Double actualTotal;

	private Date subDate; // 下订单的时间

	private Integer productNums;// 订单商品总数

	/** 是否团长，默认不是 */
	private Boolean isHead;

	private String subNumber;

	private String userName;

	private Long mergeId;

	private String addNumber;

	private Long subId;

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Boolean getIsHead() {
		return isHead;
	}

	public void setIsHead(Boolean isHead) {
		this.isHead = isHead;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getMergeId() {
		return mergeId;
	}

	public void setMergeId(Long mergeId) {
		this.mergeId = mergeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

}
