package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家装修热点模块
 */
@Entity
@Table(name = "ls_shop_layout_hot")
public class ShopLayoutHot implements GenericEntity<Long> {

	private static final long serialVersionUID = 8918611502620393152L;

	/** ID */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商家装修ID */
	private Long shopDecotateId; 
		
	/** 布局id */
	private Long layoutId; 
		
	/** 热点数据[{"height":3,"hotspotUrl":"fsdgsfdgsfdgsd","top":5,"width":2}] */
	private String hotDatas; 
		
	/** 顺序 */
	private Integer seq; 
	
	private String imageFile;
	
	private Long layoutDivId;
		
	
	public ShopLayoutHot() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_LAYOUT_HOT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_decotate_id")
	public Long  getShopDecotateId(){
		return shopDecotateId;
	} 
		
	public void setShopDecotateId(Long shopDecotateId){
			this.shopDecotateId = shopDecotateId;
		}
		
    @Column(name = "layout_id")
	public Long  getLayoutId(){
		return layoutId;
	} 
		
	public void setLayoutId(Long layoutId){
			this.layoutId = layoutId;
		}
		
    @Column(name = "hot_datas")
	public String  getHotDatas(){
		return hotDatas;
	} 
		
	public void setHotDatas(String hotDatas){
			this.hotDatas = hotDatas;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}

	 @Column(name = "image_file")
	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	 @Column(name = "layout_div_id")
	public Long getLayoutDivId() {
		return layoutDivId;
	}

	public void setLayoutDivId(Long layoutDivId) {
		this.layoutDivId = layoutDivId;
	}
	
	


} 
