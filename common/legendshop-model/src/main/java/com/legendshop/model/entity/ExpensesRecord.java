package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *消费记录表
 */
@Entity
@Table(name = "ls_expenses_record")
@ApiModel("消费记录")
public class ExpensesRecord implements GenericEntity<Long> {

	private static final long serialVersionUID = -4413844398878063499L;

	/** 主键id */
	@ApiModelProperty(value = "主键id")
	private Long id; 
		
	/** 用户id */
	@ApiModelProperty(value = "用户id")
	private String userId; 
		
	/** 记录日期 */
	@ApiModelProperty(value = "记录日期")
	private Date recordDate; 
		
	/** 记录金额 */
	@ApiModelProperty(value = "记录金额 ")
	private Double recordMoney; 
		
	/** 记录备注 */
	@ApiModelProperty(value = "记录备注")
	private String recordRemark; 
		
	
	public ExpensesRecord() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "EXPENSES_RECORD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "record_date")
	public Date  getRecordDate(){
		return recordDate;
	} 
		
	public void setRecordDate(Date recordDate){
			this.recordDate = recordDate;
		}
		
    @Column(name = "record_money")
	public Double  getRecordMoney(){
		return recordMoney;
	} 
		
	public void setRecordMoney(Double recordMoney){
			this.recordMoney = recordMoney;
		}
		
    @Column(name = "record_remark")
	public String  getRecordRemark(){
		return recordRemark;
	} 
		
	public void setRecordRemark(String recordRemark){
			this.recordRemark = recordRemark;
		}
	


} 
