/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 预售订单支付方式
 * 支付方式,0  全额支付, 1 订金支付的订金  2 订金支付尾款
 */
public enum PayPctTypeEnum implements IntegerEnum{
	
	/** 全额支付 */
	FULL_PAY(0),
	
	/** 订金支付的订金 */
	FRONT_MONEY(1),
	
	/** 订金支付尾款 */
	FINAL_MONEY(2);
	
	private Integer num;

	@Override
	public Integer value() {

		return num;
	}

	/**
	 * @param num
	 */
	private PayPctTypeEnum(Integer num) {
		this.num = num;
	}
	
}
