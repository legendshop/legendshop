package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Comparator;

/**
 * 
 * @author tony
 *
 */
public class TreeNodeComparator implements Comparator<TreeNode> ,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(TreeNode o1, TreeNode o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		}else if(o1.getSeq().equals(o2.getSeq())){
			return 0;
		} else if (o1.getSeq() < o2.getSeq()) {
			return -1;
		} else {
			return 1;
		}
	}

}
