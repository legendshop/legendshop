package com.legendshop.model.security;

import java.io.Serializable;
import java.util.List;
/**
 * 商家菜单组
 *
 */
public class ShopMenuGroup implements Serializable{

	private static final long serialVersionUID = -7220521000262406270L;

	private String label; //英文标签，唯一的ID

	private String name;//页面显示的名字
	
	private List<ShopMenu> shopMenuList;
	
	private boolean isSelected = false;
	
	/** 顺序. */
	private Integer seq;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<ShopMenu> getShopMenuList() {
		return shopMenuList;
	}

	public void setShopMenuList(List<ShopMenu> shopMenuList) {
		this.shopMenuList = shopMenuList;
	}
	
	
	//拷贝一个新的菜单组
	public ShopMenuGroup copy(List<ShopMenu> shopMenuList){
		ShopMenuGroup group = new ShopMenuGroup();
		group.setLabel(this.getLabel());
		group.setName(this.getName());
		group.setShopMenuList(shopMenuList);
		return group;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
}
