/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;

/**
 * Key Value Mapping.
 */
public class IdTextEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8762806056619273121L;

	/**
	 * 
	 */

	private String id;

	private String text;

	public IdTextEntity(){
		
	}
	public IdTextEntity(String id,String text) {
		this.id=id;
		this.text=text;
	}


	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
