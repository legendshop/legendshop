/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.security.UserEntity;
/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 */
@Entity
@Table(name = "ls_role")
public class Role  implements GenericEntity<String> {
	 
 	/** The Constant serialVersionUID. */
 	private static final long serialVersionUID = 3162332789634848522L;
	 
 	/** The id. */
 	private String id;
     
     /** The name. */
     private String name;
     
     /** The role type. */
     private String roleType;
     
     /** 角色分类。1：是普通角色，2：系统角色，系统权限只有超级管理员可以设置. */
     private Integer category;
     
	/** The enabled. */
     private String enabled;
     
     /** The note. */
     private String note;
     
     private String appNo;
     
     /** The users. */
     private List<UserEntity> users = new ArrayList<UserEntity>();
     
     /** The functions. */
     private List<Function> functions = new ArrayList<Function>();
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
     @Id
 	@Column(name = "id")
  	@GeneratedValue(strategy = GenerationType.UUID)
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the role type.
	 * 
	 * @return the role type
	 */
	@Column(name = "role_type")
	public String getRoleType() {
		return roleType;
	}
	
	/**
	 * Sets the role type.
	 * 
	 * @param roleType
	 *            the new role type
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	
    /**
     * Gets the category.
     *
     * @return the category
     */
	@Column(name = "category")
    public Integer getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(Integer category) {
		this.category = category;
	}

	
	/**
	 * Gets the enabled.
	 * 
	 * @return the enabled
	 */
	@Column(name = "enabled")
	public String getEnabled() {
		return enabled;
	}
	
	/**
	 * Sets the enabled.
	 * 
	 * @param enabled
	 *            the new enabled
	 */
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * Gets the note.
	 * 
	 * @return the note
	 */
	@Column(name = "note")
	public String getNote() {
		return note;
	}
	
	/**
	 * Sets the note.
	 * 
	 * @param note
	 *            the new note
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 * Gets the users.
	 * 
	 * @return the users
	 */
	@Transient
	public List<UserEntity> getUsers() {
		return users;
	}
	
	/**
	 * Sets the users.
	 * 
	 * @param users
	 *            the new users
	 */
	public void setUsers(List<UserEntity> users) {
		this.users = users;
	}
	
	/**
	 * Gets the functions.
	 * 
	 * @return the functions
	 */
	@Transient
	public List<Function> getFunctions() {
		return functions;
	}
	
	/**
	 * Sets the functions.
	 * 
	 * @param functions
	 *            the new functions
	 */
	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}

	@Column(name = "app_no")
	public String getAppNo() {
		return appNo;
	}

	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}

}