/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;


import com.legendshop.util.AppUtils;

/**
 * 积分类型.
 */
public enum IntegralLogTypeEnum {
	/**
	 * 系统
	 */
	LOG_DEFAULT(0, "系统"),

	/**
	 * 注册
	 */
	LOG_REG(1, "注册"),

	/**
	 * 登录
	 */
	LOG_LOGIN(2, "登录"),

	/**
	 * 充值
	 */
	LOG_RECHARGE(3, "充值"),

	/**
	 * 积分订单兑换
	 */
	LOG_ORDER(4, "积分订单兑换"),

	/**
	 * 完善用户资料
	 */
	PERFECT_USER_INFO(5, "完善用户资料"),

	/**
	 * 订单评论
	 */
	ORDER_COMMENT(6, "订单评论"),

	/**
	 * 推荐用户送积分
	 */
	RECOMMEND_USER(7, "推荐用户送积分"),

	/**
	 * 晒单送积分
	 */
	SUN_ORDER(8, "晒单送积分"),

	/**
	 * 订单消费送积分
	 */
	ORDER_CONSUMER(9, "订单消费送积分"),

	/**
	 * 积分优惠券兑换
	 */
	COUPON_EXCHANGE(10, "积分优惠券兑换"),

	/**
	 * 其它（不可能会返回"其它"的，如果返回，就是有新增的状态没有加到该枚举类中）
	 */
	OTHER(99, "其它")
	;

	/** The num. */
	private Integer num;

	/** The desc. */
	private final String desc;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	IntegralLogTypeEnum(Integer num, String desc) {
		this.num = num;
		this.desc = desc;
	}

	public static String getDesc(Integer code) {
		if (AppUtils.isNotBlank(code)) {
			IntegralLogTypeEnum[] licenseEnums = values();
			for (IntegralLogTypeEnum licenseEnum : licenseEnums) {
				if (licenseEnum.num.equals(code)) {
					return licenseEnum.desc;
				}
			}
		}
		return OTHER.desc;
	}
}
