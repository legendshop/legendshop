/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity.presell;

import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.SubItem;

/**
 * 包含订单表ls_sub 和ls_presell_sub两个表的实体类
 * @Description 
 * @author 关开发
 */
public class PresellSubEntity {
	/** 订单ID */
	private Long subId;

	/** The prod name. */
	private String prodName;
	
	private String userId;

	/** The user name. */
	private String userName;

	/** The sub date. */
	private Date subDate; // 下订单的时间

	/** The pay date. */
	private Date payDate; // 支付的时间

	/** The update date. */
	private Date updateDate; // 更新时间

	/** The sub number. */
	private String subNumber;
	
	
	/** The sub type. */
	private String subType;

	// 订单商品原价
	/** The total. */
	private Double total;

	// 订单商品实际价格(运费 折扣 促销)
	/** The actual total. */
	private Double actualTotal;

	/** The pay id. */
	private Long payId;

	/** The pay type id. */
	private String payTypeId;

	/** The status. */
	private Integer status;

	// 支付方式名称
	/** The pay type name. */
	private String payTypeName;


	/** The other. */
	private String other;

	private Long shopId;
	
	/** The shop name. */
	private String shopName;

	//购物车，每个basket代表一个商品
	private List<Basket> basket;
	
	private List<ShopCartItem> cartItems;

	/** The pay type. */
	private List<PayType> payType;

	/** The score id. */
	private Long scoreId;

	/** The score. */
	private Integer score;
	
	private String dvyType;
	
	/** The dvy type id.物流公司ID */
	private Long dvyTypeId;
	
	/** The delivery flow id. 物流单号*/
	private String dvyFlowId;

	
	
	/** The delivery invoice id. 发票单号*/
	private Long invoiceSubId;
	
	/** 物流费用 */
	private Double freightAmount;
	
	
	/** The delivery order_remark. 给买家留言 */
	private String orderRemark;
	
	
	/** The delivery addr_order_id. 用户订单地址Id*/
	private Long addrOrderId;
	
	/** 是否需要发票(0:不需要;1:需要) */
	private Integer isNeedInvoice;
	
	private Integer payManner; //支付方式(1:货到付款;2:在线支付)
	
	private Double weight; //物流重量(千克)
	
	private Double volume;//物流体积(立方米)
	
	private Double discountPrice; //优惠总金额
	
	private Double distCommisAmount;//分销的佣金总额
	
	private Double couponOffPrice;//使用优惠券的金额
	
	
	private Integer productNums;//订单商品总数
	
	/** The pay date. */
	private Date dvyDate; // 发货时间
	
	private Date finallyDate; // 完成时间/确认收货时间
	
	private Integer isCod; //是否货到付款
	
	private Integer isPayed; //是否已经支付
	
	private Integer deleteStatus; //用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除
	
	private List<SubItem> subItems;//订单项列表
	
	private Integer provinceId;
	
	/** ------ 预售订单信息 ----- */
	
	/** 支付方式,0:全额,1:订金 */
	private Integer payPctType; 
		
	/** 订金金额 */
	private java.math.BigDecimal preDepositPrice; 
		
	/** 尾款金额 */
	private java.math.BigDecimal finalPrice; 
		
	/** 尾款支付开始时间 */
	private Date finalMStart; 
		
	/** 尾款支付结束时间 */
	private Date finalMEnd; 
		
	/** 订金支付名称 */
	private String depositPayName; 
		
	/** 订金支付流水号 */
	private String depositTradeNo; 
		
	/** 订金支付时间 */
	private Date depositPayTime; 
		
	/** 是否支付订金 */
	private Integer isPayDeposit; 
		
	/** 尾款支付名称 */
	private String finalPayName; 
		
	/** 尾款支付时间 */
	private Date finalPayTime; 
		
	/** 尾款流水号 */
	private String finalTradeNo; 
		
	/** 是否支付尾款 */
	private Integer isPayFinal;

	/**
	 * @return the subId
	 */
	public Long getSubId() {
		return subId;
	}

	/**
	 * @param subId the subId to set
	 */
	public void setSubId(Long subId) {
		this.subId = subId;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the subDate
	 */
	public Date getSubDate() {
		return subDate;
	}

	/**
	 * @param subDate the subDate to set
	 */
	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	/**
	 * @return the payDate
	 */
	public Date getPayDate() {
		return payDate;
	}

	/**
	 * @param payDate the payDate to set
	 */
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the subNumber
	 */
	public String getSubNumber() {
		return subNumber;
	}

	/**
	 * @param subNumber the subNumber to set
	 */
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the actualTotal
	 */
	public Double getActualTotal() {
		return actualTotal;
	}

	/**
	 * @param actualTotal the actualTotal to set
	 */
	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	/**
	 * @return the payId
	 */
	public Long getPayId() {
		return payId;
	}

	/**
	 * @param payId the payId to set
	 */
	public void setPayId(Long payId) {
		this.payId = payId;
	}

	/**
	 * @return the payTypeId
	 */
	public String getPayTypeId() {
		return payTypeId;
	}

	/**
	 * @param payTypeId the payTypeId to set
	 */
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the payTypeName
	 */
	public String getPayTypeName() {
		return payTypeName;
	}

	/**
	 * @param payTypeName the payTypeName to set
	 */
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the basket
	 */
	public List<Basket> getBasket() {
		return basket;
	}

	/**
	 * @param basket the basket to set
	 */
	public void setBasket(List<Basket> basket) {
		this.basket = basket;
	}

	/**
	 * @return the cartItems
	 */
	public List<ShopCartItem> getCartItems() {
		return cartItems;
	}

	/**
	 * @param cartItems the cartItems to set
	 */
	public void setCartItems(List<ShopCartItem> cartItems) {
		this.cartItems = cartItems;
	}

	/**
	 * @return the payType
	 */
	public List<PayType> getPayType() {
		return payType;
	}

	/**
	 * @param payType the payType to set
	 */
	public void setPayType(List<PayType> payType) {
		this.payType = payType;
	}

	/**
	 * @return the scoreId
	 */
	public Long getScoreId() {
		return scoreId;
	}

	/**
	 * @param scoreId the scoreId to set
	 */
	public void setScoreId(Long scoreId) {
		this.scoreId = scoreId;
	}

	/**
	 * @return the score
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/**
	 * @return the dvyType
	 */
	public String getDvyType() {
		return dvyType;
	}

	/**
	 * @param dvyType the dvyType to set
	 */
	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	/**
	 * @return the dvyTypeId
	 */
	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	/**
	 * @param dvyTypeId the dvyTypeId to set
	 */
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	/**
	 * @return the dvyFlowId
	 */
	public String getDvyFlowId() {
		return dvyFlowId;
	}

	/**
	 * @param dvyFlowId the dvyFlowId to set
	 */
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	/**
	 * @return the invoiceSubId
	 */
	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	/**
	 * @param invoiceSubId the invoiceSubId to set
	 */
	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	/**
	 * @return the freightAmount
	 */
	public Double getFreightAmount() {
		return freightAmount;
	}

	/**
	 * @param freightAmount the freightAmount to set
	 */
	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	/**
	 * @return the orderRemark
	 */
	public String getOrderRemark() {
		return orderRemark;
	}

	/**
	 * @param orderRemark the orderRemark to set
	 */
	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	/**
	 * @return the addrOrderId
	 */
	public Long getAddrOrderId() {
		return addrOrderId;
	}

	/**
	 * @param addrOrderId the addrOrderId to set
	 */
	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	/**
	 * @return the isNeedInvoice
	 */
	public Integer getIsNeedInvoice() {
		return isNeedInvoice;
	}

	/**
	 * @param isNeedInvoice the isNeedInvoice to set
	 */
	public void setIsNeedInvoice(Integer isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}

	/**
	 * @return the payManner
	 */
	public Integer getPayManner() {
		return payManner;
	}

	/**
	 * @param payManner the payManner to set
	 */
	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	/**
	 * @return the weight
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	/**
	 * @return the volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @return the discountPrice
	 */
	public Double getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * @param discountPrice the discountPrice to set
	 */
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	/**
	 * @return the distCommisAmount
	 */
	public Double getDistCommisAmount() {
		return distCommisAmount;
	}

	/**
	 * @param distCommisAmount the distCommisAmount to set
	 */
	public void setDistCommisAmount(Double distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}

	/**
	 * @return the couponOffPrice
	 */
	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	/**
	 * @param couponOffPrice the couponOffPrice to set
	 */
	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	/**
	 * @return the productNums
	 */
	public Integer getProductNums() {
		return productNums;
	}

	/**
	 * @param productNums the productNums to set
	 */
	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	/**
	 * @return the dvyDate
	 */
	public Date getDvyDate() {
		return dvyDate;
	}

	/**
	 * @param dvyDate the dvyDate to set
	 */
	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	/**
	 * @return the finallyDate
	 */
	public Date getFinallyDate() {
		return finallyDate;
	}

	/**
	 * @param finallyDate the finallyDate to set
	 */
	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	/**
	 * @return the isCod
	 */
	public Integer getIsCod() {
		return isCod;
	}

	/**
	 * @param isCod the isCod to set
	 */
	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	/**
	 * @return the isPayed
	 */
	public Integer getIsPayed() {
		return isPayed;
	}

	/**
	 * @param isPayed the isPayed to set
	 */
	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	/**
	 * @return the deleteStatus
	 */
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	/**
	 * @param deleteStatus the deleteStatus to set
	 */
	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	/**
	 * @return the subItems
	 */
	public List<SubItem> getSubItems() {
		return subItems;
	}

	/**
	 * @param subItems the subItems to set
	 */
	public void setSubItems(List<SubItem> subItems) {
		this.subItems = subItems;
	}

	/**
	 * @return the provinceId
	 */
	public Integer getProvinceId() {
		return provinceId;
	}

	/**
	 * @param provinceId the provinceId to set
	 */
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * @return the payPctType
	 */
	public Integer getPayPctType() {
		return payPctType;
	}

	/**
	 * @param payPctType the payPctType to set
	 */
	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	/**
	 * @return the preDepositPrice
	 */
	public java.math.BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	/**
	 * @param preDepositPrice the preDepositPrice to set
	 */
	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	/**
	 * @return the finalPrice
	 */
	public java.math.BigDecimal getFinalPrice() {
		return finalPrice;
	}

	/**
	 * @param finalPrice the finalPrice to set
	 */
	public void setFinalPrice(java.math.BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	/**
	 * @return the finalMStart
	 */
	public Date getFinalMStart() {
		return finalMStart;
	}

	/**
	 * @param finalMStart the finalMStart to set
	 */
	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	/**
	 * @return the finalMEnd
	 */
	public Date getFinalMEnd() {
		return finalMEnd;
	}

	/**
	 * @param finalMEnd the finalMEnd to set
	 */
	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	/**
	 * @return the depositPayName
	 */
	public String getDepositPayName() {
		return depositPayName;
	}

	/**
	 * @param depositPayName the depositPayName to set
	 */
	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}

	/**
	 * @return the depositTradeNo
	 */
	public String getDepositTradeNo() {
		return depositTradeNo;
	}

	/**
	 * @param depositTradeNo the depositTradeNo to set
	 */
	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}

	/**
	 * @return the depositPayTime
	 */
	public Date getDepositPayTime() {
		return depositPayTime;
	}

	/**
	 * @param depositPayTime the depositPayTime to set
	 */
	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	/**
	 * @return the isPayDeposit
	 */
	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	/**
	 * @param isPayDeposit the isPayDeposit to set
	 */
	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	/**
	 * @return the finalPayName
	 */
	public String getFinalPayName() {
		return finalPayName;
	}

	/**
	 * @param finalPayName the finalPayName to set
	 */
	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}

	/**
	 * @return the finalPayTime
	 */
	public Date getFinalPayTime() {
		return finalPayTime;
	}

	/**
	 * @param finalPayTime the finalPayTime to set
	 */
	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	/**
	 * @return the finalTradeNo
	 */
	public String getFinalTradeNo() {
		return finalTradeNo;
	}

	/**
	 * @param finalTradeNo the finalTradeNo to set
	 */
	public void setFinalTradeNo(String finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}

	/**
	 * @return the isPayFinal
	 */
	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	/**
	 * @param isPayFinal the isPayFinal to set
	 */
	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	} 
}
