package com.legendshop.model.dto.payment;

import com.legendshop.util.AppUtils;

/**
 * 用于请求支付
 * 
 * @author Tony
 */
public class InitPayFromDto {

	private String subSettlementSn;
	
	/** 多个订单流水号(合并支付)  跟SubSettlementTypeEnum 相关 */
	private String  [] subNumbers;
	
	/** 余额支付方式(1:预付款  2：金币) */
	private Integer prePayTypeVal; 
	
	/** 是否钱包 */
	private boolean walletPay;
	
	/** 是否钱包全额支付 */
	private boolean fullPay=false;
	
	/** 支付方式 */
	private String payTypeId;
	
	/** 商家ID */
	private String userId;

	/** 用户的微信opendId */
	private String opendId;

	/** 网站的URL地址, PC或服务端的 */
	private String domainURL;
	
	/** 网站的URL地址, 前端的 */
	private String wapDomainURL;

	/** 支付的类型 SubSettlementTypeEnum */
	private String subSettlementType;

	/** 支付来源 PaySourceEnum */
	private String source;

	/** 用户的ＩＰ */
	private String ip;
	
	/** 预售支付:尾款/订金  */
	private Integer payPctType;
	
	/** 是否预售  */
	private boolean isPresell=false;

	public InitPayFromDto(String subSettlementSn, String[] subNumbers, boolean walletPay, Integer prePayTypeVal, 
			 String payTypeId, String userId, String opendId, String domainURL, String wapDomainURL,
			String subSettlementType, String source, String ip, Integer payPctType, boolean isPresell) {
		super();
		this.subSettlementSn = subSettlementSn;
		this.subNumbers = subNumbers;
		this.prePayTypeVal = prePayTypeVal;
		this.walletPay = walletPay;
		this.payTypeId = payTypeId;
		this.userId = userId;
		this.opendId = opendId;
		this.domainURL = domainURL;
		this.wapDomainURL = wapDomainURL;
		this.subSettlementType = subSettlementType;
		this.source = source;
		this.ip = ip;
		this.payPctType = payPctType;
		this.isPresell = isPresell;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpendId() {
		return opendId;
	}

	public void setOpendId(String opendId) {
		this.opendId = opendId;
	}

	public String getDomainURL() {
		return domainURL;
	}

	public void setDomainURL(String domainURL) {
		this.domainURL = domainURL;
	}
	
	public String getWapDomainURL() {
		return wapDomainURL;
	}

	public void setWapDomainURL(String wapDomainURL) {
		this.wapDomainURL = wapDomainURL;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}
	
	public String[] getSubNumbers() {
		return subNumbers;
	}

	public void setSubNumbers(String[] subNumbers) {
		this.subNumbers = subNumbers;
	}

	public Integer getPrePayTypeVal() {
		return prePayTypeVal;
	}

	public void setPrePayTypeVal(Integer prePayTypeVal) {
		this.prePayTypeVal = prePayTypeVal;
	}


	public boolean isWalletPay() {
		return walletPay;
	}

	public void setWalletPay(boolean walletPay) {
		this.walletPay = walletPay;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	
	public String getSubNumberStr() {
		if(AppUtils.isBlank(subNumbers)){
			return "";
		}
		StringBuffer buffer=new StringBuffer();
		for(String subNumber:subNumbers){
			buffer.append(subNumber).append(",");
		}
		return buffer.substring(0, buffer.length()-1);
	}

	public void setFullPay(boolean fullPay) {
		this.fullPay = fullPay;
	}
	

	public boolean isFullPay() {
		return fullPay;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public boolean isPresell() {
		return isPresell;
	}

	public void setPresell(boolean isPresell) {
		this.isPresell = isPresell;
	}
	
	
	

}
