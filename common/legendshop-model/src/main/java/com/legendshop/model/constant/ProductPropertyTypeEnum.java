/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 产品属性类型Enum.
 */
public enum ProductPropertyTypeEnum implements IntegerEnum {
	
	/**
	 * 销售属性是组成SKU的基本元素，影响价格
	 * 管理员设定选项，用户选择
	 */
	SALE_ATTR(1),

	/**
	 * 参数属性是商品的参数，在参数列表中出现
	 * 
	 */
	PARAM_ATTR(2),
	
	/**
	 * 关键属性，出现在产品详细描述里面
	 */
	KEY_ATTR(3),
	
	;

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ProductPropertyTypeEnum(Integer num) {
		this.num = num;
	}

}
