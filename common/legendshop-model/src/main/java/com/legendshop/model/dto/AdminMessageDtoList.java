/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 后台消息实体实体列表.
 */
public class AdminMessageDtoList  implements Serializable{

	private static final long serialVersionUID = 4503049789126128241L;
	
	
	private Integer totalMessage;
	

	private List<AdminMessageDto> list;
	
	
	public Integer getTotalMessage() {
		return totalMessage;
	}

	public void setTotalMessage(Integer totalMessage) {
		this.totalMessage = totalMessage;
	}


	public List<AdminMessageDto> getList() {
		return list;
	}

	public void setList(List<AdminMessageDto> list) {
		this.list = list;
	}

	
}
