/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.form;

import java.io.Serializable;

import com.legendshop.model.constant.UserSourceEnum;
import com.legendshop.util.SafeHtml;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * 手机号码注册
 */
public class UserMobileForm implements Serializable{

	private static final long serialVersionUID = -7829853098747138878L;
	
	/** 昵称 */
	@ApiModelProperty(hidden = true)
	private String nickName;
	
	/** 头像 */
	@ApiModelProperty(hidden = true)
	private String portraitPic;

	/** 用户名. */
	@ApiModelProperty(value = "用户名")
	private String userName;

	/** 密码. */
	@ApiModelProperty(value = "密码")
	private String password;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码 ")
	private String mobile;
	
	/** 性别 */
	@ApiModelProperty(hidden = true)
	private String sex;
	
	/** 验证码 **/
	@ApiModelProperty(value = "验证码")
	private String mobileCode;
	
	/** 图片验证码**/
	@ApiModelProperty(hidden = true)
	private String randNum;
	
	/** 分销上级用户*/
	@ApiModelProperty(hidden = true)
	private String parentUserName;

	/** 微信公众号OpenId */
	@ApiModelProperty(value = "微信公众号OpenId")
	private String openId;
	
	/** 用户来源, 0: 手机号注册, 1: 微信用户, 2.QQ, 3:微博 */
	@ApiModelProperty(hidden = true)
	private int source = UserSourceEnum.PHONE.value();
		
	/** 是否已经激活 */
	@ApiModelProperty(hidden = true)
	private boolean isActivated;


	public String getParentUserName() {
		return parentUserName;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void encoding(){
		// 过滤特殊字符
		SafeHtml safeHtml = new SafeHtml();
		this.nickName = safeHtml.makeSafe(nickName);
		this.mobile = safeHtml.makeSafe(mobile);
		this.mobileCode = safeHtml.makeSafe(mobileCode);
	}

	public String getRandNum() {
		return randNum;
	}

	public void setRandNum(String randNum) {
		this.randNum = randNum;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public boolean isActivated() {
		return isActivated;
	}

	public void setActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}
	
	
}