/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import java.util.List;

/**
 * 属性值DTO
 */
public class PropValueDto {
	
	/** 属性值ID */
	private Long valueId;
	
	/** 属性值名称 */
	private String name;
	
	/** 属性值图片(显示在属性上的图) */
	private String pic;
	
	/** 属性值状态 */
	private Integer status;
	
	/** 属性值图片列表 */
	private List<String> imgs;

	
	/**
	 * @return the valueId
	 */
	public Long getValueId() {
		return valueId;
	}

	/**
	 * @param valueId the valueId to set
	 */
	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * @param pic the pic to set
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the imgs
	 */
	public List<String> getImgs() {
		return imgs;
	}

	/**
	 * @param imgs the imgs to set
	 */
	public void setImgs(List<String> imgs) {
		this.imgs = imgs;
	}
}
