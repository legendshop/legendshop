/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
/**
 * 文章
 *
 */
@Entity
@Table(name = "ls_article")
public class Article implements GenericEntity<Long> {

	private static final long serialVersionUID = -5383905782205222215L;

	/** id */
	private Long id;

	/** 文章名称 */
	private String name;

	/** 简介 */
	private String intro;

	/** 概要 */
	private String summary;

	/** 图片 */
	private String pic;
	
	/** 图片文件 */
	private MultipartFile picFile;

	/** 文章内容 */
	private String content;

	/** 文章作者 */
	private String author;

	/**
	 * 类型
	 */
	private String type;

	/** 文章评论数 */
	private Long commentsNum;

	/** 文章点赞数 */
	private Long thumbNum;

	/**
	 * 文章发表时间
	 */
	private Date issueTime;

	/** 文章创建时间 */
	private Date createTime;

	/** 文章转发数 */
	private Long forwardNum;

	/** 文章审核状态(0:下线;1:上线) */
	private Integer status;

	public Article() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ARTICLE_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "intro")
	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@Column(name = "summary")
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Transient
	public MultipartFile getPicFile() {
		return picFile;
	}

	public void setPicFile(MultipartFile picFile) {
		this.picFile = picFile;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "author")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "issue_time")
	public Date getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(Date issueTime) {
		this.issueTime = issueTime;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "comments_num")
	public Long getCommentsNum() {
		return commentsNum;
	}

	public void setCommentsNum(Long commentsNum) {
		this.commentsNum = commentsNum;
	}

	@Column(name = "thumb_num")
	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	@Column(name = "forward_num")
	public Long getForwardNum() {
		return forwardNum;
	}

	public void setForwardNum(Long forwardNum) {
		this.forwardNum = forwardNum;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

}
