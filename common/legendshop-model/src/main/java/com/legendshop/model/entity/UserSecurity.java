/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户安全配置.
 */
@Entity
@Table(name = "ls_usr_security")
public class UserSecurity implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -201911563550605464L;

	/** ID. */
	private Long secId;

	/** 用户名字. */
	private String userName;

	/** 安全级别. */
	private Integer secLevel;

	/** 是否验证邮箱. */
	private Integer mailVerifn;

	/** 是否验证手机. */
	private Integer phoneVerifn;

	/** 是否验证支付密码. */
	private Integer paypassVerifn;

	/** 短信发送时间. */
	private Date sendSmsTime;

	/** 邮件发送时间. */
	private Date sendMailTime;

	/** 建立时间. */
	private Date createTime;

	/** 短信验证码. */
	private String validateCode;

	/**
	 *  邮箱验证码
	 */
	private String emailCode;

	// 支付密码强度
	/** 支付密码强度. */
	private Integer payStrength;

	/**
	 * 将要更新到这个邮箱
	 */
	private String updateMail;

	/**
	 * 计算验证次数
	 */
	private Integer times;

	/**
	 * Instantiates a new user security.
	 */
	public UserSecurity() {
	}

	/**
	 * Gets the sec id.
	 * 
	 * @return the sec id
	 */
	@Id
	@Column(name = "sec_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USR_SECURITY_SEQ")
	public Long getSecId() {
		return secId;
	}

	/**
	 * Sets the sec id.
	 * 
	 * @param secId
	 *            the new sec id
	 */
	public void setSecId(Long secId) {
		this.secId = secId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the sec level.
	 * 
	 * @return the sec level
	 */
	@Column(name = "sec_level")
	public Integer getSecLevel() {
		return secLevel;
	}

	/**
	 * Sets the sec level.
	 * 
	 * @param secLevel
	 *            the new sec level
	 */
	public void setSecLevel(Integer secLevel) {
		this.secLevel = secLevel;
	}

	/**
	 * Gets the mail verifn.
	 * 
	 * @return the mail verifn
	 */
	@Column(name = "mail_verifn")
	public Integer getMailVerifn() {
		return mailVerifn;
	}

	/**
	 * Sets the mail verifn.
	 * 
	 * @param mailVerifn
	 *            the new mail verifn
	 */
	public void setMailVerifn(Integer mailVerifn) {
		this.mailVerifn = mailVerifn;
	}

	/**
	 * Gets the phone verifn.
	 * 
	 * @return the phone verifn
	 */
	@Column(name = "phone_verifn")
	public Integer getPhoneVerifn() {
		return phoneVerifn;
	}

	/**
	 * Sets the phone verifn.
	 * 
	 * @param phoneVerifn
	 *            the new phone verifn
	 */
	public void setPhoneVerifn(Integer phoneVerifn) {
		this.phoneVerifn = phoneVerifn;
	}

	/**
	 * Gets the paypass verifn.
	 * 
	 * @return the paypass verifn
	 */
	@Column(name = "paypass_verifn")
	public Integer getPaypassVerifn() {
		return paypassVerifn;
	}

	/**
	 * Sets the paypass verifn.
	 * 
	 * @param paypassVerifn
	 *            the new paypass verifn
	 */
	public void setPaypassVerifn(Integer paypassVerifn) {
		this.paypassVerifn = paypassVerifn;
	}

	/**
	 * Gets the send sms time.
	 * 
	 * @return the send sms time
	 */
	@Column(name = "send_sms_time")
	public Date getSendSmsTime() {
		return sendSmsTime;
	}

	/**
	 * Sets the send sms time.
	 * 
	 * @param sendSmsTime
	 *            the new send sms time
	 */
	public void setSendSmsTime(Date sendSmsTime) {
		this.sendSmsTime = sendSmsTime;
	}

	/**
	 * Gets the send mail time.
	 * 
	 * @return the send mail time
	 */
	@Column(name = "send_mail_time")
	public Date getSendMailTime() {
		return sendMailTime;
	}

	/**
	 * Sets the send mail time.
	 * 
	 * @param sendMailTime
	 *            the new send mail time
	 */
	public void setSendMailTime(Date sendMailTime) {
		this.sendMailTime = sendMailTime;
	}

	/**
	 * Gets the creates the time.
	 * 
	 * @return the creates the time
	 */
	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * Sets the creates the time.
	 * 
	 * @param createTime
	 *            the new creates the time
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * Gets the validate code.
	 * 
	 * @return the validate code
	 */
	@Column(name = "validate_code")
	public String getValidateCode() {
		return validateCode;
	}

	/**
	 * Sets the validate code.
	 * 
	 * @param validateCode
	 *            the new validate code
	 */
	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	@Transient
	public Long getId() {
		return secId;
	}

	public void setId(Long id) {
		this.secId = id;
	}

	/**
	 * Gets the pay strength.
	 * 
	 * @return the pay strength
	 */
	@Column(name = "pay_strength")
	public Integer getPayStrength() {
		return payStrength;
	}

	/**
	 * Sets the pay strength.
	 * 
	 * @param payStrength
	 *            the new pay strength
	 */
	public void setPayStrength(Integer payStrength) {
		this.payStrength = payStrength;
	}

	@Column(name = "email_code")
	public String getEmailCode() {
		return emailCode;
	}

	public void setEmailCode(String emailCode) {
		this.emailCode = emailCode;
	}

	@Column(name = "update_mail")
	public String getUpdateMail() {
		return updateMail;
	}

	public void setUpdateMail(String updateMail) {
		this.updateMail = updateMail;
	}

	@Column(name = "times")
	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

}
