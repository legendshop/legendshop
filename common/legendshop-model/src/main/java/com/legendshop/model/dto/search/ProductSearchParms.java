/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.search;

import java.util.Map;

import com.legendshop.util.AppUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 查询参数
 * 
 * @author Tony
 * 
 */
@ApiModel(value="查询参数") 
public class ProductSearchParms {

	/** 属性urlQueryString*/
	@ApiModelProperty(value="属性") 
	private String prop;

	/** 开始价格*/
	@ApiModelProperty(value="开始价格") 
	private Double startPrice;

	/** 结束价格*/
	@ApiModelProperty(value="结束价格") 
	private Double endPrice;

	/** 排序*/
	@ApiModelProperty(value="排序:默认为录入时间recDate,销量为buys,价格为cash,格式如buys,desc") 
	private String orders;
	
	/** 显示的活动类型*/
	@ApiModelProperty(value="显示的活动类型")
	private String loadType;

	/** 分类ID*/
	@ApiModelProperty(value="分类ID")
	private Long categoryId;

	/** 店铺分类ID*/
	@ApiModelProperty(value="店铺分类ID")
	private Integer shopCategoryId;

	/** 店铺ID*/
	@ApiModelProperty(value="店铺ID")
	private Integer shopId;

	private Integer page;
	
	/** 搜索关键词*/
	@ApiModelProperty(value="搜索关键词")
	private String keyword;


	/** 是否有商品 */
	@ApiModelProperty(value="是否有商品")
	private Boolean hasProd;

	/** 是否支持分销 */
	@ApiModelProperty(value="是否支持分销")
	private Boolean supportDist;

	/** 标签 */
	@ApiModelProperty(value="标签")
	private String tag;

	/** 省id */
	@ApiModelProperty(value="省id")
	private String provinceid;

	/** 市id */
	@ApiModelProperty(value="市id")
	private String cityid;

	/** 区id */
	@ApiModelProperty(value="区id")
	private String areaid;
	
	/** 每页显示条数 */
	@ApiModelProperty(value="每页显示条数")
	private Integer pageSize;
	
	/** 品牌id */
	@ApiModelProperty(value="品牌id")
	private Integer brandId;
	
	/** 品牌名称 */
	@ApiModelProperty(value="品牌名称")
	private String brandName;
	
	/** 请求参数Map集合 */
	@ApiModelProperty(value="请求参数Map集合")
	private Map<String, String[]> requestParams;
	
	
	public Double getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Double startPrice) {
		this.startPrice = startPrice;
	}

	public Double getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(Double endPrice) {
		this.endPrice = endPrice;
	}

	public String getOrders() {
		return orders;
	}

	public void setOrders(String orders) {
		this.orders = orders;
	}
	
	
	public String getLoadType() {
		return loadType;
	}

	public void setLoadType(String loadType) {
		this.loadType = loadType;
	}

	public void appendParms(AttrParam param) {
		if (param == null) {
			return;
		}
		if (AppUtils.isBlank(prop)) {
			prop = param.getQueryString();
		} else {
			prop = prop + ";" + param.getQueryString();
		}
	}

	

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}



	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getShopCategoryId() {
		return shopCategoryId;
	}

	public void setShopCategoryId(Integer shopCategoryId) {
		this.shopCategoryId = shopCategoryId;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}


	public String getProvinceid() {
		return provinceid;
	}

	public void setProvinceid(String provinceid) {
		this.provinceid = provinceid;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getAreaid() {
		return areaid;
	}

	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}

	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}


	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Map<String, String[]> getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(Map<String, String[]> requestParams) {
		this.requestParams = requestParams;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Boolean getHasProd() {
		return hasProd;
	}

	public void setHasProd(Boolean hasProd) {
		this.hasProd = hasProd;
	}

	public Boolean getSupportDist() {
		return supportDist;
	}

	public void setSupportDist(Boolean supportDist) {
		this.supportDist = supportDist;
	}
}
