package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.Product;

/** 
 * 设计师列表DTO
 * @author 开发很忙
 */
public class ShopSeekDto implements Serializable{

	private static final long serialVersionUID = 3375476897495832853L;

	/** 商家ID */
	private Long shopId;
	
	/** 商家名称 */
	private String shopName;
	
	/** 商家图片 */
	private String shopPic;
	
	/** 店铺商品列表 */
	private List<Product> prodList;
	
	/** 店铺收藏数 */
	private Integer collects;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopPic() {
		return shopPic;
	}

	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	public List<Product> getProdList() {
		return prodList;
	}

	public void setProdList(List<Product> prodList) {
		this.prodList = prodList;
	}

	public Integer getCollects() {
		return collects;
	}

	public void setCollects(Integer collects) {
		this.collects = collects;
	}
}
