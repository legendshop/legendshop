package com.legendshop.model.dto.coin;

import java.io.Serializable;
import java.util.Date;

/**
 * 金币消费详情
 *
 */
public class CoinConsumeDto implements Serializable{

	private static final long serialVersionUID = 7503312741676204390L;
	
	/** 用户id */
	private String userId;
	
	/** 用户名称 */
	private String userName;
	
	/** 手机 */
	private String mobile;
	
	/** 昵称 */
	private String nickName;
	
	/** 变更金额 */
	private Double amount;
	
	/** 日志类型   recharge:充值     withdraw:提现 */
	private String logType;
	
	/** 日志详情 */
	private String details;
	
	/** 添加日期 */
	private Date addTime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
}




