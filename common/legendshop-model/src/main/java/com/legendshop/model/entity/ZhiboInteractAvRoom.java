/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 直播房间表.
 */
@Entity
@Table(name = "ls_zhibo_interact_av_room")
public class ZhiboInteractAvRoom implements GenericEntity<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3909049639242274182L;

	/**  成员id. */
	private String uid; 
		
	/**  成员所在房间ID. */
	private Long avRoomId; 
		
	/**  成员在房间的麦状态. */
	private String status; 
		
	/**  成员心跳时间戳. */
	private Long modifyTime; 
		
	/**  成员角色. */
	private Long role; 
		
	
	/**
	 * Instantiates a new zhibo interact av room.
	 */
	public ZhiboInteractAvRoom() {
    }
		
	/**
	 * Gets the uid.
	 *
	 * @return the uid
	 */
	@Id
	@Column(name = "uid")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ZHIBO_INTERACT_AV_ROOM_SEQ")
	public String  getUid(){
		return uid;
	} 
		
	/**
	 * Sets the uid.
	 *
	 * @param uid the new uid
	 */
	public void setUid(String uid){
			this.uid = uid;
		}
		
    /**
     * Gets the av room id.
     *
     * @return the av room id
     */
    @Column(name = "av_room_id")
	public Long  getAvRoomId(){
		return avRoomId;
	} 
		
	/**
	 * Sets the av room id.
	 *
	 * @param avRoomId the new av room id
	 */
	public void setAvRoomId(Long avRoomId){
			this.avRoomId = avRoomId;
		}
		
    /**
     * Gets the status.
     *
     * @return the status
     */
    @Column(name = "status")
	public String  getStatus(){
		return status;
	} 
		
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status){
			this.status = status;
		}
		
    /**
     * Gets the modify time.
     *
     * @return the modify time
     */
    @Column(name = "modify_time")
	public Long  getModifyTime(){
		return modifyTime;
	} 
		
	/**
	 * Sets the modify time.
	 *
	 * @param modifyTime the new modify time
	 */
	public void setModifyTime(Long modifyTime){
			this.modifyTime = modifyTime;
		}
		
    /**
     * Gets the role.
     *
     * @return the role
     */
    @Column(name = "role")
	public Long  getRole(){
		return role;
	} 
		
	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(Long role){
			this.role = role;
		}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public String getId() {
		return uid;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(String id) {
		uid = id;
	}


} 
