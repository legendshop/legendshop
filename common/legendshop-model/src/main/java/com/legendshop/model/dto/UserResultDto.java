package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.legendshop.model.entity.User;


/**
 * 保存用户的结果Dto.
 * 
 */
public final class UserResultDto implements Serializable {

	private static final long serialVersionUID = 8763238175607789763L;

	private int statusCode = 1;// 返回状态， 默认为成功

	private User user; // 用户

	private Map<String, Object> attributes; // 属性

	public int getStatusCode() {
		return statusCode;
	}

	public boolean success() {
		return statusCode == 1;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setAttribute(String key, Object value) {
		if (attributes == null) {
			attributes = new HashMap<String, Object>();
		}
		attributes.put(key, value);
	}
	
	public Object getAttribute(String key) {
		return attributes.get(key);
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

}
