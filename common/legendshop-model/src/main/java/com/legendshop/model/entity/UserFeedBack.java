/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;


/**
 * 用户反馈.
 */
@Entity
@Table(name = "ls_usr_feedback")
public class UserFeedBack implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 595022123989103353L;

	/** ID. */
	private Long id; 
		
	/** 用户ID. */
	private String userId; 
		
	/** 反馈人名字. */
	private String name; 
		
	/** 联系方式. */
	private String contactInformation; 
		
	/** 发起反馈的IP地址. */
	private String ip; 
		
	/** 内容. */
	private String content; 
		
	/** 记录时间. */
	private Date recDate; 
		
	/** 是否已经阅读. */
	private Integer status; 
		
	/** 回复的管理员ID. */
	private String respMgntId; 
		
	/** 处理意见. */
	private String respContent; 
		
	/** 处理时间. */
	private Date respDate; 
	
	/**反馈来源(1:PC 2:android 3:wap 4:IOS)**/
	private Integer feedBackSource; 
	
	/**
	 * Instantiates a new user feed back.
	 */
	public UserFeedBack() {
    }
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USR_FEEDBACK_SEQ")
	public Long  getId(){
		return id;
	} 
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id){
			this.id = id;
		}
		
    /**
     * Gets the user id.
     *
     * @return the user id
     */
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    /**
     * Gets the name.
     *
     * @return the name
     */
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name){
			this.name = name;
		}
		
	 /**
 	 * Gets the contact information.
 	 *
 	 * @return the contact information
 	 */
 	@Column(name = "contact_information")
    public String getContactInformation() {
		return contactInformation;
	}

	/**
	 * Sets the contact information.
	 *
	 * @param contactInformation the new contact information
	 */
	public void setContactInformation(String contactInformation) {
		this.contactInformation = contactInformation;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	@Column(name = "ip")
	public String  getIp(){
		return ip;
	} 
		
	/**
	 * Sets the ip.
	 *
	 * @param ip the new ip
	 */
	public void setIp(String ip){
			this.ip = ip;
		}
		
    /**
     * Gets the content.
     *
     * @return the content
     */
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(String content){
			this.content = content;
		}
		
    /**
     * Gets the rec date.
     *
     * @return the rec date
     */
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
		
    /**
     * Gets the status.
     *
     * @return the status
     */
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status){
			this.status = status;
		}
		
    /**
     * Gets the resp mgnt id.
     *
     * @return the resp mgnt id
     */
    @Column(name = "resp_mgnt_id")
	public String  getRespMgntId(){
		return respMgntId;
	} 
		
	/**
	 * Sets the resp mgnt id.
	 *
	 * @param respMgntId the new resp mgnt id
	 */
	public void setRespMgntId(String respMgntId){
			this.respMgntId = respMgntId;
		}
		
    /**
     * Gets the resp content.
     *
     * @return the resp content
     */
    @Column(name = "resp_content")
	public String  getRespContent(){
		return respContent;
	} 
		
	/**
	 * Sets the resp content.
	 *
	 * @param respContent the new resp content
	 */
	public void setRespContent(String respContent){
			this.respContent = respContent;
		}
		
    /**
     * Gets the resp date.
     *
     * @return the resp date
     */
    @Column(name = "resp_date")
	public Date  getRespDate(){
		return respDate;
	} 
		
	/**
	 * Sets the resp date.
	 *
	 * @param respDate the new resp date
	 */
	public void setRespDate(Date respDate){
			this.respDate = respDate;
		}

	
	@Column(name = "feedback_source")
	public Integer getFeedBackSource() {
		return feedBackSource;
	}

	public void setFeedBackSource(Integer feedBackSource) {
		this.feedBackSource = feedBackSource;
	}
	

    
} 
