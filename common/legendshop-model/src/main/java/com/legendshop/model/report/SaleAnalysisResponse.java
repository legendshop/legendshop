package com.legendshop.model.report;

import java.io.Serializable;

public class SaleAnalysisResponse implements Serializable {

	private static final long serialVersionUID = 6001669216498586656L;

	// 总会员数
	private long totalMember;
	
	//总订单数
	private long totalOrder;
	
	// 总访问次数 
	private long totalViews;
	
	// 总订单金额 
	private Double totalAmount;
	
	// 有过订单的会员数
	private long hasOrder;

	public long getTotalMember() {
		return totalMember;
	}

	public void setTotalMember(long totalMember) {
		this.totalMember = totalMember;
	}

	public long getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(long totalOrder) {
		this.totalOrder = totalOrder;
	}

	public long getTotalViews() {
		return totalViews;
	}

	public void setTotalViews(long totalViews) {
		this.totalViews = totalViews;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public long getHasOrder() {
		return hasOrder;
	}

	public void setHasOrder(long hasOrder) {
		this.hasOrder = hasOrder;
	}
}
