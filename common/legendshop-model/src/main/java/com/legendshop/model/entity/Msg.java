/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * The Class Msg.
 */
@Entity
@Table(name = "ls_msg")
public class Msg implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5931180329889191349L;

	/** The msg id. */
	private Long msgId;
	
	/** The send name. */
	private String sendName;
	
	/** The receive name. */
	private String receiveName;
	
	/** The text id. */
	private Long textId;
	
	/** The status. */
	private int status;
	
	/** The user level. */
	private String userLevel;
	
	/** The is global. */
	private int isGlobal;
	
	/** The delete status. */
	private int deleteStatus;
	
	/** The delete status2. */
	private int deleteStatus2;
	
	
	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return msgId;
	}
	
	public void setId(Long id) {
		this.msgId = id;
	}


	/**
	 * Gets the msg id.
	 *
	 * @return the msg id
	 */
	@Id
	@Column(name = "msg_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MSG_SEQ")
	public Long getMsgId() {
		return msgId;
	}


	/**
	 * Sets the msg id.
	 *
	 * @param msgId the new msg id
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}


	/**
	 * Gets the send name.
	 *
	 * @return the send name
	 */
	@Column(name = "send_name")
	public String getSendName() {
		return sendName;
	}


	/**
	 * Sets the send name.
	 *
	 * @param sendName the new send name
	 */
	public void setSendName(String sendName) {
		this.sendName = sendName;
	}


	/**
	 * Gets the receive name.
	 *
	 * @return the receive name
	 */
	@Column(name = "receive_name")
	public String getReceiveName() {
		return receiveName;
	}


	/**
	 * Sets the receive name.
	 *
	 * @param receiveName the new receive name
	 */
	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}


	/**
	 * Gets the text id.
	 *
	 * @return the text id
	 */
	@Column(name = "text_id")
	public Long getTextId() {
		return textId;
	}


	/**
	 * Sets the text id.
	 *
	 * @param textId the new text id
	 */
	public void setTextId(Long textId) {
		this.textId = textId;
	}


	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the checks if is global.
	 *
	 * @return the checks if is global
	 */
	@Column(name = "is_global")
	public Integer getIsGlobal() {
		return isGlobal;
	}


	/**
	 * Sets the checks if is global.
	 *
	 * @param isGlobal the new checks if is global
	 */
	public void setIsGlobal(Integer isGlobal) {
		this.isGlobal = isGlobal;
	}


	/**
	 * Gets the delete status.
	 *
	 * @return the delete status
	 */
	@Column(name = "delete_status")
	public Integer getDeleteStatus() {
		return deleteStatus;
	}


	/**
	 * Sets the delete status.
	 *
	 * @param deleteStatus the new delete status
	 */
	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}



	/**
	 * Gets the user level.
	 *
	 * @return the user level
	 */
	@Column(name = "user_level")
	public String getUserLevel() {
		return userLevel;
	}


	/**
	 * Sets the user level.
	 *
	 * @param userLevel the new user level
	 */
	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}


	/**
	 * Gets the delete status2.
	 *
	 * @return the delete status2
	 */
	@Column(name = "delete_status2")
	public Integer getDeleteStatus2() {
		return deleteStatus2;
	}


	/**
	 * Sets the delete status2.
	 *
	 * @param deleteStatus2 the new delete status2
	 */
	public void setDeleteStatus2(Integer deleteStatus2) {
		this.deleteStatus2 = deleteStatus2;
	}

}
