package com.legendshop.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.entity.PriceRange;

public class PriceRangeDto {
	
	List<PriceRange> priceRangeList = new ArrayList<PriceRange>();

	public List<PriceRange> getPriceRangeList() {
		return priceRangeList;
	}

	public void setPriceRangeList(List<PriceRange> priceRangeList) {
		this.priceRangeList = priceRangeList;
	}

	
}
