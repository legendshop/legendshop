package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

public enum MarketProdStatusEnum implements IntegerEnum{
	
	/*** 上线 */
	MARKETPROD_STATUS_UP(1),
	/*** 下线*/
	MARKETPROD_STATUS_DOWN(0)
	;
	
	private Integer status;
	
	MarketProdStatusEnum(Integer value){
		this.status = value;
	}
	@Override
	public Integer value() {
		return status;
	}


}
