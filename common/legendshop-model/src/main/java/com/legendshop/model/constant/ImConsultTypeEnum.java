/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 *  IM咨询类型
 */
public enum ImConsultTypeEnum implements StringEnum {
	
	/** 商品咨询 */
	PROD("prod"), 
	
	/** 订单咨询 */
	ORDER("order"),
	
	/** 退款咨询 */
	REFUND("refund"),
	
	/** 其他咨询 */
	OTHER("other")
	;
	
	/** The value. */
	private final String value;

	private ImConsultTypeEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
	
    public static boolean inCartTypeEnum(String value) {
        for (ImConsultTypeEnum imConsultTypeEnum : ImConsultTypeEnum.values()) {
            if (imConsultTypeEnum.value.equals(value)) {
                return true;
            }
        }
        return false;
    }

}
