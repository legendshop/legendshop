/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品咨询
 */
@Entity
@Table(name = "ls_prod_cons")
public class ProductConsult  implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2645969133712434418L;

	/** The cons id. */
	private Long consId;
	
	/**
	 * 咨询类型
	 * 1: 商品咨询
	 * 2:库存配送
	 * 3:售后咨询
	 */
	private Integer pointType;
	
	/** The prod id. */
	private Long prodId;
	
	/** The prod name. */
	private String prodName;
	
	private String pic;

	/** The user id. */
	private String userId;

	/** The user name. */
	private String userName;

	/** The content. */
	private String content;

	/** The answer. */
	private String answer;

	/** The rec date. */
	private Date recDate;

	/** The postip. */
	private String postip;

	/** The answertime. */
	private Date answertime;
	
	/** The ask user name. */
	private String askUserName;
	
	/** The ans user name. */
	private String ansUserName;
	
	/** The start time. */
	private Date startTime;
	
	/** The end date. */
	private Date endTime;
	
	//用户等级名称
	/** The grade name. */
	private String gradeName;
	
	/** 评论的用户头像. */
	private String portraitPic;
	
	/** 评论的用户昵称. */
	private String nickName;
	
	/**所属商城名称*/
	private String siteName;
	
	/**所属商城id*/
	private Long shopId;

	/**回复状态：0 未回复 1 已回复*/
	private Integer replySts;
	
	/**商家删除标记：0：保留 1：删除*/
	private Integer delSts;
	
	/**
	 * Instantiates a new product consult.
	 */
	public ProductConsult() {
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Transient
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Transient
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	@Column(name = "content")
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the answer.
	 * 
	 * @return the answer
	 */
	@Column(name = "answer")
	public String getAnswer() {
		return answer;
	}

	/**
	 * Sets the answer.
	 * 
	 * @param answer
	 *            the new answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * Gets the rec date.
	 * 
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 * 
	 * @param recDate
	 *            the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the postip.
	 * 
	 * @return the postip
	 */
	@Column(name = "postip")
	public String getPostip() {
		return postip;
	}

	/**
	 * Sets the postip.
	 * 
	 * @param postip
	 *            the new postip
	 */
	public void setPostip(String postip) {
		this.postip = postip;
	}

	/**
	 * Gets the answertime.
	 * 
	 * @return the answertime
	 */
	@Column(name = "answertime")
	public Date getAnswertime() {
		return answertime;
	}

	/**
	 * Sets the answertime.
	 * 
	 * @param answertime
	 *            the new answertime
	 */
	public void setAnswertime(Date answertime) {
		this.answertime = answertime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return consId;
	}

	
	public void setId(Long id) {
		this.consId = id;
	}

	/**
	 * Gets the prod id.
	 *
	 * @return the prodId
	 */
	@Column(name = "prod_id")
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the prodId to set
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the ask user name.
	 *
	 * @return the askUserName
	 */
	@Column(name = "ask_user_name")
	public String getAskUserName() {
		return askUserName;
	}

	/**
	 * Sets the ask user name.
	 *
	 * @param askUserName the askUserName to set
	 */
	public void setAskUserName(String askUserName) {
		this.askUserName = askUserName;
	}

	/**
	 * Gets the ans user name.
	 *
	 * @return the ansUserName
	 */
	@Column(name = "ans_user_name")
	public String getAnsUserName() {
		return ansUserName;
	}

	/**
	 * Sets the ans user name.
	 *
	 * @param ansUserName the ansUserName to set
	 */
	public void setAnsUserName(String ansUserName) {
		this.ansUserName = ansUserName;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prodName
	 */
	@Transient
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the startTime
	 */
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the endTime
	 */
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the cons id.
	 *
	 * @return the consId
	 */
	@Id
	@Column(name = "cons_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_CONS_SEQ")
	public Long getConsId() {
		return consId;
	}

	/**
	 * Sets the cons id.
	 *
	 * @param consId the consId to set
	 */
	public void setConsId(Long consId) {
		this.consId = consId;
	}

	/**
	 * Gets the grade name.
	 *
	 * @return the gradeName
	 */
	@Transient
	public String getGradeName() {
		return gradeName;
	}

	/**
	 * Sets the grade name.
	 *
	 * @param gradeName the gradeName to set
	 */
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	/**
	 * Gets the point type.
	 *
	 * @return the pointType
	 */
	@Column(name = "point_type")
	public Integer getPointType() {
		return pointType;
	}

	/**
	 * Sets the point type.
	 *
	 * @param pointType the pointType to set
	 */
	public void setPointType(Integer pointType) {
		this.pointType = pointType;
	}

	@Transient
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Transient
	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Transient
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "reply_sts")
	public Integer getReplySts() {
		return replySts;
	}

	public void setReplySts(Integer replySts) {
		this.replySts = replySts;
	}

	@Column(name = "del_sts")
	public Integer getDelSts() {
		return delSts;
	}

	public void setDelSts(Integer delSts) {
		this.delSts = delSts;
	}
	
}
