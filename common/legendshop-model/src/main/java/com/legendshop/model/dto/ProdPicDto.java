package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="商品图片Dto") 
public class ProdPicDto implements Serializable{
	
	private static final long serialVersionUID = -5932906048458899140L;
	
	/**
	 * 图片路径
	 */
	@ApiModelProperty(value="图片路径") 
	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
