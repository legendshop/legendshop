package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 拼团订单DTO，用于支付回调判断是否拼团订单 
 */
public class MergeGroupSubDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 订单ID */
	private Long subId;
	
	/** 订单编号*/
	private String subNumber;
	
	/** 商品名称 */
	private String prodName;
	
	/** 订单类型 */
	private String subType;
	
	/** 团运营ID */
	private Long operateId;
	
	/** 团编号*/
	private String addNumber;
	
	/** 状态[1:进行中,2:成功,3:失败] */
	private Integer status;
	
	/** 团创建时间 */
	private Date createTime;

	/** 是否团长，默认不是 */
	private Boolean isHead;
	
	/** 拼团成功数 */
	private Integer count;
	
	/** 商品拼团价 */
	private Double mergePrice;
	
	/** sku属性*/
	private String cnProperties;
	
	/** 成团人数 */
	private Integer peopleNumber;
	
	/** 已参团人数 */
	private Integer number;
	
	/** 活动ID */
	private Long mergeId;
	
	/**活动结束时间*/
	private Date endTime;
	
	/** 商品SKU图片 */
	private String pic;
	
	/** 拼团用户的头像集合*/
	List<String> pics;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Boolean getIsHead() {
		return isHead;
	}

	public void setIsHead(Boolean isHead) {
		this.isHead = isHead;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Double getMergePrice() {
		return mergePrice;
	}

	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public List<String> getPics() {
		return pics;
	}

	public void setPics(List<String> pics) {
		this.pics = pics;
	}

	public Long getMergeId() {
		return mergeId;
	}

	public void setMergeId(Long mergeId) {
		this.mergeId = mergeId;
	}

	public Long getOperateId() {
		return operateId;
	}

	public void setOperateId(Long operateId) {
		this.operateId = operateId;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
