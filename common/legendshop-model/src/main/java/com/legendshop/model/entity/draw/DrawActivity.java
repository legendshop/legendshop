package com.legendshop.model.entity.draw;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.DrawAwardsDto;

/**
 *抽奖活动主表
 */
@Entity
@Table(name = "ls_draw_activity")
public class DrawActivity implements GenericEntity<Long> {

	private static final long serialVersionUID = -2901917234072275382L;

	/** id */
	private Long id; 
		
	/** 活动名称 */
	private String actName; 
		
	/** 活动类型 */
	private String actType; 
		
	/** 活动描述 */
	private String actDes; 
		
	/** 活动状态  0：下线 1：上线 */
	private Integer actStatus; 
		
	/** 中奖概率20/100 */
	private String winPencent; 
		
	/** 开始时间 */
	private Date startTime; 
		
	/** 结束时间 */
	private Date endTime; 
		
	/** 每日抽奖次数 */
	private Long timesPerday; 
		
	/** 个人抽奖总次数 */
	private Long timesPerson; 
		
	/** 是否中奖可参与  0: 否  1：是*/
	private Integer winJoinStatus; 
		
	/** 是否绑定手机用户可参与  0: 否  1：是*/
	private Integer bindJoinStatus; 
	
	/** 活动规则 */
	private String actRule; 
	
	/** 奖项详细信息json  具体请查看DrawAwardsDto */
	private String awardInfo;
	
	private List<DrawAwardsDto> drawAwardsDtoList;
	public DrawActivity() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DRAW_ACTIVITY_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "act_name")
	public String  getActName(){
		return actName;
	} 
		
	public void setActName(String actName){
			this.actName = actName;
		}
		
    @Column(name = "act_type")
	public String  getActType(){
		return actType;
	} 
		
	public void setActType(String actType){
			this.actType = actType;
		}
		
    @Column(name = "act_des")
	public String  getActDes(){
		return actDes;
	} 
		
	public void setActDes(String actDes){
			this.actDes = actDes;
		}
		
    @Column(name = "act_status")
	public Integer  getActStatus(){
		return actStatus;
	} 
		
	public void setActStatus(Integer actStatus){
			this.actStatus = actStatus;
		}
		
    @Column(name = "win_pencent")
	public String  getWinPencent(){
		return winPencent;
	} 
		
	public void setWinPencent(String winPencent){
			this.winPencent = winPencent;
		}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}
		
    @Column(name = "times_perday")
	public Long  getTimesPerday(){
		return timesPerday;
	} 
		
	public void setTimesPerday(Long timesPerday){
			this.timesPerday = timesPerday;
		}
		
    @Column(name = "times_person")
	public Long  getTimesPerson(){
		return timesPerson;
	} 
		
	public void setTimesPerson(Long timesPerson){
			this.timesPerson = timesPerson;
		}
		
    @Column(name = "win_join_status")
	public Integer  getWinJoinStatus(){
		return winJoinStatus;
	} 
		
	public void setWinJoinStatus(Integer winJoinStatus){
			this.winJoinStatus = winJoinStatus;
		}
		
    @Column(name = "bind_join_status")
	public Integer  getBindJoinStatus(){
		return bindJoinStatus;
	} 
		
	public void setBindJoinStatus(Integer bindJoinStatus){
			this.bindJoinStatus = bindJoinStatus;
		}
		
	@Column(name = "act_rule")
	public String  getActRule(){
		return actRule;
	} 
		
	public void setActRule(String actRule){
			this.actRule = actRule;
		}

	@Column(name = "awards_info")
	public String getAwardInfo() {
		return awardInfo;
	}
	
	public void setAwardInfo(String awardInfo) {
		this.awardInfo = awardInfo;
	}
	
	
	@Transient
	public List<DrawAwardsDto> getDrawAwardsDtoList() {
		return drawAwardsDtoList;
	}

	public void setDrawAwardsDtoList(List<DrawAwardsDto> drawAwardsDtoList) {
		this.drawAwardsDtoList = drawAwardsDtoList;
	}


} 
