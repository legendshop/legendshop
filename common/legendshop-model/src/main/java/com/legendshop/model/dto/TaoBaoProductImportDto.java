/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.util.Date;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;


/**
 * @Description 
 * @author 关开发
 */

public class TaoBaoProductImportDto {
	public TaoBaoProductImportDto() {}
	/** 宝贝名称 */
	@CsvBindByPosition(position = 0)
	private String title;
	
	/** 宝贝类目 */
	@CsvBindByPosition(position = 1)
	private String cid;
	
	/** 店铺类目 */
	@CsvBindByPosition(position = 2)
	private String sellerCid;
	
	/** 新旧程度 */
	@CsvBindByPosition(position = 3)
	private String stuffStatus;
	
	/** 省 */
	@CsvBindByPosition(position = 4)
	private String locationState;
	
	/** 城市 */
	@CsvBindByPosition(position = 5)
	private String locationCity;
	
	/** 出售方式 */
	@CsvBindByPosition(position = 6)
	private String itemType;
	
	/** 宝贝价格 */
	@CsvBindByPosition(position = 7)
	private String price;
	
	/** 加价幅度 */
	@CsvBindByPosition(position = 8)
	private String auctionIncrement;
	
	/** 宝贝数量 */
	@CsvBindByPosition(position = 9)	
	private String num;
	
	/** 有效期 */
	@CsvBindByPosition(position = 10)
	private String validThru;
	
	/** 运费承担 */
	@CsvBindByPosition(position = 11)
	private String freightPayer;
	
	/** 平邮运费 */
	@CsvBindByPosition(position = 12)
	private String postFee;
	
	/** EMS运费 */
	@CsvBindByPosition(position = 13)
	private String emsFee;
	
	/** 快递运费 */
	@CsvBindByPosition(position = 14)
	private String expressFee;
	
	/** 是否有发票 */
	@CsvBindByPosition(position = 15)
	private String hasInvoice;
	
	/** 是否保修 */
	@CsvBindByPosition(position = 16)
	private String hasWarranty;
	
	/** 放入仓库 */
	@CsvBindByPosition(position = 17)
	private String approveStatus;
	
	/** 橱窗推荐 */
	@CsvBindByPosition(position = 18)
	private String hasShowcase;
	
	/** 开始时间 */
	@CsvBindByPosition(position = 19)
	@CsvDate(value="yyyy-MM-dd HH:mm:ss")
	private Date listTime;
	
	/** 宝贝描述 */
	@CsvBindByPosition(position = 20)
	private String description;
	
	/** 宝贝属性 */
	@CsvBindByPosition(position = 21)
	private String cateProps;
	
	/** 邮件模板ID */
	@CsvBindByPosition(position = 22)
	private String postageId;
	
	/** 会员打折 */
	@CsvBindByPosition(position = 23)
	private String hasDiscount;
	
	/** 修改时间 */
	@CsvBindByPosition(position = 24)
	@CsvDate(value="yyyy-MM-dd HH:mm:ss")
	private Date mofified;
	
	/** 上传状态 */
	@CsvBindByPosition(position = 25)
	private String uploadFailMsg;
	
	/** 图片状态 */
	@CsvBindByPosition(position = 26)
	private String pictureStatus;
	
	/** 返点比例 */
	@CsvBindByPosition(position = 27)
	private String auctionPoint;
	
	/** 新图片 */
	@CsvBindByPosition(position = 28)
	private String picture;
	
	/** 视频 */
	@CsvBindByPosition(position = 29)
	private String video;
	
	/** 销售属性组合 */
	@CsvBindByPosition(position = 30)
	private String skuProps;
	
	/** 用户输入ID号 */
	@CsvBindByPosition(position = 31)
	private String inputPids;
	
	/** 用户输入名-键值对 */
	@CsvBindByPosition(position = 32)
	private String inputValues;
	
	/** 商家编码 */
	@CsvBindByPosition(position = 33)
	private String outerId;
	
	/** 销售属性别名 */
	@CsvBindByPosition(position = 34)
	private String propAlias;
	
	/** 代充类型 */
	@CsvBindByPosition(position = 35)
	private String autoFill;
	
	/** 数字ID */
	@CsvBindByPosition(position = 36)
	private String numId;
	
	/** 本地ID */
	@CsvBindByPosition(position = 37)
	private String localCid;
	
	/** 宝贝分类 */
	@CsvBindByPosition(position = 39)
	private String navigationType;
	
	/** 用户名称 */
	@CsvBindByPosition(position = 40)
	private String userName;
	
	/** 宝贝状态 */
	@CsvBindByPosition(position = 41)
	private String syncStatus;
	
	/** 闪电发货 */
	@CsvBindByPosition(position = 42)
	private String isLightingConsigment;
	
	/** 新品 */
	@CsvBindByPosition(position = 43)
	private String isXinPin;
	
	/** 食品专项 */
	@CsvBindByPosition(position = 44)
	private String foodparame;
	
	/** 尺码库 */
	@CsvBindByPosition(position = 45)
	private String features;
	
	/** 采购地 */
	@CsvBindByPosition(position = 46)
	private String buyareaType;
	
	/** 库存类型 */
	@CsvBindByPosition(position = 47)
	private String globalStockType;
	
	/** 国家地区 */
	@CsvBindByPosition(position = 48)
	private String globalStockCountry;
	
	/** 库存计数 */
	@CsvBindByPosition(position = 49)
	private String subStockType;
	
	/** 物流体积 */
	@CsvBindByPosition(position = 50)
	private String itemSize;
	
	/** 物流重量 */
	@CsvBindByPosition(position = 51)
	private String itemWeight;
	
	/** 退换货承诺 */
	@CsvBindByPosition(position = 52)
	private String sellPromise;
	
	/** 定制工具 */
	@CsvBindByPosition(position = 53)
	private String customDesignFlag;
	
	/** 无线详情 */
	@CsvBindByPosition(position = 54)
	private String wirelessDesc;
	
	/** 商品条形码 */
	@CsvBindByPosition(position = 55)
	private String barcode;
	
	/** sku条形码 */
	@CsvBindByPosition(position = 56)
	private String skuBarcode;
	
	/** 7天退换货 */
	@CsvBindByPosition(position = 57)
	private String newprepay;
	
	/** 宝贝卖点 */
	@CsvBindByPosition(position = 58)
	private String subtitle;
	
	/** 属性值备注 */
	@CsvBindByPosition(position = 59)
	private String cpvMemo;
	
	/** 自定义属性值 */
	@CsvBindByPosition(position = 60)
	private String inputCustomCpv;
	
	/** 商品资质 */
	@CsvBindByPosition(position = 61)
	private String qualification;
	
	/** 增加商品资质 */
	@CsvBindByPosition(position = 62)
	private String addQualification;
	
	/** 关联线下服务 */
	@CsvBindByPosition(position = 63)
	private String o2oBindService;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the cid
	 */
	public String getCid() {
		return cid;
	}

	/**
	 * @param cid the cid to set
	 */
	public void setCid(String cid) {
		this.cid = cid;
	}

	/**
	 * @return the sellerCid
	 */
	public String getSellerCid() {
		return sellerCid;
	}

	/**
	 * @param sellerCid the sellerCid to set
	 */
	public void setSellerCid(String sellerCid) {
		this.sellerCid = sellerCid;
	}

	/**
	 * @return the stuffStatus
	 */
	public String getStuffStatus() {
		return stuffStatus;
	}

	/**
	 * @param stuffStatus the stuffStatus to set
	 */
	public void setStuffStatus(String stuffStatus) {
		this.stuffStatus = stuffStatus;
	}

	/**
	 * @return the locationState
	 */
	public String getLocationState() {
		return locationState;
	}

	/**
	 * @param locationState the locationState to set
	 */
	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	/**
	 * @return the locationCity
	 */
	public String getLocationCity() {
		return locationCity;
	}

	/**
	 * @param locationCity the locationCity to set
	 */
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	/**
	 * @return the itemType
	 */
	public String getItemType() {
		return itemType;
	}

	/**
	 * @param itemType the itemType to set
	 */
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the auctionIncrement
	 */
	public String getAuctionIncrement() {
		return auctionIncrement;
	}

	/**
	 * @param auctionIncrement the auctionIncrement to set
	 */
	public void setAuctionIncrement(String auctionIncrement) {
		this.auctionIncrement = auctionIncrement;
	}

	/**
	 * @return the num
	 */
	public String getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(String num) {
		this.num = num;
	}

	/**
	 * @return the vaildThru
	 */
	public String getValidThru() {
		return validThru;
	}

	/**
	 * @param vaildThru the vaildThru to set
	 */
	public void setVaildThru(String validThru) {
		this.validThru = validThru;
	}

	/**
	 * @return the freightPayer
	 */
	public String getFreightPayer() {
		return freightPayer;
	}

	/**
	 * @param freightPayer the freightPayer to set
	 */
	public void setFreightPayer(String freightPayer) {
		this.freightPayer = freightPayer;
	}

	/**
	 * @return the postFee
	 */
	public String getPostFee() {
		return postFee;
	}

	/**
	 * @param postFee the postFee to set
	 */
	public void setPostFee(String postFee) {
		this.postFee = postFee;
	}

	/**
	 * @return the emsFee
	 */
	public String getEmsFee() {
		return emsFee;
	}

	/**
	 * @param emsFee the emsFee to set
	 */
	public void setEmsFee(String emsFee) {
		this.emsFee = emsFee;
	}

	/**
	 * @return the expressFee
	 */
	public String getExpressFee() {
		return expressFee;
	}

	/**
	 * @param expressFee the expressFee to set
	 */
	public void setExpressFee(String expressFee) {
		this.expressFee = expressFee;
	}

	/**
	 * @return the hasInvoice
	 */
	public String getHasInvoice() {
		return hasInvoice;
	}

	/**
	 * @param hasInvoice the hasInvoice to set
	 */
	public void setHasInvoice(String hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	/**
	 * @return the hasWarranty
	 */
	public String getHasWarranty() {
		return hasWarranty;
	}

	/**
	 * @param hasWarranty the hasWarranty to set
	 */
	public void setHasWarranty(String hasWarranty) {
		this.hasWarranty = hasWarranty;
	}

	/**
	 * @return the approveStatus
	 */
	public String getApproveStatus() {
		return approveStatus;
	}

	/**
	 * @param approveStatus the approveStatus to set
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return the hasShowcase
	 */
	public String getHasShowcase() {
		return hasShowcase;
	}

	/**
	 * @param hasShowcase the hasShowcase to set
	 */
	public void setHasShowcase(String hasShowcase) {
		this.hasShowcase = hasShowcase;
	}

	/**
	 * @return the listTime
	 */
	public Date getListTime() {
		return listTime;
	}

	/**
	 * @param listTime the listTime to set
	 */
	public void setListTime(Date listTime) {
		this.listTime = listTime;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the cateProps
	 */
	public String getCateProps() {
		return cateProps;
	}

	/**
	 * @param cateProps the cateProps to set
	 */
	public void setCateProps(String cateProps) {
		this.cateProps = cateProps;
	}

	/**
	 * @return the postageId
	 */
	public String getPostageId() {
		return postageId;
	}

	/**
	 * @param postageId the postageId to set
	 */
	public void setPostageId(String postageId) {
		this.postageId = postageId;
	}

	/**
	 * @return the hasDiscount
	 */
	public String getHasDiscount() {
		return hasDiscount;
	}

	/**
	 * @param hasDiscount the hasDiscount to set
	 */
	public void setHasDiscount(String hasDiscount) {
		this.hasDiscount = hasDiscount;
	}

	/**
	 * @return the mofified
	 */
	public Date getMofified() {
		return mofified;
	}

	/**
	 * @param mofified the mofified to set
	 */
	public void setMofified(Date mofified) {
		this.mofified = mofified;
	}

	/**
	 * @return the uploadFailMsg
	 */
	public String getUploadFailMsg() {
		return uploadFailMsg;
	}

	/**
	 * @param uploadFailMsg the uploadFailMsg to set
	 */
	public void setUploadFailMsg(String uploadFailMsg) {
		this.uploadFailMsg = uploadFailMsg;
	}

	/**
	 * @return the pictureStatus
	 */
	public String getPictureStatus() {
		return pictureStatus;
	}

	/**
	 * @param pictureStatus the pictureStatus to set
	 */
	public void setPictureStatus(String pictureStatus) {
		this.pictureStatus = pictureStatus;
	}

	/**
	 * @return the auctionPoint
	 */
	public String getAuctionPoint() {
		return auctionPoint;
	}

	/**
	 * @param auctionPoint the auctionPoint to set
	 */
	public void setAuctionPoint(String auctionPoint) {
		this.auctionPoint = auctionPoint;
	}

	/**
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * @param picture the picture to set
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * @return the video
	 */
	public String getVideo() {
		return video;
	}

	/**
	 * @param video the video to set
	 */
	public void setVideo(String video) {
		this.video = video;
	}

	/**
	 * @return the skuProps
	 */
	public String getSkuProps() {
		return skuProps;
	}

	/**
	 * @param skuProps the skuProps to set
	 */
	public void setSkuProps(String skuProps) {
		this.skuProps = skuProps;
	}

	/**
	 * @return the inputPids
	 */
	public String getInputPids() {
		return inputPids;
	}

	/**
	 * @param inputPids the inputPids to set
	 */
	public void setInputPids(String inputPids) {
		this.inputPids = inputPids;
	}

	/**
	 * @return the inputValues
	 */
	public String getInputValues() {
		return inputValues;
	}

	/**
	 * @param inputValues the inputValues to set
	 */
	public void setInputValues(String inputValues) {
		this.inputValues = inputValues;
	}

	/**
	 * @return the outerId
	 */
	public String getOuterId() {
		return outerId;
	}

	/**
	 * @param outerId the outerId to set
	 */
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}

	/**
	 * @return the propAlias
	 */
	public String getPropAlias() {
		return propAlias;
	}

	/**
	 * @param propAlias the propAlias to set
	 */
	public void setPropAlias(String propAlias) {
		this.propAlias = propAlias;
	}

	/**
	 * @return the autoFill
	 */
	public String getAutoFill() {
		return autoFill;
	}

	/**
	 * @param autoFill the autoFill to set
	 */
	public void setAutoFill(String autoFill) {
		this.autoFill = autoFill;
	}

	/**
	 * @return the numId
	 */
	public String getNumId() {
		return numId;
	}

	/**
	 * @param numId the numId to set
	 */
	public void setNumId(String numId) {
		this.numId = numId;
	}

	/**
	 * @return the localCid
	 */
	public String getLocalCid() {
		return localCid;
	}

	/**
	 * @param localCid the localCid to set
	 */
	public void setLocalCid(String localCid) {
		this.localCid = localCid;
	}

	/**
	 * @return the navigationType
	 */
	public String getNavigationType() {
		return navigationType;
	}

	/**
	 * @param navigationType the navigationType to set
	 */
	public void setNavigationType(String navigationType) {
		this.navigationType = navigationType;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the syncStatus
	 */
	public String getSyncStatus() {
		return syncStatus;
	}

	/**
	 * @param syncStatus the syncStatus to set
	 */
	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	/**
	 * @return the isLightingConsigment
	 */
	public String getIsLightingConsigment() {
		return isLightingConsigment;
	}

	/**
	 * @param isLightingConsigment the isLightingConsigment to set
	 */
	public void setIsLightingConsigment(String isLightingConsigment) {
		this.isLightingConsigment = isLightingConsigment;
	}

	/**
	 * @return the isXinPin
	 */
	public String getIsXinPin() {
		return isXinPin;
	}

	/**
	 * @param isXinPin the isXinPin to set
	 */
	public void setIsXinPin(String isXinPin) {
		this.isXinPin = isXinPin;
	}

	/**
	 * @return the foodparame
	 */
	public String getFoodparame() {
		return foodparame;
	}

	/**
	 * @param foodparame the foodparame to set
	 */
	public void setFoodparame(String foodparame) {
		this.foodparame = foodparame;
	}

	/**
	 * @return the features
	 */
	public String getFeatures() {
		return features;
	}

	/**
	 * @param features the features to set
	 */
	public void setFeatures(String features) {
		this.features = features;
	}

	/**
	 * @return the buyareaType
	 */
	public String getBuyareaType() {
		return buyareaType;
	}

	/**
	 * @param buyareaType the buyareaType to set
	 */
	public void setBuyareaType(String buyareaType) {
		this.buyareaType = buyareaType;
	}

	/**
	 * @return the globalStockType
	 */
	public String getGlobalStockType() {
		return globalStockType;
	}

	/**
	 * @param globalStockType the globalStockType to set
	 */
	public void setGlobalStockType(String globalStockType) {
		this.globalStockType = globalStockType;
	}

	/**
	 * @return the globalStockCountry
	 */
	public String getGlobalStockCountry() {
		return globalStockCountry;
	}

	/**
	 * @param globalStockCountry the globalStockCountry to set
	 */
	public void setGlobalStockCountry(String globalStockCountry) {
		this.globalStockCountry = globalStockCountry;
	}

	/**
	 * @return the subStockType
	 */
	public String getSubStockType() {
		return subStockType;
	}

	/**
	 * @param subStockType the subStockType to set
	 */
	public void setSubStockType(String subStockType) {
		this.subStockType = subStockType;
	}

	/**
	 * @return the itemSize
	 */
	public String getItemSize() {
		return itemSize;
	}

	/**
	 * @param itemSize the itemSize to set
	 */
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	/**
	 * @return the itemWeight
	 */
	public String getItemWeight() {
		return itemWeight;
	}

	/**
	 * @param itemWeight the itemWeight to set
	 */
	public void setItemWeight(String itemWeight) {
		this.itemWeight = itemWeight;
	}

	/**
	 * @return the sellPromise
	 */
	public String getSellPromise() {
		return sellPromise;
	}

	/**
	 * @param sellPromise the sellPromise to set
	 */
	public void setSellPromise(String sellPromise) {
		this.sellPromise = sellPromise;
	}

	/**
	 * @return the customDesignFlag
	 */
	public String getCustomDesignFlag() {
		return customDesignFlag;
	}

	/**
	 * @param customDesignFlag the customDesignFlag to set
	 */
	public void setCustomDesignFlag(String customDesignFlag) {
		this.customDesignFlag = customDesignFlag;
	}

	/**
	 * @return the wirelessDesc
	 */
	public String getWirelessDesc() {
		return wirelessDesc;
	}

	/**
	 * @param wirelessDesc the wirelessDesc to set
	 */
	public void setWirelessDesc(String wirelessDesc) {
		this.wirelessDesc = wirelessDesc;
	}

	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @param barcode the barcode to set
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * @return the skuBarcode
	 */
	public String getSkuBarcode() {
		return skuBarcode;
	}

	/**
	 * @param skuBarcode the skuBarcode to set
	 */
	public void setSkuBarcode(String skuBarcode) {
		this.skuBarcode = skuBarcode;
	}

	/**
	 * @return the newprepay
	 */
	public String getNewprepay() {
		return newprepay;
	}

	/**
	 * @param newprepay the newprepay to set
	 */
	public void setNewprepay(String newprepay) {
		this.newprepay = newprepay;
	}

	/**
	 * @return the subtitle
	 */
	public String getSubtitle() {
		return subtitle;
	}

	/**
	 * @param subtitle the subtitle to set
	 */
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	/**
	 * @return the cpvMemo
	 */
	public String getCpvMemo() {
		return cpvMemo;
	}

	/**
	 * @param cpvMemo the cpvMemo to set
	 */
	public void setCpvMemo(String cpvMemo) {
		this.cpvMemo = cpvMemo;
	}

	/**
	 * @return the inputCustomCpv
	 */
	public String getInputCustomCpv() {
		return inputCustomCpv;
	}

	/**
	 * @param inputCustomCpv the inputCustomCpv to set
	 */
	public void setInputCustomCpv(String inputCustomCpv) {
		this.inputCustomCpv = inputCustomCpv;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the addQualification
	 */
	public String getAddQualification() {
		return addQualification;
	}

	/**
	 * @param addQualification the addQualification to set
	 */
	public void setAddQualification(String addQualification) {
		this.addQualification = addQualification;
	}

	/**
	 * @return the o2oBindService
	 */
	public String getO2oBindService() {
		return o2oBindService;
	}
	/**
	 * @param o2oBindService the o2oBindService to set
	 */
	public void setO2oBindService(String o2oBindService) {
		this.o2oBindService = o2oBindService;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaoBaoProductImportDto [title=" + title + ", cid=" + cid + ", sellerCid=" + sellerCid
				+ ", stuffStatus=" + stuffStatus + ", locationState=" + locationState + ", locationCity="
				+ locationCity + ", itemType=" + itemType + ", price=" + price + ", auctionIncrement="
				+ auctionIncrement + ", num=" + num + ", vaildThru=" + validThru + ", freightPayer=" + freightPayer
				+ ", postFee=" + postFee + ", emsFee=" + emsFee + ", expressFee=" + expressFee + ", hasInvoice="
				+ hasInvoice + ", hasWarranty=" + hasWarranty + ", approveStatus=" + approveStatus + ", hasShowcase="
				+ hasShowcase + ", listTime=" + listTime + ", description=" + description + ", cateProps=" + cateProps
				+ ", postageId=" + postageId + ", hasDiscount=" + hasDiscount + ", mofified=" + mofified
				+ ", uploadFailMsg=" + uploadFailMsg + ", pictureStatus=" + pictureStatus + ", auctionPoint="
				+ auctionPoint + ", picture=" + picture + ", video=" + video + ", skuProps=" + skuProps
				+ ", inputPids=" + inputPids + ", inputValues=" + inputValues + ", outerId=" + outerId + ", propAlias="
				+ propAlias + ", autoFill=" + autoFill + ", numId=" + numId + ", localCid=" + localCid
				+ ", navigationType=" + navigationType + ", userName=" + userName + ", syncStatus=" + syncStatus
				+ ", isLightingConsigment=" + isLightingConsigment + ", isXinPin=" + isXinPin + ", foodparame="
				+ foodparame + ", features=" + features + ", buyareaType=" + buyareaType + ", globalStockType="
				+ globalStockType + ", globalStockCountry=" + globalStockCountry + ", subStockType=" + subStockType
				+ ", itemSize=" + itemSize + ", itemWeight=" + itemWeight + ", sellPromise=" + sellPromise
				+ ", customDesignFlag=" + customDesignFlag + ", wirelessDesc=" + wirelessDesc + ", barcode=" + barcode
				+ ", skuBarcode=" + skuBarcode + ", newprepay=" + newprepay + ", subtitle=" + subtitle + ", cpvMemo="
				+ cpvMemo + ", inputCustomCpv=" + inputCustomCpv + ", qualification=" + qualification
				+ ", addQualification=" + addQualification + ", o2oBindService=" + o2oBindService + "]";
	}
}
