/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 广告位Dto
 *
 * @author Tony
 * 
 */
@ApiModel("广告位Dto")
public class AdvertsDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** 内容. */
	@ApiModelProperty(value = "内容")
	private String text;

	/** 价格. */
	@ApiModelProperty(value = "价格")
	private Double price;

	/** 主题. */
	@ApiModelProperty(value = "主题")
	private String themecode;

	/** 显示图片. */
	@ApiModelProperty(value = "显示图片")
	private String img;

	/** action参数: 跳转action带的参数. */
	@ApiModelProperty(value = "action参数: 跳转action带的参数")
	private AdvertsActionparamDto actionParam;

	/** 顺序. */
	@ApiModelProperty(value = "顺序")
	private int ordering;

	/** 动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题. */
	@ApiModelProperty(value = "动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题")
	private String action;

	/** 唯一标识. */
	@ApiModelProperty(value = "唯一标识")
	private String id;

	/**
	 * Sets the text.
	 *
	 * @param text the text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the themecode.
	 *
	 * @param themecode themecode
	 */
	public void setThemecode(String themecode) {
		this.themecode = themecode;
	}

	/**
	 * Gets the themecode.
	 *
	 * @return the themecode
	 */
	public String getThemecode() {
		return themecode;
	}

	/**
	 * Sets the img.
	 *
	 * @param img the img
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * Gets the img.
	 *
	 * @return the img
	 */
	public String getImg() {
		return img;
	}


	/**
	 * Sets the ordering.
	 *
	 * @param ordering the ordering
	 */
	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	/**
	 * Gets the ordering.
	 *
	 * @return the ordering
	 */
	public int getOrdering() {
		return ordering;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * Gets the action param.
	 *
	 * @return the action param
	 */
	public AdvertsActionparamDto getActionParam() {
		return actionParam;
	}

	/**
	 * Sets the action param.
	 *
	 * @param actionParam the action param
	 */
	public void setActionParam(AdvertsActionparamDto actionParam) {
		this.actionParam = actionParam;
	}

	
}