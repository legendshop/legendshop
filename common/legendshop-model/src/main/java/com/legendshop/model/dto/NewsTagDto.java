package com.legendshop.model.dto;



public class NewsTagDto {
	
	private Long itemId;
	
	private String itemName;
	
	private NewsDto newsDto;

	public Long getItemId() {
		return itemId;
	}



	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public NewsDto getNewsDto() {
		return newsDto;
	}



	public void setNewsDto(NewsDto newsDto) {
		this.newsDto = newsDto;
	}



}
