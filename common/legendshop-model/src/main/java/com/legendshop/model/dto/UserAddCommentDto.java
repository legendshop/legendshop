package com.legendshop.model.dto;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Transient;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户追加评论页面Dto
 *
 */
@ApiModel(value="用户追加评论页面对象")
public class UserAddCommentDto {
	
	/** 评论Id */
	@ApiModelProperty(value="评论Id")
	private Long prodCommId;
	
	/** 商品Id */
	@ApiModelProperty(value="商品Id")
	private Long prodId;
	
	/** 订单项id */
	@ApiModelProperty(value="订单项id")
	private Long subItemId;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;
	
	/** 商品名 */
	@ApiModelProperty(value="商品名")
	private String prodName;
	
	/** 产品动态属性 */
	@ApiModelProperty(value="产品动态属性")
	private String attribute;
	
	/** 购买时间 */
	@ApiModelProperty(value="购买时间")
	private Date buyTime;
	
	/** 评分 */
	@ApiModelProperty(value="评分")
	private Integer score;
	
	/** 评论内容 */
	@ApiModelProperty(value="评论内容")
	private String content;
	
	/** 评论图片 */
	@ApiModelProperty(value="评论图片",hidden=true)
	private String photos;
	
	/** 评论图片列表 */
	@ApiModelProperty(value="评论图片列表")
	private List<String> photoList;
	
	/** 评论时间 */
	@ApiModelProperty(value="评论时间")
	private Date commentTime;
	
	public Long getProdCommId() {
		return prodCommId;
	}

	public void setProdCommId(Long prodCommId) {
		this.prodCommId = prodCommId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Date getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}
	
	public Date getCommentTime() {
		return commentTime;
	}

	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}

	@Transient
	public String[] getPhotoPaths(){
		if(AppUtils.isNotBlank(this.photos)){
			String[] photoPaths = this.photos.split(",");
			return photoPaths;
		}
		
		return null;
	}

	public List<String> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<String> photoList) {
		this.photoList = photoList;
	}
	
}
