package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信组
 */
@Entity
@Table(name = "ls_weixin_group")
public class WeixinGroup implements GenericEntity<Long> {

	private static final long serialVersionUID = 3904152262529820439L;
	/**  */
	private Long id; 
		
	/** 分组名称 */
	private String name; 
		
	/** 添加时间 */
	private Date addtime; 
		
	
	public WeixinGroup() {
    }
		
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "addtime")
	public Date  getAddtime(){
		return addtime;
	} 
		
	public void setAddtime(Date addtime){
			this.addtime = addtime;
		}
	


} 
