/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 秒杀成功记录
 */
@ApiModel(value="秒杀成功记录")
public class SeckillSuccessRecord {

	/** id */
	@ApiModelProperty(value="id")
	private Long id; 
		
	/** 用户id. */
	@ApiModelProperty(value="用户id")
	private String userId; 
		
	/** 秒杀id */
	@ApiModelProperty(value="秒杀id")
	private Long seckillId; 
		
	/** 商品id */
	@ApiModelProperty(value="商品id")
	private Long prodId; 
		
	/** skuId */
	@ApiModelProperty(value="skuId")
	private Long skuId; 
		
	/** 秒杀数量 */
	@ApiModelProperty(value="秒杀数量")
	private Integer seckillNum; 
		
	/** 秒杀价格 */
	@ApiModelProperty(value="秒杀价格")
	private Double seckillPrice; 
		
	/** 创建的时间 */
	@ApiModelProperty(value="创建的时间")
	private java.sql.Timestamp seckillTime; 
		
	/** 用户的状态默认0： 0成功状态; 1:已下单; -1已取消 */
	@ApiModelProperty(value="用户的状态默认0： 0成功状态; 1:已下单; -1已取消")
	private Integer status; 
	
	/** 秒杀结果 */
	@ApiModelProperty(value="秒杀结果")
	private int result;
	
	/** 秒杀名称 */
	@ApiModelProperty(value="秒杀名称")
	private String seckillTitle;
	
	/** 秒杀活动图片 */
	@ApiModelProperty(value="秒杀活动图片")
	private String seckillPic;
	
	/** 密钥 */
	@ApiModelProperty(value="密钥")
	private String secretKey;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(Long seckillId) {
		this.seckillId = seckillId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getSeckillNum() {
		return seckillNum;
	}

	public void setSeckillNum(Integer seckillNum) {
		this.seckillNum = seckillNum;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public java.sql.Timestamp getSeckillTime() {
		return seckillTime;
	}

	public void setSeckillTime(java.sql.Timestamp seckillTime) {
		this.seckillTime = seckillTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getSeckillTitle() {
		return seckillTitle;
	}

	public void setSeckillTitle(String seckillTitle) {
		this.seckillTitle = seckillTitle;
	}

	public String getSeckillPic() {
		return seckillPic;
	}

	public void setSeckillPic(String seckillPic) {
		this.seckillPic = seckillPic;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

} 
