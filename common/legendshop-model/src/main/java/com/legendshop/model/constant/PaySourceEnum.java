package com.legendshop.model.constant;

/**
 * 支付授起来源
 */
public enum PaySourceEnum {

	/**
	 * PC端
	 */
	PC("PC"),

	/**
	 * 普通浏览器, 普通JSP端, 新版本不再支持jsp版本 TODO
	 */
	WAP_JSP("WAP_JSP"),


	/**
	 * 普通浏览器, 普通Vue端
	 */
	WAP("WAP"),
	
	/**
	 * 微信端浏览器
	 */
	WEIXIN("WEIXIN"),

	/**
	 * APP端
	 */
	APP("APP"),
	
	/** 微信小程序 */
	WX_MP("WX_MP"),
	;
	
	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		PaySourceEnum[] licenseEnums = values();
		for (PaySourceEnum licenseEnum : licenseEnums) {
			if (licenseEnum.getDesc().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isWxMp(String value){
		if(value.endsWith("WX_MP")){
			return true;
		}
		return false;
	}
	
	public static boolean isAPP(String value){
		if(value != null && value.endsWith("APP")){
			return true;
		}
		return false;
	}
	
	public static boolean isWAP(String value){
		if(value != null && value.endsWith("WAP")){
			return true;
		}
		return false;
	}
	
	public static boolean isWEIXIN(String value){
		if(value != null && value.endsWith("WEIXIN")){
			return true;
		}
		return false;
	}
	
	public static boolean isPC(String value){
		if(value != null && value.endsWith("PC")){
			return true;
		}
		return false;
	}

	public static boolean isWAPJSP(String value){
		if(value != null && value.endsWith("WAP_JSP")){
			return true;
		}
		return false;
	}
	

	/** 描述 */
	private String desc;

	private PaySourceEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	

}
