/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 系统配置
 */

@Entity
@Table(name = "ls_sys_conf")
public class SystemConfig extends UploadFile implements GenericEntity<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5056144528811885435L;

	
	/** The id. */
	private Long id ; 
		
	
	/** The icp info. */
	private String icpInfo ; 
		
	 
	/** The logo. */
	private String logo; 
		
	 
	/** The shop name. */
	private String shopName ; 
	
	private String userName; 
	 
	/** The url. */
	private String url ; 
		
	// 多个邮箱，用逗号隔开
	/** The support mail. */
	private String supportMail ; 
		
	//国际冠码
	private String internationalCode;
	//区号
	private String areaCode;
	//电话
	private String telephone;
	
	private String qqNumber;
	
	private List<String> qqList;
	
	//微信图片
	private String wechatCode;
	
	//头条图片
	private String wechatTitlePic;
	
	//邮箱验证模板
	private String mailValidateTemplate;
	
	//注册协议模板
	private String regProtocolTemplate;

	//注册成功邮件模板
	private String mailRegsuccessTemplate;
	
	//入驻模板
	private String settledProtocolTemplate;

	//开店模板
	private String openShopProtocolTemplate;

	//开店模板
	private String auctionsPayProtocolTemplate;

	// 后台logo地址
	private String adminLogoCode;

	/** The file. */
	protected MultipartFile wechatCodeFile;

	protected MultipartFile wechatServiceCodeFile;

	protected MultipartFile wechatTitlePicFile;

	protected MultipartFile adminLogoFile;


	private String title;//全局title关键字

	private String keywords; //全局SEO关键字

	private String description; //全局SEO关键描述

	/** 后台关于模板 */
	private String backAbout;

	/**
	 * Instantiates a new system config.
	 */
	public SystemConfig() {
    }

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Id
	@Override
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SYS_CONF_SEQ")
	public Long  getId(){
		return id ;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	@Override
	public void setId(Long id){
		this.id = id ;
	}


	/**
	 * Gets the icp info.
	 *
	 * @return the icp info
	 */
	@Column(name = "icp_info")
	public String  getIcpInfo(){
		return icpInfo ;
	}

	/**
	 * Sets the icp info.
	 *
	 * @param icpInfo the new icp info
	 */
	public void setIcpInfo(String icpInfo){
		this.icpInfo = icpInfo ;
	}


	/**
	 * Gets the logo.
	 *
	 * @return the logo
	 */
	@Column(name = "logo")
	public String  getLogo(){
		return logo ;
	}

	/**
	 * Sets the logo.
	 *
	 * @param logo the new logo
	 */
	public void setLogo(String logo){
		this.logo = logo ;
	}


	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	@Column(name = "shop_name")
	public String  getShopName(){
		return shopName ;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName the new shop name
	 */
	public void setShopName(String shopName){
		this.shopName = shopName ;
	}


	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	@Column(name = "url")
	public String  getUrl(){
		return url ;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url){
		this.url = url ;
	}


	/**
	 * Gets the support mail.
	 *
	 * @return the support mail
	 */
	@Column(name = "support_mail")
	public String  getSupportMail(){
		return supportMail ;
	}

	/**
	 * Sets the support mail.
	 *
	 * @param supportMail the new support mail
	 */
	public void setSupportMail(String supportMail){
		this.supportMail = supportMail ;
	}

	@Column(name = "internationalCode")
	public String getInternationalCode() {
		return internationalCode;
	}

	public void setInternationalCode(String internationalCode) {
		this.internationalCode = internationalCode;
	}

	@Column(name = "areaCode")
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Column(name = "telephone")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "QQ_number")
	public String getQqNumber() {
		return qqNumber;
	}

	public void setQqNumber(String qqNumber) {
		this.qqNumber = qqNumber;
	}

	@Transient
	public List<String> getQqList() {
		return qqList;
	}

	public void setQqList(List<String> qqList) {
		this.qqList = qqList;
	}

	@Column(name = "wechatCode")
	public String getWechatCode() {
		return wechatCode;
	}

	public void setWechatCode(String wechatCode) {
		this.wechatCode = wechatCode;
	}

	@Transient
	public MultipartFile getWechatCodeFile() {
		return wechatCodeFile;
	}

	public void setWechatCodeFile(MultipartFile wechatCodeFile) {
		this.wechatCodeFile = wechatCodeFile;
	}

	@Column(name = "mail_validate_template")
	public String getMailValidateTemplate() {
		return mailValidateTemplate;
	}

	public void setMailValidateTemplate(String mailValidateTemplate) {
		this.mailValidateTemplate = mailValidateTemplate;
	}

	@Column(name = "reg_protocol_template")
	public String getRegProtocolTemplate() {
		return regProtocolTemplate;
	}

	public void setRegProtocolTemplate(String regProtocolTemplate) {
		this.regProtocolTemplate = regProtocolTemplate;
	}

	@Column(name = "mail_regsuccess_template")
	public String getMailRegsuccessTemplate() {
		return mailRegsuccessTemplate;
	}

	public void setMailRegsuccessTemplate(String mailRegsuccessTemplate) {
		this.mailRegsuccessTemplate = mailRegsuccessTemplate;
	}

	@Column(name = "settled_protocol_template")
	public String getSettledProtocolTemplate() {
		return settledProtocolTemplate;
	}

	public void setSettledProtocolTemplate(String settledProtocolTemplate) {
		this.settledProtocolTemplate = settledProtocolTemplate;
	}
	@Column(name = "openshop_protocol_template")
	public String getOpenShopProtocolTemplate() {
		return openShopProtocolTemplate;
	}

	public void setOpenShopProtocolTemplate(String openShopProtocolTemplate) {
		this.openShopProtocolTemplate = openShopProtocolTemplate;
	}

	@Column(name = "seo_title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "seo_keywords")
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Column(name = "seo_description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "auctions_pay_protocol_template")
	public String getAuctionsPayProtocolTemplate() {
		return auctionsPayProtocolTemplate;
	}

	public void setAuctionsPayProtocolTemplate(String auctionsPayProtocolTemplate) {
		this.auctionsPayProtocolTemplate = auctionsPayProtocolTemplate;
	}

	@Column(name = "back_about")
	public String getBackAbout() {
		return backAbout;
	}
	/**
	 * @param backAbout the backAbout to set
	 */
	public void setBackAbout(String backAbout) {
		this.backAbout = backAbout;
	}

	@Column(name = "wechat_title_pic")
	public String getWechatTitlePic() {
		return wechatTitlePic;
	}

	public void setWechatTitlePic(String wechatTitlePic) {
		this.wechatTitlePic = wechatTitlePic;
	}

	@Transient
	public MultipartFile getWechatTitlePicFile() {
		return wechatTitlePicFile;
	}

	public void setWechatTitlePicFile(MultipartFile wechatTitlePicFile) {
		this.wechatTitlePicFile = wechatTitlePicFile;
	}
	@Transient
	public MultipartFile getWechatServiceCodeFile() {
		return wechatServiceCodeFile;
	}

	public void setWechatServiceCodeFile(MultipartFile wechatServiceCodeFile) {
		this.wechatServiceCodeFile = wechatServiceCodeFile;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "admin_logo_code")
	public String getAdminLogoCode() {
		return adminLogoCode;
	}

	public void setAdminLogoCode(String adminLogoCode) {
		this.adminLogoCode = adminLogoCode;
	}

	@Transient
	public MultipartFile getAdminLogoFile() {
		return adminLogoFile;
	}

	public void setAdminLogoFile(MultipartFile adminLogoFile) {
		this.adminLogoFile = adminLogoFile;
	}
}
