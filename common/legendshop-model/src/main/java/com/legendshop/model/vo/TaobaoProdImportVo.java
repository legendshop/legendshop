package com.legendshop.model.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

public class TaobaoProdImportVo{

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 7867381317188907517L;

	/** 表单ID,用于防止表单重复提交 */
	protected String formId;
	
	/** 商品类目 */
	protected Long categoryId;
	
	/** 上传csvFile文件 */
	protected MultipartFile csvFile;
	
	/** 店铺分类 */
	protected String sellerCids;
	
	/** 发布商品状态 */
	protected Integer publishStatus;
	
	/** 开始时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	protected Date startDate;
	
	/** 是否保存宝贝描述中的图片 */
	protected Boolean isSavePicture;

	/**
	 * @return the formId
	 */
	public String getFormId() {
		return formId;
	}

	/**
	 * @param formId the formId to set
	 */
	public void setFormId(String formId) {
		this.formId = formId;
	}

	/**
	 * @return the categoryId
	 */
	public Long getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the csvFile
	 */
	public MultipartFile getCsvFile() {
		return csvFile;
	}

	/**
	 * @param csvFile the csvFile to set
	 */
	public void setCsvFile(MultipartFile csvFile) {
		this.csvFile = csvFile;
	}

	/**
	 * @return the sellerCids
	 */
	public String getSellerCids() {
		return sellerCids;
	}

	/**
	 * @param sellerCids the sellerCids to set
	 */
	public void setSellerCids(String sellerCids) {
		this.sellerCids = sellerCids;
	}

	/**
	 * @return the publishStatus
	 */
	public Integer getPublishStatus() {
		return publishStatus;
	}

	/**
	 * @param publishStatus the publishStatus to set
	 */
	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the isSavePicture
	 */
	public Boolean getIsSavePicture() {
		return isSavePicture;
	}

	/**
	 * @param isSavePicture the isSavePicture to set
	 */
	public void setIsSavePicture(Boolean isSavePicture) {
		this.isSavePicture = isSavePicture;
	}
}
