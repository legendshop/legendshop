/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.legendshop.dao.support.PageProvider;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用于APP的分页DTO
 */
@ApiModel(value="返回的分页数据")
public class AppPageSupport<T> implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6155118151753555724L;

	/** 当前页. */
	@ApiModelProperty("当前页")
	private int currPage;

	/** 每页大小. */
	@ApiModelProperty("每页大小")
	private int pageSize;

	/** 总记录数. */
	@ApiModelProperty("总记录数")
	private long total = 0;
	
	/** 总页数   */
	@ApiModelProperty("总页数")
	private long pageCount;

	/** 结果集. */
	@ApiModelProperty("返回结果集")
	private List<T> resultList;

	public AppPageSupport() {
	}
	
	public AppPageSupport(List<T> resultList, PageProvider pageProvider) {
		this.resultList = resultList;
		this.currPage = pageProvider.getCurPageNO();
		this.total = pageProvider.getTotal();
		this.pageSize = pageProvider.getPageSize();
		this.pageCount = pageProvider.getPageCount();
	}
	
	/**
	 * Gets the page size.
	 *
	 * @return the page size
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the page size.
	 *
	 * @param pageSize the page size
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<T> getResultList() {
		return resultList;
	}
	
	/**
	 * 由JSONObject转化为对象数组.
	 *
	 * @param clazz the clazz
	 * @return the result list
	 */
	public List<T> getResultList(Class<T> clazz) {
		if(resultList == null || resultList.size() == 0){
			return null;
		}
		String result = resultList.toString();
		return JSON.parseArray(result, clazz);
	}

	/**
	 * Sets the result list.
	 *
	 * @param resultList the result list
	 */
	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}

	/**
	 * Gets the page count.
	 *
	 * @return the page count
	 */
	public long getPageCount() {
		return pageCount;
	}

	/**
	 * Sets the page count.
	 *
	 * @param pageCount the page count
	 */
	public void setPageCount(long pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * Gets the curr page.
	 *
	 * @return the curr page
	 */
	public int getCurrPage() {
		return currPage;
	}

	/**
	 * Sets the curr page.
	 *
	 * @param currPage the curr page
	 */
	public void setCurrPage(int currPage) {
		this.currPage = currPage;
	}

	/**
	 * Gets the total count.
	 *
	 * @return the total count
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * Sets the total count.
	 *
	 * @param total the total count
	 */
	public void setTotal(long total) {
		this.total = total;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PageSupportDto [currPage=" + currPage + ", pageSize=" + pageSize + ", totalCount=" + total
				+ ", pageCount=" + pageCount + ", resultList=" + resultList + "]";
	}
	
	

}
