package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 第三方账号通行证类型
 * @author 开发很忙
 */
public enum PassportTypeEnum implements StringEnum{
	
	/** 微信 */
	WEIXIN("weixin"),
	
	/** QQ */
	QQ("qq"),
	
	/** 微博 */
	SINAWEIBO("sinaweibo"),
	
	;
	
	private PassportTypeEnum(String value) {
		this.value = value;
	}

	private String value;

	@Override
	public String value() {
		return value;
	}
	
    public static PassportTypeEnum fromValue(String value) {
        for (PassportTypeEnum passportTypeEnum : PassportTypeEnum.values()) {
            if (passportTypeEnum.value.equals(value)) {
                return passportTypeEnum;
            }
        }
        return null;
    }

}
