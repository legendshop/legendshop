package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class OrderExprotDto  implements Serializable{
	
	private String number;//订单编号
	private String subType;//订单类型
	private String statue;//订单状态
	private String isPayed;//支付状态
	private String refundSts1;//退款状态
	private Double totalPrice;//订单总价
	private Double freightAmount;//运费
	private Double couponPrice;//优惠券优惠
	private Double redpackPrice;//红包优惠
	private String dvyType;//配送类型
	private String payTypeName;//支付类型名称(非预售)
	private String userName;//买家名称
	private String receiver; //收货人
	private String mobile;//买家手机号
	private String addr;//买家地址
	private String remark;//留言
	private String prodName;//商品名称
	private Long prodCount;//商品数量
	private String attr;//属性，规格
	private Date createDate;//下单时间
	private String shopName;//店铺名称
	private Double price;//商品价格
	private String modelId;//商品条形码
	private String partyCode;//商家编码
	private String refundSts2;//退款状态2
	private String payManner;//支付方式(1:货到付款,2:在线支付)
	private String payPctType;//支付方式,0:全额,1:订金
	private String depositTradeNo;//订金支付流水号
	private Double preDepositPrice;//订金金额
	private String depositPayName; //订金支付类型名称(预售)
	private Double finalTradeNo; //尾款支付流水号
	private Double finalPrice;//尾款金额
	private String finalPayName;//尾款支付类型名称(预售)
	private String isPayFinal;//尾款支付状态
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getStatue() {
		return statue;
	}
	public void setStatue(String statue) {
		this.statue = statue;
	}
	public String getIsPayed() {
		return isPayed;
	}
	public void setIsPayed(String isPayed) {
		this.isPayed = isPayed;
	}
	public String getRefundSts1() {
		return refundSts1;
	}
	public void setRefundSts1(String refundSts1) {
		this.refundSts1 = refundSts1;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getFreightAmount() {
		return freightAmount;
	}
	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}
	public Double getCouponPrice() {
		return couponPrice;
	}
	public void setCouponPrice(Double couponPrice) {
		this.couponPrice = couponPrice;
	}
	public Double getRedpackPrice() {
		return redpackPrice;
	}
	public void setRedpackPrice(Double redpackPrice) {
		this.redpackPrice = redpackPrice;
	}
	public String getDvyType() {
		return dvyType;
	}
	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}
	public String getPayTypeName() {
		return payTypeName;
	}
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public Long getProdCount() {
		return prodCount;
	}
	public void setProdCount(Long prodCount) {
		this.prodCount = prodCount;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getPartyCode() {
		return partyCode;
	}
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}
	public String getRefundSts2() {
		return refundSts2;
	}
	public void setRefundSts2(String refundSts2) {
		this.refundSts2 = refundSts2;
	}
	public String getPayManner() {
		return payManner;
	}
	public void setPayManner(String payManner) {
		this.payManner = payManner;
	}
	public String getPayPctType() {
		return payPctType;
	}
	public void setPayPctType(String payPctType) {
		this.payPctType = payPctType;
	}
	public String getDepositTradeNo() {
		return depositTradeNo;
	}
	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}
	public Double getPreDepositPrice() {
		return preDepositPrice;
	}
	public void setPreDepositPrice(Double preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}
	public String getDepositPayName() {
		return depositPayName;
	}
	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}
	public Double getFinalTradeNo() {
		return finalTradeNo;
	}
	public void setFinalTradeNo(Double finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}
	public Double getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}
	public String getFinalPayName() {
		return finalPayName;
	}
	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}
	public String getIsPayFinal() {
		return isPayFinal;
	}
	public void setIsPayFinal(String isPayFinal) {
		this.isPayFinal = isPayFinal;
	}
	
}
