/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum JobStatusEnum implements StringEnum {

	/** 商家结算 */
	GENERATE_SHOP_BILL,
	
	/** 分销结算 */
	SETTLE_DIST_COMMIS,
	
	/**竞拍退款*/
	AUCTION_REFUND,
	
	/** 团购退款 */
	GROUP_REFYUND,
	
	/** 未成团退款 */
	NOMERGEGROUP_REFUND,
	
	/** */
	AUCTION_REFUND_BID_PAY,
	
	AUCTION_REFUND_BID_24,
	
	AUCTION_REFUND_BID_72,
	
	/** 同步用户 */
	SYNCHRONOUS_USERINFO;
	

	@Override
	public String value() {
		return Integer.toString(this.ordinal());
	}

}
