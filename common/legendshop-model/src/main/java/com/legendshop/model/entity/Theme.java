package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *专题
 */
@Entity
@Table(name = "ls_theme")
public class Theme implements GenericEntity<Long> {

	private static final long serialVersionUID = 8347057741295019680L;

	/** 专题id */
	private Long themeId; 
		
	/** 标题 */
	private String title; 
		
	/** 开始时间 */
	private Date startTime; 
		
	/** 结束时间 */
	private Date endTime; 
		
	/** 标题颜色 */
	private String titleColor; 
		
	/** 标题是否在横幅区显示 */
	private Long isTitleShow; 
		
	/** 简介 */
	private String intro; 
	
	/** 手机端简介 */
	private String introMobile; 
		
	/** 简介字体颜色 */
	private String introColor; 
		
	/** 简介是否在横幅区显示 */
	private Long isIntroShow; 
	
	/** 手机简介字体颜色 */
	private String introMColor; 
		
	/** 手机简介是否在横幅区显示 */
	private Long isIntroMShow; 
		
	/** 专题描述 */
	private String themeDesc; 
		
	/** pc专题图片 */
	private String bannerPcImg; 
		
	/** 手机专题图片 */
	private String bannerMobileImg; 
		
	/** 横幅区背景色 */
	private String bannerImgColor; 
		
	/** 页面背景图 PC */
	private String backgroundPcImg; 
	
	/** 页面背景图  手机*/
	private String backgroundMobileImg; 
	
	/**专题pc活动图*/
	private String themePcImg;
	
	/**专题手机活动图*/
	private String themeMobileImg;
		
	/** 页面背景色 */
	private String backgroundColor; 
		
	/** 状态 */
	private Integer status; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 更新时间 */
	private Date updateTime; 
	
	/**是否移动端首页显示*/
	private Boolean showPhoneIndex;
	
	
	/**自定义模块*/
	private String customContent;
	
	/** 模板类型 */
	private Integer templateType;
	
	/**
	 * 是否pc端首页显示
	 */
	private Boolean showPcIndex;
	
	private MultipartFile bannerPcImgFile;
	
	private MultipartFile bannerMobileImgFile;
	
	private MultipartFile backgroundPcImgFile;
	
	private MultipartFile backgroundMobileImgFile;
	
	private MultipartFile themePcImgFile;
	
	private MultipartFile themeMobileImgFile;
	
	/**
	 * true为有效期内，false 为过期
	 */
	private boolean expire;
	
	
	public Theme() {
    }
		
	@Id
	@Column(name = "theme_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "LS_THEME_SEQ")
	public Long  getThemeId(){
		return themeId;
	} 
		
	public void setThemeId(Long themeId){
			this.themeId = themeId;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}
		
    @Column(name = "title_color")
	public String  getTitleColor(){
		return titleColor;
	} 
		
	public void setTitleColor(String titleColor){
			this.titleColor = titleColor;
		}
		
    @Column(name = "is_title_show")
	public Long  getIsTitleShow(){
		return isTitleShow;
	} 
		
	public void setIsTitleShow(Long isTitleShow){
			this.isTitleShow = isTitleShow;
		}
		
    @Column(name = "intro")
	public String  getIntro(){
		return intro;
	} 
		
	public void setIntro(String intro){
			this.intro = intro;
		}
		
    @Column(name = "intro_color")
	public String  getIntroColor(){
		return introColor;
	} 
		
	public void setIntroColor(String introColor){
			this.introColor = introColor;
		}
		
    @Column(name = "is_intro_show")
	public Long  getIsIntroShow(){
		return isIntroShow;
	} 
		
	public void setIsIntroShow(Long isIntroShow){
			this.isIntroShow = isIntroShow;
		}
		
    @Column(name = "theme_desc")
	public String  getThemeDesc(){
		return themeDesc;
	} 
		
	public void setThemeDesc(String themeDesc){
			this.themeDesc = themeDesc;
		}
		
    @Column(name = "banner_pc_img")
	public String  getBannerPcImg(){
		return bannerPcImg;
	} 
		
	public void setBannerPcImg(String bannerPcImg){
			this.bannerPcImg = bannerPcImg;
		}
		
    @Column(name = "banner_mobile_img")
	public String  getBannerMobileImg(){
		return bannerMobileImg;
	} 
		
	public void setBannerMobileImg(String bannerMobileImg){
			this.bannerMobileImg = bannerMobileImg;
		}
		
    @Column(name = "banner_img_color")
	public String  getBannerImgColor(){
		return bannerImgColor;
	} 
		
	public void setBannerImgColor(String bannerImgColor){
			this.bannerImgColor = bannerImgColor;
		}
	
	
		
    @Column(name = "background_color")
	public String  getBackgroundColor(){
		return backgroundColor;
	} 
		
	public void setBackgroundColor(String backgroundColor){
			this.backgroundColor = backgroundColor;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "update_time")
	public Date  getUpdateTime(){
		return updateTime;
	} 
		
	public void setUpdateTime(Date updateTime){
			this.updateTime = updateTime;
		}
	
	@Override
	@Transient
	public Long getId() {
		return themeId;
	}
	
	@Override
	public void setId(Long id) {
		themeId = id;
	}
	
	@Column(name="show_phone_index")
	public Boolean getShowPhoneIndex() {
		return showPhoneIndex;
	}

	public void setShowPhoneIndex(Boolean showPhoneIndex) {
		this.showPhoneIndex = showPhoneIndex;
	}
	
	@Column(name="show_pc_index")
	public Boolean getShowPcIndex() {
		return showPcIndex;
	}

	public void setShowPcIndex(Boolean showPcIndex) {
		this.showPcIndex = showPcIndex;
	}

	@Column(name="background_pc_img")
	public String getBackgroundPcImg() {
		return backgroundPcImg;
	}

	public void setBackgroundPcImg(String backgroundPcImg) {
		this.backgroundPcImg = backgroundPcImg;
	}

	@Column(name="background_mobile_img")
	public String getBackgroundMobileImg() {
		return backgroundMobileImg;
	}

	public void setBackgroundMobileImg(String backgroundMobileImg) {
		this.backgroundMobileImg = backgroundMobileImg;
	}

	@Transient
	public MultipartFile getBannerPcImgFile() {
		return bannerPcImgFile;
	}

	public void setBannerPcImgFile(MultipartFile bannerPcImgFile) {
		this.bannerPcImgFile = bannerPcImgFile;
	}

	@Transient
	public MultipartFile getBannerMobileImgFile() {
		return bannerMobileImgFile;
	}

	public void setBannerMobileImgFile(MultipartFile bannerMobileImgFile) {
		this.bannerMobileImgFile = bannerMobileImgFile;
	}

	@Transient
	public MultipartFile getBackgroundPcImgFile() {
		return backgroundPcImgFile;
	}

	public void setBackgroundPcImgFile(MultipartFile backgroundPcImgFile) {
		this.backgroundPcImgFile = backgroundPcImgFile;
	}

	@Transient
	public MultipartFile getBackgroundMobileImgFile() {
		return backgroundMobileImgFile;
	}

	public void setBackgroundMobileImgFile(MultipartFile backgroundMobileImgFile) {
		this.backgroundMobileImgFile = backgroundMobileImgFile;
	}

	@Column(name="theme_pc_img")
	public String getThemePcImg() {
		return themePcImg;
	}

	public void setThemePcImg(String themePcImg) {
		this.themePcImg = themePcImg;
	}

	@Column(name="theme_mobile_img")
	public String getThemeMobileImg() {
		return themeMobileImg;
	}

	public void setThemeMobileImg(String themeMobileImg) {
		this.themeMobileImg = themeMobileImg;
	}

	@Transient
	public MultipartFile getThemePcImgFile() {
		return themePcImgFile;
	}

	public void setThemePcImgFile(MultipartFile themePcImgFile) {
		this.themePcImgFile = themePcImgFile;
	}

	@Transient
	public MultipartFile getThemeMobileImgFile() {
		return themeMobileImgFile;
	}

	public void setThemeMobileImgFile(MultipartFile themeMobileImgFile) {
		this.themeMobileImgFile = themeMobileImgFile;
	}

	@Column(name="intro_mobile")
	public String getIntroMobile() {
		return introMobile;
	}

	public void setIntroMobile(String introMobile) {
		this.introMobile = introMobile;
	}
	
	@Transient
	public boolean isExpire() {
		return expire;
	}

	public void setExpire(boolean expire) {
		this.expire = expire;
	}

	@Column(name="custom_content")
	public String getCustomContent() {
		return customContent;
	}

	public void setCustomContent(String customContent) {
		this.customContent = customContent;
	}

	@Column(name="template_type")
	public Integer getTemplateType() {
		return templateType;
	}

	public void setTemplateType(Integer templateType) {
		this.templateType = templateType;
	}

	@Column(name="intro_m_color")
	public String getIntroMColor() {
		return introMColor;
	}

	public void setIntroMColor(String introMColor) {
		this.introMColor = introMColor;
	}

	@Column(name="is_intro_m_show")
	public Long getIsIntroMShow() {
		return isIntroMShow;
	}

	public void setIsIntroMShow(Long isIntroMShow) {
		this.isIntroMShow = isIntroMShow;
	}
	
	

} 
