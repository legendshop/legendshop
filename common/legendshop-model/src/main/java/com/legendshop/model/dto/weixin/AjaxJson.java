package com.legendshop.model.dto.weixin;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;

public class AjaxJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean success = true;// 是否成功
	private String msg = "操作成功";// 提示信息
	private String successValue; //成功的结果信息
	private JSONObject obj = null;// 其他信息

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public JSONObject getObj() {
		return obj;
	}

	public void setObj(JSONObject obj) {
		this.obj = obj;
	}
	

	@Override
	public String toString() {

		StringBuffer sb = new StringBuffer();

		sb.append("AjaxJson [success=").append(success).append(",msg=")
				.append(msg);

		if (obj != null) {
			sb.append(", obj=").append(obj.toJSONString());
		}
		sb.append("]");

		return sb.toString();
	}

	public String getSuccessValue() {
		return successValue;
	}

	public void setSuccessValue(String successValue) {
		this.successValue = successValue;
	}

}
