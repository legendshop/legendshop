package com.legendshop.model.dto.weibo;

/**
 * 微博响应消息
 * @author 开发很忙
 */
public class WeiboReponseMsg {
	
	public static final int SUCCESS = 0;
	
	/** 请求url */
	private String request;
	
	/** 错误码 */
	private Integer error_code;
	
	/** 响应消息 */
	private String error;

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Integer getError_code() {
		return error_code;
	}

	public void setError_code(Integer error_code) {
		this.error_code = error_code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isSuccess(){
		
		if(null == error_code){
			return true;
		}
		
		return SUCCESS == error_code;
	}
}
