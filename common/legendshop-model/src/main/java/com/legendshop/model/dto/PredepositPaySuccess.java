package com.legendshop.model.dto;


/**
 * 预付款支付成功
 * @author Tony
 *
 */
public class PredepositPaySuccess {

	/**  支付结算单据流水号  */
	private String subSettlementSn;
	
	/**  预付款金额或金币  */
	private Double pdAmount;
	
	/** 用户ID */
	private String userId;
	
	private String result;
	/**余额支付方式:1：预付款  2：金币**/
	private Integer prePayType;

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public Double getPdAmount() {
		return pdAmount;
	}

	public void setPdAmount(Double pdAmount) {
		this.pdAmount = pdAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public PredepositPaySuccess(String subSettlementSn, Double pdAmount, String userId,Integer prePayType) {
		this.subSettlementSn = subSettlementSn;
		this.pdAmount = pdAmount;
		this.userId = userId;
		this.prePayType=prePayType;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Integer getPrePayType() {
		return prePayType;
	}

	public void setPrePayType(Integer prePayType) {
		this.prePayType = prePayType;
	}

	@Override
	public String toString() {
		return "PredepositPaySuccess [subSettlementSn=" + subSettlementSn + ", pdAmount=" + pdAmount + ", userId="
				+ userId + ", result=" + result + ", prePayType=" + prePayType + "]";
	} 
	
	
	
	

}
