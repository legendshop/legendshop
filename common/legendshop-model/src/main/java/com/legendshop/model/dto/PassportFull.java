package com.legendshop.model.dto;
import java.io.Serializable;
import java.util.Date;

import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.PassportSub;

/**
 * 第三方登录通行证，记录会员与其他社区的帐户关联关系
 * 完整的PassportFull, passportSub 和 passport 一对一联合
 */
public class PassportFull implements Serializable {

	private static final long serialVersionUID = 1769261025887936143L;
	
	/** passportSubID */
	private Long id;

	/** passPortId */
	private Long passPortId; 
		
	/** 用户ID */
	private String userId; 
	
	/** 用户名称 */
	private String userName; 
	
	/** openId */
	private String openId;
	
	/** unionid */
	private String unionid;
	
	/** 用户的第三方授权凭证, 目前微信小程序登陆没有这个  **/
	private String accessToken;
	
	/** 昵称，微信/QQ或微博名字 */
	private String nickName; 
	
	/** 头像 */
	private String headPortraitUrl;
	
	/** 性别 */
	private String gender;
	
	/** 国家 */
	private String country;
	
	/** 省份 */
	private String province;
	
	/** 城市 */
	private String city;
	
	/**  第三方类型 PassportTypeEnum*/
	private String type;
	
	/** 来源 PassportSourceEnum */
	private String source;
	
	/** 属性 */
	private String props; 
		
	/** 建立时间 */
	private Date createTime; 

	public PassportFull() {
		super();
    }

	public PassportFull(Passport passport, PassportSub passportSub) {
		this.setId(passportSub.getId());
		this.setPassPortId(passport.getPassPortId());
		this.setUserId(passport.getUserId());
		this.setUserName(passport.getUserName());
		this.setAccessToken(passportSub.getAccessToken());
		this.setOpenId(passportSub.getOpenId());
		this.setUnionid(passport.getUnionid());
		this.setNickName(passport.getNickName());
		this.setGender(passport.getGender());
		this.setCountry(passport.getCountry());
		this.setProvince(passport.getProvince());
		this.setCity(passport.getCity());
		this.setHeadPortraitUrl(passport.getHeadPortraitUrl());
		this.setProps(passport.getProps());
		this.setType(passport.getType());
		this.setSource(passportSub.getSource());
		this.setCreateTime(passportSub.getCreateTime());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPassPortId() {
		return passPortId;
	}

	public void setPassPortId(Long passPortId) {
		this.passPortId = passPortId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeadPortraitUrl() {
		return headPortraitUrl;
	}

	public void setHeadPortraitUrl(String headPortraitUrl) {
		this.headPortraitUrl = headPortraitUrl;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getProps() {
		return props;
	}

	public void setProps(String props) {
		this.props = props;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
} 
