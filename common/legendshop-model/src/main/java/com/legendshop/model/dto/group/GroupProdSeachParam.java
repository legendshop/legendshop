/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

/**
 * 团购列表查询
 *
 */
public class GroupProdSeachParam {

	/** 排序. */
	private String orders;

	/** 1级分类id */
	private Long firstCid;
	
	/** 2级分类id */
	private Long twoCid;
	
	/** 3级分类id */
	private Long thirdCid;

	/** 搜索关键字 */
	private String groupKeyWord;
	
	/** 团购时间表 */
	private Integer timeTab;//0没开始，1进行中，2已经结束

	public String getOrders() {
		return orders;
	}

	public void setOrders(String orders) {
		this.orders = orders;
	}

	public String getGroupKeyWord() {
		return groupKeyWord;
	}

	public void setGroupKeyWord(String groupKeyWord) {
		this.groupKeyWord = groupKeyWord;
	}

	public Long getFirstCid() {
		return firstCid;
	}

	public void setFirstCid(Long firstCid) {
		this.firstCid = firstCid;
	}

	public Long getTwoCid() {
		return twoCid;
	}

	public void setTwoCid(Long twoCid) {
		this.twoCid = twoCid;
	}

	public Long getThirdCid() {
		return thirdCid;
	}

	public void setThirdCid(Long thirdCid) {
		this.thirdCid = thirdCid;
	}

	public Integer getTimeTab() {
		return timeTab;
	}

	public void setTimeTab(Integer timeTab) {
		this.timeTab = timeTab;
	}
}
