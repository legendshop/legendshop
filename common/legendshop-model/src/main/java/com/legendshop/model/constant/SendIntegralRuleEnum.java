package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;


/**
 * 赠送积分规则
 *
 */
public enum SendIntegralRuleEnum implements IntegerEnum {
	
	USER_REGISTER(0), // 用户注册
	
	USER_LOGIN(1), //用户登录
	
	VAL_EMAIL(2), //校验邮箱
	
	VAL_PHONE(3), //校验手机
	
	ORDER_COMMENT(4), //订单评论
	
	RECOMMEND_USER(5), //推荐用户
	
	SUN_ORDER(6), //晒单
	
	ORDER_CONSUMER(7) //订单送积分
	
	
	;


	/** The num. */
	private Integer type;

	/**
	 * Instantiates a new consult type enum.
	 * 
	 * @param num
	 *            the num
	 */
	SendIntegralRuleEnum(Integer type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	@Override
	public Integer value() {
		return type;
	}

}
