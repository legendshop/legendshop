package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户可评论的商品
 */
@ApiModel(value="用户商品评论Dto") 
public class UserCommentProdDto implements Serializable {

	private static final long serialVersionUID = 5030172556415667984L;

	/** 评论ID **/
	@ApiModelProperty(value="主键")  
	private Long id;
	
	/** 订单项Id */
	@ApiModelProperty(value="订单项Id")  
	private Long subItemId;
	
	/** 商品ID */
	@ApiModelProperty(value="商品ID")  
	private Long prodId;
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")  
	private String prodName;
	
	/** 商品属性 */
	@ApiModelProperty(value="商品属性")  
	private String attribute;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片")  
	private String pic;
	
	/** 购物时间 */
	@ApiModelProperty(value="购物时间")  
	private Date buyTime;
	
	/** 评论状态 : 是否已评论 */
	@ApiModelProperty(value="评论状态 : 是否已评论")
	private Integer commSts;
	
	/** 评论记录状态 1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1*/
	@ApiModelProperty(value="评论记录状态 1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1")  
	private Integer status;
	
	/** 是否已追加评论 */
	@ApiModelProperty(value="是否已追加评论")  
	private Boolean isAddComm;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Date getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	public Integer getCommSts() {
		return commSts;
	}

	public void setCommSts(Integer commSts) {
		this.commSts = commSts;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getIsAddComm() {
		return isAddComm;
	}

	public void setIsAddComm(Boolean isAddComm) {
		this.isAddComm = isAddComm;
	}
}
