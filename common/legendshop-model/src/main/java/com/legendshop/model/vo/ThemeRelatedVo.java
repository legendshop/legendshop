package com.legendshop.model.vo;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * 专题
 *
 */
public class ThemeRelatedVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7740229806536175912L;
	private String img1;
	private String img2;
	private String img3;
	private Long themeId;
	private Long relatedId1;
	private Long relatedId2;
	private Long relatedId3;
	private Long mRelatedThemeId1;
	private Long mRelatedThemeId2;
	private Long mRelatedThemeId3;
	private Long relatedThemeId1;
	private Long relatedThemeId2;
	private Long relatedThemeId3;
	private String link1;
	private String link2;
	private String link3;
	private String linkM1;
	private String linkM2;
	private String linkM3;
	private MultipartFile imgFile1;
	private MultipartFile imgFile2;
	private MultipartFile imgFile3;

	public Long getmRelatedThemeId1() {
		return mRelatedThemeId1;
	}

	public void setmRelatedThemeId1(Long mRelatedThemeId1) {
		this.mRelatedThemeId1 = mRelatedThemeId1;
	}

	public Long getmRelatedThemeId2() {
		return mRelatedThemeId2;
	}

	public void setmRelatedThemeId2(Long mRelatedThemeId2) {
		this.mRelatedThemeId2 = mRelatedThemeId2;
	}

	public Long getmRelatedThemeId3() {
		return mRelatedThemeId3;
	}

	public void setmRelatedThemeId3(Long mRelatedThemeId3) {
		this.mRelatedThemeId3 = mRelatedThemeId3;
	}

	public Long getRelatedThemeId1() {
		return relatedThemeId1;
	}

	public void setRelatedThemeId1(Long relatedThemeId1) {
		this.relatedThemeId1 = relatedThemeId1;
	}

	public Long getRelatedThemeId2() {
		return relatedThemeId2;
	}

	public void setRelatedThemeId2(Long relatedThemeId2) {
		this.relatedThemeId2 = relatedThemeId2;
	}

	public Long getRelatedThemeId3() {
		return relatedThemeId3;
	}

	public void setRelatedThemeId3(Long relatedThemeId3) {
		this.relatedThemeId3 = relatedThemeId3;
	}

	public String getLink1() {
		return link1;
	}

	public void setLink1(String link1) {
		this.link1 = link1;
	}

	public String getLink2() {
		return link2;
	}

	public void setLink2(String link2) {
		this.link2 = link2;
	}

	public String getLink3() {
		return link3;
	}

	public void setLink3(String link3) {
		this.link3 = link3;
	}

	public String getImg1() {
		return img1;
	}
	public void setImg1(String img1) {
		this.img1 = img1;
	}
	public String getImg2() {
		return img2;
	}
	public void setImg2(String img2) {
		this.img2 = img2;
	}
	public String getImg3() {
		return img3;
	}
	public void setImg3(String img3) {
		this.img3 = img3;
	}
	public Long getThemeId() {
		return themeId;
	}
	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}
	public Long getRelatedId1() {
		return relatedId1;
	}
	public void setRelatedId1(Long relatedId1) {
		this.relatedId1 = relatedId1;
	}
	public Long getRelatedId2() {
		return relatedId2;
	}
	public void setRelatedId2(Long relatedId2) {
		this.relatedId2 = relatedId2;
	}
	public Long getRelatedId3() {
		return relatedId3;
	}
	public void setRelatedId3(Long relatedId3) {
		this.relatedId3 = relatedId3;
	}

	public MultipartFile getImgFile1() {
		return imgFile1;
	}
	public void setImgFile1(MultipartFile imgFile1) {
		this.imgFile1 = imgFile1;
	}
	public MultipartFile getImgFile2() {
		return imgFile2;
	}
	public void setImgFile2(MultipartFile imgFile2) {
		this.imgFile2 = imgFile2;
	}
	public MultipartFile getImgFile3() {
		return imgFile3;
	}
	public void setImgFile3(MultipartFile imgFile3) {
		this.imgFile3 = imgFile3;
	}
	public String getLinkM1() {
		return linkM1;
	}
	public void setLinkM1(String linkM1) {
		this.linkM1 = linkM1;
	}
	public String getLinkM2() {
		return linkM2;
	}
	public void setLinkM2(String linkM2) {
		this.linkM2 = linkM2;
	}
	public String getLinkM3() {
		return linkM3;
	}
	public void setLinkM3(String linkM3) {
		this.linkM3 = linkM3;
	}

}
