/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;

/**
 * 邮件载体
 */
public class MailInfo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5571413803153472735L;

	/** The to. */
	private final String to;

	/** The subject. */
	private final String subject;

	/** The text. */
	private final String text;
	
	// 发送用户名称
	/** The send name. */
	private final String sendName ; 
		
	private String recieveName;
	// 用户等级
	/** The user level. */
	private final Integer userLevel ; 
	
	private final Integer type ; 
	
	/** 邮件类型. */
	private final String category;
	
	private  String mailCode;
	
	public MailInfo(String to, String subject, String text, String sendName, Integer userLevel, String category, Integer type) {
		this.to = to;
		this.subject = subject;
		this.text = text;
		this.sendName = sendName;
		this.userLevel = userLevel;
		this.category = category;
		this.type = type;
	}

	public String getTo() {
		return to;
	}

	public String getSubject() {
		return subject;
	}

	public String getText() {
		return text;
	}

	public String getSendName() {
		return sendName;
	}

	public Integer getUserLevel() {
		return userLevel;
	}

	public String getCategory() {
		return category;
	}

	public Integer getType() {
		return type;
	}

	public String getMailCode() {
		return mailCode;
	}

	public void setMailCode(String mailCode) {
		this.mailCode = mailCode;
	}

	public String getRecieveName() {
		return recieveName;
	}

	public void setRecieveName(String recieveName) {
		this.recieveName = recieveName;
	}

	
	
	

}
