package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 支付结算单据
 *
 */
@Entity
@Table(name = "ls_sub_settlement")
public class SubSettlement implements GenericEntity<Long> {

	private static final long serialVersionUID = 6665917191772945065L;

	/** 支付结算单据ID */
	private Long subSettlementId; 
		
	/** 流水号(支付宝/银行) */
	private String flowTradeNo; 
	
	/**  商户系统内部的订单号[微信]   */
	private String subSettlementSn;
	
	/*  是否全额支付 */
	private boolean fullPay;
		
	/** 支付的金额  **/
	private Double cashAmount; 
	
	/** 预存款的金额  **/
	private Double pdAmount=0d;
		
	/** 支付类型 */
	private String payTypeId; 
	
	/** 支付名称 */
	private String payTypeName; 
	
	/** 用户ID */
	private String userId; 
		
	/** 是否清算(0:N ; 1:Y) */
	private boolean isClearing; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 清算时间 */
	private Date clearingTime; 
	
	private String payUserInfo;
	
	/** 是否预售 */
	private boolean isPresell;
	
	/** 预售支付: PayPctTypeEnum **/
	private Integer payPctType;  
	
	/**余额支付类型(1:预付款  2:金币)**/
	private Integer prePayType;
	
	/** 单据类型 SubSettlementTypeEnum */
	private String type;
	
	/** 支付来源 PaySourceEnum */
	private String settlementFrom;
	
	public SubSettlement() {
    }
		
	@Id
	@Column(name = "sub_settlement_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_SETTLEMENT_SEQ")
	public Long  getSubSettlementId(){
		return subSettlementId;
	} 
		
	public void setSubSettlementId(Long subSettlementId){
			this.subSettlementId = subSettlementId;
		}
		
    @Column(name = "flow_trade_no")
	public String  getFlowTradeNo(){
		return flowTradeNo;
	} 
		
	public void setFlowTradeNo(String flowTradeNo){
			this.flowTradeNo = flowTradeNo;
		}
		
    @Column(name = "cash_amount")
	public Double  getCashAmount(){
		return cashAmount;
	} 
		
	public void setCashAmount(Double cashAmount){
			this.cashAmount = cashAmount;
		}
		
    @Column(name = "pay_type_id")
	public String  getPayTypeId(){
		return payTypeId;
	} 
		
	public void setPayTypeId(String payTypeId){
			this.payTypeId = payTypeId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "is_clearing")
	public boolean  getIsClearing(){
		return isClearing;
	} 
		
	public void setIsClearing(boolean isClearing){
			this.isClearing = isClearing;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "clearing_time")
	public Date  getClearingTime(){
		return clearingTime;
	} 
		
	public void setClearingTime(Date clearingTime){
			this.clearingTime = clearingTime;
		}
	
	@Transient
	public Long getId() {
		return subSettlementId;
	}
	
	public void setId(Long id) {
		subSettlementId = id;
	}

	@Column(name = "pay_user_info")
	public String getPayUserInfo() {
		return payUserInfo;
	}

	@Column(name = "pay_user_info")
	public void setPayUserInfo(String payUserInfo) {
		this.payUserInfo = payUserInfo;
	}
	
	@Column(name = "pay_type_name")
	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	
	@Column(name = "sub_settlement_sn")
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	@Column(name = "pd_amount")
	public Double getPdAmount() {
		return pdAmount;
	}

	public void setPdAmount(Double pdAmount) {
		this.pdAmount = pdAmount;
	}

	@Column(name = "is_presell")
	public boolean isPresell() {
		return isPresell;
	}

	public void setPresell(boolean isPresell) {
		this.isPresell = isPresell;
	}

	@Column(name = "pay_pct_type")
	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	@Column(name = "pre_pay_type")
	public Integer getPrePayType() {
		return prePayType;
	}

	public void setPrePayType(Integer prePaytype) {
		this.prePayType = prePaytype;
	}
	
	
	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
    @Column(name = "settlement_from")
	public String getSettlementFrom() {
		return settlementFrom;
	}

	public void setSettlementFrom(String settlementFrom) {
		this.settlementFrom = settlementFrom;
	}
	
	@Column(name = "full_pay")
	public boolean isFullPay() {
		return fullPay;
	}

	public void setFullPay(boolean fullPay) {
		this.fullPay = fullPay;
	}
	
	

} 
