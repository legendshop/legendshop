/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品IdDTO,记录商品ID列表，用于更新Index.
 */
public class ProductIdDto implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5943257077106143379L;
	/** The prod id. */
	private List<Long> prodIdList = new ArrayList<Long>();
	
	/**
	 * Instantiates a new product id dto.
	 */
	public ProductIdDto(){
		
	}
	
	/**
	 * Instantiates a new product id dto.
	 *
	 * @param prodIdList the prod id list
	 */
	public ProductIdDto(List<Long> prodIdList){
		this.prodIdList = prodIdList;
	}
	
	/**
	 * Instantiates a new product id dto.
	 *
	 * @param prodId the prod id
	 */
	public ProductIdDto(Long prodId){
		prodIdList.add(prodId);
	}
	


	/**
	 * add the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void addProdId(Long prodId) {
		this.prodIdList.add(prodId);
	}

	/**
	 * Gets the prod id list.
	 *
	 * @return the prod id list
	 */
	public List<Long> getProdIdList() {
		return prodIdList;
	}

	/**
	 * Sets the prod id list.
	 *
	 * @param prodIdList the new prod id list
	 */
	public void setProdIdList(List<Long> prodIdList) {
		this.prodIdList = prodIdList;
	}
	
}
