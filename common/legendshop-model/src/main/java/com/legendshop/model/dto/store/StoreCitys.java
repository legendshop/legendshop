/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.store;

/**
 * 城市里的门店.
 */
public class StoreCitys {
	
	/** 门店id */
	private Long storeId;
	
	/** 地址 */
	private String address;
	
	/** 门店名字 */
	private String storeName;
	
	/** 门店详细地址 */
	private String shopAddr;

	/**
	 * Gets the store id.
	 *
	 * @return the store id
	 */
	public Long getStoreId() {
		return storeId;
	}

	/**
	 * Sets the store id.
	 *
	 * @param storeId the store id
	 */
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the store name.
	 *
	 * @return the store name
	 */
	public String getStoreName() {
		return storeName;
	}

	/**
	 * Sets the store name.
	 *
	 * @param storeName the store name
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getShopAddr() {
		return shopAddr;
	}

	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}

}
