package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *种草社区关注表
 */
@Entity
@Table(name = "ls_grass_concern")
public class GrassConcern implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 种草文章用户ID */
	private String grauserId; 
		
	/** 用户id */
	private String userId; 
		
	/** 创建时间 */
	private Date createTime; 
		
	
	public GrassConcern() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GRASS_CONCERN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "grauser_id")
	public String  getGrauserId(){
		return grauserId;
	} 
		
	public void setGrauserId(String grauserId){
			this.grauserId = grauserId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	


} 
