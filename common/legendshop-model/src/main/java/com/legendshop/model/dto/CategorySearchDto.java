/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 分类搜索DTO
 * 
 */
public class CategorySearchDto  implements Serializable {

	private static final long serialVersionUID = 6835019257167427172L;
	
	//分类ID
	private Long categoryId;
	
	//面包屑导航名称
	private String navigationName;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getNavigationName() {
		return navigationName;
	}

	public void setNavigationName(String navigationName) {
		this.navigationName = navigationName;
	}
	
	
	
}