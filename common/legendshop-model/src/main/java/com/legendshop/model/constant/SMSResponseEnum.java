/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 短信返回状态Enum
 */

public enum SMSResponseEnum implements IntegerEnum {
	//important： 大于0 	表示短信发送数量
	
	//没有该用户账户
	NO_EXISTS_USER(-1 ),
	
	//密钥不正确
	INCORRECT_PASSWORD(-2),
	
	//短信数量不足
	 INSUFFICIENT_SMS(-3),
	
	//该用户被禁用
	 FORBIDDEN(-11),
	
	//短信内容出现非法字符
	ILLEGAL_SMS(-14),
	
	//手机号格式不正确
	INCORRECT_PHONE_FORMAT(-4),
	
	//手机号码为空
	PHONE_NULL(-41),
	
	//短信内容为空
	CONTENT_NULL(-42),
	
	//短信签名格式不正确
	SIGNATURE_ILLGEAL(-51)
	
	;

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new comment type enum.
	 * 
	 * @param num
	 *            the num
	 */
	SMSResponseEnum(Integer num) {
		this.num = num;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

	}
}
