/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.coupon;

import java.util.List;

import com.legendshop.model.entity.UserCoupon;

/**
 * 用户优惠卷集合Dto
 *
 */
public class UserCouponListDto {

	private List<UserCoupon> usableUserCouponList ;//可用的优惠券和红包
	
	private List<UserCoupon> disableUserCouponList; //不可用的优惠券和红包

	public List<UserCoupon> getUsableUserCouponList() {
		return usableUserCouponList;
	}

	public void setUsableUserCouponList(List<UserCoupon> usableUserCouponList) {
		this.usableUserCouponList = usableUserCouponList;
	}

	public List<UserCoupon> getDisableUserCouponList() {
		return disableUserCouponList;
	}

	public void setDisableUserCouponList(List<UserCoupon> disableUserCouponList) {
		this.disableUserCouponList = disableUserCouponList;
	}
	
	
	
}
