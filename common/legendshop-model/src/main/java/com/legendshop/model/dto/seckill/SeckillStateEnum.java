/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

/**
 * 这个类是将秒杀的信息封装成数据字典.
 *
 * @author Tony
 */
public enum SeckillStateEnum {

	/** The time out. */
	TIME_OUT(-5, "活动已过期"),

	/** The ready. */
	READY(-6, "未开始"),

	/** The self seckill. */
	SELF_SECKILL(-7, "不能参与自己的秒杀!"),

	/** The not find. */
	NOT_FIND(-4, "该活动已下线"),

	/** The sell out. */
	SELL_OUT(-3, "已售罄"),

	/** The noresult. */
	NORESULT(-2, "系统繁忙,请稍候！"),

	/** The nobegin. */
	NOBEGIN(-1, "秒杀还没开始"),

	/** The end. */
	END(0, "秒杀结束"),

	/** The success. */
	SUCCESS(1, "成功"),

	/** The SUCCES s2. */
	SUCCESS2(5, "已参与过该活动"),

	/** The repeatseckill. */
	REPEATSECKILL(2, "重复秒杀"),

	/** The urlchanged. */
	URLCHANGED(3, "URL被篡改,不正常访问"),

	/** The nologin. */
	NOLOGIN(4, "未登录");

	/** The state. */
	private int state;

	/** The stateinfo. */
	private String stateinfo;

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state
	 *            the state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * Gets the stateinfo.
	 *
	 * @return the stateinfo
	 */
	public String getStateinfo() {
		return stateinfo;
	}

	/**
	 * Sets the stateinfo.
	 *
	 * @param stateinfo
	 *            the stateinfo
	 */
	public void setStateinfo(String stateinfo) {
		this.stateinfo = stateinfo;
	}

	/**
	 * The Constructor.
	 *
	 * @param state
	 *            the state
	 * @param stateinfo
	 *            the stateinfo
	 */
	SeckillStateEnum(int state, String stateinfo) {
		this.state = state;
		this.stateinfo = stateinfo;
	}

	/**
	 * Gets the state.
	 *
	 * @param index
	 *            the index
	 * @return the state
	 */
	public static SeckillStateEnum getState(int index) {
		for (SeckillStateEnum seckillStateEnum : values()) {
			if (seckillStateEnum.getState() == index) {
				return seckillStateEnum;
			}
		}
		return null;
	}
}
