package com.legendshop.model.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户发表评论页面Dto
 */
@ApiModel(value="用户发表评论页面Dto")
public class UserCommentDto {
	
	/** 商品Id */
	@ApiModelProperty(value="商品Id")
	private Long prodId;
	
	/** 订单项Id */
	@ApiModelProperty(value="订单项Id")
	private Long subItemId;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;
	
	/** 商品名 */
	@ApiModelProperty(value="商品名")
	private String prodName;
	
	/** 产品动态属性 */
	@ApiModelProperty(value="产品动态属性 ")
	private String attribute;
	
	/** 购买时间 */
	@ApiModelProperty(value="购买时间")
	private Date buyTime;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Date getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}
}
