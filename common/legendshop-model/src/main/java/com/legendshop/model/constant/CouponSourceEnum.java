/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 礼券
 * 领取来源
 */
public enum CouponSourceEnum implements StringEnum {

	PC("PC"),

	APP("APP"),
	
	WEIXIN("WEIXIN"), 
	
	DRAW("DRAW");

	/** The num. */
	private String type;

	CouponSourceEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}

}
