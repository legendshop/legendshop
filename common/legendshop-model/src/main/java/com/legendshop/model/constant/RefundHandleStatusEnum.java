/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 处理退款状态: 0:退款处理中 1:退款成功 -1:退款失败
 */
public enum RefundHandleStatusEnum implements StringEnum {
	
	/* 退款中 */
	PROCESSING("0"),
	
	/* 退款成功 */
	SUCCESS("1"),
	
	/* 退款失败 */
	FAILED("-1");

	private String status;

	RefundHandleStatusEnum(String status) {
		this.status = status;
	}

	@Override
	public String value() {
		return status;
	}

}
