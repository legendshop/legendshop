package com.legendshop.model.entity.draw;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *抽奖中奖表
 */
@Entity
@Table(name = "ls_draw_win_record")
public class DrawWinRecord implements GenericEntity<Long> {

	private static final long serialVersionUID = 7736620275184628256L;

	/** id */
	private Long id; 
		
	/** 用户id */
	private String userId; 
		
	/** 手机号码 */
	private String mobile; 
		
	/** 姓名 */
	private String cusName; 
		
	/** 微信openid */
	private String weixinOpenid; 
		
	/** 微信昵称 */
	private String weixinNickname; 
		
	/** 邮寄地址 */
	private String sendAddress; 
		
	/** 活动ID */
	private Long actId; 
		
	/** 发货状态 */
	private Integer sendStatus; 
		
	/** 快递单号 */
	private String courierNumber; 
		
	/** 快递公司 */
	private String courierCompany; 
	
	private Long courierId;
		
	/** 抽奖时间 */
	private Date awardsTime; 
	
	/**中奖描述**/
	private String winDes;
	
	private String actName;
	//查询时间
	private Date startTime; 
	private Date endTime; 
	
	/**奖项类型    1：实体   2：预付款    3:积分**/
	private Integer awardsType;

	private Integer flag;
	
	
	public DrawWinRecord() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DRAW_WIN_RECORD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "mobile")
	public String  getMobile(){
		return mobile;
	} 
		
	public void setMobile(String mobile){
			this.mobile = mobile;
		}
		
    @Column(name = "cus_name")
	public String  getCusName(){
		return cusName;
	} 
		
	public void setCusName(String cusName){
			this.cusName = cusName;
		}
		
    @Column(name = "weixin_openid")
	public String  getWeixinOpenid(){
		return weixinOpenid;
	} 
		
	public void setWeixinOpenid(String weixinOpenid){
			this.weixinOpenid = weixinOpenid;
		}
		
    @Column(name = "weixin_nickname")
	public String  getWeixinNickname(){
		return weixinNickname;
	} 
		
	public void setWeixinNickname(String weixinNickname){
			this.weixinNickname = weixinNickname;
		}
		
    @Column(name = "send_address")
	public String  getSendAddress(){
		return sendAddress;
	} 
		
	public void setSendAddress(String sendAddress){
			this.sendAddress = sendAddress;
		}
		
		
    @Column(name = "act_id")
	public Long  getActId(){
		return actId;
	} 
		
	public void setActId(Long actId){
			this.actId = actId;
		}
		
    @Column(name = "send_status")
	public Integer  getSendStatus(){
		return sendStatus;
	} 
		
	public void setSendStatus(Integer sendStatus){
			this.sendStatus = sendStatus;
		}
		
    @Column(name = "courier_number")
	public String  getCourierNumber(){
		return courierNumber;
	} 
		
	public void setCourierNumber(String courierNumber){
			this.courierNumber = courierNumber;
		}
		
    @Column(name = "courier_company")
	public String  getCourierCompany(){
		return courierCompany;
	} 
		
	public void setCourierCompany(String courierCompany){
			this.courierCompany = courierCompany;
		}
	
    @Column(name = "courier_id")
	public Long getCourierId() {
		return courierId;
	}

	public void setCourierId(Long courierId) {
		this.courierId = courierId;
	}
		
    @Column(name = "awards_time")
	public Date  getAwardsTime(){
		return awardsTime;
	} 
		
	public void setAwardsTime(Date awardsTime){
			this.awardsTime = awardsTime;
		}
    
	@Transient
	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName;
	}

	
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column(name = "awards_type")
	public Integer getAwardsType() {
		return awardsType;
	}

	public void setAwardsType(Integer awardsType) {
		this.awardsType = awardsType;
	}

	@Column(name = "win_des")
	public String getWinDes() {
		return winDes;
	}

	public void setWinDes(String winDes) {
		this.winDes = winDes;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}



	
	


} 
