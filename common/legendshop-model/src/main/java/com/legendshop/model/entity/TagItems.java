package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *挑选的产品和内容块的对应关系
 */
@Entity
@Table(name = "ls_tag_items")
public class TagItems implements GenericEntity<Long> {

	private static final long serialVersionUID = -441346486297641996L;

	/** ID */
	private Long itemId; 
		
	/** 所属标签ID */
	private Long tagId; 
		
	/** TagItem名字 */
	private String itemName; 
		
	/** 建立时间 */
	private Date createTime; 
		
	
	public TagItems() {
    }
		
	@Id
	@Column(name = "item_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TAG_ITEMS_SEQ")
	public Long  getItemId(){
		return itemId;
	} 
		
	public void setItemId(Long itemId){
			this.itemId = itemId;
		}
		
    @Column(name = "tag_id")
	public Long  getTagId(){
		return tagId;
	} 
		
	public void setTagId(Long tagId){
			this.tagId = tagId;
		}
		
    @Column(name = "item_name")
	public String  getItemName(){
		return itemName;
	} 
		
	public void setItemName(String itemName){
			this.itemName = itemName;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	
	@Transient
	public Long getId() {
		return itemId;
	}
	
	public void setId(Long id) {
		itemId = id;
	}


} 
