/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ShopDetailView;

/**
 * 商家DTO
 *
 */
public class ShopListDto implements Serializable,GenericEntity<Long>  {

	private static final long serialVersionUID = 6830009574780972734L;
	
	
    protected ShopDetailView shopDetailView; 
    
    /** The shop prod list. */
    protected List<ProductDetail> productDetail;


	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long arg0) {
		// TODO Auto-generated method stub
		
	}

	public ShopDetailView getShopDetailView() {
		return shopDetailView;
	}

	public void setShopDetailView(ShopDetailView shopDetailView) {
		this.shopDetailView = shopDetailView;
	}

	public List<ProductDetail> getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(List<ProductDetail> productDetail) {
		this.productDetail = productDetail;
	}



	
	

}
