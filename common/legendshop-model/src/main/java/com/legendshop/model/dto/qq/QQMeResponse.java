package com.legendshop.model.dto.qq;

/**
 * 获取QQ授权用户的openId
 * 来源接口: https://graph.qq.com/oauth2.0/me
 * @author 开发很忙
 */
public class QQMeResponse extends QQReponseMsg{
	
	/** 申请登录时的APPID */
	private String client_id;
	
	/** 用户的openId */
	private String openid;
	
	/** 用户的unionid */
	private String unionid;

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
}
