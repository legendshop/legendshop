package com.legendshop.model.vo;

import java.io.Serializable;

/**
 * 微信代码
 *
 */
public class WechatCodeVO implements Serializable {
	private static final long serialVersionUID = -4444221475573401768L;

	private String wechatServiceCode;

	private String wechatOrderCode;

	public String getWechatServiceCode() {
		return wechatServiceCode;
	}

	public WechatCodeVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WechatCodeVO(String wechatServiceCode, String wechatOrderCode) {
		super();
		this.wechatServiceCode = wechatServiceCode;
		this.wechatOrderCode = wechatOrderCode;
	}

	public void setWechatServiceCode(String wechatServiceCode) {
		this.wechatServiceCode = wechatServiceCode;
	}

	public String getWechatOrderCode() {
		return wechatOrderCode;
	}

	public void setWechatOrderCode(String wechatOrderCode) {
		this.wechatOrderCode = wechatOrderCode;
	}

}
