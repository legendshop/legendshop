package com.legendshop.model.dto.marketing;


/*
 * 促销活动规则
 */
public class MarketingMjRule {
	
	/**
	 * 订单购满金额下限
	 */
	private Double fullAmount;

	
	private Double offAmount;
	
	private Integer calType;

	public Integer getCalType() {
		return calType;
	}

	public void setCalType(Integer calType) {
		this.calType = calType;
	}

	public Double getFullAmount() {
		return fullAmount;
	}

	public void setFullAmount(Double fullAmount) {
		this.fullAmount = fullAmount;
	}

	public Double getOffAmount() {
		return offAmount;
	}

	public void setOffAmount(Double offAmount) {
		this.offAmount = offAmount;
	}

	


}
