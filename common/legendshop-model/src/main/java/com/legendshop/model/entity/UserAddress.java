/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户订单地址
 */
@Entity
@Table(name = "ls_usr_addr")
@ApiModel(value="UserAddress地址") 
public class UserAddress implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6057981433703294161L;

	/** ID */
	@ApiModelProperty(value="地址Id")  
	private Long addrId;

	/** 用户ID */
	@ApiModelProperty(value="用户ID")  
	private String userId;

	/** 用户名称 */
	@ApiModelProperty(value="用户名称")  
	private String userName;

	/** 接受人名称 */
	@ApiModelProperty(value="接受人名称")  
	private String receiver;

	/** 地址 */
	@ApiModelProperty(value="订单地址 ") 
	private String subAdds;

	/** 邮编 */
	@ApiModelProperty(value="邮编 ") 
	private String subPost;

	/** 省份ID */
	@ApiModelProperty(value="省份ID") 
	private Integer provinceId;

	/** 城市ID */
	@ApiModelProperty(value="城市ID") 
	private Integer cityId;

	/** 区域ID */
	@ApiModelProperty(value="区域ID") 
	private Integer areaId;

	/** The province. */
	@ApiModelProperty(value="省份") 
	private String province;

	/** The city */
	@ApiModelProperty(value="城市") 
	private String city;

	/** The area */
	@ApiModelProperty(value="地区") 
	private String area;

	/** 联系人手机. */
	@ApiModelProperty(value="联系人手机") 
	private String mobile;

	/** 固话 */
	@ApiModelProperty(value="固话") 
	private String telphone;

	/** Email地址 */
	@ApiModelProperty(value="Email地址 ") 
	private String email;

	/** 状态,1正常，0无效 */
	@ApiModelProperty(value="状态,1正常，0无效") 
	private Integer status;

	/** 是否默认（常用）地址 */
	@ApiModelProperty(value="是否默认（常用）地址") 
	private String commonAddr;

	/**
	 * 地址别名
	 */
	@ApiModelProperty(value="地址别名") 
	private String aliasAddr;

	/** 建立时间 */
	@ApiModelProperty(value="建立时间") 
	private Date createTime;

	/** 版本号 */
	@ApiModelProperty(value="版本号") 
	private Long version;

	/** 更新时间 */
	@ApiModelProperty(value="更新时间") 
	private Date modifyTime;

	/** 收货地址 userAddress(json) **/
	@ApiModelProperty(value="收货地址 userAddress(json格式)") 
	private String addressData;

	@Id
	@Column(name = "addr_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USR_ADDR_SEQ")
	public Long getAddrId() {
		return addrId;
	}

	/**
	 * Sets the addr id.
	 * 
	 * @param addrId
	 *            the new addr id
	 */
	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the receiver.
	 * 
	 * @return the receiver
	 */
	@Column(name = "receiver")
	public String getReceiver() {
		return receiver;
	}

	/**
	 * Sets the receiver.
	 * 
	 * @param receiver
	 *            the new receiver
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	/**
	 * Gets the sub adds.
	 * 
	 * @return the sub adds
	 */
	@Column(name = "sub_adds")
	public String getSubAdds() {
		return subAdds;
	}

	/**
	 * Sets the sub adds.
	 * 
	 * @param subAdds
	 *            the new sub adds
	 */
	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}

	/**
	 * Gets the sub post.
	 * 
	 * @return the sub post
	 */
	@Column(name = "sub_post")
	public String getSubPost() {
		return subPost;
	}

	/**
	 * Sets the sub post.
	 * 
	 * @param subPost
	 *            the new sub post
	 */
	public void setSubPost(String subPost) {
		this.subPost = subPost;
	}

	/**
	 * Gets the province id.
	 * 
	 * @return the province id
	 */
	@Column(name = "province_id")
	public Integer getProvinceId() {
		return provinceId;
	}

	/**
	 * Sets the province id.
	 * 
	 * @param provinceId
	 *            the new province id
	 */
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * Gets the city id.
	 * 
	 * @return the city id
	 */
	@Column(name = "city_id")
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * Sets the city id.
	 * 
	 * @param cityId
	 *            the new city id
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * Gets the mobile.
	 * 
	 * @return the mobile
	 */
	@Column(name = "mobile")
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the mobile.
	 * 
	 * @param mobile
	 *            the new mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * Gets the telphone.
	 * 
	 * @return the telphone
	 */
	@Column(name = "telphone")
	public String getTelphone() {
		return telphone;
	}

	/**
	 * Sets the telphone.
	 * 
	 * @param telphone
	 *            the new telphone
	 */
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the common addr.
	 * 
	 * @return the common addr
	 */
	@Column(name = "common_addr")
	public String getCommonAddr() {
		return commonAddr;
	}

	/**
	 * Sets the common addr.
	 * 
	 * @param commonAddr
	 *            the new common addr
	 */
	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr;
	}

	/**
	 * Gets the creates the time.
	 * 
	 * @return the creates the time
	 */
	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * Sets the creates the time.
	 * 
	 * @param createTime
	 *            the new creates the time
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Transient
	@ApiModelProperty(value="地址Id")  
	public Long getId() {
		return addrId;
	}

	public void setId(Long id) {
		this.addrId = id;
	}

	/**
	 * Gets the area id.
	 * 
	 * @return the area id
	 */
	@Column(name = "area_id")
	public Integer getAreaId() {
		return areaId;
	}

	/**
	 * Sets the area id.
	 * 
	 * @param areaId
	 *            the new area id
	 */
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	@Column(name = "alias_addr")
	public String getAliasAddr() {
		return aliasAddr;
	}

	public void setAliasAddr(String aliasAddr) {
		this.aliasAddr = aliasAddr;
	}

	@Transient
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Transient
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Column(name = "version")
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Column(name = "modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Transient
	public String getAddressData() {
		return addressData;
	}

	public void setAddressData(String addressData) {
		this.addressData = addressData;
	}

}