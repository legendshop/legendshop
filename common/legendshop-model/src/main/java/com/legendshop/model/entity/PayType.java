/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 支付类型
 */
@Entity
@Table(name = "ls_pay_type")
public class PayType extends AbstractEntity implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 173116392190218430L;
	
	/** The pay id. */
	private Long payId;
	
	/** 支付方式ID. */
	private String payTypeId;
	
	/** 支付方式名称. */
	private String payTypeName;
	
	/** The memo. */
	private String memo;
	
	private Integer interfaceType;
	
	private Integer isEnable;
	
	private String paymentConfig;

	/**
	 * 默认构造函数.
	 */
	public PayType() {
	}

	/**
	 * 构造函数.
	 *
	 */
	public PayType(Long payId, String payTypeId) {
		this.payId = payId;
		this.payTypeId = payTypeId;
	}

	/**
	 * 构造函数
	 *
	 */
	public PayType(Long payId, String payTypeId, String payTypeName,  String memo) {
		this.payId = payId;
		this.payTypeId = payTypeId;
		this.payTypeName = payTypeName;
		this.memo = memo;
	}

	/**
	 * Gets the pay id.
	 * 
	 * @return the pay id
	 */
	@Id
	@Column(name = "pay_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PAY_TYPE_SEQ")
	public Long getPayId() {
		return this.payId;
	}

	/**
	 * Sets the pay id.
	 * 
	 * @param payId
	 *            the new pay id
	 */
	public void setPayId(Long payId) {
		this.payId = payId;
	}

	/**
	 * Gets the pay type id.
	 * 
	 * @return the pay type id
	 */
	@Column(name = "pay_type_id")
	public String getPayTypeId() {
		return this.payTypeId;
	}

	/**
	 * Sets the pay type id.
	 * 
	 * @param payTypeId
	 *            the new pay type id
	 */
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	/**
	 * Gets the pay type name.
	 * 
	 * @return the pay type name
	 */
	@Column(name = "pay_type_name")
	public String getPayTypeName() {
		return this.payTypeName;
	}

	/**
	 * Sets the pay type name.
	 * 
	 * @param payTypeName
	 *            the new pay type name
	 */
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	/**
	 * Gets the memo.
	 * 
	 * @return the memo
	 */
	@Column(name = "memo")
	public String getMemo() {
		return this.memo;
	}

	/**
	 * Sets the memo.
	 * 
	 * @param memo
	 *            the new memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Transient
	public Long getId() {
		return payId;
	}
	
	public void setId(Long id) {
		this.payId = id;
	}

	@Column(name = "interface_type")
	public Integer getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(Integer interfaceType) {
		this.interfaceType = interfaceType;
	}

	@Column(name = "is_enable")
	public Integer getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}

	@Column(name = "payment_config")
	public String getPaymentConfig() {
		return paymentConfig;
	}

	public void setPaymentConfig(String paymentConfig) {
		this.paymentConfig = paymentConfig;
	}
	

	
	

}