package com.legendshop.model.entity;


import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 网站导航
 * 
 */
@Entity
@Table(name = "ls_navi")
public class Navigation implements GenericEntity<Long> {

	private static final long serialVersionUID = -4558504540386164776L;

	// 主键
	private Long naviId;

	// 名称
	private String name;

	// 顺序
	private Integer seq;

	// 状态

	private Integer status;
	
	private Integer position;
 
	
	// 子节点
	/** The sub sort. */
	Set<NavigationItem> subItems = new TreeSet<NavigationItem>(new NavigationItemComparator());
		
	
	public Navigation() {
	}

	@Id
	@Column(name = "navi_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "NAVI_SEQ")
	public Long getNaviId() {
		return naviId;
	}

	public void setNaviId(Long naviId) {
		this.naviId = naviId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
  
	@Transient
	public Set<NavigationItem> getSubItems() {
		return subItems;
	}


	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Transient
	public Long getId() {
		return naviId;
	}

	public void setId(Long id) {
		this.naviId = id;
	}

	public void setSubItems(Set<NavigationItem> subItems) {
		this.subItems = subItems;
	}
	
	/**
	 * 增加子导航元素
	 * @param item
	 */
	public void addSubItems(NavigationItem item) {
		if(this.getNaviId().equals(item.getNaviId())){
			subItems.add(item);
		}
		
	}

	@Column(name = "position")
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

}
 

