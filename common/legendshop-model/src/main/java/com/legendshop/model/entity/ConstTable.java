/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.support.GenericEntity;

/**
 * 常量表
 */
@Table(name = "ls_cst_table")
public class ConstTable implements GenericEntity<ConstTableId> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8272145517063923481L;
	
	/** The id. */
	private ConstTableId id;
	
	/** The value. */
	private String value;
	
	/** The seq. */
	private Integer seq;

	/**
	 * Gets the seq.
	 * 
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 * 
	 * @param seq
	 *            the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * default constructor.
	 */
	public ConstTable() {
	}

	/**
	 * minimal constructor.
	 * 
	 * @param id
	 *            the id
	 */
	public ConstTable(ConstTableId id) {
		this.id = id;
	}

	/**
	 * full constructor.
	 * 
	 * @param id
	 *            the id
	 * @param value
	 *            the value
	 */
	public ConstTable(ConstTableId id, String value) {
		this.id = id;
		this.value = value;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public ConstTableId getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(ConstTableId id) {
		this.id = id;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	@Column(name = "value")
	public String getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}