/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 是否固定运费还是运费模板类型
 *
 */
public enum TransportTypeEnum implements IntegerEnum {
	
	/** 运费模版 **/
	TRANSPORT_TEMPLE(0),

	/** 固定运费 **/
	FIXED_TEMPLE(1);

	
	/** 数量. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	TransportTypeEnum(Integer num) {
		this.num = num;
	}

}
