/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 订单项.
 */
@Entity
@Table(name = "ls_sub_item")
public class SubItem implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5928412160398510764L;

	/** 订单项ID. */
	private Long subItemId; 
		
	/** 订单sub_number. */
	private String subNumber; 
	
	/** 订单项流水号. */
	private String subItemNumber; 
		
	/** 产品ID. */
	private Long prodId; 
		
	/** 产品SkuID. */
	private Long skuId; 
	
	/** 快照ID. */
	private Long snapshotId;
		
	/** 购物车产品个数. */
	private long basketCount; 
		
	/** 产品名称. */
	private String prodName; 
		
	/** 产品动态属性. */
	private String attribute; 
		
	/** 产品主图片路径. */
	private String pic; 
		
	/** 产品原价. */
	private Double price; 
		
	/** 产品现价. */
	private Double cash; 
		
	/** 用户名称. */
	private String userId; 
		
	/** 商品总金额. */
	private Double productTotalAmout; 
		
	/** 获得积分. */
	private Integer obtainIntegral; 
		
	/** 购物时间. */
	private Date subItemDate; 
	
	/** The weight. */
	private Double weight;
	
	/** The volume. */
	private Double volume;
	
	/** 分销的佣金金额. */
	private Double distCommisCash; 
	
	/** 会员直接上级分销会员. */
	private String distUserName;
	
	/** 会员上二级分销会员. */
	private String distSecondName;
	
	/**  会员上三级分销会员. */
	private String distThirdName;
	
	/** 会员直接上级分销佣金. */
	private Double distUserCommis;
	
	/** 会员上二级分销佣金s. */
	private Double distSecondCommis;
	
	/** 会员上三级分销佣金. */
	private Double distThirdCommis;

	/** 订单交易成功的时间. */
	private Date finallyDate;
	
	/** 订单状态. */
	private Integer subStatus;
	
	/** 促销信息. */
	private String promotionInfo;//
	
	/** 评论状态： 0 未评价 1 已评价. */
	private Integer commSts;
	
	/** 是否分销. */
	private int hasDist;
	
	/** 佣金结算状态: 0:待结算, 1:已结算. */
	private int commisSettleSts;
		
	/** 退款金额. */
	private java.math.BigDecimal refundAmount; 
	
	/** 退款数量. */
	private Long refundCount; 
	
	/** 退款记录ID. */
	private Long refundId; 
		
	/** 0:没有发起过退款,1:在处理,2:处理完成,-1:不同意. */
	private long refundState; 
		
		
	/** 申请类型:1:仅退款,2:退款退货,默认为0. */
	private Integer refundType; 
	
	/** 用于保存商品快照所用 -- 订单的运费  */
	private Double freightAmount;
	
	/** The stock counting. */
	private Integer stockCounting;
	
	//计算了营销活动之后的价格（包括限 时折扣促销  ，满折 ， 满减 ）
	private double discountedPrice=0.0;
	
	//使用的红包的优惠金额
	private double redpackOffPrice=0.0;
	
	//使用的优惠券优惠金额
	private double couponOffPrice=0.0;
	
	//计算营销活动和优惠券后的真实价值
	private double actualAmount=0.0;
		
	/**
	 * The Constructor.
	 */
	public SubItem() {}
		
	/**
	 * 主键
	 *
	 * @return the sub item id
	 */
	@Id
	@Column(name = "sub_item_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_ITEM_SEQ")
	public Long  getSubItemId(){
		return subItemId;
	} 
		
	/**
	 * Sets the sub item id.
	 *
	 * @param subItemId the sub item id
	 */
	public void setSubItemId(Long subItemId){
			this.subItemId = subItemId;
		}
		
    /**
     * Gets the sub number.
     *
     * @return the sub number
     */
    @Column(name = "sub_number")
	public String  getSubNumber(){
		return subNumber;
	} 
		
	/**
	 * Sets the sub number.
	 *
	 * @param subNumber the sub number
	 */
	public void setSubNumber(String subNumber){
			this.subNumber = subNumber;
		}
		
    /**
     * Gets the prod id.
     *
     * @return the prod id
     */
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the prod id
	 */
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    /**
     * Gets the sku id.
     *
     * @return the sku id
     */
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	/**
	 * Sets the sku id.
	 *
	 * @param skuId the sku id
	 */
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    /**
     * Gets the basket count.
     *
     * @return the basket count
     */
    @Column(name = "basket_count")
	public long  getBasketCount(){
		return basketCount;
	} 
		
	/**
	 * Sets the basket count.
	 *
	 * @param basketCount the basket count
	 */
	public void setBasketCount(long basketCount){
			this.basketCount = basketCount;
		}
		
    /**
     * Gets the prod name.
     *
     * @return the prod name
     */
    @Column(name = "prod_name")
	public String  getProdName(){
		return prodName;
	} 
		
	/**
	 * Sets the prod name.
	 *
	 * @param prodName the prod name
	 */
	public void setProdName(String prodName){
			this.prodName = prodName;
		}
		
    /**
     * Gets the attribute.
     *
     * @return the attribute
     */
    @Column(name = "attribute")
	public String  getAttribute(){
		return attribute;
	} 
		
	/**
	 * Sets the attribute.
	 *
	 * @param attribute the attribute
	 */
	public void setAttribute(String attribute){
			this.attribute = attribute;
		}
		
    /**
     * Gets the pic.
     *
     * @return the pic
     */
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	/**
	 * Sets the pic.
	 *
	 * @param pic the pic
	 */
	public void setPic(String pic){
			this.pic = pic;
		}
		
    /**
     * Gets the price.
     *
     * @return the price
     */
    @Column(name = "price")
	public Double  getPrice(){
		return price;
	} 
		
	/**
	 * Sets the price.
	 *
	 * @param price the price
	 */
	public void setPrice(Double price){
			this.price = price;
	}
		
    /**
     * Gets the cash.
     *
     * @return the cash
     */
    @Column(name = "cash")
	public Double  getCash(){
		return cash;
	} 
		
	/**
	 * Sets the cash.
	 *
	 * @param cash the cash
	 */
	public void setCash(Double cash){
			this.cash = cash;
	}
		
    /**
     * Gets the user id.
     *
     * @return the user id
     */
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    /**
     * Gets the product total amout.
     *
     * @return the product total amout
     */
    @Column(name = "product_total_amout")
	public Double  getProductTotalAmout(){
		return productTotalAmout;
	} 
		
	/**
	 * Sets the product total amout.
	 *
	 * @param productTotalAmout the product total amout
	 */
	public void setProductTotalAmout(Double productTotalAmout){
			this.productTotalAmout = productTotalAmout;
		}
		
    /**
     * Gets the obtain integral.
     *
     * @return the obtain integral
     */
    @Column(name = "obtain_integral")
	public Integer  getObtainIntegral(){
		return obtainIntegral;
	} 
		
	/**
	 * Sets the obtain integral.
	 *
	 * @param obtainIntegral the obtain integral
	 */
	public void setObtainIntegral(Integer obtainIntegral){
			this.obtainIntegral = obtainIntegral;
		}
		
    
	 /**
 	 * Gets the sub item date.
 	 *
 	 * @return the sub item date
 	 */
 	@Column(name = "sub_item_date")
	public Date getSubItemDate() {
		return subItemDate;
	}

	/**
	 * Sets the sub item date.
	 *
	 * @param subItemDate the sub item date
	 */
	public void setSubItemDate(Date subItemDate) {
		this.subItemDate = subItemDate;
	}
	
	
	 /**
 	 * Gets the weight.
 	 *
 	 * @return the weight
 	 */
 	@Column(name = "weight")
	public Double getWeight() {
		return weight;
	}

	/**
	 * Sets the weight.
	 *
	 * @param weight the weight
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	 /**
 	 * Gets the volume.
 	 *
 	 * @return the volume
 	 */
 	@Column(name = "volume")
	public Double getVolume() {
		return volume;
	}

	/**
	 * Sets the volume.
	 *
	 * @param volume the volume
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public Long getId() {
		return subItemId;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		subItemId = id;
	}

	/**
	 * Gets the snapshot id.
	 *
	 * @return the snapshot id
	 */
	@Column(name = "snapshot_id")
	public Long getSnapshotId() {
		return snapshotId;
	}

	/**
	 * Sets the snapshot id.
	 *
	 * @param snapshotId the snapshot id
	 */
	public void setSnapshotId(Long snapshotId) {
		this.snapshotId = snapshotId;
	}

	/**
	 * Gets the dist user name.
	 *
	 * @return the dist user name
	 */
	@Column(name = "dist_user_name")
	public String getDistUserName() {
		return distUserName;
	}

	/**
	 * Sets the dist user name.
	 *
	 * @param distUserName the dist user name
	 */
	public void setDistUserName(String distUserName) {
		this.distUserName = distUserName;
	}

	/**
	 * Gets the dist commis cash.
	 *
	 * @return the dist commis cash
	 */
	@Column(name = "dist_commis_cash")
	public Double getDistCommisCash() {
		return distCommisCash;
	}

	/**
	 * Sets the dist commis cash.
	 *
	 * @param distCommisCash the dist commis cash
	 */
	public void setDistCommisCash(Double distCommisCash) {
		this.distCommisCash = distCommisCash;
	}

	/**
	 * Gets the sub item number.
	 *
	 * @return the sub item number
	 */
	@Column(name = "sub_item_number")
	public String getSubItemNumber() {
		return subItemNumber;
	}

	/**
	 * Sets the sub item number.
	 *
	 * @param subItemNumber the sub item number
	 */
	public void setSubItemNumber(String subItemNumber) {
		this.subItemNumber = subItemNumber;
	}

	/**
	 * Gets the finally date.
	 *
	 * @return the finally date
	 */
	@Transient
	public Date getFinallyDate() {
		return finallyDate;
	}

	/**
	 * Sets the finally date.
	 *
	 * @param finallyDate the finally date
	 */
	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	/**
	 * Gets the sub status.
	 *
	 * @return the sub status
	 */
	@Transient
	public Integer getSubStatus() {
		return subStatus;
	}

	/**
	 * Sets the sub status.
	 *
	 * @param subStatus the sub status
	 */
	public void setSubStatus(Integer subStatus) {
		this.subStatus = subStatus;
	}

	/**
	 * Gets the promotion info.
	 *
	 * @return the promotion info
	 */
	@Column(name = "promotion_info")
	public String getPromotionInfo() {
		return promotionInfo;
	}

	/**
	 * Sets the promotion info.
	 *
	 * @param promotionInfo the promotion info
	 */
	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}

	/**
	 * Gets the comm sts.
	 *
	 * @return the comm sts
	 */
	@Column(name = "comm_sts")
	public Integer getCommSts() {
		return commSts;
	}

	/**
	 * Sets the comm sts.
	 *
	 * @param commSts the comm sts
	 */
	public void setCommSts(Integer commSts) {
		this.commSts = commSts;
	}

	/**
	 * Gets the dist second name.
	 *
	 * @return the dist second name
	 */
	@Column(name = "dist_second_name")
	public String getDistSecondName() {
		return distSecondName;
	}

	/**
	 * Sets the dist second name.
	 *
	 * @param distSecondName the dist second name
	 */
	public void setDistSecondName(String distSecondName) {
		this.distSecondName = distSecondName;
	}

	/**
	 * Gets the dist third name.
	 *
	 * @return the dist third name
	 */
	@Column(name = "dist_third_name")
	public String getDistThirdName() {
		return distThirdName;
	}

	/**
	 * Sets the dist third name.
	 *
	 * @param distThirdName the dist third name
	 */
	public void setDistThirdName(String distThirdName) {
		this.distThirdName = distThirdName;
	}

	/**
	 * Gets the dist user commis.
	 *
	 * @return the dist user commis
	 */
	@Column(name = "dist_user_commis")
	public Double getDistUserCommis() {
		return distUserCommis;
	}

	/**
	 * Sets the dist user commis.
	 *
	 * @param distUserCommis the dist user commis
	 */
	public void setDistUserCommis(Double distUserCommis) {
		this.distUserCommis = distUserCommis;
	}

	/**
	 * Gets the dist second commis.
	 *
	 * @return the dist second commis
	 */
	@Column(name = "dist_second_commis")
	public Double getDistSecondCommis() {
		return distSecondCommis;
	}

	/**
	 * Sets the dist second commis.
	 *
	 * @param distSecondCommis the dist second commis
	 */
	public void setDistSecondCommis(Double distSecondCommis) {
		this.distSecondCommis = distSecondCommis;
	}

	/**
	 * Gets the dist third commis.
	 *
	 * @return the dist third commis
	 */
	@Column(name = "dist_third_commis")
	public Double getDistThirdCommis() {
		return distThirdCommis;
	}

	/**
	 * Sets the dist third commis.
	 *
	 * @param distThirdCommis the dist third commis
	 */
	public void setDistThirdCommis(Double distThirdCommis) {
		this.distThirdCommis = distThirdCommis;
	}
	
	/**
	 * Gets the has dist.
	 *
	 * @return the checks for dist
	 */
	@Column(name = "has_dist")
	public int getHasDist() {
		return hasDist;
	}

	/**
	 * Sets the has dist.
	 *
	 * @param hasDist the checks for dist
	 */
	public void setHasDist(int hasDist) {
		this.hasDist = hasDist;
	}

	/**
	 * Gets the commis settle sts.
	 *
	 * @return the commis settle sts
	 */
	@Column(name = "commis_settle_sts")
	public int getCommisSettleSts() {
		return commisSettleSts;
	}

	/**
	 * Sets the commis settle sts.
	 *
	 * @param commisSettleSts the commis settle sts
	 */
	public void setCommisSettleSts(int commisSettleSts) {
		this.commisSettleSts = commisSettleSts;
	}

	/**
	 * Gets the refund amount.
	 *
	 * @return the refund amount
	 */
	@Column(name = "refund_amount")
	public java.math.BigDecimal getRefundAmount() {
		return refundAmount;
	}

	/**
	 * Sets the refund amount.
	 *
	 * @param refundAmount the refund amount
	 */
	public void setRefundAmount(java.math.BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	/**
	 * Gets the refund count.
	 *
	 * @return the refund count
	 */
	@Column(name = "refund_count")
	public Long getRefundCount() {
		return refundCount;
	}

	/**
	 * Sets the refund count.
	 *
	 * @param refundCount the refund count
	 */
	public void setRefundCount(Long refundCount) {
		this.refundCount = refundCount;
	}

	/**
	 * Gets the refund type.
	 *
	 * @return the refund type
	 */
	@Column(name = "refund_type")
	public Integer getRefundType() {
		return refundType;
	}

	/**
	 * Sets the refund type.
	 *
	 * @param refundType the refund type
	 */
	public void setRefundType(Integer refundType) {
		this.refundType = refundType;
	}
	
	/**
	 * Gets the refund id.
	 *
	 * @return the refundId
	 */
	@Column(name = "refund_id")
	public Long getRefundId() {
		return refundId;
	}

	/**
	 * Sets the refund id.
	 *
	 * @param refundId the refundId to set
	 */
	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	/**
	 * Gets the refund state.
	 *
	 * @return the refundState
	 */
	@Column(name = "refund_state")
	public long getRefundState() {
		return refundState;
	}

	/**
	 * Sets the refund state.
	 *
	 * @param refundState the refundState to set
	 */
	public void setRefundState(long refundState) {
		this.refundState = refundState;
	}

	/**
	 * Gets the stock counting.
	 *
	 * @return the stock counting
	 */
	@Column(name = "stock_counting")
	public Integer getStockCounting() {
		return stockCounting;
	}

	/**
	 * Sets the stock counting.
	 *
	 * @param stockCounting the stock counting
	 */
	public void setStockCounting(Integer stockCounting) {
		this.stockCounting = stockCounting;
	}
	
	/**
	 * Gets the freight amount.
	 *
	 * @return the freight amount
	 */
	@Transient
	public Double getFreightAmount() {
		return freightAmount;
	}

	/**
	 * Sets the freight amount.
	 *
	 * @param freightAmount the freight amount
	 */
	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}
	@Column(name = "discounted_price")
	public double getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
	@Column(name = "redpack_off_price")
	public double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}
	@Column(name = "coupon_off_price")
	public double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}
	
	@Column(name = "actual_amount")
	public double getActualAmount() {
		return actualAmount;
	}
	
	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}
	
} 
