/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 订单历史
 */
@Entity
@Table(name = "ls_sub_hist")
public class SubHistory implements GenericEntity<Long> {

	private static final long serialVersionUID = 4703105991736031661L;

	/** The sub hist id. */
	private Long subHistId;

	/** The sub id. */
	private Long subId;

	private String status;

	private Date recDate;
	
	private String userName;
	
	private String reason; //操作的动作信息 如: xxoo 于 2015-05-21 15:40:23 提交订单 

	/**
	 * default constructor.
	 */
	public SubHistory() {
	}

	/**
	 * Gets the sub hist id.
	 * 
	 * @return the sub hist id
	 */
	@Id
	@Column(name = "sub_hist_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SUB_HIST_SEQ")
	public Long getSubHistId() {
		return subHistId;
	}

	/**
	 * Sets the sub hist id.
	 * 
	 * @param subHistId
	 *            the new sub hist id
	 */
	public void setSubHistId(Long subHistId) {
		this.subHistId = subHistId;
	}

	/**
	 * Gets the sub id.
	 * 
	 * @return the sub id
	 */
	@Column(name = "sub_id")
	public Long getSubId() {
		return subId;
	}

	/**
	 * Sets the sub id.
	 * 
	 * @param subId
	 *            the new sub id
	 */
	public void setSubId(Long subId) {
		this.subId = subId;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
	
	
	@Column(name = "reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public  String DateToString(Date date) {
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}
	
	
	
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	@Transient
	public Long getId() {
		return subHistId;
	}

	@Override
	public void setId(Long id) {
		this.subHistId = id;
	}

}