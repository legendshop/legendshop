/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 公告类型
 */
public enum PubTypeEnum implements IntegerEnum {
	// 0：买家   1：卖家
	
	
	
	
	PUB_BUYERS(0), //买家公告
	
	PUB_SELLER(1);//卖家公告

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	PubTypeEnum(Integer num) {
		this.num = num;
	}

}
