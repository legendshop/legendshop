package com.legendshop.model.entity;
import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.marketing.*;

import java.util.Date;
import java.util.List;

/**
 *营销活动表
 */
@Entity
@Table(name = "ls_marketing")
public class Marketing implements GenericEntity<Long> {
	
	private static final long serialVersionUID = 6727725866131875604L;

	/** 营销活动编号 */
	private Long id; 
		
	/** 活动名称 */
	private String marketName; 
		
	/** 活动开始时间 */
	private Date startTime; 
		
	/** 活动结束时间 */
	private Date endTime; 
		
	/** 用户编号 */
	private String userId; 
		
	/** 店铺编号 */
	private Long shopId;

	/** 0: 满减促销 ; 1: 满折促销 2：限时促销 */
	private Integer type; 
		
	/** 是否全部商品[全店] 部分商品[需要添加活动商品] */
	private Integer isAllProds; 
		
	/** 活动状态(0-未发布/1-正在进行/) */
	private Integer state; 
		
	/** 备注 */
	private String remark; 
	
	/**店铺名称*/
	private String siteName;
		
	/**创建时间  */
	private Date createTime; 
	
	/** 过期标记 */
	private Integer isExpired;
	/** 用于搜索*/
	private String searchType;

	/** skuid集合*/
	private Long[] skuIds;

	/** 关联商品*/
	private List<MarketingProdDto> MarketingProds;
	
	private String[] manze_rule;
	
	private String[] manjian_rule;
	
	//包邮活动
	private String[] manyuan_fullmail_rule;
	private String[] manjian_fullmail_rule;
	
	//商品ID 和 价格
	private Long[] prodId;
	private Double[] cash; 
	
	private List<MarketingMjRule> marketingMjRules;
	
	private List<MarketingMzRule> marketingMzRules;
	
	private List<MarketingMyRule> marketingMyRule;
	
	private List<MarketingMjFulllRule> marketingMjFulllRule;
	
	
	private Float discount;
	
		
	
	public Marketing() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MARKETING_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
	}
		
    @Column(name = "market_name")
	public String  getMarketName(){
		return marketName;
	} 
		
	public void setMarketName(String marketName){
			this.marketName = marketName;
	}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
	}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "type")
	public Integer  getType(){
		return type;
	} 
		
	public void setType(Integer type){
			this.type = type;
		}
		
    @Column(name = "is_all_prods")
	public Integer  getIsAllProds(){
		return isAllProds;
	} 
		
	public void setIsAllProds(Integer isAllProds){
			this.isAllProds = isAllProds;
		}
		
    @Column(name = "state")
	public Integer  getState(){
		return state;
	} 
		
	public void setState(Integer state){
			this.state = state;
		}
		
    @Column(name = "remark")
	public String  getRemark(){
		return remark;
	} 
		
	public void setRemark(String remark){
			this.remark = remark;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	
	@Column(name = "is_expired")
	public Integer getIsExpired() {
		return isExpired;
	}

	public void setIsExpired(Integer isExpired) {
		this.isExpired = isExpired;
	}

	@Transient
	public String[] getManze_rule() {
		return manze_rule;
	}

	public void setManze_rule(String[] manze_rule) {
		this.manze_rule = manze_rule;
	}

	@Transient
	public String[] getManjian_rule() {
		return manjian_rule;
	}

	public void setManjian_rule(String[] manjian_rule) {
		this.manjian_rule = manjian_rule;
	}

	@Column(name = "discount")
	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}
	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Transient
	public List<MarketingMjRule> getMarketingMjRules() {
		return marketingMjRules;
	}

	public void setMarketingMjRules(List<MarketingMjRule> marketingMjRules) {
		this.marketingMjRules = marketingMjRules;
	}

	@Transient
	public List<MarketingMzRule> getMarketingMzRules() {
		return marketingMzRules;
	}

	public void setMarketingMzRules(List<MarketingMzRule> marketingMzRules) {
		this.marketingMzRules = marketingMzRules;
	}

	@Transient
	public String[] getManyuan_fullmail_rule() {
		return manyuan_fullmail_rule;
	}

	public void setManyuan_fullmail_rule(String[] manyuan_fullmail_rule) {
		this.manyuan_fullmail_rule = manyuan_fullmail_rule;
	}
	
	@Transient
	public String[] getManjian_fullmail_rule() {
		return manjian_fullmail_rule;
	}

	public void setManjian_fullmail_rule(String[] manjian_fullmail_rule) {
		this.manjian_fullmail_rule = manjian_fullmail_rule;
	}
	@Transient
	public Long[] getProdId() {
		return prodId;
	}

	public void setProdId(Long[] prodId) {
		this.prodId = prodId;
	}
	@Transient
	public Double[] getCash() {
		return cash;
	}

	public void setCash(Double[] cash) {
		this.cash = cash;
	}
	@Transient
	public List<MarketingMyRule> getMarketingMyRule() {
		return marketingMyRule;
	}

	public void setMarketingMyRule(List<MarketingMyRule> marketingMyRule) {
		this.marketingMyRule = marketingMyRule;
	}
	@Transient
	public List<MarketingMjFulllRule> getMarketingMjFulllRule() {
		return marketingMjFulllRule;
	}

	public void setMarketingMjFulllRule(List<MarketingMjFulllRule> marketingMjFulllRule) {
		this.marketingMjFulllRule = marketingMjFulllRule;
	}
	@Transient
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	@Transient
	public Long[] getSkuIds() {
		return skuIds;
	}

	public void setSkuIds(Long[] skuIds) {
		this.skuIds = skuIds;
	}

	@Transient
	public List<MarketingProdDto> getMarketingProds() {
		return MarketingProds;
	}

	public void setMarketingProds(List<MarketingProdDto> marketingProds) {
		MarketingProds = marketingProds;
	}
}
