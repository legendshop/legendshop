package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 广告 ActiveBanner
 */
@Entity
@Table(name = "ls_active_banner")
public class ActiveBanner implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id; 

	/** 图片路径 */
	private String imageFile; 

	/** 图片链接 */
	private String url; 

	/** 序号 */
	private Long seq; 

	/**banner 图类型*/
	private String bannerType;


	public ActiveBanner() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GROUP_BANNER_SEQ")
	public Long  getId(){
		return id;
	} 

	public void setId(Long id){
		this.id = id;
	}

	@Column(name = "image_file")
	public String  getImageFile(){
		return imageFile;
	} 

	public void setImageFile(String imageFile){
		this.imageFile = imageFile;
	}

	@Column(name = "url")
	public String  getUrl(){
		return url;
	} 

	public void setUrl(String url){
		this.url = url;
	}

	@Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 

	public void setSeq(Long seq){
		this.seq = seq;
	}

	@Column(name = "banner_type")	
	public String getBannerType() {
		return bannerType;
	}

	public void setBannerType(String bannerType) {
		this.bannerType = bannerType;
	}



} 
