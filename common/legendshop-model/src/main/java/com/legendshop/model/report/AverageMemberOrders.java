package com.legendshop.model.report;

import java.io.Serializable;

/**
 *平均会员订单量
 */
public class AverageMemberOrders implements Serializable {

	private static final long serialVersionUID = 2473963574271604602L;

	// 总会员数
	private long totalMember;
	
	//总订单数
	private long totalOrder;
	
	//平均会员订单量
	private Double averageOrders;
	
	/**
	 * 平均会员订单量
	 * @param totalMember
	 * @param totalOrder
	 * @param averageOrders
	 */
	public AverageMemberOrders(long totalMember, long totalOrder, double averageOrders) {
		this.totalMember = totalMember;
		this.totalOrder = totalOrder;
		this.averageOrders = averageOrders;
	}


	public void setTotalOrder(Integer totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Double getAverageOrders() {
		return averageOrders;
	}

	public void setAverageOrders(Double averageOrders) {
		this.averageOrders = averageOrders;
	}


	public long getTotalMember() {
		return totalMember;
	}


	public void setTotalMember(long totalMember) {
		this.totalMember = totalMember;
	}


	public long getTotalOrder() {
		return totalOrder;
	}


	public void setTotalOrder(long totalOrder) {
		this.totalOrder = totalOrder;
	}

}
