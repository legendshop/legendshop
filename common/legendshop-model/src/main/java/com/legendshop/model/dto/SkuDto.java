/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.buy.CartMarketRules;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 商品sku DTO
 *
 */
@ApiModel(value="商品SkuDto") 
public class SkuDto implements Serializable  {

private static final long serialVersionUID = 6830009574780972733L;
	
	/**
	 * sku id
	 */
	@ApiModelProperty(value="skuId") 
	private Long skuId ; 
		
	/**
	 * sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2
	 */
	@ApiModelProperty(value="sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2") 
	private String properties ; 
	
	/**
	 * 原价
	 */
	@ApiModelProperty(value="原价")
	private Double price ; 
	
	/**
	 * 现价 
	 */
	@ApiModelProperty(value="现价")
	private Double cash;
	
	/**拼团价格*/
	@ApiModelProperty(value="拼团价格")
	private Double mergePrice;
	
	/**
	 * 促销价格 如果没有促销 则等于现价
	 */
	@ApiModelProperty(value="促销价格 如果没有促销 则等于现价")
	private Double promotionPrice;
	
	/**
	 * 营销规则
	 */
	@ApiModelProperty(value="营销规则")
    private List<CartMarketRules> ruleList;
    
	/**
	 * sku 名称
	 */
	@ApiModelProperty(value="sku名称")
	private String name ; 
	
	/**
	 * SKU标题
	 */
	@ApiModelProperty(value="SKU标题")
	private List<KeyValueEntity> propertiesNameList;
		
	/**
	 * sku状态。 1:正常     0:删除
	 */
	@ApiModelProperty(value="sku状态。 1:正常     0:删除")
	private Integer status ;
	
	/**
	 * sku数量
	 */
	@ApiModelProperty(value="sku数量")
	private Long stocks ; 

	/**
	 * 商家编码
	 */
	@ApiModelProperty(value="商家编码")
	private String partyCode;
	
	/**
	 * 商品编码
	 */
	@ApiModelProperty(value="商品编码")
	private String productCode;
		
	/**
	 * 商品条形码
	 */
	@ApiModelProperty(value="商品条形码")
	private String modelId;
	
	/**
	 * 对应属性值ID
	 */
	@ApiModelProperty(value="对应属性值ID")
	private String propValueIds;
	
	/**
	 * V0会员价
	 */
	@ApiModelProperty(value="V0会员价")
	private Double priceV0;
	
	/**
	 * V1会员价
	 */
	@ApiModelProperty(value="V1会员价")
	private Double priceV1;
	
	/**
	 * V2会员价
	 */
	@ApiModelProperty(value="V2会员价")
	private Double priceV2;
	
	/**
	 * V3会员价
	 */
	@ApiModelProperty(value="V3会员价")
	private Double priceV3;
	
	/**
	 * 图片
	 */
	@ApiModelProperty(value="图片")
	private String images;
	
	/**
	 * 物流体积(立方米)
	 */
	@ApiModelProperty(value="物流体积(立方米)")
	private Double volume;
	
	/**
	 * 物流重量(千克)
	 */
	@ApiModelProperty(value="物流重量(千克)")
	private Double weight;
	
	/**
	 * 中文 销售属性组合
	 */
	@ApiModelProperty(value="中文 销售属性组合")
	private String cnProperties;
	
	/**
	 * sku类型
	 */
	@ApiModelProperty(value="sku类型")
	private String skuType;
	
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getPropValueIds() {
		return propValueIds;
	}

	public void setPropValueIds(String propValueIds) {
		this.propValueIds = propValueIds;
	}

	public List<KeyValueEntity> getPropertiesNameList() {
		return propertiesNameList;
	}

	public void setPropertiesNameList(List<KeyValueEntity> propertiesNameList) {
		this.propertiesNameList = propertiesNameList;
	}

	public List<CartMarketRules> getRuleList() {
		return ruleList;
	}

	public void setRuleList(List<CartMarketRules> ruleList ) {
		this.ruleList = ruleList;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	public Double getPriceV0() {
		return priceV0;
	}

	public void setPriceV0(Double priceV0) {
		this.priceV0 = priceV0;
	}

	public Double getPriceV1() {
		return priceV1;
	}

	public void setPriceV1(Double priceV1) {
		this.priceV1 = priceV1;
	}

	public Double getPriceV2() {
		return priceV2;
	}

	public void setPriceV2(Double priceV2) {
		this.priceV2 = priceV2;
	}

	public Double getPriceV3() {
		return priceV3;
	}

	public void setPriceV3(Double priceV3) {
		this.priceV3 = priceV3;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Double getMergePrice() {
		return mergePrice;
	}

	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getSkuType() {
		return skuType;
	}

	public void setSkuType(String skuType) {
		this.skuType = skuType;
	}
	
}
