package com.legendshop.model.dto.order;

import java.util.List;
/**
 * 门店下单详情参数Dto
 *
 */
public class StoreOrderDetailsParamsDto {
	
	/** 购物车id集合 */
	private List<Long> ids;
	
	private String type;
	
	private Boolean buyNow;
	
	private Long prodId;
	
	private Long skuId;
	
	private Integer count;
	
	private Long shopId;
	
	private Long storeId;

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getBuyNow() {
		return buyNow;
	}

	public void setBuyNow(Boolean buyNow) {
		this.buyNow = buyNow;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	
}
