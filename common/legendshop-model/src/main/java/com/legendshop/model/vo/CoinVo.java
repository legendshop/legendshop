package com.legendshop.model.vo;

import java.io.Serializable;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.coin.CoinLog;

/**
 * 金币
 */
public class CoinVo  implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4444221475573401768L;

	/** The coin log. */
	private PageSupport<CoinLog> coinLog;
	
	/** The coin. */
	private Double coin;//金币剩余数量

	
	/**
	 * Gets the coin.
	 *
	 * @return the coin
	 */
	public Double getCoin() {
		return coin;
	}

	/**
	 * Sets the coin.
	 *
	 * @param coin the new coin
	 */
	public void setCoin(Double coin) {
		this.coin = coin;
	}

	/**
	 * Gets the coin log.
	 *
	 * @return the coin log
	 */
	public PageSupport<CoinLog> getCoinLog() {
		return coinLog;
	}

	/**
	 * Sets the coin log.
	 *
	 * @param coinLog the new coin log
	 */
	public void setCoinLog(PageSupport<CoinLog> coinLog) {
		this.coinLog = coinLog;
	}

	
	
}
