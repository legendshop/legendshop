/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 礼券的类型
 * 通用券:common，品类券:category，指定商品券:product
 */
public enum CouponTypeEnum implements StringEnum {

	/** 通用券*/
	COMMON("common"),

	/** 品类券*/
	CATEGORY("category"),
	
	/** 指定商品券*/
	PRODUCT("product"),
	
	/** 指定店铺红包*/
	SHOP("shop"); 

	/** The num. */
	private String type;

	CouponTypeEnum(String type) {
		this.type = type;
	}

	@Override
	public String value() {
		return type;
	}

}
