/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 专题状态
 */
public enum ThemeStatusEnum implements IntegerEnum {

	OFFLINE(0), // 下线状态
	
	
	NORMAL(1), //正常状态
	
	DEL(2) //删除状态
	
	;
	
	


	/** The num. */
	private Integer Status;

	/**
	 * Instantiates a new consult type enum.
	 * 
	 * @param num
	 *            the num
	 */
	ThemeStatusEnum(Integer Status) {
		this.Status = Status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	@Override
	public Integer value() {
		return Status;
	}

}
