package com.legendshop.model.dto;

import com.legendshop.util.AppUtils;

public class ApplyReturnOrder {

	private String subNum;
	private Integer status;
	private String prods;
	private Long applyUserInfoId;

	private String imagesPaths;
	private String desc;
	
	private Long subItemId;
	
	private Double orderTotalPrice;//退款金额
	
	public Double getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public void setOrderTotalPrice(Double orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}

	public String getSubNum() {
		return subNum;
	}

	public void setSubNum(String subNum) {
		this.subNum = subNum;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProds() {
		return prods;
	}

	public void setProds(String prods) {
		this.prods = prods;
	}

	public Long getApplyUserInfoId() {
		return applyUserInfoId;
	}

	public void setApplyUserInfoId(Long applyUserInfoId) {
		this.applyUserInfoId = applyUserInfoId;
	}



	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public boolean validate(){
		if(AppUtils.isBlank(subNum)|| AppUtils.isBlank(prods)||AppUtils.isBlank(applyUserInfoId)
	|| AppUtils.isBlank(imagesPaths) || AppUtils.isBlank(desc) || AppUtils.isBlank(orderTotalPrice)){
			return false;
		}
		return true;
	}

	public String getImagesPaths() {
		return imagesPaths;
	}

	public void setImagesPaths(String imagesPaths) {
		this.imagesPaths = imagesPaths;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	

}
