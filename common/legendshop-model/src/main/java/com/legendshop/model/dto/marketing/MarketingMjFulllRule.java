package com.legendshop.model.dto.marketing;

public class MarketingMjFulllRule {
	private Double fullAmount;
	private String fullName="包邮";
	public Double getFullAmount() {
		return fullAmount;
	}
	public void setFullAmount(Double fullAmount) {
		this.fullAmount = fullAmount;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
}
