package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *相关专题表
 */
@Entity
@Table(name = "ls_theme_related")
public class ThemeRelated implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8215795988096357779L;

	/** 相关专题id */
	private Long relatedId; 
		
	/** 专题id */
	private Long themeId;

	/** 手机端相关专题ID*/
	private Long mRelatedThemeId;

	/** PC端相关专题ID*/
	private Long relatedThemeId;

	/** 相关专题图片 */
	private String img; 
		
	/** 相关专题链接 */
	private String link; 
	
	/** 手机端相关专题链接 */
	private String mLink;




	public ThemeRelated() {
    }
		
	@Id
	@Column(name = "related_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "LS_THEME_RELATED_SEQ")
	public Long  getRelatedId(){
		return relatedId;
	} 
		
	public void setRelatedId(Long relatedId){
			this.relatedId = relatedId;
		}
		
    @Column(name = "theme_id")
	public Long  getThemeId(){
		return themeId;
	} 
		
	public void setThemeId(Long themeId){
			this.themeId = themeId;
		}

	@Column(name = "m_related_theme_id")
	public Long getmRelatedThemeId() {
		return mRelatedThemeId;
	}

	public void setmRelatedThemeId(Long mRelatedThemeId) {
		this.mRelatedThemeId = mRelatedThemeId;
	}


	@Column(name = "related_theme_id")
	public Long getRelatedThemeId() {
		return relatedThemeId;
	}

	public void setRelatedThemeId(Long relatedThemeId) {
		this.relatedThemeId = relatedThemeId;
	}

	@Column(name = "img")
	public String  getImg(){
		return img;
	} 
		
	public void setImg(String img){
			this.img = img;
		}
		
    @Column(name = "link")
	public String  getLink(){
		return link;
	} 
		
	public void setLink(String link){
			this.link = link;
		}
	
	@Transient
	public Long getId() {
		return relatedId;
	}
	
	public void setId(Long id) {
		relatedId = id;
	}

	@Column(name = "m_link")
	public String getmLink() {
		return mLink;
	}

	public void setmLink(String mLink) {
		this.mLink = mLink;
	}


} 
