package com.legendshop.model.dto.weixin;

import java.io.ByteArrayOutputStream;

/**
 * 用于封装微信小程序码或二维码获取结果的DTO
 * @author 开发很忙
 */
public class WXACodeResultDto {
	
	/**
	 * 是否成功
	 */
	private boolean success;
	
	/**
	 * 图片二进制流, 失败的话则为空
	 */
	private ByteArrayOutputStream outputStream;
	
	/**
	 * 错误消息, 如果获取小程序码错误则会有错误消息, 成功则为空
	 */
	private Errormsg errormsg;
	

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}

	public void setOutputStream(ByteArrayOutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public Errormsg getErrormsg() {
		return errormsg;
	}

	public void setErrormsg(Errormsg errormsg) {
		this.errormsg = errormsg;
	}
	
	/**
	 * 错误消息
	 */
	public static class Errormsg {
		
		/**
		 * 错误码
		 */
		private String errcode;
		
		/** 
		 * 错误消息
		 */
		private String errmsg;

		public String getErrcode() {
			return errcode;
		}

		public void setErrcode(String errcode) {
			this.errcode = errcode;
		}

		public String getErrmsg() {
			return errmsg;
		}

		public void setErrmsg(String errmsg) {
			this.errmsg = errmsg;
		}
	}
}
