/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * The Class ProductConsultDto.
 */
public class ProductConsultDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8642661250805674029L;

	/** The cons id. */
	private Long consId;
	
	/** The content. */
	private String content;
	
	/** The answer. */
	private String answer;
	
	public ProductConsultDto(){
		
	}

	/**
	 * Gets the cons id.
	 *
	 * @return the cons id
	 */
	public Long getConsId() {
		return consId;
	}

	/**
	 * Sets the cons id.
	 *
	 * @param consId the new cons id
	 */
	public void setConsId(Long consId) {
		this.consId = consId;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
}
