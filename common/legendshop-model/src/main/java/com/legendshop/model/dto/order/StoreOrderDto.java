package com.legendshop.model.dto.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;


public class StoreOrderDto implements GenericEntity<Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4119000063402265033L;

	/**
	 * 
	 */

	private Long id;
	
	private String orderSn;
	
	private Long shopId;
	
	private String userId;
	
	private String reciverName;
	
	private String reciverMobile;
	
	/** 提货码 */
	private String dlyoPickupCode; 
		
	/** 付款方式 */
	private Integer payType; 
		
	/** 状态[未自提、已自提] */
	private Integer dlyoSatus; 
		
	/** 订单生成时间 */
	private Date dlyoAddtime; 
	
	private Long prodId;
	
	private Integer basketCount; 
	
	/** 产品名称 */
	private String prodName; 
	
	/** 产品动态属性 */
	private String attribute; 
	
	/** 产品主图片路径 */
	private String pic; 
	
	/** 产品现价 */
	private Double cash;
	
	private Double actualTotal;
	
	private Integer refundType;
	
	private StoreOrderItemDto storeOrderItemDto;
	
	private List<StoreOrderItemDto> storeOrderItemDtoList =new ArrayList<StoreOrderItemDto>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getReciverName() {
		return reciverName;
	}

	public void setReciverName(String reciverName) {
		this.reciverName = reciverName;
	}

	public String getReciverMobile() {
		return reciverMobile;
	}

	public void setReciverMobile(String reciverMobile) {
		this.reciverMobile = reciverMobile;
	}

	public String getDlyoPickupCode() {
		return dlyoPickupCode;
	}

	public void setDlyoPickupCode(String dlyoPickupCode) {
		this.dlyoPickupCode = dlyoPickupCode;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Integer getDlyoSatus() {
		return dlyoSatus;
	}

	public void setDlyoSatus(Integer dlyoSatus) {
		this.dlyoSatus = dlyoSatus;
	}

	public Date getDlyoAddtime() {
		return dlyoAddtime;
	}

	public void setDlyoAddtime(Date dlyoAddtime) {
		this.dlyoAddtime = dlyoAddtime;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public List<StoreOrderItemDto> getStoreOrderItemDtoList() {
		return storeOrderItemDtoList;
	}

	public void setStoreOrderItemDtoList(List<StoreOrderItemDto> storeOrderItemDtoList) {
		this.storeOrderItemDtoList = storeOrderItemDtoList;
	}

	public StoreOrderItemDto getStoreOrderItemDto() {
		return storeOrderItemDto;
	}

	public Integer getRefundType() {
		return refundType;
	}

	public void setRefundType(Integer refundType) {
		this.refundType = refundType;
	}

	public void setStoreOrderItemDto(StoreOrderItemDto storeOrderItemDto) {
		this.storeOrderItemDto = storeOrderItemDto;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}
}
