package com.legendshop.model.entity;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * APP启动广告
 */
@Entity
@Table(name = "ls_app_start_adv")
public class AppStartAdv implements GenericEntity<Long> {

	private static final long serialVersionUID = -3655407288647094811L;

	/** 主键 */
	private Long id; 
		
	/** 广告名称 */
	@NotBlank
	@Length(min = 1, max = 20)
	private String name; 
	
	/** 广告图片URL */
	private String imgUrl;
	
	/** 广告图片文件 */
	private MultipartFile imgFile;
		
	/** 跳转的url */
	@URL
	//@Pattern(regexp = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?")
	private String url; 
		
	/** 广告的描述 */
	@Length(min = 1, max = 250)
	private String description; 
		
	/** 状态 0:下线,1:上线 */
	@NotNull
	@Range(min = 0, max = 1)
	private Integer status; 
		
	/** 创建时间 */
	private Date createTime; 
		
	
	public AppStartAdv() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "APP_START_ADV_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
	}

	@Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
	}
	
	@Column(name = "img_url")
    public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	@Transient
	public MultipartFile getImgFile() {
		return imgFile;
	}

	public void setImgFile(MultipartFile imgFile) {
		this.imgFile = imgFile;
	}

	@Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "description")
	public String  getDescription(){
		return description;
	} 
		
	public void setDescription(String description){
			this.description = description;
	}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
		this.status = status;
	}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}
	


} 
