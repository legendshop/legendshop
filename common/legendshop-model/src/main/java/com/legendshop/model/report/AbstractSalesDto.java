package com.legendshop.model.report;

import java.io.Serializable;

/**
 * 销售Dto.
 */
public class AbstractSalesDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1173368499148883850L;

	
	/** 总共订单数. */
	private long totalOrders;
	
	/** 总订单金额. */
	private Double totalAmount;

	public long getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(long totalOrders) {
		this.totalOrders = totalOrders;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}



}
