/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 是否支持分销.
 */
public enum SupportDistEnum implements IntegerEnum {
	
	/** 不支持分销 */
	NOT_SUPPORT(0),
	
	/** 支持分销 */
	SUPPORT(1);

	/** The num. */
	private Integer num;

	public Integer value() {
		return num;
	}

	SupportDistEnum(Integer num) {
		this.num = num;
	}

}
