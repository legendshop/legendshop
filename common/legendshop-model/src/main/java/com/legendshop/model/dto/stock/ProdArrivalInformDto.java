package com.legendshop.model.dto.stock;

import java.io.Serializable;
import java.util.Date;

/**
 * 到货通知
 * @author weibf
 */
public class ProdArrivalInformDto implements Serializable{

	private static final long serialVersionUID = -9196562322203987794L;
	
	/** 到货通知表主键 */
	private Long prodArrivalInformId;
	private Long shopId;
	private Long prodId;
	private Long skuId;
	/** 消费者 */
	private String userId;
	private String userName;
	/** 手机 */
	private String mobilePhone;
	private String email;
	/** 商品sku名称 */
	private String productName;
	/** 价格 */
	private String price;
	/** 属性 */
	private String cnProperties;
	/** 图片 */
	private String picture;
	/** 状态： 0： 未通知  1：已通知 */
	private int status;
	/** 添加到货通知的时间 */
	private Date createTime;
	
	public Long getProdArrivalInformId() {
		return prodArrivalInformId;
	}
	public void setProdArrivalInformId(Long prodArrivalInformId) {
		this.prodArrivalInformId = prodArrivalInformId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getProdId() {
		return prodId;
	}
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	public Long getSkuId() {
		return skuId;
	}
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCnProperties() {
		return cnProperties;
	}
	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
