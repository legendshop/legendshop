package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_passport_sub")
public class PassportSub implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393091645157177517L;

	/** 主键 */
	private Long id; 
		
	/** 关联的父passport记录ID */
	private Long passportId; 
	
	/** 第三方授权凭证 */
	private String accessToken;
	
	/** refresh_token, 用于刷新access_token */
	private String refreshToken;
	
	/** access_token接口调用凭证超时时间(单位是秒) */
	private Integer expiresIn;
	
	/** access_token的获取时间 */
	private Date validateTime;
		
	/** 第三方应用的openID */
	private String openId; 
		
	/** 来源: PassportSourceEnum */
	private String source; 
		
	/** 创建时间 */
	private Date createTime; 
		
	public PassportSub() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PASSPORT_SUB_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "passport_id")
	public Long  getPassportId(){
		return passportId;
	} 
		
	public void setPassportId(Long passportId){
			this.passportId = passportId;
		}
	
	@Column(name = "access_token")
    public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@Column(name = "refresh_token")
	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Column(name = "expires_in")
	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	@Column(name = "validate_time")
	public Date getValidateTime() {
		return validateTime;
	}

	public void setValidateTime(Date validateTime) {
		this.validateTime = validateTime;
	}

	@Column(name = "open_id")
	public String  getOpenId(){
		return openId;
	} 
		
	public void setOpenId(String openId){
			this.openId = openId;
		}
		
    @Column(name = "source")
	public String  getSource(){
		return source;
	} 
		
	public void setSource(String source){
			this.source = source;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	


} 
