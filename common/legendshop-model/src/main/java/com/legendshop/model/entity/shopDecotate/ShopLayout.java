package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家装修布局表
 */
@Entity
@Table(name = "ls_shop_layout")
public class ShopLayout implements GenericEntity<Long> {

	private static final long serialVersionUID = 6485874903381762485L;

	/** 布局ID */
	private Long layoutId; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商家店铺装修ID */
	private Long shopDecotateId; 
		
	/** 布局类型 */
	private String layoutType; 
		
	/** 布局模块类型 */
	private String layoutModuleType; 
		
	/** 自定义模块内容 */
	private String layoutContent; 
		
	/** 排序 */
	private Integer seq; 
	
	/**是否启用 */
	private Integer disable;
	
   private String title; //标题名称
	
	private String fontImg; //标题背景
	
	private String fontColor; //标题颜色
	
	private String backColor; //标题背景颜色
	
	
	/**DIV layout */
	private Long layoutDivId;
	
	private String layoutDiv;
	
	private String layoutDivModuleType;
	
	private String layoutDivContent;
		
	
	public ShopLayout() {
    }
		
	@Id
	@Column(name = "layout_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_LAYOUT_SEQ")
	public Long  getLayoutId(){
		return layoutId;
	} 
		
	public void setLayoutId(Long layoutId){
			this.layoutId = layoutId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_decotate_id")
	public Long  getShopDecotateId(){
		return shopDecotateId;
	} 
		
	public void setShopDecotateId(Long shopDecotateId){
			this.shopDecotateId = shopDecotateId;
		}
		
    @Column(name = "layout_type")
	public String  getLayoutType(){
		return layoutType;
	} 
		
	public void setLayoutType(String layoutType){
			this.layoutType = layoutType;
		}
		
	
		
    @Column(name = "layout_module_type")
	public String  getLayoutModuleType(){
		return layoutModuleType;
	} 
		
	public void setLayoutModuleType(String layoutModuleType){
			this.layoutModuleType = layoutModuleType;
		}
		
    @Column(name = "layout_content")
	public String  getLayoutContent(){
		return layoutContent;
	} 
		
	public void setLayoutContent(String layoutContent){
			this.layoutContent = layoutContent;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}
	
	
	@Transient
	public Long getId() {
		return layoutId;
	}
	
	public void setId(Long id) {
		layoutId = id;
	}

	@Column(name = "disable")
	public Integer getDisable() {
		return disable;
	}

	public void setDisable(Integer disable) {
		this.disable = disable;
	}
	
	

	@Transient
	public Long getLayoutDivId() {
		return layoutDivId;
	}

	public void setLayoutDivId(Long layoutDivId) {
		this.layoutDivId = layoutDivId;
	}
	@Transient
	public String  getLayoutDiv(){
		return layoutDiv;
	} 
		
	public void setLayoutDiv(String layoutDiv){
			this.layoutDiv = layoutDiv;
		}

	@Transient
	public String getLayoutDivModuleType() {
		return layoutDivModuleType;
	}

	public void setLayoutDivModuleType(String layoutDivModuleType) {
		this.layoutDivModuleType = layoutDivModuleType;
	}

	@Transient
	public String getLayoutDivContent() {
		return layoutDivContent;
	}

	public void setLayoutDivContent(String layoutDivContent) {
		this.layoutDivContent = layoutDivContent;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "font_img")
	public String getFontImg() {
		return fontImg;
	}

	public void setFontImg(String fontImg) {
		this.fontImg = fontImg;
	}

	@Column(name = "font_color")
	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	@Column(name = "back_color")
	public String getBackColor() {
		return backColor;
	}

	public void setBackColor(String backColor) {
		this.backColor = backColor;
	}
	
	
	

} 
