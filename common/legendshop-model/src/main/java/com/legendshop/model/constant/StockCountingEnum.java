package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;


public enum StockCountingEnum implements IntegerEnum {
	/*
	 * 库存计数方式，0：拍下减库存，1：付款减库存
	 * 
     */
	
	STOCK_BY_ORDER(0), //0: 拍下减库存

	STOCK_BY_PAY(1); //1：付款减库存

	/** The num. */
	private Integer num;

	public Integer value() {
		return num;
	}

	StockCountingEnum(Integer num) {
		this.num = num;
	}

}
