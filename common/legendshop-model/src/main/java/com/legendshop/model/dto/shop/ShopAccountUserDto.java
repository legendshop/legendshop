/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shop;

import java.util.List;

/**
 * 安全验证的商家子账号用户.
 *
 */
public class ShopAccountUserDto extends ShopUserDto {

	private static final long serialVersionUID = 5529544441030574742L;
	
	private Long id;
	
	private Long shopRoleId;
	
	/*private Long departmentId;*/

	private boolean shopAccountStatus;
	
	private String shopAccountName;
	
	private String shopAccountPassword;
	
	//获取子账号的权限
	private List<String> permList;

	public String getShopAccountName() {
		return shopAccountName;
	}

	public void setShopAccountName(String shopAccountName) {
		this.shopAccountName = shopAccountName;
	}

	public String getShopAccountPassword() {
		return shopAccountPassword;
	}

	public void setShopAccountPassword(String shopAccountPassword) {
		this.shopAccountPassword = shopAccountPassword;
	}

	public boolean isShopAccountStatus() {
		return shopAccountStatus;
	}

	public void setShopAccountStatus(boolean shopAccountStatus) {
		this.shopAccountStatus = shopAccountStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopRoleId() {
		return shopRoleId;
	}

	public void setShopRoleId(Long shopRoleId) {
		this.shopRoleId = shopRoleId;
	}

	/*public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}*/

	public List<String> getPermList() {
		return permList;
	}

	public void setPermList(List<String> permList) {
		this.permList = permList;
	}


	

}
