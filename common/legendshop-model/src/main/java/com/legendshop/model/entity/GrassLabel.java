package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *种草社区标签表
 */
@Entity
@Table(name = "ls_grass_label")
public class GrassLabel implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 标签名 */
	private String name; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 是否推荐(1推荐) */
	private Long isrecommend; 
		
	/** 引用次数 */
	private Long refcount; 
		
	
	public GrassLabel() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GRASS_LABEL_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "isrecommend")
	public Long  getIsrecommend(){
		return isrecommend;
	} 
		
	public void setIsrecommend(Long isrecommend){
			this.isrecommend = isrecommend;
		}
		
    @Column(name = "refcount")
	public Long  getRefcount(){
		return refcount;
	} 
		
	public void setRefcount(Long refcount){
			this.refcount = refcount;
		}
	


} 
