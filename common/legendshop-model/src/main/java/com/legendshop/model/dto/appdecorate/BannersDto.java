package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Tony 模版的轮播图片
 */
@ApiModel("模版的轮播图片Dto")
public class BannersDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 文本 */
	@ApiModelProperty(value = "文本")
	private String text;

	/** 价格  */
	@ApiModelProperty(value = "价格")
	private Double price;

	/** 主题 */
	@ApiModelProperty(value = "主题")
	private String themecode;

	/** 显示图片 */
	@ApiModelProperty(value = "显示图片")
	private String img;

	/** action参数: 跳转action带的参数 */
	@ApiModelProperty(value = "action参数: 跳转action带的参数")
	private AdvertsActionparamDto actionParam;

	/** 顺序 */
	@ApiModelProperty(value = "顺序")
	private int ordering;

	/** 动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题 */
	@ApiModelProperty(value = "动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题")
	private String action;

	/** 唯一标识id */
	@ApiModelProperty(value = "唯一标识id")
	private String id;

	/** 跳转路径 */
	@ApiModelProperty(value = "跳转路径")
	private String path;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getThemecode() {
		return themecode;
	}

	public void setThemecode(String themecode) {
		this.themecode = themecode;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getOrdering() {
		return ordering;
	}

	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public AdvertsActionparamDto getActionParam() {
		return actionParam;
	}

	public void setActionParam(AdvertsActionparamDto actionParam) {
		this.actionParam = actionParam;
	}

}
