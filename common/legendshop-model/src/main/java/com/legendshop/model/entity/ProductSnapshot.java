package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *商品快照
 */
@Entity
@Table(name = "ls_prod_snapshot")
public class ProductSnapshot implements GenericEntity<Long> {

	private static final long serialVersionUID = 7334390474003593257L;

	/** 快照ID */
	private Long snapshotId; 
		
	/** 产品ID */
	private Long prodId; 
	
	/** sku ID */
	private Long skuId; 
		
	/** 版本号 */
	private Integer version; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 商品名称 */
	private String name; 
		
	/** 现价 */
	private Double cash; 
		
	/** 运费 */
	private Double carriage; 
		
	/** 简要描述,卖点等 */
	private String brief; 
		
	/** 详细描述 */
	private String content; 
		
	/** 商品图片 */
	private String pic; 
		
	/** 属性json */
	private String attribute; 
		
	/** 参数json */
	private String parameter; 
		
	/** 品牌名称 */
	private String brandName; 
		
	/** 是否保修 */
	private Integer hasGuarantee; 
		
	/** 售后服务 */
	private String afterSaleService; 
		
	/** 退换货承诺 */
	private Integer rejectPromise; 
		
	/** 服务保障 */
	private Integer serviceGuarantee; 
		
	/** 商品类型，P.普通商品，G:团购商品，S:二手商品，D:打折商品 */
	private String prodType; 
		
	/** 记录时间 */
	private Date recDate; 
	
	/** 购买数量  */
	private Long prodCount;
		
	
	public ProductSnapshot() {
    }
		
	@Id
	@Column(name = "snapshot_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_SNAPSHOT_SEQ")
	public Long  getSnapshotId(){
		return snapshotId;
	} 
		
	public void setSnapshotId(Long snapshotId){
			this.snapshotId = snapshotId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "version")
	public Integer  getVersion(){
		return version;
	} 
		
	public void setVersion(Integer version){
			this.version = version;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "cash")
	public Double  getCash(){
		return cash;
	} 
		
	public void setCash(Double cash){
			this.cash = cash;
		}
		
    @Column(name = "carriage")
	public Double  getCarriage(){
		return carriage;
	} 
		
	public void setCarriage(Double carriage){
			this.carriage = carriage;
		}
		
    @Column(name = "brief")
	public String  getBrief(){
		return brief;
	} 
		
	public void setBrief(String brief){
			this.brief = brief;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "attribute")
	public String  getAttribute(){
		return attribute;
	} 
		
	public void setAttribute(String attribute){
			this.attribute = attribute;
		}
		
    @Column(name = "parameter")
	public String  getParameter(){
		return parameter;
	} 
		
	public void setParameter(String parameter){
			this.parameter = parameter;
		}
		
    @Column(name = "brand_name")
	public String  getBrandName(){
		return brandName;
	} 
		
	public void setBrandName(String brandName){
			this.brandName = brandName;
		}
		
    @Column(name = "has_guarantee")
	public Integer  getHasGuarantee(){
		return hasGuarantee;
	} 
		
	public void setHasGuarantee(Integer hasGuarantee){
			this.hasGuarantee = hasGuarantee;
		}
		
    @Column(name = "after_sale_service")
	public String  getAfterSaleService(){
		return afterSaleService;
	} 
		
	public void setAfterSaleService(String afterSaleService){
			this.afterSaleService = afterSaleService;
		}
		
    @Column(name = "reject_promise")
	public Integer  getRejectPromise(){
		return rejectPromise;
	} 
		
	public void setRejectPromise(Integer rejectPromise){
			this.rejectPromise = rejectPromise;
		}
		
    @Column(name = "service_guarantee")
	public Integer  getServiceGuarantee(){
		return serviceGuarantee;
	} 
		
	public void setServiceGuarantee(Integer serviceGuarantee){
			this.serviceGuarantee = serviceGuarantee;
		}
		
    @Column(name = "prod_type")
	public String  getProdType(){
		return prodType;
	} 
		
	public void setProdType(String prodType){
			this.prodType = prodType;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	
	@Transient
	public Long getId() {
		return snapshotId;
	}
	
	public void setId(Long id) {
		snapshotId = id;
	}

	@Column(name = "sku_id")
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	@Column(name = "prod_count")
	public Long getProdCount() {
		return prodCount;
	}

	public void setProdCount(Long prodCount) {
		this.prodCount = prodCount;
	}
	
	


} 
