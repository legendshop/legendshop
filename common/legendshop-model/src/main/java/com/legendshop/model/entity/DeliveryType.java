/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 物流配送方式
 */
@Entity
@Table(name = "ls_dvy_type")
@ApiModel(value="DeliveryType物流配送方式") 
public class DeliveryType implements GenericEntity<Long> {

	private static final long serialVersionUID = -5734275885117974527L;

	/** The dvy type id. */
	@ApiModelProperty(value="主键")
	private Long dvyTypeId;
	
	/** The user id. */
	@ApiModelProperty(value="用户ID")
	private String userId;
	
	/** The user name. */
	@ApiModelProperty(value="用户名称")
	private String userName;
	
	/** The dvy id. */
	@ApiModelProperty(value="物流公司ID")
	private Long dvyId;
	
	/** The type. */
	@ApiModelProperty(value="配送类型")
	private String type;
	
	/** The name. */
	@ApiModelProperty(value="配送方式名称")
	private String name;
	
	/** The notes. */
	@ApiModelProperty(value="描述")
	private String notes;
	
	/** The create time. */
	@ApiModelProperty(value="建立时间")
	private Date createTime;
	
	/** The modify time. */
	@ApiModelProperty(value="修改时间")
	private Date modifyTime;
	
	/**  店铺ID **/
	@ApiModelProperty(value="所属店铺ID")
	private Long shopId;
	
	@ApiModelProperty(value="打印模版ID")
	private Long printtempId;
	
	@ApiModelProperty(value="是否系统默认模版")
	private Integer isSystem;
	
	/**
	 * Gets the dvy type id.
	 * 
	 * @return the dvy type id
	 */
	@Id
	@Column(name = "dvy_type_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DVY_TYPE_SEQ")
	public Long getDvyTypeId() {
		return dvyTypeId;
	}
	
	/**
	 * Sets the dvy type id.
	 * 
	 * @param dvyTypeId
	 *            the new dvy type id
	 */
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}
	
	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the dvy id.
	 * 
	 * @return the dvy id
	 */
	@Column(name = "dvy_id")
	public Long getDvyId() {
		return dvyId;
	}
	
	/**
	 * Sets the dvy id.
	 * 
	 * @param dvyId
	 *            the new dvy id
	 */
	public void setDvyId(Long dvyId) {
		this.dvyId = dvyId;
	}
	
	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	@Column(name = "type")
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the desc.
	 * 
	 * @return the desc
	 */
	@Column(name = "notes")
	public String getNotes() {
		return notes;
	}
	
	/**
	 * Sets the desc.
	 * 
	 * @param desc
	 *            the new desc
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	/**
	 * Gets the creates the time.
	 * 
	 * @return the creates the time
	 */
	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}
	
	/**
	 * Sets the creates the time.
	 * 
	 * @param createTime
	 *            the new creates the time
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	/**
	 * Gets the modify time.
	 * 
	 * @return the modify time
	 */
	@Column(name = "modify_time")
	public Date getModifyTime() {
		return modifyTime;
	}
	
	/**
	 * Sets the modify time.
	 * 
	 * @param modifyDate
	 *            the new modify time
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Transient
	public Long getId() {
		return dvyTypeId;
	}

	public void setId(Long id) {
		this.dvyTypeId = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "printtemp_id")
	public Long getPrinttempId() {
		return printtempId;
	}

	public void setPrinttempId(Long printtempId) {
		this.printtempId = printtempId;
	}

	@Column(name = "is_system")
	public Integer getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}
	
	
	
	
}