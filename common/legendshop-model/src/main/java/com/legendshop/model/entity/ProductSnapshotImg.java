package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商品快照图片表
 */
@Entity
@Table(name = "ls_prod_snapshot_img")
public class ProductSnapshotImg implements GenericEntity<Long> {

	private static final long serialVersionUID = -1266978497069846598L;

	/** id */
	private Long id; 
		
	/** 快照id */
	private Long snapshotId; 
		
	/** 图片路径 */
	private String imgPath; 
		
	/** 图片大小 */
	private Long imgSize; 
		
	/** 宽度 */
	private Long width; 
		
	/** 高度 */
	private Long height; 
		
	/** 记录时间 */
	private Date recDate; 
		
	
	public ProductSnapshotImg() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_SNAPSHOT_IMG_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "snapshot_id")
	public Long  getSnapshotId(){
		return snapshotId;
	} 
		
	public void setSnapshotId(Long snapshotId){
			this.snapshotId = snapshotId;
		}
		
    @Column(name = "img_path")
	public String  getImgPath(){
		return imgPath;
	} 
		
	public void setImgPath(String imgPath){
			this.imgPath = imgPath;
		}
		
    @Column(name = "img_size")
	public Long  getImgSize(){
		return imgSize;
	} 
		
	public void setImgSize(Long imgSize){
			this.imgSize = imgSize;
		}
		
    @Column(name = "width")
	public Long  getWidth(){
		return width;
	} 
		
	public void setWidth(Long width){
			this.width = width;
		}
		
    @Column(name = "height")
	public Long  getHeight(){
		return height;
	} 
		
	public void setHeight(Long height){
			this.height = height;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	


} 
