
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 文章分类DTO
 *
 */
public class NewsCategoryDto  implements Serializable {

	private static final long serialVersionUID = 107108647774926285L;

	private Long newsCategoryId;
	
	private String newsCategoryName;
	
	private String newsCategoryPic;
	
	private List<NewsDto> newsDtoList;

	public Long getNewsCategoryId() {
		return newsCategoryId;
	}

	public void setNewsCategoryId(Long newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}

	public String getNewsCategoryName() {
		return newsCategoryName;
	}

	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}

	public String getNewsCategoryPic() {
		return newsCategoryPic;
	}

	public void setNewsCategoryPic(String newsCategoryPic) {
		this.newsCategoryPic = newsCategoryPic;
	}

	public List<NewsDto> getNewsDtoList() {
		return newsDtoList;
	}

	public void setNewsDtoList(List<NewsDto> newsDtoList) {
		this.newsDtoList = newsDtoList;
	}
	
	
}
