/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 邮件主机端口  
 */
public enum MailPortEnum implements IntegerEnum {

	/** 普通端口. */
	COMMON(25), 

	/** 安全协议端口. */
	SAFETY(465);

	/** The num. */
	private Integer port;

	/**
	 * Instantiates a new consult type enum.
	 * 
	 * @param num
	 *            the num
	 */
	MailPortEnum(Integer port) {
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return port;
	}

}
