/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 支付类型变量
 */
public enum PayTypeEnum implements StringEnum {

	/** 支付宝担保支付. */
	ALI_PAY("ALP","支付宝支付"),
	
	/**财付通担保支付**/
	TENPAY("TDP","财付通支付"),
	
	/**PAYPAL支付**/
	PAYPAL("PAP","PAYPAL支付"),
	
	/** 快钱支付 **/
	KQ_PAY("KQP","快钱支付"),
	
	/** 微信支付 */
	WX_PAY("WX_PAY","微信支付"),
	
	/** 微信原生APP支付 */
	WX_APP_PAY("WX_APP_PAY","微信APP支付"),
	
	/** 微信小程序支付 */
	WX_MP_PAY("WX_MP_PAY", "微信小程序支付"),
	
	/** 通联支付 */
	TONGLIAN_PAY("TONGLIAN_PAY","通联支付"),
	
	/** 通联跨境支付  */
	TONGLIAN_CROSSBORDER_PAY("TONGLIAN_CROSSBORDER_PAY","通联跨境支付"),
	
	/** 通联移动支付 */
	TONGLIAN_WAP_PAY("TONGLIAN_WAP_PAY","通联移动支付"),
	
	/** 商城模拟支付  */
	SIMULATE_PAY("SIMULATE","商城模拟支付"),
	
	/** 商城预存款全款支付  */
	FULL_PAY("FULL_PAY","全款支付"),
	
	UNIONPAY("UNION_PAY","在线银联支付"),
	
	JD_PAY("JD_PAY","京东支付"),
	
	;
	/** The value. */
	private final String value;
	
	private final String desc;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private PayTypeEnum(String value,String desc) {
		this.value = value;
		this.desc=desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String desc() {
		return this.desc;
	}
	
	
	public static boolean isWeiXin(String value){
		if(value.startsWith("WX_")){
			return true;
		}
		return false;
	}
	
	public static boolean isAliPay(String value){
		if(value.startsWith("ALP")){
			return true;
		}
		return false;
	}
	
	
	
	public static PayTypeEnum fromCode(String code) {
		for (PayTypeEnum cartTypeEnum : PayTypeEnum.values()) {
			if (cartTypeEnum.value.equals(code)) {
				return cartTypeEnum;
			}
		}
		return null;
	}
	
}
