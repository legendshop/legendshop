/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文章分类实体类
 */
@Entity
@Table(name = "ls_news_cat")
@ApiModel(value="NewsCategory文章分类") 
public class NewsCategory extends UploadFile implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8019865349891607453L;

	/** 主键ID. */
	@ApiModelProperty(value="主键ID")  
	private Long id;
	
	/** 新闻栏目ID. */
	@ApiModelProperty(value="新闻栏目ID")  
	private Long newsCategoryId;

	/** 新闻栏目名称. */
	@ApiModelProperty(value="新闻栏目名称")  
	private String newsCategoryName;

	/** 状态. */
	@ApiModelProperty(value="状态")  
	private Short status;

	/** 顺序. */
	@ApiModelProperty(value="顺序")  
	private Integer seq;

	/** 发表时间. */
	@ApiModelProperty(value="发表时间")  
	private Date newsCategoryDate;

	/** 用户id. */
	@ApiModelProperty(value="用户id")  
	private String userId;

	/** 用户名. */
	@ApiModelProperty(value="用户名")  
	private String userName;

	/** 图片 */
	@ApiModelProperty(value="图片")  
	private String pic;

	/** 子节点  */
	@ApiModelProperty(value="子节点")  
	List<News> subNews = new ArrayList<News>();

	/**
	 * Instantiates a new news category.
	 */
	public NewsCategory() {
	}

	/**
	 * Gets the news category id.
	 * 
	 * @return the news category id
	 */
	@Id
	@Column(name = "news_category_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "NEWS_CAT_SEQ")
	public Long getNewsCategoryId() {
		return newsCategoryId;
	}

	/**
	 * Sets the news category id.
	 * 
	 * @param newsCategoryId
	 *            the new news category id
	 */
	public void setNewsCategoryId(Long newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}

	/**
	 * Gets the news category name.
	 * 
	 * @return the news category name
	 */
	@Column(name = "news_category_name")
	public String getNewsCategoryName() {
		return newsCategoryName;
	}

	/**
	 * Sets the news category name.
	 * 
	 * @param newsCategoryName
	 *            the new news category name
	 */
	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	@Column(name = "status")
	public Short getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * Gets the news category date.
	 * 
	 * @return the news category date
	 */
	@Column(name = "news_category_date")
	public Date getNewsCategoryDate() {
		return newsCategoryDate;
	}

	/**
	 * Sets the news category date.
	 * 
	 * @param newsCategoryDate
	 *            the new news category date
	 */
	public void setNewsCategoryDate(Date newsCategoryDate) {
		this.newsCategoryDate = newsCategoryDate;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return newsCategoryId;
	}

	public void setId(Long id) {
		this.newsCategoryId = id;
	}

	/**
	 * Gets the seq.
	 * 
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 * 
	 * @param seq
	 *            the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	
	@Transient
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 增加文章文章
	 * 
	 * @param news
	 */
	public void addNews(News news) {
		if (this.getNewsCategoryId().equals(news.getNewsCategoryId())) {
			subNews.add(news);
		}

	}

	@Transient
	public List<News> getSubNews() {
		return subNews;
	}

	public void setSubNews(List<News> subNews) {
		this.subNews = subNews;
	}

	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}
