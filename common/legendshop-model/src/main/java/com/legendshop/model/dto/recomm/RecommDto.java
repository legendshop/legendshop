package com.legendshop.model.dto.recomm;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * 分类导航
 *
 */
public class RecommDto implements Serializable{

	private static final long serialVersionUID = -5394940932762390275L;
	
	private Long categoryId;
	
	private String advPic;
	
	private MultipartFile advPicFile;
	
	private String advLink;
	
	String[] brandIds ;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public MultipartFile getAdvPicFile() {
		return advPicFile;
	}

	public void setAdvPicFile(MultipartFile advPicFile) {
		this.advPicFile = advPicFile;
	}

	public String getAdvLink() {
		return advLink;
	}

	public void setAdvLink(String advLink) {
		this.advLink = advLink;
	}

	public String getAdvPic() {
		return advPic;
	}

	public void setAdvPic(String advPic) {
		this.advPic = advPic;
	}

	public String[] getBrandIds() {
		return brandIds;
	}

	public void setBrandIds(String[] brandIds) {
		this.brandIds = brandIds;
	}
	
	

	
}
