/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 我的商品收藏
 */
@Entity
@Table(name = "ls_favorite")
public class Myfavorite  implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4386671446122853240L;

	/** The id. */
	private Long id;

	/** 商品id. */
	private Long prodId;

	/** 商品名称. */
	private String prodName;

	/** 添加时间. */
	private Date addtime;

	/** 用户id. */
	private String userId;

	/** 用户名. */
	private String userName;
	
	/** 商品图片 */
	private String pic;
	/** 商品评论 */
	private Integer comments;
	
	/** 商品库存 */
	private Integer stocks;
	
	/** 商品购买数 */
	private Integer buys;
	
	/** 商品现价 */
	private Double cash;
	
	/** 商品原价 */
	private Double price;
	
	/**已收藏商品数*/
	private Long favoriteCount;


	/**
	 * default constructor.
	 */
	public Myfavorite() {
	}
	


	@Transient
	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * minimal constructor.
	 * 
	 * @param id
	 *            the id
	 */
	public Myfavorite(Long id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MYFAVOIRTE_SEQ")
	public Long getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the prod id.
	 * 
	 * @return the prod id
	 */
	@Column(name="prod_id")
	public Long getProdId() {
		return this.prodId;
	}

	/**
	 * Sets the prod id.
	 * 
	 * @param prodId
	 *            the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the prod name.
	 * 
	 * @return the prod name
	 */
	@Transient
	public String getProdName() {
		return this.prodName;
	}

	/**
	 * Sets the prod name.
	 * 
	 * @param prodName
	 *            the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the recDate.
	 * 
	 * @return the recDate
	 */
	@Column(name="addtime")
	public Date getAddtime() {
		return this.addtime;
	}

	/**
	 * Sets the recDate.
	 *
	 * @param addtime the new addtime
	 */
	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}
	

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Column(name="user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	@Transient
	public Double getPrice() {
		return price;
	}


	/**
	 * Sets the price.
	 *
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	@Transient
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Transient
	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		if(comments==null){
			comments = 0;
		}
		this.comments = comments;
	}

	@Transient
	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	@Transient
	public Integer getBuys() {
		return buys;
	}

	public void setBuys(Integer buys) {
		this.buys = buys;
	}

	@Transient
	public Long getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	
}