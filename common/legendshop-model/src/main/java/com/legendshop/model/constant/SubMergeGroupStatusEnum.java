package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 拼团订单状态
 */
public enum SubMergeGroupStatusEnum implements IntegerEnum{
	
	UNPAY(-2),//未支付
	
	MERGE_GROUP_IN(0),//拼团进行中
	
	MERGE_GROUP_FAIL(-1),//拼团失败
	
	MERGE_GROUP_SUCCESS(1),//拼团成功
	
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		return num;
	}

	private SubMergeGroupStatusEnum(Integer num) {
		this.num = num;
	}
	
}
