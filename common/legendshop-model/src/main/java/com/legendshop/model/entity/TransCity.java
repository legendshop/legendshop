package com.legendshop.model.entity;
import java.io.Serializable;

import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.Table;

/**
 *每个城市的运费设置
 */
@Entity
@Table(name = "ls_transcity")
public class TransCity implements Serializable{

	private static final long serialVersionUID = -820611429961876423L;

	/** 运费ID */
	private Long transfeeId; 
		
	/**  */
	private Long cityId; 
		
	
	public TransCity() {
    }
		
	public Long  getTransfeeId(){
		return transfeeId;
	} 
		
	public void setTransfeeId(Long transfeeId){
			this.transfeeId = transfeeId;
		}
		
	public Long  getCityId(){
		return cityId;
	} 
		
	public void setCityId(Long cityId){
			this.cityId = cityId;
	}
	
} 
