package com.legendshop.model.dto;

import java.io.Serializable;
/**
 * 用來保存密碼
 * @author Administrator
 *
 */
public class IdPassord implements Serializable {

	private static final long serialVersionUID = -8346972874209991646L;
	
	private String id;
	private String name;
	private String password;
	public IdPassord(){
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
