/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 排序方式
 */
public enum SortTypeEnum implements StringEnum {
	/**  
	 * 升序: ASC
	 */
	ASC("asc"),
	
	/**  
	 * 降序: desc
	 */
	DESC("desc");

	private final String value;

	private SortTypeEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		SortTypeEnum[] enums = values();
		for (SortTypeEnum appEnum : enums) {
			if (appEnum.value().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
