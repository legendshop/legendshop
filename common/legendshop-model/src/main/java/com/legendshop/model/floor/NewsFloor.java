package com.legendshop.model.floor;

import java.io.Serializable;

public class NewsFloor implements Serializable{
	private static final long serialVersionUID = 3318371070285607333L;

	private Long id;
	
	private Integer seq;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	
}
