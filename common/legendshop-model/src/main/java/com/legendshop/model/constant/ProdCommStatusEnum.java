package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 商品评论状态
 * @author 开发很忙
 *
 */
public enum ProdCommStatusEnum implements IntegerEnum {

	/** 待审核 */
	WAIT_AUDIT(0), 
	
	/** 审核通过 */
	SUCCESS(1),
	
	/** 审核失败 */
	FAIL(-1),
	;

	/** The num. */
	private Integer num;

	ProdCommStatusEnum(Integer num) {
		this.num = num;
	}

	public Integer value() {
		return num;
	}

}
