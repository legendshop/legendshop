package com.legendshop.model.constant;

import com.legendshop.util.AppUtils;
import com.legendshop.util.constant.StringEnum;

/**
 * 活动类型
 */
public enum ActivityTypeEnum implements StringEnum {

	/** 满减 **/
	FULL_REDUCTION("FULL_REDUCTION"),

	/** 折扣 **/
	DISCOUNT("DISCOUNT"),

	/** 优惠券 **/
	COUPON("COUPON"),

	/** 包邮 **/
	FREE_SHIPPING("FREE_SHIPPING"),

	/** 团购活动 */
	GROUP("GROUP"),

	/**拼团活动  */
	MERGE_GROUP("MERGE"),

	/** 秒杀活动 */
	SECKILL("SECKILL"),

	/** 拍卖活动  */
	AUCTION("AUCTION"),

	/** 预售活动 */
	PRESELL("PRESELL"),

	/** 其它 **/
	OTHER("OTHER")
	;



	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 *
	 * @param value
	 *            the value
	 */
	ActivityTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	@Override
	public String value() {
		return this.value;
	}

	/**
	 * 匹配类型
	 * @param type
	 * @return
	 */
	public static ActivityTypeEnum matchType(String type) {
		if (AppUtils.isNotBlank(type)) {
			for (ActivityTypeEnum typeEnum : ActivityTypeEnum.values()) {
				if (typeEnum.name().equalsIgnoreCase(type)) {
					return typeEnum;
				}
			}
		}
		return OTHER;
	}
}
