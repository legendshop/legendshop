/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 定时器用的商家实体类.
 */
public class ShopDetailTimerDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9001963640831542538L;
	
	/** The shop id. */
	private Long shopId;
	
    /** 商城名字. */
	private String siteName;
	
	/** 商城独立分销比例 */
	private Integer commissionRate;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Integer getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(Integer commissionRate) {
		this.commissionRate = commissionRate;
	}
	
	

}
