/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商品排序方式
 */
public enum ProdOrderWayEnum implements StringEnum {
	/**  
	 * 综合排序: recDate
	 */
	recDate("recDate"),
	
	/**  
	 * 销量排序: buys
	 */
	BUYS("buys"),
	
	/**
	 * 好评排序: comments
	 */
	Comments("comments"), 
	
	/** 价格: cash */
	CASH("cash"),
	
	;

	private final String value;

	private ProdOrderWayEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		ProdOrderWayEnum[] enums = values();
		for (ProdOrderWayEnum appEnum : enums) {
			if (appEnum.value().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
