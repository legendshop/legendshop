package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *发现文章评论表
 */
@Entity
@Table(name = "ls_discover_comm")
public class DiscoverComme implements GenericEntity<Long> {

	/** id */
	private Long id; 
		
	/** 发现文章id */
	private Long disId; 
		
	/** 用户id */
	private String userId; 
		
	/** 评论内容 */
	private String content; 
	
	/** 用户名 */
	private String userName; 
	
	/** 用户头像 */
	private String userImage; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** 评论状态（0）未审核（1）审核 */
	private Boolean status; 
		
	
	public DiscoverComme() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DISCOVER_COMM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "dis_id")
	public Long  getDisId(){
		return disId;
	} 
		
	public void setDisId(Long disId){
			this.disId = disId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "create_time")
	public Date getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
		
    @Column(name = "status")
	public Boolean  getStatus(){
		return status;
	} 
		
	public void setStatus(Boolean status){
			this.status = status;
		}
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name = "user_image")
	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	


} 
