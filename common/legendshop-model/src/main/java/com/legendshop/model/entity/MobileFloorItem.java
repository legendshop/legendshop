package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 手机端html5 楼层内容关联表
 */
@Entity
@Table(name = "ls_m_floor_item")
public class MobileFloorItem extends UploadFile implements GenericEntity<Long> {

	private static final long serialVersionUID = -6262137622057868002L;

	/** id */
	private Long id; 
		
	/** 楼层id */
	private Long parentId; 
	/** 顺序 */
		
	private Long seq; 
		
	/** 状态；0：下线；1：上线； */
	private int status; 
		
	/** 创建时间 */
	private Date recDate; 
	
	/** 标题 **/
	private String title;
	
	/** 链接 **/
	private String linkPath;
	
	/** 图片 **/
	private String pic;
		
	
	public MobileFloorItem() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "M_FLOOR_ITEM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
	}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
		
    @Column(name = "status")
	public int  getStatus(){
		return status;
	} 
		
	public void setStatus(int status){
			this.status = status;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Column(name = "link_path")
	public String getLinkPath() {
		return linkPath;
	}

	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}

	@Column(name = "parent_id")
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Transient
	public String getUserName() {
		return null;
	}

} 
