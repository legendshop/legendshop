package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum SubHistoryEnum implements StringEnum {

	/** The ORDE r_ capture. */
	ORDER_SUBMIT("CA"),  // 下订单
	
	ORDER_PAY("PY"), // 订单支付
	
	ORDER_RETURNGOOD("RG"), // 订单退货退款
	
	ORDER_RETURNMONEY("RM"), // 订单退款
	
	AGREED_RETURNGOOD("ARG"), // 商家同意退货退款
	
	AGREED_RETURNMONEY("ARM"), // 商家同意退款
	
	AUDIT_RETURNGOOD("URG"), //  平台审核通过退货退款
	
	AUDIT_RETURNMONEY("URM"), //  平台审核通过退款
	
	BUYERS_RETURNGOOD("BRG"), //  买家退货
	
	SELLER_RETURNGOOD("SRG"), //  卖家收到退货
	
	DELIVER_GOODS("DG"), // 确认发货
	
	TAKE_DELIVER("TD"), //确认收货
	
	TAKE_DELIVER_TIME("TDT"), //超时,自动确认收货
	
	EVALUATE_ORDER("TD"), //评价订单
	
	APPEND_EVALUATE_ORDER("ATD"), //追加评价订单
	
	ORDER_DEL("DE"), // 删除订单
	
	ORDER_PERMANENT_DEL("PDE"), // 永久删除订单
	
	ORDER_CANCEL("OC"), // 取消订单
	
	/** The ORDE r_ ove r_ time. */
	ORDER_OVER_TIME("OT"), // 超时,自动取消订单

	/** The PRIC e_ change. */
	PRICE_CHANGE("PC"), // 改价格

	/** The CREDI t_ score. */
	CREDIT_SCORE("CS"), // 增加积分

	/** The DEBI t_ score. */
	DEBIT_SCORE("DS"), // 使用积分

	/** The CHANG e_ status. */
	CHANGE_STATUS("ST"), // 订单状态变化
	
	CHANGE_INFO("SI"); // 订单信息变化
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new sub status enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SubHistoryEnum(String value) {
		this.value = value;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
	
	
	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		SubHistoryEnum[] licenseEnums = values();
		for (SubHistoryEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
	
	

}
