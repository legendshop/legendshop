package com.legendshop.model.dto;

import java.io.Serializable;

import com.legendshop.dao.support.PageSupport;



/**
 * 
 *@author Tony
 *
 */
public class UserPredepositDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Double availablePredeposit=0d; //可用余额  Pre-deposit
	
	private Double freezePredeposit=0d;//冻结余额  Pre-deposit
	
	
	private PageSupport pdCashLogs;
	
	private PageSupport rechargeDetails;
	
	private PageSupport pdCashs;
	
	private PageSupport cashHodings;

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public Double getFreezePredeposit() {
		return freezePredeposit;
	}

	public void setFreezePredeposit(Double freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}

	public PageSupport getPdCashLogs() {
		return pdCashLogs;
	}

	public void setPdCashLogs(PageSupport pdCashLogs) {
		this.pdCashLogs = pdCashLogs;
	}

	public PageSupport getRechargeDetails() {
		return rechargeDetails;
	}

	public void setRechargeDetails(PageSupport rechargeDetails) {
		this.rechargeDetails = rechargeDetails;
	}

	public PageSupport getPdCashs() {
		return pdCashs;
	}

	public void setPdCashs(PageSupport pdCashs) {
		this.pdCashs = pdCashs;
	}

	public PageSupport getCashHodings() {
		return cashHodings;
	}

	public void setCashHodings(PageSupport cashHodings) {
		this.cashHodings = cashHodings;
	}

	
	
	
	
}
