/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.io.Serializable;

/**
 * 热门数据Dto.
 */
public class HotDataDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5054957071323487627L;

	/** 主机URL. */
	private String hotspotUrl;

	/** The x 1位置. */
	private Integer x1;

	/** The x 2 位置. */
	private Integer x2;

	/** The y 1 位置. */
	private Integer y1;

	/** The y 2 位置. */
	private Integer y2;
	

	/*
	 *  bind-coor=363,53,537,163
		width=174
		height=110
		top=53
		left=363
		
		    var width = x2 - x1;
			var height = y2 - y1;
			var top = y1;
			var left = x1;
			
	 */

	/**
	 * Gets the hotspot url.
	 *
	 * @return the hotspot url
	 */
	public String getHotspotUrl() {
		return hotspotUrl;
	}

	/**
	 * Sets the hotspot url.
	 *
	 * @param hotspotUrl the new hotspot url
	 */
	public void setHotspotUrl(String hotspotUrl) {
		this.hotspotUrl = hotspotUrl;
	}

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HotDataDto [hotspotUrl=" + hotspotUrl + ", x1=" + x1 + ", x2="
				+ x2 + ", y1=" + y1 + ", y2=" + y2 + "]";
	}
	
	/**
	 * Gets the coords.
	 *
	 * @return the coords
	 */
	public String getCoords(){
		//" 属性未遵循 "left,top,right,bottom" 格式
		//left x1 top y1 x1 right bottom y2
		//return (x1+10) +"," + (y1+30) + ","+ (x2+20) +","+(y2+38);
		return x1 +"," + y1 + ","+x2+","+y2;
		
	}

	/**
	 * Gets the x1.
	 *
	 * @return the x1
	 */
	public Integer getX1() {
		return x1;
	}

	/**
	 * Sets the x1.
	 *
	 * @param x1 the new x1
	 */
	public void setX1(Integer x1) {
		this.x1 = x1;
	}

	/**
	 * Gets the x2.
	 *
	 * @return the x2
	 */
	public Integer getX2() {
		return x2;
	}

	/**
	 * Sets the x2.
	 *
	 * @param x2 the new x2
	 */
	public void setX2(Integer x2) {
		this.x2 = x2;
	}

	/**
	 * Gets the y1.
	 *
	 * @return the y1
	 */
	public Integer getY1() {
		return y1;
	}

	/**
	 * Sets the y1.
	 *
	 * @param y1 the new y1
	 */
	public void setY1(Integer y1) {
		this.y1 = y1;
	}

	/**
	 * Gets the y2.
	 *
	 * @return the y2
	 */
	public Integer getY2() {
		return y2;
	}

	/**
	 * Sets the y2.
	 *
	 * @param y2 the new y2
	 */
	public void setY2(Integer y2) {
		this.y2 = y2;
	}

	

}
