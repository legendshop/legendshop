/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;
import java.util.List;

/**
 * 封装查询的结果Dto
 */
public class SearchResult implements Serializable, Cloneable {

	private static final long serialVersionUID = 8356623578249987473L;

	/** 返回的总记录数. */
	private long total;

	/** 记录的内容. */
	private List<?> resultList;

	/**
	 * Instantiates a new key value entity.
	 */
	public SearchResult() {

	}
	
	public SearchResult(long total, List<?> resultList) {
		super();
		this.total = total;
		this.resultList = resultList;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<?> getResultList() {
		return resultList;
	}

	public void setResultList(List<?> resultList) {
		this.resultList = resultList;
	}



}
