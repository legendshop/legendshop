package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信订阅的消息
 */
@Entity
@Table(name = "ls_weixin_subscribe")
public class WeixinSubscribe implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/** 添加时间 */
	private Date addtime; 
		
	/** 消息类型 */
	private String msgType; 
		
	/** 模版ID */
	private Long templateId; 
		
	/** 模版名称 */
	private String templatename; 
	
	private String content;
		
	
	public WeixinSubscribe() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_SUBSCRIBE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "addtime")
	public Date  getAddtime(){
		return addtime;
	} 
		
	public void setAddtime(Date addtime){
			this.addtime = addtime;
		}
		
    @Column(name = "msgType")
	public String  getMsgType(){
		return msgType;
	} 
		
	public void setMsgType(String msgType){
			this.msgType = msgType;
		}
		
    @Column(name = "templateId")
	public Long  getTemplateId(){
		return templateId;
	} 
		
	public void setTemplateId(Long templateId){
			this.templateId = templateId;
		}
		
    @Column(name = "templateName")
	public String  getTemplatename(){
		return templatename;
	} 
		
	public void setTemplatename(String templatename){
			this.templatename = templatename;
		}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	


} 
