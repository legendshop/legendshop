package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModelProperty;

/**
 *拍卖保证金
 * @author legendshop
 */
@Entity
@Table(name = "ls_auction_depositrec")
public class AuctionDepositRec implements GenericEntity<Long> {

	private static final long serialVersionUID = -7293061401991908446L;

	/** ID */
	private Long id; 
		
	/** 拍卖活动ID */
	private Long aId;

	/** 商户系统内部的订单号 */
	private String subSettlementSn;

	/** 定金订单编号 */
	private String subNumber; 
		
	/** 缴纳费用 */
	private java.math.BigDecimal payMoney; 
		
	/** 用户ID */
	private String userId; 
		
	/** 订单状态 -1:未支付 1:支付成功 */
	private Integer orderStatus; 
		
	/** 支付类型 */
	private String payType; 
		
	/** 支付名 */
	private String payName; 
		
	/** 缴纳时间 */
	private Date payTime; 
	
	/** 标识状态（退还定金），0未处理，1已处理 */
	private Long flagStatus;

	/** 违反规则扣除定金标识 */
	private int detainFlag;

	/** 商家ID */
	private Long shopId;

	/** 是否结算  */
	private boolean isBill;

	/** 订单结算编号[用于结算档期统计]  */
	private String billSn;

	/** 拍卖活动名称 */
	private String auctionsTitle;
	
	/** 用户名 */
	private String userName;

	/** 昵称 */
	private String nickName;
	
	public AuctionDepositRec() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "AUCTION_DEPOSITREC_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "a_id")
	public Long  getAId(){
		return aId;
	} 
		
	public void setAId(Long aId){
			this.aId = aId;
		}
		
    @Column(name = "sub_number")
	public String  getSubNumber(){
		return subNumber;
	} 
		
	public void setSubNumber(String subNumber){
			this.subNumber = subNumber;
		}
		
    @Column(name = "pay_money")
	public java.math.BigDecimal  getPayMoney(){
		return payMoney;
	} 
		
	public void setPayMoney(java.math.BigDecimal payMoney){
			this.payMoney = payMoney;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "order_status")
	public Integer  getOrderStatus(){
		return orderStatus;
	} 
		
	public void setOrderStatus(Integer orderStatus){
			this.orderStatus = orderStatus;
		}
		
    @Column(name = "pay_type")
	public String  getPayType(){
		return payType;
	} 
		
	public void setPayType(String payType){
			this.payType = payType;
		}
		
    @Column(name = "pay_name")
	public String  getPayName(){
		return payName;
	} 
		
	public void setPayName(String payName){
			this.payName = payName;
		}
		
    @Column(name = "pay_time")
	public Date  getPayTime(){
		return payTime;
	} 
		
	public void setPayTime(Date payTime){
			this.payTime = payTime;
		}
	@Column(name = "flag_status")
	public Long getFlagStatus() {
		return flagStatus;
	}

	public void setFlagStatus(Long flagStatus) {
		this.flagStatus = flagStatus;
	}

	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	@Column(name = "sub_settlement_sn")
	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	@Column(name = "detain_flag")
	public int getDetainFlag() {
		return detainFlag;
	}

	public void setDetainFlag(int detainFlag) {
		this.detainFlag = detainFlag;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "is_bill")
	public boolean getIsBill() {
		return isBill;
	}

	public void setIsBill(boolean bill) {
		isBill = bill;
	}

	@Column(name = "bill_sn")
	public String getBillSn() {
		return billSn;
	}

	public void setBillSn(String billSn) {
		this.billSn = billSn;
	}

	@Column(name = "auctions_title")
	public String getAuctionsTitle() {
		return auctionsTitle;
	}

	public void setAuctionsTitle(String auctionsTitle) {
		this.auctionsTitle = auctionsTitle;
	}
}
