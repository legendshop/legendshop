package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *标签里的文章
 */
@Entity
@Table(name = "ls_tag_item_news")
public class TagItemNews implements GenericEntity<Long> {

	private static final long serialVersionUID = 3258697636291360827L;

	/** 主键  */
	private Long id; 
		
	/** 文章Id */
	private Long newsId; 
		
	/** 标签itemId */
	private Long tagItemId; 
		
	/** 记录时间 */
	private Date recDate; 
	
	
	public TagItemNews() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TAG_ITEM_NEWS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "news_id")
	public Long  getNewsId(){
		return newsId;
	} 
		
	public void setNewsId(Long newsId){
			this.newsId = newsId;
		}
		
    @Column(name = "tag_item_id")
	public Long  getTagItemId(){
		return tagItemId;
	} 
		
	public void setTagItemId(Long tagItemId){
			this.tagItemId = tagItemId;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}


} 
