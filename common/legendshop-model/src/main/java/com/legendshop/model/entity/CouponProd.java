package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 优惠券商品
 */
@Entity
@Table(name = "ls_coupon_prod")
public class CouponProd implements GenericEntity<Long> {

	private static final long serialVersionUID = -3790751812938092717L;

	/** id */
	private Long couponProdId; 
		
	/** 优惠券id */
	private Long couponId; 
		
	/** 商品id */
	private Long prodId; 
		
	
	public CouponProd() {
    }
		
	@Id
	@Column(name = "coupon_prod_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "COUPON_PROD_SEQ")
	public Long  getCouponProdId(){
		return couponProdId;
	} 
		
	public void setCouponProdId(Long couponProdId){
			this.couponProdId = couponProdId;
		}
		
    @Column(name = "coupon_id")
	public Long  getCouponId(){
		return couponId;
	} 
		
	public void setCouponId(Long couponId){
			this.couponId = couponId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
	
	@Transient
	public Long getId() {
		return couponProdId;
	}
	
	public void setId(Long id) {
		couponProdId = id;
	}


} 
