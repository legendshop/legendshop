/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 系统关键角色,不可以刪除
 * 
 */
public enum FunctionEnum implements StringEnum {
	
	/** 前台用户权限 **/
	FUNCTION_USER("F_USER"), 

	/** 查看所有用户数据的权限 **/
	FUNCTION_VIEW_ALL_DATA("F_VIEW_ALL_DATA"),

	/** 管理员 **/
	FUNCTION_F_ADMIN("F_ADMIN"),

	/** 系统权限 **/
	FUNCTION_F_SYSTEM("F_SYSTEM");

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private FunctionEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		FunctionEnum[] licenseEnums = values();
		for (FunctionEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		FunctionEnum[] licenseEnums = values();
		for (FunctionEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
