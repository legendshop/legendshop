package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 * 商品分组
 */
@Entity
@Table(name = "ls_prod_group")
public class ProdGroup implements GenericEntity<Long> {

	/**  */
	private static final long serialVersionUID = -6301232236734806876L;

	/** 主键ID */
	private Long id; 
		
	/** 商品分组名称 */
	private String name; 
		
	/** 分组类型  0:系统定义 1:自定义   */
	private Integer type;
	
	/** 分组条件 */
	private String conditional;
	
	/** 组内排序 */
	private String sort;
		
	/** 商品分组描述 */
	private String description; 
		
	/** 创建/修改时间 */
	private Date recDate; 
		
	
	public ProdGroup() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_GROUP_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
	
	@Column(name = "type")
    public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "conditional")
	public String getConditional() {
		return conditional;
	}

	public void setConditional(String conditional) {
		this.conditional = conditional;
	}

	@Column(name = "sort")
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Column(name = "description")
	public String  getDescription(){
		return description;
	} 
		
	public void setDescription(String description){
			this.description = description;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	


} 
