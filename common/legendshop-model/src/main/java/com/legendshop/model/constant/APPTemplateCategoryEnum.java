package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * app装修页面类型
 *
 */

public enum APPTemplateCategoryEnum implements StringEnum {

	/**首页*/
	INDEX("INDEX"), 
	
	/**海报*/
	POSTER("POSTER"),
	
	;
	
	/** The value. */
	private final String value;
	
	/**
	 * 
	 * @param value
	 */
	private APPTemplateCategoryEnum(String value) {
		this.value = value;
	}
	
	
	public String value() {
		return this.value;
	}

}
