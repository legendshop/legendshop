/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.security;

import java.io.Serializable;
import java.util.List;

/**
 * 商家管理菜单项.
 */
public class ShopMenu implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9159434666445897447L;
	
	private String htmlId;
	
	private String target = "_self";
	
	/** 英文标签，唯一的ID. */
	private String label;
	
	/** 顺序. */
	private Integer seq;

	/** 页面显示的名字. */
	private String name;
	
	private String pluginId; //插件ID
	
	/** 页面里对应的URL，第一个为菜单的URL. 查看权限保护的url集合 */
	private List<String> urlList;
	
	/** 编辑权限保护的url集合 */
	private List<String> editorUrlList;
	
	/** 是否选择中. */
	private boolean isSelected = false;
	
	/** 是否选中查看权限  */
	private boolean checkFunc = false;
	
	/** 是否选中编辑权限  */
	private boolean editorFunc = false;
	
	/** 菜单子权限 */
	private String functions;

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the url list.
	 *
	 * @return the url list
	 */
	public List<String> getUrlList() {
		return urlList;
	}

	/**
	 * Sets the url list.
	 *
	 * @param urlList the new url list
	 */
	public void setUrlList(List<String> urlList) {
		this.urlList = urlList;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getHtmlId() {
		return htmlId;
	}

	public void setHtmlId(String htmlId) {
		this.htmlId = htmlId;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getPluginId() {
		return pluginId;
	}

	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public List<String> getEditorUrlList() {
		return editorUrlList;
	}

	public void setEditorUrlList(List<String> editorUrlList) {
		this.editorUrlList = editorUrlList;
	}

	public boolean isCheckFunc() {
		return checkFunc;
	}

	public void setCheckFunc(boolean checkFunc) {
		this.checkFunc = checkFunc;
	}

	public boolean isEditorFunc() {
		return editorFunc;
	}

	public void setEditorFunc(boolean editorFunc) {
		this.editorFunc = editorFunc;
	}

	public String getFunctions() {
		return functions;
	}

	public void setFunctions(String function) {
		this.functions = function;
	}
}
