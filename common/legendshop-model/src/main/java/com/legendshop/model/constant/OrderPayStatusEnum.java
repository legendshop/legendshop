/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 订单支付状态
 */
public enum OrderPayStatusEnum implements IntegerEnum {

	/** 没有付款. */
	UNPAY(0),

	/** 已经付款, 已经付款的订单无法删除. */
	PADYED(1),
	
	/** 订单取消. */
	CANCEL(3),
	
	/**已完成**/
	COMPLETED(2);
	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	OrderPayStatusEnum(Integer num) {
		this.num = num;
	}


}
