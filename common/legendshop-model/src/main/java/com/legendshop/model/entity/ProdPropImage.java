package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *属性图片
 */
@Entity
@Table(name = "ls_prop_image")
public class ProdPropImage implements GenericEntity<Long> {

	private static final long serialVersionUID = -473366926789786034L;

	/** 主键 */
	private Long id; 
		
	/** 商品ID */
	private Long prodId; 
		
	/** 属性ID */
	private Long propId; 
		
	/** 属性值ID */
	private Long valueId; 
		
	/** 属性值名称 */
	private String valueName; 
		
	/** 图片Url */
	private String url; 
		
	/** 顺序 */
	private Long seq; 
		
	/** 创建时间 */
	private Date createDate; 
		
	
	public ProdPropImage() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROP_IMAGE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "prop_id")
	public Long  getPropId(){
		return propId;
	} 
		
	public void setPropId(Long propId){
			this.propId = propId;
		}
		
    @Column(name = "value_id")
	public Long  getValueId(){
		return valueId;
	} 
		
	public void setValueId(Long valueId){
			this.valueId = valueId;
		}
		
    @Column(name = "value_name")
	public String  getValueName(){
		return valueName;
	} 
		
	public void setValueName(String valueName){
			this.valueName = valueName;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
	


} 
