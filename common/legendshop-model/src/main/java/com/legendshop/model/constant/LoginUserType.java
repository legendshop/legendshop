package com.legendshop.model.constant;


/**
 * 登录的用户类型,请勿更改原有类型, 否则会导致权限失效的问题
 */
public interface LoginUserType {
	
	/**  
	 * 密码登录
	 */
	String USER_TYPE = "U";
	
	/** 商家登录 **/
	String SELLER_TYPE = "S";
	
	/** 商家员工登录 **/
	String EMPLOYEE_TYPE = "E";
	
	/** 门店登录**/
	String STORE_TYPE = "M";
	
	/**
	 * 后台管理员
	 */
	String ADMIN_TYPE = "A";

}
