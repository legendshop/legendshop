/*
 * LegendShop 多用户商城系统
 * 
 *  版权所有, 并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.model.ModelUtil;
import com.legendshop.model.constant.SupportTransportFreeEnum;
import com.legendshop.model.constant.TransportTypeEnum;

/**
 * 商品对象.
 */
@Entity
@Table(name = "ls_prod")
public class Product extends AbstractProduct {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7571396124663475715L;
	
	/** 单品列表 **/
	private List<Sku> skuList;
	
	/** 将要更新的单品列表 **/
	private List<Sku> updateSkuList;
	
	/** 产品图片 **/
	private List<ImgFile> imgFileList;
	
	/** 发布商品时，存放页面返回的 image url **/
	private String imagesPaths; 
	
	/** 商品sku数据 **/
	private String skus;
	
	/** 用户自定义属性 **/
	private String userProperties;
	
	/** 删除用户自定义属性 **/
	private String deleteUserProperties;
	
	/** 商品属性值图片 **/
	private String valueImages;
	
	/** 用户自定义属性值名 **/
	private String valueAlias;
	
	/** 设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库 **/
	private Integer publishStatus;
	
	/** 建立时间 **/
	private String setUpTime;
	
	/** 商城分类  **/
	private String categoryName;
	
	/**商城名称*/
	private String siteName;
	
	/**商城标签*/
	private String tags;
	
	/** 品牌名称 **/
	private String brandName;
	
	/** 好评数量 **/
	private String goodCommentsPercent;

    /** 产品图片 **/
    private String proVideoUrl;

	/**
	 * 服务说明
	 */
	private String serviceShow;

	public String getServiceShow() {
		return serviceShow;
	}

	public void setServiceShow(String serviceShow) {
		this.serviceShow = serviceShow;
	}

	/**
	 * Instantiates a new product.
	 */
	public Product() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.model.entity.AbstractEntity#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" [prodId=").append(prodId).append(", categoryId=").append(categoryId)
				.append(", name=").append(name).append(", userName=").append(userName).append("] ");
		return sb.toString();
	}

	@Transient
	public List<Sku> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<Sku> skuList) {
		this.skuList = skuList;
	}
	
	@Transient
	public List<Sku> getUpdateSkuList() {
		return updateSkuList;
	}

	public void setUpdateSkuList(List<Sku> updateSkuList) {
		this.updateSkuList = updateSkuList;
	}

	@Transient
	public List<ImgFile> getImgFileList() {
		return imgFileList;
	}

	public void setImgFileList(List<ImgFile> imgFileList) {
		this.imgFileList = imgFileList;
	}

	@Transient
	public String getImagesPaths() {
		return imagesPaths;
	}

	public void setImagesPaths(String imagesPaths) {
		this.imagesPaths = imagesPaths;
	}

	/**
	 * 验证对象是否满足需求
	 */
	public void validate(){
		ModelUtil util = new ModelUtil();
		util.isNotNull(this.getCategoryId(),"categoryId");
		util.isNotNull(this.getPic(), "Pic");
		if(SupportTransportFreeEnum.BUYERS_SUPPORT.value().equals(this.getSupportTransportFree()) && TransportTypeEnum.TRANSPORT_TEMPLE.value().equals(this.getTransportType())){
			util.isNotNull(this.getTransportId(), "transportId");
		}
	}

	@Transient
	public String getValueAlias() {
		return valueAlias;
	}

	public void setValueAlias(String valueAlias) {
		this.valueAlias = valueAlias;
	}

	@Transient
	public String getSkus() {
		return skus;
	}

	public void setSkus(String skus) {
		this.skus = skus;
	}

	@Transient
	public String getValueImages() {
		return valueImages;
	}

	public void setValueImages(String valueImages) {
		this.valueImages = valueImages;
	}

	@Transient
	public String getUserProperties() {
		return userProperties;
	}

	public void setUserProperties(String userProperties) {
		this.userProperties = userProperties;
	}

	@Transient
	public Integer getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	@Transient
	public String getSetUpTime() {
		return setUpTime;
	}

	public void setSetUpTime(String setUpTime) {
		this.setUpTime = setUpTime;
	}

	@Transient
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
		
	/**
	 * 对比商品是否发生了变化
	 * @param prod
	 * @return
	 */
	public boolean isProdChange(Product prod){
		
		return theSame(this.getId(), prod.getId()) && 
				theSame(this.getCategoryId(), prod.getCategoryId()) &&
				theSame(this.getShopFirstCatId(), prod.getShopFirstCatId())	 &&	
				theSame(this.getShopSecondCatId(), prod.getShopSecondCatId())	 &&	
				theSame(this.getShopThirdCatId(), prod.getShopThirdCatId())	 &&	
				theSame(this.getShopId(), prod.getShopId())	 &&	
				theSame(this.getModelId(), prod.getModelId())	 &&	
				theSame(this.getPartyCode(), prod.getPartyCode())	 &&	
				theSame(this.getName(), prod.getName())	 &&	
				theSame(this.getPrice(), prod.getPrice())	 &&	
				theSame(this.getCash(), prod.getCash())	 &&	
				theSame(this.getProxyPrice(), prod.getProxyPrice())	 &&	
				theSame(this.getCarriage(), prod.getCarriage())	 &&	
				theSame(this.getBrief(), prod.getBrief())	 &&	
				theSame(this.getContent(), prod.getContent())	 &&	
				theSame(this.getPic(), prod.getPic())	 &&
				theSame(this.getStatus(), prod.getStatus())	 &&	
				theSame(this.getStartDate(), prod.getStartDate())	 &&	
				theSame(this.getEndDate(), prod.getEndDate())	 &&	
				theSame(this.getStocks(), prod.getStocks())	 &&	
				theSame(this.getCategoryId(), prod.getCategoryId())	 &&	
				theSame(this.getKeyWord(), prod.getKeyWord())	 &&	
				theSame(this.getUserParameter(), prod.getUserParameter())	 &&	
				theSame(this.getParameter(), prod.getParameter())	 &&
				theSame(this.getServiceShow(),prod.getServiceShow())
				;
	}
	
	/**
	 * 
	 * @param target
	 * @param source
	 * @return
	 */
	private boolean theSame(Object target, Object source){
		if(target == null && source == null){
			return true;
		}
		if(target != null){
			return target.equals(source);
		}else{
			return false;
		}
		
	}

	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Transient
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	@Transient
	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@Transient
	public String getDeleteUserProperties() {
		return deleteUserProperties;
	}

	public void setDeleteUserProperties(String deleteUserProperties) {
		this.deleteUserProperties = deleteUserProperties;
	}
	@Transient
	public String getGoodCommentsPercent() {
		return goodCommentsPercent;
	}

	public void setGoodCommentsPercent(String goodCommentsPercent) {
		this.goodCommentsPercent = goodCommentsPercent;
	}

    @Column(name="pro_video")
    public String getProVideoUrl() {
        return proVideoUrl;
    }

    public void setProVideoUrl(String proVideoUrl) {
        this.proVideoUrl = proVideoUrl;
    }
}