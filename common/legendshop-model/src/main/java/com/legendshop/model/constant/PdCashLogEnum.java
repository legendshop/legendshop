package com.legendshop.model.constant;

import com.legendshop.util.AppUtils;

/**
 * 余额账户记录类型
 */
public enum PdCashLogEnum {

	// { 'order_freeze':'下单支付被冻结', 'auctions_freeze':'冻结', 'order_finally':'下单','recharge':'充值', 'withdraw':'提现', 'commission':'佣金收入', 'promoter':'分销提现', 'refund':'退款', 'auctions_deduct':'扣除保证金' }
	/**
	 * 下单支付被冻结的预存[还在支付过程中，相当于还么有扣款成功]  
	 */
	ORDER_PAY_FREEZE("order_freeze", "下单支付被冻结"),
	
	/**
	 * 冻结
	 */
	AUCTION_PAY_FREEZE("auctions_freeze", "冻结"),
	
	/**
	 * 下单成功,成功扣款
	 */
	ORDER_PAY_FINALLY("order_finally", "下单"),

	/**
	 * 充值
	 */
	RECHARGE("recharge", "充值"),
	
	/**
	 * 提现
	 */
	WITHDRAW("withdraw", "提现"),
	
	/**
	 * 佣金收入
	 */
	COMMISSION("commission", "佣金收入"),
	
	/**
	 * 退款
	 */
	REFUND("refund", "退款"),

	/**
	 * 分销
	 */
	PROMOTER("promoter", "分销提现"),
	
	/**
	 * 扣除保证金
	 */
	AUCTION_DEDUCT("auctions_deduct", "扣除保证金"),

	/**
	 * 其它（不可能会返回"其它"的，如果返回，就是有新增的状态没有加到该枚举类中）
	 */
	OTHER("other", "其它")
;
	/** The value. */
	private final String value;

	private final String desc;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private PdCashLogEnum(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	public static String getDesc(String value) {
		if (AppUtils.isNotBlank(value)) {
			PdCashLogEnum[] licenseEnums = values();
			for (PdCashLogEnum licenseEnum : licenseEnums) {
				if (licenseEnum.value.equals(value)) {
					return licenseEnum.desc;
				}
			}
		}
		return OTHER.desc;
	}

}
