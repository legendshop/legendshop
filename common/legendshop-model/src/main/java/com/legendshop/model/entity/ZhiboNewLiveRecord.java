/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;


/**
 * 新版直播记录表.
 */
@Entity
@Table(name = "ls_zhibo_new_live_record")
public class ZhiboNewLiveRecord implements GenericEntity<Long> {

	private static final long serialVersionUID = 5381396238727375596L;

	/** 主键ID */
	private Long id;

	/** 创建日期 */
	private Date createTime;

	/** 更新时间 */
	private Long modifyTime;

	/** appid */
	private Long appid;

	/** 标题 */
	private String title;

	/** 封面URL */
	private String cover;

	/** 主播UID */
	private String hostUid;

	/** av房间ID */
	private Long avRoomId;

	/** 聊天室ID */
	private String chatRoomId;

	/**  */
	private String roomType;

	/**  */
	private Integer videoType;

	/**  */
	private Integer device;

	/** 点赞人数 */
	private Long admireCount;

	/**  */
	private String playUrl1;

	/**  */
	private String playUrl2;

	/**  */
	private String playUrl3;

	/** 经度 */
	private Double longitude;

	/** 纬度 */
	private Double latitude;

	/** 地址 */
	private String address;

	private String memsize;

	private String pic;
	
	@Transient
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Transient
	public String getMemsize() {
		return memsize;
	}

	public void setMemsize(String memsize) {
		this.memsize = memsize;
	}

	public ZhiboNewLiveRecord() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ZHIBO_NEW_LIVE_RECORD_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "modify_time")
	public Long getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Long modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Column(name = "appid")
	public Long getAppid() {
		return appid;
	}

	public void setAppid(Long appid) {
		this.appid = appid;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "cover")
	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	@Column(name = "host_uid")
	public String getHostUid() {
		return hostUid;
	}

	public void setHostUid(String hostUid) {
		this.hostUid = hostUid;
	}

	@Column(name = "av_room_id")
	public Long getAvRoomId() {
		return avRoomId;
	}

	public void setAvRoomId(Long avRoomId) {
		this.avRoomId = avRoomId;
	}

	@Column(name = "chat_room_id")
	public String getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(String chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	@Column(name = "room_type")
	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	@Column(name = "video_type")
	public Integer getVideoType() {
		return videoType;
	}

	public void setVideoType(Integer videoType) {
		this.videoType = videoType;
	}

	@Column(name = "device")
	public Integer getDevice() {
		return device;
	}

	public void setDevice(Integer device) {
		this.device = device;
	}

	@Column(name = "admire_count")
	public Long getAdmireCount() {
		return admireCount;
	}

	public void setAdmireCount(Long admireCount) {
		this.admireCount = admireCount;
	}

	@Column(name = "play_url1")
	public String getPlayUrl1() {
		return playUrl1;
	}

	public void setPlayUrl1(String playUrl1) {
		this.playUrl1 = playUrl1;
	}

	@Column(name = "play_url2")
	public String getPlayUrl2() {
		return playUrl2;
	}

	public void setPlayUrl2(String playUrl2) {
		this.playUrl2 = playUrl2;
	}

	@Column(name = "play_url3")
	public String getPlayUrl3() {
		return playUrl3;
	}

	public void setPlayUrl3(String playUrl3) {
		this.playUrl3 = playUrl3;
	}

	@Column(name = "longitude")
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude")
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
