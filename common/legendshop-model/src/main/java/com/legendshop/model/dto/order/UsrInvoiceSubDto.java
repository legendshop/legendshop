package com.legendshop.model.dto.order;

import java.io.Serializable;

public class UsrInvoiceSubDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 发票类型：普通发票" : "增值税" */
	private String type;

	/** 发票抬头 */
	private String title;

	/** 单位 ：个人 单位 */
	private String company;

	/** 发票内容 是否明细 */
	private String content;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public UsrInvoiceSubDto(String type, String title, String company,
			String content) {
		super();
		this.type = type;
		this.title = title;
		this.company = company;
		this.content = content;
	}

	
}
