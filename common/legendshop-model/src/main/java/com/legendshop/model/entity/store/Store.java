package com.legendshop.model.entity.store;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *门店表
 */
@Entity
@Table(name = "ls_store")
public class Store implements GenericEntity<Long> {

	private static final long serialVersionUID = 8458611055731560333L;

	/** 门店id */
	private Long id; 
		
	/** 门店登录名 */
	private String userName; 
		
	/** 门店登录密码 */
	private String passwd; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 是否启用 */
	private Boolean isDisable; 
		
	/** 门店机构码 */
	private String code; 
		
	/** 门店名称 */
	private String name; 
		
	/** 省份id */
	private Long provinceid; 
		
	/** 城市id */
	private Long cityid; 
		
	/** 地级市id */
	private Long areaid; 
		
	/** 详细地址 */
	private String address; 
		
	/** 固定手机 */
	private String contactMobile; 
		
	/** 固定电话 */
	private String contactTel; 
		
	/** 门店交通路线 */
	private String transitRoute; 
		
	/** 添加时间 */
	private Date addtime; 
	
	/** 省份 */
	private String province;
	
	/** 城市 */
	private String city;
	
	/** 地级市 */
	private String area;
	
	/**地图选择的地址*/
	private String shopAddr;
	
	/** 经度 */
	private Double lng;
	
	/** 纬度 */
	private Double lat;
	
	/** 营业时间  */
	private String businessHours;
	
	private String shopUserId;
	
	private String shopUserName;
	
	private String shopName;
	
		
	
	public Store() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STORE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "passwd")
	public String  getPasswd(){
		return passwd;
	} 
		
	public void setPasswd(String passwd){
			this.passwd = passwd;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "is_disable")
	public Boolean  getIsDisable(){
		return isDisable;
	} 
		
	public void setIsDisable(Boolean isDisable){
			this.isDisable = isDisable;
		}
		
    @Column(name = "code")
	public String  getCode(){
		return code;
	} 
		
	public void setCode(String code){
			this.code = code;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "provinceid")
	public Long  getProvinceid(){
		return provinceid;
	} 
		
	public void setProvinceid(Long provinceid){
			this.provinceid = provinceid;
		}
		
    @Column(name = "cityid")
	public Long  getCityid(){
		return cityid;
	} 
		
	public void setCityid(Long cityid){
			this.cityid = cityid;
		}
		
    @Column(name = "areaid")
	public Long  getAreaid(){
		return areaid;
	} 
		
	public void setAreaid(Long areaid){
			this.areaid = areaid;
		}
		
    @Column(name = "address")
	public String  getAddress(){
		return address;
	} 
		
	public void setAddress(String address){
			this.address = address;
		}
		
    @Column(name = "contact_mobile")
	public String  getContactMobile(){
		return contactMobile;
	} 
		
	public void setContactMobile(String contactMobile){
			this.contactMobile = contactMobile;
		}
		
    @Column(name = "contact_tel")
	public String  getContactTel(){
		return contactTel;
	} 
		
	public void setContactTel(String contactTel){
			this.contactTel = contactTel;
		}
		
    @Column(name = "transit_route")
	public String  getTransitRoute(){
		return transitRoute;
	} 
		
	public void setTransitRoute(String transitRoute){
			this.transitRoute = transitRoute;
		}
		
    @Column(name = "addtime")
	public Date  getAddtime(){
		return addtime;
	} 
		
	public void setAddtime(Date addtime){
			this.addtime = addtime;
		}


	@Column(name = "province")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name = "area")
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@Column(name = "shop_addr")
	public String getShopAddr() {
		return shopAddr;
	}

	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}

	@Column(name = "lng")
	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	@Column(name = "lat")
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	@Column(name = "business_hours")
	public String getBusinessHours() {
		return businessHours;
	}

	public void setBusinessHours(String businessHours) {
		this.businessHours = businessHours;
	}

	@Transient
	public String getShopUserId() {
		return shopUserId;
	}

	public void setShopUserId(String shopUserId) {
		this.shopUserId = shopUserId;
	}

	@Transient
	public String getShopUserName() {
		return shopUserName;
	}

	public void setShopUserName(String shopUserName) {
		this.shopUserName = shopUserName;
	}

	@Transient
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	
	

} 
