/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;

/**
 * 秒杀成功商品信息.
 *
 */
public class SuccessProdDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The sucess id. */
	private Long sucessId; 
		
	/** 用户id. */
	private String userId; 
		
	/** 秒杀id. */
	private Long seckillId; 
		
	/** 商品id. */
	private Long prodId; 
		
	/** skuId. */
	private Long skuId; 
		
	/** 秒杀数量 */
	private Integer seckillNum; 
		
	/** 秒杀价格 */
	private Double seckillPrice; 
		
	/** 用户的状态默认0 0成功状态 1:已下单. */
	private Integer status; 
	
	/** 名称 */
	private String name;
	
	/** 图片 */
	private String pic;
	
	/** sku价格 */
	private Double price;
	
	/** 秒杀商品的库存 */
	private Long seckillStock;
	
	/** 秒杀活动总库存 */
	private Long activityStock;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the seckill id.
	 *
	 * @return the seckill id
	 */
	public Long getSeckillId() {
		return seckillId;
	}

	/**
	 * Sets the seckill id.
	 *
	 * @param seckillId the seckill id
	 */
	public void setSeckillId(Long seckillId) {
		this.seckillId = seckillId;
	}

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the sku id.
	 *
	 * @return the sku id
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * Sets the sku id.
	 *
	 * @param skuId the sku id
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * Gets the seckill num.
	 *
	 * @return the seckill num
	 */
	public Integer getSeckillNum() {
		return seckillNum;
	}

	/**
	 * Sets the seckill num.
	 *
	 * @param seckillNum the seckill num
	 */
	public void setSeckillNum(Integer seckillNum) {
		this.seckillNum = seckillNum;
	}

	/**
	 * Gets the seckill price.
	 *
	 * @return the seckill price
	 */
	public Double getSeckillPrice() {
		return seckillPrice;
	}

	/**
	 * Sets the seckill price.
	 *
	 * @param seckillPrice the seckill price
	 */
	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the sucess id.
	 *
	 * @return the sucess id
	 */
	public Long getSucessId() {
		return sucessId;
	}

	/**
	 * Sets the sucess id.
	 *
	 * @param sucessId the sucess id
	 */
	public void setSucessId(Long sucessId) {
		this.sucessId = sucessId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getSeckillStock() {
		return seckillStock;
	}

	public void setSeckillStock(Long seckillStock) {
		this.seckillStock = seckillStock;
	}

	public Long getActivityStock() {
		return activityStock;
	}

	public void setActivityStock(Long activityStock) {
		this.activityStock = activityStock;
	}
	
}
