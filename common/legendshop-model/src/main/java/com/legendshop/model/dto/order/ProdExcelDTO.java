package com.legendshop.model.dto.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品导入dto
 * 
 * @author Zhongweihan
 * 
 */
public class ProdExcelDTO implements Serializable {

	/** 商品名字 */
	private String prodName;

	/** 平台分类 */
    private String categroyId;

	/** 店铺分类 */
    private String shopCategroy;

    /** 参数项1 */
    private String firstParameter;

    /** 参数值1 */
    private String firstParameterValue;

    /** 参数项2 */
    private String secondParameter;

    /** 参数值2 */
    private String secondParameterValue;

    /** 参数项3 */
    private String thirdParameter;

    /** 参数值3 */
    private String thirdParameterValue;

    /** 参数项4 */
    private String fourthParameter;

    /** 参数值4 */
    private String fourthParameterValue;


	/** spu售价 */
	private String spuPrice;

	/** 支付方式 */
//	private String payType;

    /** 图片链接 */
//    private String picture;

    /** 详细介绍(电脑版) */
    private String content;

    /** 副标题 */
    private String skuName;

    /** 规格项1 */
    private String firstProperties;

    /** 规格值1 */
    private String firstPropertiesValue;

    /** 规格项2 */
    private String secondProperties;

    /** 规格值2 */
    private String secondPropertiesValue;

    /** 规格项3 */
    private String thirdProperties;

    /** 规格值3 */
    private String thirdPropertiesValue;

    /** 规格项4 */
    private String fourthProperties;

    /** 规格值4 */
    private String fourthPropertiesValue;

    /** SKU售价 */
    private String skuPrice;

    /** 可售库存 */
    private String stocks;

    /** 物流重量 */
    private String weight;

	/** 商家编码 */
	private String partyCode;

	/** 商品条形码 */
	private String modelId;

	/** 是否上架 */
	private String status;

	/** 备注 */
	private String remark;

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getCategroyId() {
        return categroyId;
    }

    public void setCategroyId(String categroyId) {
        this.categroyId = categroyId;
    }

    public String getShopCategroy() {
        return shopCategroy;
    }

    public void setShopCategroy(String shopCategroy) {
        this.shopCategroy = shopCategroy;
    }

    public String getFirstParameter() {
        return firstParameter;
    }

    public void setFirstParameter(String firstParameter) {
        this.firstParameter = firstParameter;
    }

    public String getFirstParameterValue() {
        return firstParameterValue;
    }

    public void setFirstParameterValue(String firstParameterValue) {
        this.firstParameterValue = firstParameterValue;
    }

    public String getSecondParameter() {
        return secondParameter;
    }

    public void setSecondParameter(String secondParameter) {
        this.secondParameter = secondParameter;
    }

    public String getSecondParameterValue() {
        return secondParameterValue;
    }

    public void setSecondParameterValue(String secondParameterValue) {
        this.secondParameterValue = secondParameterValue;
    }

    public String getThirdParameter() {
        return thirdParameter;
    }

    public void setThirdParameter(String thirdParameter) {
        this.thirdParameter = thirdParameter;
    }

    public String getThirdParameterValue() {
        return thirdParameterValue;
    }

    public void setThirdParameterValue(String thirdParameterValue) {
        this.thirdParameterValue = thirdParameterValue;
    }

    public String getFourthParameter() {
        return fourthParameter;
    }

    public void setFourthParameter(String fourthParameter) {
        this.fourthParameter = fourthParameter;
    }

    public String getFourthParameterValue() {
        return fourthParameterValue;
    }

    public void setFourthParameterValue(String fourthParameterValue) {
        this.fourthParameterValue = fourthParameterValue;
    }

    public String getSpuPrice() {
        return spuPrice;
    }

    public void setSpuPrice(String spuPrice) {
        this.spuPrice = spuPrice;
    }

/*    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }*/

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getFirstProperties() {
        return firstProperties;
    }

    public void setFirstProperties(String firstProperties) {
        this.firstProperties = firstProperties;
    }

    public String getFirstPropertiesValue() {
        return firstPropertiesValue;
    }

    public void setFirstPropertiesValue(String firstPropertiesValue) {
        this.firstPropertiesValue = firstPropertiesValue;
    }

    public String getSecondProperties() {
        return secondProperties;
    }

    public void setSecondProperties(String secondProperties) {
        this.secondProperties = secondProperties;
    }

    public String getSecondPropertiesValue() {
        return secondPropertiesValue;
    }

    public void setSecondPropertiesValue(String secondPropertiesValue) {
        this.secondPropertiesValue = secondPropertiesValue;
    }

    public String getThirdProperties() {
        return thirdProperties;
    }

    public void setThirdProperties(String thirdProperties) {
        this.thirdProperties = thirdProperties;
    }

    public String getThirdPropertiesValue() {
        return thirdPropertiesValue;
    }

    public void setThirdPropertiesValue(String thirdPropertiesValue) {
        this.thirdPropertiesValue = thirdPropertiesValue;
    }

    public String getFourthProperties() {
        return fourthProperties;
    }

    public void setFourthProperties(String fourthProperties) {
        this.fourthProperties = fourthProperties;
    }

    public String getFourthPropertiesValue() {
        return fourthPropertiesValue;
    }

    public void setFourthPropertiesValue(String fourthPropertiesValue) {
        this.fourthPropertiesValue = fourthPropertiesValue;
    }

    public String getSkuPrice() {
        return skuPrice;
    }

    public void setSkuPrice(String skuPrice) {
        this.skuPrice = skuPrice;
    }

    public String getStocks() {
        return stocks;
    }

    public void setStocks(String stocks) {
        this.stocks = stocks;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
	 * 检查数据是否合法
	 * 
	 * @return
	 */
	public List<String> checkData() {
		List<String> result = new ArrayList<String>();
//		if (AppUtils.isBlank(this.getProdName()) || AppUtils.isBlank(this.getBrief()) || AppUtils.isBlank(this.getPrice())) {
//			 	result.add("数值为空");
//		}
//
//		if (this.getProdName().length() > 300 || this.getBrief().length() > 500) {
//			result.add("长度超长");
//		}
//
//		if(this.getPrice() <=0 || this.getCash() <= 0){
//			result.add("价格非法");
//		}
//
		return result;
	}

}
