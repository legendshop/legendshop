package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *标签里的商品
 */
@Entity
@Table(name = "ls_tag_item_prod")
public class TagItemProd implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/**  */
	private Long tagItemProdId; 
		
	/** 商品Id */
	private Long prodId; 
		
	/** 标签itemId */
	private Long tagItemId; 
		
	/** 记录时间 */
	private Date recDate; 
	
	private String tagName;
		
	
	public TagItemProd() {
    }
		
	@Id
	@Column(name = "tag_item_prod_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TAG_ITEM_PROD_SEQ")
	public Long  getTagItemProdId(){
		return tagItemProdId;
	} 
		
	public void setTagItemProdId(Long tagItemProdId){
			this.tagItemProdId = tagItemProdId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "tag_item_id")
	public Long  getTagItemId(){
		return tagItemId;
	} 
		
	public void setTagItemId(Long tagItemId){
			this.tagItemId = tagItemId;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
	
	@Transient
	public Long getId() {
		return tagItemProdId;
	}
	
	public void setId(Long id) {
		tagItemProdId = id;
	}

	@Transient
	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	

} 
