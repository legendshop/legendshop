/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import java.util.List;

import com.legendshop.model.KeyValueEntity;


/**
 * 计价方式.
 */
public class TransTypeDefinition  {

	/** The trans type list. */
	private List<KeyValueEntity> transTypeList;

	/**
	 * Gets the trans type list.
	 *
	 * @return the trans type list
	 */
	public List<KeyValueEntity> getTransTypeList() {
		return transTypeList;
	}

	/**
	 * Sets the trans type list.
	 *
	 * @param transTypeList the new trans type list
	 */
	public void setTransTypeList(List<KeyValueEntity> transTypeList) {
		this.transTypeList = transTypeList;
	}
}
