/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 系统参数类型
 */
public enum SysParamGroupEnum implements StringEnum {

	// 系统参数配置
	SYS("SYS"),

	// 商城参数配置
	SHOP("SHOP"),

	// 日志配置
	LOG("LOG"),

	// 邮件配置
	MAIL("MAIL"),
	
	//登录配置
	LOGIN("LOGIN"),
	
	/** 短信配置 */
	SMS("SMS")
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new sub status enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SysParamGroupEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		SysParamGroupEnum[] licenseEnums = values();
		for (SysParamGroupEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}
}
