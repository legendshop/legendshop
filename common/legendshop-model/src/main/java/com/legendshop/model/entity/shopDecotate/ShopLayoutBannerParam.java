/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity.shopDecotate;

/**
 * 布局Banner参数
 */
public class ShopLayoutBannerParam extends ShopLayoutParam {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7732767777880923764L;

	/** The img files. */
	private String[] img_files;

	/** The urls. */
	private String[] urls;

	/**
	 * Gets the img files.
	 *
	 * @return the img files
	 */
	public String[] getImg_files() {
		return img_files;
	}

	/**
	 * Sets the img files.
	 *
	 * @param img_files the new img files
	 */
	public void setImg_files(String[] img_files) {
		this.img_files = img_files;
	}

	/**
	 * Gets the urls.
	 *
	 * @return the urls
	 */
	public String[] getUrls() {
		return urls;
	}

	/**
	 * Sets the urls.
	 *
	 * @param urls the new urls
	 */
	public void setUrls(String[] urls) {
		this.urls = urls;
	}

}
