package com.legendshop.model.entity.shopDecotate;

/**
 * 
 * 装修热点参数
 * 
 */
public class ShopLayoutHotParam extends ShopLayoutParam {

	private static final long serialVersionUID = 7666081106018561491L;

	private String coorDatas;
	
	private String imageFile;

	public String getCoorDatas() {
		return coorDatas;
	}

	public void setCoorDatas(String coorDatas) {
		this.coorDatas = coorDatas;
	}

	public String getImageFile() {
		return imageFile;
	}

	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	

}
