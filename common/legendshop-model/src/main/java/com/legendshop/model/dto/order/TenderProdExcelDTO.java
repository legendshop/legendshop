package com.legendshop.model.dto.order;

import java.io.Serializable;

/**
 * 中标商品导入dto
 * 
 * @author Zhongweihan
 * 
 */
public class TenderProdExcelDTO implements Serializable {

	/** SPU编码 */
	private String spuId;

	/** 商品名字 */
	private String prodName;

	/** 店铺分类 */
	private String shopCategroy;

	/** 参数项1 */
	private String firstParameter;

	/** 参数值1 */
	private String firstParameterValue;

	/** 参数项2 */
	private String secondParameter;

	/** 参数值2 */
	private String secondParameterValue;

	/** 参数项3 */
	private String thirdParameter;

	/** 参数值3 */
	private String thirdParameterValue;

	/** 参数项4*/
	private String fourthParameter;

	/** 参数值4 */
	private String fourthParameterValue;

	/** 参数项5 */
	private String fifthParameter;

	/** 参数值5 */
	private String fifthParameterValue;

	/** 计量单位 */
//	private String unit;

	/** spu售价 */
	private String spuPrice;

	/** 支付方式 */
//	private String payType;

	/** 图片链接 */
//	private String picture;

	/** 详细介绍(电脑版) */
	private String content;

	/** SKU编码 -------------sku*/
	private String skuId;

	/** 副标题 */
	private String skuName;

	/** 项目号 */
	private String projectNum;

	/** 物料号 */
	private String materialCode;

	/** 中标有效日期 */
	private String effectiveDate;

	/** 规格项1 */
	private String firstProperties;

	/** 规格值1 */
	private String firstPropertiesValue;

	/** SKU售价 */
	private String skuPrice;

	/** 招标价格 */
	private String tenderPrice;

	/** 可售库存 */
	private String stocks;

	/** 物流重量 */
//	private String weight;

	/** 商家编码 */
	private String partyCode;

	/** 商品条形码 */
	private String modelId;

	/** 是否上架 */
	private String status;

	/** 备注 */
	private String remark;

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getShopCategroy() {
		return shopCategroy;
	}

	public void setShopCategroy(String shopCategroy) {
		this.shopCategroy = shopCategroy;
	}

	public String getFirstParameter() {
		return firstParameter;
	}

	public void setFirstParameter(String firstParameter) {
		this.firstParameter = firstParameter;
	}

	public String getFirstParameterValue() {
		return firstParameterValue;
	}

	public void setFirstParameterValue(String firstParameterValue) {
		this.firstParameterValue = firstParameterValue;
	}

	public String getSecondParameter() {
		return secondParameter;
	}

	public void setSecondParameter(String secondParameter) {
		this.secondParameter = secondParameter;
	}

	public String getSecondParameterValue() {
		return secondParameterValue;
	}

	public void setSecondParameterValue(String secondParameterValue) {
		this.secondParameterValue = secondParameterValue;
	}

	public String getThirdParameter() {
		return thirdParameter;
	}

	public void setThirdParameter(String thirdParameter) {
		this.thirdParameter = thirdParameter;
	}

	public String getThirdParameterValue() {
		return thirdParameterValue;
	}

	public void setThirdParameterValue(String thirdParameterValue) {
		this.thirdParameterValue = thirdParameterValue;
	}

	public String getFourthParameter() {
		return fourthParameter;
	}

	public void setFourthParameter(String fourthParameter) {
		this.fourthParameter = fourthParameter;
	}

	public String getFourthParameterValue() {
		return fourthParameterValue;
	}

	public void setFourthParameterValue(String fourthParameterValue) {
		this.fourthParameterValue = fourthParameterValue;
	}

	public String getFifthParameter() {
		return fifthParameter;
	}

	public void setFifthParameter(String fifthParameter) {
		this.fifthParameter = fifthParameter;
	}

	public String getFifthParameterValue() {
		return fifthParameterValue;
	}

	public void setFifthParameterValue(String fifthParameterValue) {
		this.fifthParameterValue = fifthParameterValue;
	}

	/*public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}*/

	public String getSpuPrice() {
		return spuPrice;
	}

	public void setSpuPrice(String spuPrice) {
		this.spuPrice = spuPrice;
	}

	/*public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}*/

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getProjectNum() {
		return projectNum;
	}

	public void setProjectNum(String projectNum) {
		this.projectNum = projectNum;
	}

	public String getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getFirstProperties() {
		return firstProperties;
	}

	public void setFirstProperties(String firstProperties) {
		this.firstProperties = firstProperties;
	}

	public String getFirstPropertiesValue() {
		return firstPropertiesValue;
	}

	public void setFirstPropertiesValue(String firstPropertiesValue) {
		this.firstPropertiesValue = firstPropertiesValue;
	}

	public String getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(String skuPrice) {
		this.skuPrice = skuPrice;
	}

	public String getTenderPrice() {
		return tenderPrice;
	}

	public void setTenderPrice(String tenderPrice) {
		this.tenderPrice = tenderPrice;
	}

	public String getStocks() {
		return stocks;
	}

	public void setStocks(String stocks) {
		this.stocks = stocks;
	}

	/*public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}*/

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
