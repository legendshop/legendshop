package com.legendshop.model.entity;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *团购商品
 */
@Entity
@Table(name = "ls_group")
public class Group implements GenericEntity<Long> {

	private static final long serialVersionUID = 5454972528384380307L;

	/** id */
	private Long id; 
		
	/** 团购名称 */
	private String groupName; 
		
	/** 开始时间 */
	private Date startTime; 
		
	/** 结束时间 */
	private Date endTime; 
		
	/** 商品ID */
	private Long productId; 
		
	/** 店铺ID */
	private Long shopId; 
		
	/** 店铺名称 */
	private String shopName; 
		
	/** 团购价格 */
	private java.math.BigDecimal groupPrice; 
	
	/** 原商品的售价 */
	private java.math.BigDecimal prodPrice;
		
	/** 已购买人数 */
	private Long buyerCount; 
		
	/** 每人限购数量 */
	private Integer buyQuantity; 
		
	/** 活动的状态有：未提审、已提审、上线（审核通过）、审核不通过。 */
	private Integer status; 
	
	/**分类名称 */
	private String categoryName;
		
	/** 团购1级分类ID */
	private Long firstCatId; 
		
	/** 团购2级分类ID */
	private Long secondCatId; 
		
	/** 团购3级分类ID */
	private Long thirdCatId; 
		
	/** 团购图片 */
	private String groupImage; 
		
	/** 团购详情 */
	private String groupDesc;
	
	/** 审核意见  */
	private String auditOpinion;
	
	/** 审核时间  */
	private Date auditTime;
	
	/** 团购活动图片上传文件  */
	private MultipartFile groupPicFile;
	
	/** 查询过期标识  */
	private Date overdue;
	
	/** 过滤过期 */
	private Date overdate;
	
	/** 商品状态 参考 ProductStatusEnum */
	private Integer prodStatus;
	
	/**成团人数**/
	private Integer peopleCount;
	
	/** 0:未删除 1: 商家逻辑删除 */
	private Integer deleteStatus;
	
	/** tab类型 */
	private String searchType;
	
	/** 用于区分商家/品台搜索 为空是商家搜索 */
	private String flag;

	/** 分组商品*/
	private List<GroupSku> groupProductList;
	
	public Group() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GROUP_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "group_name")
	public String  getGroupName(){
		return groupName;
	} 
		
	public void setGroupName(String groupName){
			this.groupName = groupName;
		}
		
    @Column(name = "start_time")
	public Date  getStartTime(){
		return startTime;
	} 
		
	public void setStartTime(Date startTime){
			this.startTime = startTime;
		}
		
    @Column(name = "end_time")
	public Date  getEndTime(){
		return endTime;
	} 
		
	public void setEndTime(Date endTime){
			this.endTime = endTime;
		}
		
    @Column(name = "product_id")
	public Long  getProductId(){
		return productId;
	} 
		
	public void setProductId(Long productId){
			this.productId = productId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "shop_name")
	public String  getShopName(){
		return shopName;
	} 
		
	public void setShopName(String shopName){
			this.shopName = shopName;
		}
		
    @Column(name = "group_price")
	public java.math.BigDecimal  getGroupPrice(){
		return groupPrice;
	} 
		
	public void setGroupPrice(java.math.BigDecimal groupPrice){
			this.groupPrice = groupPrice;
		}
	
	@Transient
    public java.math.BigDecimal getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(java.math.BigDecimal prodPrice) {
		this.prodPrice = prodPrice;
	}

	@Column(name = "buyer_count")
	public Long  getBuyerCount(){
		return buyerCount;
	} 
		
	public void setBuyerCount(Long buyerCount){
			this.buyerCount = buyerCount;
		}
		
    @Column(name = "buy_quantity")
	public Integer  getBuyQuantity(){
		return buyQuantity;
	} 
		
	public void setBuyQuantity(Integer buyQuantity){
			this.buyQuantity = buyQuantity;
		}
    @Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name = "category_name")
    public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Column(name = "first_cat_id")
	public Long  getFirstCatId(){
		return firstCatId;
	} 
		
	public void setFirstCatId(Long firstCatId){
			this.firstCatId = firstCatId;
		}
		
    @Column(name = "second_cat_id")
	public Long  getSecondCatId(){
		return secondCatId;
	} 
		
	public void setSecondCatId(Long secondCatId){
			this.secondCatId = secondCatId;
		}
		
    @Column(name = "third_cat_id")
	public Long  getThirdCatId(){
		return thirdCatId;
	} 
		
	public void setThirdCatId(Long thirdCatId){
			this.thirdCatId = thirdCatId;
		}
		
    @Column(name = "group_image")
	public String  getGroupImage(){
		return groupImage;
	} 
		
	public void setGroupImage(String groupImage){
			this.groupImage = groupImage;
		}
		
	@Column(name = "group_desc")
	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}
	
	@Column(name = "audit_opinion")
	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	@Column(name = "audit_time")
	public Date getAuditTime() {
		return auditTime;
	}
	
	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	@Transient
	public MultipartFile getGroupPicFile() {
		return groupPicFile;
	}

	public void setGroupPicFile(MultipartFile groupPicFile) {
		this.groupPicFile = groupPicFile;
	}
	@Transient
	public Date getOverdue() {
		return overdue;
	}

	public void setOverdue(Date overdue) {
		this.overdue = overdue;
	}
	@Transient
	public Date getOverdate() {
		return overdate;
	}

	public void setOverdate(Date overdate) {
		this.overdate = overdate;
	}
	
	@Column(name = "people_count")
	public Integer getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Integer peopleCount) {
		this.peopleCount = peopleCount;
	}

	@Transient
	public Integer getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Integer prodStatus) {
		this.prodStatus = prodStatus;
	}

	@Transient
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	@Column(name = "delete_status")
	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	@Transient
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Transient
	public List<GroupSku> getGroupProductList() {
		return groupProductList;
	}

	public void setGroupProductList(List<GroupSku> groupProductList) {
		this.groupProductList = groupProductList;
	}
}
