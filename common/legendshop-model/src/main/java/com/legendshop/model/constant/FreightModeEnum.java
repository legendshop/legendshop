package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum FreightModeEnum implements StringEnum {

	TRANSPORT_MAIL("mail"),

	TRANSPORT_EXPRESS("express"),

	TRANSPORT_EMS("ems");

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private FreightModeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
}
