/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;
/**
 * 类型
 * 
 * ----------------------------------------------------------------------------.
 */
public enum SMSTypeEnum implements IntegerEnum {
	
	/** 验证 */
	VAL(2),
	
	/** 广告 */
	ADV(3),
	
	/** 订单 */	
	ORDER(4),
	
	/** 物流 */	
	LOGISTICS(5),
	
	TIX(6), //提现
	
	COUPON(7),//礼券
	
	INFORM(8), //通知
	
	SYSTEM(9),  //系统
	
	DELIVER(10), //发货通知
	
	ARRIVAL(11) //到货通知
	
	;

	/** The value. */
	private final Integer value;

	private SMSTypeEnum(Integer value) {
		this.value = value;
	}

	public Integer value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		SMSTypeEnum[] licenseEnums = values();
		for (SMSTypeEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static Integer getValue(String name) {
		SMSTypeEnum[] licenseEnums = values();
		for (SMSTypeEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
