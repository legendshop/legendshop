package com.legendshop.model.dto.weibo;

/**
 * 微博的token信息
 * @author 开发很忙
 */
public class WeiboUserToken extends WeiboReponseMsg{
	
	/** 授权令牌，Access_Token。 */
	private String access_token;
	
	/** 该access token的有效期，单位为秒。 */
	private int expires_in;
	
	/** 刷新token */
	private String refresh_token;
	
	/** 用户ID */
	private String uid;
	
	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}
	
	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
