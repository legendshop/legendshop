package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *预存款充值表
 */
@Entity
@Table(name = "ls_pd_recharge")
@ApiModel("充值记录")
public class PdRecharge implements GenericEntity<Long> {

	private static final long serialVersionUID = -3681029744888699535L;

	/** 主键id */
	@ApiModelProperty(value = "主键id")
	private Long id; 
		
	/** 唯一的流水号 */
	@ApiModelProperty(value = "唯一的流水号")
	private String sn; 
		
	/** 会员编号 */
	@ApiModelProperty(value = "会员编号")
	private String userId; 
		
	/** 会员名称 */
	@ApiModelProperty(value = "会员名称")
	private String userName; 
		
	/** 充值金额 */
	@ApiModelProperty(value = "充值金额")
	private Double amount; 
		
	/** 支付方式 */
	@ApiModelProperty(value = "支付方式")
	private String paymentCode; 
		
	/** 支付名称 */
	@ApiModelProperty(value = "支付名称 ")
	private String paymentName; 
		
	/** 第三方支付接口交易号 */
	@ApiModelProperty(value = "第三方支付接口交易号")
	private String tradeSn; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间")
	private Date addTime; 
		
	/** 支付状态 0未支付1支付 */
	@ApiModelProperty(value = "支付状态 0未支付1支付")
	private Integer paymentState; 
		
	/** 支付时间 */
	@ApiModelProperty(value = "支付时间")
	private Date paymentTime; 
		
	/** 管理员名 */
	@ApiModelProperty(value = "管理员名")
	private String adminUserName; 
		
	/** 后台管理员操作备注 */
	@ApiModelProperty(value = "后台管理员操作备注")
	private String adminUserNote; 
	
	/** 昵称 */
	@ApiModelProperty(value = "昵称")
	private String nickName;
		
	
	public PdRecharge() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PD_RECHARGE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "sn")
	public String  getSn(){
		return sn;
	} 
		
	public void setSn(String sn){
			this.sn = sn;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "amount")
	public Double  getAmount(){
		return amount;
	} 
		
	public void setAmount(Double amount){
			this.amount = amount;
		}
		
    @Column(name = "payment_code")
	public String  getPaymentCode(){
		return paymentCode;
	} 
		
	public void setPaymentCode(String paymentCode){
			this.paymentCode = paymentCode;
		}
		
    @Column(name = "payment_name")
	public String  getPaymentName(){
		return paymentName;
	} 
		
	public void setPaymentName(String paymentName){
			this.paymentName = paymentName;
		}
		
    @Column(name = "trade_sn")
	public String  getTradeSn(){
		return tradeSn;
	} 
		
	public void setTradeSn(String tradeSn){
			this.tradeSn = tradeSn;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    @Column(name = "payment_state")
	public Integer  getPaymentState(){
		return paymentState;
	} 
		
	public void setPaymentState(Integer paymentState){
			this.paymentState = paymentState;
		}
		
    @Column(name = "payment_time")
	public Date  getPaymentTime(){
		return paymentTime;
	} 
		
	public void setPaymentTime(Date paymentTime){
			this.paymentTime = paymentTime;
		}
		
    @Column(name = "admin_user_name")
	public String  getAdminUserName(){
		return adminUserName;
	} 
		
	public void setAdminUserName(String adminUserName){
			this.adminUserName = adminUserName;
		}
		
    @Column(name = "admin_user_note")
	public String  getAdminUserNote(){
		return adminUserNote;
	} 
		
	public void setAdminUserNote(String adminUserNote){
			this.adminUserNote = adminUserNote;
		}

	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	
} 
