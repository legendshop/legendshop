package com.legendshop.model.dto;

public class ShopUrlPermissionDto {

	private String name;//权限名

	private String type;//权限类型 （查看/编辑）

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
