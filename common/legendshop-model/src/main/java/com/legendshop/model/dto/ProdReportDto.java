package com.legendshop.model.dto;

import java.io.Serializable;

public class ProdReportDto implements Serializable{

	private static final long serialVersionUID = -5526073518694234673L;

	private Long prodId;
	
	private String name;
	
	private Integer buys;
	
	private String prodPic;
	
	private String subDate;
	
	private Double totalCash;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBuys() {
		return buys;
	}

	public void setBuys(Integer buys) {
		this.buys = buys;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getSubDate() {
		return subDate;
	}

	public void setSubDate(String subDate) {
		this.subDate = subDate;
	}

	public Double getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}
	
	
}
