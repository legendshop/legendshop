package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 *  促销活动tab类型枚举
 * @author linzh
 * 
 */
public enum PromotionSearchTypeEnum implements StringEnum {

	/** 未发布 */
	NO_PUBLISH("NO_PUBLISH"),

	/** 未开始  */
	NOT_STARTED("NOT_STARTED"),

	/** 进行中  */
	ONLINE("ONLINE"),

	/** 已暂停  */
	PAUSE("PAUSE"),

	/** 已结束  */
	FINISHED("FINISHED"),

	/** 已失效   */
	EXPIRED("EXPIRED"),

	;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 *
	 * @param value
	 *            the value
	 */
	private PromotionSearchTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
	
	/**
	 * 匹配类型
	 * @param opCodeStr
	 * @return
	 */
    public static PromotionSearchTypeEnum matchType(String type) {
        for (PromotionSearchTypeEnum typeEnum : PromotionSearchTypeEnum.values()) {
            if (typeEnum.name().equalsIgnoreCase(type)) {
                return typeEnum;
            }
        }
        return null;
    }
	

}
