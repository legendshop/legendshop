/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 退款及退货的状态
 */
public enum RefundSouceEnum implements IntegerEnum {
	
	/**用户*/
	USER(0),
	
	/**商家*/
	SHOP(1),
	
	/**平台*/
	PLATFORM(2),
	
	;

	private Integer status;

	RefundSouceEnum(Integer status) {
		this.status = status;
	}

	@Override
	public Integer value() {
		return status;
	}

}
