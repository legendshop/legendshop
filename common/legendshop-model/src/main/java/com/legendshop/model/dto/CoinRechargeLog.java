package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 金币充值信息记录
 * @author legend-15
 *
 */
public class CoinRechargeLog implements Serializable{
	private static final long serialVersionUID = 5650157811849202086L;
	
	/** 是否成功 */
	private String isSuccess;
	/** 充值信息 */
	private String message;
	/** 充值成功个数 */
	private Integer successNumber;
	/** 充值失败个数 */
	private Integer failNumber;
	/** 充值成功的总金额 */
	private Double sum;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getSuccessNumber() {
		return successNumber;
	}
	public void setSuccessNumber(Integer successNumber) {
		this.successNumber = successNumber;
	}
	public Integer getFailNumber() {
		return failNumber;
	}
	public void setFailNumber(Integer failNumber) {
		this.failNumber = failNumber;
	}
	public Double getSum() {
		return sum;
	}
	public void setSum(Double sum) {
		this.sum = sum;
	}
	public String getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}
	
}
