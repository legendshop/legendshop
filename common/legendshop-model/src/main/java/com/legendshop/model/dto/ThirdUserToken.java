package com.legendshop.model.dto;

import com.legendshop.model.dto.qq.QQUserToken;
import com.legendshop.model.dto.weibo.WeiboUserToken;
import com.legendshop.model.dto.weixin.WeixinUserToken;

/**
 * 第三方用户token
 * @author 开发很忙
 */
public class ThirdUserToken {
	
	/** 授权接口调用凭证 */
	private String accessToken;
	
	/** access_token接口调用凭证超时时间，单位（秒） */
	private int expiresIn;
	
	/** 用于刷新access_token */
	private String refreshToken;
	
	/** 用户在第三方应用的唯一标识, 微博对应的是uid, QQ 在get me接口时可获得*/
	private String openId;
	
	/** unionid, QQ 在get me接口时可获得 */
	private String unionid;
	
	/** 用户授权的作用域，使用逗号（,）分隔, 目前只有微信才有 */
	private String scope;
	
	public ThirdUserToken() {
		super();
	}
	
	/**
	 * 根据微信的token 构造 ThirdUserToken
	 * @param weixinUserToken
	 */
	public ThirdUserToken(WeixinUserToken weixinUserToken) {
		this.setAccessToken(weixinUserToken.getAccess_token());
		this.setExpiresIn(weixinUserToken.getExpires_in());
		this.setRefreshToken(weixinUserToken.getRefresh_token());
		this.setOpenId(weixinUserToken.getOpenid());
		this.setScope(weixinUserToken.getScope());
	}
	
	/**
	 * 根据QQ的token 构造 ThirdUserToken
	 * @param qqUserToken
	 */
	public ThirdUserToken(QQUserToken qqUserToken) {
		this.setAccessToken(qqUserToken.getAccess_token());
		this.setExpiresIn(qqUserToken.getExpires_in());
		this.setRefreshToken(qqUserToken.getRefresh_token());
	}
	
	/**
	 * 根据微博的token 构造 ThirdUserToken
	 * @param weiboUserToken
	 */
	public ThirdUserToken(WeiboUserToken weiboUserToken) {
		this.setAccessToken(weiboUserToken.getAccess_token());
		this.setExpiresIn(weiboUserToken.getExpires_in());
		this.setRefreshToken(weiboUserToken.getRefresh_token());
		this.setOpenId(weiboUserToken.getUid());
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
