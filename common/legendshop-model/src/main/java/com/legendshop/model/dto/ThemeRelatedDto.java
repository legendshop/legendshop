package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * @author Ccc
 * @date 2019/5/22 15:33
 */
public class ThemeRelatedDto implements Serializable {
    /** 相关专题id */
    private Long relatedId;

    /** 专题id */
    private Long themeId;

    /** 相关专题图片 */
    private String img;

    /** 专题标题 */
    private String pcTitle;

    /** 专题标题 */
    private String mobileTitle;

    private Long relatedThemeId;

    private Long mRelatedThemeId;

    public Long getRelatedThemeId() {
        return relatedThemeId;
    }

    public void setRelatedThemeId(Long relatedThemeId) {
        this.relatedThemeId = relatedThemeId;
    }

    public Long getmRelatedThemeId() {
        return mRelatedThemeId;
    }

    public void setmRelatedThemeId(Long mRelatedThemeId) {
        this.mRelatedThemeId = mRelatedThemeId;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



    public String getPcTitle() {
        return pcTitle;
    }

    public void setPcTitle(String pcTitle) {
        this.pcTitle = pcTitle;
    }

    public String getMobileTitle() {
        return mobileTitle;
    }

    public void setMobileTitle(String mobileTitle) {
        this.mobileTitle = mobileTitle;
    }
}
