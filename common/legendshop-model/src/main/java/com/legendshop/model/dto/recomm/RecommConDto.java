package com.legendshop.model.dto.recomm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分类导航 推荐内容
 *
 */
public class RecommConDto implements Serializable{

	private static final long serialVersionUID = -5394940932762390275L;
	
	List<RecommBrandDto> brandList = new ArrayList<RecommBrandDto>();
	
	List<RecommAdvDto> advList = new ArrayList<RecommAdvDto>();

	public List<RecommBrandDto> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<RecommBrandDto> brandList) {
		this.brandList = brandList;
	}

	public List<RecommAdvDto> getAdvList() {
		return advList;
	}

	public void setAdvList(List<RecommAdvDto> advList) {
		this.advList = advList;
	}

}
