package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_pd_cash_holding")
public class PdCashHolding implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/** 唯一标示流水号 */
	private String sn; 
		
	/** holding金额 */
	private Double amount; 
		
	/** 登录人ID */
	private String userId; 
		
	/** 添加时间 */
	private Date addTime; 
		
	/** holdind 类型[下单/提现] */
	private String type; 
		
	/** 0：add hold状态， 1： releaseed hold状态 */
	private Integer status; 
		
	/** release hold时间 */
	private Date releaseTime; 
		
	
	public PdCashHolding() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PD_CASH_HOLDING_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "sn")
	public String  getSn(){
		return sn;
	} 
		
	public void setSn(String sn){
			this.sn = sn;
		}
		
    @Column(name = "amount")
	public Double  getAmount(){
		return amount;
	} 
		
	public void setAmount(Double amount){
			this.amount = amount;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    @Column(name = "type")
	public String  getType(){
		return type;
	} 
		
	public void setType(String type){
			this.type = type;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "release_time")
	public Date  getReleaseTime(){
		return releaseTime;
	} 
		
	public void setReleaseTime(Date releaseTime){
			this.releaseTime = releaseTime;
		}
	


} 
