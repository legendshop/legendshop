package com.legendshop.model.dto.order;

import java.util.List;

public class SubResult {

	private List<MySubDto> mySubDtos;

	private Long count;

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public List<MySubDto> getMySubDtos() {
		return mySubDtos;
	}

	public void setMySubDtos(List<MySubDto> mySubDtos) {
		this.mySubDtos = mySubDtos;
	}
	
	

}
