package com.legendshop.model.entity.store;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 门店商品
 */
@Entity
@Table(name = "ls_store_prod")
public class StoreProd implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/**  */
	private Long prodId; 
		
	/**  */
	private Long skuId; 
		
	/**  */
	private Long shopId; 
		
	/**  */
	private Long storeId; 
		
	/**  */
	private Long stock; 
		
	/**  */
	private Date addtime; 
		
	
	public StoreProd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STORE_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    //@Column(name = "sku_id")
	@Transient
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "store_id")
	public Long  getStoreId(){
		return storeId;
	} 
		
	public void setStoreId(Long storeId){
			this.storeId = storeId;
		}
		
    @Column(name = "stock")
	public Long  getStock(){
		return stock;
	} 
		
	public void setStock(Long stock){
			this.stock = stock;
		}
		
    @Column(name = "addtime")
	public Date  getAddtime(){
		return addtime;
	} 
		
	public void setAddtime(Date addtime){
			this.addtime = addtime;
		}
	


} 
