package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 预存款变更日志表
 */
@Entity
@Table(name = "ls_pd_cash_log")
@ApiModel("余额账户记录")
public class PdCashLog implements GenericEntity<Long> {

	private static final long serialVersionUID = -4506795262368477827L;

	/** 自增编号 */
	@ApiModelProperty(value = "自增编号")
	private Long id; 
		
	/** 会员编号 */
	@ApiModelProperty(value = "会员编号 ")
	private String userId; 
		
	/** 会员名称 */
	@ApiModelProperty(value = "会员名称")
	private String userName; 
		
	/** 交易流水号 */
	@ApiModelProperty(value = "交易流水号")
	private String sn; 
		
	/** order_pay日记类型 */
	@ApiModelProperty(value = "order_pay日记类型 ")
	private String logType; 
		
	/** 金额变更 */
	@ApiModelProperty(value = "金额变更 ")
	private Double amount; 
		
	/** 描述 */
	@ApiModelProperty(value = "描述")
	private String logDesc; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间 ")
	private Date addTime; 
	
	/** 昵称  */
	@ApiModelProperty(value = "昵称")
	private String nickName;
	
	/**查询开始时间**/
	@ApiModelProperty(value = "查询开始时间")
	private Date startTime;
	
	/**查询结束时间**/
	@ApiModelProperty(value = "查询结束时间")
	private Date endTime;
		
	
	public PdCashLog() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PD_CASH_LOG_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "sn")
	public String  getSn(){
		return sn;
	} 
		
	public void setSn(String sn){
			this.sn = sn;
		}
		
    @Column(name = "log_type")
	public String  getLogType(){
		return logType;
	} 
		
	public void setLogType(String logType){
			this.logType = logType;
		}
		
    @Column(name = "amount")
	public Double  getAmount(){
		return amount;
	} 
		
	public void setAmount(Double amount){
			this.amount = amount;
		}
		
    @Column(name = "log_desc")
	public String  getLogDesc(){
		return logDesc;
	} 
		
	public void setLogDesc(String logDesc){
			this.logDesc = logDesc;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	
} 
