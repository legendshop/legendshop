/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 系统参数更新Dto
 *
 */
public class SystemParameterDto implements Serializable{
	
	private static final long serialVersionUID = -4452232919399013794L;

	private String groupId;
	
	private String key;
	
	private Object value;
	
	public SystemParameterDto() {
	}

	public SystemParameterDto(String groupId, String key, Object value) {
		this.groupId = groupId;
		this.key = key;
		this.value = value;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	
}
