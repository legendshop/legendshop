package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 预存款Dto
 */
@ApiModel(value="预存款Dto") 
public class AppPredepositDto implements Serializable{

	/**  */
	private static final long serialVersionUID = -8933034556954750048L;

	/** 预存款可用金额 */
	@ApiModelProperty(value="可用金额") 
	private Double availablePredeposit; 
	
	/** 预存款冻结金额  */
	@ApiModelProperty(value="冻结金额") 
	private Double freezePredeposit; 
		
	/** 总金额  */
	@ApiModelProperty(value="总金额") 
	private Double totalPredeposit;

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}
	
	
	public AppPredepositDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "AppPredepositDto [availablePredeposit=" + availablePredeposit + ", freezePredeposit=" + freezePredeposit
				+ ", totalPredeposit=" + totalPredeposit + "]";
	}
	
	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public Double getFreezePredeposit() {
		return freezePredeposit;
	}

	public void setFreezePredeposit(Double freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}

	public Double getTotalPredeposit() {
		return totalPredeposit;
	}

	public void setTotalPredeposit(Double totalPredeposit) {
		this.totalPredeposit = totalPredeposit;
	} 
		

	

}
