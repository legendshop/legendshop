package com.legendshop.model.entity;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品标签
 */
@Entity
@Table(name = "ls_prod_tag")
public class ProdTag implements GenericEntity<Long>{
	
	private static final long serialVersionUID = -7571396124663475715L;
	
	private Long id;
	private String name;
	private Long status;
	private String rightTag;
	private String bottomTag;
	private Long shopId;
	protected MultipartFile rightFile;
	protected MultipartFile bottomFile;
	
	
	public ProdTag() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public ProdTag(Long id, String name, Long status, String rightTag,
			String bottomTag, Long shopId) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.rightTag = rightTag;
		this.bottomTag = bottomTag;
		this.shopId = shopId;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_TAG_SEQ")
	public Long getId() {
		return id;
	}
	public void setId(Long Id) {
		this.id = Id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "status")
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
	@Column(name = "right_tag")
	public String getRightTag() {
		return rightTag;
	}
	public void setRightTag(String rightTag) {
		this.rightTag = rightTag;
	}
	
	@Column(name = "bottom_tag")
	public String getBottomTag() {
		return bottomTag;
	}
	public void setBottomTag(String bottomTag) {
		this.bottomTag = bottomTag;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Transient
	public MultipartFile getRightFile() {
		return rightFile;
	}


	public void setRightFile(MultipartFile rightFile) {
		this.rightFile = rightFile;
	}

	@Transient
	public MultipartFile getBottomFile() {
		return bottomFile;
	}


	public void setBottomFile(MultipartFile bottomFile) {
		this.bottomFile = bottomFile;
	}

	
	
}
