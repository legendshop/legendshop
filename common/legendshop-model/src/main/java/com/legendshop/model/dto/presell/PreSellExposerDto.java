/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

/**
 * 预售导出Dto.
 */
public class PreSellExposerDto {

	/** The id. */
	private Long id;

	/** 加密Token. */
	private String token;

	/** 是否导出 */
	private boolean isExpose;

	/** 信息 */
	private String msg;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token
	 *            the token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Checks if is expose.
	 *
	 * @return true, if checks if is expose
	 */
	public boolean isExpose() {
		return isExpose;
	}

	/**
	 * Sets the expose.
	 *
	 * @param isExpose
	 *            the expose
	 */
	public void setExpose(boolean isExpose) {
		this.isExpose = isExpose;
	}

	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg
	 *            the msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
