package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 店铺审核
 */
@Entity
@Table(name = "ls_shop_audit")
public class ShopAudit implements GenericEntity<Long> {

	private static final long serialVersionUID = -125032606470214327L;

	/** 店铺ID */
	private Long id; 
		
	/** 店铺ID */
	private Long shopId; 
		
	/** 状态 */
	private Integer status; 
		
	/** 修改时间 */
	private Date modifyDate; 
		
	/** 审核意见 */
	private String auditOpinion; 
		
	
	public ShopAudit() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_AUDIT_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
	
	 @Column(name = "status")
    public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "modify_date")
	public Date  getModifyDate(){
		return modifyDate;
	} 
		
	public void setModifyDate(Date modifyDate){
			this.modifyDate = modifyDate;
		}
		
    @Column(name = "audit_opinion")
	public String  getAuditOpinion(){
		return auditOpinion;
	} 
		
	public void setAuditOpinion(String auditOpinion){
			this.auditOpinion = auditOpinion;
		}
} 
