package com.legendshop.model.entity.shopDecotate;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家导航表
 */
@Entity
@Table(name = "ls_shop_nav")
public class ShopNav implements GenericEntity<Long>,Cloneable {

	private static final long serialVersionUID = 5439552335878839210L;

	/** ID */
	private Long id; 
		
	/** 商家Id */
	private Long shopId; 
		
	/** 导航名称 */
	private String title; 
		
	/** 导航URL */
	private String url; 
		
	/** 是否启用 */
	private Integer status; 
		
	/** 打开方式 */
	private String winType; 
		
	/** 排序 */
	private Integer seq; 
		
	
	public ShopNav() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_NAV_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "url")
	public String  getUrl(){
		return url;
	} 
		
	public void setUrl(String url){
			this.url = url;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "win_type")
	public String  getWinType(){
		return winType;
	} 
		
	public void setWinType(String winType){
			this.winType = winType;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	


} 
