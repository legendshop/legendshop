package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 支付密钥文件位置
 * @author Tony
 *
 */
public enum CertFileEnum implements StringEnum {

	/**通联支付密钥 */
	TONGLIAN_TLCERT("/certs/tonglian/TLCert.cer"),
	
	/**通联支付测试密钥 */
	TONGLIAN_TEST_TLCERT("/certs/tonglian/TLCert-test.cer"),
	
	/**微信支付支付密钥 */
	WEIXIN_APICLIENT_CERT_P12("/certs/weixin/apiclient_cert.p12"),
	
	
	/**银联支付支付密钥 */
	UNIONPAY_ACP_TEST_ENC("/certs/unionPay/acp_test_enc.cer"),
	
	/**银联支付支付密钥 */
	UNIONPAY_ACP_TEST_SIGN("/certs/unionPay/acp_test_sign.pfx"),
	
	/**银联支付支付密钥 */
	UNIONPAY_ACP_TEST_VERIFY_SIGN("/certs/unionPay/acp_test_verify_sign.cer"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private CertFileEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
