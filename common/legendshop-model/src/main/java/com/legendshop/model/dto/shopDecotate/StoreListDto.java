/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.shopDecotate.ShopNav;

/**
 * 门店列表.
 */
public class StoreListDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2729585383034332033L;

	/**  ID. */
	private Long decId; 
		
	/**  商家ID. */
	private Long shopId; 
		
	/**  是否启用Banner. */
	private Integer isBanner; 
		
	/**  是否启用店铺信息. */
	private Integer isInfo; 
		
	/**  是否启用店铺导航. */
	private Integer isNav; 
		
	/**  是否启用店铺轮播图. */
	private Integer isSlide; 
		
	/**  主题样式. */
	private String topCss; 
		
	/**  背景图片文件ID. */
	private String bgImgId; 
		
	/**  背景颜色. */
	private String bgColor; 
		
	/**  背景样式[repeat: 平铺, stretch: 拉伸]. */
	private String bgStyle; 
	
	/** 默认ShopNav. */
	private List<ShopNav> shopDefaultNavs;
	
	/** 店铺信息. */
	private  ShopLayoutShopInfoDto shopDefaultInfo;
	
	/** 商家分类信息. */
	private  List<ShopLayoutCateogryDto> cateogryDtos;
	
	/** 店铺热销. */
	private List<ShopLayoutHotProdDto> hotProds;
	
	
	/** The layout prod dtos. */
	/*
	 * 店铺商品信息
	 */
	private List<ShopLayoutProdDto> layoutProdDtos;


	/**
	 * Gets the dec id.
	 *
	 * @return the dec id
	 */
	public Long getDecId() {
		return decId;
	}


	/**
	 * Sets the dec id.
	 *
	 * @param decId the new dec id
	 */
	public void setDecId(Long decId) {
		this.decId = decId;
	}


	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}


	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	/**
	 * Gets the checks if is banner.
	 *
	 * @return the checks if is banner
	 */
	public Integer getIsBanner() {
		return isBanner;
	}


	/**
	 * Sets the checks if is banner.
	 *
	 * @param isBanner the new checks if is banner
	 */
	public void setIsBanner(Integer isBanner) {
		this.isBanner = isBanner;
	}


	/**
	 * Gets the checks if is info.
	 *
	 * @return the checks if is info
	 */
	public Integer getIsInfo() {
		return isInfo;
	}


	/**
	 * Sets the checks if is info.
	 *
	 * @param isInfo the new checks if is info
	 */
	public void setIsInfo(Integer isInfo) {
		this.isInfo = isInfo;
	}


	/**
	 * Gets the checks if is nav.
	 *
	 * @return the checks if is nav
	 */
	public Integer getIsNav() {
		return isNav;
	}


	/**
	 * Sets the checks if is nav.
	 *
	 * @param isNav the new checks if is nav
	 */
	public void setIsNav(Integer isNav) {
		this.isNav = isNav;
	}


	/**
	 * Gets the checks if is slide.
	 *
	 * @return the checks if is slide
	 */
	public Integer getIsSlide() {
		return isSlide;
	}


	/**
	 * Sets the checks if is slide.
	 *
	 * @param isSlide the new checks if is slide
	 */
	public void setIsSlide(Integer isSlide) {
		this.isSlide = isSlide;
	}


	/**
	 * Gets the top css.
	 *
	 * @return the top css
	 */
	public String getTopCss() {
		return topCss;
	}


	/**
	 * Sets the top css.
	 *
	 * @param topCss the new top css
	 */
	public void setTopCss(String topCss) {
		this.topCss = topCss;
	}


	/**
	 * Gets the bg img id.
	 *
	 * @return the bg img id
	 */
	public String getBgImgId() {
		return bgImgId;
	}


	/**
	 * Sets the bg img id.
	 *
	 * @param bgImgId the new bg img id
	 */
	public void setBgImgId(String bgImgId) {
		this.bgImgId = bgImgId;
	}


	/**
	 * Gets the bg color.
	 *
	 * @return the bg color
	 */
	public String getBgColor() {
		return bgColor;
	}


	/**
	 * Sets the bg color.
	 *
	 * @param bgColor the new bg color
	 */
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}


	/**
	 * Gets the bg style.
	 *
	 * @return the bg style
	 */
	public String getBgStyle() {
		return bgStyle;
	}


	/**
	 * Sets the bg style.
	 *
	 * @param bgStyle the new bg style
	 */
	public void setBgStyle(String bgStyle) {
		this.bgStyle = bgStyle;
	}



	/**
	 * Gets the shop default navs.
	 *
	 * @return the shop default navs
	 */
	public List<ShopNav> getShopDefaultNavs() {
		return shopDefaultNavs;
	}


	/**
	 * Sets the shop default navs.
	 *
	 * @param shopDefaultNavs the new shop default navs
	 */
	public void setShopDefaultNavs(List<ShopNav> shopDefaultNavs) {
		this.shopDefaultNavs = shopDefaultNavs;
	}


	/**
	 * Gets the shop default info.
	 *
	 * @return the shop default info
	 */
	public ShopLayoutShopInfoDto getShopDefaultInfo() {
		return shopDefaultInfo;
	}


	/**
	 * Sets the shop default info.
	 *
	 * @param shopDefaultInfo the new shop default info
	 */
	public void setShopDefaultInfo(ShopLayoutShopInfoDto shopDefaultInfo) {
		this.shopDefaultInfo = shopDefaultInfo;
	}


	/**
	 * Gets the cateogry dtos.
	 *
	 * @return the cateogry dtos
	 */
	public List<ShopLayoutCateogryDto> getCateogryDtos() {
		return cateogryDtos;
	}


	/**
	 * Sets the cateogry dtos.
	 *
	 * @param cateogryDtos the new cateogry dtos
	 */
	public void setCateogryDtos(List<ShopLayoutCateogryDto> cateogryDtos) {
		this.cateogryDtos = cateogryDtos;
	}


	/**
	 * Gets the hot prods.
	 *
	 * @return the hot prods
	 */
	public List<ShopLayoutHotProdDto> getHotProds() {
		return hotProds;
	}


	/**
	 * Sets the hot prods.
	 *
	 * @param hotProds the new hot prods
	 */
	public void setHotProds(List<ShopLayoutHotProdDto> hotProds) {
		this.hotProds = hotProds;
	}


	/**
	 * Gets the layout prod dtos.
	 *
	 * @return the layout prod dtos
	 */
	public List<ShopLayoutProdDto> getLayoutProdDtos() {
		return layoutProdDtos;
	}


	/**
	 * Sets the layout prod dtos.
	 *
	 * @param layoutProdDtos the new layout prod dtos
	 */
	public void setLayoutProdDtos(List<ShopLayoutProdDto> layoutProdDtos) {
		this.layoutProdDtos = layoutProdDtos;
	}
	
	
	
	
}
