/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import com.legendshop.dao.support.GenericEntity;

/**
 * 装修热门商品.
 */
public class ShopLayoutHotProdDto  implements GenericEntity<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1584106259026944870L;

	/** 商品Id. */
	private Long prodId;
	
	/** 商品名称. */
	private String prodName;
	
	/** 图片. */
	private String pic;
	
	/** 现金价格. */
	private Double cash;

	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the new cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Override
	public Long getId() {
		return prodId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Long id) {
        this.prodId=id;		
	}

}
