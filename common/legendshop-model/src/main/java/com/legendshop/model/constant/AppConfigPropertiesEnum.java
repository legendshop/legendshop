package com.legendshop.model.constant;

import com.legendshop.util.FileConfig;
import com.legendshop.util.FileConfigEnum;

/**
 * 公共配置for app.properties的key
 * 已去掉app.properties
 */
@Deprecated
public enum AppConfigPropertiesEnum implements FileConfigEnum{

	//APK文件保存的子路径
	APPFILE_SUB_PATH,
	
	//版本号
	VerId,
	
	//第三方登录配置
	THIRD_WEIXIN_APPID,
	
	THIRD_WEIXIN_SECRET,
	
	THIRD_QQ_APPID,
	
	THIRD_QQ_SECRET,
	
	THIRD_WEIBO_APPID,
	
	THIRD_WEIBO_SECRET,
	
	//环信请求注册的路径
	IMREGURL;

	@Override
	public String file() {
		return FileConfig.AppConfFile;
	}
	
}
