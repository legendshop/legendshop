package com.legendshop.model.entity.weixin;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信响应表
 */
@Entity
@Table(name = "ls_weixin_autoresponse")
public class WeixinAutoresponse implements GenericEntity<Long> {

	private static final long serialVersionUID = 4735662687861340623L;

	/**  */
	private Long id; 
		
	/** 消息类型 */
	private String msgType; 
		
	/** 模版ID */
	private Long templateId; 
		
	/** 模版名称 */
	private String templateName; 
	
	/*微信用户触发消息类型*/
	private String msgTriggerType;
	
	private String content;
	
	
		
	
	public WeixinAutoresponse() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_AUTORESPONSE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "msgType")
	public String  getMsgType(){
		return msgType;
	} 
		
	public void setMsgType(String msgType){
			this.msgType = msgType;
		}
		
    @Column(name = "templateId")
	public Long  getTemplateId(){
		return templateId;
	} 
		
	public void setTemplateId(Long templateId){
			this.templateId = templateId;
		}
		
    @Column(name = "templateName")
	public String  getTemplateName(){
		return templateName;
	} 
		
	public void setTemplateName(String templateName){
			this.templateName = templateName;
		}

	 @Column(name = "msgTriggerType")
	public String getMsgTriggerType() {
		return msgTriggerType;
	}

	public void setMsgTriggerType(String msgTriggerType) {
		this.msgTriggerType = msgTriggerType;
	}

	 @Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
	

} 
