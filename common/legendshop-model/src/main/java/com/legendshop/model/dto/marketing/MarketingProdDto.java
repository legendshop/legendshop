package com.legendshop.model.dto.marketing;

import java.io.Serializable;

//import org.aspectj.apache.bcel.generic.InstructionTargeter;


/**
 * 参与活动的商品
 * @author Tony
 *
 */
public class MarketingProdDto  implements Serializable{

	private static final long serialVersionUID = 8593739419180545688L;

	/** 营销活动编号 */
	private Long marketId; 
	
	/** 店铺编号 */
	private Long shopId;
	
	/**  */
	private Long id; 
		
	/** 商品ID */
	private Long prodId; 
	
	private String prodName;
	
	/** 属性名字 **/
	private String cnProperties;
	
	private Double cash;
	
	private Integer stock;
	
	private Long skuId;
	
	private String pic;
	
	private Float discount;
	
	private Double discountPrice;
	
	private Integer type;
	
	private String marketName;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
}
