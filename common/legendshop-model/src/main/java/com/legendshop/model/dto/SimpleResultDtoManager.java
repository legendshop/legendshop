package com.legendshop.model.dto;

/**
 * 管理返回结果
 *
 */
public class SimpleResultDtoManager {
	
	/**
	 * 成功
	 * @param result
	 * @return
	 */
	public static SimpleResultDto success(Object result){
		return new SimpleResultDto(result);
	}
	
	/**
	 * 失败
	 * @param result
	 * @return
	 */
	public static SimpleResultDto fail(Object result){
		return  new SimpleResultDto(SimpleResultDto.STATUS_FALIED, result);
	}

}
