package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 团购订单状态
 */
public enum SubGroupStatusEnum implements IntegerEnum{
	
	UNPAY(-2),//未支付
	
	MERGE_GROUP_IN(0),//团购进行中
	
	MERGE_GROUP_FAIL(-1),//团购失败
	
	MERGE_GROUP_SUCCESS(1),//团购成功
	
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		return num;
	}

	private SubGroupStatusEnum(Integer num) {
		this.num = num;
	}
	
}
