/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

/**
 * @Description 预售订单项DTO
 * @author 开发很忙
 */
public class PresellSubItemDto {
	/** 商品ID */
	private Long prodId;
	
	/** SKU ID */
	private Long skuId;
	
	/** 商品名称 */
	private String prodName;
	
	/** 商品价格(原价) */
	private Double prodPrice;
	
	/** 商品价格(现价) */
	private Double prodCash;
	
	/** 商品图片 */
	private String prodPic;
	
	/** 属性 */
	private String attribute;
	
	/** 购买个数 */
	private Integer basketCount;
	
	/** 商品总金额 */
	private Double productTotalAmout;
	
	/** 评论状态 */
	private Long commSts;
	
	/** 交易快照 */
	private Long snapshotId;
	
	/**
	 * @return the prodId
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * @return the skuId
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * @param skuId the skuId to set
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the prodPrice
	 */
	public Double getProdPrice() {
		return prodPrice;
	}

	/**
	 * @param prodPrice the prodPrice to set
	 */
	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	/**
	 * @return the prodCash
	 */
	public Double getProdCash() {
		return prodCash;
	}

	/**
	 * @param prodCash the prodCash to set
	 */
	public void setProdCash(Double prodCash) {
		this.prodCash = prodCash;
	}

	/**
	 * @return the prodPic
	 */
	public String getProdPic() {
		return prodPic;
	}

	/**
	 * @param prodPic the prodPic to set
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	
	/**
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	/**
	 * @return the basketCount
	 */
	public Integer getBasketCount() {
		return basketCount;
	}

	/**
	 * @return the productTotalAmout
	 */
	public Double getProductTotalAmout() {
		return productTotalAmout;
	}

	/**
	 * @param productTotalAmout the productTotalAmout to set
	 */
	public void setProductTotalAmout(Double productTotalAmout) {
		this.productTotalAmout = productTotalAmout;
	}

	/**
	 * @param basketCount the basketCount to set
	 */
	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	/**
	 * @return the commSts
	 */
	public Long getCommSts() {
		return commSts;
	}

	/**
	 * @param commSts the commSts to set
	 */
	public void setCommSts(Long commSts) {
		this.commSts = commSts;
	}

	/**
	 * @return the snapshotId
	 */
	public Long getSnapshotId() {
		return snapshotId;
	}

	/**
	 * @param snapshotId the snapshotId to set
	 */
	public void setSnapshotId(Long snapshotId) {
		this.snapshotId = snapshotId;
	}

	
}
