/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 订单方式
 * @author Tony
 *
 */
public enum PayMannerEnum implements IntegerEnum {

	 /** 货到付款 */
	 DELIVERY_ON_PAY(1),

	 /** 在线付款 */
     ONLINE_PAY(2),
	
	 /**门店支付 */
	 SHOP_STORE_PAY(3);
	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	PayMannerEnum(Integer num) {
		this.num = num;
	}
	
    public static PayMannerEnum fromCode(Integer code) {
        for (PayMannerEnum mannerEnum : PayMannerEnum.values()) {
            if (mannerEnum.num.equals(code)) {
                return mannerEnum;
            }
        }
        return PayMannerEnum.ONLINE_PAY;
    }


}
