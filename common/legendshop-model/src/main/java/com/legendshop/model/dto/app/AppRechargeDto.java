/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.InvoiceSub;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * App充值成功订单数据Dto
 */

@ApiModel("app充值成功订单数据Dto")
public class AppRechargeDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2116640559353169082L;

	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
	private String subNumber;
	
	/** 充值金额 **/
	@ApiModelProperty(value = "充值金额 ")
	private Double actualTotal;
	
	/** 支付方式名称 **/
	@ApiModelProperty(value = "支付方式名称 ")
	private String payTypeName;
	
	/** 订单单据类型 **/
	@ApiModelProperty(value = "订单单据类型")
	private String subSettlementType;

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}
	
}
