package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信文字模板
 */
@Entity
@Table(name = "ls_weixin_texttemplate")
public class WeixinTexttemplate implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2581471674683552134L;

	/**  */
	private Long id; 
		
	/** 模版名称 */
	private String templatename; 
		
	/** 模版内容 */
	private String content; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 创建人 */
	private String createUserId; 
		
	/** 排序 */
	private Long seq; 
		
	
	public WeixinTexttemplate() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_TEXTTEMPLATE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "templatename")
	public String  getTemplatename(){
		return templatename;
	} 
		
	public void setTemplatename(String templatename){
			this.templatename = templatename;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "create_user_id")
	public String  getCreateUserId(){
		return createUserId;
	} 
		
	public void setCreateUserId(String createUserId){
			this.createUserId = createUserId;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
	


} 
