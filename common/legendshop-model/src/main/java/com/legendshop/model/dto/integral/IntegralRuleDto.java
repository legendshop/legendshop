package com.legendshop.model.dto.integral;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 积分规则DTO
 */
@ApiModel("积分规则DTO")
public class IntegralRuleDto  implements Serializable {
	
	private static final long serialVersionUID = 1172828797044574518L;
	
	/** 注册 */
	@ApiModelProperty(value = "注册")
	private Integer reg;
	
	/** 登录 */
	@ApiModelProperty(value = "登录")
	private Integer login;
	
	/** 验证邮箱 */
	@ApiModelProperty(value = "验证邮箱 ")
	private Integer verifyEmail;
	
	/** 验证手机 */
	@ApiModelProperty(value = "验证手机")
	private Integer verifyMobile;
	
	/** 商品评论 */
	@ApiModelProperty(value = "商品评论")
	private Integer productReview;
	
	/** 推荐用户 */
	@ApiModelProperty(value = "推荐用户 ")
	private Integer recUser;
	
	/** 晒单 */
	@ApiModelProperty(value = "晒单")
	private Integer showOrder;

	/*private Integer proportion;*/
	
	/*private Integer mostpoints;*/
	
	/** 积分兑换规则 */
	@ApiModelProperty(value = "积分兑换规则")
	private Integer integralConvertRelu;
	
	/** 积分消费规则  */
	@ApiModelProperty(value = "积分消费规则")
	private Double integralConsumptionRelu;
	
	/** 单次最高可获积分 */
	@ApiModelProperty(value = "单次最高可获积分")
	private Integer capping;
	
	/** 状态0为开启，-1为不开启 */
	@ApiModelProperty(value = "状态0为开启，-1为不开启")
	private Integer status;
	
	public Integer getReg() {
		return reg;
	}
	public void setReg(Integer reg) {
		this.reg = reg;
	}
	public Integer getLogin() {
		return login;
	}
	public void setLogin(Integer login) {
		this.login = login;
	}
	public Integer getProductReview() {
		return productReview;
	}
	public void setProductReview(Integer productReview) {
		this.productReview = productReview;
	}
	public Integer getVerifyEmail() {
		return verifyEmail;
	}
	public void setVerifyEmail(Integer verifyEmail) {
		this.verifyEmail = verifyEmail;
	}
	public Integer getVerifyMobile() {
		return verifyMobile;
	}
	public void setVerifyMobile(Integer verifyMobile) {
		this.verifyMobile = verifyMobile;
	}
	public Integer getRecUser() {
		return recUser;
	}
	public void setRecUser(Integer recUser) {
		this.recUser = recUser;
	}
	public Integer getShowOrder() {
		return showOrder;
	}
	public void setShowOrder(Integer showOrder) {
		this.showOrder = showOrder;
	}
	public Integer getIntegralConvertRelu() {
		return integralConvertRelu;
	}
	public void setIntegralConvertRelu(Integer integralConvertRelu) {
		this.integralConvertRelu = integralConvertRelu;
	}
	public Double getIntegralConsumptionRelu() {
		return integralConsumptionRelu;
	}
	public void setIntegralConsumptionRelu(Double integralConsumptionRelu) {
		this.integralConsumptionRelu = integralConsumptionRelu;
	}
	
	public Integer getCapping() {
		return capping;
	}
	public void setCapping(Integer capping) {
		this.capping = capping;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
