/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * --------------------------------------------
 * ---------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ------------------
 * -----------------------------------------------------------------
 * 
 * 官方网站：http://www.legendesign.net
 */
public enum SaveToCartStatusEnum implements StringEnum {
	
	/**
	 * 下单成功
	 */
	OK("OK"),
	
	/** 商品不存在，下单失败. */
	ERR("ERR"),

	/** 您是商品主人，不能购买此商品。 */
	OWNER("OWNER"),

	/** Product less 缺货. */
	LESS("LESS"),
	
	/**超出购物车大小. */
	MAX("MAX"), 
	
	/**产品下线 */
	OFFLINE("OFFLINE"),
	
	/**商家不存在*/
	NO_SHOP("NO_SHOP"),
	
	/** 用户没登录 */
	NO_LOGIN("NO_LOGIN");
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new register enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SaveToCartStatusEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
