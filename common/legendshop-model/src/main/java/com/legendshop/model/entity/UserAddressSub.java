package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *用户配送地址
 */
@Entity
@Table(name = "ls_usr_addr_sub")
@ApiModel(value = "用户订单地址")
public class UserAddressSub implements GenericEntity<Long> {

	private static final long serialVersionUID = 8092226944540764133L;
	/** ID */
	@ApiModelProperty(value = "ID")
	private Long addrOrderId; 
		
	/** 用户ID */
	@ApiModelProperty(value = "用户ID")
	private String userId; 
		
	/** 接受人名称 */
	@ApiModelProperty(value = "接受人名称")
	private String receiver; 
		
	/** 邮编 */
	@ApiModelProperty(value = "邮编")
	private String subPost; 
		
	/** 省份id. */
	@ApiModelProperty(value = "省份id")
	private Integer provinceId;

	/** 城市id. */
	@ApiModelProperty(value = "城市id")
	private Integer cityId;

	/** 地区id. */
	@ApiModelProperty(value = "地区id")
	private Integer areaId;
	
	/** 地址 */
	@ApiModelProperty(value = "地址")
	private String subAdds; 
	
	/** 地址别名 */
	@ApiModelProperty(value = "地址别名")
	private String aliasAddr;
	
	/** 详细地址 */
	@ApiModelProperty(value = "详细地址")
	private String detailAddress;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码")
	private String mobile; 
		
	/** 固话号码 */
	@ApiModelProperty(value = "固话号码")
	private String telphone; 
		
	/** Email地址 */
	@ApiModelProperty(value = "Email地址")
	private String email; 
	
	public UserAddressSub() {
    }
		
	@Transient
	public Long getId() {
		return addrOrderId;
	}
	
	public void setId(Long id) {
		addrOrderId = id;
	}

	
	@Id
	@Column(name = "addr_order_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USR_ADDR_SUB_SEQ")
	public Long  getAddrOrderId(){
		return addrOrderId;
	} 
		
	public void setAddrOrderId(Long addrOrderId){
			this.addrOrderId = addrOrderId;
		}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "receiver")
	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	@Column(name = "sub_post")
	public String getSubPost() {
		return subPost;
	}

	public void setSubPost(String subPost) {
		this.subPost = subPost;
	}

	@Column(name = "province_id")
	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	@Column(name = "city_id")
	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	@Column(name = "area_id")
	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	@Column(name = "sub_adds")
	public String getSubAdds() {
		return subAdds;
	}

	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}

	@Column(name = "alias_addr")
	public String getAliasAddr() {
		return aliasAddr;
	}

	public void setAliasAddr(String aliasAddr) {
		this.aliasAddr = aliasAddr;
	}

	@Column(name = "detail_address")
	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	@Column(name = "mobile")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "telphone")
	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
} 
