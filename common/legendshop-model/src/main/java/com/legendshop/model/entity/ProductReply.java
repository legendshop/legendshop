package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品评论回复.
 */
@Entity
@Table(name = "ls_prod_reply")
public class ProductReply  implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1435534890919090805L;

	/** The id. */
	private Long id;
	
	/** 回复的评论ID */
	private Long prodcommId;
	
	/** 回复的父节点 */
	private Long parentReplyId;
	
	/** 回复的回复的用户ID */
	private String parentUserId;
	
	/** 回复的回复的用户名 */
	private String parentUserName;

	/** 回复者IP地址 */
	private String postip;
	
	/** 回复内容 */
	private String replyContent;
	
	/** 回复人用户Id */
	private String replyUserId;
	
	/** 回复人用户名 */
	private String replyUserName;
	
	/** 回复时间 */
	private Date replyTime;
	
	private Integer status;
	
	private String commName;

	private String parentReplyName;
	
	private Long basketId;
	
	private Long prodId;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_REPLY_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="parent_reply_id")
	public Long getParentReplyId() {
		return parentReplyId;
	}

	public void setParentReplyId(Long parentReplyId) {
		this.parentReplyId = parentReplyId;
	}

	@Column(name="parent_user_id")
	public String getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}

	@Column(name="parent_user_name")
	public String getParentUserName() {
		return parentUserName;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}

	@Column(name="prod_comm_id")
	public Long getProdcommId() {
		return prodcommId;
	}

	public void setProdcommId(Long prodcommId) {
		this.prodcommId = prodcommId;
	}


	@Column(name="postip")
	public String getPostip() {
		return postip;
	}

	public void setPostip(String postip) {
		this.postip = postip;
	}

	@Column(name="reply_content")
	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	@Column(name="reply_user_id")
	public String getReplyUserId() {
		return replyUserId;
	}

	public void setReplyUserId(String replyUserId) {
		this.replyUserId = replyUserId;
	}

	@Column(name="reply_user_name")
	public String getReplyUserName() {
		return replyUserName;
	}

	public void setReplyUserName(String replyUserName) {
		this.replyUserName = replyUserName;
	}

	@Column(name="reply_time")
	public Date getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Transient
	public String getCommName() {
		return commName;
	}

	public void setCommName(String commName) {
		this.commName = commName;
	}

	@Transient
	public String getParentReplyName() {
		return parentReplyName;
	}

	public void setParentReplyName(String parentReplyName) {
		this.parentReplyName = parentReplyName;
	}

	@Transient
	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	@Transient
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	
}
