package com.legendshop.model.dto.integral;

import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("积分订单项Dto")
public class IntegralOrderItemDto  implements Cloneable, GenericEntity<Long> {
	
	@ApiModelProperty(value = "主键id")
	private Long id;
	
	@ApiModelProperty(value = "积分订单项id")
	private Long orderItemId;
	
	@ApiModelProperty(value = "积分商品ID")
	private Long prodId;
	
	@ApiModelProperty(value = "积分商品名称")
	private String prodName;
	
	@ApiModelProperty(value = "产品主图片路径")
	private String prodPic;
	
	@ApiModelProperty(value = "商品数量")
	private Integer basketCount;
	
	@ApiModelProperty(value = "积分数")
	private Integer exchangeIntegral;
	
	@ApiModelProperty(value = "总积分")
	private Double totalIntegral;
	
	@ApiModelProperty(value = "商品价格")
	private Double price;
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	public Long getId() {
		return orderItemId;
	}


	@Override
	public void setId(Long id) {
		 this.orderItemId=id;
	}


	public Long getOrderItemId() {
		return orderItemId;
	}


	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}


	public Long getProdId() {
		return prodId;
	}


	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}





	public String getProdPic() {
		return prodPic;
	}


	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}


	public Integer getBasketCount() {
		return basketCount;
	}


	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}


	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}


	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public String getProdName() {
		return prodName;
	}


	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	

	@Override
	 public Object clone() throws CloneNotSupportedException {
	        return super.clone();
	}


	public Double getTotalIntegral() {
		return totalIntegral;
	}


	public void setTotalIntegral(Double totalIntegral) {
		this.totalIntegral = totalIntegral;
	}

}
