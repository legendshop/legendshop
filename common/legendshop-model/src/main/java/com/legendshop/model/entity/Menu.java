/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;


/**
 * The Class Menu. 后台菜单
 */
@Entity
@Table(name = "ls_menu")
public class Menu  implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8596054428957749988L;

	private Long menuId;
	// 名称
	/** The name. */
	private String name;

	// 国际化标签
	/** The label. */
	private String label;

	private String title;

	//菜单对应的Role
	private List<String> roleNameList;
	// 顺序
	/** The seq. */
	private Integer seq;

	// 连接
	/** The action. */
	private String action;
	
	//点击菜单的action
	private String menuAction;

	// 所需要的权限，只要有其中一个全新即可访问
	/** The required any functions. */
	private List<String> requiredAnyFunctions;

	// 由那些插件提供的菜单
	private String providedPlugin;

	// 父菜单名称
	/** The parent name. */
	private Long parentId;

	private Integer grade;
	
	/** 状态，1：正常使用，0：下线 **/
	private Integer status;

	// 子菜单
	/** The sub menu. */
	private Set<Menu> subMenu = new TreeSet<Menu>(new MenuComparator());
	
	//父菜单
	private Menu parentMenu;
	
	//打开连接方式有 ： _blank  _self
	private String target;
	
	// 用于展示的子菜单
	private List<Menu> subList;

	/**
	 * @return the menuId
	 */
	@Id
	@Column(name = "menu_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MENU_SEQ")
	public Long getMenuId() {
		return menuId;
	}

	/**
	 * @param menuId
	 *            the menuId to set
	 */
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the label
	 */
	@Column(name = "label")
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the title
	 */
	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * @param seq
	 *            the seq to set
	 */
	public void setSeq(Integer order) {
		this.seq = order;
	}

	/**
	 * @return the action
	 */
	@Column(name = "action")
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the requiredAnyFunctions
	 */
	@Transient
	public List<String> getRequiredAnyFunctions() {
		return requiredAnyFunctions;
	}

	/**
	 * @param requiredAnyFunctions
	 *            the requiredAnyFunctions to set
	 */
	public void setRequiredAnyFunctions(List<String> requiredAnyFunctions) {
		this.requiredAnyFunctions = requiredAnyFunctions;
	}

	public void addRequiredAnyFunctions(String requiredFunction) {
		if (requiredAnyFunctions == null) {
			requiredAnyFunctions = new ArrayList<String>();
		}
		this.requiredAnyFunctions.add(requiredFunction);
	}

	public Menu copy() {
		Menu menu = new Menu();
		menu.menuId = this.menuId;
		menu.name = this.name;
		menu.label = this.label;
		menu.title = this.title;
		menu.roleNameList = this.roleNameList;
		menu.seq = this.seq;
		menu.action = this.action;
		menu.requiredAnyFunctions = this.requiredAnyFunctions;
		menu.providedPlugin = this.providedPlugin;
		menu.parentId = this.parentId;
		menu.grade = this.grade;
		menu.status = this.status;
		menu.subMenu = this.subMenu;
		menu.parentMenu = this.parentMenu;
		menu.target = this.target;
		menu.subList = this.subList;
		return menu;
	}

	/**
	 * @return the providedPlugin
	 */
	@Column(name = "provided_plugin")
	public String getProvidedPlugin() {
		return providedPlugin;
	}

	/**
	 * @param providedPlugin
	 *            the providedPlugin to set
	 */
	public void setProvidedPlugin(String provideredPlugin) {
		this.providedPlugin = provideredPlugin;
	}

	/**
	 * @return the parentId
	 */
	@Column(name = "parent_id")
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the level
	 */
	@Column(name = "grade")
	public Integer getGrade() {
		return grade;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the subMenu
	 */
	@Transient
	public Set<Menu> getSubMenu() {
		return subMenu;
	}

	/**
	 * @param subMenu
	 *            the subMenu to set
	 */
	public void addSubMenu(Menu menu) {
		if (!subMenu.contains(menu)) {
			this.subMenu.add(menu);
		}
	}

	@Override
	public int hashCode() {
		return menuId.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		Menu menu = (Menu)obj;
		return menuId.equals(menu.getMenuId());
	}

	@Transient
	public Long getId() {
		return menuId;
	}

	public void setId(Long id) {
		this.menuId = id;
	}
	
	/**
	 * @return the parentMenu
	 */
	@Transient
	public Menu getParentMenu() {
		return parentMenu;
	}

	/**
	 * @param parentMenu the parentMenu to set
	 */
	public void setParentMenu(Menu parentMenu) {
		this.parentMenu = parentMenu;
	}

	/**
	 * @return the roleNameList
	 */
	@Transient
	public List<String> getRoleNameList() {
		return roleNameList;
	}

	/**
	 * @param roleNameList the roleNameList to set
	 */
	public void addRoleNameList(String roleName) {
		if(roleNameList == null){
			roleNameList = new ArrayList<String>();
		}
		roleNameList.add(roleName);
	}

	@Column(name = "target")
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	@Transient
	public List<Menu> getSubList() {
		return subList;
	}

	public void setSubList(List<Menu> subList) {
		this.subList = subList;
	}

	@Override
	public String toString() {
		return "Menu [menuId=" + menuId + ", name=" + name + ", label=" + label + ", title=" + title
				+ ", providedPlugin=" + providedPlugin + ", parentId=" + parentId + ", status=" + status
				+ ", parentMenu=" + parentMenu + "]";
	}

	@Transient
	public String getMenuAction() {
		return menuAction;
	}

	public void setMenuAction(String menuAction) {
		this.menuAction = menuAction;
	}

}
