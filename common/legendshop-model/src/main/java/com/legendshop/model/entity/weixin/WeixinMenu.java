package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 微信菜单
 */
@Entity
@Table(name = "ls_weixin_menu")
public class WeixinMenu implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 菜单编号 */
	private Long id; 
		
	/** 菜单名称 */
	private String name; 
		
	/** 菜单类型 */
	private String menuType; 
		
	/** 菜单Url */
	private String menuUrl; 
		
	/** 菜单文本类型 */
	private String infoType; 
		
	/** 菜单素材模版ID */
	private Long templateId; 
		
	/** 创建时间 */
	private Date createdate; 
		
	/** 菜单级别 */
	private Integer grade; 
		
	/** 父节点 */
	private Long parentId; 
		
	/** 排序 */
	private Integer seq; 
		
	/** 文本内容 */
	private String content; 
	
	private String menuKey;
		
	
	public WeixinMenu() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_MENU_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "menuType")
	public String  getMenuType(){
		return menuType;
	} 
		
	public void setMenuType(String menutype){
			this.menuType = menutype;
		}
		
    @Column(name = "menuUrl")
	public String  getMenuUrl(){
		return menuUrl;
	} 
		
	public void setMenuUrl(String menuurl){
			this.menuUrl = menuurl;
		}
		
    @Column(name = "infoType")
	public String  getInfoType(){
		return infoType;
	} 
		
	public void setInfoType(String infotype){
			this.infoType = infotype;
		}
		
    @Column(name = "templateid")
	public Long  getTemplateId(){
		return templateId;
	} 
		
	public void setTemplateId(Long templateid){
			this.templateId = templateid;
		}
		
		
    @Column(name = "createDate")
	public Date  getCreatedate(){
		return createdate;
	} 
		
	public void setCreatedate(Date createdate){
			this.createdate = createdate;
		}
		
    @Column(name = "grade")
	public Integer  getGrade(){
		return grade;
	} 
		
	public void setGrade(Integer grade){
			this.grade = grade;
		}
		
    @Column(name = "parent_id")
	public Long  getParentId(){
		return parentId;
	} 
		
	public void setParentId(Long parentId){
			this.parentId = parentId;
		}
		
    @Column(name = "seq")
	public Integer  getSeq(){
		return seq;
	} 
		
	public void setSeq(Integer seq){
			this.seq = seq;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}

	@Column(name = "menu_key")
	public String getMenuKey() {
		return menuKey;
	}

	public void setMenuKey(String menuKey) {
		this.menuKey = menuKey;
	}
	


} 
