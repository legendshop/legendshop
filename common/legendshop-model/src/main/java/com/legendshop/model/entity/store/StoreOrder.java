package com.legendshop.model.entity.store;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 门店订单
 */
@Entity
@Table(name = "ls_store_order")
public class StoreOrder implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/** 订单编号 */
	private String orderSn; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 门店ID */
	private Long storeId; 
		
	/** 订单用户 */
	private String userId; 
		
	/** 收货人 */
	private String reciverName; 
		
	/** 手机 */
	private String reciverMobile; 
		
	/** 提货码 */
	private String dlyoPickupCode; 
		
	/** 付款方式 */
	private Integer payType; 
		
	/** 状态[未自提、已自提] */
	private Integer dlyoSatus; 
		
	/** 订单生成时间 */
	private Date dlyoAddtime; 
		
	
	public StoreOrder() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "STORE_ORDER_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "order_sn")
	public String  getOrderSn(){
		return orderSn;
	} 
		
	public void setOrderSn(String orderSn){
			this.orderSn = orderSn;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "store_id")
	public Long  getStoreId(){
		return storeId;
	} 
		
	public void setStoreId(Long storeId){
			this.storeId = storeId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "reciver_name")
	public String  getReciverName(){
		return reciverName;
	} 
		
	public void setReciverName(String reciverName){
			this.reciverName = reciverName;
		}
		
    @Column(name = "reciver_mobile")
	public String  getReciverMobile(){
		return reciverMobile;
	} 
		
	public void setReciverMobile(String reciverMobile){
			this.reciverMobile = reciverMobile;
		}
		
    @Column(name = "dlyo_pickup_code")
	public String  getDlyoPickupCode(){
		return dlyoPickupCode;
	} 
		
	public void setDlyoPickupCode(String dlyoPickupCode){
			this.dlyoPickupCode = dlyoPickupCode;
		}
		
    @Column(name = "pay_type")
	public Integer  getPayType(){
		return payType;
	} 
		
	public void setPayType(Integer payType){
			this.payType = payType;
		}
		
    @Column(name = "dlyo_satus")
	public Integer  getDlyoSatus(){
		return dlyoSatus;
	} 
		
	public void setDlyoSatus(Integer dlyoSatus){
			this.dlyoSatus = dlyoSatus;
		}
		
    @Column(name = "dlyo_addtime")
	public Date  getDlyoAddtime(){
		return dlyoAddtime;
	} 
		
	public void setDlyoAddtime(Date dlyoAddtime){
			this.dlyoAddtime = dlyoAddtime;
		}
	


} 
