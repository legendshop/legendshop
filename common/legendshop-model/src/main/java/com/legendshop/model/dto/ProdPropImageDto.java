package com.legendshop.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.entity.ProdPropImage;

public class ProdPropImageDto {

	private ProductPropertyValueDto productPropertyValueDto;
	
	private List<ProdPropImage> ProdPropImageList;

	public ProductPropertyValueDto getProductPropertyValueDto() {
		return productPropertyValueDto;
	}

	public void setProductPropertyValueDto(ProductPropertyValueDto productPropertyValueDto) {
		this.productPropertyValueDto = productPropertyValueDto;
	}

	public List<ProdPropImage> getProdPropImageList() {
		return ProdPropImageList;
	}

	public void setProdPropImageList(List<ProdPropImage> prodPropImageList) {
		ProdPropImageList = prodPropImageList;
	}

	public void addProdPropImage(ProdPropImage propImage) {
		if(ProdPropImageList == null){
			ProdPropImageList = new ArrayList<ProdPropImage>();
		}
		ProdPropImageList.add(propImage);
	}
	
	
}
