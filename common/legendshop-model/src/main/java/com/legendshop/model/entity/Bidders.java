package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *竞拍人
 */
@Entity
@Table(name = "ls_bidders")
public class Bidders implements GenericEntity<Long> {

	private static final long serialVersionUID = 1353150163999968489L;

	/**  */
	private Long id; 
		
	/** 拍卖活动ID */
	private Long aId; 
		
	/** 拍卖商品ID */
	private Long prodId; 
		
	/** 拍卖SKUID */
	private Long skuId; 
		
	/** 出价时间 */
	private Date bitTime; 
		
	/** 出价金额 */
	private java.math.BigDecimal price; 
		
	/** 出价用户ID */
	private String userId; 
	
	private String nickName;
	
	private String userName;
		
	
	public Bidders() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "BIDDERS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "a_id")
	public Long  getAId(){
		return aId;
	} 
		
	public void setAId(Long aId){
			this.aId = aId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "bit_time")
	public Date  getBitTime(){
		return bitTime;
	} 
		
	public void setBitTime(Date bitTime){
			this.bitTime = bitTime;
		}
		
    @Column(name = "price")
	public java.math.BigDecimal  getPrice(){
		return price;
	} 
		
	public void setPrice(java.math.BigDecimal price){
			this.price = price;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}

	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	private String prodName;

	@Transient
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	private String prodPic;

	@Transient
	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	

} 
