/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import com.legendshop.model.KeyValueEntity;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 预售商品skuDto
 */
public class PresellSkuDto implements Serializable{

	private static final long serialVersionUID = -4262283197776277458L;

	/** sku id. */
	private Long skuId;

	/** sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2. */
	private String properties;

	/** 价格. */
	private Double price;

	/** sku 名称. */
	private String name;

	/** SKU标题. */
	private List<KeyValueEntity> propertiesNameList;

	/** sku状态。 1:正常 0:删除. */
	private Integer status;

	/** 对应属性值ID. */
	private String propValueIds;

	/** 预售价格. */
	private Double prePrice;

	/** sku库存. */
	private Long skuStock;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<KeyValueEntity> getPropertiesNameList() {
		return propertiesNameList;
	}

	public void setPropertiesNameList(List<KeyValueEntity> propertiesNameList) {
		this.propertiesNameList = propertiesNameList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPropValueIds() {
		return propValueIds;
	}

	public void setPropValueIds(String propValueIds) {
		this.propValueIds = propValueIds;
	}

	public Double getPrePrice() {
		return prePrice;
	}

	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	public Long getSkuStock() {
		return skuStock;
	}

	public void setSkuStock(Long skuStock) {
		this.skuStock = skuStock;
	}
}
