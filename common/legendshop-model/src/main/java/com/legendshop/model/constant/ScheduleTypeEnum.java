package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 定时计划业务类型
 */
public enum ScheduleTypeEnum implements IntegerEnum {

	/** 过期团购未成团原路退款 */
	TUTO_RETURN_GROUP(6),
	
	/** 买家申请退款，等待商家处理，时限为7天，超时系统将自动同意退款给买家；*/
	AUTO_7_REFUND(3),
	
	/** 买家已经退货，等待商家收货，时限为自买家填写物流单后起7天，超时系统将自动同意退款给买家；*/
	AUTO_7_GOODS_REFUND(4),
	
	/** 商家同意买家退款申请，提醒买家发货，时限为7天，买家超时未填写物流单号，系统将自动关闭退款申请。  */
	AUTO_7_NOT_WRITE_CLOSE_REFUND(5),
	
	;
	
	private Integer num;

	public Integer value() {
		return num;
	}

	ScheduleTypeEnum(Integer num) {
		this.num = num;
	}
	
}

