/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * The Class ReportDto.
 */
public class ReportDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3110273513845242073L;

	/** The numbers. */
	private Integer numbers;
	
	/** The total cash. */
	private Double totalCash;
	
	/** The sub date. */
	private Integer subDate;
	
	/** The rec date. */
	private String recDate;

	/**
	 * Gets the numbers.
	 *
	 * @return the numbers
	 */
	public int getNumbers() {
		return numbers;
	}

	/**
	 * Sets the numbers.
	 *
	 * @param numbers the new numbers
	 */
	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}

	/**
	 * Gets the sub date.
	 *
	 * @return the sub date
	 */
	public Integer getSubDate() {
		return subDate;
	}

	/**
	 * Sets the sub date.
	 *
	 * @param subDate the new sub date
	 */
	public void setSubDate(Integer subDate) {
		this.subDate = subDate;
	}

	/**
	 * Gets the total cash.
	 *
	 * @return the total cash
	 */
	public Double getTotalCash() {
		return totalCash;
	}

	/**
	 * Sets the total cash.
	 *
	 * @param totalCash the new total cash
	 */
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}

	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	public String getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(String recDate) {
		this.recDate = recDate;
	}
	
	
}
