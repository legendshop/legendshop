/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 是否承担运费
 * @author tony
 *
 */
public enum SupportTransportFreeEnum implements IntegerEnum {
	/** 商家承担运费 **/
	SELLER_SUPPORT(1),

	/** 买家承担运费 **/
	BUYERS_SUPPORT(0);

	
	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	SupportTransportFreeEnum(Integer num) {
		this.num = num;
	}

}
