/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.model.dto.shop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 商家入驻信息DTO
 * @author Joe
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "商家入驻信息")
public class OpenShopInfoDTO {

	@ApiModelProperty(value = "商家Id，全局唯一")
	protected Long shopId;

	@ApiModelProperty(value = "用户Id")
	protected String userId;

	@ApiModelProperty(value = "用户名")
	protected String userName;

	@ApiModelProperty(value = "联系人姓名/负责人")
	protected String contactName;

	@ApiModelProperty(value = "负责人身份证正面")
	protected String idCardPic;

	@ApiModelProperty(value = "负责人身份证反面")
	protected String idCardBackPic;

	@ApiModelProperty(value = "店铺名称")
	protected String siteName;

	@ApiModelProperty(value = "商城类型0：个人入驻，1：企业入驻， 参见ShopTypeEnum")
	protected Integer type;

	@ApiModelProperty(value = "店铺类型 0专营,1旗舰")
	protected Integer shopType;

	@ApiModelProperty(value = "商城状态")
	protected Integer status;

	@ApiModelProperty(value = "联系人手机")
	protected String contactMobile;

	@ApiModelProperty(value = "联系人邮件")
	protected String contactMail;

	@ApiModelProperty(value = "ip地址",hidden = true)
	protected String ip;


	/********************************** 华丽分割线 *****************************************/


	@ApiModelProperty(value = "公司名称")
	private String companyName;

	@ApiModelProperty(value = "法人身份证号码")
	private String idCardNum;

	@ApiModelProperty(value = "公司营业执照号码")
	private String licenseNumber;

	@ApiModelProperty(value = "公司营业执照副本")
	private String licensePicFilePath;


}
