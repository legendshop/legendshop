/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

/**
 * 后台首页数据Dto.
 */
public class AdminIndexDto {

	/** The user counts. */
	private int userCounts;
	
	/**  在线人数 *. */
	private int onLineUser;
	
	/**  商品数量 *. */
	private int prodCounts;
	
	/**  店铺数量 *. */
	private int shopCounts;
	
	/**  订单数量 *. */
	private int subCounts;
	
	/**  咨询数量 *. */
	private int consultCounts;
		
	/**  举报数量 *. */
	private int accusationCounts;
	
	/**  品牌数量 *. */
	private int brandCounts;

	/**
	 * Gets the on line user.
	 *
	 * @return the on line user
	 */
	public int getOnLineUser() {
		return onLineUser;
	}

	/**
	 * Sets the on line user.
	 *
	 * @param onLineUser the new on line user
	 */
	public void setOnLineUser(int onLineUser) {
		this.onLineUser = onLineUser;
	}

	/**
	 * Gets the prod counts.
	 *
	 * @return the prod counts
	 */
	public int getProdCounts() {
		return prodCounts;
	}

	/**
	 * Sets the prod counts.
	 *
	 * @param prodCounts the new prod counts
	 */
	public void setProdCounts(int prodCounts) {
		this.prodCounts = prodCounts;
	}

	/**
	 * Gets the shop counts.
	 *
	 * @return the shop counts
	 */
	public int getShopCounts() {
		return shopCounts;
	}

	/**
	 * Sets the shop counts.
	 *
	 * @param shopCounts the new shop counts
	 */
	public void setShopCounts(int shopCounts) {
		this.shopCounts = shopCounts;
	}

	/**
	 * Gets the sub counts.
	 *
	 * @return the sub counts
	 */
	public int getSubCounts() {
		return subCounts;
	}

	/**
	 * Sets the sub counts.
	 *
	 * @param subCounts the new sub counts
	 */
	public void setSubCounts(int subCounts) {
		this.subCounts = subCounts;
	}

	/**
	 * Gets the consult counts.
	 *
	 * @return the consult counts
	 */
	public int getConsultCounts() {
		return consultCounts;
	}

	/**
	 * Sets the consult counts.
	 *
	 * @param consultCounts the new consult counts
	 */
	public void setConsultCounts(int consultCounts) {
		this.consultCounts = consultCounts;
	}

	/**
	 * Gets the accusation counts.
	 *
	 * @return the accusation counts
	 */
	public int getAccusationCounts() {
		return accusationCounts;
	}

	/**
	 * Sets the accusation counts.
	 *
	 * @param accusationCounts the new accusation counts
	 */
	public void setAccusationCounts(int accusationCounts) {
		this.accusationCounts = accusationCounts;
	}

	/**
	 * Gets the brand counts.
	 *
	 * @return the brand counts
	 */
	public int getBrandCounts() {
		return brandCounts;
	}

	/**
	 * Sets the brand counts.
	 *
	 * @param brandCounts the new brand counts
	 */
	public void setBrandCounts(int brandCounts) {
		this.brandCounts = brandCounts;
	}

	/**
	 * Gets the user counts.
	 *
	 * @return the user counts
	 */
	public int getUserCounts() {
		return userCounts;
	}

	/**
	 * Sets the user counts.
	 *
	 * @param userCounts the new user counts
	 */
	public void setUserCounts(int userCounts) {
		this.userCounts = userCounts;
	}
	
	
}
