/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 友情链接
 */
@Entity
@Table(name="ls_extl_link")
public class ExternalLink extends UploadFile implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5467294318649826405L;

	/** The id. */
	private Long id;

	/** The url. */
	private String url;

	/** The wordlink. */
	private String wordlink;

	/** The content. */
	private String content;

	/** The bs. */
	private Integer bs;

	/** The picture. */
	private String picture;
	
	/**
	 * Instantiates a new external link.
	 */
	public ExternalLink() {
	}

	/**
	 * Instantiates a new external link.
	 * 
	 * @param id
	 *            the id
	 */
	public ExternalLink(Long id) {
		this.id = id;
	}

	/**
	 * full constructor.
	 * 
	 * @param id
	 *            the id
	 * @param url
	 *            the url
	 * @param wordlink
	 *            the wordlink
	 * @param content
	 *            the content
	 * @param bs
	 *            the bs
	 * @param userId
	 *            the user id
	 * @param userName
	 *            the user name
	 */
	public ExternalLink(Long id, String url, String wordlink, String content,
			Integer bs) {
		this.id = id;
		this.url = url;
		this.wordlink = wordlink;
		this.content = content;
		this.bs = bs;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator",  pkColumnValue = "EXTERNAL_LINK_SEQ")
	public Long getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	@Column(name="url")
	public String getUrl() {
		return this.url;
	}

	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the wordlink.
	 * 
	 * @return the wordlink
	 */
	@Column(name="wordlink")
	public String getWordlink() {
		return this.wordlink;
	}

	/**
	 * Sets the wordlink.
	 * 
	 * @param wordlink
	 *            the new wordlink
	 */
	public void setWordlink(String wordlink) {
		this.wordlink = wordlink;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	@Column(name="content")
	public String getContent() {
		return this.content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the bs.
	 * 
	 * @return the bs
	 */
	@Column(name="bs")
	public Integer getBs() {
		return this.bs;
	}

	/**
	 * Sets the bs.
	 * 
	 * @param bs
	 *            the new bs
	 */
	public void setBs(Integer bs) {
		this.bs = bs;
	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	@Column(name="picture")
	public String getPicture() {
		return picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture
	 *            the new picture
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

}