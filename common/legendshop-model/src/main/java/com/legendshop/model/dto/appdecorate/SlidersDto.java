package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 轮播广告
 * @author Tony
 *
 */
@ApiModel("轮播广告Dto")
public class SlidersDto implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "唯一标识")
	private String id;
	
	@ApiModelProperty(value = "显示图片")
	private String img;
	
	@ApiModelProperty(value = "action参数: 跳转action带的参数")
	private AdvertsActionparamDto actionParam;
	
	@ApiModelProperty(value = "顺序")
	private int ordering;
	
	@ApiModelProperty(value = "动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题")
	private String action;
	
	@ApiModelProperty(value = "跳转路径")
	private String path;

	
	public void setImg(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	public int getOrdering() {
		return ordering;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public AdvertsActionparamDto getActionParam() {
		return actionParam;
	}

	public void setActionParam(AdvertsActionparamDto actionParam) {
		this.actionParam = actionParam;
	}

	

	
	

}