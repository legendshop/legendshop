package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 第三方账号通行证来源
 * @author 开发很忙
 */
public enum PassportSourceEnum implements StringEnum{
	
	/** PC端 */
	PC("pc"),
	
	/** H5端 */
	H5("h5"),
	
	/** 原生APP */
	APP("app"),
	
	/** 微信小程序 */
	MP("mp")
	;
	
	private PassportSourceEnum(String value) {
		this.value = value;
	}

	private String value;

	@Override
	public String value() {
		return value;
	}
	
    public static PassportSourceEnum fromValue(String value) {
        for (PassportSourceEnum passportSourceEnum : PassportSourceEnum.values()) {
            if (passportSourceEnum.value.equals(value)) {
                return passportSourceEnum;
            }
        }
        return null;
    }

}
