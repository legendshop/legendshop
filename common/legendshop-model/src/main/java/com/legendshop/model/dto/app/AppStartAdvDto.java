package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP启动广告
 * @author 开发很忙
 */
@ApiModel("启动广告详情")
public class AppStartAdvDto {
	
	/** 唯一ID */
	@ApiModelProperty(value = "启动广告唯一ID")
	private Long id;
	
	/** 图片地址 */
	@ApiModelProperty(value = "图片地址")
	private String photo;
	
	/** 广告跳转URL */
	@ApiModelProperty(value = "广告跳转动作")
	private AppDecorateActionDto action;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public AppDecorateActionDto getAction() {
		return action;
	}

	public void setAction(AppDecorateActionDto action) {
		this.action = action;
	}
}
