/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 商城状态
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------.
 */
public enum ShopStatusEnum implements IntegerEnum {
	/**正常营业*/
	NORMAL(1),

	/**等待审核*/
	AUDITING(-1),

	/**联系人信息失败*/
	CONTACT_INFO_FAIL(-4),

	/**公司信息失败*/
	COMPANY_INFO_FAIL(-5),
	
	/**店铺信息失败*/
	SHOP_INFO_FAIL(-6),
	
	/**店铺下线*/
	OFFLINE(0),

	/** 拒绝 */
	DENY(-2),
	
	/**关闭店铺*/
	CLOSED(-3),

	/** 未开店 */
	NO_SHOP(-10)
	;

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ShopStatusEnum(Integer num) {
		this.num = num;
	}

}
