/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;


/**
 * 商品类目树
 */
public class CategoryTree {
	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 父节点ID
	 */
	private String  pId;
	
	/**
	 * 是否打开
	 */
	private boolean open = false;
	
	private boolean checked = false;
	
	public CategoryTree(Long id,Long pId,String name,boolean open){
		this.id = id.toString();
		this.setpId(pId == null ? "0" : pId.toString()) ;
		this.name = name;
		this.open = open;
	}
	
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public CategoryTree(){}
	
	public CategoryTree(Long id,Long pId,String name){
		this.id = id.toString();
		this.setpId(pId == null ? "0" : pId.toString()) ;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getpId() {
		return pId;
	}


	public void setpId(String pId) {
		this.pId = pId;
	}


	public boolean getChecked() {
		return checked;
	}


	public void setChecked(boolean checked) {
		this.checked = checked;
	}


	@Override
	public String toString() {
		return "CategoryTree [id=" + id + ", name=" + name + ", pId=" + pId + ", open=" + open + ", checked=" + checked
				+ "]";
	}



}
