/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

/**
 * @Description 申请仅退款表单DTO
 * @author 关开发
 */
public class ApplyRefundForm extends ApplyRefundReturnBase{
	/** 订单项 ID */
	private Long orderItemId;
	/**
	 * @return the orderItemId
	 */
	public Long getOrderItemId() {
		return orderItemId;
	}

	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}
	
}
