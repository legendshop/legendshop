package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 运输费用
 */
@Entity
@Table(name = "ls_transfee")
public class Transfee implements GenericEntity<Long>{

	private static final long serialVersionUID = -4819232855114311782L;

	private Long id ; 
	
	//运费模板id
	private Long transportId;
	
	//运输模式
	private String freightMode;
	
	//续件运费
	private Double transAddFee;
	
	//首件运费
	private Double transFee;
	
	//续件
	private Integer transAddWeight;
	
	//首件
	private Integer transWeight;
	
	//状态
	private Integer status;
	
	private String city;
	
	private Long cityId;
	
	private List<String> cityNameList; 
	
	private List<Long> cityIdList = new ArrayList<Long>();
	
	/** 城市ID **/
	private StringBuilder cityIds;
	
	//城市id
	private List<TransCity> transCityList;
	
	//城市列表，该集合下的城市采用同样的运费
	private Set<Integer> cityList=new HashSet<Integer>();

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "TRANSFEE_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "transport_id")
	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	@Column(name = "freight_mode")
	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	@Column(name = "trans_add_fee")
	public Double getTransAddFee() {
		return transAddFee;
	}

	public void setTransAddFee(Double transAddFee) {
		this.transAddFee = transAddFee;
	}

	@Column(name = "trans_fee")
	public Double getTransFee() {
		return transFee;
	}

	public void setTransFee(Double transFee) {
		this.transFee = transFee;
	}

	@Column(name = "trans_add_weight")
	public Integer getTransAddWeight() {
		return transAddWeight;
	}

	public void setTransAddWeight(Integer transAddWeight) {
		this.transAddWeight = transAddWeight;
	}

	@Column(name = "trans_weight")
	public Integer getTransWeight() {
		return transWeight;
	}

	public void setTransWeight(Integer transWeight) {
		this.transWeight = transWeight;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void addCityName(String name) {
		if(cityNameList == null){
			cityNameList = new ArrayList<String>();
		}
		this.cityNameList.add(name);
	}

	@Transient
	public List<String> getCityNameList() {
		return cityNameList;
	}

	public void setCityNameList(List<String> cityNameList) {
		this.cityNameList = cityNameList;
	}

	@Transient
	public List<Long> getCityIdList() {
		return cityIdList;
	}

	public void setCityIdList(List<Long> cityIdList) {
		this.cityIdList = cityIdList;
	}

	@Transient
	public List<TransCity> getTransCityList() {
		return transCityList;
	}

	public void setTransCityList(List<TransCity> transCityList) {
		this.transCityList = transCityList;
	}

	@Transient
	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	
	public void addCityId(Long cityId) {
//		if(cityIdList == null){
//			cityIdList = new ArrayList<Long>();
//		}
//		this.cityIdList.add(cityId);
		if(cityIds == null){
			cityIds = new StringBuilder();
			cityIds.append(cityId);
		}else{
			cityIds.append(",").append(cityId);
		}
		
	}

	@Transient
	public StringBuilder getCityIds() {
		return cityIds;
	}

	public void setCityIds(StringBuilder cityIds) {
		this.cityIds = cityIds;
	}

	@Transient
	public Set<Integer> getCityList() {
		return cityList;
	}

	public void setCityList(Set<Integer> cityList) {
		this.cityList = cityList;
	}

	
}
