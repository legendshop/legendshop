package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.app.AppTokenDto;

/**
 * AppToken
 */
@Entity
@Table(name = "ls_app_token")
public class AppToken implements GenericEntity<Long> {

	private static final long serialVersionUID = 379746210977825886L;

	/** 主键 */
	private Long id; 
	
	/** 用户Id */
	private String userId; 
		
	/** 商家ID */
	private Long shopId;
	
	/** 用户名 */
	private String userName; 
		
	/** token */
	private String accessToken; 
		
	/** 安全码 */
	private String securiyCode; 
		
	/** 有效时长 */
	private Long validateTime; 
		
	/** 开始时间 */
	private Date startDate; 
		
	/** 记录时间 */
	private Date recDate; 
	
	/** 设备Id **/
	private String deviceId;
	
	/** 平台 **/
	private String platform;
	
	/** token类型   AppTokenTypeEnum*/
	private String type;
	
	/** 第三方应用openId */
	private String openId;

	/** 版本号 */
	private String verId; 
	
	public AppToken() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "APP_TOKEN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "access_token")
	public String  getAccessToken(){
		return accessToken;
	} 
		
	public void setAccessToken(String accessToken){
			this.accessToken = accessToken;
		}
		
    @Column(name = "securiy_code")
	public String  getSecuriyCode(){
		return securiyCode;
	} 
		
	public void setSecuriyCode(String securiyCode){
			this.securiyCode = securiyCode;
		}
		
    @Column(name = "validate_time")
	public Long  getValidateTime(){
		return validateTime;
	} 
		
	public void setValidateTime(Long validateTime){
			this.validateTime = validateTime;
		}
		
    @Column(name = "start_date")
	public Date  getStartDate(){
		return startDate;
	} 
		
	public void setStartDate(Date startDate){
			this.startDate = startDate;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
		
    @Column(name = "ver_id")
	public String  getVerId(){
		return verId;
	} 
		
	public void setVerId(String verId){
			this.verId = verId;
		}
	
	@Column(name = "shop_id")
    public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
    @Column(name = "device_id")
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

    @Column(name = "platform")
	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "open_id")
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	public AppTokenDto toDto(){
		AppTokenDto dto = new AppTokenDto();
		dto.setId(id);
		dto.setAccessToken(accessToken);
		dto.setSecuriyCode(securiyCode);
		dto.setUserId(userId);
		dto.setShopId(shopId);
		dto.setUserName(userName);
		dto.setVerId(verId);
		dto.setPlatform(platform);
		return dto;
	}
} 
