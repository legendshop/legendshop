/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

import com.legendshop.util.AppUtils;

/**
 * 门店订单搜索Dto
 */
public class StoreOrderSerachDto implements Serializable {
	
	private static final long serialVersionUID = -8901972114258799569L;

	/** 订单状态 */
	private Integer orderStatus;
	
	/** 参数名 */
	private String paramName;
	
	/** 参数值 */
	private String paramValue;
	
	/** 当前页数 */
	private Integer curPageNO;

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public Integer getCurPageNO() {
		if(AppUtils.isBlank(curPageNO)){
			curPageNO=1;
		}
		return curPageNO;
	}

	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}
	
}
