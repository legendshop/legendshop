/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import com.legendshop.model.dto.*;
import com.legendshop.model.dto.seckill.SeckillSkuDto;
import com.legendshop.model.entity.ImgFile;
import com.legendshop.model.entity.PresellProd;

import java.util.List;
import java.util.Map;

/**
 * 预售商品Dto
 */
public class PresellProdNewDto {
	/** 商品id. */
	private Long prodId;

	/** 商品编号. */
	private String modelId;

	/** skuId. */
	private Long skuId;

	/** 商品名称. */
	private String name;

	/** 商城 分类 *. */
	protected Long categoryId;

	/** 简要描述,卖点等. */
	private String brief;

	/** 预售价. */
	private Double prePrice;

	/** 商品库存. */
	private int stocks;

	/** 商品现价. */
	private Double cash;

	/** 商品主图. */
	private String pic;

	/** 商品状态. */
	private Integer status;

	/** 商品内容. */
	private String content;

	/** 手机端商品内容. */
	private String contentM;

	/** 商品属性集合. */
	private List<ProductPropertyDto> prodPropDtoList;

	/** 商品图片. */
	private List<ProdPicDto> prodPics;

	/** 商品sku集合. */
	private List<PresellSkuDto> skuDtoList;

	/** 商品sku集合 的json格式字符串. */
	private String skuDtoListJson;

	/** 属性图片map. */
	private Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap;

	/** 属性图片list(json格式),用于商品详情页 展示. */
	private String propValueImgListJson;

	/** 商品图片. */
	private List<ImgFile> imgFileList;

	/** 商品属性图片. */
	List<ProdPropImageDto> propImageDtoList;

	/** 商品属性图片. */
	List<PropValueImgDto> propValueImgList;

	/** 运费模板ID *. */
	protected Long transportId;

	/** 运费模板ID *. */
	private Integer supportTransportFree;

	/** 配送类型 *. */
	private Integer transportType;

	/** ems费用 *. */
	private Double emsTransFee;

	/** 固定运费-快递 . */
	private Double expressTransFee;

	/** 固定运费-平邮. */
	private Double mailTransFee;

	/** 商品视屏路径 */
	private String proVideoUrl;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public Double getPrePrice() {
		return prePrice;
	}

	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	public int getStocks() {
		return stocks;
	}

	public void setStocks(int stocks) {
		this.stocks = stocks;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	public List<ProductPropertyDto> getProdPropDtoList() {
		return prodPropDtoList;
	}

	public void setProdPropDtoList(List<ProductPropertyDto> prodPropDtoList) {
		this.prodPropDtoList = prodPropDtoList;
	}

	public List<ProdPicDto> getProdPics() {
		return prodPics;
	}

	public void setProdPics(List<ProdPicDto> prodPics) {
		this.prodPics = prodPics;
	}

	public List<PresellSkuDto> getSkuDtoList() {
		return skuDtoList;
	}

	public void setSkuDtoList(List<PresellSkuDto> skuDtoList) {
		this.skuDtoList = skuDtoList;
	}

	public String getSkuDtoListJson() {
		return skuDtoListJson;
	}

	public void setSkuDtoListJson(String skuDtoListJson) {
		this.skuDtoListJson = skuDtoListJson;
	}

	public Map<PropertyImageId, List<PropertyImageDto>> getPropertyImageDtoMap() {
		return propertyImageDtoMap;
	}

	public void setPropertyImageDtoMap(Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap) {
		this.propertyImageDtoMap = propertyImageDtoMap;
	}

	public String getPropValueImgListJson() {
		return propValueImgListJson;
	}

	public void setPropValueImgListJson(String propValueImgListJson) {
		this.propValueImgListJson = propValueImgListJson;
	}

	public List<ImgFile> getImgFileList() {
		return imgFileList;
	}

	public void setImgFileList(List<ImgFile> imgFileList) {
		this.imgFileList = imgFileList;
	}

	public List<ProdPropImageDto> getPropImageDtoList() {
		return propImageDtoList;
	}

	public void setPropImageDtoList(List<ProdPropImageDto> propImageDtoList) {
		this.propImageDtoList = propImageDtoList;
	}

	public List<PropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	public void setPropValueImgList(List<PropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	public Double getEmsTransFee() {
		return emsTransFee;
	}

	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	public Double getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	public Double getMailTransFee() {
		return mailTransFee;
	}

	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}
}
