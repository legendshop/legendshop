package com.legendshop.model.dto;

import java.io.Serializable;
/**
 * 属性图片ID
 * @author Administrator
 *
 */
public class PropertyImageId implements Serializable {

	private static final long serialVersionUID = -6930669929367596123L;
	
	private Long id;
	
	private String name;
	
	public PropertyImageId(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public PropertyImageId(Long id){
		this.id = id;
	}
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyImageId other = (PropertyImageId) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
