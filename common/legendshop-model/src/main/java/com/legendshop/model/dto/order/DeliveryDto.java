/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.model.dto.order;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 物流信息.
 */
@ApiModel(value="物流信息Dto") 
public class DeliveryDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8024763117705589045L;

	/** The dvy type id. */
	@ApiModelProperty(value="配送方式ID") 
	private Long dvyTypeId; 
	
	/** The dvy flow id. */
	@ApiModelProperty(value="物流单号") 
	private String dvyFlowId; 
	
	/** The del name. */
	@ApiModelProperty(value="物流公司名称") 
	private String delName;
	
	
	/** The del url. */
	@ApiModelProperty(value="物流公司官网URL") 
	private String delUrl;
	
	/** The query url. */
	@ApiModelProperty(value="物流单号") 
	private String queryUrl;

	/**
	 * Gets the del name.
	 *
	 * @return the del name
	 */
	public String getDelName() {
		return delName;
	}

	/**
	 * Sets the del name.
	 *
	 * @param delName the del name
	 */
	public void setDelName(String delName) {
		this.delName = delName;
	}

	/**
	 * Gets the del url.
	 *
	 * @return the del url
	 */
	public String getDelUrl() {
		return delUrl;
	}

	/**
	 * Sets the del url.
	 *
	 * @param delUrl the del url
	 */
	public void setDelUrl(String delUrl) {
		this.delUrl = delUrl;
	}

	/**
	 * Gets the query url.
	 *
	 * @return the query url
	 */
	public String getQueryUrl() {
		return queryUrl;
	}

	/**
	 * Sets the query url.
	 *
	 * @param queryUrl the query url
	 */
	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	/**
	 * Gets the dvy type id.
	 *
	 * @return the dvy type id
	 */
	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	/**
	 * Sets the dvy type id.
	 *
	 * @param dvyTypeId the dvy type id
	 */
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	/**
	 * Gets the dvy flow id.
	 *
	 * @return the dvy flow id
	 */
	public String getDvyFlowId() {
		return dvyFlowId;
	}

	/**
	 * Sets the dvy flow id.
	 *
	 * @param dvyFlowId the dvy flow id
	 */
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}
	
	
}
