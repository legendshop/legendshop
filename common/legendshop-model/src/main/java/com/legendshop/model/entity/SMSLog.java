/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 短信记录表
 */
@Entity
@Table(name = "ls_sms_log")
public class SMSLog implements GenericEntity<Long>{

	private static final long serialVersionUID = -4222512516272182764L;

	// 
	/** The id. */
	private Long id ; 
		
	// 用户名称
	/** The user name. */
	private String userName ; 
		
	// 手机号码
	/** The user phone. */
	private String userPhone ; 
		
	// 短信内容
	/** The content. */
	private String content ; 
	
	//验证码
	private String mobileCode;
		
	// 短信类型
	/** The type. */
	private Integer type ; 
		
	// 发送时间
	/** The rec date. */
	private Date recDate ; 
	
	/** 响应码 **/
	private Integer responseCode;
	
	//状态 1:有效 0：失效
	private Integer status;

	// 请求调用接口来源
	private String requestInterface;

	// 请求Ip地址
	private String ip;

	// 请求时间
	private Date time;
	
	/**
	 * Instantiates a new sMS log.
	 */
	public SMSLog() {
    }
		
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id){
		this.id = id ;
	}
		
		
	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String  getUserName(){
		return userName ;
	} 
		
	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName){
		this.userName = userName ;
	}
		
		
	/**
	 * Gets the user phone.
	 *
	 * @return the user phone
	 */
	@Column(name = "user_phone")
	public String  getUserPhone(){
		return userPhone ;
	} 
		
	/**
	 * Sets the user phone.
	 *
	 * @param userPhone the new user phone
	 */
	public void setUserPhone(String userPhone){
		this.userPhone = userPhone ;
	}
		
		
	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	@Column(name = "content")
	public String  getContent(){
		return content ;
	} 
		
	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(String content){
		this.content = content ;
	}
		
		
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	@Column(name = "type")
	public Integer  getType(){
		return type ;
	} 
		
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type){
		this.type = type ;
	}
		
		
	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate ;
	} 
		
	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
	
  /* (non-Javadoc)
   * @see com.legendshop.model.entity.BaseEntity#getId()
   */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SMS_LOG_SEQ")
  public Long getId() {
		return 	id;
	}

	@Column(name = "mobile_code")
	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	@Column(name = "response_code")
	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "request_interface")
	public String getRequestInterface() {
		return requestInterface;
	}

	public void setRequestInterface(String requestInterface) {
		this.requestInterface = requestInterface;
	}

	@Column(name = "ip")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "time")
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
