/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.LongEnum;

/**
 * 预售商品状态
 */
public enum PresellProdStatusEnum implements LongEnum{
	
	/** 未提审 状态:-2 */
	NOT_PUT_AUDIT(-2L),
	
	/** 待审核 状态:-1 */
	WAIT_AUDIT(-1L),
	
	/** 上线（审核通过） 状态:0 */
	ONLINE(0L),
	
	/** 拒绝 状态:1 */
	DENY(1L),

	/** 完成 状态:2 */
	FINISH(2l),
	
	/** 已终止 状态:3*/
	STOP(3L),
	
	/** 已过期标记  */
	EXPIRED(-3L),
	;
	
	/** The num. */
	private Long num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.LongEnum#value()
	 */
	public Long value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	PresellProdStatusEnum(Long num) {
		this.num = num;
	}
}
