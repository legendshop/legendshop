package com.legendshop.model.entity.integral;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 会员积分日志明细
 */
@Entity
@Table(name = "ls_integral_log")
public class IntegraLog implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 唯一标识id */
	private Long id; 
		
	/** 用户ID */
	private String userId; 
		
	/** 后台用户操作ID */
	private String updateUserId; 
		
	/** 积分数量，- 为负数 */
	private Integer integralNum; 
		
	/** 日志类型[1:注册 2：登录 3:充值] */
	private Integer logType; 
		
	/** 日志描述 */
	private String logDesc; 
		
	/** 添加时间 */
	private Date addTime;

	/** 用户名称 **/
	private String userName;

	/** 昵称 */
	private String nickName;
	
	/** 手机号码 */
	private String userMobile;
	

	public IntegraLog() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INTEGRAL_LOG_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "update_user_id")
	public String  getUpdateUserId(){
		return updateUserId;
	} 
		
	public void setUpdateUserId(String updateUserId){
			this.updateUserId = updateUserId;
		}
		
    @Column(name = "integral_num")
	public Integer  getIntegralNum(){
		return integralNum;
	} 
		
	public void setIntegralNum(Integer integralNum){
			this.integralNum = integralNum;
		}
		
    @Column(name = "log_type")
	public Integer  getLogType(){
		return logType;
	} 
		
	public void setLogType(Integer logType){
			this.logType = logType;
		}
		
    @Column(name = "log_desc")
	public String  getLogDesc(){
		return logDesc;
	} 
		
	public void setLogDesc(String logDesc){
			this.logDesc = logDesc;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}

	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	@Transient
	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
} 
