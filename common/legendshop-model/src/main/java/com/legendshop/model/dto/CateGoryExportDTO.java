package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 类目导出dto(店铺类目/平台类目)
 * 
 * @author Zhongweihan
 * 
 */
public class CateGoryExportDTO implements Serializable {

    /** 类目id */
    private Long categoryId;

    /** 类目名称(多级展示) */
    private String categoryName;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
