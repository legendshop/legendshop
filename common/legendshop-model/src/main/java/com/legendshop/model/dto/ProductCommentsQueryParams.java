/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.model.dto;

import java.io.Serializable;

import com.legendshop.util.AppUtils;

/**
 * 商品评论查询参数 
 * @author 开发很忙
 *
 */
public class ProductCommentsQueryParams implements Serializable {
	
	private Long prodId;
	
	private Integer curPageNO;
	
	/** 条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append */
	private String condition;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getCurPageNO() {
		if(null == curPageNO){
			curPageNO = 1;
		}
		return curPageNO;
	}

	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}

	public String getCondition() {
		if(AppUtils.isBlank(condition)){
			condition = "all";
		}
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
