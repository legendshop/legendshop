package com.legendshop.model.entity;

import java.io.Serializable;
import java.util.Comparator;

public class FloorSubItemComparator implements Comparator<FloorSubItem>,Serializable{

	private static final long serialVersionUID = 4424107456880959294L;

	public int compare(FloorSubItem o1, FloorSubItem o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		}else if(o1.getSeq().equals(o2.getSeq())){
			return 0;
		} else if (o1.getSeq() < o2.getSeq()) {
			return -1;
		} else {
			return 1;
		}
	}


}
