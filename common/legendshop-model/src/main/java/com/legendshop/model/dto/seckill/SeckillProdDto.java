/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.ProdPropImageDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.PropertyImageDto;
import com.legendshop.model.dto.PropertyImageId;
import com.legendshop.model.entity.ImgFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 秒杀商品.
 *
 */
@ApiModel(value="秒杀商品")
public class SeckillProdDto implements Serializable {
	
	private static final long serialVersionUID = -1146983134044766251L;

	/** 商品id. */
	@ApiModelProperty(value="商品id")
	private Long prodId;
	
	/** 商品编号. */
	@ApiModelProperty(value="商品编号")
	private String modelId;
	
	/** 商品名称. */
	@ApiModelProperty(value="商品名称")
	private String name;
	
	/** 商城 分类 *. */
	@ApiModelProperty(value="商城 分类")
	protected Long categoryId;
	
	/** 简要描述,卖点等. */
	@ApiModelProperty(value="简要描述,卖点等")
	private String brief;
	
	/** 秒杀价. */
	@ApiModelProperty(value="秒杀价")
	private Double seckPrice;
	
	/** 秒杀库存. */
	@ApiModelProperty(value="秒杀库存")
	private int seckStock;
	
	/** 商品现价. */
	@ApiModelProperty(value="商品现价")
	private Double cash;
	
	/** 商品主图. */
	@ApiModelProperty(value="商品主图")
	private String pic;
	
	/** 商品状态. */
	@ApiModelProperty(value="商品状态")
	private Integer status;
	
	/** 商品内容. */
	@ApiModelProperty(value="商品内容")
	private String content;
	
	/** 手机端商品内容. */
	@ApiModelProperty(value="手机端商品内容")
	private String contentM;
	
	/** 商品属性集合. */
	@ApiModelProperty(value="商品属性集合")
	private List<ProductPropertyDto> prodPropDtoList;
	
	/** 商品图片. */
	@ApiModelProperty(value="商品图片")
	private List<String> prodPics;
	
	/** 商品sku集合. */
	@ApiModelProperty(value="商品sku集合")
	private List<SeckillSkuDto> skuDtoList;
	
	/** 商品sku集合 的json格式字符串. */
	@ApiModelProperty(value="商品sku集合 的json格式字符串")
	private String skuDtoListJson;
	
	/** 属性图片map. */
	@ApiModelProperty(value="属性图片map")
	private Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap;
	
	/** 属性图片list(json格式),用于商品详情页 展示. */
	@ApiModelProperty(value="性图片list(json格式),用于商品详情页 展示")
	private String propValueImgListJson;
	
	/** 商品图片. */
	@ApiModelProperty(value="商品图片")
	private List<ImgFile> imgFileList;
	
	/** 商品属性图片. */
	@ApiModelProperty(value="商品属性图片")
	List<ProdPropImageDto> propImageDtoList;
	
	/** 商品属性图片. */
	@ApiModelProperty(value="商品属性图片")
	List<PropValueImgDto> propValueImgList;
	
	/** 运费模板ID *. */
	@ApiModelProperty(value="运费模板ID")
	protected Long transportId;

	/** 运费模板ID *. */
	@ApiModelProperty(value="运费模板ID")
	private Integer supportTransportFree;
	
	/** 配送类型 *. */
	@ApiModelProperty(value="配送类型")
	private Integer transportType;
	
	/** ems费用 *. */
	@ApiModelProperty(value="ems费用")
	private Double emsTransFee;
	
	/** The express trans fee. */
	@ApiModelProperty(value="快递费用")
	private Double expressTransFee;
	
	/** The mail trans fee. */
	@ApiModelProperty(value="邮政费用")
	private Double mailTransFee;


	/** 商品视频地址  */
	@ApiModelProperty(value="商品视频地址")
	protected String proVideoUrl;
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * Gets the model id.
	 *
	 * @return the model id
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * Sets the model id.
	 *
	 * @param modelId the model id
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the brief.
	 *
	 * @return the brief
	 */
	public String getBrief() {
		return brief;
	}

	/**
	 * Sets the brief.
	 *
	 * @param brief the brief
	 */
	public void setBrief(String brief) {
		this.brief = brief;
	}

	/**
	 * Gets the cash.
	 *
	 * @return the cash
	 */
	public Double getCash() {
		return cash;
	}

	/**
	 * Sets the cash.
	 *
	 * @param cash the cash
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 *
	 * @param content the content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	/**
	 * Gets the prod prop dto list.
	 *
	 * @return the prod prop dto list
	 */
	public List<ProductPropertyDto> getProdPropDtoList() {
		return prodPropDtoList;
	}

	/**
	 * Sets the prod prop dto list.
	 *
	 * @param prodPropDtoList the prod prop dto list
	 */
	public void setProdPropDtoList(List<ProductPropertyDto> prodPropDtoList) {
		this.prodPropDtoList = prodPropDtoList;
	}

	/**
	 * Gets the prod pics.
	 *
	 * @return the prod pics
	 */
	public List<String> getProdPics() {
		return prodPics;
	}

	/**
	 * Sets the prod pics.
	 *
	 * @param prodPics the prod pics
	 */
	public void setProdPics(List<String> prodPics) {
		this.prodPics = prodPics;
	}

	/**
	 * Gets the sku dto list.
	 *
	 * @return the sku dto list
	 */
	public List<SeckillSkuDto> getSkuDtoList() {
		return skuDtoList;
	}

	/**
	 * Sets the sku dto list.
	 *
	 * @param skuDtoList the sku dto list
	 */
	public void setSkuDtoList(List<SeckillSkuDto> skuDtoList) {
		this.skuDtoList = skuDtoList;
	}

	/**
	 * Gets the sku dto list json.
	 *
	 * @return the sku dto list json
	 */
	public String getSkuDtoListJson() {
		return skuDtoListJson;
	}

	/**
	 * Sets the sku dto list json.
	 *
	 * @param skuDtoListJson the sku dto list json
	 */
	public void setSkuDtoListJson(String skuDtoListJson) {
		this.skuDtoListJson = skuDtoListJson;
	}

	/**
	 * Gets the property image dto map.
	 *
	 * @return the property image dto map
	 */
	public Map<PropertyImageId, List<PropertyImageDto>> getPropertyImageDtoMap() {
		return propertyImageDtoMap;
	}

	/**
	 * Sets the property image dto map.
	 *
	 * @param propertyImageDtoMap the property image dto map
	 */
	public void setPropertyImageDtoMap(
			Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap) {
		this.propertyImageDtoMap = propertyImageDtoMap;
	}

	/**
	 * Gets the prop value img list json.
	 *
	 * @return the prop value img list json
	 */
	public String getPropValueImgListJson() {
		return propValueImgListJson;
	}

	/**
	 * Sets the prop value img list json.
	 *
	 * @param propValueImgListJson the prop value img list json
	 */
	public void setPropValueImgListJson(String propValueImgListJson) {
		this.propValueImgListJson = propValueImgListJson;
	}

	/**
	 * Gets the img file list.
	 *
	 * @return the img file list
	 */
	public List<ImgFile> getImgFileList() {
		return imgFileList;
	}

	/**
	 * Sets the img file list.
	 *
	 * @param imgFileList the img file list
	 */
	public void setImgFileList(List<ImgFile> imgFileList) {
		this.imgFileList = imgFileList;
	}

	/**
	 * Gets the prop image dto list.
	 *
	 * @return the prop image dto list
	 */
	public List<ProdPropImageDto> getPropImageDtoList() {
		return propImageDtoList;
	}

	/**
	 * Sets the prop image dto list.
	 *
	 * @param propImageDtoList the prop image dto list
	 */
	public void setPropImageDtoList(List<ProdPropImageDto> propImageDtoList) {
		this.propImageDtoList = propImageDtoList;
	}

	/**
	 * Gets the prop value img list.
	 *
	 * @return the prop value img list
	 */
	public List<PropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	/**
	 * Sets the prop value img list.
	 *
	 * @param propValueImgList the prop value img list
	 */
	public void setPropValueImgList(List<PropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	/**
	 * Gets the transport id.
	 *
	 * @return the transport id
	 */
	public Long getTransportId() {
		return transportId;
	}

	/**
	 * Sets the transport id.
	 *
	 * @param transportId the transport id
	 */
	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	/**
	 * Gets the support transport free.
	 *
	 * @return the support transport free
	 */
	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	/**
	 * Sets the support transport free.
	 *
	 * @param supportTransportFree the support transport free
	 */
	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	/**
	 * Gets the transport type.
	 *
	 * @return the transport type
	 */
	public Integer getTransportType() {
		return transportType;
	}

	/**
	 * Sets the transport type.
	 *
	 * @param transportType the transport type
	 */
	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	/**
	 * Gets the ems trans fee.
	 *
	 * @return the ems trans fee
	 */
	public Double getEmsTransFee() {
		return emsTransFee;
	}

	/**
	 * Sets the ems trans fee.
	 *
	 * @param emsTransFee the ems trans fee
	 */
	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	/**
	 * Gets the express trans fee.
	 *
	 * @return the express trans fee
	 */
	public Double getExpressTransFee() {
		return expressTransFee;
	}

	/**
	 * Sets the express trans fee.
	 *
	 * @param expressTransFee the express trans fee
	 */
	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	/**
	 * Gets the mail trans fee.
	 *
	 * @return the mail trans fee
	 */
	public Double getMailTransFee() {
		return mailTransFee;
	}

	/**
	 * Sets the mail trans fee.
	 *
	 * @param mailTransFee the mail trans fee
	 */
	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	/**
	 * Gets the seck price.
	 *
	 * @return the seck price
	 */
	public Double getSeckPrice() {
		return seckPrice;
	}

	/**
	 * Sets the seck price.
	 *
	 * @param seckPrice the seck price
	 */
	public void setSeckPrice(Double seckPrice) {
		this.seckPrice = seckPrice;
	}

	/**
	 * Gets the seck stock.
	 *
	 * @return the seck stock
	 */
	public int getSeckStock() {
		return seckStock;
	}

	/**
	 * Sets the seck stock.
	 *
	 * @param seckStock the seck stock
	 */
	public void setSeckStock(int seckStock) {
		this.seckStock = seckStock;
	}

	/**
	 * Gets the category id.
	 *
	 * @return the category id
	 */
	public Long getCategoryId() {
		return categoryId;
	}

	/**
	 * Sets the category id.
	 *
	 * @param categoryId the category id
	 */
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}
}
