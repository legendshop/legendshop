package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.support.GenericEntity;

/**
 *商家权限表
 */
@Entity
@Table(name = "ls_shop_perm")
public class ShopPerm implements GenericEntity<ShopPermId> {
	
	private static final long serialVersionUID = 4817875758555794650L;
	
	/** The id. */
	private ShopPermId id;

	/**
	 * default constructor.
	 */
	public ShopPerm() {
	}

	/**
	 * full constructor.
	 * 
	 * @param id
	 *            the id
	 */
	public ShopPerm(ShopPermId id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public ShopPermId getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(ShopPermId id) {
		this.id = id;
	}

} 
