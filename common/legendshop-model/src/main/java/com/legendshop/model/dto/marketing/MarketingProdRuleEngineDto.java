package com.legendshop.model.dto.marketing;

import java.util.List;

/**
 * 促销商品
 * @author Tony
 *
 */
public class MarketingProdRuleEngineDto {
	
	/**
	 * 商品id
	 */
	private Long prodId;
	
	/**
	 * 商家id
	 */
	private Long shopId;
	
	
    private List<MarketingSkuRuleEngineDto> engineParams;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	public List<MarketingSkuRuleEngineDto> getEngineParams() {
		return engineParams;
	}

	public void setEngineParams(List<MarketingSkuRuleEngineDto> engineParams) {
		this.engineParams = engineParams;
	}

}
