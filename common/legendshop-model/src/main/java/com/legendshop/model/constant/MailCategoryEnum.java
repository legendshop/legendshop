/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 邮件类型
 * 
 * ----------------------------------------------------------------------------.
 */
public enum MailCategoryEnum implements StringEnum {

	/** 广告 */
	ADV("ADV"),
	
	/** 订单 */
	ORD("ORD"),
	
	/** 产品 */
	PRD("PRD"),
	
	/** 注册 */
	REG("REG"),
	
	/** 密码 */
	PWD("PWD"),
	
	/** 验证 */
	VAL("VAL"),
	
	/** 提现 */
	TIX("TIX"),
	
	/** 礼券 */
	COUPON("CPN"),
	
	INFORM("INF"),
	
	/**系统*/
	SYSTEM("SYSTEM")
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private MailCategoryEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		MailCategoryEnum[] licenseEnums = values();
		for (MailCategoryEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		MailCategoryEnum[] licenseEnums = values();
		for (MailCategoryEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
