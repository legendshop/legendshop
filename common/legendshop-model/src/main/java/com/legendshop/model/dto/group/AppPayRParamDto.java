/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.group;

import java.io.Serializable;

/**
 * 提交团购订单返回给App pay的参数
 */
public class AppPayRParamDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8100570746654940121L;

	/** 订单Id **/
	private String subIds;

	/** 订单流水号字符串 **/
	private String subNumberStr;

	/** 订单支付金额 **/
	private Double totalAmount;

	/** 可用余额 **/
	private Double availablePredeposit;

	/** 商品描述 **/
	private String subjects;

	/** 支付宝密钥 **/
	private String payCode;

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getSubIds() {
		return subIds;
	}

	public void setSubIds(String subIds) {
		this.subIds = subIds;
	}

	public String getSubNumberStr() {
		return subNumberStr;
	}

	public void setSubNumberStr(String subNumberStr) {
		this.subNumberStr = subNumberStr;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

}
