/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商家首页装修 布局模块类型
 * @author tony
 *
 */
public enum ShopLayoutModuleTypeEnum implements StringEnum {
	/**  
	 * 导航模块
	 **/
	LOYOUT_MODULE_NAV("loyout_module_nav"),
	
	/**  
	 * 幻灯片模块
	 **/
	LOYOUT_MODULE_BANNER("loyout_module_banner"),
	

	/**  
	 *分类模块
	 **/
	LOYOUT_MODULE_CATEGORY("loyout_module_category"),
	
	/**  
	 *热销模块
	 **/
	LOYOUT_MODULE_HOT_PROD("loyout_module_hot_prod"),
	
	
	/**  
	 *店铺模块
	 **/
	LOYOUT_MODULE_SHOPINFO("loyout_module_shopinfo"),
	
	
	/**  
	 *自定义商品
	 **/
	LOYOUT_MODULE_CUSTOM_PROD("loyout_module_custom_prod"),
	
	
	/**  
	 *热点模块
	 **/
	LOYOUT_MODULE_HOT("loyout_module_hot"),
	
	/**  
	 *自定义模块
	 **/
	LOYOUT_MODULE_CUSTOM_DESC("loyout_module_custom_desc");
	
	
	
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private ShopLayoutModuleTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
