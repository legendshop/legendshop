package com.legendshop.model.report;

import java.io.Serializable;

/**
 *订单转化率
 */
public class AverageOrderAmouont implements Serializable {

	private static final long serialVersionUID = 1133213159946259747L;

	// 总访问次数 
	private long totalViews;
	
	//总订单数
	private long totalOrder;
	
	//订单转化率
	private Double averageRate;
	
	/**
	 * 订单转化率
	 * @param totalViews
	 * @param totalOrder
	 * @param averageRate
	 */
	public AverageOrderAmouont(long totalViews, long totalOrder, double averageRate) {
		this.totalViews = totalViews;
		this.totalOrder = totalOrder;
		this.averageRate = averageRate;
	}


	public void setTotalOrder(Integer totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Double getAverageRate() {
		return averageRate;
	}

	public void setAverageRate(Double averageRate) {
		this.averageRate = averageRate;
	}


	public long getTotalViews() {
		return totalViews;
	}


	public void setTotalViews(long totalViews) {
		this.totalViews = totalViews;
	}


	public long getTotalOrder() {
		return totalOrder;
	}


	public void setTotalOrder(long totalOrder) {
		this.totalOrder = totalOrder;
	}
	

}
