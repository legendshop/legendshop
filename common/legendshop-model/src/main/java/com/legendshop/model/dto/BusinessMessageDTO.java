package com.legendshop.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@ApiModel(value = "营业执照信息")
public class BusinessMessageDTO {

	/**
	 * 营业时间.
	 */
	@ApiModelProperty(value = "营业时间")
	private String businessHours;

	/**
	 * 企业名称.
	 */
	@ApiModelProperty(value = "企业名称")
	private String enterpriseName;

	/**
	 * 营业执照注册号.
	 */
	@ApiModelProperty(value = "营业执照注册号")
	private String registerNumber;

	/**
	 * 法定代表人姓名.
	 */
	@ApiModelProperty(value = "法定代表人姓名")
	private String representative;

	/**
	 * 营业执照所在地.
	 */
	@ApiModelProperty(value = "营业执照所在地")
	private String businessAddress;

	/**
	 * 企业注册资金.
	 */
	@ApiModelProperty(value = "企业注册资金")
	private String registerAmount;

	/**
	 * 营业执照有效期.
	 */
	@ApiModelProperty(value = "营业执照有效期")
	private String availableTime;

	/**
	 * 营业执照经营范围.
	 */
	@ApiModelProperty(value = "营业执照经营范围")
	private String businessRange;

	/**
	 * 营业执照图片.
	 */
	@ApiModelProperty(value = "执照图片集合")
	private List<String> imageList;

	/**
	 * 店铺类型：1商家 0个人
	 */
	@ApiModelProperty(value = "店铺类型")
	private Integer type;

	/**
	 * 营业执照图片未分割字符串.
	 */
	private String businessImage;

}
