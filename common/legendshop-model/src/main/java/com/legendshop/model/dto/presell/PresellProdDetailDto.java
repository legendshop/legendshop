/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.entity.Sku;
import org.springframework.cache.annotation.Cacheable;

import com.legendshop.dao.persistence.Transient;

/**
 * 预售商品详情Dto
 */
public class PresellProdDetailDto implements Serializable {

	private static final long serialVersionUID = -6764248157402611306L;

	/** 预售ID */
	private Long id;

	/** 商品id */
	private Long prodId;

	/** skuId */
	private Long skuId;

	/** 预售价格 */
	private Double prePrice;

	/** 订金金额 */
	private Double preDepositPrice;

	/** 尾款 */
	private Double finalPayment;

	/** 预售开始时间 */
	private Date preSaleStart;

	/** 预售结束时间 */
	private Date preSaleEnd;

	/** 预售支付方式 ,0:全额,1:订金 */
	private Integer payPctType;

	/** 订金占百分比 */
	private Double payPct;

	/** 预售状态 */
	private Long status;

	/** 尾款支付开始时间 */
	private Date finalMStart;

	/** 尾款支付结束时间 */
	private Date finalMEnd;

	/** 预售发货时间 */
	private Date preDeliveryTime;

	// ----------- 商品信息 ------------//
	/** 商品编号 */
	private String modelId;

	/** 商品名称 */
	private String prodName;

	/** 商品所属全局分类 */
	private Long categoryId;

	/** 商品拥有者 */
	private String userName;

	/** 商家id */
	private Long shopId;

	/** 关键字 */
	private String keyWord;

	/** 简要描述,卖点等 */
	private String brief;

	/** 商品SEOTitle */
	private String metaTitle;

	/** 商品原价 */
	private Double prodPrice;

	/** 商品现价 */
	private Double prodCash;

	/** 商品主图 */
	private String prodMainPic;

	/** 商品数量 */
	private Integer prodStocks;

	/** 商品条形码 */
	private String partyCode;

	/** pc商品详情描述 */
	private String content;

	/***mobile商品详情描述***/
	private String contentM;

	/** 商品状态 */

	/** 售后服务ID */
	private Long afterSaleId;

	/** 商品所有图片 */
	private List<String> prodPics;

	/** 商品实体类*/
	private PresellProdNewDto prodDto;

	// ---------- SKU 信息 -----------//

	/** sku名称 */
	private String skuName;

	/** sku价格 */
	private Double skuPrice;

	/** sku主图 */
	private String skuPic;

	/** sku属性 */
	private String properties;

	/** 属性键值对 */
	private Map<String, Long> propKeyValues;

	/** 属性id */
	private List<Long> propIds;

	/** 属性值id */
	private List<Long> propValueIds;

	/** sku状态 */
	private Integer skuStatus;

	/** sku 库存 */
	private Long skuStocks;

	/** sku实际库存 */
	private Long skuActualStocks;

	/** sku属性集合 */
	private List<ProdPropDto> prodPropDtos;

	/** sku属性值的所有图片URL */
	private List<String> propValImageUrls = new ArrayList<String>();

	// ----------- 运费 -----------------//
	/** 运费模板ID **/
	protected Long transportId;

	/** 商家是否承担运费 0:买家承担,1:卖家承担 */
	private Integer supportTransportFree;

	/** 是否固定运费 0:使用运费模板,1:固定运费 */
	private Integer transportType;

	/** ems运费 */
	private Double emsTransFee;

	/** express运费 */
	private Double expressTransFee;

	/** 包邮运费 */
	private Double mailTransFee;

	/** 物流体积 */
	private Double volume;

	/** 物流重量 */
	private Double weight;

	/** 商品关联集合 */
	private List<Sku> skuList;



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prodId
	 */
	public Long getProdId() {
		return prodId;
	}

	/**
	 * @param prodId
	 *            the prodId to set
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	/**
	 * @return the skuId
	 */
	public Long getSkuId() {
		return skuId;
	}

	/**
	 * @param skuId
	 *            the skuId to set
	 */
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
	 * @return the prePrice
	 */
	public Double getPrePrice() {
		return prePrice;
	}

	/**
	 * @param prePrice
	 *            the prePrice to set
	 */
	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	/**
	 * @return the preDepositPrice
	 */
	public Double getPreDepositPrice() {
		return preDepositPrice;
	}

	/**
	 * @param preDepositPrice
	 *            the preDepositPrice to set
	 */
	public void setPreDepositPrice(Double preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	/**
	 * @return the finalPayment
	 */
	public Double getFinalPayment() {
		return finalPayment;
	}

	/**
	 * @param finalPayment
	 *            the finalPayment to set
	 */
	public void setFinalPayment(Double finalPayment) {
		this.finalPayment = finalPayment;
	}

	/**
	 * @return the preSaleStart
	 */
	public Date getPreSaleStart() {
		return preSaleStart;
	}

	/**
	 * @param preSaleStart
	 *            the preSaleStart to set
	 */
	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	/**
	 * @return the preSaleEnd
	 */
	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	/**
	 * @param preSaleEnd
	 *            the preSaleEnd to set
	 */
	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}

	/**
	 * @return the payPctType
	 */
	public Integer getPayPctType() {
		return payPctType;
	}

	/**
	 * @param payPctType
	 *            the payPctType to set
	 */
	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	/**
	 * @return the payPct
	 */
	public Double getPayPct() {
		return payPct;
	}

	/**
	 * @param payPct
	 *            the payPct to set
	 */
	public void setPayPct(Double payPct) {
		this.payPct = payPct;
	}

	/**
	 * @return the status
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return the modelId
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * @param modelId
	 *            the modelId to set
	 */
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName
	 *            the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the categoryId
	 */
	public Long getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * @param shopId
	 *            the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * @return the keyWord
	 */
	public String getKeyWord() {
		return keyWord;
	}

	/**
	 * @param keyWord
	 *            the keyWord to set
	 */
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	/**
	 * @return the brief
	 */
	public String getBrief() {
		return brief;
	}

	/**
	 * @param brief
	 *            the brief to set
	 */
	public void setBrief(String brief) {
		this.brief = brief;
	}

	/**
	 * @return the prodPrice
	 */
	public Double getProdPrice() {
		return prodPrice;
	}

	/**
	 * @param prodPrice
	 *            the prodPrice to set
	 */
	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	/**
	 * @return the prodCash
	 */
	public Double getProdCash() {
		return prodCash;
	}

	/**
	 * @param prodCash
	 *            the prodCash to set
	 */
	public void setProdCash(Double prodCash) {
		this.prodCash = prodCash;
	}

	/**
	 * @return the prodMainPic
	 */
	public String getProdMainPic() {
		return prodMainPic;
	}

	/**
	 * @param prodMainPic
	 *            the prodMainPic to set
	 */
	public void setProdMainPic(String prodMainPic) {
		this.prodMainPic = prodMainPic;
	}

	/**
	 * @return the prodStocks
	 */
	public Integer getProdStocks() {
		return prodStocks;
	}

	/**
	 * @param prodStocks
	 *            the prodStocks to set
	 */
	public void setProdStocks(Integer prodStocks) {
		this.prodStocks = prodStocks;
	}

	/**
	 * @return the partyCode
	 */
	public String getPartyCode() {
		return partyCode;
	}

	/**
	 * @param partyCode
	 *            the partyCode to set
	 */
	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	/**
	 * @return the prodPics
	 */
	public List<String> getProdPics() {
		return prodPics;
	}

	/**
	 * @param prodPics
	 *            the prodPics to set
	 */
	public void setProdPics(List<String> prodPics) {
		this.prodPics = prodPics;
	}

	/**
	 * @return the afterSaleId
	 */
	public Long getAfterSaleId() {
		return afterSaleId;
	}

	/**
	 * @param afterSaleId
	 *            the afterSaleId to set
	 */
	public void setAfterSaleId(Long afterSaleId) {
		this.afterSaleId = afterSaleId;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the skuName
	 */
	public String getSkuName() {
		return skuName;
	}

	/**
	 * @param skuName
	 *            the skuName to set
	 */
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	/**
	 * @return the skuPrice
	 */
	public Double getSkuPrice() {
		return skuPrice;
	}

	/**
	 * @param skuPrice
	 *            the skuPrice to set
	 */
	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	/**
	 * @return the skuPic
	 */
	public String getSkuPic() {
		return skuPic;
	}

	/**
	 * @param skuPic
	 *            the skuPic to set
	 */
	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	/**
	 * @return the properties
	 */
	public String getProperties() {
		return properties;
	}

	/**
	 * @param properties
	 *            the properties to set
	 */
	public void setProperties(String properties) {
		this.properties = properties;
	}

	/**
	 * @return the propIds
	 */
	@Cacheable(value = "PropIds")
	@Transient
	public List<Long> getPropIds() {
		if (null == this.propIds && this.getPropKeyValues() != null) {
			Set<Entry<String, Long>> propKeyValues = this.getPropKeyValues().entrySet();
			Iterator<Entry<String, Long>> iterator = propKeyValues.iterator();

			this.propIds = new ArrayList<Long>();
			while (iterator.hasNext()) {
				Entry<String, Long> entry = iterator.next();
				this.propIds.add(Long.valueOf(entry.getKey()));
			}
		}
		return this.propIds;
	}

	@Cacheable(value = "PropValueIds")
	@Transient
	public List<Long> getPropValueIds() {
		if (null == this.propValueIds && this.getPropKeyValues() != null) {
			Set<Entry<String, Long>> propKeyValues = this.getPropKeyValues().entrySet();
			Iterator<Entry<String, Long>> iterator = propKeyValues.iterator();

			this.propValueIds = new ArrayList<Long>();
			while (iterator.hasNext()) {
				Entry<String, Long> entry = iterator.next();
				this.propValueIds.add(entry.getValue());
			}
		}
		return this.propValueIds;
	}

	/**
	 * @return the props
	 */
	@Cacheable(value = "PropKeyValues")
	@Transient
	public Map<String, Long> getPropKeyValues() {
		if (null == this.propKeyValues) {
			if (null != this.properties && !"".equals(this.properties)) {
				propKeyValues = new HashMap<String, Long>();
				String[] propKeyValueStrs = this.properties.split(";");
				for (String propKeyValue : propKeyValueStrs) {
					String[] KeyValues = propKeyValue.split(":");
					String key = KeyValues[0];
					Long value = Long.valueOf(KeyValues[1]);
					propKeyValues.put(key, value);
				}
			}
		}
		return this.propKeyValues;
	}

	/**
	 * @return the skuStatus
	 */
	public Integer getSkuStatus() {
		return skuStatus;
	}

	/**
	 * @param skuStatus
	 *            the skuStatus to set
	 */
	public void setSkuStatus(Integer skuStatus) {
		this.skuStatus = skuStatus;
	}

	/**
	 * @return the skuStocks
	 */
	public Long getSkuStocks() {
		return skuStocks;
	}

	/**
	 * @param skuStocks
	 *            the skuStocks to set
	 */
	public void setSkuStocks(Long skuStocks) {
		this.skuStocks = skuStocks;
	}

	/**
	 * @return the skuActualStocks
	 */
	public Long getSkuActualStocks() {
		return skuActualStocks;
	}

	/**
	 * @param skuActualStocks
	 *            the skuActualStocks to set
	 */
	public void setSkuActualStocks(Long skuActualStocks) {
		this.skuActualStocks = skuActualStocks;
	}

	/**
	 * @return the prodPropDtos
	 */
	public List<ProdPropDto> getProdPropDtos() {
		return prodPropDtos;
	}

	/**
	 * @param prodPropDtos
	 *            the prodPropDtos to set
	 */
	public void setProdPropDtos(List<ProdPropDto> prodPropDtos) {
		this.prodPropDtos = prodPropDtos;
	}

	/**
	 * @return the propImageUrls
	 */
	@Transient
	public List<String> getPropValImageUrls() {
		return propValImageUrls;
	}

	/**
	 * @return the transportId
	 */
	public Long getTransportId() {
		return transportId;
	}

	/**
	 * @param transportId
	 *            the transportId to set
	 */
	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	/**
	 * @return the supportTransportFree
	 */
	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	/**
	 * @param supportTransportFree
	 *            the supportTransportFree to set
	 */
	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	/**
	 * @return the transportType
	 */
	public Integer getTransportType() {
		return transportType;
	}

	/**
	 * @param transportType
	 *            the transportType to set
	 */
	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	/**
	 * @return the emsTransFee
	 */
	public Double getEmsTransFee() {
		return emsTransFee;
	}

	/**
	 * @param emsTransFee
	 *            the emsTransFee to set
	 */
	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	/**
	 * @return the expressTransFee
	 */
	public Double getExpressTransFee() {
		return expressTransFee;
	}

	/**
	 * @param expressTransFee
	 *            the expressTransFee to set
	 */
	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	/**
	 * @return the mailTransFee
	 */
	public Double getMailTransFee() {
		return mailTransFee;
	}

	/**
	 * @param mailTransFee
	 *            the mailTransFee to set
	 */
	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the metaTitle
	 */
	public String getMetaTitle() {
		return metaTitle;
	}

	/**
	 * @param metaTitle
	 *            the metaTitle to set
	 */
	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	/**
	 * @return the volume
	 */
	public Double getVolume() {
		return volume;
	}

	/**
	 * @param volume
	 *            the volume to set
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * @return the weight
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	public void setPropKeyValues(Map<String, Long> propKeyValues) {
		this.propKeyValues = propKeyValues;
	}

	public void setPropIds(List<Long> propIds) {
		this.propIds = propIds;
	}

	public void setPropValueIds(List<Long> propValueIds) {
		this.propValueIds = propValueIds;
	}

	public void setPropValImageUrls(List<String> propValImageUrls) {
		this.propValImageUrls = propValImageUrls;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public Date getPreDeliveryTime() {
		return preDeliveryTime;
	}

	public void setPreDeliveryTime(Date preDeliveryTime) {
		this.preDeliveryTime = preDeliveryTime;
	}

	public List<Sku> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<Sku> skuList) {
		this.skuList = skuList;
	}

	public PresellProdNewDto getProdDto() {
		return prodDto;
	}

	public void setProdDto(PresellProdNewDto prodDto) {
		this.prodDto = prodDto;
	}
}
