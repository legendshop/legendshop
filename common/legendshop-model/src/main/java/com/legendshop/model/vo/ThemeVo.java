package com.legendshop.model.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.legendshop.model.entity.Theme;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.model.entity.ThemeRelated;

/**
 * 主题
 *
 */
public class ThemeVo  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 专题
	 */
	private Theme theme;
	
	/**
	 * 专题模块
	 */
	private Map<ThemeModule, List<ThemeModuleProd>> moduleProdMap;
	
	
	/**
	 * 相关专题
	 */
	private List<ThemeRelated> themeRelateds;


	public Theme getTheme() {
		return theme;
	}


	public void setTheme(Theme theme) {
		this.theme = theme;
	}


	public Map<ThemeModule, List<ThemeModuleProd>> getModuleProdMap() {
		return moduleProdMap;
	}


	public void setModuleProdMap(Map<ThemeModule, List<ThemeModuleProd>> moduleProdMap) {
		this.moduleProdMap = moduleProdMap;
	}


	public List<ThemeRelated> getThemeRelateds() {
		return themeRelateds;
	}


	public void setThemeRelateds(List<ThemeRelated> themeRelateds) {
		this.themeRelateds = themeRelateds;
	}
	
	
}
