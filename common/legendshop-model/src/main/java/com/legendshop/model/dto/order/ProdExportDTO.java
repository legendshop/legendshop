package com.legendshop.model.dto.order;

import com.legendshop.model.dto.SkuExportDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品导出dto
 * 
 * @author Zhongweihan
 * 
 */
public class ProdExportDTO implements Serializable {

	/** 商品名字 */
	private String prodName;

	/** 平台分类 */
    private String categroyId;

	/** 店铺分类 */
    private String shopCategroy;

    /** 参数项1 */
    private String firstParameter;

    /** 参数值1 */
    private String firstParameterValue;

    /** 参数项2 */
    private String secondParameter;

    /** 参数值2 */
    private String secondParameterValue;

    /** 参数项3 */
    private String thirdParameter;

    /** 参数值3 */
    private String thirdParameterValue;

    /** 参数项4 */
    private String fourthParameter;

    /** 参数值4 */
    private String fourthParameterValue;


	/** spu售价 */
	private String spuPrice;

	/** 支付方式 */
	private String payType;

    /** 图片链接 */
    private String picture;

    /** 详细介绍(电脑版) */
    private String content;

    /** skuList */
    private List<SkuExportDTO> skuList = new ArrayList<SkuExportDTO>();

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getCategroyId() {
        return categroyId;
    }

    public void setCategroyId(String categroyId) {
        this.categroyId = categroyId;
    }

    public String getShopCategroy() {
        return shopCategroy;
    }

    public void setShopCategroy(String shopCategroy) {
        this.shopCategroy = shopCategroy;
    }

    public String getFirstParameter() {
        return firstParameter;
    }

    public void setFirstParameter(String firstParameter) {
        this.firstParameter = firstParameter;
    }

    public String getFirstParameterValue() {
        return firstParameterValue;
    }

    public void setFirstParameterValue(String firstParameterValue) {
        this.firstParameterValue = firstParameterValue;
    }

    public String getSecondParameter() {
        return secondParameter;
    }

    public void setSecondParameter(String secondParameter) {
        this.secondParameter = secondParameter;
    }

    public String getSecondParameterValue() {
        return secondParameterValue;
    }

    public void setSecondParameterValue(String secondParameterValue) {
        this.secondParameterValue = secondParameterValue;
    }

    public String getThirdParameter() {
        return thirdParameter;
    }

    public void setThirdParameter(String thirdParameter) {
        this.thirdParameter = thirdParameter;
    }

    public String getThirdParameterValue() {
        return thirdParameterValue;
    }

    public void setThirdParameterValue(String thirdParameterValue) {
        this.thirdParameterValue = thirdParameterValue;
    }

    public String getFourthParameter() {
        return fourthParameter;
    }

    public void setFourthParameter(String fourthParameter) {
        this.fourthParameter = fourthParameter;
    }

    public String getFourthParameterValue() {
        return fourthParameterValue;
    }

    public void setFourthParameterValue(String fourthParameterValue) {
        this.fourthParameterValue = fourthParameterValue;
    }

    public String getSpuPrice() {
        return spuPrice;
    }

    public void setSpuPrice(String spuPrice) {
        this.spuPrice = spuPrice;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<SkuExportDTO> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<SkuExportDTO> skuList) {
        this.skuList = skuList;
    }

}
