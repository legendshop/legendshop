package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum  DashboardRecDateEnum implements StringEnum{

	/** 用户表名  **/
	USER_TABLE("user_regtime"),
	
	/** 商家表名 **/
	SHOP_TABLE("rec_date"),
	
	/** 商品表名**/
	PROD_TABLE("rec_date"),
	
	/** 订单表名 **/
	SUB_TABLE("sub_date"),
	
	/** 咨询表名 **/
	CONSULT_TABLE("rec_date"),
	
	/** 举报表名 **/
	ACCUSATION_TABLE("rec_date"),
	
	/** 品牌表名 **/
	BRAND_TABLE("");
	
	private final String value;

	private DashboardRecDateEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
