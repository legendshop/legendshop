package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * 拼团活动选择商品
 */
public class MergeGroupSelProdDto implements Serializable{

	private static final long serialVersionUID = -8414979356767710211L;

	// 产品ID
	private Long prodId ; 
	
	//产品名称
	private String prodName;
	
	//商品价格
	private Double price;
	
	//商品现价
	private Double cash;

	//产品图片
	private String prodPic;
	
	//商品库存
	private Long stocks; 
	
	// 实际库存
	private Long actualStocks; 
	
	// skuId
	private Long skuId; 
		
	//sku属性
	private String cnProperties;	
	
	//商品sku价格
	private Double skuPrice;
	
	//商品库存
	private Long skuStocks; 
	
	// 实际库存
	private Long skuActualStocks; 
		
	// Sku名称
	private String skuName;	
		
	//商家编码
	private String partyCode;
	
	//SKU图片
	private String skuPic;
	
	List<MergeGroupSelProdDto> dtoList = new ArrayList<MergeGroupSelProdDto>();
	
	//拼团价，用于查看拼团活动详情
	private Double mergePrice;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public Long getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Long actualStocks) {
		this.actualStocks = actualStocks;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Double getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	public Long getSkuStocks() {
		return skuStocks;
	}

	public void setSkuStocks(Long skuStocks) {
		this.skuStocks = skuStocks;
	}

	public Long getSkuActualStocks() {
		return skuActualStocks;
	}

	public void setSkuActualStocks(Long skuActualStocks) {
		this.skuActualStocks = skuActualStocks;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getSkuPic() {
		return skuPic;
	}

	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	public List<MergeGroupSelProdDto> getDtoList() {
		return dtoList;
	}

	public void setDtoList(List<MergeGroupSelProdDto> dtoList) {
		this.dtoList = dtoList;
	}
	
	public void addMergeGroupSelProdDto(MergeGroupSelProdDto dto) {
		dtoList.add(dto);
	}

	public Double getMergePrice() {
		return mergePrice;
	}

	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}
}
