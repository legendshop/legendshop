package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_seckill_success")
public class SeckillSuccess implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/**  */
	private String userId; 
		
	/**  */
	private Long seckillId; 
		
	/**  */
	private Long prodId; 
		
	/**  */
	private Long skuId; 
		
	/**  */
	private Integer seckillNum; 
		
	/**  */
	private Double seckillPrice; 
		
	/** 创建的时间 */
	private java.sql.Timestamp seckillTime; 
		
	/** 用户的状态默认0 0成功状态 1:已下单 */
	private Integer status; 
	
	private int result;
	
	
	public SeckillSuccess() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SECKILL_SUCCESS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "seckill_id")
	public Long  getSeckillId(){
		return seckillId;
	} 
		
	public void setSeckillId(Long seckillId){
			this.seckillId = seckillId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "seckill_num")
	public Integer  getSeckillNum(){
		return seckillNum;
	} 
		
	public void setSeckillNum(Integer seckillNum){
			this.seckillNum = seckillNum;
		}
		
    @Column(name = "seckill_price")
	public Double  getSeckillPrice(){
		return seckillPrice;
	} 
		
	public void setSeckillPrice(Double seckillPrice){
			this.seckillPrice = seckillPrice;
		}
		
    @Column(name = "seckill_time")
	public java.sql.Timestamp  getSeckillTime(){
		return seckillTime;
	} 
		
	public void setSeckillTime(java.sql.Timestamp seckillTime){
			this.seckillTime = seckillTime;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}

	@Transient
	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

} 
