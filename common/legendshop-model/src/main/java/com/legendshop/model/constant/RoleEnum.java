/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 系统角色,不可以在页面上进行修改
 */
public enum RoleEnum implements StringEnum {

	//超级管理员
	ROLE_SUPERVISOR("1"),

	//管理员
	ROLE_ADMIN("2"),

	//普通用户
	ROLE_USER("3"),
	
	//供应商
	ROLE_SUPPLIER("402882833a362b9b013a363356640002"),
	
	//门店登录角色
	ROLE_STORE("5")
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new role enum.
	 * 
	 * @param value
	 *            the value
	 */
	private RoleEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		RoleEnum[] licenseEnums = values();
		for (RoleEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		RoleEnum[] licenseEnums = values();
		for (RoleEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
