package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 商家子账号权限类型
 * @author Lin
 */
public enum ShopPermFunctionEnum implements StringEnum{
	
	/** 商品详情 */
	CHECK_FUNC("check"),
	
	/** 商品列表 */
	EDITOR_FUNC("editor"),

	;
	
	private final String value;

	private ShopPermFunctionEnum(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return value;
	}
}
