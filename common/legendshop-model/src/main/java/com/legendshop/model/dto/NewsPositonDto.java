
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 文章DTO
 *
 */
public class NewsPositonDto  implements Serializable {

	private static final long serialVersionUID = 8245381133760748434L;

	private Long newsId;
	
	private String newsTitle;

	public NewsPositonDto(Long newsId, String newsTitle) {
		super();
		this.newsId = newsId;
		this.newsTitle = newsTitle;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	
}
