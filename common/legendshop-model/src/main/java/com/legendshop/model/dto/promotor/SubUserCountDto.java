/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import java.io.Serializable;

/**
 * 分销下级用户数量DTO
 *
 */
public class SubUserCountDto implements Serializable{

	private static final long serialVersionUID = 471562742691105059L;

	/** 直接下级用户数量 */
	private String firstCount;
	
	/** 下二级用户数量 */
	private String secondCount;
	
	/** 下三级用户数量 */
	private String thirdCount;

	public String getFirstCount() {
		return firstCount;
	}

	public void setFirstCount(String firstCount) {
		this.firstCount = firstCount;
	}

	public String getSecondCount() {
		return secondCount;
	}

	public void setSecondCount(String secondCount) {
		this.secondCount = secondCount;
	}

	public String getThirdCount() {
		return thirdCount;
	}

	public void setThirdCount(String thirdCount) {
		this.thirdCount = thirdCount;
	}
	
	
	
	
}
