package com.legendshop.model.dto.compare;

import java.io.Serializable;
import java.util.Map;

/**
 * 商品对比Dto
 * 
 */
public class ProductCompareDto implements Serializable {

	private static final long serialVersionUID = 8269196458836894493L;

	private IdNameImgDto [] idNameImg = new IdNameImgDto[4];

	private Double[] price = new Double[4];
	
	private String[] brandName=new String[4];
	
	private String[] categoryName=new String[4];

	private Map<IdNameDto, String[]> paramMap = null;
	
	
	public Map<IdNameDto, String[]> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<IdNameDto, String[]> paramMap) {
		this.paramMap = paramMap;
	}
	
	public Double[] getPrice() {
		return price;
	}


	public IdNameImgDto[] getIdNameImg() {
		return idNameImg;
	}

	public String[] getBrandName() {
		return brandName;
	}

	public void setBrandName(String[] brandName) {
		this.brandName = brandName;
	}

	public String[] getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String[] categoryName) {
		this.categoryName = categoryName;
	}

}
