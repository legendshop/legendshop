/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 支付请求Enum.
 */
public enum PaymentProcessorEnum implements StringEnum {

	/** 支付宝支付. */
	ALP("ALP","支付宝支付"),

	/** 腾讯支付. */
	TDP("TDP","腾讯支付"),
	
	/** 商城模拟支付. */
	SIMULATE_PAY("SIMULATE","商城模拟支付"),
	
	/** 通联支付. */
	TONGLIAN_PAY("TONGLIAN_PAY","通联支付"),
	
	/** 通联跨境支付. */
	TONGLIAN_CROSSBORDER_PAY("TONGLIAN_CROSSBORDER_PAY","通联跨境支付"),
	
	/** 通联移动支付. */
	TONGLIAN_WAP_PAY("TONGLIAN_WAP_PAY","通联移动支付"),
	
	/** 全款支付. */
	FULL_PAY("FULL_PAY","全款支付"),
	
	/** 银联支付. */
	UNION_PAY("UNION_PAY","银联支付"),

	/** 微信扫码支付. */
	WX_SACN_PAY("WX_SACN_PAY","微信扫码支付"),

	/** 微信APP支付. */
	WX_APP_PAY("WX_APP_PAY","微信APP支付"),

	/** 微信壳子混合支付. */
	WX_BLEND_PAY("WX_BLEND_PAY","微信壳子混合支付"),
	
	/** 微信公众号支付. */
	WX_PAY("WX_PAY","微信公众号支付"),

	/** 微信H5支付. */
	WX_H5_PAY("WX_H5_PAY","微信H5支付"),
	
	/** 微信小程序支付 */
	WX_MP_PAY("WX_MP_PAY", "微信小程序支付"),
	;

	/** 描述. */
	private String value;
	
	/** The desc. */
	private final String desc;

	/**
	 * The Constructor.
	 *
	 * @param value the value
	 * @param desc the desc
	 */
	private PaymentProcessorEnum(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.util.constant.StringEnum#value()
	 */
	@Override
	public String value() {
		return value;
	}
	
	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * From code.
	 *
	 * @param code the code
	 * @return the payment processor enum
	 */
	public static PaymentProcessorEnum fromCode(String code) {
		for (PaymentProcessorEnum processorEnum : PaymentProcessorEnum.values()) {
			if (processorEnum.value.equals(code)) {
				return processorEnum;
			}
		}
		return null;
	}
	
}
