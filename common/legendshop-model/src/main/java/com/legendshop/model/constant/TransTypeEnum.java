package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum TransTypeEnum implements StringEnum {

	NUMBER("1"), //按件数

	WEIGHT("2"), //按重量

	VOLUME("3");//按体积

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private TransTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
}
