/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.draw;

import java.io.Serializable;

/**
 * 响应消息
 *
 */
public class AjaxJson implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private boolean success = true;// 是否成功
	private String msg = "操作成功";// 提示信息
	private String successValue; //成功的结果信息
	
	private Integer count;//抽奖剩余次数
	private Long awardsRecordId;//中奖对应的记录ID
	/**奖项类型    1：实体   2：预付款    3:积分**/
	private Integer awardsType;
	
	private String token;//密匙
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getAwardsType() {
		return awardsType;
	}

	public void setAwardsType(Integer awardsType) {
		this.awardsType = awardsType;
	}

	public Long getAwardsRecordId() {
		return awardsRecordId;
	}

	public void setAwardsRecordId(Long awardsRecordId) {
		this.awardsRecordId = awardsRecordId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getMsg() {
		return msg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


	
	@Override
	public String toString() {

		StringBuffer sb = new StringBuffer();

		sb.append("AjaxJson [success=").append(success).append(",msg=")
				.append(msg);

		sb.append("]");

		return sb.toString();
	}

	public String getSuccessValue() {
		return successValue;
	}

	public void setSuccessValue(String successValue) {
		this.successValue = successValue;
	}
	
	
	
}
