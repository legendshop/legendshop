/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.io.Serializable;

import com.legendshop.dao.support.GenericEntity;


/**
 * The Class NamedEntity.
 */
public interface NamedEntity<ID extends Serializable> extends GenericEntity<ID> {
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getName();


}
