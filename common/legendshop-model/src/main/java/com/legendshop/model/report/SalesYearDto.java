/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.report;


/**
 * 销售Dto.
 */
public class SalesYearDto extends AbstractSalesDto{

	private static final long serialVersionUID = -7773015508919799326L;
	/** The end date. */
	private int salesMonth;

	/**
	 * Gets the sales month.
	 *
	 * @return the sales month
	 */
	public int getSalesMonth() {
		return salesMonth;
	}

	/**
	 * Sets the sales month.
	 *
	 * @param salesMonth the new sales month
	 */
	public void setSalesMonth(int salesMonth) {
		this.salesMonth = salesMonth;
	}
	
	

}
