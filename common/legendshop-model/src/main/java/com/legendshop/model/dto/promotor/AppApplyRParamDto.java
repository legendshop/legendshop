/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.promotor;

import java.io.Serializable;

/**
 * 申请推广员返回给app端的参数
 *
 */
public class AppApplyRParamDto implements Serializable {

	private static final long serialVersionUID = 562259207837852059L;

	/** 是否需要真实姓名 */
	private Integer chkRealName;

	/** 是否需要邮箱 */
	private Integer chkMail;

	/** 是否需要手机 */
	private Integer chkMobile;

	/** 是否需要住址 */
	private Integer chkAddr;

	/** 真实姓名 */
	private String realName;
	
	/** 用户电话 */
	private String userMobile;
	
	/** 用户邮箱 */
	private String userMail;
	
	/** 用户地址 */
	private String userAdds;
	
	/** 省份 */
	protected Integer provinceid;
	
	/** 城市 */
	protected Integer cityid;
	
	/** 地区 */
	protected Integer areaid;

	/**
	 * Gets the chk real name.
	 *
	 * @return the chk real name
	 */
	public Integer getChkRealName() {
		return chkRealName;
	}

	/**
	 * Sets the chk real name.
	 *
	 * @param chkRealName
	 *            the chk real name
	 */
	public void setChkRealName(Integer chkRealName) {
		this.chkRealName = chkRealName;
	}

	/**
	 * Gets the chk mail.
	 *
	 * @return the chk mail
	 */
	public Integer getChkMail() {
		return chkMail;
	}

	/**
	 * Sets the chk mail.
	 *
	 * @param chkMail
	 *            the chk mail
	 */
	public void setChkMail(Integer chkMail) {
		this.chkMail = chkMail;
	}

	/**
	 * Gets the chk mobile.
	 *
	 * @return the chk mobile
	 */
	public Integer getChkMobile() {
		return chkMobile;
	}

	/**
	 * Sets the chk mobile.
	 *
	 * @param chkMobile
	 *            the chk mobile
	 */
	public void setChkMobile(Integer chkMobile) {
		this.chkMobile = chkMobile;
	}

	/**
	 * Gets the chk addr.
	 *
	 * @return the chk addr
	 */
	public Integer getChkAddr() {
		return chkAddr;
	}

	/**
	 * Sets the chk addr.
	 *
	 * @param chkAddr
	 *            the chk addr
	 */
	public void setChkAddr(Integer chkAddr) {
		this.chkAddr = chkAddr;
	}

	/**
	 * Gets the real name.
	 *
	 * @return the real name
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * Sets the real name.
	 *
	 * @param realName
	 *            the real name
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * Gets the user mobile.
	 *
	 * @return the user mobile
	 */
	public String getUserMobile() {
		return userMobile;
	}

	/**
	 * Sets the user mobile.
	 *
	 * @param userMobile
	 *            the user mobile
	 */
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	/**
	 * Gets the user mail.
	 *
	 * @return the user mail
	 */
	public String getUserMail() {
		return userMail;
	}

	/**
	 * Sets the user mail.
	 *
	 * @param userMail
	 *            the user mail
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	/**
	 * Gets the user adds.
	 *
	 * @return the user adds
	 */
	public String getUserAdds() {
		return userAdds;
	}

	/**
	 * Sets the user adds.
	 *
	 * @param userAdds
	 *            the user adds
	 */
	public void setUserAdds(String userAdds) {
		this.userAdds = userAdds;
	}

	/**
	 * Gets the provinceid.
	 *
	 * @return the provinceid
	 */
	public Integer getProvinceid() {
		return provinceid;
	}

	/**
	 * Sets the provinceid.
	 *
	 * @param provinceid
	 *            the provinceid
	 */
	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	/**
	 * Gets the cityid.
	 *
	 * @return the cityid
	 */
	public Integer getCityid() {
		return cityid;
	}

	/**
	 * Sets the cityid.
	 *
	 * @param cityid
	 *            the cityid
	 */
	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	/**
	 * Gets the areaid.
	 *
	 * @return the areaid
	 */
	public Integer getAreaid() {
		return areaid;
	}

	/**
	 * Sets the areaid.
	 *
	 * @param areaid
	 *            the areaid
	 */
	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

}
