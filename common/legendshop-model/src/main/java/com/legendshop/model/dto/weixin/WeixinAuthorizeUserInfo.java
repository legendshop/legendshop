package com.legendshop.model.dto.weixin;

/**
 * 微信授权用户信息
 * 请参考 : https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842
 * 数据来源于这个接口: https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
 */
public class WeixinAuthorizeUserInfo extends WeiXinMsg{

	/** 用户的唯一标识 */
	private String openid;
	
	/** 用户的昵称 */
	private String nickname;
	
	/** 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 */
	private String sex;
	
	/** 用户所在省份 */
	private String province;
	
	/** 用户所在城市 */
	private String city;
	
	/** 用户所在国家 */
	private String country;
	
	/** 用户头像 */
	private String headimgurl;
	
	private String[] privilege;
	
	/** 用户在开放平下 */
	private String unionid;

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}
	
	public String[] getPrivilege() {
		return privilege;
	}

	public void setPrivilege(String[] privilege) {
		this.privilege = privilege;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
}
