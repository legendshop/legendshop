package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *移动端基础设置
 */
@ApiModel(value="移动端基础设置Dto") 
public class AppSettingDto implements Serializable  {
	
	/**  */
	private static final long serialVersionUID = -1760516691300908288L;

	/** 风格主题名称 */
	@ApiModelProperty(value="风格主题名称")  
	private String theme; 
		
	/** 主题颜色 */
	@ApiModelProperty(value="主题颜色")  
	private String color; 
		
	/** 分类设置 */
	@ApiModelProperty(value="分类设置 ")  
	private String categorySetting; 
	
	
	public AppSettingDto() {
    }


	public String getTheme() {
		return theme;
	}


	public void setTheme(String theme) {
		this.theme = theme;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getCategorySetting() {
		return categorySetting;
	}


	public void setCategorySetting(String categorySetting) {
		this.categorySetting = categorySetting;
	}

} 
