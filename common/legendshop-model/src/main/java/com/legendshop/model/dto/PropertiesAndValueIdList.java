package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * 根据Sku找到对应的属性和属性值ID
 *
 */
public class PropertiesAndValueIdList implements Serializable{

	private static final long serialVersionUID = 2726826075479931756L;
	/**
	 * skuId: prop id set
	 */
	private Map<Long, List<Long>> propIdMap = new HashMap<Long, List<Long>>();
	
	/**
	 * skuId: prop value id set
	 */
	private Map<Long, List<Long>> propValueIdMap = new HashMap<Long, List<Long>>();

	/**
	 * 属性ID列表
	 */
	private Set<Long> propIdList = new HashSet<Long>();
	
	/**
	 * 属性值ID列表
	 */
	private Set<Long> propValueIdList  = new HashSet<Long>();;
	
	
	
	public Set<Long> getPropIdList() {
		return propIdList;
	}

	public void addPropIdList(Long skuId,List<Long> propId) {
		propIdMap.put(skuId, propId);
		this.propIdList.addAll(propId);
	}

	public Set<Long> getPropValueIdList() {
		return propValueIdList;
	}

	public void addPropValueIdList(Long skuId,List<Long> propValueId) {
		propValueIdMap.put(skuId, propValueId);
		this.propValueIdList.addAll(propValueId);
	}

	public Map<Long, List<Long>> getPropIdMap() {
		return propIdMap;
	}

	public void setPropIdMap(Map<Long, List<Long>> propIdMap) {
		this.propIdMap = propIdMap;
	}

	public Map<Long, List<Long>> getPropValueIdMap() {
		return propValueIdMap;
	}

	public void setPropValueIdMap(Map<Long, List<Long>> propValueIdMap) {
		this.propValueIdMap = propValueIdMap;
	}

	public void setPropIdList(Set<Long> propIdList) {
		this.propIdList = propIdList;
	}

	public void setPropValueIdList(Set<Long> propValueIdList) {
		this.propValueIdList = propValueIdList;
	}

	

}
