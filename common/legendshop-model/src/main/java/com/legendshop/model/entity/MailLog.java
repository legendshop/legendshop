/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

// 邮件记录表
@Entity
@Table(name = "ls_mail_log")
public class MailLog implements GenericEntity<Long>{

	private static final long serialVersionUID = -7825648455235579256L;

	// 
	/** The id. */
	private Long id ; 
		
	// 用户名称
	/** The receive name. */
	private String receiveName ; 
		
	// 邮箱
	/** The receive mail. */
	private String receiveMail ; 
		
	// 邮件ID
	/** The mail id. */
	private Long mailId ; 
		
	// 发送时间
	/** The send time. */
	private Date sendTime ; 
		
	
	/**
	 * Instantiates a new mail log.
	 */
	public MailLog() {
    }
		
	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MAIL_LOG_SEQ")
	public Long  getId(){
		return id ;
	} 
		
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id){
		this.id = id ;
	}
		
		
	/**
	 * Gets the receive name.
	 *
	 * @return the receive name
	 */
	@Column(name = "receive_name")
	public String  getReceiveName(){
		return receiveName ;
	} 
		
	/**
	 * Sets the receive name.
	 *
	 * @param receiveName the new receive name
	 */
	public void setReceiveName(String receiveName){
		this.receiveName = receiveName ;
	}
		
		
	/**
	 * Gets the receive mail.
	 *
	 * @return the receive mail
	 */
	@Column(name = "receive_mail")
	public String  getReceiveMail(){
		return receiveMail ;
	} 
		
	/**
	 * Sets the receive mail.
	 *
	 * @param receiveMail the new receive mail
	 */
	public void setReceiveMail(String receiveMail){
		this.receiveMail = receiveMail ;
	}
		
		
	/**
	 * Gets the mail id.
	 *
	 * @return the mail id
	 */
	@Column(name = "mail_id")
	public Long  getMailId(){
		return mailId ;
	} 
		
	/**
	 * Sets the mail id.
	 *
	 * @param mailId the new mail id
	 */
	public void setMailId(Long mailId){
		this.mailId = mailId ;
	}
		
		
	/**
	 * Gets the send time.
	 *
	 * @return the send time
	 */
	@Column(name = "send_time")
	public Date  getSendTime(){
		return sendTime ;
	} 
		
	/**
	 * Sets the send time.
	 *
	 * @param sendTime the new send time
	 */
	public void setSendTime(Date sendTime){
		this.sendTime = sendTime ;
	}
		
	
} 
