package com.legendshop.model.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/** 用户评论参数  **/
@ApiModel(value="用户评论参数")
public class UserCommParamsDto{
	
	/** 商品ID */
	@ApiModelProperty(value="商品Id",required=true)
	private Long prodId;
	
	/** 评论内容 */
	@ApiModelProperty(value="评论内容")
	private String content;
	
	/** 宝贝评论得分 */
	@ApiModelProperty(value="宝贝评论得分",required=true)
	private Integer prodScore;
	
	/** 卖家服务得分 */
	@ApiModelProperty(value="卖家服务得分",required=true)
	private Integer shopScore;
	
	/** 物流服务得分 */
	@ApiModelProperty(value="物流服务得分",required=true)
	private Integer logisticsScore;
	
	/**订单项id*/
	@ApiModelProperty(value="订单项id",required=true)
	private Long subItemId;
	
	/** 是否匿名 */
	@ApiModelProperty(value="是否匿名",required=true)
	private Boolean isAnonymous;
	
	/** 评论图片 */
	@ApiModelProperty(value="评论图片",hidden=true)
	private List<MultipartFile> photos;
	
	/** 评论图片后缀路径 */
	@ApiModelProperty(value="评论图片后缀路径")
	private List<String> photoList;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getProdScore() {
		return prodScore;
	}

	public void setProdScore(Integer prodScore) {
		this.prodScore = prodScore;
	}

	public Integer getShopScore() {
		return shopScore;
	}

	public void setShopScore(Integer shopScore) {
		this.shopScore = shopScore;
	}

	public Integer getLogisticsScore() {
		return logisticsScore;
	}

	public void setLogisticsScore(Integer logisticsScore) {
		this.logisticsScore = logisticsScore;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public Boolean getIsAnonymous() {
		if(null == isAnonymous){
			isAnonymous = false;
		}
		return isAnonymous;
	}

	public void setIsAnonymous(Boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public List<MultipartFile> getPhotos() {
		return photos;
	}

	public void setPhotos(List<MultipartFile> photos) {
		this.photos = photos;
	}

	public List<String> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<String> photoList) {
		this.photoList = photoList;
	}
	
}
