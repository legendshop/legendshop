package com.legendshop.model.report;

import java.io.Serializable;

/**
 *注册会员购买率
 */
public class RegisteredPurchaseRate implements Serializable {

	private static final long serialVersionUID = 189911366904939265L;

	// 总会员数
	private long totalMember;
	
	// 有过订单的会员数
	private long hasOrder;
	
	//注册会员购买率
	private Double averageRate;

	/**
	 * 注册会员购买率
	 * @param totalMember
	 * @param hasOrder
	 * @param averageRate
	 */
	public RegisteredPurchaseRate(long totalMember, long hasOrder, double averageRate) {
		this.totalMember = totalMember;
		this.hasOrder = hasOrder;
		this.averageRate = averageRate;
	}

	public void setHasOrder(Integer hasOrder) {
		this.hasOrder = hasOrder;
	}

	public Double getAverageRate() {
		return averageRate;
	}

	public void setAverageRate(Double averageRate) {
		this.averageRate = averageRate;
	}

	public long getTotalMember() {
		return totalMember;
	}

	public void setTotalMember(long totalMember) {
		this.totalMember = totalMember;
	}

	public long getHasOrder() {
		return hasOrder;
	}

	public void setHasOrder(long hasOrder) {
		this.hasOrder = hasOrder;
	}

}
