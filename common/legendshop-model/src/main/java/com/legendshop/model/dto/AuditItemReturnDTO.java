package com.legendshop.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审核用户退款退货DTO
 */
@Data
@ApiModel(value = "审核用户退款退货DTO")
public class AuditItemReturnDTO {
	/**
	 * 是否同意
	 */
	@ApiModelProperty(value = "是否同意")
	private Boolean isAgree;

	/**
	 * 是否弃货
	 */
	@ApiModelProperty(value = "是否弃货")
	private Boolean isJettison;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String sellerMessage;

	/**
	 * 退货省份
	 */
	@ApiModelProperty(value = "退货省份")
	private Integer returnProvinceId;

	/**
	 * 退货城市
	 */
	@ApiModelProperty(value = "退货城市id")
	private Integer returnCityId;

	/**
	 * 退货地级市
	 */
	@ApiModelProperty(value = "退货地级市id")
	private Integer returnAreaId;

	/**
	 * 退货地址
	 */
	@ApiModelProperty(value = "退货地址")
	private String returnShopAddr;

	/**
	 * 详细退货地址
	 */
	@ApiModelProperty(value = "详细退货地址")
	private String returnDetailAddress;

	/**
	 * 退货联系人
	 */
	@ApiModelProperty(value = "退货联系人")
	private String returnContact;

	/**
	 * 退货联系电话
	 */
	@ApiModelProperty(value = "退货联系电话")
	private String returnPhone;
}
