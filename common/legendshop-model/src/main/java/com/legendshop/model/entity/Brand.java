
package com.legendshop.model.entity;

import java.util.Date;
import org.springframework.web.multipart.MultipartFile;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 产品品牌.
 */
@Entity
@Table(name = "ls_brand")
public class Brand implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3941969699979401870L;

	/** 品牌ID */
	private Long brandId;

	/** 品牌名称 */
	private String brandName;
	
	/** 状态 */
	protected Integer status;

	/** 顺序 */
	private Integer seq;

	/** 用户ID */
	private String userId;

	/** 用户名 */
	private String userName;

	/** PC品牌logo  */
	private String brandPic;

	/** 移动端品牌logo  */
	private String brandPicMobile;
	
	/** 品牌授权书 */
	private String brandAuthorize;

	/** 备注 */
	private String memo;
	
	/** 排序ID */
	private Long sortId;

	/** 是否推荐 */
	private Integer commend;

	/** 申请时间 */
	private Date applyTime;

	/** 店铺ID */
	private Long shopId;

	/** 店铺名称 */
	private String shopName;
	
	/** 简要描述 **/
	private String brief;
	
	/** 详细内容 （故事）**/
	private String content;
	
	/** 品牌大图 **/
	private String bigImage;

	/** 审核时间 */
	private Date checkTime;

	/** 审核用户名 */
	private String checkUserName;

	/** 审核意见 */
	private String checkOpinion;
	
	/** 分类导航选中 */
	private boolean recommedSelect;
	
	/** 商品数量 **/
	private Integer prodCounts;
	
	/** PC品牌logo上传文件. */
	private MultipartFile file;

	/** 移动端品牌logo上传文件. */
	private MultipartFile mobileLogoFile;

	/** 品牌大图上传文件. */
	private MultipartFile bigImagefile;
	
	
	/** 品牌授权书上传文件 */
	private MultipartFile brandAuthorizefile;
	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	@Transient
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	/**
	 * Instantiates a new brand.
	 */
	public Brand() {
	}
	
	/**
	 * Instantiates a new brand.
	 *
	 * @param brandId the brand id
	 * @param brandName the brand name
	 */
	public Brand(Long brandId, String brandName) {
		this.brandId = brandId;
		this.brandName = brandName;
	}

	/**
	 * Gets the brand id.
	 * 
	 * @return the brand id
	 */
	@Id
	@Column(name = "brand_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "BRAND_SEQ")
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 * 
	 * @param brandId
	 *            the new brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the brand name.
	 * 
	 * @return the brand name
	 */
	@Column(name = "brand_name")
	public String getBrandName() {
		return brandName;
	}

	/**
	 * Sets the brand name.
	 * 
	 * @param brandName
	 *            the new brand name
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the memo.
	 * 
	 * @return the memo
	 */
	@Column(name = "memo")
	public String getMemo() {
		return memo;
	}

	/**
	 * Sets the memo.
	 * 
	 * @param memo
	 *            the new memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * Gets the brand pic.
	 * 
	 * @return the brand pic
	 */
	@Column(name = "brand_pic")
	public String getBrandPic() {
		return brandPic;
	}

	/**
	 * Sets the brand pic.
	 * 
	 * @param brandPic
	 *            the new brand pic
	 */
	public void setBrandPic(String brandPic) {
		this.brandPic = brandPic;
	}

	@Column(name = "brand_pic_mobile")
	public String getBrandPicMobile() {
		return brandPicMobile;
	}

	public void setBrandPicMobile(String brandPicMobile) {
		this.brandPicMobile = brandPicMobile;
	}

	@Column(name = "brand_authorize")
	public String getBrandAuthorize() {
		return brandAuthorize;
	}

	public void setBrandAuthorize(String brandAuthorize) {
		this.brandAuthorize = brandAuthorize;
	}

	@Override
	@Transient
	public Long getId() {
		return brandId;
	}
	
	@Override
	public void setId(Long id) {
		this.brandId = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * Gets the sort id.
	 *
	 * @return the sortId
	 */
	@Transient
	public Long getSortId() {
		return sortId;
	}

	/**
	 * Sets the sort id.
	 *
	 * @param sortId the sortId to set
	 */
	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	@Column(name = "is_commend")
	public Integer getCommend() {
		return commend;
	}

	public void setCommend(Integer commend) {
		this.commend = commend;
	}
	
	
	@Column(name = "applyTime")
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	
	@Transient
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name = "brief")
	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "big_image")
	public String getBigImage() {
		return bigImage;
	}

	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}
	
	@Column(name = "checkTime")
	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	@Column(name = "checkUserName")
	public String getCheckUserName() {
		return checkUserName;
	}

	public void setCheckUserName(String checkUserName) {
		this.checkUserName = checkUserName;
	}
	@Column(name = "checkOpinion")
	public String getCheckOpinion() {
		return checkOpinion;
	}

	public void setCheckOpinion(String checkOpinion) {
		this.checkOpinion = checkOpinion;
	}

	@Transient
	public Integer getProdCounts() {
		return prodCounts;
	}

	public void setProdCounts(Integer prodCounts) {
		this.prodCounts = prodCounts;
	}

	@Transient
	public MultipartFile getBigImagefile() {
		return bigImagefile;
	}

	public void setBigImagefile(MultipartFile bigImagefile) {
		this.bigImagefile = bigImagefile;
	}

	@Transient
	public MultipartFile getBrandAuthorizefile() {
		return brandAuthorizefile;
	}

	public void setBrandAuthorizefile(MultipartFile brandAuthorizefile) {
		this.brandAuthorizefile = brandAuthorizefile;
	}

	@Transient
	public boolean isRecommedSelect() {
		return recommedSelect;
	}

	public void setRecommedSelect(boolean recommedSelect) {
		this.recommedSelect = recommedSelect;
	}

	@Transient
	public MultipartFile getMobileLogoFile() {
		return mobileLogoFile;
	}

	public void setMobileLogoFile(MultipartFile mobileLogoFile) {
		this.mobileLogoFile = mobileLogoFile;
	}
}