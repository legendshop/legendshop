package com.legendshop.model.dto.api;


/**
 * 
 * @author tony
 *
 */
public class NavigationBrand {

	
	private Long brandId;
	
	private String pic;
	
	private String name;


	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	
}
