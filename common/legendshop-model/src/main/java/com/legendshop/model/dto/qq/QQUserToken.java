package com.legendshop.model.dto.qq;

public class QQUserToken extends QQReponseMsg{
	
	/** 授权令牌，Access_Token。 */
	private String access_token;
	
	/** 该access token的有效期，单位为秒。 */
	private int expires_in;
	
	/** 在授权自动续期步骤中，获取新的Access_Token时需要提供的参数。 */
	private String refresh_token;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public int getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(int expires_in) {
		this.expires_in = expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
}
