/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.store;

import com.legendshop.util.AppUtils;

/**
 * 连锁Dto.
 */
public class ChainDto {

	/** The id. */
	private Long id;
	
	/** 买家名字 */
	private String buyerName;
	
	/** 电话 */
	private String telPhone;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the buyer name.
	 *
	 * @return the buyer name
	 */
	public String getBuyerName() {
		return buyerName;
	}

	/**
	 * Sets the buyer name.
	 *
	 * @param buyerName the buyer name
	 */
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	/**
	 * Gets the tel phone.
	 *
	 * @return the tel phone
	 */
	public String getTelPhone() {
		return telPhone;
	}

	/**
	 * Sets the tel phone.
	 *
	 * @param telPhone the tel phone
	 */
	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}
	
	/**
	 * Validate.
	 *
	 * @return true, if validate
	 */
	public boolean validate(){
		if(AppUtils.isBlank(id) || AppUtils.isBlank(buyerName) || AppUtils.isBlank(telPhone)){
			return false;
		}
		return true;
	}
	
	
	
}
