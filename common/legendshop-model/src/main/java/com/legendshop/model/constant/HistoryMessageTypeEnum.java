/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * IM离线消息类型Enum*
 */
public enum HistoryMessageTypeEnum implements StringEnum {
	
	/**
	 * 客服消息类型
	 */
	CUSTOMER("customer"),
	
	/**
	 * 用户消息类型
	 */
	USER("user"),

	;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private HistoryMessageTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	 public static HistoryMessageTypeEnum fromString(String text) {
		    if (text != null) {
		      for (HistoryMessageTypeEnum b : HistoryMessageTypeEnum.values()) {
		        if (text.equalsIgnoreCase(b.value)) {
		          return b;
		        }
		      }
		    }
		    return null;
	}
}
