/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 图片类型
 */
public enum AttachmentTypeEnum implements StringEnum {
	
	/**  
	 * 产品
	 */
	PRODUCT("PRODUCT", 1),
	
	/**
	 * 品牌
	 */
	BRAND("BRAND", 1),
	

	/**
	 * 团购
	 */
	GROUP("GROUP", 1),
	
	MERGEGROUP("MERGEGROUP", 1),
	
	/**
	 * 文章
	 */
	NEWS("NEWS", 1),
	
	
	/**
	 * 积分
	 */
	INTEGRAL("INTEGRAL", 1),
	
	
	
	/**
	 * 营销活动
	 */
	ACTIVITY("ACTIVITY", 1),
	
	/**
	 * 广告
	 */
	ADVERTISEMENT("ADV", 1),
	
	/**
	 * 专题
	 */
	THEME("THEME", 0), 
	
	/**
	 * 首页轮换图片
	 */
	INDEXJPG("INDEXJPG", 1), 
	
	/**
	 * 分类
	 */
	SORT("SORT", 1), 
	
	/**
	 * 友情链接
	 */
	EXTERNALLINK("EXTERNALLINK",1),
	
	/**
	 * 产品 系列图
	 */
	IMGFILE("IMGFILE", 1),
	
	/**
	 * 系统配置
	 */
	SYSTEMCONFIG("SYSTEMCONFIG", 1),
	
	/**
	 * 商品属性值
	 */
	PRODPROPVAL("PRODPROPVAL", 1),
	
	/**
	 * 用户相关
	 */
	USER("USR", 0),
	
	/**
	 * 评论
	 */
	COMMENT("COMMENT",0),
	
	
	/**
	 * 其他
	 */
	OTHER("OTHER",0),
	
	/***
	 * 商城图片
	 */
	SHOP("SHOP",0),
	
	/**
	 * 商品分类图
	 */
	CATEGORY("CATEGORY",0),
	
	/**
	 * 开店证书
	 */
	
	OPENSHOP("OPENSHOP",0),
	
	/**
	 * 秒杀
	 */
	SECKILL("SECKILL",0),
	
	/**
	 * 首页轮换图片
	 */
	MOBILEADV("MOBILEADV", 1),
	
	/**
	 * 分类导航 广告
	 */
	CATEGORY_RECOMM_ADV("RECOMM",0),
	
	/** 退款及退货凭证 */
	REFUND_RETURN_EVIDENCE("REFUND_RETURN",0),
	
	/**身份证 图片**/
	USER_IDCARD("IDCARD",0),
	
	/** 客服系统的图片 **/
	IM("IM", 0)
	;
	

	private final String value;
	
	/**
	 * 表示是否需要图片空间进行管理
	 * 1： yes
	 * 0： no
	 */
	private final Integer type;

	private AttachmentTypeEnum(String value, Integer type) {
		this.value = value;
		this.type = type;
	}
	
    public static AttachmentTypeEnum fromCode(String value) {
        for (AttachmentTypeEnum attachmentTypeEnum : AttachmentTypeEnum.values()) {
            if (attachmentTypeEnum.value.equals(value)) {
                return attachmentTypeEnum;
            }
        }
        return null;
    }
    

	public String value() {
		return this.value;
	}
	
	public Integer type() {
		return this.type;
	}


}
