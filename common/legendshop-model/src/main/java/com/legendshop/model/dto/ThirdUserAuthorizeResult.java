package com.legendshop.model.dto;

import com.legendshop.model.entity.User;

/**
 * 第三方用户授权结果
 * @author 开发很忙
 */
public class ThirdUserAuthorizeResult {
	
	/** 错误 */
	public static final int ERROR = 0;
	
	/** 成功获取到用户 */
	public static final int SUCCESS  = 1;
	
	/** 只成功创建或获取到通行证, 需要绑定手机号 */
	public static final int SUCCESS_NEED_BIND_ACCOUNT  = 2;
	
	/** 认证结果状态码, 大于0就代表成功; 1: 成功获取到用户, 2: 只成功生成通行证或获取到通行证 */
	private int status;
	
	/** 消息 */
	private String msg;
	
	/** 商城系统的用户信息 */
	private User user;
	
	/** 商城通行证 */
	private PassportFull passport;
	
	/** 缓存passportId 的 key */
	private String passportIdKey;
	
	public ThirdUserAuthorizeResult(){
		super();
	}
	
	public ThirdUserAuthorizeResult(PassportFull passport) {
		this(SUCCESS_NEED_BIND_ACCOUNT, "需要绑定账号", null, passport);
	}
	
	public ThirdUserAuthorizeResult(User user, PassportFull passport) {
		this(SUCCESS, "成功获取到用户", user, passport);
	}
	
	public ThirdUserAuthorizeResult(String msg){
		this(ERROR, msg, null, null);
	}

	public ThirdUserAuthorizeResult(int status, String msg, User user, PassportFull passport) {
		super();
		this.status = status;
		this.msg = msg;
		this.user = user;
		this.passport = passport;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public PassportFull getPassport() {
		return passport;
	}

	public void setPassport(PassportFull passport) {
		this.passport = passport;
	}
	
	public String getPassportIdKey() {
		return passportIdKey;
	}

	public void setPassportIdKey(String passportIdKey) {
		this.passportIdKey = passportIdKey;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isSuccess(){
		
		return status == SUCCESS;
	}
	
	public boolean isNeedBindAccount(){
		
		return status == SUCCESS_NEED_BIND_ACCOUNT;
	}
	
}
