package com.legendshop.model.dto;

import com.legendshop.model.dto.qq.QQAuthorizeUserInfo;
import com.legendshop.model.dto.weibo.WeiboAuthorizeUserInfo;
import com.legendshop.model.dto.weixin.WeixinAuthorizeUserInfo;
import com.legendshop.model.dto.weixin.WeixinMpUserInfo;

/**
 * 第三方用户信息, 包含oauth2.0的授权token信息
 * @author 开发很忙
 */
public class ThirdUserInfo {
	
	/** 用户的第三方授权凭证, 目前微信小程序登陆没有这个 */
	private String accessToken;
	
	/** access_token接口调用凭证超时时间，单位（秒） */
	private int expiresIn;
	
	/** 用于刷新access_token */
	private String refreshToken;
	
	/** 用户授权的作用域，使用逗号（,）分隔, 目前只有微信才有 */
	private String scope;
	
	/** 用户在第三方应用下的唯一用户标识, 其中微信和QQ, 对于用户在不同的APPID有不同的openID; 微博是uid, 多应用是唯一的 */
	private String openId;
	
	/**开放平台统一账号下的多个应用的统一标识, 目前微信和QQ有这个, 微博没有 */
	private String unionid;
	
	/** 用户的昵称 */
	private String nickName;
	
	/** 性别 */
	private String gender;
	
	/** 用户所在城市 */
	private String city;
	
	/** 用户所在国家 */
	private String country;
	
	/** 用户所在省份 */
	private String province;
	
	/** 用户的语言zh_CN */
	private String language;
	
	/** 用户头像 */
	private String avatarUrl;

	public ThirdUserInfo() {
		super();
	}
	
	/** 
	 * 微信小程序用户信息 构造  ThirdUserInfo
	 * @param weixinMpUserInfo
	 */
	public ThirdUserInfo(WeixinMpUserInfo weixinMpUserInfo) {
		this.setOpenId(weixinMpUserInfo.getOpenId());
		this.setUnionid(weixinMpUserInfo.getUnionid());
		this.setNickName(weixinMpUserInfo.getNickName());
		this.setGender(weixinMpUserInfo.getGender());
		this.setCountry(weixinMpUserInfo.getCountry());
		this.setProvince(weixinMpUserInfo.getProvince());
		this.setCity(weixinMpUserInfo.getCity());
		this.setLanguage(weixinMpUserInfo.getLanguage());
		this.setAvatarUrl(weixinMpUserInfo.getAvatarUrl());
	}
	
	/** 
	 * 微信oauth授权用户信息 构造  ThirdUserInfo
	 * @param thirdUserToken
	 * @param weixinAuthorizeUserInfo
	 */
	public ThirdUserInfo(ThirdUserToken thirdUserToken, WeixinAuthorizeUserInfo weixinAuthorizeUserInfo) {
		
		this.setAccessToken(thirdUserToken.getAccessToken());
		this.setExpiresIn(thirdUserToken.getExpiresIn());
		this.setRefreshToken(thirdUserToken.getRefreshToken());
		this.setScope(thirdUserToken.getScope());
		
		this.setOpenId(weixinAuthorizeUserInfo.getOpenid());
		this.setUnionid(weixinAuthorizeUserInfo.getUnionid());
		this.setNickName(weixinAuthorizeUserInfo.getNickname());
		this.setGender(weixinAuthorizeUserInfo.getSex());
		this.setCountry(weixinAuthorizeUserInfo.getCountry());
		this.setProvince(weixinAuthorizeUserInfo.getProvince());
		this.setCity(weixinAuthorizeUserInfo.getCity());
		this.setAvatarUrl(weixinAuthorizeUserInfo.getHeadimgurl());
	}
	
	/** 
	 * QQ oauth授权用户信息 构造  ThirdUserInfo
	 * @param thirdUserToken
	 * @param qqAuthorizeUserInfo
	 */
	public ThirdUserInfo(ThirdUserToken thirdUserToken, QQAuthorizeUserInfo qqAuthorizeUserInfo) {
		
		this.setAccessToken(thirdUserToken.getAccessToken());
		this.setExpiresIn(thirdUserToken.getExpiresIn());
		this.setRefreshToken(thirdUserToken.getRefreshToken());
		this.setOpenId(thirdUserToken.getOpenId());
		this.setUnionid(thirdUserToken.getUnionid());
		
		if(null != qqAuthorizeUserInfo){
			this.setNickName(qqAuthorizeUserInfo.getNickname());
			this.setGender(qqAuthorizeUserInfo.getGender());
			this.setAvatarUrl(qqAuthorizeUserInfo.getFigureurl_qq_1());
		}
	}
	
	/** 
	 * 微博 oauth授权用户信息 构造  ThirdUserInfo
	 * @param thirdUserToken
	 * @param qqAuthorizeUserInfo
	 */
	public ThirdUserInfo(ThirdUserToken thirdUserToken, WeiboAuthorizeUserInfo weiboAuthorizeUserInfo) {
		
		this.setAccessToken(thirdUserToken.getAccessToken());
		this.setExpiresIn(thirdUserToken.getExpiresIn());
		this.setRefreshToken(thirdUserToken.getRefreshToken());
		this.setOpenId(thirdUserToken.getOpenId());
		
		if(null != weiboAuthorizeUserInfo){
			this.setNickName(weiboAuthorizeUserInfo.getScreen_name());
			this.setGender(weiboAuthorizeUserInfo.getGender());
			this.setAvatarUrl(weiboAuthorizeUserInfo.getAvatar_hd());
		}
	}
	
	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

}
