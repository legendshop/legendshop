package com.legendshop.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 品牌导出dto
 *
 */
@Data
public class BrandExportDTO implements Serializable {

	/** 序号 */
	private Integer sequence;

	/** 店铺 */
	private String shopName;

	/** 品牌名称 */
	private String brandName;

	/** 品牌Logo链接 */
	private String brandPic;

	/** 申请时间 */
	private Date applyTime;

	/** 状态 */
	private String status;
}
