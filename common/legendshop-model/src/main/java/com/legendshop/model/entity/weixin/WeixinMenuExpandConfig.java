package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *微信菜单扩展服务中心
 */
@Entity
@Table(name = "ls_weixin_menu_expandconfig")
public class WeixinMenuExpandConfig implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/** 服务说明 */
	private String name; 
		
	/** 消息关键字 */
	private String keyword; 
		
	/** 服务service */
	private String classService; 
		
	/** 创建时间 */
	private Date createdate; 
		
	/** 创建用户 */
	private String createUserid; 
		
	
	public WeixinMenuExpandConfig() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_MENU_EXPANDCONFIG_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
		
    @Column(name = "keyword")
	public String  getKeyword(){
		return keyword;
	} 
		
	public void setKeyword(String keyword){
			this.keyword = keyword;
		}
		
    @Column(name = "class_service")
	public String  getClassService(){
		return classService;
	} 
		
	public void setClassService(String classService){
			this.classService = classService;
		}
		
    @Column(name = "createDate")
	public Date  getCreatedate(){
		return createdate;
	} 
		
	public void setCreatedate(Date createdate){
			this.createdate = createdate;
		}
		
    @Column(name = "create_userId")
	public String  getCreateUserid(){
		return createUserid;
	} 
		
	public void setCreateUserid(String createUserid){
			this.createUserid = createUserid;
		}
	


} 
