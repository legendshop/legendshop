/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户访问历史对象.
 */
@Entity
@Table(name = "ls_vit_log")
public class VisitLog implements GenericEntity<Long> {

	private static final long serialVersionUID = -6138702495183168329L;

	/** 主键 */
	private Long visitId;

	/** 访问者IP */
	private String ip;

	/** 访问者国家 */
	private String country;

	/** 访问者地区 */
	private String area;

	/** 访问者用户名 */
	private String userName;

	/** 访问店铺名称 */
	private String shopName;
	
	/** 访问的商品ID */
	private Long productId;

	/** 访问的商品名称 */
	private String productName;
	
	/** 浏览时的商品售价 */
	private Double price;
	
	/** 商品图片 */
	private String pic;

	/** 访问的页面类型, 0: 商品页, 1: 店铺页 */
	private String page;

	/** 访问时间 */
	private Date date;

	/** 访问次数 */
	private Integer visitNum;

	/** 开始时间 */
	private Date startTime;

	/** 结束时间 */
	private Date endTime;
	
	/** 店铺Id **/
	private Long shopId;
	
	/** 访问来源 **/
	private String source;
	
	/** 用户昵称 */
	private String nickName;
	
	/** 用户头像 */
	private String portrait;
	
	/** 用户是否已删除 */
	private Boolean isUserDel;
	
	/**
	 * Gets the start time.
	 * 
	 * @return the start time
	 */
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 * 
	 * @param startTime
	 *            the new start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 * 
	 * @return the end time
	 */
	@Transient
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 * 
	 * @param endTime
	 *            the new end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Instantiates a new visit log.
	 */
	public VisitLog() {
	}
	
	/**
	 * 通用的构造函数
	 * @param ip 访问者IP
	 * @param country 访问者国家
	 * @param area 访问者地区
	 * @param userName 访问者用户名
	 * @param shopName 访问的店铺名
	 * @param productId 访问的商品ID
	 * @param productName 访问的商品名
	 * @param price 访问时商品的价格
	 * @param page 访问的页面类型
	 * @param page 访问的来源
	 * @param date 访问时间
	 */
	public VisitLog(String ip, String country, String area, String userName, Long shopId, String shopName, 
			Long productId, String productName, String pic, Double price, String page, String source, Date date) {
		this.ip = ip;
		this.country = country;
		this.area = area;
		this.userName = userName;
		this.shopId = shopId;
		this.shopName = shopName;
		this.productId = productId;
		this.productName = productName;
		this.pic = pic;
		this.price = price;
		this.page = page;
		this.source = source;
		this.date = date;
		this.isUserDel = false;
	}
	
	/**
	 * 用于记录商品访问历史的构造函数
	 * @param ip 访问者IP
	 * @param userName 访问者用户名
	 * @param shopId 访问的店铺ID
	 * @param shopName 访问的店铺名
	 * @param productId 访问的商品ID
	 * @param productName 访问的商品名
	 * @param price 访问时商品的价格
	 * @param page 访问的页面类型
	 * @param page 访问的来源
	 * @param date 访问时间
	 */
	public VisitLog(String ip, String userName, Long shopId, String shopName, 
			Long productId, String productName, String pic, Double price, String page, String source, Date date) {
		this(ip, null, null, userName, shopId, shopName, productId, productName, pic, price, page, source, date);
	}
	
	/**
	 * 用于记录店铺访问历史的构造函数
	 * @param ip 访问者IP
	 * @param userName 访问者用户名
	 * @param shopId 访问的店铺Id
	 * @param shopName 访问的店铺名
	 * @param page 访问的页面类型
	 * @param page 访问的来源
	 * @param date 访问时间
	 */
	public VisitLog(String ip, String userName, Long shopId, String shopName, String page, String source, Date date) {
		this(ip, null, null, userName, shopId, shopName, null, null, null, null, page, source, date);
	}

	/**
	 * Gets the visit id.
	 * 
	 * @return the visit id
	 */
	@Id
	@Column(name = "visit_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "VIT_LOG_SEQ")
	public Long getVisitId() {
		return this.visitId;
	}

	/**
	 * Sets the visit id.
	 * 
	 * @param visitId
	 *            the new visit id
	 */
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}

	/**
	 * Gets the ip.
	 * 
	 * @return the ip
	 */
	@Column(name = "ip")
	public String getIp() {
		return this.ip;
	}

	/**
	 * Sets the ip.
	 * 
	 * @param ip
	 *            the new ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	@Column(name = "country")
	public String getCountry() {
		return this.country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the area.
	 * 
	 * @return the area
	 */
	@Column(name = "area")
	public String getArea() {
		return this.area;
	}

	/**
	 * Sets the area.
	 * 
	 * @param area
	 *            the new area
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the shop name.
	 * 
	 * @return the shop name
	 */
	@Column(name = "shop_name")
	public String getShopName() {
		return this.shopName;
	}

	/**
	 * Sets the shop name.
	 * 
	 * @param shopName
	 *            the new shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the product name.
	 * 
	 * @return the product name
	 */
	@Column(name = "product_name")
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Column(name = "page")
	public String getPage() {
		return this.page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	@Column(name = "rec_date")
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "product_id")
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	@Column(name = "visit_num")
	public Integer getVisitNum() {
		return visitNum;
	}

	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}

	@Transient
	public Long getId() {
		return visitId;
	}

	public void setId(Long id) {
		this.visitId = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Transient
	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	@Column(name = "is_user_del")
	public Boolean getIsUserDel() {
		return isUserDel;
	}

	public void setIsUserDel(Boolean isUserDel) {
		this.isUserDel = isUserDel;
	}
}