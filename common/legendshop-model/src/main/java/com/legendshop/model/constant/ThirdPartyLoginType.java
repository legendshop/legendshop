/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;


/**
 * 系统支持的第三方登陆
 */
public interface ThirdPartyLoginType {
	
		/** QQ登陆. */
		public String QQ="qq";
		
		/** 新浪 webbo登陆. */
		public String WEBBO = "weibo";
		
		/** 微信内置网页自动登陆. */
		public String WEIXIN = "weixin";
}
