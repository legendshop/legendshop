package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *预存款提现
 */
@Entity
@Table(name = "ls_pd_withdraw_cash")
public class PdWithdrawCash implements GenericEntity<Long> {

	private static final long serialVersionUID = -113117424158037314L;

	/**  */
	private Long id; 
		
	/** 记录唯一标示 */
	private String pdcSn; 
		
	/** 会员编号 */
	private String userId; 
		
	/** 会员名称 */
	private String userName; 
		
	/** 金额 */
	private Double amount; 
		
	/** 收款银行 */
	private String bankName; 
		
	/** 收款账号 */
	private String bankNo; 
		
	/** 开户人姓名 */
	private String bankUser; 
	
	/** 开户银行 */
	private String bankOfDeposit;
		
	/** 添加时间 */
	private Date addTime; 
		
	/** 付款时间[后台管理员] */
	private Date paymentTime; 
		
	/** 提现支付状态 [0:待审核 1：已完成 -1：已拒绝] */
	private Integer paymentState; 
		
	/** 支付管理员 */
	private String updateAdminName; 
		
	/** 管理员备注 */
	private String adminNote; 
		
	/** 用户提现备注 */
	private String userNote; 
	
	private String nickName;
	
	private String authType;
	
	private String authCode;
	
	private String captcha;
		
	
	public PdWithdrawCash() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PD_WITHDRAW_CASH_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "pdc_sn")
	public String  getPdcSn(){
		return pdcSn;
	} 
		
	public void setPdcSn(String pdcSn){
			this.pdcSn = pdcSn;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "amount")
	public Double  getAmount(){
		return amount;
	} 
		
	public void setAmount(Double amount){
			this.amount = amount;
		}
		
    @Column(name = "bank_name")
	public String  getBankName(){
		return bankName;
	} 
		
	public void setBankName(String bankName){
			this.bankName = bankName;
		}
		
    @Column(name = "bank_no")
	public String  getBankNo(){
		return bankNo;
	} 
		
	public void setBankNo(String bankNo){
			this.bankNo = bankNo;
		}
		
    @Column(name = "bank_user")
	public String  getBankUser(){
		return bankUser;
	} 
		
	public void setBankUser(String bankUser){
			this.bankUser = bankUser;
		}
	
	@Column(name = "bank_of_deposit")
    public String getBankOfDeposit() {
		return bankOfDeposit;
	}

	public void setBankOfDeposit(String bankOfDeposit) {
		this.bankOfDeposit = bankOfDeposit;
	}

	@Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    @Column(name = "payment_time")
	public Date  getPaymentTime(){
		return paymentTime;
	} 
		
	public void setPaymentTime(Date paymentTime){
			this.paymentTime = paymentTime;
		}
		
    @Column(name = "payment_state")
	public Integer  getPaymentState(){
		return paymentState;
	} 
		
	public void setPaymentState(Integer paymentState){
			this.paymentState = paymentState;
		}
		
    @Column(name = "update_admin_name")
	public String  getUpdateAdminName(){
		return updateAdminName;
	} 
		
	public void setUpdateAdminName(String updateAdminName){
			this.updateAdminName = updateAdminName;
		}
		
    @Column(name = "admin_note")
	public String  getAdminNote(){
		return adminNote;
	} 
		
	public void setAdminNote(String adminNote){
			this.adminNote = adminNote;
		}
		
    @Column(name = "user_note")
	public String  getUserNote(){
		return userNote;
	} 
		
	public void setUserNote(String userNote){
			this.userNote = userNote;
		}

	@Transient
	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	@Transient
	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	@Transient
	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
	
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
} 
