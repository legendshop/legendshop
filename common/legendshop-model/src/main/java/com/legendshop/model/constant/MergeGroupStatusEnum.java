package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 拼团活动状态
 */
public enum MergeGroupStatusEnum implements IntegerEnum{
	
	/** 未通过 **/
	NO_AUDITED(-2),
	
	/** 待审核 **/
	NEED_AUDITED(0),
	
	/** 已失效  **/
	OFFLINE(-1),
	
	/** 进行中 **/
	ONLINE(1),
	
	/** 已过期 **/
	OUT_OF_TIME(-3),
	;

	
	private Integer num;
	
	@Override
	public Integer value() {
		// TODO Auto-generated method stub
		return num;
	}

	private MergeGroupStatusEnum(Integer num) {
		this.num = num;
	}
	
}
