package com.legendshop.model.dto.recomm;

import java.io.Serializable;

/**
 * 品牌推荐.
 *
 * @author root
 */
public class RecommBrandDto implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5394940932762390275L;
	
	/** The brand id. */
	private Long brandId;
	
	/** The brand name. */
	private String brandName;
	
	/** The brand pic. */
	private String brandPic;

	/**
	 * Gets the brand id.
	 *
	 * @return the brand id
	 */
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 *
	 * @param brandId the brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the brand name.
	 *
	 * @return the brand name
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * Sets the brand name.
	 *
	 * @param brandName the brand name
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * Gets the brand pic.
	 *
	 * @return the brand pic
	 */
	public String getBrandPic() {
		return brandPic;
	}

	/**
	 * Sets the brand pic.
	 *
	 * @param brandPic the brand pic
	 */
	public void setBrandPic(String brandPic) {
		this.brandPic = brandPic;
	}
	
	
}
