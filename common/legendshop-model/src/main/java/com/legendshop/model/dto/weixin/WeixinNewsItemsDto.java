package com.legendshop.model.dto.weixin;

import java.io.Serializable;

public class WeixinNewsItemsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9096517767780654819L;

	private String newsItemsJson;
	
	private String delIds;

	public String getNewsItemsJson() {
		return newsItemsJson;
	}

	public void setNewsItemsJson(String newsItemsJson) {
		this.newsItemsJson = newsItemsJson;
	}

	public String getDelIds() {
		return delIds;
	}

	public void setDelIds(String delIds) {
		this.delIds = delIds;
	}

	
	
	
}
