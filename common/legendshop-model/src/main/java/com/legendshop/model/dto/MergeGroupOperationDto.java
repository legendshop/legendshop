package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MergeGroupOperationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String addNumber;

	// 参团人数
	private Integer addPeopleNumber;

	/** 拼团人数(n人团) */
	private Integer peopleNumber;

	/** 结束时间 */
	private Date endTime;

	private Long status;

	private List<MergeGroupAddDto> subList = new ArrayList<MergeGroupAddDto>();

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public List<MergeGroupAddDto> getSubList() {
		return subList;
	}

	public void setSubList(List<MergeGroupAddDto> subList) {
		this.subList = subList;
	}

	public Integer getAddPeopleNumber() {
		return addPeopleNumber;
	}

	public void setAddPeopleNumber(Integer addPeopleNumber) {
		this.addPeopleNumber = addPeopleNumber;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void addSubList(MergeGroupAddDto mergeGroupAddDto) {
		subList.add(mergeGroupAddDto);
	}

}
