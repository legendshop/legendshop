package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *每个团的运营信息
 */
@Entity
@Table(name = "ls_merge_group_operate")
public class MergeGroupOperate implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id; 
		
	/** 商家ID */
	private Long shopId; 
		
	/** 活动ID */
	private Long mergeId; 
		
	/** 拼团编号,用于区分参团的用户归属哪个团 */
	private String addNumber; 
		
	/** 该团参团人数 */
	private int number; 
	
	/** 状态MergeGroupAddStatusEnum[1:进行中,2:成功,3:失败] */
	private Integer status; 
	
	/** 开团时间 */
	private Date createTime;
	
	/** 成团截止时间 */
	private Date endTime;
	
	/** 是否团长免单,默认否 */
	private Boolean isHeadFree; 
	
	/** 该活动成团人数，n人团 */
	private int peopleNumber;
	
	/** 微信小程序码 */
	private String wxAcode;
	
	public MergeGroupOperate() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MERGE_GROUP_OPERATE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "merge_id")
	public Long  getMergeId(){
		return mergeId;
	} 
		
	public void setMergeId(Long mergeId){
			this.mergeId = mergeId;
		}
		
    @Column(name = "add_number")
	public String  getAddNumber(){
		return addNumber;
	} 
		
	public void setAddNumber(String addNumber){
			this.addNumber = addNumber;
		}
		
    @Column(name = "number")
    public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name = "end_time")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column(name = "is_head_free")
	public Boolean getIsHeadFree() {
		return isHeadFree;
	}

	public void setIsHeadFree(Boolean isHeadFree) {
		this.isHeadFree = isHeadFree;
	}

	@Transient
	public int getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(int peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	@Column(name = "wx_a_code")
	public String getWxACode() {
		return wxAcode;
	}

	public void setWxACode(String wxAcode) {
		this.wxAcode = wxAcode;
	}
} 
