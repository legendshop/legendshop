/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 首页楼层
 */
@Entity
@Table(name = "ls_floor")
public class Floor implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2045804814438964700L;

	// ID
	/** The fl id. */
	private Long flId;

	// 名称
	/** The name. */
	private String name;

	// 记录时间
	/** The rec date. */
	private Date recDate;

	// 状态
	/** The status. */
	private Integer status;

	// 楼层样式
	/** The css style. */
	private String cssStyle;
	
	// 内容类型
	private String contentType;

	//下面的内容类型
	private String contentType2;
	
	// 顺序
	/** The seq. */
	private Integer seq;
	
	private String layout;

	// 子节点
	/**楼层子元素[品牌、广告、图片、商品、分类] */
	Set<SubFloor> subFloors = new TreeSet<SubFloor>(new SubFloorComparator());
	
	/**楼层子元素[混合模式的Tap商品结构] */
	Set<FloorItem> floorItems = new TreeSet<FloorItem>(new FloorItemComparator());

	/**右边栏开关**/
	private Integer leftSwitch;
	
	/**
	 * Instantiates a new floor.
	 */
	public Floor() {
	}

	/**
	 * Gets the fl id.
	 *
	 * @return the fl id
	 */
	@Id
	@Column(name = "fl_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "FLOOR_SEQ")
	public Long getFlId() {
		return flId;
	}

	/**
	 * Sets the fl id.
	 *
	 * @param flId the new fl id
	 */
	public void setFlId(Long flId) {
		this.flId = flId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the css style.
	 *
	 * @return the css style
	 */
	@Column(name = "css_style")
	public String getCssStyle() {
		return cssStyle;
	}

	/**
	 * Sets the css style.
	 *
	 * @param cssStyle the new css style
	 */
	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return flId;
	}
	
	public void setId(Long id) {
		this.flId = id;
	}

	/**
	 * 增加二级楼层元素.
	 *
	 * @param subFloor the sub floor
	 */
	public void addSubFloor(SubFloor subFloor) {
		if (this.getFlId().equals(subFloor.getFloorId())) {
			subFloors.add(subFloor);
		}
	}

	/**
	 * Gets the sub floors.
	 *
	 * @return the sub floors
	 */
	@Transient
	public Set<SubFloor> getSubFloors() {
		return subFloors;
	}

	/**
	 * Sets the sub floors.
	 *
	 * @param subFloors the new sub floors
	 */
	public void setSubFloors(Set<SubFloor> subFloors) {
		this.subFloors = subFloors;
	}

	@Transient
	public Set<FloorItem> getFloorItems() {
		return floorItems;
	}

	public void addFloorItems(FloorItem floorItem) {
		if(floorItem!=null){
			floorItems.add(floorItem);
		}
	}

	public void setFloorItems(Set<FloorItem> floorItems) {
		this.floorItems = floorItems;
	}

	@Column(name = "content_type")
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "content_type2")
	public String getContentType2() {
		return contentType2;
	}

	public void setContentType2(String contentType2) {
		this.contentType2 = contentType2;
	}

	@Column(name = "left_switch")
	public Integer getLeftSwitch() {
		return leftSwitch;
	}

	public void setLeftSwitch(Integer leftSwitch) {
		this.leftSwitch = leftSwitch;
	}

	@Column(name = "layout")
	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}
	

}
