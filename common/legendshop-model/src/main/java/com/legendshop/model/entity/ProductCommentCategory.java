/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品评论Dto.
 */
@ApiModel(value="商品评论Dto") 
public class ProductCommentCategory implements Serializable{

	private static final long serialVersionUID = 4592618215956934080L;

	@ApiModelProperty(value="商品id")  
	private Long prodId;
	
	@ApiModelProperty(value="总数")  
	private int total;
	
	/** 好评 */
	@ApiModelProperty(value="好评")  
	private int high;
	
	/** 中评 */
	@ApiModelProperty(value="中评")  
	private int medium;
	
	/** 差评 */
	@ApiModelProperty(value="差评")  
	private int low;
	
	/** 追评 */
	@ApiModelProperty(value="追评")  
	private int append;
	
	/** 有图 */
	@ApiModelProperty(value="有图")  
	private int photo;
	
	/** 待回复 */
	@ApiModelProperty(value="待回复")  
	private int waitReply;
	
	//好评率
	@ApiModelProperty(value="好评率")  
	private double highRate;
	
	//中评率
	@ApiModelProperty(value="中评率")  
	private double mediumRate;
	
	//差评率
	@ApiModelProperty(value="差评率")  
	private double lowRate;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public int getMedium() {
		return medium;
	}
	
	public void setMedium(int medium) {
		this.medium = medium;
	}
	
	public int getAppend() {
		return append;
	}

	public void setAppend(int append) {
		this.append = append;
	}

	public int getPhoto() {
		return photo;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}

	public int getLow() {
		return low;
	}

	public void setLow(int low) {
		this.low = low;
	}

	public double getHighRate() {
		return highRate;
	}

	public void setHighRate(double highRate) {
		this.highRate = highRate;
	}

	public double getMediumRate() {
		return mediumRate;
	}

	public void setMediumRate(double mediumRate) {
		this.mediumRate = mediumRate;
	}

	public double getLowRate() {
		return lowRate;
	}

	public void setLowRate(double lowRate) {
		this.lowRate = lowRate;
	}

	public int getWaitReply() {
		return waitReply;
	}

	public void setWaitReply(int waitReply) {
		this.waitReply = waitReply;
	}
}
