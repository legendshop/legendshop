package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_prod_group_relevance")
public class ProdGroupRelevance implements GenericEntity<Long> {

	/** 主键ID */
	private Long id; 
		
	/** 商品分组ID */
	private Long groupId; 
		
	/** 商品ID */
	private Long prodId; 
		
	
	public ProdGroupRelevance() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_GROUP_RELEVANCE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "group_id")
	public Long  getGroupId(){
		return groupId;
	} 
		
	public void setGroupId(Long groupId){
			this.groupId = groupId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}

	public ProdGroupRelevance(Long groupId, Long prodId) {
		super();
		this.groupId = groupId;
		this.prodId = prodId;
	}

	
	

} 
