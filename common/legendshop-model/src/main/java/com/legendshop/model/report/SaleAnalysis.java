package com.legendshop.model.report;

import java.io.Serializable;

/**
 * 销售分析实体类
 */
public class SaleAnalysis  implements Serializable {

	private static final long serialVersionUID = 1308419526344902894L;
	
	private AverageMemberOrders averageMemberOrders;
	
	private AverageOrderAmouont averageOrderAmouont;
	
	private OrdersConversionRate ordersConversionRate;
	
	private RegisteredPurchaseRate registeredPurchaseRate;

	public AverageMemberOrders getAverageMemberOrders() {
		return averageMemberOrders;
	}

	public void setAverageMemberOrders(AverageMemberOrders averageMemberOrders) {
		this.averageMemberOrders = averageMemberOrders;
	}

	public AverageOrderAmouont getAverageOrderAmouont() {
		return averageOrderAmouont;
	}

	public void setAverageOrderAmouont(AverageOrderAmouont averageOrderAmouont) {
		this.averageOrderAmouont = averageOrderAmouont;
	}

	public OrdersConversionRate getOrdersConversionRate() {
		return ordersConversionRate;
	}

	public void setOrdersConversionRate(OrdersConversionRate ordersConversionRate) {
		this.ordersConversionRate = ordersConversionRate;
	}

	public RegisteredPurchaseRate getRegisteredPurchaseRate() {
		return registeredPurchaseRate;
	}

	public void setRegisteredPurchaseRate(RegisteredPurchaseRate registeredPurchaseRate) {
		this.registeredPurchaseRate = registeredPurchaseRate;
	}

}
