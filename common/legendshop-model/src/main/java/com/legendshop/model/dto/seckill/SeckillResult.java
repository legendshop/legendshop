/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

/**
 * 这个类是用于将秒杀的结果进行封装.
 *
 */
public class SeckillResult {

	/** The seckill id. */
	private long seckillId;

	/** 秒杀的的状态. */
	private int state;

	/** 秒杀的输出的信息. */
	private String stateInfo;

	/**
	 * Gets the seckill id.
	 *
	 * @return the seckill id
	 */
	public long getSeckillId() {
		return seckillId;
	}

	/**
	 * Sets the seckill id.
	 *
	 * @param seckillId
	 *            the seckill id
	 */
	public void setSeckillId(long seckillId) {
		this.seckillId = seckillId;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state
	 *            the state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * Gets the state info.
	 *
	 * @return the state info
	 */
	public String getStateInfo() {
		return stateInfo;
	}

	/**
	 * Sets the state info.
	 *
	 * @param stateInfo
	 *            the state info
	 */
	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}

	/**
	 * The Constructor.
	 *
	 * @param seckillId
	 *            the seckill id
	 * @param seckillStateEnum
	 *            the seckill state enum
	 */
	public SeckillResult(long seckillId, SeckillStateEnum seckillStateEnum) {
		this.seckillId = seckillId;
		this.state = seckillStateEnum.getState();
		this.stateInfo = seckillStateEnum.getStateinfo();
	}

	/**
	 * The Constructor.
	 *
	 * @param seckillId
	 *            the seckill id
	 * @param stateInfo
	 *            the state info
	 */
	public SeckillResult(long seckillId, String stateInfo) {
		super();
		this.seckillId = seckillId;
		this.stateInfo = stateInfo;
	}

}
