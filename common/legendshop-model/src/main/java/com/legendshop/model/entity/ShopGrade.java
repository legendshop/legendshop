/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户等级.
 */
@Entity
@Table(name = "ls_shop_grad")
public class ShopGrade implements GenericEntity<Integer> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2994159243674997206L;

	/** The grade id. */
	private Integer gradeId;

	/** The name. */
	private String name;

	/** The max sort size. */
	private Integer maxSortSize;

	/** The max nsort size. */
	private Integer maxNsortSize;

	/** The max prod. */
	private Integer maxProd;

	/**
	 * default constructor.
	 */
	public ShopGrade() {
	}

	/**
	 * minimal constructor.
	 * 
	 * @param gradeId
	 *            the grade id
	 */
	public ShopGrade(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * full constructor.
	 * 
	 * @param gradeId
	 *            the grade id
	 * @param name
	 *            the name
	 * @param maxSortSize
	 *            the max sort size
	 * @param maxNsortSize
	 *            the max nsort size
	 * @param maxProd
	 *            the max prod
	 */
	public ShopGrade(Integer gradeId, String name, Integer maxSortSize,
			Integer maxNsortSize, Integer maxProd) {
		this.gradeId = gradeId;
		this.name = name;
		this.maxSortSize = maxSortSize;
		this.maxNsortSize = maxNsortSize;
		this.maxProd = maxProd;
	}

	// Property accessors

	/**
	 * Gets the grade id.
	 * 
	 * @return the grade id
	 */
	@Id
	@Column(name = "grade_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_GRAD_SEQ")
	public Integer getGradeId() {
		return this.gradeId;
	}

	/**
	 * Sets the grade id.
	 * 
	 * @param gradeId
	 *            the new grade id
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the max sort size.
	 * 
	 * @return the max sort size
	 */
	@Column(name = "max_sort_size")
	public Integer getMaxSortSize() {
		return this.maxSortSize;
	}

	/**
	 * Sets the max sort size.
	 * 
	 * @param maxSortSize
	 *            the new max sort size
	 */
	public void setMaxSortSize(Integer maxSortSize) {
		this.maxSortSize = maxSortSize;
	}

	/**
	 * Gets the max nsort size.
	 * 
	 * @return the max nsort size
	 */
	@Column(name = "max_nsort_size")
	public Integer getMaxNsortSize() {
		return this.maxNsortSize;
	}

	/**
	 * Sets the max nsort size.
	 * 
	 * @param maxNsortSize
	 *            the new max nsort size
	 */
	public void setMaxNsortSize(Integer maxNsortSize) {
		this.maxNsortSize = maxNsortSize;
	}

	/**
	 * Gets the max prod.
	 * 
	 * @return the max prod
	 */
	@Column(name = "max_prod")
	public Integer getMaxProd() {
		return this.maxProd;
	}

	/**
	 * Sets the max prod.
	 * 
	 * @param maxProd
	 *            the new max prod
	 */
	public void setMaxProd(Integer maxProd) {
		this.maxProd = maxProd;
	}

	@Transient
	public Integer getId() {
		return gradeId;
	}

	public void setId(Integer id) {
		this.gradeId = id;
	}
}