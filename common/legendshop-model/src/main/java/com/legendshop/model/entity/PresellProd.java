package com.legendshop.model.entity;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 预售商品表
 */
@Entity
@Table(name = "ls_presell_prod")
public class PresellProd implements GenericEntity<Long> {

	private static final long serialVersionUID = -5508927778809206834L;

	/** 主键 */
	private Long id; 
		
	/** 产品ID */
	private Long prodId; 
		
	/** 产品SKU ID */
	private Long skuId; 
		
	/** 商家ID */
	private Long shopId; 
	
	/** 商家名称 */
	private String shopName;
	
	/** 方案名称 */
	private String schemeName;
		
	/** 支付方式,0:全额,1:订金 */
	private Long payPctType; 
		
	/** 预售价格 */
	private java.math.BigDecimal prePrice; 
	
	/** 订金金额 */
	private java.math.BigDecimal preDepositPrice; 
		
	/** 预售开始时间 */
	private Date preSaleStart; 
		
	/** 预售结束时间 */
	private Date preSaleEnd; 
		
	/** 预售发货时间 */
	private Date preDeliveryTime; 
	
	/** 预售发货时间 */
	private String preDeliveryTimeStr;
		
	/** 预售支付百分比 */
	private Double payPct; 
		
	/** 尾款支付开始时间 */
	private Date finalMStart; 
		
	/** 尾款支付结束时间 */
	private Date finalMEnd; 
		
	/** 状态,-2:未提审,-1:待审核,0:审核通过(上线),1:拒绝,2:已完成,-3:已过期*/
	private Long status; 
	
	/** 创建时间 */
	private Date createTime;
		
	/** 审核意见 */
	private String auditOpinion; 
		
	/** 审核时间 */
	private Date auditTime; 
	
	/**商品名称**/
	private String prodName;
	
	/**sku图片名称**/
	private String skuPic;
	
	/**商品图片**/
	private String prodPic;

	/**活动关联商品**/
	private List<PresellSku>skus;
	
	public PresellProd() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PRESELL_PROD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
	
	/**
	 * @return the shopName
	 */
	@Column(name="shop_name")
	public String getShopName() {
		return shopName;
	}

	/**
	 * @return the schemeName
	 */
	@Column(name="scheme_name")
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName the schemeName to set
	 */
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
		
    @Column(name = "pay_pct_type")
	public Long  getPayPctType(){
		return payPctType;
	} 
		
	public void setPayPctType(Long payPctType){
			this.payPctType = payPctType;
		}
		
    @Column(name = "pre_price")
	public java.math.BigDecimal  getPrePrice(){
		return prePrice;
	} 
		
	public void setPrePrice(java.math.BigDecimal prePrice){
			this.prePrice = prePrice;
		}
		
	/**
	 * @return the preDepositPrice
	 */
	@Column(name="pre_deposit_price")
	public java.math.BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	/**
	 * @param preDepositPrice the preDepositPrice to set
	 */
	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}
	
    @Column(name = "pre_sale_start")
	public Date  getPreSaleStart(){
		return preSaleStart;
	} 
		
	public void setPreSaleStart(Date preSaleStart){
			this.preSaleStart = preSaleStart;
		}
		
    @Column(name = "pre_sale_end")
	public Date  getPreSaleEnd(){
		return preSaleEnd;
	} 
		
	public void setPreSaleEnd(Date preSaleEnd){
			this.preSaleEnd = preSaleEnd;
		}
		
    @Column(name = "pre_delivery_time")
	public Date  getPreDeliveryTime(){
		return preDeliveryTime;
	} 
		
	public void setPreDeliveryTime(Date preDeliveryTime){
			this.preDeliveryTime = preDeliveryTime;
		}
	
	/**
	 * @return the preDeliveryTimeStr
	 */
	@Transient
	public String getPreDeliveryTimeStr() {
		return preDeliveryTimeStr;
	}

	/**
	 * @param preDeliveryTimeStr the preDeliveryTimeStr to set
	 */
	public void setPreDeliveryTimeStr(String preDeliveryTimeStr) {
		this.preDeliveryTimeStr = preDeliveryTimeStr;
	}
		
    @Column(name = "pay_pct")
	public Double  getPayPct(){
		return payPct;
	} 
		
	public void setPayPct(Double payPct){
			this.payPct = payPct;
		}
		
    @Column(name = "final_m_start")
	public Date  getFinalMStart(){
		return finalMStart;
	} 
		
	public void setFinalMStart(Date finalMStart){
			this.finalMStart = finalMStart;
		}
		
    @Column(name = "final_m_end")
	public Date  getFinalMEnd(){
		return finalMEnd;
	} 
		
	public void setFinalMEnd(Date finalMEnd){
			this.finalMEnd = finalMEnd;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
		}
	
	/**
	 * @return the createTime
	 */
	@Column(name="create_time")
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
		
    @Column(name = "audit_opinion")
	public String  getAuditOpinion(){
		return auditOpinion;
	} 
		
	public void setAuditOpinion(String auditOpinion){
			this.auditOpinion = auditOpinion;
		}
		
    @Column(name = "audit_time")
	public Date  getAuditTime(){
		return auditTime;
	} 
		
	public void setAuditTime(Date auditTime){
			this.auditTime = auditTime;
	}

	@Transient
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	@Transient
	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	
	@Transient
	public String getSkuPic() {
		return skuPic;
	}

	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	@Transient
	public List<PresellSku> getSkus() {
		return skus;
	}

	public void setSkus(List<PresellSku> skus) {
		this.skus = skus;
	}
}
