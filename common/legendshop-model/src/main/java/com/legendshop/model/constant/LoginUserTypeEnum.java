/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 登录的用户类型
 */
public enum LoginUserTypeEnum implements  LoginUserType, StringEnum {
	/**  
	 * 密码登录
	 */
	USER(USER_TYPE),
	
	/** 商家登录 **/
	SELLER(SELLER_TYPE),
	
	/** 商家员工登录 **/
	EMPLOYEE(EMPLOYEE_TYPE),
	
	/** 门店登录**/
	STORE(STORE_TYPE),
	
	/**
	 * 后台管理员
	 */
	ADMIN(ADMIN_TYPE);

	private final String value;

	private LoginUserTypeEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
	

}
