/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 首页轮换图片.
 */
@Entity
@Table(name = "ls_index_jpg")
public class Indexjpg extends UploadFile implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1888032358468727806L;

	/** The id. */
	private Long id;

	/** The img. */
	private String img;

	/** The alt. */
	private String des;

	/** The title. */
	private String title;

	/** The link. */
	private String link;

	/** The status. */
	private int status;

	private Integer seq;

	private Date uploadTime;

	/** 所在位置 **/
	private int location;

	// Constructors

	/**
	 * default constructor.
	 */
	public Indexjpg() {
	}


	public Indexjpg(Long id, String href, String img, String des, String title, String stitle, String link, String titleLink) {
		this.id = id;
		this.img = img;
		this.des = des;
		this.title = title;
		this.link = link;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INDEX_JPG_SEQ")
	public Long getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the img.
	 * 
	 * @return the img
	 */
	@Column(name = "img")
	public String getImg() {
		return this.img;
	}

	/**
	 * Sets the img.
	 * 
	 * @param img
	 *            the new img
	 */
	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "des")
	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	@Column(name = "title")
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the link.
	 * 
	 * @return the link
	 */
	@Column(name = "link")
	public String getLink() {
		return this.link;
	}

	/**
	 * Sets the link.
	 * 
	 * @param link
	 *            the new link
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the status
	 */
	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * @param seq
	 *            the seq to set
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * @return the uploadTime
	 */
	@Column(name = "upload_time")
	public Date getUploadTime() {
		return uploadTime;
	}

	/**
	 * @param uploadTime
	 *            the uploadTime to set
	 */
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	@Column(name = "location")
	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

}