package com.legendshop.model.entity;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 楼层元素
 *
 */
@Entity
@Table(name = "ls_floor_item")
public class FloorItem implements GenericEntity<Long>{

	private static final long serialVersionUID = 2700988550315445573L;

	// ID
	private Long fiId ; 
		
	// 记录时间
	private Date recDate ; 
		
	// 父节点
	private Long floorId ; 
		
	// 
	private Long referId ; 
		
	// 类型ID
	private String type ; 
		
	// 商品分类Level
	private Integer sortLevel ; 
		
	// 顺序
	private Integer seq ; 
	
	//,对应Item的名字
	private String categoryName;
	
	//商品现价
	private String cash;
	
	//商品简要说明
	private String brief;
	
	//商品价格
	private String price;
	
	//附件ID
	private Long attachmentId;
	
	//图片路径
	private String pic;
	
	//品牌名字
	private String brandName;
		
	// 子节点
	Set<FloorSubItem> floorSubItemSet = new TreeSet<FloorSubItem>(new FloorSubItemComparator());
	
	//广告标题
	private String title;
	
	private String linkUrl;
	
	private String picUrl;
	
	private String sourceInput;
	
	
	private String sortName;
	
	private String layout;
	
	public FloorItem() {
    }
		
	@Id
	@Column(name = "fi_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "FLOOR_ITEM_SEQ")
	public Long  getFiId(){
		return fiId ;
	} 
		
	public void setFiId(Long fiId){
		this.fiId = fiId ;
	}
		
		
	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate ;
	} 
		
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
		
	@Column(name = "floor_id")
	public Long  getFloorId(){
		return floorId ;
	} 
		
	public void setFloorId(Long floorId){
		this.floorId = floorId ;
	}
		
		
	@Column(name = "refer_id")
	public Long  getReferId(){
		return referId ;
	} 
		
	public void setReferId(Long referId){
		this.referId = referId ;
	}
		
		
	@Column(name = "type")
	public String  getType(){
		return type ;
	} 
		
	public void setType(String type){
		this.type = type ;
	}
		
		
	@Column(name = "sort_level")
	public Integer  getSortLevel(){
		return sortLevel ;
	} 
		
	public void setSortLevel(Integer sortLevel){
		this.sortLevel = sortLevel ;
	}
		
		
	@Column(name = "seq")
	public Integer  getSeq(){
		return seq ;
	} 
		
	public void setSeq(Integer seq){
		this.seq = seq ;
	}
		
	
  @Transient
  public Long getId() {
		return 	fiId;
	}

	public void setId(Long id) {
		this.fiId = id;
	}

/**
 * 增加楼层二级元素.
 *
 * @param subFloor the sub floor
 */
public void addFloorSubItem(FloorSubItem floorSubItem) {
	if (this.getFiId().equals(floorSubItem.getFiId())) {
		floorSubItemSet.add(floorSubItem);
	}
}

@Transient
public Set<FloorSubItem> getFloorSubItemSet() {
	return floorSubItemSet;
}

public void setFloorSubItemSet(Set<FloorSubItem> floorSubItemSet) {
	this.floorSubItemSet = floorSubItemSet;
}

@Column(name = "attachment_id")
public Long getAttachmentId() {
	return attachmentId;
}

public void setAttachmentId(Long attachmentId) {
	this.attachmentId = attachmentId;
}

@Column(name = "pic")
public String getPic() {
	return pic;
}

public void setPic(String pic) {
	this.pic = pic;
}



@Transient
public String getBrandName() {
	return brandName;
}

public void setBrandName(String brandName) {
	this.brandName = brandName;
}

@Transient
public String getPrice() {
	return price;
}

public void setPrice(String price) {
	this.price = price;
}

@Transient
public String getCash() {
	return cash;
}

public void setCash(String cash) {
	this.cash = cash;
}

@Transient
public String getBrief() {
	return brief;
}

public void setBrief(String brief) {
	this.brief = brief;
}

@Transient
public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

@Transient
public String getLinkUrl() {
	return linkUrl;
}

public void setLinkUrl(String linkUrl) {
	this.linkUrl = linkUrl;
}

@Transient
public String getPicUrl() {
	return picUrl;
}

public void setPicUrl(String picUrl) {
	this.picUrl = picUrl;
}

@Transient
public String getSourceInput() {
	return sourceInput;
}

public void setSourceInput(String sourceInput) {
	this.sourceInput = sourceInput;
}

@Transient
public String getCategoryName() {
	return categoryName;
}

public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}

@Transient
public String getSortName() {
	return sortName;
}

public void setSortName(String sortName) {
	this.sortName = sortName;
}


@Column(name = "layout")
public String getLayout() {
	return layout;
}

public void setLayout(String layout) {
	this.layout = layout;
}


} 
