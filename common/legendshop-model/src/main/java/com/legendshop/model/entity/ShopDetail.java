/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 代表一个商城基本信息.
 */
@Entity
@Table(name = "ls_shop_detail")

@ApiModel("商城信息")
public class ShopDetail extends AbstractShopDetail  {

	/** The Constant serialVerscityionUID. */
	private static final long serialVersionUID = 3924728437878080050L;
	
	/** 省份. */
	@ApiModelProperty(hidden = true)
	private String province;
	
	/** 城市. */
	@ApiModelProperty(hidden = true)
	private String city;
	
	/** 地区. */
	@ApiModelProperty(hidden = true)
	private String area;
	
	/** 昵称. */
	@ApiModelProperty(hidden = true)
	private String nickName;
	
	/** 随机数. */
	@ApiModelProperty(hidden = true)
	private String randNum;
	
	/** 上传的文件. */
	@ApiModelProperty(hidden = true)
	protected MultipartFile file2;
	
	/** 公司名字. */
	@ApiModelProperty(hidden = true)
	private String companyName;
	
	/** 公司营业执照号码. */
	@ApiModelProperty(hidden = true)
	private String licenseNumber;
	
	/** 公司营业执照图片. */
	@ApiModelProperty(hidden = true)
	private MultipartFile licensePicFile;
	
	/** 公司营业执照图片路径. */
	@ApiModelProperty(hidden = true)
	private String licensePicFilePath;

	/**
	 * default constructor.
	 */
    public ShopDetail() {
    }

    public ShopDetail(String logoPic, String briefDesc,String userId, String userName) {
    	this.logoPic = logoPic;
    	this.userId = userId;
    	this.userName = userName;
    	this.briefDesc = briefDesc;
    }

	 @Transient
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	 @Transient
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	 @Transient
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
    
	@Transient
	public String getRandNum() {
		return randNum;
	}

	public void setRandNum(String randNum) {
		this.randNum = randNum;
	}

	@Transient
	public MultipartFile getFile2() {
		return file2;
	}

	public void setFile2(MultipartFile file2) {
		this.file2 = file2;
	}
	
	@Transient
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	//
	@Transient
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Transient
	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	@Transient
	public MultipartFile getLicensePicFile() {
		return licensePicFile;
	}

	public void setLicensePicFile(MultipartFile licensePicFile) {
		this.licensePicFile = licensePicFile;
	}

	@Transient
	public String getLicensePicFilePath() {
		return licensePicFilePath;
	}

	public void setLicensePicFilePath(String licensePicFilePath) {
		this.licensePicFilePath = licensePicFilePath;
	}
	

}