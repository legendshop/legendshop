/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.legendshop.dao.persistence.Transient;

/**
 * 系統实体抽象类
 * 用于记录操作日志
 */
public abstract class AbstractEntity{
	
	/**
	 * 用于记录当前对象的
	 */
	@Transient
	public abstract Serializable getId();	
	
	/**
	 * 用于记录对象的详细情况
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	


}
