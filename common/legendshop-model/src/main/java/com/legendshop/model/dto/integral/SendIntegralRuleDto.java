package com.legendshop.model.dto.integral;

import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.util.AppUtils;


/**
 * 赠送积分参数信息
 * @author Tony
 *
 */
public class SendIntegralRuleDto {
	
	private  String userId;  //赠送的用户
	
	private Double consumerAmount; //消费总金额
	
	private SendIntegralRuleEnum ruleEnum; //赠送积分规则类型
	
	
	public SendIntegralRuleDto() {
	}

	public SendIntegralRuleDto(String userId, SendIntegralRuleEnum ruleEnum) {
		super();
		this.userId = userId;
		this.ruleEnum = ruleEnum;
	}

	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public SendIntegralRuleEnum getRuleEnum() {
		return ruleEnum;
	}

	public void setRuleEnum(SendIntegralRuleEnum ruleEnum) {
		this.ruleEnum = ruleEnum;
	}
	
	public Double getConsumerAmount() {
		return consumerAmount;
	}

	public void setConsumerAmount(Double consumerAmount) {
		this.consumerAmount = consumerAmount;
	}
	
	public boolean validate(){
		if(AppUtils.isBlank(userId) ||
				AppUtils.isBlank(ruleEnum)){
			return false;
		}
		return true;
	}

}
