package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *移动端店铺主页装修
 */
@Entity
@Table(name = "ls_shop_app_decorate")
public class ShopAppDecorate implements GenericEntity<Long> {

	/**  */
	private static final long serialVersionUID = -4254006886360460340L;

	/** 主键ID */
	private Long id; 
		
	/** 所属店铺ID */
	private Long shopId; 
		
	/** 可编辑的装修数据 */
	private String data; 
		
	/** 已上线的装修数据 */
	private String releaseData; 
		
	/** 状态 [-2：后台下线 -1：下线 0:已修改未上线 1:上线 ]参考ShopAppDecorateStatusEnum */
	private Integer status; 
		
	/** 创建/修改时间 */
	private Date recDate; 
	
	/** 店铺名称  */
	private String siteName;
		
	
	public ShopAppDecorate() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_APP_DECORATE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "data")
	public String  getData(){
		return data;
	} 
		
	public void setData(String data){
			this.data = data;
		}
		
    @Column(name = "release_data")
	public String  getReleaseData(){
		return releaseData;
	} 
		
	public void setReleaseData(String releaseData){
			this.releaseData = releaseData;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}

	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	

} 
