package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import com.legendshop.dao.support.GenericEntity;

/**
 *团购活动参加商品
 */
@Entity
@Table(name = "ls_group_sku")
public class GroupSku implements GenericEntity<Long> {

	/** 主键 */
	private Long id; 
		
	/** 团购活动id */
	private Long groupId; 
		
	/** 商家id */
	private Long shopId; 
		
	/** 商品id */
	private Long prodId; 
		
	/** 商品skuId */
	private Long skuId; 
		
	/** 团购价格 */
	private java.math.BigDecimal groupPrice; 
		
	
	public GroupSku() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "GROUP_SKU_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "group_id")
	public Long  getGroupId(){
		return groupId;
	} 
		
	public void setGroupId(Long groupId){
			this.groupId = groupId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "sku_id")
	public Long  getSkuId(){
		return skuId;
	} 
		
	public void setSkuId(Long skuId){
			this.skuId = skuId;
		}
		
    @Column(name = "group_price")
	public java.math.BigDecimal  getGroupPrice(){
		return groupPrice;
	} 
		
	public void setGroupPrice(java.math.BigDecimal groupPrice){
			this.groupPrice = groupPrice;
		}
	


} 
