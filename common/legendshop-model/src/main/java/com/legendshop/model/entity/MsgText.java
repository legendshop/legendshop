/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * The Class MsgText.
 */
@Entity
@Table(name = "ls_msg_text")
public class MsgText implements GenericEntity<Long>{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5658637962382603843L;

	/** The text_id. */
	private Long id;
	
	/** The title. */
	private String title;
	
	/** The text. */
	private String text;
	
	/** The rec date. */
	private Date recDate;

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MSG_TEXT_SEQ")
	public Long getId() {
		return id;
	}


	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	@Column(name = "text")
	public String getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the rec date.
	 *
	 * @return the rec date
	 */
	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	/**
	 * Sets the rec date.
	 *
	 * @param recDate the new rec date
	 */
	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}


	public void setId(Long id) {
		this.id = id;
	}



	
}
