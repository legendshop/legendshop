package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 任务调度服务表
 */
@Entity
@Table(name = "ls_schedule_list")
public class ScheduleList implements GenericEntity<Long> {

	private static final long serialVersionUID = -1369692632845457667L;

	/**  */
	private Long id;
	

	/** 业务类型  */
	private Integer scheduleType;

	/** 业务流水[订单/退款/活动Id..]  */
	private String scheduleSn;

	/** 商家ID */
	private Long shopId;

	/** 触发任务时间 */
	private Date triggerTime;

	/** 创建时间 */
	private Date createTime;

	/** 执行状态[0:未执行;1:执行;-1:失效]  ScheduleStatusEnum */
	private Integer status;

	/** 业务的json数据  */
	private String scheduleJson;
	

	public ScheduleList() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SCHEDULE_LIST_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "schedule_type")
	public Integer getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}

	@Column(name = "schedule_sn")
	public String getScheduleSn() {
		return scheduleSn;
	}

	public void setScheduleSn(String scheduleSn) {
		this.scheduleSn = scheduleSn;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "trigger_time")
	public Date getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "schedule_json")
	public String getScheduleJson() {
		return scheduleJson;
	}

	public void setScheduleJson(String scheduleJson) {
		this.scheduleJson = scheduleJson;
	}


	

}
