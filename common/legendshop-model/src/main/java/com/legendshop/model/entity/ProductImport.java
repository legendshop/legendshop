package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.util.AppUtils;
import com.opencsv.bean.CsvBindByPosition;

/**
 * 商品导入表
 */
@Entity
@Table(name = "ls_prod_imp")
public class ProductImport implements GenericEntity<Long> {

	private static final long serialVersionUID = 5112947369124722162L;

	/** 主键 */
	private Long id;

	/** 商城Id */
	private Long shopId;

	/** 商品名字 */
	@CsvBindByPosition(position = 0)
	private String prodName;

	/** 简要描述,卖点等 */
	@CsvBindByPosition(position = 57)
	private String brief;


	/** 原价 */
	@CsvBindByPosition(position = 7)
	private Double price;

	/** 现价 */
	private Double cash;

	/** 关键字 */
	private String keyWord;

	/** 商品SEOTitle */
	private String metaTitle;

	/** 商品SEO desc */
	private String metaDesc;

	/** 是否已经发布，1：是，0：否 */
	private int status;

	/** 库存警告 */
	@CsvBindByPosition(position = 17)
	private Long stocksArm;

	/** 商品条形码 */
	@CsvBindByPosition(position = 54)
	private String modelId;

	/** 商家编码 */
	@CsvBindByPosition(position = 33)
	private String partyCode;

	/** 修改时间 */
	private Date modifyDate;

	/** 发布时间 */
	private Date recDate;
	
	/** 转化结果 */
	private String convertResult = null;

	public ProductImport() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_IMP_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "prod_name")
	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	@Column(name = "brief")
	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "cash")
	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	@Column(name = "key_word")
	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	@Column(name = "meta_title")
	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	@Column(name = "meta_desc")
	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "stocks_arm")
	public Long getStocksArm() {
		return stocksArm;
	}

	public void setStocksArm(Long stocksArm) {
		this.stocksArm = stocksArm;
	}

	@Column(name = "model_id")
	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	@Column(name = "party_code")
	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

  @Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Transient
	public String getConvertResult() {
		return convertResult;
	}

	public void setConvertResult(String convertResult) {
		this.convertResult = convertResult;
	}
	
	/**
	 * 检查数据是否合法
	 * 
	 * @return
	 */
	public void checkData() {
		StringBuffer sb = new StringBuffer();
		if (AppUtils.isBlank(this.getProdName())){
			sb.append("商品名称为空、");
		}
		if(AppUtils.isBlank(this.getBrief())){
			sb.append("商品副标题为空、");
		}
		if(AppUtils.isBlank(this.getPrice())){
			sb.append("商品原价为空、");
		}
		if(AppUtils.isBlank(this.getCash())) {
			sb.append("商品现价为空、");
		}
		if (AppUtils.isNotBlank(this.getProdName()) && this.getProdName().length() > 300){
			sb.append("商品名称长度过长、");
		}
		if(AppUtils.isNotBlank(this.getBrief()) && this.getBrief().length() > 500) {
			sb.append("商品副标题长度过长、");
		}
		if(AppUtils.isNotBlank(this.getPrice()) && this.getPrice() <= 0){
			sb.append("商品原价不合法、");
		} 
		if(AppUtils.isNotBlank(this.getCash()) && this.getCash() <= 0){
			sb.append("商品现价不合法、");
		}
		if(sb.length() > 0){
			sb.insert(0, "您导入的表格包含了 ");
			String str = sb.toString();
			if(str.endsWith("、")){
				str = str.substring(0, str.length() - 1) + " 的数据!";
			}
			this.setConvertResult(str);
		}
	}

	@Override
	public String toString() {
		return "ProductImport [id=" + id + ", shopId=" + shopId + ", prodName=" + prodName + ", brief=" + brief + ", price=" + price
				+ ", cash=" + cash + ", keyWord=" + keyWord + ", metaTitle=" + metaTitle + ", metaDesc=" + metaDesc + ", status=" + status
				+ ", stocksArm=" + stocksArm + ", modelId=" + modelId + ", partyCode=" + partyCode + ", modifyDate=" + modifyDate
				+ ", recDate=" + recDate + ", convertResult=" + convertResult + "]";
	}


}
