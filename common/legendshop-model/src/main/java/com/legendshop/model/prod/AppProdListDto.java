package com.legendshop.model.prod;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;


@ApiModel("礼品列表Dto")
public class AppProdListDto implements Serializable{
	
	private static final long serialVersionUID = 4812767659000542767L;
	
	/** 礼品列表 */
	private List<AppProdDto> resultList;
	
	/** 每页显示条数 */
	private Long pageCount;
	
	/** 当前页码 */
	private Integer currPage;
	
	/** 总页数 */
	private Long totalCount;

	
	public List<AppProdDto> getResultList() {
		return resultList;
	}

	@Override
	public String toString() {
		return "AppProdListDto [resultList=" + resultList + ", pageCount=" + pageCount + ", currPage=" + currPage
				+ ", totalCount=" + totalCount + "]";
	}

	public void setResultList(List<AppProdDto> resultList) {
		this.resultList = resultList;
	}

	public Long getPageCount() {
		return pageCount;
	}

	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
