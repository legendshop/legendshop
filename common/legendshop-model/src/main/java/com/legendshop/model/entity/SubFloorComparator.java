package com.legendshop.model.entity;

import java.io.Serializable;
import java.util.Comparator;

/**
 * SubFloor排序器
 * @author Administrator
 *
 */
public class SubFloorComparator implements Comparator<SubFloor>,Serializable{

	private static final long serialVersionUID = -2551941306091591908L;

	public int compare(SubFloor o1, SubFloor o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		}else if(o1.getSeq().equals(o2.getSeq())){
			//排序，如果相同的序号，向上排
			return -1;
		} else if (o1.getSeq() < o2.getSeq()) {
			return -1;
		} else {
			return 1;
		}
	}

}
