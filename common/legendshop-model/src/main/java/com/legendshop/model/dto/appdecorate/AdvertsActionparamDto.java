package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.legendshop.util.JSONUtil;


/**
 * 
 * @author Tony
 *
 */
public class AdvertsActionparamDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String,Map<String, String>> searchParam;
	
	public Map<String, Map<String, String>> getSearchParam() {
		return searchParam;
	}

	public void setSearchParam(Map<String, Map<String, String>> searchParam) {
		this.searchParam = searchParam;
	}
	
	public static void main(String[] args) {
		
		Map<String, String> map=new HashMap<String, String>();
		map.put("brandId", "659");
		map.put("brandName", "BOSSsunwen正品专柜手包男包钱包皮包卡包商务S15-230643A1T");
		
		Map<String, String> map2=new HashMap<String, String>();
		map2.put("catId", "161");
		map2.put("catName", "手机电池");
		
		
		Map<String,Map<String, String>> RootMap=new HashMap<String,Map<String, String>>();
		RootMap.put("brands", map);
		RootMap.put("cates", map2);
		
		
		
		
		AdvertsActionparamDto actionparam=new AdvertsActionparamDto();
		actionparam.setSearchParam(RootMap);
		System.out.println(JSONUtil.getJson(actionparam));
		
		String str=JSONUtil.getJson(actionparam);
		AdvertsActionparamDto actionparam2=JSONUtil.getObject(str, AdvertsActionparamDto.class);
		System.out.println(JSONUtil.getJson(actionparam2));
		
	}




}