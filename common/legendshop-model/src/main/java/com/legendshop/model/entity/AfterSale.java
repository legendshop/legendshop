package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *售后服务说明表
 */
@Entity
@Table(name = "ls_after_sale")
public class AfterSale implements GenericEntity<Long> {

	private static final long serialVersionUID = -3629479667451981922L;

	/** ID */
	private Long id; 
	
	/**标题**/
	private String title;
		
	/** 内容 */
	private String content; 
		
	/** 录入时间 */
	private Date recDate; 
		
	/** 用户名称 */
	private String userId; 
		
	/** 用户名称,备用 */
	private String userName; 
	
	   /** The shop id. */
    private Long shopId;
    
    
	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
    @Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
		
	
	public AfterSale() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "AFTER_SALE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	


} 
