package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * WAP头条新闻表
 */
@Entity
@Table(name = "ls_headline")
@ApiModel(value="Headline文章Dto") 
public class Headline implements GenericEntity<Long> {

	private static final long serialVersionUID = -7180026294050256476L;

	/** 主键 */
	@ApiModelProperty(value="主键") 
	private Long id; 
		
	/** 标题 */
	@ApiModelProperty(value=" 标题") 
	private String title; 
		
	/** 详情 */
	@ApiModelProperty(value="详情") 
	private String detail; 
		
	/** 图片 */
	@ApiModelProperty(value="图片") 
	private String pic; 
		
	/** 添加时间 */
	@ApiModelProperty(value="添加时间") 
	private Date addTime; 
		
	/** 顺序 */
	@ApiModelProperty(value="顺序") 
	private Long seq; 
		
	
	public Headline() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "HEADLINE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "detail")
	public String  getDetail(){
		return detail;
	} 
		
	public void setDetail(String detail){
			this.detail = detail;
		}
		
    @Column(name = "pic")
	public String  getPic(){
		return pic;
	} 
		
	public void setPic(String pic){
			this.pic = pic;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}
	


} 
