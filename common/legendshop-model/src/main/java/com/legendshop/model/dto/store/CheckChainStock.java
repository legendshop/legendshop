/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.store;

import java.util.List;

/**
 * 检查库存
 */
public class CheckChainStock {

	/** 状态 */
	private String state;
	
	/** 商品 */
	private List<String> product;

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the product.
	 *
	 * @return the product
	 */
	public List<String> getProduct() {
		return product;
	}

	/**
	 * Sets the product.
	 *
	 * @param product the product
	 */
	public void setProduct(List<String> product) {
		this.product = product;
	}
	
	
	
}
