package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *商品类型表
 */
@Entity
@Table(name = "ls_prod_type")
public class ProdType implements GenericEntity<Long> {

	private static final long serialVersionUID = -1895488338533797162L;

	/** 主键 */
	private Long id; 
		
	/** 产品类型名称 */
	private String name; 
		
	/**  */
	private Long sequence; 
		
	/** 记录时间 */
	private Date recDate; 
		
	/** 状态 */
	private Integer status; 
	
	/** 是否允许用户在前台编辑属性 **/
	private Integer attrEditable; 
		
	/** 是否允许用户在前台编辑参数 **/
	private Integer paramEditable; 
	
	public ProdType() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_TYPE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "sequence")
	public Long  getSequence(){
		return sequence;
	} 
		
	public void setSequence(Long sequence){
			this.sequence = sequence;
		}
		
    @Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate;
	} 
		
	public void setRecDate(Date recDate){
			this.recDate = recDate;
		}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "attr_editable")
	public Integer getAttrEditable() {
		return attrEditable;
	}

	public void setAttrEditable(Integer attrEditable) {
		this.attrEditable = attrEditable;
	}
	
	@Column(name = "param_editable")
	public Integer getParamEditable() {
		return paramEditable;
	}

	public void setParamEditable(Integer paramEditable) {
		this.paramEditable = paramEditable;
	}

} 
