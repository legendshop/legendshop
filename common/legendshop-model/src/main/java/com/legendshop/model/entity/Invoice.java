package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发票
 */
@Entity
@Table(name = "ls_invoice")
@ApiModel(value="Invoice发票") 
public class Invoice implements GenericEntity<Long>{

	private static final long serialVersionUID = 5040388246649323123L;

	/** ID */
	@ApiModelProperty(value="发票id") 
	private Long id;
	
	/** 发票类型 */
	@ApiModelProperty(value="发票类型，1为普通发票 2位增值税发票")
	private Integer type;
	
	/** 普通发票类型，1为个人，2为单位 */
	@ApiModelProperty(value="普通发票类型，1为个人，2为单位")
	private Integer title;
	
	/** 普通个人：发票抬头信息 普通公司：公司名称 增值税发票：单位名称 */
	@ApiModelProperty(value="普通个人：发票抬头信息 普通公司：公司名称 增值税发票：单位名称")
	private String company;
	
	/** 发票内容 */
	@ApiModelProperty(value="发票内容,1为不开发票，2为商品明细")
	private Integer content;
	
	/** 用户id */
	@ApiModelProperty(value="用户id") 
	private String userId;
	
	/** 用户名 */
	@ApiModelProperty(value="用户名") 
	private String userName;
	
	/** 创建时间 */
	@ApiModelProperty(value="创建时间") 
	private Date createTime;
	
	/** 默认使用 */
	@ApiModelProperty(value="默认使用,1为默认") 
	private Integer commonInvoice;
	
	/** 纳税人号 */
	@ApiModelProperty(value="纳税人号") 
	private String invoiceHumNumber;

	/** 注册地址（增值税发票） */
	private String registerAddr;

	/** 注册电话（增值税发票） */
	private String registerPhone;

	/** 开户银行（增值税发票） */
	private String depositBank;

	/** 开户银行账号（增值税发票） */
	private String bankAccountNum;

	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INVOICE_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "title")
	public Integer getTitle() {
		return title;
	}

	public void setTitle(Integer title) {
		this.title = title;
	}

	@Column(name = "content")
	public Integer getContent() {
		return content;
	}

	public void setContent(Integer content) {
		this.content = content;
	}

	@Column(name = "common_invoice")
	public Integer getCommonInvoice() {
		return commonInvoice;
	}

	public void setCommonInvoice(Integer commonInvoice) {
		this.commonInvoice = commonInvoice;
	}

	@Column(name = "invoice_hum_number")
	public String getInvoiceHumNumber() {
		return invoiceHumNumber;
	}

	public void setInvoiceHumNumber(String invoiceHumNumber) {
		this.invoiceHumNumber = invoiceHumNumber;
	}

	@Column(name = "register_addr")
	public String getRegisterAddr() {
		return registerAddr;
	}

	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}

	@Column(name = "register_phone")
	public String getRegisterPhone() {
		return registerPhone;
	}

	public void setRegisterPhone(String registerPhone) {
		this.registerPhone = registerPhone;
	}

	@Column(name = "deposit_bank")
	public String getDepositBank() {
		return depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	@Column(name = "bank_account_num")
	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}
}
