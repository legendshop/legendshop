package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 包邮活动实体
 */
@Entity
@Table(name = "ls_shipping_active")
public class ShippingActive implements GenericEntity<Long> {

	private static final long serialVersionUID = 419876663248622009L;

	/** 活动ID */
	private Long id;

	/** 商家ID */
	private Long shopId;

	/** 活动名称 */
	private String name;

	/** 状态有效:1 下线：0 */
	private Boolean status;

	/** 店铺:0, 商品:1 */
	private Integer fullType;

	/** 类型1：元 2 件 **/
	private Integer reduceType;

	/** 满足的金额或件数 full_value ***/
	private Double fullValue;

	/** 优惠信息，用于活动列表展示 */
	private String description;

	/** 开始时间 */
	private Date startDate;

	/** 结束时间 */
	private Date endDate;

	/** 创建时间 */
	private Date createDate;
	
	private Long [] prodIds;//活动商品Id

	/** 搜索类型 */
	private String searchType;

	/** 来源（PC,APP），保存接口不兼容PC和APP，用这个来判断 **/
	private String source;

	public ShippingActive() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHIPPING_ACTIVE_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "status")
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name = "full_type")
	public Integer getFullType() {
		return fullType;
	}

	public void setFullType(Integer fullType) {
		this.fullType = fullType;
	}

	@Column(name = "reduce_type")
	public Integer getReduceType() {
		return reduceType;
	}

	public void setReduceType(Integer reduceType) {
		this.reduceType = reduceType;
	}

	@Column(name = "full_value")
	public Double getFullValue() {
		return fullValue;
	}

	public void setFullValue(Double fullValue) {
		this.fullValue = fullValue;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public Long[] getProdIds() {
		return prodIds;
	}

	public void setProdIds(Long[] prodIds) {
		this.prodIds = prodIds;
	}

	@Transient
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	@Transient
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
