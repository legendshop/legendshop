/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 预售订单支付状态
 * @Description 
 * @author 关开发
 */
public enum PresellOrderPayStatusEnum implements IntegerEnum{
	/** 未支付订金 */
	UPAY_DEPOSOIT(0),
	
	/** 已支付订金 ,如果是全额支付也是表示已支付订金*/
	PAYED_DEPOSIT(1),
	
	/** 未支付尾款 */
	UPAY_FINAL(0),
	
	/** 已支付尾款 */
	PAYED_FINAL(1);
	
	private Integer num;

	@Override
	public Integer value() {

		return num;
	}

	/**
	 * @param num
	 */
	private PresellOrderPayStatusEnum(Integer num) {
		this.num = num;
	}
	
}
