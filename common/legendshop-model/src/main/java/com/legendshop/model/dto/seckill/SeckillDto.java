/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.seckill;

import java.io.Serializable;
import java.util.Date;

/**
 *秒杀Dto.
 *
 */
public class SeckillDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The sid. */
	private Long sid;

	/** 用户编号. */
	private String userId;

	/** 商家编号. */
	private Long shopId;

	/** 秒杀活动店铺名称. */
	private String shopName;

	/** 活动名称. */
	private String seckillTitle;

	/** 开始时间. */
	private Date startTime;

	/** 结束时间. */
	private Date endTime;

	/** 活动倒数 */
	private String secDesc;
	
	/** 活动摘要 */
	private String secBrief;

	/** 活动状态. */
	private Long secStatus;

	/** 秒杀商品Dto. */
	private SeckillProdDto prodDto;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId
	 *            the shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/**
	 * Gets the shop name.
	 *
	 * @return the shop name
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * Sets the shop name.
	 *
	 * @param shopName
	 *            the shop name
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * Gets the seckill title.
	 *
	 * @return the seckill title
	 */
	public String getSeckillTitle() {
		return seckillTitle;
	}

	/**
	 * Sets the seckill title.
	 *
	 * @param seckillTitle
	 *            the seckill title
	 */
	public void setSeckillTitle(String seckillTitle) {
		this.seckillTitle = seckillTitle;
	}

	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 *
	 * @param startTime
	 *            the start time
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 *
	 * @param endTime
	 *            the end time
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the sid.
	 *
	 * @return the sid
	 */
	public Long getSid() {
		return sid;
	}

	/**
	 * Sets the sid.
	 *
	 * @param sid
	 *            the sid
	 */
	public void setSid(Long sid) {
		this.sid = sid;
	}

	/**
	 * Gets the sec desc.
	 *
	 * @return the sec desc
	 */
	public String getSecDesc() {
		return secDesc;
	}

	/**
	 * Sets the sec desc.
	 *
	 * @param secDesc
	 *            the sec desc
	 */
	public void setSecDesc(String secDesc) {
		this.secDesc = secDesc;
	}

	/**
	 * Gets the sec status.
	 *
	 * @return the sec status
	 */
	public Long getSecStatus() {
		return secStatus;
	}

	/**
	 * Sets the sec status.
	 *
	 * @param secStatus
	 *            the sec status
	 */
	public void setSecStatus(Long secStatus) {
		this.secStatus = secStatus;
	}

	/**
	 * Gets the prod dto.
	 *
	 * @return the prod dto
	 */
	public SeckillProdDto getProdDto() {
		return prodDto;
	}

	/**
	 * Sets the prod dto.
	 *
	 * @param prodDto
	 *            the prod dto
	 */
	public void setProdDto(SeckillProdDto prodDto) {
		this.prodDto = prodDto;
	}

	public String getSecBrief() {
		return secBrief;
	}

	public void setSecBrief(String secBrief) {
		this.secBrief = secBrief;
	}
	
}
