package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum ChangeShopCartStatusEnum implements StringEnum{
	/**
	 * 更改成功
	 */
	OK("OK"),
	
	/** 更改失败 */
	ERR("ERR"),
	
	/** 没有登录*/
	NOT_LOGIN("NOT_LOGIN"),
	
	/** 商品超出购买限制*/
	PROD_RESTRICTION("RESTRICTION"),
	
	/**参数有误 */
	PARAM_ERR("PARAM_ERR")
	;
	
	/** The value. */
	private final String value;

	private ChangeShopCartStatusEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
