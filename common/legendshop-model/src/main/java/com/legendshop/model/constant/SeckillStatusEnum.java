/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.LongEnum;

/**
 * 商城状态
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------.
 */
public enum SeckillStatusEnum implements LongEnum {
	/** 未通过 状态:-2 */
	FAILED(-2l),
	
	/** 审核中 状态:-1 */
	VALIDATING(-1l),

	/** 失效 状态:0 */
	OFFLINE(0l),
	
	/** 进行中（审核通过） 状态:1 */
	ONLINE(1l),

	/** 秒杀活动结束，释放sku 状态:2 */
	FINISH(2l),
	
	/** 转换订单结束 状态:3 */
	TRANSORDER(3l),
	
	/** 秒杀活动支付结束 状态:4 */
	PAYMENT_FINISH(4l);

	/** The num. */
	private Long num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.LongEnum#value()
	 */
	public Long value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	SeckillStatusEnum(Long num) {
		this.num = num;
	}

}
