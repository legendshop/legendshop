package com.legendshop.model.entity.integral;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 会员积分兑换订单
 */
@Entity
@Table(name = "ls_integral_order")
public class IntegralOrder implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 自增编号 */
	private Long id; 
		
	/** 订单流水号 */
	private String orderSn; 
		
	/** 用户ID */
	private String userId; 
		
	/** 用户名称 */
	private String userName; 
		
	/** 订单商品总数 */
	private Integer productNums; 
		
	/** 积分总数 */
	private Integer integralTotal; 
		
	/** 配送方式 */
	private Long dvyTypeId; 
		
	/** 物流单号 */
	private String dvyFlowId; 
		
	/** 0(默认):已兑换;1:已发货;2:已收货;3已完成;5已取消 */
	private Integer orderStatus; 
		
	/** 用户订单地址Id */
	private Long addrOrderId; 
		
	/** 添加时间 */
	private Date addTime; 
		
	/** 物流时间 */
	private Date dvyTime; 
		
	/** 完成时间 */
	private Date finallyTime; 
		
	/** 订单备注 */
	private String orderDesc; 
		
	
	public IntegralOrder() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INTEGRAL_ORDER_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "order_sn")
	public String  getOrderSn(){
		return orderSn;
	} 
		
	public void setOrderSn(String orderSn){
			this.orderSn = orderSn;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "product_nums")
	public Integer  getProductNums(){
		return productNums;
	} 
		
	public void setProductNums(Integer productNums){
			this.productNums = productNums;
		}
		
    @Column(name = "integral_total")
	public Integer  getIntegralTotal(){
		return integralTotal;
	} 
		
	public void setIntegralTotal(Integer integralTotal){
			this.integralTotal = integralTotal;
		}
		
    @Column(name = "dvy_type_id")
	public Long  getDvyTypeId(){
		return dvyTypeId;
	} 
		
	public void setDvyTypeId(Long dvyTypeId){
			this.dvyTypeId = dvyTypeId;
		}
		
    @Column(name = "dvy_flow_id")
	public String  getDvyFlowId(){
		return dvyFlowId;
	} 
		
	public void setDvyFlowId(String dvyFlowId){
			this.dvyFlowId = dvyFlowId;
		}
		
    @Column(name = "order_status")
	public Integer  getOrderStatus(){
		return orderStatus;
	} 
		
	public void setOrderStatus(Integer orderStatus){
			this.orderStatus = orderStatus;
		}
		
    @Column(name = "addr_order_id")
	public Long  getAddrOrderId(){
		return addrOrderId;
	} 
		
	public void setAddrOrderId(Long addrOrderId){
			this.addrOrderId = addrOrderId;
		}
		
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    @Column(name = "dvy_time")
	public Date  getDvyTime(){
		return dvyTime;
	} 
		
	public void setDvyTime(Date dvyTime){
			this.dvyTime = dvyTime;
		}
		
    @Column(name = "finally_time")
	public Date  getFinallyTime(){
		return finallyTime;
	} 
		
	public void setFinallyTime(Date finallyTime){
			this.finallyTime = finallyTime;
		}
		
    @Column(name = "order_desc")
	public String  getOrderDesc(){
		return orderDesc;
	} 
		
	public void setOrderDesc(String orderDesc){
			this.orderDesc = orderDesc;
		}
	


} 
