/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;

/**
 * 到货通知发送信息载体
 */
public class SendArrivalInform implements Serializable {
	
	private static final long serialVersionUID = 8851518418671491576L;
	
	private Long skuId;
	//库存
	private Long stocks;
	//商店名
	private String shopName;
	
	public SendArrivalInform(long skuId,long stocks,String shopName){
		this.setSkuId(skuId);
		this.setStocks(stocks);
		this.setShopName(shopName);
	}
	
	public Long getSkuId() {
		return skuId;
	}
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	public Long getStocks() {
		return stocks;
	}
	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
}