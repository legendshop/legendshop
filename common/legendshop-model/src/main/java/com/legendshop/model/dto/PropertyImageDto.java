/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
/**
 * 属性图片Dto.
 */
public class PropertyImageDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3410113168722578280L;
	
	private Long id;
	
	/** The img url. */
	private String imgUrl;
	
	/** The seq. */
	private Integer seq;
	
	
	/**
	 * Gets the img url.
	 *
	 * @return the img url
	 */
	public String getImgUrl() {
		return imgUrl;
	}
	
	/**
	 * Sets the img url.
	 *
	 * @param imgUrl the new img url
	 */
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}
	
	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
