package com.legendshop.model.dto.weixin;


/**
 * 微信分组信息
 * 
 * @author tony
 * 
 */
public class WeiXinGroups{

	private String name;

	private String id;

	private String count;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
