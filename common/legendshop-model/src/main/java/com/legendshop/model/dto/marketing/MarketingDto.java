package com.legendshop.model.dto.marketing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.util.AppUtils;

/**
 * 
 * 参与营销活动的商品和规格集合
 *
 */
public class MarketingDto implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 营销活动编号 */
	private Long marketId; 
		
	/** 活动名称 */
	private String marketName; 
		
	/** 活动开始时间 */
	private Date startTime; 
		
	/** 活动结束时间 */
	private Date endTime; 
		
	/** 店铺编号 */
	private Long shopId;
	
	/** 商品Id */
	private Long prodId;
	
	/** SkuId */
	private Long skuId;
	
	private Date createTime;
		
	/** 0: 满减促销 ; 1: 满折促销 2：限时促销 */
	private Integer type; 
		
	/** 是否全部商品[全店] 部分商品[需要添加活动商品] */
	private Integer isAllProds;
	
	/** 营销规则Dto **/
	private List<MarketingRuleDto> marketingRuleDtos;
	
	/** 参与活动的商品 **/ 
	private List<MarketingProdDto> marketingProdDtos;
	
	/** 命中活动的商品 **/ 
	private List<ShopCartItem> hitCartItems;
	
	/** 命中的商品总金额 **/ 
	private Double hitProdTotalPrice = 0d;
	
	/** 命中的商品总数 **/ 
	private int hitProdCount = 0;
	
	/** 营销活动减掉的总金额 **/ 
	private Double totalOffPrice = 0d;
	
	/** 促销信息备注 **/ 
	private String promotionInfo;
	
	private boolean isRegionalSales;
	
	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsAllProds() {
		return isAllProds;
	}

	public void setIsAllProds(Integer isAllProds) {
		this.isAllProds = isAllProds;
	}


	public List<MarketingRuleDto> getMarketingRuleDtos() {
		return marketingRuleDtos;
	}

	public void setMarketingRuleDtos(List<MarketingRuleDto> marketingRuleDtos) {
		this.marketingRuleDtos = marketingRuleDtos;
	}
	
	public void addRuleList(MarketingRuleDto rule) {
		if(marketingRuleDtos == null){
			marketingRuleDtos = new ArrayList<MarketingRuleDto>();
		}
		marketingRuleDtos.add(rule);
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((marketId == null) ? 0 : marketId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarketingDto other = (MarketingDto) obj;
		if (marketId == null) {
			if (other.marketId != null)
				return false;
		} else if (!marketId.equals(other.marketId))
			return false;
		return true;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public List<MarketingProdDto> getMarketingProdDtos() {
		return marketingProdDtos;
	}
	
	
	public void addProdList(MarketingProdDto rule) {
		if(marketingProdDtos == null){
			marketingProdDtos = new ArrayList<MarketingProdDto>();
		}
		marketingProdDtos.add(rule);
	}
	

	public void setMarketingProdDtos(List<MarketingProdDto> marketingProdDtos) {
		this.marketingProdDtos = marketingProdDtos;
	}

	public List<ShopCartItem> getHitCartItems() {
		return hitCartItems;
	}

	public void setHitCartItems(List<ShopCartItem> hitCartItem) {
		this.hitCartItems = hitCartItem;
	}
	
	@Override
	public Long getId() {
		return marketId;
	}

	@Override
	public void setId(Long id) {
		marketId=id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Double getHitProdTotalPrice() {
		return hitProdTotalPrice;
	}

	public void setHitProdTotalPrice(Double hitProdTotalPrice) {
		this.hitProdTotalPrice = hitProdTotalPrice;
	}

	public Double getTotalOffPrice() {
		return totalOffPrice;
	}

	public void setTotalOffPrice(Double totalOffPrice) {
		this.totalOffPrice = totalOffPrice;
	}

	public String getPromotionInfo() {
		return promotionInfo;
	}

	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}

	public int getHitProdCount() {
		return hitProdCount;
	}

	public void setHitProdCount(int hitProdCount) {
		this.hitProdCount = hitProdCount;
	}

	/**
	 * 是否含有限售的商品
	 * @return
	 */
/*	public boolean getIsRegionalSales() {
		boolean config=false;
		if(AppUtils.isNotBlank(hitCartItems)){
			for (ShopCartItem shopCartItem : hitCartItems) {
				if(shopCartItem.getStocks()==-1){
					config=true;
					break;
				}
			}
		}
		return config;
	}*/

	public void setRegionalSales(boolean isRegionalSales) {
		this.isRegionalSales = isRegionalSales;
	}

	public boolean isRegionalSales() {
		boolean config=false;
		if(AppUtils.isNotBlank(hitCartItems)){
			for (ShopCartItem shopCartItem : hitCartItems) {
				if(shopCartItem.getStocks()==-1){
					config=true;
					break;
				}
			}
		}
		return config;
	}
}
