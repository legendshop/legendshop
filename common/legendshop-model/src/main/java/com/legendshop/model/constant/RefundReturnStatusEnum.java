/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 退款及退货的状态
 */
public enum RefundReturnStatusEnum implements IntegerEnum {
	
	
	/* 退款关闭 */
	CANDEL_APPLY(-2),
	
	/* 撤销退款 */
	UNDO_APPLY(-1),

/** ------ 卖家处理状态 ------ */
	
	/** 卖家待审核状态 */
	SELLER_WAIT_AUDIT(1), 
	/** 卖家同意 */
	SELLER_AGREE(2), 
	/** 卖家不同意 */
	SELLER_DISAGREE(3), 

/** ------ 申请状态 ------ */
	
	/** 待卖家处理 **/
	APPLY_WAIT_SELLER(1),
	/** 待管理员处理 **/
	APPLY_WAIT_ADMIN(2),
	/** 已完成 **/
	APPLY_FINISH(3),
	
/** ------ 物流状态 ------ */
	
	/** 待发货 */
	LOGISTICS_WAIT_DELIVER(1),
	/** 待收货 */
	LOGISTICS_WAIT_RECEIVE(2),
	/** 未收到 */
	LOGISTICS_UNRECEIVED(3),
	/** 已收货 */
	LOGISTICS_RECEIVED(4),

/** ------ 订单的退款退货状态 ------ */
	/** 默认,代表没有发起退款,退货 */
	ORDER_NO_REFUND(0),
	/** 处理中 1*/
	ORDER_REFUND_PROCESSING(1),
	/** 处理完成 2*/
	ORDER_REFUND_FINISH(2),
	
/** ------ 订单项的退款退货状态 ------ */
	ITEM_NO_REFUND(0),
	/** 处理中 */
	ITEM_REFUND_PROCESSING(1),
	/** 处理完成 */
	ITEM_REFUND_FINISH(2),
	
	
	
/**------- 退款单 is_handle_success 的处理退款状态: 0:退款处理中 1:退款成功 -1:退款失败----------**/
	/** 处理中 1*/
	HANDLE_PROCESSS(0),
	
	/** 退款成功*/
	HANDLE_SUCCESS(1),
	
	/***退款失败**/
	HANDLE_FAIL(-1),
	
/** ------ 订金的退款退货状态 ------ */
	/**不需要退订金，无需处理**/
	DEPOSIT_NO_REFUND(0),
	
	/** 需要退订金，处理中 */
	DEPOSIT_REFUND_PROCESSING(1),
	
	/**订金退款成功*/
	DEPOSIT_REFUND_FINISH(2),
	
	/**订金退款失败*/
	DEPOSIT_REFUND_FALSE(-2)
	
	;

	private Integer status;

	RefundReturnStatusEnum(Integer status) {
		this.status = status;
	}

	@Override
	public Integer value() {
		return status;
	}

}
