package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 记录随机数于session
 *
 */
public class RandNumDto implements Serializable{

	private static final long serialVersionUID = 1112403791804853548L;
	
	/** 随机数 */
	private String randomString;
	
	/** 验证次数 */
	private int num;
	
	public RandNumDto() {
		super();
	}

	public RandNumDto(String randomString){
		this.randomString = randomString;
	}
	
	public String getRandomString() {
		return randomString;
	}

	public int getNum() {
		return num;
	}
	/**
	 * 每次加1
	 */
	public void addNum() {
		this.num++;
	}
	
	@Override
	public String toString() {
		return "RandNumDto [randomString=" + randomString + ", num=" + num + "]";
	}

}
