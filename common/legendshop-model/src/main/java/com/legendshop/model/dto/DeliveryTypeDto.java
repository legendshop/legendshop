package com.legendshop.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 运费模板
 *
 */
@ApiModel(value="DeliveryTypeDto") 
public class DeliveryTypeDto implements Serializable {

	private static final long serialVersionUID = -6885889503830262450L;

	/**运费模板Id. */
	@ApiModelProperty(value="运费模板Id") 
	private Long dvyTypeId;

	/** 物流公司ID. */
	@ApiModelProperty(value="物流公司Id") 
	private Long dvyId;
	
	/** 配送方式名称. */
	@ApiModelProperty(value="配送方式名称") 
	private String name;

	@ApiModelProperty(value="打印模版Id")
	private Long printtempId;

	@ApiModelProperty(value="是否系统配置，如果是系统配置的则商家无法改动") 
	private Integer isSystem;
	
	@ApiModelProperty(value="公司名称") 
	private String company;
	
	@ApiModelProperty(value="打印模版名称") 
	private String printName;
	
	@ApiModelProperty(value="描述") 
	private String notes;
	

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public Long getDvyId() {
		return dvyId;
	}

	public void setDvyId(Long dvyId) {
		this.dvyId = dvyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrinttempId() {
		return printtempId;
	}

	public void setPrinttempId(Long printtempId) {
		this.printtempId = printtempId;
	}

	public Integer getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	

}
