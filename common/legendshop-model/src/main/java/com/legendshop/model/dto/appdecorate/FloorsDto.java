package com.legendshop.model.dto.appdecorate;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 楼层dto
 */
@ApiModel("楼层Dto")
public class FloorsDto implements Serializable, Comparable<FloorsDto>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 文本  */
	@ApiModelProperty(value = "文本")
	private String text;
	
	/** 模板  */
	@ApiModelProperty(value = "模板")
	private String template;
	
	/** 广告位列表 */
	@ApiModelProperty(value = "广告位列表")
	private List<AdvertsDto> adverts;
	
	/** 主题  */
	@ApiModelProperty(value = "主题")
	private String themecode;
	
	/** 模板轮播图列表 */
	@ApiModelProperty(value = "模板轮播图列表")
	private List<BannersDto> banners;
	
	/** 背景色 */
	@ApiModelProperty(value = "背景色")
	private String backgroundcolor;
	
	/** 第二个颜色 */
	@ApiModelProperty(value = "第二个颜色")
	private String secondcolor;
	
	/** action参数: 跳转action带的参数 */
	@ApiModelProperty(value = "action参数: 跳转action带的参数")
	private AdvertsActionparamDto actionParam;
	
	/** 顺序 */
	@ApiModelProperty(value = "顺序")
	private int ordering;
	
	/** 动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题 */
	@ApiModelProperty(value = "动作类型, GoodsList: 商品类目或品牌, 也就是根据类目或品牌跳转商品列表; GoodsDetail: 单个商品, 跳转商品详情; Theme1: 专题")
	private String action;
	
	/** 唯一标识id */
	@ApiModelProperty(value = "唯一标识id")
	private String id;
	
	/** 查看更多 */
	@ApiModelProperty(value = "查看更多")
	private String lookMore;
	
	/** 内容 */
	@ApiModelProperty(value = "内容")
	private String content;

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getTemplate() {
		return template;
	}

	public void setAdverts(List<AdvertsDto> adverts) {
		this.adverts = adverts;
	}

	public List<AdvertsDto> getAdverts() {
		return adverts;
	}

	public void setThemecode(String themecode) {
		this.themecode = themecode;
	}

	public String getThemecode() {
		return themecode;
	}

	public void setBackgroundcolor(String backgroundcolor) {
		this.backgroundcolor = backgroundcolor;
	}

	public String getBackgroundcolor() {
		return backgroundcolor;
	}

	public void setSecondcolor(String secondcolor) {
		this.secondcolor = secondcolor;
	}

	public String getSecondcolor() {
		return secondcolor;
	}



	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	public int getOrdering() {
		return ordering;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public List<BannersDto> getBanners() {
		return banners;
	}

	public void setBanners(List<BannersDto> banners) {
		this.banners = banners;
	}

	public AdvertsActionparamDto getActionParam() {
		return actionParam;
	}

	public void setActionParam(AdvertsActionparamDto actionParam) {
		this.actionParam = actionParam;
	}

	public String getLookMore() {
		return lookMore;
	}

	public void setLookMore(String lookMore) {
		this.lookMore = lookMore;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public int compareTo(FloorsDto o) {
		if(this.getOrdering()>o.getOrdering()){
			return 1;
		}else if(this.getOrdering()==o.getOrdering()){
			return 0;
		}
		return -1;
	}


}