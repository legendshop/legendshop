package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 *  举报主题
 *
 */
@Entity
@Table(name = "ls_accu_subject")
public class AccusationSubject implements GenericEntity<Long>{

	private static final long serialVersionUID = -3261686659254152724L;

	// 主键
	private Long asId ; 
		
	// 名称
	private String title ; 
		
	// 状态
	private Long status ; 
		
	// 记录时间
	private Date recDate ; 
		
	// 
	private Long typeId ; 
		
	
	public AccusationSubject() {
    }
		
	@Id
	@Column(name = "as_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ACCU_SUBJECT_SEQ")
	public Long  getAsId(){
		return asId ;
	} 
		
	public void setAsId(Long asId){
		this.asId = asId ;
	}
		
	@Column(name = "title")
	public String  getTitle(){
		return title ;
	} 
		
	public void setTitle(String title){
		this.title = title ;
	}
		
	@Column(name = "status")
	public Long  getStatus(){
		return status ;
	} 
		
	public void setStatus(Long status){
		this.status = status ;
	}
		
	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate ;
	} 
		
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
	@Column(name = "type_id")
	public Long  getTypeId(){
		return typeId ;
	} 
		
	public void setTypeId(Long typeId){
		this.typeId = typeId ;
	}
		
	@Transient
  public Long getId() {
		return 	asId;
	}
	
	public void setId(Long id) {
		this.asId = id;
	}

} 
