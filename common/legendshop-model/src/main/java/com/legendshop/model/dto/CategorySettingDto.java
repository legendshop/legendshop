/*
 *
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 *
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * app装修--基本设置--分类设置
 */
public class CategorySettingDto implements Serializable  {

	/**  */
	private static final long serialVersionUID = -3807412284278243030L;

	/** 分类（一级、二级、三级） */
	private String category;
	
	/** 规则（有图没图） */
	private String schema;

	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	
}
