/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shop;

import java.io.Serializable;


/**
 * 安全验证的商家用户.
 *
 */
public class ShopUserDto implements Serializable {

	private static final long serialVersionUID = 5188775095770681992L;
	
	/** 用户ID */
	private Long sellerId; 
		
	/** 店主卖家用户名 */
	private String sellerName; 
		
	/** 店铺ID */
	private Long shopId; 
		
	/** 商家名字 */
	private String shopName; 
	
	/** 密码 */
	private String password; 
		
	/** 状态 */
	private boolean enabled; 
		
	/** 是否门店用户 */
	private boolean isStoreUser; 

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isStoreUser() {
		return isStoreUser;
	}

	public void setStoreUser(boolean isStoreUser) {
		this.isStoreUser = isStoreUser;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	

}
