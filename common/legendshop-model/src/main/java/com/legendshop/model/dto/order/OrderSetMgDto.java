package com.legendshop.model.dto.order;

import java.io.Serializable;

import com.legendshop.util.AppUtils;

/**
 * 订单设置管理
 * 
 * @author tony
 * 
 */
public class OrderSetMgDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String auto_order_confirm; //自动确认收货时长

	private String auto_order_notice;//自动收货提醒时长

	private String auto_order_cancel;//自动取消未支付订单时长
	
	private String auto_order_comment;//订单评论有效时间
	
	private String auto_product_return;  //退换货有效时间

	private boolean order_integral_enable;

	private int order_integral_percent;
	
	private String auto_return_review; //退款自动审核时间
	
	private String auto_seckill_cancel; //秒杀资格未转订单自动取消时间


	public String getAuto_order_confirm() {
		return auto_order_confirm;
	}

	public void setAuto_order_confirm(String auto_order_confirm) {
		this.auto_order_confirm = auto_order_confirm;
	}

	public String getAuto_order_notice() {
		return auto_order_notice;
	}

	public void setAuto_order_notice(String auto_order_notice) {
		this.auto_order_notice = auto_order_notice;
	}

	public String getAuto_order_cancel() {
		return auto_order_cancel;
	}

	public void setAuto_order_cancel(String auto_order_cancel) {
		this.auto_order_cancel = auto_order_cancel;
	}
	
	public String getAuto_order_comment() {
		return auto_order_comment;
	}

	public void setAuto_order_comment(String auto_order_comment) {
		this.auto_order_comment = auto_order_comment;
	}

	public String getAuto_product_return() {
		return auto_product_return;
	}

	public void setAuto_product_return(String auto_product_return) {
		this.auto_product_return = auto_product_return;
	}

	public boolean getOrder_integral_enable() {
		return order_integral_enable;
	}

	public void setOrder_integral_enable(boolean order_integral_enable) {
		this.order_integral_enable = order_integral_enable;
	}

	public int getOrder_integral_percent() {
		return order_integral_percent;
	}

	public void setOrder_integral_percent(int order_integral_percent) {
		this.order_integral_percent = order_integral_percent;
	}

	public boolean validate(){
		if(AppUtils.isBlank(auto_order_confirm) || AppUtils.isBlank(auto_order_cancel)|| AppUtils.isBlank(order_integral_enable)){
			return false;
		}
		if(order_integral_percent<0 || order_integral_percent>100)
		{
			return false;
		}
		return true;
	}
	
	
	public String getAuto_return_review() {
		return auto_return_review;
	}

	public void setAuto_return_review(String auto_return_review) {
		this.auto_return_review = auto_return_review;
	}

	public String getAuto_seckill_cancel() {
		return auto_seckill_cancel;
	}

	public void setAuto_seckill_cancel(String auto_seckill_cancel) {
		this.auto_seckill_cancel = auto_seckill_cancel;
	}

}
