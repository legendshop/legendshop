package com.legendshop.model.dto.order;

import java.util.Date;

import com.legendshop.dao.support.PageProvider;
import com.legendshop.util.AppUtils;

/**
 * 订单查询的参数
 * 
 * @author tony
 * 
 */
public class OrderSearchParamDto {

	/** 当前页码 */
	private String curPageNO;
	
	/** 每页显示的条数 */
	private Integer pageSize;
	
	/** 订单状态 */
	private Integer status;
	
	/** 订单号 */
	private String subNumber;

	/** 用户名 */
	private String userName;
	
	/** 用户ID */
	private String userId;

	/** 商家ID */
	private Long shopId;
	
	/** 商家名称 */
	private String shopName;
	
	/** 开始时间-用于查询某段时间范围产生的订单 */
	private Date startDate;

	/** 结束时间-用于查询某段时间范围产生的订单 */
	private Date endDate;
	
	/** 订单类型 : 普通订单,团购订单,秒杀订单等等*/
	private String subType;
	
	/** 付款开始时间-用于查询某段时间范围产生的订单 */
	private Date payStartDate;
	
	/** 付款结束时间-用于查询某段时间范围产生的订单 */
	private Date payEndDate;
	
	/** 买家 电话*/
	private String userMobile;

	/** 备注状态*/
	private Integer isShopRemak;
	
	/** 退款状态 */
	private Integer refundStatus;
	
	/** 收货人电话 */
	private String reciverMobile;
	
	/** 收货人名称  */
	private String reciverName;

	public Integer getIsShopRemak() {
		return isShopRemak;
	}

	public void setIsShopRemak(Integer isShopRemak) {
		this.isShopRemak = isShopRemak;
	}

	public Integer getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}

	public Date getPayStartDate() {
		return payStartDate;
	}

	public void setPayStartDate(Date payStartDate) {
		this.payStartDate = payStartDate;
	}

	public Date getPayEndDate() {
		return payEndDate;
	}

	public void setPayEndDate(Date payEndDate) {
		this.payEndDate = payEndDate;
	}

	@Deprecated
	/** 排除查询的状态 */
	private String removeStatus; 
	
	/** 用于缓存 */
	private Integer deleteStatus = 0; 
	
	/** 是否包含用户删除的订单 */
	private boolean includeDeleted = false; 
	
	/** 是否已经支付 */
	private Integer isPayed; 
	
	private boolean loadDelivery = true;
	
	private PageProvider pageProvider;

	private Integer needInvoice;
	
	private Integer switchInvoice;
	
	public String getCurPageNO() {
		if(AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		return curPageNO;
	}

	public void setCurPageNO(String curPageNO) {
		this.curPageNO = curPageNO;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRemoveStatus() {
		return removeStatus;
	}

	public void setRemoveStatus(String removeStatus) {
		this.removeStatus = removeStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public PageProvider getPageProvider() {
		return pageProvider;
	}

	public void setPageProvider(PageProvider pageProvider) {
		this.pageProvider = pageProvider;
	}

	public Integer getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	public boolean isLoadDelivery() {
		return loadDelivery;
	}

	public void setLoadDelivery(boolean loadDelivery) {
		this.loadDelivery = loadDelivery;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public boolean isIncludeDeleted() {
		return includeDeleted;
	}

	public void setIncludeDeleted(boolean includeDeleted) {
		this.includeDeleted = includeDeleted;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Integer getNeedInvoice() {
		return needInvoice;
	}

	public void setNeedInvoice(Integer needInvoice) {
		this.needInvoice = needInvoice;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}

	public String getReciverMobile() {
		return reciverMobile;
	}

	public void setReciverMobile(String reciverMobile) {
		this.reciverMobile = reciverMobile;
	}

	public String getReciverName() {
		return reciverName;
	}

	public void setReciverName(String reciverName) {
		this.reciverName = reciverName;
	}

}
