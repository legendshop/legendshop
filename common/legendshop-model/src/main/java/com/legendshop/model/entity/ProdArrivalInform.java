package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品到货通知
 */
@Entity
@Table(name = "ls_prod_arrival_inform")
@ApiModel("商品到货通知参数")
public class ProdArrivalInform implements GenericEntity<Long>{
	
	private static final long serialVersionUID = 5975387919877435350L;
	
	//商家
	/** 到货通知记录id */
	@ApiModelProperty(value = "到货通知记录id")
	private Long prodArrivalInformId;
	
	/** 商家id */
	@ApiModelProperty(value = "商家id")
	private Long shopId;
	
	/** 商品id */
	@ApiModelProperty(value = "商品id")
	private Long prodId;
	
	/** skuId */
	@ApiModelProperty(value = "skuId")
	private Long skuId;
	
	//消费者
	/** 用户id */
	@ApiModelProperty(value = "用户id")
	private String userId;
	
	/** 用户名 */
	@ApiModelProperty(value = "用户名")
	private String userName;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码")
	private String mobilePhone;
	
	/** 邮箱  */
	@ApiModelProperty(value = "邮箱")
	private String email;
	
	/** 通知状态 */
	@ApiModelProperty(value = "通知状态")
	private int status;
	
	/** 创建时间  */
	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@Id
	@Column(name = "prodArrivalInformId")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_COMM_STAT_SEQ")
	public Long getProdArrivalInformId() {
		return prodArrivalInformId;
	}
	public void setProdArrivalInformId(Long prodArrivalInformId) {
		this.prodArrivalInformId = prodArrivalInformId;
	}
	@Column(name = "skuId")
	public Long getSkuId() {
		return skuId;
	}
	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	@Column(name = "shopId")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Column(name = "prodId")
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name = "userId")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "mobilePhone")
	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
	
	@Transient
	public Long getId() {
		return this.prodArrivalInformId;
	}
	
	public void setId(Long id) {
		this.prodArrivalInformId=id;
	}
}
