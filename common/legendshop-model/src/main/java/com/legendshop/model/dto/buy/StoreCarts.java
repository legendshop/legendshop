package com.legendshop.model.dto.buy;

import java.io.Serializable;
import java.util.List;

/**
 * 每个门店所对应的购物车
 * 按门店分单
 * @author linzh
 * 
 */
public class StoreCarts implements Serializable {


	 /**  */
	private static final long serialVersionUID = 3880886752345928374L;

	/**
     * 门店ID
     */
	private Long storeId;
	
	 /**
     * 门店名称
     */
	private String storeName;

	/**
     * 门店状态
     */
	private Boolean storeStatus; 
	
	/** 门店详细地址 */
	private String storeAddr;
    
    /** 普通商品item */
	private List<ShopCartItem> cartItems;
	
    /**
     * 买家留言
     */
    private String remark;
    
    /** 提货人 */
    private String buyerName;
    
    /** 提货电话  */
    private String telphone;
    
    /** 订单号 */
    private String subNumber;
    
    /** 订单ID */
    private Long subId;
    
    
	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Boolean getStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(Boolean storeStatus) {
		this.storeStatus = storeStatus;
	}

	public List<ShopCartItem> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<ShopCartItem> cartItems) {
		this.cartItems = cartItems;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStoreAddr() {
		return storeAddr;
	}

	public void setStoreAddr(String storeAddr) {
		this.storeAddr = storeAddr;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}
	
}
