package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

public enum DashboardTableEnum implements StringEnum {

	/** 用户表名  **/
	USER_TABLE("ls_usr_detail"),
	
	/** 商家表名 **/
	SHOP_TABLE("ls_shop_detail"),
	
	/** 商品表名**/
	PROD_TABLE("ls_prod"),
	
	/** 订单表名 **/
	SUB_TABLE("ls_sub"),
	
	/** 咨询表名 **/
	CONSULT_TABLE("ls_prod_cons"),
	
	/** 举报表名 **/
	ACCUSATION_TABLE("ls_accusation"),
	
	/** 品牌表名 **/
	BRAND_TABLE("ls_brand");
	
	private final String value;

	private DashboardTableEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
