package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_weixin_token")
public class WeixinToken implements GenericEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**  */
	private Long id; 
		
	/**  */
	private String token; 
		
	/** 过期时长 */
	private Integer expiresIn; 
		
	/** 检验时间 */
	private Date validateTime; 
		
	/** tonken类型 */
	private String tonkenType; 
		
	public WeixinToken() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_TOKEN_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "token")
	public String  getToken(){
		return token;
	} 
		
	public void setToken(String token){
			this.token = token;
		}
		
    @Column(name = "expires_in")
	public Integer  getExpiresIn(){
		return expiresIn;
	} 
		
	public void setExpiresIn(Integer expiresIn){
			this.expiresIn = expiresIn;
		}
		
    @Column(name = "validate_time")
	public Date  getValidateTime(){
		return validateTime;
	} 
		
	public void setValidateTime(Date validateTime){
			this.validateTime = validateTime;
		}
		
    @Column(name = "tonken_type")
	public String  getTonkenType(){
		return tonkenType;
	} 
		
	public void setTonkenType(String tonkenType){
			this.tonkenType = tonkenType;
		}
} 
