/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 直播接口返回值.
 */
@ApiModel("直播接口返回值")
public class ZhiboResultDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1945491395884152235L;
	
	/** 返回状态码， 0是正常 */
	@ApiModelProperty(value = "返回状态码， 0是正常")
	private int errorCode;
	
	/** 返回说明 */
	@ApiModelProperty(value = "返回说明")
	private String errorInfo = "";
	
	/** json字符串. */
	@ApiModelProperty(value = "json字符串")
	private JSONObject data;
	
	/**
	 * Instantiates a new zhibo result dto.
	 */
	public ZhiboResultDto(){
	}


	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public int getErrorCode() {
		return errorCode;
	}


	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


	/**
	 * Gets the error info.
	 *
	 * @return the error info
	 */
	public String getErrorInfo() {
		return errorInfo;
	}


	/**
	 * Sets the error info.
	 *
	 * @param errorInfo the new error info
	 */
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}


	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public JSONObject getData() {
		return data;
	}


	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(JSONObject data) {
		this.data = data;
	}


	@Override
	public String toString() {
		return "ZhiboResultDto [errorCode=" + errorCode + ", errorInfo=" + errorInfo + ", data=" + data + "]";
	}
	
	


}
