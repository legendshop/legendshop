/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.securityCenter;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 安全等级 Enum
 * 
 */
public enum UserSecurityEnum implements IntegerEnum {

	SECURITY_DANGEROUS(1), // 不安全
   
	SECURITY_LESS_DANGEROUS(2), //有点不安全
	
	SECURITY_SAFETY(3), //安全
	
	SECURITY_MORE_SAFETY(4), //比较安全
	
 ;
	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new shop status enum.
	 * 
	 * @param num
	 *            the num
	 */
	UserSecurityEnum(Integer num) {
		this.num = num;
	}

}
