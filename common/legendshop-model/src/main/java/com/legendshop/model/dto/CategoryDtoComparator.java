package com.legendshop.model.dto;

import java.util.Comparator;

public class CategoryDtoComparator implements Comparator<CategoryDto> {

	@Override
	public int compare(CategoryDto o1, CategoryDto o2) {
		if (o1 == null || o2 == null || o1.getSeq() == null || o2.getSeq() == null) {
			return -1;
		} else if (o1.getSeq() <= o2.getSeq()) {
			return -1;
		}else {
			return 1;
		}
	}

}
