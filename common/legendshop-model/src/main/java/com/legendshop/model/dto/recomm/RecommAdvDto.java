package com.legendshop.model.dto.recomm;

import java.io.Serializable;

public class RecommAdvDto implements Serializable{

	private static final long serialVersionUID = -5394940932762390275L;
	
	private String link;
	
	private String advPic;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAdvPic() {
		return advPic;
	}

	public void setAdvPic(String advPic) {
		this.advPic = advPic;
	}
	
	
}
