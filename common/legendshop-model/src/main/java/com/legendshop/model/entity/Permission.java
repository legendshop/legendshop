/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户权限
 */
@Entity
@Table(name = "ls_perm")
public class Permission implements GenericEntity<PerssionId> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6636728502142356224L;
	
	/** The id. */
	private PerssionId id;

	// Constructors

	/**
	 * default constructor.
	 */
	public Permission() {
	}

	public Permission(String roleId, String functionId) {
		id = new PerssionId(roleId, functionId);
	}

	
	/**
	 * full constructor.
	 * 
	 * @param id
	 *            the id
	 */
	public Permission(PerssionId id) {
		this.id = id;
	}

	// Property accessors

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public PerssionId getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(PerssionId id) {
		this.id = id;
	}

}