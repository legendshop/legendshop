package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.ProductImport;
/**
 * 商品导入结果封装
 * @author fqb
 *
 */
public class ProductImportResult implements Serializable{
	
	private static final long serialVersionUID = -5870122999029872928L;
	
		private String message;
		
		private List<ProductImport> result; 
		
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public List<ProductImport> getResult() {
			return result;
		}
		public void setResult(List<ProductImport> result) {
			this.result = result;
		}
	
}
