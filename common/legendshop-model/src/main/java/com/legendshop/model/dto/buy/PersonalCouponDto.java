package com.legendshop.model.dto.buy;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 个人中心优惠券
 * @author legend-15
 *
 */
@ApiModel(value="PersonalCouponDto个人中心优惠券") 
public class PersonalCouponDto implements Serializable{
	private static final long serialVersionUID = -3467082205438589023L;

	/** 用户礼券ID */
	@ApiModelProperty(value="用户礼券ID")  
	private long userCouponId;

	/** 礼券ID */
	@ApiModelProperty(value="礼券ID")  
	private long couponId;
	/** 礼券名称 */
	@ApiModelProperty(value="礼券名称")  
	private String couponName;
	/** 券号 */
	@ApiModelProperty(value="券号")  
	private String couponSn;
	/** 消费者ID */
	@ApiModelProperty(value="消费者ID")  
	private String userId;
	/** 满多少钱可以使用 */
	@ApiModelProperty(value="满多少钱可以使用")  
	private double fullPrice;
	/** 优惠多少钱 */
	@ApiModelProperty(value="优惠多少钱")  
	private double offPrice;
	/** 礼券图片 */
	@ApiModelProperty(value="礼券图片")  
	private String couponPicture;
	/** 领取时间 */
	@ApiModelProperty(value="领取时间")  
	private Date getTime;
	/** 开始时间 */
	@ApiModelProperty(value="开始时间")  
	private Date startDate;
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")  
	private Date endDate;
	/** 使用时间 */
	@ApiModelProperty(value="使用时间")  
	private Date useTime;
	/** 使用状态 */
	@ApiModelProperty(value="使用状态")  
	private int useStatus;
	/** 礼券状态 */
	@ApiModelProperty(value="礼券状态")  
	private int status;
	/** 礼券提供方 */
	@ApiModelProperty(value="礼券提供方")  
	private String couponProvider;
	/** 礼券类型 */
	@ApiModelProperty(value="礼券类型")  
	private String couponType;
	/** 领取方式 */
	@ApiModelProperty(value="领取方式 ")  
	private String getType;
	/** 商家店名 */
	@ApiModelProperty(value="商家店名")  
	private String siteName;
	/** 商家Id */
	@ApiModelProperty(value="商家Id")  
	private Long shopId;
	
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public long getUserCouponId() {
		return userCouponId;
	}
	public void setUserCouponId(long userCouponId) {
		this.userCouponId = userCouponId;
	}
	public long getCouponId() {
		return couponId;
	}
	public void setCouponId(long couponId) {
		this.couponId = couponId;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getCouponSn() {
		return couponSn;
	}
	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getGetTime() {
		return getTime;
	}
	public void setGetTime(Date getTime) {
		this.getTime = getTime;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getUseTime() {
		return useTime;
	}
	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}
	public int getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(int useStatus) {
		this.useStatus = useStatus;
	}
	public String getCouponPicture() {
		return couponPicture;
	}
	public void setCouponPicture(String couponPicture) {
		this.couponPicture = couponPicture;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCouponProvider() {
		return couponProvider;
	}
	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getGetType() {
		return getType;
	}
	public void setGetType(String getType) {
		this.getType = getType;
	}
	public double getFullPrice() {
		return fullPrice;
	}
	public void setFullPrice(double fullPrice) {
		this.fullPrice = fullPrice;
	}
	public double getOffPrice() {
		return offPrice;
	}
	public void setOffPrice(double offPrice) {
		this.offPrice = offPrice;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
}





