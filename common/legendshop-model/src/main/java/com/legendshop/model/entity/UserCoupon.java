package com.legendshop.model.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *用户优惠券
 */
@Entity
@Table(name = "ls_user_coupon")
public class UserCoupon implements GenericEntity<Long> {

	private static final long serialVersionUID = -7597205043773555939L;

	/** 用户礼券ID */
	private Long userCouponId; 
		
	/** 礼券ID */
	private Long couponId; 
		
	/** 礼券名称 */
	private String couponName; 
	
	/** 劵号 */
	private String couponSn; 
	
	/** 卡密 */
	private String couponPwd; 
		
	/** 用户ID */
	private String userId; 
		
	/** 用户名称 */
	private String userName; 
		
	/** 领取时间 */
	private Date getTime; 
		
	/** 使用时间 */
	private Date useTime; 
		
	/** 订单总金额 */
	private Double orderPrice; 
		
	/** 订单编号 */
	private String orderNumber; 
		
	/** 领取来源 */
	private String getSources; 
		
	/** 优惠券使用状态 1:可使用  2:已使用 */
	private Integer useStatus; 
	
	//-----------------华丽丽的分割线-------------------------//
	
	/** 店铺ID**/
	private Long shopId;
	
	/** 劵值满多少金额 */
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	private Double offPrice; 
		
	/** 礼券提供方：平台: platform，店铺:shop */
	private String couponProvider;
	
	/** 分类ID */
	private Long categoryId;
	
	/** 优惠券图片 **/
	private String couponPic;
	
	/**开始时间**/
	private Date startDate;
	
	/** 结束时间*/
	private Date endDate;
	
	/** 礼券的类型 */
	private String couponType; 
	
	/** 店铺名称*/
	private String siteName;
	
	/** 是否选中*/
	private int selectSts = 0;
	
	/**优惠券描述**/
	private String description;
	
	/** 关联的商品ID集合**/
	private List<Long> prodIds = new ArrayList<Long>();
	
	/** 关联的店铺ID集合**/
	private List<Long> shopIds = new ArrayList<Long>();
	
	/** 命中的商品总金额 **/
	private Double hitTotalPrice;
	
	/**判断是否可用*/
	private String flag;
	
	public UserCoupon() {
    }
		
	@Id
	@Column(name = "user_coupon_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "USER_COUPON_SEQ")
	public Long  getUserCouponId(){
		return userCouponId;
	} 
		
	public void setUserCouponId(Long userCouponId){
			this.userCouponId = userCouponId;
		}
		
    @Column(name = "coupon_id")
	public Long  getCouponId(){
		return couponId;
	} 
		
	public void setCouponId(Long couponId){
			this.couponId = couponId;
		}
		
    @Column(name = "coupon_name")
	public String  getCouponName(){
		return couponName;
	} 
		
	public void setCouponName(String couponName){
		this.couponName = couponName;
	}
		
    @Column(name = "coupon_sn")
	public String  getCouponSn(){
		return couponSn;
	} 
		
	public void setCouponSn(String couponSn){
			this.couponSn = couponSn;
	}
		
	@Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "get_time")
	public Date  getGetTime(){
		return getTime;
	} 
		
	public void setGetTime(Date getTime){
			this.getTime = getTime;
		}
		
    @Column(name = "use_time")
	public Date  getUseTime(){
		return useTime;
	} 
		
	public void setUseTime(Date useTime){
			this.useTime = useTime;
		}
		
    @Column(name = "order_price")
	public Double  getOrderPrice(){
		return orderPrice;
	} 
		
	public void setOrderPrice(Double orderPrice){
			this.orderPrice = orderPrice;
		}
		
    @Column(name = "order_number")
	public String  getOrderNumber(){
		return orderNumber;
	} 
		
	public void setOrderNumber(String orderNumber){
			this.orderNumber = orderNumber;
		}
		
    @Column(name = "get_sources")
	public String  getGetSources(){
		return getSources;
	} 
		
	public void setGetSources(String getSources){
			this.getSources = getSources;
		}
		
    @Column(name = "use_status")
	public Integer  getUseStatus(){
		return useStatus;
	} 
		
	public void setUseStatus(Integer useStatus){
			this.useStatus = useStatus;
		}
	
	 @Column(name = "coupon_pwd")
	public String getCouponPwd() {
		return couponPwd;
	}

	public void setCouponPwd(String couponPwd) {
		this.couponPwd = couponPwd;
	}
	
	@Transient
	public Long getId() {
		return userCouponId;
	}
	
	public void setId(Long id) {
		userCouponId = id;
	}
	
	@Transient
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Transient
	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	@Transient
	public Double getOffPrice() {
		return offPrice;
	}

	public void setOffPrice(Double offPrice) {
		this.offPrice = offPrice;
	}

	@Transient
	public String getCouponProvider() {
		return couponProvider;
	}

	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}

	@Transient
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Transient
	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}
	
	@Transient
	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	
	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	@Transient
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Transient
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	 
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null){
			return false;
		}
		if(getClass() != obj.getClass()){
			return false;
		}
		UserCoupon userCoupon = (UserCoupon)obj;
		return this.userCouponId.equals(userCoupon.userCouponId);
	}

	@Transient
	public int getSelectSts() {
		return selectSts;
	}

	public void setSelectSts(int selectSts) {
		this.selectSts = selectSts;
	}

	@Transient
	public List<Long> getProdIds() {
		return prodIds;
	}

	public void setProdIds(List<Long> prodIds) {
		this.prodIds = prodIds;
	}

	@Transient
	public List<Long> getShopIds() {
		return shopIds;
	}

	public void setShopIds(List<Long> shopIds) {
		this.shopIds = shopIds;
	}

	@Transient
	public Double getHitTotalPrice() {
		return hitTotalPrice;
	}

	public void setHitTotalPrice(Double hitTotalPrice) {
		this.hitTotalPrice = hitTotalPrice;
	}

	@Transient
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
} 
