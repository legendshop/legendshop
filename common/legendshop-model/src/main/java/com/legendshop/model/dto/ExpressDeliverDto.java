package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 快递100密钥信息
 *
 */
public class ExpressDeliverDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String key;
	
	private String customer;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	
}
