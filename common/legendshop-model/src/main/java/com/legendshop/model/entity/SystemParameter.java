/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;


import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 系统参数
 */
@Entity
@Table(name = "ls_sys_param")
public class SystemParameter implements GenericEntity<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3188172953619133451L;
	
	/** The name. */
	private String name;
	
	/** The value. */
	private String value;
	
	/** The memo. */
	private String memo;
	
	/** The type. */
	private String type;
	
	/** The optional. */
	private String optional;
	
	/** The change online. */
	private String changeOnline;//是否可以线上修改
	
	/** The display order. */
	private Integer  displayOrder;
	
	private String groupId;

	/**
	 * default constructor.
	 */
	public SystemParameter() {
	}

	// Property accessors

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	@Column(name = "value")
	public String getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the memo.
	 * 
	 * @return the memo
	 */
	@Column(name = "memo")
	public String getMemo() {
		return this.memo;
	}

	/**
	 * Sets the memo.
	 * 
	 * @param memo
	 *            the new memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	@Column(name = "type")
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the optional.
	 * 
	 * @return the optional
	 */
	@Column(name = "optional")
	public String getOptional() {
		return optional;
	}

	/**
	 * Sets the optional.
	 * 
	 * @param optional
	 *            the new optional
	 */
	public void setOptional(String optional) {
		this.optional = optional;
	}

	/**
	 * Gets the display order.
	 * 
	 * @return the display order
	 */
	@Column(name = "display_order")
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * Sets the display order.
	 * 
	 * @param displayOrder
	 *            the new display order
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * Gets the change online.
	 * 
	 * @return the change online
	 */
	@Column(name = "change_online")
	public String getChangeOnline() {
		return changeOnline;
	}

	/**
	 * Sets the change online.
	 * 
	 * @param changeOnline
	 *            the new change online
	 */
	public void setChangeOnline(String changeOnline) {
		this.changeOnline = changeOnline;
	}

	@Column(name = "group_id")
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Transient
	public String getId() {
		return name;
	}
	
	public void setId(String id) {
		this.name = id;
	}

}