package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 营销活动规则表
 */
@Entity
@Table(name = "ls_marketing_rule")
public class MarketingRule implements GenericEntity<Long> {

	private static final long serialVersionUID = 8942319043831178563L;
	/** 规则编号 */
	private Long ruleId; 
		
	/** 营销活动编号 */
	private Long marketId; 
		
	/** 店铺编号 */
	private Long shopId; 
		
	/** 满减优惠 */
	private Double offAmount; 
		
	/** 购买金额满多少金额开始触发规则 */
	private Double fullPrice; 
		
	/** 减折优惠 */
	private Float offDiscount; 
		
	/** 直降金额,对应的type为直降促销 */
	private Double cutAmount; 
		
	/** 折扣类型【0:金额满减，1:金额满促】 */
	private Integer type; 
		
	/** 创建时间 */
	private Date createTime; 
	
	/** 计算类型：0:按金额  1：按件 */
	private Integer calType; 
		

	public MarketingRule() {
    }
		
	@Id
	@Column(name = "rule_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "MARKETING_RULE_SEQ")
	public Long  getRuleId(){
		return ruleId;
	} 
		
	public void setRuleId(Long ruleId){
			this.ruleId = ruleId;
		}
		
    @Column(name = "market_id")
	public Long  getMarketId(){
		return marketId;
	} 
		
	public void setMarketId(Long marketId){
			this.marketId = marketId;
		}
		
    @Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "off_amount")
	public Double  getOffAmount(){
		return offAmount;
	} 
		
	public void setOffAmount(Double offAmount){
			this.offAmount = offAmount;
		}
		
    @Column(name = "full_price")
	public Double  getFullPrice(){
		return fullPrice;
	} 
		
	public void setFullPrice(Double fullPrice){
			this.fullPrice = fullPrice;
		}
		
    @Column(name = "off_discount")
	public Float  getOffDiscount(){
		return offDiscount;
	} 
		
	public void setOffDiscount(Float offDiscount){
			this.offDiscount = offDiscount;
		}
		
    @Column(name = "cut_amount")
	public Double  getCutAmount(){
		return cutAmount;
	} 
		
	public void setCutAmount(Double cutAmount){
			this.cutAmount = cutAmount;
		}
		
    @Column(name = "type")
	public Integer  getType(){
		return type;
	} 
		
	public void setType(Integer type){
			this.type = type;
		}
		
    @Column(name = "create_time")
	public Date  getCreateTime(){
		return createTime;
	} 
		
	public void setCreateTime(Date createTime){
			this.createTime = createTime;
		}
	
	@Transient
	public Long getId() {
		return ruleId;
	}
	
	public void setId(Long id) {
		ruleId = id;
	}
	
	@Column(name = "cal_type")
	public Integer getCalType() {
		return calType;
	}

	public void setCalType(Integer calType) {
		this.calType = calType;
	}


} 
