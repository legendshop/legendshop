package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 装修点击动作子类型
 * @author 开发很忙
 */
public enum AppDecorateActionSubTypeEnum implements StringEnum{
	
	/** 商品列表 - 分类 */
	CATEGORY("category"),
	
	/** 商品列表 - 品牌 */
	BRAND("BRAND"),
	
	/** 商品列表 - 标签 */
	TAGS("tags"),
	
	/** 商品列表 - 关键字 */
	KEYWORD("keyword"),
	
	/**  自定义URL - 跳转网页 */
	WEB("web"),
	
	/**  自定义URL - 小程序页面 */
	MP("mp"),
	
	/**  自定义URL - 外部链接 */
	OUT_URL("outURL"),
	
	;
	
	private final String value;

	private AppDecorateActionSubTypeEnum(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return value;
	}
}
