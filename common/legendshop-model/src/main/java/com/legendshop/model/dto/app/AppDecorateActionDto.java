package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 装修点击动作
 * @author SSD
 */
@ApiModel("装修点击动作")
public class AppDecorateActionDto {
	
	/** 动作类型 */
	@ApiModelProperty(value = "动作类型")
	private String type;
	
	/** 子类型 */
	@ApiModelProperty(value = "子类型")
	private String subType;
	
	/** 动作目标 */
	@ApiModelProperty(value = "动作目标")
	private String target;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
}
