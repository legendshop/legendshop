package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 商品属性值
 * 
 */
@Entity
@Table(name = "ls_prod_prop_value")
public class ProductPropertyValue extends UploadFile implements GenericEntity<Long> {

	private static final long serialVersionUID = 7248859099398637969L;

	// 属性值ID
	private Long valueId;

	// 属性ID
	private Long propId;

	// 属性值名称
	private String name;
	
	//属性值别名
	private String alias;

	// 状态
	private Short status;

	// 图片路径
	private String pic;

	// 排序
	private Long sequence;

	// 修改时间
	private Date modifyDate;

	// 记录时间
	private Date recDate;
	
	//属性值图片第一张
	private String url;

	public ProductPropertyValue() {
	}

	@Id
	@Column(name = "value_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_PROP_VALUE_SEQ")
	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	@Column(name = "prop_id")
	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "status")
	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	@Column(name = "pic")
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Column(name = "sequence")
	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	@Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Transient
	public Long getId() {
		return valueId;
	}

	public void setId(Long id) {
		this.valueId = id;
	}

	@Transient
	public String getUserName() {
		return null;
	}

	@Transient
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Transient
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
