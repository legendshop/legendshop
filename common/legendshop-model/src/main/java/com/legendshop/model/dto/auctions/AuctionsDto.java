/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.auctions;

import java.io.Serializable;


/**
 * 拍卖Dto
 */
public class AuctionsDto implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 869940379013099329L;

	/** 商品名字. */
	private String prodName;
		
	/** 商品图片. */
	private String prodPic; 
	
	/** 商品价格. */
	private String prodPrice;
	
	/** SKU 属性 */
	private String cnProperties;
	
	/**
	 * 商品状态 参考ProductStatusEnum
	 */
	private Long prodStatus;
	
	/** 商品sku状态   1：上线  0：下线  */
	private Long skuStatus; 

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * Gets the prod pic.
	 *
	 * @return the prod pic
	 */
	public String getProdPic() {
		return prodPic;
	}

	/**
	 * Sets the prod pic.
	 *
	 * @param prodPic the new prod pic
	 */
	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	/**
	 * Gets the prod price.
	 *
	 * @return the prod price
	 */
	public String getProdPrice() {
		return prodPrice;
	}

	/**
	 * Sets the prod price.
	 *
	 * @param prodPrice the new prod price
	 */
	public void setProdPrice(String prodPrice) {
		this.prodPrice = prodPrice;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Long getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Long prodStatus) {
		this.prodStatus = prodStatus;
	}

	public Long getSkuStatus() {
		return skuStatus;
	}

	public void setSkuStatus(Long skuStatus) {
		this.skuStatus = skuStatus;
	}
	
}
