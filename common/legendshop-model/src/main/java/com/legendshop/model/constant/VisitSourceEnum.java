/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * 
 * 访问历史来源
 */
public enum VisitSourceEnum implements StringEnum {

	/** PC端. */
	PC("PC"),
	
	APP("APP"),
	
	/** App IOS端. */
	APP_IOS("IOS"),
	
	/** App IOS端. */
	APP_ANDROID("Android"),
	
	/** 微信端. */
	WEIXIN("Weixin"),
	
	/** WAP端. */
	WAP("Wap");

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private VisitSourceEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

}
