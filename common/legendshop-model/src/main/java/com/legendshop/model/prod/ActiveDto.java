package com.legendshop.model.prod;

import java.io.Serializable;


/**
 * 包邮Dto
 */
public class ActiveDto implements Serializable{

	private static final long serialVersionUID = 8603339400272532192L;
	
	private Long activeId;
	
	private String activeName;
	
	private String startDate;
	
	private String endDate;
	
	/** 店铺:0,商品:1 */
	private Integer fullType; 
	
	/** 类型1：元 2 件 **/
	private Integer reduceType;

	/** 满足的金额或件数 full_value ***/
	private Double fullValue;
	
	private String description;
	
	private Long [] prodIds;//活动商品Id

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long[] getProdIds() {
		return prodIds;
	}

	public void setProdIds(Long[] prodIds) {
		this.prodIds = prodIds;
	}

	public Integer getFullType() {
		return fullType;
	}

	public void setFullType(Integer fullType) {
		this.fullType = fullType;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Integer getReduceType() {
		return reduceType;
	}

	public void setReduceType(Integer reduceType) {
		this.reduceType = reduceType;
	}

	public Double getFullValue() {
		return fullValue;
	}

	public void setFullValue(Double fullValue) {
		this.fullValue = fullValue;
	}
	
}






