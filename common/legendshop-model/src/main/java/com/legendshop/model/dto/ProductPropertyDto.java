package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 
 *商品属性Dto
 */
@ApiModel(value="商品属性Dto")
public class ProductPropertyDto  implements Serializable,GenericEntity<Long>{

	private static final long serialVersionUID = -6991917665604921238L;
	
	/**
	 * 属性ID
	 */
	@ApiModelProperty(value="属性ID")
	private Long propId;

	/**
	 * 属性名称
	 */
	@ApiModelProperty(value="属性名称")
	private String propName;

	/**
	 * 是否必须
	 */
	@ApiModelProperty(value="是否必须")
	private boolean isRequired;

	/**
	 * 是否多选
	 */
	@ApiModelProperty(value="是否多选")
	private boolean isMulti;

	/**
	 * 排序
	 */
	@ApiModelProperty(value="排序")
	private Long sequence;

	/**
	 * 属性类型，1：有图片，0：文字
	 */
	@ApiModelProperty(value="属性类型，1：有图片，0：文字")
	private Short type;

	/**
	 * 规则属性：（参数属性：2，销售属性：1, 关键属性：3, 参见ProductPropertyTypeEnum）
	 */
	@ApiModelProperty(value="规则属性：（参数属性：2，销售属性：1, 关键属性：3, 参见ProductPropertyTypeEnum）")
	private Integer isRuleAttributes;

	/**
	 * 是否可以搜索
	 */
	@ApiModelProperty(value="是否可以搜索")
	private boolean isForSearch;
	
	/**
	 * 是否输入属性
	 */
	@ApiModelProperty(value="是否输入属性")
	private boolean isInputProp;
	
	/**
	 * 属性值ID
	 */
	@ApiModelProperty(value="属性值ID")
	private Long valueId;
	
	/**
	 * 当前选中值
	 */
	@ApiModelProperty(value="当前选中值")
	private String name;
	
	/**
	 * 属性值图
	 */
	@ApiModelProperty(value="属性值图")
	private String valuePic;
	
	/**
	 * 分组id
	 */
	@ApiModelProperty(value="分组id")
	private String groupId;
	
	/**
	 * 已经选择的选项值
	 */
	@ApiModelProperty(value="组名")
	private String groupName;
	
	/**
	 * 已经选择的选项值
	 */
	@ApiModelProperty(value="已经选择的选项值")
	private String selectedValue;
	
	/**
	 * 属性对应的属性值集合
	 */
	@ApiModelProperty(value="属性对应的属性值集合")
	private List<ProductPropertyValueDto> prodPropList;
	
	/**
	 * 是否自定义属性 
	 */
	@ApiModelProperty(value="是否自定义属性 ")
	private boolean isCustom;

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public boolean isMulti() {
		return isMulti;
	}

	public void setMulti(boolean isMulti) {
		this.isMulti = isMulti;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Integer getIsRuleAttributes() {
		return isRuleAttributes;
	}

	public void setIsRuleAttributes(Integer isRuleAttributes) {
		this.isRuleAttributes = isRuleAttributes;
	}

	public boolean isForSearch() {
		return isForSearch;
	}

	public void setForSearch(boolean isForSearch) {
		this.isForSearch = isForSearch;
	}

	public List<ProductPropertyValueDto> getProductPropertyValueList() {
		return prodPropList;
	}

	public void setProductPropertyValueList(List<ProductPropertyValueDto> productPropertyValueList) {
		this.prodPropList = productPropertyValueList;
	}

	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValuePic() {
		return valuePic;
	}

	public void setValuePic(String valuePic) {
		this.valuePic = valuePic;
	}

	public void addProductPropertyValueDto(Long propId,String name,Long valueId,String valuePic){
		if(prodPropList ==null){
			prodPropList = new ArrayList<ProductPropertyValueDto>();
		}
		ProductPropertyValueDto productPropertyValueDto = new ProductPropertyValueDto();
		productPropertyValueDto.setName(name);
		productPropertyValueDto.setPropId(propId);
		productPropertyValueDto.setValueId(valueId);
		productPropertyValueDto.setPic(valuePic);

		prodPropList.add(productPropertyValueDto);
	}

	public boolean isInputProp() {
		return isInputProp;
	}

	public void setInputProp(boolean isInputProp) {
		this.isInputProp = isInputProp;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

	public void addProductPropertyValueList(ProductPropertyValueDto propValueDto) {
		if(prodPropList ==null){
			prodPropList = new ArrayList<ProductPropertyValueDto>();
		}
		if(this.propId.equals(propValueDto.getPropId())){
			prodPropList.add(propValueDto);
		}
	}

	public boolean getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(boolean isCustom) {
		this.isCustom = isCustom;
	}

	@Override
	public Long getId() {
		return this.getPropId();
	}

	@Override
	public void setId(Long id) {
		this.propId=id;
	}

}
