package com.legendshop.model.entity;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *公司详细信息
 */
@Entity
@Table(name = "ls_shop_company_detail")
public class ShopCompanyDetail implements GenericEntity<Long>{
	
	private static final long serialVersionUID = 6020375459534810511L;

	/** ID */
	private Long id;
	
	private Long shopId; 
		
	/** 注册号(营业执照号) */
	private String licenseNumber; 
	
	/** 法人身份证电子版 */
	private String legalIdCard;
		
	/** 营业执照省份 */
	private Long licenseProvinceid; 
		
	/** 营业执照城市 */
	private Long licenseCityid; 
		
	/** 营业执照地级市 */
	private Long licenseAreaid; 
		
	/** 营业执照详细地址 */
	private String licenseAddr; 
		
	/** 成立日期 */
	private Date licenseEstablishDate; 
		
	/** 营业期限开始时间 */
	private Date licenseStartDate; 
		
	/** 营业期限结束时间 */
	private Date licenseEndDate; 
		
	/** 注册资本 */
	private Long licenseRegCapital; 
		
	/** 经营范围 */
	private String licenseBusinessScope; 
	
		
	/** 营业执照副本电子版 */
	private String licensePic; 
		
	/** 公司省份 */
	private Long companyProvinceid; 
		
	/** 公司城市 */
	private Long companyCityid; 
		
	/** 公司地级市 */
	private Long companyAreaid; 
		
	/** 公司详细地址 */
	private String companyAddress; 
		
	/** 公司名字 */
	private String companyName; 
		
	/** 公司人数 */
	private String companyNum; 
		
	/** 公司行业 */
	private String companyIndustry; 
		
	/** 公司性质 */
	private String companyProperty; 
		
	/** 公司电话 */
	private String companyTelephone; 
		
	/** 税务登记证号 */
	private String taxRegistrationNum; 
		
	/** 纳税人识别号 */
	private String taxpayerId; 
		
	/** 税务登记证号电子版 */
	private String taxRegistrationNumE; 
		
	/** 商事制度[1:旧版三证 2:一证一码] */
	private Long codeType; 
		
	/** 社会信用代码号 */
	private String socialCreditCode; 
		
	/** 银行开户名 */
	private String bankAccountName; 
		
	/** 银行账号 */
	private String bankCard; 
		
	/** 开户银行支行名称 */
	private String bankName; 
		
	/** 开户行支行联行号 */
	private String bankLineNum; 
		
	/** 营业执照省份 */
	private Long bankProvinceid; 
		
	/** 营业执照城市 */
	private Long bankCityid; 
		
	/** 营业执照地级市 */
	private Long bankAreaid; 
		
	/** 银行开户许可证电子版 */
	private String bankPermitImg; 
	
	protected MultipartFile legalIdCardPic;
	
	protected MultipartFile licensePics;
	
	protected MultipartFile taxRegistrationNumEPic;
	
	protected MultipartFile bankPermitPic;
	
	private String userName;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "SHOP_COMPANYDETAIL_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "shop_id")
	public Long  getShopId(){
		return shopId;
	} 
		
	

	public void setShopId(Long shopId){
			this.shopId = shopId;
		}
		
    @Column(name = "license_number")
	public String  getLicenseNumber(){
		return licenseNumber;
	} 
		
	public void setLicenseNumber(String licenseNumber){
			this.licenseNumber = licenseNumber;
		}
		
	@Column(name = "legal_IdCard")
    public String getLegalIdCard() {
		return legalIdCard;
	}

	public void setLegalIdCard(String legalIdCard) {
		this.legalIdCard = legalIdCard;
	}

	@Column(name = "license_provinceid")
	public Long  getLicenseProvinceid(){
		return licenseProvinceid;
	} 
		
	public void setLicenseProvinceid(Long licenseProvinceid){
			this.licenseProvinceid = licenseProvinceid;
		}
		
    @Column(name = "license_cityid")
	public Long  getLicenseCityid(){
		return licenseCityid;
	} 
		
	public void setLicenseCityid(Long licenseCityid){
			this.licenseCityid = licenseCityid;
		}
		
    @Column(name = "license_areaid")
	public Long  getLicenseAreaid(){
		return licenseAreaid;
	} 
		
	public void setLicenseAreaid(Long licenseAreaid){
			this.licenseAreaid = licenseAreaid;
		}
		
    @Column(name = "license_addr")
	public String  getLicenseAddr(){
		return licenseAddr;
	} 
		
	public void setLicenseAddr(String licenseAddr){
			this.licenseAddr = licenseAddr;
		}
		
    @Column(name = "license_establish_date")
	public Date  getLicenseEstablishDate(){
		return licenseEstablishDate;
	} 
		
	public void setLicenseEstablishDate(Date licenseEstablishDate){
			this.licenseEstablishDate = licenseEstablishDate;
		}
		
    @Column(name = "license_start_date")
	public Date  getLicenseStartDate(){
		return licenseStartDate;
	} 
		
	public void setLicenseStartDate(Date licenseStartDate){
			this.licenseStartDate = licenseStartDate;
		}
		
    @Column(name = "license_end_date")
	public Date  getLicenseEndDate(){
		return licenseEndDate;
	} 
		
	public void setLicenseEndDate(Date licenseEndDate){
			this.licenseEndDate = licenseEndDate;
		}
		
    @Column(name = "license_reg_capital")
	public Long  getLicenseRegCapital(){
		return licenseRegCapital;
	} 
		
	public void setLicenseRegCapital(Long licenseRegCapital){
			this.licenseRegCapital = licenseRegCapital;
		}
		
    @Column(name = "license_business_scope")
	public String  getLicenseBusinessScope(){
		return licenseBusinessScope;
	} 
		
	public void setLicenseBusinessScope(String licenseBusinessScope){
			this.licenseBusinessScope = licenseBusinessScope;
		}
		
    @Column(name = "license_pic")
	public String  getLicensePic(){
		return licensePic;
	} 
		
	public void setLicensePic(String licensePic){
			this.licensePic = licensePic;
		}
		
    @Column(name = "company_provinceid")
	public Long  getCompanyProvinceid(){
		return companyProvinceid;
	} 
		
	public void setCompanyProvinceid(Long companyProvinceid){
			this.companyProvinceid = companyProvinceid;
		}
		
    @Column(name = "company_cityid")
	public Long  getCompanyCityid(){
		return companyCityid;
	} 
		
	public void setCompanyCityid(Long companyCityid){
			this.companyCityid = companyCityid;
		}
		
    @Column(name = "company_areaid")
	public Long  getCompanyAreaid(){
		return companyAreaid;
	} 
		
	public void setCompanyAreaid(Long companyAreaid){
			this.companyAreaid = companyAreaid;
		}
		
    @Column(name = "company_address")
	public String  getCompanyAddress(){
		return companyAddress;
	} 
		
	public void setCompanyAddress(String companyAddress){
			this.companyAddress = companyAddress;
		}
		
    @Column(name = "company_name")
	public String  getCompanyName(){
		return companyName;
	} 
		
	public void setCompanyName(String companyName){
			this.companyName = companyName;
		}
		
    @Column(name = "company_num")
	public String  getCompanyNum(){
		return companyNum;
	} 
		
	public void setCompanyNum(String companyNum){
			this.companyNum = companyNum;
		}
		
    @Column(name = "company_industry")
	public String  getCompanyIndustry(){
		return companyIndustry;
	} 
		
	public void setCompanyIndustry(String companyIndustry){
			this.companyIndustry = companyIndustry;
		}
		
    @Column(name = "company_property")
	public String  getCompanyProperty(){
		return companyProperty;
	} 
		
	public void setCompanyProperty(String companyProperty){
			this.companyProperty = companyProperty;
		}
		
    @Column(name = "company_telephone")
	public String  getCompanyTelephone(){
		return companyTelephone;
	} 
		
	public void setCompanyTelephone(String companyTelephone){
			this.companyTelephone = companyTelephone;
		}
	
	
	
	@Column(name = "tax_registration_num")
    public String getTaxRegistrationNum() {
		return taxRegistrationNum;
	}

	public void setTaxRegistrationNum(String taxRegistrationNum) {
		this.taxRegistrationNum = taxRegistrationNum;
	}

	@Column(name = "taxpayer_id")
	public String  getTaxpayerId(){
		return taxpayerId;
	} 
		
	public void setTaxpayerId(String taxpayerId){
			this.taxpayerId = taxpayerId;
		}
		
	@Column(name = "tax_registration_num_e")
	public String getTaxRegistrationNumE() {
		return taxRegistrationNumE;
	}

	public void setTaxRegistrationNumE(String taxRegistrationNumE) {
		this.taxRegistrationNumE = taxRegistrationNumE;
	}
	
    @Column(name = "code_type")
	public Long  getCodeType(){
		return codeType;
	} 
		
	public void setCodeType(Long codeType){
			this.codeType = codeType;
		}
		
    @Column(name = "social_credit_code")
	public String  getSocialCreditCode(){
		return socialCreditCode;
	} 
		
	public void setSocialCreditCode(String socialCreditCode){
			this.socialCreditCode = socialCreditCode;
		}
		
    @Column(name = "bank_account_name")
	public String  getBankAccountName(){
		return bankAccountName;
	} 
		
	public void setBankAccountName(String bankAccountName){
			this.bankAccountName = bankAccountName;
		}
		
    @Column(name = "bank_card")
	public String  getBankCard(){
		return bankCard;
	} 
		
	public void setBankCard(String bankCard){
			this.bankCard = bankCard;
		}
		
    @Column(name = "bank_name")
	public String  getBankName(){
		return bankName;
	} 
		
	public void setBankName(String bankName){
			this.bankName = bankName;
		}
		
    @Column(name = "bank_line_num")
	public String  getBankLineNum(){
		return bankLineNum;
	} 
		
	public void setBankLineNum(String bankLineNum){
			this.bankLineNum = bankLineNum;
		}
		
    @Column(name = "bank_provinceid")
	public Long  getBankProvinceid(){
		return bankProvinceid;
	} 
		
	public void setBankProvinceid(Long bankProvinceid){
			this.bankProvinceid = bankProvinceid;
		}
		
    @Column(name = "bank_cityid")
	public Long  getBankCityid(){
		return bankCityid;
	} 
		
	public void setBankCityid(Long bankCityid){
			this.bankCityid = bankCityid;
		}
		
    @Column(name = "bank_areaid")
	public Long  getBankAreaid(){
		return bankAreaid;
	} 
		
	public void setBankAreaid(Long bankAreaid){
			this.bankAreaid = bankAreaid;
		}
		
    @Column(name = "bank_permit_img")
	public String  getBankPermitImg(){
		return bankPermitImg;
	} 
		
	public void setBankPermitImg(String bankPermitImg){
			this.bankPermitImg = bankPermitImg;
		}
	@Transient
	public MultipartFile getLegalIdCardPic() {
		return legalIdCardPic;
	}

	public void setLegalIdCardPic(MultipartFile legalIdCardPic) {
		this.legalIdCardPic = legalIdCardPic;
	}
	@Transient
	public MultipartFile getLicensePics() {
		return licensePics;
	}

	public void setLicensePics(MultipartFile licensePics) {
		this.licensePics = licensePics;
	}
	
	@Transient
	public MultipartFile getTaxRegistrationNumEPic() {
		return taxRegistrationNumEPic;
	}

	public void setTaxRegistrationNumEPic(MultipartFile taxRegistrationNumEPic) {
		this.taxRegistrationNumEPic = taxRegistrationNumEPic;
	}

	@Transient
	public MultipartFile getBankPermitPic() {
		return bankPermitPic;
	}

	public void setBankPermitPic(MultipartFile bankPermitPic) {
		this.bankPermitPic = bankPermitPic;
	}
	
	
	@Transient
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
} 
