package com.legendshop.model.entity.weixin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *微信图片
 */
@Entity
@Table(name = "ls_weixin_img_file")
public class WeixinImgFile implements GenericEntity<Long> {

	private static final long serialVersionUID = -4597034523073300865L;

	/**  */
	private Long id; 
		
	/** 图片组ID */
	private Long imgGoupId; 
		
	/** 图片名称 */
	private String name; 
	
	/** The file path. */
	private String filePath;

	/** The file type. */
	private String fileType;

	/** The file size. */
	private Integer fileSize;
	
		
	/** 描述 */
	private String desction; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 创建人 */
	private String createUserId; 
		
	/** 修改时间 */
	private Date updateDate; 
		
	/** 修改人 */
	private String updateUser; 
		
	/** 排序 */
	private Long seq; 
	
	private String mediaId;
		
	
	public WeixinImgFile() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "WEIXIN_IMG_FILE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "img_goup_id")
	public Long  getImgGoupId(){
		return imgGoupId;
	} 
		
	public void setImgGoupId(Long imgGoupId){
			this.imgGoupId = imgGoupId;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
	

	/**
	 * Gets the file path.
	 * 
	 * @return the file path
	 */
	@Column(name = "file_path")
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 * 
	 * @param filePath
	 *            the new file path
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	/**
	 * Gets the file type.
	 * 
	 * @return the file type
	 */
	@Column(name = "file_type")
	public String getFileType() {
		return fileType;
	}

	/**
	 * Sets the file type.
	 * 
	 * @param fileType
	 *            the new file type
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * Gets the file size.
	 * 
	 * @return the file size
	 */
	@Column(name = "file_size")
	public Integer getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the file size.
	 * 
	 * @param fileSize
	 *            the new file size
	 */
	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}
	
		
    @Column(name = "desction")
    public String getDesction() {
		return desction;
	}

	public void setDesction(String desction) {
		this.desction = desction;
	}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "create_user_id")
	public String  getCreateUserId(){
		return createUserId;
	} 
		
	public void setCreateUserId(String createUserId){
			this.createUserId = createUserId;
		}
		
    @Column(name = "update_date")
	public Date  getUpdateDate(){
		return updateDate;
	} 
		
	public void setUpdateDate(Date updateDate){
			this.updateDate = updateDate;
		}
		
    @Column(name = "update_user")
	public String  getUpdateUser(){
		return updateUser;
	} 
		
	public void setUpdateUser(String updateUser){
			this.updateUser = updateUser;
		}
		
    @Column(name = "seq")
	public Long  getSeq(){
		return seq;
	} 
		
	public void setSeq(Long seq){
			this.seq = seq;
		}

	@Column(name = "media_id")
	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	
	


} 
