package com.legendshop.model.dto.api;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.support.GenericEntity;

/**
 * 首页导航条
 * 
 */
public class NavigationCategory implements GenericEntity<Long> {

	private static final long serialVersionUID = 724132206864197841L;
	/** 产品类目ID */
	private Long id;

	/** 父节点 */
	private Long parentId;

	/** 产品类目名称 */
	private String name;
	
	/** The parent node. */
	protected NavigationCategory parentNode;
	
	private Integer level;

	/** 子分类 **/
	private List<NavigationCategory> childrenList=new ArrayList<NavigationCategory>();

	private List<NavigationBrand> navigationBrands=new ArrayList<NavigationBrand>();

	private List<NavigationProduct> navigationProducts=new ArrayList<NavigationProduct>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<NavigationBrand> getNavigationBrands() {
		return navigationBrands;
	}

	public void setNavigationBrands(List<NavigationBrand> navigationBrands) {
		this.navigationBrands = navigationBrands;
	}

	public List<NavigationProduct> getNavigationProducts() {
		return navigationProducts;
	}

	public void setNavigationProducts(List<NavigationProduct> navigationProducts) {
		this.navigationProducts = navigationProducts;
	}

	public List<NavigationCategory> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<NavigationCategory> childrenList) {
		this.childrenList = childrenList;
	}

	public void addChildNode(NavigationCategory treeNode) {
		childrenList.add(treeNode);
	}

	public NavigationCategory getParentNode() {
		return parentNode;
	}

	public void setParentNode(NavigationCategory parentNode) {
		this.parentNode = parentNode;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
	
	
}
