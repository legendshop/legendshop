package com.legendshop.model.dto;

import com.legendshop.dao.support.PageProvider;

/**
 * 店铺查询的参数
 * @author 开发很忙
 * 
 */
public class ShopSearchParamDto {
	
	/** 排序方式 */
	public static String TIME = "time";//按时间
	public static String SYNTHESIZE = "synthesize";//按综合
	public static String VISITNUM = "visitNum";
	public static String PRODNUM = "prodNum";//产品数量

	private Integer curPageNO = 1;
	
	private Integer pageSize;
	
	private PageProvider pageProvider;
	
	/** 显示商品数量 */
	private Integer prodCount;
	
	/** 店铺状态 */
	private Integer status;
	
	/** 商家类型 : 设计师,商家 */
	private Integer type;
	
	/** 搜索内容 */
	private String searchContent;
	
	/** 排序方式 */
	private String orderWay;
	
	public Integer getCurPageNO() {
		return curPageNO;
	}

	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public PageProvider getPageProvider() {
		return pageProvider;
	}

	public void setPageProvider(PageProvider pageProvider) {
		this.pageProvider = pageProvider;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSearchContent() {
		return searchContent;
	}

	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}

	public Integer getProdCount() {
		return prodCount;
	}

	public void setProdCount(Integer prodCount) {
		this.prodCount = prodCount;
	}

	public String getOrderWay() {
		return orderWay;
	}

	public void setOrderWay(String orderWay) {
		this.orderWay = orderWay;
	}
}
