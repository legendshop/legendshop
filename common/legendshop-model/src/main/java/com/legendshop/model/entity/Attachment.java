/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;



@Entity
@Table(name = "ls_attachment")
public class Attachment implements GenericEntity<Long> {

	private static final long serialVersionUID = 1375640693071960316L;

	//
	private Long id;

	// 用户名称
	private String userName;

	//
	private Date recDate;

	//
	private String ext;

	// 文件类型
	private String fileType;

	//
	private Long height;

	//
	private Long width;

	// 图片路径, 如果是视频的话，则保存的是视频的首帧
	private String filePath;
	
	// 视频路径
	private String videoPath;

	//文件大小
	private Long fileSize;

	// 状态  1 正常  0 删除
	private Boolean status;
	
	//分类id
	private Long treeId;
	
	//文件名
	private String fileName;
	
	//是否图片空间管理
	private Integer isManaged;
	
	//是否图片,参考ImageTypeEnum
	private Integer imgType;
	
	//关联数
	private Integer relCount;
	
	   /** The shop id. */
    private Long shopId;
    
    private String userId;
    
    /** 图片用途(客服系统) */
    private String purpose;
    
    
	public Attachment() {
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ATTACHMENT_SEQ")
	public Long getId() {
		return id;
	}
	
	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
    @Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "rec_date")
	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	@Column(name = "ext")
	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	@Column(name = "file_type")
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Column(name = "height")
	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	@Column(name = "width")
	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	@Column(name = "file_path")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Column(name = "file_size")
	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	@Column(name = "status")
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name = "tree_id")
	public Long getTreeId() {
		return treeId;
	}

	public void setTreeId(Long treeId) {
		this.treeId = treeId;
	}

	@Column(name = "file_name")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name = "is_managed")
	public Integer getIsManaged() {
		return isManaged;
	}

	public void setIsManaged(Integer isManaged) {
		this.isManaged = isManaged;
	}

	@Transient
	public Integer getRelCount() {
		return relCount;
	}

	public void setRelCount(Integer relCount) {
		this.relCount = relCount;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "img_type")
	public Integer getImgType() {
		return imgType;
	}

	public void setImgType(Integer imgType) {
		this.imgType = imgType;
	}

	@Column(name = "video_path")
	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	@Column(name = "purpose")
	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	
}
