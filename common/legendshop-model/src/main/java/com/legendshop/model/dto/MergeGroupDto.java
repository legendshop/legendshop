package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;

public class MergeGroupDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 主键 */
	private Long id;

	/** 商家ID */
	private Long shopId;

	/** 商家名称 */
	private String siteName;

	/** 活动名称 */
	private String mergeName;

	/** 开始时间 */
	private Date startTime;

	/** 结束时间 */
	private Date endTime;

	/** 状态[-1下线、0待审核、1上线] */
	private Integer status;

	/** 拼团人数 */
	private Integer peopleNumber;

	/** 成团订单数 */
	private Long count;

	/** 是否团长免单,默认否 */
	private Boolean isHeadFree;

	/** 订单商品实际价格(运费 折扣 促销)  */
	private Double actualTotal;

	/** 下订单的时间  */
	private Date subDate;

	/** 订单商品总数 */
	private Integer productNums;

	/** 是否团长，默认不是 */
	private Boolean isHead;

	/** 订单ID */
	private Long subId;

	/** 用户名  */
	private String userName;

	/** 订单号 */
	private String subNumber;

	/** 拼团活动编号  */
	private String mergeNumber;

	/** 拼团编号,用于区分参团的用户归属哪个团 */
	private String addNumber;

	/** 创建时间 */
	private Date createTime;
	
	/** 商品id */
	private Long prodId;
	
	/** 商品状态  参考ProductStatusEnum */
	private Integer prodStatus;
	
	/** 搜索类型 （tab搜索用） */
	private String searchType;

	/** 商家删除状态 0：未删除 1：已删除 */
	private Integer deleteStatus;
	
	/** 用于区分商家搜索/平台搜索  为空是商家搜索列表 */
	private String flag;
	
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getMergeName() {
		return mergeName;
	}

	public void setMergeName(String mergeName) {
		this.mergeName = mergeName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Boolean getIsHeadFree() {
		return isHeadFree;
	}

	public void setIsHeadFree(Boolean isHeadFree) {
		this.isHeadFree = isHeadFree;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Boolean getIsHead() {
		return isHead;
	}

	public void setIsHead(Boolean isHead) {
		this.isHead = isHead;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getMergeNumber() {
		return mergeNumber;
	}

	public void setMergeNumber(String mergeNumber) {
		this.mergeNumber = mergeNumber;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getProdStatus() {
		return prodStatus;
	}

	public void setProdStatus(Integer prodStatus) {
		this.prodStatus = prodStatus;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
