package com.legendshop.model.dto;

import com.legendshop.model.entity.Sku;
/**
 * 商品关联sku
 */
import java.util.List;

public class SkuSearchDto {
    /* 商品id*/
    private Long prodId;
    /* 商品选中的skuId集合*/
    private Long[] skuIdArray;

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public Long[] getSkuIdArray() {
        return skuIdArray;
    }

    public void setSkuIdArray(Long[] skuIdArray) {
        this.skuIdArray = skuIdArray;
    }
}
