package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("参数分组Dto")
public class ParamGroupDto implements Serializable {

	private static final long serialVersionUID = -3405015734439237718L;
	
	/** 分组名称 */
	@ApiModelProperty(value = "分组名称")
	private String groupName;
	
	/** 参数列表 */
	@ApiModelProperty(value = "参数列表")
	private List<ProdParamDto> params;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<ProdParamDto> getParams() {
		return params;
	}

	public void setParams(List<ProdParamDto> params) {
		this.params = params;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ParamGroupDto){
			if(this.groupName.equals(((ParamGroupDto)obj).getGroupName()))
				return true;
		}
		return false;
	}
	
	public int hashCode() {
		return this.groupName.hashCode();
	}

}
