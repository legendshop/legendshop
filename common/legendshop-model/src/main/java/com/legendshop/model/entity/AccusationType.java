package com.legendshop.model.entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
/**
 * 
 * 举报类型
 *
 */
@Entity
@Table(name = "ls_accu_type")
public class AccusationType implements GenericEntity<Long>{

	private static final long serialVersionUID = -7253673092676282986L;

	// 
	private Long typeId ; 
		
	// 内容
	private String content ; 
		
	// 名称
	private String name ; 
		
	// 状态
	private Long status ; 
		
	// 记录时间
	private Date recDate ; 
		
	//举报主题List
	private List<AccusationSubject> subjectList = new ArrayList<AccusationSubject>();
	
	public AccusationType() {
    }
	
	@Id
	@Column(name = "type_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ACCU_TYPE_SEQ")
	public Long  getTypeId(){
		return typeId ;
	} 
		
	public void setTypeId(Long typeId){
		this.typeId = typeId ;
	}
		
	@Column(name = "content")
	public String  getContent(){
		return content ;
	} 
		
	public void setContent(String content){
		this.content = content ;
	}
		
	@Column(name = "name")
	public String  getName(){
		return name ;
	} 
		
	public void setName(String name){
		this.name = name ;
	}
		
	@Column(name = "status")
	public Long  getStatus(){
		return status ;
	} 
		
	public void setStatus(Long status){
		this.status = status ;
	}
		
	@Column(name = "rec_date")
	public Date  getRecDate(){
		return recDate ;
	} 
		
	public void setRecDate(Date recDate){
		this.recDate = recDate ;
	}
		
	@Transient
  public Long getId() {
		return 	typeId;
	}
	
	public void setId(Long id) {
		this.typeId = id;
	}

	@Transient
	public List<AccusationSubject> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<AccusationSubject> subjectList) {
		this.subjectList = subjectList;
	}

	public void addSubjectList(AccusationSubject accusationSubject){
		if(this.getTypeId().equals(accusationSubject.getTypeId())){
			subjectList.add(accusationSubject);
		}
	}
	
} 
