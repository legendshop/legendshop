/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.shopDecotate;

import java.io.Serializable;
import java.util.List;


/**
 * 热点模块.
 *
 */
public class ShopLayoutHotDto implements Serializable {

	private static final long serialVersionUID = -4412310736770105851L;

	/** Id. */
	private Long hotId;

	/**  布局id. */
	private Long layoutId;
	
	/** 布局DIV Id. */
	private Long layoutDivId;

	/**  热点数据(fsdgsfdgsfdgsd==99,158,273,268|fdsggsfdg==0,0,174,110|). */
	private String hotDatas;

	/**  顺序. */
	private Integer seq;

	/** 图片. */
	private String imageFile;

	/** 热门数据Dto. */
	private List<HotDataDto> dataDtos;

	/**
	 * Gets the hot id.
	 *
	 * @return the hot id
	 */
	public Long getHotId() {
		return hotId;
	}

	/**
	 * Sets the hot id.
	 *
	 * @param hotId the new hot id
	 */
	public void setHotId(Long hotId) {
		this.hotId = hotId;
	}

	/**
	 * Gets the layout id.
	 *
	 * @return the layout id
	 */
	public Long getLayoutId() {
		return layoutId;
	}

	/**
	 * Sets the layout id.
	 *
	 * @param layoutId the new layout id
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}

	/**
	 * Gets the hot datas.
	 *
	 * @return the hot datas
	 */
	public String getHotDatas() {
		return hotDatas;
	}

	/**
	 * Sets the hot datas.
	 *
	 * @param hotDatas the new hot datas
	 */
	public void setHotDatas(String hotDatas) {
		this.hotDatas = hotDatas;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * Gets the image file.
	 *
	 * @return the image file
	 */
	public String getImageFile() {
		return imageFile;
	}

	/**
	 * Sets the image file.
	 *
	 * @param imageFile the new image file
	 */
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * Gets the data dtos.
	 *
	 * @return the data dtos
	 */
	public List<HotDataDto> getDataDtos() {
		return dataDtos;
	}

	/**
	 * Sets the data dtos.
	 *
	 * @param dataDtos the new data dtos
	 */
	public void setDataDtos(List<HotDataDto> dataDtos) {
		this.dataDtos = dataDtos;
	}

	/**
	 * Gets the layout div id.
	 *
	 * @return the layout div id
	 */
	public Long getLayoutDivId() {
		return layoutDivId;
	}

	/**
	 * Sets the layout div id.
	 *
	 * @param layoutDivId the new layout div id
	 */
	public void setLayoutDivId(Long layoutDivId) {
		this.layoutDivId = layoutDivId;
	}
	

}
