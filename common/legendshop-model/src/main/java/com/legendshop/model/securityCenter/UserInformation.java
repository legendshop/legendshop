package com.legendshop.model.securityCenter;

import java.io.Serializable;

/**
 * 装载用户信息
 * @author Administrator
 *
 */
public class UserInformation implements Serializable{

	private static final long serialVersionUID = -7259538812625847424L;
	//用户名称
	private String userName;
	
	//用户昵称
	private String nickName;
	
	//用户Id
	private String id;
	
	//密码
	private String password;
	
	//邮箱
	private String userMail;
	
	//手机
	private String userMobile;
	
	//支付密码
	private String payPassword;
	
	//支付密码强度
	private Integer payStrength;
	
	// 安全级别
	private Integer secLevel ; 
			
	// 是否验证邮箱
	private Integer mailVerifn ; 
			
	// 是否验证手机
	private Integer phoneVerifn ; 
			
	// 是否验证支付密码
	private Integer paypassVerifn ;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}

	public Integer getSecLevel() {
		return secLevel;
	}

	public void setSecLevel(Integer secLevel) {
		this.secLevel = secLevel;
	}

	public Integer getMailVerifn() {
		return mailVerifn;
	}

	public void setMailVerifn(Integer mailVerifn) {
		this.mailVerifn = mailVerifn;
	}

	public Integer getPhoneVerifn() {
		return phoneVerifn;
	}

	public void setPhoneVerifn(Integer phoneVerifn) {
		this.phoneVerifn = phoneVerifn;
	}

	public Integer getPaypassVerifn() {
		return paypassVerifn;
	}

	public void setPaypassVerifn(Integer paypassVerifn) {
		this.paypassVerifn = paypassVerifn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getPayStrength() {
		return payStrength;
	}

	public void setPayStrength(Integer payStrength) {
		this.payStrength = payStrength;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}  
	
}
