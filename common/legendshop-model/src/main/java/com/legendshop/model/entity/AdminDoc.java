package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 *
 */
@Entity
@Table(name = "ls_admin_doc")
public class AdminDoc implements GenericEntity<Long> {

	private static final long serialVersionUID = 3587078426608427432L;

	/** 主键 */
	private Long id; 
		
	/** 标题 */
	private String title; 
		
	/** 内容 */
	private String content; 
		
	
	public AdminDoc() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ADMIN_DOC_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "title")
	public String  getTitle(){
		return title;
	} 
		
	public void setTitle(String title){
			this.title = title;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
	


} 
