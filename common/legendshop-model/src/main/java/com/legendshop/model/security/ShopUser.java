/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.security;

import java.io.Serializable;
import java.util.List;

/**
 * 商家用户.
 *
 */
public class ShopUser implements Serializable {

	private static final long serialVersionUID = 5188775095770681992L;

	/** 商城ID. */
	private Long shopId;
	
	private String shopName;

	/** 用户ID. */
	private String userId;
	
	/** 用户名. */
	private String userName;
	
	/** 状态. */
	private Integer status;
	
	/** 商家菜单 **/
	private List<ShopMenuGroup> shopMenuGroupList;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ShopMenuGroup> getShopMenuGroupList() {
		return shopMenuGroupList;
	}

	public void setShopMenuGroupList(List<ShopMenuGroup> shopMenuGroupList) {
		this.shopMenuGroupList = shopMenuGroupList;
	}


	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	

}
