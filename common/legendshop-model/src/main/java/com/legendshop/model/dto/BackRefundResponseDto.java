package com.legendshop.model.dto;

/**
 * 第三方查询退款信息Dto
 * 
 * @author Tony
 *
 */
public class BackRefundResponseDto {

	/**
	 * 是否成功
	 */
	private boolean result;

	/**
	 * 操作信息
	 */
	private String message;
	
	private String outRefundNo;

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}
	

}
