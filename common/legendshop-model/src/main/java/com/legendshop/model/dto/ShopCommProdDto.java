/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;

/**
 * 产品评论表Dto.
 */

public class ShopCommProdDto implements Serializable {

	private static final long serialVersionUID = 6317298566845679559L;

	private Long prodId;

	private String prodName;

	private String pic;
	
	private Integer reviewScores;
	
	private Integer comments;
	
	private float avgScore;
	
	//待回复数
	private Integer waitReply;

	public Integer getWaitReply() {
		return waitReply;
	}

	public void setWaitReply(Integer waitReply) {
		this.waitReply = waitReply;
	}

	public ShopCommProdDto(){
		
	}
	
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public Long getProdId() {
		return prodId;
	}
	
	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}
	
	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}
	
	/**
	 * Sets the pic.
	 *
	 * @param pic the new pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	/**
	 * Gets the review scores.
	 *
	 * @return the review scores
	 */
	public Integer getReviewScores() {
		return reviewScores;
	}
	
	/**
	 * Sets the review scores.
	 *
	 * @param reviewScores the new review scores
	 */
	public void setReviewScores(Integer reviewScores) {
		this.reviewScores = reviewScores;
	}
	
	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public Integer getComments() {
		return comments;
	}
	
	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public float getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(float avgScore) {
		this.avgScore = avgScore;
	}
}