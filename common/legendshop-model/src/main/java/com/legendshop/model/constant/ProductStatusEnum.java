/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

/**
 * 产品状态.
 */
public enum ProductStatusEnum implements IntegerEnum {
	
	/** 仓库中的商品，用户点击下线. */
	PROD_OFFLINE(0),
	
	/** 上线的商品，正常销售的商品. */
	PROD_ONLINE(1),

	/** 商品违规下线 **/
	PROD_ILLEGAL_OFF(2),
	
	/** 商品审核失败状态 **/
	PROD_AUDIT_FAIL(4),
	
	/** 商品待审核状态 **/
	PROD_AUDIT(3),
	
	/** 商品删除状态 (放商品回收站)**/
	PROD_DELETE(-1),
	
	/**商家永久删除状态**/
	PROD_SHOP_DELETE(-2),
	;

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new product status enum.
	 * 
	 * @param num
	 *            the num
	 */
	ProductStatusEnum(Integer num) {
		this.num = num;
	}

}
