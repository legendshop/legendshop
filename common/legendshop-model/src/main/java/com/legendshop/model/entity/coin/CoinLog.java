package com.legendshop.model.entity.coin;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;
/**
 * 金币充值下单记录
 */
@Entity
@Table(name = "ls_coin_log")
public class CoinLog implements GenericEntity<Long> {

	private static final long serialVersionUID = 797201739885970826L;
	
	/** 主键 */
	private Long id;
	
	/** 会员id */
	private String userId;
	
	/** 会员名称 */
	private String userName;
	
	/** 交易流水号 */
	private String sn;
	
	/** order_pay日记类型 */
	private String logType;
	
	/** 金币变更 */
	private Double amount;
	
	/** 描述 */
	private String logDesc;
	
	/** 添加时间 */
	private Date addTime;
	
	public CoinLog(){}
	
	public CoinLog(String userId,String userName,String sn,String logType,Double amount,String logDesc,Date addTime){
		this.setUserId(userId);
		this.setUserName(userName);
		this.setSn(sn);
		this.setLogType(logType);
		this.setAmount(amount);
		this.setLogDesc(logDesc);
		this.setAddTime(addTime);
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "COIN_LOG_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "sn")
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "log_type")
	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "log_desc")
	public String getLogDesc() {
		return logDesc;
	}

	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}

	@Column(name = "add_time")
	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
} 
