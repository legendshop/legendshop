/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.model.dto.order;

import java.util.Date;

import com.legendshop.dao.support.GenericEntity;

public class InvoiceSubDto implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 订单ID */
	private String subNumber;

	private Integer type;

	private Integer title;
	
	private String company;
	
	private Date createTime;
	
	private String invoiceHumNumber;
	
	private String userName;
	
	private Date buyDate;
	
	private Integer hasInvoice;
	
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getInvoiceHumNumber() {
		return invoiceHumNumber;
	}

	public void setInvoiceHumNumber(String invoiceHumNumber) {
		this.invoiceHumNumber = invoiceHumNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public Integer getTitle() {
		return title;
	}

	public void setTitle(Integer title) {
		this.title = title;
	}

	public Integer getHasInvoice() {
		return hasInvoice;
	}

	public void setHasInvoice(Integer hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}
