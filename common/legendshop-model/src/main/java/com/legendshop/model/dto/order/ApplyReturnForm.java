/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * @Description 申请退款及退款表单DTO
 * @author 关开发
 */
public class ApplyReturnForm extends ApplyRefundReturnBase{
	/** 订单项 ID */
	@NotNull
	private Long orderItemId;
	
	/** 退货数量 */
	@NotNull
	@Min(value=1)
	private Long goodsNum;

	/**
	 * @return the goodsNum
	 */
	public Long getGoodsNum() {
		return goodsNum;
	}

	/**
	 * @param goodsNum the goodsNum to set
	 */
	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	/**
	 * @return the orderItemId
	 */
	public Long getOrderItemId() {
		return orderItemId;
	}

	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}
	
}
