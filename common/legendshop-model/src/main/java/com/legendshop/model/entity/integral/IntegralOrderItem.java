package com.legendshop.model.entity.integral;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 积分订单项表
 */
@Entity
@Table(name = "ls_integral_order_item")
public class IntegralOrderItem implements GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	/** 自增编号 */
	private Long id; 
		
	/** 订单编号 */
	private Long orderId; 
		
	/** 用户ID */
	private String userId; 
		
	/** 积分商品ID */
	private Long prodId; 
		
	/** 积分商品名称 */
	private String prodName; 
		
	/** 产品主图片路径 */
	private String prodPic; 
	
	private Integer basketCount;
		
	/** 积分数 */
	private Integer exchangeIntegral; 
		
	/** 商品价格 */
	private Double price; 
		
	
	public IntegralOrderItem() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "INTEGRAL_ORDER_ITEM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "order_id")
	public Long  getOrderId(){
		return orderId;
	} 
		
	public void setOrderId(Long orderId){
			this.orderId = orderId;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "prod_id")
	public Long  getProdId(){
		return prodId;
	} 
		
	public void setProdId(Long prodId){
			this.prodId = prodId;
		}
		
    @Column(name = "prod_name")
	public String  getProdName(){
		return prodName;
	} 
		
	public void setProdName(String prodName){
			this.prodName = prodName;
		}
		
    @Column(name = "prod_pic")
	public String  getProdPic(){
		return prodPic;
	} 
		
	public void setProdPic(String prodPic){
			this.prodPic = prodPic;
		}
		
    @Column(name = "exchange_integral")
	public Integer  getExchangeIntegral(){
		return exchangeIntegral;
	} 
		
	public void setExchangeIntegral(Integer exchangeIntegral){
			this.exchangeIntegral = exchangeIntegral;
		}
		
    @Column(name = "price")
	public Double  getPrice(){
		return price;
	} 
		
	public void setPrice(Double price){
			this.price = price;
		}

	@Column(name = "basket_count")
	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}
	
	

	

} 
