/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.presell;

import java.util.Date;

/**
 * 预售活动搜索参数DTO
 */
public class PresellSearchParamDto {
	
	/** 所有 */
	public static final String ALL = "all";
	
	/** 待提审 */
	public static final String WAIT_PUT_AUDIT = "waitPutAudit";
	
	/** 待审核  */
	public static final String WAIT_AUDIT = "waitAudit";
	
	/** 已拒绝 */
	public static final String DENY = "deny";
	
	/** 上线中 */
	public static final String ONLINE = "online";
	
	/** 已完成 */
	public static final String FINISH = "finish";
	
	/** 已终止 */
	public static final String STOP = "stop";
	
	/** 已过期 */
	public static final String EXPIRE = "expire";
	
	/** 搜索类型 */
	private String searchType;

	/** 方案名称 */
	private String schemeName;
	
	/** 商品名称 */
	private String prodName;
	
	/** 所属商家 */
	private String shopName;
	
	/** 支付类型 */
	private Integer payPctType;
	
	/** 预售开始时间 */
	private Date preSaleStart;
	
	/** 预售结束时间 */
	private Date preSaleEnd;
	
	/** 预售状态 */
	private Integer status;
	
	
	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return searchType;
	}

	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * @return the schemeName
	 */
	public String getSchemeName() {
		return schemeName;
	}

	/**
	 * @param schemeName the schemeName to set
	 */
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	/**
	 * @return the prodName
	 */
	public String getProdName() {
		return prodName;
	}

	/**
	 * @param prodName the prodName to set
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @return the payPctType
	 */
	public Integer getPayPctType() {
		return payPctType;
	}

	/**
	 * @param payPctType the payPctType to set
	 */
	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	/**
	 * @return the preSaleStart
	 */
	public Date getPreSaleStart() {
		return preSaleStart;
	}

	/**
	 * @param preSaleStart the preSaleStart to set
	 */
	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	/**
	 * @return the preSaleEnd
	 */
	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	/**
	 * @param preSaleEnd the preSaleEnd to set
	 */
	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
