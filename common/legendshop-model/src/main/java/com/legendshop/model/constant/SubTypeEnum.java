/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;
/**
 * 支付的订单类型
 *
 */
public enum SubTypeEnum implements StringEnum {

	/**
	 * 普通订单
	 */
	common("COMMON"),
	
	/**
	 * 拼团订单
	 */
	MERGE_GROUP("MERGE_GROUP"),
	
	/**
	 * 团购订单
	 */
	group("GROUP"),
	
	/**
	 * 拍卖订单
	 */
	auctions("AUCTIONS"),
	
	/**
	 * 预售订单
	 */
	PRESELL("PRE_SELL"),  
	
	/**
	 * 门店订单
	 */
	SHOP_STORE("SHOP_STORE"),
	
	/**
	 * 秒杀订单
	 * */
	SECKILL("SECKILL");
	

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private SubTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		SubTypeEnum[] licenseEnums = values();
		for (SubTypeEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the value.
	 * 
	 * @param name
	 *            the name
	 * @return the value
	 */
	public static String getValue(String name) {
		SubTypeEnum[] licenseEnums = values();
		for (SubTypeEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
