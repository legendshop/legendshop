package com.legendshop.model.prod;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("直播礼品Dto")
public class AppProdDto implements Serializable{
	private static final long serialVersionUID = 5049919089101634020L;

	/** 商品id */
	@ApiModelProperty(value = "商品id")
	private Long prodId;
	
	/** 商品名称 */
	@ApiModelProperty(value = "商品名称")
	private String name;
	
	/** 商品图片 */
	@ApiModelProperty(value = "商品图片")
	private String pic;
	
	/** 现价 */
	@ApiModelProperty(value = "现价")
	private Double cash;
	
	/** 原价 */
	@ApiModelProperty(value = "原价")
	private Double price;
	
	/** 库存 */
	@ApiModelProperty(value = "库存")
	private Integer stocks;
	
	/** 已购买数量 */
	@ApiModelProperty(value = "已购买数量")
	private Long buys;
	
	/** 商品状态 */
	@ApiModelProperty(value = "商品状态")
	private Integer status;
	
	/** 分享链接 */
	@ApiModelProperty(value = "分享链接")
	private String shareUrl;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	
	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public Long getBuys() {
		return buys;
	}

	public void setBuys(Long buys) {
		this.buys = buys;
	}

	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	@Override
	public String toString() {
		return "AppProdDto [prodId=" + prodId + ", name=" + name + ", pic=" + pic + ", price=" + price + ", stocks=" + stocks + ", buys=" + buys + "]";
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}
}
