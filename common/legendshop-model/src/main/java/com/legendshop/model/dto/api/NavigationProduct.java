package com.legendshop.model.dto.api;

/**
 * 
 * @author tony
 *
 */
public class NavigationProduct {

	
	private Long prodId;
	
	private String pic;
	
	private String name;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
