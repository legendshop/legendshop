package com.legendshop.model.dto.marketing;

import java.util.Date;


public class PurchaseProdDto{

	private  Long prodId;
	
	private String prodName;
	
	private String brief;
	
	private String pic;
	
	private Double cash;
	
	private Double discountPrice;
	
	private Date startTime;
	
	private Date endTime;
	
	public Long getProdId() {
		return prodId;
	}
	
	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public String getProdName() {
		return prodName;
	}
	
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	public String getBrief() {
		return brief;
	}
	
	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	public String getPic() {
		return pic;
	}
	
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public Double getCash() {
		return cash;
	}
	
	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	public Double getDiscountPrice() {
		return discountPrice;
	}
	
	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
