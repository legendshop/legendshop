package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.support.GenericEntity;

/**
 * 商品二次评论表
 */
@Entity
@Table(name = "ls_prod_add_comm")
public class ProdAddComm implements GenericEntity<Long> {

	private static final long serialVersionUID = -7029873587953849831L;

	/** 主键ID */
	private Long id; 
		
	/** 商品评论ID */
	private Long prodCommId; 
		
	/** 评论内容 */
	private String content; 
	
	/** 评论图片 多张已逗号隔开 */
	private String photos;
		
	/** 来源IP */
	private String postip; 
		
	/** 是否显示，1:为显示，0:待审核， -1：不通过审核，不显示。 如果需要审核评论，则是0,，否则1 */
	private Integer status; 
	
	/** 创建时间 */
	private Date createTime;
	
	/** 商家是否已回复 */
	private Boolean isReply;
		
	/** 商家回复 */
	private String shopReplyContent; 
		
	/** 商家回复时间 */
	private Date shopReplyTime; 
		
	
	public ProdAddComm() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "PROD_ADD_COMM_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "prod_comm_id")
	public Long  getProdCommId(){
		return prodCommId;
	} 
		
	public void setProdCommId(Long prodCommId){
			this.prodCommId = prodCommId;
		}
		
    @Column(name = "content")
	public String  getContent(){
		return content;
	} 
		
	public void setContent(String content){
			this.content = content;
		}
	
	@Column(name = "photos")
    public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	@Column(name = "postip")
	public String  getPostip(){
		return postip;
	} 
		
	public void setPostip(String postip){
		this.postip = postip;
	}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
	
	public void setStatus(Integer status){
			this.status = status;
		}
	
	@Column(name = "create_time")
    public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "is_reply")
	public Boolean getIsReply() {
		return isReply;
	}

	public void setIsReply(Boolean isReply) {
		this.isReply = isReply;
	}

	@Column(name = "shop_reply_content")
	public String  getShopReplyContent(){
		return shopReplyContent;
	} 
		
	public void setShopReplyContent(String shopReplyContent){
			this.shopReplyContent = shopReplyContent;
		}
		
    @Column(name = "shop_reply_time")
	public Date  getShopReplyTime(){
		return shopReplyTime;
	} 
		
	public void setShopReplyTime(Date shopReplyTime){
			this.shopReplyTime = shopReplyTime;
	}
} 
