package com.legendshop.model.form;

import java.io.Serializable;

import com.legendshop.model.constant.OrderStatusEnum;

/**
 * 用户支付银行 返回业务参数封装
 * 
 * @author tony
 * 
 */
public class BankCallbackForm implements Serializable {

	private static final long serialVersionUID = -3117379476216708665L;

	/**
	 * 外部订单编号 ----> settlementNumbers
	 */
	private String outOrderNo;

	/**
	 * 支付流水号 ----> 第三方或者银行返回
	 */
	private String flowTradeNo;

	/**
	 * 支付者的帐号信息
	 */
	private String payUserInfo;

	/**
	 * 总金额
	 */
	private String totalFee;

	/**
	 * 验证订单状态
	 */
	private OrderStatusEnum validateStatus;

	/**
	 * 要修改的订单状态
	 */
	private OrderStatusEnum updateStatus;

	public BankCallbackForm(String totalFee, String outOrderNo, String flowTradeNo, String payUserInfo, OrderStatusEnum validateStatus,
			OrderStatusEnum updateStatus) {
		super();
		this.outOrderNo = outOrderNo;
		this.flowTradeNo = flowTradeNo;
		this.payUserInfo = payUserInfo;
		this.validateStatus = validateStatus;
		this.updateStatus = updateStatus;
		this.totalFee = totalFee;
	}

	public BankCallbackForm() {
		super();
	}

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public String getPayUserInfo() {
		return payUserInfo;
	}

	public String getTotalFee() {
		return totalFee;
	}

	public OrderStatusEnum getValidateStatus() {
		return validateStatus;
	}

	public OrderStatusEnum getUpdateStatus() {
		return updateStatus;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	public void setPayUserInfo(String payUserInfo) {
		this.payUserInfo = payUserInfo;
	}

	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	public void setValidateStatus(OrderStatusEnum validateStatus) {
		this.validateStatus = validateStatus;
	}

	public void setUpdateStatus(OrderStatusEnum updateStatus) {
		this.updateStatus = updateStatus;
	}
	
	

}
