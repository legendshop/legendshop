package com.legendshop.model.entity;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *附件类型
 */
@Entity
@Table(name = "ls_attmnt_tree")
public class AttachmentTree implements GenericEntity<Long> {

	private static final long serialVersionUID = -7798050331228280874L;

	/** 主键 */
	private Long id; 
		
	/** 分类名称 */
	private String name; 
		
	/** 用户名称 */
	private String userName; 
		
	/** 父节点 */
	private Long parentId; 
	
	private boolean isParent = true;
	
	private boolean open = true;
	
	   /** The shop id. */
    private Long shopId;
    
    /** 子目录*. */
	private List<AttachmentTree> childrenList;
    
    
	public AttachmentTree() { 
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "ATTMNT_TREE_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "name")
	public String  getName(){
		return name;
	} 
		
	public void setName(String name){
			this.name = name;
		}
		
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    @Column(name = "parent_id")
	public Long  getParentId(){
		return parentId;
	} 
		
	public void setParentId(Long parentId){
			this.parentId = parentId;
		}

	@Transient
	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	@Transient
	public boolean getOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	/**
	 * Gets the shop id.
	 *
	 * @return the shop id
	 */
    @Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	/**
	 * Sets the shop id.
	 *
	 * @param shopId the new shop id
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	@Transient
	public List<AttachmentTree> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<AttachmentTree> childrenList) {
		this.childrenList = childrenList;
	}
	
	public void addChildren(AttachmentTree attachmentTree) {
		if(childrenList == null){
			childrenList = new ArrayList<AttachmentTree>();
		}
		childrenList.add(attachmentTree);
	}

	

} 
