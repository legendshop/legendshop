package com.legendshop.model.entity;
import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


/**
 *优惠券
 */
@Entity
@Table(name = "ls_coupon")
@ApiModel(value="Coupon优惠券") 
public class Coupon implements GenericEntity<Long> {

	private static final long serialVersionUID = 5355919958229807487L;
	
	/** 主键id */
	@ApiModelProperty(value="主键ID") 
	private Long id;
	
	/** 优惠券 ID */
	@ApiModelProperty(value="优惠券 ID")  
	private Long couponId; 
		
	/** 优惠券 名称 */
	@ApiModelProperty(value="优惠券 名称")  
	private String couponName; 
		
	@Deprecated
	/** 优惠券 号码 前缀 */
	@ApiModelProperty(value="优惠券 号码 前缀")  
	private String couponNumPrefix; 
		
	/** 优惠券状态 */
	@ApiModelProperty(value="优惠券状态")  
	private Integer status; 
		
	/** 劵值满多少金额 */
	@ApiModelProperty(value="劵值满多少金额")  
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	@ApiModelProperty(value="劵值减多少金额")  
	private Double offPrice; 
		
	/** 活动开始时间 */
	@ApiModelProperty(value="活动开始时间")  
	private Date startDate; 
		
	/** 活动结束时间 */
	@ApiModelProperty(value="活动结束时间")  
	private Date endDate; 
	
	/** 优惠券提供方：平台: platform，店铺:shop */
	@ApiModelProperty(value="优惠券提供方：平台: platform，店铺:shop")  
	private String couponProvider;
	
	/** 分类ID */
	@ApiModelProperty(value="分类ID ")  
	private Long categoryId;
	
	
	/** 领取方式：积分兑换:points，卡密兑换:pwd，免费领取:free */
	@ApiModelProperty(value="领取方式：积分兑换:points，卡密兑换:pwd，免费领取:free,大转盘红包:draw")  
	private String getType;
	
	/** 兑换所需积分（领取方式为"积分兑换"时生效） **/
	@ApiModelProperty(value="兑换所需积分（领取方式为'积分兑换'时生效）")  
	private Integer needPoints;
	
	/** 修改时间 **/
	@ApiModelProperty(value="修改时间")  
	private Date modifyDate;
	
	/** 优惠券图片 **/
	@ApiModelProperty(value="优惠券图片")  
	private String couponPic;
		
	@Deprecated
	/** 领取频率 */
	@ApiModelProperty(value="领取频率")  
	private Double frequency; 
		
	/** 优惠券的类型 */
	@ApiModelProperty(value="礼券类型：通用券:common，品类券:category，指定商品券:product")  
	private String couponType; 
		
	@Deprecated
	/** 如何发放此类型红包 */
	@ApiModelProperty(value="如何发放此类型红包")  
	private Integer sendType; 
		
	/** 领取上限 */
	@ApiModelProperty(value="领取上限")  
	private Long getLimit; 
		
	/** 初始化券数量 */
	@ApiModelProperty(value="初始化券数量")  
	private Long couponNumber; 
		
	/** 绑定优惠券数量 */
	@ApiModelProperty(value="绑定优惠券数量")  
	private Long bindCouponNumber; 
		
	@Deprecated
	/** 发放优惠券数量 */
	@ApiModelProperty(value="发放优惠券数量")  
	private Long sendCouponNumber; 
		
	/** 使用优惠券数量 */
	@ApiModelProperty(value="使用优惠券数量")  
	private Long useCouponNumber; 
		
	/** 创建时间 */
	@ApiModelProperty(value="创建时间")  
	private Date createDate; 
		
	/** 描述 */
	@ApiModelProperty(value="描述")  
	private String description; 
		
	@Deprecated
	/** 通知方式[站内信,邮件,短信] */
	@ApiModelProperty(value="通知方式[站内信,邮件,短信]")  
	private String messageType; 
	
	/** 优惠券用户ID */
	@ApiModelProperty(value=" 优惠券用户ID")  
	private Long couponUsrId;  
	
	/**优惠优惠券券号**/
	@ApiModelProperty(value="优惠优惠券券号")  
	private String couponSn;
	
	/**使用日期**/
	@ApiModelProperty(value="使用日期")  
	private Date useTime;
	
	/**
	 * 活动的时间状态(1.已过期 2.进行中 3,将要开始)
	 */
	@ApiModelProperty(value="活动的时间状态(1.已过期 2.进行中 3,将要开始)")  
	private int timeStatus;
	
	/** 剩余时间*/
	@ApiModelProperty(value="剩余时间")  
	private String surplusTime;
	
	/**券值*/
	@ApiModelProperty(value="券值")  
	private String couponPrice;
	
	/** 店铺id */
	@ApiModelProperty(value="店铺id")  
	private Long shopId;
	
	/** 店铺名称 */
	@ApiModelProperty(value="店铺名称")  
	private String shopName;
	
	/** 商品id列表 */
	@ApiModelProperty(value="商品id列表")  
	private String prodidList;
	
	/** 上传文件 */
	@ApiModelProperty(value="上传文件")  
	protected MultipartFile file;
	
	/** 未知 */
	@ApiModelProperty(value="siteName",hidden = true)  
	private String siteName;
	
	/** 产品类目名称 */
	@ApiModelProperty(value="产品类目名称")  
	private String name;
	
	/** 店铺id列表 */
	@ApiModelProperty(value="店铺id列表")  
	private String shopIdList;
	
	/**是否对指定用户发放*/
	@ApiModelProperty(value="是否对指定用户发放")  
	private Integer isDsignatedUser;
	
	/**发布体验劵时,所选的用户id*/
	@ApiModelProperty(value="发布体验劵时,所选的用户id")  
	private String userIdLists;
	
	/**卖家是否删除(0未删除,1删除)*/ //原来的含义搞反了? Newway, 1应该代表true, 0代表false
	@ApiModelProperty(value="卖家是否删除(0未删除,1删除)")
	private Integer shopDel;
	
	public Coupon() {
    }
		
	@Id
	@Column(name = "coupon_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "COUPON_SEQ")
	public Long  getCouponId(){
		return couponId;
	} 
		
	public void setCouponId(Long couponId){
			this.couponId = couponId;
		}
		
    @Column(name = "coupon_name")
	public String  getCouponName(){
		return couponName;
	} 
		
	public void setCouponName(String couponName){
			this.couponName = couponName;
		}
		
	@Deprecated
    @Column(name = "coupon_num_prefix")
	public String  getCouponNumPrefix(){
		return couponNumPrefix;
	} 
		
	@Deprecated
	public void setCouponNumPrefix(String couponNumPrefix){
			this.couponNumPrefix = couponNumPrefix;
		}
		
    @Column(name = "status")
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
    @Column(name = "full_price")
	public Double  getFullPrice(){
		return fullPrice;
	} 
		
	public void setFullPrice(Double fullPrice){
			this.fullPrice = fullPrice;
		}
		
    @Column(name = "off_price")
	public Double  getOffPrice(){
		return offPrice;
	} 
		
	public void setOffPrice(Double offPrice){
			this.offPrice = offPrice;
		}
		
    @Column(name = "start_date")
	public Date  getStartDate(){
		return startDate;
	} 
		
	public void setStartDate(Date startDate){
			this.startDate = startDate;
		}
		
    @Column(name = "end_date")
	public Date  getEndDate(){
		return endDate;
	} 
		
	public void setEndDate(Date endDate){
			this.endDate = endDate;
		}
		
	@Deprecated
    @Column(name = "frequency")
	public Double  getFrequency(){
		return frequency;
	} 
		
	@Deprecated
	public void setFrequency(Double frequency){
			this.frequency = frequency;
		}
		
    @Column(name = "coupon_type")
	public String  getCouponType(){
		return couponType;
	} 
		
	public void setCouponType(String couponType){
			this.couponType = couponType;
		}
		
	@Deprecated
    @Column(name = "send_type")
	public Integer  getSendType(){
		return sendType;
	} 
		
	@Deprecated
	public void setSendType(Integer sendType){
			this.sendType = sendType;
		}
		
    @Column(name = "get_limit")
	public Long  getGetLimit(){
		return getLimit;
	} 
		
	public void setGetLimit(Long getLimit){
			this.getLimit = getLimit;
		}
		
    @Column(name = "coupon_number")
	public Long  getCouponNumber(){
		return couponNumber;
	} 
		
	public void setCouponNumber(Long couponNumber){
			this.couponNumber = couponNumber;
		}
		
    @Column(name = "bind_coupon_number")
	public Long  getBindCouponNumber(){
		return bindCouponNumber;
	} 
		
	public void setBindCouponNumber(Long bindCouponNumber){
			this.bindCouponNumber = bindCouponNumber;
		}
		
	@Deprecated
    @Column(name = "send_coupon_number")
	public Long  getSendCouponNumber(){
		return sendCouponNumber;
	} 
		
	@Deprecated
	public void setSendCouponNumber(Long sendCouponNumber){
			this.sendCouponNumber = sendCouponNumber;
		}
		
    @Column(name = "use_coupon_number")
	public Long  getUseCouponNumber(){
		return useCouponNumber;
	} 
		
	public void setUseCouponNumber(Long useCouponNumber){
			this.useCouponNumber = useCouponNumber;
		}
		
    @Column(name = "create_date")
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
    @Column(name = "description")
	public String  getDescription(){
		return description;
	} 
		
	public void setDescription(String description){
			this.description = description;
		}
		
	@Deprecated
    @Column(name = "message_type")
	public String  getMessageType(){
		return messageType;
	} 
		
	@Deprecated
	public void setMessageType(String messageType){
			this.messageType = messageType;
		}
	
	@Transient
	public Long getId() {
		return couponId;
	}
	
	public void setId(Long id) {
		couponId = id;
	}
	
	
	@Transient
	public Date getUseTime() {
		return useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	
	@Transient
	public String getSurplusTime() {
		return surplusTime;
	}

	public void setSurplusTime(String surplusTime) {
		this.surplusTime = surplusTime;
	}

	@Transient
	public int getTimeStatus() {
		return timeStatus;
	}

	public void setTimeStatus(int timeStatus) {
		this.timeStatus = timeStatus;
	}

	@Transient
	public Long getCouponUsrId() {
		return couponUsrId;
	}

	public void setCouponUsrId(Long couponUsrId) {
		this.couponUsrId = couponUsrId;
	}

	@Transient
	public String getCouponSn() {
		return couponSn;
	}

	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}

	@Transient
	public String getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(String couponPrice) {
		this.couponPrice = couponPrice;
	}

	 @Column(name = "shop_id")
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	@Transient
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	@Transient
	public String getProdidList() {
		return prodidList;
	}

	public void setProdidList(String prodidList) {
		this.prodidList = prodidList;
	}

	@Column(name = "coupon_provider")
	public String getCouponProvider() {
		return couponProvider;
	}

	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}

	@Column(name = "category_id")
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "get_type")
	public String getGetType() {
		return getType;
	}

	public void setGetType(String getType) {
		this.getType = getType;
	}

	@Column(name = "need_points")
	public Integer getNeedPoints() {
		return needPoints;
	}

	public void setNeedPoints(Integer needPoints) {
		this.needPoints = needPoints;
	}

	@Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Column(name = "coupon_pic")
	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}
	
	@Column(name = "shop_del")
	public Integer getShopDel() {
		return shopDel;
	}

	public void setShopDel(Integer shopDel) {
		this.shopDel = shopDel;
	}
	
	@Transient
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	@Transient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Transient
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Transient
	public String getShopIdList() {
		return shopIdList;
	}

	public void setShopIdList(String shopIdList) {
		this.shopIdList = shopIdList;
	}

	@Column(name = "is_dsignated_user")
	public Integer getIsDsignatedUser() {
		return isDsignatedUser;
	}

	public void setIsDsignatedUser(Integer isDsignatedUser) {
		this.isDsignatedUser = isDsignatedUser;
	}
	
	@Transient
	public String getUserIdLists() {
		return userIdLists;
	}

	public void setUserIdLists(String userIdLists) {
		this.userIdLists = userIdLists;
	}
} 
