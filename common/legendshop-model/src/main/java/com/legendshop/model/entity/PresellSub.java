/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 预售订单.
 */
@Entity
@Table(name = "ls_presell_sub")
public class PresellSub implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2702328803464625844L;

	/** 订购ID. */
	private Long subId; 
	
	/** 订单总金额. */
	private java.math.BigDecimal total; 
	
	/** 订购ID. */
	private String subNumber; 
		
	/** 支付方式,0:全额,1:订金. */
	private Integer payPctType; 
		
	/** 订金金额. */
	private java.math.BigDecimal preDepositPrice; 
		
	/** 尾款金额. */
	private java.math.BigDecimal finalPrice; 
	
	/** 订金支付开始时间. */
	private Date preSaleStart; 
	
	/** 订金支付结束时间. */
	private Date preSaleEnd; 
		
	/** 尾款支付开始时间. */
	private Date finalMStart; 
		
	/** 尾款支付结束时间. */
	private Date finalMEnd; 
		
	/** 订金支付名称. */
	private String depositPayName; 
		
	/** 订金支付流水号. */
	private String depositTradeNo; 
		
	/** 订金支付时间. */
	private Date depositPayTime; 
		
	/** 是否支付订金. */
	private Integer isPayDeposit; 
	
	/** ls_sub_settlement清算单据号 */
	private String finalSettlementSn;  
		
	/** 尾款支付名称. */
	private String finalPayName; 
	
	/** 尾款支付类型Id. */
	private String finalPayTypeId;
		
	/** 尾款支付时间. */
	private Date finalPayTime; 
		
	/** 尾款支付流水号. */
	private String finalTradeNo; 
		
	/** 是否支付尾款. */
	private Integer isPayFinal;

	/** 是否结算[用于结算档期统计,结算未退还的定金]  */
	private Boolean isBill;

	/** 结算编号[用于结算档期统计]  */
	private String billSn;
	
	/**
	 * The Constructor.
	 */
	public PresellSub() {
    }
		
	/**
	 * Gets the sub id.
	 *
	 * @return the sub id
	 */
	@Id
	@Column(name = "sub_id")
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
	public Long  getSubId(){
		return subId;
	} 
		
	/**
	 * Sets the sub id.
	 *
	 * @param subId the sub id
	 */
	public void setSubId(Long subId){
			this.subId = subId;
		}
		
    /**
     * Gets the pay pct type.
     *
     * @return the pay pct type
     */
    @Column(name = "pay_pct_type")
	public Integer  getPayPctType(){
		return payPctType;
	} 
		
	/**
	 * Sets the pay pct type.
	 *
	 * @param payPctType the pay pct type
	 */
	public void setPayPctType(Integer payPctType){
			this.payPctType = payPctType;
		}
		
    /**
     * Gets the pre deposit price.
     *
     * @return the pre deposit price
     */
    @Column(name = "pre_deposit_price")
	public java.math.BigDecimal  getPreDepositPrice(){
		return preDepositPrice;
	} 
		
	/**
	 * Sets the pre deposit price.
	 *
	 * @param preDepositPrice the pre deposit price
	 */
	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice){
			this.preDepositPrice = preDepositPrice;
		}
		
    /**
     * Gets the final price.
     *
     * @return the final price
     */
    @Column(name = "final_price")
	public java.math.BigDecimal  getFinalPrice(){
		return finalPrice;
	} 
		
	/**
	 * Sets the final price.
	 *
	 * @param finalPrice the final price
	 */
	public void setFinalPrice(java.math.BigDecimal finalPrice){
			this.finalPrice = finalPrice;
	}
	
	
	
	
		
    /**
     * Gets the final m start.
     *
     * @return the final m start
     */
    @Column(name = "final_m_start")
	public Date  getFinalMStart(){
		return finalMStart;
	} 
		
	/**
	 * Sets the final m start.
	 *
	 * @param finalMStart the final m start
	 */
	public void setFinalMStart(Date finalMStart){
			this.finalMStart = finalMStart;
		}
		
    /**
     * Gets the final m end.
     *
     * @return the final m end
     */
    @Column(name = "final_m_end")
	public Date  getFinalMEnd(){
		return finalMEnd;
	} 
		
	/**
	 * Sets the final m end.
	 *
	 * @param finalMEnd the final m end
	 */
	public void setFinalMEnd(Date finalMEnd){
			this.finalMEnd = finalMEnd;
		}
		
    /**
     * Gets the deposit pay name.
     *
     * @return the deposit pay name
     */
    @Column(name = "deposit_pay_name")
	public String  getDepositPayName(){
		return depositPayName;
	} 
		
	/**
	 * Sets the deposit pay name.
	 *
	 * @param depositPayName the deposit pay name
	 */
	public void setDepositPayName(String depositPayName){
			this.depositPayName = depositPayName;
		}
		
    /**
     * Gets the deposit trade no.
     *
     * @return the deposit trade no
     */
    @Column(name = "deposit_trade_no")
	public String  getDepositTradeNo(){
		return depositTradeNo;
	} 
		
	/**
	 * Sets the deposit trade no.
	 *
	 * @param depositTradeNo the deposit trade no
	 */
	public void setDepositTradeNo(String depositTradeNo){
			this.depositTradeNo = depositTradeNo;
		}
		
    /**
     * Gets the deposit pay time.
     *
     * @return the deposit pay time
     */
    @Column(name = "deposit_pay_time")
	public Date  getDepositPayTime(){
		return depositPayTime;
	} 
		
	/**
	 * Sets the deposit pay time.
	 *
	 * @param depositPayTime the deposit pay time
	 */
	public void setDepositPayTime(Date depositPayTime){
			this.depositPayTime = depositPayTime;
		}
		
    /**
     * Gets the is pay deposit.
     *
     * @return the checks if is pay deposit
     */
    @Column(name = "is_pay_deposit")
	public Integer  getIsPayDeposit(){
		return isPayDeposit;
	} 
		
	/**
	 * Sets the is pay deposit.
	 *
	 * @param isPayDeposit the checks if is pay deposit
	 */
	public void setIsPayDeposit(Integer isPayDeposit){
			this.isPayDeposit = isPayDeposit;
		}
		
    /**
     * Gets the final pay name.
     *
     * @return the final pay name
     */
    @Column(name = "final_pay_name")
	public String  getFinalPayName(){
		return finalPayName;
	} 
		
	/**
	 * Sets the final pay name.
	 *
	 * @param finalPayName the final pay name
	 */
	public void setFinalPayName(String finalPayName){
			this.finalPayName = finalPayName;
		}
		
    /**
     * Gets the final pay time.
     *
     * @return the final pay time
     */
    @Column(name = "final_pay_time")
	public Date  getFinalPayTime(){
		return finalPayTime;
	} 
		
	/**
	 * Sets the final pay time.
	 *
	 * @param finalPayTime the final pay time
	 */
	public void setFinalPayTime(Date finalPayTime){
			this.finalPayTime = finalPayTime;
		}
		
    /**
     * Gets the final trade no.
     *
     * @return the final trade no
     */
    @Column(name = "final_trade_no")
	public String  getFinalTradeNo(){
		return finalTradeNo;
	} 
		
	/**
	 * Sets the final trade no.
	 *
	 * @param finalTradeNo the final trade no
	 */
	public void setFinalTradeNo(String finalTradeNo){
			this.finalTradeNo = finalTradeNo;
		}
		
    /**
     * Gets the is pay final.
     *
     * @return the checks if is pay final
     */
    @Column(name = "is_pay_final")
	public Integer  getIsPayFinal(){
		return isPayFinal;
	} 
		
	/**
	 * Sets the is pay final.
	 *
	 * @param isPayFinal the checks if is pay final
	 */
	public void setIsPayFinal(Integer isPayFinal){
			this.isPayFinal = isPayFinal;
		}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public Long getId() {
		return subId;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		subId = id;
	}

	
	/**
	 * Gets the sub number.
	 *
	 * @return the sub number
	 */
	@Column(name = "sub_number")
	public String getSubNumber() {
		return subNumber;
	}

	/**
	 * Sets the sub number.
	 *
	 * @param subNumber the sub number
	 */
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	@Transient
	public java.math.BigDecimal getTotal() {
		return total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the total
	 */
	public void setTotal(java.math.BigDecimal total) {
		this.total = total;
	}

	@Column(name = "final_settlement_sn")
	public String getFinalSettlementSn() {
		return finalSettlementSn;
	}

	public void setFinalSettlementSn(String finalSettlementSn) {
		this.finalSettlementSn = finalSettlementSn;
	}

	@Column(name = "pre_sale_start")
	public Date getPreSaleStart() {
		return preSaleStart;
	}

	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	@Column(name = "pre_sale_end")
	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}
	
	@Column(name = "final_pay_type_id")
	public String getFinalPayTypeId() {
		return finalPayTypeId;
	}

	public void setFinalPayTypeId(String finalPayTypeId) {
		this.finalPayTypeId = finalPayTypeId;
	}

	@Column(name="is_bill")
	public Boolean getIsBill() {
		return isBill;
	}

	public void setIsBill(Boolean isBill) {
		this.isBill = isBill;
	}

	@Column(name="bill_sn")
	public String getBillSn() {
		return billSn;
	}

	public void setBillSn(String billSn) {
		this.billSn = billSn;
	}

} 
