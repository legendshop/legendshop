package com.legendshop.model.entity.draw;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *抽奖记录表
 */
@Entity
@Table(name = "ls_draw_record")
public class DrawRecord implements GenericEntity<Long> {

	private static final long serialVersionUID = -7596535328209201587L;

	/** id */
	private Long id; 
		
	/** 用户id */
	private String userId; 
		
	/** 微信openid */
	private String weixinOpenid; 
		
	/** 微信昵称 */
	private String weixinNickname; 
		
	/** 活动ID */
	private Long actId; 
		
	/**抽奖时间**/
	private Date awardsTime;
		
	/** 中奖id */
	private Long winId; 
		
	//查询时间
	private Date startTime; 
	private Date endTime; 
	private String actName;
	private Integer winStatus;//中奖状态
	public DrawRecord() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DRAW_RECORD_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    @Column(name = "weixin_openid")
	public String  getWeixinOpenid(){
		return weixinOpenid;
	} 
		
	public void setWeixinOpenid(String weixinOpenid){
			this.weixinOpenid = weixinOpenid;
		}
		
    @Column(name = "weixin_nickname")
	public String  getWeixinNickname(){
		return weixinNickname;
	} 
		
	public void setWeixinNickname(String weixinNickname){
			this.weixinNickname = weixinNickname;
		}
		
    @Column(name = "act_id")
	public Long  getActId(){
		return actId;
	} 
		
	public void setActId(Long actId){
			this.actId = actId;
		}
		
		
    @Column(name = "win_id")
	public Long  getWinId(){
		return winId;
	} 
		
	public void setWinId(Long winId){
			this.winId = winId;
		}

	 
	@Transient
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Transient
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@Transient
	public String getActName() {
		return actName;
	}

	public void setActName(String actName) {
		this.actName = actName;
	}

	
	@Column(name = "awards_time")
	public Date getAwardsTime() {
		return awardsTime;
	}

	public void setAwardsTime(Date awardsTime) {
		this.awardsTime = awardsTime;
	}

	@Transient
	public Integer getWinStatus() {
		return winStatus;
	}

	public void setWinStatus(Integer winStatus) {
		this.winStatus = winStatus;
	}
	
	


} 
