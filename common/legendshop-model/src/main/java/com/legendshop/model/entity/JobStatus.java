package com.legendshop.model.entity;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 *Job状态控制表
 */
@Entity
@Table(name = "ls_job_status")
public class JobStatus implements GenericEntity<Long> {

	private static final long serialVersionUID = -631129546970869635L;

	/**  */
	private Long id; 
		
	/** 运行的Job名字 */
	private String jobName; 
		
	/**  */
	private Long status; 
		
	/** 更新时间 */
	private Date updateTime; 
	
	private boolean startSuccessed = false;
		
	
	public JobStatus() {
    }
		
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "JOB_STATUS_SEQ")
	public Long  getId(){
		return id;
	} 
		
	public void setId(Long id){
			this.id = id;
		}
		
    @Column(name = "job_name")
	public String  getJobName(){
		return jobName;
	} 
		
	public void setJobName(String jobName){
			this.jobName = jobName;
		}
		
    @Column(name = "status")
	public Long  getStatus(){
		return status;
	} 
		
	public void setStatus(Long status){
			this.status = status;
		}
		
    @Column(name = "update_time")
	public Date  getUpdateTime(){
		return updateTime;
	} 
		
	public void setUpdateTime(Date updateTime){
			this.updateTime = updateTime;
		}

    @Transient
	public boolean isStartSuccessed() {
		return startSuccessed;
	}

	public void setStartSuccessed(boolean startSuccessed) {
		this.startSuccessed = startSuccessed;
	}
	


} 
