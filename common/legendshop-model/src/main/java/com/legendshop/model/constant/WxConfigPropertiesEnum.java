package com.legendshop.model.constant;

import com.legendshop.util.FileConfig;
import com.legendshop.util.FileConfigEnum;

/**
 * wechart.properties的配置文件
 *
 */
public enum WxConfigPropertiesEnum implements FileConfigEnum{

	/**
	 * 微信服务的外部域名
	 */
	WEIXIN_DOMAIN_NAME,
	
	/**
	 * 微信服务的内部IP或者docker服务, 是供系统内部调用之用
	 */
	WEIXIN_SERVCE, 
	
	/**
	 * 是否启用了文本消息记录
	 */
	IS_RECORD_RECEIVETEXT,
	
	/**
	 * #是否启动图灵智能机器人, 集成 http://www.turingapi.com
	 */
	START_INTELLIGENT_ROBOT,
	
	/**
	 * 在其他系统和微信服务(weixin_server)之间沟通的秘钥
	 */
	WEIXIN_KEY,
	
	ALLOWIPLIST, 
	
	;

	@Override
	public String file() {
		return FileConfig.WeiXinConfigFile;
	}
	
	
}
