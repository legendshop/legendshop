/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.legendshop.dao.support.GenericEntity;

public class MyPersonInfo implements GenericEntity<String> {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4386671445232853240L;

	private String id;

	private String nickName;
	
	private String userName;
	
	private String realName;
	
	private String userMail;
	
	private String userMobile;
	
	private String idCard;
	
	private String userAdds;
	
	private Date birthDate;
	
	private String sex;
	
	private String qq;
	
	private String areaid;
	
	private String area;
	
	private String provinceid;
	
	private String province;
	
	private String cityid;
	
	private String city;
	
	private String gradeName;
	//婚姻状态
	private Integer marryStatus;
	//收入情况
	private Integer incomeLevel;
	//兴趣爱好
	private String interest;
	//行为等级
	private Integer behaviorLevel;
	//头像路径
	private String portraitPic;
	//预存款
	private Double availablePredeposit;
	//积分
	private Integer score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getUserAdds() {
		return userAdds;
	}

	public void setUserAdds(String userAdds) {
		this.userAdds = userAdds;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getAreaid() {
		return areaid;
	}

	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}

	public String getProvinceid() {
		return provinceid;
	}

	public void setProvinceid(String provinceid) {
		this.provinceid = provinceid;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Integer getMarryStatus() {
		return marryStatus;
	}

	public void setMarryStatus(Integer marryStatus) {
		this.marryStatus = marryStatus;
	}

	public Integer getIncomeLevel() {
		return incomeLevel;
	}

	public void setIncomeLevel(Integer incomeLevel) {
		this.incomeLevel = incomeLevel;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public Integer getBehaviorLevel() {
		return behaviorLevel;
	}

	public void setBehaviorLevel(Integer behaviorLevel) {
		this.behaviorLevel = behaviorLevel;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}