/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.order;

import java.util.Date;

/**
 * @Description 
 * @author 关开发
 */
public class RefundSearchParamDto {
	/** 订单编号 */
	public static final Long ORDER_NO = 1L; 
	/** 退款编号 */
	public static final Long REFUND_NO = 2L; 
	/** 商家名称 */
	public static final Long SHOP_NAME = 3L; 
	/** 会员名称 */
	public static final Long USER_NAME = 4L; 
	
	/** 申请类型 1:退款,2:退款且退货 */
	private Long applyType;
	/** 开始时间 */
	private Date startDate;
	/** 结束时间 */
	private Date endDate;
	/** 卖家处理状态 */
	private Long sellerState;
	/** 编号类型 1:订单编号,2:退款编号,3:会员名称 */
	private Long numType;
	/** 编号 */
	private String number;
	/** 申请状态 */
	private Long applyState;
	
	/***
	 * 自定义组合状态义状态  用于退款退货操作状态查询
	 * sellerState  卖家处理状态
	 * returnType   退货类型
	 * goodsState  	物流状态
	 * applyState   申请状态
	 * 规则: customStatus = 1, 即对应  sellerState = 1;returnType= null; returnType =null
	 * 	  	customStatus = 2, 即对应  sellerState = 2;returnType= 2; returnType =1
	 * 		...
	 * 
	 * 		    sellerState  returnType   goodsState   applyState
	 * (1)待审核      	1										1
	 * (2)已撤销		1										-1
	 * (3)待买家退货	2			2			1
	 * (4)待收货		2			2			2
	 * (5)未收到		2			2			3
	 * (6)已收到		2			2			4
	 * (7)弃货		2			1
	 * (8)已同意        	2
	 * (9)不同意		3
	 * **/
	private Integer customStatus;
	
	//店铺id
	private Integer shopId;
	
	/**
	 * @return the applyType
	 */
	public Long getApplyType() {
		return applyType;
	}
	/**
	 * @param applyType the applyType to set
	 */
	public void setApplyType(Long applyType) {
		this.applyType = applyType;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the sellerState
	 */
	public Long getSellerState() {
		return sellerState;
	}
	/**
	 * @param sellerState the sellerState to set
	 */
	public void setSellerState(Long sellerState) {
		this.sellerState = sellerState;
	}
	/**
	 * @return the numType
	 */
	public Long getNumType() {
		return numType;
	}
	/**
	 * @param numType the numType to set
	 */
	public void setNumType(Long numType) {
		this.numType = numType;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the applyState
	 */
	public Long getApplyState() {
		return applyState;
	}
	/**
	 * @param applyState the applyState to set
	 */
	public void setApplyState(Long applyState) {
		this.applyState = applyState;
	}
	
	public Integer getCustomStatus() {
		return customStatus;
	}
	public void setCustomStatus(Integer customStatus) {
		this.customStatus = customStatus;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	
	
}
