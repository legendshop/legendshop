/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.entity.ImgFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品DTO
 *
 */
@ApiModel(value="ProductDto")
public class ProductDto implements Serializable,GenericEntity<Long>  {

	private static final long serialVersionUID = 6830009574780972733L;
	
	/**
	 * 商品id
	 */
	@ApiModelProperty(value="商品id")
	private Long prodId;
	
	/**
	 * 默认的skuId
	 */
	@ApiModelProperty(value="默认的skuId")
	private Long skuId;
	
	/**
	 * 默认的skuType
	 */
	@ApiModelProperty(value="默认的skuType")
	private String skuType;
	
	/**
	 * 商品编号
	 */
	@ApiModelProperty(value="商品编号")
	private String modelId;
	
	/**
	 * 商品名称
	 */
	@ApiModelProperty(value="商品名称")
	private String name;
	
	/**
	 * 商品拥有者
	 */
	@ApiModelProperty(value="商品拥有者")
	private String userName;
	
	/**
	 * 商家id
	 */
	@ApiModelProperty(value="商家id")
	private Long shopId;
	
	/**
	 * 关键字
	 */
	@ApiModelProperty(value="关键字")
	private String keyWord;
	
	/**
	 * 简要描述,卖点等
	 */
	@ApiModelProperty(value="简要描述,卖点等")
	private String brief;
	
	/**
	 * 商品原价
	 */
	@ApiModelProperty(value="商品原价")
	private Double price;
	
	/**
	 * 商品现价
	 */
	@ApiModelProperty(value="商品现价")
	private Double cash;
	
	/**
	 * 是否支持分销
	 */
	@ApiModelProperty(value="是否支持分销")
	private Integer supportDist;
	
	@ApiModelProperty(value="会员直接上级分佣比例")
	private Double firstLevelRate;//会员直接上级分佣比例
	
	@ApiModelProperty(value="会员上二级分佣比例")
	private Double secondLevelRate;//会员上二级分佣比例
	
	@ApiModelProperty(value="会员上三级分佣比例")
	private Double thirdLevelRate;//会员上三级分佣比例
	
	/**
	 * 分销佣金比例
	 */
	@ApiModelProperty(value="分销佣金比例")
	private Double distCommisRate;
	
	/**
	 * 商品主图
	 */
	@ApiModelProperty(value="商品主图")
	private String pic;
	
	/**
	 * 商品状态
	 */
	@ApiModelProperty(value="商品状态")
	private Integer status;
	
	/** 开始时间 */
	@ApiModelProperty(value="开始时间")
	private Date startDate;
	
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")
	private Date endDate;
	
	/**
	 * 商品数量
	 */
	@ApiModelProperty(value="商品数量")
	private Integer stocks;
	
	@ApiModelProperty(value="库存警告")
	private Integer stocksArm;
	
	/**
	 * 商品内容
	 */
	@ApiModelProperty(value="商品内容")
	private String content;
	
	/**
	 * 手机版商品详情
	 */
	@ApiModelProperty(value="手机版商品详情")
	private String contentM;
	
	/**
	 * 商品条形码
	 */
	@ApiModelProperty(value="商品条形码")
	private String partyCode;

	/**
	 * 属性是否可以编辑
	 */
	@ApiModelProperty(value="属性是否可以编辑")
	private boolean isAttrEditable;
	
	/**
	 * 商品参数
	 */
	@ApiModelProperty(value="商品参数")
	private String parameter;
	
	/**
	 * 用户自定义参数
	 */
	@ApiModelProperty(value="用户自定义参数")
	private String userParameter;
	
//	/**
//	 * 商品一级分类
//	 */
//	private Long globalSort;
//	
//	/**
//	 * 商品二级分类
//	 */
//	private Long globalNsort;
//	
//	/**
//	 * 商品三级分类
//	 */
//	private Long globalSubSort;
	
	/**
	 *  全局商品分类
	 */
	@ApiModelProperty(value="全局商品分类")
	private Long categoryId;
	
	/**
	 * 商品品牌ID
	 */
	@ApiModelProperty(value="商品品牌ID")
	private Long brandId;
	
	/** 品牌名称 */
	@ApiModelProperty(value="品牌名称")
	private String brandName;
	
	/** 物流体积(立方米) **/
	@ApiModelProperty(value="物流体积(立方米)")
	private Double volume;

	/** 物流重量(千克) **/
	@ApiModelProperty(value="物流重量(千克)")
	private Double weight;
	
	/** 运费模板ID **/
	//protected Long transportId;
	
	/**
	 * 商品属性集合
	 */
	@ApiModelProperty(value="商品属性集合")
	private List<ProductPropertyDto> prodPropDtoList;
	
	/**
	 * 商品图片
	 */
	@ApiModelProperty(value="商品图片集合")
	private List<ProdPicDto> prodPics;



    /**
	 * 商品sku集合
	 */
	@ApiModelProperty(value="商品sku集合")
	private List<SkuDto> skuDtoList;
	
	/**
	 * 选中商品属性名
	 */
	@ApiModelProperty(value="选中商品属性名")
	private List<String> propNameList;
	
	/**
	 * 商品sku集合 的json格式字符串
	 */
	@ApiModelProperty(value="商品sku集合 的json格式字符串")
	private String skuDtoListJson = "";
	
	/** 用户自定义参数Json **/
	@ApiModelProperty(value="用户自定义参数Json")
	private String userParamJson;
	
	/**
	 * 商品评分 (平均得分)
	 */
	@ApiModelProperty(value="商品评分 (平均得分)")
	private Integer prodAvrScore;
	
	/**
	 * 属性图片map
	 */
	@ApiModelProperty(value="属性图片map")
	private Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap;
	
	
	/**
	 * 属性图片list(json格式),用于商品详情页 展示
	 */
	@ApiModelProperty(value="属性图片list(json格式),用于商品详情页 展示")
	private String propValueImgListJson;
	
	/**
	 * 评论数
	 */
	@ApiModelProperty(value="评论数")
	private Integer commCount;
	
	/**
	 * 商品图片
	 */
	@ApiModelProperty(value="商品图片")
	private List<ImgFile> imgFileList;
	
	/**
	 * 商品属性图片
	 */
	@ApiModelProperty(value="商品属性图片")
	List<ProdPropImageDto> propImageDtoList;
	
	/**
	 * 商品属性图片
	 */
	@ApiModelProperty(value="商品属性图片")
	List<PropValueImgDto> propValueImgList;
	
	/** (商家小分类) 一级分类  **/
	@ApiModelProperty(value="(商家小分类) 一级分类")
	private Long shopFirstCatId;
	
	/** (商家小分类) 二级分类  **/
	@ApiModelProperty(value="(商家小分类) 二级分类")
	private Long shopSecondCatId;
	
	/** (商家小分类) 三级分类  **/
	@ApiModelProperty(value="(商家小分类) 三级分类")
	private Long shopThirdCatId;
	
	/** 发票 **/
	@ApiModelProperty(value="发票")
	private Integer hasInvoice;
	
	/** 保修 **/
	@ApiModelProperty(value="保修")
	private Integer hasGuarantee;
	
	/** 退换货承诺 **/
	@ApiModelProperty(value="退换货承诺")
	private Integer rejectPromise;
	
	/** 服务保障 **/
	@ApiModelProperty(value="服务保障")
	private Integer serviceGuarantee;
	
	/** 售后说明 **/
	@ApiModelProperty(value="售后说明")
	private Long afterSaleId;
	
	/** 售后说明 **/
	@ApiModelProperty(value="售后说明 ")
	private String afterSaleName;
	
	/** 库存计数方式  **/
	@ApiModelProperty(value="库存计数方式")
	private Integer stockCounting;
	
	/* 运费 */
	/** 运费模板ID **/
	@ApiModelProperty(value="运费模板ID")
	protected Long transportId;

	@ApiModelProperty(value="是否承担运费[1:商家承担运费;0: 买家承担运费]")
	private Integer supportTransportFree;
	
	@ApiModelProperty(value="是否固定运费[0:使用运费模板;1:固定运费],在使用买家承担运费时有用")
	private Integer transportType;
	
	@ApiModelProperty(value="固定运费-EMS")
	private Double emsTransFee;
	
	@ApiModelProperty(value="固定运费-快递")
	private Double expressTransFee;
	
	@ApiModelProperty(value="固定运费-平邮")
	private Double mailTransFee;
	
	/** 查取 用户自定义 属性值 **/
	@ApiModelProperty(value="查取 用户自定义 属性值")
	private String valueAliasList;
	
	//设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库
	@ApiModelProperty(value="设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库")
	private Integer publishStatus;
	
	@ApiModelProperty(value="setUpTime")
	private String setUpTime;
	
	@ApiModelProperty(value="选择天数")
	private String selectDays;
	
	@ApiModelProperty(value="选择小时")
	private String selectHours;
		
	@ApiModelProperty(value="选择分钟")
	private String selectMinutes; 
	
	/** 用户自定义属性 **/
	@ApiModelProperty(value="用户属性集合")
	private String userPropList;
	
	@ApiModelProperty(value="商品SEOTitle")
	private String metaTitle;
	
	@ApiModelProperty(value="商品SEO desc")
	private String metaDesc;
	
	/** 是否关注 **/
	@ApiModelProperty(value="是否关注")
	private Boolean isExistsFavorite;
	
	@ApiModelProperty(value="是否参加团购")
	private Integer isGroup;
	
	/** 商品标签. */
	@ApiModelProperty(value="商品标签")
	private String tags;
	
	@ApiModelProperty(value="商品类型，P.普通商品，G:团购商品，S:二手商品，D:打折商品")
	private String prodType;
	
	@ApiModelProperty(value="活动Id [秒杀活动ID]")
	private Integer activeId;

    /**
     * 商品视频
     */
    @ApiModelProperty(value="商品视频")
    private String proVideoUrl;

	/** (商家小分类) 一级分类名称  **/
	@ApiModelProperty(value="(商家小分类) 一级分类名称")
	private String shopFirstCatName;

	/** (商家小分类) 二级分类名称  **/
	@ApiModelProperty(value="(商家小分类) 二级分类名称")
	private String shopSecondCatName;

	/** (商家小分类) 三级分类名称  **/
	@ApiModelProperty(value="(商家小分类) 三级分类名称")
	private String shopThirdCatName;

	@ApiModelProperty(value="运费模板名称")
	protected String transportName;

	@ApiModelProperty(value="服务说明")
	protected String serviceShow;

	public String getServiceShow() {
		return serviceShow;
	}

	public void setServiceShow(String serviceShow) {
		this.serviceShow = serviceShow;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ProductPropertyDto> getProdPropDtoList() {
		return prodPropDtoList;
	}

	public void setProdPropDtoList(List<ProductPropertyDto> prodPropDtoList) {
		this.prodPropDtoList = prodPropDtoList;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public List<ProdPicDto> getProdPics() {
		return prodPics;
	}

	public void setProdPics(List<ProdPicDto> prodPics) {
		this.prodPics = prodPics;
	}

	public List<SkuDto> getSkuDtoList() {
		return skuDtoList;
	}

	public void setSkuDtoList(List<SkuDto> skuDtoList) {
		this.skuDtoList = skuDtoList;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public Integer getProdAvrScore() {
		return prodAvrScore;
	}

	public void setProdAvrScore(Integer prodAvrScore) {
		this.prodAvrScore = prodAvrScore;
	}

	public Integer getCommCount() {
		return commCount;
	}

	public void setCommCount(Integer commCount) {
		this.commCount = commCount;
	}

	public String getSkuDtoListJson() {
		return skuDtoListJson;
	}

	public void setSkuDtoListJson(String skuDtoListJson) {
		this.skuDtoListJson = skuDtoListJson;
	}

	public String getUserParamJson() {
		return userParamJson;
	}

	public void setUserParamJson(String userParamJson) {
		this.userParamJson = userParamJson;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getUserParameter() {
		return userParameter;
	}

	public void setUserParameter(String userParameter) {
		this.userParameter = userParameter;
	}

	public Map<PropertyImageId, List<PropertyImageDto>> getPropertyImageDtoMap() {
		return propertyImageDtoMap;
	}

	public void setPropertyImageDtoMap(Map<PropertyImageId, List<PropertyImageDto>> propertyImageDtoMap) {
		this.propertyImageDtoMap = propertyImageDtoMap;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public List<ImgFile> getImgFileList() {
		return imgFileList;
	}

	public void setImgFileList(List<ImgFile> imgFileList) {
		this.imgFileList = imgFileList;
	}
	public String getPropValueImgListJson() {
		return propValueImgListJson;
	}

	public void setPropValueImgListJson(String propValueImgListJson) {
		this.propValueImgListJson = propValueImgListJson;
	}

	public List<ProdPropImageDto> getPropImageDtoList() {
		return propImageDtoList;
	}

	public void setPropImageDtoList(List<ProdPropImageDto> propImageDtoList) {
		this.propImageDtoList = propImageDtoList;
	}

	public Integer getHasInvoice() {
		return hasInvoice;
	}

	public void setHasInvoice(Integer hasInvoice) {
		this.hasInvoice = hasInvoice;
	}

	public Integer getHasGuarantee() {
		return hasGuarantee;
	}

	public void setHasGuarantee(Integer hasGuarantee) {
		this.hasGuarantee = hasGuarantee;
	}

	public Integer getRejectPromise() {
		return rejectPromise;
	}

	public void setRejectPromise(Integer rejectPromise) {
		this.rejectPromise = rejectPromise;
	}

	public Integer getServiceGuarantee() {
		return serviceGuarantee;
	}

	public void setServiceGuarantee(Integer serviceGuarantee) {
		this.serviceGuarantee = serviceGuarantee;
	}

	public Long getAfterSaleId() {
		return afterSaleId;
	}

	public void setAfterSaleId(Long afterSaleId) {
		this.afterSaleId = afterSaleId;
	}

	public Integer getStockCounting() {
		return stockCounting;
	}

	public void setStockCounting(Integer stockCounting) {
		this.stockCounting = stockCounting;
	}

	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public String getValueAliasList() {
		return valueAliasList;
	}

	public void setValueAliasList(String valueAliasList) {
		this.valueAliasList = valueAliasList;
	}

	public List<String> getPropNameList() {
		return propNameList;
	}

	public void setPropNameList(List<String> propNameList) {
		this.propNameList = propNameList;
	}

	public Integer getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	public String getSelectDays() {
		return selectDays;
	}

	public void setSelectDays(String selectDays) {
		this.selectDays = selectDays;
	}

	public String getSelectHours() {
		return selectHours;
	}

	public void setSelectHours(String selectHours) {
		this.selectHours = selectHours;
	}

	public String getSelectMinutes() {
		return selectMinutes;
	}

	public void setSelectMinutes(String selectMinutes) {
		this.selectMinutes = selectMinutes;
	}

	public String getSetUpTime() {
		return setUpTime;
	}

	public void setSetUpTime(String setUpTime) {
		this.setUpTime = setUpTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getShopFirstCatId() {
		return shopFirstCatId;
	}

	public void setShopFirstCatId(Long shopFirstCatId) {
		this.shopFirstCatId = shopFirstCatId;
	}

	public Long getShopSecondCatId() {
		return shopSecondCatId;
	}

	public void setShopSecondCatId(Long shopSecondCatId) {
		this.shopSecondCatId = shopSecondCatId;
	}

	public Long getShopThirdCatId() {
		return shopThirdCatId;
	}

	public void setShopThirdCatId(Long shopThirdCatId) {
		this.shopThirdCatId = shopThirdCatId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getUserPropList() {
		return userPropList;
	}

	public void setUserPropList(String userPropList) {
		this.userPropList = userPropList;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	public Double getEmsTransFee() {
		return emsTransFee;
	}

	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	public Double getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	public Double getMailTransFee() {
		return mailTransFee;
	}

	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	public Boolean getIsExistsFavorite() {
		return isExistsFavorite;
	}

	public void setIsExistsFavorite(Boolean isExistsFavorite) {
		this.isExistsFavorite = isExistsFavorite;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getAfterSaleName() {
		return afterSaleName;
	}

	public void setAfterSaleName(String afterSaleName) {
		this.afterSaleName = afterSaleName;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getSupportDist() {
		return supportDist;
	}

	public void setSupportDist(Integer supportDist) {
		this.supportDist = supportDist;
	}


	public Double getDistCommisRate() {
		return distCommisRate;
	}

	public void setDistCommisRate(Double distCommisRate) {
		this.distCommisRate = distCommisRate;
	}

	public Integer getStocksArm() {
		return stocksArm;
	}

	public void setStocksArm(Integer stocksArm) {
		this.stocksArm = stocksArm;
	}

	public List<PropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	public void setPropValueImgList(List<PropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public Integer getActiveId() {
		return activeId;
	}

	public void setActiveId(Integer activeId) {
		this.activeId = activeId;
	}
	
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(Integer isGroup) {
		this.isGroup = isGroup;
	}

	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	@Override
	public Long getId() {
		return prodId;
	}

	@Override
	public void setId(Long prodId) {
		this.prodId = prodId;
	}

	public Double getFirstLevelRate() {
		return firstLevelRate;
	}

	public void setFirstLevelRate(Double firstLevelRate) {
		this.firstLevelRate = firstLevelRate;
	}

	public Double getSecondLevelRate() {
		return secondLevelRate;
	}

	public void setSecondLevelRate(Double secondLevelRate) {
		this.secondLevelRate = secondLevelRate;
	}

	public Double getThirdLevelRate() {
		return thirdLevelRate;
	}

	public void setThirdLevelRate(Double thirdLevelRate) {
		this.thirdLevelRate = thirdLevelRate;
	}

	public String getSkuType() {
		return skuType;
	}

	public void setSkuType(String skuType) {
		this.skuType = skuType;
	}

    public String getProVideoUrl() {
        return proVideoUrl;
    }

    public void setProVideoUrl(String proVideoUrl) {
        this.proVideoUrl = proVideoUrl;
    }

	public boolean getIsAttrEditable() {
		return isAttrEditable;
	}

	public void setAttrEditable(boolean isAttrEditable) {
		this.isAttrEditable = isAttrEditable;
	}

	public String getShopFirstCatName() {
		return shopFirstCatName;
	}

	public void setShopFirstCatName(String shopFirstCatName) {
		this.shopFirstCatName = shopFirstCatName;
	}

	public String getShopSecondCatName() {
		return shopSecondCatName;
	}

	public void setShopSecondCatName(String shopSecondCatName) {
		this.shopSecondCatName = shopSecondCatName;
	}

	public String getShopThirdCatName() {
		return shopThirdCatName;
	}

	public void setShopThirdCatName(String shopThirdCatName) {
		this.shopThirdCatName = shopThirdCatName;
	}

	public String getTransportName() {
		return transportName;
	}

	public void setTransportName(String transportName) {
		this.transportName = transportName;
	}
}
