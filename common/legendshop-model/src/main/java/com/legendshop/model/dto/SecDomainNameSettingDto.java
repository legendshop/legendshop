/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto;

import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopDomain;

/**
 * @Description 
 * @author 关开发
 */
public class SecDomainNameSettingDto {
	private ShopDetail shopDetail;
	private ShopDomain shopDomain;
	/**
	 * @return the shopDetail
	 */
	public ShopDetail getShopDetail() {
		return shopDetail;
	}
	/**
	 * @param shopDetail the shopDetail to set
	 */
	public void setShopDetail(ShopDetail shopDetail) {
		this.shopDetail = shopDetail;
	}
	/**
	 * @return the shopDomain
	 */
	public ShopDomain getShopDomain() {
		return shopDomain;
	}
	/**
	 * @param shopDomain the shopDomain to set
	 */
	public void setShopDomain(ShopDomain shopDomain) {
		this.shopDomain = shopDomain;
	}
}
