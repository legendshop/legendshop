/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.constant;

import com.legendshop.util.constant.StringEnum;

/**
 * LegendShop 版权所有,并保留所有权利。
 * 首页楼层保存状态
 */
public enum FloorStatusEnum implements StringEnum {
	/**  
	 * 保存成功
	 **/
	SUCCESS("OK"),
	
	/**
	 * 数据不允许为空
	 */
	NO_NULLABLE("NN"),
	
	/**
	 * 权限问题，不允许编辑别人的楼层
	 */
	PERMISSION("PN")

;
	/** The value. */
	private final String value;

	/**
	 * Instantiates a new visit type enum.
	 * 
	 * @param value
	 *            the value
	 */
	private FloorStatusEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}


}
