/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model;

import java.io.Serializable;

/**
 * 站内信载体
 */
public class SiteMessageInfo implements Serializable {

	private static final long serialVersionUID = 7623308479014199483L;

	/** 发送者 */
	private final String sendName;
	
	/** 接收者 */
	private final String receiveName;
	
	/** 标题 */
	private final String title;
	
	/** 内容 */
	private final String text;
	

	public SiteMessageInfo(String sendName, String receiveName, String title, String text) {
		this.sendName = sendName;
		this.receiveName = receiveName;
		this.title = title;
		this.text = text;
	}

	public String getSendName() {
		return sendName;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}
	
	
}
