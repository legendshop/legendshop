package com.legendshop.model.constant;

import com.legendshop.util.constant.IntegerEnum;

public enum MarketActivityStatusEnum implements IntegerEnum{
	
	/*** 上线 */
	MARKETACT_STATUS_UP(1),
	/*** 下线*/
	MARKETACT_STATUS_DOWN(0)
	;
	
	private Integer status;
	
	MarketActivityStatusEnum(Integer value){
		this.status = value;
	}
	@Override
	public Integer value() {
		return status;
	}


}
