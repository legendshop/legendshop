/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.model;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 用户佣金表.
 */
@Entity
@Table(name = "ls_user_commis")
public class UserCommis implements GenericEntity<String> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7107434868485993332L;

	/** 用户id */
	private String userId; 
		
	/** 用户名 */
	private String userName; 
		
	/** 分销上级用户. */
	private String parentUserName; 
		
	/** 绑定上级时间. */
	private Date parentBindingTime; 
		
	/** 累积赚得佣金. */
	private Double totalDistCommis; 
		
	/** 直接下级会员贡献佣金. */
	private Double firstDistCommis; 
		
	/** 下两级会员贡献佣金. */
	private Double secondDistCommis; 
		
	/** 下三级会员贡献佣金. */
	private Double thirdDistCommis; 
		
	/** 已结算佣金. */
	private Double settledDistCommis; 
		
	/** 未结算佣金. */
	private Double unsettleDistCommis; 
		
	/** 推广员状态1是0否 -1拒绝. */
	private Long promoterSts; 
		
	/** 推广员审核意见. */
	private String promoterAuditComm; 
		
	/** 推广员申请时间. */
	private Date promoterApplyTime; 
		
	/** 成为推广员时间. */
	private Date promoterTime; 
		
	/** 分销上两级用户. */
	private String parentTwoUserName; 
		
	/** 分销上三级用户. */
	private String parentThreeUserName; 
	
	/** 分佣状态  0 关闭  1 正常. */
	private Integer commisSts;
	
	/** 开始申请时间 */
	private Date startApplyTime; 
	
	/** 结束申请时间 */
	private Date endApplyTime;  
	
	/** 开始成为推广员时间 */
	private Date startPromoterTime;
	
	/** 结束成为推广员时间 */
	private Date endPromoterTime;
	
	/** 绑定上级开始时间 */
	private Date startParentBindingTime;
	
	/** 绑定上级结束时间 */
	private Date endParentBindingTime;
		
	/** 直接下级数 */
	private Integer distCommisCount;
	
	/** 用户真实姓名. */
	private String realName;
	
	/** 用户手机号码姓名. */
	private String userMobile;
	
	/** 小程序码 */
	private String wxAcode;
	
	/**
	 * The Constructor.
	 */
	public UserCommis() {
    }
		
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.ASSIGNED)
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    /**
     * Gets the user name.
     *
     * @return the user name
     */
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	/**
	 * Sets the user name.
	 *
	 * @param userName the user name
	 */
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    /**
     * Gets the parent user name.
     *
     * @return the parent user name
     */
    @Column(name = "parent_user_name")
	public String  getParentUserName(){
		return parentUserName;
	} 
		
	/**
	 * Sets the parent user name.
	 *
	 * @param parentUserName the parent user name
	 */
	public void setParentUserName(String parentUserName){
			this.parentUserName = parentUserName;
		}
		
    /**
     * Gets the parent binding time.
     *
     * @return the parent binding time
     */
    @Column(name = "parent_binding_time")
	public Date  getParentBindingTime(){
		return parentBindingTime;
	} 
		
	/**
	 * Sets the parent binding time.
	 *
	 * @param parentBindingTime the parent binding time
	 */
	public void setParentBindingTime(Date parentBindingTime){
			this.parentBindingTime = parentBindingTime;
		}
		
    /**
     * Gets the total dist commis.
     *
     * @return the total dist commis
     */
    @Column(name = "total_dist_commis")
	public Double  getTotalDistCommis(){
		return totalDistCommis;
	} 
		
	/**
	 * Sets the total dist commis.
	 *
	 * @param totalDistCommis the total dist commis
	 */
	public void setTotalDistCommis(Double totalDistCommis){
		if(null == totalDistCommis){
			totalDistCommis = 0D;
		}
		this.totalDistCommis = totalDistCommis;
	}
		
    /**
     * Gets the first dist commis.
     *
     * @return the first dist commis
     */
    @Column(name = "first_dist_commis")
	public Double  getFirstDistCommis(){
		return firstDistCommis;
	} 
		
	/**
	 * Sets the first dist commis.
	 *
	 * @param firstDistCommis the first dist commis
	 */
	public void setFirstDistCommis(Double firstDistCommis){
		if(null == firstDistCommis){
			firstDistCommis = 0D;
		}
		this.firstDistCommis = firstDistCommis;
	}
		
    /**
     * Gets the second dist commis.
     *
     * @return the second dist commis
     */
    @Column(name = "second_dist_commis")
	public Double  getSecondDistCommis(){
		return secondDistCommis;
	} 
		
	/**
	 * Sets the second dist commis.
	 *
	 * @param secondDistCommis the second dist commis
	 */
	public void setSecondDistCommis(Double secondDistCommis){
		if(null == secondDistCommis){
			secondDistCommis = 0D;
		}
		this.secondDistCommis = secondDistCommis;
	}
		
    /**
     * Gets the third dist commis.
     *
     * @return the third dist commis
     */
    @Column(name = "third_dist_commis")
	public Double  getThirdDistCommis(){

		return thirdDistCommis;
	} 
		
	/**
	 * Sets the third dist commis.
	 *
	 * @param thirdDistCommis the third dist commis
	 */
	public void setThirdDistCommis(Double thirdDistCommis){
		if(null == thirdDistCommis){
			thirdDistCommis = 0D;
		}
		this.thirdDistCommis = thirdDistCommis;
	}
		
    /**
     * Gets the settled dist commis.
     *
     * @return the settled dist commis
     */
    @Column(name = "settled_dist_commis")
	public Double  getSettledDistCommis(){
		return settledDistCommis;
	} 
		
	/**
	 * Sets the settled dist commis.
	 *
	 * @param settledDistCommis the settled dist commis
	 */
	public void setSettledDistCommis(Double settledDistCommis){
		if(null == settledDistCommis){
			settledDistCommis = 0D;
		}
		this.settledDistCommis = settledDistCommis;
	}
		
    /**
     * Gets the unsettle dist commis.
     *
     * @return the unsettle dist commis
     */
    @Column(name = "unsettle_dist_commis")
	public Double  getUnsettleDistCommis(){
		return unsettleDistCommis;
	} 
		
	/**
	 * Sets the unsettle dist commis.
	 *
	 * @param unsettleDistCommis the unsettle dist commis
	 */
	public void setUnsettleDistCommis(Double unsettleDistCommis){
		if(null == unsettleDistCommis){
			unsettleDistCommis = 0D;
		}
		this.unsettleDistCommis = unsettleDistCommis;
	}
		
    /**
     * Gets the promoter sts.
     *
     * @return the promoter sts
     */
    @Column(name = "promoter_sts")
	public Long  getPromoterSts(){
		return promoterSts;
	} 
		
	/**
	 * Sets the promoter sts.
	 *
	 * @param promoterSts the promoter sts
	 */
	public void setPromoterSts(Long promoterSts){
			this.promoterSts = promoterSts;
		}
		
    /**
     * Gets the promoter audit comm.
     *
     * @return the promoter audit comm
     */
    @Column(name = "promoter_audit_comm")
	public String  getPromoterAuditComm(){
		return promoterAuditComm;
	} 
		
	/**
	 * Sets the promoter audit comm.
	 *
	 * @param promoterAuditComm the promoter audit comm
	 */
	public void setPromoterAuditComm(String promoterAuditComm){
			this.promoterAuditComm = promoterAuditComm;
		}
		
    /**
     * Gets the promoter apply time.
     *
     * @return the promoter apply time
     */
    @Column(name = "promoter_apply_time")
	public Date  getPromoterApplyTime(){
		return promoterApplyTime;
	} 
		
	/**
	 * Sets the promoter apply time.
	 *
	 * @param promoterApplyTime the promoter apply time
	 */
	public void setPromoterApplyTime(Date promoterApplyTime){
			this.promoterApplyTime = promoterApplyTime;
		}
		
    /**
     * Gets the promoter time.
     *
     * @return the promoter time
     */
    @Column(name = "promoter_time")
	public Date  getPromoterTime(){
		return promoterTime;
	} 
		
	/**
	 * Sets the promoter time.
	 *
	 * @param promoterTime the promoter time
	 */
	public void setPromoterTime(Date promoterTime){
			this.promoterTime = promoterTime;
		}
		
    /**
     * Gets the parent two user name.
     *
     * @return the parent two user name
     */
    @Column(name = "parent_two_user_name")
	public String  getParentTwoUserName(){
		return parentTwoUserName;
	} 
		
	/**
	 * Sets the parent two user name.
	 *
	 * @param parentTwoUserName the parent two user name
	 */
	public void setParentTwoUserName(String parentTwoUserName){
			this.parentTwoUserName = parentTwoUserName;
		}
		
    /**
     * Gets the parent three user name.
     *
     * @return the parent three user name
     */
    @Column(name = "parent_three_user_name")
	public String  getParentThreeUserName(){
		return parentThreeUserName;
	} 
		
	/**
	 * Sets the parent three user name.
	 *
	 * @param parentThreeUserName the parent three user name
	 */
	public void setParentThreeUserName(String parentThreeUserName){
			this.parentThreeUserName = parentThreeUserName;
		}

	/**
	 * Gets the commis sts.
	 *
	 * @return the commis sts
	 */
	@Column(name = "commis_sts")
	public Integer getCommisSts() {
		return commisSts;
	}

	/**
	 * Sets the commis sts.
	 *
	 * @param commisSts the commis sts
	 */
	public void setCommisSts(Integer commisSts) {
		this.commisSts = commisSts;
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Override
	@Transient
	public String getId() {
		return this.userId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	@Override
	public void setId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the start apply time.
	 *
	 * @return the start apply time
	 */
	@Transient
	public Date getStartApplyTime() {
		return startApplyTime;
	}

	/**
	 * Sets the start apply time.
	 *
	 * @param startApplyTime the start apply time
	 */
	public void setStartApplyTime(Date startApplyTime) {
		this.startApplyTime = startApplyTime;
	}

	/**
	 * Gets the end apply time.
	 *
	 * @return the end apply time
	 */
	@Transient
	public Date getEndApplyTime() {
		return endApplyTime;
	}

	/**
	 * Sets the end apply time.
	 *
	 * @param endApplyTime the end apply time
	 */
	public void setEndApplyTime(Date endApplyTime) {
		this.endApplyTime = endApplyTime;
	}

	/**
	 * Gets the start promoter time.
	 *
	 * @return the start promoter time
	 */
	@Transient
	public Date getStartPromoterTime() {
		return startPromoterTime;
	}

	/**
	 * Sets the start promoter time.
	 *
	 * @param startPromoterTime the start promoter time
	 */
	public void setStartPromoterTime(Date startPromoterTime) {
		this.startPromoterTime = startPromoterTime;
	}

	/**
	 * Gets the end promoter time.
	 *
	 * @return the end promoter time
	 */
	@Transient
	public Date getEndPromoterTime() {
		return endPromoterTime;
	}

	/**
	 * Sets the end promoter time.
	 *
	 * @param endPromoterTime the end promoter time
	 */
	public void setEndPromoterTime(Date endPromoterTime) {
		this.endPromoterTime = endPromoterTime;
	}

	
	/**
	 * Gets the start parent binding time.
	 *
	 * @return the start parent binding time
	 */
	@Transient
	public Date getStartParentBindingTime() {
		return startParentBindingTime;
	}

	/**
	 * Sets the start parent binding time.
	 *
	 * @param startParentBindingTime the start parent binding time
	 */
	public void setStartParentBindingTime(Date startParentBindingTime) {
		this.startParentBindingTime = startParentBindingTime;
	}
	
	/**
	 * Gets the end parent binding time.
	 *
	 * @return the end parent binding time
	 */
	@Transient
	public Date getEndParentBindingTime() {
		return endParentBindingTime;
	}

	/**
	 * Sets the end parent binding time.
	 *
	 * @param endParentBindingTime the end parent binding time
	 */
	public void setEndParentBindingTime(Date endParentBindingTime) {
		this.endParentBindingTime = endParentBindingTime;
	}

	/**
	 * Gets the dist commis count.
	 *
	 * @return the dist commis count
	 */
	@Transient
	public Integer getDistCommisCount() {
		return distCommisCount;
	}

	/**
	 * Sets the dist commis count.
	 *
	 * @param distCommisCount the dist commis count
	 */
	public void setDistCommisCount(Integer distCommisCount) {
		this.distCommisCount = distCommisCount;
	}
	
	/**
	 * Gets the real name.
	 *
	 * @return the real name
	 */
	@Transient
	public String getRealName() {
		return realName;
	}

	/**
	 * Sets the real name.
	 *
	 * @param realName the real name
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	/**
	 * Gets the user mobile.
	 *
	 * @return the user mobile
	 */
	@Transient
	public String getUserMobile() {
		return userMobile;
	}

	/**
	 * Sets the user mobile.
	 *
	 * @param userMobile the user mobile
	 */
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	@Column(name = "wx_a_code")
	public String getWxAcode() {
		return wxAcode;
	}

	public void setWxAcode(String wxAcode) {
		this.wxAcode = wxAcode;
	}

} 
