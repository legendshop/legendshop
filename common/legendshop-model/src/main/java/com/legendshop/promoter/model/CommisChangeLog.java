/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.model;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 佣金变更历史表.
 */
@Entity
@Table(name = "ls_commis_chg_log")
public class CommisChangeLog implements GenericEntity<Long> {

	private static final long serialVersionUID = -6238377945848035866L;

	/** ID. */
	private Long id; 
		
	/** 流水号. */
	private String sn; 
		
	/** user_id. */
	private String userId; 
		
	/** 会员名称. */
	private String userName; 
		
	/** 变更金额. */
	private Double amount; 
		
	/** 添加时间. */
	private Date addTime; 
	
	/** 佣金类型. */
	private String commisType;
		
	/** 下级用户. */
	private String subUserName;
	
	/** 结算时间. */
	private Date settleTime;
	
	/** 结算状态. */
	private Integer settleSts;
		
	/** 描述. */
	private String logDesc; 
	
	/**
	 * The Constructor.
	 */
	public CommisChangeLog() {
    }
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "COMMIS_CHG_LOG_SEQ")
	public Long  getId(){
		return id;
	} 
		
	/* (non-Javadoc)
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id){
			this.id = id;
		}
		
    /**
     * Gets the sn.
     *
     * @return the sn
     */
    @Column(name = "sn")
	public String  getSn(){
		return sn;
	} 
		
	/**
	 * Sets the sn.
	 *
	 * @param sn the sn
	 */
	public void setSn(String sn){
			this.sn = sn;
		}
		
    /**
     * Gets the user id.
     *
     * @return the user id
     */
    @Column(name = "user_id")
	public String  getUserId(){
		return userId;
	} 
		
	/**
	 * Sets the user id.
	 *
	 * @param userId the user id
	 */
	public void setUserId(String userId){
			this.userId = userId;
		}
		
    /**
     * Gets the user name.
     *
     * @return the user name
     */
    @Column(name = "user_name")
	public String  getUserName(){
		return userName;
	} 
		
	/**
	 * Sets the user name.
	 *
	 * @param userName the user name
	 */
	public void setUserName(String userName){
			this.userName = userName;
		}
		
    /**
     * Gets the amount.
     *
     * @return the amount
     */
    @Column(name = "amount")
	public Double  getAmount(){
		return amount;
	} 
		
	/**
	 * Sets the amount.
	 *
	 * @param amount the amount
	 */
	public void setAmount(Double amount){
			this.amount = amount;
		}
		
    /**
     * Gets the add time.
     *
     * @return the adds the time
     */
    @Column(name = "add_time")
	public Date  getAddTime(){
		return addTime;
	} 
		
	/**
	 * Sets the add time.
	 *
	 * @param addTime the adds the time
	 */
	public void setAddTime(Date addTime){
			this.addTime = addTime;
		}
		
    /**
     * Gets the log desc.
     *
     * @return the log desc
     */
    @Column(name = "log_desc")
	public String  getLogDesc(){
		return logDesc;
	} 
		
	/**
	 * Sets the log desc.
	 *
	 * @param logDesc the log desc
	 */
	public void setLogDesc(String logDesc){
			this.logDesc = logDesc;
		}

	/**
	 * Gets the commis type.
	 *
	 * @return the commis type
	 */
	@Column(name = "commis_type")
	public String getCommisType() {
		return commisType;
	}

	/**
	 * Sets the commis type.
	 *
	 * @param commisType the commis type
	 */
	public void setCommisType(String commisType) {
		this.commisType = commisType;
	}

	/**
	 * Gets the sub user name.
	 *
	 * @return the sub user name
	 */
	@Column(name = "sub_user_name")
	public String getSubUserName() {
		return subUserName;
	}

	/**
	 * Sets the sub user name.
	 *
	 * @param subUserName the sub user name
	 */
	public void setSubUserName(String subUserName) {
		this.subUserName = subUserName;
	}

	/**
	 * Gets the settle time.
	 *
	 * @return the settle time
	 */
	@Column(name = "settle_time")
	public Date getSettleTime() {
		return settleTime;
	}

	/**
	 * Sets the settle time.
	 *
	 * @param settleTime the settle time
	 */
	public void setSettleTime(Date settleTime) {
		this.settleTime = settleTime;
	}

	/**
	 * Gets the settle sts.
	 *
	 * @return the settle sts
	 */
	@Column(name = "settle_sts")
	public Integer getSettleSts() {
		return settleSts;
	}

	/**
	 * Sets the settle sts.
	 *
	 * @param settleSts the settle sts
	 */
	public void setSettleSts(Integer settleSts) {
		this.settleSts = settleSts;
	}
	/** 用户手机号. */
	private String userMobile;
	
	@Transient
	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	} 
} 
