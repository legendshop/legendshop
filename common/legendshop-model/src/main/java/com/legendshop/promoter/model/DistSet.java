/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.model;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

/**
 * 分销设置表.
 */
@Entity
@Table(name = "ls_dist_set")
public class DistSet implements GenericEntity<Long> {

	private static final long serialVersionUID = 5302353679856331892L;

	/** 分销设置id */
	private Long distSetId;

	/** 是否开启分销. */
	private Integer supportDist;

	/** 是否需要关注公众号. */
	private Integer needFollowWebchat;

	/** 商品详情是否显示佣金. */
	private Integer isShowCommis;

	/** 自己购买是否计算佣金. */
	private Integer selfBuyCommis;

	/** 会员直接上级分佣比例. */
	private Double firstLevelRate;

	/** 会员上二级分佣比例. */
	private Double secondLevelRate;

	/** 会员上三级分佣比例. */
	private Double thirdLevelRate;

	/** 注册佣金 */
	private Double regCommis;

	/** 注册即为推广员 */
	private Integer regAsPromoter;

	/** 是否需要真实姓名 */
	private Integer chkRealName;

	/** 是否需要邮箱 */
	private Integer chkMail;

	/** 是否需要手机 */
	private Integer chkMobile;

	/** 是否需要住址 */
	private Integer chkAddr;

	/**
	 * The Constructor.
	 */
	public DistSet() {
	}

	/**
	 * Gets the dist set id.
	 *
	 * @return the dist set id
	 */
	@Id
	@Column(name = "dist_set_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "DIST_SET_SEQ")
	public Long getDistSetId() {
		return distSetId;
	}

	/**
	 * Sets the dist set id.
	 *
	 * @param distSetId
	 *            the dist set id
	 */
	public void setDistSetId(Long distSetId) {
		this.distSetId = distSetId;
	}

	/**
	 * Gets the support dist.
	 *
	 * @return the support dist
	 */
	@Column(name = "support_dist")
	public Integer getSupportDist() {
		return supportDist;
	}

	/**
	 * Sets the support dist.
	 *
	 * @param supportDist
	 *            the support dist
	 */
	public void setSupportDist(Integer supportDist) {
		this.supportDist = supportDist;
	}

	/**
	 * Gets the need follow webchat.
	 *
	 * @return the need follow webchat
	 */
	@Column(name = "need_follow_webchat")
	public Integer getNeedFollowWebchat() {
		return needFollowWebchat;
	}

	/**
	 * Sets the need follow webchat.
	 *
	 * @param needFollowWebchat
	 *            the need follow webchat
	 */
	public void setNeedFollowWebchat(Integer needFollowWebchat) {
		this.needFollowWebchat = needFollowWebchat;
	}

	/**
	 * Gets the is show commis.
	 *
	 * @return the checks if is show commis
	 */
	@Column(name = "is_show_commis")
	public Integer getIsShowCommis() {
		return isShowCommis;
	}

	/**
	 * Sets the is show commis.
	 *
	 * @param isShowCommis
	 *            the checks if is show commis
	 */
	public void setIsShowCommis(Integer isShowCommis) {
		this.isShowCommis = isShowCommis;
	}

	/**
	 * Gets the self buy commis.
	 *
	 * @return the self buy commis
	 */
	@Column(name = "self_buy_commis")
	public Integer getSelfBuyCommis() {
		return selfBuyCommis;
	}

	/**
	 * Sets the self buy commis.
	 *
	 * @param selfBuyCommis
	 *            the self buy commis
	 */
	public void setSelfBuyCommis(Integer selfBuyCommis) {
		this.selfBuyCommis = selfBuyCommis;
	}

	/**
	 * Gets the first level rate.
	 *
	 * @return the first level rate
	 */
	@Column(name = "first_level_rate")
	public Double getFirstLevelRate() {
		return firstLevelRate;
	}

	/**
	 * Sets the first level rate.
	 *
	 * @param firstLevelRate
	 *            the first level rate
	 */
	public void setFirstLevelRate(Double firstLevelRate) {
		this.firstLevelRate = firstLevelRate;
	}

	/**
	 * Gets the second level rate.
	 *
	 * @return the second level rate
	 */
	@Column(name = "second_level_rate")
	public Double getSecondLevelRate() {
		return secondLevelRate;
	}

	/**
	 * Sets the second level rate.
	 *
	 * @param secondLevelRate
	 *            the second level rate
	 */
	public void setSecondLevelRate(Double secondLevelRate) {
		this.secondLevelRate = secondLevelRate;
	}

	/**
	 * Gets the third level rate.
	 *
	 * @return the third level rate
	 */
	@Column(name = "third_level_rate")
	public Double getThirdLevelRate() {
		return thirdLevelRate;
	}

	/**
	 * Sets the third level rate.
	 *
	 * @param thirdLevelRate
	 *            the third level rate
	 */
	public void setThirdLevelRate(Double thirdLevelRate) {
		this.thirdLevelRate = thirdLevelRate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.dao.support.GenericEntity#getId()
	 */
	@Transient
	public Long getId() {
		return distSetId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.dao.support.GenericEntity#setId(java.io.Serializable)
	 */
	public void setId(Long id) {
		distSetId = id;
	}

	/**
	 * Gets the reg commis.
	 *
	 * @return the reg commis
	 */
	@Column(name = "reg_commis")
	public Double getRegCommis() {
		return regCommis;
	}

	/**
	 * Sets the reg commis.
	 *
	 * @param regCommis
	 *            the reg commis
	 */
	public void setRegCommis(Double regCommis) {
		this.regCommis = regCommis;
	}

	/**
	 * Gets the reg as promoter.
	 *
	 * @return the reg as promoter
	 */
	@Column(name = "reg_as_promoter")
	public Integer getRegAsPromoter() {
		return regAsPromoter;
	}

	/**
	 * Sets the reg as promoter.
	 *
	 * @param regAsPromoter
	 *            the reg as promoter
	 */
	public void setRegAsPromoter(Integer regAsPromoter) {
		this.regAsPromoter = regAsPromoter;
	}

	/**
	 * Gets the chk real name.
	 *
	 * @return the chk real name
	 */
	@Column(name = "chk_real_name")
	public Integer getChkRealName() {
		return chkRealName;
	}

	/**
	 * Sets the chk real name.
	 *
	 * @param chkRealName
	 *            the chk real name
	 */
	public void setChkRealName(Integer chkRealName) {
		this.chkRealName = chkRealName;
	}

	/**
	 * Gets the chk mail.
	 *
	 * @return the chk mail
	 */
	@Column(name = "chk_mail")
	public Integer getChkMail() {
		return chkMail;
	}

	/**
	 * Sets the chk mail.
	 *
	 * @param chkMail
	 *            the chk mail
	 */
	public void setChkMail(Integer chkMail) {
		this.chkMail = chkMail;
	}

	/**
	 * Gets the chk mobile.
	 *
	 * @return the chk mobile
	 */
	@Column(name = "chk_mobile")
	public Integer getChkMobile() {
		return chkMobile;
	}

	/**
	 * Sets the chk mobile.
	 *
	 * @param chkMobile
	 *            the chk mobile
	 */
	public void setChkMobile(Integer chkMobile) {
		this.chkMobile = chkMobile;
	}

	/**
	 * Gets the chk addr.
	 *
	 * @return the chk addr
	 */
	@Column(name = "chk_addr")
	public Integer getChkAddr() {
		return chkAddr;
	}

	/**
	 * Sets the chk addr.
	 *
	 * @param chkAddr
	 *            the chk addr
	 */
	public void setChkAddr(Integer chkAddr) {
		this.chkAddr = chkAddr;
	}

}
