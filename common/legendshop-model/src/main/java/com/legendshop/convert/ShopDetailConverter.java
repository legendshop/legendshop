package com.legendshop.convert;


import com.legendshop.model.dto.shop.OpenShopInfoDTO;
import com.legendshop.model.entity.ShopDetail;
import org.mapstruct.Mapper;

/**
 * 商家信息转换
 *
 */
@Mapper(componentModel="spring")
public interface ShopDetailConverter {


	/**
	 *  shopDetail to OpenShopInfoDTO
	 */
	OpenShopInfoDTO contervtToOpenShopInfoDTO(ShopDetail shopDetail);
}
