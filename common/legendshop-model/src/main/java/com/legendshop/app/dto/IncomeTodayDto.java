package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
@ApiModel(value="IncomeTodayDto今日收入") 
public class IncomeTodayDto implements Serializable{

	private static final long serialVersionUID = 6585017639945626124L;

	@ApiModelProperty(value="总数")
	private Double total;
	
	@ApiModelProperty(value="用户名")
	private String userName;
	
	@ApiModelProperty(value="日期")
	private Date subDate;

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

}
