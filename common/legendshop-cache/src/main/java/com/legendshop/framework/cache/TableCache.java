/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.cache;

import java.util.Map;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
public interface TableCache {

	/**
	 * Gets the code table.
	 * 
	 * @param beanName
	 *            the bean name
	 * @return the code table
	 */
	public abstract Map<String, String> getCodeTable(String beanName);

	/**
	 * Inits the code tables cache.
	 */
	public abstract void initCodeTablesCache();

}