/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.listener;

import com.legendshop.framework.cache.caffeine.CacheMessage;

/**
 * 更新系统参数缓存.
 */
public interface UpdateSystemParameterCache {
	
	/**
	 * 接收到消息通知本地缓存更新
	 * @param cacheMessage
	 */
	public void onMessage(CacheMessage cacheMessage);

}
