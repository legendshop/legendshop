/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.caffeine;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.legendshop.framework.cache.AbstractLegendCacheManager;
import com.legendshop.framework.cache.LegendCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * 二级缓存管理器.
 */
public class RedisCaffeineCacheManager extends AbstractLegendCacheManager implements LegendCacheManager {

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(RedisCaffeineCacheManager.class);

	/** 缓存名称. */
	private ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<String, Cache>();

	/** 缓存配置. */
	private CacheRedisCaffeineProperties cacheRedisCaffeineProperties;

	/** redis客户端. */
	private RedisTemplate<Object, Object> redisTemplate;

	/** 是否动态建立缓存. */
	private boolean dynamic = true;

	/** 缓存名字. */
	private Set<String> cacheNames;

	/** 每个cacheName的过期时间，单位秒，优先级比defaultExpiration高. */
	private Map<String, Long> expires;

	/**
	 * 构造函数.
	 *
	 * @param cacheRedisCaffeineProperties
	 *            the cache redis caffeine properties
	 * @param redisTemplate
	 *            the redis template
	 */
	public RedisCaffeineCacheManager(CacheRedisCaffeineProperties cacheRedisCaffeineProperties, RedisTemplate<Object, Object> redisTemplate) {
		super();
		this.cacheRedisCaffeineProperties = cacheRedisCaffeineProperties;
		this.redisTemplate = redisTemplate;
		this.dynamic = cacheRedisCaffeineProperties.isDynamic();
		this.cacheNames = cacheRedisCaffeineProperties.getExpires().keySet();
		this.expires = cacheRedisCaffeineProperties.getExpires();
		
		if(cacheNames != null){
			for (String name : cacheNames) {
				RedisCaffeineCache	cache = new RedisCaffeineCache(name, redisTemplate, caffeineCache(name), cacheRedisCaffeineProperties);
				cacheMap.putIfAbsent(name, cache);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.cache.support.AbstractCacheManager#getCache(java.lang
	 * .String)
	 */
	@Override
	public Cache getCache(String name) {
		Cache cache = cacheMap.get(name);
		if (cache != null) {
			return cache;
		}
		if (!dynamic && !cacheNames.contains(name)) {
			return cache;
		}

		cache = new RedisCaffeineCache(name, redisTemplate, caffeineCache(name), cacheRedisCaffeineProperties);
		Cache oldCache = cacheMap.putIfAbsent(name, cache);
		logger.debug("create cache instance, the cache name is : {}", name);
		return oldCache == null ? cache : oldCache;
	}

	/**
	 * Caffeine cache.
	 *
	 * @return the cache< object, object>
	 */
	public com.github.benmanes.caffeine.cache.Cache<Object, Object> caffeineCache(String name) {
		Caffeine<Object, Object> cacheBuilder = Caffeine.newBuilder();
		Long expireTime = expires.get(name);// 单位是秒
		
		if(expireTime != null){ //计算一级缓存的生命周期TTL
			expireTime = new Double(expireTime * cacheRedisCaffeineProperties.getCaffeine().getTimeScale()).longValue();
			if(expireTime <=0){
				expireTime = 1l; //一旦设置了就一定要有有效期
			}
		}

		Long exipireAfterWrite = expireTime == null ? cacheRedisCaffeineProperties.getCaffeine().getExpireAfterWrite() : expireTime;
		
		if(exipireAfterWrite > 0){
			cacheBuilder.expireAfterWrite(exipireAfterWrite, TimeUnit.SECONDS);
		}
		
//		Long exipireAfterAccess = expireTime == null ? cacheRedisCaffeineProperties.getCaffeine().getExpireAfterAccess() : expireTime * 1000;
//		cacheBuilder.expireAfterAccess(exipireAfterAccess,TimeUnit.MILLISECONDS);		
		
//		Long refreshAfterWrite = expireTime == null ? cacheRedisCaffeineProperties.getCaffeine().getRefreshAfterWrite() : expireTime * 1000;
//		cacheBuilder.refreshAfterWrite(refreshAfterWrite, TimeUnit.MILLISECONDS);

		if (cacheRedisCaffeineProperties.getCaffeine().getInitialCapacity() > 0) {
			cacheBuilder.initialCapacity(cacheRedisCaffeineProperties.getCaffeine().getInitialCapacity());
		}
		if (cacheRedisCaffeineProperties.getCaffeine().getMaximumSize() > 0) {
			cacheBuilder.maximumSize(cacheRedisCaffeineProperties.getCaffeine().getMaximumSize());
		}
		return cacheBuilder.build();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.cache.support.AbstractCacheManager#getCacheNames()
	 */
	@Override
	public Collection<String> getCacheNames() {
		return this.cacheNames;
	}

	/**
	 * Clear local.
	 *
	 * @param cacheName
	 *            the cache name
	 * @param key
	 *            the key
	 */
	public void clearLocal(String cacheName, Object key) {
		Cache cache = cacheMap.get(cacheName);
		if (cache == null) {
			return;
		}

		RedisCaffeineCache redisCaffeineCache = (RedisCaffeineCache) cache;
		redisCaffeineCache.clearLocal(key);
	}

	/**
	 * 父类的Cache不再使用
	 */
	@Override
	protected Collection<? extends Cache> loadCaches() {
		return new ArrayList<Cache>();
	}
}