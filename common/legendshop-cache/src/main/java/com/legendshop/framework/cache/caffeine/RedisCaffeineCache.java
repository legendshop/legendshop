/*
 *
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 *
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.legendshop.framework.cache.CacheKeyValueEntity;
import com.legendshop.framework.cache.EntityCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.cache.support.NullValue;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.lang.Nullable;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 二级缓存实现，包括Redis + Caffeine
 * 1. 优先从一级缓存中读取，如果一级缓存读取不到，则从二级缓存读取，如果二级缓存也没有则从数据库中读取，再填充一级和二级缓存.
 * 2. 一级和二级缓存可以设置是否开启， 如果二级缓存不开启的情况下，一级缓存在集群模式下会无法更新其他节点，因此在集群环境下必须要开启二级缓存。
 * 3. 由于一级缓存是JVM内部缓存，受限于内存大小，所以一级缓存的生命周期和最大数量应该少于二级缓存，具体值可以根据内存设置来调整。
 * 4. 通过缓存前缀来设置不同子系统的缓存分区，不至于key冲撞
 */
public class RedisCaffeineCache extends AbstractValueAdaptingCache implements EntityCache {

    /**
     * The logger.
     */
    private final Logger logger = LoggerFactory.getLogger(RedisCaffeineCache.class);

    /**
     * 缓存的名字，在applicationContext-caffeine-cache.xml中定义。每个名字代表一个缓存区.
     */
    private String name;

    /**
     * Redis调用模板.
     */
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 一级缓存实现，采用caffeine实现.
     */
    private Cache<Object, Object> caffeineCache;

    /**
     * 缓存一二级设置
     **/
    private CacheRedisCaffeineProperties cacheRedisCaffeineProperties;

    /**
     * 默认超时时间，如果不在XML中定义超时时间，则采用该值.
     */
    private long defaultExpiration = 300;

    /**
     * 构造函数.
     *
     * @param allowNullValues 缓存是否允许空值
     */
    protected RedisCaffeineCache(boolean allowNullValues) {
        super(allowNullValues);
    }

    /**
     * 构造函数.
     *
     * @param name                         缓存的名字，在applicationContext-caffeine-cache.xml中定义。每个名字代表一个缓存区.
     * @param redisTemplate                Redis调用模板
     * @param caffeineCache                一级缓存实现，采用caffeine实现
     * @param cacheRedisCaffeineProperties 一二级缓存设置
     */
    public RedisCaffeineCache(String name, RedisTemplate<Object, Object> redisTemplate, Cache<Object, Object> caffeineCache,
                              CacheRedisCaffeineProperties cacheRedisCaffeineProperties) {
        super(cacheRedisCaffeineProperties.isCacheNullValues());
        this.name = name;
        this.redisTemplate = redisTemplate;
        this.caffeineCache = caffeineCache;
        this.cacheRedisCaffeineProperties = cacheRedisCaffeineProperties;
    }

    /**
     * 缓存名字
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * 获取本地缓存
     */
    @Override
    public Object getNativeCache() {
        return this;
    }

    /**
     * 获取缓存值
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        Object value = lookup(key);
        if (value != null) {
            return (T) value;
        }

        ReentrantLock lock = new ReentrantLock();
        try {
            lock.lock();
            value = lookup(key);
            if (value != null) {
                return (T) value;
            }
            value = valueLoader.call();
            Object storeValue = toStoreValue(valueLoader.call());
            put(key, storeValue);
            return (T) value;
        } catch (Exception e) {
            try {
                Class<?> c = Class.forName("org.springframework.cache.Cache$ValueRetrievalException");
                Constructor<?> constructor = c.getConstructor(Object.class, Callable.class, Throwable.class);
                RuntimeException exception = (RuntimeException) constructor.newInstance(key, valueLoader, e.getCause());
                throw exception;
            } catch (Exception e1) {
                throw new IllegalStateException(e1);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * 加入缓存中， 优先加入二级缓存，然后加入一级缓存
     */
    @Override
    public void put(Object key, Object value) {
        if (!super.isAllowNullValues() && value == null) {
            this.evict(key);
            return;
        }

        if (cacheRedisCaffeineProperties.getRedis().isCachable()) {//如果支持二级缓存
            long expire = getExpire();
            if (expire > 0) {
                redisTemplate.opsForValue().set(getKey(key), toStoreValue(value), expire, TimeUnit.SECONDS);
            } else {
                redisTemplate.opsForValue().set(getKey(key), toStoreValue(value));
            }
        }

        if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
            //push(new CacheMessage(this.name, key)); TODO 通知其他节点也会更新缓存， 因为其他节点之前会删除了缓存，再次读的时候会从二级缓存中重新获取， 无需同时更新全部备份节点. 去掉观察是否有影响到缓存有脏读问题.
            caffeineCache.put(key, toStoreValue(value)); //加入一级缓存
        }

    }

    /**
     * 如果目标对象不在缓存中， 则加入二级缓存，然后加入一级缓存。
     */
    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        Object cacheKey = getKey(key);
        Object prevValue = null;
        // 考虑使用分布式锁，或者将redis的setIfAbsent改为原子性操作
        synchronized (key) {
            prevValue = redisTemplate.opsForValue().get(cacheKey);
            if (prevValue == null) {
                if (cacheRedisCaffeineProperties.getRedis().isCachable()) {//如果支持二级缓存
                    long expire = getExpire();
                    //如果二级缓存取不到则填充二级缓存
                    if (expire > 0) {
                        redisTemplate.opsForValue().set(getKey(key), toStoreValue(value), expire, TimeUnit.SECONDS);
                    } else {
                        redisTemplate.opsForValue().set(getKey(key), toStoreValue(value));
                    }

                }
                if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
                    // push(new CacheMessage(this.name, key)); TODO 去掉观察是否有影响到缓存有脏读问题
                    caffeineCache.put(key, toStoreValue(value));
                }

            }
        }
        return toValueWrapper(prevValue);
    }

    /**
     * 清理缓存， 先清理二级缓存，再清理一级缓存,在CacheEvict的属性allEntries=false的情况下调用该方法。
     */
    @Override
    public void evict(Object key) {

        if (cacheRedisCaffeineProperties.getRedis().isCachable()) {//如果支持二级缓存
            // 先清除redis中缓存数据，然后清除caffeine中的缓存，避免短时间内如果先清除caffeine缓存后其他请求会再从redis里加载到caffeine中
            redisTemplate.delete(getKey(key)); //1. 删除redis的数据
        }

        if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
            push(new CacheMessage(this.name, key)); //通知其他节点的本地缓存更新

            caffeineCache.invalidate(key); //3. 本地删除缓存数据，防止出现脏数据的问题
        }
    }

    /**
     * 清理缓存区间的所有数据， 先清理二级缓存，再清理一级缓存,在CacheEvict的属性allEntries=true的情况下调用该方法。
     */
    @Override
    public void clear() {
        if (cacheRedisCaffeineProperties.getRedis().isCachable()) {//如果支持二级缓存
            // 先清除redis中缓存数据，然后清除caffeine中的缓存，避免短时间内如果先清除caffeine缓存后其他请求会再从redis里加载到caffeine中
            Set<Object> keys = redisTemplate.keys(getKey("*"));//1. 找到这个cache下所有的key，然后逐个删除缓存，注意：这种情况性能比较差，不合适大量的数据删除的应用场景
            for (Object key : keys) {
                redisTemplate.delete(key);
            }
        }

        if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
            push(new CacheMessage(this.name, null));//2. 通知其他节点更新本地缓存
            caffeineCache.invalidateAll(); //3. 本地删除缓存数据，防止出现脏数据的问题
        }

    }

    /**
     * 从内存中返回对象，没有封装对象,优先读取一级缓存，一级缓存找不到则读取二级缓存，再填充一级缓存并返回。
     * 因为二级缓存的生命周期比一级缓存要长，所有有可能发生二级缓存存在一级缓存不存在的现象。
     */
    @Override
    protected Object lookup(Object key) {
        Object value = null;
        if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
            value = caffeineCache.getIfPresent(key);
            if (value != null) {
                logger.debug("get cache from caffeine.  name is {}, the key is {}", name, key);
                return value;
            }
        }

        if (cacheRedisCaffeineProperties.getRedis().isCachable()) {//如果支持二级缓存
            Object cacheKey = getKey(key);
            value = redisTemplate.opsForValue().get(cacheKey);

            if (value != null && cacheRedisCaffeineProperties.getCaffeine().isCachable()) {
                logger.debug("get cache from redis and put in caffeine. name is {}, the key is {}", name, cacheKey);
                caffeineCache.put(key, toStoreValue(value));
            }
        }

        return value;
    }

    /**
     * Gets the key.
     *
     * @param key the key
     * @return the key
     */
    private Object getKey(Object key) {
        String cachePrefix = cacheRedisCaffeineProperties.getCachePrefix();
        String redisKey = cachePrefix.concat(":").concat(this.name).concat(":").concat(key.toString());
        return redisKey;
    }

    /**
     * 基类中有bug，因此需要在子类重构该方法
     */
    @Nullable
    protected Object fromStoreValue(Object storeValue) {
        if (cacheRedisCaffeineProperties.isCacheNullValues() && storeValue instanceof NullValue) {
            return null;
        }
        return storeValue;
    }

    /**
     * 每一个缓存的超时时间设定
     *
     * @return the expire
     */
    private long getExpire() {
        long expire = defaultExpiration;
        Long cacheNameExpire = cacheRedisCaffeineProperties.getExpires().get(this.name);
        return cacheNameExpire == null ? expire : cacheNameExpire.longValue();
    }

    /**
     * 缓存变更时通知其他节点清理本地缓存.
     *
     * @param message the message
     */
    private void push(CacheMessage message) {
        redisTemplate.convertAndSend(cacheRedisCaffeineProperties.getRedis().getTopic(), message);
    }

    /**
     * 清理本地缓存.
     *
     * @param key 缓存名字
     */
    public void clearLocal(Object key) {
        logger.debug("clear local cache, the key is : {}", key);
        if (cacheRedisCaffeineProperties.getCaffeine().isCachable()) {//如果支持一级缓存
            if (key == null) {
                caffeineCache.invalidateAll();//没有指定key的话就是清除该缓存区所有的缓存
            } else {
                caffeineCache.invalidate(key);
            }

        }
    }

    public Collection<CacheKeyValueEntity> getEntity(String cacheName) {
        Map map = caffeineCache.asMap();
        List<CacheKeyValueEntity> result = new ArrayList<CacheKeyValueEntity>();
        if (map.size() > 0) {
            for (Object key : map.keySet()) {
                CacheKeyValueEntity entity = new CacheKeyValueEntity();
                entity.setCacheName(cacheName);
                entity.setKey(key.toString());
                Object obj = caffeineCache.getIfPresent(key);
                if (obj != null) {
                    entity.setValue(obj.toString());
                }

                result.add(entity);
            }
        }
        return result;
    }


}