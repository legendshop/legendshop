/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import de.javakaffee.kryoserializers.*;
import de.javakaffee.kryoserializers.cglib.CGLibProxySerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.lang.reflect.InvocationHandler;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;

/**
 * Kryo序列化
 */
public class KryoSerializer implements RedisSerializer<Object> {

	//线程上下文
	private final ThreadLocal<KryoHolder> kryoThreadLocal = new ThreadLocal<KryoHolder>() {
		@Override
		protected KryoHolder initialValue() {
			return new KryoHolder(new Kryo());
		}
	};

	/**
	 * 序列化
	 */
	@Override
	public byte[] serialize(Object obj) throws SerializationException {
		try {
			if(obj == null){
				return new byte[0];
			}
			KryoHolder kryoHolder = kryoThreadLocal.get();
			kryoHolder.output.clear();  //clear Output    -->每次调用的时候  重置
			kryoHolder.kryo.writeClassAndObject(kryoHolder.output, obj);
			return kryoHolder.output.toBytes();// 无法避免拷贝  ~~~
		} finally {
			obj = null;
		}
	}

	/**
	 * 反序列化
	 */
	public Object deserialize(byte[] bytes) throws SerializationException {
		try {
			if(bytes == null || bytes.length <= 0){
				return null;
			}
			KryoHolder kryoHolder = kryoThreadLocal.get();
			kryoHolder.input.setBuffer(bytes, 0, bytes.length);//call it ,and then use input object  ,discard any array
			return kryoHolder.kryo.readClassAndObject(kryoHolder.input);
		} finally {
			bytes = null;       //  for gc
		}
	}

	/**
	 * Kyro封装类，专门优化了Kryo的参数
	 */
	private class KryoHolder {
		private Kryo kryo;
		static final int BUFFER_SIZE = 1024;
		private Output output = new Output(BUFFER_SIZE, -1);     //reuse
		private Input input = new Input();

		KryoHolder(Kryo kryo) {
			this.kryo = kryo;
			this.kryo.setReferences(false);

			//   register
			this.kryo.register(Arrays.asList("").getClass(), new ArraysAsListSerializer());
			this.kryo.register(Collections.EMPTY_LIST.getClass(), new CollectionsEmptyListSerializer());
			this.kryo.register(Collections.EMPTY_MAP.getClass(), new CollectionsEmptyMapSerializer());
			this.kryo.register(Collections.EMPTY_SET.getClass(), new CollectionsEmptySetSerializer());
			this.kryo.register(Collections.singletonList("").getClass(), new CollectionsSingletonListSerializer());
			this.kryo.register(Collections.singleton("").getClass(), new CollectionsSingletonSetSerializer());
			this.kryo.register(Collections.singletonMap("", "").getClass(), new CollectionsSingletonMapSerializer());
			this.kryo.register(GregorianCalendar.class, new GregorianCalendarSerializer());
			this.kryo.register(InvocationHandler.class, new JdkProxySerializer());

			// register CGLibProxySerializer, works in combination with the appropriate action in handleUnregisteredClass (see below)
			this.kryo.register(CGLibProxySerializer.CGLibProxyMarker.class, new CGLibProxySerializer());
			UnmodifiableCollectionsSerializer.registerSerializers(this.kryo);
			SynchronizedCollectionsSerializer.registerSerializers(this.kryo);

		}

	}

}
