/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.cache;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.dao.support.PageSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;

import java.util.Collection;

/**
 * 缓存抽象层.
 */
public abstract class AbstractLegendCache implements EntityCache {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AbstractLegendCache.class);

	/** 查询缓存后缀. 对象缓存后面加上List就表示是列表缓存 */
	protected String SUFFIX = "List";
	
	/** 在List对象中不再支持列表缓存,针对部分返回很多对象的情况,不再适宜记录关联关系 **/
	protected String NoQuery = "NQ_";

	/** The cache manager. */
	protected LegendCacheManager cacheManager;

	/**
	 * 删除缓存， 针对对象缓存，当删除对象缓存时，同时也把相关的列表缓存删除.
	 * 
	 * @param entityId
	 *            ，注意不是fullKey，而是entityId 删除相关的列表缓存.
	 */
	protected void evictObject(String entityId) {
		// 1. For Entity 对象缓存
		if (cacheManager.isSupportQueryCache() && !this.getName().endsWith(SUFFIX) && !this.getName().equals(cacheManager.getRelCacheName())) {
			// clean entity
			AbstractLegendCache relCache = (AbstractLegendCache) cacheManager.getCache(cacheManager.getRelCacheName());
			if (relCache != null) {
				/*
				 * 缓存的Key是customerId + entity cache name +key
				 */
				String relCacheKey = generateRelCacheKey(relCache, this.getName(), entityId);
				SimpleValueWrapper valueWrapper = (SimpleValueWrapper) relCache.getRelCache(relCacheKey);
				if (valueWrapper != null) {
					IdListRel idListRel = (IdListRel) valueWrapper.get();
					if (idListRel != null && idListRel.getRelObject() != null) {
						for (CacheNameAndItemWrapper relationShop : idListRel.getRelObject()) {
							AbstractLegendCache relCache2 = (AbstractLegendCache) cacheManager.getCache(relationShop.getCacheName());
							// 删除缓存记录里对应的记录
							Long result = relCache2.evictRelCache(relationShop.getKey());
							if (log.isDebugEnabled()) {
								log.debug("Evict RelationShop Cache Name: {} by Key: {}, result {}", relationShop.getCacheName(), relationShop.getKey(), result);
							}
						}
					}
					// 删除相关缓存本身
					relCache.evictRelCache(relCacheKey);
				}
			}
		} else if (cacheManager.isRemoveAllEntries()) {
			Cache listCache = cacheManager.getCache(this.getName() + SUFFIX);
			if (listCache != null) {
				listCache.clear();
			}
		}
	}

	/**
	 * 如果是List类型的保存相关的缓存
	 * 
	 * @param fullKey
	 *            当前对象或列表缓存的名字,只有列表缓存有效 缓存key格式为customerId +cacheName + key
	 * @param value
	 *            the value
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void putObject(Object fullKey, Object value) {
		// For List, 如果是列表缓存，则保存以ID为主键的关系
		if (cacheManager.isSupportQueryCache() && this.getName().endsWith(SUFFIX) && !this.getName().equals(cacheManager.getRelCacheName()) && value != null) {
			Collection<GenericEntity> coll = null;
			if (Collection.class.isAssignableFrom(value.getClass())) {
				coll = (Collection<GenericEntity>) value;
			} else if (PageSupport.class.isAssignableFrom(value.getClass())) {
				coll = (Collection<GenericEntity>) ((PageSupport) value).getResultList();
			}
			if (coll != null) {
				// 循环开始
				for (GenericEntity entity : coll) {
					AbstractLegendCache relCache = (AbstractLegendCache) cacheManager.getCache(cacheManager.getRelCacheName());
					// 每个列表缓存都会带上List作为后缀，需要去掉List来拼装缓存的名字
					String relCacheKey = generateRelCacheKey(relCache, this.getName().substring(0, this.getName().length() - 4), entity.getId().toString());
					SimpleValueWrapper valueWrapper = (SimpleValueWrapper) relCache.getRelCache(relCacheKey);
					IdListRel idListRel = null;
					if (valueWrapper != null) {
						idListRel = (IdListRel) valueWrapper.get();
					} else {
						idListRel = new IdListRel(entity.getId());
					}

					if (idListRel.addRelObject(this.getName(), fullKey)) {
						relCache.putRelCache(relCacheKey, idListRel);
					}
				}
			}
		}
	}

	/**
	 * 保存相关缓存.
	 * 
	 * @param relCacheKey
	 *            the rel cache key
	 * @return the string
	 */
	public abstract Long putRelCache(Object key, Object value);
	
	
	/**
	 * 获取相关缓存,Key不用重新计算
	 * @param key
	 * @return
	 */
	public abstract ValueWrapper getRelCache(Object key);

	/**
	 * 清除相关缓存.
	 * 
	 * @param relCacheKey
	 *            the rel cache key
	 * @return the string
	 */
	public abstract Long evictRelCache(Object key);

	/**
	 * Generate rel cache key.
	 * 
	 * @param relCacheKey
	 *            the rel cache key
	 * @return the string
	 */
	public abstract String generateRelCacheKey(AbstractLegendCache relCache, String cacheName, String relCacheKey);

	/**
	 * Sets the cache manager.
	 * 
	 * @param cacheManager
	 *            the new cache manager
	 */
	public void setCacheManager(LegendCacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

}
