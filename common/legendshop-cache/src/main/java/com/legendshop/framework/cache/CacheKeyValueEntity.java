/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.cache;

import java.io.Serializable;

/**
 * Key Value Mapping.
 */
public class CacheKeyValueEntity implements Serializable, Cloneable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5568358970483740841L;
	
	/** 缓存名字. */
	private String cacheName;

	/** The key. */
	private String key;

	/** The value. */
	private String value = "";

	/**
	 * Instantiates a new key value entity.
	 */
	public CacheKeyValueEntity() {

	}

	/**
	 * Instantiates a new key value entity.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public CacheKeyValueEntity(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public CacheKeyValueEntity(Integer key, String value) {
		this.key = String.valueOf(key);
		this.value = value;
	}

	/**
	 * Gets the key.
	 * 
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 * 
	 * @param key
	 *            the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int result = 17;
		result = 31 * result + cacheName.hashCode();
		result = 31 * result + key.hashCode();
		result = 31 * result + value.hashCode();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof CacheKeyValueEntity) {
			CacheKeyValueEntity entity = (CacheKeyValueEntity) obj;
			boolean result = theSame(entity.getCacheName(), cacheName);
			if(result){
				result = theSame(entity.getKey(), key);
			}
			
			if(result){
				result = theSame(entity.getValue(), value);
			}
			return result;
		}
		return false;
	}
	
	private boolean theSame(Object origin, Object obj){
		if(origin == null && obj == null){
			return true;
		}
		if(origin != null && origin.equals(obj)){
			return true;
		}
		return false;
	}

	public CacheKeyValueEntity clone(){
		CacheKeyValueEntity entity = new CacheKeyValueEntity();
		entity.setKey(this.getKey());
		entity.setValue(this.getValue());
		
		return entity;
	}

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
}
