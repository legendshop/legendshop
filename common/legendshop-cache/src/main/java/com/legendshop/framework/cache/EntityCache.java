/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.cache;

import org.springframework.cache.Cache;

import java.util.Collection;

/**
 * 获取entity对象的缓存
 */
public interface EntityCache extends Cache {
	
	/**
	 * 根据缓存的名称获取缓存值
	 * @param cacheName 缓存名称
	 * @return
	 */
	public Collection<CacheKeyValueEntity> getEntity(String cacheName);

}
