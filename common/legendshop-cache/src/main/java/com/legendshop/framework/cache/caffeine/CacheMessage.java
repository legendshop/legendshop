package com.legendshop.framework.cache.caffeine;

import java.io.Serializable;

/**
 * 更新消息体
 */
public class CacheMessage implements Serializable {

	private static final long serialVersionUID = 5987219310442078193L;

	private String cacheName;
	private Object key;

	/**
	 * 必须要有空的构造函数，用于序列化
	 */
	public CacheMessage(){
		
	}
	
	public CacheMessage(String cacheName, Object key) {
		super();
		this.cacheName = cacheName;
		this.key = key;
	}

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}

	public Object getKey() {
		return key;
	}

	public void setKey(Object key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return "CacheMessage [cacheName=" + cacheName + ", key=" + key + "]";
	}
	

}