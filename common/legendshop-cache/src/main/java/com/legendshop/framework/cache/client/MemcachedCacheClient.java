package com.legendshop.framework.cache.client;

/**
 * memcached客户端
 *
 */
public interface MemcachedCacheClient {
	
	public Object get(String key);
	
	public boolean add(String key,int expiredDuration, Object obj);
	
	public boolean set(String key, int expiredDuration, Object obj);
	
	public Object delete(String key);
	
	public boolean flush();
	
}
