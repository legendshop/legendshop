/*
 * 
 * LegendShop 多用户商城系统
 * 
 *    版权所有,并保留所有权利。
 *  Redis封装类
 */
package com.legendshop.framework.cache.serializer;

import java.io.Serializable;

/**
 * 
 * 缓存静态类
 *
 */
class ProtoWrapper implements Serializable{

	private static final long serialVersionUID = -3048933160614391768L;

	public Object data;

}
