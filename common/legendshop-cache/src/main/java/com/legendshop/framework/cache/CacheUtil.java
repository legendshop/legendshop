/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.cache;

/**
 * 缓存工具类
 */
public class CacheUtil {
	
	/**
	 * 初始化缓存前缀,缓存格式为customerId +cacheName + key
	 * 
	 * @return 缓存的名字
	 */
	public static String getCachePrefix(String cacheName, String key) {
		String result = cacheName + key;
		if (result.length() > 255) {
			throw new RuntimeException("Cache key {} is too long for max length 255 " + result);
		}
		return result;
	}
	
	/**
	 * 初始化相关缓存前缀,缓存格式为customerId +cacheName + key
	 * 
	 * @return 缓存的名字
	 */
	public static String getRelCachePrefix( AbstractLegendCache relCache, String cacheName, String key) {
		String result =  relCache.getName() +  cacheName + key;
		if (result.length() > 255) {
			throw new RuntimeException("Cache key {} is too long for max length 255 " + result);
		}
		return result;
	}

}
