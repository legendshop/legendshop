/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.caffeine;

/**
 * 一级缓存的配置.
 */
public class Caffeine {

	/** 是否支持一级缓存 **/
	private boolean cachable;

    /** 一级缓存和二级缓存的时间比例 **/
	private float timeScale = 0.6f;
	
	/** 访问后过期时间，单位毫秒. */
	private long expireAfterAccess = 1000 * 10;

	/** 写入后过期时间，单位毫秒. */
	private long expireAfterWrite = 1000 * 10;

	/** 写入后刷新时间，单位毫秒. */
	private long refreshAfterWrite = 1000 * 10;

	/** 初始化大小. */
	private int initialCapacity;

	/** 最大缓存对象个数，超过此数量时之前放入的缓存将失效. */
	private long maximumSize;

	/**
	 * 由于权重需要缓存对象来提供，对于使用spring cache这种场景不是很适合，所以暂不支持配置.
	 *
	 * @return the expire after access
	 */
	// private long maximumWeight;

	public long getExpireAfterAccess() {
		return expireAfterAccess;
	}

	/**
	 * Sets the expire after access.
	 *
	 * @param expireAfterAccess the expire after access
	 */
	public void setExpireAfterAccess(long expireAfterAccess) {
		this.expireAfterAccess = expireAfterAccess;
	}

	/**
	 * Gets the expire after write.
	 *
	 * @return the expire after write
	 */
	public long getExpireAfterWrite() {
		return expireAfterWrite;
	}

	/**
	 * Sets the expire after write.
	 *
	 * @param expireAfterWrite the expire after write
	 */
	public void setExpireAfterWrite(long expireAfterWrite) {
		this.expireAfterWrite = expireAfterWrite;
	}

	/**
	 * Gets the refresh after write.
	 *
	 * @return the refresh after write
	 */
	public long getRefreshAfterWrite() {
		return refreshAfterWrite;
	}

	/**
	 * Sets the refresh after write.
	 *
	 * @param refreshAfterWrite the refresh after write
	 */
	public void setRefreshAfterWrite(long refreshAfterWrite) {
		this.refreshAfterWrite = refreshAfterWrite;
	}

	/**
	 * Gets the initial capacity.
	 *
	 * @return the initial capacity
	 */
	public int getInitialCapacity() {
		return initialCapacity;
	}

	/**
	 * Sets the initial capacity.
	 *
	 * @param initialCapacity the initial capacity
	 */
	public void setInitialCapacity(int initialCapacity) {
		this.initialCapacity = initialCapacity;
	}

	/**
	 * Gets the maximum size.
	 *
	 * @return the maximum size
	 */
	public long getMaximumSize() {
		return maximumSize;
	}

	/**
	 * Sets the maximum size.
	 *
	 * @param maximumSize the maximum size
	 */
	public void setMaximumSize(long maximumSize) {
		this.maximumSize = maximumSize;
	}

	public boolean isCachable() {
		return cachable;
	}

	public void setCachable(boolean cachable) {
		this.cachable = cachable;
	}

	public float getTimeScale() {
		return timeScale;
	}

	public void setTimeScale(float timeScale) {
		this.timeScale = timeScale;
	}
}