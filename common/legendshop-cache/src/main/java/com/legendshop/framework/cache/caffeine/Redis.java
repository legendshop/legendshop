/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.caffeine;


/**
 * Redis的配置类.
 */
public class Redis {

	/** 是否支持二级缓存 **/
	private boolean cachable;

	/** 全局过期时间，单位秒，默认2分钟过期. */
	private long defaultExpiration = 120;

	/** 缓存更新时通知其他节点的topic名称. */
	private String topic = "cache:redis:caffeine:topic";
	
	/** 系统缓存更新. */
	private String systemTopic = "cache:redis:system:topic";

	/**
	 * Gets the default expiration.
	 *
	 * @return the default expiration
	 */
	public long getDefaultExpiration() {
		return defaultExpiration;
	}

	/**
	 * Sets the default expiration.
	 *
	 * @param defaultExpiration the default expiration
	 */
	public void setDefaultExpiration(long defaultExpiration) {
		this.defaultExpiration = defaultExpiration;
	}

	/**
	 * Gets the topic.
	 *
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * Sets the topic.
	 *
	 * @param topic the topic
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * Gets the system topic.
	 *
	 * @return the system topic
	 */
	public String getSystemTopic() {
		return systemTopic;
	}

	/**
	 * Sets the system topic.
	 *
	 * @param systemTopic the system topic
	 */
	public void setSystemTopic(String systemTopic) {
		this.systemTopic = systemTopic;
	}


	public boolean isCachable() {
		return cachable;
	}

	public void setCachable(boolean cachable) {
		this.cachable = cachable;
	}
}
