/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.listener;

import com.legendshop.framework.cache.caffeine.CacheMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * 公共缓存消息监听者，专门用于更新系统的业务缓存，例如更新商品分类
 * 
 */
public class SystemCacheMessageListener implements MessageListener {
	
	private final Logger logger = LoggerFactory.getLogger(SystemCacheMessageListener.class);
	
	private RedisTemplate<Object, Object> redisTemplate;
	
	@Autowired(required = false)
	private UpdateSystemParameterCache updateCache;
	
	public SystemCacheMessageListener(RedisTemplate<Object, Object> redisTemplate) {
		super();
		this.redisTemplate = redisTemplate;
	}

	@Override
	public void onMessage(Message message, byte[] pattern) {
		if (message.getBody() == null) {
			return;
		}
		ReentrantLock lock = new ReentrantLock();
		try {
			lock.lock();
			CacheMessage cacheMessage = (CacheMessage) redisTemplate.getValueSerializer().deserialize(message.getBody());
			
			if(cacheMessage == null) {
				logger.info("Receive null cache message");
				return;
			}
			
			logger.debug("recevice a redis topic message, clear local cache, the cacheName is {}, the key is {}", cacheMessage.getCacheName(),
					cacheMessage.getKey());

			//更新本地缓存
			logger.warn("notify other node to update their cache {} ", cacheMessage.getCacheName());
			if(updateCache != null){
				updateCache.onMessage(cacheMessage);
			}

		} catch (Exception e) {
			logger.error("", e);
		}finally {
			lock.unlock();
		}
	}
}