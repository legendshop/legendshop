/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.caffeine;

import java.util.HashMap;
import java.util.Map;

/**
 * 缓存属性配置，包括RemoteCache 和 LocalCache, RemoteCache 采用 Redis实现， LocalCache采用Caffeine实现
 * 
 */
public class CacheRedisCaffeineProperties {

	/** 是否存储空值，默认true，防止缓存穿透. */
	private boolean cacheNullValues = true;

	/** 是否动态根据cacheName创建Cache的实现，默认true. */
	private boolean dynamic = true;

	/** 缓存key的前缀. */
	private String cachePrefix;

	/** Redis默认值  */
	private Redis redis = new Redis();

	/** LocalCache默认值 */
	private Caffeine caffeine = new Caffeine();

	/** 每个cacheName的过期时间，单位秒，优先级比defaultExpiration高. */
	private Map<String, Long> expires = new HashMap<>();

	/**
	 * Checks if is cache null values.
	 *
	 * @return true, if checks if is cache null values
	 */
	public boolean isCacheNullValues() {
		return cacheNullValues;
	}

	/**
	 * Sets the cache null values.
	 *
	 * @param cacheNullValues the cache null values
	 */
	public void setCacheNullValues(boolean cacheNullValues) {
		this.cacheNullValues = cacheNullValues;
	}

	/**
	 * Checks if is dynamic.
	 *
	 * @return true, if checks if is dynamic
	 */
	public boolean isDynamic() {
		return dynamic;
	}

	/**
	 * Sets the dynamic.
	 *
	 * @param dynamic the dynamic
	 */
	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}

	/**
	 * Gets the cache prefix.
	 *
	 * @return the cache prefix
	 */
	public String getCachePrefix() {
		return cachePrefix;
	}

	/**
	 * Sets the cache prefix.
	 *
	 * @param cachePrefix the cache prefix
	 */
	public void setCachePrefix(String cachePrefix) {
		this.cachePrefix = cachePrefix;
	}

	/**
	 * Gets the redis.
	 *
	 * @return the redis
	 */
	public Redis getRedis() {
		return redis;
	}

	/**
	 * Sets the redis.
	 *
	 * @param redis the redis
	 */
	public void setRedis(Redis redis) {
		this.redis = redis;
	}

	/**
	 * Gets the caffeine.
	 *
	 * @return the caffeine
	 */
	public Caffeine getCaffeine() {
		return caffeine;
	}

	/**
	 * Sets the caffeine.
	 *
	 * @param caffeine the caffeine
	 */
	public void setCaffeine(Caffeine caffeine) {
		this.caffeine = caffeine;
	}

	/**
	 * Gets the expires.
	 *
	 * @return the expires
	 */
	public Map<String, Long> getExpires() {
		return expires;
	}

	/**
	 * Sets the expires.
	 *
	 * @param expires the expires
	 */
	public void setExpires(Map<String, Long> expires) {
		this.expires = expires;
	}
}