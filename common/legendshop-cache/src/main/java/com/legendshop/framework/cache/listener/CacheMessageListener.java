/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.framework.cache.caffeine.RedisCaffeineCacheManager;

/**
 * 
 * 缓存消息监听者,专门用于清理一级缓存。
 * 
 */
public class CacheMessageListener implements MessageListener {
	private final Logger logger = LoggerFactory.getLogger(CacheMessageListener.class);
	private RedisTemplate<Object, Object> redisTemplate;
	private RedisCaffeineCacheManager redisCaffeineCacheManager;

	public CacheMessageListener(RedisTemplate<Object, Object> redisTemplate, RedisCaffeineCacheManager redisCaffeineCacheManager) {
		super();
		this.redisTemplate = redisTemplate;
		this.redisCaffeineCacheManager = redisCaffeineCacheManager;
	}

	@Override
	public void onMessage(Message message, byte[] pattern) {
		if (message.getBody() == null) {
			return;
		}
		try {
			CacheMessage cacheMessage = (CacheMessage) redisTemplate.getValueSerializer().deserialize(message.getBody());
			
			if(cacheMessage == null) {
				logger.info("Receive null cache message");
				return;
			}
			
			logger.debug("recevice a redis topic message, clear local cache, the cacheName is {}, the key is {}", cacheMessage.getCacheName(),
					cacheMessage.getKey());
			redisCaffeineCacheManager.clearLocal(cacheMessage.getCacheName(), cacheMessage.getKey());
		} catch (Exception e) {
			logger.error("", e);
		}

	}
}