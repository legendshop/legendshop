/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.client;


import java.util.Set;

/**
 * redis 客户端.
 */
public interface CacheClient {

	/**
	 * 根据key或者缓存的值.
	 *
	 * @param <T> the generic type
	 * @param key the key
	 * @return the t
	 */
	public <T> T get(String key);

	/**
	 * 加入缓存.
	 *
	 * @param key 关键字
	 * @param exp 超时时间, 为0则永久有效
	 * @param value 值
	 * @return 结果
	 */
	public Long put(String key, int exp, Object value);

	/**
	 * 删除缓存.
	 *
	 * @param key the key
	 * @return the long
	 */
	public Long delete(String key);

	/**
	 * 清除所有的缓存.
	 *
	 * @return the string
	 */
	public String clear();

	/**
	 * Keys.
	 *
	 * @param cacheName the cache name
	 * @return the sets the
	 */
	public Set<Object> keys(String cacheName);

	/**
	 * 增加数量.
	 *
	 * @param key the key
	 * @param num the num
	 * @return the long
	 */
	long incrBy(String key, long num);
	
	/**
	 * 减少数量
	 * @param key
	 * @param num
	 * @return
	 */
	long decr(String key, long num);
	
	/**
	 * 批量删除
	 * @param refixKey 前缀
	 * @return
	 */
	public void batchDelete(String prefixKey);

}
