package com.legendshop.framework.cache.serializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import io.protostuff.LinkedBuffer;
import io.protostuff.ProtostuffIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;

/**
 * Protostuff Redis的序列化
 *
 */
public class ProtostuffSerializer implements RedisSerializer<Object> {

    private final Logger logger = LoggerFactory.getLogger(ProtostuffSerializer.class);

    private final Schema<ProtoWrapper> schema;

    public ProtostuffSerializer() {
        this.schema = RuntimeSchema.getSchema(ProtoWrapper.class);
    }

    @Override
    public byte[] serialize(Object t) throws SerializationException {
        if (t == null) {
            return new byte[0];
        }
        try {
            ProtoWrapper wrapper = new ProtoWrapper();
            wrapper.data = t;

            LinkedBuffer buffer = LinkedBuffer.allocate(4096);
            try {
                return ProtostuffIOUtil.toByteArray(wrapper, schema, buffer);
            } finally {
                buffer.clear();
                wrapper = null;
            }
        }catch (Exception e){
            logger.error("error happened in parsing " + t.getClass(),e.getLocalizedMessage());
            return null;
        }

    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        if (isEmpty(bytes)) {
            return null;
        }

        try {
            ProtoWrapper newMessage = schema.newMessage();
            if(newMessage == null) {
                return null;
            }
            ProtostuffIOUtil.mergeFrom(bytes, newMessage, schema);
            Object result = newMessage.data;
            //System.out.println(Thread.currentThread().getName() +   " 反序列化对象大小 =  " + bytes.length + ", 反序列化出来的对象是 " + result.getClass());
            return result;
        } catch (Exception e) {
            logger.error("error happened in parsing bytes size = " + bytes.length,e.getLocalizedMessage());
            return null;
        }
    }


    private boolean isEmpty(byte[] data) {
        return (data == null || data.length == 0);
    }
    
}
