/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.framework.cache.client;

import com.legendshop.framework.cache.CacheUtil;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.DecoratedRedisConnection;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * RedisCacheClient.
 * Redis缓存客户端
 */
public class RedisCacheClient implements CacheClient {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(RedisCacheClient.class);

   private  RedisTemplate<Object, Object> redisTemplate;
   
   private RedisSerializer<Object> serializer;
   
   public void setRedisTemplate(RedisTemplate<Object, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
   }
   
   public CacheClient getNativeCache() {
		return this;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		Object object = null;
	    final String keyf = key;
		object = redisTemplate.execute(new RedisCallback<Object>() {
			public Object doInRedis(RedisConnection connection)
					throws DataAccessException {
				byte[] keyByte = keyf.getBytes();
				byte[] value = connection.get(keyByte);
				if (value == null) {
					return null;
				}
				return serializer.deserialize(value);
			}
		});
		return (T) object;
	}
	
	@Override
	public Long delete(String key) {
		final String keyf =key;
		return redisTemplate.execute(new RedisCallback<Long>() {
			public Long doInRedis(RedisConnection connection)
					throws DataAccessException {
				return connection.del(keyf.getBytes());
			}
		});
	}
	

	@Override
	public Long put(String key, int exp, Object value) {
		final String keyf =key;
		final Object valuef = value;
		final int liveTime = exp;
		return redisTemplate.execute(new RedisCallback<Long>() {
			public Long doInRedis(RedisConnection connection)
					throws DataAccessException {
				
				if (!isClusterConnection(connection)) {  //是否是Redis集群模式
					connection.multi();
				}
				byte[] keyb = keyf.getBytes();
				byte[] valueb = serializer.serialize(valuef);
				if (keyb.length == 0) {
					connection.del(keyb);
				} else {
					connection.set(keyb, valueb);
					if (liveTime > 0) {
						connection.expire(keyb, liveTime);
					}
				}
				if (!isClusterConnection(connection)) {
					connection.exec();
				}
				return 1l;//成功设置
			}
		});
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.springframework.cache.Cache#clear()
	 */
	public String clear() {
		return redisTemplate.execute(new RedisCallback<String>() {    
            public String doInRedis(RedisConnection connection)    
                    throws DataAccessException {    
                 connection.flushDb();    
                 return "ok";    
             }    
         });    
	}

	public Set<Object> keys(final String value){
		return redisTemplate.keys(value+"*");
	}
	
	
	private static boolean isClusterConnection(RedisConnection connection) {
		while (connection instanceof DecoratedRedisConnection) {
			connection = ((DecoratedRedisConnection) connection).getDelegate();
		}
		return connection instanceof RedisClusterConnection;
	}

	public void setSerializer(RedisSerializer<Object> serializer) {
		this.serializer = serializer;
	}

	@Override
	public long incrBy(String key, long num) {
		return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection connection) throws DataAccessException {
				return connection.incrBy(key.getBytes(),num);
			}
		});
	}

	public long decr(String key, long i) {
		return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection connection) throws DataAccessException {
				return connection.decrBy(key.getBytes(),i);
			}
		});
	}
	
	
	/**
	 * 批量删除
	 * @param prefixKey 前缀
	 * @return
	 */
	@Override
	public void batchDelete(String prefixKey) {
		String fullKey = CacheUtil.getCachePrefix(prefixKey, "");
		Set<Object> keys = redisTemplate.keys(fullKey + "*");
		if(AppUtils.isNotBlank(keys)){
			for (Object key : keys) {
				log.debug("删除cache key {}", key);
			}
			redisTemplate.delete(keys);
		}
	}
	
	public Boolean expire(String sessionKey, int defaultMaxExpireTime, TimeUnit timeUnit) {
		return redisTemplate.expire(sessionKey, defaultMaxExpireTime, timeUnit);
	}

	public RedisTemplate<Object, Object> getRedisTemplate() {
		return redisTemplate;
	}

	
}
