package com.legendshop.test;

import org.junit.Before;
import org.junit.Test;

import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.framework.cache.caffeine.Caffeine;
import com.legendshop.framework.cache.caffeine.Redis;
import com.legendshop.framework.cache.serializer.ProtostuffSerializer;

public class ProtostuffSerializerTest {
	
	private ProtostuffSerializer protostuffSerializer;

	@Before
	public void setUp() throws Exception {
		protostuffSerializer = new ProtostuffSerializer();
	}

	@Test
	public void testSerialize() {
		System.out.println("testSerializes");
		for (int i = 0; i < 1; i++) {
			System.out.println(" ---------------------------------------------  开始序列化 " + i);
			Caffeine caffeine = new Caffeine();
			caffeine.setExpireAfterAccess(1000);
			caffeine.setExpireAfterWrite(100);
			Thread thread = new Thread(new TestSerializeThread(caffeine));
			thread.start();
			
			
			CacheMessage cacheMessage = new CacheMessage();
			cacheMessage.setCacheName("cacheName");
			cacheMessage.setKey("Key");
			Thread thread1 = new Thread(new TestSerializeThread(cacheMessage));
			thread1.start();
			
			
			Redis redis = new Redis();
			redis.setSystemTopic("systemTopic");
			redis.setTopic("topic");
			Thread thread2 = new Thread(new TestSerializeThread(redis));
			thread2.start();
			
		}
		
	}
	
	private class TestSerializeThread implements Runnable{
		
		private Object obj;

		TestSerializeThread(Object obj){
			this.obj = obj;
		}
		
		@Override
		public void run() {
			byte[] bytes = protostuffSerializer.serialize(obj);
			System.out.println("序列化对象 = " + obj.getClass() + ", result = " + bytes.length);
			
			Object obj = protostuffSerializer.deserialize(bytes);
			System.out.println(" >>>>>>>>>>   反序列化对象 = " + obj.getClass() + ", 序列化数据大小为 =  " + bytes.length);
		}
		
	}
	

}
