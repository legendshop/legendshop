package com.legendshop.config.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "wechat")
public class WeixinPropertiesDto {

  /**
   * 微信服务的域名
   */
  @Value("${wechat.WEIXIN_DOMAIN_NAME}")
  private String weixinDomainName;
  
  /**
   * 微信服务器
   */
  @Value("${wechat.WEIXIN_SERVER}")
  private String weixinServer;
  
  /**
   * 是否记录发送日记
   */
  @Value("${wechat.IS_RECORD_RECEIVETEXT}")
  private Boolean recordReceivetext;
  
  /**
   * 是否启动智能机器人
   */
  @Value("${wechat.START_INTELLIGENT_ROBOT}")
  private Boolean startIntelligentRobot;
  
  /**
   * IP限制白名单
   */
  @Value("${wechat.ALLOWIPLIST}")
  private String allowiplist;
  
  /**
   * 微信的加密串，防止攻击用
   */
  @Value("${wechat.WEIXIN_KEY}")
  private String weixinKey;

  public String getWeixinDomainName() {
    return weixinDomainName;
  }

  public void setWeixinDomainName(String weixinDomainName) {
    this.weixinDomainName = weixinDomainName;
  }

  public String getWeixinServer() {
		return weixinServer;
	}

	public void setWeixinServer(String weixinServer) {
		this.weixinServer = weixinServer;
	}

	public Boolean getRecordReceivetext() {
    return recordReceivetext;
  }

  public void setRecordReceivetext(Boolean recordReceivetext) {
    this.recordReceivetext = recordReceivetext;
  }

  public Boolean getStartIntelligentRobot() {
    return startIntelligentRobot;
  }

  public void setStartIntelligentRobot(Boolean startIntelligentRobot) {
    this.startIntelligentRobot = startIntelligentRobot;
  }

  public String getAllowiplist() {
    return allowiplist;
  }

  public void setAllowiplist(String allowiplist) {
    this.allowiplist = allowiplist;
  }

  public String getWeixinKey() {
    return weixinKey;
  }

  public void setWeixinKey(String weixinKey) {
    this.weixinKey = weixinKey;
  }
}
