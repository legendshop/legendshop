package com.legendshop.config;

import com.legendshop.util.SystemUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.config.dto.CommonPropertiesDto;
import com.legendshop.config.dto.DBPropertiesDto;
import com.legendshop.config.dto.RedisPropertiesDto;
import com.legendshop.config.dto.WeixinPropertiesDto;
import com.legendshop.util.AppUtils;


/**
 * 公共的配置文件的地方
 *
 */
@Component("propertiesUtil")
public class PropertiesUtil{


	@Autowired
	private CommonPropertiesDto commonPropertiesDto;

	@Autowired
	private WeixinPropertiesDto weixinPropertiesDto;

	@Autowired
	private DBPropertiesDto dbPropertiesDto;

	@Autowired
	private RedisPropertiesDto redisPropertiesDto;


	// /////////////////////////////////////////////////////////////////////////////////////
	// 以下是配置文件的配置
	// /////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks if is system in debug mode.
	 *
	 * @return true, if is system in debug mode
	 */
	public Boolean isSQLInDebugMode() {
		return dbPropertiesDto.getSqlDebugMode();
	}

	/**
	 * 读取PC端域名配置
	 *
	 */
	public String getPcDomainName() {
		return commonPropertiesDto.getPcDomainName();
	}

	/**
	 * 读取手机端域名配置
	 *
	 */
	public String getMobileDomainName() {
		return commonPropertiesDto.getMobileDomainName();
	}

	/**
	 * 得到用户的App地址
	 *
	 * @return
	 */
	public String getUserAppDomainName() {
		return commonPropertiesDto.getAppUserDomainName();
	}

	/**
	 * 得到商家端的App地址
	 *
	 * @return
	 */
	public String getBizAppDomainName() {
		return commonPropertiesDto.getAppBizDomainName();
	}

	/**
	 * 读取VUE域名配置
	 *
	 */
	public String getVueDomainName() {
		return commonPropertiesDto.getVueDomainName();
	}

	/**
	 * 读取微信的密钥
	 *
	 * @return
	 */
	public String getWeiXinKey() {
		String secret = weixinPropertiesDto.getWeixinKey();
		if (AppUtils.isBlank(secret)) {
			secret = "sjdjgkldfgkldfjgljdasdfhgdfjkl12334";
		}
		return secret;
	}

	/**
	 * Gets the domain name.
	 *
	 * @return the domain name
	 */
	public String getWeiXinService() {
		return weixinPropertiesDto.getWeixinServer();
	}

	/**
	 * 微信域名
	 *
	 * @return
	 */
	public String getWeiXinDomainName() {
		return weixinPropertiesDto.getWeixinDomainName();
	}

	/**
	 * 获取图片的管理员IP,图片服务器使用
	 *
	 * @return
	 */
	public String getPhotoAdminIp() {
		String photoAdminIp = commonPropertiesDto.getPhotoAdminIp();
		if (AppUtils.isBlank(photoAdminIp)) {
			photoAdminIp = "127.0.0.1";
		}
		return photoAdminIp;
	}

	/**
	 * 查询Master_Solr的Url 如果有值就是远程服务 没有值就是本地链接
	 */
	public String getMasterSolrUrl() {
		return commonPropertiesDto.getMasterSearchServerSolrurl();
	}

	/**
	 * 查询Slave_Solr的Url 如果有值就是远程服务 没有值就是本地链接
	 */
	public String getSlaveSolrUrl() {
		return commonPropertiesDto.getSlaveSearchServerSolrurl();
	}

	/**
	 * 获取系统的token
	 */
	public String getSystemToken() {
		return commonPropertiesDto.getSystemToken();
	}

	/**
	 * 获取Im地址
	 */
	public String getImBindAddress() {
		return commonPropertiesDto.getImBindAddress();
	}


	public String getAllowIPList() {
		return commonPropertiesDto.getAllowIPList();
	}


	public String getBigPicPath() {
		String bigImagePath = commonPropertiesDto.getBigPicPath();
		if (AppUtils.isBlank(bigImagePath)) {
			bigImagePath = SystemUtil.getSystemRealPath() + "bigImage";
		}
		return bigImagePath;
	}


	public String getSnapshotBigPicPath() {
		String bigImagePath = commonPropertiesDto.getSnapshotBigPicPath();
		if (AppUtils.isBlank(bigImagePath)) {
			bigImagePath = SystemUtil.getSystemRealPath() + "snapBigImage";
		}
		return bigImagePath;
	}


	public String getSmallPicPath() {
		String smallImagePath = commonPropertiesDto.getSmallPicPath();
		if (AppUtils.isBlank(smallImagePath)) {
			smallImagePath = SystemUtil.getSystemRealPath() + "smallImage";
		}
		return smallImagePath;
	}


	public String getBackupPicPath() {
		return commonPropertiesDto.getBackupPicPath();
	}

	/**
	 * 读取图片服务器的地址 如果没有配置，则采用系统contextPath
	 *
	 * @return
	 */

	public String getPhotoServer() {
		return commonPropertiesDto.getPhotoServer();
	}

	/**
	 * 读取图片服务器的地址 如果没有配置，则采用系统contextPath
	 *
	 * @return
	 */

	public String getInnerPhotoServer() {
		String photoServerPath = commonPropertiesDto.getInnerPhotoServer();
		if (AppUtils.isBlank(photoServerPath)) {
			photoServerPath = SystemUtil.getContextPath();
		}
		return photoServerPath;
	}


	public String getOssDomainName() {
		return commonPropertiesDto.getOssDomainName();
	}


	public String getAttachmentType() {
		return commonPropertiesDto.getAttachmentType();
	}


	public String getCacheTopic() {
		return redisPropertiesDto.getCacheTopic();
	}


	public String getSystemTopic() {
		return redisPropertiesDto.getSystemTopic();
	}


	public String getEndpoint() {
		return commonPropertiesDto.getEndpoint();
	}


	public String getAccessKeyId() {
		return commonPropertiesDto.getAccessKeyId();
	}


	public String getAccessKeySecret() {
		return commonPropertiesDto.getAccessKeySecret();
	}


	public String getBucketName() {
		return commonPropertiesDto.getBucketName();
	}

	public String getBucketNamePrivate() {
		return commonPropertiesDto.getBucketNamePrivate();
	}


    public String getAppFileSubPath() {
		return "apps";
    }



}
