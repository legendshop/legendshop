package com.legendshop.config;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
/**
 * 用来封装那些不能在spring bean中直接引用propertiesUtil的地方
 *
 */
public class PropertiesUtilManager {
	/**
	 * 示例
	 */
	private static PropertiesUtil propertiesUtil;
	
	/**
	 * 获取配置中心
	 */
	public static PropertiesUtil getPropertiesUtil() {
		if(propertiesUtil == null) {
			synchronized (PropertiesUtilManager.class) {
				if(propertiesUtil == null) {
					propertiesUtil = (PropertiesUtil) ContextServiceLocator.getBean("propertiesUtil");
				}
			}
		}
		
		return propertiesUtil;
	}

}
