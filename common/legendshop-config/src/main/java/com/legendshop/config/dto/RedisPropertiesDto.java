package com.legendshop.config.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * redis.properties 配置文件
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "redis")
public class RedisPropertiesDto {

	/**
	 * 主机名称
	 */
	@Value("${redis.hostName}")
	private String hostName;

	/**
	 * 主机名称
	 */
	@Value("${redis.clusterHostName}")
	private String clusterHostName;

	/**
	 * 是否是集群
	 */
	@Value("${redis.cluster}")
	private Boolean cluster;

	/**
	 * 端口
	 */
	@Value("${redis.port}")
	private Integer port;

	/**
	 * app用户端域名
	 */
	@Value("${redis.timeout}")
	private Integer timeout;

	/**
	 * 是否使用池
	 */
	@Value("${redis.usePool}")
	private Boolean usePool;

	/**
	 * 密码
	 */
	@Value("${redis.password}")
	private String password;

	/**
	 * #表示当borrow一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
	 */
	@Value("${redis.maxIdle}")
	private Integer maxIdle;

	@Value("${redis.ssl:false}")
	public boolean ssl;

	/**
	 * 空闲连接多长时间后会被收回
	 */
	@Value("${redis.minEvictableIdleTimeMillis:3000}")
	private Integer minEvictableIdleTimeMillis;

	/**
	 * 表示idle object evitor每次扫描的最多的对象数
	 */
	@Value("${redis.numTestsPerEvictionRun:10}")
	private Integer numTestsPerEvictionRun;

	/**
	 * 多长时间检查一次连接池中空闲的连接
	 */
	@Value("${redis.timeBetweenEvictionRunsMillis:60000}")
	private Integer timeBetweenEvictionRunsMillis;

	/**
	 * 最大的连接数
	 */
	@Value("${redis.maxTotal:1000}")
	private Integer maxTotal;

	/**
	 * 最大的等待时间
	 */
	@Value("${redis.maxWaitMillis:2000}")
	private Integer maxWaitMillis;

	/**
	 * 缓存数据存放的地方
	 */
	@Value("${redis.database:14}")
	private Integer database;

	/**
	 * session缓存存放的地方
	 */
	@Value("${redis.session.database:14}")
	private Integer sessionDatabase;

	/**
	 * 消息更新的topic
	 */
	@Value("${redis.cache.topic:'redis.cache.topic'}")
	private String cacheTopic;

	/**
	 * 系统缓存消息更新的topic
	 */
	@Value("${redis.system.topic:'redis.system.topic'}")
	private String systemTopic;

	/**
	 * 系统缓存消息更新的topic
	 */
	@Value("${redis.cache.prefix:'legndshop'}")
	private String cachePrefix;



}
