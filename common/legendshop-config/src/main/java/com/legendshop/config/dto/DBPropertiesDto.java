package com.legendshop.config.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "jdbc")
public class DBPropertiesDto {

  /**
   * SQLDebug模式
   */
  @Value("${jdbc.SQL_DEBUG_MODE}")
  private Boolean sqlDebugMode;
  
  /**
   * 驱动名称
   */
  @Value("${jdbc.driverClassName}")
  private String driverClassName;
  
  /**
   * 数据库连接
   */
  @Value("${jdbc.url}")
  private String url;
  
  /**
   * 数据库连接
   */
  @Value("${jdbc.username}")
  private String userName;
  
  /**
   * 数据库密码
   */
  @Value("${jdbc.password}")
  private String passWord;
  
  /**
   * 数据库最大连接池数
   */
  @Value("${jdbc.maxActive}")
  private String maxActive;
  
  /**
   * 初始化时建立物理连接的个数
   */
  @Value("${jdbc.initialSize}")
  private String initialSize;
  
  /**
   * 获取连接超时时间（单位：ms）
   */
  @Value("${jdbc.maxWait}")
  private String maxWait;
  
  /**
   * 最小连接池数量
   */
  @Value("${jdbc.minIdle}")
  private String minIdle;
  
  /**
   * 连接有效性检测时间(单位:ms)
   */
  @Value("${jdbc.timeBetweenEvictionRunsMillis}")
  private String timeBetweenEvictionRunsMillis;
  
  /**
   * 最大空闲时间(单位ms)
   */
  @Value("${jdbc.minEvictableIdleTimeMillis}")
  private String minEvictableIdleTimeMillis;
  
  /**
   * 建议配置为true，不影响性能，并且保证安全性。
   */
  @Value("${jdbc.testWhileIdle}")
  private String testWhileIdle;
  
  /**
   * 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。
   */
  @Value("${jdbc.testOnBorrow}")
  private String testOnBorrow;

  /**
   * 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
   */
  @Value("${jdbc.testOnReturn}")
  private String testOnReturn;
  
  /**
   * 是否缓存preparedStatement，也就是PSCache。
   */
  @Value("${jdbc.poolPreparedStatements}")
  private String poolPreparedStatements;
  
  /**
   * 要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。
   */
  @Value("${jdbc.maxOpenPreparedStatements}")
  private String maxOpenPreparedStatements;
  
  /**
   * 要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。
   */
  @Value("${jdbc.maxPoolPreparedStatementPerConnectionSize}")
  private String maxPoolPreparedStatementPerConnectionSize;
  
  public Boolean getSqlDebugMode() {
    return sqlDebugMode;
  }

  public void setSqlDebugMode(Boolean sqlDebugMode) {
    this.sqlDebugMode = sqlDebugMode;
  }

  public String getDriverClassName() {
    return driverClassName;
  }

  public void setDriverClassName(String driverClassName) {
    this.driverClassName = driverClassName;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassWord() {
    return passWord;
  }

  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }

  public String getMaxActive() {
    return maxActive;
  }

  public void setMaxActive(String maxActive) {
    this.maxActive = maxActive;
  }

  public String getInitialSize() {
    return initialSize;
  }

  public void setInitialSize(String initialSize) {
    this.initialSize = initialSize;
  }

  public String getMaxWait() {
    return maxWait;
  }

  public void setMaxWait(String maxWait) {
    this.maxWait = maxWait;
  }

  public String getMinIdle() {
    return minIdle;
  }

  public void setMinIdle(String minIdle) {
    this.minIdle = minIdle;
  }

  public String getTimeBetweenEvictionRunsMillis() {
    return timeBetweenEvictionRunsMillis;
  }

  public void setTimeBetweenEvictionRunsMillis(String timeBetweenEvictionRunsMillis) {
    this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
  }

  public String getMinEvictableIdleTimeMillis() {
    return minEvictableIdleTimeMillis;
  }

  public void setMinEvictableIdleTimeMillis(String minEvictableIdleTimeMillis) {
    this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
  }

  public String getTestWhileIdle() {
    return testWhileIdle;
  }

  public void setTestWhileIdle(String testWhileIdle) {
    this.testWhileIdle = testWhileIdle;
  }

  public String getTestOnBorrow() {
    return testOnBorrow;
  }

  public void setTestOnBorrow(String testOnBorrow) {
    this.testOnBorrow = testOnBorrow;
  }

  public String getTestOnReturn() {
    return testOnReturn;
  }

  public void setTestOnReturn(String testOnReturn) {
    this.testOnReturn = testOnReturn;
  }

  public String getPoolPreparedStatements() {
    return poolPreparedStatements;
  }

  public void setPoolPreparedStatements(String poolPreparedStatements) {
    this.poolPreparedStatements = poolPreparedStatements;
  }

  public String getMaxOpenPreparedStatements() {
    return maxOpenPreparedStatements;
  }

  public void setMaxOpenPreparedStatements(String maxOpenPreparedStatements) {
    this.maxOpenPreparedStatements = maxOpenPreparedStatements;
  }

  public String getMaxPoolPreparedStatementPerConnectionSize() {
    return maxPoolPreparedStatementPerConnectionSize;
  }

  public void setMaxPoolPreparedStatementPerConnectionSize(String maxPoolPreparedStatementPerConnectionSize) {
    this.maxPoolPreparedStatementPerConnectionSize = maxPoolPreparedStatementPerConnectionSize;
  }
}
