package com.legendshop.config.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * common.yml 配置文件
 */
@Configuration
@ConfigurationProperties(prefix = "common")
public class CommonPropertiesDto {

	/**
	 * pc域名
	 */
	@Value("${common.PC_DOMAIN_NAME}")
	private String pcDomainName;

	/**
	 * 手机端域名
	 */
	@Value("${common.MOBILE_DOMAIN_NAME}")
	private String mobileDomainName;

	/**
	 * app用户端域名
	 */
	@Value("${common.APP_USER_DOMAIN_NAME}")
	private String appUserDomainName;

	/**
	 * app商家端域名
	 */
	@Value("${common.APP_BIZ_DOMAIN_NAME}")
	private String appBizDomainName;

	/**
	 * vue端域名
	 */
	@Value("${common.VUE_DOMAIN_NAME}")
	private String vueDomainName;

	/**
	 * 图片服务器的类型，# 1. Local、Http、OSS
	 */
	@Value("${common.ATTACHMENT_TYPE}")
	private String attachmentType;

	/**
	 * 大图的存储路径
	 */
	@Value("${common.BIG_PIC_PATH:}")
	private String bigPicPath;

	/**
	 * 商品快照的存储路径
	 */
	@Value("${common.SNAPSHOT_BIG_PIC_PATH:}")
	private String snapshotBigPicPath;

	/**
	 * 缩略图的存储路径
	 */
	@Value("${common.SMALL_PIC_PATH:}")
	private String smallPicPath;

	/**
	 * 备份图片存储路径
	 */
	@Value("${common.BACKUP_PIC_PATH:}")
	private String backupPicPath;
	// ------- for local的配置end ------ //

	// ------- for Http的配置begin ------ //
	/**
	 * 图片服务器的URL,外网地址
	 */
	@Value("${common.PHOTO_SERVER:}")
	private String photoServer;

	/**
	 * 图片写入的URL地址，内网地址
	 */
	@Value("${common.INNER_PHOTO_SERVER:}")
	private String innerPhotoServer;

	// ------- for Http的配置end ------ //

	// ------- for OSS的配置begin ------ //
	/**
	 * OOS域名
	 */
	@Value("${common.OSS_DOMAIN_NAME:}")
	private String ossDomainName;

	/**
	 * OSS的key
	 */
	@Value("${common.accessKeyId:}")
	private String accessKeyId;

	/**
	 * OSS的Secret
	 */
	@Value("${common.accessKeySecret:}")
	private String accessKeySecret;

	/**
	 * OSS的endpoint
	 */
	@Value("${common.endpoint:}")
	private String endpoint;

	/**
	 * OSS的bucketName
	 */
	@Value("${common.bucketName:}")
	private String bucketName;

	/**
	 * OSS的私有bucketName
	 */
	@Value("${common.bucketNamePrivate:}")
	private String bucketNamePrivate;


	// ------- for OSS的配置end ------ //


	/**
	 * solr服务的地址
	 */
	@Value("${common.MASTER_SEARCH_SERVER_SOLRURL}")
	private String masterSearchServerSolrurl;

	/**
	 * solr服务的地址
	 */
	@Value("${common.SLAVE_SEARCH_SERVER_SOLRURL:}")
	private String slaveSearchServerSolrurl;

	/**
	 * 系统token，系统之间相互调用需要用到，统一一套
	 */
	@Value("${common.SYSTEM_TOKEN}")
	private String systemToken;

	/**
	 * 图片服务器管理IP
	 */
	@Value("${common.PHOTO_ADMIN_IP:}")
	private String photoAdminIp;

	/**
	 * LegendShop版本号
	 */
	@Value("${common.LEGENDSHOP_VERSION}")
	private String legendshopVersion;

	/**
	 * #验证登录模式，可选值 #basic 采用普通security方式 #cas 采用cas单点登录模式，需要配合cas系统使用
	 */
	@Value("${common.AUTHENTICATION_MODE}")
	private String authenticationMode;

	/**
	 * IM 地址 如: wss://diamondsim.legendshop.cn
	 */
	@Value("${common.IM_BIND_ADDRESS}")
	private String imBindAddress;

	/**
	 * IM IP地址 如: 0.0.0.0
	 */
	@Value("${common.IM_BIND_INSIDE_IP}")
	private String imBindInsideIp;

	/**
	 * 后台AdminIP访问控制白名单
	 * 单个或多个IP的白名单列表,多个用","或";"隔开,如:192.168.1.1;192.168.1.2;192.168.0.1-192.168.0.10;
	 * 
	 */
	@Value("${common.ADMIN_ALLOWS_IP:}")
	private String allowIPList;

	public String getPcDomainName() {
		return pcDomainName;
	}

	public void setPcDomainName(String pcDomainName) {
		this.pcDomainName = pcDomainName;
	}

	public String getMobileDomainName() {
		return mobileDomainName;
	}

	public void setMobileDomainName(String mobileDomainName) {
		this.mobileDomainName = mobileDomainName;
	}

	public String getAppUserDomainName() {
		return appUserDomainName;
	}

	public void setAppUserDomainName(String appUserDomainName) {
		this.appUserDomainName = appUserDomainName;
	}

	public String getAppBizDomainName() {
		return appBizDomainName;
	}

	public void setAppBizDomainName(String appBizDomainName) {
		this.appBizDomainName = appBizDomainName;
	}

	public String getVueDomainName() {
		return vueDomainName;
	}

	public void setVueDomainName(String vueDomainName) {
		this.vueDomainName = vueDomainName;
	}

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getBigPicPath() {
		return bigPicPath;
	}

	public void setBigPicPath(String bigPicPath) {
		this.bigPicPath = bigPicPath;
	}

	public String getSnapshotBigPicPath() {
		return snapshotBigPicPath;
	}

	public void setSnapshotBigPicPath(String snapshotBigPicPath) {
		this.snapshotBigPicPath = snapshotBigPicPath;
	}

	public String getSmallPicPath() {
		return smallPicPath;
	}

	public void setSmallPicPath(String smallPicPath) {
		this.smallPicPath = smallPicPath;
	}

	public String getBackupPicPath() {
		return backupPicPath;
	}

	public void setBackupPicPath(String backupPicPath) {
		this.backupPicPath = backupPicPath;
	}

	public String getPhotoServer() {
		return photoServer;
	}

	public void setPhotoServer(String photoServer) {
		this.photoServer = photoServer;
	}

	public String getInnerPhotoServer() {
		return innerPhotoServer;
	}

	public void setInnerPhotoServer(String innerPhotoServer) {
		this.innerPhotoServer = innerPhotoServer;
	}

	public String getOssDomainName() {
		return ossDomainName;
	}

	public void setOssDomainName(String ossDomainName) {
		this.ossDomainName = ossDomainName;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public String getAccessKeySecret() {
		return accessKeySecret;
	}

	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getMasterSearchServerSolrurl() {
		return masterSearchServerSolrurl;
	}

	public void setMasterSearchServerSolrurl(String masterSearchServerSolrurl) {
		this.masterSearchServerSolrurl = masterSearchServerSolrurl;
	}

	public String getLegendshopVersion() {
		return legendshopVersion;
	}

	public void setLegendshopVersion(String legendshopVersion) {
		this.legendshopVersion = legendshopVersion;
	}

	public String getAuthenticationMode() {
		return authenticationMode;
	}

	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}

	public String getImBindAddress() {
		return imBindAddress;
	}

	public String getImBindInsideIp() {
		return imBindInsideIp;
	}

	public void setImBindInsideIp(String imBindInsideIp) {
		this.imBindInsideIp = imBindInsideIp;
	}

	public String getSlaveSearchServerSolrurl() {
		return slaveSearchServerSolrurl;
	}

	public void setSlaveSearchServerSolrurl(String slaveSearchServerSolrurl) {
		this.slaveSearchServerSolrurl = slaveSearchServerSolrurl;
	}

	public String getSystemToken() {
		return systemToken;
	}

	public void setSystemToken(String systemToken) {
		this.systemToken = systemToken;
	}

	public String getAllowIPList() {
		return allowIPList;
	}

	public void setAllowIPList(String allowIPList) {
		this.allowIPList = allowIPList;
	}

	public String getPhotoAdminIp() {
		return photoAdminIp;
	}

	public void setPhotoAdminIp(String photoAdminIp) {
		this.photoAdminIp = photoAdminIp;
	}

	public String getBucketNamePrivate() {
		return bucketNamePrivate;
	}

	public void setBucketNamePrivate(String bucketNamePrivate) {
		this.bucketNamePrivate = bucketNamePrivate;
	}

}
