/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.test;

import com.legendshop.util.MethodUtils;
import com.legendshop.util.test.dto.FromDto;
import com.legendshop.util.test.dto.ToDto;

/**
 * GetterSetterGenerator测试类.
 */
public class MethodUtilsTest {
	
	/**
	 * The main method.
	 *
	 * @param args the args
	 */
	public static void main(String[] args) {
		System.out.println(MethodUtils.generateCode(FromDto.class, ToDto.class));
		
	}
}
