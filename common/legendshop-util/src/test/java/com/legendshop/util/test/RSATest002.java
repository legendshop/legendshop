/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.test;

import javax.crypto.Cipher;

import com.legendshop.util.RSAUtil;


public class RSATest002 {

	public static void main(String[] args) {

		// 1. 生成(公钥和私钥)密钥对
		RSAUtil.generateKey();
		System.out.println("公钥:" + RSAUtil.publicKey);
		System.out.println("私钥:" + RSAUtil.privateKey);
		System.out.println("----------公钥加密私钥解密-------------");
		// 使用 公钥加密,私钥解密
		String textsr = "yushengjun";
		String encryptByPublic = RSAUtil.encryptByPublicKey(textsr, RSAUtil.publicKey, Cipher.ENCRYPT_MODE);
		System.out.println("公钥加密:" + encryptByPublic);
		String text = RSAUtil.encryptByprivateKey(encryptByPublic, RSAUtil.privateKey, Cipher.DECRYPT_MODE);
		System.out.print("私钥解密:" + text);
	}

}
