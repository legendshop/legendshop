package com.legendshop.util.test.dto;

import java.io.Serializable;

public class ToDto  extends AbstractDto implements Serializable{

	private static final long serialVersionUID = 1463569270702659505L;

	private String id;
	
	private String name;
	
	private String name1; //to模块没有该字段
	
	private SameDto sameDto;
	
	private String demo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public SameDto getSameDto() {
		return sameDto;
	}

	public void setSameDto(SameDto sameDto) {
		this.sameDto = sameDto;
	}

	public String getDemo() {
		return demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}

}
