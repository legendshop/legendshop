package com.legendshop.util.test.dto;

public class AbstractDto {
	
	private Long value1;
	
	protected Long value2;
	
	public Long value3;

	public Long getValue1() {
		return value1;
	}

	public void setValue1(Long value1) {
		this.value1 = value1;
	}

	public Long getValue2() {
		return value2;
	}

	public void setValue2(Long value2) {
		this.value2 = value2;
	}

	public Long getValue3() {
		return value3;
	}

	public void setValue3(Long value3) {
		this.value3 = value3;
	}
	
}
