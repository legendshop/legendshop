package com.legendshop.util.test.dto;

import java.io.Serializable;

public class SameDto implements Serializable{

	private static final long serialVersionUID = -5228292923100844925L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
