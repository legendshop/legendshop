package com.legendshop.util.test;

import com.legendshop.util.AppUtils;
import com.legendshop.util.des.DES2;
/**
 * 获取微信的配置文件
 *
 */
public class WxPropertiesUtils{

	
	//public static final String ConfigFile = "/spring/wechat.properties";
	
	private static String APPSECRET=null;
	
	private static String PARTNERKEY=null;
	
	
	/**
	 * 获取AppSecret并解密,key为SECRETKEY
	 * @return
	 */
	public static String getAppSecret() {
		if(AppUtils.isBlank(APPSECRET)){
			String SECRETKEY="ls123456";
			System.out.println(SECRETKEY);
			String value="glg7ZAbiaWoLk6Kt2+yS03mbROmBT5RYt2g3fsJu2Jjh0ALkpahfyA==";
			System.out.println(value);
			if(AppUtils.isNotBlank(value)){
				DES2 des = new DES2(SECRETKEY);
				String aimStr = new String(des.createDecryptor(des.stringToByte(value)));
				APPSECRET=aimStr;
				return APPSECRET;
			}
		}
		return APPSECRET;
	}
	


	
	/** 支付密钥，商户平台 > API安全 > 密钥管理 中进行设置，加密过的 */
	public static String getPartnerKey() {
		if(AppUtils.isBlank(PARTNERKEY)){
			String SECRETKEY="ls123456";
			System.out.println(SECRETKEY);
			String value="XciO4QwrM7tYsn1t/cb7PUnHJb/kZjRThh6+ursIc0BGcBQVeLzZNA==";
			System.out.println("Value----"+value);
			if(AppUtils.isNotBlank(value)){
				DES2 des = new DES2(SECRETKEY);
				String aimStr = new String(des.createDecryptor(des.stringToByte(value)));
				PARTNERKEY=aimStr;
				return PARTNERKEY;
			}
		}
		return PARTNERKEY;
	}
	


	
	public static void main(String[] args) throws Exception {
		
		System.out.println(getAppSecret());
		System.out.println(getPartnerKey());
		
	}
}
