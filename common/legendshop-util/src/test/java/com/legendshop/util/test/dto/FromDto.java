package com.legendshop.util.test.dto;

public class FromDto extends AbstractDto{
	private static final long serialVersionUID = 1463569270702659505L;

	private String id;
	
	private String name;
	
	private Integer desc;
	
	private SameDto sameDto;
	
	private String demo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDesc() {
		return desc;
	}

	public void setDesc(Integer desc) {
		this.desc = desc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SameDto getSameDto() {
		return sameDto;
	}

	public void setSameDto(SameDto sameDto) {
		this.sameDto = sameDto;
	}

	public String getDemo() {
		return demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}

}
