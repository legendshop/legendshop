package com.legendshop.util.test;

public class Thread1 implements Runnable {
	private static Object obj = new Object();
	public void run() {
		synchronized (obj) { // 请问这里的this怎样理解，t1和t2的地址不是不一样嘛，怎么会只有一个线程运行？？？
			for (int i = 0; i < 5; i++) {
				//System.out.println(Thread.currentThread().getName() + " synchronized loop " + i);
				Util.run(Thread.currentThread().getName() + " synchronized loop " + i);
			}
		}
	}
	

}