package com.legendshop.util.test;

import com.legendshop.util.AppUtils;
import com.legendshop.util.des.DES2;

public class Des2Test {

	public static void main(String[] args) {
		getAppSecret();

	}
	
	public static void getAppSecret() {
			String SECRETKEY="ls123456";
			System.out.println("加密因子" + SECRETKEY);
			String value="ab8e3ec976fcaa7ae1c094d04d80d513";
			
			System.out.println("加密前"+value);
			if(AppUtils.isNotBlank(value)){
				DES2 des = new DES2(SECRETKEY);
				//String aimStr = new String(des.createDecryptor(des.stringToByte(value)));
				String aimStr = des.byteToString(des.createEncryptor(value));
				System.out.println("加密后的字符 = " + aimStr);
				
				String decryptorString = new String(des.createDecryptor(des.stringToByte(aimStr)));
				System.out.println("解密后的字符 = " + decryptorString);
			}
		}


}
