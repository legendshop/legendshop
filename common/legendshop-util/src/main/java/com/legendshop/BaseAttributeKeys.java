/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop;

/**
 *LegendShop 基本属性定义
 */
public interface BaseAttributeKeys {

	/** 真的表达式. */
	public static final String TRUE_INDICATOR = "Y";

	/** 假的表达式. */
	public static final String FALSE_INDICATOR = "N";

	/** 上线状态. */
	public static final Integer ONLINE = 1;

	/** 下线状态. */
	public static final Integer OFFLINE = 0;

	/** . 停止使用，普通用户不能改为 ONLINE 或者 OFFLINE */
	public static final Integer STOPUSE = -1;

	/** 国际化文件放置的文件目录. */
	public static final String LOCALE_FILE = "i18n/ApplicationResources";
	
	/** 国际化文件放置的文件目录. */
	public static final String LOCALE_KEY = "LOCALE_KEY";
	
	public static final String LOCALE_RESOLVER = "localeResolver";

	public static final String LOCALE_MESSAGE_PREFIX = "message:";
}
