/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * 配置文件工具类
 */
public class EnvironmentConfig {

	/** The logger. */
	private final static Logger logger = LoggerFactory.getLogger(EnvironmentConfig.class);

	/** 单个实例. */
	static EnvironmentConfig ec;// 创建对象ec

	/** 内容容器. */
	private static Map<String, Properties> register = new HashMap<String, Properties>();// 静态对象初始化[在其它对象之前进行]

	/**
	 * 构造函数。
	 */
	private EnvironmentConfig() {
		super();
	}

	/**
	 * 取得EnvironmentConfig的一个实例.
	 * 
	 * @return ec
	 */
	public static EnvironmentConfig getInstance() {
		if (ec == null) {
			synchronized (EnvironmentConfig.class) {
				if (ec == null) {
					ec = new EnvironmentConfig();// 创建EnvironmentConfig对象
				}
			}

		}
		return ec;// 返回EnvironmentConfig对象
	}

	/**
	 * 读取配置文件.
	 * 
	 * @param fileName
	 *            the file name
	 * @return 该配置文件的方式
	 */

	private Properties getProperties(String fileName) {// 传递配置文件路径
		Properties p = register.get(fileName);// 将fileName存于一个HashTable
		if (p == null) {
			synchronized (EnvironmentConfig.class) {
				/**
				 * 如果为空就尝试输入进文件
				 */
				try {
					InputStream is = null;// 定义输入流is
					try {
						is = new FileInputStream(fileName);// 创建输入流
					} catch (Exception e) {
						if (fileName.startsWith("/"))
							// 用getResourceAsStream()方法用于定位并打开外部文件。
							is = EnvironmentConfig.class.getResourceAsStream(fileName);
						else
							is = EnvironmentConfig.class.getResourceAsStream("/" + fileName);
					}

					if(is == null){//spring boot的写法
						ClassPathResource cpr = new ClassPathResource(fileName);
						is = cpr.getInputStream();
					}
					if(is == null){
						throw new RuntimeException("It can not read  file " + fileName);
					}
					p = new Properties();
					p.load(is);// 加载输入流
					register.put(fileName, p);// 将其存放于HashTable
					is.close();// 关闭输入流

				} catch (Exception e) {
					logger.warn("getProperties: ", e);
				}
			}
		}
		return p;// 返回Properties对象
	}

	/**
	 * 获取属性定义在文件中取值
	 * 
	 */
	public String getValueFromFile(String fileName, String strKey) {
		Properties p = getProperties(fileName);
		try {
			return p.getProperty(strKey);
		} catch (Exception e) {
			logger.warn("get properties failed. fileName = " + fileName);
		}
		return null;
	}

	/**
	 * 获取属性定义在文件中取值
	 * 
	 */
	public String getValueFromFile(String fileName, String strKey, String defaultValue) {
		String result = null;
		try {
			Properties p = getProperties(fileName);
			result = p.getProperty(strKey);
		} catch (Exception e) {
			logger.warn("get properties failed. fileName = " + fileName);
		}
		if(result == null){
			return result;
		}
		return result;
	}

	/**
	 * 根据Enum获取属性定义
	 * 
	 * @param fileConfig
	 * @return
	 */
	public String getPropertyValue(FileConfigEnum fileConfig) {
		Properties p = getProperties(fileConfig.file());
		try {
			return p.getProperty(fileConfig.name());
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 根据Enum获取属性定义
	 * 
	 * @param fileConfig
	 * @return
	 */
	public String getPropertyValue(FileConfigEnum fileConfig, String defaultValue) {
		String result = null;
		try {
			Properties p = getProperties(fileConfig.file());
			result = p.getProperty(fileConfig.name());
		} catch (Exception e) {
		}
		if (result == null) {
			result = defaultValue;
		}
		return result;
	}

	/**
	 * 写入配置文件
	 * 
	 * @param filePath
	 *            文件路径
	 * @param paraKey
	 *            the para key
	 * @param paraValue
	 *            the para value
	 */
	private void writeProperties(String filePath, String paraKey, String paraValue) {
		if (paraValue == null)
			paraValue = "";
		Properties props = getProperties(filePath);
		try {
			OutputStream ops = new FileOutputStream(filePath);
			props.setProperty(paraKey, paraValue);
			props.store(ops, "set");
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * 写入配置文件
	 * 
	 * @param filePath
	 *            the file path
	 * @param map
	 *            the map
	 */
	public synchronized void writeProperties(String filePath, Map<String, String> map) {
		Properties props = getProperties(filePath);
		try {
			OutputStream ops = new FileOutputStream(filePath);
			for (String key : map.keySet()) {
				String value = map.get(key);
				if (value == null)
					value = "";
				props.setProperty(key, value);
			}
			props.store(ops, "set");

		} catch (IOException e) {
			// e.printStackTrace();
		}
		// 重新初始化
		initProperties(filePath);
	}

	/**
	 * 初始化
	 * 
	 * @param fileName
	 *            文件名字
	 */
	private void initProperties(String fileName) {
		logger.info("reload configuration file " + fileName);
		Properties p = null;
		try {
			InputStream is = null;// 定义输入流is
			try {
				is = new FileInputStream(fileName);// 创建输入流
			} catch (Exception e) {
				logger.warn("initProperties, error message: " + e.getLocalizedMessage());
				if (fileName.startsWith("/")) {
					// 用getResourceAsStream()方法用于定位并打开外部文件。
					is = EnvironmentConfig.class.getResourceAsStream(fileName);
				} else {
					is = EnvironmentConfig.class.getResourceAsStream("/" + fileName);
				}
			}
			p = new Properties();
			p.load(is);// 加载输入流
			register.put(fileName, p);// 将其存放于HashMap
			is.close();// 关闭输入流
		} catch (Exception e) {
			// logger.error("initProperties: ", e);
		}
	}

}
