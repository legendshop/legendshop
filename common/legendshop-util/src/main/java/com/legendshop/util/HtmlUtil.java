/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
/**
 * 
 * Html处理器
 *
 */
public class HtmlUtil {

	/**
	 * 构造器
	 */
	public HtmlUtil() {
		super();
	}

	 /** 从一段html中拿到<img>标签中的图片路径 **/
	public static List<String> getImgsSrc(String htmlText){
		List<String> imgUrls = new ArrayList<String>();
		if(AppUtils.isNotBlank(htmlText)){
			Document  doc = Jsoup.parse(htmlText);
			for(Element elem : doc.select("img")){
				if(StringUtils.isBlank(elem.attr("src"))){
					continue;
				}
				imgUrls.add(elem.attr("src"));
			}
		}
		return imgUrls;
	}

}
