/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.ip;

/**
 * 
 * 本地IP地址
 * 
 */
public class LocalAddress {

	/** The ip. */
	public String ip;

	/** The host name. */
	public String hostName;

	/**
	 * 
	 * 构造函数
	 * 
	 */
	public LocalAddress(String ip, String hostName) {
		this.ip = ip;
		this.hostName = hostName;
	}

	/**
	 * 打印显示
	 */
	public String toString() {
		return new StringBuffer().append("ip:").append(this.ip).append(", hostname:").append(this.hostName).toString();
	}

	/**
	 * 获取Ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * 设置Ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * 获取HostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * 设置HostName
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
}