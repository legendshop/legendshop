/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.constant;

/**
 * 整型Enum基类
 */
public interface IntegerEnum {

	/**
	 * Value.
	 * 
	 * @return the integer
	 */
	public Integer value();
}
