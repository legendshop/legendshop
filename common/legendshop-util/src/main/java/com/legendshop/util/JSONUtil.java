/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

/**
 * JSON工具类
 *  采用fastjson实现
 */
public class JSONUtil {
	
	/**
	 * 从普通的Bean转换为字符串
	 *
	 * @param o the o
	 * @return the json
	 */
	public static String getJson(Object o) {
		return JSON.toJSONString(o);
	}

	/**
	 * 从Java的列表转换为字符串
	 *
	 * @param list the list
	 * @return the json
	 */
	public static String getJson(List<?> list) {
		return JSON.toJSONString(list);
	}

	/**
	 * 从Java对象数组转换为字符串
	 *
	 * @param arry the arry
	 * @return the json
	 */
	public static String getJson(Object[] array) {
		return JSON.toJSONString(array);
	}

	
	/**
	 * 从Map对象转换为字符串.
	 *
	 * @param arry the arry
	 * @return the json
	 */
	public static String getJson(Map map) {
		return JSON.toJSONString(map);
	}
	
	/**
	 * 从json格式的字符串转换为Map对象
	 *
	 * @param text
	 * @return the object
	 */
	@SuppressWarnings("rawtypes")
	public static Map getMap(String text) {
		return (Map)JSON.parseObject(text, new TypeReference<Map>() {});
	}

	/**
	 * 从json格式的字符串转换为某个Bean
	 *
	 * @param <T> the generic type
	 * @param s the s
	 * @param entityClass the entity class
	 * @return the object
	 */
	public static <T> T getObject(String s, Class<T> entityClass) {
		return JSON.parseObject(s, entityClass);
	}

	/**
	 * 从json格式的字符串转换为某类对象的数组
	 *
	 * @param <T> the generic type
	 * @param s the s
	 * @param cls the cls
	 * @return 数组
	 */
	public static <T> List<T> getArray(String s, Class<T> cls) {
		return JSON.parseArray(s, cls);
	}
	
	/**
	 * 由json字符串转为对象
	 * @param s
	 * @return
	 */
	public static Object[] getArray(String s) {
		return JSON.parseArray(s).toArray();
	}
	
}