/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

/**
 *配置文件名称定义
 */
public interface FileConfig {
	
	/**  获取系统配置文件 **/
	public static final String ConfigFile = "config/common.properties";
	
	/**   获取手机系统配置文件 **/
	public static final String AppConfFile = "config/app.properties";
	
	/**   获取微信统配置文件 **/
	public static final String WeiXinConfigFile = "config/wechat.properties";

	/**   获取公用配置相关配置文件 **/
	public static final String GlobalFile = "config/global.properties";

	/**  数据库连接文件 **/
	public static final String JdbcFile = "config/jdbc.properties";
	

}
