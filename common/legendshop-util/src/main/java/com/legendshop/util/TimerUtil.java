/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 时间工具类
 */
public class TimerUtil {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(TimerUtil.class);

	/** The time type. */
	private static String timeType = "yyyy-MM-dd";

	/** The TIM e_ min. */
	public static String TIME_MIN = "MIN";

	/** The TIM e_ hour. */
	public static String TIME_HOUR = "HOUR";

	/** The TIM e_ day. */
	public static String TIME_DAY = "DAY";

	/** The TIM e_ month. */
	public static String TIME_MONTH = "MONTH";

	/** The TIM e_ year. */
	public static String TIME_YEAR = "YEAR";

	/** The MI d_ dat a_ format. */
	public static String MID_DATA_FORMAT;

	/** The calendar. */
	public static Calendar calendar = new GregorianCalendar();

	/** The date format. */
	public static DateFormat dateFormat;

	/** Java日期格式yyyy:MM:dd HH:mm:ss. */
	public static String JAVA_DATE_FORMAT = "yyyy:MM:dd HH:mm:ss";

	/** ORACLE日期格式YYYY:MM:DD HH24:mi:ss. */
	public static String ORACLE_DATE_FORMAT = "YYYY:MM:DD HH24:mi:ss";

	/** MSSQLSERVER日期格式yyyy-MM-dd HH:mm:ss. */
	public static String MSSQLSERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	static {
		MID_DATA_FORMAT = "yyyy-MM-dd";
		dateFormat = new SimpleDateFormat(MID_DATA_FORMAT);
	}

	/**
	 * 构造函数
	 */
	public TimerUtil() {
	}

	/**
	 * 构造函数
	 * 
	 */
	public TimerUtil(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * 得到现有 String 型的日期格式.
	 * 
	 * @return the str current date
	 */
	public String getStrCurrentDate() {
		String strTime = null;
		try {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(timeType);
			strTime = simpledateformat.format(new Date());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return strTime;
	}

	/**
	 * 得到长时间格式的long.
	 * 
	 */
	public long getTimeToLong(String time) {
		Date date = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(timeType);
			ParsePosition pos = new ParsePosition(0);
			date = formatter.parse(time, pos);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return date.getTime();
	}

	/**
	 * 得到现有的时间.
	 * 
	 * @return the current date
	 */
	public static Date getCurrentDate() {
		return new Date();
	}

	/**
	 * 得到yyyy-MM-dd格式的日期.
	 * 
	 * @return the now date short
	 */
	public static Date getNowDateShort() {
		Date date = new Date();
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
		String s = simpledateformat.format(date);
		ParsePosition parseposition = new ParsePosition(0);
		Date date1 = simpledateformat.parse(s, parseposition);
		return date1;
	}

	/**
	 * 得到现在yyyy-MM-dd HH:mm:ss格式的日期字符串.
	 * 
	 */
	public static String getStrDate() {
		Date date = new Date();
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = simpledateformat.format(date);
		return s;
	}

	/**
	 * 得到现在yyyy-MM-dd格式的日期字符串
	 * 
	 * @return the str date short
	 */
	public static String getStrDateShort() {
		Date date = new Date();
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
		String s = simpledateformat.format(date);
		return s;
	}

	/**
	 * 根据String得到Date型现在的时间.
	 * 
	 * @param s
	 *            the s
	 * @return the date
	 */
	public static Date strToDate(String s) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition parseposition = new ParsePosition(0);
		Date date = simpledateformat.parse(s, parseposition);
		return date;
	}

	/**
	 * 根据Date得到yyyy-MM-dd HH:mm:ss格式的现在的时间.
	 */
	public static String dateToStr(Date date) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = simpledateformat.format(date);
		return s;
	}

	/**
	 * 根据Date得到yyyy-MM-dd格式的现在的时间.
	 * 
	 */
	public static String dateToStrShort(Date date) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
		String s = simpledateformat.format(date);
		return s;
	}

	/**
	 * 根据Date得到yyyy-MM-dd格式的现在时间.
	 * 
	 * @param s
	 *            the s
	 * @return the date
	 */
	public static Date strToDateShort(String s) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition parseposition = new ParsePosition(0);
		Date date = simpledateformat.parse(s, parseposition);
		return date;
	}

	/**
	 * 获取最后的时间.
	 * 
	 */
	public static Date getLastDate(long l) {
		Date date = new Date();
		long l1 = date.getTime() - 0x74bad00L * l;
		Date date1 = new Date(l1);
		return date1;
	}

	/**
	 * 计算日期.
	 * 
	 */
	public static Date getDate(Date date, Integer integer, String s) {
		if (integer != null)
			return getDate(date, integer.intValue(), s);
		else
			return date;
	}

	/**
	 * 计算日期.
	 * 
	 */
	public static Date getDate(Date date, int i, String s) {
		calendar.setTime(date);
		if (s.equalsIgnoreCase(TIME_MIN))
			calendar.add(12, i);
		else if (s.equalsIgnoreCase(TIME_HOUR))
			calendar.add(11, i);
		else if (s.equalsIgnoreCase(TIME_DAY))
			calendar.add(5, i);
		else if (s.equalsIgnoreCase(TIME_MONTH))
			calendar.add(2, i);
		else if (s.equalsIgnoreCase(TIME_YEAR))
			calendar.add(1, i);
		return calendar.getTime();
	}

	/**
	 * 得到Oracle的日期格式.
	 * 
	 */
	public static String getOracleDateStr(Date date) {
		if (date == null) {
			return null;
		} else {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(ORACLE_DATE_FORMAT);
			String s = simpledateformat.format(date);
			return s;
		}
	}

	/**
	 * 得到MSSQLSERVER的日期格式.
	 * 
	 */
	public static String getSqlServerDateStr(Date date) {
		if (date == null) {
			return null;
		} else {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(MSSQLSERVER_DATE_FORMAT);
			String s = simpledateformat.format(date);
			return s;
		}
	}

}
