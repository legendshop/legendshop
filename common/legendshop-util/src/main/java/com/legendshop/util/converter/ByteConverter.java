/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.converter;

import java.io.ByteArrayOutputStream;

/**
 * 
 * 字节码转换器
 */
public class ByteConverter {
	

	/** 十六进制的字符串. */
	private static String HEX_STRING = "0123456789ABCDEF";

	/**
	 * 将字符串由十进制改为十六进制.
	 * @return 十六进制的字符
	 */
	public static String stringToHexString(String strPart) {
		String hexString = "";
		for (int i = 0; i < strPart.length(); i++) {
			int ch = (int) strPart.charAt(i);
			String strHex = Integer.toHexString(ch);
			hexString = hexString + strHex;
		}
		return hexString;
	}


	/**
	 * 加密
	 */
	public static String encode(String str) {
		// 根据默认编码获取字节数组
		byte[] bytes = str.getBytes();
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		// 将字节数组中每个字节拆解成2位16进制整数
		for (int i = 0; i < bytes.length; i++) {
			sb.append(HEX_STRING.charAt((bytes[i] & 0xf0) >> 4));
			sb.append(HEX_STRING.charAt((bytes[i] & 0x0f) >> 0));
		}
		return sb.toString();
	}

	/**
	 * 解密
	 * 
	 */
	public static String decode(String bytes) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length() / 2);
		// 将每2位16进制整数组装成一个字节
		for (int i = 0; i < bytes.length(); i += 2)
			baos.write((HEX_STRING.indexOf(bytes.charAt(i)) << 4 | HEX_STRING.indexOf(bytes.charAt(i + 1))));
		return new String(baos.toByteArray());
	}

	/**
	 * 按位置加密
	 * 
	 */
	public static String encode(String str, int pos) {
		// 根据默认编码获取字节数组
		byte[] bytes = str.getBytes();
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		// 将字节数组中每个字节拆解成2位16进制整数
		for (int i = 0; i < bytes.length; i++) {
			sb.append(HEX_STRING.charAt((bytes[i] & 0xf0) >> pos));
			sb.append(HEX_STRING.charAt((bytes[i] & 0x0f) >> 0));
		}
		return sb.toString();
	}

	/**
	 * 按位置解密
	 */
	public static String decode(String bytes, int pos) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length() / 2);
		// 将每2位16进制整数组装成一个字节
		for (int i = 0; i < bytes.length(); i += 2)
			baos.write((HEX_STRING.indexOf(bytes.charAt(i)) << pos | HEX_STRING.indexOf(bytes.charAt(i + 1))));
		return new String(baos.toByteArray());
	}

	/**
	 * Unite bytes.
	 * 
	 * @param src0
	 *            the src0
	 * @param src1
	 *            the src1
	 * @return the byte
	 */
	private static byte uniteBytes(byte src0, byte src1) {
		byte _b0 = Byte.decode("0x" + new String(new byte[] { src0 })).byteValue();
		_b0 = (byte) (_b0 << 4);
		byte _b1 = Byte.decode("0x" + new String(new byte[] { src1 })).byteValue();
		byte ret = (byte) (_b0 | _b1);
		return ret;
	}

	/**
	 * 十六进制字符转化为Bytes.
	 * 
	 */
	public static byte[] HexString2Bytes(String src) {
		byte[] ret = new byte[6];
		byte[] tmp = src.getBytes();
		for (int i = 0; i < 6; ++i) {
			ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
		}
		return ret;
	}

	/**
	 *  十六进制字符转化为Bytes.
	 * 
	 */
	public static byte[] hexStringToBytes(String hexString) throws IllegalArgumentException {
		int len = hexString.length();
		if (len % 2 > 0) {
			hexString = "0" + hexString;
			len++;
		}

		byte[] bytes = new byte[len / 2];

		char[] chars = hexString.toUpperCase().toCharArray();
		for (int i = 0, j = 0; i < bytes.length; i++) {
			int indexHigh = HEX_STRING.indexOf(chars[j++]);
			if (indexHigh < 0) {
				throw new IllegalArgumentException(chars[j - 1] + " is not a hex char");
			}
			int indexLow = HEX_STRING.indexOf(chars[j++]);
			if (indexLow < 0) {
				throw new IllegalArgumentException(chars[j - 1] + " is not a hex char");
			}
			bytes[i] = (byte) ((indexHigh << 4) | indexLow);
		}

		return bytes;
	}

}
