package com.legendshop.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 网络链接工具类
 * 
 */
public class HttpUtil {

	/** The log. */
	private final static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	public final static String POST = "POST";

	public final static String GET = "GET";

	public final static String PUT = "PUT";

	public final static String DELETE = "DELETE";

	public final static String CHARSET = "UTF-8";

	/**
	 * 发送https请求
	 * 
	 * @param requestUrl
	 * @param requestMethod
	 * @return 结果
	 */
	public static String httpsRequest(String requestUrl, String requestMethod) {
		return httpsRequest(requestUrl, requestMethod, null);
	}

	/**
	 * 发送https请求
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return 返回微信服务器响应的信息
	 */
	public static String httpsRequest(String requestUrl, String requestMethod, String outputStr) {
		logger.debug("*********requestUrl is {}  AND requestMethod IS {}  AND  outputStr {} END ********", requestUrl, requestMethod, outputStr);
		StringBuffer buffer = new StringBuffer();
		try {
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new SecureRandom());

			SSLSocketFactory ssf = sslContext.getSocketFactory();

			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			httpUrlConn.setRequestMethod(requestMethod);
			httpUrlConn
					.setRequestProperty(
							"User-Agent",
							"Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			if (GET.equalsIgnoreCase(requestMethod)) {
				httpUrlConn.connect();
			}

			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				outputStream.write(outputStr.getBytes(CHARSET));
				outputStream.close();
			}

			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, CHARSET);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();

			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			return buffer.toString();
		} catch (ConnectException ce) {
			logger.error("ConnectException happened ", ce);
		} catch (Exception e) {
			logger.error("Exception happened ", e.getMessage());
		}
		logger.error("Can not get content from {} ", requestUrl);
		return null;
	}

	/**
	 * 发送Http请求 参数为数组
	 * */
	public static String httpRequest(String urlAddress, String requestMethod) {
		return httpRequest(urlAddress, requestMethod, null, null, "application/x-www-form-urlencoded;charset=" + CHARSET);
	}
	
	public static String httpRequest(String urlAddress, String requestMethod, Map<String, String> headers, Map<String, String> params) {
		return httpRequest(urlAddress, requestMethod, headers, params, "application/x-www-form-urlencoded;charset=" + CHARSET);
	}

	/**
	 * 发送请求
	 * 
	 * @param urlAddress
	 * @param requestMethod
	 * @param headers
	 * @param params
	 * @return
	 */
	public static String httpRequest(String urlAddress, String requestMethod, Map<String, String> headers, Map<String, String> params, String contentType) {
		StringBuffer paramsTemp = new StringBuffer();
		if (params != null && !params.isEmpty()) {
			for (String paramKey : params.keySet()) {
				String paramValue = params.get(paramKey);
				if(paramValue != null && paramValue.length() > 0){
					try {
						paramValue = URLEncoder.encode(params.get(paramKey), CHARSET);
					} catch (UnsupportedEncodingException e) {
						logger.warn(urlAddress, e);
					}
				}
				paramsTemp.append("&").append(paramKey).append("=").append(paramValue);
			}
			return request(urlAddress, requestMethod, headers, paramsTemp.substring(1).toString(), contentType);//去掉第一个&
		}else{
			return request(urlAddress, requestMethod, headers,null, contentType);
		}
		
		
	}
	
	/**
	 * 发送请求
	 * 
	 * @param urlAddress
	 * @param requestMethod
	 * @param headers
	 * @param params 采用Json方式传递
	 * @return
	 */
	public static String request(String urlAddress, String requestMethod, Map<String, String> headers, String params, String contentType) {
		logger.debug("*********requestUrl is {}  AND requestMethod IS {}  END ********", urlAddress, requestMethod);
		URL url = null;
		HttpURLConnection con = null;
		BufferedReader in = null;
		try {
			url = new URL(urlAddress);
			con = (HttpURLConnection) url.openConnection();
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setConnectTimeout(30000); // 设置连接超时为30秒
			con.setRequestMethod(requestMethod);

			// 一定要设置 Content-Type 要不然服务端接收不到参数
			con.setRequestProperty("Content-Type",contentType);
			con.setRequestProperty("Accept-Charset", CHARSET);
			con.setRequestProperty(
					"User-Agent",
					"Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			if (headers != null && !headers.isEmpty()) {// 设置头部
				for (String headerKey : headers.keySet()) {
					con.setRequestProperty(headerKey, headers.get(headerKey));
				}
			}

			if (params != null && params.length() > 0) {
					DataOutputStream out = new DataOutputStream(con.getOutputStream());
					out.writeBytes(params);
					out.flush();
					out.close();
			}

			int returnCode = con.getResponseCode();
			if (returnCode == 200) {
				StringBuffer result = new StringBuffer();
				in = new BufferedReader(new InputStreamReader(con.getInputStream(), CHARSET));
				while (true) {
					String line = in.readLine();
					if (line == null) {
						break;
					} else {
						result.append(line);
					}
				}
				return result.toString();
			} else {
				return "returnCode=" + returnCode;
			} 
		} catch (ConnectException ce) {
			logger.warn(urlAddress + " connection timed out.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (con != null) {
					con.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.error("Can not get content from {} ", urlAddress);
		return null;
	}

	/**
	 * 发送Http请求 把paramMap转化为数组
	 * */
	public static String httpPost(String urlAddress, Map<String, String> params) {
		return httpRequest(urlAddress, POST, null, params,"application/x-www-form-urlencoded;charset=" + CHARSET);
	}

	/**
	 * 发送Http请求 把paramMap转化为数组
	 * */
	public static String httpPost(String urlAddress, Map<String, String> headers, Map<String, String> params) {
		return httpRequest(urlAddress, POST, headers, params, "application/x-www-form-urlencoded;charset=" + CHARSET);
	}

	public static String httpGet(String urlAddress) {
		URL url = null;
		HttpURLConnection con = null;
		BufferedReader in = null;
		try {
			url = new URL(urlAddress);
			con = (HttpURLConnection) url.openConnection();
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setConnectTimeout(30000); // 设置连接超时为30秒
			con.setReadTimeout(30000);//设置读取超时时间 
			con.setRequestMethod(GET);
			con.setRequestProperty(
					"User-Agent",
					"Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			con.setRequestProperty("Charset", CHARSET);
			int returnCode = con.getResponseCode();
			if (returnCode == 200) {
				StringBuffer result = new StringBuffer();
				in = new BufferedReader(new InputStreamReader(con.getInputStream(), CHARSET));
				while (true) {
					String line = in.readLine();
					if (line == null) {
						break;
					} else {
						result.append(line);
					}
				}
				return result.toString();
			} else {
				return "returnCode=" + returnCode;
			} 

		} catch (SocketTimeoutException e) {
			logger.warn(urlAddress + " connection timed out.");
		} catch (IOException e) {
			logger.warn(urlAddress + " IOException.", e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (con != null) {
					con.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.error("Can not get content from {} ", urlAddress);
		return null;
	}

	/**
	 * HttpUrlConnection支持Header,Body传值，支持Multipart上传文件
	 * 
	 * @param actionUrl
	 *            动作
	 * @param headers
	 *            头部信息
	 * @param params
	 *            参数
	 * @param files
	 *            上传文件
	 * @return
	 * @throws IOException
	 */
	public static String httpPost(String actionUrl, Map<String, String> headers, Map<String, String> params, Map<String, File> files) throws IOException {

		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		URL uri = new URL(actionUrl);

		HttpURLConnection conn = null;
		DataOutputStream outStream = null;
		BufferedReader bufferedReader = null;
		try {
			conn = (HttpURLConnection) uri.openConnection();
			conn.setConnectTimeout(30000); // 设置连接超时为30秒
			conn.setReadTimeout(30000); // 缓存的最长时间
			conn.setDoInput(true);// 允许输入
			conn.setDoOutput(true);// 允许输出
			conn.setUseCaches(false); // 不允许使用缓存
			conn.setRequestMethod(POST);
			conn.setRequestProperty("connection", "keep-alive");
			conn.setRequestProperty("Charsert", CHARSET);
			conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA + ";boundary=" + BOUNDARY);
			if (headers != null) {
				for (String key : headers.keySet()) {
					conn.setRequestProperty(key, headers.get(key));
				}
			}
			StringBuilder sb = new StringBuilder();
			if (params != null) {
				// 首先组拼文本类型的参数
				for (Map.Entry<String, String> entry : params.entrySet()) {
					sb.append(PREFIX);
					sb.append(BOUNDARY);
					sb.append(LINEND);
					sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEND);
					sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
					sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
					sb.append(LINEND);
					sb.append(entry.getValue());
					sb.append(LINEND);
				}

			}

			outStream = new DataOutputStream(conn.getOutputStream());
			if (sb.length() > 0) {
				outStream.write(sb.toString().getBytes());
			}
			// 发送文件数据
			if (files != null)
				for (Map.Entry<String, File> file : files.entrySet()) {
					StringBuilder sb1 = new StringBuilder();
					sb1.append(PREFIX);
					sb1.append(BOUNDARY);
					sb1.append(LINEND);
					sb1.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getKey() + "\"" + LINEND);
					sb1.append("Content-Type: application/octet-stream; charset=" + CHARSET + LINEND);
					sb1.append(LINEND);
					outStream.write(sb1.toString().getBytes());

					InputStream is = new FileInputStream(file.getValue());
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = is.read(buffer)) != -1) {
						outStream.write(buffer, 0, len);
					}
					is.close();
					outStream.write(LINEND.getBytes());
				}

			// 请求结束标志
			byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
			outStream.write(end_data);
			outStream.flush();
			logger.info("HttpUtil", "conn.getContentLength():" + conn.getContentLength());

			// 得到响应码
			int res = conn.getResponseCode();
			InputStream in = conn.getInputStream();
			if (res == 200) {
				 bufferedReader = new BufferedReader(new InputStreamReader(in, CHARSET));
				StringBuffer buffer = new StringBuffer();
				String line = "";
				while ((line = bufferedReader.readLine()) != null) {
					buffer.append(line);
				}
				return buffer.toString();
			}else {
				return "returnCode=" + res;
			} 
		} catch (Exception e) {
			logger.error(actionUrl + " Exception", e);
		} finally {
			if(bufferedReader != null){
				bufferedReader.close();
			}
			
			if (outStream != null) {
				outStream.close();
			}
			
			if (conn != null) {
				conn.disconnect();
			}
		}
		logger.error("Can not get content from {} ", actionUrl);
		return null;

	}
}
