/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;


/**
 * 系统的工具类，记录系统的实际安装路径和上下文.
 */
public class SystemUtil {
	/** 系统的安装实际路径 */
	private static String systemRealPath;
	
	/** 系统上下文. */
	private static String contextPath;
	

	/**
	 * 获取系统的安装实际路径.
	 * 
	 * @return 系统的安装实际路径
	 */
	public static String getSystemRealPath() {
		return systemRealPath;
	}

	/**
	 * 设置系统的安装实际路径
	 * 
	 * @param systemRealPath
	 *            系统的安装实际路径
	 */
	public static void setSystemRealPath(String systemRealPath) {
		SystemUtil.systemRealPath = systemRealPath;
	}

	/**
	 *  获取系统上下文..
	 *
	 * @return 系统上下文.
	 */
	public static String getContextPath() {
		if(contextPath==null){
			return "";
		}
		return contextPath;
	}

	/**
	 * 设置系统上下文.
	 *
	 */
	public static void setContextPath(String path) {
		SystemUtil.contextPath = path;
	}


}
