/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.constant;

/**
 * 长整型Enum基类
 */
public interface LongEnum {

	/**
	 * Value.
	 * 
	 * @return the long
	 */
	public Long value();
}
