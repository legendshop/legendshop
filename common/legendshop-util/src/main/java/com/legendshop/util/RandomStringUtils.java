/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.util.Random;

import com.legendshop.util.des.DES2;

/**
 * 
 * 随机数工具类
 */
public class RandomStringUtils {

	/** The Constant RANDOM. */
	private static final Random RANDOM = new Random();

	/**
	 * Instantiates a new random string utils.
	 */
	public RandomStringUtils() {
	}

	/**
	 * 随机获取count位字符.
	 * 
	 */
	public static String random(int count) {
		return random(count, false, false);
	}

	/**
	 * 随机获取count位Ascii字符
	 * 
	 */
	public static String randomAscii(int count) {
		return random(count, 32, 127, false, false);
	}

	/**
	 * 随机获取count位字母
	 * 
	 */
	public static String randomAlphabetic(int count) {
		return random(count, true, false);
	}

	/**
	 * 随机获取count位数字
	 * 
	 */
	public static String randomAlphanumeric(int count) {
		return random(count, true, true);
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String randomNumeric(int count) {
		return randomNumeric(count, 4);
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String randomNumeric(int count, int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int val = RANDOM.nextInt(10);
			sb.append(String.valueOf(val));
		}
		return sb.toString();
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String randomLetter(int count) {
		DES2 des = new DES2();
		String encryptorString = random(count, false, true);
		String desStr = "1234";
		try {
			desStr = des.byteToString(des.createEncryptor(encryptorString));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return desStr;
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String random(int count, boolean letters, boolean numbers) {
		return random(count, 0, 0, letters, numbers);
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String random(int count, int start, int end, boolean letters, boolean numbers) {
		return random(count, start, end, letters, numbers, null);
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String random(int count, int start, int end, boolean letters, boolean numbers, char set[]) {
		if (start == 0 && end == 0) {
			end = 122;
			start = 32;
			if (!letters && !numbers) {
				start = 0;
				end = 0x7fffffff;
			}
		}
		StringBuffer buffer = new StringBuffer();
		int gap = end - start;
		while (count-- != 0) {
			char ch;
			if (set == null) {
				ch = (char) (RANDOM.nextInt(gap) + start);
			} else {
				ch = set[RANDOM.nextInt(gap) + start];
			}
			if (letters && numbers && Character.isLetterOrDigit(ch) || letters && Character.isLetter(ch) || numbers
					&& Character.isDigit(ch) || !letters && !numbers) {
				buffer.append(ch);
			} else {
				count++;
			}
		}
		return buffer.toString();
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String random(int count, String set) {
		return random(count, set.toCharArray());
	}

	/**
	 * 获取随机数.
	 * 
	 */
	public static String random(int count, char set[]) {
		return random(count, 0, set.length - 1, false, false, set);
	}


}
