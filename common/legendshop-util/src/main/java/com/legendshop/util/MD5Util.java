/*
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 */

/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * 
 * LegendShop MD5加密类
 * 
 */
public class MD5Util {

	/** 为了兼容以前的做法采用大写，注意alipay是用小写的. */
	private static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	/**
	 * Md5加密, 注意不能直接用于加密用户名密码，如果加密用户名密码请用加盐的方法
	 * 
	 * @param source
	 *            将要加密的字符串
	 * @return Md5后的加密码
	 */
	public static String toMD5(String source) {
		MessageDigest msgDigest = null;
		try {
			msgDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("System doesn't support MD5 algorithm.");
		}
		try {
			msgDigest.update(source.getBytes("utf-8")); // 注意改接口是按照指定编码形式签名
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("System doesn't support your  EncodingException.");
		}
		byte[] bytes = msgDigest.digest();
		String md5Str = new String(encodeHex(bytes));
		return md5Str;
	}

	/**
	 * 创建md5摘要,规则是:按参数名称a-z排序,遇到空值的参数不参加签名。
	 */
	public static String createSign(SortedMap packageParams) {
		StringBuffer sb = new StringBuffer();
		Set es = packageParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = String.valueOf(entry.getKey());
			String v = String.valueOf(entry.getValue());
			sb.append(k + "=" + v + "&");
		}
		String sign = toMD5(sb.toString());
		return sign;
	}


	/**
	 * For IM 使用
	 * @param parameters
	 * @param token
	 * @return
	 */
	public static String createSign(SortedMap<String, String> parameters, String token) {
		StringBuffer sb = new StringBuffer();
		Set es = parameters.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = String.valueOf(entry.getKey());
			String v = String.valueOf(entry.getValue());
			sb.append(k + "=" + v + "&");
		}
		sb.append("token=" + token);
		String sign = toMD5(sb.toString());
		return sign;
	}

	/**
	 * 将byte数组转化为字符数组
	 * 
	 * @param data
	 *            需要转化的数据
	 * @return 字符数组
	 */
	private static char[] encodeHex(byte[] data) {
		int l = data.length;
		char[] out = new char[l << 1];
		// two characters form the hex value.
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
			out[j++] = DIGITS[0x0F & data[i]];
		}
		return out;
	}
	

}
