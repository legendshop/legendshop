package com.legendshop.util;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

//import org.apache.catalina.loader.WebappClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  ClassLoader工具类
 */
public final class ClassLoaderUtil {
	
	/** The log. */
	protected static final Logger log = LoggerFactory.getLogger(ClassLoaderUtil.class);
	
	private static ClassLoaderUtil  instance = null;

	/** URLClassLoader的addURL方法. */
	private  Method addURL = initAddMethod();

	/** The system. */
	private  URLClassLoader system = (URLClassLoader) ClassLoader.getSystemClassLoader();
	
	
	private ClassLoaderUtil(){
		
	}
	
	public static ClassLoaderUtil getInstance(){
		if(instance == null){
			synchronized (ClassLoaderUtil.class) {
				if(instance == null ){
					instance = new ClassLoaderUtil();
				}
			}
		}
		return instance;
	}

	/**
	 * 初始化方法.
	 * 
	 * @return the method
	 */
	private  final Method initAddMethod() {
		try {
			Method add = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
			add.setAccessible(true);
			return add;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public  final void loadClassPath(URL[] urls) {
		if(urls ==null){
			log.debug("urls is null");
			return;
		}
		
		try {
			for (URL url : urls) {
				addURL.invoke(system, new Object[] { url });
				log.debug("加载JAR包：{}", url.getFile());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 增加Jar包
	 * @param urls
	 * @throws MalformedURLException
	 */
	synchronized public  void addJar(URL[] urls) throws MalformedURLException {
//		WebappClassLoader loader = (WebappClassLoader) getClass().getClassLoader();
//		for (URL url : urls) {
//			loader.addRepository(url.toString());
//		}
	}
}