/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.ip;

/**
 * 
 * 一条IP范围记录，不仅包括国家和区域，也包括起始IP和结束IP *
 * 
 */
public class IPEntry {

	/** 开始的Ip地址. */
	public String beginIp;

	/** 结束的Ip地址. */
	public String endIp;

	/** 国家. */
	public String country;

	/** 地区. */
	public String area;

	/**
	 * 构造函数.
	 */
	public IPEntry() {
		beginIp = endIp = country = area = "";
	}

	/**
	 * 打印内容
	 */
	@Override
	public String toString() {
		return this.area + "  " + this.country + "IP范围:" + this.beginIp + "-" + this.endIp;
	}
}