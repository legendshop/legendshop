/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * java Method工具类.
 */
public class MethodUtils {
	
    /** The Constant SUPPORTED_SQL_OBJECTS. */
    static final Set<Class<?>> SUPPORTED_SQL_OBJECTS = new HashSet<Class<?>>();

    static {
        Class<?>[] classes = {
                boolean.class, Boolean.class,
                short.class, Short.class,
                int.class, Integer.class,
                long.class, Long.class,
                float.class, Float.class,
                double.class, Double.class,
                String.class,
                Date.class,
                Timestamp.class
        };
        SUPPORTED_SQL_OBJECTS.addAll(Arrays.asList(classes));
    }
    

	/**
	 * Find public getters.
	 *
	 * @param clazz
	 *            the clazz
	 * @return the map
	 */
	public static Map<String, Method> findPublicGetters(Class<?> clazz) {
		Map<String, Method> map = new HashMap<String, Method>();
		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			if (method.isBridge())
				continue;
			if (Modifier.isStatic(method.getModifiers()))
				continue;
			if (method.getParameterTypes().length != 0)
				continue;
			if (method.getName().equals("getClass"))
				continue;
			Class<?> returnType = method.getReturnType();
			if (void.class.equals(returnType))
				continue;
			if ((returnType.equals(boolean.class) || returnType.equals(Boolean.class)) && method.getName().startsWith("is") && method.getName().length() > 2) {
				map.put(getGetterName(method), method);
				continue;
			}
			if (!method.getName().startsWith("get"))
				continue;
			if (method.getName().length() < 4)
				continue;
			map.put(getGetterName(method), method);
		}
		return map;
	}

	/**
	 * Find public setters.
	 *
	 * @param clazz
	 *            the clazz
	 * @return the map
	 */
	public static Map<String, Method> findPublicSetters(Class<?> clazz) {
		Map<String, Method> map = new HashMap<String, Method>();
		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			if (Modifier.isStatic(method.getModifiers()))
				continue;
			if (!void.class.equals(method.getReturnType()))
				continue;
			if (method.getParameterTypes().length != 1)
				continue;
			if (!method.getName().startsWith("set"))
				continue;
			if (method.getName().length() < 4)
				continue;
			map.put(getSetterName(method), method);
		}
		return map;
	}

	/**
	 * Gets the getter name.
	 *
	 * @param getter
	 *            the getter
	 * @return the getter name
	 */
	public static String getGetterName(Method getter) {
		String name = getter.getName();
		if (name.startsWith("is"))
			name = name.substring(2);
		else
			name = name.substring(3);
		return Character.toLowerCase(name.charAt(0)) + name.substring(1);
	}

	/**
	 * 生成getter setter方法.
	 *
	 * @param setter
	 *            the setter
	 * @return the setter name
	 */
	public static String getSetterName(Method setter) {
		String name = setter.getName().substring(3);
		return Character.toLowerCase(name.charAt(0)) + name.substring(1);
	}

	/**
	 * 生成getter setter代码
	 * @param from
	 * @param to
	 * @return
	 */
	public static String generateCode(Class<?> from, Class<?> to) {
		StringBuffer sb = new StringBuffer();
		Map<String, Method> fromGetters = findPublicGetters(from);
		Map<String, Method> toGetters = findPublicGetters(to);

		String fromClassName = from.getSimpleName();
		
		String fromEntityName = lowerFirstChar(from.getSimpleName());
		
		String toClassName = to.getSimpleName();
		
		String toEntityName = lowerFirstChar(to.getSimpleName());
		
		
		
//		System.out.println("from.getName() = " + lowerFirstChar(from.getSimpleName()));
//
//		for (Method method : fromGetters.values()) {
//			System.out.println("from method Name = " + method.getName() + ", from method Modifiers = " + method.getModifiers());
//		}
//		
//		System.out.println(" ------------------ ");
//		
//		for (Method method : toGetters.values()) {
//			System.out.println("to method Name = " + method.getName() + ", to method Modifiers = " + method.getModifiers());
//		}

		
		sb.append("\nGenerate copy method: \n\n");
		
		sb.append("public ").append(toClassName).append(" convertTo").append(toClassName).append("(").append(fromClassName).append(" ").append(fromEntityName).append("){\n");
		String whiteSpace = "  ";
		sb.append(whiteSpace).append(toClassName).append(" ").append(toEntityName).append(" = new ").append(toClassName).append("();\n");
		
		for (String field : toGetters.keySet()) {
			if(fromGetters.containsKey(field)) {
				Method fromGetter = fromGetters.get(field);
				Method toGetter = toGetters.get(field);
				if(toGetter.getReturnType().equals(fromGetter.getReturnType())) {//判断返回类型是否一致
					sb.append(whiteSpace).append(toEntityName).append(".set").append(upperFirstChar(field)).append("(").append(fromEntityName).append(".get").append(upperFirstChar(field)).append("());\n");
				}
			}
		}
		sb.append(whiteSpace).append("return ").append(toEntityName).append(";\n");
		sb.append("}\n");
		return sb.toString();
	}

	/**
	 * 首字母小写
	 * 
	 * @return
	 */
	private static String lowerFirstChar(String name) {
		if (Character.isLowerCase(name.charAt(0))) {
			return name;
		}else {
			return (new StringBuilder()).append(Character.toLowerCase(name.charAt(0))).append(name.substring(1)).toString();
		}
	}
	
	/**
	 * 首字母大写
	 * 
	 * @return
	 */
	private static String upperFirstChar(String name) {
		if (Character.isUpperCase(name.charAt(0))) {
			return name;
		}else {
			return (new StringBuilder()).append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
		}
	}
	
	
}
