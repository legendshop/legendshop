/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * 日期转化器
 */
public class DateConverter implements Converter {

	/**
	 *根据类型转化为Date或者String
	 */
	public Object convert(Class type, Object value) {
		if (value == null) {
			return null;
		} else if (type == Date.class) {
			return convertToDate(type, value);
		} else if (type == String.class) {
			return convertToString(type, value);
		}

		throw new ConversionException("Could not convert " + value.getClass().getName() + " to " + type.getName());
	}

	/**
	 * Convert to date.
	 * 
	 * @param type
	 *            the type
	 * @param value
	 *            the value
	 * @return the object
	 */
	protected Object convertToDate(Class type, Object value) {
		DateFormat df = new SimpleDateFormat(ConvertDateUtil.getDatePattern());
		if (value instanceof String) {
			try {
				if (StringUtils.isEmpty(value.toString())) {
					return null;
				}

				return df.parse((String) value);
			} catch (Exception pe) {
				throw new ConversionException("Error converting String to Date");
			}
		}

		throw new ConversionException("Could not convert " + value.getClass().getName() + " to " + type.getName());
	}

	/**
	 * Convert to string.
	 * 
	 * @param type
	 *            the type
	 * @param value
	 *            the value
	 * @return the object
	 */
	protected Object convertToString(Class type, Object value) {
		DateFormat df = new SimpleDateFormat(ConvertDateUtil.getDatePattern());
		if (value instanceof Date) {
			try {
				return df.format(value);
			} catch (Exception e) {
				throw new ConversionException("Error converting Date to String");
			}
		}

		return value.toString();
	}
}
