/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.des;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 */
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.legendshop.util.converter.ByteConverter;

/**
 * DES加密介绍 DES是一种对称加密算法，所谓对称加密算法即：加密和解密使用相同密钥的算法。DES加密算法出自IBM的研究，
 * 后来被美国政府正式采用，之后开始广泛流传，但是近些年使用越来越少，因为DES使用56位密钥，以现代计算能力，
 * 24小时内即可被破解。虽然如此，在某些简单应用中，我们还是可以使用DES加密算法，本文简单讲解DES的JAVA实现 。
 * 注意：DES加密和解密过程中，密钥长度都必须是8的倍数
 */
public class DES2 {

	/** The Algorithm. */
	private final String Algorithm = "DES";

	/** The deskey. */
	private SecretKey deskey;

	/** The cipher. */
	private Cipher cipher;

	/** The key string. */
	private String keyString = "12345678";

	/**
	 * 初始化 DES 实例.
	 * 
	 * @param keyString
	 *            the key string
	 */
	public DES2(String keyString) {
		if (keyString != null) {
			init(keyString);
		} else {
			init(keyString);
		}
	}

	/**
	 * Instantiates a new dE s2.
	 */
	public DES2() {
		init(keyString);
	}

	/**
	 * DES加密的，文件中共有两个方法,加密、解密.
	 * 
	 * @param keyString
	 *            the key string
	 */
	public void init(String keyString) {
		try {
			byte key[] = keyString.getBytes();
			deskey = new SecretKeySpec(key, "DES");
			cipher = Cipher.getInstance(Algorithm);
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		} catch (NoSuchPaddingException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 对 byte[] 进行加密.
	 * 
	 * @param datasource
	 *            要加密的数据
	 * @return 返回加密后的 byte 数组
	 */
	public byte[] createEncryptor(byte[] datasource) {
		 byte[] encryptorData = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, deskey);
			encryptorData = cipher.doFinal(datasource);
		} catch (java.security.InvalidKeyException ex) {
			ex.printStackTrace();
		} catch (javax.crypto.BadPaddingException ex) {
			ex.printStackTrace();
		} catch (javax.crypto.IllegalBlockSizeException ex) {
			ex.printStackTrace();
		}
		return encryptorData;
	}

	/**
	 * 将字符串加密.
	 * 
	 * @param datasource
	 *            the datasource
	 * @return the byte[]
	 */
	public byte[] createEncryptor(String datasource) {
		return createEncryptor(datasource.getBytes());
	}

	/**
	 * 对 datasource 数组进行解密.
	 * 
	 * @param datasource
	 *            要解密的数据
	 * @return 返回加密后的 byte[]
	 */
	public byte[] createDecryptor(byte[] datasource) {
		 byte[] decryptorData = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, deskey);
			decryptorData = cipher.doFinal(datasource);
		} catch (java.security.InvalidKeyException ex) {
			ex.printStackTrace();
		} catch (javax.crypto.BadPaddingException ex) {
			ex.printStackTrace();
		} catch (javax.crypto.IllegalBlockSizeException ex) {
			ex.printStackTrace();
		}
		return decryptorData;
	}

	/**
	 * 将 DES 加密过的 byte数组转换为字符串.
	 * 
	 * @param dataByte
	 *            the data byte
	 * @return the string
	 */
	public String byteToString(byte[] dataByte) {
		return Base64.encodeBase64String(dataByte);
	}

	/**
	 * 将字符串转换为DES算法可以解密的byte数组.
	 * 
	 * @param datasource
	 *            the datasource
	 * @return the byte[]
	 */
	public byte[] stringToByte(String datasource) {
		return Base64.decodeBase64(datasource);
	}

	/**
	 * 输出 byte数组.
	 * 
	 * @param data
	 *            the data
	 */
	public void printByte(byte[] data) {
		System.out.println("*********开始输出字节流**********");
		System.out.println("字节流: " + data.toString());
		for (int i = 0; i < data.length; i++) {
			System.out.println("第 " + i + "字节为：" + data[i]);
		}
		System.out.println("*********结束输出字节流**********");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception {
		DES2 des = new DES2("deep8521");
		des.test(des);
	}

	/**
	 * Test.
	 * 
	 * @param des
	 *            the des
	 * @throws Exception
	 *             the exception
	 */
	public void test(DES2 des) throws Exception {
		// 加密源数据
		String encryptorString = "glg7ZAbiaWoLk6Kt2+yS03mbROmBT5RYt2g3fsJu2Jjh0ALkpahfyA==";
		System.out.println("加密前的数据：" + encryptorString);

		// 加密后的byte[] 转换来的字符串
		String byteToString = des.byteToString(des.createEncryptor(encryptorString));
		System.out.println("加密后的数据:" + byteToString);

		String aimStr = ByteConverter.encode(byteToString);
		System.out.println("加密后的16进制编码 :" + aimStr);
		String decodeAim = ByteConverter.decode(aimStr);
		System.out.println("解密后的16进制编码 :" + decodeAim);
		String decryptorString = new String(des.createDecryptor(des.stringToByte(decodeAim)));

		System.out.println("解密后的数据：" + decryptorString);
		
		
		
	}

	/**
	 * Gets the key string.
	 * 
	 * @return the key string
	 */
	public String getKeyString() {
		return keyString;
	}

	/**
	 * Sets the key string.
	 * 
	 * @param keyString
	 *            the new key string
	 */
	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}

}
