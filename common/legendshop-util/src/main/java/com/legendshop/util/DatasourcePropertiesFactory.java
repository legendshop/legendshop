/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据源属性工厂类
 */
public class DatasourcePropertiesFactory {

	/** The log. */
	protected final static Logger log = LoggerFactory.getLogger(DatasourcePropertiesFactory.class);

	/** 是否生产模式. */
	private static Boolean PRODUCTION_MODE = true;

	/** 密码. */
	private static final String PROP_PASSWORD = "password";

	/** 默认安全码. */
	private static String DEFAULT_SECURE_KEY = "LegendShop";

	/**
	 * 获取属性.
	 * 
	 */
	public static Properties getProperties(String pwd, Boolean production) throws Exception {
		return getProperties(pwd, production, null);
	}

	/**
	 * 获取属性.
	 * 
	 */
	public static Properties getProperties(String pwd, Boolean production, String secureKey) throws Exception {
		log.debug("jdbc production {} , secureKey {}", new Object[] { production, secureKey });
		Properties p = new Properties();
		PRODUCTION_MODE = production;
		// production mode
		if (PRODUCTION_MODE) {
			try {
				if (secureKey!=null) {
					DEFAULT_SECURE_KEY = secureKey;
				}
				p.setProperty(PROP_PASSWORD, decode(pwd));
			} catch (Exception e) {
				throw e;
			}
		} else {
			p.setProperty(PROP_PASSWORD, pwd);
		}
		return p;
	}

	/**
	 * 加密
	 * 
	 */
	private static String encode(String secret) throws NamingException, NoSuchAlgorithmException, InvalidKeyException,
			NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
		if (PRODUCTION_MODE) {
			byte[] kbytes = DEFAULT_SECURE_KEY.getBytes();
			SecretKeySpec key = new SecretKeySpec(kbytes, "Blowfish");

			Cipher cipher;
			cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encoding = cipher.doFinal(secret.getBytes());
			BigInteger n = new BigInteger(encoding);
			return n.toString(16);
		} else {
			return secret;
		}

	}

	/**
	 * 解密
	 */
	public static String decode(String secret) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
			BadPaddingException, IllegalBlockSizeException {
		if (PRODUCTION_MODE) {
			byte[] kbytes = DEFAULT_SECURE_KEY.getBytes();
			SecretKeySpec key = new SecretKeySpec(kbytes, "Blowfish");

			BigInteger n = new BigInteger(secret, 16);
			byte[] encoding = n.toByteArray();
			Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] decode = cipher.doFinal(encoding);
			return new String(decode);
		} else {
			return secret;
		}
	}

	/**
	 * 主方法.
	 * 
	 */
	public static void main(String[] args) throws NamingException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
		DatasourcePropertiesFactory.PRODUCTION_MODE = true;
		String secret = "root";
		String encoded = encode(secret);
		System.out.println("加密后密码是 " + encoded);
		System.out.println("还原的明文密码是 " + decode(encoded));
	}
}