/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util.constant;

/**
 * 字符串Enum基类
 */
public interface StringEnum {

	/**
	 * Value.
	 * 
	 * @return the string
	 */
	public String value();

}
