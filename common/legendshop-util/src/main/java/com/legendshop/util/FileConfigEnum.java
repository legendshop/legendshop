/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.util;

/**
 * 配置文件定义
 */
public interface FileConfigEnum {

	/**
	 * 获取定义名称
	 * 
	 */
	public String name();
	
	/**
	 * 获取配置文件
	 * @return
	 */
	public String file();

}
