/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

import com.legendshop.framework.event.BaseEventId;

/**
 *     系统事件ID, 
 * 
 *   作为系统的扩展点, 注意事件主要用于做系统解耦, 例如独立运行的插件,或者跟第三方公司进行对接,在插件中写每个项目特有的业务逻辑而不影响主体业务
 *   如果业务系统有特殊需求,请写在事件处理器中,请勿污染主体业务, 每个业务点只是发送一次消息
 */
public enum EventId implements BaseEventId {
	
	/**
	 *  系统登录
	 */
	LOGIN_EVENT("LOGIN_EVENT"),
	
	/**
	 *  系统注销
	 */
	LOGOUT_EVENT("LOGOUT_EVENT"),
	
	/**
	 *  发送邮件   
	 */
	SEND_MAIL_EVENT("SEND_MAIL"),

	/**
	 * 
	 *   发送短消息
	 */
	SEND_SMS_EVENT("SEND_SMS"),
	
	/**
	 *   
	  *   发送站内信
	  *   
	 */
	SEND_SITE_MSG_EVENT("SEND_SITE_MSG");
	
	/** The value. */
	private final String value;

	public String getEventId() {
		return this.value;
	}

	private EventId(String value) {
		this.value = value;
	}

	public boolean instance(String name) {
		EventId[] eventIds = values();
		for (EventId eventId : eventIds) {
			if (eventId.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
