package com.legendshop.base.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 定时器Log
 * @author tony
 *
 */
public class TimerLog {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(TimerLog.class);

	public static void log(String value, Object... params) {
		if (log.isDebugEnabled()) {
			log.debug(value, params);
		}

	}

	public static void info(String value, Object... params) {
		if (log.isInfoEnabled()) {
			log.info(value, params);
		}

	}

	public static void warn(String value, Object... params) {
		if (log.isWarnEnabled()) {
			log.warn(value, params);
		}

	}

	public static void error(String message, Exception e) {
		if (log.isErrorEnabled()) {
			log.error(message, e);
		}

	}

	public static void error(String message, Object... params) {
		if (log.isErrorEnabled()) {
			log.error(message, params);
		}

	}

	public static Logger getLog() {
		return log;
	}

}
