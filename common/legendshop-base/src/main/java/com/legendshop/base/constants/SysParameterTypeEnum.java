/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.constants;

/**
 * 系统配置参数类型
 * 
 */
public enum SysParameterTypeEnum {
	/** The Password. */
	Password,
	/** The String. */
	String,
	/** The Integer. */
	Integer,
	/** The Boolean. */
	Boolean,
	/** The Long. */
	Long,
	/** The Double. */
	Double,
	/** The List. */
	List;
}
