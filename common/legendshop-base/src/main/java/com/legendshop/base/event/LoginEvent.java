/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.dto.LoginSuccess;

/**
 * 登录时触发的事件.
 */
public class LoginEvent extends SystemEvent<LoginSuccess> {

	/**
	 *  实例化一个登录实体
	 */
	public LoginEvent(LoginSuccess loginSuccess) {
		super(EventId.LOGIN_EVENT);
		this.setSource(loginSuccess);
		
	}

}
