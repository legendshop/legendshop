/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.SiteMessageInfo;
/**
 * 发送站内信事件
 *
 */
public class SendSiteMsgEvent extends SystemEvent<SiteMessageInfo> {

	public SendSiteMsgEvent(String sendName, String receiveName, String title, String text) {
		super(EventId.SEND_SITE_MSG_EVENT);
		
		setSource(new SiteMessageInfo(sendName,receiveName, title,text));
	}
	
	/**
	 * 系统发送消息
	 * @param receiveName
	 * @param title
	 * @param text
	 */
	public SendSiteMsgEvent(String receiveName, String title, String text) {
		super(EventId.SEND_SITE_MSG_EVENT);
		String defaultShopName = "";
		setSource(new SiteMessageInfo(defaultShopName,receiveName, title,text));
	}

}
