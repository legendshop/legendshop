/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

/**
 * 事件处理器
 * 
 * 可以支持异步调用, 采用Spring的@async 标签来支持异步操作
 *    见VisitLogProcessor
 */
public interface EventProcessor<T> {

	/**
	 * 处理事件,采用泛型
	 */
	public void process(T task);
}
