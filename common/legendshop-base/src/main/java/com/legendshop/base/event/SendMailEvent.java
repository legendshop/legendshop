/*
 * 
\ * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.MailInfo;
import com.legendshop.model.constant.MailTypeEnum;

public class SendMailEvent extends SystemEvent<MailInfo> {

	/**
	 * Instantiates a new user reg event.
	 * 
	 * @param source
	 *            the source
	 */
	public SendMailEvent(String to, String subject, String text,String sendName,Integer userLevel,String category,Integer type) {
		super(EventId.SEND_MAIL_EVENT);
		setSource(new MailInfo(to, subject, text, sendName, userLevel, category,type));
	}
	
	/**
	 * 单独发送
	 * @param to
	 * @param subject
	 * @param text
	 * @param sendName
	 * @param category
	 */
	public SendMailEvent(String to, String subject, String text,String sendName,String category) {
		this(to, subject, text, sendName, null, category, MailTypeEnum.ONE_TO_ONE.value());
	}
	

	/**
	 * 验证码发送
	 * @param to
	 * @param subject
	 * @param text
	 * @param sendName
	 * @param category
	 */
	public SendMailEvent(String to, String subject, String text,String sendName,String category,String mailCode) {
		super(EventId.SEND_MAIL_EVENT);
		MailInfo info=new MailInfo(to, subject, text, sendName, null, category,MailTypeEnum.ONE_TO_ONE.value());
		info.setMailCode(mailCode);
		setSource(info);
	}
	

}
