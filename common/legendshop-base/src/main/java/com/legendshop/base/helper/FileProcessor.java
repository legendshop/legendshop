/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.helper;

import com.legendshop.base.model.InvokeRetObj;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.util.AppUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 
 * 文件上传工具类
 */
public class FileProcessor {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(FileProcessor.class);

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
	}

	/**
	 * Instantiates a new file processor.
	 */
	public FileProcessor() {
	}

	/**
	 * 删除文件.
	 * 
	 * @param fileName
	 *            文件名称
	 * @return 0： 删除成功， -1：文件存在，但删除失败 1： 没有文件 2： 未知原因失败
	 */
	public static InvokeRetObj deleteFile(String fileName) {
		return deleteFile(fileName, true);
	}

	/**
	 * 删除文件.
	 * 
	 * @param filename
	 *            文件名称
	 * @param backup
	 *            是否备份大图片
	 * @return 0： 删除成功， -1：文件存在，但删除失败 1： 没有文件 2： 未知原因失败
	 */
	public static InvokeRetObj deleteFile(String filename, boolean backup) {
		InvokeRetObj invokeRetObj = new InvokeRetObj();
		try {
			File f = new File(filename);
			if (f.exists()) {// 检查是否存在
				// 备份文件
				if (backup) {
					String backupPath = PropertiesUtilManager.getPropertiesUtil().getBackupPicPath();
					if (AppUtils.isNotBlank(backupPath)) {
						String bigPath = PropertiesUtilManager.getPropertiesUtil().getBigPicPath();
						if (AppUtils.isNotBlank(bigPath)) {
							if (filename.indexOf(bigPath) > -1) {// 大图片才会备份
								// copy file
								String destFilePath = filename;
								destFilePath = destFilePath.replace(bigPath, backupPath);
								File destFile = new File(destFilePath);
								FileUtils.copyFile(f, destFile);
							}
						}
					}
				}
				boolean result = f.delete();// 删除文件
				if (result) {
					invokeRetObj.setMsg("删除成功");
					invokeRetObj.setFlag(true);
					invokeRetObj.setRetCode(0);
					return invokeRetObj; // 删除成功
				} else {
					invokeRetObj.setMsg("删除失败");
					invokeRetObj.setRetCode(-1);
					invokeRetObj.setFlag(false);
					return invokeRetObj;
				}

			} else {
				log.warn("没有该文件：{}", filename);
				invokeRetObj.setMsg("没有该文件: " + filename);
				invokeRetObj.setRetCode(1);
				invokeRetObj.setFlag(false);
				return invokeRetObj;// 没有文件
			}
		} catch (Exception e) {
			log.error("deleteFile file error:", e);
			invokeRetObj.setMsg("未知原因失败: " + e.getLocalizedMessage());
			invokeRetObj.setRetCode(2);
			invokeRetObj.setFlag(false);
			return invokeRetObj;// 失败
		}
	}

	/**
	 * 删除目录和下面的所有的文件.
	 * 
	 * @param path
	 *            the path
	 */
	public static void deleteDirectory(File path) {
		if (!path.exists())
			return;
		if (path.isFile()) {
			path.delete();
			return;
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteDirectory(files[i]);
		}
		path.delete();
	}

	/**
	 * Upload file.
	 * 
	 * @param is
	 *            the is
	 * @param storeDir
	 *            the store dir
	 * @param prefix
	 *            the prefix
	 * @param uploadedFileName
	 *            the uploaded file name
	 * @param nameChanged
	 *            the name changed
	 * @param overwrited
	 *            the overwrited
	 * @return the string
	 */
	public static String uploadFile(InputStream is, String storeDir, String prefix, String uploadedFileName, boolean nameChanged, boolean overwrited) {
		FileOutputStream fos = null;
		try {
			String extName = getFileExtName(uploadedFileName);
			if (nameChanged) {
				uploadedFileName = prefix + System.currentTimeMillis() + randomNumeric(new Random(), 6) + extName;
			}
			File parentPath = new File(storeDir);
			if (!parentPath.exists()) {
				parentPath.mkdirs();
			}
			File f = new File(storeDir + "/" + uploadedFileName);
			if (f.exists() && nameChanged) {
				uploadedFileName = System.currentTimeMillis() + "_" + uploadedFileName;
				f = new File(storeDir + "/" + uploadedFileName);
			} else if (f.exists() && !overwrited) {
				return uploadedFileName;
			}
			fos = new FileOutputStream(f);
			int len = 0;
			byte[] buf = new byte[4096];
			while ((len = is.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}

		} catch (Exception e) {
			log.error("uploadFile file error:", e);
			return null;
		} finally {
			try {
				fos.close();

			} catch (Exception e) {
			}
			try {
				is.close();
			} catch (Exception e2) {
			}
		}

		return uploadedFileName;
	}

	/**
	 * Random numeric.
	 * 
	 * @param random
	 *            the random
	 * @param count
	 *            the count
	 * @return the string
	 */
	public static String randomNumeric(Random random, int count) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			int val = random.nextInt(10);
			sb.append(String.valueOf(val));
		}
		return sb.toString();
	}

	/**
	 * 拷贝文件.
	 * 
	 * @param srcFile
	 *            the src file
	 * @param destFile
	 *            the dest file
	 * @return true, if successful
	 */
	public static boolean copyFile(File srcFile, File destFile) {
		try {
			FileInputStream fSrc = new FileInputStream(srcFile);
			FileOutputStream fDest = new FileOutputStream(destFile);
			int len = 0;
			byte[] buf = new byte[4096];
			while ((len = fSrc.read(buf)) > 0) {
				fDest.write(buf, 0, len);
			}
			fDest.close();
			fSrc.close();
		} catch (Exception e) {
			log.error("copyFile file error:", e);
			return false;
		}

		return true;
	}

	/**
	 * Write file. 默认写文件方法
	 * 
	 * @param fileContent
	 *            the file content
	 * @param parentFilePath
	 *            the parent file path
	 * @param uploadedFileName
	 *            the uploaded file name
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String writeFile(String fileContent, String parentFilePath, String uploadedFileName) throws IOException {
		return writeFile(fileContent, parentFilePath, uploadedFileName, false, true, false);
	}

	/**
	 * Write file.
	 * 
	 * @param fileContent
	 *            the file content
	 * @param parentFilePath
	 *            the parent file path
	 * @param uploadedFileName
	 *            the uploaded file name
	 * @param nameChanged
	 *            the name changed
	 * @param overwrited
	 *            the overwrited
	 * @param formatText
	 *            the format text
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String writeFile(String fileContent, String parentFilePath, String uploadedFileName, boolean nameChanged, boolean overwrited,
			boolean formatText) throws IOException {
		File parentPath = null;
		File outFile = null;
		FileWriter outFileWriter = null;
		OutputStreamWriter outputStreamWriter = null;
		try {

			parentPath = new File(parentFilePath);
			if (!parentPath.exists()) {
				parentPath.mkdirs();
			}

			outFile = new File(parentFilePath + "/" + uploadedFileName);
			if (outFile.exists() && nameChanged) {
				uploadedFileName = System.currentTimeMillis() + "_" + uploadedFileName;
				outFile = new File(parentFilePath + "/" + uploadedFileName);
			} else if (outFile.exists() && !overwrited) {
				return uploadedFileName;
			}

			outFileWriter = new FileWriter(outFile);
			outputStreamWriter = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8");

			if (formatText) {
				StringBuffer formatSb = new StringBuffer();
				formatSb.append(" <P> ");
				for (int i = 0; i < fileContent.length(); i++) {
					if (new String(new char[] { fileContent.charAt(i) }).equals(" ")) {
						formatSb.append(" &nbsp;&nbsp; ");
					} else if (new String(new char[] { fileContent.charAt(i) }).equals("\n")) {
						formatSb.append(" <br> ");
					} else if (new String(new char[] { fileContent.charAt(i) }).equals("\n")
							&& new String(new char[] { fileContent.charAt(i + 1) }).equals("\n") && ((i + 1) != fileContent.length())) {
						formatSb.append(" </P><br><P> ");
						i++;
					} else {
						formatSb.append(new String(new char[] { fileContent.charAt(i) }));
					}
				}
				formatSb.append("</P>");
				fileContent = formatSb.toString();
			}

			outputStreamWriter.write(fileContent);
			outFileWriter.close();

		} catch (Exception e) {
			// e.printStackTrace();
			log.error("writeFile file error:", e);
			return null;
		} finally {
			if (outputStreamWriter != null) {
				outputStreamWriter.close();
			}
			if (outFileWriter != null) {
				outFileWriter.close();
			}
			outputStreamWriter = null;
			outFileWriter = null;
		}

		return uploadedFileName;
	}

	/**
	 * Read file.
	 * 
	 * @param file
	 *            the file
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String readFile(File file) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		String line = "";
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	/**
	 * Read file.
	 * 
	 * @param file
	 *            the file
	 * @param breakline
	 *            the breakline
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String readFile(File file, boolean breakline) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		String line = "";
		while ((line = br.readLine()) != null) {
			sb.append(line).append("\n");
		}
		return sb.toString();
	}

	/**
	 * 保存文件
	 */
	public static String uploadFileAndCallback(MultipartFile file) {
		String fileName = file.getOriginalFilename();
		String extName = getFileExtName(fileName);
		String lastFileName = UUID.randomUUID().toString() + extName;

		String path = getSubPath() + lastFileName;
		File dirPath = new File(PropertiesUtilManager.getPropertiesUtil().getBigPicPath() + "/" + getSubPath());
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		String fileFullPath = PropertiesUtilManager.getPropertiesUtil().getBigPicPath() + "/" + path;

		File f = new File(fileFullPath);
		try {
			FileCopyUtils.copy(file.getBytes(), f);
		} catch (Exception e) {
			log.error("uploadFileAndCallback file error:", e);
		}
		return path;

	}

	/**
	 * 获取文件的后缀名
	 */
	public static String getFileExtName(String fileName) {
		if (AppUtils.isBlank(fileName)) {
			return "";
		}

		int index = fileName.lastIndexOf(".");
		if (index <= 0) {
			return "";
		}
		return fileName.substring(index).toLowerCase();
	}

	/**
	 * 保存文件
	 * 
	 * @param is
	 * @param uploadedFileName
	 * @return 文件的全路径
	 */
	public static String uploadFile(InputStream is, String uploadedFileName) {
		FileOutputStream fos = null;
		try {
			// create file folder
			String filePath = PropertiesUtilManager.getPropertiesUtil().getBigPicPath() + "/" + getSubPath();
			File parentPath = new File(filePath);
			if (!parentPath.exists()) {
				parentPath.mkdirs();
			}
			File f = new File(filePath + "/" + uploadedFileName);
			fos = new FileOutputStream(f);
			int len = 0;
			byte[] buf = new byte[4096];
			while ((len = is.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}
			uploadedFileName = getSubPath() + uploadedFileName;
		} catch (Exception e) {
			log.error("uploadFile error", e);
			return null;
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
			}
			try {
				is.close();
			} catch (Exception e2) {
			}
		}

		return uploadedFileName;
	}

	public static String uploadFile(InputStream is, String subPath, String uploadedFileName) {
		FileOutputStream fos = null;
		try {
			// create file folder
			String filePath = PropertiesUtilManager.getPropertiesUtil().getBigPicPath() + "/" + subPath;
			File parentPath = new File(filePath);
			if (!parentPath.exists()) {
				parentPath.mkdirs();
			}
			File f = new File(filePath + "/" + uploadedFileName);
			fos = new FileOutputStream(f);
			int len = 0;
			byte[] buf = new byte[4096];
			while ((len = is.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}
			uploadedFileName = subPath + uploadedFileName;
		} catch (Exception e) {
			log.error("uploadFile error", e);
			return null;
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
			}
			try {
				is.close();
			} catch (Exception e2) {
			}
		}

		return uploadedFileName;
	}

	/**
	 * 获取图片的保存路径
	 * 
	 * @return
	 */
	private static String getSubPath() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String ymd = sdf.format(new Date());
		return ymd + "/";
	}


	/**
	 * 获取excel模板路径
	 * */
	public static String getExcelByName(String fileName){
		try {
			ClassPathResource resource = new ClassPathResource("templet/excel/"+fileName);

			String filePath = resource.getURL().toString();

			return filePath;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取excel流
	 * */
	public static InputStream getInputStreamByName(String fileName){

		InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("templet/excel/" + fileName);
		return resourceAsStream;
	}

}