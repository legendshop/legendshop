/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

public final class SeckillStringUtils {

	/**
	 * Cache key joint.
	 *
	 * @param key the key
	 * @param seckillId the seckill id
	 * @param userId the user id
	 * @param prodId the prod id
	 * @param skuId the sku id
	 * @return the string
	 */
	public  static String cacheKeyJoint(String key,Long seckillId,String userId,long prodId,long skuId){
		StringBuilder cached_key=new StringBuilder(key).append(seckillId).append("_").append(userId)
				.append("_").append(prodId).append("_").append(skuId);
		return cached_key.toString();
	}
	
	public  static String cacheKeyJoint(String key,Long seckillId,long prodId,long skuId){
		StringBuilder cached_key=new StringBuilder(key).append(seckillId)
				.append("_").append(prodId).append("_").append(skuId);
		return cached_key.toString();
	}
	
	public  static String cacheKeyJoint(String key,Long seckillId,String userId){
		StringBuilder cached_key=new StringBuilder(key).append(seckillId).append("_").append(userId);
		return cached_key.toString();
	}
	
	
	
	public  static String cacheKeyJoint(String key,Long seckillId){
		StringBuilder cached_key=new StringBuilder(key).append(seckillId);
		return cached_key.toString();
	}
	
	public  static String joint(Long seckillId,long prodId,long skuId){
		StringBuilder cached_key=new StringBuilder().append(seckillId).append(prodId).append(skuId);
		return cached_key.toString();
	}
	
	
}
