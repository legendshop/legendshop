package com.legendshop.base.util;

import com.legendshop.framework.cache.client.RedisCacheClient;
import com.legendshop.model.dto.im.ChatBody;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Web端操作IM Redis缓存
 */
@Component
public class RedisImCacheUtil {

	@Autowired
	private RedisCacheClient redisCacheClient;
	
	/**
	 * 获取发给平台所有的离线消息的key
	 * 
	 * @param type 离线消息类型
	 * @return
	 */
	public Set<Object> offPlatformMessageKeys(String type) {
		String session_key = "off-line-session:*:0:"+ type +":history";
		Set<Object> keys = redisCacheClient.getRedisTemplate().keys(session_key);
		return keys;
	}
	
	/**
	 * 获取发给用户离线消息的keys
	 * @param toUserId 接收消息用户ID
	 * @param type 离线消息类型
	 * @return
	 */
	public Set<Object> offCustomerMessageKeys(String toUserId, String type) {
		String session_key = "off-line-session:"+ toUserId+":*:"+ type +":history";
		Set<Object> keys = redisCacheClient.getRedisTemplate().keys(session_key);
		return keys;
	}
	
	
	/**
	 * 统计离线消息的数量
	 * @param fromUserId 发送消息用户ID
	 * @param toUserId 接收消息用户ID
	 * @param type 离线消息类型
	 * @return
	 */
	public long  countOffLineMessage(String fromUserId, String toUserId, String type){
		String session_key= "off-line-session:"+fromUserId+":"+toUserId+":"+type+":history";
		Long resut = redisCacheClient.getRedisTemplate().boundZSetOps(session_key).zCard();
		if(resut == null) {
			return 0l;
		}else {
			return resut;
		}
		
	}
	
	
	/**
	 * 离线消息的数量
	 * 
	 * @param key
	 * @return
	 */
	public Long offCountMessage(String key) {
		return redisCacheClient.getRedisTemplate().boundZSetOps(key).zCard();
	}

	/**
	 * 离线消息
	 * 
	 * @param key
	 * @return
	 */
	public void delOffMessage(String key) {
		redisCacheClient.getRedisTemplate().delete(key);
	}

	/**
	 * 获取离线消息
	 * 
	 * @param key
	 * @return
	 */
	public Set<Object> getOffMessage(Object key) {
		Set<Object> messages = redisCacheClient.getRedisTemplate().boundZSetOps(key).reverseRange(0, -1);
		return messages;
	}
	
	/**
	 *  获取最后一条离线消息
	 * 
	 * @param fromUserId  发送消息用户ID
	 * @param toUserId 接收消息用户ID
	 * @param type   离线消息类型
	 * @return
	 */
	public Set<Object> getLastOnLineMessage(String fromUserId, String toUserId, String type) {
		String off_session_key = "off-line-session:" + fromUserId + ":" + toUserId + ":" + type + ":history";
		Set<Object> messages = redisCacheClient.getRedisTemplate().boundZSetOps(off_session_key).reverseRange(0, 0);
		return messages;
	}

	/**
	 * 获取客服在线消息
	 * 
	 * @param fromUserId 发送消息用户ID
	 * @param toUserId 接收消息用户ID
	 * @param beginTime 消息的开始时间
	 * @param endTime 消息结束时间
	 * @param start  偏移量
	 * @return
	 */
	public Set<Object> getOnlineMessage(String fromUserId, String toUserId, Double beginTime, Double endTime, Integer start, Integer end) {
		String session_key = "session:" + fromUserId + ":" + toUserId + ":history";// s格式ession:86e34e47-aae7-4e1d-bebf-57b66b9ce6ac:204:history
		BoundZSetOperations<Object, Object> boundZSetOperations = redisCacheClient.getRedisTemplate().boundZSetOps(session_key);
		if (AppUtils.isBlank(start) || AppUtils.isBlank(end)) {
			start = 0;
			end = 100;
		}
		Set<Object> messages = boundZSetOperations.reverseRange(start, end);
		return messages;
	}

	/**
	 * 离线消息转化成在线消息
	 * 
	 * @param fromUserId 发送消息用户ID
	 * @param toUserId 接收消息用户ID
	 * @param type 离线消息类型
	 * @return
	 */
	public void converOfflineMessage(String fromUserId, String toUserId, String type) {
		String off_session_key = "off-line-session:" + fromUserId + ":" + toUserId + ":" + type + ":history";
		// 1.拿到用户发给平台离线消息
		Set<Object> messages = getOffMessage(off_session_key);
		System.out.println("获取得离线消息数为 " + messages.size());
		if (AppUtils.isNotBlank(messages)) {
			// 2.转化成在线消息
			for (Object message : messages) {
				ChatBody chatBody = (ChatBody) message;
				chatBody.setStatus("success");
				String session_key = "session:" + fromUserId + ":" + toUserId + ":history";
				writeMessage(session_key, chatBody);
			}

			// 3.删除离线消息
			delOffMessage(off_session_key);
		}
	}

	/**
	 * 获取平台在线消息
	 * 
	 * @param fromUserId 发送消息用户ID
	 * @param beginTime 消息的开始时间
	 * @param endTime 消息结束时间
	 * @param offset  偏移量
	 * @param count  每页消息数
	 * @return
	 */
	public Set<Object> getOnlineMessage(String fromUserId, Double beginTime, Double endTime, Integer offset, Integer count) {
		// shopId = 0代表平台
		String session_key = "session:" + fromUserId + ":" + 0 + ":history"; // s格式ession:86e34e47-aae7-4e1d-bebf-57b66b9ce6ac:204:history
		BoundZSetOperations<Object, Object> boundZSetOperations = redisCacheClient.getRedisTemplate().boundZSetOps(session_key);
		if (AppUtils.isBlank(offset) || AppUtils.isBlank(count)) {
			offset = 0;
			count = 100;
		}
		Set<Object> messages = boundZSetOperations.reverseRange(offset, count);
		return messages;
	}

	/**
	 * 根据type获取离线消息
	 * 
	 * @param fromUserId 发送消息用户ID
	 * @param toUserId 接收消息用户ID
	 * @param type 离线消息类型      
	 * @return
	 */
	public Set<Object> getOfflineMessageForSelf(String fromUserId, String toUserId, String type) {
		//key格式： session:86e34e47-aae7-4e1d-bebf-57b66b9ce6ac:204:history
		String session_key = "off-line-session:" + fromUserId + ":" + toUserId + ":"+ type + ":history";
		BoundZSetOperations<Object, Object> boundZSetOperations = redisCacheClient.getRedisTemplate().boundZSetOps(session_key);
		Set<Object> messages = boundZSetOperations.reverseRange(0, 100);
		return messages;
	}
	
	
	
	/**
	 * 获取客服所有聊天记录
	 */
	public Set<Object> getcustomerMessage(String fromUserId, String toUserId, String shopId, Integer offset, Integer count) {
		String shop_session_key= "customer-session:" + fromUserId + ":" + shopId + ":chatbody";
		BoundZSetOperations<Object, Object> boundZSetOperations = redisCacheClient.getRedisTemplate().boundZSetOps(shop_session_key);
		if (AppUtils.isBlank(offset) || AppUtils.isBlank(count)) {
			offset = 0;
			count = 100;
		}
		Set<Object> messages = boundZSetOperations.reverseRange(offset, count);
		return messages;
	}
	
	/**
	 * 获取客服所有聊天记录总数
	 */
	public Long getcustomerMessageCount(String fromUserId, String toUserId, String shopId) {
		String shop_session_key= "customer-session:" + fromUserId + ":" + shopId + ":chatbody";
		return redisCacheClient.getRedisTemplate().boundZSetOps(shop_session_key).zCard();
	}
	
	/**
	 * 储存用户的聊天信息
	 * 
	 * @param timelineId redis存储key
	 * @param chatBody 消息实体
	 */
	public void writeMessage(String timelineId, ChatBody chatBody) {
		double score = chatBody.getCreateTime();
		redisCacheClient.getRedisTemplate().boundZSetOps(timelineId).add(chatBody, score);
	}
}
