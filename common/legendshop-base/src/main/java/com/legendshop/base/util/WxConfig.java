package com.legendshop.base.util;


import com.legendshop.config.PropertiesUtilManager;

/**
 * 微信服务地址
 */
public class WxConfig {

  public static final String WEIXIN_SERVICE = PropertiesUtilManager.getPropertiesUtil().getWeiXinService();

  /** 同步微信菜单地址 */
  public static final String SYNCHRONOUS = WEIXIN_SERVICE + "/admin/weixin/menu/synchronous";

  /** 下拉微信用户 */
  public static final String SYNCHRONOUS_USERINFO = WEIXIN_SERVICE + "/admin/weixin/usersManage/synchronousUserinfo";

  /** 下拉微信微信分组 */
  public static final String SYNCHRONOUSGROUP = WEIXIN_SERVICE + "/admin/weixin/usersManage/synchronousGroup";

  /** 创建分组 */
  public static final String createGroups = WEIXIN_SERVICE + "/admin/weixin/usersManage/createGroups";

  /** 修改分组名称 */
  public static final String updateGroup = WEIXIN_SERVICE + "/admin/weixin/usersManage/updateGroup";

  /** 修改用户分组名称 */
  public static final String updateUserGroup = WEIXIN_SERVICE + "/admin/weixin/usersManage/updateUserGroup";

  /** 删除用户分组 */
  public static final String deleteGroup = WEIXIN_SERVICE + "/admin/weixin/usersManage/deleteGroup";

  /** 更新用户备注 */
  public static final String updateRemark = WEIXIN_SERVICE + "/admin/weixin/usersManage/updateRemark";
}
