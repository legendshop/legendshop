package com.legendshop.base.util;

import com.legendshop.util.SafeHtml;

/**
 * XssFilter
 */
public class XssFilterUtil {

	private static SafeHtml safe = new SafeHtml();

	public static String cleanXSS(String value) {
		String result = safe.makeSafe(value);

		return cleanXSS2(result);
	}


	/**
	 * 过滤Html javascript 和sql
	 *
	 */
	private static String cleanXSS2(String value) {
		if(value == null){
			return null;
		}
		if(value.length() == 0){
			return value;
		}
		//You'll need to remove the spaces from the html entities below
		value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		value = value.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
		value = value.replaceAll("'", "");  //&#39; 因为安全原因，置空
		value = value.replaceAll("\"", ""); //&#34;  因为安全原因，置空
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		return value;
	}

	public static void main(String[] args) {
		System.out.println(XssFilterUtil.cleanXSS("<img src='x' onerror='alert(1)' >"));
	}


}
