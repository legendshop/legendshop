package com.legendshop.base.encrypt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 接受前台jsencrypt.js加密的参数并解密, jsp的publickey位于publickey.jsp, publickey和privatekey可以用RSAUtils来生成
 *
 */
public class RSAHelper {
  

  private final static Logger logger = LoggerFactory.getLogger(RSAHelper.class);
  
  //在前端中中定义好,采用公钥加密好之后后台用私钥加密
  private static  String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwurkbCT+G3dfRktSLL3lszMsEFcNko5qyqJRmhOz065k1St7Ukty5XuLzN0LDPbFyZCklJzS7cXDm2vWQiIvO1b/yEBlmYNOp3OEd2u2FUPP5OuVZahyzdUkt+PPNO8VCyFUGDsB0bmVW1xLyWuiRXl5idA09wcErSXPkJkxpiwIDAQAB";

  private static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALC6uRsJP4bd19GS1IsveWzMywQVw2SjmrKolGaE7PTrmTVK3tSS3Lle4vM3QsM9sXJkKSUnNLtxcOba9ZCIi87Vv/IQGWZg06nc4R3a7YVQ8/k65VlqHLN1SS348807xULIVQYOwHRuZVbXEvJa6JFeXmJ0DT3BwStJc+QmTGmLAgMBAAECgYBasijG0DMy4mycl+7N0zpMdLkHSqfw4OlGIK+wAS2d7767k8XRb+EonKJ50PVJP0Lugqp+76TnQSuAvgPh/cFJNLkKyaa7wH47otSwVZekzvWZ7ysVdQTjdoat2tfqgLb6y51TPzLr4kNpOdAWHQvBPtONsLdnShp45XEm3OMQmQJBAOJgRTtvUFUuKolWmKIA96VY0No1QWTtVPVvOWncwgi/XKLk+INQ1To6L9P+7hIaeAMznTfDb3Vfv9Livrx5OLUCQQDH20Kk3gwYbxyYnRdkkacoKpVYwagVbO2IbbVPd3T3BHD6DvXz0eEI7MtvfMhw8xLnmiOYFzwJxBDVM4dzO8E/AkEAm+y9oxugFJvw9posFeJesYhEixiFi5QhodBTloRVLhtJHnZiamjCBhJQIVCGAPg0U9g4YVF/MzSZdxNxV3aTxQJAcJBeCN3HkjuCr7JK6qnerBDP7R2lveuMHbss04VEq5wkxbKHdLxRMuWr7y8o9fTyQN/gDFmJxzZXBL3xHEYboQJAZjykoVs9iPPp1mhKwnss+6tVBTpjBnCZrai+9bn2SU6ptbKBCBJvoUEtD1ORsJtDq5Hg/+hBIRg7FkB1z8D3KQ==";
  
  /**
      * 解密从jsp传过来的密码
   * @param password
   * @return
   */
  public static String getDecodePassword(String password) {
   String result = "";
    try {
      byte[]  decryptData = RSAUtils.decryptByPrivateKey(RSAUtils.decryptBASE64(password), privateKey);
      result = new String(decryptData);
    } catch (Exception e) {
      logger.warn("password decode failed. password =  " + password, e.getLocalizedMessage());
    }
    return result;
  }
}
