/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class SeckillStockMap.
 */
public class SeckillStockMap {
	
	
	/** The Constant storeMap. */
	private final static Map<String, Integer> storeMap = new ConcurrentHashMap<String, Integer>();


	/**
	 * Put.
	 *
	 * @param key the key
	 * @param number the number
	 */
	public static void put(String key,int number){
		storeMap.put(key, number);
	}
	
	/**
	 * Gets the.
	 *
	 * @param key the key
	 * @return the int
	 */
	public static int get(String key){
		Integer number=storeMap.get(key);
		if(number==null){
			return 0;
		}
		return number.intValue();
	}
	
	/**
	 * Decr.
	 *
	 * @param key the key
	 * @return the int
	 */
	public static synchronized int decr(String key){
		int number=storeMap.get(key);
		if(number<=0){
			return 0;
		}
		number=number-1;
		if(number<=0){
			return 0;
		}
		storeMap.put(key, number);
		return number;
	}

	/**
	 * Incr.
	 *
	 * @param _key the _key
	 * @return the long
	 */
	public static synchronized long incr(String _key) {
		int number=storeMap.get(_key);
		number=number+1;
		storeMap.put( _key, number);
		return number;
	}
	
	
}
