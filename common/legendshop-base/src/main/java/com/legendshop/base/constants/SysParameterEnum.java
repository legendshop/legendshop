/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.constants;

import com.legendshop.base.dto.DefaultAddressDTO;

import java.util.List;

/**
 * 系统配置参数
 */
public enum SysParameterEnum {

	DEFAULT_PRODUCT_ADDRESS(String.class),
	
	/** 是否发送短信. */
	SEND_SMS(Boolean.class),
	
	/** 短信验证码的模板 **/
	VAL_SMS_TEMPATE_FORMAT(String.class),
	
	/** 邮件验证码的模板 **/
	VAL_EMAIL_TEMPATE_FORMAT(String.class),
	
	/**
	 * 获取短信今天能发送的限额
	 */
	SMS_TODAY_LIMIT(Integer.class),
	
	
	SMS_USE_FUL_MIN(Integer.class),
	
	/**
	 * 获取短信的超时时间， 单位分钟 min
	 */
	SMS_BETWEEN_MIN(Integer.class),
	
	/**
	 * 获取短信最大发送的条数
	 */
	SMS_MAX_SEND_NUM(Integer.class),
	

	/** 导出记录数大小. */
	EXPORT_SIZE(Integer.class),

	/** 前台页面的每页记录大小. */
	FRONT_PAGE_SIZE(Integer.class),

	/** 每页的记录数大小. */
	PAGE_SIZE(Integer.class),

	/** 是否发送邮件. */
	SEND_MAIL(Boolean.class),

	/** 支持的邮件列表. */
	SUPPORT_MAIL_LIST(String.class),

	/** 上传文件最大大小. */
	MAX_FILE_SIZE(Long.class),

	/** 最大的首页图片数量. */
	MAX_INDEX_JPG(Integer.class),

	/** 允许上传的文件类型. */
	ALLOWED_UPLOAD_FILE_TPYE(List.class),

	/** 邮件名字. */
	MAIL_NAME(String.class),

	/** 邮件主机. */
	MAIL_HOST(String.class),

	/** 邮件端口. */
	MAIL_PORT(Integer.class),

	/** 邮件密码. */
	MAIL_PASSWORD(String.class),

	/** 是否验证邮件. */
	MAIL_STMP_AUTH(Boolean.class),

	/** 邮件smtp超时时间. */
	MAIL_STMP_TIMEOUT(String.class),

	/** 是否需要验证邮件. */
	VALIDATION_FROM_MAIL(Boolean.class),

	/** 评论级别. */
	COMMENT_LEVEL(String.class),
	
	/** 评论是否需要审核. */
	COMMENT_NEED_REVIEW(Boolean.class),
	
	/** 是否记录登录历史. */
	VISIT_LOG_ENABLE(Boolean.class),

	/** 是否记用户录登录历史. */
	LOGIN_LOG_ENABLE(Boolean.class),

	/** 是否使用积分. */
	USE_SCORE(Boolean.class),

	/** 图片验证码. */
	VALIDATION_IMAGE(Boolean.class),

	/** 是否使用独立域名 *. */
	INDEPEND_DOMAIN(Boolean.class),
	
	/** PC端QQ登陆网站应用ID **/
	QQ_APP_ID(String.class),
	
	/** PC端QQ登陆网站应用secret **/
	QQ_APP_SECRET(String.class),
	
	/** H5端QQ登陆网站应用ID **/
	QQ_H5_APP_ID(String.class),
	
	/** H5端QQ登陆网站应用secret **/
	QQ_H5_APP_SECRET(String.class),
	
	/** 新浪微博应用ID**/
	WEIBO_APP_ID(String.class),
	
	/** 新浪微博应用 SECRET ID**/
	WEIBO_SECRET(String.class), 
	
	/** 新浪微博回调地址 **/
	WEIBO_REDIRECT_URL(String.class),
	
	/** 新浪微博应用ID**/
	WEIBO_H5_APP_ID(String.class),
	
	/** 新浪微博应用 SECRET ID**/
	WEIBO_H5_SECRET(String.class), 
		
	/** 微信第三方登陆应用ID **/
	WEIXIN_LOGIN_APPID(String.class),
	
	/** * 微信第三方登陆应用 APPSECRET **/
	WEIXIN_LOGIN_APPSECRET(String.class),
	
	
	/** 是否启用QQ登录 **/
	QQ_LOGIN_ENABLE(Boolean.class),
	
	/** 是否启用微博登录 **/
	WEIBO_LOGIN_ENABLE(Boolean.class),
	
	/** 是否启用微信登录 **/
	WEIXIN_LOGIN_ENABLE(Boolean.class),
	
	
	/**启用 Cookie 还是  Session**/
	ENABLE_COOKIE_SESSION(String.class), 
	
	
	/** COOKIE保存的时间 **/
	COOKIE_MAX_TIME(Integer.class),
	
	/**站点名称*/
	SITE_NAME(String.class),
	
	/** 商品 是否需要审核*/
	PROD_REQUIRE_AUDIT(Boolean.class),
	
	/** 商品结算日*/
	SHOP_BILL_DAY(Integer.class),
	
	/** 结算佣金比例**/
	SHOP_COMMIS_RATE(Double.class),
	
	/** 分销结算日**/
	DISTRIBUTION_SETTLEMENT_DAY(Integer.class), 
	
	/**拍卖发布是否需要审核*/
	AUCTION_REQUIRE_AUDIT(Boolean.class),

	APP_START_ADV(Double.class),


	/** 	关闭验证码（正式环境不能开启）. */
	CLOSE_VALIDATE_CODE(Boolean.class),

	/** 关闭短信验证码（正式环境不能开启）. */
	CLOSE_SMS_VALIDATE_CODE(Boolean.class),
	
	/**是否发送发货通知短信*/
	DELIVER_GOODS_SMS(Boolean.class),
	
	/** 是否支持开店 */
	OPEN_SHOP(Boolean.class), 
	
	/***成团倒计时，以小时算**/
	GROUP_SURVIVAL_TIME(Integer.class),

	/**
	 *  IM 绑定 IP
	 */
	IM_BIND_IP(String.class), 
	
	/**
	 * 文件下载路径
	 */
	DOWNLOAD_PATH(String.class),
	
	/**
	 * 是否使用 SQL debug模式
	 */
	SQL_DEBUG_MODE(Boolean.class),
	
	CURRENCY_PATTERN(String.class), 
	
	/**
	 * 是否读取图片的高宽
	 * 
	 */
	IS_READ_IMG_WIDTH_HEIGHT(Boolean.class), 
	
	FRONTEND_TEMPLET(String.class), 
	
	MOBILE_FRONTEND_TEMPLET(String.class), 
	
	BACKEND_TEMPLET(String.class), 
	
	/**
	 * 是否模拟支付
	 */
	BOOLEAN_PAY(Boolean.class), 
	
	/**
	 * 获取后台统计报表的统计时段
	 */
	DASHBOARD_STATISTICS_DAYS(String.class),
	
	/**
	 * 资源版本号
	 */
	RESOURCE_VERSION(String.class),
	
	
	SYSTEM_VERSION(String.class),
	
	IMG_ALLOW_TYPE(String.class),
	
	IMG_ALLOW_SIZE(String.class),
	
	VIDEO_ALLOW_TYPE(String.class),
	
	VIDEO_ALLOW_SIZE(String.class),
	
	/** 商家结算周期类型  */
	BILL_PERIOD_TYPE(String.class)
	
	;

	/** The clazz. */
	private final Class<?> clazz;

	/**
	 * Instantiates a new parameter enum.
	 * 
	 * @param clazz
	 *            the clazz
	 */
	private SysParameterEnum(Class<?> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Gets the clazz.
	 * 
	 * @return the clazz
	 */
	public Class<?> getClazz() {
		return this.clazz;
	}
	
	public static SysParameterEnum formCode(String code){
		for (SysParameterEnum parameterEnum:SysParameterEnum.values()) {
			if(parameterEnum.name().equals(code)){
				return parameterEnum;
			}
		}
		return null;
	}
	
	
}
