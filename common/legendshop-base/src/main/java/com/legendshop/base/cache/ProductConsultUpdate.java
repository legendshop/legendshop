package com.legendshop.base.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;

/**
 * 清除商品咨询的前2页
 * @author Administrator
 *  //参见 @Cacheable(value = "ProductConsultList",  condition = "T(Integer).parseInt(#curPageNO) < 3", key="#curPageNO + '.' + #pointType + '.' + #prodId")
 *  pointType: 0代表所有咨询
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Caching(evict = { 
		@CacheEvict(value = "ProductConsultList", key="'1.0.' + #productConsult.prodId"), //清除全部列表
		@CacheEvict(value = "ProductConsultList", key="'2.0.' + #productConsult.prodId"), //清除全部列表
		@CacheEvict(value = "ProductConsultList", key="'1.' + #productConsult.pointType  + '.' + #productConsult.prodId"),
		@CacheEvict(value = "ProductConsultList", key="'2.' + #productConsult.pointType +'.' + #productConsult.prodId")})
public @interface ProductConsultUpdate {

}
