/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.helper;

import com.legendshop.dao.support.PageProvider;

/**
 * 工具条帮助工具.
 */
public class PageProviderHelper {
	
	/**
	 * 得到前台的工具条，会分不同的模板
	 *
	 * @param request the request
	 * @return the page provider
	 */
	public static PageProvider getFrontEndPageProvider(String providerName) {
		return null;
	}
	
	/**
	 * 得到后台的工具条，会分不同的模板
	 *
	 * @param request the request
	 * @return the page provider
	 */
	public static PageProvider getBackEndPageProvider(String providerName) {
		return null;
	}

}
