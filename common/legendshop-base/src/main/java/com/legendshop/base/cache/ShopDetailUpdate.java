/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;

/**
 * 清除商城缓存.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Caching(evict = {
		@CacheEvict(value = "ShopDetailView", key = "#shopDetail.shopId"), 
		@CacheEvict(value = "ShopDetail", key = "#shopDetail.shopId")
})
public @interface ShopDetailUpdate {

}
