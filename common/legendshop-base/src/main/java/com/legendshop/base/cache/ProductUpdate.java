/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.base.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;

/**
 * 清除商品缓存， 参数中一定要带有product的字段.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Caching(evict = { 
		@CacheEvict(value = "ProductDetail", key = "#product.prodId"),
		@CacheEvict(value = "Product", key = "#product.prodId"),
		@CacheEvict(value = "ProductDto", key = "#product.prodId")
		})
public @interface ProductUpdate {

}
