package com.legendshop.base.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;

/**
 * 商品评论列表的前两页
 * @author Administrator
 * 参见 @Cacheable(value = "ProductCommentList",  condition = "T(Integer).parseInt(#params.curPageNO) < 3", key="#params.curPageNO + '.' + #params.condition + '.' + #params.prodId")
 *  pointType: 0代表所有咨询
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Caching(evict = {
		@CacheEvict(value = "ProductCommentList", key="'1.all.' + #productComment.prodId"), 
		@CacheEvict(value = "ProductCommentList", key="'2.good.' + #productComment.prodId")})
public @interface ProductCommentUpdate {

}
