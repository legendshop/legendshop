/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.base.event;

import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.constant.SMSTypeEnum;

import java.util.Date;

/**
 * 发送短信事件
 *
 */
public class SendSMSEvent extends SystemEvent<ShortMessageInfo> {

	/**
	 * Instantiates a new user reg event.
	 *
	 */
	public SendSMSEvent(String mobile,String mobileCode,String userName, SMSTypeEnum smsType) {
		super(EventId.SEND_SMS_EVENT);
		ShortMessageInfo sms = new ShortMessageInfo(mobile,mobileCode,userName, smsType);
		setSource(sms);
	}

	//发送短信验证码
	public SendSMSEvent(String mobile, String mobileCode, String userName) {
		super(EventId.SEND_SMS_EVENT);
		ShortMessageInfo sms = new ShortMessageInfo(mobile,mobileCode, userName, SMSTypeEnum.VAL);
		setSource(sms);
	}

	//发送短信验证码
	public SendSMSEvent(String mobile, String mobileCode, String userName, String requestInterface, String ip, Date time) {
		super(EventId.SEND_SMS_EVENT);
		ShortMessageInfo sms = new ShortMessageInfo(mobile, mobileCode, userName, SMSTypeEnum.VAL, requestInterface, ip, time);
		setSource(sms);
	}

	public SendSMSEvent(String mobile,String mobileCode,String userName, SMSTypeEnum smsType,String requestInterface, String ip, Date time) {
		super(EventId.SEND_SMS_EVENT);
		ShortMessageInfo sms = new ShortMessageInfo(mobile, mobileCode, userName, smsType, requestInterface, ip, time);
		setSource(sms);
	}

	//发送自定码短信
	public SendSMSEvent(String mobile, String mobileCode, String userName, String address, String orderSn) {
		super(EventId.SEND_SMS_EVENT);
		setSource(new ShortMessageInfo(mobile, mobileCode, userName, address, orderSn, SMSTypeEnum.ORDER));
	}

	//发送发货通知
	public SendSMSEvent(String mobile,String userName, String templateParam,String text) {
		super(EventId.SEND_SMS_EVENT);
		setSource(new ShortMessageInfo(mobile, userName,templateParam,text,SMSTypeEnum.DELIVER));
	}

	//到货通知
	public SendSMSEvent(String mobile,String prodName) {
		super(EventId.SEND_SMS_EVENT);
		setSource(new ShortMessageInfo(mobile, prodName, SMSTypeEnum.ARRIVAL));
	}
}
