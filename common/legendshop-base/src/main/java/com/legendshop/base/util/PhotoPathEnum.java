/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

/**
 * 用来区分是否使用了阿里云的oss.
 */
public enum PhotoPathEnum {
	
	LOCAL, //本地图片
	
	HTTP, //内部的图片服务器
	
	OSS //采用阿里云的oss

}
