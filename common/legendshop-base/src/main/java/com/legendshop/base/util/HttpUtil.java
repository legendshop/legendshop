package com.legendshop.base.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MyX509TrustManager;


/**
 * 网络链接
 * 
 * @author tony
 * 
 */
public class HttpUtil {
	
	private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);
	
	public static String httpPostJSON(String url, Map<String, Object> paramMap) {

		return httpPost(url, paramMap, "JSON", "UTF-8");
	}
	
	/**
	 * 发送http的POST请求
	 * @param url 请求URL
	 * @param paramMap 请求参数
	 * @return 响应结果String
	 */
	public static String httpPost(String url, Map<String, Object> paramMap) {

		return httpPost(url, paramMap, "DEFAULT", "UTF-8");
	}
	
	/**
	 * 发送http的POST请求
	 * @param url 请求URL
	 * @param paramMap 请求参数
	 * @param paramsType 请求参数类型
	 * @return
	 */
	public static String httpPost(String url, Map<String, Object> paramMap, String paramsType) {

		return httpPost(url, paramMap, paramsType, "UTF-8");
	}
	
	public static String httpPost(String url, Map<String, Object> paramMap, String paramsType, String charset) {

		String contentType = getContentType(paramsType);
		String outputStr = hanleParams(paramMap, paramsType, charset);
		return httpRequest(url, outputStr, "POST", contentType, charset);
	}

	public static String httpGet(String url) {
		
		return httpRequest(url, null, "GET", "DEFAULT", "UTF-8");
	}
	
	public static String httpGet(String url, Map<String, Object> paramMap) {
		
		String outputStr = hanleParams(paramMap, "DEFAULT", "UTF-8");
		
		return httpRequest(url, outputStr, "GET", "DEFAULT", "UTF-8");
	}
	
	public static String httpsPostJSON(String url, Map<String, Object> paramMap) {

		return httpsPost(url, paramMap, "JSON", "UTF-8");
	}
	
	public static String httpsPost(String url, Map<String, Object> paramMap) {

		return httpsPost(url, paramMap, "DEFAULT", "UTF-8");
	}
	
	public static String httpsPost(String url, Map<String, Object> paramMap, String paramsType) {

		return httpsPost(url, paramMap, paramsType, "UTF-8");
	}
	
	public static String httpsPost(String url, Map<String, Object> paramMap, String paramsType, String charset) {

		String contentType = getContentType(paramsType);
		String outputStr = hanleParams(paramMap, paramsType, charset);
		return httpsRequest(url, outputStr, "POST", contentType, charset);
	}

	public static String httpsGet(String url) {
		
		
		return httpsRequest(url, null, "GET", "DEFAULT", "UTF-8");
	}
	
	public static String httpsGet(String url, Map<String, Object> paramMap) {
		
		String outputStr = hanleParams(paramMap, "DEFAULT", "UTF-8");
		
		return httpsRequest(url, outputStr, "GET", "DEFAULT", "UTF-8");
	}
	
	public static String httpRequest(String requestUrl, String outputStr, String requestMethod, String contentType, String charset) {
		
		URL url = null;
		HttpURLConnection con = null;
		BufferedReader in = null;
		OutputStream out = null;
		try {
			if(AppUtils.isNotBlank(outputStr) && requestMethod.equalsIgnoreCase("GET")){
				requestUrl = requestUrl + "?" + outputStr;
			}
			url = new URL(requestUrl);
			con = (HttpURLConnection) url.openConnection();
			
			if(requestMethod.equalsIgnoreCase("POST")){
				con.setDoInput(true);
				con.setDoOutput(true);
			}
			
			con.setUseCaches(false);
			con.setConnectTimeout(10000); // 设置连接超时为10秒
			con.setRequestMethod(requestMethod);
			con.setRequestProperty("Content-type", contentType);
			con.setRequestProperty("Charset", "utf-8");
			con.setRequestProperty("User-Agent", 
					"Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			
			if ("GET".equalsIgnoreCase(requestMethod)) {
				con.connect();
			}
			
			if (AppUtils.isNotBlank(outputStr) && requestMethod.equalsIgnoreCase("POST")) {
				out = con.getOutputStream();

				out.write(outputStr.getBytes(charset));
				out.flush();
			}
			
			int returnCode = con.getResponseCode();
			if (returnCode == 200) {
				
				StringBuffer result = new StringBuffer();
				in = new BufferedReader(new InputStreamReader(con.getInputStream(), charset));
				while (true) {
					String line = in.readLine();
					if (line == null) {
						break;
					} else {
						result.append(line);
					}
				}
				
				return result.toString();
			}

		} catch (SocketTimeoutException e) {
			System.out.println("Weixin server connection timed out.");
		}
		catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(out != null){
					out.close();
				}
				if (in != null) {
					in.close();
				}
				if (con != null) {
					con.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static String httpsRequest(String requestUrl, String outputStr, String requestMethod, String contentType, String charset) {
		
		logger.debug("*********HTTPREQUEST START********");
		logger.debug("*********requestUrl: {}, outputStr: {}, requestMethod: {}, contentType ********", 
				requestUrl, outputStr, requestMethod, contentType);
		StringBuffer buffer = new StringBuffer();
		
		URL url = null;
		HttpsURLConnection conn = null;
		BufferedReader in = null;
		OutputStream out = null;
		try {
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new SecureRandom());

			SSLSocketFactory ssf = sslContext.getSocketFactory();

			url = new URL(requestUrl);
			conn = (HttpsURLConnection) url.openConnection();
			conn.setSSLSocketFactory(ssf);
			
			if(requestMethod.equalsIgnoreCase("POST")){
				conn.setDoOutput(true);
				conn.setDoInput(true);
			}
			
			conn.setUseCaches(false);
			conn.setRequestMethod(requestMethod);
			conn.setRequestProperty("Content-Type", contentType);
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			
			if ("GET".equalsIgnoreCase(requestMethod)) {
				conn.connect();
			}

			if (null != outputStr) {
				out = conn.getOutputStream();
				out.write(outputStr.getBytes(charset));
				out.flush();
			}
			
			int returnCode = conn.getResponseCode();
			if (returnCode == 200) {
				
				in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));

				String str = null;
				while ((str = in.readLine()) != null) {
					buffer.append(str);
				}
			}

			return buffer.toString();
		} catch (ConnectException ce) {
			
			System.out.println("https connection timed out.");
		} catch (Exception e) {
			
			System.out.println("https request error:{}" + e.getMessage());
		} finally {
			try {
				if(out != null){
					out.close();
				}
				if (in != null) {
					in.close();
				}
				if (conn != null) {
					conn.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		logger.debug("*********HTTPREQUEST END********");
		return null;
	}
	
	private static String hanleParams(Map<String, Object> paramMap, String paramsType, String charset){
		if(null == paramMap || paramMap.isEmpty()){
			return null;
		}
		
		if(paramsType.equals("JSON")){
			
			return JSONObject.toJSONString(paramMap);
		}else{
			StringBuilder outputStr = new StringBuilder();
			for (String paramKey : paramMap.keySet()) {
				
				Object paramValue = paramMap.get(paramKey);
				if(AppUtils.isBlank(paramValue)){
					paramValue = "";
				}
				
				try {
					outputStr.append(paramKey).append("=").append(URLEncoder.encode(paramValue.toString(), charset)).append("&");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			if(outputStr.length() > 0){
				outputStr.setLength(outputStr.length() - 1);
			}
			return outputStr.toString();
		}
		
	}
	
	private static String getContentType(String type){
		
		String contentType = "application/x-www-form-urlencoded;charset=UTF-8";
		
		if(type.equals("JSON")){
			
			contentType = "application/json;charset=UTF-8";
		}else if(type.equals("XML")){
			contentType = "application/json;charset=UTF-8";
		}
		
		return contentType;
	}
	 
    public static void main(String[] args) throws Exception {
    	
    	JSONObject params = new JSONObject();
    	
    	params.put("ak", "UTgBm4GLIaP7bzy9HAQkjBqNmTe3wYtB");
    	params.put("service_id", "159406");
    	params.put("entity_name", "2018030109575046441577");
    	params.put("entity_desc", "关开发测试");
    	String result = HttpUtil.httpPost("http://yingyan.baidu.com/api/v3/entity/add", params);
    	
    	System.out.println(result);
    	
	}
    

}
