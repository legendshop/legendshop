/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.event;

import java.io.Serializable;
import java.util.Date;

import com.legendshop.model.constant.SMSTypeEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * 短信载体, 移到processor中
 */
public class ShortMessageInfo implements Serializable {

	private static final long serialVersionUID = 1744146642494363080L;

	private String mobile;
	
	private String text;

	private String templateParam;
	
	private String userName;
	
	private String mobileCode;
	
	private String address;
	
	private String orderSn;
	
	private String prodName;
	
	private SMSTypeEnum smsType;

	private String requestInterface;

	private String ip;

	private Date time;
	
	public ShortMessageInfo(){
	  
	}

	
	//对于验证码来说，格式是固定的，这里无需配置
	public ShortMessageInfo(String mobile,String mobileCode,String userName, SMSTypeEnum smsType) {
		 String text = String.format("${siteName} 验证码为：%s, 有效时间是5分钟,请勿告诉他人。",mobileCode);
		 String templateParam =   String.format("{\"code\":\"%s\"}", mobileCode);
		 this.mobile = mobile;
		 this.mobileCode = mobileCode;
		 this.userName = userName;
		 this.smsType = smsType;
		 this.text = text;
		 this.templateParam = templateParam;
	}

	//对于验证码来说，格式是固定的，这里无需配置
	public ShortMessageInfo(String mobile,String mobileCode,String userName, SMSTypeEnum smsType,String requestInterface,String ip,Date time) {
		String text = String.format("${siteName} 验证码为：%s, 有效时间是5分钟,请勿告诉他人。",mobileCode);
		String templateParam =   String.format("{\"code\":\"%s\"}", mobileCode);
		this.mobile = mobile;
		this.mobileCode = mobileCode;
		this.userName = userName;
		this.smsType = smsType;
		this.text = text;
		this.templateParam = templateParam;
		this.requestInterface = requestInterface;
		this.ip = ip;
		this.time = time;
	}


	//门店订单，提货码短信
	public ShortMessageInfo(String mobile, String mobileCode, String userName, String address, String orderSn, SMSTypeEnum smsType) {
      String text = String.format("${siteName} 您的订单{%s}，提货码为：{%s}，门店地址：{%s}，如果非本人操作，请忽略此消息！",orderSn,mobileCode,address);
		String replaceString = replaceString(address, 19, "...");
		String templateParam =   String.format("{\"orderid\":\"%s\",\"code\":\"%s\",\"address\":\"%s\"}", orderSn,mobileCode,replaceString);
		 this.mobile = mobile;
		 this.mobileCode = mobileCode;
		 this.userName = userName;
		 this.address = address;
		 this.orderSn = orderSn;
		 this.smsType = smsType;
		 this.text = text;
		 this.templateParam = templateParam;
	}

	//发货通知
	public ShortMessageInfo(String mobile,String userName, String templateParam,String text, SMSTypeEnum smsType) {
		 this.mobile = mobile;
		 this.userName = userName;
		 this.smsType = smsType;
		 this.text = text;
		 this.mobileCode = "0";
		 this.templateParam = templateParam;
	}
	
	//到货通知
	public ShortMessageInfo(String mobile,  String prodName, SMSTypeEnum smsType) {

		String replaceString = replaceString(prodName, 15, "...");
		String templateParam=String.format("{\"prodName\":\"%s\"}", replaceString);
	  	String text = String.format("尊敬的客户，您的喜欢的商品 %s 已经到货啦.", prodName);
		 this.mobile = mobile;
		 this.prodName = prodName;
		 this.smsType = smsType;
		 this.text = text;
		 this.mobileCode = "0";
		 this.templateParam = templateParam;
	}

      public String getMobile() {
        return mobile;
      }
    
      public void setMobile(String mobile) {
        this.mobile = mobile;
      }
    
      public String getText() {
        return text;
      }
    
      public void setText(String text) {
        this.text = text;
      }
    
      public String getTemplateParam() {
        return templateParam;
      }
    
      public void setTemplateParam(String templateParam) {
        this.templateParam = templateParam;
      }
    
      public String getUserName() {
        return userName;
      }
    
      public void setUserName(String userName) {
        this.userName = userName;
      }
    
      public String getMobileCode() {
        return mobileCode;
      }
    
      public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
      }
    
      public String getAddress() {
        return address;
      }
    
      public void setAddress(String address) {
        this.address = address;
      }
    
      public String getOrderSn() {
        return orderSn;
      }
    
      public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
      }
    
      public String getProdName() {
        return prodName;
      }
    
      public void setProdName(String prodName) {
        this.prodName = prodName;
      }
      
      public void setSmsType(SMSTypeEnum smsType) {
        this.smsType = smsType;
      }

      public SMSTypeEnum getSmsType() {
        return smsType;
      }

	public String getRequestInterface() {
		return requestInterface;
	}

	public void setRequestInterface(String requestInterface) {
		this.requestInterface = requestInterface;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * 处理字符串超长后替换为指定字符
	 * @param resource 源字符串
	 * @param length 替换后的字符串长度
	 * @param replaceStr 指定字符
	 * @return
	 */
	private String replaceString(String resource,Integer length,String replaceStr){

		if(StringUtils.isNotBlank(resource)){
			if(resource.length() > length-replaceStr.length() ){
				return resource.substring(0,length-replaceStr.length())+replaceStr;
			}
		}
		return resource;
	}
	
}
