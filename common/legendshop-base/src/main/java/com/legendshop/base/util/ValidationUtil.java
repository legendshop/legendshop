/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 页面验证.
 */
public class ValidationUtil {
	 
 	/**
 	 * 检查是否是邮件
 	 *
 	 * @param email the email
 	 * @return true, if successful
 	 */
 	public static boolean checkEmail(String email){
	        Pattern pattern = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\\.[a-zA-Z0-9_-]{2,3}){1,2})$");
	        Matcher matcher = pattern.matcher(email);
	        if (matcher.matches()) {
	            return true;
	        }
	        return false;
	    }
	 
 	/**
 	 * 检查是否是数字或者字母
 	 * @param str
 	 * @return
 	 */
 	public static boolean checkAlpha(String str){
 		 Pattern pattern = Pattern.compile("^(?!_)(?!.*?_$)[a-zA-Z0-9_]+$");
	     Matcher matcher = pattern.matcher(str);
	        if (matcher.matches()) {
	            return true;
	        }
	        return false;
 	}
 	
 	
 	/**
 	 * 检查是否是电话
 	 * @param phone
 	 * @return
 	 */
 	public static boolean checkPhone(String phone){
	        Pattern pattern = Pattern.compile("^1\\d{10}$");
	        Matcher matcher = pattern.matcher(phone);
	        if (matcher.matches()) {
	            return true;
	        }
	        return false;
	    }
 	
 	
	/**
	 * 昵称可以是数字，英文或者中文，不能是全数字
	 * @param nickName
	 * @return
	 */
	public static boolean checkNickName(String nickName){
		return nickName.matches("^(?!\\d+$)[\\u4e00-\\u9fa5a0-9a-zA-Z]+$"); //是否只有数字，英文或者中文
	}


	/**
	 * 
	 * 用户名可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头
	 * @param userName
	 * @return
	 * 原来的^(?![_0-9])(?!.*?_$)[a-zA-Z0-9_\u4e00-\u9fa5]{4,16}$
	 */
	public static boolean checkUserName(String userName){
		return userName.matches("^[a-zA-Z][\\dA-Za-z_]{3,16}$"); //是否只有数字，英文或者中文
	}

	 /**
 	 * The main method.
 	 *
 	 * @param args the arguments
 	 */
 	public static void main(String[] args) {
		System.out.println(checkNickName("123123123好吗"));
		System.out.println(checkAlpha("q_asderfe"));
		
	}


}
