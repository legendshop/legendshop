/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.base.compare;

/**
 * 两个对象列表的对照比较.
 *
 * @param <T> the generic type
 * @param <D> the generic type
 */
public interface DataComparer<T, D> {
	
	/**
	 * 是否需要更新.
	 *
	 * @param dto the dto
	 * @param dbObj the db obj
	 * @param obj the obj
	 * @return true, if successful
	 */
	public boolean needUpdate(T dto, D dbObj, Object obj);
		
		/**
		 * 在新的对象列表中是否存在.
		 *
		 * @param dto the dto
		 * @param dbObj the db obj
		 * @return true, if is exist
		 */
		public boolean isExist(T dto, D dbObj);
		
		
		
		/**
		 * 复制到新的数据模型.
		 *
		 * @param dtoj the dtoj
		 * @param obj the obj
		 * @return the d
		 */
		public D copyProperties(T dtoj, Object obj);
		
		

}
