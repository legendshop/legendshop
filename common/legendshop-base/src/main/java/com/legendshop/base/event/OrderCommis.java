package com.legendshop.base.event;

import java.util.List;

/**
 * 订单分佣
 *
 */
public class OrderCommis {
	
	private List<String> subNumbers;
	
	private String userId;

	public List<String> getSubNumbers() {
		return subNumbers;
	}

	public void setSubNumbers(List<String> subNumbers) {
		this.subNumbers = subNumbers;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public OrderCommis(List<String> subNumbers, String userId) {
		super();
		this.subNumbers = subNumbers;
		this.userId = userId;
	}
}
