package com.legendshop.base.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DefaultAddressDTO implements Serializable {

	private static final long serialVersionUID = -5963900059693889089L;
	/**
	 * 省名称
	 */
	private String ipProvince;
	/**
	 * 市名称
	 */
	private String ipCity;
	/**
	 * 区名称
	 */
	private String ipArea;
	/**
	 * 省名称id
	 */
	private Integer ipProvinceId;
	/**
	 * 市名称id
	 */
	private Integer ipCityId;
	/**
	 * 区名称id
	 */
	private Integer ipAreaId;
}
