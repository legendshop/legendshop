/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.base.config;

import com.legendshop.framework.handler.impl.ContextServiceLocator;

/**
 * 数据库参数配置工厂类
 *
 */
public class SystemParameterUtilManager {
	
	private static SystemParameterUtil systemParameterUtil;
	
	/**
	 * 获取系统配置的工具类
	 */
	public static SystemParameterUtil getSystemParameterUtil() {
		if(systemParameterUtil == null){
			synchronized (SystemParameterUtilManager.class) {
				if(systemParameterUtil == null) {
					systemParameterUtil = (SystemParameterUtil) ContextServiceLocator.getBean(SystemParameterUtil.class, "systemParameterUtil");
				}
			}
		}
		return systemParameterUtil;
	}


	
}
