/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base;

import org.springframework.core.convert.converter.Converter;

import com.legendshop.util.AppUtils;

/**
 * xssfilter 转移
 */
public class XssStringConverter implements Converter<String, String> {
	
	
	/**
	 * 转移
	 */
	public String convert(String source) {
		if(AppUtils.isBlank(source)) {
			return null;
		}
		String result = cleanXSS(source);
		return result;
	}
	
	/**
	 * 过滤Html javascript 和sql
	 *
	 */
	private  String cleanXSS(String value) {
          //You'll need to remove the spaces from the html entities below
		  value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
		  value = value.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
		  value = value.replaceAll("'", "&#39;");
		  value = value.replaceAll("\"", "&#34;");
		  value = value.replaceAll("eval\\((.*)\\)", "");
		  value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		  return value;
	}

}