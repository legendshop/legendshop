package com.legendshop.base.util;

/**
 * The Interface MailHelper.
 */
public interface MailHelper {
	
	/**
	 * Gets the mail address.
	 *
	 * @param mailPrefix the mail prefix
	 * @return the mail address
	 */
	public String getMailAddress(String mailPrefix);
}
