/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

/**
 * 秒杀常量
 *
 */
public final class SeckillConstant {

	public static final String SECKILL_DETAILS_ = "SECKILL_DETAILS_";
	
	public static final String SECKILL_ = "SECKILL_";
	
	
	public static final String SECKILL_STOCK_ = "SECKILL_STOCK_";
	
	public static final String SECKILL_BUY_USERS_ = "SECKILL_BUY_USERS_";
	
	public static final String SECKILL_SUCCESS_ = "SECKILL_SUCCESS_";
	
	public static final String SECKILL_CART_ = "SECKILL_CART_";

	/** 冲突键. */
	public static final String MUTEX_KEY_PREFIX = "SECKILL_MUTEX_";

	public static final String SECKILL_SESSION_TOKEN = "SECKILL_SESSION_TOKEN";

}
