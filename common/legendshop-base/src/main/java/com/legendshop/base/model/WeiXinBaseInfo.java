/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.model;

import java.io.Serializable;

/**
 * 微信的基本信息.
 */
public class WeiXinBaseInfo implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The app id. */
	private String appId;
	
	/** The app secret. */
	private String appSecret;
	
	/** The token. */
	private String token;
	
	/** The partner key. */
	private String partnerKey;
	
	/** The partner id. */
	private String partnerId;

	/**
	 * Gets the app id.
	 *
	 * @return the app id
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * Sets the app id.
	 *
	 * @param appId the app id
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * Gets the app secret.
	 *
	 * @return the app secret
	 */
	public String getAppSecret() {
		return appSecret;
	}

	/**
	 * Sets the app secret.
	 *
	 * @param appSecret the app secret
	 */
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token the token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Gets the partner key.
	 *
	 * @return the partner key
	 */
	public String getPartnerKey() {
		return partnerKey;
	}

	/**
	 * Sets the partner key.
	 *
	 * @param partnerKey the partner key
	 */
	public void setPartnerKey(String partnerKey) {
		this.partnerKey = partnerKey;
	}

	/**
	 * Gets the partner id.
	 *
	 * @return the partner id
	 */
	public String getPartnerId() {
		return partnerId;
	}

	/**
	 * Sets the partner id.
	 *
	 * @param partnerId the partner id
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

}
