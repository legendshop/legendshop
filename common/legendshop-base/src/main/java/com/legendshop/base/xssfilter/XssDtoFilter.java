package com.legendshop.base.xssfilter;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.model.dto.ShopSearchParamDto;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.util.JSONUtil;

import java.util.Map;
import java.util.Set;

/**
 * 过滤参数用
 */
public class XssDtoFilter {


	/**
	 * 加密商品查询的参数
	 *
	 * **/
	public static ProductSearchParms safeProductSearchParms(ProductSearchParms params){


		params.setTag(XssFilterUtil.cleanXSS(params.getTag()));
		params.setAreaid(XssFilterUtil.cleanXSS(params.getAreaid()));
		params.setBrandName(XssFilterUtil.cleanXSS(params.getBrandName()));
		params.setKeyword(XssFilterUtil.cleanXSS(params.getKeyword()));
		params.setLoadType(XssFilterUtil.cleanXSS(params.getLoadType()));
		params.setOrders(XssFilterUtil.cleanXSS(params.getOrders()));
		params.setCityid(XssFilterUtil.cleanXSS(params.getCityid()));
		params.setProvinceid(XssFilterUtil.cleanXSS(params.getProvinceid()));
		params.setProp(XssFilterUtil.cleanXSS(params.getProp()));

		Map<String, String[]> requestParams = params.getRequestParams();
		if(requestParams != null){
			Set<String> keys = requestParams.keySet();
			if(keys != null){
				for (String key : keys) {
					String[] values = requestParams.get(key);
					if(values != null){
						for (int i = 0; i < values.length; i++) {
							values[i] = XssFilterUtil.cleanXSS(values[i]);
						}
					}
				}
			}
		}

		return params;
	}

	/**
	 * 商家转换
	 * @param searchParams
	 * @return
	 */
	public static ShopSearchParamDto safeProductSearchParms(ShopSearchParamDto searchParams) {
		searchParams.setOrderWay(XssFilterUtil.cleanXSS(searchParams.getOrderWay()));
		searchParams.setSearchContent(XssFilterUtil.cleanXSS(searchParams.getSearchContent()));
		return searchParams;
	}

	public static GroupProdSeachParam safeProductSearchParms(GroupProdSeachParam searchParams) {
		searchParams.setOrders(XssFilterUtil.cleanXSS(searchParams.getOrders()));
		searchParams.setGroupKeyWord(XssFilterUtil.cleanXSS(searchParams.getGroupKeyWord()));
		return searchParams;

	}
}
