package com.legendshop.base.util;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.util.AppUtils;

/**
 * 分页相关的工具类
 * @author 开发很忙
 *
 */
public class PageUtils {

	/**
	 * 转换PageSupport对象
	 * @param ps 
	 * @return 转成APP的查询结果对象 
	 */
	public static <F,T> AppPageSupport<T> convertPageSupport(PageSupport<F> ps, ResultListConvertor<F,T> resultListConvertor){
		if(AppUtils.isNotBlank(ps)){
			AppPageSupport<T> prodListDto = new AppPageSupport<T>();
			prodListDto.setPageSize(ps.getPageSize());
			prodListDto.setPageCount(ps.getPageCount());
			prodListDto.setCurrPage(ps.getCurPageNO());
			prodListDto.setTotal(ps.getTotal());
			
			List<T> resultList = resultListConvertor.convert(ps.getResultList());
			prodListDto.setResultList(resultList);
			
			return prodListDto;
		}
		
		return null;
	}
	
	/**
	 * 转换PageSupport对象内容
	 * @param ps 
	 * @return 
	 */
	public static <F,T> PageSupport<T> convert(PageSupport<F> ps, ResultListConvertor<F,T> resultListConvertor){
		if(AppUtils.isNotBlank(ps)){
			
			PageProvider pageProvider = new DefaultPagerProvider(ps);
			List<T> list = new ArrayList<T>();
			PageSupport<T> pageSupport = new PageSupport<T>(list, pageProvider);
			
			List<T> resultList = resultListConvertor.convert(ps.getResultList());
			pageSupport.setResultList(resultList);
			
			return pageSupport;
		}
		
		return null;
	}

	/**
	 * 只转换PageSupport
	 * @param ps
	 * @return 转成APP的查询结果对象
	 */
	public static <T> AppPageSupport<T> convertPageSupport(PageSupport<T> ps){

		if(AppUtils.isNotBlank(ps)){
			AppPageSupport<T> appPageSupport  = new AppPageSupport<T>();
			appPageSupport.setPageSize(ps.getPageSize());
			appPageSupport.setPageCount(ps.getPageCount());
			appPageSupport.setCurrPage(ps.getCurPageNO());
			appPageSupport.setTotal(ps.getTotal());
			appPageSupport.setResultList(ps.getResultList());

			return appPageSupport;
		}
		return null;
	}
	
	
	public static interface ResultListConvertor<F, T> {
		
		List<T> convert(List<F> form);
	}
	
	
	

}
