package com.legendshop.base.util.des;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.legendshop.util.converter.ByteConverter;
import com.legendshop.util.des.DES2;

/**
 * 加解密可逆算法
 *
 */
@Component
public class EncryptHelper{
	
	
	private DES2 des = null;
	
	public EncryptHelper(){
		des = new DES2();
	}
	
	public EncryptHelper(@Value("LEGENDSP")  String key){
		des = new DES2(key);
	}
	
	/**
	 * 加密
	 * @param source
	 * @return
	 */
	public String  encrypt(String source){
		return des.byteToString(des.createEncryptor(source));
	}
	
	/**
	 * 解密
	 * @param source
	 * @return
	 */
	public String  decrypt(String source){
		String aimStr = ByteConverter.encode(source);
		String decodeAim = ByteConverter.decode(aimStr);
		String decryptorString = new String(des.createDecryptor(des.stringToByte(decodeAim)));
		return decryptorString;
	}
	
	public static void main(String[] args) {
		String source = "e8056529f2629f0bb7abab7586133a3a";
		EncryptHelper helper = new EncryptHelper("ls123456");
		String encryptStr = helper.encrypt(source);
		System.out.println("encryptStr = " + encryptStr);
		System.out.println("decryptStr = " + helper.decrypt(encryptStr));
		
		String APPSECRET = helper.decrypt("Qr1EO1Pi6XtJCvXqCxMcyujQ29F93Lh3zSEBBC6km6zh0ALkpahfyA==");
		System.out.println(APPSECRET);
		
		
	}


}

