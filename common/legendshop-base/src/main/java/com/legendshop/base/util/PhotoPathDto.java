/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

import java.io.Serializable;

/**
 * 计算出文件的类型和路径.
 */
public class PhotoPathDto implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4778952063074194336L;

	/** The type. */
	private PhotoPathEnum type;
	
	/** The file path. */
	private String filePath;
	
	/**
	 * 构造函数
	 * @param type
	 * @param filePath
	 */
	public PhotoPathDto(PhotoPathEnum type, String filePath) {
		this.type = type;
		this.filePath = filePath;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public PhotoPathEnum getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the type
	 */
	public void setType(PhotoPathEnum type) {
		this.type = type;
	}

	/**
	 * Gets the file path.
	 *
	 * @return the file path
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 *
	 * @param filePath the file path
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
