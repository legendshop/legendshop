/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.util;

/**
 * 计算图片资源路径等，因为不用变,所以固定死在代码里
 */
public class ResourcePathUtil {

	public static final String DOWNLOAD_PATH = "WEB-INF/download";

	/**
	 * 获取缩略图的前缀.
	 * 
	 * #缩略图前缀，采用ImagesServlet来提供服务，后面必须带上"/"  IMAGES_PATH_PREFIX=/photoserver/images/
	 */
	public static String getSmallImagePathPrefix() {
		return "/photoserver/images/";
	}

	// 读取的大图片路径
	public static String getPhotoPathPrefix() {
		return "/photoserver/photo/";
	}

	public static String getSnapPhotoPathPrefix() {
		return "/photoserver/snapshot/";
	}

}
