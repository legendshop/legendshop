package com.legendshop.base.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.legendshop.base.constants.SysParameterTypeEnum;
import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.framework.cache.caffeine.CacheRedisCaffeineProperties;
import com.legendshop.model.constant.SystemCacheEnum;
import com.legendshop.model.dto.SystemParameterDto;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.util.AppUtils;
/**
 *  数据库中的数据字典
 *
 */
@Component
public class SystemParameterProvider {
	
	/** 从数据库中获取的对象在本地的一级缓存. */
	private  Map<String, Object> commonObject = new ConcurrentHashMap<String, Object>();
	
	@Autowired
	private  RedisTemplate<Object, Object> redisTemplate;
	
	@Autowired
	private CacheRedisCaffeineProperties cacheRedisCaffeineProperties;
	
	
	/**
	 * 系统初始化时设置缓存,如无必要，无需刷新缓存时间.
	 *
	 */
	public  void setParameter(List<SystemParameter> systemParameterList) {
		if(systemParameterList != null){
			for (SystemParameter parameter : systemParameterList) {
				refreshParameter(parameter,true);
			}
		}
		
	}

	
	/**
	 * 页面主动发起变更，因此需要通知其他节点更新缓存.
	 * 
	 * @param parameter
	 *            the parameter
	 * @param propertiesUpdater
	 *            the properties updater
	 */
	public  void setParameter(SystemParameter parameter) {
		refreshParameter(parameter,false);
	}
	
	
	/**
	 * 更新缓存
	 * @param parameter
	 */
	private void refreshParameter(SystemParameter parameter, boolean setLocal) {
		String type = parameter.getType();
		if (SysParameterTypeEnum.Integer.name().equalsIgnoreCase(type)) {
			try {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Integer.valueOf(parameter.getValue()),setLocal);
			} catch (Exception e) {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Integer.valueOf(parameter.getOptional()),setLocal);
			}

		} else if (SysParameterTypeEnum.Boolean.name().equalsIgnoreCase(type)) {
			refreshLocalCache(parameter.getGroupId(), parameter.getName(), Boolean.valueOf(parameter.getValue()),setLocal);
		} else if (SysParameterTypeEnum.Double.name().equalsIgnoreCase(type)) {
			try {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Double.valueOf(parameter.getValue()),setLocal);
			} catch (Exception e) {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Double.valueOf(parameter.getOptional()),setLocal);
			}
		} else if (SysParameterTypeEnum.Long.name().equalsIgnoreCase(type)) {
			try {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Long.valueOf(parameter.getValue()),setLocal);
			} catch (Exception e) {
				refreshLocalCache(parameter.getGroupId(), parameter.getName(), Long.valueOf(parameter.getOptional()),setLocal);
			}

		} else if (SysParameterTypeEnum.List.name().equalsIgnoreCase(type)) {
			// 以英文逗号分隔
			List<String> list = new ArrayList<String>();
			if (AppUtils.isNotBlank(parameter.getValue())) {
				String[] values = parameter.getValue().split(",");
				for (String str : values) {
					list.add(str);
				}
			}
			refreshLocalCache(parameter.getGroupId(), parameter.getName(), list,setLocal);
		} else {// String default and Selection and password
			refreshLocalCache(parameter.getGroupId(), parameter.getName(), parameter.getValue(),setLocal);
		}
	}
	
	/**
	 * 是否刷新本地缓存
	 * @param groupId 分组,用于在更新端设置是否有更新的逻辑
	 * @param key 关键字
	 * @param value 值
	 * @param setLocal 是否设置本地的缓存,可以通过redis来通知其他节点也同步更新
	 */
	public  void refreshLocalCache(String groupId, String key, Object value, boolean setLocal) {
		if(setLocal){
			commonObject.put(key, value);
		}else{
			String topic = cacheRedisCaffeineProperties.getRedis().getSystemTopic();
			if (AppUtils.isNotBlank(topic)) {
				redisTemplate.convertAndSend(topic, new CacheMessage(SystemCacheEnum.PROPERTIES.value(), new SystemParameterDto(groupId, key, value)));
			}
		}
	}

	/**
	 * 只能同package的包访问
	 * @return
	 */
    Map<String, Object> getCommonObject() {
		return commonObject;
	}

	public void setCommonObject(Map<String, Object> commonObject) {
		this.commonObject = commonObject;
	}
	

}
