package com.legendshop.base.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;

/**
 * 根据ID更新
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Caching(evict = { 
		@CacheEvict(value = "ProductDetail", key = "#prodId"),
		@CacheEvict(value = "Product", key = "#prodId"),
		@CacheEvict(value = "ProductDto", key = "#prodId")
	})
public @interface ProductIdUpdate {

}
