package com.legendshop.base.constants;

/**
 * 页面参数枚举
 */
public enum PagesParameterEnum {

    //前端模板页
    FRONTEND_TEMPLET("main"),

    //后端模板页
    BACKEND_TEMPLET("admin");

    private String value;

    PagesParameterEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
