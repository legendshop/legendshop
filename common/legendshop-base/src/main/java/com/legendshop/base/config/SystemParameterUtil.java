/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.base.config;

import com.legendshop.base.constants.PagesParameterEnum;
import com.legendshop.base.constants.SysParameterEnum;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 数据库参数配置
 *
 */
@Component("systemParameterUtil")
public class SystemParameterUtil {
	
	@Autowired 
	private SystemParameterProvider systemParameterProvider;
	
	/**
	 * sms短信配置,是否发送短信
	 */
	
	public Boolean sendSms() {
		return getObject(SysParameterEnum.SEND_SMS, Boolean.class);
	}

	/**
	 * 获取短信验证码模板
	 */
	
	public String getValSmsTempateFormat() {
		return getObject(SysParameterEnum.VAL_SMS_TEMPATE_FORMAT, String.class);
	}
	
	/**
	 * 获取短信验证码模板
	 */
	
	public String getValEmailTempateFormat() {
		return getObject(SysParameterEnum.VAL_EMAIL_TEMPATE_FORMAT, String.class);
	}

	/**
	 * 获取短信今天能发送的限额
	 */
	
	public Integer getSmsTodayLimit() {
		return getObject(SysParameterEnum.SMS_TODAY_LIMIT, Integer.class);
	}

	/**
	 * 超时时间，单位为分钟
	 */
	public Integer getSmsUsefulMin() {
		return getObject(SysParameterEnum.SMS_USE_FUL_MIN, Integer.class);
	}
	
	
	/**
	 * 获取短信的超时时间， 单位分钟 min
	 */
	
	public Integer getSmsBetweenMin() {
		return getObject(SysParameterEnum.SMS_BETWEEN_MIN, Integer.class);
	}

	/**
	 * 获取短信最大发送的条数
	 */
	
	public Integer getSmsMaxSendNum() {
		return getObject(SysParameterEnum.SMS_MAX_SEND_NUM, Integer.class);
	}
	

	/**
	 * 是否发送邮件.
	 * 
	 * @return true, if successful
	 */
	public Boolean sendMail() {
		return getObject(SysParameterEnum.SEND_MAIL, Boolean.class);
	}

	/**
	 * 默认商家名字, 空表示是平台发送的数据
	 */
	public String getDefaultShopName() {
		return "";
	}

	/**
	 * 0代表是平台的数据
	 * 
	 * @return
	 */
	public Long getDefaultShopId() {
		return 0l;
	}

	public Boolean isProdRequireAudit() {
		return getObject(SysParameterEnum.PROD_REQUIRE_AUDIT, Boolean.class);
	}

	/**
	 * 获取商城结算日
	 */
	public Integer getShopBillDay() {
		return getObject(SysParameterEnum.SHOP_BILL_DAY, Integer.class);
	}
	
	/**
	 * 获取商家结算周期类型
	 */
	public String getShopBillPeriodType() {
		return getObject(SysParameterEnum.BILL_PERIOD_TYPE, String.class);
	}

	/**
	 * 获取商城结算佣金比例
	 */
	public Double getShopCommisRate() {
		Double result = getObject(SysParameterEnum.SHOP_COMMIS_RATE, Double.class);
		return result == null ? 0d : result;
	}

	/**
	 * 获取分销结算日
	 */
	public Integer getDistributionSettlementDay() {
		return getObject(SysParameterEnum.DISTRIBUTION_SETTLEMENT_DAY, Integer.class);
	}

	/**
	 * 得到下载文件路径
	 */
	public String getDownloadFilePath() {
		return getObject(SysParameterEnum.DOWNLOAD_PATH, String.class);
	}
	
	
	/**
	 * 金钱的模式.
	 * 
	 * @return the currency pattern
	 */
	public String getCurrencyPattern() {
		return getObject(SysParameterEnum.CURRENCY_PATTERN, String.class);
	}

	/**
	 * 是否读取图片的高宽
	 * 
	 * @return
	 */
	public Boolean isReadImgWidthHeight() {
		return getObject(SysParameterEnum.IS_READ_IMG_WIDTH_HEIGHT, Boolean.class);
	}

	/**
	 * 获取前台模板
	 * 
	 */
	public String getFrontEndTemplet() {
		return PagesParameterEnum.FRONTEND_TEMPLET.getValue();
	}


	/**
	 * 获取手机端前台模板, 手机端改为vue, 不再支持JSP版本 TODO
	 * 
	 */
	public String getMobileFrontEndTemplet() {
		return "red";
	}

	/**
	 * 获取后台模板
	 * 
	 */
	public String getBackEndTemplet() {
		return PagesParameterEnum.BACKEND_TEMPLET.getValue();
	}

	/**
	 * 模拟支付
	 */
	public Boolean getBooleanPay() {
		return getObject(SysParameterEnum.BOOLEAN_PAY, Boolean.class);
	}

	/**
	 * 获取后台统计报表的统计时段
	 */
	public String getDashboardDate() {
		String dashboardDate = getObject(SysParameterEnum.DASHBOARD_STATISTICS_DAYS, String.class);
		if (AppUtils.isBlank(dashboardDate)) {
			return "-30";
		}
		return dashboardDate;

	}

	/** 评论是否需要审核 */
	
	public Boolean isCommentNeedReview() {
		return getBooleanObject(SysParameterEnum.COMMENT_NEED_REVIEW, false);
	}

	/**
	 * 判断是否支持开店，不支持开店，给出友好提示
	 */
	
	public Boolean isSupportOpenShop() {
		return getObject(SysParameterEnum.OPEN_SHOP, Boolean.class);
	}

	
	public String getQqAppId() {
		return getObject(SysParameterEnum.QQ_APP_ID, String.class);
	}
	
	
	public String getQqSecret() {
		return getObject(SysParameterEnum.QQ_APP_SECRET, String.class);
	}
	
	
	public String getQqH5AppId() {
		return getObject(SysParameterEnum.QQ_H5_APP_ID, String.class);
	}
	
	
	public String getQqH5Secret() {
		return getObject(SysParameterEnum.QQ_H5_APP_SECRET, String.class);
	}

	
	public Boolean getCloseSmsValidateCode() {
		return getObject(SysParameterEnum.CLOSE_SMS_VALIDATE_CODE, Boolean.class);
	}

	
	public String getWeixinLoginAppId() {
		return getObject(SysParameterEnum.WEIXIN_LOGIN_APPID, String.class);
	}

	
	public Integer getPageSize() {
		return getObject(SysParameterEnum.PAGE_SIZE, Integer.class);
	}

	
	public Integer getMaxIndexJpg() {
		return getObject(SysParameterEnum.MAX_INDEX_JPG, Integer.class);
	}

	
	public Integer getExportSize() {
		return getObject(SysParameterEnum.EXPORT_SIZE, Integer.class);
	}

	
	public String getMailHost() {
		return getObject(SysParameterEnum.MAIL_HOST, String.class);
	}

	
	public String getMailStmpAuth() {
		return (getBooleanObject(SysParameterEnum.MAIL_STMP_AUTH, false)).toString();
	}

	
	public String getMailStmpTimeout() {
		return getObject(SysParameterEnum.MAIL_STMP_TIMEOUT, String.class);
	}

	
	public Integer getMailPort() {
		return getObject(SysParameterEnum.MAIL_PORT, Integer.class);
	}

	
	public String getMailPassword() {
		return getObject(SysParameterEnum.MAIL_PASSWORD, String.class);
	}

	
	public Boolean getIndependDonain() {
		return getBooleanObject(SysParameterEnum.INDEPEND_DOMAIN, false);
	}

	
	public Double getAppStartAdv() {
		return getObject(SysParameterEnum.APP_START_ADV, Double.class);
	}

	
	public Long getMaxFileSize() {
		return getObject(SysParameterEnum.MAX_FILE_SIZE, Long.class);
	}

	
	public Integer getFrontPageSize() {
		return getObject(SysParameterEnum.FRONT_PAGE_SIZE, Integer.class);
	}

	
	public List getAllowedUploadFileType() {
		return getObject(SysParameterEnum.ALLOWED_UPLOAD_FILE_TPYE, List.class);
	}

	
	public Boolean getCloseValidateCode() {
		return getBooleanObject(SysParameterEnum.CLOSE_VALIDATE_CODE, false);
	}

	
	public Boolean getValidationImage() {
		return getBooleanObject(SysParameterEnum.VALIDATION_IMAGE, false);
	}

	
	public Boolean isLoginLogEnable() {
		return getBooleanObject(SysParameterEnum.LOGIN_LOG_ENABLE, false);
	}

	
	public Integer getGroupSurvivalTime() {
		return getObject(SysParameterEnum.GROUP_SURVIVAL_TIME, Integer.class);
	}

	
	public String getCommentLevel() {
		return getObject(SysParameterEnum.COMMENT_LEVEL, String.class);
	}

	
	public Boolean getUseScore() {
		return getBooleanObject(SysParameterEnum.USE_SCORE, false);
	}

	
	public Boolean isValidationFromMail() {
		return getBooleanObject(SysParameterEnum.VALIDATION_FROM_MAIL, false);
	}

	
	public Boolean isDeliverProdSms() {
		return getBooleanObject(SysParameterEnum.DELIVER_GOODS_SMS.name());
	}

	
	public String getMailName() {
		return getObject(SysParameterEnum.MAIL_NAME, String.class);
	}

	
	public Boolean isQqLoginEnable() {
		return getObject(SysParameterEnum.QQ_LOGIN_ENABLE, Boolean.class);
	}

	
	public Boolean isWeiboLoginEnable() {
		return getObject(SysParameterEnum.WEIBO_LOGIN_ENABLE, Boolean.class);
	}

	
	public Boolean isWeixinLoginEnable() {
		return getObject(SysParameterEnum.WEIXIN_LOGIN_ENABLE, Boolean.class);
	}

	
	public Boolean isAuctionRequereAudit() {
		return getBooleanObject(SysParameterEnum.AUCTION_REQUIRE_AUDIT, false);
	}

	
	public String getSiteName() {
		return getObject(SysParameterEnum.SITE_NAME, String.class);
	}

	public Boolean isVisitLogEnable() {
		return getBooleanObject(SysParameterEnum.VISIT_LOG_ENABLE, false);
	}

	
	public String getWeixinLoginAppsecret() {
		return getObject(SysParameterEnum.WEIXIN_LOGIN_APPSECRET, String.class);
	}

	
	public String getWeiboAppId() {
		return getObject(SysParameterEnum.WEIBO_APP_ID, String.class);
	}

	
	public String getWeiboSecret() {
		return getObject(SysParameterEnum.WEIBO_SECRET, String.class);
	}
	
	
	public String getWeiboH5AppId() {
		return getObject(SysParameterEnum.WEIBO_H5_APP_ID, String.class);
	}
	
	
	public String getWeiboH5Secret() {
		return getObject(SysParameterEnum.WEIBO_H5_SECRET, String.class);
	}

	
	public String getWeiboRedirectUrl() {
		return getObject(SysParameterEnum.WEIBO_REDIRECT_URL, String.class);
	}

	
	public String getResourceVersion() {
		return getObject(SysParameterEnum.RESOURCE_VERSION, String.class);
	}
	
	public String getSystemVersion() {
		return getObject(SysParameterEnum.SYSTEM_VERSION, String.class);
	}

	
	public String getImgAllowType() {
		return getObject(SysParameterEnum.IMG_ALLOW_TYPE, String.class);
	}

	
	public String getImgAllowSize() {
		return getObject(SysParameterEnum.IMG_ALLOW_SIZE, String.class);
	}

	
	public String getVideoAllowType() {
		return getObject(SysParameterEnum.VIDEO_ALLOW_SIZE, String.class);
	}

	
	public String getVideoAllowSize() {
		return getObject(SysParameterEnum.VIDEO_ALLOW_SIZE, String.class);
	}


	/**
	 * 获取数据库的通用配置
	 * 
	 * @param                  <T> the generic type
	 * @param sysParameterEnum the parameter enum
	 * @param clazz            the clazz
	 * @return the object
	 */
	@SuppressWarnings("unchecked")
	private <T> T getObject(SysParameterEnum sysParameterEnum, Class<T> clazz) {
		T result = (T) systemParameterProvider.getCommonObject().get(sysParameterEnum.name());
		return result;
	}

	/**
	 * Gets the Boolean object.
	 * 
	 * @param parameterEnum the parameter enum
	 * @return the Boolean object
	 */
	private Boolean getBooleanObject(String parameter) {
		return (Boolean) systemParameterProvider.getCommonObject().get(parameter);
	}

	/**
	 * 采用默认值，获取Boolean值
	 * 
	 * @param parameter
	 * @param defaultValue
	 * @return
	 */
	private Boolean getBooleanObject(SysParameterEnum parameter, Boolean defaultValue) {
		Boolean result = getBooleanObject(parameter.name());
		if (result == null) {
			return defaultValue;
		}
		return result;
	}
	
	
	
}
