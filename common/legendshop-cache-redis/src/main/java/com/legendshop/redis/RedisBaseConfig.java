/*
 * LegendShop SAAS商城系统
 *
 * 广州朗尊软件科技有限公司. 版权所有,并保留所有权利。
 *
 */

package com.legendshop.redis;


import com.legendshop.config.dto.RedisPropertiesDto;
import com.legendshop.framework.cache.caffeine.CacheRedisCaffeineProperties;
import com.legendshop.framework.cache.caffeine.Caffeine;
import com.legendshop.framework.cache.caffeine.Redis;
import com.legendshop.framework.cache.caffeine.RedisCaffeineCacheManager;
import com.legendshop.framework.cache.client.RedisCacheClient;
import com.legendshop.framework.cache.listener.CacheMessageListener;
import com.legendshop.framework.cache.listener.SystemCacheMessageListener;
import com.legendshop.framework.cache.serializer.KryoSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * Redis的配置
 */
@Configuration
@Slf4j
@ImportResource({ "classpath:spring/applicationContext-caffeine-cache.xml" })
public class RedisBaseConfig {

	@Autowired
	private RedisPropertiesDto propertiesDto;

	@Autowired(required = false)
	private TaskExecutor redisTaskExecutor;

	/**
	 * 超时时间配置
	 */
	@Resource(name = "expires")
	private Map<String, Long> expires;

	//读取默认的application.properties文件的redis的配置参数
	@Bean
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory, RedisSerializer redisSerializer) {
		//设置序列化

		//配置redisTemplate
		RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();

		// 配置连接工厂
		redisTemplate.setConnectionFactory(redisConnectionFactory);
		RedisSerializer stringSerializer = new StringRedisSerializer();
		redisTemplate.setKeySerializer(stringSerializer);//key序列化
		redisTemplate.setValueSerializer(redisSerializer);//value序列化
		redisTemplate.setHashKeySerializer(stringSerializer);//Hash key序列化
		redisTemplate.setHashValueSerializer(redisSerializer);//Hash value序列化
		redisTemplate.afterPropertiesSet();
		return redisTemplate;
	}


	/**
	 * 序列化方式
	 * @return
	 */
	@Bean
	public RedisSerializer redisSerializer(){
		return new KryoSerializer();
	}

	/**
	 * 缓存的K操作类
	 * @param redisTemplate
	 * @param redisSerializer
	 * @return
	 */
	@Bean
	public RedisCacheClient redisCacheClient(RedisTemplate<Object, Object> redisTemplate, RedisSerializer redisSerializer){
		RedisCacheClient cacheClient = new RedisCacheClient();
		cacheClient.setRedisTemplate(redisTemplate);
		cacheClient.setSerializer(redisSerializer);
		return cacheClient;
	}

	@Bean
	public RedisCaffeineCacheManager cacheManager(CacheRedisCaffeineProperties cacheRedisCaffeineProperties, RedisTemplate<Object, Object> redisTemplate){
		return new RedisCaffeineCacheManager(cacheRedisCaffeineProperties, redisTemplate);
	}

	/**
	 * 缓存属性配置
	 * @return
	 */
	@Bean
	public CacheRedisCaffeineProperties cacheRedisCaffeineProperties(){
		CacheRedisCaffeineProperties properties = new CacheRedisCaffeineProperties();
		//缓存前缀
		properties.setCachePrefix(propertiesDto.getCachePrefix());

		//是否动态根据cacheName创建Cache的实现
		properties.setDynamic(true);

		Redis redis = new Redis();
		//开启二级缓存
		redis.setCachable(true);
		redis.setTopic(propertiesDto.getCacheTopic());
		redis.setSystemTopic(propertiesDto.getSystemTopic());

		properties.setRedis(redis);


		Caffeine caffeine = new Caffeine();

		//开启一级缓存
		caffeine.setCachable(false);
		//一级缓存和二级缓存的时间比例，一级缓存要比二级缓存的生命周期要短
		caffeine.setTimeScale(0.6f);
		caffeine.setExpireAfterAccess(100);
		caffeine.setExpireAfterWrite(110);
		caffeine.setRefreshAfterWrite(120);
		caffeine.setMaximumSize(10000);
		caffeine.setInitialCapacity(5);

		properties.setCaffeine(caffeine);

		properties.setExpires(expires);

		return  properties;
	}




}
