package com.legendshop.redis;

import com.legendshop.config.dto.RedisPropertiesDto;
import com.legendshop.framework.cache.client.RedisCacheClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 用户是否在线工具类
 */
@Component
@Slf4j
public class UserOnlineStatusHelper {

	@Autowired
	private RedisCacheClient cacheClient;

	@Autowired
	private RedisPropertiesDto propertiesDto;

	/** 单位是秒, 默认是7天 */
	protected static final int LOGIN_STATUS_VALID_TIME = 60 * 60 * 24 * 7;


	/**
	 * 检查用户的登录状态，如果在后台剔除就不能再登录，在filter中每次检查用户状态
	 * @param loginUserType 用户登录类型
	 * @param userId 用户Id
	 * @return
	 */
	public boolean isUserOnline(String loginUserType, String userId){
		Date date = cacheClient.get(parseCacheKey(loginUserType, userId));
		return date != null;
	}

	/**
	 * 用户登录的时候在redis中标记上线
	 * @param loginUserType 用户登录类型
	 * @param userId 用户Id
	 */
	public void online(String loginUserType, String userId){
		cacheClient.put(parseCacheKey(loginUserType, userId), LOGIN_STATUS_VALID_TIME, new Date());
	}

	/**
	 * 用户登出时
	 * @param loginUserType 用户登录类型
	 * @param userId
	 */
	public void offline(String loginUserType, String userId){
		Long result = cacheClient.delete(parseCacheKey(loginUserType, userId));
	}


	/**
	 * 构造一个cache key
	 * @param loginUserType
	 * @param userId
	 * @return
	 */
	private String parseCacheKey(String loginUserType,String userId){
		return propertiesDto.getCachePrefix() + ":" + loginUserType + ":" + userId;
	}

}
