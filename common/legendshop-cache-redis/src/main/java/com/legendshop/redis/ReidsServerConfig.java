package com.legendshop.redis;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.config.dto.RedisPropertiesDto;
import com.legendshop.util.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Redis的服务器配置
 */
@Component
@Slf4j
public class ReidsServerConfig {

	@Autowired
	private RedisPropertiesDto propertiesDto;

	/**
	 * connectionFactory
	 * @return
	 */
	@Bean
	public RedisConnectionFactory connectionFactory() {

		if(propertiesDto.getCluster() != null && propertiesDto.getCluster()){//集群配置

			JedisPoolConfig poolConfig = new JedisPoolConfig();
			poolConfig.setMaxTotal(propertiesDto.getMaxTotal());
			poolConfig.setMaxIdle(propertiesDto.getMaxIdle());
			poolConfig.setMaxWaitMillis(propertiesDto.getMaxWaitMillis());
			poolConfig.setMinIdle(propertiesDto.getMaxIdle());
			poolConfig.setTestOnBorrow(true);
			poolConfig.setTestOnReturn(false);
			poolConfig.setTestWhileIdle(true);
			JedisClientConfiguration jedisClientConfiguration = null;
			if (propertiesDto.isSsl()){
				jedisClientConfiguration = JedisClientConfiguration.builder().usePooling().
						poolConfig(poolConfig).and().
						readTimeout(Duration.ofMillis(propertiesDto.getTimeout())).useSsl()
						.build();
			}else {
				jedisClientConfiguration = JedisClientConfiguration.builder().usePooling().
						poolConfig(poolConfig).and().
						readTimeout(Duration.ofMillis(propertiesDto.getTimeout())).build();
			}

			RedisClusterConfiguration configuration = new RedisClusterConfiguration();
			configuration.setMaxRedirects(2);
			configuration.setPassword(RedisPassword.of(propertiesDto.getPassword()));
			List<RedisNode> redisNodes  = getRedisNode(propertiesDto.getClusterHostName());

			configuration.setClusterNodes(redisNodes);

			log.info("ClusterReidsConfiguration redisNodes size is {}", redisNodes.size());

			RedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(configuration, jedisClientConfiguration);
			return redisConnectionFactory;

		}else{
			log.info("StandaloneReidsConfiguration redis地址：" + propertiesDto.getHostName() + ":" + propertiesDto.getPort());
			JedisPoolConfig poolConfig = new JedisPoolConfig();
			poolConfig.setMaxTotal(propertiesDto.getMaxTotal());
			poolConfig.setMaxIdle(propertiesDto.getMaxIdle());
			poolConfig.setMaxWaitMillis(propertiesDto.getMaxWaitMillis());
			poolConfig.setMinIdle(propertiesDto.getMaxIdle());
			poolConfig.setTestOnBorrow(true);
			poolConfig.setTestOnReturn(false);
			poolConfig.setTestWhileIdle(true);
			JedisClientConfiguration jedisClientConfiguration = null;
			if (propertiesDto.isSsl()){
				jedisClientConfiguration = JedisClientConfiguration.builder().usePooling().
						poolConfig(poolConfig).and().
						readTimeout(Duration.ofMillis(propertiesDto.getTimeout())).useSsl()
						.build();
			}else {
				jedisClientConfiguration = JedisClientConfiguration.builder().usePooling().
						poolConfig(poolConfig).and().
						readTimeout(Duration.ofMillis(propertiesDto.getTimeout())).build();
			}
			RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();

			redisStandaloneConfiguration.setDatabase(propertiesDto.getDatabase());
			redisStandaloneConfiguration.setPort(propertiesDto.getPort());
			redisStandaloneConfiguration.setPassword(RedisPassword.of(propertiesDto.getPassword()));
			redisStandaloneConfiguration.setHostName(propertiesDto.getHostName());
			RedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration);
			return redisConnectionFactory;
		}
	}


	/**
	 * 单台redis机器配置
	 *
	 */
	@Bean
	public RedissonClient redisson(){
		Config config = new Config();

		if(propertiesDto.getCluster() != null && propertiesDto.getCluster()) {//集群配置

			ClusterServersConfig serversConfig = config.useClusterServers();
			serversConfig.setSslEnableEndpointIdentification(propertiesDto.isSsl());

			List<RedisNode> redisNodes = getRedisNode(propertiesDto.getClusterHostName());
			if (redisNodes != null) {
				for (RedisNode redisNode : redisNodes) {
					serversConfig.addNodeAddress("redis://" + redisNode.getHost() + ":" + redisNode.getPort());
				}

				log.info("ClusterRedissonClient redis node size {}", redisNodes.size());
			}

			serversConfig.setSubscriptionConnectionMinimumIdleSize(1)
					.setSubscriptionConnectionPoolSize(30);

			if (StringUtils.isNotBlank(propertiesDto.getPassword())) {
				serversConfig.setPassword(propertiesDto.getPassword());
			}
			return Redisson.create(config);
		}else{
			//standalone mode
			log.info("ClusterRedissonClient in standalone mode");
			config.useSingleServer().setSslEnableEndpointIdentification(propertiesDto.isSsl());
			config.useSingleServer().setAddress("redis://" + propertiesDto.getHostName() + ":" + propertiesDto.getPort())
					.setDatabase(propertiesDto.getDatabase())
					.setSubscriptionConnectionPoolSize(30)
					.setSubscriptionConnectionMinimumIdleSize(1);
			if (StringUtils.isNotBlank(propertiesDto.getPassword())) {
				config.useSingleServer().setPassword(propertiesDto.getPassword());
			}
			return Redisson.create(config);
		}


	}

	/**
	 * 解析Node
	 * @param clusterHostName
	 * @return
	 */
	private List<RedisNode> getRedisNode(String clusterHostName){
		if(AppUtils.isBlank(clusterHostName)){
			return null;
		}
		String[] hostAndPortList = clusterHostName.split(",");
		List<RedisNode> redisNodeList = new ArrayList<RedisNode>();
		try{
			for (String hostAndPort : hostAndPortList) {
				String[] str = hostAndPort.split(":");
				RedisNode node = new RedisNode(str[0].trim(),Integer.parseInt(str[1].trim()));
				redisNodeList.add(node);
			}
		}catch (Exception e){
			throw new BusinessException(e, "Parse clusterHostName error!");
		}


		return redisNodeList;
	}

}
