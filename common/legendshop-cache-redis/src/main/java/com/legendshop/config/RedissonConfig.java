package com.legendshop.config;

import com.legendshop.config.dto.RedisPropertiesDto;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class RedissonConfig {

	@Autowired
	private RedisPropertiesDto redisPropertiesDto;

	@Bean
	public RedissonClient redisson() throws IOException {
		Config config = new Config();
		config.useSingleServer()
				.setAddress("redis://" + redisPropertiesDto.getHostName() + ":" + redisPropertiesDto.getPort())
				.setSubscriptionConnectionPoolSize(30)
				.setSubscriptionConnectionMinimumIdleSize(1);
		if (redisPropertiesDto.getPassword() != null && !redisPropertiesDto.getPassword().trim().equals("")) {
			config.useSingleServer().setPassword(redisPropertiesDto.getPassword());
		}
		return Redisson.create(config);
	}
}
