package com.legendshop.central.test;

import java.sql.Connection;
import java.sql.DriverManager;

public class ProxoolUtil {
	public static void main(String[] args) {
		/**
		 * 普通配置文件实现方式，用的是proxool.xml的配置
		 */
//		ProxoolDataSource proxoolDataSource = null;
//		try {
//			// Class.forName("org.logicalcobwebs.proxool.ProxoolDriver");
//			// JAXPConfigurator.configure(new
//			// InputStreamReader(DataSourceFactory.class.getClassLoader()
//			// .getResourceAsStream("proxool.xml")), false);
//			// PropertyConfigurator.configure("jdbc.properties");
//
//			proxoolDataSource = getConnectProxool();
//
//			System.out.println("proxoolDataSource = " + proxoolDataSource);
//			System.out.println("proxoolDataSource Connection = " + proxoolDataSource.getConnection());
//			System.out.println("Connection = " + getConnectThrowProxoolDriver());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}

	public static Connection getConnectThrowProxoolDriver() {
		Connection connection = null;
		try {
			Class.forName("org.logicalcobwebs.proxool.ProxoolDriver");
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/legendshop3032c2c?characterEncoding=UTF-8", "root", "gmhewq");
		} catch (Exception e) {
			System.out.println(e);
		}
		return connection;
	}

//	public static ProxoolDataSource getConnectProxool() {
//	    ProxoolDataSource ds = new ProxoolDataSource(); 
//	    ds.setDriver("com.mysql.jdbc.Driver"); 
//	    ds.setDriverUrl("jdbc:mysql://localhost:3306/legendshop?characterEncoding=UTF-8"); 
//	    ds.setUser("root"); 
//	    ds.setPassword("gmhewq"); 
//		return ds;
//	}

}
