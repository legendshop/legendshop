package com.legendshop.license.util.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.legendshop.central.license.LSResponse;
import com.legendshop.util.AppUtils;

public class LSResopnseTest {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {

		LSResponse response = new LSResponse();
		response.setLicense("B2C");
		response.setAction("update");
		response.setDomainName("http://www.127.0.0.1");
		writeObject(response);
		getObject();
	}

	private static void writeObject(LSResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("resopnse", response);
		System.out.println("response.toString = " + response.toString());
		map.put("validate", AppUtils.getCRC32(response.toString()));
		System.out.println("response = " + response);
		FileOutputStream fos = new FileOutputStream("temp.out");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(map);
		oos.flush();
		oos.close();
	}

	private static Map getObject() throws Exception {
		FileInputStream fis = new FileInputStream("temp.out");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Map map = (Map) oin.readObject();
		Long validate = (Long) map.get("validate");
		LSResponse response = (LSResponse) map.get("resopnse");
		System.out.println("response.toString = " + response);
		response.setLicense("C2C");
		System.out.println("response.toString = " + response);
		System.out.println("validate=" + validate);
		System.out.println("response validate = " + AppUtils.getCRC32(response.toString()));
		oin.close();
		return null;
	}
}
