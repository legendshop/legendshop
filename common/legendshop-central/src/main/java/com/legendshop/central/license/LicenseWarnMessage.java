package com.legendshop.central.license;

import com.legendshop.util.constant.StringEnum;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ------------------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ------------------------------------------------------------------------------------
 */
public enum LicenseWarnMessage implements StringEnum {

	EXPIRED_WARNING(",请购买商业版权"),

	EXPIRED_ERROR(",已经过期，请购买商业版权"),

	SYSTEM_ERROR(",系统错误，请确认是否安装成功");

	private final String value;

	private LicenseWarnMessage(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		LicenseWarnMessage[] licenseEnums = values();
		for (LicenseWarnMessage licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static String getValue(String name) {
		LicenseWarnMessage[] licenseEnums = values();
		for (LicenseWarnMessage licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
}
