/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central;

import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.framework.handler.LicenseManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 		系统初始化事件
 *     eventId: SYS_INIT
 */
@Component("centralSysInitProcessor")
public class SysInitProcessor extends BaseProcessor<String>{
	
	@Autowired
	private LicenseManger licenseManger;
	
	/**
	 * 处理器
	 */
	@Override
	public void process(String domainName) {
		HealthCheckerHolder.isInitialized(domainName,licenseManger);
	}

}
