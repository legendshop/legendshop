package com.legendshop.central.license;


/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 * 
 */
public class LSResponse extends DtoEntity implements java.io.Serializable {
	
	private static final long serialVersionUID = 5411216601389890098L;

	private String ip;

	private String hostname;

	private String domainName;

	private String expireDate;

	private String newestVersion;

	private String license;
	
	/** 
	 * 调用结果
	 * 1:查询成功
	 * -1:更新失败:  
	 * 2:更新成功
	 * -2:更新失败
	 **/
	private int returnCode;

	// 默认的网店数量
	private Integer shopCount = 10;

	public LSResponse() {

	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getNewestVersion() {
		return newestVersion;
	}

	public void setNewestVersion(String newestVersion) {
		this.newestVersion = newestVersion;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public Integer getShopCount() {
		return shopCount;
	}

	public void setShopCount(Integer shopCount) {
		this.shopCount = shopCount;
	}

	@Override
	public String toString() {
		return new StringBuffer("response: returnCode = ").append(returnCode)
				.append(", IP = ").append(ip)
				.append(", hostname = ").append(hostname)
				.append(", domainName = ").append(domainName)
				.append(", expireDate = ").append(expireDate)
				.append(", = newestVersion = ").append(newestVersion)
				.append(", license = ").append(license)
				.append(", shopCount = ").append(shopCount).toString();
	}

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

}
