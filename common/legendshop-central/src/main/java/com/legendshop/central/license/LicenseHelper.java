package com.legendshop.central.license;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.legendshop.central.model.LicenseKeyDto;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SystemUtil;
import com.legendshop.util.converter.ByteConverter;
import com.legendshop.util.des.DES2;
import com.legendshop.util.ip.LocalAddress;
import com.legendshop.util.ip.LocalAddressUtil;

public class LicenseHelper {

	private static HttpClientLicenseHelper helper = new HttpClientLicenseHelper();

	private static String licenseFilePath = SystemUtil.getSystemRealPath() + "WEB-INF/license.out";

	public static LSResponse checkLicense(String domainName) throws Exception {
		String result = postRequest(domainName, "checkLicense", null);
		LSResponse lsResponse = null;
		if (AppUtils.isNotBlank(result)) {
			lsResponse = JSONUtil.getObject(result, LSResponse.class);
			persistResopnse(lsResponse);
		}
		return lsResponse;
	}



	public static void persistResopnse(LSResponse response) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("resopnse", response);
		map.put("validate", AppUtils.getCRC32(response.toString()));
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream(licenseFilePath);
			oos = new ObjectOutputStream(fos);
			// System.out.println("persistResopnse map = " + map);
			oos.writeObject(map);
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (oos != null) {
				oos.flush();
			}
			if (fos != null) {
				fos.close();
			}
		}
	}

	public static LSResponse getPersistedResopnse() throws Exception {
		FileInputStream fis = null;
		ObjectInputStream oin = null;
		try {
			fis = new FileInputStream(licenseFilePath);
			oin = new ObjectInputStream(fis);
			Map map = (Map) oin.readObject();
			// System.out.println("getPersistedResopnse map = " + map);
			Long validate = (Long) map.get("validate");
			LSResponse response = (LSResponse) map.get("resopnse");
			Long crcValidateKey = AppUtils.getCRC32(response.toString());
			if (validate != null && validate.equals(crcValidateKey)) {
				return response;
			}
		} catch (Exception e) {
		} finally {
			if (oin != null) {
				oin.close();
			}
			if (fis != null) {
				fis.close();
			}
		}

		return null;
	}

	public static String updateLicense(LicenseKeyDto licenseKeyDto) {
		String result = null;
		try {
			result = postRequest(licenseKeyDto);
		} catch (Exception e) {
			return null;
		}
		return result;
	}

	private static String postRequest(LicenseKeyDto licenseKeyDto) {
		String entityString = JSONUtil.getJson(licenseKeyDto);
		return helper.postNewMethod(entityString);
	}

	private static String postRequest(String domainName, String action, String licenseKey) {
		try {
			DES2 des = new DES2();
			String systemId =  ByteConverter.encode(des.byteToString(des.createEncryptor(getStrDate())));
			String date = new String(des.createDecryptor(des.stringToByte(ByteConverter.decode(systemId))));
			LocalAddress addr = LocalAddressUtil.getLocalAddress();

			LSRequest request = new LSRequest();
			request.setAction(action);
			request.setBusinessMode("");
			request.setDomainName(domainName);
			request.setLanguage("CN");
			request.setHostname(addr.getHostName());
			request.setIp(addr.getIp());
			request.setDate(date);
			request.setVersion("5.0");
			if (licenseKey != null) {
				request.setLicenseKey(licenseKey);
			}
			String entityString = JSONUtil.getJson(request);
			return helper.postMethod(entityString);
		} catch (Exception e) {
			return null;
		}

	}
	
	private static String getStrDate() {
		Date date = new Date();
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = simpledateformat.format(date);
		return s;
	}
}
