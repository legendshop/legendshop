/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central;

import com.legendshop.central.license.HealthCheckImpl;
import com.legendshop.framework.handler.LicenseManger;

/**
 * 健康检查.
 */
public class HealthCheckerHolder {

	/** The health checker holder. */
	private static HealthCheckerHolder healthCheckerHolder = null;

	/** The inited. */
	private static boolean inited = false;

	/**
	 *  构造函数
	 */
	private HealthCheckerHolder(String domainName, LicenseManger licenseManger) {
		if (!inited) {
			Thread t1 = new Thread(new HealthCheckImpl(domainName, licenseManger));
			t1.setName("health-check");
			t1.start();
			inited = true;
		}
	}

	/**
	 * Checks if is initialized.
	 *
	 */
	public static boolean isInitialized(String domainName, LicenseManger licenseManger) {
		if (healthCheckerHolder == null) {
			synchronized (HealthCheckerHolder.class) {
				if (healthCheckerHolder == null) {
					healthCheckerHolder = new HealthCheckerHolder(domainName, licenseManger);
				}
			}
		}
		return healthCheckerHolder != null;
	}
}
