package com.legendshop.central;

import com.legendshop.util.constant.StringEnum;


/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ------------------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ------------------------------------------------------------------------------------
 */
public enum LicenseEnum  implements StringEnum{

	B2C_ALWAYS("单用户终身正式版"),

	C2C_ALWAYS("多用户终身正式版"),

	B2C_YEAR("单用户年度正式版"),

	C2C_YEAR("多用户年度正式版"),

	FREE("免费版"),
	
	LOCAL("测试版"), //本地运行的版本

	EXPIRED("授权过期"),

	UNKNOWN("未知版本"),

	UN_AUTH("未授权系统");

	private final String value;

	private LicenseEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean instance(String name) {
		LicenseEnum[] licenseEnums = values();
		for (LicenseEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean needUpgrade(String name) {
		LicenseEnum[] licenseEnums = { B2C_YEAR, C2C_YEAR ,FREE,EXPIRED,UNKNOWN,UN_AUTH};
		for (LicenseEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNormal(String name) {
		LicenseEnum[] licenseEnums = { B2C_ALWAYS, C2C_ALWAYS, B2C_YEAR, C2C_YEAR };
		for (LicenseEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static String getValue(String name) {
		LicenseEnum[] licenseEnums = values();
		for (LicenseEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum.value();
			}
		}
		return null;
	}
	
	public static LicenseEnum getLicenseEnum(String name) {
		LicenseEnum[] licenseEnums = values();
		for (LicenseEnum licenseEnum : licenseEnums) {
			if (licenseEnum.name().equals(name)) {
				return licenseEnum;
			}
		}
		return null;
	}

}
