package com.legendshop.central.license;


/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 * 
 */
public class LSRequest extends DtoEntity implements java.io.Serializable {

	private static final long serialVersionUID = -2573903794153845729L;

	private String ip;

	private String hostname;

	private String domainName;

	private String date;

	private String businessMode;

	private String language;

	private String version;

	private String licenseKey;

	public LSRequest() {

	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBusinessMode() {
		return businessMode;
	}

	public void setBusinessMode(String businessMode) {
		this.businessMode = businessMode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return new StringBuffer("LSRequest: ").append("action = ").append(this.getAction()).append(", ip = ").append(ip).append(", hostname = ").append(hostname).append(
				" ,domainName = ").append(domainName).append(", date = ").append(date).append(", businessMode = ").append(
				businessMode).append(", language = ").append(language).append(", version=").append(version).append(
				", licenseKey = ").append(licenseKey).toString();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLicenseKey() {
		return licenseKey;
	}

	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

}
