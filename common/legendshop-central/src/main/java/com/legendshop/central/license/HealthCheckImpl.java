package com.legendshop.central.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.central.LicenseEnum;
import com.legendshop.framework.handler.LicenseManger;

/**
 * 健康检查
 * 
 */
public class HealthCheckImpl implements HealthCheck {

	protected Logger log = LoggerFactory.getLogger(HealthCheckImpl.class);

	// private int pauseTime = 1000 * 3600 * 6; //6个小时
	private int pauseTime = 1000 * 120; // 2分钟

	private String domainName;

	private boolean checker = true;

	private LicenseManger licenseManger;

	public HealthCheckImpl(String domainName, LicenseManger licenseManger) {
		this.domainName = domainName;
		this.licenseManger = licenseManger;
	}

	public void run() {
		while (checker) {
			try {
				Thread.sleep(pauseTime);
				String licenseType = licenseManger.getLicenseType();
				if (LicenseEnum.UN_AUTH.name().equals(licenseType)) {
					String result = check();
				}

			} catch (Exception e) {
			}
		}
	}

	public String check() {
		try {
			LSResponse response = LicenseHelper.checkLicense(domainName);
			if (response != null) {
				return response.getLicense();
			} else {
				return LicenseEnum.FREE.name();
			}
		} catch (Exception e) {
			return LicenseEnum.UNKNOWN.name();
		}

	}

}
