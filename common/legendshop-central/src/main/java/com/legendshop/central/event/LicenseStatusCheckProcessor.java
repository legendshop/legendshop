/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.event;



import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.legendshop.central.license.LicenseHelper;
import com.legendshop.central.model.LicenseKeyDto;
import com.legendshop.central.model.LicenseRequest;
import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.framework.handler.LicenseManger;
import com.legendshop.util.AppUtils;

/**
 * 检查License是否合法.
 */
@Component
public class LicenseStatusCheckProcessor extends BaseProcessor<LicenseRequest> {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(LicenseStatusCheckProcessor.class);
	
	@Resource(name="licenseManger")
	private LicenseManger licenseManger;

	@Override
	public void process(LicenseRequest request) {
		
		String companyCode = request.getCompanyCode();
		String companyName = request.getCompanyName();
		String links = request.getLinks();
		String des = request.getDes();
		
		//封装 LicenseKeyDto
		LicenseKeyDto licenseKeyDto = new LicenseKeyDto();
		licenseKeyDto.setCompanyCode(companyCode);
		licenseKeyDto.setCompanyName(companyName);
		if(AppUtils.isNotBlank(links)){
			String domainNames[] = links.split(",");
			licenseKeyDto.setDomainNames(domainNames);
		}
		if(AppUtils.isNotBlank(des)){
			String ips[] = des.split(",");
			licenseKeyDto.setIps(ips);
		}
		
		try {
			licenseKeyDto.setInstallDate(licenseManger.getInstallDate());
			licenseKeyDto.setVersion(licenseManger.getVersion());
			String cert = LicenseHelper.updateLicense(licenseKeyDto);
			if(AppUtils.isNotBlank(cert)){
				boolean result = licenseManger.updateLicense(cert);
				request.setUpgradeResult(result);
			}else{
				request.setUpgradeResult(false);//记录升级的结果
			}
		} catch (Exception e) {
			log.error("update license error happened {}", e);
		}
	}

}
