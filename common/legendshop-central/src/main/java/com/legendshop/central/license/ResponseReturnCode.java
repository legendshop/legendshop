/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.license;

/**
 * 状态.
 */
public interface ResponseReturnCode {
    
    /** 升级成功. */
    public int UPGRADE_OK = 1;
    
    /** 密码错误. */
    public int KEY_UNMATCH = 2;
    
    /** 密码正确，但是版本不对，不能升级. */
    public int UPGRADE_FAIL = 3;
    
}
