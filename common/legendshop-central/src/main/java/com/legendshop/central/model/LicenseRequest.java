package com.legendshop.central.model;

import java.io.Serializable;

public class LicenseRequest implements Serializable {

	private static final long serialVersionUID = -5663416390718777133L;
	
	String companyCode;
	
	String companyName;
	
	String links;
	
	String des;
	
	boolean upgradeResult;

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public boolean isUpgradeResult() {
		return upgradeResult;
	}

	public void setUpgradeResult(boolean upgradeResult) {
		this.upgradeResult = upgradeResult;
	}

}
