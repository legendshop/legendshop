/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.event;


import com.legendshop.framework.event.processor.BaseProcessor;
import com.legendshop.framework.handler.LicenseManger;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 检查License是否合法.
 */
@Component
public class UpdateLicenseKeyProcessor extends BaseProcessor<String> {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(UpdateLicenseKeyProcessor.class);
	
	@Resource(name="licenseManger")
	private LicenseManger licenseManger;

	/**
	 * 直接更新证书
	 * @param certificate 证书
	 */
	@Override
	public void process(String certificate) {
		try {
			if(AppUtils.isNotBlank(certificate)){
				boolean result = licenseManger.updateLicense(certificate);
				log.warn("Update License result is {}", result);
			}else{
				log.warn("certificate is null");
			}
		} catch (Exception e) {
			log.error("update license error happened {}", e);
		}
	}

}
