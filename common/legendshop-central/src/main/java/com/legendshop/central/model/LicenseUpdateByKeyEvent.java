/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.model;

import com.legendshop.framework.event.CoreEventId;
import com.legendshop.framework.event.SystemEvent;

/**
 *  采用激活码更新License.
 */
public class LicenseUpdateByKeyEvent extends SystemEvent<String> {

	public LicenseUpdateByKeyEvent(String certificate) {
		super(certificate, CoreEventId.LICENSE_UPDATE_BY_KEY_EVENT);
	}

}
