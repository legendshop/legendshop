/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.model;

import com.legendshop.framework.event.CoreEventId;
import com.legendshop.framework.event.SystemEvent;

/**
 *  检查和更新License.
 */
public class LicenseUpdateEvent extends SystemEvent<LicenseRequest> {

	public LicenseUpdateEvent(LicenseRequest request) {
		super(request, CoreEventId.LICENSE_STATUS_CHECK_EVENT);
	}

}
