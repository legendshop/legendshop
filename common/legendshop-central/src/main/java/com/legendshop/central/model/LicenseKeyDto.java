/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.central.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * The Class LicenseKeyDto.
 */
public class LicenseKeyDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7900179882429250688L;
	
	/** The company code. */
	private String companyCode;
	
	/** The company name. */
	private String companyName;
	
	/** The domain names. */
	private String[] domainNames;
	
	/** The ips. */
	private String[] ips;
	
	private Date installDate;
	
	private String version;
	
	/**
	 * Gets the company code.
	 *
	 * @return the company code
	 */
	public String getCompanyCode() {
		return companyCode;
	}
	
	/**
	 * Sets the company code.
	 *
	 * @param companyCode the new company code
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}
	
	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	/**
	 * Gets the domain names.
	 *
	 * @return the domain names
	 */
	public String[] getDomainNames() {
		return domainNames;
	}
	
	/**
	 * Sets the domain names.
	 *
	 * @param domainNames the new domain names
	 */
	public void setDomainNames(String[] domainNames) {
		this.domainNames = domainNames;
	}
	
	/**
	 * Gets the ips.
	 *
	 * @return the ips
	 */
	public String[] getIps() {
		return ips;
	}
	
	/**
	 * Sets the ips.
	 *
	 * @param ips the new ips
	 */
	public void setIps(String[] ips) {
		this.ips = ips;
	}

	public Date getInstallDate() {
		return installDate;
	}

	public void setInstallDate(Date installDate) {
		this.installDate = installDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "LicenseKeyDto [companyCode=" + companyCode + ", companyName=" + companyName + ", domainNames="
				+ Arrays.toString(domainNames) + ", ips=" + Arrays.toString(ips) + ", installDate=" + installDate
				+ ", version=" + version + "]";
	}
	

}
