package com.legendshop.central.license;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class HttpClientLicenseHelper {

	private final String licenseString;
	
	private final String newLicenseString;
	

	public final static String CHARSET = "UTF-8";

	/**
	 * @param args
	 */
	public HttpClientLicenseHelper() {
		licenseString =  "http://www.legendshop.cn/license/query";
		newLicenseString = "http://www.legendshop.cn/license/upgrade";
		//for local env
//		licenseString =  "http://localhost:8080/license/query";
//		newLicenseString = "http://localhost:8080/license/upgrade";
	}

	public HttpClientLicenseHelper(String hostName) {
		licenseString = hostName + "/license/query";
		newLicenseString = hostName + "/license/upgrade";
	}

	
	public String postMethod(String entity) {
		Map<String,String> params = new HashMap<String, String>();
		params.put("_ENTITY", entity);
		String result = httpRequest(licenseString,"POST", null, params);
		return result;
	}
	
	public String postNewMethod(String entity) {
		Map<String,String> params = new HashMap<String, String>();
		params.put("_ENTITY", entity);
		String result = httpRequest(newLicenseString,"POST", null, params);
		return result;
	}
	
	/**
	 * 发送请求
	 * 
	 * @param urlAddress
	 * @param requestMethod
	 * @param headers
	 * @param params
	 * @return
	 */
	public  String httpRequest(String urlAddress, String requestMethod, Map<String, String> headers, Map<String, String> params) {
		URL url = null;
		HttpURLConnection con = null;
		BufferedReader in = null;
		try {
			url = new URL(urlAddress);
			con = (HttpURLConnection) url.openConnection();
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setConnectTimeout(30000); // 设置连接超时为30秒
			con.setRequestMethod(requestMethod);

			// 一定要设置 Content-Type 要不然服务端接收不到参数
			con.setRequestProperty("Content-Type","application/x-www-form-urlencoded;charset=" + CHARSET);
			con.setRequestProperty("Accept-Charset", CHARSET);
			con.setRequestProperty(
					"User-Agent",
					"Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			if (headers != null && !headers.isEmpty()) {// 设置头部
				for (String headerKey : headers.keySet()) {
					con.setRequestProperty(headerKey, headers.get(headerKey));
				}
			}

			if (params != null && !params.isEmpty()) {
				StringBuffer paramsTemp = new StringBuffer();
				for (String paramKey : params.keySet()) {
					String paramValue = params.get(paramKey);
					if(paramValue != null && paramValue.length() > 0){
						paramValue = URLEncoder.encode(params.get(paramKey), CHARSET);
					}
					paramsTemp.append("&").append(paramKey).append("=").append(paramValue);
				}
				
				if(paramsTemp.length() > 0){
					DataOutputStream out = new DataOutputStream(con.getOutputStream());
					String paramsOut = paramsTemp.substring(1).toString();//去掉第一个&符号
					out.writeBytes(paramsOut);
					out.flush();
					out.close();
				}
			}

			int returnCode = con.getResponseCode();
			if (returnCode == 200) {
				StringBuffer result = new StringBuffer();
				in = new BufferedReader(new InputStreamReader(con.getInputStream(), CHARSET));
				while (true) {
					String line = in.readLine();
					if (line == null) {
						break;
					} else {
						result.append(line);
					}
				}
				return result.toString();
			} else {
				return "returnCode=" + returnCode;
			} 
		} catch (ConnectException ce) {
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (con != null) {
					con.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
