package com.legendshop.util;

import org.javia.arity.Function;
import org.javia.arity.Symbols;
import org.javia.arity.SyntaxException;

/**
 * Java算术引擎 Arity
 * 
 * @author tony
 * 
 */
public class ArithmeticEngine {

	private static ArithmeticEngine arithmeticEngine = null;

	private Symbols symbols = new Symbols();

	private ArithmeticEngine() {
		 Function quzhenFunc = new Function(){
			@Override
			public int arity() {
				return 1;
			}

			@Override
			public double eval(double value) {
				if(value < 0){
					return 0;
				}
				Double doubleValue = value;
				if(doubleValue.intValue() == doubleValue){
					return doubleValue;
				}else{
					return doubleValue.intValue() + 1;
				}
			}
        };
        symbols.define("quzhen", quzhenFunc);
	}

	public static synchronized ArithmeticEngine getInstance() {
		if (arithmeticEngine == null) {
			arithmeticEngine = new ArithmeticEngine();
		}
		return arithmeticEngine;
	}


	public double clacStanderd(double firstWeight, double continueWeight,
			double firstPrice, double continuePrice, double totalWeight)
			throws SyntaxException {
		String formula = new StringBuilder().append(firstPrice)
				.append("+quzhen((").append(totalWeight).append("-")
				.append(firstWeight).append(")/").append(continueWeight)
				.append(")*").append(continuePrice).toString();
		return symbols.compile(formula).eval();
	}

}
