/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.base.model;

import java.io.Serializable;

/**
 * key value键值对.
 */
public class KeyValueObj implements Serializable{
	
	private static final long serialVersionUID = -2670952483041689535L;

	private String key;
	
	private Object value;
	
	public KeyValueObj() {
	}

	public KeyValueObj(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
