package com.legendshop.base.exception;

/**
 * 库存异常
 *
 */
public class NotStocksException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6362673926910246867L;


	/**
	 * Instantiates a new business exception.
	 *
	 * @param message the message
	 */
	public NotStocksException(String message) {
		super(message, ErrorCodes.NOT_ENOUGH_STOCKS);
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message the message
	 * @param code the code
	 */
	public NotStocksException(String message,  String errorCode) {
		super(message, errorCode);
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param cause the cause
	 * @param message the message
	 * @param errorCode the error code
	 */
	public NotStocksException(Throwable cause, String message, String errorCode) {
		super(cause, message, errorCode);
	}
}
