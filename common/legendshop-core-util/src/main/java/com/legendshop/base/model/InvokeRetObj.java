/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.base.model;

import java.io.Serializable;

/**
 * 调用接口的返回结果.
 */
public class InvokeRetObj implements Serializable{

	private static final long serialVersionUID = 4779155350624170975L;

	/** 成功与否的标记, true为正常状态,false为异常状态. */
	private boolean flag = true;//
	
	/** 返回码, 一般来说, 0为正常,具体以调用业务为准 **/
	private int retCode = 0;
	
	/** 返回信息 */
	private String msg;
	
	/** 如果调用成功,则返回调用结果. */
	private Object obj;
	
	/**
	 * 成功执行
	 * @return 结果
	 */
	public static InvokeRetObj success(){
		return new InvokeRetObj(true, "SUCCESS");
	}
	
	/** 
	 * 执行失败
	 * @return 结果
	 */
	public static InvokeRetObj fail(){
		return new InvokeRetObj(false, -1, "FAILED",null);
	}
	
	/**
	 * Checks if is flag.
	 *
	 * @return true, if is flag
	 */
	public boolean isFlag() {
		return flag;
	}
	
	/**
	 * Sets the flag.
	 *
	 * @param flag the new flag
	 */
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	/**
	 * Gets the msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	
	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	/**
	 * Gets the obj.
	 *
	 * @return the obj
	 */
	public Object getObj() {
		return obj;
	}
	
	/**
	 * Sets the obj.
	 *
	 * @param obj the new obj
	 */
	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	/**
	 * Instantiates a new ret obj.
	 */
	public InvokeRetObj() {

	}
	
	/**
	 * Instantiates a new ret obj.
	 *
	 * @param flag the flag
	 * @param msg the msg
	 * @param obj the obj
	 */
	public InvokeRetObj(boolean flag, String msg, Object obj) {
		this.flag = flag;
		this.msg = msg;
		this.obj = obj;
	}
	
	public InvokeRetObj(boolean flag, int retCode, String msg, Object obj) {
		this.flag = flag;
		this.retCode = retCode;
		this.msg = msg;
		this.obj = obj;
	}
	
	/**
	 * Instantiates a new ret obj.
	 *
	 * @param flag the flag
	 * @param msg the msg
	 */
	public InvokeRetObj(boolean flag, String msg) {
		this.flag = flag;
		this.msg = msg;
	}
	
	public InvokeRetObj( String msg) {
		super();
		this.flag = true;
		this.msg = msg;
	}

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}
	
	@Override
	public String toString() {
		return "InvokeRetObj [flag=" + flag + ", msg=" + msg + "]";
	}

}
