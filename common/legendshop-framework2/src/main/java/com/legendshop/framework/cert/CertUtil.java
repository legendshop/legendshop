package com.legendshop.framework.cert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.CRC32;

public class CertUtil {
	
	
	private static String CRC32_KEY = "shdhdskjfhsfhskf!@#1";
	
	public  static String dateToString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}
	
	/**
	 * Gets the cR c32.
	 * 
	 * @param value
	 *            the value
	 * @return the cR c32
	 */
	public static Long getCRC32(String value) {
		String v = value + CRC32_KEY;
		CRC32 crc32 = new CRC32();
		crc32.update(v.getBytes());
		return crc32.getValue();
	}
	
	public static  Date stringToDate(String date) {
		Date myDate = null;
		try {
		    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			myDate = format.parse(date);
		} catch (ParseException e) {
		}
		return myDate;
	}
	

}
