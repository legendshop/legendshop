package com.legendshop.framework.cert;

import com.legendshop.central.LicenseEnum;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtil;
import com.legendshop.util.EnvironmentConfig;
import com.legendshop.util.converter.ByteConverter;
import com.legendshop.util.des.DES2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("certManager")
public class CertManagerImpl implements CertManager{
	
	DES2 des = new DES2("12%4567!");
	
	private CertHolder certHolder = null;
	
	private Boolean IS_INNERIP = null;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public CertManagerImpl(){
		
	}
	
	/*
	 * Version       1-10  4.0.1.0.1
	 * installDate:  2016-08-12 17:17    11- 30
	 * LicenseEnum: 31 - 40
	 * cert numbers: 41 - 45
	 * cert content: 46 - 60
	 * CRC32: Last word
	 * 
	 */
	/* (non-Javadoc)
	 * @see com.legendshop.cert.CertManager#write(com.legendshop.cert.CertHolder)
	 */
	
	public String write(CertHolder holder){
		StringBuffer sb = new StringBuffer();
		sb.append(formatString(holder.getVersion(),10));
		sb.append(formatString(CertUtil.dateToString(holder.getInstallDate()),19));
		sb.append(formatString(holder.getLicenseEnum().name(),15));
		sb.append(formatString(holder.getCertNums().toString(),3));
		
		if(holder.getCerts() != null && holder.getCerts().size() > 0){
			StringBuffer certs = new StringBuffer();
			
			for (Cert cert : holder.getCerts()) {
				certs =certs.append(cert.toString()).append(";");
			}
			certs.setLength(certs.length() - 1);
			
			int contentStartPos = sb.length() + 15;
			String contentPos = contentStartPos +"," + (contentStartPos +certs.length() );
			sb.append(formatString(contentPos,15));
			sb.append(formatString(certs.toString(), certs.length()));
		}
		sb.append(CertUtil.getCRC32(sb.toString()));
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.cert.CertManager#read(java.lang.String)
	 */
	
	public CertHolder read(String cert){
		if(cert == null || cert.length() == 0){
			return null;
		}
		StringBuffer sb = new StringBuffer();
		CertHolder holder = new CertHolder();
		//String version = PropertiesUtil.getProperties(FileConfig.GlobalFile, ConfigPropertiesEnum.LEGENDSHOP_VERSION.name());
		String version = cert.substring(0, 10);
		sb.append(version);
		holder.setVersion(version.trim());
		
		String installDate = cert.substring(10, 29);
		sb.append(installDate);
		holder.setInstallDate(CertUtil.stringToDate(installDate.trim()));
		
		String licenseEnum = cert.substring(29, 44);
		sb.append(licenseEnum);
		holder.setLicenseEnum(LicenseEnum.getLicenseEnum(licenseEnum.trim()));
		
		String certNums = cert.substring(44, 47);
		sb.append(certNums);
		Integer certNum = Integer.valueOf(certNums.trim());
		holder.setCertNums(certNum);
		String crc32= null;
		if(certNum > 0){
			String certContentPos= cert.substring(47, 62);
			sb.append(certContentPos);
			String[] pos = certContentPos.trim().split(",");
			
			String certContents = cert.substring(Integer.valueOf(pos[0]), Integer.valueOf(pos[1]));
			sb.append(certContents);
			
			String[] certs = certContents.split(";");
			for (String c : certs) {
				Cert  entity = new Cert();
				String[] entities = c.split(",");
				entity.setDomainName(entities[0]);
				entity.setIp(entities[1]);
				entity.setValidateDate(CertUtil.stringToDate(entities[2]));
				holder.addCerts(entity);
			}
			
			crc32= cert.substring(Integer.valueOf(pos[1]));
		}else{
			crc32 = cert.substring(47);
		}

		
		Long crc2 = CertUtil.getCRC32(sb.toString());
		if(crc32.equals(crc2.toString())){
			return holder;
		}
		throw new RuntimeException("cert error");
		
	}
	
	private String formatString(String str, int length){
	        StringBuffer sb = new StringBuffer();
	        sb.append(str);
	        
	        if(str.length() > length){
	        	sb.setLength(length);
	        	return sb.toString();
	        }
	        int count = length - str.length();
	        while(count > 0){
	            sb.append(" ");
	            count --;
	        }
	        return sb.toString();
	    }
	
	
	/* (non-Javadoc)
	 * @see com.legendshop.cert.CertManager#encode(java.lang.String)
	 */
	
	public String encode(String value){
		String byteToString = des.byteToString(des.createEncryptor(value));
		return ByteConverter.encode(byteToString);
	}
	
	/* (non-Javadoc)
	 * @see com.legendshop.cert.CertManager#decode(java.lang.String)
	 */
	
	public String decode(String value){
		String decodeAim = ByteConverter.decode(value);
		return new String(des.createDecryptor(des.stringToByte(decodeAim)));
	}
	
	
	/* (non-Javadoc)
	 * @see com.legendshop.cert.CertManager#genInitCert()
	 */
	
	public String genInitCert(){
		CertHolder holder = new CertHolder();
		holder.setInstallDate(new Date());
		holder.setLicenseEnum(LicenseEnum.UN_AUTH);
		String version = null;
		try {
			version =  EnvironmentConfig.getInstance().getPropertyValue(GlobalConfigPropertiesEnum.LEGENDSHOP_VERSION);
		} catch (Exception e) {
			version = "5.x";
		}
		if(version == null){
			version = "5.x";
		}
		holder.setVersion(version);
		holder.setCertNums(0);
		return write(holder);
	}
	
	
	public static void main(String[] args) {
		CertHolder holder = new CertHolder();
		holder.setInstallDate(DateUtil.getDate("2016", "08", "13"));
		holder.setLicenseEnum(LicenseEnum.C2C_ALWAYS);
		holder.setVersion("4.0.1");
		holder.setCertNums(2);
		
		Cert cert1 = new Cert();
		cert1.setDomainName("legendshop.cn");
		cert1.setIp("127.0.0.1");
		cert1.setValidateDate(DateUtil.getDate("2017", "01", "02"));
		
		holder.addCerts(cert1);
		
		Cert cert2 = new Cert();
		cert2.setDomainName("legendshop.com");
		cert2.setIp("127.0.0.1");
		cert2.setValidateDate(DateUtil.getDate("2018", "11", "12"));
		
		holder.addCerts(cert2);
		
		CertManager manager = new CertManagerImpl();
		manager.setCertHolder(holder);
		String holderStr = manager.write(holder);
		System.out.println(holderStr);
		
		DES2 des = new DES2("12%4567!");
		String byteToString = des.byteToString(des.createEncryptor(holderStr));
		System.out.println("加密后的数据:" + byteToString);
		
		String aimStr = ByteConverter.encode(byteToString);
		System.out.println("加密后的16进制编码 :" + aimStr);
		
		
		String decodeAim = ByteConverter.decode(aimStr);
		System.out.println("解密后的16进制编码 :" + decodeAim);
		String decryptorString = new String(des.createDecryptor(des.stringToByte(decodeAim)));

		System.out.println("解密后的数据：" + decryptorString);
		
		//////////////////////////
		
		CertHolder holder2 = manager.read(holderStr);
		System.out.println(holder2);
		String initCert = manager.genInitCert();
		System.out.println(initCert);
		
		System.out.println(manager.read(initCert));
		
	}


	public boolean isCertValid(String ip, String domainName) {

		if("localhost".equalsIgnoreCase(domainName) || "0:0:0:0:0:0:0:1".equals(ip) || "127.0.0.1".equals(ip)) {//排除掉本地开发环境
			return true;
		}
		
		Date now = new Date();
		CertHolder certHolder = getCertHolder();
		if(LicenseEnum.UN_AUTH.equals(certHolder.getLicenseEnum())){
			Date validateDate = DateUtil.getDay(certHolder.getInstallDate(), 90);
			boolean result = now.before(validateDate);//没有注册的用户有效期90天
			return result;
		}else{
			String dName;
			String dName2;

			for (Cert cert : certHolder.getCerts()) {
				dName = DomainUtils.getTopDomainWithoutSubdomain(cert.getDomainName());
				dName2= DomainUtils.getTopDomainWithoutSubdomain(domainName);
				if(AppUtils.isNotBlank(dName) &&  AppUtils.isNotBlank(dName2)){
					if(dName.equalsIgnoreCase(dName2)){
						if(!LicenseEnum.B2C_ALWAYS.equals(certHolder.getLicenseEnum()) || !LicenseEnum.C2C_ALWAYS.equals(certHolder.getLicenseEnum())){
							if(cert.getValidateDate() != null){ //有时间限制
								if(now.before(cert.getValidateDate())){
									return true;//在期限之内
								}else {
									return false;//在期限之外
								}
							}
						}

						return true;
					}
				}

			}
		}
		return false;
	}

	/**
	 *  获取证书
	 */
	public CertHolder getCertHolder() {
		if(certHolder == null) {
			synchronized (CertManagerImpl.class) {
				if(certHolder == null) {
					String certs = queryDBCert();
					if(AppUtils.isBlank(certs)){
						certs = genInitCert();
						updateCert(encode(certs));
						 certHolder = read(certs);
					}else{
						try {
							certHolder = read(decode(certs));
						} catch (Exception e) {
							certs = genInitCert();
							certHolder = read(decode(certs));
						}
						 
					}
				}
			}
		}
		return certHolder;
	}

	public void setCertHolder(CertHolder certHolder) {
		this.certHolder = certHolder;
	}
	
	private String queryDBCert(){
		String sql = "select certificate from ls_sys_conf";
		String certificate = jdbcTemplate.queryForObject(sql, String.class);
		return certificate;
	}
	
	private boolean updateCert(String certificate){
		String sql = "update ls_sys_conf set certificate = ? ";
		int updateResult = jdbcTemplate.update(sql, certificate);
		return updateResult > 0;
	}
	
}
