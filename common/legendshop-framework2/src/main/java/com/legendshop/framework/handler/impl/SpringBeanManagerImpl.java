package com.legendshop.framework.handler.impl;

import java.util.Set;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.legendshop.framework.handler.SpringBeanManager;

@Component("springBeanManager")
public class SpringBeanManagerImpl implements SpringBeanManager{
	
	private static Logger log = LoggerFactory.getLogger(SpringBeanManagerImpl.class);

	/**
	 * Adds the data source.
	 * 
	 * @param servletContext
	 *            the servlet context
	 * @param dataSource
	 *            the data source
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	public  void addDataSource(ServletContext servletContext, DataSource dataSource) throws ClassNotFoundException {
		// 1 get BeanFactory
		DefaultListableBeanFactory factory = (DefaultListableBeanFactory) WebApplicationContextUtils.getWebApplicationContext(servletContext).getAutowireCapableBeanFactory();

		// 2 create Bean Definition
		AbstractBeanDefinition beanDef = BeanDefinitionReaderUtils.createBeanDefinition(null, dataSource.getClass().getName(),WebApplicationContextUtils.getWebApplicationContext(servletContext).getClassLoader());

		// 3 add Bean Definition to BeanFactory
		factory.registerBeanDefinition("dataSource", beanDef);
	}

	public  void removeBean(ServletContext servletContext, String beanName) {
		log.debug("remove beanName {} from spring contxt ", beanName);
		// 1 get BeanFactory
		DefaultListableBeanFactory factory = (DefaultListableBeanFactory) WebApplicationContextUtils.getWebApplicationContext(servletContext).getAutowireCapableBeanFactory();

		// 2 add Bean Definition to BeanFactory
		factory.removeBeanDefinition(beanName);
	}

	public void removeBean(ServletContext servletContext, Set<String> keySet) {
		if (keySet == null) {
			return;
		}
		for (String key : keySet) {
			removeBean(servletContext, key);
		}
	}

}
