/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.handler;

import javax.servlet.http.HttpServletRequest;

/**
 * 每次请求， 保存HttpRequest上下文.
 */
public class RequestHolder {
	
	/** The request holder. */
	private static ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
	
	/**
	 * 是否开始请求.
	 * 
	 * @return true, if successful
	 */
	public static boolean requestStarted() {
		return request.get() != null;
	}

	/**
	 * Clean.
	 */
	public static void clean() {
		if (request.get() != null) {
			request.remove();
		}
	}


	/**
	 * Gets the request.
	 * 
	 * @return the request
	 */
	public static HttpServletRequest getRequest() {
		return request.get();
	}

	/**
	 * Sets the request.
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setRequest(HttpServletRequest httpRequest) {
		request.set(httpRequest);
	}


}
