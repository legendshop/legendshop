package com.legendshop.framework.cert;

import com.legendshop.util.FileConfig;
import com.legendshop.util.FileConfigEnum;


/**
 * 公共配置for global.properties的key
 * 
 */
public enum GlobalConfigPropertiesEnum implements FileConfigEnum{

	/** LegendShop版本号. */
	LEGENDSHOP_VERSION;

	@Override
	public String file() {
		return FileConfig.GlobalFile;
	}

	
}
