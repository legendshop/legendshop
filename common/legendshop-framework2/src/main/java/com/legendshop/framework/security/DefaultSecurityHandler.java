package com.legendshop.framework.security;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.framework.handler.SecurityHandler;

/**
 * 
 * 默认的安全处理器
 *
 */
public class DefaultSecurityHandler implements SecurityHandler {

	@Override
	public boolean handle(HttpServletRequest request) {
		return true;
	}

}
