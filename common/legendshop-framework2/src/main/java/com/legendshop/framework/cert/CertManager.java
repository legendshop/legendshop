package com.legendshop.framework.cert;


public interface CertManager {

	/*
	 * Version       1-10  4.0.1.0.1
	 * installDate:  2016-08-12 17:17    11- 30
	 * LicenseEnum: 31 - 40
	 * cert numbers: 41 - 45
	 * cert content: 46 - 60
	 * CRC32: Last word
	 * 
	 */
	public abstract String write(CertHolder holder);

	public abstract CertHolder read(String cert);

	public abstract String encode(String value);

	public abstract String decode(String value);

	public abstract String genInitCert();

	public abstract boolean isCertValid(String ip, String domainName);
	
	public CertHolder getCertHolder();

	public void setCertHolder(CertHolder certHolder) ;
}