/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.framework.event;

public class SysInitEvent extends SystemEvent<String> {

	/**
	 * Instantiates a new sys init event.
	 * 
	 * @param event
	 *            the event
	 */
	public SysInitEvent(String domainName) {
		super(domainName, CoreEventId.SYS_INIT_EVENT);
	}

}
