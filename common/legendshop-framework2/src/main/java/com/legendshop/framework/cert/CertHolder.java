package com.legendshop.framework.cert;

import com.legendshop.central.LicenseEnum;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CertHolder implements Serializable{

	private static final long serialVersionUID = -2797529628368523671L;

	private List<Cert> certs;
	
	private LicenseEnum licenseEnum;
	
	private Date installDate;
	
	private String version;
	
	private Integer certNums;

	public List<Cert> getCerts() {
		return certs;
	}

	public void setCerts(List<Cert> certs) {
		this.certs = certs;
	}
	
	public void addCerts(Cert cert) {
		if(certs == null){
			certs = new ArrayList<Cert>();
		}
		certs.add(cert);
	}

	public Date getInstallDate() {
		return installDate;
	}

	public void setInstallDate(Date installDate) {
		this.installDate = installDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public LicenseEnum getLicenseEnum() {
		return licenseEnum;
	}

	public void setLicenseEnum(LicenseEnum licenseEnum) {
		this.licenseEnum = licenseEnum;
	}

	public Integer getCertNums() {
		return certNums;
	}

	public void setCertNums(Integer certNums) {
		this.certNums = certNums;
	}

	@Override
	public String toString() {
		return "CertHolder [certs=" + certs + ", licenseEnum=" + licenseEnum + ", installDate=" + dateToString(installDate)
				+ ", version=" + version + ", certNums=" + certNums + "]";
	}
	
	private  static String dateToString(Date date) {
		if(date == null){
			return "";
		}
		try{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return format.format(date);
		}catch (Exception e){
			return "";
		}
	}
	

}
