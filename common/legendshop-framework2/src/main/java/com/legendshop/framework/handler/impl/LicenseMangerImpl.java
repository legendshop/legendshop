package com.legendshop.framework.handler.impl;

import com.legendshop.central.LicenseEnum;
import com.legendshop.framework.cert.Cert;
import com.legendshop.framework.cert.CertHolder;
import com.legendshop.framework.cert.CertManager;
import com.legendshop.framework.cert.CertUtil;
import com.legendshop.framework.handler.LicenseManger;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("licenseManger")
public class LicenseMangerImpl implements LicenseManger {

	@Autowired
	private CertManager certManager;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public boolean updateLicense(String cert) {
		CertHolder certHolder = null;
		try {
			String certDes = certManager.decode(cert);
			certHolder = certManager.read(certDes);
		} catch (Exception e) {
			certHolder = null;
		}
		if (certHolder == null) {
			return false;
		}
		updateCert(cert);
		certManager.setCertHolder(certHolder);
		return true;
	}

	private boolean updateCert(String certificate) {
		String sql = "update ls_sys_conf set certificate = ?";
		int updateResult = jdbcTemplate.update(sql, certificate);
		return updateResult > 0;
	}

	public Date getInstallDate() {
		CertHolder certHolder = certManager.getCertHolder();
		if (certHolder == null) {
			return new Date();
		}
		return certHolder.getInstallDate();
	}

	public String getVersion() {
		CertHolder certHolder = certManager.getCertHolder();
		if (certHolder == null) {
			return null;
		}
		return certHolder.getVersion();
	}

	public String getLicenseType() {
		CertHolder certHolder = certManager.getCertHolder();
		if (certHolder == null) {
			return null;
		}
		return certHolder.getLicenseEnum().name();
	}

	public String getDomainName() {
		CertHolder certHolder = certManager.getCertHolder();
		if (certHolder == null) {
			return null;
		}
		String licenseMsg = "";
		List<Cert> certs = certHolder.getCerts();
		if (AppUtils.isNotBlank(certs)) {
			List<Object> list = new ArrayList<>();
			for (Cert cert : certs) {
				if (certHolder.getLicenseEnum().equals(LicenseEnum.B2C_YEAR)
						|| certHolder.getLicenseEnum().equals(LicenseEnum.C2C_YEAR)
						|| certHolder.getLicenseEnum().equals(LicenseEnum.UN_AUTH)
						|| certHolder.getLicenseEnum().equals(LicenseEnum.EXPIRED)) {
					if (AppUtils.isBlank(cert.getValidateDate())) {
						list.add(cert.getDomainName() + " 有效期至长期");
					} else {
						list.add(cert.getDomainName() + " 有效期至" + CertUtil.dateToString(cert.getValidateDate()));
					}
				} else {
					list.add(cert.getDomainName());
				}
			}

			licenseMsg = AppUtils.list2String(list);
		}
		return licenseMsg;
	}


}
