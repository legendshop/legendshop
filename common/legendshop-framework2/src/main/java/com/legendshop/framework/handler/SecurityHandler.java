package com.legendshop.framework.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * 安全过滤器.
 */
public interface SecurityHandler {

	/**
	 * Handle.
	 *
	 * @param request the request
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean handle(HttpServletRequest request);


}
