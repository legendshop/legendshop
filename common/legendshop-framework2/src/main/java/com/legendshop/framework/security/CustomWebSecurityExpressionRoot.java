/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.framework.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;

import com.legendshop.framework.cert.CertManager;
import com.legendshop.framework.handler.RequestHolder;
import com.legendshop.framework.handler.SecurityHandler;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.framework.model.AbstractUserDetail;
import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.util.AppUtils;
import com.legendshop.util.ip.LocalAddressUtil;

/**
 * 构建一个自定义实现的SpEL表达式处理器.
 */
public class CustomWebSecurityExpressionRoot extends WebSecurityExpressionRoot {

    private SecurityHandler adminSecurityResourceFilterHandler;

    private SecurityHandler shopSecurityResourceFilterHandler;

    private CertManager certManager;

    /**
     * 登录的是管理员类型
     */
    private String ADMIN_TYPE = "A";

    /**
     * 登录的是普通用户类型
     */
    private String USER_TYPE = "U";

    /**
     * Instantiates a new custom web security expression root.
     *
     * @param a  the a
     * @param fi the fi
     */
    public CustomWebSecurityExpressionRoot(Authentication a, FilterInvocation fi, CertManager certManager) {
        super(a, fi);
        this.certManager = certManager;
    }

    /**
     * 检查是否有权限
     *
     * @param functions the functions
     * @return true, if successful
     */
    public boolean hasAnyFunction(String... functions) {
        AbstractUserDetail user = getUser();
        if (user == null) {
            return false;
        }
        Collection<GrantedFunction> funcList = user.getFunctions();
        for (String func : functions) {
            for (GrantedFunction grantedFunction : funcList) {
                if (grantedFunction.getFunction().equals(func)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 检查是否是管理员
     *
     * @return
     */
    public boolean isAdmin() {
        AbstractUserDetail user = getUser();
        if (user == null) {
            return false;
        }
        HttpServletRequest request = RequestHolder.getRequest();
        if (request == null) {
            System.err.println("Request is null, it should be access by filter to set the http request, and then rediect to login page");
            return false;
        }
        String serviceName = request.getServerName();
        String ip = RequestHolder.getRequest().getRemoteAddr();
        if (!certManager.isCertValid(ip, serviceName)) {
            System.err.println(serviceName + "[" + LocalAddressUtil.getLocalAddress() + "] 域名证书过期或无效，请联系供应商更新证书！");
        }

        if (ADMIN_TYPE.equals(user.getLoginUserType())) {// 管理员登录
            // 检查url权限
            return getSecurityHandler().handle(request);
        } else {
            return false;
        }
    }

    /**
     * 普通用户登录 第三方用户登录
     *
     * @return
     */
    public boolean isUser() {
        AbstractUserDetail user = getUser();
        if (user == null) {
            return false;
        }

        return USER_TYPE.equals(user.getLoginUserType());
    }

    /**
     * 是否供应商用户登录
     *
     * @return
     */
    public boolean isSupplier(String role) {
        AbstractUserDetail user = getUser();
        HttpServletRequest request = RequestHolder.getRequest();
        if (AppUtils.isBlank(request)) {
            return false;
        }

        String serviceName = request.getServerName();
        String ip = request.getRemoteAddr();
        if (!certManager.isCertValid(ip, serviceName)) {
            System.err.println(serviceName + "[" + LocalAddressUtil.getLocalAddress() + "] 域名证书过期或无效，请联系供应商更新证书！");
            return false;
        }

        boolean isSupplier = false;
        if (user == null) {
            return isSupplier;
        }

        if (user.isShopUser()) {// 是否是商家
            Collection<? extends GrantedAuthority> roleSet = authentication.getAuthorities();
            for (GrantedAuthority authority : roleSet) {
                if (role.equals(authority.getAuthority())) {
                    isSupplier = true;
                    break;
                }
            }

            if (!isSupplier) {
                return isSupplier;
            }
            //检查权限
            return getShopSecurityHandler().handle(request);

        }
        return isSupplier;
    }


    /**
     * 检查是否有权限.
     *
     * @param function the function
     * @return true, if successful
     */
    public final boolean hasFunction(String function) {
        Collection<GrantedFunction> funcList = getUser().getFunctions();
        if (funcList == null) {
            return false;
        }
        for (GrantedFunction grantedFunction : funcList) {
            if (grantedFunction.getFunction().equals(function)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    private AbstractUserDetail getUser() {
        if (authentication.getPrincipal() instanceof AbstractUserDetail) {
            return (AbstractUserDetail) authentication.getPrincipal();
        }
        return null;
    }

    /**
     * 获取后台的handler
     *
     * @return
     */
    private SecurityHandler getSecurityHandler() {
        if (adminSecurityResourceFilterHandler == null) {
        	try {
        		 adminSecurityResourceFilterHandler = (SecurityHandler) ContextServiceLocator.getBean("adminSecurityResourceFilterHandler");
			} catch (Exception e) {
				adminSecurityResourceFilterHandler = new DefaultSecurityHandler();
			}
           
        }
        return adminSecurityResourceFilterHandler;
    }

    /**
     * 获取后台的handler
     *
     * @return
     */
    private SecurityHandler getShopSecurityHandler() {
        if (shopSecurityResourceFilterHandler == null) {
        	try {
        		  shopSecurityResourceFilterHandler = (SecurityHandler) ContextServiceLocator.getBean("shopSecurityResourceFilterHandler");
			} catch (Exception e) {
				shopSecurityResourceFilterHandler = new DefaultSecurityHandler();
			}
        }
        return shopSecurityResourceFilterHandler;
    }

}