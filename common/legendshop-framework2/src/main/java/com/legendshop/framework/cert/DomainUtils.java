package com.legendshop.framework.cert;

import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * 获取url中的顶级域名不包括子域名
 */
public class DomainUtils {
	
	
	/** The log. */
	private static final Logger log = LoggerFactory.getLogger(DomainUtils.class);

	private static final String RE_TOP = "[^\\.]+(\\.com\\.cn|\\.net\\.cn|\\.org\\.cn|\\.gov\\.cn|\\.com|\\.top|\\.net|\\.cn|\\.org|\\.vip|\\.store|\\.shop|\\.club|\\.xin|\\.site|\\.online|\\.ren|\\.love" +
			"|\\.ltd|\\.cc|\\.me|\\.tel|\\.mobi|\\.asia|\\.biz|\\.info|\\.name|\\.tv|\\.hk|\\.公司|\\.中国|\\.网络|\\.网店|\\.中文网)";

	//记录域名对应的一级域名
	private static ConcurrentHashMap<String, String> domainMap = new ConcurrentHashMap<String, String>();

	/**
	 * 返回顶级域名
	 * @param url
	 * @return
	 */
	public static String getTopDomainWithoutSubdomain(String url) {
		if(AppUtils.isBlank(url)){
			return null;
		}

		String shortUrl = domainMap.get(url);
		if(shortUrl != null){
			return shortUrl;
		}

		String result = url.toLowerCase();
		try {
			Pattern pattern = Pattern.compile(RE_TOP);
			Matcher matcher = pattern.matcher(result);
			while (matcher.find()) {
				result = matcher.group();
			}
		} catch (Exception e) {
			log.error("getTopDomainWithoutSubdomain error {}", e);
		}
		domainMap.put(url, result);
		return result;
	}

	public static void main(String[] args) {
		int total= 100000;
		String topDoamin = null;
		long t1 = System.currentTimeMillis();
		for (int i = 0; i < total; i++) {
			String url = "http://test.admin.legendesign.net/";
			topDoamin = getTopDomainWithoutSubdomain(url);
		}
		long t2 = System.currentTimeMillis();
		System.out.println(topDoamin + " , 耗时 = " + (t2 - t1));

	}

}
