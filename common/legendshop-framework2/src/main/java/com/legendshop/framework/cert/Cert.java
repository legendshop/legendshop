package com.legendshop.framework.cert;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cert implements Serializable{

	private static final long serialVersionUID = -5248670407088097997L;

	private String domainName;
	
	private String ip;

	private Date validateDate;

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getValidateDate() {
		return validateDate;
	}

	public void setValidateDate(Date validateDate) {
		this.validateDate = validateDate;
	}

	@Override
	public String toString() {
		return  domainName + "," + ip + "," + dateToString(validateDate);
	}
	
	private  String dateToString(Date date) {
		if(date == null){
			return "";
		}
		try{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return format.format(date);
		}catch (Exception e){
			return "";
		}
	}
	
}
