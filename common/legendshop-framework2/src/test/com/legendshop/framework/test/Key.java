package com.legendshop.framework.test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Key {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
		System.out.println("11234");
		System.out.println("GenerateKey");
		String keyFilename = "key.data";
		String algorithm = "DES";
		// 生成密匙
		SecureRandom sr = new SecureRandom();
		KeyGenerator kg = KeyGenerator.getInstance(algorithm);
		kg.init(sr);
		SecretKey key = kg.generateKey();
		
		// 把密匙数据保存到文件
		 Util.writeFile(keyFilename, key.getEncoded());

	}

}
