package com.legendshop.framework.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.legendshop.security.Util;

/**
 * 加密Jar包
 * @author Newway
 * 1.执行 jockybuild.xml的dist方法
 * 2. 修改main方法的文件夹路径
 *
 */
public class EncryptClass {
	
	private byte rawKey[];

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		EncryptClass test = new  EncryptClass();
		//encrypt_model.jar
		System.out.println();
		//test.generate("C:/encrypt_model.jar","C:/encrypt_model1.jar");
		//C:/Users/Administrator/Desktop/jar/central.jar
		test.generate("D:/Java/projects/legendshop-release-standard/legendshop_framework2/jocky/framework2.jar",
				"D:/Java/projects/legendshop-release-standard/legendshop_framework2/jocky/framework21.zip");

	}
	
	/**
	 * 生成新的包
	 * @param jarFilePath
	 * @param newJarPos
	 * @throws IOException
	 */
	private void generate(String jarFilePath, String newJarPos) throws Exception{
		File file = new File(jarFilePath);
		JarFile jarFile = new JarFile(file);
		
		//定义一个jaroutputstream流
		  JarOutputStream outPutStream=new JarOutputStream(new FileOutputStream(newJarPos));
		
		Enumeration<JarEntry> enum1 = jarFile.entries();
		while (enum1.hasMoreElements()) {
		  process(jarFile, enum1.nextElement(), outPutStream);
		}

		//最后不能忘记关闭流
		outPutStream.close();
	}
	
	 private  void process(JarFile jarFile, JarEntry entry, JarOutputStream outPutStream) throws Exception {
		    String name = entry.getName();
		    long size = entry.getSize();
		    long compressedSize = entry.getCompressedSize();
		    System.out.println(name + " " + size + " " + compressedSize);
		    
		    
		    JarEntry newEntry=new JarEntry(name);
		    outPutStream.putNextEntry(newEntry);
		    
		    if(name.endsWith(".class")){//加密class
		    	byte[] bytes = readContent(jarFile, entry);
		    	outPutStream.write(encrypt1(bytes));
		    }else{
		    	outPutStream.write(readContent(jarFile, entry));
		    }
		  }
	 
	 private byte[] readContent(JarFile jarFile, JarEntry entry) throws IOException{
		 	InputStream input = jarFile.getInputStream(entry);
		 	ByteArrayOutputStream swapStream = new ByteArrayOutputStream(); 
		 	byte[] buff = new byte[100];
		 	int rc = 0; 
		     while ((rc = input.read(buff, 0, 100))>0) {
		    	 swapStream.write(buff, 0, rc); 
		     }
		     swapStream.close();
		     input.close();
		     return swapStream.toByteArray();
	 }
	
	 /**
	  * 加密
	  * @param byets
	  */
	private byte[] encrypt(byte[] byets){
		byte[] data = new byte[byets.length];  
		for(int i = 0; i < data.length; i++){
		    data[i] =(byte)( data[i] + 1);  
		}  
		return data;
	}
	
	 /**
	  * 采用DES加密
	  * @param classData 读入类文件
	  */
	 private  byte[] encrypt1(byte[] classData) throws Exception {

		    String algorithm = "DES";  
		    // 生成密匙  
		    SecureRandom sr = new SecureRandom();  
		    byte rawKey[] =  readKey();  
		    DESKeySpec dks = new DESKeySpec(rawKey);  
		    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance( algorithm );  
		    SecretKey key = keyFactory.generateSecret(dks);  
		  
		    // 创建用于实际加密操作的Cipher对象  
		    Cipher ecipher = Cipher.getInstance(algorithm);  
		    ecipher.init(Cipher.ENCRYPT_MODE, key, sr);  
		  
		    byte encryptedClassData[] = ecipher.doFinal(classData);  //加密  
		    return encryptedClassData;
		  }
	 
	 /**
	  * 加密文件
	  * @return
	 * @throws IOException 
	  */
	 private byte[] readKey() throws IOException{
		 String keyFilename ="key.data";  
		 if(this.rawKey != null){
			 return this.rawKey;
		 }
		    this.rawKey = Util.readFile(keyFilename);  
		    return this.rawKey;
	 }
	

}
