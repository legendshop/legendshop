package com.legendshop.framework.test.modify;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
  
  
public class CjClassloader extends ClassLoader {
  
    String classpath;  
      
    Map<String, Class> loadedClassPool = new HashMap<String, Class>();  
  
    public CjClassloader(String classpath) {
        this.classpath = classpath;  
    }  
  
      
    @SuppressWarnings("unchecked")  
    @Override  
    public synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {  
        Class claz = null;  
        if (loadedClassPool.containsKey(name)) {
            claz = this.loadedClassPool.get(name);  
        } else { 
  
            try {
                if (claz == null) {  
                    claz = super.loadClass(name, false);  
                    if (claz != null) {  
                        System.out.println("系统加载成功：" + name);  
                    }  
                }  
            } catch (Exception e) {  
                System.out.println("系统无法加载：" + name);  
            } catch(ClassFormatError e1){
            	System.out.println("系统无法加载1：" + name);  
            }
              
            try {  
                if (claz == null) {  
                    claz = loadByCjClassLoader(name);  
                    if (claz != null) {  
                        System.out.println("自定义加载成功：" + name);  
                    }  
                }  
            } catch (Exception e) {  
                System.out.println("自定义无法加载：" + name);  
            }  
  
            if (claz != null) {  
                this.loadedClassPool.put(name, claz);  
            }  
  
        }  
        if (resolve) {  
            resolveClass(claz);  
        }  
        return claz;  
    }  
  
    /** 
     *  
     * 解密加载 
     *  
     *  
     * @param name 
     * @return 
     */  
    @SuppressWarnings("unchecked")  
    private Class loadByCjClassLoader(String name) {
        Class claz = null;  
        try {
            byte[] rawData = loadClassData(name);  
            if (rawData != null) {  
//                byte[] classData = decrypt(getReverseCypher(this.cjcipher.getKeycode()), rawData);  
//                classData = CipherUtil.filter(classData, this.cjcipher);  
                  
                claz = defineClass(name, rawData, 0, rawData.length);  
            }  
        } catch (Exception e) {  
            claz = null;  
        }  
        return claz;  
    }


	private byte[] loadClassData(String name) throws Exception {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream("D:/Java/Projects/Projects/legendshop-release-20160409/legendshop_central/target/classes/com/legendshop/loader/Hello.class"));  
		byte[] data = new byte[bis.available()];  
		bis.read(data);  
		bis.close();  
		for(int i = 0; i < data.length; i++){  
		    data[i] =(byte)( data[i] - 1);  
		}  
		//Class claz = defineClass("Hello", data, 0, data.length);  
		return data;
	}  
  
}  