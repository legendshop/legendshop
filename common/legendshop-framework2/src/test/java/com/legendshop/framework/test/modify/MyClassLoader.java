package com.legendshop.framework.test.modify;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class MyClassLoader extends ClassLoader {

	/**
	 * @param args
	 */
	private native void encrypt();

	public byte[] bytes;
	public String classDir;
	private String LocalName;

	private boolean Flag(FileInputStream fis, ByteArrayOutputStream bos)
			throws Exception {

		boolean Result = false;
		if (fis.read() == 0xCA)
			Result = true;
		return Result;

	}

	@SuppressWarnings("deprecation")
	@Override
	protected Class<?> findClass(String arg0) throws ClassNotFoundException {

		String name;
		if (LocalName != null)
			name = LocalName;
		else
			name = arg0;
		System.out.println("on my Findclass way");
		String ClassName = name.substring(name.lastIndexOf('.') + 1) + ".class";
		String classFileName = System.getProperty("user.dir") + "\\cn\\drawingbox\\" + ClassName;
		try {
			FileInputStream fis = new FileInputStream(classFileName);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			fis.close();
			// 这里调用JNI函数进行解码
			// System.getProperties().setProperties(String key,String value)
			// System.setProperties(arg0);
			System.load(classDir + "\\encrypt_main.dll");
			// System.loadLibrary("encrypt_main");
			MyClassLoader encypt_function = new MyClassLoader();
			encypt_function.classDir = classDir;
			encypt_function.bytes = bos.toByteArray();
			encypt_function.encrypt();

			// /////
			LocalName = null;
			return defineClass(encypt_function.bytes, 0,
					encypt_function.bytes.length);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		// TODO Auto-generated method stub

		boolean Result = false;
		String ClassName = name.substring(name.lastIndexOf('.') + 1) + ".class";
		if (ClassName.equals("Foo.class") || ClassName.equals("bar.class")) {
			String classFileName = classDir + "\\" + ClassName;
			try {
				FileInputStream fis = new FileInputStream(classFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				Result = Flag(fis, bos);
				fis.close();
				bos.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// TODO Auto-generated method stub
			if (Result == true) {
				LocalName = name;
				return super.loadClass("ThisIsJoy");
			} else {
				LocalName = null;
				return super.loadClass(name);
			}
		} else
			return super.loadClass(name);
	}

}