package com.legendshop.framework.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DomainNameExtractor {
	
   private static Pattern pattern;
	// 一级域名提取  
	private static final String RE_TOP = "(\\w*\\.?){1}\\.(com.cn|net.cn|gov.cn|org\\.nz|org.cn|com|net|org|gov|cc|biz|info|cn|co)$";  
	  
	// 二级域名提取  
	private static final String RE_TOP2 = "(\\w*\\.?){2}\\.(com.cn|net.cn|gov.cn|org\\.nz|org.cn|com|net|org|gov|cc|biz|info|cn|co)$";  
	  
	// 三级域名提取  
	private static final String RE_TOP3 = "(\\w*\\.?){3}\\.(com.cn|net.cn|gov.cn|org\\.nz|org.cn|com|net|org|gov|cc|biz|info|cn|co)$";  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	     pattern = Pattern.compile(RE_TOP , Pattern.CASE_INSENSITIVE); 
		DomainNameExtractor obj = new DomainNameExtractor();
		  // 示例  
        String url = "www.baidu.cc";  
        String res1 = obj.getTopDomain(url);  
        System.out.println(url + " ==> " + res1);  
  
        url = "ac.asd.c.sina.com.cn";  
        String res2 = obj.getTopDomain(url);  
        System.out.println(url + " ==> " + res2);  
  
        url = "whois.chinaz.com/reverse?ddlSearchMode=1";  
//        String res3 = obj.getTopDomain(url);  
//        System.out.println(url + " ==> " + res3);  
  
        url = "http://write.blog.csdn.net/";  
        String res4 = obj.getTopDomain(url);  
        System.out.println(url + " ==> " + res4);  
  
        url = "http://write.test.org.nz/";  
        String res5 = obj.getTopDomain(url);  
        System.out.println(url + " ==> " + res5);  
	}
	
    public String getTopDomain(String url) {  
        String result = url;  
        try {  
            Matcher matcher = this.pattern.matcher(url);  
            matcher.find();  
            result = matcher.group();  
        } catch (Exception e) {  
            System.out.println("[getTopDomain ERROR]====>");  
            e.printStackTrace();  
        }  
        return result;  
    }  

}
