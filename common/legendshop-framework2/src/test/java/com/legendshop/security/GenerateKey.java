package com.legendshop.security;

import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class GenerateKey {

	public static void main(String[] args) throws Exception {
		System.out.println("GenerateKey");
		// String keyFilename = args[0];
		String keyFilename = "key.data";
		String algorithm = "DES";

		// 生成密匙
		SecureRandom sr = new SecureRandom();
		KeyGenerator kg = KeyGenerator.getInstance(algorithm);
		kg.init(256,sr);
		SecretKey key = kg.generateKey();

		// 把密匙数据保存到文件
	   Util.writeFile(keyFilename, key.getEncoded());
	}
}