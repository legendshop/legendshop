package com.legendshop.web.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.WeiXinMD5Util;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/*
 '微信支付服务器签名支付请求请求类
 '============================================================================
 'api说明：
 'init(app_id, app_secret, partner_key, app_key);
 '初始化函数，默认给一些参数赋值，如cmdno,date等。
 'setKey(key_)'设置商户密钥
 'getLasterrCode(),获取最后错误号
 'GetToken();获取Token
 'getTokenReal();Token过期后实时获取Token
 'createMd5Sign(signParams);生成Md5签名
 'genPackage(packageParams);获取package包
 'createSHA1Sign(signParams);创建签名SHA1
 'sendPrepay(packageParams);提交预支付
 'getDebugInfo(),获取debug信息
 '============================================================================
 '*/
public class RequestHandler {
	
	private final static String CREATEORDERURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

	/** 商户参数 */
	private String appid;
	
	private String partnerkey;
	
	private String appsecret;
	/** 请求的参数 */
	private SortedMap<Object,Object> parameters;
	/** Token */
	private String token;

	private String charset;

	private HttpServletRequest request;

	private HttpServletResponse response;

	/**
	 * 初始构造函数。
	 * 
	 * @return
	 */
	public RequestHandler(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
		this.charset = "UTF-8";
	}
	
	public RequestHandler() {
		this.charset = "UTF-8";
	}

	/**
	 * 初始化函数。
	 */
	public void init(String token, String app_id, String app_secret,
			String partner_key) {
		this.token = token;
		this.appid = app_id;
		this.appsecret = app_secret;
		this.partnerkey = partner_key;
	}

	
	// 设置密钥

	public void setKey(String key) {
		this.partnerkey = key;
	}

	public void setPartnerkey(String partnerkey) {
		this.partnerkey = partnerkey;
	}

	

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public SortedMap<Object,Object> getParameters() {
		return parameters;
	}


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public String getPartnerkey() {
		return partnerkey;
	}
	
	/**
	 * 获取参数值
	 * 
	 * @param parameter
	 *            参数名称
	 * @return String
	 */
	public String getParameter(String parameter) {
		String s = (String) this.parameters.get(parameter);
		return (null == s) ? "" : s;
	}

	
	// 特殊字符处理
	public String UrlEncode(String src) throws UnsupportedEncodingException {
		return URLEncoder.encode(src, this.charset).replace("+", "%20");
	}

	// 获取package的签名包
	public String genPackage(SortedMap<Object,Object> packageParams)
			throws UnsupportedEncodingException {
		String sign = createSign(packageParams);

		StringBuffer sb = new StringBuffer();
		Set es = packageParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + UrlEncode(v) + "&");
		}

		// 去掉最后一个&
		String packageValue = sb.append("sign=" + sign).toString();
		// System.out.println("UrlEncode后 packageValue=" + packageValue);
		return packageValue;
	}

	/**
	 * 创建md5摘要,规则是:按参数名称a-z排序,遇到空值的参数不参加签名。
	 */
	public String createSign(SortedMap packageParams) {
		StringBuffer sb = new StringBuffer();
		Set es = packageParams.entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = String.valueOf(entry.getKey());
			String v = String.valueOf(entry.getValue());
			if (null != v && !"".equals(v) && !"sign".equals(k)
					&& !"key".equals(k)) {
				sb.append(k + "=" + v + "&");
			}
		}
		/** 支付密钥必须参与加密，放在字符串最后面 */
		sb.append("key=" + this.getPartnerkey());
		String sign = WeiXinMD5Util.encode(sb.toString()).toUpperCase();
		/*System.out.println(MD5Util.toMD5(sb.toString()).toUpperCase());
		System.out.println(WeiXinMD5Util.encode(sb.toString()).toUpperCase());*/
		return sign;
	}
	

	/**
	 *  TODO JDK8 有问题
	 * @param params
	 * @return
	 */
	public Map<String, String>  getPrepayId(SortedMap<Object,Object> params) {
		StringBuffer sb = new StringBuffer();
		sb.append("<xml>");
		Set<Entry<Object, Object>> es = params.entrySet();
		Iterator<Entry<Object, Object>> it = es.iterator();
		while (it.hasNext()) {
			Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) it
					.next();
			String k = String.valueOf(entry.getKey());
			String v = String.valueOf(entry.getValue());
			if ("attach".equalsIgnoreCase(k) || "body".equalsIgnoreCase(k)
					|| "sign".equalsIgnoreCase(k)) {
				sb.append("<" + k + ">" + "<![CDATA[" + v + "]]></" + k + ">");
			} else {
				sb.append("<" + k + ">" + v + "</" + k + ">");
			}
		}
	    sb.append("</xml>");
		String prepay_id = "";
		String str = null;
		try {
			str = new String(sb.toString().getBytes("UTF-8"),"ISO8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, String> map = WeiXinUtil.httpPostXML(CREATEORDERURL,str );
		System.out.println("WX请求统一支付接口的返回结果result:" + JSONUtil.getJson(map));
		/*if (map != null) {
			prepay_id = map.get("prepay_id");
		}
		System.out.println("WX请求统一支付接口的返回结果result:" + prepay_id);*/
		return map;
	}
	
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		String str1= "sdfsf鬼地方郭德纲梵蒂冈";
		String str = new String(str1.getBytes("UTF-8"),"ISO8859-1");
		System.out.println(str);
	}
	
	/**
	 * TODO 
	 * java  sha1加密结果 和官方给的算法验证结果不同 
	 * @param parameters
	 * @return
	 */
	public String createSign_wx_config(SortedMap<Object, Object> parameters) {
		StringBuffer sb = new StringBuffer();
		Set<Entry<Object, Object>> es = parameters.entrySet();
		Iterator<Entry<Object, Object>> it = es.iterator();
		while (it.hasNext()) {
			Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) it
					.next();
			String k = (String) entry.getKey();
			Object v = entry.getValue();
			if (null != v && !"".equals(v)) {
				sb.append(k + "=" + v + "&");
			}
		}
		String params = sb.substring(0, sb.lastIndexOf("&"));
		String sign =AppUtils.encodePassword(params, "sha1");
		return sign;
	}
	
	public String createSign_wx_config(String jsapi_ticket,String noncestr,long timestamp,String url) {
		String params="jsapi_ticket="+jsapi_ticket+"&noncestr="+noncestr+"&timestamp="+timestamp+"&url="+url;
		String sign =AppUtils.encodePassword(params, "sha1");
		return sign;
	}
	
	
}
