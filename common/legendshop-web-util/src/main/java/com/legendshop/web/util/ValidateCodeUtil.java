/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.web.util;

import javax.servlet.http.HttpSession;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.dto.RandNumDto;
import com.legendshop.util.AppUtils;

/**
 * 验证码  工具类
 *
 */
public class ValidateCodeUtil {
	
	private static CacheClient cacheClient;
	
	private static SystemParameterUtil systemParameterUtil;
	
	static {
		cacheClient = ContextServiceLocator.getBean(CacheClient.class, "redisCacheClient");
		
		systemParameterUtil = ContextServiceLocator.getBean(SystemParameterUtil.class, "systemParameterUtil");
	}
	
	/**
	 * 验证用户输入的 随机验证码  是否正确，验证后清除
	 * @param randNum 用户输入的验证码
	 * @param session 用户的session
	 * @return 验证正确 返回true 否则返回false
	 */
	public static boolean validateCode(String randNum,HttpSession session) {
		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseValidateCode()) {
			return true;
		}
		if(randNum==null || session==null){
			return false;
		}
		RandNumDto code = (RandNumDto)session.getAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY);
		if(code==null){
			return false;
		}
		session.removeAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY);
		return code.getRandomString().equals(randNum.toLowerCase());
	}
	
	/**
	 * 验证用户输入的 随机验证码  是否正确，验证后不清除, 验证超过5次则清除,防止攻击
	 * @param randNum 用户输入的验证码
	 * @param session 用户的session
	 * @return 验证正确 返回true 否则返回false
	 */
	public synchronized static boolean validateCodeAndKeep(String randNum,HttpSession session){

		//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseValidateCode()) {
			return true;
		}
		
		if (session == null || randNum==null) {
			return false;
		}
		RandNumDto result = null;
		try {
		    result =  (RandNumDto)session.getAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY);
			if(result!=null && result.getRandomString().equals(randNum.toLowerCase())){
				//同一验证码 只能验证5次
				int times = result.getNum();
				if(times>5){
					session.removeAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY);
					return false;
				}else{
					 result.addNum();
					 session.setAttribute(AttributeKeys.RANDOM_VALIDATE_CODE_KEY,   result);
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 验证用户输入的 随机验证码  是否正确，验证后清除 (从redis去验证)
	 * @param randNum 用户输入的验证码
	 * @return 验证正确 返回true 否则返回false
	 */
	public static boolean validateCodeFromCache(String randNum) {
		
		if(AppUtils.isBlank(randNum)){
			return false;
		}
		
		RandNumDto code = (RandNumDto)cacheClient.get(AttributeKeys.RANDOM_VALIDATE_CODE_KEY + ":" + randNum);
		if(code == null){
			return false;
		}
		
		cacheClient.delete(AttributeKeys.RANDOM_VALIDATE_CODE_KEY);
		
		return code.getRandomString().equals(randNum.toLowerCase());
	}
}